#include <iostream>
#include <cstdlib>
#include <functional>
#include <chrono>
#include <fmt/core.h>

#define DEDUCING_THIS 0

int main(int, char * *)
{
    // Parameters ..............................................................
    size_t number_of_trials = 500;
    int fib_query = 35;
    
    // Factorial tests .........................................................
    auto factorialA = [](int value) -> int
    {
        //auto inner = [](const auto &self, int val) -> int
        auto inner = [](auto &&self, int val) -> int
        {
            return (val <= 1)?1:(val * self(self, val - 1));
        };
        return inner(inner, value);
    };
    // Not added gcc yet?
#if DEDUCING_THIS
    auto factorialB = [](this const auto &&self, int val) -> int
    {
        return (val <= 1)?1:(val * self(val - 1));
    };
#endif
    std::function<int(int)> factorialC = [&](int val) -> int
    {
        return (val <= 1)?1:(val * factorialC(val - 1));
    };
    std::cout << factorialA(5) << '\n';
#if DEDUCING_THIS
    std::cout << factorialB(5) << '\n';
#endif
    std::cout << factorialC(5) << '\n';
    
    // Fibonacci series ........................................................
    int result = -1;
    const auto startA = std::chrono::steady_clock::now();
    for (size_t trial = 0; trial < number_of_trials; ++trial)
    {
        auto fibA = [](int value) -> int
        {
            auto inner = [](auto &&self, int val) -> int
            {
                return (val <= 2)?1:(self(self, val - 1) + self(self, val - 2));
            };
            return inner(inner, value);
        };
        result = fibA(fib_query);
    }
    const auto endA = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsedA{endA - startA};
    fmt::print("[IMPLEMENTATION A] Fibonacci({}): {} in {} seconds ({} ms per trial.)\n",
            fib_query, result, elapsedA.count(), 1000 * elapsedA.count() / static_cast<double>(number_of_trials));
    
    result = -1;
    const auto startC = std::chrono::steady_clock::now();
    for (size_t trial = 0; trial < number_of_trials; ++trial)
    {
        std::function<int(int)> fibC = [&](int val) -> int
        {
            return (val <= 2)?1:(fibC(val - 1) + fibC(val - 2));
        };
        result = fibC(fib_query);
    }
    const auto endC = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsedC{endC - startC};
    fmt::print("[IMPLEMENTATION C] Fibonacci({}): {} in {} seconds ({} ms per trial.)\n",
            fib_query, result, elapsedC.count(), 1000 * elapsedC.count() / static_cast<double>(number_of_trials));
    
    return EXIT_SUCCESS;
}

