#include <string>
#include <array>

// If you call std::move in the constructor of 'data', the move will not effect
// the std::array because it is stack allocated, but it will effect all the
// std::strings allocated in the array.
// If the class contained in the std::array doesn't have a move constructor
// on their own, the std::move doesn't have any effect. For example, the compiler
// generates exactly the same code (with or without std::move) when the array is
// an array of int's.
template <typename T>
struct Holder
{
#if 1
    Holder(std::array<T, 42> data_) : data(std::move(data_)) {}
#else
    Holder(std::array<T, 42> data_) : data(data_) {}
#endif
private:
    std::array<T, 42> data;
};

int main()
{
#if 0
    Holder h(std::array<std::string, 42>{});
#else
    Holder h(std::array<int, 42>{});
#endif
    return 0;
}


