#include <iostream>
// THIS IS FROM C++ WEEKLY 318: https://www.youtube.com/watch?v=15U4qutsPGk

int funct1(int a, int b, int c)
{
    return a + b + c;
}

template <typename Callable, typename... Params>
auto curry_old(Callable f,   //< Function parameter.
               Params... ps) //< Variadic parameters.
{
    // First, checks if the function with the binded parameters is a valid callable function.
    // If it is, then return the result of the call of the function with all the given
    // parameters.
    if constexpr(requires{f(ps...);})
    {
        return f(ps...);
    }
    else
    {
        // Otherwise,
        // return a lambda function binded to the function parameter (f) and a zero set of
        // variadic template parameters (ps) and can take any number of subsequent
        // parameters (qs).
        return [f, ps...](auto... qs)
        {
            // If "f(ps..., qs...)" is callable with the original set of parameters followed
            // by the next set of parameters, then we call it and return the value.
            if constexpr(requires{f(ps..., qs...);})
            {
                return f(ps..., qs...);
            }
            // Otherwise we call curry again with the original set of parameters with the new
            // set of parameters. This allows you to 'curry' a partial function in a nested way.
            else
            {
                return curry(f, ps..., qs...);
            }
        };
    }
}

// Everything is copied?? A lot of overhead with large objects?!?!
template <typename Callable, typename... Params>
auto curry(Callable f,   //< Function parameter.
           Params... ps) //< Variadic parameters.
{
    // First, checks if the function with the binded parameters is a valid callable function.
    // If it is, then return the result of the call of the function with all the given
    // parameters.
    if constexpr(requires{f(ps...);})
    {
        return f(ps...);
    }
    else
    {
        // return a lambda function binded to the function parameter (f) and a zero set of
        // variadic template parameters (ps) and can take any number of subsequent
        // parameters (qs). This function simply returns 'curry' to with all parameters,
        // as the recursive call will generate the result when all parameters are available
        // or return a lambda function that expects more parameters when the function call
        // is not valid.
        return [f, ps...](auto... qs) {
            return curry(f, ps..., qs...);
        };
    }
}


int main()
{
    const auto bound_5 = curry(funct1, 5);
    const auto bound_5_7 = curry(bound_5, 7);
    std::cout << "5+7+3=15 --> " << bound_5_7(3) << '\n';
    std::cout << "Range add:\n";
    for (unsigned int i = 0; i < 10; ++i)
        std::cout << "5+7+" << i << "=" << 12 + i << " --> " << bound_5_7(i) << '\n';
    const auto bound = curry(funct1, 1, 2);
    const auto bound2 = curry(funct1)(5)(2);
    return curry(funct1, 1, 2, 3) /*6*/ + bound(3) /*6*/ + bound2(3) /*10*/;
}

