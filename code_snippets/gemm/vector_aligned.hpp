#ifndef __VECTOR_ALIGNED_HEADER_FILE__
#define __VECTOR_ALIGNED_HEADER_FILE__

#include <cstring>
#include <memory>

template <typename T, size_t ALIGNMENT_SIZE>
class Vector
{
public:
    // -[ CONSTRUCTORS, DESTRUCTOR AND ASSIGNATION OPERATOR ]-------------------
    Vector(void) : m_data(nullptr), m_size(0) {}
    Vector(size_t size) :
        m_raw_data(nullptr),
        m_data(nullptr),
        m_size(0),
        m_raw_size(0)
    {
        reserve(size);
    }
    Vector(size_t size, const T &init_value) :
        m_raw_data(nullptr),
        m_data(nullptr),
        m_size(0),
        m_raw_size(0)
    {
        reserve(size);
        if (m_data != nullptr)
            for (size_t i = 0; i < m_size; ++i)
                m_data[i] = init_value;
    }
    Vector(const Vector<T, ALIGNMENT_SIZE> &other) :
        m_raw_data(nullptr),
        m_data(nullptr),
        m_size(0),
        m_raw_size(0)
    {
        reserve(other.m_size);
        if (m_data != nullptr)
            for (size_t i = 0; i < other.m_size; ++i)
                m_data[i] = other.m_data[i];
    }
    Vector(Vector<T, ALIGNMENT_SIZE> &&other) :
        m_raw_data(std::exchange(other.m_raw_data, nullptr)),
        m_data(std::exchange(other.m_data, nullptr)),
        m_size(std::exchange(other.m_size, 0)),
        m_raw_size(std::exchange(other.m_raw_size, 0)) {}
    Vector<T, ALIGNMENT_SIZE>& operator=(const Vector<T, ALIGNMENT_SIZE> &other)
    {
        reserve(other.m_size);
        if (m_data != nullptr)
            for (size_t i = 0; i < other.m_size; ++i)
                m_data[i] = other.m_data[i];
        return *this;
    }
    Vector<T, ALIGNMENT_SIZE>& operator=(Vector<T, ALIGNMENT_SIZE> &&other)
    {
        m_raw_data = std::exchange(other.m_raw_data, nullptr);
        m_data = std::exchange(other.m_data, nullptr);
        m_size = std::exchange(other.m_size, 0);
        m_raw_size = std::exchange(other.m_raw_size, 0);
        return *this;
    }
    ~Vector(void)
    {
        delete [] m_raw_data;
    }
    
    // -[ ACCESS FUNCTIONS ]----------------------------------------------------
    T * data(void) { return m_data; }
    const T * data(void) const { return m_data; }
    size_t size(void) const { return m_size; }
    const T& operator[](size_t index) const { return m_data[index]; }
    T& operator[](size_t index) { return m_data[index]; }
    constexpr size_t alignmentSize(void) const { return ALIGNMENT_SIZE; }
    
    // -[ MEMORY ALLOCATION FUNCTION ]------------------------------------------
    void reserve(size_t size)
    {
        size_t raw_size = size + ALIGNMENT_SIZE - 1;
        T * raw_data = new T[raw_size];
        void * ptr = static_cast<void *>(raw_data);
        size_t storage_size = raw_size * sizeof(T);
        T * data = static_cast<T *>(std::align(ALIGNMENT_SIZE, size * sizeof(T), ptr, storage_size));
        if (data == nullptr)
        {
            delete [] m_raw_data;
            m_raw_data = m_data = nullptr;
            m_size = m_raw_size = 0;
            return;
        }
        std::memmove(data, m_data, sizeof(T) * std::min(m_size, size));
        delete [] m_raw_data;
        m_raw_data = raw_data;
        m_data = data;
        m_size = size;
        m_raw_size = raw_size;
    }
    
private:
    T * m_raw_data;
    T * m_data;
    size_t m_size;
    size_t m_raw_size;
};

#endif
