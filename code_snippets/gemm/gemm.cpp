#include <cstdlib>
#include <iostream>
#include <vector>
#include <array>
#include <tuple>
#include <chrono>
#include <utility>
#include <fmt/core.h>
#include <random>
#include <stdexcept>
#include <mkl.h>
#include <omp.h>
#include "vector_aligned.hpp"
#include "matrix.hpp"
#include "dot.hpp"

#if 1
void GEMM_MKL(const Matrix<double> &left,
              const Matrix<double> &right,
              Matrix<double> &result)
{
    if (left.columns() != right.rows())
        throw std::invalid_argument(fmt::format("Wrong geometry: matrices are ({}x{})*({}x{}) where left columns ({}) is different than right rows ({}).", left.rows(), left.columns(), right.rows(), right.columns(), left.columns(), right.rows()));
    result.set(left.rows(), right.columns(), zeros);
    const int m = static_cast<int>(left.rows());
    const int n = static_cast<int>(right.columns());
    const int k = static_cast<int>(right.rows());
    cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, 
                m, n, k, 1.0, left(), m, right(), k, 0, result(), m);
}
#endif

#if 1
void GEMM_A(const Matrix<double> &left,
            const Matrix<double> &right,
            Matrix<double> &result)
{
    if (left.columns() != right.rows())
        throw std::invalid_argument(fmt::format("Wrong geometry: matrices are ({}x{})*({}x{}) where left columns ({}) is different than right rows ({}).", left.rows(), left.columns(), right.rows(), right.columns(), left.columns(), right.rows()));
    result.set(left.rows(), right.columns(), zeros);
    for (size_t c = 0; c < right.columns(); ++c)
        for (size_t r = 0; r < left.rows(); ++r)
            for (size_t i = 0; i < left.columns(); ++i)
                result(r, c) += left(r, i) * right(i, c);
}
#endif

#if 1
void GEMM_B(const Matrix<double> &left,
            const Matrix<double> &right,
            Matrix<double> &result)
{
    if (left.columns() != right.rows())
        throw std::invalid_argument(fmt::format("Wrong geometry: matrices are ({}x{})*({}x{}) where left columns ({}) is different than right rows ({}).", left.rows(), left.columns(), right.rows(), right.columns(), left.columns(), right.rows()));
    result.set(left.rows(), right.columns(), zeros);
    double * __restrict result_ptr = result();
    for (size_t c = 0; c < right.columns(); ++c)
    {
        for (size_t r = 0; r < left.rows(); ++r, ++result_ptr)
        {
            const double * __restrict left_ptr = &left(r, 0);
            const double * __restrict right_ptr = &right(0, c);
            for (size_t i = 0; i < left.columns(); ++i, left_ptr += left.rows(), ++right_ptr)
            {
                *result_ptr += *left_ptr * *right_ptr;
            }
        }
    }
}
#endif

#if 1
void GEMM_C(const Matrix<double> &left,
            const Matrix<double> &right,
            Matrix<double> &result)
{
    constexpr size_t block_size = 16;
    if (left.columns() != right.rows())
        throw std::invalid_argument(fmt::format("Wrong geometry: matrices are ({}x{})*({}x{}) where left columns ({}) is different than right rows ({}).", left.rows(), left.columns(), right.rows(), right.columns(), left.columns(), right.rows()));
    result.set(left.rows(), right.columns(), zeros);
    auto blockMult = [&](size_t row0, size_t row1, size_t col0, size_t col1, size_t idx0, size_t idx1) -> void
    {
        for (size_t c = col0; c < col1; ++c)
        {
            double * __restrict result_ptr = &result(row0, c);
            for (size_t r = row0; r < row1; ++r, ++result_ptr)
            {
                const double * __restrict left_ptr  = &left(r, idx0);
                const double * __restrict right_ptr = &right(idx0, c);
                for (size_t i = idx0; i < idx1; ++i, left_ptr += left.rows(), ++right_ptr)
                {
                    *result_ptr += *left_ptr * *right_ptr;
                }
            }
        }
    };
    
    for (size_t block_i = 0; block_i < left.columns(); block_i += block_size)
        for (size_t block_row = 0; block_row < left.rows(); block_row += block_size)
            for (size_t block_column = 0; block_column < right.columns(); block_column += block_size)
                blockMult(block_row   , std::min(left.rows()    , block_row    + block_size),
                          block_column, std::min(right.columns(), block_column + block_size),
                          block_i     , std::min(left.columns() , block_i      + block_size));
}
#endif

#if 1
void GEMM_D(const Matrix<double> &left,
            const Matrix<double> &right,
            Matrix<double> &result)
{
    constexpr size_t block_size = 16;
    alignas(block_size * sizeof(double)) std::vector<std::array<std::array<double, block_size>, block_size> > buffer(32);
    
    if (left.columns() != right.rows())
        throw std::invalid_argument(fmt::format("Wrong geometry: matrices are ({}x{})*({}x{}) where left columns ({}) is different than right rows ({}).", left.rows(), left.columns(), right.rows(), right.columns(), left.columns(), right.rows()));
    result.set(left.rows(), right.columns(), zeros);
    auto blockMult = [&](size_t row0, size_t row1, size_t col0, size_t col1, size_t idx0, size_t idx1) -> void
    {
        const int thread_id = omp_get_thread_num();
        const size_t idx_n = idx1 - idx0;
        [[maybe_unused]] Dot dot(idx_n);
        for (size_t c = col0; c < col1; ++c)
        {
            double * __restrict result_ptr = &result(row0, c);
            for (size_t r = row0; r < row1; ++r, ++result_ptr)
            {
                const double * __restrict left_ptr  = &buffer[thread_id][r - row0][0];
                const double * __restrict right_ptr = &right(idx0, c);
#if 1
                *result_ptr += dot.dotSIMD(left_ptr, right_ptr, idx_n);
#else
                double partial_result = 0;
                    //#pragma omp simd
                for (size_t i = 0; i < idx_n; ++i)
                    partial_result += left_ptr[i] * right_ptr[i];
                *result_ptr += partial_result;
#endif
            }
        }
    };
    std::vector<std::tuple<size_t, size_t> > starting_points;
    for (size_t row0 = 0; row0 < left.rows(); row0 += block_size)
        for (size_t col0 = 0; col0 < right.columns(); col0 += block_size)
            starting_points.push_back({row0, col0});
    
    for (size_t idx0 = 0; idx0 < left.columns(); idx0 += block_size)
    {
        const size_t idx1 = std::min(left.columns(), idx0 + block_size);
        size_t prev_row = left.rows() + 1;
        #pragma omp parallel for firstprivate(prev_row)
        for (auto [row0, col0] : starting_points)
        {
            const size_t row1 = std::min(left.rows(), row0 + block_size);
            const size_t col1 = std::min(right.columns(), col0 + block_size);
            const int thread_id = omp_get_thread_num();
            if (prev_row != row0)
            {
                for (size_t row = row0; row < row1; ++row)
                    for (size_t col = idx0; col < idx1; ++col)
                        buffer[thread_id][row - row0][col - idx0] = left(row, col);
                prev_row = row0;
            }
            blockMult(row0, row1, col0, col1, idx0, idx1);
        }
    }
}
#endif

#if 1
// Todo: Nobody said that the cached blocks have to be a square, we should try
//       a rectangular region with less columns than rows.
void GEMM_E(const Matrix<double> &left,
            const Matrix<double> &right,
            Matrix<double> &result)
{
    if (left.columns() != right.rows())
        throw std::invalid_argument(fmt::format("Wrong geometry: matrices are ({}x{})*({}x{}) where left columns ({}) is different than right rows ({}).", left.rows(), left.columns(), right.rows(), right.columns(), left.columns(), right.rows()));
    
    constexpr int block_size = 128;
    const int nrows = static_cast<int>(left.rows());
    const int ncols = static_cast<int>(right.columns());
    const int nprod = static_cast<int>(left.columns());
    struct Position
    {
        int row0;
        int row1;
        int col0;
        int col1;
    };
    std::vector<Position> starting_positions;
    Dot dot[2] = {Dot(block_size), Dot(nprod % block_size)};
    
    result.set(nrows, ncols, zeros);
    for (int row0 = 0; row0 < nrows; row0 += block_size)
    {
        const int row1 = std::min(nrows, row0 + block_size);
        for (int col0 = 0; col0 < ncols; col0 += block_size)
            starting_positions.push_back({row0, row1,
                                          col0, std::min(ncols, col0 + block_size)});
    }
    
    for (int idx0 = 0; idx0 < nprod; idx0 += block_size)
    {
        const int idx1 = std::min(nprod, idx0 + block_size),
                  idxn = idx1 - idx0;
        int prev_row = -1;
        #pragma omp parallel for firstprivate(prev_row)
        for (const auto &position : starting_positions)
        {
            alignas(block_size * sizeof(double)) double buffer[block_size][block_size];
            //double buffer[block_size][block_size];
            if (prev_row != position.row0)
            {
                for (int row = position.row0; row < position.row1; ++row)
                    for (int col = idx0; col < idx1; ++col)
                        buffer[row - position.row0][col - idx0] = left(row, col);
                prev_row = position.row0;
            }
            for (int c = position.col0; c < position.col1; ++c)
            {
                double * __restrict result_ptr = result(c);
                for (int r = position.row0; r < position.row1; ++r)
                {
                    const double * __restrict left_ptr  = &buffer[r - position.row0][0];
                    const double * __restrict right_ptr = &right(idx0, c);
                    result_ptr[r] += dot[idxn != block_size].dotSIMD<DotMode::Avx512>(left_ptr, right_ptr, idxn);
                }
            }
        }
    }
}
#endif

#if 1
void GEMM_F(const MatrixAligned<double> &left,
            const MatrixAligned<double> &right,
            MatrixAligned<double> &result)
{
    if (left.columns() != right.rows())
        throw std::invalid_argument(fmt::format("Wrong geometry: matrices are ({}x{})*({}x{}) where left columns ({}) is different than right rows ({}).", left.rows(), left.columns(), right.rows(), right.columns(), left.columns(), right.rows()));
    
    constexpr int block_size = 128;
    const int nrows = static_cast<int>(left.rows());
    const int ncols = static_cast<int>(right.columns());
    const int nprod = static_cast<int>(left.columns());
    struct Position
    {
        int row0;
        int row1;
        int col0;
        int col1;
    };
    std::vector<Position> starting_positions;
    Dot dot[2] = {Dot(block_size), Dot(nprod % block_size)};
    
    result.set(nrows, ncols, zeros);
    for (int row0 = 0; row0 < nrows; row0 += block_size)
    {
        const int row1 = std::min(nrows, row0 + block_size);
        for (int col0 = 0; col0 < ncols; col0 += block_size)
            starting_positions.push_back({row0, row1,
                                          col0, std::min(ncols, col0 + block_size)});
    }
    
    for (int idx0 = 0; idx0 < nprod; idx0 += block_size)
    {
        const int idx1 = std::min(nprod, idx0 + block_size),
                  idxn = idx1 - idx0;
        int prev_row = -1;
        #pragma omp parallel for firstprivate(prev_row)
        for (const auto &position : starting_positions)
        {
            alignas(block_size * sizeof(double)) double buffer[block_size][block_size];
            if (prev_row != position.row0)
            {
                for (int row = position.row0; row < position.row1; ++row)
                    for (int col = idx0; col < idx1; ++col)
                        buffer[row - position.row0][col - idx0] = left(row, col);
                prev_row = position.row0;
            }
            for (int c = position.col0; c < position.col1; ++c)
            {
                double * __restrict result_ptr = result(c);
                for (int r = position.row0; r < position.row1; ++r)
                {
                    const double * __restrict left_ptr  = &buffer[r - position.row0][0];
                    const double * __restrict right_ptr = &right(idx0, c);
                    result_ptr[r] += dot[idxn != block_size].dotSIMD<DotMode::Avx512 | DotMode::Aligned>(left_ptr, right_ptr, idxn);
                }
            }
        }
    }
}
#endif

#if 1
void GEMM_H(const MatrixAligned<double> &left,
            const MatrixAligned<double> &right,
            MatrixAligned<double> &result)
{
    if (left.columns() != right.rows())
        throw std::invalid_argument(fmt::format("Wrong geometry: matrices are ({}x{})*({}x{}) where left columns ({}) is different than right rows ({}).", left.rows(), left.columns(), right.rows(), right.columns(), left.columns(), right.rows()));
    
    const int nrows = static_cast<int>(left.rows());
    const int ncols = static_cast<int>(right.columns());
    const int nprod = static_cast<int>(left.columns());
    Dot dot(nprod);
    Vector<double, 64> current_row(nprod);
    
    result.set(nrows, ncols, zeros);
    #pragma omp parallel for firstprivate(current_row)
    for (int row = 0; row < nrows; ++row)
    {
        for (int i = 0; i < nprod; ++i)
            current_row[i] = left(row, i);
        for (int col = 0; col < ncols; ++col)
            result(row, col) = dot.dotSIMD<DotMode::Avx512 | DotMode::Aligned>(current_row.data(), right(col), nprod);
    }
}
#endif

#if 1
void GEMM_I(const MatrixAligned<double> &left,
            const MatrixAligned<double> &right,
            MatrixAligned<double> &result)
{
    if (left.columns() != right.rows())
        throw std::invalid_argument(fmt::format("Wrong geometry: matrices are ({}x{})*({}x{}) where left columns ({}) is different than right rows ({}).", left.rows(), left.columns(), right.rows(), right.columns(), left.columns(), right.rows()));
    
    constexpr int block_columns = 256;
    constexpr int block_rows = 16;
    const int nrows = static_cast<int>(left.rows());
    const int ncols = static_cast<int>(right.columns());
    const int nprod = static_cast<int>(left.columns());
    struct Position
    {
        int row0;
        int row1;
        int col0;
        int col1;
    };
    std::vector<Position> starting_positions;
    Dot dot[2] = {Dot(block_columns), Dot(nprod % block_columns)};
    
    result.set(nrows, ncols, zeros);
    for (int row0 = 0; row0 < nrows; row0 += block_rows)
    {
        const int row1 = std::min(nrows, row0 + block_rows);
        for (int col0 = 0; col0 < ncols; col0 += block_columns)
            starting_positions.push_back({row0, row1,
                                          col0, std::min(ncols, col0 + block_columns)});
    }
    
    for (int idx0 = 0; idx0 < nprod; idx0 += block_columns)
    {
        const int idx1 = std::min(nprod, idx0 + block_columns),
                  idxn = idx1 - idx0;
        int prev_row = -1;
        #pragma omp parallel for firstprivate(prev_row)
        for (const auto &position : starting_positions)
        {
            alignas(block_columns * sizeof(double)) double buffer[block_rows][block_columns];
            if (prev_row != position.row0)
            {
                for (int row = position.row0; row < position.row1; ++row)
                    for (int col = idx0; col < idx1; ++col)
                        buffer[row - position.row0][col - idx0] = left(row, col);
                prev_row = position.row0;
            }
            for (int c = position.col0; c < position.col1; ++c)
            {
                double * __restrict result_ptr = result(c);
                const double * __restrict right_ptr = &right(idx0, c);
                for (int r = position.row0; r < position.row1; ++r)
                {
                    const double * __restrict left_ptr  = &buffer[r - position.row0][0];
                    result_ptr[r] += dot[idxn != block_columns].dotSIMD<DotMode::Avx512 | DotMode::Aligned>(left_ptr, right_ptr, idxn);
                }
            }
        }
    }
}
#endif

double squaredError(const auto &first, const auto &second)
{
    double error = 0;
    if ((first.rows() != second.rows()) || (first.columns() != second.columns()))
        throw std::invalid_argument(fmt::format("Both matrices must have same geometry, ({}x{}) != ({}x{}), to compute the sum of the squared error between its elements.", first.rows(), first.columns(), second.rows(), second.columns()));
    for (size_t row = 0; row < first.rows(); ++row)
        for (size_t column = 0; column < first.columns(); ++column)
            error += (first(row, column) - second(row, column))
                   * (first(row, column) - second(row, column));
    return error;
}

int main(int, char * *)
{
    //[[maybe_unused]] constexpr size_t size = 1500;
    //[[maybe_unused]] constexpr size_t size = 1000;
    [[maybe_unused]] constexpr size_t size = 500;
    //[[maybe_unused]] constexpr size_t size = 1000;
    //[[maybe_unused]] constexpr size_t size = 5;
    auto elapsedTimeMilliseconds = [](auto start, auto end) -> double
    {
        return std::chrono::duration<double>{end - start}.count() * 1'000;
    };
    
    Matrix<double> a(size, size, uniform(-10, 10)), b(size, size, uniform(-10, 10));
    //Matrix<double> a(5, 7, uniform(-10, 10)), b(7, 3, uniform(-10, 10));
    //Matrix<double> a(3, 5, ones), b(5, 2, ones);
    //Matrix<double> a(4, 5, uniform(-10, 10)), b(5, 5, eye);
    Matrix<double> r_mkl, r_a, r_b, r_c, r_d, r_e;
    double time_mkl, time_baseline;
    MatrixAligned<double> align_a(a), align_b(b), align_r_f, align_r_h, align_r_i;
    
    auto log = [&time_mkl,&time_baseline,&r_mkl](std::string method_name,
                                 const auto &r,
                                 double time) -> void
    {
        fmt::print(" - METHOD {}: error={:.3f} | runtime={:.2f}ms (MKL: {:.2f}x {} || Vanilla: {:.2f}x {}).\n",
                method_name, squaredError(r_mkl, r), time,
                std::max(time, time_mkl) / std::min(time, time_mkl),
                ((time > time_mkl)?"slower":"faster"),
                std::max(time, time_baseline) / std::min(time, time_baseline),
                ((time > time_baseline)?"slower":"faster"));
    };
    try
    {
        constexpr size_t repetitions_slower = 10;
        constexpr size_t repetitions = 100;
        for (size_t init_prerun = 0; init_prerun < 25; ++init_prerun)
            GEMM_MKL(a, b, r_mkl);
        auto start_timer = std::chrono::steady_clock::now();
        for (size_t repeat = 0; repeat < repetitions; ++repeat)
            GEMM_MKL(a, b, r_mkl);
        auto end_timer = std::chrono::steady_clock::now();
        time_mkl = elapsedTimeMilliseconds(start_timer, end_timer) / static_cast<double>(repetitions);
        fmt::print("[RESULT] MKL OUTPUT MATRIX ({}x{})*({}x{})=({}x{}) "
                   "(Computed in {:.2f}ms):\n",
                a.rows(), a.columns(), b.rows(), b.columns(),
                r_mkl.rows(), r_mkl.columns(), time_mkl);
        fmt::print("[RESULT] RUNTIME AND SUM SQUARED ERROR VS MKL RESULT:\n");
        // .....................................................................
        start_timer = std::chrono::steady_clock::now();
        for (size_t repeat = 0; repeat < repetitions_slower; ++repeat)
            GEMM_A(a, b, r_a);
        end_timer = std::chrono::steady_clock::now();
        double time_a = elapsedTimeMilliseconds(start_timer, end_timer) / static_cast<double>(repetitions_slower);
        time_baseline = time_a;
        log("A", r_a, time_a);
        // .....................................................................
        start_timer = std::chrono::steady_clock::now();
        for (size_t repeat = 0; repeat < repetitions_slower; ++repeat)
            GEMM_B(a, b, r_b);
        end_timer = std::chrono::steady_clock::now();
        double time_b = elapsedTimeMilliseconds(start_timer, end_timer) / static_cast<double>(repetitions_slower);
        log("B", r_b, time_b);
        // .....................................................................
        start_timer = std::chrono::steady_clock::now();
        for (size_t repeat = 0; repeat < repetitions_slower; ++repeat)
            GEMM_C(a, b, r_c);
        end_timer = std::chrono::steady_clock::now();
        double time_c = elapsedTimeMilliseconds(start_timer, end_timer) / static_cast<double>(repetitions_slower);
        log("C", r_c, time_c);
        // .....................................................................
        start_timer = std::chrono::steady_clock::now();
        for (size_t repeat = 0; repeat < repetitions; ++repeat)
            GEMM_D(a, b, r_d);
        end_timer = std::chrono::steady_clock::now();
        double time_d = elapsedTimeMilliseconds(start_timer, end_timer) / static_cast<double>(repetitions);
        log("D", r_d, time_d);
        // .....................................................................
        start_timer = std::chrono::steady_clock::now();
        for (size_t repeat = 0; repeat < repetitions; ++repeat)
            GEMM_E(a, b, r_e);
        end_timer = std::chrono::steady_clock::now();
        double time_e = elapsedTimeMilliseconds(start_timer, end_timer) / static_cast<double>(repetitions);
        log("E", r_e, time_e);
        // .....................................................................
        start_timer = std::chrono::steady_clock::now();
        for (size_t repeat = 0; repeat < repetitions; ++repeat)
            GEMM_F(align_a, align_b, align_r_f);
        end_timer = std::chrono::steady_clock::now();
        double time_f = elapsedTimeMilliseconds(start_timer, end_timer) / static_cast<double>(repetitions);
        log("F", align_r_f, time_f);
        // .....................................................................
        start_timer = std::chrono::steady_clock::now();
        for (size_t repeat = 0; repeat < repetitions; ++repeat)
            GEMM_H(align_a, align_b, align_r_h);
        end_timer = std::chrono::steady_clock::now();
        double time_h = elapsedTimeMilliseconds(start_timer, end_timer) / static_cast<double>(repetitions);
        log("H", align_r_h, time_h);
        // .....................................................................
        start_timer = std::chrono::steady_clock::now();
        for (size_t repeat = 0; repeat < repetitions; ++repeat)
            GEMM_I(align_a, align_b, align_r_i);
        end_timer = std::chrono::steady_clock::now();
        double time_i = elapsedTimeMilliseconds(start_timer, end_timer) / static_cast<double>(repetitions);
        log("H", align_r_i, time_i);
        // .....................................................................
    }
    catch (std::exception &e)
    {
        std::cerr << "[ERROR] " << e.what() << '\n';
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}


