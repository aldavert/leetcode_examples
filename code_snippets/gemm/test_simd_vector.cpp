#include <vector>
#include <algorithm>
#include <assert.h>
#include <stdint.h>
#include <cstdlib>
#include <iostream>
#include <random>
#include <chrono>
#include <utility>
#include "vector_simd.hpp"
#include "dot.hpp"

template <typename T>
std::vector<T> generate(size_t size)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::vector<T> result(size);
    if constexpr (std::is_integral<T>::value)
    {
        std::uniform_int_distribution<> distribution(std::is_signed<T>::value?-120:0,
                                                     std::is_signed<T>::value?120:250);
        for (size_t i = 0; i < size; ++i)
            result[i] = static_cast<T>(distribution(gen));
    }
    else
    {
        std::uniform_real_distribution<> distribution(-1, 1);
        for (size_t i = 0; i < size; ++i)
            result[i] = static_cast<T>(distribution(gen));
    }
    return result;
}

template <typename T>
void test(size_t number_of_elements, size_t number_of_dimensions)
{
    std::vector<std::vector<double> > data;
    for (size_t i = 0; i < number_of_elements; ++i)
        data[i] = generate<T>(number_of_dimensions);
    std::vector<SIMDVector<double> > information;
    for (size_t i = 0; i < number_of_dimensions; ++i)
        information[i].init(data[i]);
}

int main(int, char * *)
{
    test<double>(16, 70);
    return EXIT_SUCCESS;
}

