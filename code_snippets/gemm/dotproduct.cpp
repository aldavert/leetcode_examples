#include <vector>
#include <algorithm>
#include <assert.h>
#include <stdint.h>
#include <cstdlib>
#include <iostream>
#include <random>
#include <chrono>
#include <utility>
#include "vector_aligned.hpp"
#include <fmt/core.h>
#include "dot.hpp"

//#if 1
//#define VECTOR Vector<T, 64>
//#else
//#define VECTOR std::vector<T>
//#endif

template <DotMode mode, typename T, typename VECTOR>
void evaluateDot(size_t number_of_dimensions, size_t trials, size_t number_of_vectors)
{
    auto elapsedTimeMilliseconds = [&](auto start, auto end) -> double
    {
        return (std::chrono::duration<double>{end - start}.count() * 1'000);
             // static_cast<double>(trials); // Show total time, is better to compare.
    };
    auto average = []<typename R>(const std::vector<R> &data) -> R
    {
        R result = 0;
        for (size_t i = 0, n = data.size() - 1; i < n; ++i)
            result += data[i];
        return result / static_cast<R>(data.size() - 1);
    };
    auto errorAverage = []<typename R>(const std::vector<R> &base, const std::vector<R> &data) -> R
    {
        R result = 0;
        for (size_t i = 0, n = data.size() - 1; i < n; ++i)
            result += std::abs(base[i] - data[i]);
        return result / static_cast<R>(data.size() - 1);
    };
    auto errorMin = []<typename R>(const std::vector<R> &base, const std::vector<R> &data) -> R
    {
        R result = std::abs(base[0] - data[0]);
        for (size_t i = 1, n = data.size() - 1; i < n; ++i)
            result = std::min(result, std::abs(base[i] - data[i]));
        return result;
    };
    auto errorMax = []<typename R>(const std::vector<R> &base, const std::vector<R> &data) -> R
    {
        R result = std::abs(base[0] - data[0]);
        for (size_t i = 1, n = data.size() - 1; i < n; ++i)
            result = std::max(result, std::abs(base[i] - data[i]));
        return result;
    };
    auto generate = [](size_t size) -> VECTOR
    {
        std::random_device rd;
        std::mt19937 gen(rd());
        VECTOR result(size);
        if constexpr (std::is_integral<T>::value)
        {
            std::uniform_int_distribution<> distribution(-120, 120);
            for (size_t i = 0; i < size; ++i)
                result[i] = static_cast<T>(distribution(gen));
        }
        else
        {
            std::uniform_real_distribution<> distribution(-1, 1);
            for (size_t i = 0; i < size; ++i)
                result[i] = static_cast<T>(distribution(gen));
        }
        return result;
    };
    Dot dot(number_of_dimensions);
    std::vector<VECTOR> data(number_of_vectors);
    std::vector<T> dot_base(number_of_vectors),
                   dot_simd_unroll(number_of_vectors),
                   dot_simd(number_of_vectors);
    
    for (size_t i = 0; i < number_of_vectors; ++i)
        data[i] = generate(number_of_dimensions);
    // -------------------------------------------------------------------------
    auto start_timer = std::chrono::steady_clock::now();
    #pragma omp parallel for
    for (size_t t = 0; t < trials; ++t)
        dot_base[t % number_of_vectors] = dot.dotVanilla(data[t % number_of_vectors], data[(t + 1) % number_of_vectors]);
    auto end_timer = std::chrono::steady_clock::now();
    double elapsed_time_vanilla = elapsedTimeMilliseconds(start_timer, end_timer);
    fmt::print("{:>30}: {:.5f} in {:>7.1f} ms [ ERRORS ]\n",
            "Dot-product basic",
            average(dot_base),
            elapsed_time_vanilla);
    // -------------------------------------------------------------------------
    start_timer = std::chrono::steady_clock::now();
    #pragma omp parallel for
    for (size_t t = 0; t < trials; ++t)
        dot_simd_unroll[t % number_of_vectors] = dot.dotSIMD<mode | DotMode::Unroll>(data[t % number_of_vectors], data[(t + 1) % number_of_vectors]);
    end_timer = std::chrono::steady_clock::now();
    double elapsed_time_unrolled_simd = elapsedTimeMilliseconds(start_timer, end_timer);
    fmt::print("{:>30}: {:.5f} in {:>7.1f}ms ({:5.2f}X {}) [avg={:<10.4g}|min={:<10.4g}|max={:10.4g}]\n",
            "Dot-product unrolled SIMD",
            average(dot_simd_unroll),
            elapsed_time_unrolled_simd,
            elapsed_time_vanilla / elapsed_time_unrolled_simd,
            ((elapsed_time_vanilla > elapsed_time_unrolled_simd)?("faster"):("slower")),
            errorAverage(dot_base, dot_simd_unroll),
            errorMin(dot_base, dot_simd_unroll),
            errorMax(dot_base, dot_simd_unroll));
    // -------------------------------------------------------------------------
    start_timer = std::chrono::steady_clock::now();
    #pragma omp parallel for
    for (size_t t = 0; t < trials; ++t)
        dot_simd[t % number_of_vectors] = dot.dotSIMD<mode>(data[t % number_of_vectors], data[(t + 1) % number_of_vectors]);
    end_timer = std::chrono::steady_clock::now();
    double elapsed_time_simd = elapsedTimeMilliseconds(start_timer, end_timer);
    fmt::print("{:>30}: {:.5f} in {:>7.1f}ms ({:5.2f}X {}) [avg={:<10.4g}|min={:<10.4g}|max={:10.4g}]\n",
            "Dot-product SIMD",
            average(dot_simd),
            elapsed_time_simd,
            elapsed_time_vanilla / elapsed_time_simd,
            ((elapsed_time_vanilla > elapsed_time_simd)?("faster"):("slower")),
            errorAverage(dot_base, dot_simd),
            errorMin(dot_base, dot_simd),
            errorMax(dot_base, dot_simd));
    // -------------------------------------------------------------------------
#if 1
    {
        for (size_t elements = 0; elements < 9000; ++elements)
        {
            Dot cdot(elements);
            VECTOR A = generate(elements), B = generate(elements);
            T dot_expected = cdot.dotVanilla(A, B);
            if (double current = cdot.dotSIMD<DotMode::Unroll | mode>(A, B), error = std::abs(dot_expected - current);
                error > 0.01)
                fmt::print("[UNROLL] {:<4d} {:12.7f} {:12.7f} {:12.7f}\n",
                        elements,
                        dot_expected,
                        current,
                        error);
            if (double current = cdot.dotSIMD<mode>(A, B), error = std::abs(dot_expected - current);
                error > 0.01)
                fmt::print("[SINGLE] {:<4d} {:12.7f} {:12.7f} {:12.7f}\n",
                        elements,
                        dot_expected,
                        current,
                        error);
        }
    }
#endif
}

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

int main(int, char * *)
{
#if 1
    constexpr size_t number_of_dimensions = 71;//10'000'000;
    constexpr size_t trials = 10'000'000;
#else
    constexpr size_t number_of_dimensions = 1'000'000;
    constexpr size_t trials = 1'000;
#endif
    constexpr size_t number_of_vectors = std::min<size_t>(trials + 1, 128);
    
    fmt::print("---------------===============[UNALIGNED memory | UNALIGNED Dot]===============---------------\n");
    fmt::print("TESTING AVX256\n");
    fmt::print("Evaluating FLOAT (32-bit precision) vector arrays:\n");
    evaluateDot<DotMode::Avx256, float, std::vector<float> >(number_of_dimensions, trials, number_of_vectors);
    fmt::print("Evaluating DOUBLE (64-bit precision) vector arrays:\n");
    evaluateDot<DotMode::Avx256, double, std::vector<double> >(number_of_dimensions, trials, number_of_vectors);
    fmt::print("\n");
    fmt::print("TESTING AVX512\n");
    fmt::print("Evaluating FLOAT (32-bit precision) vector arrays:\n");
    evaluateDot<DotMode::Avx512, float, std::vector<float> >(number_of_dimensions, trials, number_of_vectors);
    fmt::print("Evaluating DOUBLE (64-bit precision) vector arrays:\n");
    evaluateDot<DotMode::Avx512, double, std::vector<double> >(number_of_dimensions, trials, number_of_vectors);
    fmt::print("---------------===============[  ALIGNED memory | UNALIGNED Dot]===============---------------\n");
    fmt::print("TESTING AVX256\n");
    fmt::print("Evaluating FLOAT (32-bit precision) vector arrays:\n");
    evaluateDot<DotMode::Avx256, float, Vector<float, 64> >(number_of_dimensions, trials, number_of_vectors);
    fmt::print("Evaluating DOUBLE (64-bit precision) vector arrays:\n");
    evaluateDot<DotMode::Avx256, double, Vector<double, 64> >(number_of_dimensions, trials, number_of_vectors);
    fmt::print("\n");
    fmt::print("TESTING AVX512\n");
    fmt::print("Evaluating FLOAT (32-bit precision) vector arrays:\n");
    evaluateDot<DotMode::Avx512, float, Vector<float, 64> >(number_of_dimensions, trials, number_of_vectors);
    fmt::print("Evaluating DOUBLE (64-bit precision) vector arrays:\n");
    evaluateDot<DotMode::Avx512, double, Vector<double, 64> >(number_of_dimensions, trials, number_of_vectors);
    fmt::print("---------------===============[  ALIGNED memory |   ALIGNED Dot]===============---------------\n");
    fmt::print("TESTING AVX256\n");
    fmt::print("Evaluating FLOAT (32-bit precision) vector arrays:\n");
    evaluateDot<DotMode::Avx256 | DotMode::Aligned, float, Vector<float, 64> >(number_of_dimensions, trials, number_of_vectors);
    fmt::print("Evaluating DOUBLE (64-bit precision) vector arrays:\n");
    evaluateDot<DotMode::Avx256 | DotMode::Aligned, double, Vector<double, 64> >(number_of_dimensions, trials, number_of_vectors);
    fmt::print("\n");
    fmt::print("TESTING AVX512\n");
    fmt::print("Evaluating FLOAT (32-bit precision) vector arrays:\n");
    evaluateDot<DotMode::Avx512 | DotMode::Aligned, float, Vector<float, 64> >(number_of_dimensions, trials, number_of_vectors);
    fmt::print("Evaluating DOUBLE (64-bit precision) vector arrays:\n");
    evaluateDot<DotMode::Avx512 | DotMode::Aligned, double, Vector<double, 64> >(number_of_dimensions, trials, number_of_vectors);
#if 0
    typedef double TEST_TYPE;
    auto generate = [](size_t size) -> std::vector<TEST_TYPE>
    {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::vector<TEST_TYPE> result(size);
        std::uniform_real_distribution<> distribution(-1, 1);
        for (size_t i = 0; i < size; ++i)
            result[i] = static_cast<TEST_TYPE>(distribution(gen));
        return result;
    };
    for (size_t nelements = 1; nelements < 1000; ++nelements)
    {
        std::vector<TEST_TYPE> a = generate(nelements), b = generate(nelements);
        Dot dot(nelements);
        TEST_TYPE vanilla = dot.dotVanilla(a, b);
        TEST_TYPE simd = dot.dotSIMD<DotMode::Avx512>(a, b);
        if (std::abs(vanilla - simd) > 1e-5)
            fmt::print("         [SIMD] {:4d}: |{:14.9g}-{:14.9g}|={:14.9g}\n", nelements, vanilla, simd, std::abs(vanilla - simd));
        TEST_TYPE unroll_simd = dot.dotSIMD<DotMode::Unroll | DotMode::Avx512>(a, b);
        if (std::abs(vanilla - unroll_simd) > 1e-5)
            fmt::print("[Unrolled SIMD] {:4d}: |{:14.9g}-{:14.9g}|={:14.9g}\n", nelements, vanilla, unroll_simd, std::abs(vanilla - unroll_simd));
        if (std::abs(vanilla - unroll_simd) > 1e-5)
        {
            std::cout << a << '\n';
            std::cout << b << '\n';
            std::vector<TEST_TYPE> test;
            TEST_TYPE test_prev = 0;
            for (size_t m = 0; m < nelements; ++m)
                test.push_back(test_prev += a[m] * b[m]);
            std::cout << test << '\n';
        }
    }
#endif
    
    return EXIT_SUCCESS;
}


