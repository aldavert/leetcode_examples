#ifndef __MATRIX_HEADER_FILE__
#define __MATRIX_HEADER_FILE__

#include <memory>

template <typename T>
class Matrix
{
    size_t m_nrows = 0;
    size_t m_ncols = 0;
    T * m_data = nullptr;
public:
    // .........................................................................
    Matrix(void) = default;
    Matrix(size_t nrows, size_t ncols) :
        m_nrows(((nrows > 0) && (ncols > 0))?nrows:0),
        m_ncols(((nrows > 0) && (ncols > 0))?ncols:0),
        m_data(((nrows > 0) && (ncols > 0))?new T[nrows * ncols]:nullptr)
    {
        for (size_t col = 0, i = 0; col < m_ncols; ++col)
            for (size_t row = 0; row < m_nrows; ++row, ++i)
                m_data[i] = 0;
    }
    Matrix(size_t nrows, size_t ncols, auto &&func) :
        m_nrows(((nrows > 0) && (ncols > 0))?nrows:0),
        m_ncols(((nrows > 0) && (ncols > 0))?ncols:0),
        m_data(((nrows > 0) && (ncols > 0))?new T[nrows * ncols]:nullptr)
    {
        for (size_t col = 0, i = 0; col < m_ncols; ++col)
            for (size_t row = 0; row < m_nrows; ++row, ++i)
                func(row, col, m_data[i]);
    }
    Matrix(const Matrix<T> &other) :
        m_nrows(other.m_nrows),
        m_ncols(other.m_ncols),
        m_data((other.m_data)?new T[other.m_nrows * other.m_ncols]:nullptr) {}
    Matrix(Matrix<T> &&other) :
        m_nrows(other.m_nrows),
        m_ncols(other.m_ncols),
        m_data(std::exchange(other.m_data, nullptr)) {}
    Matrix& operator=(const Matrix<T> &other)
    {
        delete [] m_data;
        if ((other.m_nrows > 0) && (other.m_ncols > 0))
        {
            m_nrows = other.m_nrows;
            m_ncols = other.m_ncols;
            m_data = new T[other.m_nrows * other.m_ncols];
        }
        else
        {
            m_nrows = m_ncols = 0;
            m_data = nullptr;
        }
    }
    Matrix& operator=(Matrix<T> &&other)
    {
        std::swap(m_nrows, other.m_nrows);
        std::swap(m_ncols, other.m_ncols);
        std::swap(m_data, other.m_data);
    }
    ~Matrix(void)
    {
        delete [] m_data;
    }
    void set(size_t nrows, size_t ncols)
    {
        if ((nrows > 0) && (ncols > 0))
        {
            m_nrows = nrows;
            m_ncols = ncols;
            m_data = new T[nrows * ncols];
            for (size_t i = 0, n = nrows * ncols; i < n; ++i)
                m_data[i] = 0;
        }
        else
        {
            m_nrows = m_ncols = 0;
            m_data = nullptr;
        }
    }
    void set(size_t nrows, size_t ncols, auto &&func)
    {
        if ((nrows > 0) && (ncols > 0))
        {
            m_nrows = nrows;
            m_ncols = ncols;
            m_data = new T[nrows * ncols];
            for (size_t col = 0, i = 0; col < m_ncols; ++col)
                for (size_t row = 0; row < m_nrows; ++row, ++i)
                    func(row, col, m_data[i]);
        }
        else
        {
            m_nrows = m_ncols = 0;
            m_data = nullptr;
        }
    }
    // .........................................................................
    inline size_t rows(void) const { return m_nrows; }
    inline size_t columns(void) const { return m_ncols; }
    const T& operator()(size_t row, size_t col) const { return m_data[row + col * m_nrows]; }
    T& operator()(size_t row, size_t col) { return m_data[row + col * m_nrows]; }
    const T * operator()(size_t col) const { return &m_data[col * m_nrows]; }
    T * operator()(size_t col) { return &m_data[col * m_nrows]; }
    const T * operator()(void) const { return m_data; }
    T * operator()(void) { return m_data; }
    // .........................................................................
    friend std::ostream& operator<<(std::ostream &out, const Matrix<auto> &mat)
    {
        for (size_t row = 0; row < mat.rows(); ++row)
        {
            out << '[';
            bool next_column = false;
            for (size_t col = 0; col < mat.columns(); ++col)
            {
                if (next_column) [[likely]] out << ", ";
                next_column = true;
                out << fmt::format("{:>14.8g}", mat(row, col));
                if ((col == 4) && (mat.columns() > 7))
                {
                    out << ", ...";
                    col = mat.columns() - 2;
                }
            }
            out << "]\n";
            if ((row == 4) && (mat.rows() > 7))
            {
                if (mat.columns() > 7)
                {
                    for (int r = 0; r < 3; ++r)
                    {
                        out << "[             ."
                            << fmt::format("{:>{}}", '.', 4 * 16 + 3 + r)
                            << fmt::format("{:>{}}]\n", '.', 3 - r + 15);
                    }
                }
                else
                {
                    for (int r = 0; r < 3; ++r)
                    {
                        if (mat.columns() > 1)
                            out << "[             ."
                                << fmt::format("{:>{}}]\n", '.', (mat.columns() - 1) * 16);
                        else out << "[             .]\n";
                    }
                }
                row = mat.rows() - 2;
            }
        }
        return out;
    }
};

// =============================================================================
// =============================================================================

template <typename T, size_t ALIGNMENT_SIZE = 64>
class MatrixAligned
{
    size_t m_nrows = 0;
    size_t m_ncols = 0;
    size_t m_size_raw = 0;
    T * * m_column_ptr = nullptr;
    T * m_data_raw = nullptr;
    bool reserve(size_t nrows, size_t ncols)
    {
        if ((nrows == 0) || (ncols == 0))
        {
            delete [] m_column_ptr;
            delete [] m_data_raw;
            m_nrows = m_ncols = m_size_raw = 0;
            return false;
        }
        size_t size_raw = nrows * ncols + ALIGNMENT_SIZE * ncols - 1;
        T * * column_ptr = new T * [ncols];
        T * data_raw = new T[size_raw];
        void * ptr = static_cast<void *>(data_raw);
        for (size_t col = 0; col < ncols; ++col)
        {
            size_t storage_size = reinterpret_cast<char *>(data_raw + size_raw)
                                - reinterpret_cast<char *>(ptr);
            column_ptr[col] = static_cast<T *>(std::align(ALIGNMENT_SIZE,
                                                          nrows * sizeof(T),
                                                          ptr,
                                                          storage_size));
            std::memset(column_ptr[col], 0, nrows * sizeof(T));
            if (column_ptr[col] == nullptr)
            {
                delete [] column_ptr;
                delete [] data_raw;
                delete [] m_column_ptr;
                delete [] m_data_raw;
                m_nrows = m_ncols = m_size_raw = 0;
                return false;
            }
            ptr = static_cast<void *>(column_ptr[col] + nrows);
        }
        delete [] m_column_ptr;
        delete [] m_data_raw;
        m_data_raw = data_raw;
        m_column_ptr = column_ptr;
        m_ncols = ncols;
        m_nrows = nrows;
        m_size_raw = size_raw;
        return true;
    }
public:
    // .........................................................................
    MatrixAligned(void) = default;
    MatrixAligned(size_t nrows, size_t ncols) :
        m_nrows(0),
        m_ncols(0),
        m_size_raw(0),
        m_column_ptr(nullptr),
        m_data_raw(nullptr)
    {
        if (reserve(nrows, ncols))
            for (size_t col = 0; col < m_ncols; ++col)
                for (size_t row = 0; row < m_nrows; ++row)
                    m_column_ptr[col][row] = 0;
    }
    MatrixAligned(size_t nrows, size_t ncols, auto &&func) :
        m_nrows(0),
        m_ncols(0),
        m_size_raw(0),
        m_column_ptr(nullptr),
        m_data_raw(nullptr)
    {
        if (reserve(nrows, ncols))
            for (size_t col = 0; col < m_ncols; ++col)
                for (size_t row = 0; row < m_nrows; ++row)
                    func(row, col, m_column_ptr[col][row]);
    }
    MatrixAligned(const Matrix<T> &other) :
        m_nrows(0),
        m_ncols(0),
        m_size_raw(0),
        m_column_ptr(nullptr),
        m_data_raw(nullptr)
    {
        if (reserve(other.rows(), other.columns()))
            for (size_t col = 0; col < other.columns(); ++col)
                for (size_t row = 0; row < other.rows(); ++row)
                    m_column_ptr[col][row] = other(row, col);
    }
    MatrixAligned(const MatrixAligned<T, ALIGNMENT_SIZE> &other) :
        m_nrows(0),
        m_ncols(0),
        m_size_raw(0),
        m_column_ptr(nullptr),
        m_data_raw(nullptr)
    {
        if (reserve(other.m_nrows, other.m_ncols))
            for (size_t col = 0; col < m_ncols; ++col)
                for (size_t row = 0; row < m_nrows; ++row)
                    m_column_ptr[col][row] = other.m_column_ptr[col][row];
    }
    MatrixAligned(MatrixAligned<T, ALIGNMENT_SIZE> &&other) :
        m_nrows(std::exchange(other.m_nrows, 0)),
        m_ncols(std::exchange(other.m_ncols, 0)),
        m_size_raw(std::exchange(other.m_size_raw, 0)),
        m_column_ptr(std::exchange(other.m_column_ptr, nullptr)),
        m_data_raw(std::exchange(other.m_data_raw, nullptr)) {}
    MatrixAligned& operator=(const MatrixAligned<T, ALIGNMENT_SIZE> &other)
    {
        if (reserve(other.m_nrows, other.m_ncols))
            for (size_t col = 0; col < m_ncols; ++col)
                for (size_t row = 0; row < m_nrows; ++row)
                    m_column_ptr[col][row] = other.m_column_ptr[col][row];
        return *this;
    }
    MatrixAligned& operator=(MatrixAligned<T, ALIGNMENT_SIZE> &&other)
    {
        std::swap(m_nrows, other.m_nrows);
        std::swap(m_ncols, other.m_ncols);
        std::swap(m_size_raw, other.m_size_raw);
        std::swap(m_column_ptr, other.m_column_ptr);
        std::swap(m_data_raw, other.m_data_raw);
    }
    MatrixAligned& operator=(Matrix<T> &other)
    {
        if (reserve(other.rows(), other.columns()))
            for (size_t col = 0; col < other.columns(); ++col)
                for (size_t row = 0; row < other.rows(); ++row)
                    m_column_ptr[col][row] = other(row, col);
    }
    ~MatrixAligned(void)
    {
        delete [] m_column_ptr;
        delete [] m_data_raw;
    }
    void set(size_t nrows, size_t ncols)
    {
        if (reserve(nrows, ncols))
            for (size_t col = 0; col < m_ncols; ++col)
                for (size_t row = 0; row < m_nrows; ++row)
                    m_column_ptr[col][row] = 0;
    }
    void set(size_t nrows, size_t ncols, auto &&func)
    {
        if (reserve(nrows, ncols))
            for (size_t col = 0; col < m_ncols; ++col)
                for (size_t row = 0; row < m_nrows; ++row)
                    func(row, col, m_column_ptr[col][row]);
    }
    // .........................................................................
    inline size_t rows(void) const { return m_nrows; }
    inline size_t columns(void) const { return m_ncols; }
    const T& operator()(size_t row, size_t col) const { return m_column_ptr[col][row]; }
    T& operator()(size_t row, size_t col) { return m_column_ptr[col][row]; }
    const T * operator()(size_t col) const { return m_column_ptr[col]; }
    T * operator()(size_t col) { return m_column_ptr[col]; }
    // .........................................................................
    friend std::ostream& operator<<(std::ostream &out, const MatrixAligned<auto, ALIGNMENT_SIZE> &mat)
    {
        for (size_t row = 0; row < mat.rows(); ++row)
        {
            out << '[';
            bool next_column = false;
            for (size_t col = 0; col < mat.columns(); ++col)
            {
                if (next_column) [[likely]] out << ", ";
                next_column = true;
                out << fmt::format("{:>14.8g}", mat(row, col));
                if ((col == 4) && (mat.columns() > 7))
                {
                    out << ", ...";
                    col = mat.columns() - 2;
                }
            }
            out << "]\n";
            if ((row == 4) && (mat.rows() > 7))
            {
                if (mat.columns() > 7)
                {
                    for (int r = 0; r < 3; ++r)
                    {
                        out << "[             ."
                            << fmt::format("{:>{}}", '.', 4 * 16 + 3 + r)
                            << fmt::format("{:>{}}]\n", '.', 3 - r + 15);
                    }
                }
                else
                {
                    for (int r = 0; r < 3; ++r)
                    {
                        if (mat.columns() > 1)
                            out << "[             ."
                                << fmt::format("{:>{}}]\n", '.', (mat.columns() - 1) * 16);
                        else out << "[             .]\n";
                    }
                }
                row = mat.rows() - 2;
            }
        }
        return out;
    }
};

// =============================================================================
// =============================================================================

auto eye = []([[maybe_unused]] size_t row,
              [[maybe_unused]] size_t col,
              auto &value)
{
    value = (row == col);
};
auto zeros = []([[maybe_unused]] size_t row,
               [[maybe_unused]] size_t col,
               auto &value)
{
    value = 0;
};
auto ones = []([[maybe_unused]] size_t row,
               [[maybe_unused]] size_t col,
               auto &value)
{
    value = 1;
};

// https://en.cppreference.com/w/cpp/header/random
struct uniform
{
    std::random_device m_rd;
    std::mt19937 m_gen;
    std::uniform_real_distribution<> m_distribution;
    uniform(double min, double max) :
        m_gen(m_rd()),
        m_distribution(min, max)
    {
    }
    void operator()([[maybe_unused]] size_t row,
                    [[maybe_unused]] size_t col,
                    auto &value)
    {
        value = m_distribution(m_gen);
    }
};

struct normal
{
    std::random_device m_rd;
    std::mt19937 m_gen;
    std::normal_distribution<> m_distribution;
    normal(double mean, double std) :
        m_gen(m_rd()),
        m_distribution(mean, std)
    {
    }
    void operator()([[maybe_unused]] size_t row,
                    [[maybe_unused]] size_t col,
                    auto &value)
    {
        value = m_distribution(m_gen);
    }
};

#endif

