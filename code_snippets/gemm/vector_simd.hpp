#ifndef __VECTOR_SIMD_HEADER_FILE__
#define __VECTOR_SIMD_HEADER_FILE__
#include <type_traits>
#include "vector_aligned.hpp"

enum class VectorMode
{
#ifdef __AVX2__
    Avx256  = 0,
#endif
#ifdef __AVX512F__
    Avx512  = 1
#endif
};
constexpr VectorMode operator|(VectorMode left, VectorMode right)
{
    return static_cast<VectorMode>(static_cast<int>(left) | static_cast<int>(right));
}
constexpr bool operator&(VectorMode left, VectorMode right)
{
    return (static_cast<int>(left) & static_cast<int>(right)) > 0;
}

template <typename T, VectorMode MODE = VectorMode::Avx256>
class SIMDVector
{
#ifdef __AVX512F__
    struct Avx512Instructions {};
    __m512  type(float , Avx512Instructions) const;
    __m512d type(double, Avx512Instructions) const;
    constexpr size_t length(__m512) const  { return 16; }
    constexpr size_t length(__m512d) const { return 8; }
#endif
#ifdef __AVX2__
    struct Avx256Instructions {};
    __m256  type(float , Avx256Instructions) const;
    __m256d type(double, Avx256Instructions) const;
    constexpr size_t length(__m256) const  { return 8; }
    constexpr size_t length(__m256d) const { return 4; }
#endif
    constexpr size_t length(size_t nelements) const
    {
        constexpr size_t vector_size = lenght(VECTOR_TYPE());
        return nelements / vector_size + nelements % vector_size;
    }
    
    typedef typename std::conditional(MODE | VectorMode::Avx512, Avx512Instructions, Avx256Instructions)::type INSTRUCTIONSET;
    typedef typename decltype(type(T(), INSTRUCTIONSET())) VECTOR_TYPE;
    std::vector<VECTOR_TYPE> m_data;
public:
    // Create the initialization functions for the different vector types:
    // - Aligned/unaligned data (depending on the vector).
    // - We have to create the mask to initialize the object.
    // Make a main file to test the vector.
    // Add dot product between vector/Vector and SIMDVector
    // Replace the row cached values for an SIMD vector.
    // Make a cache for the columns too?
    SIMDVector(void) = default;
    SIMDVector(const T * data, size_t nelements) : m_data(length(nelements)) {};
    SIMDVector(const std::vector<T> &data);
    template <size_t ALIGNMENT_SIZE>
    SIMDVector(const Vector<T, ALIGNMENT_SIZE> &data);
    template <size_t ALIGNMENT_SIZE>
    SIMDVector(const Vector<T, ALIGNMENT_SIZE> &data, size_t begin, size_t size);
    void init(const T * data, size_t nelements) : m_data(length(nelements)) {};
    void init(const std::vector<T> &data);
    template <size_t ALIGNMENT_SIZE>
    void init(const Vector<T, ALIGNMENT_SIZE> &data);
    template <size_t ALIGNMENT_SIZE>
    void init(const Vector<T, ALIGNMENT_SIZE> &data, size_t begin, size_t size);
};

#endif

