#include <immintrin.h>
#include <type_traits>

#ifndef __DOT_HEADER_FILE__
#define __DOT_HEADER_FILE__

enum class DotMode
{
    Normal  = 0,
    Unroll  = 1,
    Avx256  = 2,
    Avx512  = 4,
    Aligned = 8
};
constexpr DotMode operator|(DotMode left, DotMode right)
{
    return static_cast<DotMode>(static_cast<int>(left) | static_cast<int>(right));
}
constexpr bool operator&(DotMode left, DotMode right)
{
    return (static_cast<int>(left) & static_cast<int>(right)) > 0;
}
//// void display(__m512d value) const
//// {
////     double aux[8];
////     std::memcpy(aux, &value, sizeof(aux));
////     fmt::print("Packed values: [{}, {}, {}, {}, {}, {}, {}, {}]\n",
////             aux[0], aux[1], aux[2], aux[3],
////             aux[4], aux[5], aux[6], aux[7]);
//// };

class Dot
{
private:
    struct Avx512Instructions {};
    struct Avx256Instructions {};
    struct AlignedData {};
    struct UnalignedData {};
#ifdef __AVX512F__
    __mmask16 mask_single_512[5];
    __mmask8  mask_double_512[5];
#endif
#ifdef __AVX2__
    __m256i mask_single_256[5];
    __m256i mask_double_256[5];
#endif
    // Use the types parameters to select the correct function and return the type
    // used by the given configuration:
    // 1st parameter: input array data type.
    // 2on parameter: length of the AVX vector (long 512, int 256)
#ifdef __AVX512F__
    __m512  type(float , Avx512Instructions) const;
    __m512d type(double, Avx512Instructions) const;
#endif
#ifdef __AVX2__
    __m256  type(float , Avx256Instructions) const;
    __m256d type(double, Avx256Instructions) const;
#endif
public:
    Dot(size_t length)
    {
        for (int i = 0; i < 4; ++i)
        {
#ifdef __AVX512F__
            mask_single_512[i] = maskSingle512((16 + i * 16) - length % 64);
            mask_double_512[i] = maskDouble512(( 8 + i *  8) - length % 32);
#endif
#ifdef __AVX2__
            mask_single_256[i] = maskSingle256((8 + i * 8) - length % 32);
            mask_double_256[i] = maskDouble256((4 + i * 4) - length % 16);
#endif
        }
#ifdef __AVX512F__
        mask_single_512[4] = maskSingle512((16 + 0 * 16) - length % 16);
        mask_double_512[4] = maskDouble512(( 8 + 0 *  8) - length %  8);
#endif
#ifdef __AVX2__
        mask_single_256[4] = maskSingle256((8 + 0 * 8) - length % 8);
        mask_double_256[4] = maskDouble256((4 + 0 * 4) - length % 4);
#endif
    }
#if defined(__AVX512F__) || defined(__AVX2__)
    template <DotMode mode = DotMode::Normal, typename T>
    T dotSIMD(const T * __restrict p1,
              const T * __restrict p2,
              size_t length) const
    {
        if (length == 0) return 0.0;
        typedef typename std::conditional<mode & DotMode::Avx512, Avx512Instructions, Avx256Instructions>::type AvxInstructionsType;
        constexpr int packet_size = (1 + std::is_same<AvxInstructionsType, Avx512Instructions>::value) * 256 / (sizeof(T) * 8);
        typedef decltype(type(T(), AvxInstructionsType())) IT;
        typedef typename std::conditional<mode & DotMode::Aligned, AlignedData, UnalignedData>::type AlignedType;
        if constexpr (mode & DotMode::Unroll)
        {
            constexpr int block_size = packet_size * 4;
            
            const ptrdiff_t rem = length % block_size;
            IT dot0 = zero(IT()), dot1 = zero(IT()),
               dot2 = zero(IT()), dot3 = zero(IT());
            for (const T * const p1_end = p1 + length - rem; p1 < p1_end; p1 += block_size, p2 += block_size)
            {
                dot0 = accumulateProduct<0>(dot0, p1, p2, AlignedType());
                dot1 = accumulateProduct<1>(dot1, p1, p2, AlignedType());
                dot2 = accumulateProduct<2>(dot2, p1, p2, AlignedType());
                dot3 = accumulateProduct<3>(dot3, p1, p2, AlignedType());
            }
            if (rem > 0 * packet_size)
                dot0 = accumulateRemainingProduct<0>(dot0, p1, p2, AlignedType());
            if (rem > 1 * packet_size)
                dot1 = accumulateRemainingProduct<1>(dot1, p1, p2, AlignedType());
            if (rem > 2 * packet_size)
                dot2 = accumulateRemainingProduct<2>(dot2, p1, p2, AlignedType());
            if (rem > 3 * packet_size)
                dot3 = accumulateRemainingProduct<3>(dot3, p1, p2, AlignedType());
            return unpackSum(blockSum(dot0, dot1, dot2, dot3));
        }
        else
        {
            const ptrdiff_t rem = length % packet_size;
            IT dot0 = zero(IT());
            for (const T * const p1_end = p1 + length - rem; p1 < p1_end; p1 += packet_size, p2 += packet_size)
                dot0 = accumulateProduct<0>(dot0, p1, p2, AlignedType());
            if (rem > 0 * packet_size)
                dot0 = accumulateRemainingProduct<4>(dot0, p1, p2, AlignedType());
            
            return unpackSum(dot0);
        }
    }
#endif
    template <DotMode mode = DotMode::Normal, typename CONTAINER>
    inline auto dotSIMD(const CONTAINER &a, const CONTAINER &b) const
    {
        return dotSIMD<mode>(a.data(), b.data(), std::min(a.size(), b.size()));
    }
    template <typename CONTAINER>
    inline auto dotVanilla(const CONTAINER &a, const CONTAINER &b) const
    {
        return dotVanilla(a.data(), b.data(), std::min(a.size(), b.size()));
    }
    template <typename T>
    inline T dotVanilla(const T * __restrict a, const T * __restrict b, size_t length) const
    {
        T current_dot = 0;
        for (size_t i = 0; i < length; ++i)
            current_dot += a[i] * b[i];
        return current_dot;
    }
protected:
    // -[ SET VECTOR TO ZERO ]--------------------------------------------------
#ifdef __AVX512F__
    inline __m512d zero(__m512d) const { return _mm512_setzero_pd(); }
    inline __m512  zero(__m512 ) const { return _mm512_setzero_ps(); }
#endif
#if __AVX2__
    inline __m256d zero(__m256d) const { return _mm256_setzero_pd(); }
    inline __m256  zero(__m256 ) const { return _mm256_setzero_ps(); }
#endif
    // -[ INITIALIZE THE MASK ]-------------------------------------------------
#ifdef __AVX512F__
    inline __mmask8 maskDouble512(ptrdiff_t missing_lanes) const
    {
        missing_lanes = std::max(missing_lanes, (ptrdiff_t)0);
        return static_cast<__mmask8>(static_cast<uint8_t>(0xFF) >> missing_lanes);
    }
    inline __mmask16 maskSingle512(ptrdiff_t missing_lanes) const
    {
        missing_lanes = std::max(missing_lanes, (ptrdiff_t)0);
        return static_cast<__mmask16>(static_cast<uint16_t>(0xFFFF) >> missing_lanes);
    }
#endif
#if __AVX2__
    inline __m256i maskDouble256(ptrdiff_t missing_lanes) const
    {
        missing_lanes = std::max(missing_lanes, (ptrdiff_t)0);
        int64_t mask = static_cast<int64_t>(0xFFFFFFFF) >> (missing_lanes * 8);
        return _mm256_cvtepi8_epi64(_mm_cvtsi64_si128(mask));
    }
    inline __m256i maskSingle256(ptrdiff_t missing_lanes) const
    {
        missing_lanes = std::max(missing_lanes, (ptrdiff_t)0);
        uint64_t mask = ~(uint64_t)0;
        mask >>= missing_lanes * 8;
        return _mm256_cvtepi8_epi32(_mm_cvtsi64_si128((int64_t)mask));
    }
#endif
    
    // -[ MULTIPLY AND ACCUMULATE ]---------------------------------------------
#ifdef __AVX512F__
    template<int OFFSETREGS>
    inline __m512d accumulateProduct(__m512d acc,
                                     const double * __restrict p1,
                                     const double * __restrict p2,
                                     UnalignedData) const
    {
        constexpr ptrdiff_t lanes = OFFSETREGS * 8;
        const __m512d a = _mm512_loadu_pd(p1 + lanes);
        const __m512d b = _mm512_loadu_pd(p2 + lanes);
        return _mm512_fmadd_pd(a, b, acc);
    }
    template<int OFFSETREGS>
    inline __m512 accumulateProduct(__m512 acc,
                                    const float * __restrict p1,
                                    const float * __restrict p2,
                                    UnalignedData) const
    {
        constexpr ptrdiff_t lanes = OFFSETREGS * 16;
        const __m512 a = _mm512_loadu_ps(p1 + lanes);
        const __m512 b = _mm512_loadu_ps(p2 + lanes);
        return _mm512_fmadd_ps(a, b, acc);
    }
    template<int OFFSETREGS>
    inline __m512d accumulateProduct(__m512d acc,
                                     const double * __restrict p1,
                                     const double * __restrict p2,
                                     AlignedData) const
    {
        constexpr ptrdiff_t lanes = OFFSETREGS * 8;
        const __m512d a = _mm512_load_pd(p1 + lanes);
        const __m512d b = _mm512_load_pd(p2 + lanes);
        return _mm512_fmadd_pd(a, b, acc);
    }
    template<int OFFSETREGS>
    inline __m512 accumulateProduct(__m512 acc,
                                    const float * __restrict p1,
                                    const float * __restrict p2,
                                    AlignedData) const
    {
        constexpr ptrdiff_t lanes = OFFSETREGS * 16;
        const __m512 a = _mm512_load_ps(p1 + lanes);
        const __m512 b = _mm512_load_ps(p2 + lanes);
        return _mm512_fmadd_ps(a, b, acc);
    }
#endif
#ifdef __AVX2__
    template<int OFFSETREGS>
    inline __m256d accumulateProduct(__m256d acc,
                                     const double * __restrict p1,
                                     const double * __restrict p2,
                                     UnalignedData) const
    {
        constexpr ptrdiff_t lanes = OFFSETREGS * 4;
        const __m256d a = _mm256_loadu_pd(p1 + lanes);
        const __m256d b = _mm256_loadu_pd(p2 + lanes);
        return _mm256_fmadd_pd(a, b, acc);
    }
    template<int OFFSETREGS>
    inline __m256 accumulateProduct(__m256 acc,
                                    const float * __restrict p1,
                                    const float * __restrict p2,
                                    UnalignedData) const
    {
        constexpr ptrdiff_t lanes = OFFSETREGS * 8;
        const __m256 a = _mm256_loadu_ps(p1 + lanes);
        const __m256 b = _mm256_loadu_ps(p2 + lanes);
        return _mm256_fmadd_ps(a, b, acc);
    }
    template<int OFFSETREGS>
    inline __m256d accumulateProduct(__m256d acc,
                                     const double * __restrict p1,
                                     const double * __restrict p2,
                                     AlignedData) const
    {
        constexpr ptrdiff_t lanes = OFFSETREGS * 4;
        const __m256d a = _mm256_load_pd(p1 + lanes);
        const __m256d b = _mm256_load_pd(p2 + lanes);
        return _mm256_fmadd_pd(a, b, acc);
    }
    template<int OFFSETREGS>
    inline __m256 accumulateProduct(__m256 acc,
                                    const float * __restrict p1,
                                    const float * __restrict p2,
                                    AlignedData) const
    {
        constexpr ptrdiff_t lanes = OFFSETREGS * 8;
        const __m256 a = _mm256_load_ps(p1 + lanes);
        const __m256 b = _mm256_load_ps(p2 + lanes);
        return _mm256_fmadd_ps(a, b, acc);
    }
#endif
    // -[ MULTIPLY AND ACCUMULATE WITH A MASK ]---------------------------------
#ifdef __AVX512F__
    template<int OFFSETREGS>
    inline __m512d accumulateRemainingProduct(__m512d acc,
                                              const double * __restrict p1,
                                              const double * __restrict p2,
                                              UnalignedData) const
    {
        constexpr ptrdiff_t lanes = (OFFSETREGS != 4) * OFFSETREGS * 8;
        __m512d init_value = _mm512_setzero_pd();
        const __m512d a = _mm512_mask_loadu_pd(init_value, mask_double_512[OFFSETREGS], p1 + lanes);
        const __m512d b = _mm512_mask_loadu_pd(init_value, mask_double_512[OFFSETREGS], p2 + lanes);
        return _mm512_fmadd_pd(a, b, acc);
    }
    template<int OFFSETREGS>
    inline __m512 accumulateRemainingProduct(__m512 acc,
                                             const float * __restrict p1,
                                             const float * __restrict p2,
                                             UnalignedData) const
    {
        constexpr ptrdiff_t lanes = (OFFSETREGS != 4) * OFFSETREGS * 16;
        __m512 init_value = _mm512_setzero_ps();
        const __m512 a = _mm512_mask_loadu_ps(init_value, mask_single_512[OFFSETREGS], p1 + lanes);
        const __m512 b = _mm512_mask_loadu_ps(init_value, mask_single_512[OFFSETREGS], p2 + lanes);
        return _mm512_fmadd_ps(a, b, acc);
    }
    template<int OFFSETREGS>
    inline __m512d accumulateRemainingProduct(__m512d acc,
                                              const double * __restrict p1,
                                              const double * __restrict p2,
                                              AlignedData) const
    {
        constexpr ptrdiff_t lanes = (OFFSETREGS != 4) * OFFSETREGS * 8;
        __m512d init_value = _mm512_setzero_pd();
        const __m512d a = _mm512_mask_load_pd(init_value, mask_double_512[OFFSETREGS], p1 + lanes);
        const __m512d b = _mm512_mask_load_pd(init_value, mask_double_512[OFFSETREGS], p2 + lanes);
        return _mm512_fmadd_pd(a, b, acc);
    }
    template<int OFFSETREGS>
    inline __m512 accumulateRemainingProduct(__m512 acc,
                                             const float * __restrict p1,
                                             const float * __restrict p2,
                                             AlignedData) const
    {
        constexpr ptrdiff_t lanes = (OFFSETREGS != 4) * OFFSETREGS * 16;
        __m512 init_value = _mm512_setzero_ps();
        const __m512 a = _mm512_mask_load_ps(init_value, mask_single_512[OFFSETREGS], p1 + lanes);
        const __m512 b = _mm512_mask_load_ps(init_value, mask_single_512[OFFSETREGS], p2 + lanes);
        return _mm512_fmadd_ps(a, b, acc);
    }
#endif
#ifdef __AVX2__
    template<int OFFSETREGS>
    inline __m256d accumulateRemainingProduct(__m256d acc,
                                              const double * __restrict p1,
                                              const double * __restrict p2,
                                              UnalignedData) const
    {
        constexpr ptrdiff_t lanes = (OFFSETREGS != 4) * OFFSETREGS * 4;
        const __m256d a = _mm256_maskload_pd(p1 + lanes, mask_double_256[OFFSETREGS]);
        const __m256d b = _mm256_maskload_pd(p2 + lanes, mask_double_256[OFFSETREGS]);
        return _mm256_fmadd_pd(a, b, acc);
    }
    template<int OFFSETREGS>
    inline __m256 accumulateRemainingProduct(__m256 acc,
                                             const float * __restrict p1,
                                             const float * __restrict p2,
                                             UnalignedData) const
    {
        constexpr ptrdiff_t lanes = (OFFSETREGS != 4) * OFFSETREGS * 8;
        const __m256 a = _mm256_maskload_ps(p1 + lanes, mask_single_256[OFFSETREGS]);
        const __m256 b = _mm256_maskload_ps(p2 + lanes, mask_single_256[OFFSETREGS]);
        return _mm256_fmadd_ps(a, b, acc);
    }
    template<int OFFSETREGS>
    inline __m256d accumulateRemainingProduct(__m256d acc,
                                              const double * __restrict p1,
                                              const double * __restrict p2,
                                              AlignedData) const
    {
        constexpr ptrdiff_t lanes = (OFFSETREGS != 4) * OFFSETREGS * 4;
        const __m256d a = _mm256_maskload_pd(p1 + lanes, mask_double_256[OFFSETREGS]);
        const __m256d b = _mm256_maskload_pd(p2 + lanes, mask_double_256[OFFSETREGS]);
        return _mm256_fmadd_pd(a, b, acc);
    }
    template<int OFFSETREGS>
    inline __m256 accumulateRemainingProduct(__m256 acc,
                                             const float * __restrict p1,
                                             const float * __restrict p2,
                                             AlignedData) const
    {
        constexpr ptrdiff_t lanes = (OFFSETREGS != 4) * OFFSETREGS * 8;
        const __m256 a = _mm256_maskload_ps(p1 + lanes, mask_single_256[OFFSETREGS]);
        const __m256 b = _mm256_maskload_ps(p2 + lanes, mask_single_256[OFFSETREGS]);
        return _mm256_fmadd_ps(a, b, acc);
    }
#endif
    // -[ ACCUMULATED UNROLLED PACKED VECTORS ]---------------------------------
#ifdef __AVX512F__
    inline __m512d blockSum(__m512d v0, __m512d v1, __m512d v2, __m512d v3) const
    {
        return _mm512_add_pd(_mm512_add_pd(v0, v2), _mm512_add_pd(v1, v3));
    }
    inline __m512 blockSum(__m512 v0, __m512 v1, __m512 v2, __m512 v3) const
    {
        return _mm512_add_ps(_mm512_add_ps(v0, v2), _mm512_add_ps(v1, v3));
    }
#endif
#ifdef __AVX2__
    inline __m256d blockSum(__m256d v0, __m256d v1, __m256d v2, __m256d v3) const
    {
        return _mm256_add_pd(_mm256_add_pd(v0, v2), _mm256_add_pd(v1, v3));
    }
    inline __m256 blockSum(__m256 v0, __m256 v1, __m256 v2, __m256 v3) const
    {
        return _mm256_add_ps(_mm256_add_ps(v0, v2), _mm256_add_ps(v1, v3));
    }
#endif
    // -[ ACCUMULATE PACKED VECTOR ]--------------------------------------------
#ifdef __AVX512F__
    inline double unpackSum(__m512d v) const
    {
        __m256d fold = _mm256_add_pd(_mm512_castpd512_pd256(v),
                                     _mm512_extractf64x4_pd(v, 1));
        __m128d r4 = _mm_add_pd(_mm256_castpd256_pd128(fold),
                                _mm256_extractf128_pd(fold, 1));
        return _mm_cvtsd_f64(_mm_add_sd(r4, _mm_shuffle_pd(r4, r4, 1)));
    }
    inline float unpackSum(__m512 v) const
    {
        __m256 fold = _mm256_add_ps(_mm512_castps512_ps256(v),
                                    _mm512_extractf32x8_ps(v, 1));
        __m128 r4 = _mm_add_ps(_mm256_castps256_ps128(fold),
                               _mm256_extractf128_ps(fold, 1));
        r4 = _mm_add_ps(r4, _mm_movehl_ps(r4, r4));
        r4 = _mm_add_ss(r4, _mm_movehdup_ps(r4));
        return _mm_cvtss_f32(r4);
    }
#endif
#ifdef __AVX2__
    inline double unpackSum(__m256d v) const
    {
        __m128d r4 = _mm_add_pd(_mm256_castpd256_pd128(v),
                                _mm256_extractf128_pd(v, 1));
        return _mm_cvtsd_f64(_mm_add_sd(r4, _mm_shuffle_pd(r4, r4, 1)));
    }
    inline float unpackSum(__m256 v) const
    {
        __m128 r4 = _mm_add_ps(_mm256_castps256_ps128(v),
                               _mm256_extractf128_ps(v, 1));
        r4 = _mm_add_ps(r4, _mm_movehl_ps(r4, r4));
        r4 = _mm_add_ss(r4, _mm_movehdup_ps(r4));
        return _mm_cvtss_f32(r4);
    }
#endif
};

#endif

