#include <cstdlib>
#include <iostream>
#include <vector>
#include <array>
#include <tuple>
#include <chrono>
#include <utility>
#include <cmath>
#include <queue>
#include <fmt/core.h>

std::vector<size_t> primesSieveEratosthenes(size_t max_value)
{
    std::vector<size_t> result;
    std::vector<bool> available(max_value + 1, true);
    for (size_t i = 2; i < max_value; ++i)
    {
        if (!available[i]) continue;
        result.push_back(i);
        for (size_t j = i * i; j <= max_value; j += i)
            available[j] = false;
    }
    return result;
}

std::vector<size_t> primesTrialDivision(size_t max_value)
{
    std::vector<size_t> result;
    for (size_t i = 2; i < max_value; ++i)
    {
        bool divisor_found = false;
        for (size_t prime : result)
        {
            if (prime * prime > i) break;
            if (i % prime == 0)
            {
                divisor_found = true;
                break;
            }
        }
        if (!divisor_found) result.push_back(i);
    }
    return result;
}

std::vector<size_t> primesDijkstra(size_t max_value)
{
#if 1
    if (max_value < 2) return {};
    std::vector<size_t> result = {2}, pool = {0};
    for (size_t value = 1, lim = 0, square = 4; value <= max_value;)
    {
        bool is_prime = false;
        do
        {
            value += 2;
            if (value > max_value) break;
            if (square <= value)
            {
                pool[lim++] = square;
                square = result[lim] * result[lim];
            }
            is_prime = true;
            for (size_t k = 1; is_prime && (k < lim); ++k)
            {
                if (pool[k] < value) pool[k] += result[k];
                is_prime = value != pool[k];
            }
        } while (!is_prime);
        if (is_prime)
        {
            result.push_back(value);
            pool.push_back(0);
        }
    }
    return result;
#else
    struct Information
    {
        size_t prime;
        size_t multiple;
    };
    std::vector<Information> pool;
    std::vector<size_t> result;
    for (size_t i = 2; i < max_value; ++i)
    {
        bool divisor_found = false;
        for (auto &[prime, multiple] : pool)
        {
            if (prime * prime > i) break;
            if (multiple == i)
            {
                multiple += prime;
                divisor_found = true;
            }
        }
        if (!divisor_found)
        {
            pool.push_back({i, i * i});
            result.push_back(i);
        }
    }
    return result;
#endif
}

int main(int, char **)
{
    constexpr size_t max_number = 100'000'000;
    auto elapsedTimeMilliseconds = [](auto start, auto end) -> double
    {
        return static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()) / 1000.0;
    };
    std::vector<std::vector<size_t> > baseline;
    std::vector<std::vector<double> > times;
    std::vector<std::string> methods;
    auto add = [&](std::string name, auto &&function, bool is_baseline = false) -> bool
    {
        methods.push_back(name);
        times.push_back({});
        for (size_t mv = 100, k = 0; mv <= max_number; mv *= 10, ++k)
        {
            auto start_timer = std::chrono::steady_clock::now();
            auto result = function(mv);
            auto end_timer = std::chrono::steady_clock::now();
            if (is_baseline)
                baseline.push_back(result);
            else if (baseline[k] != result) return false;
            times.back().push_back(elapsedTimeMilliseconds(start_timer, end_timer));
        }
        return true;
    };
    if (!add("Sieve", primesSieveEratosthenes, true))
    {
        std::cout << "Sieve of Eratosthenes method failed.\n";
        return EXIT_FAILURE;
    }
    if (!add("Trial", primesTrialDivision))
    {
        std::cout << "Trial Division method failed.\n";
        return EXIT_FAILURE;
    }
    if (!add("Dijkstra", primesDijkstra))
    {
        std::cout << "Trial Division method failed.\n";
        return EXIT_FAILURE;
    }
    size_t size_column_width = std::max(size_t{9},
            static_cast<size_t>(std::ceil(std::log10(max_number)) + 1)) + 2;
    size_t time_column_width = std::max_element(methods.cbegin(), methods.cend(),
            [](auto l, auto r) {return l.size() < r.size();})->size();
    for (size_t m = 0; m < methods.size(); ++m)
    {
        double max_time = std::ceil(*std::max_element(times[m].cbegin(), times[m].cend()));
        if (max_time < 2) max_time = 2;
        time_column_width = std::max(time_column_width,
                static_cast<size_t>(std::ceil(std::log10(max_time)) + 2));
    }
    time_column_width += 2;
    fmt::print("{:>{}}", "Max Value", size_column_width);
    fmt::print("{:>{}}", "N. Primes", size_column_width);
    for (size_t m = 0; m < methods.size(); ++m)
        fmt::print("{:>{}}", methods[m], time_column_width);
    fmt::print("\n");
    for (size_t mv = 100, k = 0; mv <= max_number; mv *= 10, ++k)
    {
        fmt::print("{:{}}", mv, size_column_width);
        fmt::print("{:{}}", baseline[k].size(), size_column_width);
        for (size_t m = 0; m < methods.size(); ++m)
            fmt::print("{:{}.1f}", times[m][k], time_column_width);
        fmt::print("\n");
    }
    
    return EXIT_SUCCESS;
}

