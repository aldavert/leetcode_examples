#include <iostream>
#include <vector>
#include <cstdlib>
#include <typeinfo>
#include <memory>
#include <cxxabi.h>
#include "utils/map.hpp"
#include "utils/enumerate.hpp"

std::string demangle(const char * name)
{
    int status = -4;
    std::unique_ptr<char, void(*)(void*)> res {
        abi::__cxa_demangle(name, NULL, NULL, &status),
        std::free
    };
    return (status == 0)?res.get():name;
}
std::string type(const auto &t) { return demangle(typeid(t).name()); }

int main(int /*argc*/, char * * /*argv*/)
{
    std::vector<int> first  = {1, 2, 3,  4,  5,  6,  7,  8,  9},
                     second = {2, 3, 5,  7, 11, 13, 17, 23, 29, 100, 100},
                     third  = {1, 4, 9, 16, 25, 36, 49, 64, 81};
    auto merged = utls::map([](auto a, auto b, auto c) { return a * b * c; }, first, second, third);
    for (auto [index, value] : utls::enumerate(merged))
        std::cout << index << ':' << value << ' ';
    std::cout << '\n';
    std::cout << type(merged) << '\n';
    
    return EXIT_SUCCESS;
}

