#include <iostream>
#include <vector>
#include <cstdlib>
#include <typeinfo>
#include <memory>
#include <cxxabi.h>
#include "utils/zip.hpp"
#include "utils/unzip.hpp"

std::string demangle(const char * name)
{
    int status = -4;
    std::unique_ptr<char, void(*)(void*)> res {
        abi::__cxa_demangle(name, NULL, NULL, &status),
        std::free
    };
    return (status == 0)?res.get():name;
}
std::string type(const auto &t) { return demangle(typeid(t).name()); }

template <size_t index, size_t size>
void recurseOut(std::ostream &out, const auto &tuple)
{
    out << std::get<index>(tuple);
    if constexpr (index + 1 < size)
    {
        out << ", ";
        recurseOut<index + 1, size>(out, tuple);
    }
}

template <typename ...ARGS>
std::ostream& operator<<(std::ostream &out, const std::tuple<ARGS...> &t)
{
    out << '{';
    recurseOut<0, sizeof...(ARGS)>(out, t);
    out << '}';
    return out;
};

int main(int /*argc*/, char * * /*argv*/)
{
    std::vector<int>    first  = {1, 2, 3,  4,  5,  6,  7,  8,  9};
    std::vector<float>  second = {2, 3, 5,  7, 11, 13, 17, 23, 29, 100, 100};
    std::vector<double> third  = {1, 4, 9, 16, 25, 36, 49, 64, 81};
    
    auto zipped = utls::zip(first, second, third);
    std::cout << type(zipped) << "\n\n";
    for (bool next = false; auto value : zipped)
    {
        if (next) [[likely]] std::cout << ", ";
        next = true;
        std::cout << value;
    }
    std::cout << '\n';

    auto unzipped = utls::unzip(zipped);
    std::cout << type(unzipped) << "\n\n";
    auto [first_u, second_u, third_u] = unzipped;
    for (size_t i = 0, n = first_u.size(); i < n; ++i)
    {
        if (first_u[i] != first[i]) std::cout << '*';
        std::cout << first_u[i] << ' ';
    }
    std::cout << '\n';
    for (size_t i = 0, n = second_u.size(); i < n; ++i)
    {
        if (second_u[i] != second[i]) std::cout << '*';
        std::cout << second_u[i] << ' ';
    }
    std::cout << '\n';
    for (size_t i = 0, n = third_u.size(); i < n; ++i)
    {
        if (third_u[i] != third[i]) std::cout << '*';
        std::cout << third_u[i] << ' ';
    }
    std::cout << '\n';
    
    return EXIT_SUCCESS;
}

