#include <iostream>
#include <vector>
#include <cstdlib>
#include "utils/utils.hpp"

int main(int /*argc*/, char * * /*argv*/)
{
    std::string dummy_string = " this is a testing   string which it's supposed to be split into   14 words.    ";
    auto result = utls::split(dummy_string);
    std::cout << "##" << dummy_string << "##\n";
    for (std::string word : result)
        std::cout << "##" << word << "##\n";
    std::cout << "\n\n";
    
    // -------------------------------------------------------------------------
    std::cout << "SPLITTING AT 'ing' (3 substrings):\n";
    result = utls::split(dummy_string, {"ing"});
    for (std::string word : result)
        std::cout << "##" << word << "##\n";
    std::cout << "\n\n";
    
    // -------------------------------------------------------------------------
    std::cout << "SPLITTING AT 'ing', 'in' and 'not present' (4 substrings):\n";
    result = utls::split(dummy_string, {"in", "ing", "not present"});
    for (std::string word : result)
        std::cout << "##" << word << "##\n";
    std::cout << "\n\n";
    
    // -------------------------------------------------------------------------
    std::cout << "NO SEPARATOR (1 substrings):\n";
    result = utls::split(dummy_string, {});
    for (std::string word : result)
        std::cout << "##" << word << "##\n";
    std::cout << "\n\n";

    // -------------------------------------------------------------------------
    std::cout << "Dummy string 'd c b a' (1 substrings):\n";
    result = utls::split(std::string("d c b a"));
    for (std::string word : result)
        std::cout << "##" << word << "##\n";
    std::cout << "\n\n";
    
    return EXIT_SUCCESS;
}

