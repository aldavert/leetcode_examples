#include <iostream>
#include <vector>
#include <cstdlib>
#include "utils/utils.hpp"

int main(int /*argc*/, char * * /*argv*/)
{
    auto test = [](std::string text) -> void
    {
        std::cout << "({' ', '\\n', '\\t'}, OVERLAP)";
        std::cout << "##" << text << "##"
                  << "##" << utls::ltrim(text) << "##"
                  << "##" << utls::rtrim(text) << "##"
                  << "##" << utls::trim(text) << "##\n";
        std::cout << "({'.'}, OVERLAP)";
        std::cout << "##" << text << "##"
                  << "##" << utls::ltrim(text, {"."}) << "##"
                  << "##" << utls::rtrim(text, {"."}) << "##"
                  << "##" << utls::trim(text, {"."}) << "##\n";
        std::cout << "({'ho', 'la'}, OVERLAP)";
        std::cout << "##" << text << "##"
                  << "##" << utls::ltrim(text, {"ho", "la"}) << "##"
                  << "##" << utls::rtrim(text, {"ho", "la"}) << "##"
                  << "##" << utls::trim(text, {"ho", "la"}) << "##\n";
        std::cout << "({' '}, IN_ORDER)";
        std::cout << "##" << text << "##"
                  << "##" << utls::ltrim(text, {" "}, utls::REPLACE::IN_ORDER) << "##"
                  << "##" << utls::rtrim(text, {" "}, utls::REPLACE::IN_ORDER) << "##"
                  << "##" << utls::trim(text, {" "}, utls::REPLACE::IN_ORDER) << "##\n";
        std::cout << "({' '}, SHORTEST_TO_LONGEST)";
        std::cout << "##" << text << "##"
                  << "##" << utls::ltrim(text, {" "}, utls::REPLACE::SHORTEST_TO_LONGEST) << "##"
                  << "##" << utls::rtrim(text, {" "}, utls::REPLACE::SHORTEST_TO_LONGEST) << "##"
                  << "##" << utls::trim(text, {" "}, utls::REPLACE::SHORTEST_TO_LONGEST) << "##\n";
        std::cout << "({' '}, LONGEST_TO_SHORTEST)";
        std::cout << "##" << text << "##"
                  << "##" << utls::ltrim(text, {" "}, utls::REPLACE::LONGEST_TO_SHORTEST) << "##"
                  << "##" << utls::rtrim(text, {" "}, utls::REPLACE::LONGEST_TO_SHORTEST) << "##"
                  << "##" << utls::trim(text, {" "}, utls::REPLACE::LONGEST_TO_SHORTEST) << "##\n";
        std::cout << "({'hol', 'ho', 'h'}, SHORTEST_TO_LONGEST)";
        std::cout << "##" << text << "##"
                  << "##" << utls::ltrim(text, {"hol", "ho", "h"}, utls::REPLACE::SHORTEST_TO_LONGEST) << "##"
                  << "##" << utls::rtrim(text, {"hol", "ho", "h"}, utls::REPLACE::SHORTEST_TO_LONGEST) << "##"
                  << "##" << utls::trim(text, {"hol", "ho", "h"}, utls::REPLACE::SHORTEST_TO_LONGEST) << "##\n";
        std::cout << "({'hol', 'ho', 'h'}, LONGEST_TO_SHORTEST)";
        std::cout << "##" << text << "##"
                  << "##" << utls::ltrim(text, {"hol", "ho", "h"}, utls::REPLACE::LONGEST_TO_SHORTEST) << "##"
                  << "##" << utls::rtrim(text, {"hol", "ho", "h"}, utls::REPLACE::LONGEST_TO_SHORTEST) << "##"
                  << "##" << utls::trim(text, {"hol", "ho", "h"}, utls::REPLACE::LONGEST_TO_SHORTEST) << "##\n";
    };
    test("   hola");
    test("hola  ");
    test("   hola  ");
    test("...hola..");
    
    return EXIT_SUCCESS;
}

