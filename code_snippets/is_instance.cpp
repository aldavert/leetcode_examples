// Checks if two template objects are the same 'base' class ignoring
// the template parameters of the second class. For example, in the function
// we add a require constrain that only allows the function to be called
// when the implicit conversion is to a float point type or when it's a
// conversion to std::complex regardless of the type of std::complex
// (std::complex<double>, std::complex<float>,...).

namespace
{
    template <typename, template <typename...> typename>
    struct is_instance_impl : public std::false_type {};
    template <template <typename...> typename U, typename...Ts>
    struct is_instance_impl<U<Ts...>, U> : public std::true_type {};
}
template <typename T, template <typename ...> typename U>
constexpr bool is_instance = is_instance_impl<std::decay_t<T>, U>::value;


template <typename T>
requires std::floating_point<T> || is_instance<T, std::complex>
inline operator T(void)
{
    T value;
    std::istringstream istr(m_data);
    istr.imbue(std::locale("C"));
    istr >> value;
    return value;
}
