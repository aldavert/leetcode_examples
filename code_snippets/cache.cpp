#include <map>
#include <tuple>
#include <type_traits>
#include <fmt/format.h>
#include <cstdlib>

template <typename Func, typename... Params>
auto cache(Func func, Params&&... params)
{
    using param_set = std::tuple<std::remove_cvref_t<Params>...>;
    param_set key{params...};
    using result_type =
        std::remove_cvref_t<std::invoke_result_t<Func, decltype(params)...> >;
    static std::map<param_set, result_type> cached_values;
    using value_type = decltype(cached_values)::value_type;
    auto itr = cached_values.find(key);
    if (itr != cached_values.end())
        return itr->second;
    return cached_values
        .insert(value_type{std::move(key), func(std::forward<Params>(params)...)})
        .first->second;
}

int factorial(int n)
{
    if (n < 2) return 1;
    return n * factorial(n - 1);
}

int& call_counter(void)
{
    static int counter = 0;
    return counter;
}

size_t fibonacci(size_t a)
{
    ++call_counter();
    if (a < 2) return a;
    return cache(fibonacci, a - 1) + cache(fibonacci, a - 2);
}

int main(int, char * *)
{
    fmt::print("factorial(5) = {}\n", cache(factorial, 5));
    fmt::print("Cached Fibonacci(10) = {} ({} calls).\n", fibonacci(10), call_counter());
    return EXIT_SUCCESS;
}

