#include <iostream>
#include <type_traits>
#include "index_enum.hpp"

#if 0
class ATTRBase
{
private:
    template <int IDX>
    struct Index
    {
        consteval static int value(void) { return IDX; }
    };
public:
    static constexpr Index<0> A{};
    static constexpr Index<1> B{};
    static constexpr Index<2> C{};
    consteval static size_t size(void) { return 3; }
};
#endif

INDEXENUM(ATTR, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T,
                U, V, W, X, Y, Z)

INDEXENUM(A3, A, B, C)
INDEXENUM(A7, P1, P2, P3, P4, P5, P6, P7)

template <typename ATT>
struct Features
{
    int data[ATT::size()] = {};
    template <int IDX> constexpr int& operator[](typename ATT::Index<IDX>)
    {
        return data[IDX];
    }
    template <int IDX>
    constexpr const int& operator[](typename ATT::Index<IDX>) const
    {
        return data[IDX];
    }
    std::vector<int> selection(const ATT::Selection &s)
    {
        std::vector<int> result;
        for (auto index : s.indexes())
            result.push_back(data[index]);
        return result;
    }
private:
};

int main(int, char **)
{
    Features<ATTR> features;
    for (int i = 0; i < (int)ATTR::size(); ++i)
        features.data[i] = 1;
    features[ATTR::A] = 9;
    features[ATTR::C] = -2;
    features[ATTR::X] = 32;
    for (int i = 0; i < (int)ATTR::size(); ++i)
        std::cout << ' ' << features.data[i];
    std::cout << '\n';
    ATTR::Selection bins = ATTR::A | ATTR::B | ATTR::X;
    bins.reset(ATTR::B);
    std::cout << "Is C? " << bins.is(ATTR::C) << '\n';
    bins.set(ATTR::C);
    std::cout << "Is C? " << bins.is(ATTR::C) << '\n';
    for (int value : features.selection(bins))
        std::cout << value << ' ';
    std::cout << '\n';
    std::cout << features[ATTR::A] << ' '
              << features[ATTR::B] << ' '
              << features[ATTR::C] << '\n';
    Features<A3> f3;
    Features<A7> f7;
    f3[A3::A] = 1;
    f3[A3::B] = 2;
    f3[A3::C] = 3;
    std::cout << "Number of features: " << A3::size() << "; Features:";
    for (size_t i = 0; i < A3::size(); ++i)
        std::cout << ' ' << f3.data[i];
    std::cout << '\n';
    std::cout << "Number of features: " << A7::size() << "; Features:";
    for (size_t i = 0; i < A7::size(); ++i)
        std::cout << ' ' << f7.data[i];
    std::cout << '\n';
    return 0;
}


