#ifndef __INDEX_ENUM_HEADER_FILE__
#define __INDEX_ENUM_HEADER_FILE__
#include <vector>
#include <bitset>

#define PARENS ()
// Rescan macro tokens 256 times
#define EXPAND(arg) EXPAND1(EXPAND1(EXPAND1(EXPAND1(arg))))
#define EXPAND1(arg) EXPAND2(EXPAND2(EXPAND2(EXPAND2(arg))))
#define EXPAND2(arg) EXPAND3(EXPAND3(EXPAND3(EXPAND3(arg))))
#define EXPAND3(arg) EXPAND4(EXPAND4(EXPAND4(EXPAND4(arg))))
#define EXPAND4(arg) arg
#define FOR_EACH(macro, ...)                                \
  __VA_OPT__(EXPAND(FOR_EACH_HELPER(0, macro, __VA_ARGS__)))
#define FOR_EACH_HELPER(count, macro, a1, ...)              \
  macro(a1, count)                                          \
  __VA_OPT__(FOR_EACH_AGAIN PARENS (count + 1, macro, __VA_ARGS__))
#define FOR_EACH_AGAIN() FOR_EACH_HELPER

#define COUNT(...)                                          \
  __VA_OPT__(EXPAND(COUNT_HELPER(1, __VA_ARGS__)))
#define COUNT_HELPER(count, a1, ...)                        \
    __VA_OPT__(DISCARD)(count)                              \
    __VA_OPT__(COUNT_AGAIN PARENS (count + 1, __VA_ARGS__))
#define COUNT_AGAIN() COUNT_HELPER
#define DISCARD(value) 

#define MEMBERS(name, count) static constexpr Index<count> name{};


#define INDEXENUM(name, ...)                                \
class name                                                  \
{                                                           \
public:                                                     \
    class Selection;                                        \
    template <int IDX>                                      \
    struct Index                                            \
    {                                                       \
        consteval static int value(void) { return IDX; }    \
        template <int IDX2>                                 \
        Selection operator|(name::Index<IDX2> other) const  \
        {                                                   \
            Selection result(*this);                        \
            return result | other;                          \
        }                                                   \
    };                                                      \
    class Selection                                         \
    {                                                       \
    public:                                                 \
        Selection(void) = default;                          \
        template <int IDX>                                  \
        Selection(name::Index<IDX>)                         \
        {                                                   \
            m_selection[IDX] = true;                        \
        }                                                   \
        template <int IDX>                                  \
        Selection operator|(name::Index<IDX>)               \
        {                                                   \
            Selection result = *this;                       \
            result.m_selection[IDX] = true;                 \
            return result;                                  \
        }                                                   \
        template <int IDX>                                  \
        inline bool is(name::Index<IDX>) const              \
        {                                                   \
            return m_selection[IDX];                        \
        }                                                   \
        inline void reset(void) { m_selection.reset(); }    \
        template <int IDX>                                  \
        inline void reset(name::Index<IDX>)                 \
        {                                                   \
            m_selection[IDX] = false;                       \
        }                                                   \
        inline void set(void)   { m_selection.set();   }    \
        template <int IDX>                                  \
        inline void set(name::Index<IDX>)                   \
        {                                                   \
            m_selection[IDX] = true;                        \
        }                                                   \
        std::vector<size_t> indexes(void) const             \
        {                                                   \
            std::vector<size_t> result;                     \
            for (size_t i = 0; i < m_selection.size(); ++i) \
                if (m_selection[i])                         \
                    result.push_back(i);                    \
            return result;                                  \
        }                                                   \
    private:                                                \
        static constexpr size_t SIZE = COUNT(__VA_ARGS__);  \
        std::bitset<SIZE> m_selection;                      \
    };                                                      \
    FOR_EACH(MEMBERS, __VA_ARGS__)                          \
    consteval static size_t size(void)                      \
    {                                                       \
        return COUNT(__VA_ARGS__);                          \
    }                                                       \
};

#endif
