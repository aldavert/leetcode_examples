import cv2

# Keybindings:
# q - Exit
# s - swap
# SPACE - capture

left = cv2.VideoCapture(3)
right = cv2.VideoCapture(2)
index = 0

while True:
    if not (left.grab() and right.grab()):
        print("No more frames")
        break
    _, leftFrame = left.retrieve();
    _, rightFrame = right.retrieve()
    cv2.imshow('left', leftFrame)
    cv2.imshow('right', rightFrame)
    key = cv2.waitKey(1) & 0xFF
    if key == ord('q'):
        break
    if key == ord('s'):
        left, right = right, left
    if key == ord(' '):
        cv2.imwrite(f'left{index:05d}.png', leftFrame)
        cv2.imwrite(f'right{index:05d}.png', rightFrame)
        index += 1
left.release()
right.release()
cv2.destroyAllWindows()
