#include <iostream>
#include <vector>
#include <cstdlib>
#include <tuple>

#if 1
template <typename T>
auto max(const std::vector<T> &container) -> std::tuple<T, size_t>
{
    T max_value = container[0];
    size_t max_position = 0;
    for (size_t i = 1; i < container.size(); ++i)
        if (container[i] > max_value)
            max_value = container[i],
            max_position = i;
    return { max_value, max_position };
}
#endif

int main(int, char * *)
{
    std::vector<int> elements = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    auto [value, position] = max(elements);
    std::cout << value << ' ' << position << '\n';
    return EXIT_SUCCESS;
}


