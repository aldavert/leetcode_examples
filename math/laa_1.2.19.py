# Problem from 'W.K. Nicholson, "Linear Algebra with Applications", McGraw-Hill 2013'.
# Problem 1.2.19: Given points (p_1, q_1), (p_2, q_2), and (p_3, q_3) in the plane
#                 with p_1, p_2, and p_3 distinct, show that they lie on some curve
#                 with equation y = a + bx + cx**2. [Hint: Solve for a, b, and c.]

import sympy

if __name__ == '__main__':
    p = sympy.symbols(' '.join([f'p_{i}' for i in range(1, 4)]))
    q = sympy.symbols(' '.join([f'q_{i}' for i in range(1, 4)]))
    a, b, c = sympy.symbols('a b c')
    m = sympy.Matrix()
    for i in range(3):
        new_row = sympy.Matrix([[1, p[i], p[i] ** 2, q[i]]])
        m = m.row_insert(i, new_row)
    # First row already has a leading one, set the values below this leading one
    # to zero.
    m[1, :] -= m[0, :]
    m[2, :] -= m[0, :]
    # Set second row leading one.
    m[1, :] /= m[1, 1]
    # Set the elements below this leading one to zero.
    m[2, :] -= m[1, :] * m[2, 1]
    # Set third (last) row leading one.
    m[2, :] /= m[2, 2]
    # Set the elements above the last leading one to zero.
    m[1, :] -= m[2, :] * m[1, 2]
    m[0, :] -= m[2, :] * m[0, 2]
    # Set the elements above the second leading one to zero.
    m[0, :] -= m[1, :] * m[0, 1]
    C = m[2, 3]
    m = m.subs(m[2, 3], c)
    B = m[1, 3]
    m = m.subs(m[1, 3], b)
    A = m[0, 3]
    m = m.subs(m[0, 3], a)
    sympy.pprint(m)
    print("A =")
    sympy.pprint(A.simplify())
    print("B =")
    sympy.pprint(B.simplify())
    print("C =")
    sympy.pprint(C.simplify())
