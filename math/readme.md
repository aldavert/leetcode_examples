# Math problems:
Code used to compute the solution math problems that aren't solved in their books.

## Linear algebra book:
- *W.K. Nicholson,* **Linear Algebra with Applications**, *McGraw-Hill 2003*
    - Problems form this books have the format: `laa_<problem_number>.*`
    - The solution of problems from **section 1.5** are validated using: [www.falstad.com/circuit/](www.falstad.com/circuit/).
