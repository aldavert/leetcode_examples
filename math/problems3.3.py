import numpy as np
import sympy as sp
import operator

def problem_3_3_2():
    l, k = sp.symbols('lambda,k')
    A = sp.Matrix([[1, 3, 2], [-1, 2, 1], [4, -1, -1]])
    roots = map(operator.itemgetter(0), sp.roots((l * sp.eye(3) - A).det(), l).items())
    roots = sorted(roots, key = lambda x: abs(x), reverse = True)
    P = sp.zeros(3, 3)
    idx = 0
    for root in roots:
        sol = (root * sp.eye(3) - A).nullspace()[0]
        P[:, idx] = sol
        idx += 1
    P = sp.Matrix([[1, -1, 1], [0, -1, -2], [1, 3, 3]])
    P_inv = (1/6) * sp.Matrix([[3, 6, 3], [-2, 2, 2], [1, -4, -1]])
    V0 = sp.Matrix([[2], [0], [1]])
    B = P_inv * V0
    print("Problem 3.3.2.d) Approximated lineal dynamic system:")
    sp.pprint(B[0, 0] * roots[0] ** k * P[:, 0])

if __name__ == '__main__':
    problem_3_3_2()
