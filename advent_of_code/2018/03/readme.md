# [Day 3: No Matter How You Slice It](https://adventofcode.com/2018/day/3)

#### Problem A

You are given a set of rectangle with their ID, the coordinates of the top-left corner and its width-height. What is the surface area of the regions with overlapping rectangles.

#### Problem B

Find the only rectangle that does not intersects with any other rectangle.

