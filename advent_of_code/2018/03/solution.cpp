#include <iostream>
#include <fstream>
#include <string>
#include <vector>

struct Rectangle
{
    int id = 0;
    int x0 = 0;
    int y0 = 0;
    int x1 = 0;
    int y1 = 0;
};

int main(int argc, char * * argv)
{
    std::vector<Rectangle> rectangles;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    int max_x = 0, max_y = 0;
    for (std::string line; std::getline(file, line); )
    {
        if (line.size() < 10) continue;
        auto sep_l = line.find('@');
        rectangles.push_back({});
        rectangles.back().id = std::stoi(line.substr(1, sep_l));
        auto sep_r = line.find(',', sep_l);
        rectangles.back().x0 = std::stoi(line.substr(sep_l + 1, sep_r - sep_l - 1));
        sep_l = sep_r + 1;
        sep_r = line.find(':', sep_l);
        rectangles.back().y0 = std::stoi(line.substr(sep_l, sep_r - sep_l));
        sep_l = sep_r + 2;
        sep_r = line.find('x', sep_l);
        rectangles.back().x1 = rectangles.back().x0
                             + std::stoi(line.substr(sep_l, sep_r - sep_l));
        sep_l = sep_r + 1;
        rectangles.back().y1 = rectangles.back().y0 + std::stoi(line.substr(sep_l));
        max_x = std::max(max_x, rectangles.back().x1);
        max_y = std::max(max_y, rectangles.back().y1);
    }
    file.close();
    std::vector<std::vector<int> > counter(max_y + 1, std::vector<int>(max_x + 1));
    for (auto [id, x0, y0, x1, y1] : rectangles)
        for (int y = y0; y < y1; ++y)
            for (int x = x0; x < x1; ++x)
                ++counter[y][x];
    int problem_a = 0;
    for (int y = 0; y <= max_y; ++y)
        for (int x = 0; x <= max_x; ++x)
            problem_a += counter[y][x] > 1;
    std::cout << "Problem A: " << problem_a << '\n';
    int problem_b = -1;
    for (auto [id, x0, y0, x1, y1] : rectangles)
    {
        int count = 0;
        for (int y = y0; y < y1; ++y)
            for (int x = x0; x < x1; ++x)
                count += counter[y][x];
        if (count == (y1 - y0) * (x1 - x0))
            problem_b = id;
    }
    std::cout << "Problem B: " << problem_b << '\n';
    
    return EXIT_SUCCESS;
}

