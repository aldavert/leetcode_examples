# [Day 11: Chronal Charge](https://adventofcode.com/2018/day/11)

#### Problem A

You have a grid that goes from coordinate `(1, 1)` to coordinate `(300, 300)`. The values of the grid are computed as:
1. `A = x + 10`, where `x` is the X-coordinate.
2. `B = ((A * y) + serial) * A` where `y` is the Y-coordinate and `serial` is your problem input.
3. Get the hundreds digit of `B` and subtract `5`. That is the value of the cell.

Find the top-left `<x,y>`-coordinates of the `3x3` square that contains the highest sum of all cell values.

#### Problem B

Now you are not limited to `3x3` squares, but you can have squares of any size (from `1x1` to `300x300`). What are the square parameters `<x>,<y>,<s>`, where `<x,y>` is the top-left coordinate of the square and `<s>` is its size?

