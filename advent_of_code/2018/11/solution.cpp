#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <fmt/core.h>

std::tuple<int, int, int> maxSum(int serial, int s)
{
    constexpr int size = 300;
    int accum[size + 1][size + 1] = {};
    for (int y = 0; y < size; ++y)
    {
        for (int x = 0; x < size; ++x)
        {
            int rack_id = x + 10;
            int battery = ((rack_id * y) + serial) * rack_id;
            int power = (battery / 100) % 10 - 5;
            accum[y + 1][x + 1] = power + accum[y][x + 1]
                                + accum[y + 1][x] - accum[y][x];
        }
    }
    int max_power = 0;
    std::tuple<int, int, int> result;
    for (int y = 0; y < size - s + 1; ++y)
    {
        for (int x = 0; x < size - s + 1; ++x)
        {
            int power = accum[y][x] + accum[y + s][x + s]
                      - accum[y + s][x] - accum[y][x + s];
            if (power > max_power)
            {
                result = {x, y, power};
                max_power = power;
            }
        }
    }
    return result;
}

int main(int argc, char * * argv)
{
    int serial = 5235;
    std::tuple<int, int> expected_a = {-1, -1};
    std::tuple<int, int, int> expected_b = {-1, -1, -1};
    if (argc == 2) serial = std::stoi(argv[1]);
    else if ((argc == 3) && (std::string(argv[1]) == "test"))
    {
        if (std::string id = argv[2]; id == "0")
            serial = 18, expected_a = {33, 45}, expected_b = {90, 269, 16};
        else if (id == "1")
            serial = 42, expected_a = {21, 61}, expected_b = {232, 251, 12};
        else
        {
            std::cerr << "[ERROR] Wrong test case id.\n";
            return EXIT_FAILURE;
        }
    }
    else if (argc != 1)
    {
        std::cerr << "[ERROR] Incorrect parameters.\n";
        return EXIT_FAILURE;
    }
    auto [a_x, a_y, a_power] = maxSum(serial, 3);
    std::cout << "Problem A: " <<  a_x << ',' << a_y;
    if (expected_a != std::make_tuple(-1, -1))
    {
        if (expected_a == std::make_tuple(a_x, a_y)) std::cout << " CORRECT";
        else std::cout << " WRONG (" << std::get<0>(expected_a)
                       << ',' << std::get<1>(expected_a)
                       << " was expected instead).";
    }
    std::cout << '\n';
    int b_x = 0, b_y = 0, b_s = 0, b_power = -9 * 300 * 300;
    for (int s = 1; s <= 300; ++s)
    {
        auto [_x, _y, _power] = maxSum(serial, s);
        if (_power > b_power)
        {
            b_x = _x;
            b_y = _y;
            b_s = s;
            b_power = _power;
        }
    }
    std::cout << "Problem B: " <<  b_x << ',' << b_y << ',' << b_s;
    if (expected_b != std::make_tuple(-1, -1, -1))
    {
        if (expected_b == std::make_tuple(b_x, b_y, b_s)) std::cout << " CORRECT";
        else std::cout << " WRONG (" << std::get<0>(expected_b) << ','
                       << std::get<1>(expected_b) << ',' << std::get<2>(expected_b)
                       << " was expected instead).";
    }
    std::cout << '\n';
    return EXIT_SUCCESS;
}

