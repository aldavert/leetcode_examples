# [Day 19: Go With The Flow](https://adventofcode.com/2018/day/19)

You have a machine similar to the one in [problem 16](../16/readme.md). Now, the machine has 6 registers instead of four and it also has the new *instruction* `#ip <register>` that bounds the **program counter** to one of the machine of the register. For example, `#ip 2` makes that register `2` is the **program counter** of the machine.

This is used to modify the **program counter** so the machine can have control flow instructions (i.e. relative, absolute and conditional jumps). The instructions applied on the **program counter** register are executed as a normal instruction, so they modify the **program counter** register and the they also increase the **program counter** register (not like a regular jump instruction that just changes the value of the **program counter** to the desired value).

#### Problem A

What is the value left in register 0 when the given program halts?

#### Problem B

Now, restart the program with all registers to `0` except register `0` that starts with the value `1`. Now, what is the value left in register 0 when the given program halts?

