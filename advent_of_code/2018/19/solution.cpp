#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <unordered_map>

struct Instruction
{
    int opcode;
    int regval_a;
    int regval_b;
    int reg_c;
};

void execute(std::vector<Instruction> program, int * regs, int pc_register)
{
    const int n = static_cast<int>(program.size());
    for (int &pc = regs[pc_register]; (pc >= 0) && (pc < n); ++pc)
    {
        const Instruction &code = program[pc];
        switch (code.opcode)
        {
        case 0: // addr: reg_c = reg_a + reg_b
            regs[code.reg_c] = regs[code.regval_a] + regs[code.regval_b];
            break;
        case 1: // addi: reg_c = reg_a + val_b
            regs[code.reg_c] = regs[code.regval_a] + code.regval_b;
            break;
        case 2: // mulr: reg_c = reg_a * reg_b
            regs[code.reg_c] = regs[code.regval_a] * regs[code.regval_b];
            break;
        case 3: // muli: reg_c = reg_a * val_b
            regs[code.reg_c] = regs[code.regval_a] * code.regval_b;
            break;
        case 4: // banr: reg_c = reg_a & reg_b
            regs[code.reg_c] = regs[code.regval_a] & regs[code.regval_b];
            break;
        case 5: // bani: reg_c = reg_a & val_b
            regs[code.reg_c] = regs[code.regval_a] & code.regval_b;
            break;
        case 6: // borr: reg_c = reg_a | reg_b
            regs[code.reg_c] = regs[code.regval_a] | regs[code.regval_b];
            break;
        case 7: // bori: reg_c = reg_a | val_b
            regs[code.reg_c] = regs[code.regval_a] | code.regval_b;
            break;
        case 8: // setr: reg_c = reg_a
            regs[code.reg_c] = regs[code.regval_a];
            break;
        case 9: // seti: reg_c = val_a
            regs[code.reg_c] = code.regval_a;
            break;
        case 10: // gtir: reg_c = (val_a > reg_b)
            regs[code.reg_c] = (code.regval_a > regs[code.regval_b]);
            break;
        case 11: // gtri: reg_c = (reg_a > val_b)
            regs[code.reg_c] = (regs[code.regval_a] > code.regval_b);
            break;
        case 12: // gtrr: reg_c = (reg_a > reg_b)
            regs[code.reg_c] = (regs[code.regval_a] > regs[code.regval_b]);
            break;
        case 13: // eqir: reg_c = (val_a == reg_b)
            regs[code.reg_c] = (code.regval_a == regs[code.regval_b]);
            break;
        case 14: // eqri: reg_c = (reg_a == val_b)
            regs[code.reg_c] = (regs[code.regval_a] == code.regval_b);
            break;
        case 15: // eqrr: reg_c = (reg_a == reg_b)
            regs[code.reg_c] = (regs[code.regval_a] == regs[code.regval_b]);
            break;
        default:
            std::cerr << "[ERROR] Unknown instruction.\n";
            return;
        }
    }
}

int main(int argc, char * * argv)
{
    const std::unordered_map<std::string, int> inst2opcode = {
        {"addr",  0}, {"addi",  1}, {"mulr",  2}, {"muli",  3}, {"banr",  4},
        {"bani",  5}, {"borr",  6}, {"bori",  7}, {"setr",  8}, {"seti",  9},
        {"gtir", 10}, {"gtri", 11}, {"gtrr", 12}, {"eqir", 13}, {"eqri", 14},
        {"eqrr", 15} };
    int pc_register = 6;
    std::vector<Instruction> program;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.substr(0, 4) == "#ip ")
            pc_register = std::stoi(line.substr(4));
        else
        {
            std::istringstream iss(line);
            std::string inst, param1, param2, param3;
            iss >> inst >> param1 >> param2 >> param3;
            program.push_back({inst2opcode.at(inst), std::stoi(param1),
                               std::stoi(param2), std::stoi(param3)});
        }
    }
    file.close();
    int registers[7] = {};
    execute(program, registers, pc_register);
    std::cout << "Problem A: " << registers[0] << '\n';
    long n = 836 + 22 * program[21].regval_b + program[23].regval_b + 10'550'400,
         sq = static_cast<long>(std::sqrt(n));
    long sum = 0;
    for (int i = 1; i <= sq; ++i)
        if (n % i == 0)
            sum += i + n / i;
    std::cout << "Problem B: " << sum - sq * (sq * sq == n) << '\n';
    return EXIT_SUCCESS;
}

