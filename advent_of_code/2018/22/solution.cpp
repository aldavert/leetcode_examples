#define DEBUG 0

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <limits>
#include <queue>
#if DEBUG
#include <fmt/core.h>
#include <fmt/color.h>
#endif

int main(int argc, char * * argv)
{
    //constexpr int SIDE = 2'000;
    int depth = 0;
    static std::pair<int, int> target = {0, 0};
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        auto colon = line.find(": ");
        if (colon == std::string::npos) continue;
        if (line.substr(0, colon) == "depth")
        {
            try
            {
                depth = std::stoi(line.substr(colon + 2));
            }
            catch (...)
            {
                std::cerr << "[WARNING] Exception while trying to retrieve <depth>.\n";
                continue;
            }
        }
        else if (line.substr(0, colon) == "target")
        {
            try
            {
                auto coma = line.find(',', colon + 2);
                target.first = std::stoi(line.substr(colon + 2, coma - colon - 2));
                target.second = std::stoi(line.substr(coma + 1));
            }
            catch (...)
            {
                std::cerr << "[WARNING] Exception while trying to retrieve <target>.\n";
                continue;
            }
        }
    }
    file.close();
    std::cout << "Depth: " << depth << "; Target: (" << target.first << ", "
              << target.second << ")\n";
    const long n_cols = static_cast<long>(target.first + 100),
               n_rows = static_cast<long>(target.second + 100);
    std::vector<std::vector<long> > erosion(n_rows, std::vector<long>(n_cols));
    std::vector<std::string> cave(n_rows, std::string(n_cols, 'X'));
    for (long y = 1; y < n_rows; ++y)
        erosion[y][0] = (y * 48271 + depth) % 20183;
    for (long x = 1; x < n_cols; ++x)
        erosion[0][x] = (x * 16807 + depth) % 20183;
    for (long y = 1; y < n_rows; ++y)
        for (long x = 1; x < n_cols; ++x)
            erosion[y][x] = ((y != target.second) || (x != target.first))
                          ?(erosion[y - 1][x] * erosion[y][x - 1] + depth) % 20183
                          :(depth) % 20183;
    erosion[0][0] = erosion[target.second][target.first] = 0;
    for (long y = 0; y < n_rows; ++y)
        for (long x = 0; x < n_cols; ++x)
            cave[y][x] = static_cast<char>(erosion[y][x] % 3);
    int risk = 0;
    for (long y = 0; y <= target.second; ++y)
        for (long x = 0; x <= target.first; ++x)
            risk += static_cast<int>(cave[y][x]);
    std::cout << "Problem A: " << risk << '\n';
    
    constexpr int dirs[] = {-1, 0, 1, 0, -1};
    constexpr int INF = std::numeric_limits<int>::max();
    std::vector<std::vector<std::vector<int> > > celltime(3,
            std::vector<std::vector<int> >(n_rows, std::vector<int>(n_cols, INF)));
    struct Location
    {
        int x = -1;
        int y = -1;
        int time = 0;
        char state = 0; // 0 - Empty handed, 1 - Torch, 2 - Climbing gear.
        bool operator<(const Location &other) const { return time > other.time; };
    };
    std::priority_queue<Location> q;
    q.push({0, 0, 0, 1});
    while (!q.empty())
    {
        auto [x, y, time, state] = q.top();
        q.pop();
        if ((time >= celltime[state][y][x])
        ||  (time >= celltime[1][target.second][target.first])) continue;
        celltime[state][y][x] = time;
        for (char s = 0; s < 3; ++s)
            if ((s != state) && (s != cave[y][x]))
                q.push({x, y, time + 7, s});
        for (int k = 0; k < 4; ++k)
        {
            int nx = x + dirs[k], ny = y + dirs[k + 1];
            if ((nx < 0) || (ny < 0) || (nx >= n_cols) || (ny >= n_rows)) continue;
            if (cave[ny][nx] == state) continue;
            q.push({nx, ny, time + 1, state});
        }
    }
    std::cout << "Problem B: " << celltime[1][target.second][target.first] << '\n';
#if DEBUG
    constexpr char symbols[] = { '.', '=', '|' };
    for (int y = 0; y < 20; ++y)
    {
        for (int x = 0; x < 20; ++x)
        {
            auto format = ((x <= 10) && (y <= 10))
                        ?(bg(fmt::color(0x005500)) | fg(fmt::color(0xAAFFAA)))
                        :(bg(fmt::color(0x550000)) | fg(fmt::color(0xFFAAAA)));
            fmt::print(format, "{}", symbols[static_cast<int>(cave[y][x])]);
        }
        fmt::print("\n");
    }
    for (int y = 0; y < 20; ++y)
    {
        for (int x = 0; x < 20; ++x)
        {
            auto format = ((x <= 10) && (y <= 10))
                        ?fg(fmt::color(0xAAFFAA))
                        :fg(fmt::color(0xFFAAAA));
            if (celltime[1][y][x] == INF) fmt::print(format, " XXX");
            else fmt::print(format, " {:3d}", celltime[1][y][x]);
        }
        fmt::print("\n");
    }
#endif
    return EXIT_SUCCESS;
}

