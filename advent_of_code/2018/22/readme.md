# [Day 22: Mode Maze](https://adventofcode.com/2018/day/22)

You are given the depth of a cave and the coordinates of a target. Given these two numbers, you can compute the information of the 2D grid representing the cave.

First, you have to compute the **geologic index** of each cell:
- The regions `(0, 0)` (your initial position) and the ***target position*** region have a *geologic index* of 0.
- If the region's `Y` coordinate is `0`, the geologic index is its `X` multiplied by `16807`.
- If the region's `X` coordinate is `0`, the geologic index is its `Y` multiplied by `48271`.
- Otherwise, the region's geologic index is the result multiplying the **erosion level** of the regions at `(X - 1, Y)` and `(X, Y - 1)`.

The **erosion level** of a cell is it's **geologic index** plus the cave's depth, all modulo `20183`.

Finally, we can derive the type of cell by computing the modulo `3` of the **erosion level** so that if the result is
- `0`, the cell is **rocky**.
- `1`, the cell is **wet**.
- `2`, the cell is **narrow**.

#### Problem A

The risk level of a region is computed by the sum of all the cells in a region. The risk of each cell is `0` for **rocky** cells, `1` for **wet** cells and `2` for **narrow** cells.

What is the risk level of the rectangle between the origin at `(0, 0)` and the target's coordinates?

#### Problem B

Once you have generated the layout of the cave, you have to find out what is the fastest route to go from the origin location at `(0, 0)` to the target coordinates. Cells with negative coordinates are inaccessible while cells further away of the target coordinates are available and can be accessed.

You have a **torch** and **climbing gear**. You can only at most equip one, so you can be **empty handed**, **carry a torch** or **wear climbing gear**. Changing your equipment takes `7` minutes, so going from **carrying a torch** to **wearing climbing gear** or **being empty handed** it takes both `7` minutes.

To move from one cell to another cell it takes `1` minute. To enter a cell you have to fulfill the following constrains:
- In **rocky** cells, you cannot enter if you are **empty handed**.
- In **wet** cells, you cannot enter if you **carry a torch**.
- In **narrow** cells, you cannot enter if you **wear climbing gear**.
Additionally, you need to have a **torch** equipped when you enter the target cell.

What is the fewest number of minuted need to go from the origin to the target coordinates?

