#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <numeric>
#include <unordered_set>

int main(int argc, char * * argv)
{
    std::vector<long> values;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line[0] == '+') values.push_back(std::stol(line.substr(1)));
        else values.push_back(std::stol(line));
    }
    file.close();
    
    std::cout << "Problem A: " << std::accumulate(values.begin(), values.end(), 0l)
              << '\n';
    std::unordered_set<long> seen;
    long accumulate = 0;
    for (size_t i = 0; true; ++i)
    {
        if (i == values.size()) i = 0;
        accumulate += values[i];
        if (seen.find(accumulate) != seen.end())
        {
            std::cout << "Problem B: " << accumulate << '\n';
            break;
        }
        seen.insert(accumulate);
    }
    return EXIT_SUCCESS;
}

