# [Day 1: Chronal Calibration](https://adventofcode.com/2018/day/1)

#### Problem A

You are given a list of positive and negative integers. If the initial value is zero, what is the result of adding all values?

#### Problem B

If the list of integers repeats infinitely and you keep accumulates the values, find the first accumulated value that is repeated.

