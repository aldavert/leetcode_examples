# [Day 7: The Sum of Its Parts](https://adventofcode.com/2018/day/7)

#### Problem A

You have a set of tasks that need to be completed once and, you are given a set of constrains that define which tasks need to have been previously completed before they can be started. The constrains have the format:

```
Step C must be finished before step A can begin.
```

Task `A` can only be started if task `C` has been already completed. While completing all the tasks, you have to select a task that has **not been previously completed**, that **fulfills all constrains** and, when there are multiple options, the one that is **the smallest** identifier.

If you concatenated the IDs of all tasks as they are completed, what is the resulting string?

#### Problem B

Now, you can process up to 5 times in parallel and tasks require time to complete. The amount of time needed to complete the task is `60 seconds + <Task IDs> seconds` where `<Task IDs>` is the numerical value assigned at the task letter, starting with `A=1` and ending with `Z=26`.

What is the total amount of seconds to complete all the tasks?

