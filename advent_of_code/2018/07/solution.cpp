#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <queue>

int main(int argc, char * * argv)
{
    int constrains[26][26] = {}, number_of_constrains[26] = {};
    bool present[26] = {};
    int number_of_workers = 5, task_offset_time = 61;
    
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    int number_of_lines = 0;
    for (std::string line; std::getline(file, line); )
    {
        if (line.size() != 48)
        {
            std::cout << "[WARING] Line has an unexpected format";
            return EXIT_FAILURE;
        }
        int src = line[5], dst = line[36];
        line[5] = line[36] = '#';
        if (line != "Step # must be finished before step # can begin.")
        {
            std::cout << "[WARING] Line has an unexpected format";
            return EXIT_FAILURE;
        }
        src -= 'A';
        dst -= 'A';
        present[src] = present[dst] = true;
        constrains[dst][number_of_constrains[dst]++] = src;
        ++number_of_lines;
    }
    file.close();
    if (number_of_lines < 10) // Parameters for the example.
    {
        number_of_workers = 2;
        task_offset_time = 1;
    }
    
    std::priority_queue<int, std::vector<int>, std::greater<> > available;
    std::string problem_a;
    bool done[26] = {};
    auto activate = [&](void) -> void
    {
        for (int i = 0; i < 26; ++i)
        {
            if (done[i] || !present[i]) continue;
            bool current_active = true;
            for (int j = 0; j < number_of_constrains[i]; ++j)
                current_active = current_active && done[constrains[i][j]];
            if (current_active) available.push(i);
        }
    };
    
    // Initialization: add tasks without constraints into the queue.
    for (int i = 0; i < 26; ++i)
        if (present[i] && (number_of_constrains[i] == 0))
            available.push(i);
    // Process the tasks:
    while (!available.empty())
    {
        int current = available.top();
        available.pop();
        if (done[current]) continue;
        problem_a += static_cast<char>('A' + current);
        done[current] = true;
        activate();
    }
    std::cout << "Problem A: " << problem_a << '\n';
    
    // Initialization: add tasks without constraints into the queue.
    for (int i = 0; i < 26; ++i)
    {
        done[i] = false;
        if (present[i] && (number_of_constrains[i] == 0))
            available.push(i);
    }
    std::vector<std::pair<int, int> > workers(number_of_workers, {0, -1});
    bool working_on_it[26] = {};
    int runtime = 0, workers_available = number_of_workers;
    // Process the tasks:
    while (!available.empty())
    {
        while (!available.empty() && (workers_available > 0))
        {
            int current = available.top();
            available.pop();
            if (done[current]) continue;
            if (working_on_it[current]) continue;
            for (int w = 0; w < number_of_workers; ++w)
            {
                if (workers[w].second == -1)
                {
                    workers[w] = { runtime + task_offset_time + current, current };
                    working_on_it[current] = true;
                    --workers_available;
                    break;
                }
            }
        }
        int min_time = 1'000'000'000, min_index = -1;
        for (int w = 0; w < number_of_workers; ++w)
        {
            if ((workers[w].second != -1) && (workers[w].first < min_time))
            {
                min_time = workers[w].first;
                min_index = w;
            }
        }
        if (min_index == -1) break;
        done[workers[min_index].second] = true;
        runtime = workers[min_index].first;
        working_on_it[workers[min_index].second] = false;
        workers[min_index] = {0, -1};
        ++workers_available;
        activate();
    }
    std::cout << "Problem B: " << runtime << '\n';
    
    return EXIT_SUCCESS;
}

