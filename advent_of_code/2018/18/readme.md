# [Day 18: Settlers of The North Pole](https://adventofcode.com/2018/day/18)

#### Problem A

You have a map where there are three types of cells: **open ground** (`.`), **trees** (`|`) and **lumberyards** (`#`). At each step, the content of the cells is changed depending these rules:

- **Open ground** becomes **trees** if its surrounded by *three or more* trees. Otherwise, nothing happens.
- **Trees** becomes a **lumberyard** if *three or more* adjacent **lumberyard**. Otherwise, nothing happens.
- **Lumberyards** remain **lumberyards** if there is at least *one lumberyard* and at least *one tree*. Otherwise, it becomes **open ground**.

The changes happens all at once and based only on the previous step state.

The resource score is computed as the product between the number of **trees** cells and the number of **lumberyards**. What is the resource score after 10 steps?

#### Problem B

What is the resource score after 1'000'000'000 steps?


