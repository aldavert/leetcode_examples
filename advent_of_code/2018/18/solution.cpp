#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>

long resourceScore(std::vector<std::string> world, size_t number_of_iterations)
{
    std::pair<int, int> neighbors[] = {
        {-1, -1}, {0, -1}, {1, -1}, {-1, 0}, {1, 0}, {-1, 1}, {0, 1}, {1, 1}};
    if ((world.size() == 0) || (world[0].size() == 0)) return -1;
    const int n_rows = static_cast<int>(world.size()),
              n_cols = static_cast<int>(world[0].size());
    for (int r = 0; r < n_rows; ++r)
        if (static_cast<int>(world[r].size()) != n_cols)
            return -1;
    std::unordered_map<std::string, size_t> lut;
    for (size_t iteration = 0; iteration < number_of_iterations; ++iteration)
    {
        std::vector<std::string> next = world;
        for (int r = 0; r < n_rows; ++r)
        {
            for (int c = 0; c < n_cols; ++c)
            {
                int trees = 0, lumberyards = 0, open_space = 0;
                for (int k = 0; k < 8; ++k)
                {
                    int nr = r + neighbors[k].first,
                        nc = c + neighbors[k].second;
                    if ((nr < 0) || (nc < 0) || (nr >= n_rows) || (nc >= n_cols))
                        continue;
                    if      (world[nr][nc] == '|') ++trees;
                    else if (world[nr][nc] == '#') ++lumberyards;
                    else if (world[nr][nc] == '.') ++open_space;
                    else
                    {
                        std::cerr << "[ERROR] Unexpected cell type.\n";
                        return -1;
                    }
                }
                if ((world[r][c] == '.') && (trees >= 3)) next[r][c] = '|';
                if ((world[r][c] == '|') && (lumberyards >= 3)) next[r][c] = '#';
                if ((world[r][c] == '#') && ((lumberyards < 1) || (trees < 1)))
                    next[r][c] = '.';
            }
        }
        world = next;
        std::string flat_world;
        for (const auto &row : world)
            flat_world += row;
        if (auto search = lut.find(flat_world); search != lut.end())
        {
            size_t loop_size = iteration - search->second;
            number_of_iterations = iteration
                                 + (number_of_iterations - iteration) % loop_size;
            lut.clear();
        }
        else lut[flat_world] = iteration;
    }
    long trees = 0, lumberyards = 0;
    for (const auto &row : world)
        for (char cell : row)
            trees += (cell == '|'),
            lumberyards += (cell == '#');
    return trees * lumberyards;
}

int main(int argc, char * * argv)
{
    std::vector<std::string> world;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        world.push_back(line);
    file.close();
    std::cout << "Problem A: " << resourceScore(world, 10) << '\n';
    std::cout << "Problem B: " << resourceScore(world, 1'000'000'000) << '\n';
    return EXIT_SUCCESS;
}

