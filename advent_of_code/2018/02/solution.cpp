#include <iostream>
#include <fstream>
#include <vector>
#include <string>

long checksum(const std::vector<std::string> &identifiers)
{
    long frequency_two = 0, frequency_three = 0;
    for (const auto &id : identifiers)
    {
        int histogram[26] = {};
        for (char letter : id)
            ++histogram[letter - 'a'];
        bool has_two = false, has_three = false;
        for (size_t i = 0; i < 26; ++i)
            has_two = has_two || (histogram[i] == 2),
            has_three = has_three || (histogram[i] == 3);
        frequency_two += has_two;
        frequency_three += has_three;
    }
    return frequency_two * frequency_three;
}

std::string commonString(const std::vector<std::string> &identifiers)
{
    for (size_t i = 0; i < identifiers.size(); ++i)
    {
        for (size_t j = 0; j < identifiers.size(); ++j)
        {
            if ((i == j) || (identifiers[i].size() != identifiers[j].size()))
                continue;
            size_t count = 0;
            for (size_t k = 0; k < identifiers[i].size(); ++k)
                count += identifiers[i][k] == identifiers[j][k];
            if (count + 1 == identifiers[i].size())
            {
                std::string result;
                for (size_t k = 0; k < identifiers[i].size(); ++k)
                    if (identifiers[i][k] == identifiers[j][k])
                        result += identifiers[i][k];
                return result;
            }
        }
    }
    return "";
}

int main(int argc, char * * argv)
{
    std::vector<std::string> identifiers;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        identifiers.push_back(line);
    file.close();
    std::cout << "Problem A: " << checksum(identifiers) << '\n';
    std::cout << "Problem B: " << commonString(identifiers) << '\n';
    return EXIT_SUCCESS;
}

