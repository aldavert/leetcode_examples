# [Day 2: Inventory Management System](https://adventofcode.com/2018/day/2)

#### Problem A

You are given a set of strings. Count the **amount of strings** that have characters that **exactly appear two and three times** in the string (in any position/order). Then, return the product of the amount of both strings types.

#### Problem B

Search the only two strings that only differ by one character and return the string with the common part (i.e. the string removing the character that is different).

