#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <queue>

struct GameResult
{
    int iterations = -1;
    int total_health = -1;
    int score = -1;
    int deaths[2] = {};
};

GameResult calcScore(const std::vector<std::string> &battlemap, int elf_attack = 3)
{
    constexpr int neighbors[4][2] = { {0, -1}, {-1, 0}, {1, 0}, {0, 1} };
    constexpr int dirs[] = {-1, 0, 1, 0, -1};
    struct Unit
    {
        Unit(void) = default;
        Unit(bool elf, int h, int a) :
            is_elf(elf), health(h), attack(a), can_move(false) {}
        bool is_elf = false;
        int health = 200;
        int attack = 3;
        bool can_move = false;
    };
    const int n_rows = static_cast<int>(battlemap.size()),
              n_cols = static_cast<int>(battlemap[0].size());
    std::vector<Unit> units;
    std::vector<std::string> battlefield;
    int iteration = 0;
    unsigned int alive[2] = {}, total_units[2] = {};
    
    auto moveNext = [&](int row, int col, int idx) -> std::pair<int, int>
    {
        const bool is_elf = units[idx].is_elf;
        for (int i = 0; i < 4; ++i)
        {
            std::pair<int, int> curr = {row + neighbors[i][1], col + neighbors[i][0]};
            if (battlefield[curr.first][curr.second] < 'A') continue;
            if (units[battlefield[curr.first][curr.second] - 'A'].is_elf != is_elf)
                return {row, col};
        }
        std::queue<std::pair<int, int> > q;
        std::vector<std::vector<bool> > visited(n_rows,
                std::vector<bool>(n_cols, false));
        for (int k = 0; k < 4; ++k)
            q.push({row + dirs[k], col + dirs[k + 1]});
        std::vector<std::pair<int, int> > enemies;
        while (!q.empty() && enemies.empty())
        {
            for (size_t i = q.size(); i > 0; --i)
            {
                auto [r, c] = q.front();
                q.pop();
                if (visited[r][c]) continue;
                visited[r][c] = true;
                if (battlefield[r][c] == ' ')
                    for (int k = 0; k < 4; ++k)
                        q.push({r + dirs[k], c + dirs[k + 1]});
                else if (battlefield[r][c] != '#')
                    if (is_elf != units[battlefield[r][c] - 'A'].is_elf)
                        enemies.push_back({r, c});
            }
        }
        if (enemies.empty()) return std::make_pair(row, col);
        std::sort(enemies.begin(), enemies.end());
        std::vector<std::vector<int> > distance(n_rows,
                std::vector<int>(n_cols, n_rows * n_cols * 2));
        q = {};
        distance[enemies.front().first][enemies.front().second] = 0;
        for (int k = 0; k < 4; ++k)
        {
            int nr = enemies.front().first + dirs[k],
                nc = enemies.front().second + dirs[k + 1];
            if (battlefield[nr][nc] == ' ')
                q.push({nr, nc});
        }
        for (int dist = 1, not_done = 1; !q.empty() && not_done; ++dist)
        {
            for (size_t i = q.size(); i > 0; --i)
            {
                auto [r, c] = q.front();
                q.pop();
                if (distance[r][c] <= dist) continue;
                distance[r][c] = dist;
                if ((r == row) && (c == col))
                    not_done = 0;
                else
                {
                    for (int k = 0; k < 4; ++k)
                    {
                        int nr = r + dirs[k],
                            nc = c + dirs[k + 1];
                        if (battlefield[nr][nc] == ' ')
                            q.push({nr, nc});
                    }
                }
            }
        }
        std::pair<int, int> selected_position;
        int selected_distance = n_rows * n_cols * 2;
        for (int i = 0; i < 4; ++i)
        {
            std::pair<int, int> curr = {row + neighbors[i][1], col + neighbors[i][0]};
            if (distance[curr.first][curr.second] < selected_distance)
            {
                selected_distance = distance[curr.first][curr.second];
                selected_position = curr;
            }
        }
        return selected_position;
    };
    auto attack = [&](int row, int col, int current) -> bool
    {
        int selected_health = 1'000'000;
        std::pair<int, int> selected_pos = {-1, -1};
        for (int i = 0; i < 4; ++i)
        {
            std::pair<int, int> curr = {row + neighbors[i][1],
                                        col + neighbors[i][0]};
            if (battlefield[curr.first][curr.second] < 'A') continue;
            int other = battlefield[curr.first][curr.second] - 'A';
            if ((units[other].is_elf != units[current].is_elf)
            &&  (units[other].health < selected_health))
            {
                selected_health = units[other].health;
                selected_pos = curr;
            }
        }
        if (selected_pos.first == -1) return false;
        int other = battlefield[selected_pos.first][selected_pos.second] - 'A';
        units[other].health -= units[current].attack;
        if (units[other].health <= 0)
        {
            --alive[units[other].is_elf];
            battlefield[selected_pos.first][selected_pos.second] = ' ';
            return true;
        }
        return false;
    };
    
    for (int row = 0; row < n_rows; ++row)
    {
        battlefield.push_back("");
        for (int col = 0; col < n_cols; ++col)
        {
            battlefield.back() += ((battlemap[row][col] == '#')?'#':' ');
            if ((battlemap[row][col] == 'E') || (battlemap[row][col] == 'G'))
            {
                battlefield.back().back() = static_cast<char>('A' + units.size());
                bool is_elf = (battlemap[row][col] == 'E');
                units.push_back({is_elf, 200, (is_elf)?elf_attack:3});
                ++alive[is_elf];
            }
        }
    }
    total_units[0] = alive[0];
    total_units[1] = alive[1];
    bool last_kill = false;
    for (iteration = 0; (alive[0] != 0) && (alive[1] != 0); ++iteration)
    {
        for (auto &unit : units) unit.can_move = true;
        for (int row = 0; row < n_rows; ++row)
        {
            for (int col = 0; col < n_cols; ++col)
            {
                char current = battlefield[row][col];
                if (current < 'A') continue;
                if (!units[current - 'A'].can_move) continue;
                units[current - 'A'].can_move = false;
                battlefield[row][col] = ' ';
                auto [e_row, e_col] = moveNext(row, col, current - 'A');
                battlefield[e_row][e_col] = current;
                last_kill = attack(e_row, e_col, current - 'A');
            }
        }
    }
    GameResult result;
    result.iterations = (iteration - !last_kill);
    result.total_health = 0;
    for (auto &unit : units)
        result.total_health += std::max(0, unit.health);
    result.score = result.iterations * result.total_health;
    result.deaths[0] = total_units[0] - alive[0];
    result.deaths[1] = total_units[1] - alive[1];
    return result;
}


int elfWinScore(const std::vector<std::string> &battlemap)
{
    int min_attack = 4, max_attack = 200;
#if 0
    for (int attack = min_attack; attack <= max_attack; ++attack)
    {
        GameResult result = calcScore(battlemap, attack);
        if (result.deaths[1] == 0)
            return result.score;
    }
    return -1;
#else
    while (min_attack < max_attack)
    {
        int attack = (min_attack + max_attack) / 2;
        GameResult result = calcScore(battlemap, attack);
        if (result.deaths[1] == 0) max_attack = attack;
        else min_attack = attack + 1;
    }
    return calcScore(battlemap, min_attack).score;
#endif
}

int main(int argc, char * * argv)
{
    std::vector<std::string> battlemap;
    int expected_score[2] = { -1, -1 };
    if (argc == 3)
    {
        if (std::string(argv[1]) != "test")
        {
            std::cerr << "[ERROR] Unexpected parameters.\n";
            return EXIT_FAILURE;
        }
        int test_idx = 0;
        try
        {
            test_idx = std::stoi(argv[2]);
        }
        catch (...)
        {
            std::cerr << "[ERROR] Unexpected parameters.\n";
            return EXIT_FAILURE;
        }
        if (test_idx == 0)
        {
            battlemap.push_back("#######");
            battlemap.push_back("#.G...#");
            battlemap.push_back("#...EG#");
            battlemap.push_back("#.#.#G#");
            battlemap.push_back("#..G#E#");
            battlemap.push_back("#.....#");
            battlemap.push_back("#######");
            expected_score[0] = 27730;
            expected_score[1] = 4988;
        }
        else if (test_idx == 1)
        {
            battlemap.push_back("#######");
            battlemap.push_back("#G..#E#");
            battlemap.push_back("#E#E.E#");
            battlemap.push_back("#G.##.#");
            battlemap.push_back("#...#E#");
            battlemap.push_back("#...E.#");
            battlemap.push_back("#######");
            expected_score[0] = 36334;
            expected_score[1] = -1;
        }
        else if (test_idx == 2)
        {
            battlemap.push_back("#######");
            battlemap.push_back("#E..EG#");
            battlemap.push_back("#.#G.E#");
            battlemap.push_back("#E.##E#");
            battlemap.push_back("#G..#.#");
            battlemap.push_back("#..E#.#");
            battlemap.push_back("#######");
            expected_score[0] = 39514;
            expected_score[1] = 31284;
        }
        else if (test_idx == 3)
        {
            battlemap.push_back("#######");
            battlemap.push_back("#E.G#.#");
            battlemap.push_back("#.#G..#");
            battlemap.push_back("#G.#.G#");
            battlemap.push_back("#G..#.#");
            battlemap.push_back("#...E.#");
            battlemap.push_back("#######");
            expected_score[0] = 27755;
            expected_score[1] = 3478;
        }
        else if (test_idx == 4)
        {
            battlemap.push_back("#######");
            battlemap.push_back("#.E...#");
            battlemap.push_back("#.#..G#");
            battlemap.push_back("#.###.#");
            battlemap.push_back("#E#G#G#");
            battlemap.push_back("#...#G#");
            battlemap.push_back("#######");
            expected_score[0] = 28944;
            expected_score[1] = 6474;
        }
        else if (test_idx == 5)
        {
            battlemap.push_back("#########");
            battlemap.push_back("#G......#");
            battlemap.push_back("#.E.#...#");
            battlemap.push_back("#..##..G#");
            battlemap.push_back("#...##..#");
            battlemap.push_back("#...#...#");
            battlemap.push_back("#.G...G.#");
            battlemap.push_back("#.....G.#");
            battlemap.push_back("#########");
            expected_score[0] = 18740;
            expected_score[1] = 1140;
        }
        else
        {
            std::cerr << "[ERROR] Unexpected parameters: test does not exist.\n";
            return EXIT_FAILURE;
        }
    }
    else if (argc > 2)
    {
        std::cerr << "[ERROR] Unexpected number of parameters.\n";
        return EXIT_FAILURE;
    }
    else
    {
        std::string filename = "data.txt";
        if (argc == 2) filename = argv[1];
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
            return EXIT_FAILURE;
        }
        for (std::string line; std::getline(file, line); )
        {
            if (line.size() < 3) continue;
            if ((battlemap.size() > 0) && (line.size() != battlemap.back().size()))
                continue;
            battlemap.push_back(line);
        }
        file.close();
    }
    auto score = calcScore(battlemap);
    if (expected_score[0] != -1)
    {
        std::cout << "Problem A: " << score.score;
        if (expected_score[0] == score.score) std::cout << " [CORRECT]\n";
        else std::cout << " [WRONG] " << expected_score[0] << " was expected.\n";
    }
    else std::cout << "Problem A: " << score.score << '\n';
    int modified_score = elfWinScore(battlemap);
    if (expected_score[1] != -1)
    {
        std::cout << "Problem B: " << modified_score;
        if (expected_score[1] == modified_score) std::cout << " [CORRECT]\n";
        else std::cout << " [WRONG] " << expected_score[1] << " was expected.\n";
    }
    else std::cout << "Problem B: " << modified_score << '\n';
    return EXIT_SUCCESS;
}

