# [Day 15: Beverage Bandits](https://adventofcode.com/2018/day/15)

#### Problem A

You are given a map where you have walls `#`, open space `.` and two types of units (`G` and `E`) that are fighting each other.

The units always search for the closest enemy using the Manhattan distance and move toward it. Unit only move to the closest *reachable* enemy unit and if non is reachable, the do not move.

The unit order is decided by the position of the unit in the map. Units on the top have a higher priority than units in the bottom and units on the left have also a greater priority than units on the right. This is similar to reading the map left-to-right. This order is also used to solve ties when taking other decisions. For example, when a unit can take multiple paths to reach its enemy, the next tile will be selected following this priority (top-left tiles have priority over bottom-right tiles).

Once they reach its 4-neighbors neighborhood, they start fighting. Once the fight has started, the do not stop until one unit is dead. The units have 200 hit points and deal a damage of 3 hit points per turn. When attacking, the unit always attack the unit with the lowest health. If there is a tie when selecting the attack target, then use the reading order priority.

Once the battle is over and one team has been wiped out, the map score is obtained by multiplying the **number of turns** it took to finish by the **total sum of the units' health** that are still alive. What is the score of the given map?

#### Problem B

Modifying the attack power of `E` units, find the minimum attack power that allows `E` units to win the conflict without losing a single unit.

Now, what is the score of the given map?

