#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <bitset>

struct Instruction
{
    int opcode;
    int regval_a;
    int regval_b;
    int reg_c;
};

struct Sample
{
    int in[4] = {};
    Instruction code;
    int out[4] = {};
};

void execute(Instruction code, const int * in, int * out)
{
    switch (code.opcode)
    {
    case 0: // addr: reg_c = reg_a + reg_b
        out[code.reg_c] = in[code.regval_a] + in[code.regval_b];
        break;
    case 1: // addi: reg_c = reg_a + val_b
        out[code.reg_c] = in[code.regval_a] + code.regval_b;
        break;
    case 2: // mulr: reg_c = reg_a * reg_b
        out[code.reg_c] = in[code.regval_a] * in[code.regval_b];
        break;
    case 3: // muli: reg_c = reg_a * val_b
        out[code.reg_c] = in[code.regval_a] * code.regval_b;
        break;
    case 4: // banr: reg_c = reg_a & reg_b
        out[code.reg_c] = in[code.regval_a] & in[code.regval_b];
        break;
    case 5: // bani: reg_c = reg_a & val_b
        out[code.reg_c] = in[code.regval_a] & code.regval_b;
        break;
    case 6: // borr: reg_c = reg_a | reg_b
        out[code.reg_c] = in[code.regval_a] | in[code.regval_b];
        break;
    case 7: // bori: reg_c = reg_a | val_b
        out[code.reg_c] = in[code.regval_a] | code.regval_b;
        break;
    case 8: // setr: reg_c = reg_a
        out[code.reg_c] = in[code.regval_a];
        break;
    case 9: // seti: reg_c = val_a
        out[code.reg_c] = code.regval_a;
        break;
    case 10: // gtir: reg_c = (val_a > reg_b)
        out[code.reg_c] = (code.regval_a > in[code.regval_b]);
        break;
    case 11: // gtri: reg_c = (reg_a > val_b)
        out[code.reg_c] = (in[code.regval_a] > code.regval_b);
        break;
    case 12: // gtrr: reg_c = (reg_a > reg_b)
        out[code.reg_c] = (in[code.regval_a] > in[code.regval_b]);
        break;
    case 13: // eqir: reg_c = (val_a == reg_b)
        out[code.reg_c] = (code.regval_a == in[code.regval_b]);
        break;
    case 14: // eqri: reg_c = (reg_a == val_b)
        out[code.reg_c] = (in[code.regval_a] == code.regval_b);
        break;
    case 15: // eqrr: reg_c = (reg_a == reg_b)
        out[code.reg_c] = (in[code.regval_a] == in[code.regval_b]);
        break;
    default:
        std::cerr << "[ERROR] Unknown instruction.\n";
        return;
    }
}

int main(int argc, char * * argv)
{
    std::vector<Sample> samples;
    std::vector<Instruction> program;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    bool loading_example = false;
    for (std::string line; std::getline(file, line); )
    {
        std::string val0, val1, val2, val3, val4;
        std::istringstream iss(line);
        iss >> val0 >> val1 >> val2 >> val3 >> val4;
        if (val0 == "Before:")
        {
            loading_example = true;
            int register0 = std::stoi(val1.substr(1, val1.size() - 2));
            int register1 = std::stoi(val2.substr(0, val2.size() - 1));
            int register2 = std::stoi(val3.substr(0, val3.size() - 1));
            int register3 = std::stoi(val4.substr(0, val4.size() - 1));
            samples.push_back({});
            samples.back().in[0] = register0;
            samples.back().in[1] = register1;
            samples.back().in[2] = register2;
            samples.back().in[3] = register3;
        }
        else if (val0 == "After:")
        {
            loading_example = false;
            int register0 = std::stoi(val1.substr(1, val1.size() - 2));
            int register1 = std::stoi(val2.substr(0, val2.size() - 1));
            int register2 = std::stoi(val3.substr(0, val3.size() - 1));
            int register3 = std::stoi(val4.substr(0, val4.size() - 1));
            samples.back().out[0] = register0;
            samples.back().out[1] = register1;
            samples.back().out[2] = register2;
            samples.back().out[3] = register3;
        }
        else
        {
            if (line.size() < 7) continue;
            Instruction inst;
            inst.opcode = std::stoi(val0);
            inst.regval_a = std::stoi(val1);
            inst.regval_b = std::stoi(val2);
            inst.reg_c = std::stoi(val3);
            
            if (loading_example)
                samples.back().code = inst;
            else program.push_back(inst);
        }
    }
    file.close();
    
    int problem_a = 0;
    std::bitset<16> valid_mask[16];
    std::bitset<16> init;
    for (auto sample : samples)
    {
        int out[4], valid = 0;
        std::bitset<16> putative;
        int original = sample.code.opcode;
        for (int op = 0; op < 16; ++op)
        {
            sample.code.opcode = op;
            execute(sample.code, sample.in, out);
            if (out[sample.code.reg_c] == sample.out[sample.code.reg_c])
            {
                ++valid;
                putative[op] = true;
            }
        }
        if (init[original]) valid_mask[original] &= putative;
        else
        {
            init[original] = true;
            valid_mask[original] = putative;
        }
        problem_a += (valid >= 3);
    }
    std::cout << "Problem A: " << problem_a << '\n';
    for (bool change = true; change; )
    {
        change = false;
        for (int i = 0; i < 16; ++i)
        {
            if (valid_mask[i].count() != 1) continue;
            int bit = -1;
            for (int j = 0; j < 16; ++j)
                if (valid_mask[i][j]) bit = j;
            for (int j = 0; j < 16; ++j)
            {
                if (j == i) continue;
                change = change || valid_mask[j][bit];
                valid_mask[j][bit] = false;
            }
        }
    }
    int translation[16] = {};
    for (int i = 0; i < 16; ++i)
    {
        int bit = -1;
        for (int j = 0; j < 16; ++j)
            if (valid_mask[i][j]) bit = j;
        translation[i] = bit;
        // Check that there are no repeated translations.
        for (int j = 0; j < 16; ++j)
            if ((j != i) && valid_mask[j][bit])
                std::cout << "[ERROR] To opcodes have the same translation.\n";
    }
    for (auto &inst : program)
        inst.opcode = translation[inst.opcode];
    int regs[4] = {};
    for (auto &inst : program)
        execute(inst, regs, regs);
    std::cout << "Problem B: " << regs[0] << '\n';
    return EXIT_SUCCESS;
}

