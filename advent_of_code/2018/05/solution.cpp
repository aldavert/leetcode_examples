#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stack>

int lengthProcess(const std::string &data)
{
    auto canEliminate = [](char l, char r) -> bool
    {
        return (l - 'a' + 'A' == r) || (r - 'a' + 'A' == l);
    };
    std::stack<char> s;
    for (char letter : data)
    {
        if (!s.empty() && canEliminate(s.top(), letter))
            s.pop();
        else s.push(letter);
    }
    return static_cast<int>(s.size());
}

int lengthEliminate(const std::string &data)
{
    int result = static_cast<int>(data.size());
    for (char filter = 'A'; filter <= 'Z'; ++filter)
    {
        std::string current;
        for (char letter : data)
            if ((letter != filter) && (letter != filter - 'A' + 'a'))
                current += letter;
        result = std::min(result, lengthProcess(current));
    }
    return result;
}

int main(int argc, char * * argv)
{
    std::string filename = "data.txt";
    std::string data;
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        data += line;
    file.close();
    std::cout << "Problem A: " << lengthProcess(data) << '\n';
    std::cout << "Problem B: " << lengthEliminate(data) << '\n';
    return EXIT_SUCCESS;
}

