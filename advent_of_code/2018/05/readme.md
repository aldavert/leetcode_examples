# [Day 5: Alchemical Reduction](https://adventofcode.com/2018/day/5)

#### Problem A

You are given a string where you can perform the following operation:
- When two adjacent elements are the same letter but different capitalization, they eliminate each other.

Applying this operation until the string remains unchanged, what is the length of the resulting string?

#### Problem B

If you remove all instances of a letter, what is the shortest length of the string after applying the operand?

