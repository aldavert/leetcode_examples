# [Day 4: Repose Record](https://adventofcode.com/2018/day/4)

#### Problem A

You are given a log with the timestamps indicating the exact time in which a guard starts working, falls asleep or wakes up. The log is not ordered.

You have to find the guard that sleeps the most time and then find the minute in which he sleeps the most. Finally, return the product between the minute that the selected guard sleeps the most and its ID.

#### Problem B

Now, search for the guard that has slept the most amount of times at a given minute. Return the product of its ID by the minute.

