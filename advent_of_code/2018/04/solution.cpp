#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>

struct Log
{
    std::string date;
    int id = -1;
    int minute = 0;
    bool sleeps = false;
    bool starts = false;
    bool operator<(const Log &other) const { return date < other.date; }
    bool operator==(const Log &other) const { return date == other.date; }
    void initDate(const std::string &d)
    {
        date = d;
        std::string aux = d.substr(d.find(' ') + 1);
        int hour = std::stoi(aux.substr(0, 2));
        minute = std::stoi(aux.substr(3));
        if (hour == 23) minute = 60 - minute;
        else if (hour != 0)
            std::cerr << "[WARNING] Unexpected time\n";
    }
};

std::ostream& operator<<(std::ostream &out, const Log &log)
{
    out << '[' << log.date << "] ";
    if (log.starts)
        out << "Guard #" << log.id << " begins shift";
    else if (log.sleeps)
        out << "falls asleep";
    else out << "wakes up";
    return out;
}

long bestSleep(const std::vector<Log> &timestamps)
{
    std::unordered_map<int, int> sleep_time;
    for (int starts = -1000; const auto &ts : timestamps)
    {
        if (ts.starts) continue;
        if (ts.sleeps)
            starts = ts.minute;
        else if (starts == -1000)
            std::cerr << "[ERROR] Unexpected log time.\n";
        else sleep_time[ts.id] += ts.minute - starts;
    }
    int selected_id = -1, selected_time = -1;
    for (auto [id, time] : sleep_time)
    {
        if (time > selected_time)
        {
            selected_time = time;
            selected_id = id;
        }
    }
    int histogram[121] = {};
    for (int starts = -1000; const auto &ts : timestamps)
    {
        if (ts.starts || (ts.id != selected_id)) continue;
        if (ts.sleeps) { starts = ts.minute; continue; }
        for (int i = starts + 60, n = ts.minute + 60; i < n; ++i)
            ++histogram[i];
    }
    int best_time = 0, best_frequency = 0;
    for (int i = 0; i < 121; ++i)
    {
        if (histogram[i] > best_frequency)
        {
            best_time = i;
            best_frequency = histogram[i];
        }
    }
    return static_cast<long>(best_time - 60) * static_cast<long>(selected_id);
}

long guardMinute(const std::vector<Log> &timestamps)
{
    std::unordered_map<int, int> id2idx;
    std::vector<int> idx2id;
    for (const auto &ts : timestamps)
        if (ts.starts && (id2idx.find(ts.id) == id2idx.end()))
                id2idx[ts.id] = static_cast<int>(id2idx.size()),
                idx2id.push_back(ts.id);
    std::vector<std::array<int, 121> > histograms(id2idx.size());
    for (int starts = -1000; const auto &ts : timestamps)
    {
        if (ts.starts) continue;
        if (ts.sleeps) { starts = ts.minute; continue; }
        auto &current = histograms[id2idx[ts.id]];
        for (int i = starts + 60, n = ts.minute + 60; i < n; ++i)
            ++current[i];
    }
    int selected_id = -1, selected_time = -1, selected_frequency = 0;
    for (int t = 0; t < 121; ++t)
    {
        for (size_t k = 0; k < histograms.size(); ++k)
        {
            if (histograms[k][t] > selected_frequency)
            {
                selected_frequency = histograms[k][t];
                selected_time = t - 60;
                selected_id = idx2id[k];
            }
        }
    }
    return static_cast<long>(selected_id) * static_cast<long>(selected_time);
}

int main(int argc, char * * argv)
{
    std::vector<Log> timestamps;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.size() < 10) continue;
        auto lsep = line.find('[');
        auto rsep = line.find(']', lsep + 1);
        timestamps.push_back({});
        timestamps.back().initDate(line.substr(lsep + 1, rsep - lsep - 1));
        timestamps.back().sleeps = false;
        timestamps.back().starts = true;
        if (line.substr(rsep + 2, 7) == "Guard #")
        {
            lsep = line.find('#', rsep + 1);
            rsep = line.find(' ', lsep + 1);
            timestamps.back().id = std::stoi(line.substr(lsep + 1, rsep - lsep - 1));
        }
        else if (line.substr(rsep + 2) == "wakes up")
            timestamps.back().starts = false;
        else if (line.substr(rsep + 2) == "falls asleep")
            timestamps.back().sleeps = true,
            timestamps.back().starts = false;
        else std::cerr << "[WARNING] Unexpected line: " << line << '\n';
    }
    file.close();
    std::sort(timestamps.begin(), timestamps.end());
    for (int previous = -1; auto &ts : timestamps)
    {
        if (ts.starts) previous = ts.id;
        else ts.id = previous;
    }
    std::cout << "Problem A: " << bestSleep(timestamps) << '\n';
    std::cout << "Problem B: " << guardMinute(timestamps) << '\n';
    
    return EXIT_SUCCESS;
}

