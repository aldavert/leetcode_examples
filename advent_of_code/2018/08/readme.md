# [Day 8: Memory Maneuver](https://adventofcode.com/2018/day/8)

#### Problem A

You are given the information of a tree as a list of integers. The flatten tree contains the information of the nodes, where each node consists of:
- A **header** with two numbers that indicate *the quantity of child nodes* and *the quantity of metadata entries* (i.e. number of integer values assigned to the node).
- Zero or more child nodes (as specified in the header).
- One or more metadata entries (as specified in the header).

What is the sum of all the tree's metadata entries?

#### Problem B

The identifier of a node is computed as:
- The sum of its metadata, when it doesn't have children.
- The sum of the identifiers of its children in the order specified by its metadata. For example, a node with two childs with IDs, `33` and `0`, and metadata `1, 1, 2` has the ID `33+33+0` as metadata value `1` corresponds to the first child, `2` to the seconds, ... When the metadata value points to a non existing child, then we add a 0.

What is the ID of the root node.



