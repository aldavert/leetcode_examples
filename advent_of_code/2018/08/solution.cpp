#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <memory>
#include <numeric>

struct Node
{
    std::vector<std::unique_ptr<Node> > children;
    std::vector<int> metadata;
};

std::unique_ptr<Node> build(const std::vector<int> data)
{
    auto create = [&](auto &&self, size_t i) -> std::tuple<std::unique_ptr<Node>, int>
    {
        int number_of_childs = data[i++];
        int metadata_size = data[i++];
        std::unique_ptr<Node> node = std::make_unique<Node>();
        for (int k = 0; k < number_of_childs; ++k)
        {
            auto [child, next] = self(self, i);
            i = next;
            node->children.push_back(std::move(child));
        }
        for (int k = 0; k < metadata_size; ++k)
            node->metadata.push_back(data[i++]);
        return std::make_tuple(std::move(node), i);
    };
    auto [root, idx] = create(create, 0);
    return root;
}

int sumMetadata(const std::unique_ptr<Node> &node)
{
    int result = std::accumulate(node->metadata.begin(), node->metadata.end(), 0);
    for (const auto &child : node->children)
        result += sumMetadata(child);
    return result;
}

int nodeID(const std::unique_ptr<Node> &node)
{
    const int n = static_cast<int>(node->children.size());
    std::vector<int> ids = node->metadata;
    if (n > 0)
    {
        for (int &value : ids)
            value = (value <= n)?nodeID(node->children[value - 1]):0;
    }
    return std::accumulate(ids.begin(), ids.end(), 0);
}

int main(int argc, char * * argv)
{
    std::vector<int> data;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        std::istringstream iss(line);
        int value;
        while (iss >> value)
            data.push_back(value);
    }
    file.close();
    std::unique_ptr<Node> tree = build(data);
    std::cout << "Problem A: " << sumMetadata(tree) << '\n';
    std::cout << "Problem B: " << nodeID(tree) << '\n';
    return EXIT_SUCCESS;
}

