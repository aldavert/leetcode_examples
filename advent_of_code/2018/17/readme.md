# [Day 17: Reservoir Research](https://adventofcode.com/2018/day/17)

#### Problem A

You are given a list of the coordinates of obstacles. You have a *source of water* at coordinates `x = 500, y = 0` which generate an infinite stream of water. The water falls downward and it will be stopped by the obstacles. Depending on the obstacles configurations, the stream can create reservoirs of water. Once these reservoirs overflow, can create one or two streams depending on the configuration of the obstacles.

The simulation doesn't represent water pressure, so water is only accumulated if there is a stream that directly pours water into the tile. For example, in the following configuration...
```
......+.......            ......+.......
............#.            ......w.....#.
.#..#.......#.            .#..#wwww...#.
.#..#..#......            .#..#ww#w.....
.#..#..#......            .#..#ww#w.....
.#.....#......            .#wwwww#w.....
.#............            .#wwwww#w.....
.#######...... ---------> .#######w.....
..............            ........w.....
..............            ...wwwwwwwww..
....#.....#...            ...w#wwwww#w..
....#.....#...            ...w#wwwww#w..
....#.....#...            ...w#wwwww#w..
....#######...            ...w#######w..
..............            ...|.......|..
```
... tiles `.` are empty space, `#` is an obstacle, `w` is a tile with water and `|` are tiles with water that are below the lowest obstacle.

Count the total amount of tiles at the same height or above than the obstacles that contain water (in the previous example there are 57).

#### Problem B

If the *source of water* dries, what is the amount of water that remains in the map?

