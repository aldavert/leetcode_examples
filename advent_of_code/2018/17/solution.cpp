#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stack>

struct Pixel
{
    unsigned char r = 0;
    unsigned char g = 0;
    unsigned char b = 0;
};

void save(const char * filename, const std::vector<std::vector<Pixel> > &image)
{
    const int height = static_cast<int>(image.size());
    if (height == 0) return;
    const int width  = static_cast<int>(image[0].size());
    if (width == 0) return;
    std::ofstream file(filename, std::ios::binary);
    
    unsigned char fileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
    unsigned char infoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
    unsigned char pad[3] = {0, 0, 0};
    const int filesize = 54 + 3 * height * width;
    fileheader[ 2] = static_cast<unsigned char>(filesize      );
    fileheader[ 3] = static_cast<unsigned char>(filesize >>  8);
    fileheader[ 4] = static_cast<unsigned char>(filesize >> 16);
    fileheader[ 5] = static_cast<unsigned char>(filesize >> 24);
    infoheader[ 4] = static_cast<unsigned char>(   width      );
    infoheader[ 5] = static_cast<unsigned char>(   width >>  8);
    infoheader[ 6] = static_cast<unsigned char>(   width >> 16);
    infoheader[ 7] = static_cast<unsigned char>(   width >> 24);
    infoheader[ 8] = static_cast<unsigned char>(  height      );
    infoheader[ 9] = static_cast<unsigned char>(  height >>  8);
    infoheader[10] = static_cast<unsigned char>(  height >> 16);
    infoheader[11] = static_cast<unsigned char>(  height >> 24);
    file.write(reinterpret_cast<char *>(fileheader), 14);
    file.write(reinterpret_cast<char *>(infoheader), 40);
    for (int row = height - 1; row >= 0; --row)
    {
        std::vector<unsigned char> interlaced_row(width * 3);
        for (int col = 0; col < width; ++col)
        {
            interlaced_row[3 * col + 0] = image[row][col].b;
            interlaced_row[3 * col + 1] = image[row][col].g;
            interlaced_row[3 * col + 2] = image[row][col].r;
        }
        file.write(reinterpret_cast<const char *>(interlaced_row.data()), 3 * width);
        file.write(reinterpret_cast<char *>(pad), (4 - (3 * width) % 4) % 4);
    }
    file.close();
}

std::vector<std::vector<Pixel> > colorize(const std::vector<std::string> &world)
{
    if ((world.size() == 0) || (world[0].size() == 0)) return {};
    const size_t n_rows = world.size(),
                 n_cols = world[0].size();
    std::vector<std::vector<Pixel> > image(n_rows, std::vector<Pixel>(n_cols));
    for (size_t row = 0; row < n_rows; ++row)
    {
        for (size_t col = 0; col < n_cols; ++col)
        {
            if      (world[row][col] == ' ') image[row][col] = { 255, 255, 255 };
            else if (world[row][col] == '#') image[row][col] = { 0, 0, 0 };
            else if (world[row][col] == 'w') image[row][col] = { 128, 128, 255 };
            else if (world[row][col] == 'W') image[row][col] = { 0, 0, 255 };
            else image[row][col] = { 255, 0, 0 };
        }
    }
    return image;
}

struct Rectangle
{
    int x0 = -1;
    int y0 = -1;
    int x1 = -1;
    int y1 = -1;
};


std::vector<std::string> waterFlow(const std::vector<Rectangle> &obstacles)
{
    int min_x = obstacles.front().x0,
        max_x = obstacles.front().x1, max_y = obstacles.front().y1;
    for (const auto &obst : obstacles)
    {
        min_x = std::min({min_x, obst.x0, obst.x1});
        max_x = std::max({max_x, obst.x0, obst.x1});
        max_y = std::max({max_y, obst.y0, obst.y1});
    }
    std::vector<std::string> world(max_y + 1, std::string(max_x - min_x + 3, ' '));
    for (const auto &obst : obstacles)
    {
        for (int y = obst.y0; y <= obst.y1; ++y)
            for (int x = obst.x0; x <= obst.x1; ++x)
                world[y][x - min_x + 1] = '#';
    }
    auto flow = [&](auto &&self, int x, int y, int dir) -> bool
    {
        if (y > max_y) return true;
        if (world[y][x] != ' ')
            return (world[y][x] != '#') && (world[y][x] != 'W');
        world[y][x] = 'w';
        bool leaky = self(self, x, y + 1, 0);
        if (leaky) return true;
        leaky  = (dir <= 0) && self(self, x - 1, y, -1);
        leaky |= (dir >= 0) && self(self, x + 1, y,  1);
        if (leaky)	return true;
        if (dir == 0)
        {
            for (int o = x + 1; world[y][o] == 'w'; ++o)
                world[y][o] = 'W';
            for (int o = x; world[y][o] == 'w'; --o)
                world[y][o] = 'W';
        }
        return false;
    };
    flow(flow, 500 - min_x + 1, 0, 0);
    return world;
}

int main(int argc, char * * argv)
{
    std::vector<Rectangle> obstacles;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        auto separator = line.find(", ");
        if (separator == std::string::npos) continue;
        std::string tokens[2] = { line.substr(0, separator),
                                  line.substr(separator + 2) };
        Rectangle rect;
        for (int t = 0; t < 2; ++t)
        {
            auto dots = tokens[t].find("..");
            if (dots != std::string::npos)
            {
                if (tokens[t][0] == 'x')
                    rect.x0 = std::stoi(tokens[t].substr(2, dots - 2)),
                    rect.x1 = std::stoi(tokens[t].substr(dots + 2));
                else
                    rect.y0 = std::stoi(tokens[t].substr(2, dots - 2)),
                    rect.y1 = std::stoi(tokens[t].substr(dots + 2));
            }
            else
            {
                if (tokens[t][0] == 'x')
                    rect.x0 = rect.x1 = std::stoi(tokens[t].substr(2));
                else rect.y0 = rect.y1 = std::stoi(tokens[t].substr(2));
            }
        }
        if ((rect.x0 == -1) || (rect.y0 == -1) || (rect.x1 == -1) || (rect.y1 == -1))
        {
            std::cerr << "[WARNING] Malformatted obstacle information: " << line << '\n';
            continue;
        }
        obstacles.push_back(rect);
    }
    file.close();
    auto filled_map = waterFlow(obstacles);
    //save("result.bmp", colorize(filled_map));
    int water_still = 0, water_flowing = 0;
    for (size_t r = 0; r < filled_map.size() - 1; ++r)
        for (char cell : filled_map[r])
            water_still += (cell == 'W'),
            water_flowing += (cell == 'w');
    std::cout << "Problem A: " << water_flowing + water_still << '\n';
    std::cout << "Problem B: " << water_still << '\n';
    return EXIT_SUCCESS;
}

