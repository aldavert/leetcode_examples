#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <queue>
#include <fmt/core.h>

constexpr int margin = 50;

struct Point
{
    int x = 0;
    int y = 0;
    int distance(const Point &other) const
    {
        return std::abs(x - other.x) + std::abs(y - other.y);
    }
};

int maxArea(const std::vector<Point> &points)
{
    const int m = static_cast<int>(points.size());
    int max_x = 0, max_y = 0;
    for (auto [x, y] : points)
    {
        max_x = std::max(max_x, x);
        max_y = std::max(max_y, y);
    }
    int n = std::max(max_x, max_y) + margin;
    std::vector<std::vector<int> > cells(n, std::vector<int>(n));
    for (int y = 0; y < n; ++y)
    {
        for (int x = 0; x < n; ++x)
        {
            Point current = {x, y};
            int min_distance[2] = { 3 * n, 3 * n }, selected = 0;
            for (int i = 0; i < m; ++i)
            {
                if (int d = points[i].distance(current); d < min_distance[0])
                {
                    min_distance[1] = min_distance[0];
                    min_distance[0] = d;
                    selected = i;
                }
                else if (d < min_distance[1]) min_distance[1] = d;
            }
            cells[y][x] = (min_distance[0] != min_distance[1])?selected:-1;
        }
    }
    constexpr int dirs[] = {-1, 0, 1, 0, -1};
    std::vector<int> region_size(points.size());
    for (int y = 0; y < n; ++y)
    {
        for (int x = 0; x < n; ++x)
        {
            if (cells[y][x] == -1) continue;
            std::queue<Point> q;
            q.push({x, y});
            int current_label = cells[y][x], counter = 0;
            bool border = false;
            while (!q.empty())
            {
                auto [cx, cy] = q.front();
                q.pop();
                if ((cx < 0) || (cy < 0) || (cx >= n) || (cy >= n))
                {
                    border = true;
                    continue;
                }
                if (cells[cy][cx] != current_label) continue;
                cells[cy][cx] = -1;
                ++counter;
                for (int o = 0; o < 4; ++o)
                {
                    int nx = cx + dirs[o], ny = cy + dirs[o + 1];
                    q.push({nx, ny});
                }
            }
            if (border)
                region_size[current_label] = 0;
            else region_size[current_label] = counter;
        }
    }
    return *std::max_element(region_size.begin(), region_size.end());
}

int cellsWithinDistance(const std::vector<Point> &points, int min_distance)
{
    const int m = static_cast<int>(points.size());
    int max_x = 0, max_y = 0;
    for (auto [x, y] : points)
    {
        max_x = std::max(max_x, x);
        max_y = std::max(max_y, y);
    }
    int n = std::max(max_x, max_y) + margin;
    int result = 0;
    for (int y = 0; y < n; ++y)
    {
        for (int x = 0; x < n; ++x)
        {
            Point current = {x, y};
            int total_distance = 0;
            for (int i = 0; i < m; ++i)
                total_distance += points[i].distance(current);
            result += (total_distance < min_distance);
        }
    }
    return result;

}

int main(int argc, char * * argv)
{
    std::vector<Point> points;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        auto sep = line.find(", ");
        if (sep == std::string::npos)
        {
            std::cerr << "[ERROR] Unexpected line format: " << line << '\n';
            continue;
        }
        points.push_back({std::stoi(line.substr(0, sep)) + margin / 2,
                          std::stoi(line.substr(sep + 2)) + margin / 2});
    }
    file.close();
    std::cout << "Problem A: " << maxArea(points) << '\n';
    int min_dist = (points.size() < 10)?32:10'000;
    std::cout << "Problem B: " << cellsWithinDistance(points, min_dist) << '\n';
    return EXIT_SUCCESS;
}

