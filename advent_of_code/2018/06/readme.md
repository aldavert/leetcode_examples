# [Day 6: Chronal Coordinates](https://adventofcode.com/2018/day/6)

#### Problem A

You are given a set of coordinates corresponding points situated in a infinitely big 2D grid. You have to compute all the bounded Voronoi cells (i.e. the cells that have all boundaries defined) using the **Manhattan distance** and return the size of the largest region.

**Note:** The area is computed as the amount of cells that are assigned to each point. Cells that are at the same distance between several points are not assigned to any point (they are ignored).

#### Problem B

Find a region where the sum of the distances between the cell coordinates and the given points is lower than the given threshold. What is the size of the region?

