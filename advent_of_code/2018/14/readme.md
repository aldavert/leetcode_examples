# [Day 14: Chocolate Charts](https://adventofcode.com/2018/day/14)

#### Problem A

You start with the list `{3, 7}` and you have two pointers, one pointing to each element of the list.

At each iteration, you add the two values currently pointed by the pointers and add the digits into the queue from most significant to less significant. The you move the pointers.

The pointers are moved by moving forward the current pointed value plus one. If you reach the end of the list, you restart from the beginning.

After the list has grown into `825401` elements, what are the 10 digits immediately after all these digits?

#### Problem B

Now, search the first position where the digit sequence `825401` appears.

