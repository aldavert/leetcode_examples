#include <iostream>
#include <vector>
#include <string>

int numberOfRecipes(std::string expected_chain)
{
    std::string digits = "37";
    int pos1 = 0, pos2 = 1;
    while (true)
    {
        int digit1 = digits[pos1] - '0',
            digit2 = digits[pos2] - '0';
        digits += std::to_string(digit1 + digit2);
        const int m = static_cast<int>(digits.size());
        pos1 = (pos1 + digit1 + 1) % m;
        pos2 = (pos2 + digit2 + 1) % m;
        if (digits.size() >= expected_chain.size() + 3)
        {
            for (int k = 0; k < 3; ++k)
            {
                std::string tail =
                    digits.substr(digits.size() - expected_chain.size() - k,
                                  expected_chain.size());
                if (tail == expected_chain)
                    return static_cast<int>(digits.size() - expected_chain.size() - k);
            }
        }
    }
    return -1;
}

std::string digitValues(int number_of_values, int size)
{
    std::vector<int> digits;
    digits.reserve(number_of_values + size + 2);
    digits.push_back(3);
    digits.push_back(7);
    int positions[2] = {0, 1};
    for (int i = 2, n = number_of_values + size - 1; i < n; ++i)
    {
        int sum = digits[positions[0]] + digits[positions[1]];
        if (sum >= 10)
        {
            digits.push_back(1);
            sum -= 10;
        }
        digits.push_back(sum);
        for (int j = 0, m = static_cast<int>(digits.size()); j < 2; ++j)
            positions[j] = (positions[j] + digits[positions[j]] + 1) % m;
    }
    std::string output;
    for (int i = number_of_values, j = 0; j < size; ++i, ++j)
        output += static_cast<char>('0' + digits[i]);
    return output;
}

int main(int argc, char * * argv)
{
    int number_of_values = 825'401;
    std::string expected_output;
    if (argc == 2) number_of_values = std::stoi(argv[1]);
    else if (argc == 3)
    {
        if (std::string(argv[1]) == "test")
        {
            switch (std::stoi(argv[2]))
            {
            case 0:
                number_of_values = 9;
                expected_output = "5158916779";
                break;
            case 1:
                number_of_values = 5;
                expected_output = "0124515891";
                break;
            case 2:
                number_of_values = 18;
                expected_output = "9251071085";
                break;
            case 3:
                number_of_values = 2018;
                expected_output = "5941429882";
                break;
            default:
                std::cerr << "[ERROR] Unexpected parameter.\n";
                return EXIT_FAILURE;
            };
        }
        else
        {
            std::cerr << "[ERROR] Unexpected parameter.\n";
            return EXIT_FAILURE;
        }
    }
    else if (argc != 1)
    {
        std::cerr << "[ERROR] Unexpected parameter.\n";
        return EXIT_FAILURE;
    }
    std::string output = digitValues(number_of_values, 10);
    std::cout << "Problem A: " << output;
    if (expected_output.size() > 0)
    {
        if (expected_output == output)
            std::cout << " CORRECT";
        else std::cout << " WRONG (instead of " << expected_output << ")";
    }
    std::cout << '\n';
    if (expected_output.size() > 0)
    {
        int n_recipes = numberOfRecipes(expected_output.substr(0, 5));
        std::cout << "Problem B: " << n_recipes;
        if (number_of_values == n_recipes)
            std::cout << " CORRECT";
        else std::cout << " WRONG (instead of " << number_of_values << ")";
        std::cout << '\n';
    }
    else std::cout << "Problem B: "
                   << numberOfRecipes(std::to_string(number_of_values)) << '\n';
    return EXIT_SUCCESS;
}

