#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <unordered_set>

int main([[maybe_unused]] int argc, [[maybe_unused]] char * * argv)
{
	std::unordered_set<long> numbers;
    long last = -1, a = 0, c = 123;
    
    while ((c = ((c & 456) == 72)) == 0) {}
	c = 0;
    do
    {
        long b = c | 0x1'00'00;
        c = 9'010'242;
        while (true)
        {
            c = (((c + (b & 0xFF)) & 0xFF'FF'FF) * 65'899) & 0xFF'FF'FF;
            if ((b < 0x100) != 0) break;
            a = 0;
            for (; ((a + 1) * 0x100) <= b; ++a);
            b = a;
        }
        a = (c == 0);
        
        if (last == -1) std::cout << "Problem A: " << c << '\n';
        if (numbers.find(c) != numbers.end())
        {
            std::cout << "Problem B: " << last << '\n';
            return EXIT_SUCCESS;
        }
        last = c;
        numbers.insert(last);
    } while (a == 0);
    
    return EXIT_SUCCESS;
}

