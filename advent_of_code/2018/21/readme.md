# [Day 21: Chronal Conversion](https://adventofcode.com/2018/day/21)

#### Problem A

You are given a program that is to run on the same machine as in problem [problem 19](../19/readme.md). The given program has an initial loop that keeps running infinitely unless *register 0* has the right value.

What is the lowest value of *register 0* that allows the program to exit the loop?

#### Problem B

What is the lowest value of *register 0* that makes the program to finish executing most program's instructions?

