#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>

struct Antenna
{
    long x = 0;
    long y = 0;
    long z = 0;
    long strength = 0;
    long dist(const Antenna &other) const
    {
        return std::abs(x - other.x) + std::abs(y - other.y) + std::abs(z - other.z);
    }
};

template <typename T1, typename T2>
bool covers(const T1 &p1, const T2 &p2, long padding)
{
  return std::abs(p1.x - p2.x) + std::abs(p1.y - p2.y) + std::abs(p1.z - p2.z)
      <= p1.strength + padding;
}

struct Point
{
    long x;
    long y;
    long z;
    bool operator<(const Point &other) const
    {
        if (x < other.x) return true;
        if (x > other.x) return false;
        if (y < other.y) return true;
        if (y > other.y) return false;
        return z < other.z;
    }
    bool operator==(const Point &other) const
    {
        return (x == other.x) && (y == other.y) && (z == other.z);
    }
};

int main(int argc, char * * argv)
{
    std::vector<Antenna> antennas;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.size() < 16) continue;
        try
        {
            Antenna current;
            auto l = line.find("pos=<");
            if (l == std::string::npos) continue;
            auto r = line.find(',');
            current.x = std::stol(line.substr(l + 5, r - l - 5));
            l = r + 1;
            r = line.find(',', l);
            current.y = std::stol(line.substr(l, r - l));
            l = r + 1;
            r = line.find('>', l);
            current.z = std::stol(line.substr(l, r - l));
            l = line.find("r=") + 2;
            current.strength = std::stol(line.substr(l));
            antennas.push_back(current);
        }
        catch (...) {}
    }
    file.close();
    
    // -------------------------------------------------------------------------
    size_t selected_index = 0;
    long selected_strength = antennas[0].strength;
    for (size_t i = 1; i < antennas.size(); ++i)
        if (antennas[i].strength > selected_strength)
            selected_strength = antennas[i].strength,
            selected_index = i;
    int problem_a = 0;
    for (size_t i = 0; i < antennas.size(); ++i)
        problem_a += antennas[selected_index].dist(antennas[i])
                  <= antennas[selected_index].strength;
    std::cout << "Problem A: " << problem_a << '\n';
    
    // -------------------------------------------------------------------------
    long x_min = std::numeric_limits<long>::max(),
         y_min = std::numeric_limits<long>::max(),
         z_min = std::numeric_limits<long>::max(),
         x_max = std::numeric_limits<long>::lowest(),
         y_max = std::numeric_limits<long>::lowest(),
         z_max = std::numeric_limits<long>::lowest();
    for (size_t i = 0; i < antennas.size(); ++i)
        x_min = std::min(x_min, antennas[i].x),
        y_min = std::min(y_min, antennas[i].y),
        z_min = std::min(z_min, antennas[i].z),
        x_max = std::max(x_max, antennas[i].x),
        y_max = std::max(y_max, antennas[i].y),
        z_max = std::max(z_max, antennas[i].z);
    long scale = 1 << static_cast<long>(std::log2(x_max - x_min + y_max - y_min
                + z_max - z_min) + 1);
    x_min = (x_min / scale) * scale;
    x_max = (x_max / scale + 1) * scale;
    y_min = (y_min / scale) * scale;
    y_max = (y_max / scale + 1) * scale;
    z_min = (z_min / scale) * scale;
    z_max = (z_max / scale + 1) * scale;
    std::vector<Point> points = {{x_min, y_min, z_min}};
    while (true)
    {
        size_t max_antennas = 0;
        std::vector<Point> new_points;
        for (const auto &p : points)
        {
            size_t num_antennas = 0;
            for (const auto &a : antennas)
                num_antennas += covers(a, p, scale);
            if ((num_antennas != 0) && (num_antennas == max_antennas))
                new_points.push_back(p);
            if (num_antennas > max_antennas)
            {
                max_antennas = num_antennas;
                new_points.clear();
                new_points.push_back(p);
            }
        }
        
        if (scale == 0)
        {
            std::swap(points, new_points);
            break;
        }
        points.clear();
        scale /= 2;
        if (scale == 0) std::swap(points, new_points);
        else
        {
            for (const auto &p : new_points)
                for (long dx = -scale; dx <= scale; dx += scale)
                    for (long dy = -scale; dy <= scale; dy += scale)
                        for (long dz = -scale; dz <= scale; dz += scale)
                            points.push_back({p.x + dx, p.y + dy, p.z + dz});
            std::sort(points.begin(), points.end());
            points.erase(std::unique(points.begin(), points.end()), points.end());
        }
    }
    long problem_b = std::numeric_limits<long>::max();
    for (const auto &p : points)
        problem_b = std::min(problem_b, std::abs(p.x) + std::abs(p.y) + std::abs(p.z));
    std::cout << "Problem B: " << problem_b << "\n";
    // -------------------------------------------------------------------------
    
    return EXIT_SUCCESS;
}

