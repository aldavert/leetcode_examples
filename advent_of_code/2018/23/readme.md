# [Day 23: Experimental Emergency Teleportation](https://adventofcode.com/2018/day/23)

#### Problem A

You are given a list of antennas information. For each antenna, you are given the coordinates and its signal radius. The distance between antennas is computed using the Manhattan distance.

Search the antenna with the largest signal radius and return how many other antennas are in range of its signal.

#### Problem B

Now, you need to find the coordinates of the position that is in range of the largest number of antennas. If there are multiple points that fulfill the condition, select the one that is closer to the origin (coordinates `0,0,0`).

