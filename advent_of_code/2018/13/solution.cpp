#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <set>
#include <fmt/core.h>

struct Cart
{
    Cart(void) = default;
    Cart(int _x, int _y, int _o) : x(_x), y(_y), o(_o), last_turn(0), active(true) {}
    constexpr static int dir[] = {-1, 0, 1, 0, -1}; // Left, down, right, up.
    int x = -1;
    int y = -1;
    int o = 0;
    int last_turn = 0;
    bool active = true;
    bool operator<(const Cart &other) const
    {
        return (y < other.y) || ((y == other.y) && (x < other.x));
    }
    bool operator==(const Cart &other) const
    {
        return (y == other.y) && (x == other.x);
    }
    inline void turnRight(void)
    {
        if      (o == 0) o = 3;
        else if (o == 1) o = 0;
        else if (o == 2) o = 1;
        else if (o == 3) o = 2;
    }
    inline void turnLeft(void)
    {
        if      (o == 0) o = 1;
        else if (o == 1) o = 2;
        else if (o == 2) o = 3;
        else if (o == 3) o = 0;
    }
};

std::vector<std::pair<int, int> > cartCrashes(const std::vector<std::string> &tracks,
                                              std::vector<Cart> carts)
{
    std::vector<std::pair<int, int> > result;
    for (int number_of_carts = static_cast<int>(carts.size()); number_of_carts > 1; )
    {
        std::sort(carts.begin(), carts.end());
        for (size_t i = 0; i < carts.size(); ++i)
        {
            if (!carts[i].active) continue;
            char tile = tracks[carts[i].y][carts[i].x];
            if ((tile == '-') || (tile == '|')) { /* DON'T TURN */ }
            else if (tile == '/')
            {
                if ((carts[i].o & 1) == 0) carts[i].turnLeft();
                else carts[i].turnRight();
            }
            else if (tile == '\\')
            {
                if ((carts[i].o & 1) == 0) carts[i].turnRight();
                else carts[i].turnLeft();
            }
            else if (tile == '+')
            {
                if      (carts[i].last_turn == 0) carts[i].turnLeft();
                else if (carts[i].last_turn == 2) carts[i].turnRight();
                carts[i].last_turn = (carts[i].last_turn + 1) % 3;
            }
            else
            {
                std::cerr << "[ERROR] Cart is out of track.\n";
                return result;
            }
            carts[i].x += Cart::dir[carts[i].o];
            carts[i].y += Cart::dir[carts[i].o + 1];
            bool crash = false;
            for (size_t j = 0; j < carts.size(); ++j)
            {
                if ((i == j) || !carts[j].active) continue;
                if (carts[j] == carts[i])
                {
                    carts[j].active = false;
                    carts[i].active = false;
                    crash = true;
                    number_of_carts -= 2;
                }
            }
            if (crash)
                result.push_back({carts[i].x, carts[i].y});
        }
    }
    for (size_t i = 0; i < carts.size(); ++i)
        if (carts[i].active)
            result.push_back({carts[i].x, carts[i].y});
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<std::string> tracks;
    std::vector<Cart> carts;
    
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        const int y = static_cast<int>(tracks.size());
        for (int x = 0; char &current : line)
        {
            if      (current == '<') { current = '-'; carts.push_back({x, y, 0}); }
            else if (current == '^') { current = '|'; carts.push_back({x, y, 3}); }
            else if (current == '>') { current = '-'; carts.push_back({x, y, 2}); }
            else if (current == 'v') { current = '|'; carts.push_back({x, y, 1}); }
            ++x;
        }
        tracks.push_back(line);
    }
    file.close();
    std::vector<std::pair<int, int> > crashes = cartCrashes(tracks, carts);
    std::cout << "Problem A: " << crashes.front().first << ','
                               << crashes.front().second << '\n';
    std::cout << "Problem B: " << crashes.back().first << ','
                               << crashes.back().second << '\n';
    return EXIT_SUCCESS;
}

