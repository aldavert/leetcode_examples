# [Day 13: Mine Cart Madness](https://adventofcode.com/2018/day/13)

#### Problem A

You are given a *maze* of paths where you have straight lines (`-` and `|`), turn tiles (`\` and `/`) and intersections (`+`). The maze have multiple carts that are facing left (indicated as `<`), up (`^`), right (`>`) or (`v`).

The carts move one tile every iteration, moving forward in straight lines, changing its orientation in turn tiles and following the sequence *turn left*, *keep straight* and *turn right* each time that it reaches an intersection. This means that the first time that a cart reaches an intersection, it turns left. The second intersection, it goes straight. The third it turns right. The forth, turns left again. And it continues doing turns this way indefinitely.

The orders that carts are moved depends on their location in the maze. Carts that are on the upper rows move before than carts that are on the lower rows. When two carts on the same row, the cart that is closer to the left edge moves first.

What are the `X,Y` coordinates of the first location where two carts collide?

#### Problem B

Now, when two carts collide, they eliminate each other. What are the `X,Y` coordinates of the last remaining cart?

