#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_set>
#include <fmt/core.h>

long evolveScore(const std::string &initial,
                 const std::unordered_set<std::string> rules,
                 long number_of_iterations)
{
    const int n = static_cast<int>(initial.size() + 10) * 30,
              m = 2 * n + 1,
              last_iter = static_cast<int>(std::min(10'000'000l, number_of_iterations));
    std::string state[2] = { std::string(m, '.'), std::string(m, '.') };
    int shift[2] = { 0, 0 }, iter = 0, total_shift = 0;
    bool second = false;
    
    for (size_t i = 0; i < initial.size(); ++i)
        state[0][n + i] = initial[i];
    for (iter = 0; iter < last_iter; ++iter, second = !second)
    {
        for (int i = 0; i < m; ++i)
            state[!second][i] = '.';
        for (int i = 0; i < m; ++i)
        {
            if (state[second][i] == '#')
            {
                shift[!second] = i - n;
                break;
            }
        }
        for (int i = 2; i < m - 2; ++i)
        {
            std::string current = state[second].substr(i - 2, 5);
            if (rules.find(current) != rules.end())
                state[!second][i - shift[!second]] = '#';
        }
        total_shift += shift[!second];
        if (state[second] == state[!second])
        {
            ++iter; // The iteration has been already done.
            break;
        }
    }
    long offset = (number_of_iterations - iter) * shift[second];
    long score = 0;
    for (int i = 0; i < m; ++i)
        if (state[second][i] == '#')
            score += offset + total_shift + i - n;
    return score;
}

int main(int argc, char * * argv)
{
    std::string initial_state;
    std::unordered_set<std::string> rules;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.size() < 10) continue;
        if (line.substr(0, 15) == "initial state: ")
            initial_state = line.substr(15);
        else if (line.find(" => ") != 5)
        {
            std::cerr << "[ERROR] Unexpected format in: " << line << '\n';
            return EXIT_FAILURE;
        }
        else if (line[9] == '#') rules.insert(line.substr(0, 5));
    }
    file.close();
    std::cout << "Problem A: " << evolveScore(initial_state, rules, 20) << '\n';
    std::cout << "Problem B: "
              << evolveScore(initial_state, rules, 50'000'000'000) << '\n';
    return EXIT_SUCCESS;
}

