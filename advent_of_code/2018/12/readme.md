# [Day 12: Subterranean Sustainability](https://adventofcode.com/2018/day/12)


#### Problem A

You are given a string with your initial state. This initial states indicates if the cells in *in finite line* are **on** or **off**. Each cell has an index, and the indices of the initial status start at index 0.

You are also given a set of rules that indicate what is the what will happen to a cell at each iteration of the algorithm. These rules modify the state of the cell depending on the values of the two neighbors around the cell. For example, the rule `##.## => #` means that given a cell that is off (current cell is the one in the middle of the rule) but surrounded by two on cells at each side, the cell will turn on.

After 20 iterations, what is the sum of all the indices that are **on**?

**Note:** The line is infinite, so some **on** elements can have a negative index.

#### Problem B

What is the sum of all indices that are **on** after `50'000'000'000` iterations?

