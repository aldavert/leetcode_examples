# [Day 10: The Stars Align](https://adventofcode.com/2018/day/10)

#### Problem A

You are given the information of a set of particles where you are given *the position* and *the velocity* of each particle.

The particles move until the generate a message. **What message will appear?**

#### Problem B

How many iterations are necessary until the message appears?

