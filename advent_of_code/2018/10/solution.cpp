#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <limits>
#include "fmt/core.h"

struct Particle
{
    int x = -1;
    int y = -1;
    int dx = -1;
    int dy = -1;
};

std::tuple<std::string, int> findStructure(std::vector<Particle> particles)
{
    constexpr int MAX = std::numeric_limits<int>::max(),
                  MIN = std::numeric_limits<int>::lowest();
    auto isStructured = [&](void) -> bool
    {
        int min_x = MAX, max_x = MIN, min_y = MAX, max_y = MIN;
        for (auto [x, y, dx, dy] : particles)
        {
            min_x = std::min(min_x, x);
            min_y = std::min(min_y, y);
            max_x = std::max(max_x, x);
            max_y = std::max(max_y, y);
        }
        if (max_y - min_y + 1 > 50) return false;
        std::vector<int> horizontal(max_x - min_x + 1);
        for (auto [x, y, dx, dy] : particles)
            ++horizontal[x - min_x];
        int n_columns = 0;
        for (int h : horizontal)
            n_columns += (h > max_y - min_y - 2);
        return n_columns > 1;
    };
    int counter = 0;
    while (!isStructured())
    {
        for (auto &[x, y, dx, dy] : particles)
        {
            x += dx;
            y += dy;
        }
        ++counter;
    }
    int min_x = MAX, max_x = MIN, min_y = MAX, max_y = MIN;
    for (auto [x, y, dx, dy] : particles)
    {
        min_x = std::min(min_x, x);
        min_y = std::min(min_y, y);
        max_x = std::max(max_x, x);
        max_y = std::max(max_y, y);
    }
    std::vector<std::string> screen(max_y - min_y + 1,
                                    std::string(max_x - min_x + 1, ' '));
    for (auto [x, y, dx, dy] : particles)
        screen[y - min_y][x - min_x] = '#';
    std::string result;
    for (auto row : screen)
        result += row + '\n';
    return {result, counter};
}

int main(int argc, char * * argv)
{
    std::vector<Particle> particles;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.size() < 10) continue;
        auto left = line.find('<') + 1,
             right = line.find(',');
        particles.push_back({});
        particles.back().x = std::stoi(line.substr(left, right - left));
        left = right + 1;
        right = line.find('>', left);
        particles.back().y = std::stoi(line.substr(left, right - left));
        left = line.find('<', right + 1) + 1;
        right = line.find(',', left);
        particles.back().dx = std::stoi(line.substr(left, right - left));
        left = right + 1;
        right = line.find('>', left);
        particles.back().dy = std::stoi(line.substr(left, right - left));
    }
    file.close();
    auto [message, time] = findStructure(particles);
    std::cout << "Problem A:\n" << message << '\n';
    std::cout << "Problem B: " << time << '\n';
    return EXIT_SUCCESS;
}

