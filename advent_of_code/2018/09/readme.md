# [Day 9: Marble Mania](https://adventofcode.com/2018/day/9)

#### Problem A

You have a circular list where a group of users insert consecutive integers (`0, 1, 2, 3, 4, 5, 6, 7, 8, ...`). The insertion is done as follows,
- Move from current position forward *(to the right)* one position , insert the new value as the next element *(to the right)* and move the current pointer to the newly inserted element.
- Inserted values that are **multiples of 23** you perform a different operation: you move backward *(to the left)* 7 positions, remove the current element, and set the next element *(to the right*) as the current position. The sum of the **removed element** and the **multiple of 23** is given as a score to the user that has done the insertion.

Given the number of players and the value of the last integer added to the list, what is the maximum score attained by a user?

#### Problem B

What is the maximum score if now the value of the last integer is `100` times larger?

