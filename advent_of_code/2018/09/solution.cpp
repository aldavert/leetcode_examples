#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <list>

long highScore(int number_of_players, int number_of_points)
{
    std::vector<long> scores(number_of_players);
    std::list<long> values;
    values.push_back(0);
    auto iter = values.begin();
    auto next = [&](int counter) -> void
    {
        while (counter-- > 0)
        {
            ++iter;
            if (iter == values.end()) iter = values.begin();
        }
    };
    auto back = [&](int counter) -> void
    {
        while (counter-- > 0)
        {
            if (iter == values.begin()) iter = values.end();
            --iter;
        }
    };
    for (int s = 1, gather = 23; s <= number_of_points; ++s)
    {
        if (s == gather)
        {
            back(7);
            scores[(s - 1) % number_of_players] += s + *iter;
            gather += 23;
            iter = values.erase(iter);
        }
        else
        {
            next(2);
            iter = values.insert(iter, s);
        }
    }
    return *std::max_element(scores.begin(), scores.end());
}

int main(int argc, char * * argv)
{
    std::string filename = "data.txt";
    int number_of_players = 0, number_of_points = 0;
    long expected_score = -1;
    if (argc <= 2)
    {
        if (argc == 2) filename = argv[1];
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
            return EXIT_FAILURE;
        }
        std::string text;
        for (std::string line; std::getline(file, line); ) text += line;
        auto first = text.find(" players; last marble is worth ");
        number_of_players = std::stoi(text.substr(0, first));
        first += 31;
        auto second = text.find(' ', first);
        number_of_points = std::stoi(text.substr(first, second - first));
        file.close();
    }
    else if (argc == 3)
    {
        if (std::string(argv[1]) != "test")
        {
            std::cerr << "[ERROR] Unexpected number of parameters (test).\n";
            return EXIT_FAILURE;
        }
        switch (std::stoi(argv[2]))
        {
        case 0:
            number_of_players = 10, number_of_points = 1618, expected_score = 8317;
            break;
        case 1:
            number_of_players = 13, number_of_points = 7999, expected_score = 146373;
            break;
        case 2:
            number_of_players = 17, number_of_points = 1104, expected_score = 2764;
            break;
        case 3:
            number_of_players = 21, number_of_points = 6111, expected_score = 54718;
            break;
        case 4:
            number_of_players = 30, number_of_points = 5807, expected_score = 37305;
            break;
        default:
            std::cerr << "[ERROR] Unexpected test value (it must be between 0 and 4).\n";
            return EXIT_FAILURE;
        }
    }
    else
    {
        std::cerr << "[ERROR] Unexpected number of parameters.\n";
        return EXIT_FAILURE;
    }
    long problem_a = highScore(number_of_players, number_of_points);
    std::cout << "Problem A: " << problem_a;
    if (expected_score == -1) std::cout << '\n';
    else
    {
        if (problem_a == expected_score) std::cout << " [CORRECT]\n";
        else std::cout << " [INCORRECT] (Expected value is '"
                       << expected_score << "')\n";
    }
    std::cout << "Problem B: " << highScore(number_of_players, number_of_points * 100) << '\n';
}

