# [Day 24: Immune System Simulator 20XX](https://adventofcode.com/2018/day/24)

#### Problem A

You are given the information of two armies corresponding to the immune system. The information contains the **amount of units**, the **hit points** of each unit, the **types of properties** that the units have (immunities and weaknesses) and the **attack** and **initiative** of the unit (the initiative is used to determine which unit attacks first and to resolve ties).

The fight has two phases: **target selection** and **attacking**.

During the **target selection** each group attempts to choose one target: the attacking group selects as target the group to which is would deal most damage, i.e. taking into account weaknesses and immunities but ignoring if the defending group has enough points to actually receive all the damage (it can overkill).

During the **attacking phase**, each group deals damage to the target it selected, if any. Groups attack in decreasing order of initiative, regardless of the group that they belong to. The damage taken by units depends on the attacking power of the attacker and the attributes of the defenders. If the defender is **immune** to the type of attack dealt by the attacker, it takes no damage. On the other hand, if the defender is **weak** to the type of attack, then it takes **double damage**. Defenders only lose whole units.

With the given parameters of the problem, how many units would the winning army have?

#### Problem B

In order for the *immune system* to win, you can give a boost to its units that allow them to increase their attack power. How many units does the immune system has left after getting the smallest boost it needs to win?

