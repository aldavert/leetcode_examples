#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <tuple>

template <typename T>
std::ostream& operator<<(std::ostream &out, std::vector<T> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

struct Unit
{
    int index = -1;
    int units = -1;
    int hitpoints = -1;
    std::vector<size_t> immune_idx;
    std::vector<size_t> weak_idx;
    std::vector<bool> immune;
    std::vector<bool> weak;
    int attack = -1;
    size_t attack_type = 0;
    int initiative = -1;
    inline int effectiveAttack(void) const { return units * attack; }
};

struct Battlefield
{
    std::vector<std::string> index2attack;
    std::unordered_map<std::string, size_t> attack2index;
    std::vector<Unit> immune_system;
    std::vector<Unit> infection;
};

std::tuple<int, int> fight(Battlefield battlefield, int attack_boost = 0)
{
    for (auto &unit : battlefield.immune_system)
        unit.attack += attack_boost;
    const size_t units_immune_system = battlefield.immune_system.size(),
                 units_infection = battlefield.infection.size();
    int total_active[2] = { static_cast<int>(units_immune_system),
                            static_cast<int>(units_infection) };
    std::vector<size_t> targets_immune(units_immune_system),
                        targets_infection(units_infection);
    auto search = [&](const std::vector<Unit> &attack,
                      const std::vector<Unit> &defend,
                      std::vector<size_t> &target) -> void
    {
        std::vector<size_t> order(attack.size());
        std::vector<bool> already_selected(defend.size(), false);
        for (size_t i = 0; i < attack.size(); ++i)
            order[i] = i;
        std::sort(order.begin(), order.end(), [&](size_t l, size_t r) -> bool
        {
            const int power_l = attack[l].effectiveAttack(),
                      power_r = attack[r].effectiveAttack();
            return (power_l > power_r) || ((power_l == power_r)
                && (attack[l].initiative > attack[r].initiative));
        });
        for (size_t i : order)
        {
            target[i] = defend.size();
            int attack_power = attack[i].units * attack[i].attack;
            std::vector<std::tuple<int, int, int, size_t> > enemies;
            for (size_t j = 0; j < defend.size(); ++j)
            {
                if ( already_selected[j]
                ||  (defend[j].immune[attack[i].attack_type])
                ||  (defend[j].units == 0))
                    continue;
                int damage = attack_power;
                damage *= 1
                       + defend[j].weak[attack[i].attack_type]
                       - defend[j].immune[attack[i].attack_type];
                enemies.push_back({damage,
                                   defend[j].effectiveAttack(),
                                   defend[j].initiative,
                                   j});
            }
            if (enemies.size() == 0) continue;
            std::sort(enemies.begin(), enemies.end());
            already_selected[std::get<3>(enemies.back())] = true;
            target[i] = std::get<3>(enemies.back());
        }
    };
    while ((total_active[0] > 0) && (total_active[1] > 0))
    {
        std::vector<std::tuple<int, int, const Unit *, Unit *> > attack_orders;
        search(battlefield.immune_system, battlefield.infection, targets_immune);
        for (size_t i = 0; i < units_immune_system; ++i)
        {
            if (targets_immune[i] == units_infection) continue;
            attack_orders.push_back({battlefield.immune_system[i].initiative, 1,
                                     &battlefield.immune_system[i],
                                     &battlefield.infection[targets_immune[i]]});
        }
        search(battlefield.infection, battlefield.immune_system, targets_infection);
        for (size_t i = 0; i < units_infection; ++i)
        {
            if (targets_infection[i] == units_immune_system) continue;
            attack_orders.push_back({battlefield.infection[i].initiative, 0,
                                     &battlefield.infection[i],
                                     &battlefield.immune_system[targets_infection[i]]});
        }
        std::sort(attack_orders.begin(), attack_orders.end(), std::greater<>());
        bool no_death = true;
        for (auto [initiative, group_id, attacker, defender] : attack_orders)
        {
            if ((attacker->units == 0) || (defender->units == 0)) continue;
            int attack_damage = attacker->units * attacker->attack;
            attack_damage *= 1
                           + defender->weak[attacker->attack_type]
                           - defender->immune[attacker->attack_type];
            int killed = std::min(attack_damage / defender->hitpoints, defender->units);
            defender->units -= killed;
            no_death = no_death && (killed == 0);
            if (defender->units == 0)
                --total_active[group_id];
        }
        if (no_death) break;
    }
    int result_immune = 0, result_infection = 0;
    for (const auto &unit : battlefield.immune_system)
        result_immune += unit.units;
    for (const auto &unit : battlefield.infection)
        result_infection += unit.units;
    return {result_immune, result_infection};
}

int main(int argc, char * * argv)
{
    Battlefield battlefield;
    auto attackIndex = [&](std::string token) -> size_t
    {
        auto &lut = battlefield.attack2index;
        size_t index = 0;
        if (auto search = lut.find(token); search == lut.end())
        {
            index = lut.size();
            lut[token] = index;
            battlefield.index2attack.push_back(token);
        }
        else index = search->second;
        return index;
    };
    auto processAttributes =
        [&](std::string text) -> std::pair<std::vector<size_t>, std::vector<size_t> >
    {
        std::vector<size_t> immune_weak[2];
        std::istringstream iss(text);
        std::string token;
        bool is_weak = false;
        while (iss >> token)
        {
            if (token == "weak")
            {
                is_weak = true;
                iss >> token;
            }
            else if (token == "immune")
            {
                is_weak = false;
                iss >> token;
            }
            else
            {
                if ((token.back() == ',') || (token.back() == ';'))
                    token.pop_back();
                immune_weak[is_weak].push_back(attackIndex(token));
            }
        }
        return {immune_weak[1], immune_weak[0]};
    };
    
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    bool is_immune_system = false;
    int index = 0;
    for (std::string line; std::getline(file, line); )
    {
        Unit current;
        if (line.size() < 10) continue;
        if (line == "Immune System:") { is_immune_system = true; continue; }
        if (line == "Infection:") { is_immune_system = false; continue; }
        auto r = line.find(" units each with ");
        if (r == std::string::npos)
        {
            std::cerr << "[WARNING] The line is wrongly formatted (A).\n";
            continue;
        }
        current.units = std::stoi(line.substr(0, r));
        auto l = r + 17;
        r = line.find(" hit points ", l);
        current.hitpoints = std::stoi(line.substr(l, r - l));
        if (r == std::string::npos)
        {
            std::cerr << "[WARNING] The line is wrongly formatted (B).\n";
            continue;
        }
        l = line.find('(', r + 12);
        if (l != std::string::npos)
        {
            r = line.find(')', l + 1);
            if (r == std::string::npos)
            {
                std::cerr << "[WARNING] The line is wrongly formatted (C).\n";
                continue;
            }
            const std::string part = line.substr(l + 1, r - l - 1);
            std::tie(current.weak_idx, current.immune_idx) = processAttributes(part);
        }
        l = line.find(" with an attack that does ") + 26;
        r = line.find(" damage at initiative ");
        if ((l == std::string::npos) || (r == std::string::npos))
        {
            std::cerr << "[WARNING] The line is wrongly formatted (D).\n";
            continue;
        }
        std::stringstream iss(line.substr(l, r - l));
        std::string attack_type;
        iss >> current.attack >> attack_type;
        current.initiative = std::stoi(line.substr(r + 22));
        current.attack_type = attackIndex(attack_type);
        
        current.index = index++;
        if (is_immune_system) battlefield.immune_system.push_back(current);
        else battlefield.infection.push_back(current);
    }
    file.close();
    for (auto &unit : battlefield.immune_system)
    {
        unit.weak = unit.immune = std::vector<bool>(battlefield.index2attack.size());
        for (size_t idx : unit.immune_idx)
            unit.immune[idx] = true;
        for (size_t idx : unit.weak_idx)
            unit.weak[idx] = true;
    }
    for (auto &unit : battlefield.infection)
    {
        unit.weak = unit.immune = std::vector<bool>(battlefield.index2attack.size());
        for (size_t idx : unit.immune_idx)
            unit.immune[idx] = true;
        for (size_t idx : unit.weak_idx)
            unit.weak[idx] = true;
    }
    auto [units_immune, units_infection] = fight(battlefield);
    std::cout << "Problem A: " << units_infection << '\n';
    int l = 0, r = 1'000'000;
    while (l < r)
    {
        int mid = (l + r) / 2;
        std::tie(units_immune, units_infection) = fight(battlefield, mid);
        if (units_infection > 0) l = mid + 1;
        else r = mid;
    }
    std::tie(units_immune, units_infection) = fight(battlefield, l);
    std::cout << "Problem B: " << units_immune << '\n';
    return EXIT_SUCCESS;
}

