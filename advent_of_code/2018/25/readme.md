# [Day 25: Four-Dimensional Adventure](https://adventofcode.com/2018/day/25)

#### Problem A

You are given a collection of 4-dimensional points. Points that are at 3 units of distance or less, using the Manhattan distance, form a cluster.

How many cluster are there in your data?

