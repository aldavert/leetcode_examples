#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <queue>

struct Point
{
    int coord[4] = {};
    inline int distance(const Point &other) const
    {
        return std::abs(coord[0] - other.coord[0])
             + std::abs(coord[1] - other.coord[1])
             + std::abs(coord[2] - other.coord[2])
             + std::abs(coord[3] - other.coord[3]);
    }
};

int main(int argc, char * * argv)
{
    std::vector<Point> points;
    int expected_clusters = -1;
    
    std::string filename = "data.txt";
    if (argc <= 2)
    {
        if (argc == 2) filename = argv[1];
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
            return EXIT_FAILURE;
        }
        for (std::string line; std::getline(file, line); )
        {
            int x, y, z, a;
            auto l = line.find(',');
            x = std::stoi(line.substr(0, l));
            ++l;
            auto r = line.find(',', l);
            y = std::stoi(line.substr(l, r - l));
            l = r + 1;
            r = line.find(',', l);
            z = std::stoi(line.substr(l, r - l));
            l = r + 1;
            a = std::stoi(line.substr(l));
            points.push_back({x, y, z, a});
        }
        file.close();
    }
    else if ((argc == 3) && (std::string(argv[1]) == "test"))
    {
        int index = -1;
        try
        {
            index = std::stoi(argv[2]);
        }
        catch (...)
        {
            std::cerr << "[ERROR] Unknown example ID.\n";
            return EXIT_FAILURE;
        }
        switch (index)
        {
        case 0:
            points = {{ 0, 0, 0, 0}, { 3, 0, 0, 0}, { 0, 3, 0, 0}, { 0, 0, 3, 0},
                      { 0, 0, 0, 3}, { 0, 0, 0, 6}, { 9, 0, 0, 0}, {12, 0, 0, 0}};
            expected_clusters = 2;
            break;
        case 1:
            points = {{-1, 2, 2, 0}, {0, 0, 2, -2}, {0, 0, 0, -2}, {-1, 2, 0, 0},
                      {-2, -2, -2, 2}, {3, 0, 2, -1}, {-1, 3, 2, 2}, {-1, 0, -1, 0},
                      {0, 2, 1, -2}, {3, 0, 0, 0}};
            expected_clusters = 4;
            break;
        case 2:
            points = {{1, -1, 0, 1}, {2, 0, -1, 0}, {3, 2, -1, 0}, {0, 0, 3, 1},
                      {0, 0, -1, -1}, {2, 3, -2, 0}, {-2, 2, 0, 0}, {2, -2, 0, -1},
                      {1, -1, 0, -1}, {3, 2, 0, 2}};
            expected_clusters = 3;
            break;
        case 3:
            points = {{1, -1, -1, -2}, {-2, -2, 0, 1}, {0, 2, 1, 3}, {-2, 3, -2, 1},
                      {0, 2, 3, -2}, {-1, -1, 1, -2}, {0, -2, -1, 0}, {-2, 2, 3, -1},
                      {1, 2, 2, 0}, {-1, -2, 0, -2}};
            expected_clusters = 8;
            break;
        default:
            std::cerr << "[ERROR] Unknown example ID.\n";
            return EXIT_FAILURE;
        }
    }
    else
    {
        std::cerr << "[ERROR] Unexpected number of parameters.\n";
        return EXIT_FAILURE;
    }
    
    std::vector<std::vector<size_t> > neighbors(points.size());
    for (size_t i = 0; i < points.size(); ++i)
        for (size_t j = i + 1; j < points.size(); ++j)
            if (points[i].distance(points[j]) <= 3)
                neighbors[i].push_back(j),
                neighbors[j].push_back(i);
    int number_of_clusters = 0;
    std::vector<int> cluster_label(points.size(), -1);
    for (size_t i = 0; i < points.size(); ++i)
    {
        if (cluster_label[i] != -1) continue;
        std::queue<size_t> q;
        q.push(i);
        while (!q.empty())
        {
            size_t current = q.front();
            q.pop();
            if (cluster_label[current] != -1) continue;
            cluster_label[current] = number_of_clusters;
            for (size_t next : neighbors[current])
                q.push(next);
        }
        ++number_of_clusters;
    }
    
    std::cout << "Problem A: " << number_of_clusters;
    if (expected_clusters != -1)
    {
        if (expected_clusters == number_of_clusters) std::cout << " [CORRECT]";
        else std::cout << " [WRONG] (" << expected_clusters
                       << " was expected instead).\n";
    }
    std::cout << '\n';
    return EXIT_SUCCESS;
}

