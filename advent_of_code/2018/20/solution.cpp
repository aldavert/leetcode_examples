#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <set>
#include <tuple>
#include <stack>

std::pair<int, int> roomsTraverse(std::string expression)
{
    std::unordered_map<char, std::pair<int, int> > dir = {
        {'N', {0, -1}}, {'E', {1, 0}}, {'S', {0, 1}}, {'W', {-1, 0}} };
    std::stack<std::pair<int, int> > positions;
    int x = 0, y = 0, prev_x = 0, prev_y = 0;
    std::map<std::pair<int, int>, std::set<std::pair<int, int> > > m;
    std::map<std::pair<int, int>, int> distances;
    for (size_t i = 1; i < expression.size() - 1; ++i)
    {
        if      (expression[i] == '(')
            positions.push({x, y});
        else if (expression[i] == ')')
            std::tie(x, y) = positions.top(),
            positions.pop();
        else if (expression[i] == '|')
            std::tie(x, y) = positions.top();
        else
        {
            auto [dx, dy] = dir[expression[i]];
            x += dx;
            y += dy;
            m[{x, y}].insert({prev_x, prev_y});
            int prev = distances[{prev_x, prev_y}];
            if (distances[{x, y}] != 0)
                distances[{x, y}] = std::min(distances[{x, y}], prev + 1);
            else distances[{x, y}] = prev + 1;
        }
        prev_x = x;
        prev_y = y;
    }
    int max_distance = 0, more_than_thousand = 0;
    for (const auto &[coor, d] : distances)
    {
        max_distance = std::max(max_distance, d);
        more_than_thousand += d >= 1000;
    }
    return {max_distance, more_than_thousand};
}

int main(int argc, char * * argv)
{
    std::string expression;
    int expected_distance = -1;
    std::string filename = "data.txt";
    if ((argc == 3) && (std::string(argv[1]) == "test"))
    {
        int test_id = -1;
        try
        {
            test_id = std::stoi(argv[2]);
        }
        catch (...)
        {
            std::cerr << "[ERROR] Unexpected test ID value.\n";
            return EXIT_FAILURE;
        }
        if (test_id == 0)
            expression = "^WNE$",
            expected_distance = 3;
        else if (test_id == 1)
            expression = "^ENWWW(NEEE|SSE(EE|N))$",
            expected_distance = 10;
        else if (test_id == 2)
            expression = "^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$",
            expected_distance = 18;
        else if (test_id == 3)
            expression = "^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$",
            expected_distance = 23;
        else if (test_id == 4)
            expression = "^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|"
                         "WWS(E|SS))))$",
            expected_distance = 31;
        else
        {
            std::cerr << "[ERROR] There is no test with ID=" << test_id << '\n';
            return EXIT_FAILURE;
        }
    }
    else
    {
        if (argc == 2) filename = argv[1];
        else if (argc != 1)
        {
            std::cerr << "[ERROR] Unexpected number of parameters.\n";
            return EXIT_FAILURE;
        }
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
            return EXIT_FAILURE;
        }
        for (std::string line; std::getline(file, line);)
            expression += line;
        file.close();
    }
    if ((expression.front() != '^') || (expression.back() != '$'))
    {
        std::cerr << "[ERROR] The input regular expression has an unexpected format"
                  << " (bad head and/or tail).\n";
        return EXIT_FAILURE;
    }
    auto [problem_a, problem_b] = roomsTraverse(expression);
    std::cout << "Problem A: " << problem_a;
    if (expected_distance == -1) std::cout << '\n';
    else
    {
        if (expected_distance == problem_a) std::cout << " [CORRECT]\n";
        else std::cout << " [WRONG] (" << expected_distance << " was expected "
                       << "instead.\n";
    }
    std::cout << "Problem B: " << problem_b << '\n';
    
    return EXIT_SUCCESS;
}

