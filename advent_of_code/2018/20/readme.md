# [Day 20: A Regular Map](https://adventofcode.com/2018/day/20)

You are given a regular expression that represents all the paths that you can follow in a maze (i.e. it represents the layout of a maze). The sequence starts with the symbol `^` and ends with `$`. Inside there are characters indicating four possible directions: `N` north/up, `E` east/right, `S` south/down and `W` west/left. There are also symbols indicating branches. Branches are surrounded by parentheses and separated by pipe symbols `|`. For example, the sequence `^ENWWW$` corresponds to the maze:
```
#########
#.|.|.|.#
#######-#
    #X|.#
    #####
```
where `.` indicates that there is a room, `#` a wall, `|` a door and `X` is your starting location. While the sequence `^ENWWW(NEEE|SSE(EE|N))$` corresponds to
```
#########       #########
#.|.|.|.#       # | | | #
#-#######       #-#######
#.|.|.|.#       #A| | | #
#-#####-# ----> #-#####-#
#.#.#X|.#       # # #X|.#
#-#-#####       #-#-#####
#.|.|.|.#       # |B| | #
#########       #########
```
where we have the two branches `A` and `B`. Branch `A` separates between `NEEE` and `SEE(EE|N)` while branch `B` separates between `EE` and `N`.

#### Problem A

What is the largest amount of doors that you need to pass to reach a room?

#### Problem B

How many rooms have a shortest path from your current location that pass through at least `1000` doors?

