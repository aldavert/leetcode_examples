#include <iostream>
#include <fstream>
#include <vector>
#include <string>

int penalty(const std::vector<int> &depth, const int delay)
{
    const int n = static_cast<int>(depth.size());
    int cost = 0, number_of_penalties = 0;
    for (int i = 0; i < n; ++i)
    {
        if (depth[i] < 2) continue;
        if ((i + delay) % (2 * depth[i] - 2) == 0)
        {
            cost += i * depth[i];
            ++number_of_penalties;
        }
    }
    return (number_of_penalties > 0)?cost:-1;
}

int main(int argc, char * * argv)
{
    std::vector<int> depth;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        auto separator = line.find(':');
        size_t index = std::stoi(line.substr(0, separator));
        while (depth.size() <= index) depth.push_back(-1);
        depth[index] = std::stoi(line.substr(separator + 2));
    }
    file.close();
    std::cout << "Problem A: " << penalty(depth, 0) << '\n';
    std::vector<bool> sieve(5'000'000, true);
    for (int i = 0, n = static_cast<int>(depth.size()); i < n; ++i)
    {
        if (depth[i] < 2) continue;
        int cycle_size = 2 * depth[i] - 2,
            start = (cycle_size - i) % cycle_size;
        while (start < 0) start += cycle_size;
        for (size_t j = start; j < sieve.size(); j += cycle_size)
        {
            sieve[j] = false;
        }
    }
    for (size_t i = 0; i < sieve.size(); ++i)
    {
        if (sieve[i])
        {
            std::cout << "Problem B: " << i << '\n';
            break;
        }
    }
    return EXIT_SUCCESS;
}

