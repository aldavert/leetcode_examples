# [Day 13: Packet Scanners](https://adventofcode.com/2017/day/13)

#### Problem A

You have multiple columns at different depths, where at each column there is an scanner. This scanner moves back and forth at each tick. Empty columns do not have a scanner. 

You have to travel from the first column to the last. At each tick you move to the next column and you always move through the top position of the column.

When you enter a column and you happen to jump on to of the scanner, i.e. the scanner is at the top position, you infer a penalty. The penalty is equal to the column id multiplied by the depth of the column.

In this game, you move first, and then the scanner move. So you are only detected, if the scanner will arrives at your position you are not detected. You are only detected when you arrive at a position where a scanner is present.

If you start right away, what is the sum of all the penalties incurred during your path?

#### Problem B

How many ticks do you have to wait before crossing so you won't incur any penalty?


