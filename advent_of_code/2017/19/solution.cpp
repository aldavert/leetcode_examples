#include <iostream>
#include <fstream>
#include <vector>
#include <string>

std::tuple<std::string, int> mazeMessage(const std::vector<std::string> &maze)
{
    constexpr int direction[] = {-1, 0, 1, 0, -1};
    const int n_row = static_cast<int>(maze.size()),
              n_col = static_cast<int>(maze[0].size());
    int y = 0, x = 0, o = 1;
    for (int col = 0; col < n_col; ++col)
        if (maze[0][col] == '|')
            x = col;
    std::string message;
    int n_steps = 0;
    while ((y >= 0) && (y < n_row) && (x >= 0) && (x < n_col)
       &&  (maze[y][x] != ' '))
    {
        if (maze[y][x] == '+')
        {
            int no = o - 1;
            if (no < 0) no = 3;
            int nx = x + direction[no],
                ny = y + direction[no + 1];
            if (maze[ny][nx] != ' ')
                o = no;
            else o =(o + 1) % 4;
        }
        else if ((maze[y][x] != '|') && (maze[y][x] != '-'))
                message += maze[y][x];
        x += direction[o];
        y += direction[o + 1];
        ++n_steps;
    }
    return {message, n_steps};
}

int main(int argc, char * * argv)
{
    std::vector<std::string> maze;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        maze.push_back(line);
    file.close();
    
    auto [message, n_steps] = mazeMessage(maze);
    std::cout << "Problem A: " << message << '\n';
    std::cout << "Problem B: " << n_steps << '\n';
    
    return EXIT_SUCCESS;
}

