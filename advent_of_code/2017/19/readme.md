# [Day 19: A Series of Tubes](https://adventofcode.com/2017/day/19)

#### Problem A

You are given the diagram of a path formed by pipes (`|` and `-`), corners (`+`) and letters. You start at the top row of the diagram and you end when you cannot follow any path.

When following the path you only have to turn when you are at a corner (`+`). When you find a letter, you have to keep following the path treating it as a pipe (whatever pipe is the more convenient in each case).

Sometimes pipes cross each other, in this case you have to go *under* the other pipe and keep following the current direction.

Following the pipe from the start to end you will encounter different letters. Joining them in the order that they are encountered, what is the resulting string?

#### Problem B

How long is the path?

