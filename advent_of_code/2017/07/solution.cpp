#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_set>
#include <unordered_map>

struct Node
{
    std::string name;
    int weight = -1;
    std::vector<std::string> children;
};

std::ostream& operator<<(std::ostream &out, const Node &node)
{
    out << node.name << " (" << node.weight << ')';
    if (node.children.size() > 0)
    {
        out << " -> ";
        for (bool next = false; std::string child : node.children)
        {
            if (next) [[likely]] out << ", ";
            next = true;
            out << child;
        }
    }
    return out;
}

int expectedWeight(std::string root, const std::vector<Node> &nodes)
{
    // The solution is a bit ad-hoc and only works with the problem's restriction
    // where there is a single node with the wrong weight.
    std::unordered_map<std::string, const Node *> tree;
    for (const auto &node : nodes)
        tree[node.name] = &node;
    int result = -1;
    auto calcWeight = [&](auto &&self, std::string current) -> int
    {
        std::vector<std::pair<int, std::string> > weights;
        int children_weight = 0;
        for (std::string child : tree[current]->children)
        {
            int child_weight = self(self, child);
            children_weight += child_weight;
            weights.push_back({child_weight, child});
        }
        if ((weights.size() > 1) && (result == -1))
        {
            std::sort(weights.begin(), weights.end());
            int expected_weight = weights[weights.size() / 2].first;
            if (weights.front().first != expected_weight)
            {
                int wrong_weight = tree[weights.front().second]->weight;
                result = wrong_weight - (weights.front().first - expected_weight);
            }
            else if (weights.back().first != expected_weight)
            {
                int wrong_weight = tree[weights.back().second]->weight;
                result = wrong_weight - (weights.back().first - expected_weight);
            }
        }
        return children_weight + tree[current]->weight;
    };
    calcWeight(calcWeight, root);
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<Node> nodes;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        nodes.push_back({});
        if (line.size() < 5) continue;
        auto idx_l = line.find(' ');
        nodes.back().name = line.substr(0, idx_l);
        idx_l = line.find('(', idx_l + 1) + 1;
        auto idx_r = line.find(')', idx_l);
        nodes.back().weight = std::stoi(line.substr(idx_l, idx_r - idx_l));
        idx_l = line.find("->", idx_r + 1);
        if (idx_l == std::string::npos) continue;
        idx_l += 2;
        while (idx_l != std::string::npos)
        {
            idx_r = line.find(' ', idx_l + 1);
            std::string current = line.substr(idx_l + 1, idx_r - idx_l - 1);
            if (current.back() == ',') current.pop_back();
            if (current.size() > 0)
                nodes.back().children.push_back(current);
            idx_l = idx_r;
        }
    }
    file.close();
    std::unordered_set<std::string> root_nodes;
    for (const auto &node : nodes)
        root_nodes.insert(node.name);
    for (const auto &node : nodes)
        for (const std::string &child : node.children)
            if (auto search = root_nodes.find(child); search != root_nodes.end())
                root_nodes.erase(search);
    if (root_nodes.size() != 1)
    {
        std::cout << "[ERROR] The given tree has an unexpected number of root nodes: "
                  << root_nodes.size() << '\n';
        return EXIT_FAILURE;
    }
    std::string root_node = *root_nodes.begin();
    std::cout << "Problem A: " << root_node << '\n';
    std::cout << "Problem B: " << expectedWeight(root_node, nodes) << '\n';
    return EXIT_SUCCESS;
}

