# [Day 7: Recursive Circus](https://adventofcode.com/2017/day/7)

#### Problem A

You are given a list with the information of the nodes on a tree. At each line, you are given the name of the node, its weight and, after a `->` symbol, a comma separated list with the name of its children.

What is the name of the root node of the tree?

#### Problem B

The weights of the tree must be balanced, i.e. the weight of all the children of a node need to be the same. The weight of a child is computed as its weight plus the weight of all its descendants. In the tree, there is a single node that has the wrong weight. What weight should have this node?

