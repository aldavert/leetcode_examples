# [Day 14: Disk Defragmentation](https://adventofcode.com/2017/day/14)

We are using the hash algorithm from [problem 10](../10/readme.md) to represent the information of a `128 * 128` occupancy grid where each cell indicates if the space is **free** or **used**. You are given a key that by appending the row index will generate the hexadecimal representation of that row. For example, given the key `flqrgnkx` we can obtain the content of the 10th row by doing `hash("flqrgnkx-10")`.

The hash function generates a hexadecimal representation with 32 digits that when converted to binary results in the 128 bits row representation.

#### Problem A

Given a puzzle key, how many occupied cells contains the grid?

#### Problem B

And, how many 4-connected blocks exist in the grid?


