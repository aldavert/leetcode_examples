#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
#include <queue>

std::string hash(std::string input)
{
    constexpr int list_size = 256;
    constexpr char hexadecimal[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8',
        '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    std::vector<int> substring_length, values(list_size);
    
    for (char symbol : input)
        substring_length.push_back(static_cast<int>(symbol));
    substring_length.push_back(17);
    substring_length.push_back(31);
    substring_length.push_back(73);
    substring_length.push_back(47);
    substring_length.push_back(23);
    for (int i = 0; i < list_size; ++i) values[i] = i;
    int position = 0, skip = 0;
    for (int repetition = 0; repetition < 64; ++repetition)
    {
        for (int length : substring_length)
        {
            for (int i = 0, j = length - 1; i < j; ++i, --j)
                std::swap(values[(position + i) % list_size],
                          values[(position + j) % list_size]);
            position = (position + length + skip) % list_size;
            ++skip;
        }
    }
    std::string result;
    for (int i = 0; i < list_size; i += 16)
    {
        int value = 0;
        for (int j = i; j < i + 16; ++j)
            value = value ^ values[j];
        result += hexadecimal[0x0f & (value >> 4)];
        result += hexadecimal[0x0f & (value     )];
    }
    return result;
}

int main(int argc, char * * argv)
{
    std::unordered_map<char, std::string> convert = {
        { '0',  "0000" },
        { '1',  "0001" },
        { '2',  "0010" },
        { '3',  "0011" },
        { '4',  "0100" },
        { '5',  "0101" },
        { '6',  "0110" },
        { '7',  "0111" },
        { '8',  "1000" },
        { '9',  "1001" },
        { 'a',  "1010" },
        { 'b',  "1011" },
        { 'c',  "1100" },
        { 'd',  "1101" },
        { 'e',  "1110" },
        { 'f',  "1111" }
    };
    std::unordered_map<char, int> nbits = {
        { '0',  0 },
        { '1',  1 },
        { '2',  1 },
        { '3',  2 },
        { '4',  1 },
        { '5',  2 },
        { '6',  2 },
        { '7',  3 },
        { '8',  1 },
        { '9',  2 },
        { 'a',  2 },
        { 'b',  3 },
        { 'c',  2 },
        { 'd',  3 },
        { 'e',  3 },
        { 'f',  4 }
    };
    std::string data = "stpzcrnm";
    if (argc > 1) data = argv[1];
    int problem_a = 0, problem_b = 0;
    std::vector<std::string> grid;
    for (int i = 0; i < 128; ++i)
    {
        std::string row = data + "-" + std::to_string(i);
        std::string output = hash(row);
        grid.push_back("");
        for (char h : output)
        {
            problem_a += nbits[h];
            grid.back() += convert[h];
        }
    }
    std::cout << "Problem A: " << problem_a << '\n';
    constexpr int dirs[] = {-1, 0, 1, 0, -1};
    std::vector<std::vector<int> > labels(128, std::vector<int>(128, -1));
    for (int row = 0; row < 128; ++row)
    {
        for (int col = 0; col < 128; ++col)
        {
            if ((grid[row][col] == '1') && (labels[row][col] == -1))
            {
                std::queue<std::pair<int, int> > q;
                q.push({row, col});
                while (!q.empty())
                {
                    auto [r, c] = q.front();
                    q.pop();
                    if ((grid[r][c] != '1') || (labels[r][c] == problem_b)) continue;
                    labels[r][c] = problem_b;
                    for (int d = 0; d < 4; ++d)
                    {
                        int nr = r + dirs[d], nc = c + dirs[d + 1];
                        if ((nr < 0) || (nc < 0) || (nr >= 128) || (nc >= 128))
                            continue;
                        q.push({nr, nc});
                    }
                }
                ++problem_b;
            }
        }
    }
    std::cout << "Problem B: " << problem_b << '\n';
    
    return EXIT_SUCCESS;
}

