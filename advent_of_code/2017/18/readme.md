# [Day 18: Duet](https://adventofcode.com/2017/day/18)

#### Problem A

You are given a computer program written for a small computer that only has 7 assembler instructions:
- `snd X` plays a sound with a frequency equal to value `X`
- `set X Y` sets register `X` to the value of `Y`.
- `add X Y` increases register `X` by the value of `Y`.
- `mul X Y` multiplies the register `X` by the value of `Y` and stores the result back to `X`.
- `mod X Y` computes the modulo of the register `X` by the value of `Y` and stores the result back to `X`.
- `rcv X` if the value of `X` is not zero, it recovers the frequency of the last sound played, otherwise it doesn't do anything.
- `jgz X Y` jumps the offset `Y` if `X` is greater than zero.

The computer has many registers, registers are identified by a single character, and are initialized as 0. The instructions can take a register or a number. The value of the register is the integer it contains while, the value of a number is that number.

The program stops when it reaches an instruction that is outside by either end of the program.

What is the first value recovered by the `rcv` instruction?

#### Problem B

Now, you have two processes that are running the same code with the difference that the commands `snd X` and `rcv X` correspond, respectively,
- to send the values of `X` to the other process
- to receive a value from the other process and store it to register `X`.

The processes are connected by a pipeline that behaves like a queue, so if a process sends more than one message, they are stored in order inside the pipeline. Additionally, if `rcv` is called but the pipeline is empty, the process is locked waiting until a message is received.

The processes end when a process finishes its execution because it exits the code or when both processes are locked waiting a message from one another (they are in **deadlock**).

Both programs start at the same time with the only difference that both processes start with register `p` indicating the process id, i.e. process 0 starts with `p=0` and process 1 starts with `p=1`.

How many messages has send process 1 to process 0 up until it finishes its execution?

