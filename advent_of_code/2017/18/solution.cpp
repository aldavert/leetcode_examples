#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <variant>
#include <queue>

enum class ID { SND = 0, SET, ADD, MUL, MOD, RCV, JGZ };

struct Instruction
{
    Instruction(void) = default;
    Instruction(ID i, std::variant<char, long> v) : id(i)
    {
        if (std::holds_alternative<char>(v))
        {
            is_register[0] = true;
            value[0] = 0;
            name[0] = std::get<char>(v);
        }
        else
        {
            is_register[0] = false;
            value[0] = std::get<long>(v);
            name[0] = '\0';
        }
        is_register[1] = false;
        value[1] = 0;
        name[1] = '\0';
    }
    Instruction(ID i, std::variant<char, long> f, std::variant<char, long> s) : id(i)
    {
        if (std::holds_alternative<char>(f))
        {
            is_register[0] = true;
            value[0] = 0;
            name[0] = std::get<char>(f);
        }
        else
        {
            is_register[0] = false;
            value[0] = std::get<long>(f);
            name[0] = '\0';
        }
        if (std::holds_alternative<char>(s))
        {
            is_register[1] = true;
            value[1] = 0;
            name[1] = std::get<char>(s);
        }
        else
        {
            is_register[1] = false;
            value[1] = std::get<long>(s);
            name[1] = '\0';
        }
    }
    ID id = ID::SND;
    bool is_register[2] = {false, false};
    char name[2] = {'\0', '\0'};
    long value[2] = {0, 0};
};

std::ostream& operator<<(std::ostream &out, const Instruction &inst)
{
    auto display = [&](int op) -> std::string
    {
        if (inst.is_register[op]) return std::string(1, inst.name[op]);
        else return std::to_string(inst.value[op]);
    };
    switch (inst.id)
    {
    case ID::SND:
        out << "snd " << display(0);
        break;
    case ID::SET:
        out << "set " << display(0) << ' ' << display(1);
        break;
    case ID::ADD:
        out << "add " << display(0) << ' ' << display(1);
        break;
    case ID::MUL:
        out << "mul " << display(0) << ' ' << display(1);
        break;
    case ID::MOD:
        out << "mod " << display(0) << ' ' << display(1);
        break;
    case ID::RCV:
        out << "rcv " << display(0);
        break;
    case ID::JGZ:
        out << "jgz " << display(0) << ' ' << display(1);
        break;
    default:
        out << "<UNKNOWN> ";
        break;
    };
    return out;
}

long runFirstRCV(const std::vector<Instruction> &program)
{
    const int n = static_cast<int>(program.size());
    long registers[26] = {};
    int pc = 0;
    long sound = 0;
    auto value = [&registers](const Instruction &inst, int op) -> long
    {
        if (inst.is_register[op]) return registers[inst.name[op] - 'a'];
        else return inst.value[op];
    };
    
    while ((pc >= 0) && (pc < n))
    {
        switch (program[pc].id)
        {
        case ID::SND:
            sound = value(program[pc], 0);
            ++pc;
            break;
        case ID::SET:
            registers[program[pc].name[0] - 'a'] = value(program[pc], 1);
            ++pc;
            break;
        case ID::ADD:
            registers[program[pc].name[0] - 'a'] += value(program[pc], 1);
            ++pc;
            break;
        case ID::MUL:
            registers[program[pc].name[0] - 'a'] *= value(program[pc], 1);
            ++pc;
            break;
        case ID::MOD:
            registers[program[pc].name[0] - 'a'] %= value(program[pc], 1);
            ++pc;
            break;
        case ID::RCV:
            if (sound != 0) return sound;
            ++pc;
            break;
        case ID::JGZ:
            if (value(program[pc], 0) > 0)
                pc += static_cast<int>(value(program[pc], 1));
            else ++pc;
            break;
        default:
            std::cerr << "[ERROR] Unexpected instruction.\n";
            return -1000;
        }
    }
    return -1;
}

long runMessages(const std::vector<Instruction> &program)
{
    const int n = static_cast<int>(program.size());
    long registers[2][26] = {};
    std::queue<long> pipes[2];
    registers[1]['p' - 'a'] = 1;
    int pc[2] = {};
    int active = 0;
    
    auto value = [&](int op) -> long
    {
        const Instruction &inst = program[pc[active]];
        if (inst.is_register[op]) return registers[active][inst.name[op] - 'a'];
        else return inst.value[op];
    };
    auto reg = [&](void) -> long&
    {
        return registers[active][program[pc[active]].name[0] - 'a'];
    };
    long messages_1 = 0;
    bool run = true;
    while (run && (pc[active] >= 0) && (pc[active] < n))
    {
        switch (program[pc[active]].id)
        {
        case ID::SND:
            pipes[!active].push(value(0));
            messages_1 += (active == 1);
            ++pc[active];
            break;
        case ID::SET:
            reg() = value(1);
            ++pc[active];
            break;
        case ID::ADD:
            reg() += value(1);
            ++pc[active];
            break;
        case ID::MUL:
            reg() *= value(1);
            ++pc[active];
            break;
        case ID::MOD:
            reg() %= value(1);
            ++pc[active];
            break;
        case ID::RCV:
            if (!pipes[active].empty())
            {
                reg() = pipes[active].front();
                pipes[active].pop();
                ++pc[active];
            }
            else
            {
                if (pipes[!active].empty())
                    run = false;
                active = !active;
            }
            break;
        case ID::JGZ:
            if (value(0) > 0)
                pc[active] += static_cast<int>(value(1));
            else ++pc[active];
            break;
        default:
            std::cerr << "[ERROR] Unexpected instruction.\n";
            return -1000;
        }
    }
    return messages_1;
}

int main(int argc, char * * argv)
{
    std::vector<Instruction> program;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        std::istringstream iss(line);
        std::string command, first, second;
        iss >> command >> first >> second;
        ID id;
        if      (command == "snd") id = ID::SND;
        else if (command == "set") id = ID::SET;
        else if (command == "add") id = ID::ADD;
        else if (command == "mul") id = ID::MUL;
        else if (command == "mod") id = ID::MOD;
        else if (command == "rcv") id = ID::RCV;
        else if (command == "jgz") id = ID::JGZ;
        else
        {
            std::cerr << "[ERROR] Command '" << line << "' is not known.\n";
            return EXIT_FAILURE;
        }
        if ((id == ID::SND) || (id == ID::RCV))
        {
            if ((first.size() == 1) && (first[0] >= 'a') && (first[0] <= 'z'))
                program.push_back({id, first[0]});
            else program.push_back({id, std::stol(first)});
        }
        else if (id == ID::JGZ)
        {
            std::variant<char, long> f, s;
            if ((first.size() == 1) && (first[0] >= 'a') && (first[0] <= 'z'))
                f = first[0];
            else f = std::stol(first);
            if ((second.size() == 1) && (second[0] >= 'a') && (second[0] <= 'z'))
                s = second[0];
            else s = std::stol(second);
            program.push_back({id, f, s});
        }
        else
        {
            std::variant<char, long> f, s;
            f = first[0];
            if ((second.size() == 1) && (second[0] >= 'a') && (second[0] <= 'z'))
                s = second[0];
            else s = std::stol(second);
            program.push_back({id, f, s});
        }
    }
    file.close();
    
    std::cout << "Problem A: " << runFirstRCV(program) << '\n';
    std::cout << "Problem B: " << runMessages(program) << '\n';
    
    return EXIT_SUCCESS;
}

