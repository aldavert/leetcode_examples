#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <bit>

int numberActive2States(const std::vector<std::string> &grid, int n_iterations)
{
    constexpr int direction[] = {-1, 0, 1, 0, -1};
    auto ID = [](int x, int y) -> size_t
    {
        return static_cast<size_t>(std::bit_cast<unsigned int>(x)) << 32
             | static_cast<size_t>(std::bit_cast<unsigned int>(y));
    };
    std::unordered_set<size_t> active;
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>(grid[0].size());
    for (int row = 0; row < n_rows; ++row)
        for (int col = 0; col < n_cols; ++col)
            if (grid[row][col] == '#')
                active.insert(ID(col - n_cols / 2, row - n_rows / 2));
    int activated_cells = 0, x = 0, y = 0, o = 3;
    for (int i = 0; i < n_iterations; ++i)
    {
        if (auto search = active.find(ID(x, y)); search != active.end())
        {
            active.erase(search);
            o = (o > 0)?o - 1:3;
        }
        else
        {
            active.insert(ID(x, y));
            o = (o < 3)?o + 1:0;
            ++activated_cells;
        }
        x += direction[o];
        y += direction[o + 1];
    }
    return activated_cells;
}

int numberActive4States(const std::vector<std::string> &grid, int n_iterations)
{
    constexpr int direction[] = {-1, 0, 1, 0, -1};
    auto ID = [](int x, int y) -> size_t
    {
        return static_cast<size_t>(std::bit_cast<unsigned int>(x)) << 32
             | static_cast<size_t>(std::bit_cast<unsigned int>(y));
    };
    std::unordered_map<size_t, int> active;
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>(grid[0].size());
    for (int row = 0; row < n_rows; ++row)
        for (int col = 0; col < n_cols; ++col)
            if (grid[row][col] == '#')
                active[ID(col - n_cols / 2, row - n_rows / 2)] = 2;
    int activated_cells = 0, x = 0, y = 0, o = 3;
    for (int i = 0; i < n_iterations; ++i)
    {
        if (auto search = active.find(ID(x, y)); search != active.end())
        {
            if      (search->second == 0) o = (o < 3)?o + 1:0;
            else if (search->second == 2) o = (o > 0)?o - 1:3;
            else if (search->second == 3) o = (o + 2) % 4;
            search->second = (search->second + 1) % 4;
            activated_cells += (search->second == 2);
        }
        else
        {
            active[ID(x, y)] = 1;
            o = (o < 3)?o + 1:0;
        }
        x += direction[o];
        y += direction[o + 1];
    }
    return activated_cells;
}

int main(int argc, char * * argv)
{
    std::vector<std::string> grid;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        if (line.size() > 1)
            grid.push_back(line);
    file.close();
    std::cout << "Problem A: " << numberActive2States(grid, 10'000) << '\n';
    std::cout << "Problem B: " << numberActive4States(grid, 10'000'000) << '\n';
    return EXIT_SUCCESS;
}

