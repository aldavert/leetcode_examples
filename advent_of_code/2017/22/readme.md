# [Day 22: Sporifica Virus](https://adventofcode.com/2017/day/22)

#### Problem A

You are provided an initial grid where cells are active `#` or inactive `.`. You start at the **center of the grid facing up** and you move around by following these steps each time you move:
- If the current cell is **active**, turn **right**. Otherwise turn **left**. *Turning is done in place, without changing your cell.*
- Once rotated, flip the status of the cell: if active, make it inactive and, if inactive, make it active.
- The move forward to the next cell.

After `10'000` moves, how many moves have caused a cell to become *active*?

#### Problem B

Now, the cells have four states, *inactive*, *prepared*, *active* and *flagged* and, the state is changed in order: `inactive -> prepared -> active -> flagged -> inactive`.

Your behavior while moving on the map also changes:
- The orientation is modified depending on the current status of the cell:
    - When *inactive*, turn **left**.
    - When *prepared*, **do not turn**.
    - When *active*, turn **right**.
    - When *flagged*, **reverse the direction**: you will go back the way you came.
- Modify the status of the current cell as expected: `inactive -> prepared -> active -> flagged -> inactive`.
- The move forward to the next cell.

After `10'000'000` moves, how many moves have caused a cell to become *active*?

