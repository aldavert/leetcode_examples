# [Day 1: Inverse Captcha](https://adventofcode.com/2017/day/1)

#### Problem A

You are given a big number that is in a circular queue. Search all the adjacent digits that are repeated, and return the sum of these digits.

#### Problem B

Now, instead of comparing digits that are adjacent, check digits that are halfway around the circular queue. What is the sum of the paired digits?

