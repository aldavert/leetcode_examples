#include <iostream>
#include <fstream>
#include <string>

int main(int argc, char * * argv)
{
    std::string digits;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    file >> digits;
    file.close();
    int problem_a = 0;
    for (char previous = digits.back(); char digit : digits)
    {
        if (digit == previous) problem_a += digit - '0';
        previous = digit;
    }
    std::cout << "Problem A: " << problem_a << '\n';
    int problem_b = 0;
    for (size_t i = 0, n = digits.size(); i < n; ++i)
        if (digits[i] == digits[(i + n / 2) % n])
            problem_b += digits[i] - '0';
    std::cout << "Problem B: " << problem_b << '\n';
    return EXIT_SUCCESS;
}

