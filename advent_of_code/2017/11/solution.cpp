#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>

enum class DIRECTION { NORTH, NORTHEAST, SOUTHEAST, SOUTH, SOUTHWEST, NORTHWEST };

int main(int argc, char * * argv)
{
    std::unordered_map<std::string, DIRECTION> to_direction;
    to_direction["n"]  = DIRECTION::NORTH;
    to_direction["ne"] = DIRECTION::NORTHEAST;
    to_direction["se"] = DIRECTION::SOUTHEAST;
    to_direction["s"]  = DIRECTION::SOUTH;
    to_direction["sw"] = DIRECTION::SOUTHWEST;
    to_direction["nw"] = DIRECTION::NORTHWEST;
    std::vector<DIRECTION> path;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    std::string data;
    for (std::string line; std::getline(file, line); ) data += line;
    file.close();
    std::string current;
    for (size_t i = 0; i < data.size(); ++i)
    {
        if (data[i] != ',') current += data[i];
        else
        {
            path.push_back(to_direction[current]);
            current = "";
        }
    }
    path.push_back(to_direction[current]);
    int x = 0, y = 0, z = 0;
    std::vector<int> distance;
    for (auto dir : path)
    {
        switch (dir)
        {
        case DIRECTION::NORTH:
            ++y;
            --z;
            break;
        case DIRECTION::SOUTH:
            --y;
            ++z;
            break;
        case DIRECTION::NORTHEAST:
            ++x;
            --z;
            break;
        case DIRECTION::SOUTHWEST:
            --x;
            ++z;
            break;
        case DIRECTION::NORTHWEST:
            --x;
            ++y;
            break;
        case DIRECTION::SOUTHEAST:
            ++x;
            --y;
            break;
        default:
            break;
        }
        distance.push_back((std::abs(x) + std::abs(y) + std::abs(z)) / 2);
    }
    std::cout << "Problem A: " << distance.back() << '\n';
    std::cout << "Problem B: " << *std::max_element(distance.begin(), distance.end())
              << '\n';
    return EXIT_SUCCESS;
}

