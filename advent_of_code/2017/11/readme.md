# [Hex Ed](https://adventofcode.com/2017/day/11)

#### Problem A

You have an hex grid and a path to follow in this path grid. The directions are north `n`, northeast `ne`, southeast `se`, south `s`, southwest `sw` and northwest `nw`.

What is the distance, number of hops between cells, between the start position and the last position after following the path.

#### Problem B

What is the furthest away position?

