# [Day 25: The Halting Problem](https://adventofcode.com/2017/day/25)

You have to implement a Turing machine like program, where you have an infinite binary tape and a cursor that can move left-right and flip a tape cell.

You are give a set of instruction with a state machine with the instructions applied on the Turing machine. Once the specified number of steps have been completed, how many cells contain a `1`?


