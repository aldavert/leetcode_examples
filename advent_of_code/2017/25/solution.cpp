#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>
#include <unordered_set>

bool startsWith(const std::string &input, const std::string &pattern)
{
    return input.substr(0, pattern.size()) == pattern;
}

struct State
{
    int write[2] = {};
    bool to_left[2] = {};
    char next_state[2] = {};
};

int main(int argc, char * * argv)
{
    char start_state = '\0';
    long checksum = 0, current_value = 0;
    std::unordered_map<char, State> states;
    State * active = nullptr;
    
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (startsWith(line, "Begin in state "))
        {
            start_state = line[line.size() - 2];
        }
        else if (startsWith(line, "Perform a diagnostic checksum after "))
        {
            std::string current = line.substr(36);
            checksum = std::stol(current.substr(0, current.find(' ')));
        }
        else if (startsWith(line, "In state "))
        {
            states[line[line.size() - 2]] = {};
            active = &states[line[line.size() - 2]];
        }
        else if (startsWith(line, "  If the current value is "))
            current_value = std::stol(line.substr(26, line.size() - 27));
        else if (startsWith(line, "    - Write the value "))
            (*active).write[current_value] =
                std::stoi(line.substr(22, line.size() - 23));
        else if (startsWith(line, "    - Move one slot to the right."))
            (*active).to_left[current_value] = false;
        else if (startsWith(line, "    - Move one slot to the left."))
            (*active).to_left[current_value] = true;
        else if (startsWith(line, "    - Continue with state "))
            (*active).next_state[current_value] = line[line.size() - 2];
    }
    file.close();
    
    std::unordered_set<int> tape;
    int result = 0, position = 0;
    char state = start_state;
    for (int step = 0; step < checksum; ++step)
    {
        active = &states[state];
        if (auto search = tape.find(position); search == tape.end())
        {
            // It's a 0.
            if (active->write[0])
            {
                tape.insert(position);
                ++result;
            }
            position += 1 - 2 * active->to_left[0];
            state = active->next_state[0];
        }
        else
        {
            // It's a 1.
            if (!active->write[1])
            {
                tape.erase(search);
                --result;
            }
            position += 1 - 2 * active->to_left[1];
            state = active->next_state[1];
        }
    }
    std::cout << "Problem A: " << result << '\n';
    
    return EXIT_SUCCESS;
}

