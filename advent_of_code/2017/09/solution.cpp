#include <iostream>
#include <fstream>
#include <string>

int score(std::string data)
{
    const int n = static_cast<int>(data.size());
    int result = 0;
    for (int i = 0, level = 0; i < n; ++i)
    {
        if (data[i] == '!') ++i;
        else if (data[i] == '<')
        {
            ++i;
            while (data[i] != '>')
                i += 1 + (data[i] == '!');
        }
        else if (data[i] == '{')
        {
            ++level;
        }
        else if (data[i] == '}')
        {
            result += level;
            --level;
        }
    }
    return result;
}

int removedGarbage(std::string data)
{
    const int n = static_cast<int>(data.size());
    int result = 0;
    for (int i = 0; i < n; ++i)
    {
        if (data[i] == '!') ++i;
        else if (data[i] == '<')
        {
            ++i;
            for (; data[i] != '>'; ++i)
            {
                if (data[i] == '!') ++i;
                else ++result;
            }
        }
    }
    return result;
}

int main(int argc, char * * argv)
{
    std::string data;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << " is not opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        data += line;
    file.close();
    std::cout << "Problem A: " << score(data) << '\n';
    std::cout << "Problem B: " << removedGarbage(data) << '\n';
    return EXIT_SUCCESS;
}

