# [Day 9: Stream Processing](https://adventofcode.com/2017/day/9)

#### Problem A

You have a stream of characters where you have block of information and blocks of garbage. Blocks of information are blocks of text that are surrounded by `{` and `}` while, blocks of garbage are surrounded between `<` and `>`. Information blocks can be nested (recursively) but blocks of garbage cannot, i.e. the symbol `<` is ignored inside a block of garbage.

Additionally, the symbol `!` is used to disable the next symbol on the stream. For example, this garbage block `<{!>}>` ends at the second `>` because the first `>` is disabled by `!`. The symbol `!` can disable itself. For example, `<!!>` is equivalent to `<>`.

Given a string, blocks of information are given the score depending on its nested depths. For example, `{}` has a score of 1, `{{}, {}}` has a score of `1 + 2 + 2 = 5` and `{{{}}}` has a score of `1 + 2 + 3 = 6`.

Compute the total score of your input.

#### Problem B

Compute how many characters are inside the garbage block. Don't count the characters that are disabled by `!`. For example, `<>` contains 0 characters, `<<<<>` contains 3 characters, `<!!!>>` contains 0 characters or `<{o"i!a,<{i<a>` contains 10 characters.


