# [Day 20: Particle Swarm](https://adventofcode.com/2017/day/20)

#### Problem A

You are given the information of a set of particles, their position, velocity and acceleration. Once these particles have moved for a long time, which is the particle is the closest to the origin `(0, 0, 0)`.

**Note:** Use the Manhattan distance to compute the distance between the particles and the origin.

#### Problem B

Now, when two particles collide, they are destroyed. How many particles remain when all collisions are resolved?

