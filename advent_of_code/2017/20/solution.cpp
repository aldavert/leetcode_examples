#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <limits>
#include <unordered_map>

struct Point
{
    Point(void) = default;
    Point(std::string text)
    {
        auto f = text.find(',');
        auto s = text.find(',', f + 1);
        x = std::stol(text.substr(0, f));
        y = std::stol(text.substr(f + 1, s - f - 1));
        z = std::stol(text.substr(s + 1));
    }
    Point& operator+=(const Point &o)
    {
        x += o.x;
        y += o.y;
        z += o.z;
        return *this;
    }
    bool operator==(const Point &o) const
    {
        return (x == o.x) && (y == o.y) && (z == o.z);
    }
    long x = 0;
    long y = 0;
    long z = 0;
};

template <>
struct std::hash<Point>
{
    std::size_t operator()(const Point &p) const noexcept
    {
        std::size_t hx = std::hash<long>{}(p.x),
                    hy = std::hash<long>{}(p.y),
                    hz = std::hash<long>{}(p.z);
        return ((hx ^ (hy << 1)) >> 1) ^ hz;
    }
};

std::ostream& operator<<(std::ostream &out, const Point &p)
{
    out << '<' << p.x << ',' << p.y << ',' << p.z << '>';
    return out;
}

struct Particle
{
    Point position;
    Point velocity;
    Point acceleration;
};

std::ostream& operator<<(std::ostream &out, const Particle &p)
{
    out << "p=" << p.position << ", v=" << p.velocity << ", a=" << p.acceleration;
    return out;
}

int closestApproachParticle(std::vector<Particle> particles)
{
    const long t = 1'000;
    long selected_distance = std::numeric_limits<long>::max();
    int selected_idx = 0;
    for (int i = 0, n = static_cast<int>(particles.size()); i < n; ++i)
    {
        long distance = std::abs(particles[i].position.x
                               + particles[i].velocity.x * t
                               + particles[i].acceleration.x * t * t / 2)
                      + std::abs(particles[i].position.y
                               + particles[i].velocity.y * t
                               + particles[i].acceleration.y * t * t / 2)
                      + std::abs(particles[i].position.z
                               + particles[i].velocity.z * t
                               + particles[i].acceleration.z * t * t / 2);
        if (distance < selected_distance)
        {
            selected_distance = distance;
            selected_idx = i;
        }
    }
    return selected_idx;
}

int solveCollisions(std::vector<Particle> particles)
{
    std::vector<bool> enabled(particles.size(), true);
    std::unordered_map<Point, size_t> collisions;
    auto checkCollisions = [&](void) -> void
    {
        collisions.clear();
        for (size_t i = 0; i < particles.size(); ++i)
        {
            if (!enabled[i]) continue;
            const Point &p = particles[i].position;
            if (auto search = collisions.find(p); search != collisions.end())
            {
                enabled[i] = false;
                enabled[search->second] = false;
            }
            else collisions[p] = i;
        }
    };
    
    checkCollisions();
    for (int t = 0; t < 1'000; ++t)
    {
        for (auto &p : particles)
        {
            p.velocity += p.acceleration;
            p.position += p.velocity;
        }
        checkCollisions();
    }
    int total_valid = 0;
    for (size_t i = 0; i < particles.size(); ++i)
        total_valid += enabled[i];
    return total_valid;
}

int main(int argc, char * * argv)
{
    std::vector<Particle> particles;
    auto setPoint = [&](char type, const Point &p) -> void
    {
        if      (type == 'p') particles.back().position = p;
        else if (type == 'v') particles.back().velocity = p;
        else if (type == 'a') particles.back().acceleration = p;
        else std::cerr << "[WARNING] Unknown point type.\n";
    };
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        particles.push_back({});
        if (line.size() < 20) continue;
        std::string::size_type block = 0;
        while (block != std::string::npos)
        {
            auto l = line.find('<', block) + 1;
            auto r = line.find('>', l);
            setPoint(line[block], Point(line.substr(l, r - l)));
            block = line.find(", ", r);
            if (block != std::string::npos) block += 2;
        }
    }
    file.close();
    std::cout << "Problem A: " << closestApproachParticle(particles) << '\n';
    std::cout << "Problem B: " << solveCollisions(particles) << '\n';
    return EXIT_SUCCESS;
}

