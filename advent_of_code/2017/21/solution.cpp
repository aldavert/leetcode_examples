#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>

std::string filter(std::string input)
{
    std::string output;
    for (char symbol : input)
        if (symbol != '/')
            output += symbol;
    return output;
}

std::unordered_set<std::string> rotations(std::string input)
{
    std::unordered_set<std::string> result;
    int n = 2 + (input.size() == 9);
    std::string output(n * n, ' ');
    for (int mode = 0; mode < 8; ++mode)
    {
        for (int r = 0, k = 0; r < n; ++r)
        {
            for (int c = 0; c < n; ++c, ++k)
            {
                int nr = r, nc = c;
                if ((mode & 1) != 0) nr = n - 1 - nr;
                if ((mode & 2) != 0) nc = n - 1 - nc;
                if ((mode & 4) != 0) std::swap(nr, nc);
                output[k] = input[nr * n + nc];
            }
        }
        result.insert(output);
    }
    return result;
}

std::vector<std::string>
magnify(const std::unordered_map<std::string, std::string> &rules,
        std::vector<std::string> pixels)
{
    const int n_rows = static_cast<int>(pixels.size()),
              n_cols = static_cast<int>(pixels[0].size());
    if (n_rows % 2 == 0)
    {
        int new_size = 3 * n_rows / 2;
        std::vector<std::string> result(new_size, std::string(new_size, '.'));
        for (int row = 0, r = 0; row < n_rows; row += 2, r += 3)
        {
            for (int col = 0, c = 0; col < n_cols; col += 2, c += 3)
            {
                std::string code;
                for (int i = 0; i < 2; ++i)
                    for (int j = 0; j < 2; ++j)
                        code += pixels[row + i][col + j];
                std::string output = rules.at(code);
                for (int i = 0, k = 0; i < 3; ++i)
                    for (int j = 0; j < 3; ++j, ++k)
                        result[r + i][c + j] = output[k];
            }
        }
        return result;
    }
    else
    {
        int new_size = 4 * n_rows / 3;
        std::vector<std::string> result(new_size, std::string(new_size, '.'));
        for (int row = 0, r = 0; row < n_rows; row += 3, r += 4)
        {
            for (int col = 0, c = 0; col < n_cols; col += 3, c += 4)
            {
                std::string code;
                for (int i = 0; i < 3; ++i)
                    for (int j = 0; j < 3; ++j)
                        code += pixels[row + i][col + j];
                std::string output = rules.at(code);
                for (int i = 0, k = 0; i < 4; ++i)
                    for (int j = 0; j < 4; ++j, ++k)
                        result[r + i][c + j] = output[k];
            }
        }
        return result;
    }
}

int main(int argc, char * * argv)
{
    std::vector<std::string> pattern;
    pattern.push_back(".#.");
    pattern.push_back("..#");
    pattern.push_back("###");
    
    std::unordered_map<std::string, std::string> rules;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        auto split = line.find(" => ");
        std::string input = filter(line.substr(0, split)),
                    output = filter(line.substr(split + 4));
        for (std::string current : rotations(input))
            rules[current] = output;
    }
    file.close();
    std::vector<std::string> pixels = pattern;
    //int problem_a = 0, problem_b = 0;
    for (int i = 0; i < 18; ++i)
    {
        pixels = magnify(rules, pixels);
        if ((i != 4) && (i != 17)) continue;
        int count = 0;
        for (auto row : pixels)
            for (char symbol : row)
                count += symbol == '#';
        std::cout << "Problem " << static_cast<char>('A' + (i == 17))
                  << ": " << count << '\n';
    }
    return EXIT_SUCCESS;
}

