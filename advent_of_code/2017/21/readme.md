# [Day 21: Fractal Art](https://adventofcode.com/2017/day/21)

#### Problem A

You have an initial `3 * 3` image:
```
.#.
..#
###
```

You are given a set of rules that define how to increase the size of the image:
- If the image is divisible by `2`, break it into `2 * 2` blocks and apply the `2 * 2 -> 3 * 3` magnification rules.
- Otherwise, your image is divisible by `3`, so break it into `3 * 3` blocks and apply the `3 * 3 -> 4 * 4` magnification rules.

The given magnification rules, apply to the input pattern and to all the rotated and flipped versions of the input pattern (the output is not modified).

How many pixels are `#` after 5 magnification iterations?

#### Problem B

How many pixels are `#` after 18 magnification iterations?

