# [Day 17: Spinlock](https://adventofcode.com/2017/day/17)

#### Problem A

You have a circular queue with `0` inside and a **step** value `n` *(your puzzle input)*. The queue has a pointer that indicates which is the current active value. You can move this pointer forward and you can insert elements to the *right* of the pointer position. Once you insert an element, the pointer will point to this new number.

If you insert `2017` elements to the queue, what will be the number after the last inserted value?

#### Problem B

If you insert `50'000'000` elements in the queue, what is the number after `0`?

