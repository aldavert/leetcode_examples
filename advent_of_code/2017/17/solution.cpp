#include <iostream>
#include <vector>

int main(int argc, char * * argv)
{
    int step = 354;
    if (argc > 1) step = std::stoi(argv[1]);
    std::vector<int> next = {0}, value = {0};
    int current = 0;
    for (int i = 1; i <= 2017; ++i)
    {
        for (int j = 0; j < step; ++j)
            current = next[current];
        next.push_back(next[current]);
        value.push_back(i);
        next[current] = static_cast<int>(next.size() - 1);
        current = static_cast<int>(next.size() - 1);
    }
    std::cout << "Problem A: " << value[next[current]] << '\n';
    int value_after = -1;
    for (int i = 1, position = 0; i <= 50'000'000; ++i)
    {
        position = (position + step) % i + 1;
        if (position == 1) value_after = i;
    }
    std::cout << "Problem B: " << value_after << '\n';
}

