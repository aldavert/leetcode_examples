# [Day 16: Permutation Promenade](https://adventofcode.com/2017/day/16)

#### Problem A

You have a string of sixteen letters that go from `a` to `p`. You are given a string with multiple manipulations commands that modify the order of the characters of of the string:
- `sX`, moves the position `X` to the front of the string but maintains the order. For example, applying `s3` on `abcde` results in `cdeab`.
- `xA/B`, exchanges the characters is positions `A` and `B`. For example, applying `x3/4` on `abcde` results in `abced`.
- `pA/B`, swaps the characters `A` with the character `B`. For example, applying `pe/b` on `abcde` results in `aecdb`.

What is the resulting string after all the manipulation commands have been applied?

#### Problem B

If you apply all permutations `1'000'000'000` times, what is the resulting string?

