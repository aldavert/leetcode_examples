#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdexcept>
#include <unordered_map>

enum class ID { SPIN, EXCHANGE, PARTNER };

struct Command
{
    Command(void) = default;
    Command(std::string text)
    {
        if (text[0] == 's')
        {
            id = ID::SPIN;
            char_a = char_b = '\0';
            pos_a = std::stoi(text.substr(1));
            pos_b = -1;
        }
        else if (text[0] == 'x')
        {
            id = ID::EXCHANGE;
            char_a = char_b = '\0';
            auto slash = text.find('/');
            pos_a = std::stoi(text.substr(1, slash - 1));
            pos_b = std::stoi(text.substr(slash + 1));
        }
        else if (text[0] == 'p')
        {
            id = ID::PARTNER;
            pos_a = pos_b = -1;
            char_a = text[1];
            char_b = text[3];
            if (text.size() != 4) throw std::invalid_argument("Wrong instruction.");
        }
        else throw std::invalid_argument("Wrong instruction.");
    }
    void modify(std::string &text) const
    {
        if (id == ID::SPIN)
        {
            text = text.substr(text.size() - pos_a)
                 + text.substr(0, text.size() - pos_a);
        }
        else if (id == ID::EXCHANGE)
        {
            std::swap(text[pos_a], text[pos_b]);
        }
        else if (id == ID::PARTNER)
        {
            auto idx_a = text.find(char_a),
                 idx_b = text.find(char_b);
            if ((idx_a == std::string::npos) || (idx_b == std::string::npos))
                throw std::invalid_argument("Wrong instruction.\n");
            std::swap(text[idx_a], text[idx_b]);
        }
        else throw std::invalid_argument("Wrong instruction.");
    }
    ID id = ID::SPIN;
    char char_a = '\0';
    char char_b = '\0';
    int pos_a = -1;
    int pos_b = -1;
};

int main(int argc, char * * argv)
{
    std::vector<Command> commands;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        std::string::size_type start = 0, stop = line.find(',');
        while (stop != std::string::npos)
        {
            commands.push_back(Command(line.substr(start, stop - start)));
            start = stop + 1;
            stop = line.find(',', start);
        }
        commands.push_back(Command(line.substr(start)));
    }
    file.close();
    std::string input = "abcde";
    std::unordered_map<std::string, int> previous;
    std::vector<std::string> history;
    previous[input] = 0;
    history.push_back(input);
    if (commands.size() > 10)
        for (char chr = 'f'; chr <= 'p'; ++chr) input += chr;
    for (auto command : commands)
        command.modify(input);
    std::cout << "Problem A: " << input << '\n';
    previous[input] = 1;
    history.push_back(input);
    
    constexpr int total_permuntations = 1'000'000'000;
    for (int i = 1; i < total_permuntations; ++i)
    {
        for (auto command : commands)
            command.modify(input);
        if (auto search = previous.find(input); search != previous.end())
        {
            input = history[search->second + (total_permuntations - search->second) % i];
            break;
        }
        previous[input] = i;
        history.push_back(input);
    }
    std::cout << "Problem B: " << input << '\n';
    return EXIT_SUCCESS;
}

