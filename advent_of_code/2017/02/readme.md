# [Day 2: Corruption Checksum](https://adventofcode.com/2017/day/2)

#### Problem A

You are given a set of lists of integers. You need to compute the sum of the difference between the largest and smallest element of each list.

#### Problem B

Now search in each list the only pair of integers that are evenly divisible, and accumulate the result of the division.

