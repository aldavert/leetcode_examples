#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int divisible(const std::vector<int> &row)
{
    for (size_t i = 0; i < row.size(); ++i)
    {
        for (size_t j = i + 1; j < row.size(); ++j)
        {
            if ((row[i] > row[j]) && (row[i] % row[j] == 0))
                return row[i] / row[j];
            else if ((row[i] < row[j]) && (row[j] % row[i] == 0))
                return row[j] / row[i];
        }
    }
    return 0;
}

int main(int argc, char * * argv)
{
    std::vector<std::vector<int> > spreadsheet;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        spreadsheet.push_back({});
        int digit = 0, size = 0;
        for (char symbol : line)
        {
            if (std::isdigit(symbol))
            {
                digit = digit * 10 + symbol - '0';
                ++size;
            }
            else if (size != 0)
            {
                spreadsheet.back().push_back(digit);
                digit = size = 0;
            }
        }
        if (size != 0)
            spreadsheet.back().push_back(digit);
    }
    file.close();
    int problem_a = 0;
    for (const auto &row : spreadsheet)
        problem_a += *max_element(row.begin(), row.end())
                   - *min_element(row.begin(), row.end());
    std::cout << "Problem A: " << problem_a << '\n';
    int problem_b = 0;
    for (const auto &row : spreadsheet)
        problem_b += divisible(row);
    std::cout << "Problem B: " << problem_b << '\n';
    return EXIT_SUCCESS;
}

