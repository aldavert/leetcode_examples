#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int stepsOut(std::vector<int> jumps, bool threshold)
{
    const int n = static_cast<int>(jumps.size());
    int number_of_steps = 0;
    for (int position = 0; (position >= 0) && (position < n); ++number_of_steps)
    {
        int next = position + jumps[position];
        if (threshold && (jumps[position] >= 3)) --jumps[position];
        else ++jumps[position];
        position = next;
    }
    return number_of_steps;
}

int main(int argc, char * * argv)
{
    std::vector<int> jumps_offset;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        if (!line.empty())
            jumps_offset.push_back(std::stoi(line));
    file.close();
    
    std::cout << "Problem A: " << stepsOut(jumps_offset, false) << '\n';
    std::cout << "Problem B: " << stepsOut(jumps_offset, true) << '\n';
    return EXIT_SUCCESS;
}

