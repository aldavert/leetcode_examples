# [Day 5: A Maze of Twisty Trampolines, All Alike](https://adventofcode.com/2017/day/5)

#### Problem A

You are given a list of integers that represent the amount of relative positions that you have to jump at each step. Before jumping to the next position, you have to increase the value of the current position by one, so the next time you enter this position you will jump further away.

Starting at position `0`, how many steps are required to exit the list?

#### Problem B

Repeat the process but now jumps only increase the jump position when the value is smaller than 3. Otherwise, decrease the value. How many steps are required to exit the list now?


