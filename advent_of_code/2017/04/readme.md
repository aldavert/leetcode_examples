# [Day 4: High-Entropy Passphrases](https://adventofcode.com/2017/day/4)

#### Problem A

You are given a set of strings which contains words separated by spaces. How many strings do not contain repeated words?

#### Problem B

Now, repeat the search but making sure that words are not anagrams of each other. How many strings do not contains any anagram of each other string words'?

