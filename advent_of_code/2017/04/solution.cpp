#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_set>

int main(int argc, char * * argv)
{
    std::vector<std::vector<std::string> > text;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_SUCCESS;
    }
    for (std::string line; std::getline(file, line); )
    {
        text.push_back({});
        std::string current;
        for (char symbol : line)
        {
            if (symbol == ' ')
            {
                if (current.empty()) continue;
                text.back().push_back(current);
                current.clear();
            }
            else current += symbol;
        }
        if (!current.empty())
            text.back().push_back(current);
        if (text.back().empty()) text.pop_back();
    }
    file.close();
    
    int problem_a = 0, problem_b = 0;;
    for (const auto &row : text)
    {
        bool unique = true, no_anagram = true;
        std::unordered_set<std::string> single, sorted;
        for (std::string word : row)
        {
            unique = unique && (single.find(word) == single.end());
            single.insert(word);
            std::sort(word.begin(), word.end());
            no_anagram = no_anagram && (sorted.find(word) == sorted.end());
            sorted.insert(word);
        }
        problem_a += unique;
        problem_b += no_anagram;
    }
    std::cout << "Problem A: " << problem_a << '\n';
    std::cout << "Problem B: " << problem_b << '\n';
    
    return EXIT_SUCCESS;
}

