# [Day 24: Electromagnetic Moat](https://adventofcode.com/2017/day/24)

#### Problem A

You are given a set of pairs of integers. You want to create chains of integers by joining pairs that have a matching end. For example, given pairs `0|3`, `7|4` and `3|7` you can create the chain: `0|3->3|7->7|4`. The elements of the chain can be rotated if necessary. For example, given `0|1`, `10|1` and `9|10`, you can create the chain: `0|1->1|10->10|9`.

The score of a chain is computed as the sum of all integers in the chain. For example, `0|3->3|7->7|4` has a score of `24` and `0|1->1|10->10|9` has a score of `31`. What is the **maximum score** that you can achieve by joining the given set of integer pairs?

#### Problem B

Return the score of the longest chain. If there is a tie, return the highest score between the chains of the same length.

