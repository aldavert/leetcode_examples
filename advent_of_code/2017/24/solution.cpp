#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <bitset>

#define DP 0

int strongestChainSum(const std::vector<std::tuple<int, int> > &pairs)
{
    std::unordered_map<int, std::unordered_set<size_t> > number_to_pair;
    for (size_t i = 0; i < pairs.size(); ++i)
    {
        number_to_pair[std::get<0>(pairs[i])].insert(i);
        number_to_pair[std::get<1>(pairs[i])].insert(i);
    }
    std::bitset<64> available = 0xFFFFFFFFFFFFFFFF;
#if DP
    std::unordered_map<size_t, int> dp;
#endif
    auto strongestSum = [&](auto &&self, int current) -> int
    {
        if (auto search = number_to_pair.find(current); search == number_to_pair.end())
            return current;
        else
        {
#if DP
            size_t id = (std::hash<int>{}(current) << 1)
                      ^ std::hash<std::bitset<64> >{}(available);
            if (auto s = dp.find(id); s != dp.end())
                return s->second;
#endif
            int result = 0, counter = 0;
            for (size_t i : search->second)
            {
                if (!available[i]) continue;
                ++counter;
                available[i] = false;
                int next = (std::get<0>(pairs[i]) == current)
                         ? std::get<1>(pairs[i])
                         : std::get<0>(pairs[i]);
                result = std::max(result, self(self, next));
                available[i] = true;
            }
#if DP
            return dp[id] = (counter > 0)?(2 * current + result):current;
#else
            return (counter > 0)?(2 * current + result):current;
#endif
        }
    };
    return strongestSum(strongestSum, 0);
}

int longestChain(const std::vector<std::tuple<int, int> > pairs)
{
    std::unordered_map<int, std::unordered_set<size_t> > number_to_pair;
    for (size_t i = 0; i < pairs.size(); ++i)
    {
        number_to_pair[std::get<0>(pairs[i])].insert(i);
        number_to_pair[std::get<1>(pairs[i])].insert(i);
    }
    std::bitset<64> available = 0xFFFFFFFFFFFFFFFF;
    auto strongestSum = [&](auto &&self, int current) -> std::pair<int, int>
    {
        if (auto search = number_to_pair.find(current); search == number_to_pair.end())
            return {1, current};
        else
        {
            int length = 0, strength = 0;
            for (size_t i : search->second)
            {
                if (!available[i]) continue;
                available[i] = false;
                int next = (std::get<0>(pairs[i]) == current)
                         ? std::get<1>(pairs[i])
                         : std::get<0>(pairs[i]);
                auto [cur_length, cur_strength] = self(self, next);
                if (cur_length > length)
                {
                    length = cur_length;
                    strength = cur_strength;
                }
                else if ((cur_length == length) && (cur_strength > strength))
                    strength = cur_strength;
                available[i] = true;
            }
            return (length > 0)
                 ?std::make_pair(length + 1, 2 * current + strength)
                 :std::make_pair(1, current);
        }
    };
    return std::get<1>(strongestSum(strongestSum, 0));
}

int main(int argc, char * * argv)
{
    std::vector<std::tuple<int, int> > pairs;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        auto separator = line.find('/');
        pairs.push_back({std::stoi(line.substr(0, separator)),
                         std::stoi(line.substr(separator + 1))});
    }
    file.close();
    std::cout << "Problem A: " << strongestChainSum(pairs) << '\n';
    std::cout << "Problem B: " << longestChain(pairs) << '\n';
    return EXIT_SUCCESS;
}

