#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <unordered_set>
#include <queue>

int main(int argc, char * * argv)
{
    std::vector<std::unordered_set<size_t> > graph;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.size() < 4) continue;
        std::string separator;
        size_t node = 0;
        std::istringstream iss(line);
        iss >> node;
        while (graph.size() < node + 1) graph.push_back({});
        iss >> separator;
        if (separator != "<->")
        {
            std::cerr << "[ERROR] Malformed line: " << line << '\n';
            return EXIT_FAILURE;
        }
        for (std::string token; iss >> token; )
        {
            if (token.back() == ',') token.pop_back();
            graph[node].insert(std::stoi(token));
        }
    }
    file.close();
    std::vector<bool> visited(graph.size(), false);
    std::vector<int> group_size(graph.size());
    size_t number_of_groups = 0;
    for (size_t init = 0; init < graph.size(); ++init)
    {
        if (visited[init]) continue;
        std::queue<size_t> active;
        active.push(init);
        ++number_of_groups;
        while (!active.empty())
        {
            size_t node = active.front();
            active.pop();
            if (visited[node]) continue;
            ++group_size[init];
            visited[node] = true;
            for (size_t neighbor : graph[node])
                active.push(neighbor);
        }
    }
    std::cout << "Problem A: " << group_size[0] << '\n';
    std::cout << "Problem B: " << number_of_groups << '\n';
    
    return EXIT_SUCCESS;
}

