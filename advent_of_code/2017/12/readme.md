# [Day 12: Digital Plumber](https://adventofcode.com/2017/day/12)

#### Problem A

You are given a list of relationships between the nodes of a graph. Since the graph may be disconnected, you want to know how many nodes are connected to node `0`.

#### Problem B

How many fully connected sub-graphs are in the disconnected graph?


