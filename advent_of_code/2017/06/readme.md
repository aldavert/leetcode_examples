# [Day 6: Memory Reallocation](https://adventofcode.com/2017/day/6)

#### Problem A

You have a set of bins, where at each bin you have an amount of objects. You have to search the bin with the largest amount of objects, take all them out and start to redistribute them into the rest bins.

The redistribution is done by moving right to the next bin and, putting one of the pulled objects in the bin. Keep doing this until all the objects have been redistributed. While redistribution the objects, once you reach the last been and try to move right, you actually move to the first bin.

If you keep repeating this process, eventually, you will find a duplicated configuration. How many redistributions have to be done before obtaining a previously seen distribution?

#### Problem B

What is the distance between the two instances of the repeated distribution?

