#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::vector<T> &vec)
{
    out << '{';
    for (bool next = false; const auto &value : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << value;
    }
    out << '}';
    return out;
}

std::tuple<size_t, size_t> redistributeCycle(std::vector<int> memory_banks)
{
    std::map<std::vector<int>, size_t> seen;
    seen[memory_banks] = 0;
    while (true)
    {
        int max_value = memory_banks[0];
        size_t max_index = 0;
        for (size_t i = 1; i < memory_banks.size(); ++i)
        {
            if (memory_banks[i] > max_value)
            {
                max_value = memory_banks[i];
                max_index = i;
            }
        }
        memory_banks[max_index] = 0;
        while (max_value > 0)
        {
            ++max_index;
            if (max_index >= memory_banks.size()) max_index = 0;
            ++memory_banks[max_index];
            --max_value;
        }
        if (auto search = seen.find(memory_banks); search == seen.end())
            seen[memory_banks] = seen.size();
        else return { seen.size(), seen.size() - search->second };
    }
    return {0, 0};
}

int main(int argc, char * * argv)
{
    std::vector<int> memory_banks;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (int value = 1; file >> value; )
        memory_banks.push_back(value);
    file.close();
    auto [number_of_distributions, cycle_size] = redistributeCycle(memory_banks);
    std::cout << "Problem A: " << number_of_distributions << '\n';
    std::cout << "Problem B: " << cycle_size << '\n';
    return EXIT_SUCCESS;
}

