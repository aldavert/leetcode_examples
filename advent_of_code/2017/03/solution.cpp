#include <iostream>
#include <string>
#include <cmath>
#include <vector>
#include <fmt/core.h>

int minDistance(int query)
{
    if (query <= 1) return 0;
    //k   : 0  1    2    3    4    5    6    7    8
    //f(k): 1  (8k) (8k) (8k) (8k) (8k) (8k) (8k) (8k)
    //#cur: 1  8    16   24   32   40   48   56   64
    //#tot: 1  9    25   49   81   121  169  225  289
    //    : 1  9    25   49   81   121  169  225  289
    //total = 1 + 8(k * (k + 1) / 2 = 1 + 4 * (k * (k + 1))
    //k = (-1 +- sqrt(1 + 4 * 1 * (total - 1) / 4)) / 2
    int k = (int)std::ceil((std::sqrt(static_cast<double>(query)) - 1) / 2);
    int previous = 1 + 4 * (k * (k - 1));
    int x = k, y = k - 1;
    query -= previous + 1;
    if (query < 2 * k)
        return std::abs(x) + std::abs(y - query);
    else
    {
        query -= 2 * k;
        y -= 2 * k - 1;
        --x;
    }
    if (query < 2 * k)
        return std::abs(x - query) + std::abs(y);
    else
    {
        query -= 2 * k;
        x -= 2 * k - 1;
        ++y;
    }
    if (query < 2 * k)
        return std::abs(x) + std::abs(y + query);
    else
    {
        query -= 2 * k;
        y += 2 * k - 1;
        ++x;
    }
    return std::abs(x + query) + std::abs(y);
}

int positionSum(int query)
{
    constexpr int grid_size = 11;//1'001;
    std::vector<std::vector<int> > grid(grid_size, std::vector<int>(grid_size, 0));
    int x = grid_size / 2, y = grid_size / 2;
    grid[y][x] = 1;
    for (int idx = 1, ring = 0, total = 1; grid[y][x] < query; )
    {
        if (idx == total)
        {
            ++x;
            ++ring;
            total = 8 * ring - 1;
            idx = 0;
        }
        else
        {
            ++idx;
            if (idx < 2 * ring)
                --y;
            else if (idx < 4 * ring)
                --x;
            else if (idx < 6 * ring)
                ++y;
            else ++x;
        }
        int current = 0;
        for (int cy = y - 1; cy <= y + 1; ++cy)
            for (int cx = x - 1; cx <= x + 1; ++cx)
                current += grid[cy][cx];
        grid[y][x] = current;
    }
    return grid[y][x];
}

int main(int argc, char * * argv)
{
    int query = 361527;
    if (argc > 1) query = std::stoi(argv[1]);
    std::cout << "Query: " << query << '\n';
    std::cout << "Problem A: " << minDistance(query) << '\n';
    std::cout << "Problem B: " << positionSum(query) << '\n';
}

