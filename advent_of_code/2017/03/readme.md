# [Day 3: Spiral Memory](https://adventofcode.com/2017/day/3)

#### Problem A

You have data stored in a spiral form:
```
17  16  15  14  13
18   5   4   3  12
19   6   1   2  11
20   7   8   9  10
21  22  23 --> ...
```

In order to access any memory position, you start at position `1` and you can move `up`, `down`, `left` and `right` following the shortest path to reach the desired memory location. So, the number of steps (distance) to reach a position is the Manhattan distance between position `1` and the queried position.

How many steps are required to access the data position queried in the problem?


