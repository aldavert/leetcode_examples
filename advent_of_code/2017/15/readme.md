# [Day 15: Dueling Generators](https://adventofcode.com/2017/day/15)

#### Problem A

You generate a sequence by multiplying the previous value by a **factor** and computing the modulo of the result so the value does not exceed `2147483647`. You have two generators, **generator A** uses the factor `16807` while **generator B** uses the factor `48271`. Your puzzle input is the generators' initial value.

You are using the generators to create a pairs of integers. If you generate `40'000'000` pairs, how many numbers have the same `16` less significant bits?

#### Problem B

Now, your generators only return a value when values are multiples of `4` (for **generator A**) and multiples of `8` (for **generator B**). How many numbers have the same `16` less significant bits when you generate `5'000'000` pairs?

