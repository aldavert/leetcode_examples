#include <iostream>
#include <fstream>
#include <vector>
#include <string>

int main(int argc, char * * argv)
{
    std::string filename = "data.txt";
    int init_a = -1, init_b = -1;
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    std::string generator, generator_id, starts, with;
    file >> generator >> generator_id >> starts >> with >> init_a;
    file >> generator >> generator_id >> starts >> with >> init_b;
    file.close();
    int problem_a = 0;
    for (size_t i = 0, value_a = init_a, value_b = init_b; i < 40'000'000; ++i)
    {
        value_a = (value_a * 16807) % 2147483647;
        value_b = (value_b * 48271) % 2147483647;
        problem_a += std::countr_zero(value_a ^ value_b) >= 16;
    }
    std::cout << "Problem A: " << problem_a << '\n';
    int problem_b = 0;
    for (size_t i = 0, value_a = init_a, value_b = init_b; i < 5'000'000; ++i)
    {
        do
        {
            value_a = (value_a * 16807) % 2147483647;
        } while (value_a % 4 != 0);
        do
        {
            value_b = (value_b * 48271) % 2147483647;
        } while (value_b % 8 != 0);
        problem_b += std::countr_zero(value_a ^ value_b) >= 16;
    }
    std::cout << "Problem B: " << problem_b << '\n';
    return EXIT_SUCCESS;
}

