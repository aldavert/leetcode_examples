# [Day 23: Coprocessor Conflagration](https://adventofcode.com/2017/day/23)

#### Problem A

You are given a custom CPU with the following instructions:
- `set X Y` **sets** register `X` to the value of `Y`.
- `sub X Y` **decreases** register `X` by the value of `Y`.
- `mul X Y` sets register `X` to the value of **multiplying** the value contained in register `X` by the value of `Y`.
- `jnz X Y` **jumps** with an offset of value `Y` when the value of `X` is **not zero**.

The custom CPU has `8` registers, from `a` to `h`, which all start at `0`. The programs run until the program counter falls outside the program memory.

If you run the given program, **how many times is the** `mul` **instruction invoked?**

#### Problem B

If you set register `a` to `1` and execute the code, what is the result stored in register `h` after the program is executed.

**Note:** You are supposed to figure out what the program is doing. The program will not execute in a reasonable time if you run it in your emulator.

