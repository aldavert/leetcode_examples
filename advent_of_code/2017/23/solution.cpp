#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <stdexcept>

enum class ID { SET = 0, SUB, MUL, JNZ };

struct Instruction
{
    Instruction(void) = default;
    Instruction(ID op, std::string first, std::string second) : id(op)
    {
        auto isRegister = [](std::string text) -> bool
        {
            return (text.size() == 1) && (text[0] >= 'a') && (text[0] <= 'h');
        };
        if (isRegister(first))
        {
            reg_name[0] = first[0];
            value[0] = 0;
            is_value[0] = false;
        }
        else
        {
            if (op != ID::JNZ)
                throw std::invalid_argument("Only 'JNZ' can have a value as the "
                                            "first operand.");
            reg_name[0] = '\0';
            value[0] = std::stol(first);
            is_value[0] = true;
        }
        if (isRegister(second))
        {
            reg_name[1] = second[0];
            value[1] = 0;
            is_value[1] = false;
        }
        else
        {
            reg_name[1] = '\0';
            value[1] = std::stol(second);
            is_value[1] = true;
        }
    }
    ID id = ID::SET;
    char reg_name[2] = {'\0', '\0'};
    bool is_value[2] = {false, false};
    long value[2] = {0, 0};
};

std::ostream& operator<<(std::ostream &out, const Instruction &inst)
{
    if      (inst.id == ID::SET) out << "set";
    else if (inst.id == ID::SUB) out << "sub";
    else if (inst.id == ID::MUL) out << "mul";
    else if (inst.id == ID::JNZ) out << "jnz";
    else out << "UNKNONW";
    if (inst.is_value[0]) out << ' ' << inst.value[0];
    else out << ' ' << inst.reg_name[0];
    if (inst.is_value[1]) out << ' ' << inst.value[1];
    else out << ' ' << inst.reg_name[1];
    return out;
}

int countMUL(const std::vector<Instruction> &code)
{
    const int n = static_cast<int>(code.size());
    long registers[8] = {}, pc = 0;
    auto value = [&](int reg_id) -> long
    {
        if (code[pc].is_value[reg_id]) return code[pc].value[reg_id];
        else return registers[code[pc].reg_name[reg_id] - 'a'];
    };
    int result = 0;
    while ((pc >= 0) && (pc < n))
    {
        switch (code[pc].id)
        {
        case ID::SET:
            registers[code[pc].reg_name[0] - 'a'] = value(1);
            ++pc;
            break;
        case ID::SUB:
            registers[code[pc].reg_name[0] - 'a'] -= value(1);
            ++pc;
            break;
        case ID::MUL:
            registers[code[pc].reg_name[0] - 'a'] *= value(1);
            ++pc;
            ++result;
            break;
        case ID::JNZ:
            if (value(0) != 0) pc += value(1);
            else ++pc;
            break;
        default:
            throw std::invalid_argument("Unknown instruction.");
        }
    }
    return result;
}

long resultH(void)
{
    std::vector<bool> sieve(200'000, true);
    sieve[0] = sieve[1] = false;
    for (size_t i = 2; i < sieve.size(); ++i)
    {
        if (!sieve[i]) continue;
        for (size_t j = i * i; j < sieve.size(); j += i)
            sieve[j] = false;
    }
    long result = 0;
    for (size_t b = 109'900; b <= 126'900; b += 17)
        result += !sieve[b];
    return result;
#if 0
    // CLEAN CODE:
    // -------------------------------------------------------------------------
    long b = 9'900 + 100'000;
    long c = b + 17'000;
    long h = 0;
    for (; b != c; b += 17)
    {
        long f = 1;
        for (long d = 2; d != b; ++d)
            for (long e = 2; e != b; ++e)
                if (d * e == b)
                    f = 0;
        if (f == 0) ++h;
    }
    return h;
#elif 0
    // ASSEMBLY TO C
    // -------------------------------------------------------------------------
    long b = 9'900 + 100'000;
    long c = b + 17'000;
    long h = 0;
    while (true)
    {
        long f = 1;
        long d = 2;
        do
        {
            long e = 2;
            do
            {
                if (d * e == b) f = 0;
                ++e;
            }
            while (e != b);
            ++d;
        }
        while (d - b != 0);
        if (f == 0) ++h;
        if (b - c == 0) return h;
        b += 17;
    }
#endif
}

long resultH(const std::vector<Instruction> &code)
{
    const int n = static_cast<int>(code.size());
    long registers[8] = {}, pc = 0;
    registers[0] = 1;
    auto value = [&](int reg_id) -> long
    {
        if (code[pc].is_value[reg_id]) return code[pc].value[reg_id];
        else return registers[code[pc].reg_name[reg_id] - 'a'];
    };
    while ((pc >= 0) && (pc < n))
    {
        switch (code[pc].id)
        {
        case ID::SET:
            registers[code[pc].reg_name[0] - 'a'] = value(1);
            ++pc;
            break;
        case ID::SUB:
            registers[code[pc].reg_name[0] - 'a'] -= value(1);
            ++pc;
            break;
        case ID::MUL:
            registers[code[pc].reg_name[0] - 'a'] *= value(1);
            ++pc;
            break;
        case ID::JNZ:
            if (value(0) != 0) pc += value(1);
            else ++pc;
            break;
        default:
            throw std::invalid_argument("Unknown instruction.");
        }
    }
    return registers[7];
}

int main(int argc, char * * argv)
{
    std::string filename = "data.txt";
    std::vector<Instruction> code;
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        std::istringstream iss(line);
        std::string token, reg_a, reg_b;
        iss >> token >> reg_a >> reg_b;
        if (token == "set")
            code.push_back({ID::SET, reg_a, reg_b});
        else if (token == "sub")
            code.push_back({ID::SUB, reg_a, reg_b});
        else if (token == "mul")
            code.push_back({ID::MUL, reg_a, reg_b});
        else if (token == "jnz")
            code.push_back({ID::JNZ, reg_a, reg_b});
        else
        {
            std::cerr << "[ERROR] Unknown instruction: " << line << '\n';
            return EXIT_FAILURE;
        }
    }
    file.close();
    std::cout << "Problem A: " << countMUL(code) << '\n';
    std::cout << "Problem B: " << resultH() << '\n';
    return EXIT_SUCCESS;
}

