#include <iostream>
#include <fstream>
#include <vector>
#include <string>

int problemA(int list_size, std::vector<int> substring_length)
{
    std::vector<int> values(list_size);
    for (int i = 0; i < list_size; ++i) values[i] = i;
    int position = 0, skip = 0;
    for (int length : substring_length)
    {
        for (int i = 0, j = length - 1; i < j; ++i, --j)
            std::swap(values[(position + i) % list_size],
                      values[(position + j) % list_size]);
        position = (position + length + skip) % list_size;
        ++skip;
    }
    return values[0] * values[1];
}

std::string problemB(std::vector<int> substring_length)
{
    constexpr int list_size = 256;
    constexpr char hexadecimal[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8',
        '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    std::vector<int> values(list_size);
    for (int i = 0; i < list_size; ++i) values[i] = i;
    int position = 0, skip = 0;
    for (int repetition = 0; repetition < 64; ++repetition)
    {
        for (int length : substring_length)
        {
            for (int i = 0, j = length - 1; i < j; ++i, --j)
                std::swap(values[(position + i) % list_size],
                          values[(position + j) % list_size]);
            position = (position + length + skip) % list_size;
            ++skip;
        }
    }
    std::string result;
    for (int i = 0; i < list_size; i += 16)
    {
        int value = 0;
        for (int j = i; j < i + 16; ++j)
            value = value ^ values[j];
        result += hexadecimal[0x0f & (value >> 4)];
        result += hexadecimal[0x0f & (value     )];
    }
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<int> substring_length, substring_length_hash;
    int list_size = 256;
    std::string data;
    std::string filename = "data.txt";
    if (argc > 2)
    {
        list_size = std::stoi(argv[1]);
        filename = argv[2];
    }
    else if (argc > 1)
        data = argv[1];
    if (data.empty())
    {
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
            return EXIT_FAILURE;
        }
        for (std::string line; std::getline(file, line); )
            data += line;
        file.close();
        int value = 0;
        bool value_active = false;
        for (char symbol : data)
        {
            if (std::isdigit(symbol))
            {
                value = value * 10 + (symbol - '0');
                value_active = true;
            }
            else if (value_active)
            {
                value_active = false;
                substring_length.push_back(value);
                value = 0;
            }
            substring_length_hash.push_back(static_cast<int>(symbol));
        }
        if (value_active)
            substring_length.push_back(value);
    }
    else
    {
        for (char symbol : data)
            substring_length_hash.push_back(static_cast<int>(symbol));
    }
    substring_length_hash.push_back(17);
    substring_length_hash.push_back(31);
    substring_length_hash.push_back(73);
    substring_length_hash.push_back(47);
    substring_length_hash.push_back(23);
    
    std::cout << "Problem A: " << problemA(list_size, substring_length) << '\n';
    std::cout << "Problem B: " << problemB(substring_length_hash) << '\n';
    
    return EXIT_SUCCESS;
}

