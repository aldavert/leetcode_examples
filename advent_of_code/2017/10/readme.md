# [Day 10: Knot Hash](https://adventofcode.com/2017/day/10)

#### Problem A

You have a circular list of numbers that go from 0 to 255. You are also given a list of lengths which denote the size of the selected sub-lists. Finally, you have a `skip` counter that starts at `0` and is increased by one each time a sub-list is processed.

You start at position `0`. Then, you select the elements of the sub-list indicated by the current sub-list length starting at your position and reverse its order. The you move forward the sub-list length plus the skip value.

Once you have processed all the sub-list lengths, what is the product between the first to elements of the list.

#### Problem B

Instead of parsing the list of integers given as the problem input, you obtain the sub-list lengths as the value of the different characters of the list. For example, if your input is `1,2,3` the lengths are `49, 44, 50, 44, 51`. Additionally, add at the end of the list the following lengths: `17, 31, 73, 47, 23`.

Now, instead of running a single run, repeat the rearranging process on the list of integers `64` time. The current position and `skip` value are preserved between round.

Then, compress the elements of the list of integers from `256` to `16` by dividing the list in `16` equally sized blocks and performing an XOR between all the elements within the block.

Finally, convert the `16` digits into hexadecimal, resulting in a hexadecimal string of `32` digits.

What is the hashing value of the puzzle input?

