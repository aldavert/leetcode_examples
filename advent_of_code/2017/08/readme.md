# [Day 8: I Heard You Like Registers](https://adventofcode.com/2017/day/8)

#### Problem A

You are given a list of conditional instructions that increase or decrease the value of a register when the condition is meet. The instructions have the format: `<register> (inc/dec) <value> if <register> <condition> <value>`, where `<value>` is an integer and `<conditions>` are one of the C/C++ comparison operators (`<`, `>`, `!=`, `==`, `>=` and `<=`). You don't know the amount of registers present in your CPU, so you don't know which register names you can expect.

Once you have executed all instructions, what is the value of the largest in any register?

#### Problem B

What is the highest value in any register during the execution of the program?

