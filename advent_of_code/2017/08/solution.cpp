#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>
#include <limits>

enum class CMP { EQUAL, DIFFERENT, LESS_THAN, MORE_THAN, LESS_EQUAL, MORE_EQUAL };

bool evalCondition(int left_op, CMP operand, int right_op)
{
    switch (operand)
    {
    case CMP::EQUAL:
        return left_op == right_op;
    case CMP::DIFFERENT:
        return left_op != right_op;
    case CMP::LESS_THAN:
        return left_op < right_op;
    case CMP::MORE_THAN:
        return left_op > right_op;
    case CMP::LESS_EQUAL:
        return left_op <= right_op;
    case CMP::MORE_EQUAL:
        return left_op >= right_op;
    default:
        return false;
    }
    return false;
}

struct Instruction
{
    std::string register_name;
    bool increase = false;
    int value = 0;
    std::string condition_register;
    CMP operand = CMP::EQUAL;
    int condition_value = 0;
};

std::ostream& operator<<(std::ostream &out, const Instruction &inst)
{
    const std::unordered_map<CMP, std::string> operands = {
        {CMP::EQUAL, "=="}, {CMP::DIFFERENT, "!="},
        {CMP::LESS_THAN, "<"}, {CMP::MORE_THAN, ">"},
        {CMP::LESS_EQUAL, "<="}, {CMP::MORE_EQUAL, ">="}
    };
    out << inst.register_name << ' ' << ((inst.increase)?"inc":"dec") << ' '
        << inst.value << " if " << inst.condition_register << ' '
        << operands.at(inst.operand) << ' ' << inst.condition_value;
    return out;
}

int findHighestRegister(const std::vector<Instruction> &code, bool ever)
{
    std::unordered_map<std::string, int> registers;
    int max_value = std::numeric_limits<int>::lowest();
    for (const auto &inst : code)
    {
        if (evalCondition(registers[inst.condition_register],
                          inst.operand,
                          inst.condition_value))
        {
            registers[inst.register_name] += (2 * inst.increase - 1) * inst.value;
            if (ever)
                max_value = std::max(max_value, registers[inst.register_name]);
        }
    }
    if (!ever)
        for (const auto &[register_name, register_value] : registers)
            max_value = std::max(max_value, register_value);
    return max_value;
}

int main(int argc, char * * argv)
{
    const std::unordered_map<std::string, CMP> operands = {
        {"==", CMP::EQUAL}, {"!=", CMP::DIFFERENT},
        {"<", CMP::LESS_THAN}, {">", CMP::MORE_THAN},
        {"<=", CMP::LESS_EQUAL}, {">=", CMP::MORE_EQUAL}
    };
    std::vector<Instruction> code;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        Instruction current;
        auto end_it = line.find(' ');
        current.register_name = line.substr(0, end_it);
        auto start_it = end_it + 1;
        end_it = line.find(' ', start_it + 1);
        current.increase = line.substr(start_it, end_it - start_it) == "inc";
        start_it = end_it + 1;
        end_it = line.find(' ', start_it + 1);
        current.value = std::stoi(line.substr(start_it, end_it - start_it));
        start_it = end_it + 4;
        end_it = line.find(' ', start_it + 1);
        current.condition_register = line.substr(start_it, end_it - start_it);
        start_it = end_it + 1;
        end_it = line.find(' ', start_it + 1);
        current.operand = operands.at(line.substr(start_it, end_it - start_it));
        start_it = end_it + 1;
        current.condition_value = std::stoi(line.substr(start_it));
        code.push_back(current);
    }
    file.close();
    
    std::cout << "Problem A: " << findHighestRegister(code, false) << '\n';
    std::cout << "Problem B: " << findHighestRegister(code, true) << '\n';
    
    return EXIT_SUCCESS;
}

