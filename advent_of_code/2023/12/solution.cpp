#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>
#include "utils/utils.hpp"

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

struct Problem
{
    std::string input;
    std::vector<int> damaged;
};

long numberArrangements(const std::vector<Problem> &problems)
{
    long result = 0;
    #pragma omp parallel for reduction(+:result)
    for (auto [partial, groups] : problems)
    {
        std::vector<std::unordered_map<size_t, long> > map_results(partial.size());
        const size_t n = partial.size();
        std::function<long(size_t)> recursive = [&](size_t idx) -> long
        {
            while ((idx < n) && (partial[idx] != '?')) ++idx;
            size_t k = 0;
            if (idx == n)
            {
                int count = 0;
                char previous = '.';
                for (size_t i = 0; i < n; ++i)
                {
                    if (partial[i] == '#')
                        ++count;
                    else if (previous == '#')
                    {
                        if ((k >= groups.size()) || (count != groups[k]))
                            return 0;
                        count = 0;
                        ++k;
                    }
                    previous = partial[i];
                }
                if (partial.back() == '#')
                {
                    if ((k + 1 != groups.size()) || (count != groups.back()))
                        return 0;
                }
                else if (k != groups.size()) return 0;
                return 1;
            }
            else if (idx > 0)
            {
                int count = 0;
                char previous = '.';
                for (size_t i = 0; i < idx; ++i)
                {
                    if (partial[i] == '#')
                        ++count;
                    else if (previous == '#')
                    {
                        if ((k >= groups.size()) || (count != groups[k]))
                            return 0;
                        count = 0;
                        ++k;
                    }
                    previous = partial[i];
                }
                if (partial[idx - 1] == '#')
                {
                    if ((k + 1 > groups.size()) || (count > groups[k]))
                        return 0;
                }
                else
                {
                    auto search = map_results[idx].find(k);
                    if (search != map_results[idx].end())
                        return search->second;
                }
            }
            long r = 0;
            partial[idx] = '.';
            r += recursive(idx);
            partial[idx] = '#';
            r += recursive(idx);
            partial[idx] = '?';
            map_results[idx][k] = r;
            return r;
        };
        result += recursive(0);
    }
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<Problem> problemsA, problemsB;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                auto data = utls::split(line);
                problemsA.push_back({data[0], utls::map([](auto x) -> int { return std::atoi(x.c_str()); }, utls::split(data[1], {","}))});
                auto [partial, blocks] = problemsA.back();
                for (size_t i = 1; i < 5; ++i)
                {
                    partial += "?" + problemsA.back().input;
                    for (int value : problemsA.back().damaged)
                        blocks.push_back(value);
                }
                problemsB.push_back({partial, blocks});
                
            }
        }
        file.close();
    }
    // -[ End File Parser ]-----------------------------------------------------
    long resultA = numberArrangements(problemsA),
         resultB = numberArrangements(problemsB);
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

