#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>
#include "utils/utils.hpp"

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

struct Problem
{
    std::string input;
    std::vector<int> damaged;
};

long numberArrangements(const std::vector<Problem> &problems)
{
    std::vector<int> partial_groups;
    size_t max_partial_size = 0, max_damaged_size = 0;
    for (const auto &[partial, groups] : problems)
    {
        max_damaged_size = std::max(max_damaged_size, groups.size());
        max_partial_size = std::max(max_partial_size, partial.size());
    }
    partial_groups.reserve(max_damaged_size + 1);
    std::vector<std::unordered_map<long, long> > map_results(max_partial_size + 1);
    auto signature = [](const std::vector<int> &group) -> long
    {
        return (static_cast<long>(group.size()) << 31)
             | ((group.size() > 0)?static_cast<long>(group.back()):0);
    };
    long result = 0;
    #pragma omp parallel for reduction(+:result) firstprivate(partial_groups,map_results)
    for (auto [partial, groups] : problems)
    {
        for (size_t i = 0; i <= partial.size(); ++i)
            map_results[i].clear();
        const size_t n = partial.size();
        std::function<long(size_t)> recursive = [&](size_t idx) -> long
        {
            if (idx == n) return groups == partial_groups;
            if (partial[idx] == '?')
            {
                long valid = 0;
                partial[idx] = '.';
                valid += recursive(idx);
                partial[idx] = '#';
                valid += recursive(idx);
                partial[idx] = '?';
                return valid;
            }
            else if (partial[idx] == '.')
            {
                long position_sig = signature(partial_groups);
                auto search = map_results[idx].find(position_sig);
                if (search != map_results[idx].end())
                    return search->second;
                if (partial[idx - 1] == '#')
                {
                    if (partial_groups.back() != groups[partial_groups.size() - 1])
                        return 0;
                }
                return map_results[idx][position_sig] = recursive(idx + 1);
            }
            else if (partial[idx] == '#')
            {
                if (partial[idx - 1] == '.')
                {
                    partial_groups.push_back(0);
                    if (partial_groups.size() > groups.size())
                    {
                        partial_groups.pop_back();
                        return 0;
                    }
                }
                ++partial_groups.back();
                if (partial_groups.back() > groups[partial_groups.size() - 1])
                {
                    --partial_groups.back();
                    if (partial_groups.back() == 0) partial_groups.pop_back();
                    return 0;
                }
                long valid = recursive(idx + 1);
                --partial_groups.back();
                if (partial[idx - 1] == '.')
                    partial_groups.pop_back();
                return valid;
            }
            else return 0;
        };
        
        long partial_result = 0;
        if (partial[0] == '?')
        {
            partial[0] = '.';
            partial_result += recursive(1);
            partial[0] = '#';
            partial_groups.push_back(1);
            partial_result += recursive(1);
            partial_groups.pop_back();
            partial[0] = '?';
        }
        else
        {
            if (partial[0] == '#') partial_groups.push_back(1);
            partial_result = recursive(1);
            if (partial[0] == '#') partial_groups.pop_back();
        }
        result += partial_result;
    }
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<Problem> problemsA, problemsB;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                auto data = utls::split(line);
                problemsA.push_back({data[0], utls::map([](auto x) -> int { return std::atoi(x.c_str()); }, utls::split(data[1], {","}))});
                auto [partial, blocks] = problemsA.back();
                for (size_t i = 1; i < 5; ++i)
                {
                    partial += "?" + problemsA.back().input;
                    for (int value : problemsA.back().damaged)
                        blocks.push_back(value);
                }
                problemsB.push_back({partial, blocks});
                
            }
        }
        file.close();
    }
    // -[ End File Parser ]-----------------------------------------------------
    long resultA = numberArrangements(problemsA),
         resultB = numberArrangements(problemsB);
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

