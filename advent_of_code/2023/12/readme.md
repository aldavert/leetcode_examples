# [Day 12: Hot Springs](https://adventofcode.com/2023/day/12)

You are given a string with values:
- `#` occupied
- `.` free
- `?` unknown

Followed by a list of integers corresponding to the lengths of the different contiguous blocks of occupied values. For example, the input `.??..??...?##. 1,1,3` corresponds to the string `.??..??...?##.` which has three blocks of contiguous occupied values, the first two have a single occupied block while the third one has three.

#### Problem A
Replace unknown values `?` to free `.` or occupied `#` and count how many valid combinations are possible. For example, for `.??..??...?##. 1,1,3` you can get 4 valid configurations:
- `.#...#....###.`
- `.#....#...###.`
- `..#..#....###.`
- `..#...#...###.`

Return the **sum of valid configurations of all the problems**.

#### Problem B
Same as before but now increase the difficulty of the problem by
- Concatenate five copies of the string using `?` to join each copy. For example, `.??..??...?##.` becomes `.??..??...?##.?.??..??...?##.?.??..??...?##.?.??..??...?##.?.??..??...?##.`
- Concatenate five copies of the block length list. For example the previous `1,1,3` becomes `1,1,3,1,1,3,1,1,3,1,1,3,1,1,3`.

Return the **sum of valid configurations of all the expanded problems**.

