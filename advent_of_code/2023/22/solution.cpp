#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>
#include <queue>
#include <bitset>
#include <bit>
#include "utils/utils.hpp"

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

struct Point
{
    long x = 0;
    long y = 0;
    long z = 0;
};

struct Brick
{
    Point min;
    Point max;
};

std::ostream& operator<<(std::ostream &out, const Brick &brick)
{
    out << "[(" << brick.min.x << ", " << brick.min.y << ", " << brick.min.z
        << "), (" << brick.max.x << ", " << brick.max.y << ", " << brick.max.z
        << ")]";
    return out;
}

std::vector<Brick> gravity(std::vector<Brick> bricks)
{
    std::sort(bricks.begin(), bricks.end(),
              [](const auto &left, const auto &right)
              { return left.min.z < right.min.z; });
    long height[10][10] = {};
    for (auto &brick : bricks)
    {
        long max_height = 0;
        for (long x = brick.min.x; x <= brick.max.x; ++x)
            for (long y = brick.min.y; y <= brick.max.y; ++y)
                max_height = std::max(max_height, height[x][y]);
        long dz = brick.max.z - brick.min.z;
        brick.min.z = max_height + 1;
        brick.max.z = brick.min.z + dz;
        for (long x = brick.min.x; x <= brick.max.x; ++x)
            for (long y = brick.min.y; y <= brick.max.y; ++y)
                height[x][y] = brick.max.z;
    }
    return bricks;
}

long countCanDesintegrate(std::vector<Brick> bricks)
{
    long height[10][10] = {}, label[10][10];
    for (long x = 0; x < 10; ++x)
        for (long y = 0; y < 10; ++y)
            label[x][y] = -1;
    std::vector<std::set<long> > supported_by(bricks.size()),
                                 supporting(bricks.size());
    for (long i = 0, n = static_cast<long>(bricks.size()); i < n; ++i)
    {
        for (long x = bricks[i].min.x; x <= bricks[i].max.x; ++x)
        {
            for (long y = bricks[i].min.y; y <= bricks[i].max.y; ++y)
            {
                if ((label[x][y] != -1) && (height[x][y] + 1 == bricks[i].min.z))
                {
                    supported_by[i].insert(label[x][y]);
                    supporting[label[x][y]].insert(i);
                }
                height[x][y] = bricks[i].max.z;
                label[x][y] = i;
            }
        }
    }
    long result = 0;
    for (long i = 0, n = static_cast<long>(bricks.size()); i < n; ++i)
    {
        bool can_remove = true;
        for (auto neighbor : supporting[i])
            can_remove = can_remove && (supported_by[neighbor].size() != 1);
        result += can_remove;
    }
    return result;
}

long sumDesintegrate(std::vector<Brick> bricks)
{
    long height[10][10] = {}, label[10][10];
    for (long x = 0; x < 10; ++x)
        for (long y = 0; y < 10; ++y)
            label[x][y] = -1;
    std::vector<std::set<long> > supported_by(bricks.size()),
                                 supporting(bricks.size());
    for (long i = 0, n = static_cast<long>(bricks.size()); i < n; ++i)
    {
        for (long x = bricks[i].min.x; x <= bricks[i].max.x; ++x)
        {
            for (long y = bricks[i].min.y; y <= bricks[i].max.y; ++y)
            {
                if ((label[x][y] != -1) && (height[x][y] + 1 == bricks[i].min.z))
                {
                    supported_by[i].insert(label[x][y]);
                    supporting[label[x][y]].insert(i);
                }
                height[x][y] = bricks[i].max.z;
                label[x][y] = i;
            }
        }
    }
    std::vector<bool> can_remove(bricks.size(), true);
    for (long i = static_cast<long>(bricks.size()) - 1; i >= 0; --i)
    {
        for (auto neighbor : supporting[i])
            can_remove[i] = can_remove[i] && (supported_by[neighbor].size() != 1);
        if (supported_by[i].empty())
            supported_by[i].insert(-1);
    }
    long result = 0;
    for (long i = static_cast<long>(bricks.size()) - 1; i >= 0; --i)
    {
        for (long j = static_cast<long>(bricks.size()) - 1; j > i; --j)
        {
            if (supported_by[j].find(i) != supported_by[j].end())
            {
                supported_by[j].erase(i);
                result += (!can_remove[i]) && (supported_by[j].size() == 0);
                supported_by[j].insert(supported_by[i].begin(), supported_by[i].end());
            }
        }
    }
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<Brick> bricks;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                auto coord = utls::map([](auto x) -> long { return std::atol(x.c_str()); }, utls::split(line, {"~", ","}));
                bricks.push_back({{coord[0], coord[1], coord[2]},
                                  {coord[3], coord[4], coord[5]}});
            }
        }
        file.close();
    }
    // -[ End File Parser ]-----------------------------------------------------
    long resultA = countCanDesintegrate(gravity(bricks)),
         resultB = sumDesintegrate(gravity(bricks));
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}


