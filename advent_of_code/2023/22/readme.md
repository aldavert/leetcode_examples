# [Day 22: Sand Slabs](https://adventofcode.com/2023/day/22)

You are given the coordinates of a set of blocks which are floating in the air. The blocks will fall downward and place themselves one on top of another. As long as there is contact, any pile of blocks will be supported (ignore physics).

#### Problem A
You can eliminate blocks from the tower in any position, even if they are completely surrounded by other blocks. Once a block is removed, the blocks on top of it will not fall as long as they are in contact with another different block.

**Return the number of blocks that you can remove of the pile without causing any collapse.**

#### Problem B
Compute the number of blocks that fall when each block is independently eliminated, and **return the sum of blocks that can fall for each block**.
