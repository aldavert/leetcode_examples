# [Day 10: Pipe Maze](https://adventofcode.com/2023/day/10)

You are given a map with pipes layout, where a cell means:
- `.` empty space.
- `|` vertical pipe.
- `-` horizontal pipe
- `F` bottom-to-right corner pipe.
- `7` bottom-to-left corner pipe.
- `J` top-to-left corner pipe.
- `L` top-to-right corner pipe.
- `S` your starting position.

Your starting position is **on top** of a pipe that creates a loop. The only two pipes that connect to the cell `S` are the ones necessary to close the loop so, there are no ambiguities.

#### Problem A
Find the number of jumps from `S` to the point furthest away in the pipe.

#### Problem B
Determine which map cells are enclosed by the pipe.

[*Hint*](https://en.wikipedia.org/wiki/Point_in_polygon)

