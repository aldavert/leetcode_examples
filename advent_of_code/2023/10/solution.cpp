#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>

struct Position
{
    long row = -1;
    long col = -1;
    bool isUnset(void) const { return (row == -1) && (col == -1); }
};

std::tuple<long, long> maxDistance(std::vector<std::string> map, Position start)
{
    Position previousA = start, pointA, previousB = start, pointB;
    std::vector<std::vector<int> > distance(map.size(), std::vector<int>(map[0].size(), -1));
    auto init = [&pointA, &pointB](long row, long col) -> void
    {
        if (pointA.isUnset()) pointA = {row, col};
        else pointB = {row, col};
    };
    auto next = [](Position current, Position previous, char shape) -> Position
    {
        long d_row = current.row - previous.row,
             d_col = current.col - previous.col;
        switch (shape)
        {
        case '|':
            return { current.row + d_row, current.col };
        case '-':
            return { current.row, current.col + d_col };
        case 'L':
            if (d_row) return { current.row, current.col + 1 };
            else return { current.row - 1, current.col };
        case 'J':
            if (d_row) return { current.row, current.col - 1 };
            else return { current.row - 1, current.col };
        case '7':
            if (d_col) return { current.row + 1, current.col };
            else return { current.row, current.col - 1 };
        case 'F':
            if (d_col) return { current.row + 1, current.col };
            else return { current.row, current.col + 1 };
        default:
            return current;
        }
    };
    
    // Initializes the first two next positions after 'start' by searching the
    // only other two cells which are connected to 'S'.
    if ((map[start.row][start.col + 1] == '-')
    ||  (map[start.row][start.col + 1] == '7')
    ||  (map[start.row][start.col + 1] == 'J')) init(start.row, start.col + 1);
    if ((map[start.row][start.col - 1] == '-')
    ||  (map[start.row][start.col - 1] == 'F')
    ||  (map[start.row][start.col - 1] == 'L')) init(start.row, start.col - 1);
    if ((map[start.row + 1][start.col] == '|')
    ||  (map[start.row + 1][start.col] == 'J')
    ||  (map[start.row + 1][start.col] == 'L')) init(start.row + 1, start.col);
    if ((map[start.row - 1][start.col] == '|')
    ||  (map[start.row - 1][start.col] == 'F')
    ||  (map[start.row - 1][start.col] == '7')) init(start.row - 1, start.col);
    
    // Removes 'S' from the map and replaces it by the pipe necessary to complete
    // the loop (needed to fill in/out regions of the pipe polygon).
    long d_rowA = pointA.row - previousA.row,
         d_colA = pointA.col - previousA.col,
         d_rowB = pointB.row - previousB.row,
         d_colB = pointB.col - previousB.col;
    if ((d_rowA != 0) && (d_rowB != 0)) map[start.row][start.col] = '|';
    if ((d_colA != 0) && (d_colB != 0)) map[start.row][start.col] = '-';
    if (((d_rowA == 0) && (d_colA > 0) && (d_rowB > 0) && (d_colB == 0))
    ||  ((d_rowB == 0) && (d_colB > 0) && (d_rowA > 0) && (d_colA == 0)))
        map[start.row][start.col] = 'F';
    if (((d_rowA == 0) && (d_colA > 0) && (d_rowB < 0) && (d_colB == 0))
    ||  ((d_rowB == 0) && (d_colB > 0) && (d_rowA < 0) && (d_colA == 0)))
        map[start.row][start.col] = 'L';
    if (((d_rowA == 0) && (d_colA < 0) && (d_rowB > 0) && (d_colB == 0))
    ||  ((d_rowB == 0) && (d_colB < 0) && (d_rowA > 0) && (d_colA == 0)))
        map[start.row][start.col] = '7';
    if (((d_rowA == 0) && (d_colA < 0) && (d_rowB < 0) && (d_colB == 0))
    ||  ((d_rowB == 0) && (d_colB < 0) && (d_rowA < 0) && (d_colA == 0)))
        map[start.row][start.col] = 'J';
    
    // Compute the furthest away point in pipe.
    distance[start.row][start.col] = 0;
    while ((distance[pointA.row][pointA.col] == -1)
        && (distance[pointB.row][pointB.col] == -1))
    {
        distance[pointA.row][pointA.col] = distance[previousA.row][previousA.col] + 1;
        distance[pointB.row][pointB.col] = distance[previousB.row][previousB.col] + 1;
        previousA = std::exchange(pointA, next(pointA, previousA, map[pointA.row][pointA.col]));
        previousB = std::exchange(pointB, next(pointB, previousB, map[pointB.row][pointB.col]));
    }
    
    // Fill the polygon defined by the previous distance algorithm and, use the
    // map crossings ('|', "FJ" and "L7", note that "FJ" and "L7" can be extended
    // with '-') to determine which positions are inside the polygon (polygon fill
    // algorithm).
    long inside_count = 0;
    const size_t nrows = distance.size(), ncols = distance[0].size();
    for (size_t row = 0; row < nrows; ++row)
    {
        bool outside = true;
        char previous = '\0';
        for  (size_t col = 0; col < ncols; ++col)
        {
            if (distance[row][col] < 0)
            {
                if (!outside) ++inside_count;
            }
            else
            {
                if (map[row][col] == '|') outside = !outside;
                if ((previous == 'F') && (map[row][col] == 'J')) outside = !outside;
                if ((previous == 'L') && (map[row][col] == '7')) outside = !outside;
            }
            if (map[row][col] != '-') previous = map[row][col];
        }
    }
    
    return { distance[previousA.row][previousA.col], inside_count };
}

int main(int argc, char * * argv)
{
    Position start;
    std::vector<std::string> map;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        std::vector<long> time, distance;
        long row = 1;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                if (map.empty())
                    map.push_back(std::string(line.size() + 2, '.'));
                map.push_back("." + line + ".");
                if (auto col = line.find('S'); col != std::string::npos)
                    start = { row, static_cast<long>(col) + 1 };
                ++row;
            }
        }
        file.close();
        if (!map.empty())
            map.push_back(std::string(map.back().size(), '.'));
    }
    // -[ End File Parser ]-----------------------------------------------------
    auto [resultA, resultB] = maxDistance(map, start);
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

