# [Day 23: A Long Walk](https://adventofcode.com/2023/day/23)

You are given a maze where cells are:
- `#` for blocked cells,
- `.` for empty space.
- `>` for cells that can only be passed when moving to **right**.
- `^` for cells that can only be passed when moving to **up**.
- `<` for cells that can only be passed when moving to **left**.
- `v` for cells that can only be passed when moving to **down**.

The **entry** to the mace is an empty cell at the **top of the maze** and the **exit** at **the bottom of the maze**.

#### Problem A
Find the number of steps to traverse the maze following the longest path stepping on each step only once.

#### Problem B
Same as before, but now cells `>`, `^`, `<` and `v` will behave as empty space cells `.` (i.e. no directional blocking).

