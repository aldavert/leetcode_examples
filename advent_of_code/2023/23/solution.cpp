#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>
#include <queue>
#include <bitset>
#include <bit>

struct Position
{
    int row = 0;
    int col = 0;
    bool operator==(const Position &other) const
    {
        return (row == other.row) && (col == other.col);
    }
    bool operator<(const Position &other) const
    {
        return (row < other.row)
            || ((row == other.row) && (col < other.col));
    };
};

struct Node
{
    Position position;
    std::unordered_map<size_t, long> neighbors;
};

std::vector<Node> extractGraph(const std::vector<std::string> &forest,
                               Position start,
                               Position end,
                               bool slope_enabled)
{
    const int n_rows = static_cast<int>(forest.size());
    const int n_cols = static_cast<int>(forest[0].size());
    constexpr int directions[] = {-1, 0, 1, 0, -1, 0, 0};
    constexpr char slope[] = {'^', '>', 'v', '<'};
    struct Visit
    {
        Position position;
        std::vector<size_t> available;
        size_t index;
    };
    std::vector<Node> graph;
    std::map<Position, size_t> position_lut;
    std::tuple<size_t, long, bool, bool> edge_to_end = {n_rows * n_cols, -1, true, true};
    
    graph.push_back({start, {}});
    position_lut[start] = 0;
    std::stack<Visit> q;
    q.push({start, {2}, 0});
    while (!q.empty())
    {
        auto &current = q.top();
        if (current.available.empty())
        {
            q.pop();
            continue;
        }
        std::vector<size_t> available = {current.available.back()};
        current.available.pop_back();
        auto [row, col] = current.position;
        long length = 0;
        bool path_back = true, path_forth = true;
        while (available.size() == 1)
        {
            size_t previous = available[0];
            row += directions[previous];
            col += directions[previous + 1];
            available.pop_back();
            if (forest[row][col] != '.')
            {
                path_forth = forest[row][col] == slope[previous];
                path_back  = forest[row][col] == slope[(previous + 2) % 4];
            }
            ++length;
            for (size_t d = 0; d < 4; ++d)
            {
                if (((previous + 2) % 4) == d) continue;
                int new_row = row + directions[d], new_col = col + directions[d + 1];
                if ((new_row < 0) || (new_row >= n_rows)
                ||  (new_col < 0) || (new_col >= n_cols)) continue;
                if (forest[new_row][new_col] == '#') continue;
                available.push_back(d);
            }
        }
        path_forth = !slope_enabled || path_forth;
        path_back  = !slope_enabled || path_back;
        if (available.size() == 0)
        {
            if ((row == end.row) && (col == end.col))
                edge_to_end = {current.index, length, path_forth, path_back};
            continue;
        }
        Position new_position = {row, col};
        size_t index;
        if (auto search = position_lut.find(new_position); search == position_lut.end())
        {
            graph.push_back({new_position, {}});
            position_lut[new_position] = graph.size() - 1;
            q.push({new_position, available, graph.size() - 1});
            index = graph.size() - 1;
        }
        else index = search->second;
        if (path_back)
            graph[index].neighbors[current.index] = length;
        if (path_forth)
            graph[current.index].neighbors[index] = length;
    }
    graph.push_back({end.row, end.col, {}});
    if (std::get<2>(edge_to_end))
        graph[std::get<0>(edge_to_end)].neighbors[graph.size() - 1] = std::get<1>(edge_to_end);
    if (std::get<3>(edge_to_end))
        graph.back().neighbors[std::get<0>(edge_to_end)] = std::get<1>(edge_to_end);
    return graph;
}

long longestPath(const std::vector<Node> &graph)
{
    std::vector<bool> active(graph.size(), true);
    long result = 0;
    std::function<void(size_t, long)> traverse =
        [&](size_t node, long steps) -> void
    {
        if (!active[node]) return;
        if (node == graph.size() - 1)
        {
            result = std::max(result, steps);
            return;
        }
        active[node] = false;
        for (int neighbor = 0; auto [next, length] : graph[node].neighbors)
        {
            traverse(next, steps + length);
            ++neighbor;
        }
        active[node] = true;
    };
    traverse(0, 0);
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<std::string> forest;
    Position start, end;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        while (std::getline(file, line))
        {
            if (!line.empty())
                forest.push_back(line);
        }
        file.close();
        start = { 0, static_cast<int>(forest.front().find('.')) };
        end = { static_cast<int>(forest.size() - 1), static_cast<int>(forest.back().find('.'))};
    }
    // -[ End File Parser ]-----------------------------------------------------
    long resultA = longestPath(extractGraph(forest, start, end, true)),
         resultB = longestPath(extractGraph(forest, start, end, false));
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    
    return EXIT_SUCCESS;
}

