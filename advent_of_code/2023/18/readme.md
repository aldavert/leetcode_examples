# [Day 18: Lavaduct Lagoon](https://adventofcode.com/2023/day/18)

You are given the shape of a polygon as `<direction> <length> (<RGB>)` where direction can be **up** `U`, **right** `R`, **down** `D` and **left** `L` and `RGB` is a color value in hexadecimal code `#RRGGBB`.

You start at `(0, 0)` and after following all the instructions you will go back to `(0, 0)`. The polygon does not have crossings.

#### Problem A
**Find the area of the polygon**.

#### Problem B
The color code encodes another polygon, where the first five digits correspond to the length of the polygon in hexadecimal and the last digit corresponds to the heading: `{0: 'R', 1: 'D', 2: 'L', 3: 'U'}`.

**Find the area of the polygon encoded in the RGB color codes**.

