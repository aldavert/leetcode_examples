#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>
#include <queue>
#include "utils/utils.hpp"

enum class HEADING { UP    = 0, RIGHT = 1, DOWN  = 2, LEFT  = 3};
struct PolygonVector
{
    HEADING heading = HEADING::UP;
    long length = 0;
};

long polygonArea(const std::vector<PolygonVector> &vectors)
{
    struct Point
    {
        long x = 0;
        long y = 0;
    };
    struct Segment
    {
        Point begin;
        Point end;
        bool operator<(const Segment &other) const
        {
            return  (begin.y < other.begin.y)
                || ((begin.y == other.begin.y) && (end.y < other.end.y));
        }
    };
    
    std::vector<Segment> segments;
    long perimeter = 0, clockwise = 0;
    HEADING previous_heading = vectors.back().heading;
    for (auto [heading, length] : vectors)
    {
        perimeter += length;
        switch (heading)
        {
        case HEADING::UP:
            clockwise += 2 * (previous_heading == HEADING::LEFT) - 1;
            break;
        case HEADING::RIGHT:
            clockwise += 2 * (previous_heading == HEADING::UP) - 1;
            break;
        case HEADING::DOWN:
            clockwise += 2 * (previous_heading == HEADING::RIGHT) - 1;
            break;
        case HEADING::LEFT:
            clockwise += 2 * (previous_heading == HEADING::DOWN) - 1;
            break;
        default:
            continue;
        }
        previous_heading = heading;
    }
    clockwise = clockwise == 4;
    
    Point previous = {0, 0};
    previous_heading = vectors.back().heading;
    for (int i = 0, n = static_cast<int>(vectors.size()); i < n; ++i)
    {
        Point current = previous;
        switch (vectors[i].heading)
        {
        case HEADING::UP:
        {
            current.y -= vectors[i].length;
            int offset_begin = ( clockwise && (vectors[(i + 1) % n].heading == HEADING::RIGHT))
                             + (!clockwise && (vectors[(i + 1) % n].heading == HEADING::LEFT ));
            int offset_end = ( clockwise && (previous_heading == HEADING::LEFT ))
                           + (!clockwise && (previous_heading == HEADING::RIGHT));
            Point segment_begin = { current.x,  current.y + offset_begin},
                  segment_end   = {previous.x, previous.y - offset_end  };
            segments.push_back({segment_begin, segment_end});
            break;
        }
        case HEADING::RIGHT:
            current.x += vectors[i].length;
            break;
        case HEADING::DOWN:
        {
            current.y += vectors[i].length;
            int offset_begin = ( clockwise && (previous_heading == HEADING::RIGHT))
                             + (!clockwise && (previous_heading == HEADING::LEFT ));
            int offset_end = ( clockwise && (vectors[(i + 1) % n].heading == HEADING::LEFT ))
                           + (!clockwise && (vectors[(i + 1) % n].heading == HEADING::RIGHT));
            Point segment_begin = {previous.x, previous.y + offset_begin},
                  segment_end   = {current.x , current.y  - offset_end};
            segments.push_back({segment_begin, segment_end});
            break;
        }
        case HEADING::LEFT:
            current.x -= vectors[i].length;
            break;
        default:
            continue;
        }
        previous = current;
        previous_heading = vectors[i].heading;
    }
    std::sort(segments.begin(), segments.end());
    std::vector<size_t> active, ending;
    long current_y = 0, area = 0;
    for (size_t position = 0; position <= segments.size();)
    {
        std::vector<long> horizontal_partitions;
        if (active.size() == 0)
        {
            if (position == segments.size())
                break;
            current_y = segments[position].begin.y;
        }
        while ((position < segments.size())
           &&  (current_y == segments[position].begin.y))
            active.push_back(position++);
        for (size_t i : active)
        {
            horizontal_partitions.push_back(segments[i].begin.x);
            if ((ending.empty())
            ||  (segments[i].end.y == segments[ending.front()].end.y))
                ending.push_back(i);
            else if (segments[i].end.y < segments[ending.front()].end.y)
            {
                ending.clear();
                ending.push_back(i);
            }
        }
        long ending_y = segments[ending.front()].end.y;
        if (position < segments.size())
            ending_y = std::min(ending_y, segments[position].begin.y - 1);
        std::sort(horizontal_partitions.begin(), horizontal_partitions.end());
        for (size_t i = 0; i < active.size(); i += 2)
            area += (ending_y - current_y + 1)
                 *  (horizontal_partitions[i + 1] - horizontal_partitions[i] - 1);
        current_y = ending_y + 1;
        if (segments[ending.front()].end.y == ending_y)
        {
            for (size_t i = 0, n = active.size(); i < n;)
            {
                if (std::find(ending.begin(), ending.end(), active[i]) != ending.end())
                    std::swap(active[i], active[--n]);
                else ++i;
            }
            for (size_t i = 0; i < ending.size(); ++i)
                active.pop_back();
        }
        ending.clear();
    }
    return area + perimeter;
}

int main(int argc, char * * argv)
{
    std::vector<PolygonVector> problemA, problemB;
    
    // -[ Begin File Parser ]---------------------------------------------------
    {
        auto extractInfo = [](std::string text) -> PolygonVector
        {
            constexpr static HEADING lut[] =
                { HEADING::RIGHT, HEADING::DOWN, HEADING::LEFT, HEADING::UP };
            std::stringstream s;
            long value = 0;
            for (char symbol : text.substr(0, text.size() - 1))
                value = value * 16
                      + ((symbol >= '0') && (symbol <= '9')) * (symbol - '0')
                      + ((symbol == 'a') || (symbol == 'A')) * 10
                      + ((symbol == 'b') || (symbol == 'B')) * 11
                      + ((symbol == 'c') || (symbol == 'C')) * 12
                      + ((symbol == 'd') || (symbol == 'D')) * 13
                      + ((symbol == 'e') || (symbol == 'E')) * 14
                      + ((symbol == 'f') || (symbol == 'F')) * 15;
            return { lut[text.back() - '0'], value };
        };
        std::unordered_map<char, HEADING> chr_to_direction;
        chr_to_direction['U'] = HEADING::UP;
        chr_to_direction['D'] = HEADING::DOWN;
        chr_to_direction['L'] = HEADING::LEFT;
        chr_to_direction['R'] = HEADING::RIGHT;
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                auto tmp = utls::split(line);
                problemA.push_back({chr_to_direction[tmp[0][0]], std::atoi(tmp[1].c_str())});
                problemB.push_back(extractInfo(tmp[2].substr(1, tmp[2].size() - 2)));
            }
        }
        file.close();
    }
    // -[ End File Parser ]-----------------------------------------------------
    
    long resultA = polygonArea(problemA),
         resultB = polygonArea(problemB);
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}


