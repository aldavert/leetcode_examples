# [Day 8: Haunted Wasteland](https://adventofcode.com/2023/day/8)

You are given a list of instructions and a set of graph nodes.

The instructions contain the values `L` to select the left node and `R` to select the right node. For example, `LRL` to take first left node, then right node and finally left node.

The nodes have the format `<node> = (<left node>, <right node>)`. For example,
```
AAA = (BBB, CCC)
BBB = (DDD, AAA)
CCC = (ZZZ, CCC)
ZZZ = (CCC, BBB)
```

#### Problem A
Following the instructions, **how many steps do you have to follow to go from node** `AAA` **to node** `ZZZ`.

The *instructions are followed cyclically*, so once you follow the last instruction, you restart from the beginning if you have not reached `ZZZ`.

#### Problem B
Following the instructions, now start from all nodes that end with a `A` (e.g. `AAA`, `NJA`, `BHA`, ...) and finish when you reach a position ending with `Z` (e.g. `HXZ`, `GHZ`, `TPZ`, ...).

**How many steps do you have to follow so you reach all finishing position at the same time.**


