#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>

struct Node
{
    std::string left = "";
    std::string right = "";
};

long solveA(const std::unordered_map<std::string, Node> &graph,
            const std::string &instructions)
{
    if (graph.count("AAA") == 0) return -1;
    long counter = 0;
    size_t idx = 0;
    for (std::string position = "AAA"; position != "ZZZ"; ++idx, ++counter)
    {
        idx *= (idx != instructions.size());
        if (instructions[idx] == 'L') position = graph.at(position).left;
        else position = graph.at(position).right;
    }
    return counter;
}

long solveB(const std::unordered_map<std::string, Node> &graph,
            const std::string &instructions)
{
    std::vector<std::string> positions;
    for (const auto &[position, node] : graph)
        if (position[2] == 'A')
            positions.push_back(position);
    size_t idx = 0;
    std::vector<size_t> counter[3];
    counter[0] = std::vector<size_t>(positions.size(), 100);
    counter[1] = std::vector<size_t>(positions.size(), 0);
    counter[2] = std::vector<size_t>(positions.size(), 0);
    while (true)
    {
        idx *= (idx != instructions.size());
        bool update = false;
        for (size_t p = 0; p < positions.size(); ++p)
        {
            if (instructions[idx] == 'L') positions[p] = graph.at(positions[p]).left;
            else positions[p] = graph.at(positions[p]).right;
            ++counter[2][p];
            if (positions[p][2] == 'Z')
                update = true;
        }
        if (update)
        {
            size_t same_update = 0;
            for (size_t p = 0; p < positions.size(); ++p)
                same_update += counter[0][p] == counter[1][p];
            if (same_update == positions.size())
                break;
            for (size_t p = 0; p < positions.size(); ++p)
                if (positions[p][2] == 'Z')
                    counter[0][p] = std::exchange(counter[1][p], std::exchange(counter[2][p], 0));
        }
        ++idx;
    }
    long result = counter[1][0];
    for (size_t p = 1; p < positions.size(); ++p)
        result = std::lcm(result, counter[1][p]);
    return result;
}

int main(int argc, char * * argv)
{
    std::string instructions;
    std::unordered_map<std::string, Node> graph;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        std::string line;
        std::vector<long> time, distance;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                if (instructions.empty())
                    instructions = line;
                else graph[line.substr(0, 3)] = {line.substr(7, 3), line.substr(12, 3)};
            }
        }
        file.close();
    }
    // -[ End File Parser ]-----------------------------------------------------
    
    long resultA = solveA(graph, instructions),
         resultB = solveB(graph, instructions);
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

