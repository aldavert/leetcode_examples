# [Day 2: Cube Conundrum](https://adventofcode.com/2023/day/2)

#### Problem A
You are given a list of dice games where for each game you are show multiple times a random sample of a bag containing *red*, *blue* and *green* dices. Each line corresponds to a game starting with `Game <id>:` followed by a list of drawn results separated by semicolons. Each draw result is a list (separated by commas) with the frequency of each cube type.

Find all the games where the bag contains at most *12 red cubes*, *13 green cubes* and *14 blue cubes*. The **solution** is the sum of all these problems IDs.

#### Problem B
Given the same data as before, now compute for each game the minimum amount of red, green and blue cubes needed (i.e. maximum number of cubes of each color), multiply them and **return the sum of the products**.

