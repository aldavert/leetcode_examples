#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>

int main(int argc, char * * argv)
{
    auto parseList = [](std::string values) -> std::tuple<int, int, int>
    {
        int red = 0, green = 0, blue = 0;
        for (int value = 0; char letter : values)
        {
            if ((letter >= '0') && (letter <= '9'))
                value = value * 10 + (letter - '0');
            else if (letter != ' ')
            {
                if      ((value != 0) && (letter == 'r')) red = value;
                else if ((value != 0) && (letter == 'g')) green = value;
                else if ((value != 0) && (letter == 'b')) blue = value;
                value = 0;
            }
        }
        return { red, green, blue };
    };
    auto validID = [&parseList](std::string line) -> int
    {
        constexpr size_t END = std::string::npos;
        size_t pos_colon = line.find(':');
        int id = atoi(line.substr(5, pos_colon - 5).c_str());
        for (size_t i = pos_colon, next = 0; i < line.size(); i = next)
        {
            next = line.find(";", i + 1);
            size_t count = (next < END)?next - i - 1:END;
            auto [red, green, blue] = parseList(line.substr(i + 1, count));
            if ((red > 12) || (green > 13) || (blue > 14)) return 0;
        }
        return id;
    };
    auto power = [&parseList](std::string line) -> long
    {
        constexpr size_t END = std::string::npos;
        size_t pos_colon = line.find(':');
        long max_red = 0, max_green = 0, max_blue = 0;
        for (size_t i = pos_colon, next = 0; i < line.size(); i = next)
        {
            next = line.find(";", i + 1);
            size_t count = (next < END)?next - i - 1:END;
            auto [red, green, blue] = parseList(line.substr(i + 1, count));
            max_red   = std::max<long>(max_red,    red);
            max_green = std::max<long>(max_green, green);
            max_blue  = std::max<long>(max_blue,   blue);
        }
        return max_red * max_green * max_blue;
    };
    const char * filename = (argc > 1)?argv[1]:"data.txt";
    std::ifstream file(filename);
    std::string line;
    long resultA = 0, resultB = 0;
    while (std::getline(file, line))
    {
        if (!line.empty())
            resultA += validID(line),
            resultB += power(line);
    }
    file.close();
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}
