#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>

struct Element
{
    int row = 0;
    int col = 0;
    inline bool operator<(const Element &other) const { return row < other.row; }
};

struct Height
{
    Height(int r) : row(r) {}
    int row = 0;
    inline bool operator<(const Height &other) const { return row > other.row; }
    inline bool operator<(int value) const { return row < value; }
    inline bool operator>(int value) const { return row > value; }
    friend std::ostream& operator<<(std::ostream &out, const Height &h)
    {
        out << h.row;
        return out;
    }
};

long tiltUp(std::vector<Element> &rock,
            std::vector<Element> stop,
            int n_rows,
            int n_cols)
{
    std::vector<std::set<Height> > obstacle(n_cols);
    for (int i = 0; i < n_cols; ++i)
        obstacle[i].insert(-1);
    for (auto [row, col] : stop)
        obstacle[col].insert(row);
    std::sort(rock.begin(), rock.end());
    long result = 0;
    for (auto &[row, col] : rock)
    {
        auto boulder = obstacle[col].upper_bound(row);
        int score = boulder->row + 1;
        obstacle[col].insert(row = score);
        result += n_rows - score;
    }
    return result;
}

void draw(std::vector<Element> rock,
          std::vector<Element> stop,
          int n_rows,
          int n_cols)
{
    std::vector<std::string> show(n_rows, std::string(n_cols, '.'));
    for (auto [row, col] : rock)
        show[row][col] = 'O';
    for (auto [row, col] : stop)
        show[row][col] = '#';
    for (auto line : show)
        std::cout << line << '\n';
}

void rotate(std::vector<Element> &rock,
            std::vector<Element> &stop,
            int &n_rows,
            int &n_cols)
{
    for (auto &[row, col] : rock)
    {
        std::swap(row, col);
        col = n_cols - col - 1;
    }
    for (auto &[row, col] : stop)
    {
        std::swap(row, col);
        col = n_cols - col - 1;
    }
    std::swap(n_rows, n_cols);
}

long computeWeight(std::vector<Element> &rock,
                   int n_rows)
{
    long result = 0;
    for (auto &[row, col] : rock)
        result += n_rows - row;
    return result;
}

long cycle(std::vector<Element> rock,
           std::vector<Element> stop,
           int n_rows,
           int n_cols)
{
    std::vector<long> weights;
    std::unordered_map<long, std::vector<int> > positions;
    long result = 0;
    for (int cycle = 0; cycle < 1000; ++cycle)
    {
        tiltUp(rock, stop, n_rows, n_cols); // North
        rotate(rock, stop, n_rows, n_cols); // West
        tiltUp(rock, stop, n_rows, n_cols);
        rotate(rock, stop, n_rows, n_cols); // South
        tiltUp(rock, stop, n_rows, n_cols);
        rotate(rock, stop, n_rows, n_cols); // East
        tiltUp(rock, stop, n_rows, n_cols);
        rotate(rock, stop, n_rows, n_cols); // Back to north
        
        result = computeWeight(rock, n_rows);
        weights.push_back(result);
        if (auto search = positions.find(result); search != positions.end())
        {
            for (auto begin = search->second.rbegin(), end = search->second.rend(); begin != end; ++begin)
            {
                int size = cycle - *begin;
                if ((size == 1) || (size > *begin)) continue;
                bool is_same = true;
                for (int i = 0; is_same && (i < size); ++i)
                    is_same = is_same && (weights[*begin - i] == weights[cycle - i]);
                if (is_same)
                    return weights[*begin + (1'000'000'000 - *begin) % size - 1];
            }
        }
        positions[result].push_back(cycle);
    }
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<Element> rock, stop;
    int n_rows = 0, n_cols = 0;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        int row = 0, col = 0;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                col = 0;
                for (char symbol : line)
                {
                    if      (symbol == 'O') rock.push_back({row, col});
                    else if (symbol == '#') stop.push_back({row, col});
                    ++col;
                }
                ++row;
            }
        }
        file.close();
        n_rows = row;
        n_cols = col;
    }
    // -[ End File Parser ]-----------------------------------------------------
    auto data_a = rock, data_b = rock;
    long resultA = tiltUp(data_a, stop, n_rows, n_cols),
         resultB = cycle(data_b, stop, n_rows, n_cols);
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

