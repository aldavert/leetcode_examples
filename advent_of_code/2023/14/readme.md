# [Day 14: Parabolic Reflector Dish](https://adventofcode.com/2023/day/14)

You are given a map with `.` empty space, `#` obstacles, and `O` moving objects. The map can tilt to `north`, `west`, `south` and `east` and, when it does, all moving objects move all the way to the border of the tilted direction. Moving objects will be stopped by the *border of the map*, *an obstacle* and *another moving object which cannot move anymore*. 

#### Problem A
Tilt the map north and **return the accumulated weight of the moving objects**. The object's weight increases as they are closest to the northern border. So an object's weight is computed as `map.number_of_rows - object.row`.

#### Problem B
Same as before, but now tilt the map cyclically tilts `north -> west -> south -> east`. What is the **accumulated weight of the moving objects after 1'000'000'000 cycles?**

