#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <numeric>

int main(int argc, char * * argv)
{
    auto cardPoints = [&](std::string line) -> long
    {
        size_t i = line.find(':') + 2, n = line.size();
        bool present[100] = {};
        int value;
        for (value = 0; line[i] != '|'; ++i)
        {
            if ((line[i] >= '0') && (line[i] <= '9'))
                value = value * 10 + static_cast<int>(line[i] - '0');
            else if (line[i] == ' ')
                present[value] = value != 0, value = 0;
        }
        present[value] = value != 0;
        ++i;
        long result = 0;
        for (value = 0; i < n; ++i)
        {
            if ((line[i] >= '0') && (line[i] <= '9'))
                value = value * 10 + static_cast<int>(line[i] - '0');
            else if (line[i] == ' ')
            {
                result += present[value];
                value = 0;
            }
        }
        result += present[value];
        return result;
    };
    auto totalScrachCards = [](const std::vector<long> &matching_numbers) -> long
    {
        const long n = matching_numbers.size();
        std::vector<long> number_of_cards(n, 1);
        for (long i = 0; i < n; ++i)
            for (long j = 0, k = i + 1; (j < matching_numbers[i]) && (k < n); ++j, ++k)
                number_of_cards[k] += number_of_cards[i];
        return std::accumulate(number_of_cards.begin(), number_of_cards.end(), 0);
    };
    const char * filename = (argc > 1)?argv[1]:"data.txt";
    std::ifstream file(filename);
    std::string line;
    long resultA = 0;
    std::vector<long> matching_numbers;
    while (std::getline(file, line))
    {
        if (!line.empty())
        {
            long card_matches = cardPoints(line);
            long points = (card_matches)?(1 << (card_matches - 1)):0;
            resultA += points;
            matching_numbers.push_back(card_matches);
        }
    }
    file.close();
    long resultB = totalScrachCards(matching_numbers);
    
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

