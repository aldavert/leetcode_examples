# [Day 4: Scratchcards](https://adventofcode.com/2023/day/4)

You are given a set of scratch cards with the following format: `Card <ID>: (<number>)+ | (<number>)+`, where the first set of number corresponds to the card's winning number (list of numbers separated by spaces), and the second list of numbers (after `'|'`) corresponds to the numbers you selected in the card.

#### Problem A
For each card, check how many matches `m` the card has between the winning numbers and the number you have selected. When any matches are present, the card is associated `2^(m - 1)` points, otherwise `0` points.

**Return the total number of points accumulated by all cards.**

#### Problem B
The `m` matches obtained by a card is used to increase by one the number of cards the next `m` cards.

**Return the total number of cards available once all cards have been processed.**

