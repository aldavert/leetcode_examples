#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>
#include <queue>
#include <algorithm>
#include <memory>
#include "utils/utils.hpp"

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

enum class Signal{ Low = 0, High = 1, None = 2 };

struct Device
{
    Device(const std::vector<std::string> &n) :
        next(n) {}
    virtual ~Device(void) {}
    virtual void addConnection(std::string source) = 0;
    virtual Signal process(std::string source, Signal input) = 0;
    virtual std::string info(void) const = 0;
    virtual void reset(void) = 0;
    std::vector<std::string> next;
};

struct FlipFlop : public Device
{
    FlipFlop(const std::vector<std::string> &n) :
        Device(n),
        status_on(false) {}
    void addConnection(std::string) { }
    Signal process(std::string, Signal input)
    {
        Signal result = Signal::None;
        if (input == Signal::Low)
        {
            result = (status_on)?Signal::Low:Signal::High;
            status_on = !status_on;
        }
        return result;
    }
    std::string info() const
    {
        std::stringstream output;
        output << "[FLIP-FLOP] Status: " << ((status_on)?"On":"Off") << "; Next: " << next;
        return output.str();
    }
    void reset(void)
    {
        status_on = false;
    }
    bool status_on;
};

struct Conjunction : public Device
{
    Conjunction(const std::vector<std::string> &n) :
        Device(n) {}
    void addConnection(std::string source)
    {
        memory[source] = Signal::Low;
    }
    Signal process(std::string source, Signal input)
    {
        memory[source] = input;
        bool all_high = true;
        for (const auto &[_, signal] : memory)
            all_high = all_high && (signal == Signal::High);
        return (all_high)?Signal::Low:Signal::High;
    }
    std::string info() const
    {
        std::stringstream output;
        output << "[CONJUNCTION] Memory: [";
        for (bool n = false; const auto &[parent, signal] : memory)
        {
            if (n) output << '|';
            n = true;
            output << parent << ':' << ((signal == Signal::High)?"High":"Low");
        }
        output << "]; Next: " << next;
        return output.str();
    }
    void reset(void)
    {
        for (auto &[source, signal] : memory)
            signal = Signal::Low;
    }
    std::unordered_map<std::string, Signal> memory;
};

long prodPositiveNegativeSignals(
        std::unordered_map<std::string, std::unique_ptr<Device> > &devices,
        std::vector<std::string> &broadcast,
        int number_of_pushes)
{
    std::queue<std::tuple<std::string, std::string, Signal> > signals;
    long number_of_low = 0, number_of_high = 0;
    for (int push = 0; push < number_of_pushes; ++push)
    {
        for (std::string dst : broadcast)
            signals.push({"broadcaster", dst, Signal::Low});
        number_of_low += 1;
        while (!signals.empty())
        {
            auto [source, device_id, signal] = signals.front();
            signals.pop();
            if (signal == Signal::Low) ++number_of_low;
            else ++number_of_high;
            auto search = devices.find(device_id);
            if (search == devices.end()) continue;
            auto &device = search->second;
            signal = device->process(source, signal);
            if (signal == Signal::None) continue;
            for (auto next : device->next)
                signals.push({device_id, next, signal});
        }
    }
    return number_of_low * number_of_high;
}

long problemB(std::unordered_map<std::string, std::unique_ptr<Device> > &devices,
              std::vector<std::string> &broadcast)
{
    std::vector<long> rx_count;
    std::string rx_parent;
    for (const auto &[device, object] : devices)
    {
        if (std::find(object->next.begin(), object->next.end(), "rx") != object->next.end())
        {
            const Conjunction * rx_object = static_cast<Conjunction *>(object.get());
            rx_count = std::vector<long>(rx_object->memory.size(), 0);
            rx_parent = device;
        }
    }
    if (rx_count.empty()) return -1;
            
    std::queue<std::tuple<std::string, std::string, Signal> > signals;
    for (long push = 1; push < std::numeric_limits<long>::max() - 1; ++push)
    {
        long result = 1;
        for (long count : rx_count)
            result = std::lcm(result, count);
        if (result != 0) return result;
        for (std::string dst : broadcast)
            signals.push({"broadcaster", dst, Signal::Low});
        while (!signals.empty())
        {
            auto [source, device_id, signal] = signals.front();
            signals.pop();
            auto search = devices.find(device_id);
            if (search == devices.end()) continue;
            auto &device = search->second;
            signal = device->process(source, signal);
            if (signal == Signal::None) continue;
            for (auto next : device->next)
                signals.push({device_id, next, signal});
            if ((device_id == rx_parent) && (signal == Signal::High))
            {
                const Conjunction * rx_object =
                    static_cast<Conjunction *>(device.get());
                for (size_t idx = 0;
                     const auto &[_, memory_signal] : rx_object->memory)
                {
                    if (memory_signal == Signal::High)
                        rx_count[idx] = push;
                    ++idx;
                }
            }
        }
    }
    return -2;
}

int main(int argc, char * * argv)
{
    std::unordered_map<std::string, std::unique_ptr<Device> > devices;
    std::vector<std::string> broadcast;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                auto token_line = utls::split(line, {" -> "});
                auto destination = utls::split(token_line[1], {", "});
                auto command = token_line[0];
                if (command == "broadcaster")
                    broadcast = destination;
                else if (command[0] == '%')
                    devices[command.substr(1, command.size())] =
                        std::make_unique<FlipFlop>(destination);
                else if (command[0] == '&')
                    devices[command.substr(1, command.size())] =
                        std::make_unique<Conjunction>(destination);
                else std::cerr << "[ERROR] Unknown: " << line << '\n';
            }
        }
        file.close();
        for (auto &[device_id, object] : devices)
            for (auto next : object->next)
                if (auto search = devices.find(next); search != devices.end())
                    search->second->addConnection(device_id);
    }
    // -[ End File Parser ]-----------------------------------------------------
    ////std::cout << "Broadcast: " << broadcast << '\n';
    ////for (const auto &[device_id, object] : devices)
    ////    std::cout << device_id << ": " << object->info() << '\n';
    long resultA = prodPositiveNegativeSignals(devices, broadcast, 1000);
    for (const auto &[device_id, object] : devices)
        object->reset();
    long resultB = problemB(devices, broadcast);
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}


