# [Day 20: Pulse Propagation](https://adventofcode.com/2023/day/20)

You are given a group of *electrical* devices connected between them. The devices are **flip-flop** and **conjunction** modules:
- **flip-flop** have an internal status signaling if the module is `on` or `off` (initially set to `off`). This status is changed when the module receives a `high pulse`, otherwise the module does nothing. The module generates:
    - `high pulse` when it switches from `on` to `off` status.
    - `low pulse` when it switches from `off` to `on` status.
- **conjunction** have an internal memory storing the last signal received by any module connected to it (initially set to `off`). After receiving a pulse, it generates:
    - `low pulse` when all memory cells were set to `high`.
    - `high pulse` otherwise.

There is also a **button** which generates a `low pulse` and a **broadcaster** which splits the button pulse and sends it to multiple modules.

#### Problem A
Compute how many `high pulses` and `low pulses` are going to be generated when the **button** is pulsed `1000` times and **return their product**.

#### Problem B
The circuit has an output to a `rx` device (not present in the circuit). **How many times you have to press the button so** `rx` **receives a** `low pulse`?


