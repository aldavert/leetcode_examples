#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>

enum class HandType {
    FiveOfAKind  = 7,
    FourOfAKind  = 6,
    FullHouse    = 5,
    ThreeOfAKind = 4,
    TwoPair      = 3,
    OnePair      = 2,
    HighCard     = 1 };

struct Hand
{
    Hand(const char * cards, bool joker)
    {
        std::unordered_map<char, int> histogram;
        int number_of_jokers = 0;
        for (int i = 0; i < 5; ++i)
        {
            m_cards[i] = cards[i];
            m_value[i] = static_cast<int>(cards[i] - '0') - !joker;
            if      (cards[i] == 'A') m_value[i] = 13;
            else if (cards[i] == 'K') m_value[i] = 12;
            else if (cards[i] == 'Q') m_value[i] = 11;
            else if (cards[i] == 'J') m_value[i] = (joker)?1:10;
            else if (cards[i] == 'T') m_value[i] = 9 + joker;
            if (joker && (cards[i] == 'J')) ++number_of_jokers;
            else ++histogram[cards[i]];
        }
        std::vector<int> frequencies;
        if (number_of_jokers < 5)
        {
            for (auto [_, frequency] : histogram)
                frequencies.push_back(frequency);
            std::sort(frequencies.begin(), frequencies.end());
            frequencies.back() += number_of_jokers;
        }
        else frequencies.push_back(5);
        if (frequencies.size() == 1) m_type = HandType::FiveOfAKind;
        else if (frequencies.size() == 2)
            m_type = (frequencies[1] == 4)?HandType::FourOfAKind
                                          :HandType::FullHouse;
        else if (frequencies.size() == 3)
            m_type = (frequencies[2] == 3)?HandType::ThreeOfAKind
                                          :HandType::TwoPair;
        else if (frequencies.size() == 4)
            m_type = HandType::OnePair;
        else m_type = HandType::HighCard;
    }
    char m_cards[5] = { '\0', '\0', '\0', '\0', '\0' };
    int m_value[5] = {};
    HandType m_type = HandType::HighCard;
    bool operator<(const Hand &other) const
    {
        if (static_cast<int>(m_type) == static_cast<int>(other.m_type))
        {
            for (int i = 0; i < 5; ++i)
            {
                if (m_value[i] == other.m_value[i]) continue;
                return m_value[i] < other.m_value[i];
            }
            return false;
        }
        return static_cast<int>(m_type) < static_cast<int>(other.m_type);
    }
    friend std::ostream& operator<<(std::ostream &out, const Hand &hand)
    {
        const std::string type_name[] = {"", "High Card", "One Pair", "Two Pair",
            "Three Of A Kind", "Full  House", "Four Of A Kind", "Five Of A Kind" };
        out << hand.m_cards[0] << hand.m_cards[1] << hand.m_cards[2]
            << hand.m_cards[3] << hand.m_cards[4] << " - "
            << hand.m_value[0] << ':' << hand.m_value[1] << ':' << hand.m_value[2]
            << ':' << hand.m_value[3] << ':' << hand.m_value[4] << " - "
            << " (" << type_name[static_cast<int>(hand.m_type)] << ")";
        return out;
    }
};

int main(int argc, char * * argv)
{
    std::vector<std::tuple<Hand, long> > infoA, infoB;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        std::string line;
        std::vector<long> time, distance;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                infoA.push_back({{line.substr(0, 5).c_str(), false},
                                 std::atoi(line.substr(6).c_str())});
                infoB.push_back({{line.substr(0, 5).c_str(), true},
                                 std::atoi(line.substr(6).c_str())});
            }
        }
        file.close();
    }
    // -[ End File Parser ]-----------------------------------------------------
    
    long resultA = 0,
         resultB = 0;
    std::sort(infoA.begin(), infoA.end());
    for (long counter = 0; auto [hand, bid] : infoA)
        resultA += bid * (++counter);
    std::sort(infoB.begin(), infoB.end());
    for (long counter = 0; auto [hand, bid] : infoB)
        resultB += bid * (++counter);
    
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

