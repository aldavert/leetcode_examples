# [Day 7: Camel Cards](https://adventofcode.com/2023/day/7)

You are given a **sets of 5 cards** its **strength** is computed as:
- `[Strengh 7]` All cards are the same: `(AAAAA)`.
- `[Strengh 6]` Four cards have the same label: `(ABAAA)`.
- `[Strengh 5]` Three cards have the same label and the remaining two share a different label: `(AABBB)`.
- `[Strengh 4]` Three cards have the same label and the remaining two are different from any other card: `(ABBCB)`.
- `[Strengh 3]` Two cards have the same label, another set of two cards share a different label and the remaining card is different from the rest: `(ABABC)`.
- `[Strengh 2]` A single pair of cards share the same label, the other three are different from the pair and each other: `(ABACD)`.
- `[Strengh 1]` All cards are different from each other: `(ABCDE)`.

When comparing two sets of cards, they will be sorted depending on their strength. When there is a tie however, the tie will be solved by comparing the individual card values **in the order** that they appear in the card.

The value of each card in order of strength is `A`, `K`, `Q`, `J`, `T`, `9`, `8`, `7`, `6`, `5`, `4`, `3`, `2`.

#### Problem A
Given a list of tuples `(sets of cards, bid)`, sort them depending of the cards strength and **return the sum of the bids multiplied by their rank**.

#### Problem B
Change the behavior of `J` to behave as a joker. Now `J` are replace to the card that will maximize the strength of the card. Additionally, its weight is reduced and it becomes the card with less value in the deck. So, when comparing cards *in a tie of strength*, the cards values in order of strength becomes: `A`, `K`, `Q`, `T`, `9`, `8`, `7`, `6`, `5`, `4`, `3`, `2`, `J`.

Using the **new set of rules**, sort the tuples depending on the card strength and **return the sum of the bids multiplied by their rank**.

