# [Day 9: Mirage Maintenance](https://adventofcode.com/2023/day/9)

You are give a set of number sequences.

#### Problem A
Determine which is the next number of each sequence and **return the sum of all these values**.

#### Problem B
Same as before but backwards, determine which is the value ***anterior*** the to the first element of each sequence and **return the sum of all these values**.

