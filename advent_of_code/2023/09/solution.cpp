#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>

std::vector<long> split(std::string text)
{
    std::vector<long> result;
    bool new_number = true, negative = false;
    for (char letter : text)
    {
        if ((letter >= '0') && (letter <= '9'))
        {
            if (new_number)
            {
                if (negative && (!result.empty())) result.back() *= -1;
                result.push_back(0);
                negative = new_number = false;
            }
            result.back() = result.back() * 10 + static_cast<long>(letter - '0');
        }
        else if (letter == '-')
        {
            if (new_number)
            {
                if (negative && (!result.empty())) result.back() *= -1;
                result.push_back(0);
                new_number = false;
            }
            negative = true;
        }
        else new_number = true;
    }
    if (negative) result.back() *= -1;
    return result;
}

long sequenceNext(std::vector<long> sequence)
{
    auto copy = sequence;
    const size_t n = sequence.size();
    for (size_t i = 0; i < n; ++i)
    {
        size_t m = n - i - 1;
        bool all_zero = true;
        for (size_t j = 0; j < m; ++j)
        {
            sequence[j] = sequence[j + 1] - sequence[j];
            all_zero = all_zero && (sequence[j] == 0);
        }
        if (all_zero)
        {
            long result = 0;
            for (size_t p = m; p < n; ++p)
                result += sequence[p];
            return result;
        }
    }
    return 0;
}

int main(int argc, char * * argv)
{
    long resultA = 0,
         resultB = 0;
    auto reverse = [](std::vector<long> values) -> std::vector<long>
    {
        std::reverse(values.begin(), values.end());
        return values;
    };
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        std::string line;
        std::vector<long> time, distance;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                auto values = split(line);
                resultA += sequenceNext(values);
                resultB += sequenceNext(reverse(values));
            }
        }
        file.close();
    }
    // -[ End File Parser ]-----------------------------------------------------
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

