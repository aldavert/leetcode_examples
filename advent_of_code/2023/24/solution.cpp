#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>
#include <queue>
#include <bitset>
#include <bit>
#include "utils/utils.hpp"
#include <armadillo>

struct Point
{
    long double x = 0;
    long double y = 0;
    long double z = 0;
};

struct Vector
{
    Point start;
    Point heading;
};

long countIntersectXY(const std::vector<Vector> info,
                      long double min_value,
                      long double max_value)
{
    auto intersect = [&](const Vector &a, const Vector &b) -> std::tuple<long double, long double>
    {
        long double dx = b.start.x - a.start.x;
        long double dy = b.start.y - a.start.y;
        long double numerator = a.heading.x * dy - a.heading.y * dx;
        long double denominator = b.heading.x * a.heading.y - b.heading.y * a.heading.x;
        long double beta = numerator / denominator;
        long double alpha = (dx + beta * b.heading.x) / a.heading.x;
        return { alpha, beta };
    };
    long result = 0;
    for (size_t i = 0, n = info.size(); i < n; ++i)
    {
        for (size_t j = i + 1; j < n; ++j)
        {
            auto [alpha, beta] = intersect(info[i], info[j]);
            long double x = info[j].start.x + beta * info[j].heading.x;
            long double y = info[j].start.y + beta * info[j].heading.y;
            if ((alpha < 0) || (beta < 0)) continue;
            result += ((x >= min_value) && (x <=  max_value)
                    && (y >= min_value) && (y <=  max_value));
        }
    }
    return result;
}

long sumIntersectionAll(const std::vector<Vector> info)
{
    auto solve = [](const Vector &a, const Vector &b, const Vector &c) -> Vector
    {
        long double px1 = a.start.x, py1 = a.start.y, pz1 = a.start.z,
                    px2 = b.start.x, py2 = b.start.y, pz2 = b.start.z,
                    px3 = c.start.x, py3 = c.start.y, pz3 = c.start.z;
        long double vx1 = a.heading.x, vy1 = a.heading.y, vz1 = a.heading.z,
                    vx2 = b.heading.x, vy2 = b.heading.y, vz2 = b.heading.z,
                    vx3 = c.heading.x, vy3 = c.heading.y, vz3 = c.heading.z;
        long double alpha1 = (px1 * ((py3 - py2) * (vz3 - vz2) + (pz2 - pz3) * (vy3 - vy2))
                            + px2 * ((py1 - py3) * (vz3 - vz2) + (pz3 - pz1) * (vy3 - vy2))
                            + px3 * ((py2 - py1) * (vz3 - vz2) + (pz1 - pz2) * (vy3 - vy2))
                            +        (vx3 - vx2) * (py1 * (pz3 - pz2)
                            +        py2 * (pz1 - pz3)
                            +        py3 * (pz2 - pz1)))
                        / (px2 * (vy2 * (vz3 - vz1) + vy3 * (vz1 - vz2) - vy1 * (vz3 - vz2))
                         + px3 * (vy1 * (vz3 - vz2) + vy2 * (vz1 - vz3) + vy3 * (vz2 - vz1))
                         + py2 * (vx1 * (vz3 - vz2) + vx2 * (vz1 - vz3) + vx3 * (vz2 - vz1))
                         + py3 * (vx2 * (vz3 - vz1) + vx3 * (vz1 - vz2) - vx1 * (vz3 - vz2))
                         + pz2 * (vx2 * (vy3 - vy1) + vx3 * (vy1 - vy2) - vx1 * (vy3 - vy2))
                         + pz3 * (vx1 * (vy3 - vy2) + vx2 * (vy1 - vy3) + vx3 * (vy2 - vy1)));
        long double alpha2 = (alpha1 * ((px3 - px2) * (vy3 - vy1) + (py2 - py3) * (vx3 - vx1))
                            + px1 * (py3 - py2) + px2 * (py1 - py3) + px3 * (py2 - py1))
                        / (alpha1 * (vx2 * (vy3 - vy1) + vx3 * (vy1 - vy2) - vx1 * (vy3 - vy2))
                         + (px3 - px1) * (vy3 - vy2) + (py1 - py3) * (vx3 - vx2));
        Vector result;
        result.heading.x = (alpha1 * vx1 - alpha2 * vx2 + px1 - px2)/(alpha1 - alpha2);
        result.heading.y = (alpha1 * vy1 - alpha2 * vy2 + py1 - py2)/(alpha1 - alpha2);
        result.heading.z = (alpha1 * vz1 - alpha2 * vz2 + pz1 - pz2)/(alpha1 - alpha2);
        result.start.x = alpha1 * (vx1 - result.heading.x) + px1;
        result.start.y = alpha1 * (vy1 - result.heading.y) + py1;
        result.start.z = alpha1 * (vz1 - result.heading.z) + pz1;
        return result;
    };
    // Note: You only need three vectors to generate enough equations to solve
    //       the system: you have the 6 unknowns of the problem (origin coordinates
    //       [x, y, z] and throw vector [dx, dy, dz]) and each equation adds an
    //       additional *alpha* unknown. Therefore, with three equations we have
    //       6 + 3 = 9 unknowns and 9 equations.
    //       Since we known from the description that all vectors belong to the
    //       solution, it doesn't matter which three vectors we select to compute
    //       it.
    auto solution = solve(info[0], info[1], info[2]);
    return static_cast<long>(solution.start.x + solution.start.y + solution.start.z);
}

int main(int argc, char * * argv)
{
    std::vector<Vector> info;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                auto parts = utls::split(line, {"@"});
                auto position_text = utls::split(parts[0], {","});
                auto heading_text = utls::split(parts[1], {","});
                auto position = utls::map([](std::string x) { return (long double)std::atol(utls::trim(x).c_str()); }, position_text);
                auto heading = utls::map([](std::string x) { return (long double)std::atol(utls::trim(x).c_str()); }, heading_text);
                info.push_back({{position[0], position[1], position[2]},
                                {heading[0] , heading[1] , heading[2] }});
            }
        }
        file.close();
    }
    // -[ End File Parser ]-----------------------------------------------------
    long resultA = countIntersectXY(info,
                                    (info.size() < 300)?7:200'000'000'000'000,
                                    (info.size() < 300)?27:400'000'000'000'000),
         resultB = sumIntersectionAll(info);
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

