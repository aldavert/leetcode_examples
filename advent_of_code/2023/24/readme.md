# [Day 24: Never Tell Me The Odds](https://adventofcode.com/2023/day/24)

You are given a list 3D vectors as `X, Y, Z @ dx, dy, dz`, where `(X, Y, Z)` is the point of origin and `(dx, dy, dz)` is the direction where the vector.

#### Problem A
Ignoring the `Z` coordinate, **find the number of vectors that intersect inside a given flat square test area**.

#### Problem B
Assuming that the vectors correspond to velocity vectors of objects in a 3D space, **find the initial coordinates of a vector that is going to intersect with all the objects**.


