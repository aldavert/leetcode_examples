import sympy
import pickle
import os

if __name__ == '__main__':
    # We only need three equations to solve the system:
    # P1 + alpha1 * V1 = P_solution + alpha1 * D_solution
    # P2 + alpha2 * V2 = P_solution + alpha2 * D_solution
    # P3 + alpha3 * V3 = P_solution + alpha3 * D_solution
    # where P1 = [p1_x, p1_y, p1_x], P2 = [p2_x, p2_y, p2_z], P3 = [p3_x, p3_y, p3_z]
    #       P_solution = [x, y, z], D_solution = [d_x, d_y, d_z]
    # Define all symbols:
    alpha1, alpha2, alpha3 = sympy.symbols('alpha_1 alpha_2 alpha_3')
    px1, py1, pz1, vx1, vy1, vz1 = sympy.symbols('px1 py1 pz1 vx1 vy1 vz1')
    px2, py2, pz2, vx2, vy2, vz2 = sympy.symbols('px2 py2 pz2 vx2 vy2 vz2')
    px3, py3, pz3, vx3, vy3, vz3 = sympy.symbols('px3 py3 pz3 vx3 vy3 vz3')
    x, y, z, dx, dy, dz = sympy.symbols('x y z dx dy dz')
    
    # Generating a single equation system and letting sympy solve the equations
    # manually to obtain P_solution and D_solution is too slow. So, I've solved
    # the equations manually with the help of sympy.
    
    # It's still slow (it takes about 30 seconds), it uses pickle to save the
    # model to disk and display the results faster.
    if False or not os.path.isfile('solution.dat'): # Set to true to force recompute
                                                    # or remove 'solution.dat'.
        
        eq1x = px1 + alpha1 * vx1 - (x + alpha1 * dx)
        eq1y = py1 + alpha1 * vy1 - (y + alpha1 * dy)
        eq1z = pz1 + alpha1 * vz1 - (z + alpha1 * dz)
        eq2x = px2 + alpha2 * vx2 - (x + alpha2 * dx)
        eq2y = py2 + alpha2 * vy2 - (y + alpha2 * dy)
        eq2z = pz2 + alpha2 * vz2 - (z + alpha2 * dz)
        eq3x = px3 + alpha3 * vx3 - (x + alpha3 * dx)
        eq3y = py3 + alpha3 * vy3 - (y + alpha3 * dy)
        eq3z = pz3 + alpha3 * vz3 - (z + alpha3 * dz)
        
        new_x = sympy.solve(eq1x, x)[0]
        print("\n\nX:\n")
        sympy.pprint(new_x)
        new_y = sympy.solve(eq1y, y)[0]
        print("\n\nY:\n")
        sympy.pprint(new_y)
        new_z = sympy.solve(eq1z, z)[0]
        print("\n\nZ:\n")
        sympy.pprint(new_z)
        
        eq2x = sympy.separatevars(eq2x.subs(x, new_x), force = True)
        eq2y = sympy.separatevars(eq2y.subs(y, new_y), force = True)
        eq2z = sympy.separatevars(eq2z.subs(z, new_z), force = True)
        eq3x = sympy.separatevars(eq3x.subs(x, new_x), force = True)
        eq3y = sympy.separatevars(eq3y.subs(y, new_y), force = True)
        eq3z = sympy.separatevars(eq3z.subs(z, new_z), force = True)
        
        new_dx = sympy.separatevars(sympy.solve(eq2x, dx)[0], force = True)
        print("\n\ndX:\n")
        sympy.pprint(new_dx)
        new_dy = sympy.separatevars(sympy.solve(eq2y, dy)[0], force = True)
        print("\n\ndY:\n")
        sympy.pprint(new_dy)
        new_dz = sympy.separatevars(sympy.solve(eq2z, dz)[0], force = True)
        print("\n\ndZ:\n")
        sympy.pprint(new_dz)
        
        eq3x = sympy.simplify(eq3x.subs(dx, new_dx))
        eq3y = sympy.simplify(eq3y.subs(dy, new_dy))
        eq3z = sympy.simplify(eq3z.subs(dz, new_dz))
        
        new_alpha3 = sympy.solve(sympy.collect(sympy.separatevars(eq3x, force = True), alpha3), alpha3)[0]
        print("\n\nalpha3:\n")
        sympy.pprint(new_alpha3)
        eq3y = sympy.simplify(sympy.separatevars(eq3y.subs(alpha3, new_alpha3), force = True))
        eq3z = sympy.simplify(sympy.separatevars(eq3z.subs(alpha3, new_alpha3), force = True))
        
        new_alpha2 = sympy.solve(eq3y, alpha2)[0]
        print("\n\nalpha2:\n")
        sympy.pprint(new_alpha2)
        eq3z = sympy.simplify(sympy.separatevars(eq3z.subs(alpha2, new_alpha2), force = True))
        new_alpha1 = sympy.solve(eq3z, alpha1)[0]
        print("\n\nalpha1:\n")
        sympy.pprint(new_alpha1)
        
        file = open('solution.dat', 'wb')
        pickle.dump((new_x, new_y, new_z, new_dx, new_dy, new_dz, new_alpha1, new_alpha2, new_alpha3), file)
        file.close()
    else:
        file = open('solution.dat', 'rb')
        (new_x, new_y, new_z, new_dx, new_dy, new_dz, new_alpha1, new_alpha2, new_alpha3) = pickle.load(file)
        file.close()
    
    print("\n\nX:\n")
    aux = sympy.collect(new_x, (alpha1))
    print(aux)
    print("\n\nY:\n")
    aux = sympy.collect(new_y, (alpha1))
    print(aux)
    print("\n\nZ:\n")
    aux = sympy.collect(new_z, (alpha1))
    print(aux)
    print("\n\ndX:\n")
    print(new_dx)
    print("\n\ndY:\n")
    print(new_dy)
    print("\n\ndZ:\n")
    print(new_dz)
    print("\n\nalpha3:\n")
    aux = sympy.collect(new_alpha3, (alpha1, alpha2))
    aux = sympy.collect(aux, (alpha1, alpha2))
    print(aux)
    print("\n\nalpha2:\n")
    aux = sympy.collect(new_alpha2, (alpha1, px1, px2, px3, py1, py2, py3, pz1, pz2, pz3))
    aux = sympy.collect(aux, (alpha1, px1, px2, px3, py1, py2, py3, pz1, pz2, pz3))
    aux = sympy.collect(aux, (vx1, vx2, vx3, vy1, vy2, vy3, vz1, vz2, vz3))
    aux = sympy.collect(aux, (vx1, vx2, vx3, vy1, vy2, vy3, vz1, vz2, vz3))
    aux = sympy.collect(aux, (vx3 - vx1, vy3 - vy1, vx3 - vx2, vy3 - vy2))
    print(aux)
    print("\n\nalpha1:\n")
    aux = sympy.collect(new_alpha1, (px1, px2, px3, py1, py2, py3, pz1, pz2, pz3))
    aux = sympy.collect(aux, (px1, px2, px3, py1, py2, py3, pz1, pz2, pz3))
    aux = sympy.collect(aux, (vx1, vx2, vx3, vy1, vy2, vy3, vz1, vz2, vz3))
    aux = sympy.collect(aux, (vx1, vx2, vx3, vy1, vy2, vy3, vz1, vz2, vz3))
    aux = sympy.collect(aux, (vx3 - vx2, vy3 - vy2, vz3 - vz2))
    print(aux)
        
