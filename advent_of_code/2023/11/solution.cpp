#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>

struct Star
{
    long row = 0;
    long col = 0;
    inline long distance(const Star &other) const
    {
        return std::abs(row - other.row) + std::abs(col - other.col);
    }
};

long closestStartAccumDistance(const std::vector<std::string> &skymap, long expansion = 2)
{
    const long n_rows = static_cast<long>(skymap.size()),
               n_cols = static_cast<long>(skymap[0].size());
    std::vector<bool> empty_row(n_rows, true), empty_col(n_cols, true);
    std::vector<Star> stars;
    for (long row = 0; row < n_rows; ++row)
    {
        for (long col = 0; col < n_cols; ++col)
        {
            if (skymap[row][col] == '#')
            {
                empty_row[row] = false;
                empty_col[col] = false;
                stars.push_back({row, col});
            }
        }
    }
    std::vector<long> coord_row, coord_col;
    for (long row = 0; bool val : empty_row)
    {
        coord_row.push_back(row++);
        row += val * (expansion - 1);
    }
    for (long col = 0; bool val : empty_col)
    {
        coord_col.push_back(col++);
        col += val * (expansion - 1);
    }
    for (auto &[row, col] : stars)
        row = coord_row[row],
        col = coord_col[col];
    
    const long n = static_cast<long>(stars.size());
    long result = 0;
    for (long i = 0; i < n; ++i)
        for (long j = i + 1; j < n; ++j)
            result += stars[i].distance(stars[j]);
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<std::string> skymap;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        while (std::getline(file, line))
        {
            if (!line.empty())
                skymap.push_back(line);
        }
        file.close();
    }
    // -[ End File Parser ]-----------------------------------------------------
    long resultA = closestStartAccumDistance(skymap),
         resultB = closestStartAccumDistance(skymap, 1'000'000);
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

