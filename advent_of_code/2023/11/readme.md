# [Day 11: Cosmic Expansion](https://adventofcode.com/2023/day/11)

You are given a sky-map where `.` cells correspond to *empty space* and `#` correspond to a *galaxy*.

In the sky-map the columns and rows that are completely empty are *bigger* than the ones that contain at least one galaxy. This multiplicative factor affecting empty columns and rows needs to be taken into account while obtaining the coordinates of a galaxy.

#### Problem A
**Find the accumulated Manhattan distance between all galaxy pairs** taking into account that empty columns and rows are **twice** as wide as the normal ones.

#### Problem B
Same as before but now the empty space is one **million times bigger** than normal space.

