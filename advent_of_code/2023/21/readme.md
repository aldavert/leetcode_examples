# [Day 21: Step Counter](https://adventofcode.com/2023/day/21)

You are given a map, containing free space `.` and obstacles `#`, and the coordinates of a starting point.

When you move through the map, you can only enter cells not containing an obstacle. You want to compute how many cells you can reach after taking `n` number of steps.

#### Problem A
Compute the number of cells that can be reached after taking **64 steps inside the map**.

#### Problem B
Compute the number of cells that can be reached after taking **26501365 steps in a map that expands infinitely in a periodic manner** (e.g. if you exit the map from the left side, you enter to an exact copy of the map at the same row but on the right side).

