#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>
#include <queue>
#include <bitset>
#include <bit>

struct Position
{
    short row = -1;
    short col = -1;
    short block_row = 0;
    short block_col = 0;
    inline size_t signature(void) const
    {
        return (static_cast<size_t>(std::bit_cast<unsigned short>(row)) << 48)
             | (static_cast<size_t>(std::bit_cast<unsigned short>(col)) << 32)
             | (static_cast<size_t>(std::bit_cast<unsigned short>(block_row)) << 16)
             | (static_cast<size_t>(std::bit_cast<unsigned short>(block_col)));
    }
};

long gardenPlotsCount(const std::vector<std::string> &garden, Position start, size_t steps)
{
    static constexpr short directions[] = {-1, 0, 1, 0, -1};
    std::queue<Position> active;
    active.push(start);
    for (size_t s = 0; s < steps; ++s)
    {
        auto empty = garden;
        for (size_t i = 0, n_elements = active.size(); i < n_elements; ++i)
        {
            auto current = active.front();
            active.pop();
            for (int d = 0; d < 4; ++d)
            {
                short new_row = current.row + directions[d    ];
                short new_col = current.col + directions[d + 1];
                if (empty[new_row][new_col] == '.')
                {
                    empty[new_row][new_col] = 'O';
                    active.push({new_row, new_col, 0, 0});
                }
            }
        }
    }
    
    return static_cast<long>(active.size());
}

long gardenPlotsCountInfinite(const std::vector<std::string> &garden, Position start, long steps)
{
    static constexpr short directions[] = {-1, 0, 1, 0, -1};
    constexpr long block_half = 3;
    constexpr long block_size = block_half * 2 + 1;
    const long n_rows = static_cast<short>(garden.size());
    const long n_cols = static_cast<short>(garden[0].size());
    std::vector<std::vector<int> > distances(n_rows * block_size,
            std::vector<int>(n_cols * block_size, -1));
    std::queue<std::tuple<int, int> > q;
    q.push({block_half * n_rows + start.row, block_half * n_cols + start.col});
    int max_distance = 0;
    while (!q.empty())
    {
        for (size_t i = 0, n = q.size(); i < n; ++i)
        {
            auto [row, col] = q.front();
            q.pop();
            if (distances[row][col] != -1) continue;
            distances[row][col] = max_distance;
            for (size_t d = 0; d < 4; ++d)
            {
                int new_row = row + directions[d],
                    new_col = col + directions[d + 1];
                if ((new_row < 0) || (new_row >= n_rows * block_size)
                ||  (new_col < 0) || (new_col >= n_cols * block_size)) continue;
                if (garden[new_row % n_rows][new_col % n_cols] == '#') continue;
                if (distances[new_row][new_col] != -1) continue;
                q.push({new_row, new_col});
            }
        }
        ++max_distance;
    }
    std::vector<long> solve_lut[2] = {
        std::vector<long>(max_distance + 1, -1),
        std::vector<long>(max_distance + 1, -1)
    };
    
    auto solve = [&](long distance, bool corner) -> long
    {
        if (distance < 0) return 0;
        if (solve_lut[corner][distance] != -1)
            return solve_lut[corner][distance];
        long solution = 0;
        for (long i = 1, jumps = (steps - distance) / n_rows; i <= jumps; ++i)
            if ( (distance + n_rows * i <= steps)
            &&  ((distance + n_rows * i) % 2 == steps % 2))
                solution += corner * (i + 1) + !corner;
        return solve_lut[corner][distance] = solution;
    };
    
    long result = 0;
    // Middle region result.
    for (const auto &line : distances)
        for (int distance : line)
            result += ((distance % 2 == steps % 2) && (distance <= steps));
    long row_offset = n_rows * (block_size - 1),
         col_offset = n_cols * (block_size - 1);
    // Lateral repetitions.
    for (long row = n_rows; row < row_offset; ++row)
        for (long col = 0; col < n_cols; ++col)
            result += solve(distances[row][col], false)
                   +  solve(distances[row][col + col_offset], false);
    for (long row = 0; row < n_rows; ++row)
        for (long col = n_cols; col < col_offset; ++col)
            result += solve(distances[row][col], false)
                   +  solve(distances[row + row_offset][col], false);
    // Corners repetitions.
    for (long row = 0; row < n_rows; ++row)
        for (long col = 0; col < n_cols; ++col)
            result += solve(distances[row][col], true)
                   +  solve(distances[row + row_offset][col], true)
                   +  solve(distances[row             ][col + col_offset], true)
                   +  solve(distances[row + row_offset][col + col_offset], true);
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<std::string> walled_garden, garden;
    Position walled_start, start;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        bool first = true;
        short row = 0;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                if (first)
                {
                    walled_garden.push_back(std::string(line.size() + 2, '#'));
                    first = false;
                }
                if (auto pos = line.find('S'); pos != std::string::npos)
                {
                    walled_start = {static_cast<short>(row + 1),
                                    static_cast<short>(pos + 1), 0, 0};
                    start = {row, static_cast<short>(pos), 0, 0};
                    line[walled_start.col - 1] = '.';
                }
                walled_garden.push_back("#" + line + "#");
                garden.push_back(line);
                ++row;
            }
        }
        walled_garden.push_back(std::string(walled_garden.back().size(), '#'));
        file.close();
    }
    // -[ End File Parser ]-----------------------------------------------------
    long resultA = gardenPlotsCount(walled_garden, walled_start, ((walled_garden.size() < 15)?6:64)),
         resultB = gardenPlotsCountInfinite(garden, start, 26501365);
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

