# [Day 17: Clumsy Crucible](https://adventofcode.com/2023/day/17)

You are given a map with the cost to enter into a cell. You have a robot that can only move forward for a limited distance *(after that it has to turn)*, turn left and turn right.

The robot starts at the top-left corner of the map and the exit is at the bottom right corner.

#### Problem A
What is the **minimum cost** to go from start to exit, with a robot that **can only move forward at most three tiles**.

#### Problem B
What is the **minimum cost** to go from start to exit, with a robot that **must move forward at least four tiles** and **can only move forward at most ten tiles**.
