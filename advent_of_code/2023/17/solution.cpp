#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>
#include <queue>

long minimumHeatLoss(std::vector<std::vector<int> > loss,
                     std::tuple<int, int> start,
                     std::tuple<int, int> goal,
                     int min_distance,
                     int max_distance)
{
    constexpr static int directions[5] = {-1, 0, 1, 0, -1};
    const int n_rows = static_cast<int>(loss.size());
    const int n_cols = static_cast<int>(loss[0].size());
    const int MAX_HEAT = n_rows * n_cols * 10;
    enum class HEADING { UP    = 0, RIGHT = 1, DOWN  = 2, LEFT  = 3};
    enum class MOVE { FORWARD, TURN_LEFT, TURN_RIGHT };
    struct Robot
    {
        int row = 0;
        int col = 0;
        int heat = 0;
        HEADING heading = HEADING::RIGHT;
        bool operator<(const Robot &other) const { return heat > other.heat; }
    };
    std::vector<std::vector<std::vector<int> > > min_heat(4,
            std::vector<std::vector<int> >(n_rows,
            std::vector<int>(n_cols, std::numeric_limits<int>::max())));
    std::priority_queue<Robot> queue;
    auto isOutside= [&](int row, int col) -> bool
    {
        return (row < 0) || (col < 0) ||  (row >= n_rows) || (col >= n_cols);
    };
    auto updateHeat = [&](const Robot &position) -> bool
    {
        int direction = static_cast<int>(position.heading);
        if (position.heat >= min_heat[direction][position.row][position.col])
            return false;
        min_heat[direction][position.row][position.col] = position.heat;
        return true;
    };
    auto next = [&](const Robot &robot, MOVE action) -> void
    {
        int direction = static_cast<int>(robot.heading);
        if (action == MOVE::TURN_LEFT)
            direction = (direction == 0)?3:(direction - 1);
        else if (action == MOVE::TURN_RIGHT)
            direction = (direction == 3)?0:(direction + 1);
        
        int new_row = robot.row, new_col = robot.col, new_heat = robot.heat;
        for (int distance = 1; distance < min_distance; ++distance)
        {
            new_row += directions[direction    ];
            new_col += directions[direction + 1];
            if (isOutside(new_row, new_col)) return;
            new_heat += loss[new_row][new_col];
        }
        for (int distance = min_distance; distance <= max_distance; ++distance)
        {
            new_row += directions[direction    ];
            new_col += directions[direction + 1];
            if (isOutside(new_row, new_col)) return;
            new_heat += loss[new_row][new_col];
            Robot new_robot(new_row, new_col, new_heat, static_cast<HEADING>(direction));
            if (updateHeat(new_robot))
                queue.push(new_robot);
        }
    };
    
    next({std::get<0>(start), std::get<1>(start), 0, HEADING::RIGHT}, MOVE::FORWARD);
    next({std::get<0>(start), std::get<1>(start), 0, HEADING::DOWN} , MOVE::FORWARD);
    next({std::get<0>(start), std::get<1>(start), 0, HEADING::LEFT}, MOVE::FORWARD);
    next({std::get<0>(start), std::get<1>(start), 0, HEADING::UP} , MOVE::FORWARD);
    while (!queue.empty())
    {
        Robot current = queue.top();
        queue.pop();
        if (std::make_tuple(current.row, current.col) == goal) return current.heat;
        next(current, MOVE::TURN_LEFT );
        next(current, MOVE::TURN_RIGHT);
    }
    int result = MAX_HEAT;
    for (int row = std::get<0>(goal), col = std::get<1>(goal), i = 0; i < 4; ++i)
        result = std::min(result, min_heat[i][row][col]);
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<std::vector<int> > heat_loss;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                heat_loss.push_back({});
                for (char digit : line)
                    heat_loss.back().push_back(static_cast<int>(digit - '0'));
            }
        }
        file.close();
    }
    // -[ End File Parser ]-----------------------------------------------------
    int n_rows = static_cast<int>(heat_loss.size());
    int n_cols = static_cast<int>(heat_loss[0].size());
    long resultA = minimumHeatLoss(heat_loss, {0, 0}, {n_rows - 1, n_cols - 1}, 1, 3),
         resultB = minimumHeatLoss(heat_loss, {0, 0}, {n_rows - 1, n_cols - 1}, 4, 10);
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

