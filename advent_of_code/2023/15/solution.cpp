#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>
#include "utils/utils.hpp"

long hash(std::string code)
{
    long result = 0;
    for (char letter : code)
        result = (17 * (result + static_cast<long>(letter))) % 256;
    return result;
}

long sumHash(const std::vector<std::string> &information)
{
    long result = 0;
    for (std::string text : information)
        result += hash(text);
    return result;
}

long sumPower(const std::vector<std::string> &information)
{
    struct Lens
    {
        std::string id;
        long focal_length = 0;
    };
    std::vector<Lens> boxes[256];
    auto add = [](std::vector<Lens> &lenses, std::string lens_id, long focal_length)
    {
        for (auto &lens : lenses)
        {
            if (lens.id == lens_id)
            {
                lens.focal_length = focal_length;
                return;
            }
        }
        lenses.push_back({lens_id, focal_length});
    };
    auto remove = [](std::vector<Lens> &lenses, std::string lens_id)
    {
        const int n = static_cast<int>(lenses.size());
        int idx = 0;
        for (; idx < n; ++idx)
            if (lenses[idx].id == lens_id)
                break;
        if (idx == n) return;
        for (int j = idx; j < n - 1; ++j)
            lenses[j] = lenses[j + 1];
        lenses.pop_back();
    };
    for (std::string instruction : information)
    {
        if (instruction.back() == '-')
        {
            std::string lens_id = instruction.substr(0, instruction.size() - 1);
            remove(boxes[hash(lens_id)], lens_id);
        }
        else
        {
            std::string lens_id = instruction.substr(0, instruction.size() - 2);
            long focal_length = static_cast<long>(instruction.back() - '0');
            add(boxes[hash(lens_id)], lens_id, focal_length);
        }
    }
    long result = 0;
    for (size_t i = 0; i < 256; ++i)
        for (long index = 0; auto [lens_id, focal_length] : boxes[i])
            result += (i + 1) * ++index * focal_length;
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<std::string> information;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                for (std::string text : utls::split(line, {","}))
                    information.push_back(text);
            }
        }
        file.close();
    }
    // -[ End File Parser ]-----------------------------------------------------
    long resultA = sumHash(information),
         resultB = sumPower(information);
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

