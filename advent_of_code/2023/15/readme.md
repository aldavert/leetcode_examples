# [Day 15: Lens Library](https://adventofcode.com/2023/day/15)

You are given a list of string separated by commas.

#### Problem A
Implement the hashing function `value = (17 * (value + (int)character)) % 256` where `value` is 0-initialized and `character` traverses through each character of the string.

**Return the sum of all the hash values of all strings.**

#### Problem B
The string actually encodes an operation as `<identifier><operation>[<value>]` where `<identifier>` is the object identifier, `operation` is `=` to add an object and `-` to remove an object, and `value` is the value of the object when it's added into the structure:
- The structure containing 256 different list of object-value tuples. Each object is assigned to a list depending on the **hash of Problem A** applied onto the object `identifier`.
- When adding an object, we first check if the object is already on the list. If it's present, just replace the value. Otherwise, add the `(identifier, value)` pair at the end of the list.
- When removing an object, search the object in the list. If it's not present, do nothing. Otherwise, remove and modify the position of the remaining elements by moving one position forward.

Each object in the structure has a power value associated that is computed as the product between the `box_number` by `position_in_list` by `focal_length`. **Return the sum of all object's power values.**

