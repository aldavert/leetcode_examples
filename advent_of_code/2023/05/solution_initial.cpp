#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>

int main(int argc, char * * argv)
{
    auto split = [](std::string text) -> std::vector<long>
    {
        std::vector<long> result;
        bool new_number = true;
        for (char letter : text)
        {
            if ((letter >= '0') && (letter <= '9'))
            {
                if (new_number)
                {
                    result.push_back(0);
                    new_number = false;
                }
                result.back() = result.back() * 10 + static_cast<long>(letter - '0');
            }
            else new_number = true;
        }
        return result;
    };
    struct MapInfo
    {
        long dst = -1;
        long min = -1;
        long max = -1;
        bool operator<(const MapInfo &other) const { return min < other.min; }
    };
    std::vector<std::vector<MapInfo> > transforms;
    const char * filename = (argc > 1)?argv[1]:"data.txt";
    std::ifstream file(filename);
    std::string line;
    std::tuple<int, int> active_field = {-1, -1};
    std::vector<std::array<long, 8> > info_single;
    std::vector<long> seeds;
    while (std::getline(file, line))
    {
        if (!line.empty())
        {
            if (auto pos = line.find(':'); pos != std::string::npos)
            {
                std::string id = line.substr(0, pos);
                if (id == "seeds")
                {
                    seeds = split(line.substr(pos, std::string::npos));
                    for (long seed : seeds)
                        info_single.push_back({seed, seed, seed, seed,
                                               seed, seed, seed, seed});
                }
                else if (id == "seed-to-soil map")            active_field = {0, 1};
                else if (id == "soil-to-fertilizer map")      active_field = {1, 2};
                else if (id == "fertilizer-to-water map")     active_field = {2, 3};
                else if (id == "water-to-light map")          active_field = {3, 4};
                else if (id == "light-to-temperature map")    active_field = {4, 5};
                else if (id == "temperature-to-humidity map") active_field = {5, 6};
                else if (id == "humidity-to-location map")    active_field = {6, 7};
                else std::cerr << "[ERROR] UNKNOWN FIELD\n";
                transforms.push_back({});
            }
            else
            {
                auto [field_src, field_dst] = active_field;
                if ((field_src < 0) || (field_src > 7)
                ||  (field_dst < 0) || (field_dst > 7))
                {
                    std::cerr << "[ERROR] INVALID FIELD MAP\n";
                    continue;
                }
                auto values = split(line);
                if (values.size() != 3)
                {
                    std::cerr << "[ERROR] INVALID RANGE FORMAT\n";
                    continue;
                }
                long dst = values[0], src = values[1], length = values[2];
                transforms.back().push_back({dst, src, src + length - 1});
                for (auto &seed_info : info_single)
                {
                    if ((seed_info[field_src] >= src)
                    &&  (seed_info[field_src] < src + length))
                        for (int field = field_dst; field < 8; ++field)
                            seed_info[field] = seed_info[field_src] + dst - src;
                }
            }
        }
    }
    file.close();
    long resultA = std::numeric_limits<long>::max(),
         resultB = std::numeric_limits<long>::max();
    for (auto &seed_info : info_single)
        if (seed_info[7] < resultA)
            resultA = seed_info[7];
    for (auto &transform : transforms)
        std::sort(transform.begin(), transform.end());
    // ########################################################################
    // ########################################################################
    bool overlap = false, continuous = true;
    for (auto &transform : transforms)
    {
        if (transform.empty()) continue;
        for (size_t i = 0; i < transform.size() - 1; ++i)
        {
            auto [dst1, min1, max1] = transform[i];
            for (size_t j = i + 1; j < transform.size(); ++j)
            {
                auto [dst2, min2, max2] = transform[j];
                overlap = overlap || (std::max(min1, min2) <= std::min(max1, max2));
            }
        }
        for (size_t i = 1; i < transform.size(); ++i)
            continuous = continuous && (transform[i - 1].max + 1 == transform[i].min);
    }
    if (overlap) std::cout << "[INFO] Some transforms overlap!\n";
    if (continuous) std::cout << "[INFO] All transforms do not overlap and are continuous!\n";
    // ########################################################################
    // ########################################################################
    std::function<long(size_t, long, long)> project =
        [&](size_t idx, long seed_min, long seed_max) -> long
    {
        if (idx == transforms.size())
            return seed_min;
        long result = std::numeric_limits<long>::max();
        for (auto [dst, map_min, map_max] : transforms[idx])
        {
            long inc = dst - map_min;
            if (seed_min < map_min)
            {
                long next = std::min(seed_max, map_min - 1);
                result = std::min(result, project(idx + 1, seed_min, next));
                seed_min = next + 1;
            }
            if (seed_min > seed_max) break;
            if (long next = std::min(seed_max, map_max); seed_min <= next)
            {
                result = std::min(result, project(idx + 1, seed_min + inc, next + inc));
                seed_min = next + 1;
            }
            if (seed_min > seed_max) break;
        }
        if (seed_min <= seed_max)
            result = std::min(result, project(idx + 1, seed_min, seed_max));
        return result;
    };
    for (size_t i = 0; i < seeds.size(); i+= 2)
        resultB = std::min(resultB, project(1, seeds[i], seeds[i] + seeds[i + 1] - 1));
    
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

