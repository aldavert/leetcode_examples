# [Day 5: If You Give A Seed A Fertilizer](https://adventofcode.com/2023/day/5)

The data consists of a text file where first there are given a set of seeds followed by a set of maps:
```
seeds: (<SEED_ID>)+
(
<MAP_TAG>:
(<DESTINATION> <SOURCE> <LENGTH>)+
)+
```
Where `<DESTINATION> <SOURCE> <LENGTH>` defines that coordinates that go from `[<SOURCE>, <SOURCE> + <LENGTH> - 1]` are mapped to coordinates `[<DESTINATION>, <DESTINATION> + <LENGTH> - 1]`.

These maps define a coordinate transform in order: `seed->soil->fertilizer->water->light->temperature->humidity->location`. So, applying the maps **in order** we can convert a seed coordinate into a location coordinate.

The mapping in each set of coordinate maps (e.g. `seed-to-soil map` or `water-to-light map`) **do not overlap** and **they are not continuous** (i.e. there may be gaps).

When a coordinate is not modified by a map, it retains the same value. For example, given
```
seed-to-soil map:
50 98 2
52 50 48
```
seed coordinate `51` is affected by map `(52 50 48)` so it's mapped to `53`, however seed `49` is not affected by any map, so it remains `49`.

#### Problem A
Given the initial set of seeds, map them into their corresponding location and **return the lowest location obtained.**

#### Problem B
Same as before, but now the values in the list of seeds **represent ranges**. The values are processed as pairs where the first value is the beginning of the range and the second value is its length.

Map the seed ranges into locations and **return the lowest location possible.**

