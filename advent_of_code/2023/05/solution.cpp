#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>

struct MapInfo
{
    long dst = -1;
    long min = -1;
    long max = -1;
    bool operator<(const MapInfo &other) const { return min < other.min; }
};

std::vector<long> split(std::string text)
{
    std::vector<long> result;
    bool new_number = true;
    for (char letter : text)
    {
        if ((letter >= '0') && (letter <= '9'))
        {
            if (new_number)
            {
                result.push_back(0);
                new_number = false;
            }
            result.back() = result.back() * 10 + static_cast<long>(letter - '0');
        }
        else new_number = true;
    }
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<std::vector<MapInfo> > transforms;
    std::vector<long> seeds;
    
    // -[ Begin File Parser ]---------------------------------------------------
    const char * filename = (argc > 1)?argv[1]:"data.txt";
    std::ifstream file(filename);
    std::string line;
    while (std::getline(file, line))
    {
        if (!line.empty())
        {
            if (auto pos = line.find(':'); pos != std::string::npos)
            {
                std::string id = line.substr(0, pos);
                if (id == "seeds")
                    seeds = split(line.substr(pos, std::string::npos));
                transforms.push_back({});
            }
            else
            {
                auto values = split(line);
                long dst = values[0], src = values[1], length = values[2];
                transforms.back().push_back({dst, src, src + length - 1});
            }
        }
    }
    file.close();
    // -[ End File Parser ]-----------------------------------------------------
    
    for (auto &transform : transforms)
        std::sort(transform.begin(), transform.end());
    std::function<long(size_t, long, long)> project =
        [&](size_t idx, long seed_min, long seed_max) -> long
    {
        if (idx == transforms.size())
            return seed_min;
        long result = std::numeric_limits<long>::max();
        for (auto [dst, map_min, map_max] : transforms[idx])
        {
            long inc = dst - map_min;
            if (seed_min < map_min)
            {
                long next = std::min(seed_max, map_min - 1);
                result = std::min(result, project(idx + 1, seed_min, next));
                seed_min = next + 1;
            }
            if (seed_min > seed_max) break;
            if (long next = std::min(seed_max, map_max); seed_min <= next)
            {
                result = std::min(result, project(idx + 1, seed_min + inc, next + inc));
                seed_min = next + 1;
            }
            if (seed_min > seed_max) break;
        }
        if (seed_min <= seed_max)
            result = std::min(result, project(idx + 1, seed_min, seed_max));
        return result;
    };
    long resultA = std::numeric_limits<long>::max(),
         resultB = std::numeric_limits<long>::max();
    for (size_t i = 0; i < seeds.size(); ++i)
        resultA = std::min(resultA, project(1, seeds[i], seeds[i]));
    for (size_t i = 0; i < seeds.size(); i+= 2)
        resultB = std::min(resultB, project(1, seeds[i], seeds[i] + seeds[i + 1] - 1));
    
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

