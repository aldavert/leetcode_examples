#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>

enum class HEADING { UP, LEFT, DOWN, RIGHT };

struct Beam
{
    int row;
    int col;
    HEADING direction;
    Beam next(void) const
    {
        switch (direction)
        {
        case HEADING::UP:
            return {row - 1, col, direction};
        case HEADING::LEFT:
            return {row, col - 1, direction};
        case HEADING::DOWN:
            return {row + 1, col, direction};
        case HEADING::RIGHT:
            return {row, col + 1, direction};
        default:
            return {row, col, direction};
        }
    }
    std::vector<Beam> splitHorizontal(void) const
    {
        if ((direction == HEADING::UP) || (direction == HEADING::DOWN))
            return { {row, col - 1, HEADING::LEFT},
                     {row, col + 1, HEADING::RIGHT}};
        else return { next() };
    }
    std::vector<Beam> splitVertical(void) const
    {
        if ((direction == HEADING::LEFT) || (direction == HEADING::RIGHT))
            return { {row - 1, col, HEADING::UP},
                     {row + 1, col, HEADING::DOWN}};
        else return { next() };
    }
    Beam rotateTiltedRight(void) const
    {
        switch (direction)
        {
        case HEADING::UP:
            return {row, col + 1, HEADING::RIGHT};
        case HEADING::LEFT:
            return {row + 1, col, HEADING::DOWN};
        case HEADING::DOWN:
            return {row, col - 1, HEADING::LEFT};
        case HEADING::RIGHT:
            return {row - 1, col, HEADING::UP};
        default:
            return {row, col, direction};
        }
    }
    Beam rotateTiltedLeft(void) const
    {
        switch (direction)
        {
        case HEADING::UP:
            return {row, col - 1, HEADING::LEFT};
        case HEADING::LEFT:
            return {row - 1, col, HEADING::UP};
        case HEADING::DOWN:
            return {row, col + 1, HEADING::RIGHT};
        case HEADING::RIGHT:
            return {row + 1, col, HEADING::DOWN};
        default:
            return {row, col, direction};
        }
    }
};

long energizedCells(Beam start, const std::vector<std::string> &layout)
{
    const int n_rows = static_cast<int>(layout.size());
    const int n_cols = (!layout.empty())?static_cast<int>(layout[0].size()):0;
    struct Direction
    {
        bool up = false;
        bool left = false;
        bool down = false;
        bool right = false;
        bool isActive(HEADING direction) const
        {
            switch (direction)
            {
            case HEADING::UP:
                return up;
            case HEADING::LEFT:
                return left;
            case HEADING::DOWN:
                return down;
            case HEADING::RIGHT:
                return right;
            default:
                return false;
            }
        }
        void update(HEADING direction)
        {
            switch (direction)
            {
            case HEADING::UP:
                up = true;
                break;
            case HEADING::LEFT:
                left = true;
                break;
            case HEADING::DOWN:
                down = true;
                break;
            case HEADING::RIGHT:
                right = true;
                break;
            default:
                break;
            }
        }
        bool isActive(void) const { return up || left || down || right; }
    };
    std::vector<std::vector<Direction> > cells(n_rows, std::vector<Direction>(n_cols));
    std::stack<Beam> beams;
    beams.push(start);
    while (!beams.empty())
    {
        Beam beam = beams.top();
        beams.pop();
        if ((beam.row < 0) || (beam.col < 0)
        ||  (beam.row >= n_rows) || (beam.col >= n_cols)) continue;
        if (cells[beam.row][beam.col].isActive(beam.direction)) continue;
        cells[beam.row][beam.col].update(beam.direction);
        switch (layout[beam.row][beam.col])
        {
        case '/':
            beams.push(beam.rotateTiltedRight());
            break;
        case '\\':
            beams.push(beam.rotateTiltedLeft());
            break;
        case '|':
            for (Beam new_beam : beam.splitVertical())
                beams.push(new_beam);
            break;
        case '-':
            for (Beam new_beam : beam.splitHorizontal())
                beams.push(new_beam);
            break;
        default:
            beams.push(beam.next());
            break;
        }
    }
    
    long result = 0;
    for (const auto &line : cells)
        for (const auto &cell : line)
            result += cell.isActive();
    return result;
}

long maxEnergizedCells(const std::vector<std::string> &layout)
{
    const int n_rows = static_cast<int>(layout.size());
    const int n_cols = (!layout.empty())?static_cast<int>(layout[0].size()):0;
    long result = 0;
    for (int i = 0; i < n_cols; ++i)
    {
        result = std::max(result, energizedCells({0         , i, HEADING::DOWN}, layout));
        result = std::max(result, energizedCells({n_rows - 1, i, HEADING::UP  }, layout));
    }
    for (int i = 0; i < n_rows; ++i)
    {
        result = std::max(result, energizedCells({i, 0         , HEADING::RIGHT}, layout));
        result = std::max(result, energizedCells({i, n_cols - 1, HEADING::LEFT }, layout));
    }
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<std::string> layout;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        while (std::getline(file, line))
        {
            if (!line.empty())
                layout.push_back(line);
        }
        file.close();
    }
    // -[ End File Parser ]-----------------------------------------------------
    long resultA = energizedCells({0, 0, HEADING::RIGHT}, layout),
         resultB = maxEnergizedCells(layout);
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

