# [Day 16: The Floor Will Be Lava](https://adventofcode.com/2023/day/16)

You are given a layout of a machine which is able to change the direction of a beam of light. The machine has five types of cells:
- `.` empty space.
- `/` right tilted mirror that changes the direction of the beam (`right -> up`, `down -> left`, `up -> right` and `left -> down`).
- `\` left tilted mirror that changes the direction of the beam (`right -> down`, `down -> right`, `up -> left` and `left -> up`).
- `-` horizontal splitter that divides vertical (`up` or `down`) beams into two horizontal beams (`left` and `right`). If the beam is horizontal (`left` or `right`), the beam ignores it and keeps going its way.
- `|` vertical splitter that divides vertical (`left` or `right`) beams into two vertical beams (`up` or `down`). If the beam is vertical (`up` or `down`), the beam ignores it and keeps going its way.

#### Problem A
If you have a beam starting at the top-left corner heading right, return **how many cells it will visit at most.**

#### Problem B
If you start a beam at any border cell heading towards the opposite border. Beams at corners go both directions. For example, a beam heading `down` and a beam heading `right` will be launched from the top-left corner. Return **what is the number of cells visited by the beam that visited the most cells.**


