#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

struct Problem
{
    std::vector<std::string> mirrors;
    int n_rows = 0;
    int n_cols = 0;
};

long distance(const std::string &left, const std::string &right)
{
    long result = 0;
    for (size_t i = 0, n = left.size(); i < n; ++i)
        result += left[i] != right[i];
    return result;
}

long reflexionRows(const std::vector<std::string> &mirrors, int n_rows, long expected_error)
{
    for (int row = 1; row < n_rows; ++row)
    {
        long error = 0;
        for (int top = row - 1, bottom = row; (top >= 0) && (bottom < n_rows); --top, ++bottom)
            error += distance(mirrors[top], mirrors[bottom]);
        if (error == expected_error)
            return row;
    }
    return 0;
}

long reflexionColumns(const std::vector<std::string> &mirrors, int n_rows, int n_cols, long expected_error)
{
    std::vector<std::string> transpose(n_cols);
    for (int col = 0; col < n_cols; ++col)
        for (int row = 0; row < n_rows; ++row)
            transpose[col] += mirrors[row][col];
    return reflexionRows(transpose, n_cols, expected_error);
}

long accumulateReflexionRowsAndCols(const std::vector<Problem> &problems, long expected_error)
{
    long result = 0;
    for (auto [mirrors, n_rows, n_cols] : problems)
        result += 100 * reflexionRows(mirrors, n_rows, expected_error)
               +        reflexionColumns(mirrors, n_rows, n_cols, expected_error);
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<Problem> problems;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        bool empty = true;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                if (empty)
                {
                    problems.push_back({});
                    problems.back().n_cols = static_cast<int>(line.size());
                    empty = false;
                }
                ++problems.back().n_rows;
                problems.back().mirrors.push_back(line);
            }
            else empty = true;
        }
        file.close();
    }
    // -[ End File Parser ]-----------------------------------------------------
    long resultA = accumulateReflexionRowsAndCols(problems, 0),
         resultB = accumulateReflexionRowsAndCols(problems, 1);
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

