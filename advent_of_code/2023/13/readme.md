# [Day 13: Point of Incidence](https://adventofcode.com/2023/day/13)

You are given a set of maps where maps cells are `.` empty or `#` occupied. The maps can be folded horizontally and vertically.

#### Problem A
Find the fold that perfectly mirror each map. Each fold gives `row_column` points for vertical folds and `100 * fold_column` points for horizontal folds.

**Return the points accumulated by all maps**.

#### Problem B
**Return the points accumulated by all maps**, but now **find folds that contain a single error** (i.e. a `.` which aligns to a `#` or vice versa).

