#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>

int main(int argc, char * * argv)
{
    auto isSymbol = [](char letter) -> bool
    {
        return (letter != '.') && (!((letter >= '0') && (letter <= '9')));
    };
    auto sumAdjacents = [&](const std::vector<std::string> &schematic) -> long
    {
        const int nrows = static_cast<int>(schematic.size()),
                  ncols = static_cast<int>(schematic[0].size());
        long result = 0;
        for (int row = 0; row < nrows; ++row)
        {
            long value = 0;
            for (int col = 0, begin = 0; col < ncols; ++col)
            {
                if ((schematic[row][col] >= '0') && (schematic[row][col] <= '9'))
                {
                    if (value == 0) begin = col;
                    value = value * 10 + (schematic[row][col] - '0');
                }
                else if (value != 0)
                {
                    bool valid = isSymbol(schematic[row][begin - 1])
                              || isSymbol(schematic[row][col]);
                    for (int i = begin - 1; !valid && (i <= col); ++i)
                        valid = isSymbol(schematic[row - 1][i])
                             || isSymbol(schematic[row + 1][i]);
                    if (valid) result += value;
                    value = 0;
                }
            }
        }
        return result;
    };
    auto sumGearRatio = [&](const std::vector<std::string> &schematic) -> long
    {
        const int nrows = static_cast<int>(schematic.size()),
                  ncols = static_cast<int>(schematic[0].size());
        long result = 0;
        std::map<std::tuple<int, int>, std::vector<long> > gear_map;
        for (int row = 0; row < nrows; ++row)
        {
            long value = 0;
            for (int col = 0, begin = 0; col < ncols; ++col)
            {
                if ((schematic[row][col] >= '0') && (schematic[row][col] <= '9'))
                {
                    if (value == 0) begin = col;
                    value = value * 10 + (schematic[row][col] - '0');
                }
                else if (value != 0)
                {
                    if (schematic[row][begin - 1] == '*')
                        gear_map[std::make_tuple(row, begin - 1)].push_back(value);
                    if (schematic[row][col] == '*')
                        gear_map[std::make_tuple(row, col)].push_back(value);
                    for (int i = begin - 1; i <= col; ++i)
                    {
                        if (schematic[row - 1][i] == '*')
                            gear_map[std::make_tuple(row - 1, i)].push_back(value);
                        if (schematic[row + 1][i] == '*')
                            gear_map[std::make_tuple(row + 1, i)].push_back(value);
                    }
                    value = 0;
                }
            }
        }
        for (auto &[_, values] : gear_map)
            if (values.size() == 2)
                result += values[0] * values[1];
        return result;
    };
    const char * filename = (argc > 1)?argv[1]:"data.txt";
    std::ifstream file(filename);
    std::string line;
    std::vector<std::string> schematic;
    while (std::getline(file, line))
    {
        if (schematic.empty()) schematic.push_back(std::string(line.size() + 2, '.'));
        schematic.push_back("." + line + ".");
    }
    schematic.push_back(std::string(schematic.back().size() + 2, '.'));
    file.close();
    
    long resultA = sumAdjacents(schematic),
         resultB = sumGearRatio(schematic);
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

