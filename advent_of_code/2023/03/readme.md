# [Day 3: Gear Ratios](https://adventofcode.com/2023/day/3)

#### Problem A
You are given an matrix of characters with numbers, symbols and white spaces (represented as `.`). *Return the sum of all numbers which have a symbol at* [Chebyshev distance](https://en.wikipedia.org/wiki/Chebyshev_distance) `1`.

#### Problem B
Using the same input data as before, now *select the numbers pairs which have an* `*` *as neighbor, multiply them and add them up*. Only pairs are considered, if a `*` has three or more neighbors, discard them.


