# [Day 25: Snowverload](https://adventofcode.com/2023/day/25)

You are given a list of nodes and their neighbors of an undirected graph. Find which **edges you can remove** to partition the graph in two so that **the product between the number of nodes of the two resulting graphs is maximized**. At most, you can only remove 3 edges.
