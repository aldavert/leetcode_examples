#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <queue>
#include "utils/utils.hpp"

long maxGraphCutSizeProduct(const std::unordered_map<std::string, std::unordered_set<std::string> > &graph, int max_number_of_edges_cut)
{
    std::vector<std::vector<int> > adjacency_matrix(graph.size(), std::vector<int>(graph.size(), 0));
    std::unordered_map<std::string, int> node_index;
    for (auto [node, neighbors] : graph)
        if (node_index.find(node) == node_index.end())
            node_index[node] = static_cast<int>(node_index.size());
    for (auto [node, neighbors] : graph)
    {
        for (auto n : neighbors)
        {
            adjacency_matrix[node_index[node]][node_index[n]] = 1;
            adjacency_matrix[node_index[n]][node_index[node]] = 1;
        }
    }
    
    long result = 0;
	int n = static_cast<int>(std::size(adjacency_matrix));
    std::vector<int> cut_size(n, 1), w(n);
    for (int ph = 1; ph < n; ++ph)
    {
        for (int i = 0; i < n; ++i) w[i] = adjacency_matrix[0][i];
		size_t s = 0, t = 0;
		for (int it = 0; it < n - ph; ++it)
        {
			w[t] = std::numeric_limits<int>::lowest();
			s = std::exchange(t, std::max_element(w.begin(), w.end()) - w.begin());
			for (int i = 0; i < n; ++i) w[i] += adjacency_matrix[t][i];
		}
        if (w[t] - adjacency_matrix[t][t] <= max_number_of_edges_cut)
            result = std::max(result,
                    static_cast<long>(cut_size[t]) * static_cast<long>(n - cut_size[t]));
        cut_size[s] += cut_size[t];
		for (int i = 0; i < n; ++i) adjacency_matrix[s][i] += adjacency_matrix[t][i];
		for (int i = 0; i < n; ++i) adjacency_matrix[i][s] = adjacency_matrix[s][i];
		adjacency_matrix[0][t] = std::numeric_limits<int>::lowest();
	}
	return result;
}

int main(int argc, char * * argv)
{
    std::unordered_map<std::string, std::unordered_set<std::string> > graph;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                auto parts = utls::split(line, {":"});
                auto node = utls::trim(parts[0]);
                auto neighbors = utls::split(utls::trim(parts[1]));
                for (auto n : neighbors)
                {
                    graph[node].insert(n);
                    graph[n].insert(node);
                }
            }
        }
        file.close();
    }
    // -[ End File Parser ]-----------------------------------------------------
    long resultA = maxGraphCutSizeProduct(graph, 3);
    std::cout << "Problem solution: " << resultA << '\n';
    return EXIT_SUCCESS;
}

