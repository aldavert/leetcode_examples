#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>

std::vector<long> split(std::string text)
{
    std::vector<long> result;
    bool new_number = true;
    for (char letter : text)
    {
        if ((letter >= '0') && (letter <= '9'))
        {
            if (new_number)
            {
                result.push_back(0);
                new_number = false;
            }
            result.back() = result.back() * 10 + static_cast<long>(letter - '0');
        }
        else new_number = true;
    }
    return result;
}

int main(int argc, char * * argv)
{
    struct Info
    {
        long time = -1;
        long distance = -1;
    };
    std::vector<Info> information;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        std::string line;
        std::vector<long> time, distance;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                if (auto pos = line.find(':'); pos != std::string::npos)
                {
                    std::string id = line.substr(0, pos);
                    if (id == "Time")
                        time = split(line.substr(pos, std::string::npos));
                    else if (id == "Distance")
                        distance = split(line.substr(pos, std::string::npos));
                }
            }
        }
        file.close();
        if (time.size() != distance.size())
        {
            std::cout << "[ERROR] Distance and Time information mismatch, cannot load!\n";
            return EXIT_FAILURE;
        }
        for (size_t i = 0; i < time.size(); ++i)
            information.push_back({time[i], distance[i]});
    }
    // -[ End File Parser ]-----------------------------------------------------
    
    auto combinations = [](const std::vector<Info> &info) -> long
    {
        long result = 1;
        for (auto [time, distance] : info)
        {
            double disciminant = static_cast<double>(time * time - 4 * distance);
            if (disciminant < 0) return 0;
            disciminant = std::sqrt(disciminant);
            double sol_min = (static_cast<double>(time) - disciminant) / 2.0,
                   sol_max = (static_cast<double>(time) + disciminant) / 2.0;
            sol_min = std::ceil(sol_min) + (std::ceil(sol_min) == sol_min);
            sol_max = std::floor(sol_max) - (std::floor(sol_max) == sol_max);
            if (sol_min > sol_max) return 0;
            result *= static_cast<long>(sol_max - sol_min) + 1;
        }
        return result;
    };
    std::vector<Info> merged_information = {{0, 0}};
    auto shiftAccumulate = [&](long value, long accum)
    {
        double log_value = std::log10(accum);
        double shift = std::pow(10.0, std::ceil(log_value) + (std::ceil(log_value) == log_value));
        return value * static_cast<long>(shift) + accum;
    };
    for (auto [time, distance] : information)
    {
        merged_information[0].time = shiftAccumulate(merged_information[0].time, time);
        merged_information[0].distance = shiftAccumulate(merged_information[0].distance, distance);
    }
    
    long resultA = combinations(information),
         resultB = combinations(merged_information);
    
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}

