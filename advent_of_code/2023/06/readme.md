# [Day 6: Wait For It](https://adventofcode.com/2023/day/6)

The input data is a set of times in milliseconds and distances in millimeters:
```
Time: (time)+
Distance: (distance)+
```
Each pair time-distance corresponds to a *race* information, where the race lasted *time* milliseconds and the best result was *distance* millimeters.

During your race, your car *charges up* its initial velocity by one millimeter per millisecond. While *charging* the vehicle **does not move**. Once released, the vehicle will start moving (instantaneously) at the *charged up* velocity until no more time is available. The race result will be the total distance traveled by the vehicle.

For example, given `time = 7`:
    - if you *charge* the car for *6 milliseconds*, then the vehicle will travel at *6 millimeters per millisecond* for *1 millisecond*, traversing a total of **6 millimeters**.
    - if you *charge* the car for *3 milliseconds*, then the vehicle will travel at *3 millimeters per millisecond* for *4 milliseconds*, traversing a total of **12 millimeters**.

#### Problem A
Find the total number of *charging* times that will allow you to win each race and **compute the total number of combinations possible.**

#### Problem B
Same as before, but now concatenate all times and distances and treat them as a single race.

