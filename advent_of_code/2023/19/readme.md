# [Day 19: Aplenty](https://adventofcode.com/2023/day/19)

You are given a set of rules which create a decision tree in a four dimensional space. The leaves of the rules tell if a point is accepted or if it is rejected.

After the decision tree, you are also given a set of points.

#### Problem A
From the list of points, select all the points that are **accepted** and return the **sum of their attributes**.

#### Problem B
Since the coordinates of the feature space are restricted to values between `1` and `4000` (both included), compute the **total volume of the accepted regions**.

