#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>
#include <queue>
#include <algorithm>
#include "utils/utils.hpp"

struct Attributes
{
    long x = 0;
    long m = 0;
    long a = 0;
    long s = 0;
};

enum class ATTRIBUTE { X, M, A, S, NONE};

struct Condition
{
    ATTRIBUTE attribute = ATTRIBUTE::NONE;
    bool greater = false;
    long value = 0;
    bool eval(const Attributes &att) const
    {
        switch (attribute)
        {
        case ATTRIBUTE::X:
            return (greater)?(att.x > value):(att.x < value);
        case ATTRIBUTE::M:
            return (greater)?(att.m > value):(att.m < value);
        case ATTRIBUTE::A:
            return (greater)?(att.a > value):(att.a < value);
        case ATTRIBUTE::S:
            return (greater)?(att.s > value):(att.s < value);
        case ATTRIBUTE::NONE:
        default:
            return true;
        }
    }
    std::tuple<Attributes, Attributes, Attributes, Attributes> cut(Attributes tl, Attributes br) const
    {
        Attributes tl_pass, br_pass, tl_fail, br_fail;
        tl_pass = tl;
        br_pass = br;
        tl_fail = tl;
        br_fail = br;
        switch (attribute)
        {
        case ATTRIBUTE::X:
            if (greater)
            {
                tl_pass.x = value + 1;
                br_fail.x = value;
            }
            else
            {
                br_pass.x = value - 1;
                tl_fail.x = value;
            }
            break;
        case ATTRIBUTE::M:
            if (greater)
            {
                tl_pass.m = value + 1;
                br_fail.m = value;
            }
            else
            {
                br_pass.m = value - 1;
                tl_fail.m = value;
            }
            break;
        case ATTRIBUTE::A:
            if (greater)
            {
                tl_pass.a = value + 1;
                br_fail.a = value;
            }
            else
            {
                br_pass.a = value - 1;
                tl_fail.a = value;
            }
            break;
        case ATTRIBUTE::S:
            if (greater)
            {
                tl_pass.s = value + 1;
                br_fail.s = value;
            }
            else
            {
                br_pass.s = value - 1;
                tl_fail.s = value;
            }
            break;
        case ATTRIBUTE::NONE:
        default:
            tl_fail = {1, 1, 1, 1};
            br_fail = {0, 0, 0, 0};
        }
        return {tl_pass, br_pass, tl_fail, br_fail};
    }
};

enum class ACTION { ACCEPT, REJECT, JUMP };

struct Action
{
    ACTION action = ACTION::REJECT;
    std::string next = "";
    bool operator==(const Action &other) const = default;
};

struct RuleToken
{
    Condition condition;
    Action action;
};

auto simplify(std::unordered_map<std::string, std::vector<RuleToken> > rules)
{
    while (true)
    {
        std::unordered_map<std::string, Action> to_remove;
        for (const auto &[name, conditions] : rules)
            if (std::all_of(conditions.begin(), conditions.end(), [&](const auto &token) { return token.action == conditions.back().action; }))
                to_remove[name] = conditions.back().action;
        if (to_remove.empty()) break;
        for (auto &[name, tokens] : rules)
        {
            for (auto &[condition, action] : tokens)
            {
                if (auto search = to_remove.find(action.next);
                    search != to_remove.end())
                {
                    action = search->second;
                }
            }
        }
        for (const auto &[name, token] : to_remove)
            rules.erase(name);
    }
    return rules;
}

bool isAccepted(const std::unordered_map<std::string, std::vector<RuleToken> > &rules,
                const std::string start_point,
                const Attributes &att)
{
    for (const auto &[condition, action] : rules.at(start_point))
    {
        if (condition.eval(att))
        {
            if (action.action == ACTION::JUMP)
                return isAccepted(rules, action.next, att);
            return action.action == ACTION::ACCEPT;
        }
    }
    return false;
}

long sumAcceptedParts(const std::unordered_map<std::string, std::vector<RuleToken> > &rules,
                      const std::vector<Attributes> &parts)
{
    long result = 0;
    for (auto part : parts)
        if (isAccepted(rules, "in", part))
            result += part.x + part.m + part.a + part.s;
    return result;
}


long possibleCombinations(const std::unordered_map<std::string, std::vector<RuleToken> > &rules)
{
    std::function<long(std::string, Attributes, Attributes)> traverse =
        [&](std::string rule, Attributes tl, Attributes br) -> long
    {
        long area = 0;
        for (const auto &[condition, action] : rules.at(rule))
        {
            auto [tl_pass, br_pass, tl_fail, br_fail] = condition.cut(tl, br);
            if (action.action == ACTION::JUMP)
                area += traverse(action.next, tl_pass, br_pass);
            else if (action.action == ACTION::ACCEPT)
                area += std::max<long>(br_pass.x - tl_pass.x + 1, 0)
                      * std::max<long>(br_pass.m - tl_pass.m + 1, 0)
                      * std::max<long>(br_pass.a - tl_pass.a + 1, 0)
                      * std::max<long>(br_pass.s - tl_pass.s + 1, 0);
            tl = tl_fail;
            br = br_fail;
        }
        return area;
    };
    return traverse("in", {1, 1, 1, 1}, {4000, 4000, 4000, 4000});
}

int main(int argc, char * * argv)
{
    std::unordered_map<std::string, std::vector<RuleToken> > rules;
    std::vector<Attributes> parts;
    // -[ Begin File Parser ]---------------------------------------------------
    {
        auto processAction = [](std::string text) -> Action
        {
            if ((text.size() == 1) && (text[0] == 'A'))
                return { ACTION::ACCEPT, "" };
            if ((text.size() == 1) && (text[0] == 'R'))
                return { ACTION::REJECT, "" };
            return { ACTION::JUMP, text };
        };
        std::unordered_map<char, ATTRIBUTE> attribute_lut = {
            {'x', ATTRIBUTE::X },
            {'m', ATTRIBUTE::M },
            {'a', ATTRIBUTE::A },
            {'s', ATTRIBUTE::S } };
        const char * filename = (argc > 1)?argv[1]:"data.txt";
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "File '" << filename << "' not found.\n";
            return EXIT_FAILURE;
        }
        std::string line;
        bool coordinates = false;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                if (coordinates)
                {
                    Attributes attributes;
                    for (auto value : utls::split(line.substr(1, line.size() - 2), {","}))
                    {
                        auto attr = utls::split(value, {"="});
                        long v = std::atol(attr[1].c_str());
                        if      (attr[0] == "x") attributes.x = v;
                        else if (attr[0] == "m") attributes.m = v;
                        else if (attr[0] == "a") attributes.a = v;
                        else if (attr[0] == "s") attributes.s = v;
                    }
                    parts.push_back(attributes);
                }
                else
                {
                    auto part = utls::split(line, {"{"});
                    std::string rule_name = part[0];
                    std::vector<RuleToken> conditions;
                    for (auto partial_rule : utls::split(part[1].substr(0, part[1].size() - 1), {","}))
                    {
                        Condition condition;
                        Action action;
                        if (partial_rule.find(':') != std::string::npos)
                        {
                            auto tokens = utls::split(partial_rule, {":"});
                            condition = { attribute_lut[tokens[0][0]],
                                          tokens[0][1] == '>',
                                          std::atol(tokens[0].substr(2, tokens[0].size() - 2).c_str()) };
                            action = processAction(tokens[1]);
                        }
                        else
                        {
                            condition = { ATTRIBUTE::NONE, false, 0 };
                            action = processAction(partial_rule);
                        }
                        conditions.push_back({condition, action});
                    }
                    rules[rule_name] = conditions;
                }
            }
            else coordinates = true;
        }
        file.close();
    }
    // -[ End File Parser ]-----------------------------------------------------
    
    ////std::cout << "Number of rules: " << rules.size() << '\n';
    ////std::unordered_map<ATTRIBUTE, char> lut_att = {
    ////    { ATTRIBUTE::X, 'X' },
    ////    { ATTRIBUTE::M, 'M' },
    ////    { ATTRIBUTE::A, 'A' },
    ////    { ATTRIBUTE::S, 'S' } };
    ////for (auto [name, conditions] : rules)
    ////{
    ////    std::cout << name << " (" << conditions.size() << "):";
    ////    for (bool next = false; auto [condition, action] : conditions)
    ////    {
    ////        if (next) std::cout << ',';
    ////        next = true;
    ////        if (condition.attribute != ATTRIBUTE::NONE)
    ////        {
    ////            std::cout << " ("
    ////                      << lut_att[condition.attribute]
    ////                      << (condition.greater?'>':'<')
    ////                      << condition.value
    ////                      << ") ->";
    ////        }
    ////        std::cout << ' ';
    ////        if      (action.action == ACTION::ACCEPT) std::cout << "[ACCEPT]";
    ////        else if (action.action == ACTION::REJECT) std::cout << "[REJECT]";
    ////        else std::cout << action.next;
    ////    }
    ////    std::cout << '\n';
    ////}
    ////std::cout << "=========================================================\n";
    ////std::cout << "Number of rules: " << simplify(rules).size() << '\n';
    ////for (auto [name, conditions] : simplify(rules))
    ////{
    ////    std::cout << name << " (" << conditions.size() << "):";
    ////    for (bool next = false; auto [condition, action] : conditions)
    ////    {
    ////        if (next) std::cout << ',';
    ////        next = true;
    ////        if (condition.attribute != ATTRIBUTE::NONE)
    ////        {
    ////            std::cout << " ("
    ////                      << lut_att[condition.attribute]
    ////                      << (condition.greater?'>':'<')
    ////                      << condition.value
    ////                      << ") ->";
    ////        }
    ////        std::cout << ' ';
    ////        if      (action.action == ACTION::ACCEPT) std::cout << "[ACCEPT]";
    ////        else if (action.action == ACTION::REJECT) std::cout << "[REJECT]";
    ////        else std::cout << action.next;
    ////    }
    ////    std::cout << '\n';
    ////}
    ////std::cout << "\n\n";
    ////for (auto part : parts)
    ////{
    ////    std::cout << "{x=" << part.x << ", m=" << part.m << ", a=" << part.a
    ////              << ", s=" << part.s << "}\n";
    ////}
    rules = simplify(rules);
    long resultA = sumAcceptedParts(rules, parts),
         resultB = possibleCombinations(rules);
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}


