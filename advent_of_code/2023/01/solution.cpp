#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <array>

int main(int argc, char * * argv)
{
    auto extractValue = [](std::string line) -> int
    {
        int first = -1, last = -1;
        for (char letter : line)
        {
            if ((letter >= '0') && (letter <= '9'))
            {
                if (first == -1) first = last = letter - '0';
                else last = letter - '0';
            }
        }
        return first * 10 + last;
    };
    auto extractValueAndText = [](std::string line) -> int
    {
        int first = -1, last = -1;
        auto update = [&](int value)
        {
            last = value;
            if (first == -1) first = last;
        };
        std::array<std::string, 10> text_values = {"zero", "one", "two", "three",
            "four", "five", "six", "seven", "eight", "nine"};
        for (size_t i = 0, n = line.size(); i < n; ++i)
        {
            if ((line[i] >= '0') && (line[i] <= '9')) update(line[i] - '0');
            else
            {
                for (int j = 1; j < 10; ++j)
                {
                    if ((line[i] == text_values[j][0])
                    &&  (i + text_values[j].size() <= n)
                    &&  (line.substr(i, text_values[j].size()) == text_values[j]))
                    {
                        update(j);
                        break;
                    }
                }
            }
        }
        return first * 10 + last;
    };
    const char * filename = (argc > 1)?argv[1]:"data.txt";
    std::ifstream file(filename);
    std::string line;
    long resultA = 0, resultB = 0;
    while (std::getline(file, line))
    {
        if (!line.empty())
            resultA += extractValue(line),
            resultB += extractValueAndText(line);
    }
    file.close();
    std::cout << "Problem A solution: " << resultA << '\n';
    std::cout << "Problem B solution: " << resultB << '\n';
    return EXIT_SUCCESS;
}
