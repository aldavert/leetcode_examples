# [Day 1: Trebuchet?!](https://adventofcode.com/2023/day/1)

#### Problem A
You are given a file with multiple alphanumeric strings, for each line extract the number formed by the first and last digit in the string. The **solution** is obtained by adding all values.

*Note:* All number must have two digits, if a line contains a single digit the numerical value corresponds this value concatenated to itself.

#### Problem B
Same as before, but now you also have to consider numbers written as a text string: `['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight, 'nine']`

