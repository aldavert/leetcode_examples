# [Day 3: Crossed Wires](https://adventofcode.com/2019/day/3)

#### Problem A

You are given two strings corresponding to two paths. Find the distance of the wires' crossing that is closer to the origin.

#### Problem B

What is the minimum number of steps (distance between the origin and the current coordinate obtained by following the path) between the origin and the paths' intersections.

