#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <set>
#include <map>
#include <limits>
#define DEBUG 0

#if DEBUG
struct Pixel
{
    unsigned char r = 0;
    unsigned char g = 0;
    unsigned char b = 0;
};

void save(const char * filename, const std::vector<std::vector<Pixel> > &image)
{
    const int height = static_cast<int>(image.size());
    if (height == 0) return;
    const int width  = static_cast<int>(image[0].size());
    if (width == 0) return;
    std::ofstream file(filename, std::ios::binary);
    
    unsigned char fileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
    unsigned char infoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
    unsigned char pad[3] = {0, 0, 0};
    const int filesize = 54 + 3 * height * width;
    fileheader[ 2] = static_cast<unsigned char>(filesize      );
    fileheader[ 3] = static_cast<unsigned char>(filesize >>  8);
    fileheader[ 4] = static_cast<unsigned char>(filesize >> 16);
    fileheader[ 5] = static_cast<unsigned char>(filesize >> 24);
    infoheader[ 4] = static_cast<unsigned char>(   width      );
    infoheader[ 5] = static_cast<unsigned char>(   width >>  8);
    infoheader[ 6] = static_cast<unsigned char>(   width >> 16);
    infoheader[ 7] = static_cast<unsigned char>(   width >> 24);
    infoheader[ 8] = static_cast<unsigned char>(  height      );
    infoheader[ 9] = static_cast<unsigned char>(  height >>  8);
    infoheader[10] = static_cast<unsigned char>(  height >> 16);
    infoheader[11] = static_cast<unsigned char>(  height >> 24);
    file.write(reinterpret_cast<char *>(fileheader), 14);
    file.write(reinterpret_cast<char *>(infoheader), 40);
    for (int row = height - 1; row >= 0; --row)
    {
        std::vector<unsigned char> interlaced_row(width * 3);
        for (int col = 0; col < width; ++col)
        {
            interlaced_row[3 * col + 0] = image[row][col].b;
            interlaced_row[3 * col + 1] = image[row][col].g;
            interlaced_row[3 * col + 2] = image[row][col].r;
        }
        file.write(reinterpret_cast<const char *>(interlaced_row.data()), 3 * width);
        file.write(reinterpret_cast<char *>(pad), (4 - (3 * width) % 4) % 4);
    }
    file.close();
}

std::vector<std::vector<Pixel> > colorize(const std::vector<std::string> &world)
{
    if ((world.size() == 0) || (world[0].size() == 0)) return {};
    const size_t n_rows = world.size(),
                 n_cols = world[0].size();
    std::vector<std::vector<Pixel> > image(n_rows, std::vector<Pixel>(n_cols));
    for (size_t row = 0; row < n_rows; ++row)
    {
        for (size_t col = 0; col < n_cols; ++col)
        {
            if      (world[row][col] == ' ') image[row][col] = { 255, 255, 255 };
            else if (world[row][col] == 'o') image[row][col] = { 0, 0, 0 };
            else if (world[row][col] == 'a') image[row][col] = { 255, 0, 0 };
            else if (world[row][col] == 'b') image[row][col] = { 0, 0, 255 };
            else if (world[row][col] == 'c') image[row][col] = { 255, 0, 255 };
            else image[row][col] = { 255, 0, 0 };
        }
    }
    return image;
}
#endif

int distanceCrossing(const std::vector<std::tuple<char, int> > &first,
                     const std::vector<std::tuple<char, int> > &second)
{
    constexpr int dirs[] = {-1, 0, 1, 0, -1};
    std::set<std::tuple<int, int> > chain;
    int min_distance = std::numeric_limits<int>::max();
#if DEBUG
    int min_x = std::numeric_limits<int>::max(),
        min_y = std::numeric_limits<int>::max(),
        max_x = std::numeric_limits<int>::lowest(),
        max_y = std::numeric_limits<int>::lowest();
    std::vector<std::string> image;
    
    auto updateWindow = [&](int x, int y) -> void
    {
        min_x = std::min(min_x, x);
        min_y = std::min(min_y, y);
        max_x = std::max(max_x, x);
        max_y = std::max(max_y, y);
    };
#endif
    auto buildChain = [&](int x, int y) -> void
    {
#if DEBUG
        image[y - min_y + 1][x - min_x + 1] = 'a';
#endif
        chain.insert({x, y});
    };
    auto checkChain = [&](int x, int y) -> void
    {
#if DEBUG
        image[y - min_y + 1][x - min_x + 1] = 'b';
#endif
        if (auto search = chain.find({x, y}); search != chain.end())
        {
#if DEBUG
            image[y - min_y + 1][x - min_x + 1] = 'c';
#endif
            min_distance = std::min(min_distance, std::abs(x) + std::abs(y));
        }
    };
    auto run = [&](const std::vector<std::tuple<char, int> > &string,
                   auto func) -> void
    {
        int x = 0, y = 0;
        for (auto [direction, distance] : string)
        {
            int d = 0;
            if      (direction == 'l') d = 0;
            else if (direction == 'd') d = 1;
            else if (direction == 'r') d = 2;
            else if (direction == 'u') d = 3;
            for (int steps = 0; steps < distance; ++steps)
            {
                x += dirs[d];
                y += dirs[d + 1];
                func(x, y);
            }
        }
    };
#if DEBUG
    run(first, updateWindow);
    run(second, updateWindow);
    image.resize(max_y - min_y + 3, std::string(max_x - min_x + 3, ' '));
    image[-min_y + 1][-min_x + 1] = 'o';
#endif
    run(first, buildChain);
    run(second, checkChain);
#if DEBUG
    save("wire.bmp", colorize(image));
#endif
    return min_distance;
}

int stepsCrossing(const std::vector<std::tuple<char, int> > &first,
                  const std::vector<std::tuple<char, int> > &second)
{
    constexpr int dirs[] = {-1, 0, 1, 0, -1};
    std::map<std::tuple<int, int>, int> chain;
    int min_distance = std::numeric_limits<int>::max();
    auto buildChain = [&](int x, int y, int steps) -> void
    {
        if (chain.find({x, y}) == chain.end())
            chain[{x, y}] = steps;
    };
    auto checkChain = [&](int x, int y, int steps) -> void
    {
        if (auto search = chain.find({x, y}); search != chain.end())
            min_distance = std::min(min_distance, search->second + steps);
    };
    auto run = [&](const std::vector<std::tuple<char, int> > &string,
                   auto func) -> void
    {
        int x = 0, y = 0, steps = 0;
        for (auto [direction, distance] : string)
        {
            int d = 0;
            if      (direction == 'l') d = 0;
            else if (direction == 'd') d = 1;
            else if (direction == 'r') d = 2;
            else if (direction == 'u') d = 3;
            for (int s = 0; s < distance; ++s)
            {
                x += dirs[d];
                y += dirs[d + 1];
                func(x, y, ++steps);
            }
        }
    };
    run(first, buildChain);
    run(second, checkChain);
    return min_distance;
}

int main(int argc, char * * argv)
{
    std::vector<std::tuple<char, int> > chains[2];
    int expected_distance = -1, expected_steps = -1;
    if (argc <= 2)
    {
        std::string filename = "data.txt";
        if (argc > 1) filename = argv[1];
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
            return EXIT_FAILURE;
        }
        int index = 0;
        for (std::string line; std::getline(file, line); ++index)
        {
            std::istringstream iss(line);
            for (std::string token; std::getline(iss, token, ','); )
            {
                chains[index].push_back({token[0] - 'A' + 'a',
                                         std::stoi(token.substr(1))});
            }
        }
        file.close();
    }
    else if ((argc == 3) && (std::string(argv[1]) == "test"))
    {
        int index = -1;
        try
        {
            index = std::stoi(argv[2]);
        }
        catch (...)
        {
            std::cerr << "[ERROR] Sample index cannot be converted to integer.\n";
            return EXIT_FAILURE;
        }
        if (index == 0)
        {
            chains[0] = {{'r', 8}, {'u', 5}, {'l', 5}, {'d', 3}};
            chains[1] = {{'u', 7}, {'r', 6}, {'d', 4}, {'l', 4}};
            expected_distance = 6;
            expected_steps = 30;
        }
        else if (index == 1)
        {
            chains[0] = {{'r', 75}, {'d', 30}, {'r', 83}, {'u', 83}, {'l', 12},
                         {'d', 49}, {'r', 71}, {'u', 7}, {'l', 72}};
            chains[1] = {{'u', 62}, {'r', 66}, {'u', 55}, {'r', 34}, {'d', 71},
                         {'r', 55}, {'d', 58}, {'r', 83}};
            expected_distance = 159;
            expected_steps = 610;
        }
        else if (index == 2)
        {
            chains[0] = {{'r', 98}, {'u', 47}, {'r', 26}, {'d', 63}, {'r', 33},
                         {'u', 87}, {'l', 62}, {'d', 20}, {'r', 33}, {'u', 53},
                         {'r', 51}};
            chains[1] = {{'u', 98}, {'r', 91}, {'d', 20}, {'r', 16}, {'d', 67},
                         {'r', 40}, {'u', 7}, {'r', 15}, {'u', 6}, {'r', 7}};
            expected_distance = 135;
            expected_steps = 410;
        }
        else
        {
            std::cerr << "[ERROR] Wrong sample index.\n";
            return EXIT_FAILURE;
        }
    }
    int distance = distanceCrossing(chains[0], chains[1]);
    std::cout << "Problem A: " << distance;
    if (expected_distance != -1)
    {
        if (expected_distance == distance) std::cout << " [CORRECT]";
        else std::cout << " [WRONG] (" << expected_distance
                       << " is expected instead).";
    }
    std::cout << '\n';
    int steps = stepsCrossing(chains[0], chains[1]);
    std::cout << "Problem B: " << steps;
    if (expected_steps != -1)
    {
        if (expected_steps == steps) std::cout << " [CORRECT]";
        else std::cout << " [WRONG] (" << expected_steps
                       << " is expected instead).";
    }
    std::cout << '\n';
    return EXIT_SUCCESS;
}

