# [Day 2: 1202 Program Alarm](https://adventofcode.com/2019/day/2)

#### Problem A

You are given a list of integers separated by commas corresponding to a strip of memory containing a program. The machine three instructions, two arithmetic operations, **addition** and **multiplication**, and a instruction to terminate the program. Instruction with opcode `99` indicates that the program must terminate. The arithmetic instructions consists of 4 integer tuples: `<op code>, <left op position>, <right op position>, <result position>` where
- `<op code>` is `1` for **addition** and `2` for **multiplication**,
- `<left op position>` and `<right op position>` are the positions in the *program list* of the left and right operand values.
- `<result position>` is the position in the *program list* where the result is stored.

Every time that an arithmetic instruction is read, the pointer to the current instruction on the list (the *program counter*) moves 4 positions forward.

You have to take the given program, replace the integer in position `1` of the list and replace it by `12` and replace the value in position `2` by value `2`. **What is the value left at position** `0` after the program halts?

#### Problem B

Setting the values of position `1` and `2` to `a` and `b` respectively. Find the values of `a` and `b` that make the program put the value `19690720` on register `0` after running the program. Then, return `100 * a + b`.

