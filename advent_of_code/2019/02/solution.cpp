#include <iostream>
#include <fstream>
#include <vector>
#include <string>

struct Literal
{
    Literal(void) = default;
    Literal(int value) : factor_a(0), factor_b(0), free_term(value) {}
    Literal(int a, int b, int c) : factor_a(a), factor_b(b), free_term(c) {}
    int factor_a = 0;
    int factor_b = 0;
    int free_term = 0;
    inline Literal operator*(const Literal &other)
    {
        if ((factor_a == 0) && (factor_b == 0))
            return { other.factor_a * free_term,
                     other.factor_b * free_term,
                     other.free_term * free_term };
        else if ((other.factor_a == 0) && (other.factor_b == 0))
            return { factor_a * other.free_term,
                     factor_b * other.free_term,
                     free_term * other.free_term };
        else return {};
    }
    inline Literal operator+(const Literal &other)
    {
        return { factor_a + other.factor_a,
                 factor_b + other.factor_b,
                 free_term + other.free_term };
    }
    inline Literal& operator=(int value)
    {
        factor_a = factor_b = 0;
        free_term = value;
        return *this;
    }
    inline int operator*(void) const { return free_term; }
    inline bool operator!=(int value) const { return free_term != value; }
    inline bool operator==(int value) const { return free_term == value; }
};

std::ostream& operator<<(std::ostream &out, const Literal &lit)
{
    bool next = false;
    if (lit.factor_a != 0) { out << lit.factor_a << "·a"; next = true; }
    if (lit.factor_b != 0)
    {
        if (next) out << " + ";
        out << lit.factor_b << "·b";
        next = true;
    }
    if (lit.free_term != 0)
    {
        if (next) out << " + ";
        out << lit.free_term;
    }
    return out;
};

Literal execute(std::vector<Literal> tape, Literal a, Literal b)
{
    const int n = static_cast<int>(tape.size());
    tape[1] = a;
    tape[2] = b;
    for (int pc = 0; (tape[pc] != 99) && (pc >= 0) && (pc < n); pc += 4)
    {
        if (tape[pc] == 1)
            tape[*tape[pc + 3]] = tape[*tape[pc + 1]] + tape[*tape[pc + 2]];
        else if (tape[pc] == 2)
            tape[*tape[pc + 3]] = tape[*tape[pc + 1]] * tape[*tape[pc + 2]];
        else if (tape[pc] == 99)
            break;
        else
        {
            std::cerr << "Unknown op-code " << tape[pc].free_term << '\n';
            return {};
        }
    }
    return tape[0];
}

int main(int argc, char * * argv)
{
    std::vector<Literal> tape;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string token; std::getline(file, token, ','); )
        tape.push_back({std::stoi(token)});
    file.close();
    
    std::cout << "Problem A: " << execute(tape, 12, 2) << '\n';
    auto problem_b = execute(tape, {1, 0, 0}, {0, 1, 0});
    problem_b = problem_b + Literal({0, 0, -19'690'720});
    int noun = -problem_b.free_term / problem_b.factor_a,
        verb = - noun * problem_b.factor_a - problem_b.free_term;
    std::cout << "Problem B: " << 100 * noun + verb << '\n';
    
    return EXIT_SUCCESS;
}

