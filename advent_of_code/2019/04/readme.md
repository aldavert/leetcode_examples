# [Day 4: Secure Container](https://adventofcode.com/2019/day/4)

#### Problem A

Given a range between two integers, you have to find all the integers that fulfill the following conditions:
- It's a six digit number.
- At least two adjacent digits are the same.
- Going front left to right, the digits never decrease.

#### Problem B

Now, instead of *at least two adjacent digits* the condition is that *exactly two digits* are equal.

How many numbers in the range fulfill the conditions?

