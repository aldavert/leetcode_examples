#include <iostream>
#include <string>

int main(int argc, char * * argv)
{
    int minimum = 183564, maximum = 657474;
    if (argc == 3)
    {
        try
        {
            minimum = std::stoi(argv[1]);
            maximum = std::stoi(argv[2]);
        }
        catch (...)
        {
            std::cerr << "[ERROR] Parameter cannot be converted to integer.\n";
            return EXIT_FAILURE;
        }
    }
    int problem_a = 0, problem_b = 0;
    for (int value = minimum; value <= maximum; ++value)
    {
        std::string text = std::to_string(value);
        if (text.size() != 6) continue;
        bool valid = true, equal_burst = false, just_two = false;
        int counter = 0;
        for (char previous = '\0'; char digit : text)
        {
            valid = valid && (digit >= previous);
            if (digit == previous) ++counter;
            else
            {
                equal_burst = equal_burst || (counter >= 2);
                just_two = just_two || (counter == 2);
                counter = 1;
            }
            previous = digit;
        }
        equal_burst = equal_burst || (counter >= 2);
        just_two = just_two || (counter == 2);
        problem_a += valid && equal_burst;
        problem_b += valid && just_two;
    }
    std::cout << "Problem A: " << problem_a << '\n';
    std::cout << "Problem B: " << problem_b << '\n';
    return EXIT_SUCCESS;
}

