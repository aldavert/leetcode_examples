#include <iostream>
#include <fstream>
#include <vector>
#include <string>

int main(int argc, char * * argv)
{
    std::vector<int> values;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (int value = 0; file >> value; )
        values.push_back(value);
    file.close();
    
    int problem_a = 0;
    for (int value : values)
        problem_a += value / 3 - 2;
    std::cout << "Problem A: " << problem_a << '\n';
    
    long problem_b = 0;
    for (int value : values)
    {
        while (true)
        {
            value = value / 3 - 2;
            if (value > 0) problem_b += value;
            else break;
        }
    }
    std::cout << "Problem B: " << problem_b << '\n';
    return EXIT_SUCCESS;
}

