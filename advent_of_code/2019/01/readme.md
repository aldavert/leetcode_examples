# [Day 1: The Tyranny of the Rocket Equation](https://adventofcode.com/2019/day/1)

#### Problem A

You are given a list of integer. You have to calculate divide each number by 3, subtract 2 and accumulate all the results.

#### Problem B

Now, the result of applying the operation (divide it by 3 and subtract 2) is considered as a new value, as long as it is greater than zero. What is the sum of all the values?


