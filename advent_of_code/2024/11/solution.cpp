#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <unordered_map>

#if 1
long numberOfDigits(const std::vector<int> &values, int number_of_iterations)
{
    std::unordered_map<long long, long> memo;
    auto recursiveLine = [&](auto &&self, long long current, int iteration) -> long
    {
        if (iteration >= number_of_iterations) return 1;
        const long long id = (current << 8) + iteration;
        if (auto search = memo.find(id); search != memo.end())
            return search->second;
        
        long result = 0;
        if (current == 0)
            result = self(self, 1, iteration + 1);
        else if (std::string text = std::to_string(current); text.size() % 2 == 0)
        {
            const size_t n = text.size() / 2;
            result  = self(self, std::stoll(text.substr(0, n)), iteration + 1);
            result += self(self, std::stoll(text.substr(n)), iteration + 1);
        }
        else result = self(self, current * 2024, iteration + 1);
        return memo[id] = result;
    };
    long number_of_digits = 0;
    for (int v : values)
        number_of_digits += recursiveLine(recursiveLine, v, 0);
    return number_of_digits;
}
#else
int numberOfDigits(const std::vector<int> &values, int number_of_iterations)
{
    std::queue<long long> line;
    for (int v : values) line.push(v);
    for (int iteration = 0; iteration < number_of_iterations; ++iteration)
    {
        for (size_t i = line.size(); i > 0; --i)
        {
            long long current = line.front();
            line.pop();
            if (current == 0) line.push(1);
            else if (std::string text = std::to_string(current); text.size() % 2 == 0)
            {
                size_t n = text.size() / 2;
                line.push(std::stoll(text.substr(0, n)));
                line.push(std::stoll(text.substr(n)));
            }
            else line.push(current * 2024);
        }
    }
    return static_cast<int>(line.size());
}
#endif

int main(int argc, char * * argv)
{
    std::vector<int> values;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string token; file >> token; )
        values.push_back(std::stoi(token));
    file.close();
    
    std::cout << "Problem A: " << numberOfDigits(values, 25) << '\n';
    std::cout << "Problem B: " << numberOfDigits(values, 75) << '\n';
    return EXIT_SUCCESS;
}

