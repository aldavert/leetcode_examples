# [Day 11: Plutonian Pebbles](https://adventofcode.com/2024/day/11)

#### Problem A

You are given a series of numbers and, at each iteration, you modify them by applying the *first applicable rule* in this list:

- If 0, it becomes 1.
- If it has an even number of digits, split in two numbers.
- Otherwise, multiply the number by `2024`.

Given an initial set of numbers, how many numbers has the list after `25` iterations?

#### Problem B

How many numbers has the list after `75` iterations?

