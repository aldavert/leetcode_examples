# [Day 3: Mull It Over](https://adventofcode.com/2024/day/3)

#### Problem A

You are given a text document with multiple instruction gabbled together. You have to parse all the valid **multiplication** instruction and **return the sum of all valid multiplications**.

A valid multiplication operator has the form of `mul(x,y)` where `x` and `y` are two 1-3 digit integers.

#### Problem B

You have two extra instruction that you need to take into account while parsing the instructions: `do()` and `don't()`. The first enable the use of `mul(x,y)` and the second disables it. Compute the sum of all **valid and enabled** multiplications.

