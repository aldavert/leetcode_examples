#include <iostream>
#include <fstream>

int main(int argc, char * * argv)
{
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    long dot_product_a = 0, dot_product_b = 0;
    constexpr char current[] = "mul(";
    long number[2] = {};
    bool right_op = false, enabled = true;
    for (std::string line; std::getline(file, line); )
    {
        for (size_t position = 0, counter = 0, i = 0; i < line.size(); ++i)
        {
            if (position == 0)
            {
                number[0] = number[1] = 0;
                right_op = false;
                counter = 0;
            }
            if (position < 4)
            {
                if (line[i] == current[position])
                    ++position;
                else position = 0;
            }
            else
            {
                if ((line[i] >= '0') && (line[i] <= '9'))
                {
                    if (++counter > 3) position = 0;
                    number[right_op] = number[right_op] * 10 + line[i] - '0';
                }
                else if (!right_op && (line[i] == ','))
                {
                    right_op = true;
                    counter = 0;
                }
                else if (right_op && (line[i] == ')'))
                {
                    dot_product_a += number[0] * number[1];
                    if (enabled)
                        dot_product_b += number[0] * number[1];
                    position = 0;
                }
                else position = 0;
            }
            if (position != 0) continue;
            if (line.substr(i, 4) == "do()") enabled = true;
            if (line.substr(i, 7) == "don't()") enabled = false;
        }
    }
    file.close();
    std::cout << "Problem A: " << dot_product_a << '\n';
    std::cout << "Problem B: " << dot_product_b << '\n';
    return EXIT_SUCCESS;
}

