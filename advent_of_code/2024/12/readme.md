# [Day 12: Garden Groups](https://adventofcode.com/2024/day/12)

#### Problem A

You are given a map of regions where each cell region is labeled with a single uppercase letter.

Compute the sum of the product between the area and perimeter of the different regions.

#### Problem B

Now, compute the sum of the product between the area and the ***number of sides*** (not the perimeter) of the different regions.

