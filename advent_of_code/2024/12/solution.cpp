#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_set>

std::vector<std::vector<int> > coloring(const std::vector<std::string> &grid)
{
    std::vector<int> id;
    auto find = [&](auto &&self, int u) -> int
    {
        return (id[u] == u)?u:id[u] = self(self, id[u]);
    };
    auto merge = [&](int i, int j) -> int
    {
        int u = find(find, i), v = find(find, j);
        if (u == v) return u;
        else if (u < v) return id[v] = u;
        else return id[u] = v;
    };
    auto newLabel = [&](int &value) -> void
    {
        id.push_back(value = static_cast<int>(id.size()));
    };
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>(grid[0].size());
    std::vector<std::vector<int> > labels(n_rows, std::vector<int>(n_cols, -1));
    labels[0][0] = 0;
    id.push_back(0);
    for (int col = 1; col < n_cols; ++col)
    {
        if (grid[0][col] == grid[0][col - 1])
            labels[0][col] = labels[0][col - 1];
        else newLabel(labels[0][col]);
    }
    for (int row = 1; row < n_rows; ++row)
    {
        if (grid[row][0] == grid[row - 1][0])
            labels[row][0] = labels[row - 1][0];
        else newLabel(labels[row][0]);
        for (int col = 1; col < n_cols; ++col)
        {
            if ((grid[row][col] == grid[row][col - 1])
            &&  (grid[row][col] == grid[row - 1][col]))
            {
                labels[row][col] = merge(labels[row][col - 1], labels[row - 1][col]);
            }
            if (grid[row][col] == grid[row][col - 1])
                labels[row][col] = labels[row][col - 1];
            else if (grid[row][col] == grid[row - 1][col])
                labels[row][col] = labels[row - 1][col];
            else newLabel(labels[row][col]);
        }
    }
    for (int i = 0, n = static_cast<int>(id.size()); i < n; ++i)
        find(find, i);
    for (int row = 0; row < n_rows; ++row)
        for (int col = 0; col < n_cols; ++col)
            labels[row][col] = find(find, labels[row][col]);
    return labels;
}

long priceNormal(std::vector<std::vector<int> > &labels)
{
    const int n_rows = static_cast<int>(labels.size()),
              n_cols = static_cast<int>(labels[0].size());
    int max_label = 0;
    for (int row = 0; row < n_rows; ++row)
        for (int col = 0; col < n_cols; ++col)
            max_label = std::max(max_label, labels[row][col]);
    std::vector<int> area(max_label + 1), perimeter(max_label + 1);
    for (int r = 0; r < n_rows; ++r)
    {
        for (int c = 0; c < n_cols; ++c)
        {
            ++area[labels[r][c]];
            perimeter[labels[r][c]] +=
                  ((r     ==      0) || (labels[r][c] != labels[r - 1][c]))
                + ((c     ==      0) || (labels[r][c] != labels[r][c - 1]))
                + ((r + 1 == n_rows) || (labels[r][c] != labels[r + 1][c]))
                + ((c + 1 == n_cols) || (labels[r][c] != labels[r][c + 1]));
        }
    }
    int price = 0;
    for (int i = 0; i <= max_label; ++i)
        price += area[i] * perimeter[i];
    return price;
}

long priceDiscount(std::vector<std::vector<int> > &labels)
{
    const int n_rows = static_cast<int>(labels.size()),
              n_cols = static_cast<int>(labels[0].size()),
              T = 0, B = n_rows - 1, L = 0, R = n_cols - 1;
    int max_label = 0;
    for (int row = 0; row < n_rows; ++row)
        for (int col = 0; col < n_cols; ++col)
            max_label = std::max(max_label, labels[row][col]);
    std::vector<int> area(max_label + 1), perimeter(max_label + 1);
    for (int r = 0; r < n_rows; ++r)
    {
        for (int c = 0; c < n_cols; ++c)
        {
            bool c0 = (r == T) || (labels[r][c] != labels[r - 1][c]),
                 c1 = (c == L) || (labels[r][c] != labels[r][c - 1]),
                 c2 = (r == B) || (labels[r][c] != labels[r + 1][c]),
                 c3 = (c == R) || (labels[r][c] != labels[r][c + 1]),
                 d0 = (r != T) && (c != L) && (labels[r][c] != labels[r - 1][c - 1]),
                 d1 = (r != T) && (c != R) && (labels[r][c] != labels[r - 1][c + 1]),
                 d2 = (r != B) && (c != R) && (labels[r][c] != labels[r + 1][c + 1]),
                 d3 = (r != B) && (c != L) && (labels[r][c] != labels[r + 1][c - 1]);
            ++area[labels[r][c]];
            perimeter[labels[r][c]] += (c0 && c1)
                                    +  (c1 && c2)
                                    +  (c2 && c3)
                                    +  (c3 && c0)
                                    +  (d0 && !(c1 || c0))
                                    +  (d1 && !(c0 || c3))
                                    +  (d2 && !(c3 || c2))
                                    +  (d3 && !(c2 || c1));
        }
    }
    int price = 0;
    for (int i = 0; i <= max_label; ++i)
        price += area[i] * perimeter[i];
    return price;

}

int main(int argc, char * * argv)
{
    std::vector<std::string> grid;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        grid.push_back(line);
    file.close();
    auto labels = coloring(grid);
    std::cout << "Problem A: " << priceNormal(labels) << '\n';
    std::cout << "Problem B: " << priceDiscount(labels) << '\n';
    
    return EXIT_SUCCESS;
}

