# [Day 15: Warehouse Woes](https://adventofcode.com/2024/day/15)

#### Problem A

You are given a map with walls `#`, a robot `@` and obstacles `O`. You are also given a set of instructions indicating the moves followed by the robot `<` left, `^` up, `>` right and `v` down. The robot can push objects `O` but gets stuck when pushing a wall `#`. Objects can only be pushed when they are not stopped by a wall.

Once all instructions have been processed, what is the summation of all the coordinate points of the objects? Coordinate points are computed as `100 * <row> + <column>`.

#### Problem B

Now, you have to solve the same problem but objects double their width:
- Wall tile `#` converts to `##`.
- Object tile `O` converts to `[]`
- Free space tile `.` converts to `..`
- Robot tile `@` converts to `@.`

Also, objects `[]` move together as a single object, so when the robot pushes up/down it can push multiple boxes at once.

Once all instructions have been processed, what is the summation of all the coordinate points of the objects?

