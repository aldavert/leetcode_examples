#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stack>

int coordinateScore(const std::vector<std::string> &grid)
{
    int sum_coordinates = 0;
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>(grid[0].size());
    for (int row = 0; row < n_rows; ++row)
        for (int col = 0; col < n_cols; ++col)
            if ((grid[row][col] == 'O') || (grid[row][col] == '['))
                sum_coordinates += 100 * row + col;
    return sum_coordinates;
}

std::pair<int, int> robotPosition(const std::vector<std::string> &grid)
{
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>(grid[0].size());
    for (int row = 0; row < n_rows; ++row)
        for (int col = 0; col < n_cols; ++col)
            if (grid[row][col] == '@') return {row, col};
    return {-1, -1};
};

std::vector<std::string> execute(std::vector<std::string> grid,
                                 const std::string &instructions)
{
    auto [y, x] = robotPosition(grid);
    grid[y][x] = '.';
    for (char dir : instructions)
    {
        if (dir == '<') // Left
        {
            if (grid[y][x - 1] == '.') { --x; continue; }
            if (grid[y][x - 1] == '#') continue;
            int p = x - 1;
            while (grid[y][p] == 'O') --p;
            if (grid[y][p] == '#') continue;
            grid[y][p] = 'O';
            grid[y][--x] = '.';
        }
        else if (dir == '^') // Up
        {
            if (grid[y - 1][x] == '.') { --y; continue; }
            if (grid[y - 1][x] == '#') continue;
            int p = y - 1;
            while (grid[p][x] == 'O') --p;
            if (grid[p][x] == '#') continue;
            grid[p][x] = 'O';
            grid[--y][x] = '.';
        }
        else if (dir == '>') // Right
        {
            if (grid[y][x + 1] == '.') { ++x; continue; }
            if (grid[y][x + 1] == '#') continue;
            int p = x + 1;
            while (grid[y][p] == 'O') ++p;
            if (grid[y][p] == '#') continue;
            grid[y][p] = 'O';
            grid[y][++x] = '.';
        }
        else if (dir == 'v') // Down
        {
            if (grid[y + 1][x] == '.') { ++y; continue; }
            if (grid[y + 1][x] == '#') continue;
            int p = y + 1;
            while (grid[p][x] == 'O') ++p;
            if (grid[p][x] == '#') continue;
            grid[p][x] = 'O';
            grid[++y][x] = '.';
        }
        else
        {
            std::cerr << "[ERROR] Unexpected direction: " << dir << '\n';
            return {};
        }
    }
    grid[y][x] = '@';
    //for (std::string row : grid)
    //    std::cout << row << '\n';
    return grid;
}

std::vector<std::string> executeDouble(const std::vector<std::string> grid_original,
                                       const std::string &instructions)
{
    std::vector<std::string> grid;
    for (const auto &row : grid_original)
    {
        grid.push_back("");
        for (char symbol : row)
        {
            if      (symbol == '#') grid.back() += "##";
            else if (symbol == 'O') grid.back() += "[]";
            else if (symbol == '@') grid.back() += "@.";
            else grid.back() += "..";
        }
    }
    auto [y, x] = robotPosition(grid);
    grid[y][x] = '.';
    for (char dir : instructions)
    {
        if (dir == '<') // Left
        {
            if (grid[y][x - 1] == '.') { --x; continue; }
            if (grid[y][x - 1] == '#') continue;
            int p = x - 1;
            while ((grid[y][p] == '[') || (grid[y][p] == ']')) --p;
            if (grid[y][p] == '#') continue;
            grid[y][--x] = '.';
            for (int k = p; k < x; k += 2)
            {
                grid[y][k] = '[';
                grid[y][k + 1] = ']';
            }
        }
        else if (dir == '>') // Right
        {
            if (grid[y][x + 1] == '.') { ++x; continue; }
            if (grid[y][x + 1] == '#') continue;
            int p = x + 1;
            while ((grid[y][p] == '[') || (grid[y][p] == ']')) ++p;
            if (grid[y][p] == '#') continue;
            grid[y][++x] = '.';
            for (int k = x + 1; k <= p; k += 2)
            {
                grid[y][k] = '[';
                grid[y][k + 1] = ']';
            }
        }
        else if (dir == '^') // Up
        {
            if (grid[y - 1][x] == '.') { --y; continue; }
            if (grid[y - 1][x] == '#') continue;
            std::stack<std::pair<int, int> > active;
            std::vector<std::pair<int, int> > to_move;
            active.push({y - 1, x - (grid[y - 1][x] == ']')});
            bool valid = true;
            while (valid && !active.empty())
            {
                auto [py, px] = active.top();
                active.pop();
                if ((grid[py - 1][px] == '#') || (grid[py - 1][px + 1] == '#'))
                {
                    valid = false;
                    break;
                }
                to_move.push_back({py, px});
                if (grid[py - 1][px] == ']') active.push({py - 1, px - 1});
                if (grid[py - 1][px] == '[') active.push({py - 1, px});
                if (grid[py - 1][px + 1] == '[') active.push({py - 1, px + 1});
            }
            if (!valid) continue;
            for (auto [py, px] : to_move)
                grid[py][px] = grid[py][px + 1] = '.';
            for (auto [py, px] : to_move)
            {
                grid[py - 1][px] = '[';
                grid[py - 1][px + 1] = ']';
            }
            --y;
        }
        else if (dir == 'v') // Down
        {
            if (grid[y + 1][x] == '.') { ++y; continue; }
            if (grid[y + 1][x] == '#') continue;
            std::stack<std::pair<int, int> > active;
            std::vector<std::pair<int, int> > to_move;
            active.push({y + 1, x - (grid[y + 1][x] == ']')});
            bool valid = true;
            while (valid && !active.empty())
            {
                auto [py, px] = active.top();
                active.pop();
                if ((grid[py + 1][px] == '#') || (grid[py + 1][px + 1] == '#'))
                {
                    valid = false;
                    break;
                }
                to_move.push_back({py, px});
                if (grid[py + 1][px] == ']') active.push({py + 1, px - 1});
                if (grid[py + 1][px] == '[') active.push({py + 1, px});
                if (grid[py + 1][px + 1] == '[') active.push({py + 1, px + 1});
            }
            if (!valid) continue;
            for (auto [py, px] : to_move)
                grid[py][px] = grid[py][px + 1] = '.';
            for (auto [py, px] : to_move)
            {
                grid[py + 1][px] = '[';
                grid[py + 1][px + 1] = ']';
            }
            ++y;
        }
        else
        {
            std::cerr << "[ERROR] Unexpected direction: " << dir << '\n';
            return {};
        }
    }
    //grid[y][x] = '@';
    //for (std::string row : grid)
    //    std::cout << row << '\n';
    return grid;
}

int main(int argc, char * * argv)
{
    std::vector<std::string> grid;
    std::string instructions;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.find('#') != std::string::npos)
            grid.push_back(line);
        else instructions += line;
    }
    file.close();
    auto processed_grid = execute(grid, instructions);
    std::cout << "Problem A: " << coordinateScore(processed_grid) << '\n';
    auto double_grid = executeDouble(grid, instructions);
    std::cout << "Problem B: " << coordinateScore(double_grid) << '\n';
    return EXIT_SUCCESS;
}

