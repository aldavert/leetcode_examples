#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <numeric>

struct Trajectory
{
    int px = 0;
    int py = 0;
    int dx = 0;
    int dy = 0;
};

int main(int argc, char * * argv)
{
    std::vector<Trajectory> objects;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        int px = 0, py = 0, dx = 0, dy = 0;
        auto search = line.find(',');
        px = std::stoi(line.substr(2, search - 2));
        auto next = line.find(' ', search);
        py = std::stoi(line.substr(search + 1, next - search));
        search = line.find('=', next);
        next = line.find(',', search);
        dx = std::stoi(line.substr(search + 1, next - search));
        dy = std::stoi(line.substr(next + 1));
        objects.push_back({px, py, dx, dy});
    }
    file.close();
    const int n_rows = (objects.size() > 100)?103:7,
              n_cols = (objects.size() > 100)?101:11;
    std::vector<std::vector<int> > count(n_rows, std::vector<int>(n_cols));
    for (auto [px, py, dx, dy] : objects)
    {
        for (int i = 0; i < 100; ++i)
        {
            px += dx;
            py += dy;
            if (px >= n_cols) px -= n_cols;
            if (px <       0) px += n_cols;
            if (py >= n_rows) py -= n_rows;
            if (py <       0) py += n_rows;
        }
        count[py][px] += 1;
    }
    long accum = 0, problem_a = 1;
    for (int row = 0; row < n_rows / 2; ++row)
        for (int col = 0; col < n_cols / 2; ++col)
            accum += count[row][col];
    problem_a *= accum;
    accum = 0;
    for (int row = n_rows / 2 + 1; row < n_rows; ++row)
        for (int col = 0; col < n_cols / 2; ++col)
            accum += count[row][col];
    problem_a *= accum;
    accum = 0;
    for (int row = 0; row < n_rows / 2; ++row)
        for (int col = n_cols / 2 + 1; col < n_cols; ++col)
            accum += count[row][col];
    problem_a *= accum;
    accum = 0;
    for (int row = n_rows / 2 + 1; row < n_rows; ++row)
        for (int col = n_cols / 2 + 1; col < n_cols; ++col)
            accum += count[row][col];
    problem_a *= accum;
    std::cout << "Problem A: " << problem_a << '\n';
    
    // The parameters of this loop have been selected by first doing a visual
    // inspection of the generated maps and selecting the value where it seemed
    // to appear a sort of pattern that is different than random noise.
    //for (int t = 1; t < 10000; ++t)
    for (int t = 18; t < 10000; t += 101)
    {
        std::vector<std::vector<int> > positions(n_rows, std::vector<int>(n_cols));
        for (auto [px, py, dx, dy] : objects)
        {
            for (int i = 0; i < t; ++i)
            {
                px += dx;
                py += dy;
                if (px >= n_cols) px -= n_cols;
                if (px <       0) px += n_cols;
                if (py >= n_rows) py -= n_rows;
                if (py <       0) py += n_rows;
            }
            positions[py][px] += 1;
        }
        bool possible = false;
        for (auto &row : positions)
        {
            possible = possible || (std::accumulate(row.begin(), row.end(), 0) > 20);
        }
        if (possible)
#if 1
            std::cout << "Problem B: " << t << '\n';
#else
        {
            std::cout << "\nTIME: " << t << '\n';
            std::cout << "=========================================================\n";
            for (auto &row : positions)
            {
                for (auto val : row)
                {
                    std::cout << ((val != 0)?std::to_string(val):" ");
                }
                std::cout << '\n';
            }
            std::cout << "=========================================================\n";
        }
#endif
    }
    
    return EXIT_SUCCESS;
}

