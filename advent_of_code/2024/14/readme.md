# [Day 14: Restroom Redoubt](https://adventofcode.com/2024/day/14)

#### Problem A

You are given a list with the information about object trajectories. Each entry of the list contains the initial position of the object and the step to get to the next location. The objects are within a map of `101x103` tiles and, when an object hits a side, it teleports to the other side and continues moving as usual.

First, calculate the location visited by the object after 100 iterations. Then, divide the grid into four quadrants and compute the product between the number of visited locations in each quadrant. That is the expected result.

#### Problem B

Looking at the locations of objects, return the first time value where the objects draw  Christmas tree on the map:

```
1111111111111111111111111111111
1                             1
1                             1
1                             1
1                             1
1              1              1
1             111             1
1            11111            1
1           1111111           1
1          111111111          1
1            11111            1
1           1111111           1
1          111111111          1
1         11111111111         1
1        1111111111111        1
1          111111111          1
1         11111111111         1
1        1111111111111        1
1       111111111111111       1
1      11111111111111111      1
1        1111111111111        1
1       111111111111111       1
1      11111111111111111      1
1     1111111111111111111     1
1    111111111111111111111    1
1             111             1
1             111             1
1             111             1
1                             1
1                             1
1                             1
1                             1
1111111111111111111111111111111
```
