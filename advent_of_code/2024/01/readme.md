# [Day 1: Historian Hysteria](https://adventofcode.com/2024/day/1)

# Problem A

You are given to list of numbers. Sort them and return the sum of the distance between the elements of the two lists.

# Problem B

Now, return the sum the product between the value and its frequencies in both lists.


