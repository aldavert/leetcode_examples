#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include "utils/split.hpp"
#include "utils/map.hpp"

int main([[maybe_unused]] int argc, [[maybe_unused]] char * * argv)
{
    auto to_int = [](auto in) -> int { return std::stoi(in); };
    std::ifstream file("data.txt");
    std::vector<int> list_a, list_b;
    std::unordered_map<int, int> set_a, set_b;
    for (std::string line; std::getline(file, line); )
    {
        std::vector<int> values = utls::map(to_int, utls::split(line));
        list_a.push_back(values[0]);
        list_b.push_back(values[1]);
        ++set_a[values[0]];
        ++set_b[values[1]];
    }
    file.close();
    std::sort(list_a.begin(), list_a.end());
    std::sort(list_b.begin(), list_b.end());
    int problem_a = 0;
    for (size_t i = 0; i < list_a.size(); ++i)
        problem_a += std::abs(list_a[i] - list_b[i]);
    std::cout << "Problem A: " << problem_a << '\n';
    int problem_b = 0;
    for (auto [value, frequency] : set_a)
        if (auto search = set_b.find(value); search != set_b.end())
            problem_b += value * frequency * search->second;
    std::cout << "Problem B: " << problem_b << '\n';
    
    return EXIT_SUCCESS;
}

