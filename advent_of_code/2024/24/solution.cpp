#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <limits>
#include <unordered_map>
#include <set>

enum class ID { AND, OR, XOR };
struct Component
{
    ID id;
    std::string inputs[2] = {"", ""};
    std::string output = "";
};

long run(const std::vector<Component> &components,
         std::unordered_map<std::string, bool> variables)
{
    std::vector<bool> enable(components.size(), true);
    int number_of_components = static_cast<int>(components.size());
    while (number_of_components > 0)
    {
        int previous_number_of_components = number_of_components;
        for (size_t idx = 0; idx < components.size(); ++idx)
        {
            if (enable[idx])
            {
                auto first = variables.find(components[idx].inputs[0]);
                if (first == variables.end()) continue;
                auto second = variables.find(components[idx].inputs[1]);
                if (second == variables.end()) continue;
                
                if      (components[idx].id == ID::AND)
                    variables[components[idx].output] = first->second && second->second;
                else if (components[idx].id == ID::OR )
                    variables[components[idx].output] = first->second || second->second;
                else if (components[idx].id == ID::XOR)
                    variables[components[idx].output] = first->second ^ second->second;
                enable[idx] = false;
                --number_of_components;
            }
        }
        if (previous_number_of_components == number_of_components)
            return std::numeric_limits<long>::lowest();
    }
    long result = 0;
    for (auto [name, value] : variables)
    {
        if (name[0] != 'z') continue;
        result = result | (static_cast<long>(value) << std::stol(name.substr(1)));
    }
    return result;
}

std::string swapped(const std::vector<Component> &components)
{
    const std::vector<ID> checkA = {ID::AND, ID::XOR}, checkB = {ID::XOR, ID::AND},
                          checkC = {ID::OR};
    auto isInput = [](const std::string &var) -> bool
    {
        return (var[0] == 'x') || (var[0] == 'y');
    };
    std::unordered_map<std::string, std::vector<size_t> > use;
    for (size_t i = 0; i < components.size(); ++i)
        use[components[i].inputs[0]].push_back(i),
        use[components[i].inputs[1]].push_back(i);
    std::set<std::string> swapped;
    for (const auto &component : components)
    {
        std::string left = component.inputs[0],
                    right = component.inputs[1],
                    result = component.output;
        if ((result == "z45") || (left == "x00")) continue;
        std::vector<ID> operations;
        switch (component.id)
        {
        case ID::XOR:
            if (isInput(left))
            {
                if (result == "z00") continue;
                if ((!isInput(right)) || (result[0] == 'z'))
                    swapped.insert(result);
                for (size_t i : use[result])
                    operations.push_back(components[i].id);
                if ((operations != checkA) && (operations != checkB))
                    swapped.insert(result);
            }
            else if (result[0] != 'z')
                swapped.insert(result);
            break;
        case ID::AND:
            if (isInput(left) && !isInput(right))
                swapped.insert(result);
            for (size_t i : use[result])
                operations.push_back(components[i].id);
            if (operations != checkC)
                swapped.insert(result);
            break;
        case ID::OR:
            if (isInput(left) || isInput(right))
                swapped.insert(result);
            for (size_t i : use[result])
                operations.push_back(components[i].id);
            if ((operations != checkA) && (operations != checkB))
                swapped.insert(result);
            break;
        default:
            break;
        }
    }
    std::string result;
    for (bool next = false; std::string name : swapped)
    {
        if (next) [[likely]] result += ',';
        next = true;
        result += name;
    }
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<Component> components;
    std::unordered_map<std::string, bool> variables;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (auto separator = line.find(':'); separator != std::string::npos)
            variables[line.substr(0, separator)] = line.substr(separator + 2) == "1";
        else
        {
            auto arrow = line.find("->");
            
            if (arrow == std::string::npos) continue;
            if (auto op = line.find("AND"); op != std::string::npos)
            {
                components.push_back({ID::AND,
                        {line.substr(0, op - 1),
                        line.substr(op + 4, arrow - 1 - op - 4)},
                        line.substr(arrow + 3)});
            }
            else if (op = line.find("XOR"); op != std::string::npos)
            {
                components.push_back({ID::XOR,
                        {line.substr(0, op - 1),
                        line.substr(op + 4, arrow - 1 - op - 4)},
                        line.substr(arrow + 3)});
            }
            else if (op = line.find("OR"); op != std::string::npos)
            {
                components.push_back({ID::OR,
                        {line.substr(0, op - 1),
                        line.substr(op + 3, arrow - 1 - op - 3)},
                        line.substr(arrow + 3)});
            }
        }
    }
    file.close();
    
    std::cout << "Problem A: " << run(components, variables) << '\n';
    std::cout << "Problem B: " << swapped(components) << '\n';
#if 0
    for (auto component : components)
    {
        std::cout << component.inputs[0];
        if      (component.id == ID::AND) std::cout << "&&";
        else if (component.id == ID::OR ) std::cout << "||";
        else if (component.id == ID::XOR) std::cout << "^";
        std::cout << component.inputs[1] << "=" << component.output << '\n';
    }
#endif
    return EXIT_SUCCESS;
}

