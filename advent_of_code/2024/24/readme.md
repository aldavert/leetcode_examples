# [Day 24: Crossed Wires](https://adventofcode.com/2024/day/24)

#### Problem A

You are given a set of input signals and a list of logic gates (`AND`, `OR` and `XOR`). The gate circuit outputs are a group of variables starting with `z` and ending with a two digit number (e.g. `z00`, `z01`, `z02`, ...). These outputs correspond to the bits of a integer. What is this integer?

#### Problem B

The given circuit actually corresponds to an adder, which adds the binary inputs starting with `x` and `y` and outputs the addition in the outputs starting with `z`. Unfortunately, some of the output labels of the components have been swapped and the output is not correct.

Find all the outputs that have been swapped and, return a list with these output labels separated by comas.

