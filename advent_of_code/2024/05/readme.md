# [Day 5: Print Queue](https://adventofcode.com/2024/day/5)

#### Problem A

You are given a set of rules that set the precedence between pairs of integers. For example, `56|54` means that number `56` goes before `54` (`56 < 54`). After the rules, you are given a set of integer lists.

Your task is to first filter out lists that are not ordered properly (i.e. lists that do not follow the previous rules) and the compute the **addition of the middle element** of the remaining valid lists.

#### Problem B

Resort the filtered out elements so they follow the ordering rules and now, compute the **addition of the middle element** of the reordered lists.


