#include <iostream>
#include <fstream>
#include <algorithm>
#include <bitset>
#include <vector>

int main(int argc, char * * argv)
{
    std::vector<std::vector<int> > lists;
    std::bitset<100> smaller[100];
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be loaded.\n";
        return EXIT_SUCCESS;
    }
    for (std::string line; std::getline(file, line); )
    {
        auto search = line.find('|');
        if (search != std::string::npos)
        {
            const int left = std::stoi(line.substr(0, search)),
                      right = std::stoi(line.substr(search + 1, line.size() - search));
            smaller[left][right] = true;
        }
        else if (line.find(',') != std::string::npos)
        {
            lists.push_back({});
            int value = 0;
            for (char symbol : line)
            {
                if ((symbol >= '0') && (symbol <= '9'))
                    value = value * 10 + symbol - '0';
                else
                {
                    lists.back().push_back(value);
                    value = 0;
                }
            }
            lists.back().push_back(value);
        }
    }
    auto cmp = [&](int l, int r) -> bool
    {
        return smaller[l][r];
    };
    int problem_a = 0, problem_b = 0;
    for (const auto &l : lists)
    {
        std::vector<int> sorted_copy = l;
        std::sort(sorted_copy.begin(), sorted_copy.end(), cmp);
        if (l == sorted_copy)
            problem_a += l[l.size() / 2];
        else problem_b += sorted_copy[l.size() / 2];
    }
    std::cout << "Problem A: " << problem_a << '\n';
    std::cout << "Problem B: " << problem_b << '\n';
    return EXIT_SUCCESS;
}

