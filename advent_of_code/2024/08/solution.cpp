#include <iostream>
#include <fstream>
#include <vector>

int numberAntinodes(std::vector<std::string> grid, bool armonics)
{
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>(grid[0].size());
    std::vector<std::string> occupied(n_rows, std::string(n_cols, '.'));
    std::vector<std::tuple<int, int> > antennas[128];
    for (int row = 0; row < n_rows; ++row)
        for (int col = 0; col < n_cols; ++col)
            if (grid[row][col] != '.')
                antennas[static_cast<int>(grid[row][col])].push_back({row, col});
    int unique = 0;
    for (size_t k = 0; k < 128; ++k)
    {
        if (antennas[k].size() < 2) continue;
        for (size_t i = 0; i < antennas[k].size(); ++i)
        {
            for (size_t j = i + 1; j < antennas[k].size(); ++j)
            {
                auto [y1, x1] = antennas[k][i];
                auto [y2, x2] = antennas[k][j];
                int dx = x1 - x2, dy = y1 - y2;
                int nx = x1 + dx, ny = y1 + dy;
                while ((nx >= 0) && (nx < n_cols) && (ny >= 0) && (ny < n_rows))
                {
                    unique += occupied[ny][nx] == '.';
                    occupied[ny][nx] = '#';
                    nx += dx;
                    ny += dy;
                    if (!armonics) break;
                }
                nx = x2 - dx, ny = y2 - dy;
                while ((nx >= 0) && (nx < n_cols) && (ny >= 0) && (ny < n_rows))
                {
                    unique += occupied[ny][nx] == '.';
                    occupied[ny][nx] = '#';
                    nx -= dx;
                    ny -= dy;
                    if (!armonics) break;
                }
                if (armonics)
                {
                    unique += occupied[y1][x1] == '.';
                    occupied[y1][x1] = '#';
                    unique += occupied[y2][x2] == '.';
                    occupied[y2][x2] = '#';
                }
            }
        }
    }
    return unique;
}

int main(int argc, char * * argv)
{
    std::vector<std::string> grid;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cout << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; file >> line; )
        grid.push_back(line);
    file.close();
    
    std::cout << "Problem A: " << numberAntinodes(grid, false) << '\n';
    std::cout << "Problem B: " << numberAntinodes(grid, true) << '\n';
    
    return EXIT_SUCCESS;
}

