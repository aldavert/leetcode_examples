# [Day 19: Linen Layout](https://adventofcode.com/2024/day/19)

#### Problem A

You are given a list of patterns followed by a set of text strings.

How many text strings can be formed by using the patterns? A pattern can be used as many times as needed.

#### Problem B

How many different ways the strings can be formed?

