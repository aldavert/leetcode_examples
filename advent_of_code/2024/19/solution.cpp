#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <unordered_set>
#include <unordered_map>

int howManyCanBeFormed(std::unordered_set<std::string> patterns,
                       std::vector<std::string> queries)
{
    size_t longest_size = 0;
    for (const std::string &current : patterns)
        longest_size = std::max(longest_size, current.size());
    auto isPossible = [&](auto &&self, size_t idx, const std::string &query) -> bool
    {
        if (idx == query.size()) return true;
        if (idx > query.size()) return false;
        for (size_t length = 1; length <= longest_size; ++length)
            if ((patterns.find(query.substr(idx, length)) != patterns.end())
            &&  self(self, idx + length, query))
                return true;
        return false;
    };
    int result = 0;
    for (std::string query : queries)
        result += isPossible(isPossible, 0, query);
    return result;
}

long long howManyWaysTheyCanBeFormed(std::unordered_set<std::string> patterns,
                                     std::vector<std::string> queries)
{
    size_t longest_size = 0;
    for (const std::string &current : patterns)
        longest_size = std::max(longest_size, current.size());
    std::unordered_map<std::string, long long> memo;
    auto isPossible = [&](auto &&self, size_t idx, const std::string &query) -> long long
    {
        if (idx == query.size()) return true;
        if (idx > query.size()) return false;
        if (auto search = memo.find(query.substr(idx)); search != memo.end())
            return search->second;
        long long number_of_ways = 0;
        for (size_t length = 1; length <= longest_size; ++length)
            if (patterns.find(query.substr(idx, length)) != patterns.end())
                number_of_ways += self(self, idx + length, query);
        return memo[query.substr(idx)] = number_of_ways;
    };
    long long result = 0;
    for (std::string query : queries)
        result += isPossible(isPossible, 0, query);
    return result;
}

int main(int argc, char * * argv)
{
    std::unordered_set<std::string> patterns;
    std::vector<std::string> queries;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_SUCCESS;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (patterns.empty())
        {
            std::istringstream iss(line);
            for (std::string token; iss >> token; )
            {
                if (token.back() == ',') token.pop_back();
                patterns.insert(token);
            }
        }
        else if (!line.empty())
            queries.push_back(line);
    }
    file.close();
    std::cout << "Problem A: " << howManyCanBeFormed(patterns, queries) << '\n';
    std::cout << "Problem B: " << howManyWaysTheyCanBeFormed(patterns, queries) << '\n';
    return EXIT_SUCCESS;
}

