#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <limits>

int main(int argc, char * * argv)
{
    std::vector<std::string> maze;
    std::string filename = "data.txt";
    int min_expected_save = 100;
    if (argc > 1) filename = argv[1];
    if (argc > 2) min_expected_save = std::stoi(argv[2]);
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        if (line.size() > 4)
            maze.push_back(line);
    file.close();
    
    std::pair<int, int> race_start = {-1, -1}, race_end = {-1, -1};
    const int n_rows = static_cast<int>(maze.size()),
              n_cols = static_cast<int>(maze[0].size());
    for (int row = 0; row < n_rows; ++row)
    {
        for (int col = 0; col < n_cols; ++col)
        {
            if (maze[row][col] == 'S')
                race_start = {row, col};
            else if (maze[row][col] == 'E')
                race_end = {row, col};
        }
    }
    constexpr int dir[] = { -1, 0, 1, 0, -1 };
    std::vector<std::vector<int> > distance(n_rows, std::vector<int>(n_cols, -1));
    std::vector<std::tuple<int, int, int> > path;
    std::pair<int, int> race_position = race_start;
    int d = 0;
    while (maze[race_position.first][race_position.second] != 'E')
    {
        path.push_back({race_position.first, race_position.second, d});
        distance[race_position.first][race_position.second] = d;
        for (int i = 0; i < 4; ++i)
        {
            int r = race_position.first  + dir[i];
            int c = race_position.second + dir[i + 1];
            if ((maze[r][c] != '#') && (distance[r][c] == -1))
            {
                race_position = {r, c};
                ++d;
                break;
            }
        }
    }
    distance[race_position.first][race_position.second] = d;
    path.push_back({race_position.first, race_position.second, d});
    
    int problem_a = 0;
    for (int row = 1; row < n_rows - 1; ++row)
    {
        for (int col = 1; col < n_cols - 1; ++col)
        {
            if (distance[row][col] != -1) continue;
            if ((distance[row - 1][col] != -1) && (distance[row + 1][col] != -1))
            {
                int save = std::abs(distance[row - 1][col]
                                  - distance[row + 1][col]) - 2;
                problem_a += (save >= min_expected_save);
            }
            if ((distance[row][col - 1] != -1) && (distance[row][col + 1] != -1))
            {
                int save = std::abs(distance[row][col - 1]
                                  - distance[row][col + 1]) - 2;
                problem_a += (save >= min_expected_save);
            }
        }
    }
    // Expected in little example:
    //  2: 14 14 44
    //  4: 14 28 30
    //  6:  2 30 16
    //  8:  4 34 14
    // 10:  2 36 10
    // 12:  3 39  8
    // 20:  1 40  5
    // 36:  1 41  4
    // 38:  1 42  3
    // 40:  1 43  2
    // 64:  1 44  1
    std::cout << "Problem A: " << problem_a << '\n';
    
    int problem_b = 0;
    for (size_t i = 0; i < path.size(); ++i)
    {
        auto [rs, cs, ds] = path[i];
        for (size_t j = i + 1; j < path.size(); ++j)
        {
            auto [re, ce, de] = path[j];
            int md = std::abs(rs - re) + std::abs(cs - ce);
            if (md <= 20)
            {
                int save = std::abs(de - ds) - md;
                problem_b += save >= min_expected_save; 
            }
        }
    }
    // Expected in little example:
    // 50: 32 285
    // 52: 31 253
    // 54: 29 222
    // 56: 39 193
    // 58: 25 154
    // 60: 23 129
    // 62: 20 106
    // 64: 19  86
    // 66: 12  67
    // 68: 14  55
    // 70: 12  41
    // 72: 22  29
    // 74:  4   7
    // 76:  3   3
    std::cout << "Problem B: " << problem_b << '\n';
    if ((filename == "data.txt") && (problem_b != 1033983))
        std::cout << "WRONG!\n";
    
    return EXIT_SUCCESS;
}

