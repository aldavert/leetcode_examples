# [Day 20: Race Condition](https://adventofcode.com/2024/day/20)

#### Problem A

You are given a maze with a single path from the beginning `S` to the end `E`. You are allowed to cross over one single obstacle cell **only once**.

How many crossed obstacles will allow you to save up to 100 cells in the path?


