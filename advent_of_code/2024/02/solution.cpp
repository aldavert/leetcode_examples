#include <iostream>
#include <fstream>
#include "utils/utils.hpp"

int main(int argc, char * * argv)
{
    auto to_int = [](std::string text) -> int { return std::stoi(text); };
    std::string filename = "data.txt";
    if (argc > 1)
        filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] Cannot open file '" << filename << "'.\n";
        return EXIT_FAILURE;
    }
    int problem_a = 0, problem_b = 0;
    for (std::string line; std::getline(file, line);)
    {
        std::vector<int> values = utls::map(to_int, utls::split(line));
        if (values.size() == 1)
        {
            ++problem_a;
            continue;
        }
        int sign = 0;
        for (size_t i = 1; i < values.size(); ++i)
            sign += 2 * (values[i] >= values[i - 1]) - 1;
        sign = 2 * (sign > 0) - 1;
        bool safe = true;
        for (size_t i = 1; safe && (i < values.size()); ++i)
        {
            int diff = sign * (values[i] - values[i - 1]);
            safe = (diff > 0) && (diff < 4);
        }
        if (safe)
        {
            ++problem_a;
            ++problem_b;
            continue;
        }
        for (size_t k = 1; k <= values.size(); ++k)
        {
            safe = true;
            for (size_t i = 1; safe && (i < values.size()); ++i)
            {
                int diff = 0;
                if ((i + 1 == k) || ((i == k) && (i == 1))) continue;
                diff = sign * (values[i] - values[i - 1 - (i == k)]);
                safe = (diff > 0) && (diff < 4);
            }
            if (safe)
            {
                ++problem_b;
                break;
            }
        }
    }
    file.close();
    std::cout << "Problem A: safe = " << problem_a << '\n';
    std::cout << "Problem B: 1-error safe = " << problem_b << '\n';
    // Between 307 and 329
    return EXIT_SUCCESS;
}

