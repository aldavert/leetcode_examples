# [Day 2: Red-Nosed Reports](https://adventofcode.com/2024/day/2)

#### Problem A

You are given a set of number sequences. A number sequence is considered **safe**, if its strictly decreasing or increasing with a maximum increase/decrease of 3.

How many sequences are **safe**?

#### Problem B

Same as before, but now you can remove one number of the sequence to make it safe.

How many sequences are now **safe**?

