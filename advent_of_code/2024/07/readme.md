# [Day 7: Bridge Repair](https://adventofcode.com/2024/day/7)

#### Problem A

You are given a set of lists of numbers. Each list is separated in two parts by a colon symbol. In the first part there is a single integer and in the second part there is a list of integers. In this problem, **select all lists** where you can generate the first part integer by applying operators `+` and `*` to the elements of the list. The operators are applied from left to right without taking into account the precedence of the operators.

The solution of this problem corresponds to the number obtained by adding the first part integer of each selected list.

#### Problem B

Now, we have the extra operator `||` which concatenates two numbers. The selected lists are computed with the new operator and the solution of the problem is the same as before but using the new selected list.

