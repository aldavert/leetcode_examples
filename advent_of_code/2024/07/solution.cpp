#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

struct Equation
{
    long long result;
    std::vector<long long> values;
};

bool valid2eq(const Equation &eq)
{
    auto generate = [&](auto &&self, size_t idx, long long value)
    {
        if (idx == eq.values.size()) return value == eq.result;
        else
        {
            if (self(self, idx + 1, value + eq.values[idx])) return true;
            return self(self, idx + 1, value * eq.values[idx]);
        }
    };
    return generate(generate, 1, eq.values[0]);
}

bool valid3eq(const Equation &eq)
{
    auto concat = [](long long left, long long right) -> long long
    {
        return std::stol(std::to_string(left) + std::to_string(right));
    };
    auto generate = [&](auto &&self, size_t idx, long long value)
    {
        if (idx == eq.values.size()) return value == eq.result;
        else
        {
            if (self(self, idx + 1, value + eq.values[idx])) return true;
            if (self(self, idx + 1, value * eq.values[idx])) return true;
            return self(self, idx + 1, concat(value, eq.values[idx]));
        }
    };
    return generate(generate, 1, eq.values[0]);
}

int main(int argc, char * * argv)
{
    std::vector<Equation> equations;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        std::istringstream parser(line);
        std::string token;
        parser >> token;
        while (!(token.empty() || std::isdigit(token.back()))) token.pop_back();
        equations.push_back({std::stol(token), {}});
        while (parser >> token)
            equations.back().values.push_back(std::stol(token));
    }
    file.close();
    
    long long problem_a = 0;
    for (auto &eq : equations)
        if (valid2eq(eq))
            problem_a += eq.result;
    std::cout << "Problem A: " << problem_a << '\n';
    long long problem_b = 0;
    for (auto &eq : equations)
        if (valid3eq(eq))
            problem_b += eq.result;
    std::cout << "Problem B: " << problem_b << '\n';
    
    return EXIT_SUCCESS;
}

