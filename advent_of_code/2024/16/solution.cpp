#include <iostream>
#include <fstream>
#include <vector>
#include <array>
#include <string>
#include <limits>

int minCost(const std::vector<std::string> &maze)
{
    constexpr int dir[] = { -1, 0, 1, 0, -1 };
    constexpr int UNSET = std::numeric_limits<int>::max();
    const int n_rows = static_cast<int>(maze.size()),
              n_cols = static_cast<int>(maze[0].size());
    std::vector<std::vector<std::array<int, 4> > > min_cost(n_rows,
            std::vector<std::array<int, 4> >(n_cols, {UNSET, UNSET, UNSET, UNSET}));
    auto [start_y, start_x] = [&]() -> std::pair<int, int>
    {
        for (int row = 0; row < n_rows; ++row)
            for (int col = 0; col < n_cols; ++col)
                if (maze[row][col] == 'S')
                    return {row, col};
        return {-1, -1};
    }();
    if ((start_y == -1) || (start_x == -1)) return -1;
    int result = UNSET;
    auto process = [&](auto &&self, int y, int x, int o, int cost) -> void
    {
        if ((maze[y][x] == '#')
        ||  (cost >= result)
        ||  (min_cost[y][x][o] <= cost)
        ||  (min_cost[y][x][(o + 2) % 4] <= cost)) return;
        min_cost[y][x][o] = cost;
        if (maze[y][x] == 'E')
        {
            result = std::min(result, cost);
            return;
        }
        for (int i = 0; i < 4; ++i)
        {
            int ny = y + dir[i], nx = x + dir[i + 1];
            if (maze[ny][nx] == '#') continue;
            int rotation_cost = std::abs(i - o);
            rotation_cost = 1000 * ((rotation_cost < 3)?rotation_cost:1);
            self(self, ny, nx, i, cost + 1 + rotation_cost);
        }
    };
    process(process, start_y, start_x, 1, 0);
    return result;
}

int minPathTiles(const std::vector<std::string> &maze)
{
    constexpr int dir[] = { -1, 0, 1, 0, -1 };
    constexpr int UNSET = std::numeric_limits<int>::max();
    const int n_rows = static_cast<int>(maze.size()),
              n_cols = static_cast<int>(maze[0].size());
    std::vector<std::vector<std::array<int, 4> > > min_cost(n_rows,
            std::vector<std::array<int, 4> >(n_cols, {UNSET, UNSET, UNSET, UNSET}));
    std::vector<std::string> best_paths = maze;
    auto [start_y, start_x] = [&]() -> std::pair<int, int>
    {
        for (int row = 0; row < n_rows; ++row)
            for (int col = 0; col < n_cols; ++col)
                if (maze[row][col] == 'S')
                    return {row, col};
        return {-1, -1};
    }();
    if ((start_y == -1) || (start_x == -1)) return -1;
    int expected_cost = minCost(maze);
    auto process = [&](auto &&self, int y, int x, int o, int cost) -> bool
    {
        if ((maze[y][x] == '#')
        ||  (cost > expected_cost)
        ||  (min_cost[y][x][o] < cost)
        ||  (min_cost[y][x][(o + 2) % 4] <= cost)) return false;
        min_cost[y][x][o] = cost;
        if (maze[y][x] == 'E')
        {
            best_paths[y][x] = 'O';
            return expected_cost == cost;
        }
        int best_path_count = 0;
        for (int i = 0; i < 4; ++i)
        {
            int ny = y + dir[i], nx = x + dir[i + 1];
            if (maze[ny][nx] == '#') continue;
            int rotation_cost = std::abs(i - o);
            rotation_cost = 1000 * ((rotation_cost < 3)?rotation_cost:1);
            best_path_count += self(self, ny, nx, i, cost + 1 + rotation_cost);
        }
        if (best_path_count > 0)
            best_paths[y][x] = 'O';
        return best_path_count > 0;
    };
    process(process, start_y, start_x, 1, 0);
    int result = 0;
    for (const auto &row : best_paths)
        for (char cell : row)
            result += cell == 'O';
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<std::string> maze;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        maze.push_back(line);
    file.close();
    
    std::cout << "Problem A: " << minCost(maze) << '\n';
    std::cout << "Problem B: " << minPathTiles(maze) << '\n';
    return EXIT_SUCCESS;
}

