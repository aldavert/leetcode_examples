# [Day 16: Reindeer Maze](https://adventofcode.com/2024/day/16)

#### Problem A

You are given a maze where you start at tile `S` facing east and you have to reach tile `E`. At each iteration you can rotate clockwise or counterclockwise at the cost of `1000` points or you can move forward at the cost of `1` points. You cannot move into a wall.

What is the minimum cost to go from `S` to `E`?

#### Problem B

Now, count how many cells in the gaze belong to a path from `S` to `E` that has minimum cost.

