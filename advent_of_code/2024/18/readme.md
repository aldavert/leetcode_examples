# [Day 18: RAM Run](https://adventofcode.com/2024/day/18)

#### Problem A

You are given a `70x70` grid and a list of positions that cannot be accessed. What is the length of the shortest path that goes from the top-left corner to the bottom-right corner?

**Note:** Use only the first `1024` blocks of the access denied list.

#### Problem B

If you keep adding obstacles of the list in order then, then there will be a moment where the path will be cut. What are the coordinates of the first obstacle that completely cuts the path?

