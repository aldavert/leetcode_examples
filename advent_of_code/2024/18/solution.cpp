#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <string>

int numberOfSteps(std::vector<std::string> grid)
{
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>(grid[0].size());
    std::queue<std::pair<int, int> > q;
    q.push({0, 0});
    for (int steps = 0; !q.empty(); ++steps)
    {
        for (size_t i = 0, n = q.size(); i < n; ++i)
        {
            auto [r, c] = q.front();
            q.pop();
            if ((r < 0) || (c < 0) || (r >= n_rows) || (c >= n_cols))
                continue;
            if (grid[r][c] != '.')
                continue;
            grid[r][c] = 'O';
            if ((r == n_rows - 1) && (c == n_cols - 1))
                return steps;
            q.push({r + 1, c});
            q.push({r - 1, c});
            q.push({r, c + 1});
            q.push({r, c - 1});
        }
    }
    return -1;
}

int main(int argc, char * * argv)
{
    std::vector<std::pair<int, int> > obstacles;
    int n_rows = 71, n_cols = 71, max_obstacles = 1024;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    if (argc > 4)
    {
        n_rows = std::stoi(argv[2]);
        n_cols = std::stoi(argv[3]);
        max_obstacles = std::stoi(argv[4]);
    }
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        auto search = line.find(',');
        obstacles.push_back({std::stoi(line.substr(0, search)),
                             std::stoi(line.substr(search + 1))});
    }
    file.close();
    
    std::vector<std::string> grid(n_rows, std::string(n_cols, '.'));
    for (int counter = 0; auto [c, r] : obstacles)
    {
        grid[r][c] = '#';
        if (++counter >= max_obstacles) break;
    }
    std::cout << "Problem A: " << numberOfSteps(grid) << '\n';
    // You can do it faster with binary search, but now it's only 100ms, so...
    for (size_t i = max_obstacles; i < obstacles.size(); ++i)
    {
        grid[obstacles[i].second][obstacles[i].first] = '#';
        if (numberOfSteps(grid) < 0)
        {
            std::cout << "Problem B: " << obstacles[i].first << ','
                                       << obstacles[i].second << '\n';
            break;
        }
    }
    return EXIT_SUCCESS;
}

