#include <iostream>
#include <fstream>
#include <vector>

int main(int argc, char * * argv)
{
    constexpr int direction[] = {-1, 0, 1, 0, -1};
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    std::vector<std::string> map;
    for (std::string line; std::getline(file, line);)
        map.push_back(line);
    const int n_rows = static_cast<int>(map.size()),
              n_cols = static_cast<int>(map[0].size());
    int start_row = -1, start_col = -1;
    for (int r = 0; (start_row < 0) && (r < n_rows); ++r)
        for (int c = 0; (start_col < 0) && (c < n_cols); ++c)
            if (map[r][c] == '^')
                start_row = r, start_col = c;
    std::vector<std::vector<bool> > available(n_rows, std::vector<bool>(n_cols, true));
    int problem_a = 1;
    available[start_row][start_col] = false;
    for (int row = start_row, col = start_col, dir = 0; true; )
    {
        int next_row = row + direction[dir],
            next_col = col + direction[dir + 1];
        if ((next_row < 0) || (next_row >= n_rows)
        ||  (next_col < 0) || (next_col >= n_cols))
            break;
        if (map[next_row][next_col] == '#')
            dir = (dir + 1) % 4;
        else
        {
            row = next_row;
            col = next_col;
            problem_a += available[next_row][next_col];
            available[next_row][next_col] = false;
        }
        
    }
    std::cout << "Problem A: " << problem_a << '\n';
    // -------------------------------------------------------------------------
    int problem_b = 0;
    for (int obs_row = 0; obs_row < n_rows; ++obs_row)
    {
        for (int obs_col = 0; obs_col < n_cols; ++obs_col)
        {
            if (available[obs_row][obs_col] || (map[obs_row][obs_col] != '.')) continue;
            map[obs_row][obs_col] = '#';
            std::vector<std::vector<char> > visited(n_rows,
                                                    std::vector<char>(n_cols, 0));
            visited[start_row][start_col] = 1;
            for (int row = start_row, col = start_col, dir = 0; true; )
            {
                int next_row = row + direction[dir],
                    next_col = col + direction[dir + 1];
                if ((next_row < 0) || (next_row >= n_rows)
                ||  (next_col < 0) || (next_col >= n_cols))
                    break;
                if (map[next_row][next_col] == '#')
                    dir = (dir + 1) % 4;
                else
                {
                    row = next_row;
                    col = next_col;
                    if (visited[next_row][next_col] & (1 << dir))
                    {
                        ++problem_b;
                        break;
                    }
                    visited[next_row][next_col] |= 1 << dir;
                }
                
            }
            map[obs_row][obs_col] = '.';
        }
    }
    std::cout << "Problem B: " << problem_b << '\n';
    return EXIT_SUCCESS;
}

