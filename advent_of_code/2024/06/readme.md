# [Day 6: Guard Gallivant](https://adventofcode.com/2024/day/6)

#### Problem A

You are given a map an initial position `^` indicating the position of the agent in the map. Initial the agent is facing `up`. The agent moves following the direction that is facing until it exits the map or it is obstructed by an obstacle. When it is obstructed, it rotates 90 degrees right and continues. **How many cells visits the agent before leaving the map?**


