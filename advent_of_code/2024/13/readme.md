# [Day 13: Claw Contraption](https://adventofcode.com/2024/day/13)

#### Problem A

You are given a series of problems where you have two options: **Option A** costs you 3 tokens while **Option B** cost you 1 token. Both options move you diagonally towards the top-right direction. You start at `(0, 0)` and you have to reach the given `prize` coordinates (if possible).

What is the minimum number of tokens necessary to get prize of each problem?

#### Problem B

Same as before, but now add `10'000'000'000'000` to both coordinates of the prize location.

What now is the minimum number of tokens necessary to get prize of each problem?

