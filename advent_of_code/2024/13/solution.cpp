#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <limits>

struct Problem
{
    long buttonA[2] = {};
    long buttonB[2] = {};
    long prize[2] = {};
};

#if 1
long minCost(const Problem &problem)
{
    const long Ax = problem.buttonA[0], Ay = problem.buttonA[1],
               Bx = problem.buttonB[0], By = problem.buttonB[1],
               Px = problem.prize[0],   Py = problem.prize[1];
    long nom = Ay * Px - Ax * Py,
         den = Ay * Bx - Ax * By;
    if (nom % den != 0) return 0;
    long m = nom / den;
    nom = Px - Bx * m;
    den = Ax;
    if (nom % den != 0) return 0;
    long n = nom / den;
    return 3 * n + m;
}
#else
struct PairHash
{
public:
    template <typename T, typename U>
    std::size_t operator()(const std::pair<T, U> &x) const
    {
        return std::hash<T>()(x.first) ^ std::hash<U>()(x.second);
    }
};

long minCost(const Problem &problem)
{
    long result = std::numeric_limits<long>::max();
    std::unordered_map<std::pair<long, long>, long, PairHash> memo;
    auto process = [&](auto &&self, long cost, long x, long y) -> void
    {
        if ((x > problem.prize[0]) || (y > problem.prize[1])) return;
        if ((x == problem.prize[0]) && (y == problem.prize[1]))
        {
            result = std::min(result, cost);
            return;
        }
        if (auto search = memo.find({x, y});
            (search != memo.end()) && (cost >= search->second))
            return;
        memo[{x, y}] = cost;
        self(self, cost + 3, x + problem.buttonA[0], y + problem.buttonA[1]);
        self(self, cost + 1, x + problem.buttonB[0], y + problem.buttonB[1]);
    };
    process(process, 0, 0, 0);
    return (result != std::numeric_limits<long>::max())?result:0;
}
#endif

int main(int argc, char * * argv)
{
    std::vector<Problem> problems;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    bool set_button_a = false, set_button_b = false, set_prize = false;
    Problem current;
    for (std::string line; std::getline(file, line); )
    {
        auto search = line.find(':');
        if (search == std::string::npos) continue;
        std::string command = line.substr(0, search);
        if (command == "Button A")
        {
            search = line.find("X+", search);
            size_t size = line.find(',', search) - search - 2;
            current.buttonA[0] = std::stoi(line.substr(search + 2, size));
            current.buttonA[1] = std::stoi(line.substr(line.find("Y+", search) + 2));
            set_button_a = true;
        }
        else if (command == "Button B")
        {
            search = line.find("X+", search);
            size_t size = line.find(',', search) - search - 2;
            current.buttonB[0] = std::stoi(line.substr(search + 2, size));
            current.buttonB[1] = std::stoi(line.substr(line.find("Y+", search) + 2));
            set_button_b = true;
        }
        else if (command == "Prize")
        {
            search = line.find("X=", search);
            size_t size = line.find(',', search) - search - 2;
            current.prize[0] = std::stoi(line.substr(search + 2, size));
            current.prize[1] = std::stoi(line.substr(line.find("Y=", search) + 2));
            set_prize = true;
        }
        else
        {
            std::cerr << "[WARNING] Unexpected command: " << command << '\n';
            continue;
        }
        if (set_button_a && set_button_b && set_prize)
        {
            set_button_a = set_button_b = set_prize = false;
            problems.push_back(current);
        }
    }
    file.close();
    long problem_a = 0;
    for (auto problem : problems)
        problem_a += minCost(problem);
    std::cout << "Problem A: " << problem_a << '\n';
    constexpr long correction = 10'000'000'000'000;
    long problem_b = 0;
    for (auto problem : problems)
    {
        problem.prize[0] += correction;
        problem.prize[1] += correction;
        problem_b += minCost(problem);
    }
    std::cout << "Problem B: " << problem_b << '\n';
    return EXIT_SUCCESS;
}

