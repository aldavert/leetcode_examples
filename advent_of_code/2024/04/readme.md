# [Day 4: Ceres Search](https://adventofcode.com/2024/day/4)

#### Problem A

You are given a grid with letters, find all the instances of the chain `XMAS` in any of the possible directions (horizontal, vertical, diagonal or written backwards). How many `XMAS` appear?

#### Problem B
Now search how many crossed `MAS` patterns appear in the text. A crossed `MAS` pattern
has one of the following 4 forms:
```
M.S M.M S.M S.S
.A. .A. .A. .A.
M.S S.S S.M M.M
```
where `.` represents any symbol.

