#include <iostream>
#include <fstream>
#include <vector>

int main(int argc, char * * argv)
{
    std::vector<std::string> puzzle;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be loaded.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line);)
        puzzle.push_back(line);
    file.close();
    constexpr int directions[] = {-1, -1, 0, -1, 1, 0, 1, 1, -1};
    constexpr char pattern[] = "XMAS";
    constexpr int pattern_size = sizeof(pattern) - 1;
    const int n_rows = static_cast<int>(puzzle.size()),
              n_cols = static_cast<int>(puzzle[0].size());
    int problem_a = 0;
    for (int row = 0; row < n_rows; ++row)
    {
        for (int col = 0; col < n_cols; ++col)
        {
            if (puzzle[row][col] == pattern[0])
            {
                for (int dir = 0; dir < 8; ++dir)
                {
                    int len = 0, y = row, x = col;
                    while (len < pattern_size)
                    {
                        if ((x < 0) || (y < 0) || (x >= n_cols) || (y >= n_rows))
                            break;
                        if (puzzle[y][x] != pattern[len])
                            break;
                        y += directions[dir];
                        x += directions[dir + 1];
                        ++len;
                    }
                    problem_a += len == pattern_size;
                }
            }
        }
    }
    int problem_b = 0;
    for (int r = 1; r < n_rows - 1; ++r)
    {
        for (int c = 1; c < n_cols - 1; ++c)
        {
            if (puzzle[r][c] != 'A') continue;
            bool diagonal =
                ((puzzle[r - 1][c - 1] == 'M') && (puzzle[r + 1][c + 1] == 'S'))
             || ((puzzle[r + 1][c + 1] == 'M') && (puzzle[r - 1][c - 1] == 'S'));
            bool anti_diagonal =
                ((puzzle[r - 1][c + 1] == 'M') && (puzzle[r + 1][c - 1] == 'S'))
             || ((puzzle[r + 1][c - 1] == 'M') && (puzzle[r - 1][c + 1] == 'S'));
            problem_b += diagonal && anti_diagonal;
        }
    }
    std::cout << "Problem A: " << problem_a << '\n';
    std::cout << "Problem B: " << problem_b << '\n';
    return EXIT_SUCCESS;
}

