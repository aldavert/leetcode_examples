#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <set>

std::set<std::vector<std::string> >
findNCliques(size_t clique_size,
             const std::unordered_map<std::string, std::vector<std::string> > &g,
             auto &&constraint)
{
    std::set<std::vector<std::string> > unique;
    std::vector<std::string> group(clique_size);
    auto populate = [&](auto &&self,
                        const std::vector<std::string> &neighbors,
                        size_t idx,
                        size_t size) -> void
    {
        if (size == clique_size)
        {
            std::vector<std::string> sorted_group = group;
            bool valid = false;
            for (size_t i = 0; i < clique_size; ++i)
                valid = valid || constraint(group[i]);
            if (!valid) return;
            std::sort(sorted_group.begin(), sorted_group.end());
            unique.insert(sorted_group);
            return;
        }
        if (idx >= neighbors.size()) return;
        for (size_t i = idx; i < neighbors.size(); ++i)
        {
            const auto &current = g.at(neighbors[i]);
            bool valid = true;
            for (size_t j = size - 1; valid && (j > 0); --j)
            {
                valid = false;
                for (auto neighbor : current)
                {
                    if (neighbor == group[j])
                    {
                        valid = true;
                        break;
                    }
                }
            }
            if (!valid) continue;
            group[size] = neighbors[i];
            self(self, neighbors, i + 1, size + 1);
        }
    };
    for (const auto &[first, neighbors] : g)
    {
        group[0] = first;
        populate(populate, neighbors, 0, 1);
    }
    return unique;
}

int main(int argc, char * * argv)
{
    std::unordered_map<std::string, std::vector<std::string> > graph;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        auto separator = line.find('-');
        std::string node1 = line.substr(0, separator),
                    node2 = line.substr(separator + 1);
        graph[node1].push_back(node2);
        graph[node2].push_back(node1);
    }
    file.close();
    
    auto contains_t = [](std::string t) { return t[0] == 't'; };
    auto no_constraint = [](std::string) { return true; };
    std::cout << "Problem A: " << findNCliques(3, graph, contains_t).size() << '\n';
    std::set<std::vector<std::string> > selected_clique;
    for (size_t i = 2; i < 20; ++i)
    {
        auto clique = findNCliques(i, graph, no_constraint);
        if (clique.size() == 0) break;
        std::cout << i << ": " << clique.size() << '\n';
        selected_clique = clique;
    }
    std::cout << "Problem B: ";
    auto &selected = *selected_clique.begin();
    for (bool next = false; std::string node : selected)
    {
        if (next) std::cout << ',';
        next = true;
        std::cout << node;
    }
    std::cout << '\n';
    
    return EXIT_SUCCESS;
}

