# [Day 23: LAN Party](https://adventofcode.com/2024/day/23)

#### Problem A

You are given a set of edges between nodes of a graph. These edges are bidirectional. How many groups of three fully-interconnected nodes, containing at least a node whose name starts with `t`, are present in the graph?

#### Problem B

Search the largest amount of fully-interconnected nodes and return the names of nodes ordered alphabetically and separated by commas.

