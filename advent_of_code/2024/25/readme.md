# [Day 25: Code Chronicle](https://adventofcode.com/2024/day/25)

#### Problem A

You are given a set of key and lock sketches, where you have a `7x5` grid representing the occupied `#` and `.` free space. The locks columns of occupied space that start from the **top of the grid**, while keys are columns of occupied space that start from the **bottom of the grid**.

A key and a lock can match if there is no overlapping (they don't have to fit perfectly). How many pairs of locks-keys exist?



