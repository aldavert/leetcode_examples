#include <iostream>
#include <fstream>
#include <vector>
#include <string>

int main(int argc, char * * argv)
{
    const int n_rows = 7, n_cols = 5;
    std::vector<std::vector<std::string> > grid_locks, grid_keys;
    std::vector<std::vector<int> > locks, keys;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    std::vector<std::string> current;
    for (std::string line; std::getline(file, line); )
    {
        if (line.size() < 5)
        {
            if (current.size() == 0) continue;
            if (current[0] == std::string(n_cols, '#'))
                grid_locks.push_back(current);
            else grid_keys.push_back(current);
            current.clear();
            continue;
        }
        current.push_back(line);
    }
    if (current.size() > 0)
    {
        if (current[0] == std::string(n_cols, '#'))
            grid_locks.push_back(current);
        else grid_keys.push_back(current);
    }
    file.close();
    for (const auto &grid : grid_locks)
    {
        locks.push_back(std::vector<int>(n_cols, 0));
        for (int row = 0; row < n_rows; ++row)
            for (int col = 0; col < n_cols; ++col)
                if (grid[row][col] == '#')
                    locks.back()[col] = row;
    }
    for (const auto &grid : grid_keys)
    {
        keys.push_back(std::vector<int>(n_cols, 0));
        for (int row = n_rows - 1; row >= 0; --row)
            for (int col = 0; col < n_cols; ++col)
                if (grid[row][col] == '#')
                    keys.back()[col] = n_cols - row + 1;
    }
    int problem_a = 0;
    for (const auto &lock_id : locks)
    {
        for (const auto &key_id : keys)
        {
            bool valid = true;
            for (int col = 0; col < n_cols; ++col)
                valid = valid && (lock_id[col] + key_id[col] <= 5);
            problem_a += valid;
        }
    }
    std::cout << "Problem A: " << problem_a << '\n';
    
    return EXIT_SUCCESS;
}

