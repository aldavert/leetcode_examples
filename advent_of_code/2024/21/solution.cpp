#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <string>
#include <utility>
#include <unordered_set>
#include <unordered_map>
#include <limits>

struct Keypad
{
    Keypad(bool b) : big(b)
    {
        if (b) { row = 3; col = 2; }
        else   { row = 0; col = 2; }
    }
    std::pair<int, int> position(char key) const
    {
        if (big)
        {
            switch (key)
            {
            case '0':
                return {3, 1};
            case 'A':
                return {3, 2};
            default:
                return { 2 - (key - '1') / 3, (key - '1') % 3 };
            }
        }
        else
        {
            if      (key == 'A') return { 0, 2 };
            else if (key == '^') return { 0, 1 };
            else if (key == '<') return { 1, 0 };
            else if (key == 'v') return { 1, 1 };
            else if (key == '>') return { 1, 2 };
            return {-1, -1};
        }
    }
    std::vector<std::string> press(char key)
    {
        auto [dst_row, dst_col] = position(key);
        bool single_path = ( big && (((col == 0) && (dst_row == 3))
                                  || ((row == 3) && (dst_col == 0))))
                        || (!big && ((dst_col == 0) || (col == 0)));
        int diff_row = dst_row - std::exchange(row, dst_row),
            diff_col = dst_col - std::exchange(col, dst_col);
        if (single_path || (diff_row == 0) || (diff_col == 0))
        {
            std::string action;
            if (big) // Movement priority: up > right > left > down
            {
                if (diff_row < 0) action += std::string(-diff_row, '^');
                if (diff_col > 0) action += std::string( diff_col, '>');
                if (diff_col < 0) action += std::string(-diff_col, '<');
                if (diff_row > 0) action += std::string( diff_row, 'v');
            }
            else // Movement priority: down > right > left > up
            {
                if (diff_row > 0) action += std::string( diff_row, 'v');
                if (diff_col > 0) action += std::string( diff_col, '>');
                if (diff_col < 0) action += std::string(-diff_col, '<');
                if (diff_row < 0) action += std::string(-diff_row, '^');
            }
            return { action + 'A' };
        }
        else return { std::string(std::abs(diff_row), (diff_row < 0)?'^':'v')
                    + std::string(std::abs(diff_col), (diff_col < 0)?'<':'>') + 'A',
                      std::string(std::abs(diff_col), (diff_col < 0)?'<':'>')
                    + std::string(std::abs(diff_row), (diff_row < 0)?'^':'v') + 'A'};
    }
    void move(int r, int c) { row = r; col = c; }
    void move(char key)
    {
        auto [dst_row, dst_col] = position(key);
        row = dst_row;
        col = dst_col;
    }
    bool big = true;
    int row = 3;
    int col = 2;
};

#if 0
std::string generateSequenceSequencially(std::string code, size_t number_of_keypads)
{
    std::unordered_set<std::string> encoded;
    encoded.insert(code);
    for (size_t level = 0; level < number_of_keypads; ++level)
    {
        std::unordered_set<std::string> next_encoded;
        for (std::string current : encoded)
        {
            std::vector<std::string> variations = {""};
            Keypad keypad(level == 0);
            std::string history;
            for (char key : current)
            {
                std::vector<std::string> next_variations;
                std::vector<std::string> current_var = keypad.press(key);
                for (const std::string &prev : variations)
                    for (const std::string &curr : current_var)
                        next_variations.push_back(prev + curr);
                variations = std::move(next_variations);
            }
            for (const std::string &var : variations)
                next_encoded.insert(var);
        }
        encoded.clear();
        size_t min_size = std::numeric_limits<size_t>::max();
        for (const std::string &current : next_encoded)
            min_size = std::min(min_size, current.size());
        for (const std::string &current : next_encoded)
            if (current.size() == min_size)
                encoded.insert(current);
    }
    std::string selected_sequence = *(encoded.begin());
    for (std::string current : encoded)
        if (current.size() < selected_sequence.size())
            selected_sequence = current;
    return selected_sequence;
}
#endif

#if 0
std::string generateSequence(std::string text, int max_level)
{
    std::vector<std::unordered_map<std::string, std::string> > memo(max_level);
    auto generate = [&](auto &&self, std::string code, int level) -> std::string
    {
        if (level == max_level) return code;
        if (auto search = memo[level].find(code); search != memo[level].end())
            return search->second;
        Keypad keypad(level == 0);
        std::string result;
        for (char key : code)
        {
            std::vector<std::string> paths = keypad.press(key);
            std::string shortest = self(self, paths[0], level + 1);
            for (size_t i = 1; i < paths.size(); ++i)
            {
                std::string current = self(self, paths[i], level + 1);
                if (current.size() < shortest.size())
                    shortest = current;
            }
            result += shortest;
        }
        memo[level][code] = result;
        return result;
    };
    //std::vector<std::string> common_paths = { ">vA", "v>A", "v<A", "v<<A", ">^A",
    //    "vA", "<A", ">>^A", ">A", "^>A", "<vA", "A", "^<A", "<^A", "^A" };
    //for (std::string code : common_paths)
    //    memo[1][code] = generate(generate, code, 1);
    std::string res = generate(generate, text, 0);
    return res;
}
#endif

long sequenceLength(std::string text, int max_level)
{
    std::vector<std::unordered_map<std::string, long> > memo(max_level);
    auto generate = [&](auto &&self, std::string code, int level) -> long
    {
        if (level == max_level) return static_cast<long>(code.size());
        if (auto search = memo[level].find(code); search != memo[level].end())
            return search->second;
        Keypad keypad(level == 0);
        long result = 0;
        for (char key : code)
        {
            std::vector<std::string> paths = keypad.press(key);
            long shortest = self(self, paths[0], level + 1);
            for (size_t i = 1; i < paths.size(); ++i)
                shortest = std::min(shortest, self(self, paths[i], level + 1));
            result += shortest;
        }
        memo[level][code] = result;
        return result;
    };
    return generate(generate, text, 0);
}

int main(int argc, char * * argv)
{
    std::vector<std::string> codewords;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        codewords.push_back(line);
    file.close();
    
    long problem_a = 0;
    for (std::string code : codewords)
    {
        long sequence_length = sequenceLength(code, 3);
        int value = 0;
        for (char chr : code)
            if (std::isdigit(chr))
                value = value * 10 + chr - '0';
        problem_a += value * sequence_length;
    }
    std::cout << "Problem A: " << problem_a << '\n';
    
    long problem_b = 0;
    for (std::string code : codewords)
    {
        long sequence_length = sequenceLength(code, 26);
        long value = 0;
        for (char chr : code)
            if (std::isdigit(chr))
                value = value * 10 + chr - '0';
        problem_b += value * sequence_length;
    }
    std::cout << "Problem B: " << problem_b << '\n';
    
    return EXIT_SUCCESS;
}

