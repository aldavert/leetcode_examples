#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <utility>
#include <queue>
#include <limits>

struct Machine
{
    long register_a = 0;
    long register_b = 0;
    long register_c = 0;
    std::vector<int> instructions;
};

std::string generateOutput(Machine machine)
{
    auto combo = [&](int value) -> long
    {
        if (value <= 3) return value;
        if (value == 4) return machine.register_a;
        if (value == 5) return machine.register_b;
        if (value == 6) return machine.register_c;
        return -1;
    };
    std::string output;
    size_t pc = 0;
    while (pc < machine.instructions.size())
    {
        switch (machine.instructions[pc])
        {
        case 0:
            machine.register_a /= (1 << combo(machine.instructions[pc + 1]));
            break;
        case 1:
            machine.register_b = machine.register_b ^ machine.instructions[pc + 1];
            break;
        case 2:
            machine.register_b = combo(machine.instructions[pc + 1]) % 8;
            break;
        case 3:
            if (machine.register_a != 0)
                pc = machine.instructions[pc + 1] - 2;
            break;
        case 4:
            machine.register_b = machine.register_b ^ machine.register_c;
            break;
        case 5:
            if (!output.empty()) output += ",";
            output += std::to_string(combo(machine.instructions[pc + 1]) % 8);
            break;
        case 6:
            machine.register_b = machine.register_a
                               / (1 << combo(machine.instructions[pc + 1]));
            break;
        case 7:
            machine.register_c = machine.register_a
                               / (1 << combo(machine.instructions[pc + 1]));
            break;
        default:
            std::cerr << "[ERROR] Unknown opcode: " << machine.instructions[pc] << '\n';
            return "";
        }
        pc += 2;
    }
    return output;
}

int main(int argc, char * * argv)
{
    Machine machine;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.size() < 8) continue;
        if (line.substr(0, 8) == "Register")
        {
            if      (line[9] == 'A') machine.register_a = std::stoi(line.substr(12));
            else if (line[9] == 'B') machine.register_b = std::stoi(line.substr(12));
            else if (line[9] == 'C') machine.register_c = std::stoi(line.substr(12));
            else
            {
                std::cerr << "[ERROR] Unknown register initialization: " << line << '\n';
                return EXIT_FAILURE;
            }
        }
        else if (line.substr(0, 7) == "Program")
        {
            auto search = line.find(' ');
            for (auto next = line.find(',', search);
                 search != std::string::npos;
                 search = std::exchange(next, line.find(',', next + 1)))
            {
                auto token = line.substr(search + 1, next - search - 1);
                machine.instructions.push_back(std::stoi(token));
            }
        }
        else
        {
            std::cerr << "[ERROR] Unexpected program specification: " << line << '\n';
            return EXIT_FAILURE;
        }
    }
    std::cout << "Problem A: " << generateOutput(machine) << '\n';
    
    // Instructions in the program:
    // 0: BST 4 [reg_b] = [reg_a] % 8
    // 1: BXL 5 [reg_b] = [reg_b] XOR 5
    // 2: CDV 5 [reg_c] = [reg_a] / (2^[reg_b])
    // 3: ADV 3 [reg_a] = [reg_a] / 8
    // 4: BXL 6 [reg_b] = [reg_b] XOR 6
    // 5: BXC 3 [reg_b] = [reg_b] XOR [reg_c]
    // 6: OUT 5 out [reg_b] % 8
    // 7: JNZ 0 
    // Reduced mathematical operations:
    // [reg_b] = ([reg_a] % 8) XOR 5
    // [reg_b] = ([reg_b] XOR 6) XOR ([reg_a] / (2^[reg_b]))
    // [reg_a] = [reg_a] / 8
    // Populate the numbers in reverse, start by guessing the most significant
    // bits (blocks of 3 bits) and keep guessing into the less significant bits.
    // Generate all possible combinations and later select the combination with
    // the smallest value.
    std::queue<long> register_a;
    register_a.push(0);
    for (int i = static_cast<int>(machine.instructions.size()) - 1; i >= 0; --i)
    {
        for (size_t j = 0, n = register_a.size(); j < n; ++j)
        {
            long reg_a = register_a.front() << 3;
            register_a.pop();
            for (long block_a = 0; block_a < 8; ++block_a)
            {
                long reg_b = block_a ^ 5;
                reg_b = (reg_b ^ 6) ^ ((reg_a + block_a)/ (1 << reg_b));
                if (static_cast<int>(reg_b % 8) == machine.instructions[i])
                    register_a.push(reg_a + block_a);
            }
        }
    }
    long selected = std::numeric_limits<long>::max();
    while (!register_a.empty())
    {
        selected = std::min(selected, register_a.front());
        register_a.pop();
    }
    std::cout << "Problem B: " << selected << '\n';
    
    return EXIT_SUCCESS;
}

