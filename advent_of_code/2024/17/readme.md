# [Day 17: Chronospatial Computer](https://adventofcode.com/2024/day/17)

#### Problem A

You are given a 3-bit computer with 3 integer registers (`A`, `B` and `C`). The registers are not limited to 3 bits. The instructions consist of an `opcode` and an `operand`. The operand represents the value itself when it is treated as a **literal** but it encodes the following when used as a **combo**:
- Values `0` to `3` represent literal values from `0` to `3`.
- Value `4` represents the value of register `A`.
- Value `5` represents the value of register `B`.
- Value `6` represents the value of register `C`.
- Value `7` is invalid.

Instructions are always tuples of values, an `opcode` and an `operand`. The machine has the following 8 `opcodes`:
- **opcode 0** (`adv`): `[register A] = [register A] / 2^COMBO`. For example instruction `0,2` means `[register A] = [register A] / 2^2 = [register A] / 4` while, `0,5` means `[register A] = [register A] / 2^[register B]`.
- **opcode 1** (`bxl`): `[register B] = [register B] XOR operand`. In this instruction operand is used as a literal value.
- **opcode 2** (`bst`): `[register B] = (COMBO) % 8`
- **opcode 3** (`jnz`): Conditional jump. If `[register A] != 0` jumps to position indicated by the literal.
- **opcode 4** (`bxc`): `[register B] = [register B] XOR [register C]`
- **opcode 5** (`out`): outputs the `COMBO` operand value.
- **opcode 6** (`bdv`): `[register B] = [register A] / 2^COMBO`.
- **opcode 7** (`cdv`): `[register C] = [register A] / 2^COMBO`.

Given the initial values of the registers and a program instructions, return a string with the different values outputted by the program separated by commas.

#### Problem B

The given code is supposed to generate the code as output however, the value of the register A is corrupted. Find which is the **minimum value** that can be assigned to register `A`, so the program will generate its code as output when executed.

