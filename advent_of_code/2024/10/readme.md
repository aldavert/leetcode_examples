# [Day 10: Hoof It](https://adventofcode.com/2024/day/10)

#### Problem A

You are given a grid with elevation values that change from `0` (lowest) to `9` (highest). A valid path between two positions is a 4-connectivity path (only `up`, `down`, `left` and `right` is allowed) where at each step you increase the elevation by one. How many pairs of lowest-highest positions that have a valid path?

**Note:** if two positions have more than one valid path, they still count as a single path.

#### Problem B

Now, you have to sum all the possible valid paths between the `0`'s and the `9`'s in the map.

