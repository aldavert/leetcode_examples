#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <stack>

int main(int argc, char * * argv)
{
    std::vector<std::vector<int> > elevation;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        elevation.push_back({});
        for (char value : line)
            elevation.back().push_back(value - '0');
    }
    file.close();
    const int n_rows = static_cast<int>(elevation.size()),
              n_cols = static_cast<int>(elevation[0].size());
    std::vector<std::vector<int> > visited(n_rows, std::vector<int>(n_cols, -1));
    int problem_a = 0, problem_b = 0;
    for (int row = 0, n_trailhead = 0; row < n_rows; ++row)
    {
        for (int col = 0; col < n_cols; ++col)
        {
            if (elevation[row][col] != 0) continue;
            std::queue<std::pair<int, int> > q;
            q.push({row, col});
            int altitude = 0, trailtail = 0;
            for (; !q.empty() && (altitude < 10); ++altitude)
            {
                for (int sz = static_cast<int>(q.size()); sz > 0; --sz)
                {
                    auto [r, c] = q.front();
                    q.pop();
                    if ((r < 0) || (c < 0) || (r >= n_rows) || (c >= n_cols))
                        continue;
                    if (elevation[r][c] != altitude) continue;
                    if (visited[r][c] == n_trailhead) continue;
                    visited[r][c] = n_trailhead;
                    if (altitude == 9) ++trailtail;
                    else
                    {
                        q.push({r + 1, c});
                        q.push({r - 1, c});
                        q.push({r, c + 1});
                        q.push({r, c - 1});
                    }
                }
            }
            problem_a += trailtail;
            ++n_trailhead;
            if (trailtail == 0) continue;
            trailtail = 0;
            std::stack<std::tuple<int, int, int> > st;
            st.push({row, col, 0});
            while (!st.empty())
            {
                auto [r, c, height] = st.top();
                st.pop();
                if ((r < 0) || (c < 0) || (r >= n_rows) || (c >= n_cols))
                    continue;
                if (elevation[r][c] != height) continue;
                if (height == 9) ++trailtail;
                else
                {
                    st.push({r + 1, c, height + 1});
                    st.push({r - 1, c, height + 1});
                    st.push({r, c + 1, height + 1});
                    st.push({r, c - 1, height + 1});
                }
            }
            problem_b += trailtail;
        }
    }
    std::cout << "Problem A: " << problem_a << '\n';
    std::cout << "Problem B: " << problem_b << '\n';
    
    return EXIT_SUCCESS;
}

