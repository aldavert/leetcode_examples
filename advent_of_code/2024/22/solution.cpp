#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>
#include <unordered_set>

long next(long value)
{
    value = ((value * 64) ^ value) % 16777216;
    value = ((value / 32) ^ value) % 16777216;
    value = ((value * 2048) ^ value) % 16777216;
    return value;
}

int main(int argc, char * * argv)
{
    std::vector<int> initial_secrets;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        initial_secrets.push_back(std::stoi(line));
    file.close();
    
    long problem_a = 0;
    for (int initial : initial_secrets)
    {
        long secret = initial;
        for (int i = 0; i < 2000; ++i)
            secret = next(secret);
        problem_a += secret;
    }
    std::cout << "Problem A: " << problem_a << '\n';
    
    std::unordered_map<long, long> sequence_price;
    long sequence[4];
    auto signature = [&](void) -> long
    {
        return ((sequence[0] + 10) << (5 * 3))
             | ((sequence[1] + 10) << (5 * 2))
             | ((sequence[2] + 10) << (5 * 1))
             | ((sequence[3] + 10) << (5 * 0));
    };
    for (int initial : initial_secrets)
    {
        long secret = initial;
        long previous = secret % 10;
        for (int i = 0; i < 3; ++i)
        {
            secret = next(secret);
            long next = secret % 10;
            sequence[i] = next - previous;
            previous = next;
        }
        std::unordered_set<long> first_found;
        for (int i = 3; i < 2000; ++i)
        {
            secret = next(secret);
            long next = secret % 10;
            sequence[3] = next - previous;
            previous = next;
            long id = signature();
            if (first_found.find(id) == first_found.end())
            {
                first_found.insert(id);
                sequence_price[id] += next;
            }
            sequence[0] = sequence[1];
            sequence[1] = sequence[2];
            sequence[2] = sequence[3];
        }
    }
    long problem_b = 0;
    for (auto [id, price] : sequence_price)
        problem_b = std::max(problem_b, price);
    std::cout << "Problem B: " << problem_b << '\n';
    
    return EXIT_SUCCESS;
}
    
