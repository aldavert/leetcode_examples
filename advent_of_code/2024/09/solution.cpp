#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <queue>

long long checksum(const std::vector<int> &disk)
{
    const long long n = static_cast<long>(disk.size());
    long long result = 0;
    for (long long i = 0; i < n; ++i)
        if (disk[i] >= 0)
            result += i * static_cast<long long>(disk[i]);
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<std::pair<int, int> > freespace_blocks;
    std::string disk_map;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    file >> disk_map;
    file.close();
    int total_size = 0;
    for (char block_size : disk_map)
        total_size += block_size - '0';
    std::vector<int> disk(total_size, -1);
    int idx = 0, file_id = 0;
    for (bool free_space = false; char block_size : disk_map)
    {
        const int bs = static_cast<int>(block_size - '0');
        if (free_space)
        {
            freespace_blocks.push_back({idx, bs});
            idx += bs;
        }
        else
        {
            for (int k = bs; k > 0; --k, ++idx)
                disk[idx] = file_id;
            ++file_id;
        }
        free_space = !free_space;
    }
    std::vector<int> disk_a = disk;
    for (int left = 0, right = total_size - 1; left < right;)
    {
        while ((left < total_size) && (disk_a[left] >= 0)) ++left;
        while ((right >= 0) && (disk_a[right] < 0)) --right;
        if (left >= right) continue;
        std::swap(disk_a[left], disk_a[right]);
    }
    std::vector<int> disk_b = disk;
    for (int right = total_size - 1; right >= 0;)
    {
        while ((right >= 0) && (disk_b[right] < 0)) --right;
        int filesize = right;
        while ((filesize >= 0) && (disk_b[filesize] == disk_b[right])) --filesize;
        filesize = right - filesize;
        for (auto &[position, block_size] : freespace_blocks)
        {
            if (position >= right) break;
            if (block_size < filesize) continue;
            for (; filesize > 0; --filesize, --block_size, ++position, --right)
                std::swap(disk_b[right], disk_b[position]);
            break;
        }
        if (filesize != 0) right -= filesize;
    }
#if 0
    for (int value : disk_b)
    {
        if (value < 0) std::cout << '.';
        else std::cout << value;
        std::cout << '|';
    }
    std::cout << '\n';
#endif
    std::cout << "Problem A: " << checksum(disk_a) << '\n';
#if 0
    int current = -1, size = 0;
    std::map<int, std::tuple<int, int> > files;
    for (int mm = 0; int value : disk_b)
    {
        if (value != -1)
        {
            if (value != current)
            {
                if (size > 0)
                {
                    files[current] = { mm - size , size };
                }
                current = value;
                size = 1;
            }
            else ++size;
        }
        ++mm;
    }
    for (auto [fid, info] : files)
    {
        auto [pos, sz] = info;
        std::cout << "(" << pos << ", " << sz << ")\n";
    }
#endif
    std::cout << "Problem B: " << checksum(disk_b) << '\n';
    return EXIT_SUCCESS;
}

