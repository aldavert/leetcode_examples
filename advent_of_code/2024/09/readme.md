# [Day 9: Disk Fragmenter](https://adventofcode.com/2024/day/9)

You are given the run-length corresponding to a *hard-drive filesystem*. The run-length represents the amount of blocks that alternate between a file and empty space. The amount of blocks ranges from 0 to 9. The files are assigned a numeric ID that represents the order of the file from left to right (starting with 0).

#### Problem A

In the first problem, you have to decode the run-length and compress all the blocks (as if you are *defragmenting* the hard-drive) by moving file blocks to empty space. The file blocks are read from right-to-left while free-space blocks are parsed from left-to right. At the end, you will obtain a *hard-drive* where all file blocks will be on the left side of the disk while all free-space blocks will be on the right.

Return the ***checksum*** of the *defragmented* disk.

The **checksum** is computed by adding the products between the file ID of the block and the position of the block in the *hard-drive*.

#### Problem B

In the second problem you have to use a different defragmentation method. Now, you have to move, if possible, all file blocks at the same time to the left-most free space that can contain the file. You have to process files from right to left.

What is the ***checksum*** of the *defragmented* disk now?

