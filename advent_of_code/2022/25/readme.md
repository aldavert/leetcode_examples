# [Day 25: Full of Hot Air](https://adventofcode.com/2022/day/25)

You are given a list of numbers in base 5 that are coded as 2, 1, 0, - (for -1) and = for (-2).

**Sum all the values of the list and return the result in the same format.**


