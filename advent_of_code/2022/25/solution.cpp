#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>
#include <fmt/core.h>

std::ostream& operator<<(std::ostream &out, const std::vector<long> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    for (const auto &v : vec)
        out << v << '\n';;
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<char> &vec)
{
    for (const auto &v : vec) out << v;
    return out;
}

//------------------------------------------------------------------------------

std::string codedSum(std::vector<std::string> information)
{
    const std::unordered_map<char, long> chr2value = {{'2', 2}, {'1', 1}, {'0', 0}, {'-', -1}, {'=', -2}};
    const std::unordered_map<long, char> value2chr = {{2, '2'}, {1, '1'}, {0, '0'}, {-1, '-'}, {-2, '='}};
    long sum = 0;
    for (std::string code : information)
    {
        long value = 0;
        for (long i = static_cast<long>(code.size()) - 1, p = 1; i >= 0; --i, p *= 5)
            value += p * chr2value.at(code[i]);
        sum += value;
    }
    std::vector<char> code;
    for (bool carry = false; sum || carry; sum /= 5)
    {
        long value = sum % 5 + carry;
        carry = false;
        if (value > 2)
        {
            carry = true;
            value = value - 5;
        }
        code.push_back(value2chr.at(value));
    }
    std::string result;
    for (int i = static_cast<int>(code.size()) - 1; i >= 0; --i)
        result += code[i];
    return result;
}

//------------------------------------------------------------------------------

int main(int argc, char * * argv)
{
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line;
    std::vector<std::string> information;
    while (std::getline(file, line))
        information.push_back(line);
    std::cout << "[TEST A] Coded sum: " << codedSum(information) << '\n';
    
    return EXIT_SUCCESS;
}

