# [Day 5: Supply Stacks](https://adventofcode.com/2022/day/5)

#### Problem A

You are given a set of stacks with characters inside and a set of instructions which specify how to reorganize the stacks. The instructions have the form `move <amount> from <source> to <destination`, indicating that you have to move `<amount>` characters from stack `<source>` to stack `<destination>`. In this problem **you can only move one character at a time.**

After the rearrangement procedure completes, **what character ends up on top of each stack?**

#### Problem B

Same problem as before, but now **you can move multiple characters at a time.**

