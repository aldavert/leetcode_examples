#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>

class Supplies
{
public:
    void initialize(const std::vector<std::string> &information)
    {
        initializeLabels(information.back());
        for (int row = static_cast<int>(information.size()) - 2; row >= 0; --row)
        {
            const std::string &active = information[row];
            for (size_t column = 0, last_column = active.size(); column < last_column; ++column)
            {
                while ((column < last_column) && (active[column] != '[')) ++column;
                size_t begin = ++column;
                while ((column < last_column) && (active[column] != ']')) ++column;
                if (begin - column)
                {
                    std::string crate_id = active.substr(begin, column - begin);
                    auto search = labels.upper_bound(begin);
                    if (search == labels.end()) --search;
                    else if ((search != labels.begin())
                         && (begin - std::prev(search)->first < search->first - begin))
                        --search;
                    crates[search->second].push(crate_id);
                }
            }
        }
    };
    void updateSingle(int amount, std::string source, std::string destination)
    {
        for (int unit = 0; unit < amount; ++unit)
        {
            std::string value = crates[source].top();
            crates[source].pop();
            crates[destination].push(value);
        }
    }
    void updateBlock(int amount, std::string source, std::string destination)
    {
        std::stack<std::string> buffer;
        for (int unit = 0; unit < amount; ++unit)
        {
            buffer.push(crates[source].top());
            crates[source].pop();
        }
        while (!buffer.empty())
        {
            crates[destination].push(buffer.top());
            buffer.pop();
        }
    }
    std::string top(void) const
    {
        std::vector<std::string> crates_labels;
        for (const auto& [label, crate] : crates)
            crates_labels.push_back(label);
        std::sort(crates_labels.begin(), crates_labels.end());
        std::string top_id;
        for (auto label : crates_labels)
            top_id += crates.at(label).top();
        return top_id;
    }
    friend std::ostream& operator<<(std::ostream &out, const Supplies &supplies);
protected:
    void initializeLabels(std::string crates_labels)
    {
        for (size_t column = 0, last_column = crates_labels.size(); column < last_column; ++column)
        {
            while ((column < last_column) && (!std::isalnum(crates_labels[column]))) ++column;
            size_t begin = column;
            while ((column < last_column) && (std::isalnum(crates_labels[column]))) ++column;
            if (column - begin > 0)
                labels[begin] = crates_labels.substr(begin, column - begin);
        }
    };
    
    std::unordered_map<std::string, std::stack<std::string> > crates;
    std::map<size_t, std::string> labels;
};
std::ostream& operator<<(std::ostream &out, const Supplies &supplies)
{
    size_t tallest_stack = 0;
    std::vector<std::string> crates_labels;
    for (const auto& [id, stack] : supplies.crates)
    {
        tallest_stack = std::max(tallest_stack, stack.size());
        crates_labels.push_back(id);
    }
    std::sort(crates_labels.begin(), crates_labels.end());
    auto work_copy = supplies.crates;
    for (size_t height = tallest_stack; height > 0; --height)
    {
        for (auto label : crates_labels)
        {
            if (height == work_copy[label].size())
            {
                out << '[' << work_copy[label].top() << ']';
                work_copy[label].pop();
            }
            else out << "   ";
            out << ' ';
        }
        out << '\n';
    }
    for (auto label : crates_labels)
        out << ' ' << label << ' ' << ((label.size() == 1)?" ":"");
    out << '\n';
    return out;
}

int main(int argc, char * * argv)
{
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line;
    Supplies taskA;
    
    // Parse stacks configuration ..............................................
    std::vector<std::string> initial_configuration;
    while (std::getline(file, line))
    {
        initial_configuration.push_back(line);
        if ((line.find('[') == std::string::npos))
            break;
    }
    taskA.initialize(initial_configuration);
    Supplies taskB = taskA;
    std::cout << "Initial cargo configuration:\n";
    std::cout << taskA << '\n';
    // Process crane move orders ...............................................
    while (std::getline(file, line))
    {
        std::istringstream input(line);
        std::string _MOVE, _FROM, _TO;
        int amount;
        std::string source, destination;
        input >> _MOVE >> amount >> _FROM >> source >> _TO >> destination;
        taskA.updateSingle(amount, source, destination);
        taskB.updateBlock(amount, source, destination);
    }
    std::cout << "Final cargo configuration for TASK A:\n";
    std::cout << taskA << '\n';
    std::cout << "Solution is: " << taskA.top() << '\n';
    std::cout << '\n';
    std::cout << "Final cargo configuration for TASK B:\n";
    std::cout << taskB << '\n';
    std::cout << "Solution is: " << taskB.top() << '\n';
    std::cout << '\n';
    
}


