#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>
#include <unordered_set>
#include <variant>
#include <functional>
#include <fmt/core.h>

std::ostream& operator<<(std::ostream &out, const std::vector<long> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    for (const auto &v : vec)
        out << v << '\n';;
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<char> &vec)
{
    for (const auto &v : vec) out << v;
    return out;
}

//------------------------------------------------------------------------------
struct Operation
{
    std::string monkey = "";
    std::variant<std::string, long> first = "";
    std::variant<std::string, long> second = "";
    char operation = '\0';
};
long computeOperations(std::unordered_map<std::string, long> numbers,
                       std::vector<Operation> operations)
{
    std::vector<bool> active(operations.size(), true);
    while (numbers.find("root") == numbers.end())
    {
        for (size_t i = 0, n = operations.size(); i < n; ++i)
        {
            if (!active[i]) continue;
            bool first_number = false, second_number = false;
            if (std::holds_alternative<std::string>(operations[i].first))
            {
                auto search = numbers.find(std::get<std::string>(operations[i].first));
                if (search != numbers.end())
                    operations[i].first = search->second,
                    first_number = true;
            }
            else first_number = true;
            if (std::holds_alternative<std::string>(operations[i].second))
            {
                auto search = numbers.find(std::get<std::string>(operations[i].second));
                if (search != numbers.end())
                    operations[i].second = search->second,
                    second_number = true;
            }
            else second_number = true;
            
            if (first_number && second_number)
            {
                long first_operand = std::get<long>(operations[i].first);
                long second_operand = std::get<long>(operations[i].second);
                active[i] = false;
                switch (operations[i].operation)
                {
                case '+':
                    numbers[operations[i].monkey]  = first_operand + second_operand;
                    break;
                case '-':
                    numbers[operations[i].monkey]  = first_operand - second_operand;
                    break;
                case '*':
                    numbers[operations[i].monkey]  = first_operand * second_operand;
                    break;
                case '/':
                    numbers[operations[i].monkey]  = first_operand / second_operand;
                    break;
                default:
                    break;
                }
            }
        }
    }
    return numbers["root"];
}

long solveEquation([[maybe_unused]] std::unordered_map<std::string, long> numbers,
                   [[maybe_unused]] std::vector<Operation> operations)
{
    std::unordered_map<std::string, std::tuple<std::string, std::string> > neighbors;
    for (size_t i = 0, n = operations.size(); i < n; ++i)
        neighbors[operations[i].monkey] = {std::get<std::string>(operations[i].first),
                                           std::get<std::string>(operations[i].second)};
    std::unordered_set<std::string> left, right;
    std::function<void(std::string, std::unordered_set<std::string> &)> traverseGroup =
        [&](std::string monkey, std::unordered_set<std::string> &group) -> void
    {
        group.insert(monkey);
        if (auto search = neighbors.find(monkey); search != neighbors.end())
        {
            traverseGroup(std::get<0>(search->second), group);
            traverseGroup(std::get<1>(search->second), group);
        }
    };
    std::string left_monkey = std::get<0>(neighbors["root"]);
    std::string right_monkey = std::get<1>(neighbors["root"]);
    traverseGroup(left_monkey, left);
    traverseGroup(right_monkey, right);
    if (right.find("humn") != right.end()) // Human (X) is in the left.
    {
        std::swap(left, right);
        std::swap(left_monkey, right_monkey);
    }
    std::vector<Operation> operations_subset;
    for (size_t i = 0, n = operations.size(); i < n; ++i)
    {
        Operation operation = operations[i];
        if (right.find(operation.monkey) != right.end())
        {
            if (operation.monkey == right_monkey)
                operation.monkey = "root";
            operations_subset.push_back(operation);
        }
    }
    numbers[left_monkey] = computeOperations(numbers, operations_subset);
    operations_subset.clear();
    std::vector<std::string> humanPath;
    std::function<bool(std::string)> searchHuman = [&](std::string monkey) -> bool
    {
        if (monkey == "humn") return true;
        if (auto search = neighbors.find(monkey); search != neighbors.end())
        {
            if (searchHuman(std::get<0>(search->second))
            ||  searchHuman(std::get<1>(search->second)))
            {
                humanPath.push_back(monkey);
                return true;
            }
            return false;
        }
        return false;
    };
    searchHuman(left_monkey);
    std::unordered_set<std::string> humanSet(humanPath.begin(), humanPath.end());
    std::unordered_map<std::string, size_t> humanLut;
    for (size_t i = 0, n = operations.size(); i < n; ++i)
    {
        Operation operation = operations[i];
        if (left.find(operation.monkey) != left.end())
        {
            if (humanSet.find(operation.monkey) != humanSet.end())
                humanLut[operation.monkey] = i;
            else if (operation.monkey != left_monkey)
                operations_subset.push_back(operation);
        }
    }
    std::unordered_map<char, char> inverse = {{'+', '-'}, {'-', '+'}, {'*', '/'}, {'/', '*'}};
    std::string monkey_interest = "humn";
    for (auto monkey : humanPath)
    {
        Operation operation = operations[humanLut[monkey]];
        std::string left_op  = std::get<std::string>(operation.first);
        std::string right_op = std::get<std::string>(operation.second);
        Operation new_operation;
        if (left_op == monkey_interest)
            new_operation = {
                monkey_interest,
                operation.monkey,
                right_op,
                inverse[operation.operation] };
        else
        {
            if ((operation.operation == '-') || (operation.operation == '/'))
                new_operation = {
                    monkey_interest,
                    left_op,
                    operation.monkey,
                    operation.operation };
            else
                new_operation = {
                    monkey_interest,
                    operation.monkey,
                    left_op,
                    inverse[operation.operation] };
        }
        if (monkey_interest == "humn") new_operation.monkey = "root";
        monkey_interest = monkey;
        operations_subset.push_back(new_operation);
    }
    return computeOperations(numbers, operations_subset);
/****
pppw: 150
pppw: cczh / lfqf -> cczh = pppw * lfqf | pppw: lfqf / cczh -> cczh = 
cczh: sllz + lgvd -> lgvd = cczh - sllz
lgvd: ljgn * ptdq -> ptdq = lgvd / ljgn
ptdq: humn - dvpt -> humn = ptdq + dvpt
Tree where 'humn' is found in the example:
                        pppw (/)
                       /    \
                      /      \
                     /        \
                    /          \
                cczh (+)        lfqf (Number)
               /    \
              /      \
             /        \
            /          \
        sllz (Number)   lgvd (*)
                       /    \
                      /      \
                     /        \
                    /          \
                ljgn (Number)   ptdq (-)
                               /    \
                              /      \
                             /        \
                            /          \
                        humn (X)        dvpt (Number)
*/
}

//------------------------------------------------------------------------------

int main(int argc, char * * argv)
{
    std::unordered_map<std::string, long> numbers;
    //std::unordered_map<std::string, std::vector<Operation *> dependencies;
    std::vector<Operation> operations;
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line;
    while (std::getline(file, line))
    {
        std::string monkey, text;
        char op;
        std::istringstream input(line);
        input >> monkey;
        monkey.pop_back();
        input >> text;
        if (input >> op)
        {
            std::string second;
            input >> second;
            operations.push_back({monkey, text, second, op});
        }
        else numbers[monkey] = std::atoi(text.c_str());
    }
    std::cout << "[TEST A] Operations result: " << computeOperations(numbers, operations) << '\n';
    std::cout << "[TEST A] Solve equation: " << solveEquation(numbers, operations) << '\n';
    
    return EXIT_SUCCESS;
}


