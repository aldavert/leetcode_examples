# [Day 21: Monkey Math](https://adventofcode.com/2022/day/21)

#### Problem A

You are given a list of *labels* that are associated to a *number* or to a *mathematical operation*. You have to send forward the *number* labels and **compute the result of the mathematical operation labeled** `root`.

#### Problem B

Its a variation of the previous problem, but now **you have to compute the value of the variable** `humn` and the operation labeled `root` **is a comparison operator**.

*Solution notes*
Figure out which the to branches of `root` has `humn` and which only has regular operations. Then, solve the branch containing regular operations *(using function from problem A)*. Now, you know that the branch containing `humn` has to be equal to the obtained value. Finally,create the equations needed to solve `humn` modifying the necessary equations from the branch containing it and, compute its value using the function created in the previous problem.


