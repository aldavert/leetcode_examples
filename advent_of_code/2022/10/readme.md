# [Day 10: Cathode-Ray Tube](https://adventofcode.com/2022/day/10)

#### Problem A

You have a device with a single register `x` (initialized at `1`) and it is able to interpret two instructions `noop` and `addx <value>`:

- `noop` does nothing *(no operation)* and takes **1 cycle to execute**.
- `addx <value>` adds the value to the `x` register. It takes **2 cycles to execute** and the value of `x` is only changed after the instruction is completed (i.e. at the end of the second cycle).

For example these three instructions:
```
noop
addx 3
addx -5
```
Modify the register `x` as follows:

```
instruction cycle register
noop        1     1
addx 3      2     1
  "         3     1
addx -5     4     4
  "         5     4
.......     6     -1
```

Given a longer sequence of instructions **compute what is the sum of the register x multiplied by the cycle value at the 20th, 60th, 100th, 140th, 180th and 220th cycles.**

For the following sample instruction set, the accumulated signal strengths is `13140`:

```
addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop
```

#### Problem B

The CPU is used to draw messages on a simple CRT screen with `6` rows of `40` pixels each. Pixels are activated or deactivated following a raster pattern. To activate a pixel, a **3 pixel wide sprite** has to be over it, otherwise the pixel is deactivated. The position of the sprite over the current row is controlled by the register `x` of the CPU.

Following the previous sample instruction set, the CRT displays the following image:
```
##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######.....
```

