#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>

struct Device
{
    int cycle = 0;
    int x = 1;
    int sum = 0;
    char screen[10][41] = {};
    void step(void)
    {
        ++cycle;
        if ((cycle >= 20) && ((cycle - 20) % 40 == 0))
            sum += x * cycle;
        int mod_cycle = (cycle - 1) % 40;
        screen[(cycle - 1) / 40][mod_cycle] =
            ((mod_cycle + 1 - x >= 0) && (mod_cycle + 1 - x <= 2))?'#':' ';
    }
};

int main(int argc, char * * argv)
{
    Device d;
    std::string line;
    
    std::ifstream file((argc == 1)?"data_test_larger.txt":argv[1]);
    while (std::getline(file, line))
    {
        if (line == "noop")
        {
            d.step();
        }
        else
        {
            std::istringstream input(line);
            std::string instruction;
            int value;
            input >> instruction >> value;
            d.step();
            d.step();
            d.x += value;
        }
    }
    std::cout << "Sum of signal strength: " << d.sum << '\n';
    std::cout << "Screen image:\n";
    for (int i = 0; i < 6; ++i)
        std::cout << d.screen[i] << '\n';
    return EXIT_SUCCESS;
}

