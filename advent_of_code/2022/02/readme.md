# [Day 2: Rock Paper Scissors](https://adventofcode.com/2022/day/2)

#### Problem A

You have to simulate a game of rock-paper-scissors and compute the score with the given set of rules. You are provided a list of actions where:

- `playerA` plays *Rock* when the cue is `A`, *Paper* for `B`, and *Scissors* for `C`.
- `playerB` plays *Rock* when the cue is `X`, *Paper* for `Y`, and *Scissors* for `Z`.

The player's score at each round is computed as:

- Depending on the **selected shape**: `1` for *Rock*, `2` for *Paper*, and `3` for *Scissors*.
- Depending on the **outcome of the round**: `0` if you *lose*, `3` for a *draw*, and `6` if you *win*.

For example, for the given set of plays:
```
A Y
B X
C Z
```
The scores are computed as:

|`playerA`|`playerB`|
|:-------:|:-------:|
|`1+0=1`|`2+6=8`|
|`2+6=8`|`1+0=1`|
|`3+3=6`|`3+3=6`|

In this case, both players will score `15`. **What is the score of** `playerB` **when computed on the complete set of instructions?**

#### Problem B

Now, the list of instructions the play of `playerB` has a different meaning:
- `X` means that `playerB` has to lose.
- `Y` means that `playerB` has to end in a draw.
- `Z` means that `playerB` has to win.

**Compute the score of** `playerB` **using the new interpretion of the list meaning.**


