#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>

int main(int /*argc*/, char * * /*argv*/)
{
    std::unordered_map<char, int> decode_elf = {{'A', 0}, {'B', 1}, {'C', 2}};
    std::unordered_map<char, int> decode_you = {{'X', 0}, {'Y', 1}, {'Z', 2}};
    auto roundScore = [](int elf, int you) -> int
    {
        return 6 * ((elf == 0) && (you == 1))
             + 6 * ((elf == 1) && (you == 2))
             + 6 * ((elf == 2) && (you == 0))
             + 3 * (elf == you);
    };
    std::ifstream file("data.txt");
    std::string line;
    int result_guessed = 0, result_actual = 0;
    while (std::getline(file, line))
    {
        std::istringstream input(line);
        char code_elf, code_you;
        input >> code_elf >> code_you;
        int elf = decode_elf[code_elf];
        int you = decode_you[code_you];
        result_guessed += roundScore(elf, you) + you + 1;
        int expected = 0;
        if      (you == 0) expected = (3 + (elf - 1) % 3) % 3;
        else if (you == 1) expected = elf;
        else if (you == 2) expected = (elf + 1) % 3;
        result_actual += you * 3 + expected + 1;
    }
    file.close();
    std::cout << "Expected score with guessed guide: "
              << result_guessed << '\n';
    std::cout << "Expected score with actual guide: "
              << result_actual << '\n';
}

