# [Day 4: Camp Cleanup](https://adventofcode.com/2022/day/4)

#### Problem A

You are given a list of pairs of intervals: `a-b,c-d`, where `a` and `c` are the start of the intervals, `b` and `d` are their ends and, `a <= b` and `c <= d`.

**How many intervals are fully contained inside another interval?**

#### Problem B

**How many intervals are overlapping?**

