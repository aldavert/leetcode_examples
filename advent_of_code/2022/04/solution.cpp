#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>

int main(int /*argc*/, char * * /*argv*/)
{
    std::ifstream file("data.txt");
    std::string line;
    int result_contained = 0, result_overlap = 0;
    while (std::getline(file, line))
    {
        // Parse file ........................................................
        std::istringstream input(line);
        int first[2], second[2];
        char comma;
        input >> first[0] >> first[1] >> comma
              >> second[0] >> second[1];
        first[1] = -first[1];
        second[1] = -second[1];
        // Compute ...........................................................
        int intersect[2] = { std::max(first[0], second[0]),
                             std::min(first[1], second[1]) };
        if (intersect[0] <= intersect[1])
        {
            if (((intersect[0] == first[0] ) && (intersect[1] == first[1] ))
            ||  ((intersect[0] == second[0]) && (intersect[1] == second[1])))
                ++result_contained;
            ++result_overlap;
        }
    }
    std::cout << "Number of pairs where an interval is completely contained "
                 "in the other: " << result_contained << '\n';
    std::cout << "Number of pairs overlapping: " << result_overlap << '\n';
    file.close();
}

