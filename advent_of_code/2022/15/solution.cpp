#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>
#include <fmt/core.h>

#define GEOMETRIC_SEARCH 1

std::ostream& operator<<(std::ostream &out, const std::vector<long> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    for (const auto &v : vec)
        out << v << '\n';;
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<char> &vec)
{
    for (const auto &v : vec) out << v;
    return out;
}

struct SensorBeacon
{
    SensorBeacon(long sx, long sy, long bx, long by) :
        sensor_x(sx),
        sensor_y(sy),
        beacon_x(bx),
        beacon_y(by)
    {
        distance = std::abs(sx - bx) + std::abs(sy - by);
    }
    long sensor_x = 0;
    long sensor_y = 0;
    long beacon_x = 0;
    long beacon_y = 0;
    long distance = 0;
    friend std::ostream& operator<<(std::ostream &out, const SensorBeacon &sb)
    {
        out << fmt::format("S({}, {})->B({}, {})",
                           sb.sensor_x, sb.sensor_y,
                           sb.beacon_x, sb.beacon_y);
        return out;
    }
    bool operator<(const SensorBeacon &other) const { return sensor_x < other.sensor_x; }
};

struct Segment
{
    long begin = std::numeric_limits<long>::lowest();
    long end = std::numeric_limits<long>::lowest();
    bool operator<(const Segment &other) const
    {
        return (begin < other.begin)
            || ((begin == other.begin) && (end < other.end));
    }
    friend std::ostream& operator<<(std::ostream &out, const Segment &s)
    {
        out << '(' << s.begin << ", " << s.end << ')';
        return out;
    }
};
// TEST A ----------------------------------------------------------------------
long nonBeaconRow(const std::vector<SensorBeacon> &devices, long row)
{
    std::vector<Segment> segments;
    for (const auto &device : devices)
    {
        long p1 = device.sensor_x - device.distance + std::abs(device.sensor_y - row);
        long p2 = device.sensor_x + device.distance - std::abs(device.sensor_y - row);
        if (p1 < p2)
            segments.push_back({p1, p2});
    }
    if (segments.size())
    {
        std::sort(segments.begin(), segments.end());
        size_t overlap = 0;
        for (Segment previous; auto segment : segments)
        {
            if (segment.end <= previous.end) continue; // Segment within larger segment.
            overlap += segment.end - segment.begin;
            if (segment.begin <= previous.end)
                overlap -= previous.end - segment.begin;
            previous = segment;
        }
        
        return overlap;
    }
    return 0;
}
// TEST B ----------------------------------------------------------------------
std::tuple<long, long> searchFreeSpace(const std::vector<SensorBeacon> &devices,
                                       long separation_minimum,
                                       long separation_maximum,
                                       long begin,
                                       long end)
{
    const size_t n = devices.size();
    struct Line
    {
        long x = -1;
        long y = -1;
        long length = 0;
    };
    std::vector<Line> left_leaning_lines, right_leaning_lines;
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = i + 1; j < n; ++j)
        {
            long separation = std::abs(devices[i].sensor_x - devices[j].sensor_x)
                            + std::abs(devices[i].sensor_y - devices[j].sensor_y)
                            - devices[i].distance - devices[j].distance - 1;
            // NOTE: 'separation' is the distance between the influence areas of two
            //       different devices (devices 'i' and 'j').
            if ((separation >= separation_minimum) && (separation <= separation_maximum))
            {
                // NOTE: This ignores devices that are aligned. Although the devices
                //       are at the proper distance, they only touch by an edge of
                //       their influence areas.
                if ((devices[i].sensor_x == devices[j].sensor_x)
                ||  (devices[i].sensor_y == devices[j].sensor_y)) continue;
                
                long dx = devices[i].sensor_x - devices[j].sensor_x;
                long dy = devices[i].sensor_y - devices[j].sensor_y;
                long x0, y0, x1, y1;
                if (std::abs(dy) > std::abs(dx))
                {
                    long sign = (2 * (dy < 0) - 1);
                    x0 = devices[i].sensor_x - dx;
                    y0 = devices[i].sensor_y + sign * (devices[i].distance - std::abs(dx) + 1);
                    x1 = devices[i].sensor_x;
                    y1 = devices[i].sensor_y + sign * (devices[i].distance + 1);
                }
                else
                {
                    long sign = (2 * (dx < 0) - 1);
                    x0 = devices[i].sensor_x + sign * (devices[i].distance - std::abs(dy) + 1);
                    y0 = devices[i].sensor_y - dy;
                    x1 = devices[i].sensor_x + sign * (devices[i].distance + 1);
                    y1 = devices[i].sensor_y;
                }
                if (y0 > y1) std::swap(x0, x1), std::swap(y0, y1);
                if (x0 > x1) right_leaning_lines.push_back({x0, y0, y1 - y0});
                else left_leaning_lines.push_back({x0, y0, y1 - y0});
            }
        }
    }
    std::set<std::tuple<long, long> > intersections;
    auto intersection = [&](const Line &left, const Line &right) -> void
    {
        double lambda = -static_cast<double>(right.y - left.y - right.x + left.x)
                      *  static_cast<double>(left.length)
                      /  static_cast<double>(2 * right.length * left.length);
        if ((lambda > 0.01) && (lambda < 0.99))
        {
            long x = static_cast<long>(static_cast<double>(right.x)
                   - lambda * static_cast<double>(right.length));
            long y = static_cast<long>(static_cast<double>(right.y)
                   + lambda * static_cast<double>(right.length));
            if ((y >= begin) && (y <= end))
                intersections.insert({x, y});
        }
    };
    for (auto left_leaning_line : left_leaning_lines)
        for (auto right_leaning_line : right_leaning_lines)
            intersection(left_leaning_line, right_leaning_line);
    return (intersections.size())?*intersections.begin():std::tuple<long, long>({-1, -1});
}

long possibleBeacon(const std::vector<SensorBeacon> &devices,
                    long row,
                    long begin,
                    long end,
                    std::vector<Segment> &segments)
{
    size_t valid = 0;
    for (const auto &device : devices)
    {
        long p1 = device.sensor_x - device.distance + std::abs(device.sensor_y - row);
        long p2 = device.sensor_x + device.distance - std::abs(device.sensor_y - row);
        p1 = std::max(p1, begin);
        p2 = std::min(p2, end);
        if (p1 < p2)
            segments[valid++] = {p1, p2};
    }
    if (valid)
    {
        std::sort(&segments[0], &segments[valid]);
        Segment previous = {begin, begin};
        for (size_t i = 0; i < valid; ++i)
        {
            if (segments[i].end <= previous.end) continue; // Segment within larger segment.
            if (segments[i].begin > previous.end + 1)
                return previous.end + 1;
            previous = segments[i];
        }
        return -1;
    }
    return -1;
}
// -----------------------------------------------------------------------------

int main(int argc, char * * argv)
{
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line;
    std::vector<SensorBeacon> devices;
    while (std::getline(file, line))
    {
        std::string text;
        char letter;
        
        std::istringstream input(line);
        int sx, sy, bx, by;
        input >> text >> text >> letter >> letter;
        input >> sx >> letter >> letter >> letter;
        input >> sy >> letter >> text >> text >> text >> text;
        input >> letter >> letter >> bx >> letter >> letter;
        input >> letter >> by;
        devices.emplace_back(sx, sy, bx, by);
    }
    std::sort(devices.begin(), devices.end());
    std::vector<Segment> segments(devices.size());
    if (devices.size() < 20)
    {
        // Problem 1:
        std::cout << "Non-beacon positions at 10: "
                  << nonBeaconRow(devices, 10) << '\n';
        // Problem 2:
#if GEOMETRIC_SEARCH
        auto [sx, sy] = searchFreeSpace(devices, 1, 1, 0, 20);
        std::cout << "Tuning frequency: " << 4'000'000 * sx + sy << '\n';
#else
        for (int y = 0; y <= 20; ++y)
            if (long x = possibleBeacon(devices, y, 0, 20, segments); x != -1)
                std::cout << "Tuning frequency: " << 4'000'000 * x + y << '\n';
#endif
    }
    else
    {
        std::cout << "Non-beacon positions at 2'000'000: "
                  << nonBeaconRow(devices, 2'000'000) << '\n';
#if GEOMETRIC_SEARCH
        auto [sx, sy] = searchFreeSpace(devices, 1, 1, 0, 4'000'000);
        std::cout << "Tuning frequency: " << 4'000'000 * sx + sy << '\n';
#else
        for (int y = 0; y <= 4'000'000; ++y)
            if (long x = possibleBeacon(devices, y, 0, 4'000'000, segments); x != -1)
                std::cout << "Tuning frequency: " << 4'000'000 * x + y << '\n';
#endif
    }
    
    return EXIT_SUCCESS;
}

