# [Day 15: Beacon Exclusion Zone](https://adventofcode.com/2022/day/15)

#### Problem A

You have a set of sensors and beacons that can be detected by this sensors. Each sensor detects a single beacon (the one closest to it using the Manhattan distance). You are given a list with the coordinates of the sensors and its closest beacons.

Using this information, **calculate how many positions cannot contain a beacon in a given row?**

#### Problem B

Now, compute where is the only position in the whole map between coordinates `(0, 0)` and `(4000000, 4000000)` that does not contain a beacon and **compute the beacon frequency as** `4000000 * x + y` **where** `x` **and** `y` **are the coordinates of the beacon.**.

