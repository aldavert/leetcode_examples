# [Day 24: Blizzard Basin](https://adventofcode.com/2022/day/24)

#### Problem A

You are given a map with moving obstacles. You have to navigate the map from the entry point to the exit.

The obstacles move horizontally, *left-right* or *right-left*, and vertically, *top-bottom* or *bottom-top*. When an object reaches the map edge, it is respawned at the opposite edge and keeping the same heading.

**How many ticks of time it take to go from the entry to the exit?**

#### Problem B

**How many ticks of time it take to go from the entry to the exit, back to the entry and finally back to the exit?**


