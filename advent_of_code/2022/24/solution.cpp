#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>
#include <fmt/core.h>

std::ostream& operator<<(std::ostream &out, const std::vector<long> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::string> &vec)
{
    for (const auto &v : vec)
        out << v << '\n';;
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<char> &vec)
{
    for (const auto &v : vec) out << v;
    return out;
}

//------------------------------------------------------------------------------
enum class DIRECTION { EAST = 0, SOUTH, WEST, NORTH };
struct Point
{
    int x = -1;
    int y = -1;
    bool operator==(const Point &other) const { return (x == other.x) && (y == other.y); }
    bool operator<(const Point &other) const
    {
        return (x < other.x)
            || ((x == other.x) && (y < other.y));
    }
    Point move(DIRECTION heading)
    {
        constexpr int dx[4] = { 1, 0, -1, 0 };
        constexpr int dy[4] = { 0, 1, 0, -1 };
        return {x + dx[static_cast<int>(heading)],
                y + dy[static_cast<int>(heading)]};
    }
    friend std::ostream& operator<<(std::ostream &out, const Point &p)
    {
        out << '{' << p.x << ", " << p.y << '}';
        return out;
    }
};
struct Blizzard
{
    Point position;
    void move(int width, int height)
    {
        position = position.move(heading);
        if      (position.x == 0) position.x = width - 2;
        else if (position.x == width - 1) position.x = 1;
        if      (position.y == 0) position.y = height - 2;
        else if (position.y == height - 1) position.y = 1;
    }
    DIRECTION heading;
};
struct Map
{
    Point entry;
    Point exit;
    int width;
    int height;
    std::vector<Blizzard> blizzards;
    // -------------------------------------------------------------------------
    std::vector<std::string> draw(void) const
    {
        const static std::unordered_map<DIRECTION, char> dir2symb = {
            {DIRECTION::EAST, '>'}, {DIRECTION::SOUTH, 'v'},
            {DIRECTION::WEST, '<'}, {DIRECTION::NORTH, '^'}};
        std::vector<std::string> out;
        out.push_back(std::string(width, '#'));
        out.back()[entry.x] = '.';
        for (int h = 1; h < height - 1; ++h)
        {
            out.push_back(std::string(width, '.'));
            out.back()[0] = '#';
            out.back()[width - 1] = '#';
        }
        out.push_back(std::string(width, '#'));
        out.back()[exit.x] = '.';
        for (const auto &blizzard : blizzards)
        {
            char &pixel = out[blizzard.position.y][blizzard.position.x];
            if (pixel == '.')
                pixel = dir2symb.at(blizzard.heading);
            else
            {
                if ((pixel >= '0') && (pixel <= '9'))
                    ++pixel;
                else pixel = '2';
            }
        }
        return out;
    }
    void tick(void)
    {
        for (auto &blizzard : blizzards)
            blizzard.move(width, height);
    }
    bool collision(Point pos) const
    {
        for (const auto &blizzard : blizzards)
            if (blizzard.position == pos)
                return true;
        return false;
    }
};

int shortestPathLength(Map &map)
{
    int time = 0;
    std::queue<Point> active;
    active.push(map.entry);
    auto valid = [&](const Point &p) -> bool
    {
        return (p == map.exit)
            || (p == map.entry)
            || ((p.x > 0) && (p.y > 0) && (p.x < map.width - 1) && (p.y < map.height - 1));
    };
    while (!active.empty())
    {
        map.tick();
        std::set<Point> putative;
        for (size_t i = 0, n = active.size(); i < n; ++i)
        {
            Point current = active.front();
            active.pop();
            
            if (current == map.exit) return time;
            if (!map.collision(current)) putative.insert(current);
            if (auto next = current.move(DIRECTION::EAST);
                valid(next) && (!map.collision(next)))
                putative.insert(next);
            if (auto next = current.move(DIRECTION::SOUTH);
                valid(next) && (!map.collision(next)))
                putative.insert(next);
            if (auto next = current.move(DIRECTION::WEST);
                valid(next) && (!map.collision(next)))
                putative.insert(next);
            if (auto next = current.move(DIRECTION::NORTH);
                valid(next) && (!map.collision(next)))
                putative.insert(next);
        }
        for (auto &position : putative)
            active.push(position);
        ++time;
    }
    return time;
}
//------------------------------------------------------------------------------

int main(int argc, char * * argv)
{
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line;
    int h = 0;
    Map map;
    while (std::getline(file, line))
    {
        if ((line[1] == '#') || (line[2] == '#') || (line[3] == '#'))
        {
            if (h == 0) map.entry = {static_cast<int>(line.find('.')), 0};
            else map.exit = {static_cast<int>(line.find('.')), h};
            map.width = static_cast<int>(line.size());
        }
        else
        {
            for (int w = 0, n = static_cast<int>(line.size()); w < n; ++w)
            {
                if      (line[w] == '>') map.blizzards.push_back({{w, h}, DIRECTION::EAST});
                else if (line[w] == 'v') map.blizzards.push_back({{w, h}, DIRECTION::SOUTH});
                else if (line[w] == '<') map.blizzards.push_back({{w, h}, DIRECTION::WEST});
                else if (line[w] == '^') map.blizzards.push_back({{w, h}, DIRECTION::NORTH});
            }
        }
        ++h;
    }
    map.height = h;
    
    int time = shortestPathLength(map);
    std::cout << "[TEST A] Shortest path to go reach the exit: " << time << '\n';
    std::swap(map.entry, map.exit);
    time += shortestPathLength(map) + 1;
    std::cout << "Back to entry: " << time << '\n';
    std::swap(map.entry, map.exit);
    time += shortestPathLength(map) + 1;
    std::cout << "[TEST B] Back to exit: " << time << '\n';
    
    return EXIT_SUCCESS;
}
