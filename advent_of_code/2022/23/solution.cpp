#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>
#include <fmt/core.h>

std::ostream& operator<<(std::ostream &out, const std::vector<long> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    for (const auto &v : vec)
        out << v << '\n';;
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<char> &vec)
{
    for (const auto &v : vec) out << v;
    return out;
}

//------------------------------------------------------------------------------
int expandArea(const std::vector<std::string> text_map, int max_iteration)
{
    const int lut_window_size = 5;
    constexpr int MID = std::numeric_limits<int>::max() / 2;
    struct Position
    {
        int y = 0;
        int x = 0;
        unsigned long signature(unsigned int factor = 1) const
        {
            return static_cast<unsigned long>((y + MID) / factor) << 32
                 | static_cast<unsigned long>((x + MID) / factor);
        }
        int distance(const Position &other) const
        {
            return std::max(std::abs(y - other.y), std::abs(x - other.x));
        }
        unsigned char neighborsMask(const Position * neighbors, size_t number_of_neighbors) const
        {
            unsigned char mask = 0;
            for (size_t i = 0; i < number_of_neighbors; ++i)
            {
                int dy = y - neighbors[i].y;
                int dx = x - neighbors[i].x;
                int shift = (-dy + 1) * 3 + (-dx + 1)
                          - (((dy == 0) && (dx <= 0)) || (dy == -1));
                mask |= 1 << shift;
            }
            return mask;
        }
        Position move(int direction)
        {
            static constexpr int dx[4] = {  0,  0, -1,  1 };
            static constexpr int dy[4] = { -1,  1,  0,  0 };
            return { y + dy[direction], x + dx[direction] };
        }
    };
    Position neighborhood[8];
    //// Neighborhood coding:
    //// 012 Activate the flags of the mask `76543210` when there is a neighbor
    //// 3X4 at one of the positions of the grid.
    //// 567                                         76543210
    constexpr unsigned char orientationMask[4] = { 0b00000111,     // NORTH CLEAR
                                                   0b11100000,     // SOUTH CLEAR
                                                   0b00101001,     // WEST CLEAR
                                                   0b10010100 };   // EAST CLEAR
    std::vector<Position> elfs;
    if (text_map.size() == 0) return -1;
    const int height = static_cast<int>(text_map.size());
    const int width = static_cast<int>(text_map[0].size());
    if (width == 0) return -1;
    for (int h = 0; h < height; ++h)
        for (int w = 0; w < width; ++w)
            if (text_map[h][w] == '#')
                elfs.push_back({h, w});
    const size_t n = elfs.size();
    int iteration = 0;
    std::unordered_map<unsigned long, std::vector<size_t> > positions;
    for (bool all_valid = false; !all_valid; ++iteration)
    {
        if (iteration % ((lut_window_size - 1) / 2) == 0)
        {
            positions.clear();
            for (size_t i = 0; i < n; ++i)
            {
                for (int dy = -lut_window_size; dy <= lut_window_size; dy += lut_window_size)
                {
                    for (int dx = -lut_window_size; dx <= lut_window_size; dx += lut_window_size)
                    {
                        Position current = {elfs[i].y + dy, elfs[i].x + dx};
                        positions[current.signature(lut_window_size)].push_back(i);
                    }
                }
            }
        }
        
        std::unordered_map<unsigned long, int> occupation;
        std::vector<std::tuple<Position, int> > putative;
        all_valid = true;
        for (size_t i = 0; i < n; ++i)
        {
            size_t number_of_neighbors = 0;
            for (size_t j : positions[elfs[i].signature(lut_window_size)])
                if ((i != j) && (elfs[i].distance(elfs[j]) == 1))
                    neighborhood[number_of_neighbors++] = elfs[j];
            if (number_of_neighbors > 0)
            {
                all_valid = false;
                if (number_of_neighbors > 5) continue;
                unsigned char mask = elfs[i].neighborsMask(neighborhood, number_of_neighbors);
                for (int o = 0; o < 4; ++o)
                {
                    if (int direction = (o + iteration) % 4;
                        (orientationMask[direction] & mask) == 0)
                    {
                        auto next = elfs[i].move(direction);
                        ++occupation[next.signature()];
                        putative.push_back({next, i});
                        break;
                    }
                }
            }
        }
        for (auto [next, i] : putative)
            if (occupation[next.signature()] == 1)
                elfs[i] = next;
        if ((max_iteration > 0) && (iteration == 10)) break;
    }
    int x_min = std::numeric_limits<int>::max(),
        x_max = std::numeric_limits<int>::lowest(),
        y_min = std::numeric_limits<int>::max(),
        y_max = std::numeric_limits<int>::lowest();
    for (size_t i = 0; i < n; ++i)
    {
        x_min = std::min(x_min, elfs[i].x);
        y_min = std::min(y_min, elfs[i].y);
        x_max = std::max(x_max, elfs[i].x);
        y_max = std::max(y_max, elfs[i].y);
    }
    
    return (max_iteration <= 0)?iteration:((x_max - x_min + 1) * (y_max - y_min + 1) - static_cast<int>(n));
}

//------------------------------------------------------------------------------

int main(int argc, char * * argv)
{
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line;
    std::vector<std::string> text_map;
    while (std::getline(file, line))
        text_map.push_back(line);
    std::cout << "EXPECTED RESULTS:\n";
    std::cout << "  · [SIMPLE] Test A:  110; Test B:  20\n";
    std::cout << " · [COMPLEX] Test A: 4114; Test B: 970\n";
    std::cout << "----------------------------------------------\n\n\n";
    std::cout << "[TEST A] Number of empty cells: " << expandArea(text_map, 10) << '\n';
    std::cout << "[TEST B] Total number of iterations: " << expandArea(text_map, -1) << '\n';
    
    return EXIT_SUCCESS;
}

