#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>
#include <fmt/core.h>

std::ostream& operator<<(std::ostream &out, const std::vector<long> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    for (const auto &v : vec)
        out << v << '\n';;
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<char> &vec)
{
    for (const auto &v : vec) out << v;
    return out;
}

//------------------------------------------------------------------------------
enum class DIRECTION { NORTH = 0, SOUTH, WEST, EAST };
int expandArea(std::vector<std::vector<bool> > map, int max_iterations)
{
    constexpr int check_dx[4][3] = {{-1,  0,  1},
                                    {-1,  0,  1},
                                    {-1, -1, -1},
                                    { 1,  1,  1}};
    constexpr int check_dy[4][3] = {{-1, -1, -1},
                                    { 1,  1,  1},
                                    {-1,  0,  1},
                                    {-1,  0,  1}};
    if (map.size() == 0) return -1;
    const int height = static_cast<int>(map.size());
    const int width = static_cast<int>(map[0].size());
    if (width == 0) return -1;
    for (const auto &row : map)
        if (row.size() != static_cast<size_t>(width))
            return -1;
    std::vector<std::vector<char> > proposed(height, std::vector<char>(width));
    std::vector<std::vector<char> > movement(height, std::vector<char>(width));
    
    DIRECTION direction = DIRECTION::NORTH;
    int iteration = 0;
    for (bool all_valid = false; !all_valid;)
    {
        // Phase 0: Initialization.
        all_valid = true;
        for (int h = 0; h < height; ++h)
            for (int w = 0; w < width; ++w)
                proposed[h][w] = 0,
                movement[h][w] = -1;
        // Phase 1: Move proposal.
        for (int h = 0; h < height; ++h)
        {
            for (int w = 0; w < width; ++w)
            {
                if (map[h][w])
                {
                    // No out-of-bounds check.
                    if ((h == 1) || (w == 1) || (h + 2 == height) || (w + 2 == width))
                        return -1;
                    if (map[h + 1][w - 1] || map[h + 1][w] || map[h + 1][w + 1]
                    ||  map[h - 1][w - 1] || map[h - 1][w] || map[h - 1][w + 1]
                    ||  map[h][w - 1]     || map[h][w + 1])
                    {
                        all_valid = false;
                        DIRECTION current = direction;
                        for (int k = 0; k < 4; ++k)
                        {
                            int o = static_cast<int>(current);
                            if (!(map[h + check_dy[o][0]][w + check_dx[o][0]]
                            ||    map[h + check_dy[o][1]][w + check_dx[o][1]]
                            ||    map[h + check_dy[o][2]][w + check_dx[o][2]]))
                            {
                                ++proposed[h + check_dy[o][1]][w + check_dx[o][1]];
                                movement[h][w] = static_cast<char>(o);
                                break;
                            }
                            current = static_cast<DIRECTION>((o + 1) % 4);
                        }
                    }
                }
            }
        }
        // Phase 2: Move execution.
        for (int h = 0; h < height; ++h)
        {
            for (int w = 0; w < width; ++w)
            {
                int o = movement[h][w];
                if ((o >= 0) && (proposed[h + check_dy[o][1]][w + check_dx[o][1]] == 1))
                    map[h + check_dy[o][1]][w + check_dx[o][1]] = true,
                    map[h][w] = false;
            }
        }
        direction = static_cast<DIRECTION>((static_cast<int>(direction) + 1) % 4);
        ++iteration;
        if ((max_iterations > 0) && (iteration >= max_iterations)) break;
    }
    if (max_iterations < 0) return iteration;
    int x_min = width, x_max = 0, y_min = height, y_max = 0, count = 0;
    for (int h = 0; h < height; ++h)
    {
        for (int w = 0; w < width; ++w)
        {
            if (map[h][w])
            {
                x_min = std::min(x_min, w);
                x_max = std::max(x_max, w);
                y_min = std::min(y_min, h);
                y_max = std::max(y_max, h);
                ++count;
            }
        }
    }
    return (x_max - x_min + 1) * (y_max - y_min + 1) - count;
}

int expandArea(const std::vector<std::string> text_map, unsigned int max_iteration)
{
    const int margins = 100;
    if (text_map.size() == 0) return -1;
    const int height = static_cast<int>(text_map.size());
    const int width = static_cast<int>(text_map[0].size());
    if (width == 0) return -1;
    for (const auto &row : text_map)
        if (row.size() != static_cast<size_t>(width))
            return -1;
    std::vector<std::vector<bool> > map(height * (margins * 2 + 1), std::vector<bool>(width * (margins * 2 + 1), false));
    for (int h = 0; h < height; ++h)
        for (int w = 0; w < width; ++w)
            map[margins * height + h][margins * width + w] = (text_map[h][w] == '#');
    return expandArea(map, max_iteration);
}

//------------------------------------------------------------------------------

int main(int argc, char * * argv)
{
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line;
    std::vector<std::string> text_map;
    while (std::getline(file, line))
        text_map.push_back(line);
    std::cout << "EXPECTED RESULTS:\n";
    std::cout << "  · [SIMPLE] Test A:  110; Test B:  20\n";
    std::cout << " · [COMPLEX] Test A: 4114; Test B: 970\n";
    std::cout << "----------------------------------------------\n\n\n";
    std::cout << "[TEST A] Number of empty cells: " << expandArea(text_map, 10) << '\n';
    std::cout << "[TEST B] Total number of iterations: " << expandArea(text_map, -1) << '\n';
    
    return EXIT_SUCCESS;
}

