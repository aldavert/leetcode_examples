# [Day 23: Unstable Diffusion](https://adventofcode.com/2022/day/23)

#### Problem A
You are given a set of points and you want to make sure that they are properly spaced so that the **[Chebyshev](https://en.wikipedia.org/wiki/Chebyshev_distance)** distance between any point and its neighbors is always greater than 1.

Given a point and its 1-tick neighborhood:
```
012
3P4
567
```
Consider moving any point that has another point in this neighborhood. The possible motion is selected evaluating the free-space around the point in the following order:
- **Up** if `0`, `1` and `2` are empty.
- **Down** if `5`, `6` and `7` are empty.
- **Left** if `0`, `3` and `5` are empty.
- **Right** if `2`, `4` and `7` are empty.
The initial direction considered is moved at the end of the list after each iteration. So, in the first iteration, you first try **up,** then **down,** then **left** and finally **right**, but in the second iteration you try **down**, **left**, **right** and **up**.

Then, move all points that will not collide with another point. You keep repeating the process until the algorithm cannot move any point, i.e. they are all properly spaced.

**At iteration 10, what is the free space in the bounding box that encloses all points?**

#### Problem B

**How many iterations it takes for the algorithm to converge?**

