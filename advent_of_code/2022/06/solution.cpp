#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <queue>

int main(int argc, char * * argv)
{
    std::ifstream file((argc == 1)?"data_test00.txt":argv[1]);
    std::string line;
    auto update =
        [](int c, int histogram[26], std::queue<int> &buffer, size_t length) -> bool
    {
        ++histogram[c];
        buffer.push(c);
        if (buffer.size() == length)
        {
            size_t number_of_ones = 0;
            for (size_t i = 0; i < 26; ++i)
                number_of_ones += histogram[i] == 1;
            if (number_of_ones == length)
                return true;
            --histogram[buffer.front()];
            buffer.pop();
        }
        return false;
    };
    
    while (std::getline(file, line))
    {
        size_t message_short = std::string::npos, message_long = std::string::npos;
        std::queue<int> buffer_short, buffer_long;
        int histogram_short[26] = {}, histogram_long[26] = {};
        
        std::istringstream input(line);
        std::string message;
        input >> message;
        
        for (size_t index = 0, n = message.size(); index < n; ++index)
            if (update(message[index] - 'a', histogram_short, buffer_short,  4))
            {
                message_short = index + 1;
                break;
            }
        for (size_t index = 0, n = message.size(); index < n; ++index)
            if (update(message[index] - 'a', histogram_long , buffer_long , 14))
            {
                message_long  = index + 1;
                break;
            }
        if (message_short != std::string::npos)
            std::cout << "Short message at " << message_short << '\n';
        if (message_long != std::string::npos)
            std::cout << "Long message at " << message_long << '\n';
    }
    return EXIT_SUCCESS;
}

