# [Day 6: Tuning Trouble](https://adventofcode.com/2022/day/6)

#### Problem A

Information is transmitted as a stream of characters. The beginning of an information packed is marked by any group of four characters that are all different. **How many characters need to be processed before detecting the beginning of a new information packet?**

#### Problem B

Same as before, but now the packed starting sequence consists of `14` distinct characters instead of `4`. So, **how many characters need to be processed before detecting the beginning of a new information packet?**

