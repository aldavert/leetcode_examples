#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    for (const auto &v : vec)
        out << v;
    out << '\n';
    return out;
}

#if 0
size_t visible(std::vector<std::vector<int> > forest)
{
    const int nrows = static_cast<int>(forest.size());
    const int ncols = static_cast<int>(forest[0].size());
    std::set<std::tuple<int, int> > visible;
    int maximum;
    auto check = [&](int row, int col) -> void
    {
        if (forest[row][col] > maximum)
        {
            visible.insert({row, col});
            maximum = forest[row][col];
        }
    };
    for (int row = 1; row < nrows - 1; ++row)
    {
        maximum = forest[row][0];
        for (int col = 1; col < ncols - 1; ++col)
            check(row, col);
        maximum = forest[row][ncols - 1];
        for (int col = ncols - 2; col > 0; --col)
            check(row, col);
    }
    for (int col = 1; col < ncols - 1; ++col)
    {
        maximum = forest[0][col];
        for (int row = 1; row < nrows - 1; ++row)
            check(row, col);
        maximum = forest[nrows - 1][col];
        for (int row = nrows - 2; row > 0; --row)
            check(row, col);
    }
    return visible.size();
}
#elif 1
size_t visible(std::vector<std::vector<int> > forest)
{
    const int nrows = static_cast<int>(forest.size());
    const int ncols = static_cast<int>(forest[0].size());
    auto flush = [](std::stack<std::pair<int, int> > &stack, int value)
    {
        while (!stack.empty() && (stack.top().first <= value))
            stack.pop();
    };
    std::vector<std::stack<std::pair<int, int> > > monotonic_stack_vertical(ncols);
    std::vector<int> maximum_vertical = forest[0];
    int visible = 0;
    for (int row = 1; row < nrows - 1; ++row)
    {
        std::stack<std::pair<int, int> > monotonic_stack_horizontal;
        int maximum_horizontal = forest[row][0];
        for (int col = 1; col < ncols - 1; ++col)
        {
            bool increase = false;
            flush(monotonic_stack_horizontal, forest[row][col]);
            flush(monotonic_stack_vertical[col], forest[row][col]);
            if (forest[row][col] > maximum_horizontal)
            {
                maximum_horizontal = forest[row][col];
                increase = true;
            }
            if (forest[row][col] > maximum_vertical[col])
            {
                maximum_vertical[col] = forest[row][col];
                increase = true;
            }
            if (increase)
                ++visible;
            else
            {
                monotonic_stack_horizontal.push({forest[row][col], col});
                monotonic_stack_vertical[col].push({forest[row][col], row});
            }
        }
        flush(monotonic_stack_horizontal, forest[row][ncols - 1]);
        visible += static_cast<int>(monotonic_stack_horizontal.size());
        while (!monotonic_stack_horizontal.empty())
        {
            int col = monotonic_stack_horizontal.top().second;
            if (!monotonic_stack_vertical[col].empty()
             && (monotonic_stack_vertical[col].top().second == row))
                monotonic_stack_vertical[col].pop();
            monotonic_stack_horizontal.pop();
        }
    }
    for (int col = 1; col < ncols - 1; ++col)
    {
        flush(monotonic_stack_vertical[col], forest[nrows - 1][col]);
        visible += static_cast<int>(monotonic_stack_vertical[col].size());
    }
    return visible;
}
#else
size_t visible(std::vector<std::vector<int> > forest)
{
    const int nrows = static_cast<int>(forest.size());
    const int ncols = static_cast<int>(forest[0].size());
    // Forward.
    std::vector<int> max_col_height = forest[0];
    std::vector<std::vector<bool> > available(nrows, std::vector<bool>(ncols, true));
    int visible_tree = 0;
    for (int row = 1; row < nrows - 1; ++row)
    {
        int max_row_height = forest[row][0];
        for (int col = 1; col < ncols - 1; ++col)
        {
            if (forest[row][col] > max_row_height)
            {
                visible_tree += available[row][col];
                available[row][col] = false;
                max_row_height = forest[row][col];
                max_col_height[col] = std::max(max_col_height[col], forest[row][col]);
            }
            else if (forest[row][col] > max_col_height[col])
            {
                visible_tree += available[row][col];
                available[row][col] = false;
                max_col_height[col] = forest[row][col];
            }
        }
    }
    // Backward
    max_col_height = forest[nrows - 1];
    for (int row = nrows - 2; row > 0; --row)
    {
        int max_row_height = forest[row][ncols - 1];
        for (int col = ncols - 2; col > 0; --col)
        {
            if (forest[row][col] > max_row_height)
            {
                visible_tree += available[row][col];
                available[row][col] = false;
                max_row_height = forest[row][col];
                max_col_height[col] = std::max(max_col_height[col], forest[row][col]);
            }
            else if (forest[row][col] > max_col_height[col])
            {
                visible_tree += available[row][col];
                available[row][col] = false;
                max_col_height[col] = forest[row][col];
            }
        }
    }
    
    return visible_tree;
}
#endif

int scenicScore(const std::vector<std::vector<int> > &forest, int row, int col)
{
    const int nrows = static_cast<int>(forest.size());
    const int ncols = static_cast<int>(forest[0].size());
    int score = 1, pos;
    for (pos = row - 1; (pos > 0   ) && (forest[row][col] > forest[pos][col]); --pos);
    score *= std::max(1, row - pos);
    for (pos = col - 1; (pos > 0   ) && (forest[row][col] > forest[row][pos]); --pos);
    score *= std::max(1, col - pos);
    for (pos = col + 1; (pos < ncols - 1) && (forest[row][col] > forest[row][pos]); ++pos);
    score *= std::max(1, pos - col);
    for (pos = row + 1; (pos < nrows - 1) && (forest[row][col] > forest[pos][col]); ++pos);
    score *= std::max(1, pos - row);
    return score;
}

int maxScenicScore(const std::vector<std::vector<int> > &forest)
{
    const int nrows = static_cast<int>(forest.size());
    const int ncols = static_cast<int>(forest[0].size());
    int result = 0;
    for (int row = 1; row < nrows - 1; ++row)
        for (int col = 1; col < ncols - 1; ++col)
            result = std::max(result, scenicScore(forest, row, col));
    return result;
}

int main(int argc, char * * argv)
{
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line;
    std::vector<std::vector<int> > forest;
    while (std::getline(file, line))
    {
        std::vector<int> current;
        for (char height : line)
            current.push_back(height - '0');
        forest.emplace_back(current);
    }
    size_t result = 2 * forest.size() + 2 * forest[0].size() - 4 + visible(forest);
    std::cout << "Visible trees: " << result << '\n';
    std::cout << "Maximum scenic score: " << maxScenicScore(forest) << '\n';
    
    return EXIT_SUCCESS;
}

