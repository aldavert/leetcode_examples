# [Day 8: Treetop Tree House](https://adventofcode.com/2022/day/8)

#### Problem A

Given a grid with tree's elevations that range from `0` (*shortest tree*) to `9` (*tallest tree*), **compute how many trees are visible from outside the grid?**

A tree is only visible when its height is **higher** than all the trees between it and one of the edges of the grid. Only trees in the same row or column are considered, so only look up, down, left or right from any given tree.

Given this patch of forest:

```
30373
25512
65332
33549
35390
```

There are `21` visible trees from outside: `16` of them correspond to the edge trees (which are always visible) and the remaining `5` are inner trees that are visible from outside:

1. Tree at `(1, 1)`, visible while looking `down` and `right`.
2. Tree at `(1, 2)`, visible while looking `down` and `left`.
3. Tree at `(2, 1)`, visible while looking `left`.
4. Tree at `(2, 3)`, visible while looking `left`.
5. Tree at `(3, 2)`, visible while looking `right` and `up`.

![forest_visibility.svg](images/forest_visibility.svg)

#### Problem B

The scenic score of a tree gives a rough measure of the area of the forest that can be spotted from the tree. It is computed as the product of the distances from the tree to the tree of the same height of tallest in each of the four cardinal directions.

1. The tree at `(1, 3)` has a scenic score of `1 * 1 * 2 * 2 = 4`:

![forest_visibility.svg](images/scenic_score01.svg)

2. The tree at `(3, 2)` has a scenic score of `2 * 2 * 1 * 2 = 8`:

![forest_visibility.svg](images/scenic_score02.svg)


**Find the tree which has the highest scenic score.**
