#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>
#include <fmt/core.h>

#define GENERATE_IMAGES 0
constexpr int LIMIT = std::numeric_limits<int>::max();

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << fmt::format("{:>2}", v);
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::vector<int> > &vec)
{
    out << "{";
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ",\n ";
        next = true;
        out << v;
    }
    out << "}";
    return out;
}

void save(const char * filename, const std::vector<std::vector<unsigned char> > image)
{
    const int height = static_cast<int>(image.size());
    if (height == 0) return;
    const int width  = static_cast<int>(image[0].size()) / 3;
    if (width == 0) return;
    std::ofstream file(filename, std::ios::binary);
    
    unsigned char fileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
    unsigned char infoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
    unsigned char pad[3] = {0, 0, 0};
    const int filesize = 54 + 3 * height * width;
    fileheader[ 2] = static_cast<unsigned char>(filesize      );
    fileheader[ 3] = static_cast<unsigned char>(filesize >>  8);
    fileheader[ 4] = static_cast<unsigned char>(filesize >> 16);
    fileheader[ 5] = static_cast<unsigned char>(filesize >> 24);
    infoheader[ 4] = static_cast<unsigned char>(   width      );
    infoheader[ 5] = static_cast<unsigned char>(   width >>  8);
    infoheader[ 6] = static_cast<unsigned char>(   width >> 16);
    infoheader[ 7] = static_cast<unsigned char>(   width >> 24);
    infoheader[ 8] = static_cast<unsigned char>(  height      );
    infoheader[ 9] = static_cast<unsigned char>(  height >>  8);
    infoheader[10] = static_cast<unsigned char>(  height >> 16);
    infoheader[11] = static_cast<unsigned char>(  height >> 24);
    file.write(reinterpret_cast<char *>(fileheader), 14);
    file.write(reinterpret_cast<char *>(infoheader), 40);
    for (int row = height - 1; row >= 0; --row)
    {
        file.write(reinterpret_cast<const char *>(image[row].data()), 3 * width);
        file.write(reinterpret_cast<char *>(pad), (4 - (3 * width) % 4) % 4);
    }
    file.close();
}

struct Color { unsigned char red = 0; unsigned char green = 0; unsigned char blue = 0; };
struct Position
{
    int row = 0;
    int col = 0;
    bool operator==(const Position &other) const { return (row == other.row) && (col == other.col); }
    bool operator!=(const Position &other) const { return (row != other.row) || (col != other.col); }
};

auto drawMap(const std::vector<std::vector<int> > &map,
             Position start,
             Position destination) -> std::vector<std::vector<unsigned char> >
{
    const int nrows = static_cast<int>(map.size());
    if (nrows == 0) return {};
    const int ncols = static_cast<int>(map[0].size());
    if (ncols == 0) return {};
    int value_min = LIMIT;
    int value_max = std::numeric_limits<int>::lowest();
    for (int row = 0; row < nrows; ++row)
        for (int col = 0; col < ncols; ++col)
            if (map[row][col] != LIMIT)
                value_min = std::min(value_min, map[row][col]),
                value_max = std::max(value_max, map[row][col]);
    
    const int scale = 14;
    const int width  = ncols * scale;
    const int height = nrows * scale;
    std::vector<std::vector<unsigned char> > image(height, std::vector<unsigned char>(width * 3, 0));
    auto drawSquareGrid = [&](Position p, Color c)
    {
        for (int y = 1; y < scale - 1; ++y)
        {
            for (int x = 1; x < scale - 1; ++x)
            {
                image[scale * p.row + y][scale * 3 * p.col + 3 * x + 2] = c.red;
                image[scale * p.row + y][scale * 3 * p.col + 3 * x + 1] = c.green;
                image[scale * p.row + y][scale * 3 * p.col + 3 * x + 0] = c.blue;
            }
        }
    };
    int value_inc = std::max(1, value_max - value_min);
    for (int row = 0; row < nrows; ++row)
    {
        for (int col = 0; col < ncols; ++col)
        {
            if (map[row][col] != LIMIT)
            {
                int value = 200 * (map[row][col] - value_min) / value_inc + 55;
                unsigned char uc_value = static_cast<unsigned char>(value);
                drawSquareGrid({row, col}, { uc_value, uc_value, uc_value });
            }
            else drawSquareGrid({row, col}, { 0, 25, 150 });
        }
    }
    drawSquareGrid(start, { 255, 128, 0 }); 
    drawSquareGrid(destination, { 100, 200, 0 }); 
    return image;
}

auto drawPath(std::vector<std::vector<unsigned char> > image,
              [[maybe_unused]] const std::vector<std::vector<Position> > &previous,
              [[maybe_unused]] Position start,
              [[maybe_unused]] Position destination) -> std::vector<std::vector<unsigned char> >
{
    const int scale = 14;
    auto next = [&](Position p) -> Position
    {
        return previous[p.row][p.col];
    };
    auto drawSquareGrid = [&](Position p, Color c)
    {
        for (int y = 1; y < scale - 1; ++y)
        {
            for (int x = 1; x < scale - 1; ++x)
            {
                image[scale * p.row + y][scale * 3 * p.col + 3 * x + 2] = c.red;
                image[scale * p.row + y][scale * 3 * p.col + 3 * x + 1] = c.green;
                image[scale * p.row + y][scale * 3 * p.col + 3 * x + 0] = c.blue;
            }
        }
    };
    for (auto current = next(destination); current != start; current = next(current))
        drawSquareGrid(current, {255, 0, 0});
    return image;
}

struct Information
{
    Information(Position p, int s, Position dst, Position prev) :
        row(p.row),
        col(p.col),
        step(s),
        heuristic(s + std::abs(p.row - dst.row) + std::abs(p.col - dst.col)),
        previous(prev) {}
    int row = 0;
    int col = 0;
    int step = 0;
    int heuristic = 0;
    Position previous = { -1, -1 };
    bool operator<(const Information &other) const
    {
        return heuristic > other.heuristic;
    }
};

bool wrongParameters(const std::vector<std::vector<int> > &map,
                     const Position &start,
                     const Position &destination)
{
    const int nrows = static_cast<int>(map.size());
    const int ncols = static_cast<int>(map[0].size());
    return (map.size() == 0) || (map[0].size() == 0)                            // Zero size map
        || (start.row < 0) || (start.row >= nrows)                              // Start point out of bounds.
        || (start.col < 0) || (start.col >= ncols)
        || (destination.row < 0) || (destination.row >= nrows)                  // Destination point out of bounds.
        || (destination.col < 0) || (destination.col >= ncols);
}

int minimumStepsAmplitude(const std::vector<std::vector<int> > &map,
                          Position start,
                          Position destination)
{
    if (wrongParameters(map, start, destination)) return -1;
    const int nrows = static_cast<int>(map.size());
    const int ncols = static_cast<int>(map[0].size());
    std::vector<std::vector<int> > distance(nrows, std::vector<int>(ncols, LIMIT));
    std::vector<std::vector<Position> > source(nrows, std::vector<Position>(ncols));
    std::queue<Information> search;
    
    auto update = [&](Information pos)
    {
        // Position outside the map -> return.
        if ((pos.row < 0) || (pos.row >= nrows)
        ||  (pos.col < 0) || (pos.col >= ncols)) return;
        // Too step -> return.
        if (map[pos.row][pos.col] - map[pos.previous.row][pos.previous.col] > 1)
            return;
        // No improvement -> return.
        if (distance[pos.row][pos.col] < pos.step)
            return;
        search.push(pos);
    };
    
    
    search.push(Information(start, 0, destination, {-1, -1}));
    size_t number_of_ticks = 0, number_of_positions_considered = 0, max_considered = 0;
    int maximum_steps_destination = LIMIT;
    while (!search.empty())
    {
        max_considered = std::max(max_considered, search.size());
        auto [row, col, steps, heuristic, previous] = search.front();
        search.pop();
        ++number_of_positions_considered;
        if ((steps < distance[row][col]) && (steps < maximum_steps_destination))
        {
            ++number_of_ticks;
            distance[row][col] = steps;
            source[row][col] = previous;
            if ((row == destination.row) && (col == destination.col))
            {
                maximum_steps_destination = std::max(maximum_steps_destination, steps);
                break;
            }
            update({{row - 1, col}, steps + 1, destination, {row, col}});
            update({{row, col - 1}, steps + 1, destination, {row, col}});
            update({{row + 1, col}, steps + 1, destination, {row, col}});
            update({{row, col + 1}, steps + 1, destination, {row, col}});
        }
    }
    fmt::print("Number of positions considered: {} (maximum queue size: {})\n", number_of_positions_considered, max_considered);
    fmt::print("Number of positions evaluated: {}\n", number_of_ticks);
#if GENERATE_IMAGES
    save("map_path_search.bmp", drawPath(drawMap(map, start, destination), source, start, destination));
    save("distance_search.bmp", drawPath(drawMap(distance, start, destination), source, start, destination));
#endif
    return distance[destination.row][destination.col];
}

int minimumStepsAStar(const std::vector<std::vector<int> > &map,
                      Position start,
                      Position destination)
{
    if (wrongParameters(map, start, destination)) return -1;
    const int nrows = static_cast<int>(map.size());
    const int ncols = static_cast<int>(map[0].size());
    std::vector<std::vector<int> > distance(nrows, std::vector<int>(ncols, LIMIT));
    std::vector<std::vector<Position> > source(nrows, std::vector<Position>(ncols));
    std::priority_queue<Information> search;
    int maximum_steps_destination = LIMIT;
    
    auto update = [&](Information pos)
    {
        // Position outside the map -> return.
        if ((pos.row < 0) || (pos.row >= nrows)
        ||  (pos.col < 0) || (pos.col >= ncols)) return;
        // Too step -> return.
        if (map[pos.row][pos.col] - map[pos.previous.row][pos.previous.col] > 1)
            return;
        // No improvement -> return.
        if (distance[pos.row][pos.col] < pos.step)
            return;
        // Impossible because of heuristics -> return.
        if (pos.heuristic > maximum_steps_destination)
            return;
        search.push(pos);
    };
    
    search.push(Information(start, 0, destination, {-1, -1}));
    size_t number_of_ticks = 0, number_of_positions_considered = 0, max_considered = 0;
    while (!search.empty())
    {
        max_considered = std::max(max_considered, search.size());
        auto [row, col, steps, heuristic, previous] = search.top();
        search.pop();
        ++number_of_positions_considered;
        if ((steps < distance[row][col]) && (heuristic <= maximum_steps_destination))
        {
            ++number_of_ticks;
            distance[row][col] = steps;
            source[row][col] = previous;
            if ((row == destination.row) && (col == destination.col))
                maximum_steps_destination = std::min(maximum_steps_destination, steps);
            //fmt::print("map_path_astart{:04d}.bmp: {} {} {}\n", number_of_ticks, maximum_steps_destination, steps, heuristic);
            //save(fmt::format("distance_astart{:04d}.bmp", number_of_ticks).c_str(), drawMap(distance, start, destination));
            update(Information({row - 1, col}, steps + 1, destination, {row, col}));
            update(Information({row, col - 1}, steps + 1, destination, {row, col}));
            update(Information({row + 1, col}, steps + 1, destination, {row, col}));
            update(Information({row, col + 1}, steps + 1, destination, {row, col}));
        }
    }
    fmt::print("Number of positions considered: {} (maximum queue size: {})\n", number_of_positions_considered, max_considered);
    fmt::print("Number of positions evaluated: {}\n", number_of_ticks);
#if GENERATE_IMAGES
    save("map_path_astart.bmp", drawPath(drawMap(map, start, destination), source, start, destination));
    save("distance_astart.bmp", drawPath(drawMap(distance, start, destination), source, start, destination));
#endif
    return distance[destination.row][destination.col];
}

int minimumStepsBidirectionalSearch(const std::vector<std::vector<int> > &map,
                                    Position start,
                                    Position destination)
{
    if (wrongParameters(map, start, destination)) return -1;
    const int nrows = static_cast<int>(map.size());
    const int ncols = static_cast<int>(map[0].size());
    std::vector<std::vector<int> > distance[2] = {
        std::vector<std::vector<int> >(nrows, std::vector<int>(ncols, LIMIT)),
        std::vector<std::vector<int> >(nrows, std::vector<int>(ncols, LIMIT)) };
    std::vector<std::vector<Position> > direction[2] = {
        std::vector<std::vector<Position> >(nrows, std::vector<Position>(ncols)),
        std::vector<std::vector<Position> >(nrows, std::vector<Position>(ncols)) };
    std::priority_queue<Information> search[2];
    int maximum_steps_heuristic = LIMIT;
    
    auto update = [&](const Information pos, bool backward)
    {
        // Position outside the map -> return.
        if ((pos.row < 0) || (pos.row >= nrows)
        ||  (pos.col < 0) || (pos.col >= ncols)) return;
        // Too step -> return.
        if (!backward && (map[pos.row][pos.col] - map[pos.previous.row][pos.previous.col] > 1))
            return;
        else if (backward && (map[pos.row][pos.col] - map[pos.previous.row][pos.previous.col] < -1))
            return;
        // No improvement -> return.
        if (distance[backward][pos.row][pos.col] < pos.step)
            return;
        // Impossible because of heuristics -> return.
        if (pos.heuristic > maximum_steps_heuristic)
            return;
        if (distance[!backward][pos.row][pos.col] != LIMIT)
            return;
        search[backward].push(pos);
    };
    
    search[0].push(Information(start, 0, destination, {-1, -1}));
    search[1].push(Information(destination, 0, start, {-1, -1}));
    size_t number_of_ticks = 0, number_of_positions_considered = 0, max_considered = 0;
    for (bool backward = false; !(search[0].empty() || search[1].empty()); backward = !backward)
    {
        max_considered = std::max(max_considered, search[backward].size());
        ++number_of_positions_considered;
        auto information = search[backward].top();
        auto [row, col, steps, heuristic, previous] = information;
        search[backward].pop();
        if ((steps < distance[backward][row][col]) && (heuristic <= maximum_steps_heuristic))
        {
            distance[backward][row][col] = steps;
            direction[backward][row][col] = previous;
            if (( backward && (row == start.row) && (col == start.col))
            ||  (!backward && (row == destination.row) && (col == destination.col)))
                maximum_steps_heuristic = std::min(maximum_steps_heuristic, steps);
            else if ((distance[0][row][col] < LIMIT) && (distance[1][row][col] < LIMIT))
                maximum_steps_heuristic = std::min(maximum_steps_heuristic,
                        distance[0][row][col] + distance[1][row][col]);
            update(Information({row - 1, col}, steps + 1, backward?start:destination, {row, col}), backward);
            update(Information({row, col - 1}, steps + 1, backward?start:destination, {row, col}), backward);
            update(Information({row + 1, col}, steps + 1, backward?start:destination, {row, col}), backward);
            update(Information({row, col + 1}, steps + 1, backward?start:destination, {row, col}), backward);
            ++number_of_ticks;
            // DEBUG // fmt::print("map_path_astart{:04d}.bmp: {} {} {}\n", number_of_ticks, maximum_steps_heuristic, steps, heuristic);
            // DEBUG // auto image_forward = drawMap(distance[1], start, destination),
            // DEBUG //      image_backward = drawMap(distance[0], destination, start);
            // DEBUG // std::vector<std::vector<unsigned char> > image_merged(image_forward.size() + image_backward.size() + 10, std::vector<unsigned char>(image_forward[0].size(), 0));
            // DEBUG // for (size_t y = 0; y < image_forward.size(); ++y)
            // DEBUG //     image_merged[y] = image_forward[y],
            // DEBUG //     image_merged[y + image_forward.size() + 10] = image_backward[y];
            // DEBUG // save(fmt::format("distance_bidirectional{:04d}.bmp", number_of_ticks).c_str(), image_merged);
        }
    }
    fmt::print("Number of positions considered: {} (maximum queue size: {})\n", number_of_positions_considered, max_considered);
    fmt::print("Number of positions evaluated: {}\n", number_of_ticks);
#if GENERATE_IMAGES
    save("map_path_astart.bmp", drawPath(drawMap(map, start, destination), direction[0], start, destination));
    save("distance_astart.bmp", drawPath(drawMap(distance[0], start, destination), direction[0], start, destination));
#endif
    return maximum_steps_heuristic;
}

int main(int argc, char * * argv)
{
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line;
    std::vector<std::vector<int> > map;
    Position start, destination;
    
    for (int row = 0; std::getline(file, line); ++row)
    {
        std::vector<int> current;
        const int n = static_cast<int>(line.size());
        for (int col = 0; col < n; ++col)
        {
            if (line[col] == 'S')
            {
                start = { row, col };
                line[col] = 'a';
            }
            else if (line[col] == 'E')
            {
                destination = { row, col };
                line[col] = 'z';
            }
            current.push_back(line[col] - 'a');
        }
        map.emplace_back(current);
    }
    std::cout << "Map size: " << map.size() << 'x' << map[0].size() << " (";
    std::cout << map.size() * map[0].size() << " cells).\n";
    std::cout << "[SEARCHING] Amplitude search using a queue...\n";
    int minimum_number_of_steps = minimumStepsAmplitude(map, start, destination);
    std::cout << "Minimum number of steps: " << minimum_number_of_steps << '\n';
    
    std::cout << "[SEARCHING] A* algorithm (priority queue with Manhattan distance as heuristic)...\n";
    minimum_number_of_steps = minimumStepsAStar(map, start, destination);
    std::cout << "Minimum number of steps: " << minimum_number_of_steps << '\n';
    
    std::cout << "[SEARCHING] Bidirectional search algorithm from both ends...\n";
    minimum_number_of_steps = minimumStepsBidirectionalSearch(map, start, destination);
    std::cout << "Minimum number of steps: " << minimum_number_of_steps << '\n';
    
    return EXIT_SUCCESS;
}

