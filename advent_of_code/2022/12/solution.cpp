#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>
#include <fmt/core.h>

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << fmt::format("{:>2}", v);
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::vector<int> > &vec)
{
    out << "{";
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ",\n ";
        next = true;
        out << v;
    }
    out << "}";
    return out;
}

int minimumSteps(std::vector<std::vector<int> > map,
                 std::pair<int, int> start,
                 std::pair<int, int> destination)
{
    struct Position
    {
        int row = 0;
        int col = 0;
        int step = 0;
    };
    const int nrows = static_cast<int>(map.size());
    if (nrows == 0) return -1;
    const int ncols = static_cast<int>(map[0].size());
    if ((start.first < 0) || (start.first >= nrows))
        return -1;
    if ((start.second < 0) || (start.second >= ncols))
        return -1;
    if ((destination.first < 0) || (destination.first >= nrows))
        return -1;
    if ((destination.second < 0) || (destination.second >= ncols))
        return -1;
    std::vector<std::vector<int> > distance(nrows, std::vector<int>(ncols, std::numeric_limits<int>::max()));
    std::queue<Position> search;
    search.push({ start.first, start.second, 0 });
    while (!search.empty())
    {
        auto [row, col, steps] = search.front();
        search.pop();
        if (steps < distance[row][col])
        {
            distance[row][col] = steps;
            if ((row > 0) && (map[row - 1][col] - map[row][col] <= 1))
                search.push({row - 1, col, steps + 1});
            if ((col > 0) && (map[row][col - 1] - map[row][col] <= 1))
                search.push({row, col - 1, steps + 1});
            if ((row + 1 < nrows) && (map[row + 1][col] - map[row][col] <= 1))
                search.push({row + 1, col, steps + 1});
            if ((col + 1 < ncols) && (map[row][col + 1] - map[row][col] <= 1))
                search.push({row, col + 1, steps + 1});
        }
    }
    return distance[destination.first][destination.second];
}

int main(int argc, char * * argv)
{
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line;
    std::vector<std::vector<int> > map;
    std::pair<int, int> start, destination;
    
    std::vector<std::pair<int, int> > all_starting_points;
    for (int row = 0; std::getline(file, line); ++row)
    {
        std::vector<int> current;
        const int n = static_cast<int>(line.size());
        for (int col = 0; col < n; ++col)
        {
            if (line[col] == 'S')
            {
                start = { row, col };
                line[col] = 'a';
            }
            else if (line[col] == 'E')
            {
                destination = { row, col };
                line[col] = 'z';
            }
            if (line[col] == 'a')
                all_starting_points.push_back({row, col});
            current.push_back(line[col] - 'a');
        }
        map.emplace_back(current);
    }
    std::cout << "Minimum number of steps: " << minimumSteps(map, start, destination) << '\n';
    int minimum = std::numeric_limits<int>::max();
    for (auto s : all_starting_points)
        minimum = std::min(minimum, minimumSteps(map, s, destination));
    std::cout << "Minimum number of steps from all lowest points: " << minimum << '\n';
    
    return EXIT_SUCCESS;
}

