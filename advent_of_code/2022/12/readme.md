# [Day 12: Hill Climbing Algorithm](https://adventofcode.com/2022/day/12)

#### Problem A

You are given an elevation map and the following constrains:

1. The heights are marked as lowercase letters that go from `'a'` **(lowest)** to `'z'` **(highest)**.
2. You start at `'S'` (at elevation `'a'`) and you want to reach `'E'` (at elevation `'z'`).
3. You can move *UP*, *DOWN*, *LEFT* or *RIGHT*.
4. From one cell you can only jump to another cell that is at **a lower elevation** or **that at most is one step higher than the current cell**.

**Find the number of steps of the shortest valid path to go from** `S` **start to** `E` **exit**.

Example of the map where the algorithm has to be applied to:
![Image with the test map and example of optimal path](images/solution.png)
The *orange square* corresponds to the starting position, the *green square* is the exit position and, the red path is *one of the paths* that has the smallest number of steps.

#### Problem B

Instead of starting at a predefined location, you start at any lowest location, i.e. any location with elevation `'a'`.

**Find the number of steps of the shortest path from ALL valid locations to the exit.**.


