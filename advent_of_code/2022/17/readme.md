# [Day 17: Pyroclastic Flow](https://adventofcode.com/2022/day/17)

#### Problem A

You have to implement a *tetris* like problem where you have the following patterns:
```
####

.#.
###
.#.

..#
..#
###

#
#
#
#

##
##
```

where `#` is blocked and `.` is free space. The patterns appear always in the same order and cycle.

The board is 7 positions wide and the pieces appear so that the **left edge is two units away of the left wall** and the **bottom edge is three units above the highest rock in the room**.

**Compute how tall will be the tower when 2022 rocks have been put down.**

#### Problem B

Same problem as before but with putting down more rocks. **How tall will be the tower if 1'000'000'000'000 have been put down?**

