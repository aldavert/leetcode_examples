#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>
#include <fmt/core.h>

std::ostream& operator<<(std::ostream &out, const std::vector<long> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    for (const auto &v : vec)
        out << v << '\n';;
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<char> &vec)
{
    for (const auto &v : vec) out << v;
    return out;
}

// -----------------------------------------------------------------------------

class Shape
{
    int m_width;
    int m_height;
    std::vector<bool> m_pattern[4];
public:
    Shape(std::string input, int width, int height) :
        m_width(width),
        m_height(height)
    {
        for (int i = 0; i < height; ++i)
            m_pattern[i].resize(width, false);
        for (int h = 0, i = 0; h < height; ++h)
            for (int w = 0; w < width; ++w, ++i)
                m_pattern[h][w] = input[i] == '#';
    }
    friend std::ostream& operator<<(std::ostream &out, const Shape &s)
    {
        for (int h = 0; h < s.m_height; ++h)
        {
            for (int w = 0; w < s.m_width; ++w)
                out << ((s.m_pattern[h][w])?'#':' ');
            out << '\n';
        }
        return out;
    }
    bool occupied(int h, int w) const { return m_pattern[h][w]; }
    int width(void) const { return m_width; }
    int height(void) const { return m_height; }
};

class Tetris
{
    Shape m_shapes[5] = {Shape("####", 4, 1),
                         Shape(" # "
                               "###"
                               " # ", 3, 3),
                         Shape("  #"
                               "  #"
                               "###", 3, 3),
                         Shape("#"
                               "#"
                               "#"
                               "#", 1, 4),
                         Shape("##"
                               "##", 2, 2)};
    int m_width;
    int m_height;
    int m_relative_floor;
    long m_height_removed;
    std::vector<std::vector<bool> > m_occupacy;
    std::vector<int> m_local_column_height;
    bool collision(int x, int y, int shape_idx)
    {
        for (int h = 0; h < m_shapes[shape_idx].height(); ++h)
            for (int w = 0; w < m_shapes[shape_idx].width(); ++w)
                if (m_occupacy[y - h][w + x] && m_shapes[shape_idx].occupied(h, w))
                    return true;
        return false;
    }
public:
    Tetris(int width, int maximum_height) :
        m_width(width),
        m_height(0),
        m_relative_floor(0),
        m_height_removed(0),
        m_occupacy(maximum_height, std::vector<bool>(width, false)),
        m_local_column_height(width, 0) {}
    void clean(void)
    {
        m_relative_floor = m_height = 0;
        m_height_removed = 0;
        for (auto &row : m_occupacy)
            row.assign(m_width, false);
        for (int i = 0; i < m_width; ++i)
            m_local_column_height[i] = 0;
    }
    long height(void) const { return static_cast<long>(m_height) + m_height_removed; }
    void simulate(long number_of_stones, [[maybe_unused]] std::string movements)
    {
        for (long rock_n = 0, motion_idx = 0; rock_n < number_of_stones; ++rock_n, ++motion_idx)
        {
            if ((rock_n > 0) && (rock_n % 1'000'000 == 0))
                std::cout << "Processing rock: " << rock_n << "...\n";
            int shape_idx = static_cast<int>(rock_n % 5);
            int y = m_height + 3 + m_shapes[shape_idx].height() - 1;
            int x = 2;
            for (; true; ++motion_idx)
            {
                if (movements[motion_idx % movements.size()] == '>') // Right
                {
                    if ((x + m_shapes[shape_idx].width() < m_width)
                    &&  (!collision(x + 1, y, shape_idx)))
                        ++x;
                }
                else                                                
                {
                    if ((x > 0)
                    &&  (!collision(x - 1, y, shape_idx)))
                        --x;
                }
                if ((y - m_shapes[shape_idx].height() < 0)
                ||  (collision(x, y - 1, shape_idx)))
                    break;
                --y;
            }
            for (int h = 0; h < m_shapes[shape_idx].height(); ++h, --y)
                for (int px = x, w = 0; w < m_shapes[shape_idx].width(); ++w, ++px)
                    if (m_shapes[shape_idx].occupied(h, w))
                        m_occupacy[y][px] = true,
                        m_local_column_height[px] = std::max(m_local_column_height[px], y + 1);
            m_height = std::max(m_height, y + m_shapes[shape_idx].height() + 1);
            if (int lowest = *std::min_element(m_local_column_height.begin(), m_local_column_height.end());
                lowest > m_relative_floor)
            {
                for (int h = 0; h < m_height; ++h)
                {
                    m_occupacy[h] = m_occupacy[lowest + h];
                }
                m_height -= lowest;
                m_height_removed += lowest;
                for (int w = 0; w < m_width; ++w)
                    m_local_column_height[w] -= lowest;
            }
        }
    }
    void draw(void) const
    {
        for (int h = m_height - 1; h >= 0; --h)
        {
            fmt::print("{:4d} ", h);
            std::cout << '+';
            for (int w = 0; w < m_width; ++w)
                std::cout << (m_occupacy[h][w]?'#':'.');
            std::cout << "+\n";
        }
        std::cout << "     +";
        for (int w = 0; w < m_width; ++w) std::cout << '-';
        std::cout << "+\n";
        for (int i = 0; i < m_width; ++i)
            std::cout << m_local_column_height[i] << " ";
        std::cout << '\n';
    }
};


// -----------------------------------------------------------------------------

int main(int argc, char * * argv)
{
    const int width = 7;
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string jet_pattern;
    for (std::string line; std::getline(file, line); )
        jet_pattern += line;
    Tetris board(width, 100'000);
    board.simulate(2'022, jet_pattern);
    std::cout << "[TEST A] Expected output for test example is 3'068.\n";
    std::cout << "[TEST A] Tower height: " << board.height() << '\n';
    std::cout << "This solution is too slow to run TEST B\n";
    //board.clean();
    //board.simulate(1'000'000'000'000, jet_pattern);
    //std::cout << "[TEST B] Expected output for test example is 1'514'285'714'288.\n";
    //std::cout << "[TEST B] Tower height: " << board.height() << '\n';
    return EXIT_SUCCESS;
}

