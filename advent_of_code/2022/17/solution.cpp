#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>
#include <numeric>
#include <fmt/core.h>

std::ostream& operator<<(std::ostream &out, const std::vector<long> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    for (const auto &v : vec)
        out << v << '\n';;
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<char> &vec)
{
    for (const auto &v : vec) out << v;
    return out;
}

// -----------------------------------------------------------------------------

class Shape
{
    int m_width;
    int m_height;
    char m_pattern[4];
public:
    Shape(std::string input, int width, int height) :
        m_width(width),
        m_height(height)
    {
        for (int h = 0, i = 0; h < height; ++h)
        {
            m_pattern[h] = 0;
            for (int w = 0; w < width; ++w, ++i)
                m_pattern[h] = (m_pattern[h] << 1) | (input[i] == '#');
            m_pattern[h] <<= 7 - width - 2; // Put the pattern in the initial position.
        }
    }
    friend std::ostream& operator<<(std::ostream &out, const Shape &s)
    {
        for (int h = 0; h < s.m_height; ++h)
        {
            for (int w = 0; w < 7; ++w)
                out << ((s.m_pattern[h] & (1 << (7 - w - 1)))?'#':'.');
            out << '\n';
        }
        return out;
    }
    void left(int shift = 1)
    {
        for (int h = 0; h < m_height; ++h)
            m_pattern[h] <<= shift;
    }
    void right(int shift = 1)
    {
        for (int h = 0; h < m_height; ++h)
            m_pattern[h] >>= shift;
    }
    char row(int h) const { return m_pattern[h]; }
    bool collision(char row, int h) const { return row & m_pattern[h]; }
    bool occupied(int h, int w) const { return (m_pattern[h] & (1 << (7 - w - 1))) != 0; }
    int width(void) const { return m_width; }
    int height(void) const { return m_height; }
};

class Tetris
{
    Shape m_shapes[5] = {Shape("####", 4, 1),
                         Shape(" # "
                               "###"
                               " # ", 3, 3),
                         Shape("  #"
                               "  #"
                               "###", 3, 3),
                         Shape("#"
                               "#"
                               "#"
                               "#", 1, 4),
                         Shape("##"
                               "##", 2, 2)};
    const int m_width = 7;
    int m_height;
    int m_relative_floor;
    long m_height_removed;
    std::vector<char> m_occupacy;
    std::vector<int> m_local_column_height;
    bool collision(int y, const Shape &shape)
    {
        for (int h = 0; h < shape.height(); ++h)
            if (shape.collision(m_occupacy[y - h], h))
                return true;
        return false;
    }
public:
    Tetris(void) :
        m_height(0),
        m_relative_floor(0),
        m_height_removed(0),
        m_occupacy(10'000, 0),
        m_local_column_height(7, 0) {}
    void clean(void)
    {
        m_relative_floor = m_height = 0;
        m_height_removed = 0;
        for (auto &row : m_occupacy) row = 0;
        for (int i = 0; i < m_width; ++i)
            m_local_column_height[i] = 0;
    }
    long height(void) const { return static_cast<long>(m_height) + m_height_removed; }
    void simulate(long number_of_stones, std::string movements)
    {
        constexpr char characters[] = "abcdefghijklmnop";
        std::unordered_map<std::string, std::tuple<long, long> > memo;
        for (long rock_n = 0, motion_idx = 0; rock_n < number_of_stones; ++rock_n, ++motion_idx)
        {
            if ((rock_n > 0) && (rock_n % 10'000'000 == 0))
                std::cout << "Processing rock: " << rock_n << " total height: " << height() << "...\n";
            
            //motion_idx % movements.size()
            std::string descriptor = std::to_string(motion_idx % movements.size());
            descriptor += "::";
            descriptor += std::to_string(rock_n % 5);
            descriptor += "::";
            for (int h = 0; h < m_height; ++h)
            {
                descriptor += characters[m_occupacy[h] & 0xF];
                descriptor += characters[(m_occupacy[h] >> 4) & 0xF];
            }
            if (auto search = memo.find(descriptor); search != memo.end())
            {
                long drock = rock_n - std::get<0>(search->second);
                long dheight = height() - std::get<1>(search->second);
                ////fmt::print("ID={}; Rock number difference=({}-{})={}; Height difference=({}-{})={}\n",
                ////            search->first, rock_n, std::get<0>(search->second), drock,
                ////            height(), std::get<1>(search->second), dheight);
                long number_skipped = (number_of_stones - rock_n) / drock;
                if (number_skipped > 0)
                {
                    rock_n = drock * ((number_of_stones - rock_n) / drock) + rock_n;
                    m_height_removed += number_skipped * dheight;
                    memo.clear();
                }
            }
            else memo[descriptor] = {rock_n, height()};
            
            Shape shape = m_shapes[rock_n % 5];
            int y = m_height + shape.height() - 1;
            int x = 2;
            // Compute the free space.
            for (int i = 0; i < 3; ++i, ++motion_idx)
            {
                if (movements[motion_idx % movements.size()] == '>') // Right
                {
                    if (x + shape.width() < m_width)
                        if (!collision(y, shape)) ++x;
                }
                else                                                
                {
                    if (x > 0)
                        if (!collision(y, shape)) --x;
                }
            }
            
            if (int dx = 2 - x; dx < 0) shape.right(-dx);
            else if (dx > 0) shape.left(dx);
            for (; true; ++motion_idx)
            {
                if (movements[motion_idx % movements.size()] == '>') // Right
                {
                    if (x + shape.width() < m_width)
                    {
                        shape.right();
                        if (!collision(y, shape)) ++x;
                        else shape.left();
                    }
                }
                else                                                
                {
                    if (x > 0)
                    {
                        shape.left();
                        if (!collision(y, shape)) --x;
                        else shape.right();
                    }
                }
                if ((y - shape.height() < 0)
                ||  (collision(y - 1, shape)))
                    break;
                --y;
            }
            for (int h = 0; h < shape.height(); ++h, --y)
            {
                m_occupacy[y] |= shape.row(h);
                for (int w = 0; w < 7; ++w)
                    if (shape.occupied(h, w))
                        m_local_column_height[w] = std::max(m_local_column_height[w], y + 1);
            }
            m_height = std::max(m_height, y + shape.height() + 1);
            if (int lowest = *std::min_element(m_local_column_height.begin(), m_local_column_height.end());
                lowest > m_relative_floor)
            {
                for (int h = 0; h <= m_height; ++h)
                    m_occupacy[h] = m_occupacy[lowest + h];
                m_height -= lowest;
                m_height_removed += lowest;
                for (int w = 0; w < m_width; ++w)
                    m_local_column_height[w] -= lowest;
            }
        }
    }
    void draw(void) const
    {
        for (int h = m_height - 1; h >= 0; --h)
        {
            fmt::print("{:4d} ", h);
            std::cout << '|';
            for (int w = 0; w < m_width; ++w)
                std::cout << ((m_occupacy[h] & (1 << (7 - w - 1)))?'#':'.');
            std::cout << "|\n";
        }
        std::cout << "     +";
        for (int w = 0; w < m_width; ++w) std::cout << '-';
        std::cout << "+\n";
        for (int i = 0; i < m_width; ++i)
            std::cout << m_local_column_height[i] << " ";
        std::cout << '\n';
    }
};


// -----------------------------------------------------------------------------

int main(int argc, char * * argv)
{
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string jet_pattern;
    for (std::string line; std::getline(file, line); )
        jet_pattern += line;
    Tetris board;
#if 0
    board.simulate(10, jet_pattern);
    board.draw();
#else
    board.simulate(2'022, jet_pattern);
    std::cout << "[TEST A] Expected output for test example is 3'068.\n";
    std::cout << "[TEST A] Tower height: " << board.height() << '\n';
    board.clean();
    board.simulate(1'000'000'000'000, jet_pattern);
    std::cout << "[TEST B] Expected output for test example is 1'514'285'714'288.\n";
    std::cout << "[TEST B] Tower height: " << board.height() << '\n';
#endif
    return EXIT_SUCCESS;
}


