#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>

auto tailPositions(int number_of_knots,
                   const std::vector<std::tuple<char, int> > &actions)
{
    std::set<std::tuple<int, int> > positions;
    positions.insert({0, 0});
    std::vector<std::tuple<int, int> > rope(number_of_knots, {0, 0});
    for (auto [motion, displacement] : actions)
    {
        for (int d = 0; d < displacement; ++d)
        {
            switch (motion)
            {
            case 'U':
                std::get<1>(rope[0]) += 1;
                break;
            case 'D':
                std::get<1>(rope[0]) -= 1;
                break;
            case 'L':
                std::get<0>(rope[0]) -= 1;
                break;
            case 'R':
                std::get<0>(rope[0]) += 1;
                break;
            default:
                std::cout << "Unknown\n";
                break;
            }
            for (int i = 1; i < number_of_knots; ++i)
            {
                int dx = std::get<0>(rope[i - 1]) - std::get<0>(rope[i]);
                int dy = std::get<1>(rope[i - 1]) - std::get<1>(rope[i]);
                // When the distance is two in any of the directions...
                if ((std::abs(dx) > 1) || (std::abs(dy) > 1))
                {
                    // move only **one** step in the direction of the difference.
                    std::get<0>(rope[i]) += (2 * (dx > 0) - 1) * (dx != 0);
                    std::get<1>(rope[i]) += (2 * (dy > 0) - 1) * (dy != 0);
                }
            }
            positions.insert(rope.back());
        }
    }
    return positions;
}

int main(int argc, char * * argv)
{
    std::vector<std::tuple<char, int> > actions;
    std::set<std::tuple<int, int> > positions;
    std::string line;
    
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    while (std::getline(file, line))
    {
        std::istringstream input(line);
        char motion;
        int displacement;
        input >> motion >> displacement;
        actions.push_back({motion, displacement});
    }
    std::cout << "Short rope's number of tail positions: " << tailPositions(2, actions).size() << '\n';
    std::cout << "Long rope's number of tail positions: " << tailPositions(10, actions).size() << '\n';
    
    return EXIT_SUCCESS;
}

