# [Day 9: Rope Bridge](https://adventofcode.com/2022/day/9)

#### Problem A

You have a rope with two knots that are always at most one unit of distance away:

```
....       ....       ....
.TH.       .H..       .H..
....       ..T.       ....
....       ....       ....
left     diagonal   covering
```

The tail of the rope will move depending on the distance of the head:
- When they are two steps away directly *up*, *down*, *left*, or *right*, the tail must also move one step in that direction:
```
.....    .....    .....
.TH.. -> .T.H. -> ..TH.
.....    .....    .....

...    ...    ...
.T.    .T.    ...
.H. -> ... -> .T.
...    .H.    .H.
...    ...    ...
```
- When the separation is bigger *(diagonal displacement)*, the tail always moves one step diagonally to keep up:
```
.....    .....    .....
.....    ..H..    ..H..
..H.. -> ..... -> ..T..
.T...    .T...    .....
.....    .....    .....

.....    .....    .....
.....    .....    .....
..H.. -> ...H. -> ..TH.
.T...    .T...    .....
.....    .....    .....
```
Therefore, moving the rope's head following `U` up, `D` down, `L` left or `R` right for  several steps, the rope tail will generate a path while following the head. For example, the following instructions:
```
R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2
```

Generate this path:

![Path followed by the head and tail knots of the rope](images/path.svg)

Red dots correspond to positions only visited by the head knot while blue dots are visited by both knots.

**How many different positions visits the tail knot at least once?**

#### Problem B

Suppose now that the rope has 10 different knots, where the first knot behaves as the head knot while the remaining knots follow the knot in front of it using the same rules as the tail knot from before.

**How many different places will visit the last knot of this rope?**

