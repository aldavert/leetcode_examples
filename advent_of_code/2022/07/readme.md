# [Day 7: No Space Left On Device](https://adventofcode.com/2022/day/7)

#### Problem A

Given a log of the commands `cd` and `ls` obtained while navigating through a filesystem, you can figure out the structure of that filesystem. Find all of the directories with a total size of at most `100'000`, i.e. the directories where the addition of the size of all **its files and subdirectories** is at most `100'000`. **What is the sum of the total sizes of those directories?**

#### Problem B

The total disk space available to the filesystem is `70'000'000` and you need unused space of at least `30'000'000`.

Find the smallest directory that, if deleted, would free up enough space on the filesystem to run the update. **What is the total size of that directory?**

***Note:*** *The disk is not completely full, so you first need to determine the amount of free space and then determine the directory to delete.*


