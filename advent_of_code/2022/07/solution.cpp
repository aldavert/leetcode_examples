#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>

int main(int argc, char * * argv)
{
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string command;
    auto previous = [](std::string current) -> std::string
    {
        size_t last = current.substr(0, current.size() - 1).rfind("/");
        return current.substr(0, last + 1);
    };
    
    std::map<std::string, size_t> folders;
    std::unordered_map<std::string, size_t> folders_accumulate;
    std::string current_folder = "/";
    bool listing = false;
    while (std::getline(file, command))
    {
        if      (command == "$ cd /")
        {
            current_folder = "/";
            listing = false;
        }
        else if (command == "$ cd ..")
        {
            current_folder = previous(current_folder);
            listing = false;
        }
        else if (command.substr(0, 5) == "$ cd ")
        {
            current_folder += command.substr(5, command.size() - 5) + "/";
            listing = false;
        }
        else if (command == "$ ls")
            listing = true;
        else if (listing)
        {
            std::istringstream input(command);
            std::string description, filename;
            input >> description >> filename;
            if (description != "dir")
                folders[current_folder] += stol(description);
        }
    }
    for (auto begin = folders.rbegin(), end = folders.rend(); begin != end; ++begin)
    {
        auto [filename, size] = *begin;
        for (std::string current = filename; current != "/"; current = previous(current))
            folders_accumulate[current] += size;
        folders_accumulate["/"] += size;
    }
    // Task 1 ..................................................................
    size_t result = 0;
    for (auto [filename, size] : folders_accumulate)
        if (size <= 100'000)
            result += size;
    // Task 2 ..................................................................
    const size_t total_space = 70'000'000;
    const size_t wanted_space = 30'000'000;
    const size_t free_space = total_space - folders_accumulate["/"];
    const size_t minimum_freed = (wanted_space > free_space)?wanted_space - free_space:0;
    size_t minimum_folder_size = std::numeric_limits<size_t>::max();
    for (auto [filename, size] : folders_accumulate)
        if (size >= minimum_freed)
            minimum_folder_size = std::min(minimum_folder_size, size);
    
    
    std::cout << "Used space: " << folders_accumulate["/"] << '\n';
    std::cout << "Free space: " << free_space << '\n';
    std::cout << "Minimum space to be freed: " << minimum_freed << '\n';
    std::cout << "-----------------------------------------------------------------\n";
    std::cout << "[TASK 1] Sum of all the smallest directories: " << result << '\n';
    std::cout << "[TASK 2] Size of the smallest folder that will liberate enough "
                 "space: " << minimum_folder_size << '\n';
    return EXIT_SUCCESS;
}

