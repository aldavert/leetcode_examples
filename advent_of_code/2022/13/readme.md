# [Day 13: Distress Signal](https://adventofcode.com/2022/day/13)

#### Problem A

You receive a message but the packets are in the wrong order. You need to identify how many pairs of packets are in the right order:

- A data packet consist of lists and integers.
- Lists are enclosed between square brackets (`[` and `]`) and contains zero or more comma-separated values (either lists or integers).
- Each packet is a list and it appears on its own line.

When comparing two values (the value from first packet is named `A` while the value from the second packet is called `B`):
- If both are integers, the **lower integer** should come first:
    - If `A < B` the inputs are in the right order.
    - If `B < A` the inputs are not in the right order.
    - If `A == B` continue checking
- If both elements are lists:
    - Compare the values of the list in sequential order (first, second, third, ...).
    - If list `A` runs out of items first, inputs are in the right order.
    - If list `B` runs out of items first, inputs are not in the right order.
    - If both lists have the same number of elements, continue checking.
- If **exactly one value is an integer**, convert the integer to a list which contains that integer as its only value, the retry the comparison.

**What is the sum of the indexes of the pairs of packages that are in the right order**

#### Problem B

Create a list with all packages received and add two extra divider packages: `[[2]]` and `[[6]]`. Then, using the comparison function created in **Problem A** sort all the packages.

**What is the product of the indices of the two divider packages?**

