#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    for (const auto &v : vec)
        out << v << '\n';;
    return out;
}

struct Parser
{
    Parser(std::string t) :
        index(0),
        previous(0),
        list_level(0),
        value(0),
        just_closed(false),
        is_value(false),
        text(t) {}
    void next(void)
    {
        previous = index;
        is_value = just_closed = false;
        if (text[index] == '[')
            ++list_level, ++index;
        else if (text[index] == ']')
        {
            --list_level, ++index, just_closed = true;
            if (text[index] == ',') ++index;
        }
        else
        {
            is_value = true;
            value = 0;
            while ((text[index] != ',') && (text[index] != ']'))
                value = value * 10 + static_cast<int>(text[index++] - '0');
            if (text[index] == ',') ++index;
        }
    }
    void back(void) { index = previous; }
    inline bool done(void) const { return index >= text.size(); }
    
    size_t index = 0;
    size_t previous = 0;
    int list_level = 0;
    int value = 0;
    bool just_closed = false;
    bool is_value = false;
    std::string text = "";
};

bool compare(std::string first, std::string second)
{
    Parser pfirst(first), psecond(second);
    while (!(pfirst.done() || psecond.done()))
    {
        pfirst.next();
        psecond.next();
        if (pfirst.list_level == psecond.list_level)
        {
            if (pfirst.is_value && psecond.is_value)
            {
                if (pfirst.value == psecond.value) continue;
                return pfirst.value < psecond.value;
            }
            else
            {
                // Nothing to do here. It can only be that both lists have 
                // just closed.
            }
        }
        else
        {
            if      (pfirst.just_closed) return true;   // First list ran out of elements.
            else if (psecond.just_closed) return false; // Second list ran out of elements.
            else // Comparing list with integer.
            {
                if (pfirst.is_value)
                {
                    pfirst.text.insert(pfirst.index, "]");
                    pfirst.text.insert(pfirst.previous, "[");
                    --psecond.list_level;
                }
                else
                {
                    psecond.text.insert(psecond.index, "]");
                    psecond.text.insert(psecond.previous, "[");
                    --pfirst.list_level;
                }
                pfirst.back();
                psecond.back();
            }
        }
    }
    return pfirst.index < psecond.index;
}

int main(int argc, char * * argv)
{
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line, packets[2];
    int index = 1, resultA = 0;
    std::vector<std::string> all_packages;
    for (bool second = false; std::getline(file, line);)
    {
        std::string first_packet, second_packet;
        if (!line.empty())
        {
            std::istringstream input(line);
            input >> packets[second];
            all_packages.push_back(packets[second]);
            if (second)
                resultA += index++ * compare(packets[0], packets[1]);
            second = !second;
        }
    }
    std::cout << "Test A: Sum of indices = " << resultA << '\n';
    
    all_packages.push_back("[[2]]");
    all_packages.push_back("[[6]]");
    std::sort(all_packages.begin(), all_packages.end(), compare);
    size_t resultB = 1;
    for (size_t i = 0, n = all_packages.size(); i < n; ++i)
        if ((all_packages[i] == "[[2]]") || (all_packages[i] == "[[6]]"))
            resultB *= (i + 1);
    std::cout << "Test B: Product of the divider packet indexes: " << resultB << '\n';
    
    return EXIT_SUCCESS;
}

