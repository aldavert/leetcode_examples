#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>
#include <functional>
#include <fmt/core.h>

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

//------------------------------------------------------------------------------
std::string tolower(std::string text)
{
    std::transform(text.begin(), text.end(), text.begin(), [](char c){return std::tolower(c);});
    return text;
}; 

enum class Material { Ore = 0, Clay, Obsidian, Geode };
Material materialID(std::string text)
{
    const static std::unordered_map<std::string, Material> dictionary = {
        {"ore", Material::Ore}, {"clay", Material::Clay},
        {"obsidian", Material::Obsidian}, {"geode", Material::Geode} };
    text = tolower(text);
    while (!std::isalnum(text.back())) text = text.substr(0, text.size() - 1);
    while (!std::isalnum(text.front())) text = text.substr(1, text.size() - 1);
    return dictionary.at(text);
}

struct Blueprint
{
    int cost[4][3] = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
    void update(Material robot_id, Material material_id, int value)
    {
        cost[static_cast<int>(robot_id)][static_cast<int>(material_id)] = value;
    }
};

int production(const Blueprint &bp, int time)
{
    //                                  Ore     Clay Obsidian    Geode
    std::vector<int> init_robots = {      1,       0,       0,       0};
    std::vector<int> init_materials = {   0,       0,       0,       0};
    
    int best_result = 0;
    std::unordered_map<std::string, std::tuple<int, int, int, int> > memoA;
    std::function<void(std::vector<int>, std::vector<int>, int)> process =
        [&](std::vector<int> robots, std::vector<int> materials, int remaining_time) -> void
    {
        while (remaining_time > 0)
        {
            // Best possible expected result in ideal conditions: current number
            // of geodes + geodes that can be produced with the available robots
            // + geodes that can be produced if a geode cracking robot is created
            // each minute up to the time limit.
            int expected_result = materials[3]
                                + robots[3] * remaining_time
                                + remaining_time * (remaining_time - 1) / 2;
            if (expected_result < best_result)
                break;
            // Reject configurations the have the same amount of robots at a
            // certain time but have obtained the same or less resources.
            const char identifier[6] = {
                static_cast<char>(robots[0] + 'A'),
                static_cast<char>(robots[1] + 'A'),
                static_cast<char>(robots[2] + 'A'),
                static_cast<char>(robots[3] + 'A'),
                static_cast<char>(remaining_time + 'A'), 0};
            bool not_found = true;
            if (auto search = memoA.find(identifier); search != memoA.end())
            {
                if ((materials[0] <= std::get<0>(search->second))
                &&  (materials[1] <= std::get<1>(search->second))
                &&  (materials[2] <= std::get<2>(search->second))
                &&  (materials[3] <= std::get<3>(search->second)))
                    not_found = false;
            }
            if (not_found) memoA[identifier] = {materials[0], materials[1], materials[2], materials[3]};
            else break;
            // Branching algorithm.
            auto materials_before = materials;
            for (int i = 0; i < 4; ++i)
                materials[i] += robots[i];
            best_result = std::max(best_result, materials[3]);
            --remaining_time;
            for (int i = 3; i >= 0; --i)    // Go backward to prioritize geode
            {                               // cracking robots.
                if ((materials_before[0] >= bp.cost[i][0])
                &&  (materials_before[1] >= bp.cost[i][1])
                &&  (materials_before[2] >= bp.cost[i][2]))
                {
                    auto new_robots = robots;
                    ++new_robots[i];
                    std::vector<int> new_materials = {
                        materials[0] - bp.cost[i][0],
                        materials[1] - bp.cost[i][1],
                        materials[2] - bp.cost[i][2],
                        materials[3] };
                    process(new_robots, new_materials, remaining_time);
                }
            }
        }
    };
    process(init_robots, init_materials, time);
    return best_result;
}

//------------------------------------------------------------------------------

int main(int argc, char * * argv)
{
    std::unordered_map<int, Blueprint> blueprints;
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line;
    int blueprint_index = -1;
    Blueprint parser_blueprint;
    Material parser_robot_id = Material::Ore;
    while (std::getline(file, line))
    {
        std::istringstream input(line);
        for (std::string text; input >> text; )
        {
            if (tolower(text) == "blueprint")
            {
                if (blueprint_index != -1)
                    blueprints[blueprint_index] = parser_blueprint;
                input >> blueprint_index;
                parser_blueprint = Blueprint();
            }
            else if (tolower(text) == "each")
            {
                std::string robot_text, material_text;
                int value;
                input >> robot_text >> text >> text;
                input >> value >> material_text;
                parser_robot_id = materialID(robot_text);
                parser_blueprint.update(parser_robot_id, materialID(material_text), value);
            }
            else if (tolower(text) == "and")
            {
                std::string material_text;
                int value;
                input >> value >> material_text;
                parser_blueprint.update(parser_robot_id, materialID(material_text), value);
            }
        }
    }
    if (blueprint_index != -1)
        blueprints[blueprint_index] = parser_blueprint;
    int result = 0;
    for (auto [index, blueprint] : blueprints)
        result += index * production(blueprint, 24);
    std::cout << "[PROBLEM A] Sum(index_i * production_i) = " << result << '\n';
    if (blueprints.size() > 2)
    {
        std::cout << "[PROBLEM B] Product of the three top blueprints = ";
        std::cout << production(blueprints[1], 32) * production(blueprints[2], 32)
                   * production(blueprints[3], 32) << '\n';
    }
    
    return EXIT_SUCCESS;
}

