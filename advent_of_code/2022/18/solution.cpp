#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>
#include <fmt/core.h>

int main(int argc, char * * argv)
{
    constexpr char EMPTY = -1;
    constexpr char AIR = 0;
    constexpr char OBSIDIAN = 1;
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line;
    std::vector<std::tuple<int, int, int> > coordinates;
    int min_values[3] = { std::numeric_limits<int>::max(),
                          std::numeric_limits<int>::max(),
                          std::numeric_limits<int>::max() };
    int max_values[3] = { std::numeric_limits<int>::lowest(),
                          std::numeric_limits<int>::lowest(),
                          std::numeric_limits<int>::lowest() };
    while (std::getline(file, line))
    {
        std::istringstream input(line);
        int x, y, z;
        char comma;
        input >> x >> comma >> y >> comma >> z;
        coordinates.push_back({x, y, z});
        min_values[0] = std::min(min_values[0], x);
        min_values[1] = std::min(min_values[1], y);
        min_values[2] = std::min(min_values[2], z);
        max_values[0] = std::max(max_values[0], x);
        max_values[1] = std::max(max_values[1], y);
        max_values[2] = std::max(max_values[2], z);
    }
    int size[3] = { max_values[0] - min_values[0] + 1,
                    max_values[1] - min_values[1] + 1,
                    max_values[2] - min_values[2] + 1 };
    std::vector<std::vector<std::vector<char> > > grid(size[0], std::vector<std::vector<char> >(size[1], std::vector<char>(size[2], EMPTY)));
    for (auto [x, y, z] : coordinates)
    {
        x -= min_values[0];
        y -= min_values[1];
        z -= min_values[2];
        grid[x][y][z] = OBSIDIAN;
    }
    auto fillAir = [&](int sx, int sy, int sz) -> void
    {
        std::queue<std::tuple<int, int, int> > elements;
        elements.push({sx, sy, sz});
        while (!elements.empty())
        {
            auto [x, y, z] = elements.front();
            elements.pop();
            if ((x < 0) || (x >= size[0])
            ||  (y < 0) || (y >= size[1])
            ||  (z < 0) || (z >= size[2])) continue;
            if (grid[x][y][z] != EMPTY) continue;
            grid[x][y][z] = AIR;
            elements.push({x - 1, y, z});
            elements.push({x + 1, y, z});
            elements.push({x, y - 1, z});
            elements.push({x, y + 1, z});
            elements.push({x, y, z - 1});
            elements.push({x, y, z + 1});
        }
    };
    for (int y = 0; y < size[1]; ++y)
    {
        for (int z = 0; z < size[2]; ++z)
        {
            if (grid[0][y][z] == EMPTY) fillAir(0, y, z);
            if (grid[size[0] - 1][y][z] == EMPTY) fillAir(size[0] - 1, y, z);
        }
    }
    for (int x = 0; x < size[0]; ++x)
    {
        for (int z = 0; z < size[2]; ++z)
        {
            if (grid[x][0][z] == EMPTY) fillAir(x, 0, z);
            if (grid[x][size[1] - 1][z] == EMPTY) fillAir(x, size[1] - 1, z);
        }
    }
    for (int x = 0; x < size[0]; ++x)
    {
        for (int y = 0; y < size[1]; ++y)
        {
            if (grid[x][y][0] == EMPTY) fillAir(x, y, 0);
            if (grid[x][y][size[2] - 1] == EMPTY) fillAir(x, y, size[2] - 1);
        }
    }
    int exposed_sides = 0;
    int exterior_sides = 0;
    for (int x = 0; x < size[0]; ++x)
    {
        for (int y = 0; y < size[1]; ++y)
        {
            for (int z = 0; z < size[2]; ++z)
            {
                if (grid[x][y][z] == OBSIDIAN)
                {
                    exposed_sides += (x == 0)
                                   + ((x > 0) && (grid[x - 1][y][z] != OBSIDIAN))
                                   + (x + 1 == size[0])
                                   + ((x + 1 < size[0]) && (grid[x + 1][y][z] != OBSIDIAN))
                                   + (y == 0)
                                   + ((y > 0) && (grid[x][y - 1][z] != OBSIDIAN))
                                   + (y + 1 == size[1])
                                   + ((y + 1 < size[1]) && (grid[x][y + 1][z] != OBSIDIAN))
                                   + (z == 0)
                                   + ((z > 0) && (grid[x][y][z - 1] != OBSIDIAN))
                                   + (z + 1 == size[2])
                                   + ((z + 1 < size[2]) && (grid[x][y][z + 1] != OBSIDIAN));
                    exterior_sides += (x == 0)
                                    + ((x > 0) && (grid[x - 1][y][z] == AIR))
                                    + (x + 1 == size[0])
                                    + ((x + 1 < size[0]) && (grid[x + 1][y][z] == AIR))
                                    + (y == 0)
                                    + ((y > 0) && (grid[x][y - 1][z] == AIR))
                                    + (y + 1 == size[1])
                                    + ((y + 1 < size[1]) && (grid[x][y + 1][z] == AIR))
                                    + (z == 0)
                                    + ((z > 0) && (grid[x][y][z - 1] == AIR))
                                    + (z + 1 == size[2])
                                    + ((z + 1 < size[2]) && (grid[x][y][z + 1] == AIR));
                }
            }
        }
    }
    std::cout << "Exposed sides: " << exposed_sides << '\n';
    std::cout << "Exterior sides: " << exterior_sides << '\n';
    
    
    return EXIT_SUCCESS;
}

