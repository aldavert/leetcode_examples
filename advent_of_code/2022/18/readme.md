# [Day 18: Boiling Boulders](https://adventofcode.com/2022/day/18)

#### Problem A

You are given the coordinates of cubes forming an object ([voxel](https://en.wikipedia.org/wiki/Voxel) representation). You have to compute the total surface of the object, i.e. how many faces are not touching another cube.

#### Problem A

Same as before, but you have to ignore the air pockets inside the cube.

