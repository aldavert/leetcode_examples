# [Day 16: Proboscidea Volcanium](https://adventofcode.com/2022/day/16)

#### Problem A

You are given a graph where the nodes are valves and the edges are path connecting the valves. The valves takes a minute to open and the time needed to traverse to a neighboring valve is also a minute. Each valve you open releases a fixed amount of gas per minute. **You need to find which operations you have to do to maximize the amount of gas released in 30 minutes.**

***Solution notes:***<br>
Instead of traversing the graph, we can compute the distance between the graph nodes. For instance, the [distance matrix](https://en.wikipedia.org/wiki/Distance_matrix) of the small example is:
```
    AA  BB  CC  DD  EE  FF  GG  HH  II  JJ
AA   0   1   2   1   2   3   4   5   1   2
BB   1   0   1   2   3   4   5   6   2   3
CC   2   1   0   1   2   3   4   5   3   4
DD   1   2   1   0   1   2   3   4   2   3
EE   2   3   2   1   0   1   2   3   3   4
FF   3   4   3   2   1   0   1   2   4   5
GG   4   5   4   3   2   1   0   1   5   6
HH   5   6   5   4   3   2   1   0   6   7
II   1   2   3   2   3   4   5   6   0   1
JJ   2   3   4   3   4   5   6   7   1   0
```
Then, you only have to maximize the flow rate by finding the order which visiting each node **only once** releases the most steam. So the `time` at each move is reduced by the `distance` minutes between nodes `+1 minute` to open the valve. Since you open the valve each time you move to a node, you can avoid nodes where the *flow rate* is 0. Notice that both problems (example and actual problem) have a large number of nodes that can be ignored:
```
Flow rate per node:
 AA:  0 BB: 13 CC:  2 DD: 20 EE:  3 FF:  0 GG:  0 HH: 22 II:  0 JJ: 21
```

Finally, you can further optimize your solution by pruning paths that are not going to improve the score. When you arrive at a node, you have the accumulated partial score accumulated while traversing the graph. Then, you can estimate what is the *possible maximum score* that you can reach by estimating what would be the maximum flow rate if you could travel to any node with just one move. If adding this estimation to the actual accumulated score, gives you a value lower than the best score that you have already seen score, you can stop following the current solution branch. Likewise, sort the nodes that you are going to go next by the possible gain that you can have, so you are going to obtain a high score quickly that will allow you to prune more bad search branches.

#### Problem B

**Same problem but now you have a helper (so you can work in parallel) and you only have 26 minutes.**
