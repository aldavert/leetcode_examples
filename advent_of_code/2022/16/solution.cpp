#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <queue>
#include <limits>
#include <set>
#include <fmt/core.h>
#include <functional>

#define DEBUG 0

std::ostream& operator<<(std::ostream &out, const std::vector<std::string> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    for (const auto &v : vec)
        out << v << '\n';;
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<char> &vec)
{
    for (const auto &v : vec) out << v;
    return out;
}

// -----------------------------------------------------------------------------

struct Node
{
    int flow;
    std::vector<std::string> neighbors;
};

auto computeDistanceMap(const std::unordered_map<std::string, Node> &graph,
                        const std::unordered_map<std::string, size_t> &id2idx,
                        const std::vector<std::string> &idx2id) -> std::vector<std::vector<int> >
{
    const size_t n = graph.size();
    std::vector<std::vector<int> > distances(n);
    for (size_t i = 0; i < n; ++i)
    {
        std::vector<int> current(n, std::numeric_limits<int>::max());
        std::queue<std::string> active;
        active.push(idx2id[i]);
        int steps = 0;
        while (!active.empty())
        {
            size_t number_same_distance = active.size();
            for (size_t j = 0; j < number_same_distance; ++j)
            {
                std::string node = active.front();
                active.pop();
                size_t node_idx = id2idx.at(node);
                if (steps < current[node_idx])
                {
                    current[node_idx] = steps;
                    for (auto neighbor : graph.at(node).neighbors)
                        active.push(neighbor);
                }
            }
            ++steps;
        }
        distances[i] = current;
    }
    return distances;
}

int maxPressure(const std::unordered_map<std::string, Node> &graph, std::string start, int time)
{
    const size_t n = graph.size();
    std::unordered_map<std::string, size_t> id2idx;
    std::vector<std::string> idx2id;
    std::vector<std::tuple<int, size_t> > flow_order;
    std::vector<int> flow;
    for (const auto &node : graph)
    {
        flow_order.push_back({node.second.flow, id2idx.size()});
        id2idx[node.first] = id2idx.size();
        idx2id.push_back(node.first);
        flow.push_back(node.second.flow);
    }
    std::vector<bool> available(n);
    for (size_t i = 0; i < n; ++i)      // Ignore all nodes without any pressure.
        available[i] = flow[i] != 0;
    std::sort(flow_order.begin(), flow_order.end(), std::greater());
    std::vector<std::vector<int> > distances = computeDistanceMap(graph, id2idx, idx2id);
    std::vector<std::vector<std::tuple<int, size_t> > > expected_gain;
    for (size_t i = 0; i < n; ++i)
    {
        std::vector<std::tuple<int, size_t> > current_gain;
        for (size_t j = 0; j < n; ++j)
            current_gain.push_back({(i != j) * (n - distances[i][j]) * flow[j], j});
        std::sort(current_gain.begin(), current_gain.end(), std::greater());
        expected_gain.emplace_back(current_gain);
    }
    
    // -[ DEBUG ]---------------------------------------------------------------
#if DEBUG
    std::vector<std::string> node_names;
    for (const auto &[name, data] : graph)
        node_names.push_back(name);
    std::sort(node_names.begin(), node_names.end());
    std::cout << "\n\n\n";
    std::cout << "Distance between nodes:\n";
    std::cout << "  ";
    for (auto source : node_names)
        fmt::print(" {:>3}", source);
    std::cout << '\n';
    for (auto source : node_names)
    {
        fmt::print("{:>2}", source);
        for (auto destination : node_names)
            fmt::print(" {:3d}", distances[id2idx[source]][id2idx[destination]]);
        std::cout << '\n';
    }
    std::cout << "\n\n";
    std::cout << "Flow per node:\n";
    for (auto name : node_names)
        fmt::print(" {:>2}:{:3d}", name, flow[id2idx[name]]);
    std::cout << "\n\n";
    std::cout << "Gain expected to jump to another node:\n";
    for (size_t i = 0; i < n; ++i)
    {
        fmt::print("{:>2} ", idx2id[i]);
        for (auto [gain, index] : expected_gain[i])
            fmt::print(" {:3d}:{:>2}", gain, idx2id[index]);
        std::cout << '\n';
    }
#endif
    // -[ DEBUG ]---------------------------------------------------------------
    
    auto possibleFlow = [&](int remaining_time) -> int
    {
        int result = 0;
        for (size_t i = 0; (remaining_time > 0) && (i < n); ++i)
        {
            if (available[get<1>(flow_order[i])])
            {
                result += remaining_time * std::get<0>(flow_order[i]);
                remaining_time -= 2;
            }
        }
        return result;
    };
    int best_flow = 0;
    std::function<void(size_t, int, int)> compute =
        [&](size_t node_idx, int expected_flow, int time_available) -> void
    {
        for (auto [gain, i] : expected_gain[node_idx])
        {
            if ((i == node_idx) // Itself
            ||  (!available[i]) // Already opened.
            ||  (distances[i][node_idx] + 1 >= time_available)) // Too far away.
                continue;
            int new_time = time_available - distances[node_idx][i] - 1;
            if (new_time > 0)
            {
                expected_flow += flow[i] * new_time;
                available[i] = false;
                best_flow = std::max(best_flow, expected_flow);
                if (expected_flow + possibleFlow(new_time) >= best_flow)
                    compute(i, expected_flow, new_time);
                available[i] = true;
                expected_flow -= flow[i] * new_time;
            }
        }
    };
    
    compute(id2idx[start], 0, time);
    return best_flow;
}

int maxPressureTwo(const std::unordered_map<std::string, Node> &graph, std::string start, int time)
{
    const size_t n = graph.size();
    std::unordered_map<std::string, size_t> id2idx;
    std::vector<std::string> idx2id;
    std::vector<std::tuple<int, size_t> > flow_order;
    std::vector<int> flow;
    for (const auto &node : graph)
    {
        flow_order.push_back({node.second.flow, id2idx.size()});
        id2idx[node.first] = id2idx.size();
        idx2id.push_back(node.first);
        flow.push_back(node.second.flow);
    }
    std::vector<bool> available(n);
    for (size_t i = 0; i < n; ++i)      // Ignore all nodes without any pressure.
        available[i] = flow[i] != 0;
    std::sort(flow_order.begin(), flow_order.end(), std::greater());
    std::vector<std::vector<int> > distances = computeDistanceMap(graph, id2idx, idx2id);
    std::vector<std::vector<std::tuple<int, size_t> > > expected_gain;
    for (size_t i = 0; i < n; ++i)
    {
        std::vector<std::tuple<int, size_t> > current_gain;
        for (size_t j = 0; j < n; ++j)
            current_gain.push_back({(i != j) * (n - distances[i][j]) * flow[j], j});
        std::sort(current_gain.begin(), current_gain.end(), std::greater());
        expected_gain.emplace_back(current_gain);
    }
    auto possibleFlow = [&](int remaining_time) -> int
    {
        int result = 0;
        for (size_t i = 0; (remaining_time > 0) && (i < n); ++i)
        {
            if (available[get<1>(flow_order[i])])
            {
                result += remaining_time * std::get<0>(flow_order[i]);
                remaining_time -= 2;
            }
        }
        return result;
    };
    struct Agent
    {
        size_t node_idx;
        int time;
    };
    int best_flow = 0;
    std::function<void(Agent, Agent, int)> compute =
        [&](Agent userA, Agent userB, int expected_flow) -> void
    {
        if ((userA.time == 0) && (userB.time == 0)) return;
        Agent &user = (userA.time >= userB.time)?userA:userB;
        bool no_change = true;
        for (auto [gain, i] : expected_gain[user.node_idx])
        {
            if (!available[i]) continue;
            int new_time = user.time - distances[user.node_idx][i] - 1;
            if (new_time >= 0)
            {
                no_change = false;
                expected_flow += flow[i] * new_time;
                available[i] = false;
                best_flow = std::max(best_flow, expected_flow);
                if (expected_flow + possibleFlow(new_time) >= best_flow)
                {
                    Agent backup = std::exchange(user, {i, new_time});
                    compute(userA, userB, expected_flow);
                    user = backup;
                }
                available[i] = true;
                expected_flow -= flow[i] * new_time;
            }
        }
        if (no_change)
        {
            int aux = std::exchange(user.time, 0);
            compute(userA, userB, expected_flow);
            user.time = aux;
        }
    };
    
    compute({id2idx[start], time}, {id2idx[start], time}, 0);
    return best_flow;
}

int main(int argc, char * * argv)
{
    const std::string start_node = "AA";
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line;
    std::unordered_map<std::string, Node> graph;
    while (std::getline(file, line))
    {
        std::vector<std::string> destinations;
        std::string text, origin;
        char chr;
        int flow;
        
        std::istringstream input(line);
        input >> text >> origin >> text >> text >> chr >> chr >> chr >> chr >> chr;
        input >> flow >> chr >> text >> text >> text >> text;
        while (input >> text)
        {
            if (text.back() == ',') text.pop_back();
            destinations.push_back(text);
        }
        graph[origin] = {flow, destinations};
    }
    std::cout << "[TEST A] Expected answer for example is 1651 and for problem is 2124\n";
    std::cout << "[TEST A] Maximum pressure is " << maxPressure(graph, start_node, 30) << '\n';
    std::cout << "[TEST B] Expected answer for example is 1707 and for problem is 2775\n";
    std::cout << "[TEST B] Maximum pressure is " << maxPressureTwo(graph, start_node, 26) << '\n';
    
    return EXIT_SUCCESS;
}

