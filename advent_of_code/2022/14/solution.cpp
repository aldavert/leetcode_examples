#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>

std::ostream& operator<<(std::ostream &out, const std::vector<std::pair<int, int> > &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << '(' << v.first << ", " << v.second << ')';
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    for (const auto &v : vec)
        out << v << '\n';;
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<char> &vec)
{
    for (const auto &v : vec) out << v;
    return out;
}

struct Map
{
    Map(const std::vector<std::vector<std::pair<int, int> > > &polylines,
        const std::pair<int, int> &entry_point)
    {
        std::pair<int, int> corner_top_left = entry_point,
                            corner_bottom_right = entry_point;
        for (const auto &polygon : polylines)
            for (auto [x, y] : polygon)
                corner_top_left.first  = std::min(corner_top_left.first , x),
                corner_top_left.second = std::min(corner_top_left.second, y),
                corner_bottom_right.first  = std::max(corner_bottom_right.first , x),
                corner_bottom_right.second = std::max(corner_bottom_right.second, y);
        --corner_top_left.first;
        ++corner_bottom_right.first;
        corner_bottom_right.second += 2; // Add for the extra floor.
        height = corner_bottom_right.second - corner_top_left.second + 1;
        int threshold = height + 1;
        corner_top_left.first = std::min(corner_top_left.first, 500 - threshold);
        corner_bottom_right.first = std::max(corner_bottom_right.first, 500 + threshold);
        
        width = corner_bottom_right.first - corner_top_left.first + 1;
        grid = std::vector<std::vector<char> >(height, std::vector<char>(width, '.'));
        entry_y = 0 - corner_top_left.second;
        entry_x = 500 - corner_top_left.first;
        for (const auto &polygon : polylines)
        {
            for (size_t i = 1, n = polygon.size(); i < n; ++i)
            {
                int dx = polygon[i].first  - polygon[i - 1].first;
                int dy = polygon[i].second - polygon[i - 1].second;
                dx = (dx != 0) * ((dx > 0) - (dx < 0));
                dy = (dy != 0) * ((dy > 0) - (dy < 0));
                for (auto current = polygon[i - 1]; current != polygon[i]; current.first += dx, current.second += dy)
                    grid[current.second - corner_top_left.second][current.first - corner_top_left.first] = '#';
                grid[polygon[i].second - corner_top_left.second][polygon[i].first - corner_top_left.first] = '#';
            }
        }
    }
    bool addParticle(void)
    {
        int x = entry_x, y = entry_y;
        while (y < height - 1)
        {
            if      (grid[y + 1][x] == '.') ++y;
            else if ((x > 0) && (grid[y + 1][x - 1] == '.')) --x, ++y;
            else if ((x + 1 < width) && (grid[y + 1][x + 1] == '.')) ++x, ++y;
            else
            {
                grid[y][x] = 'o';
                return true;
            }
        }
        return false;
    }
    void clean(void)
    {
        for (auto &row : grid)
            for (auto &cell : row)
                if (cell == 'o')
                    cell = '.';
    }
    void addFloor(void)
    {
        for (auto &cell : grid.back())
            cell = '#';
    }
    bool blocked(void) { return grid[entry_y][entry_x] == 'o'; }
    int entry_x = 0;
    int entry_y = 0;
    int width = 0;
    int height = 0;
    std::vector<std::vector<char> > grid;
};

int main(int argc, char * * argv)
{
    const std::pair<int, int> entry_point = {500, 0};
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line;
    std::vector<std::vector<std::pair<int, int> > > polylines;
    while (std::getline(file, line))
    {
        std::vector<std::pair<int, int> > current;
        std::istringstream input(line);
        while (input)
        {
            int x, y;
            char comma;
            std::string arrow;
            input >> x >> comma >> y >> arrow;
            current.push_back({x, y});
        }
        polylines.push_back(current);
    }
    
    Map m(polylines, entry_point);
    
    int counter = 0;
    for (; m.addParticle(); ++counter);
    //for (int y = 0; y < m.height; ++y)
    //    std::cout << m.grid[y] << '\n';
    std::cout << "Test A: Number of particles: " << counter << '\n';
    m.clean();
    m.addFloor();
    counter = 0;
    for (; !m.blocked() && m.addParticle(); ++counter);
    //for (int y = 0; y < m.height; ++y)
    //    std::cout << m.grid[y] << '\n';
    std::cout << "Test B: Number of particles: " << counter << '\n';
    
    return EXIT_SUCCESS;
}

