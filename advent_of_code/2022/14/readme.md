# [Day 14: Regolith Reservoir](https://adventofcode.com/2022/day/14)

#### Problem A

You are given a map as a file with different lines polygons made of straight lines:
- This lines represent obstacles.
- Below the obstacles there is nothing, there is the *void*.
- At point `(500, 0)`, you have an **entry point** where sand is poured into the map.

Sand physics:
- The **sand** is produced one at a time: the next sand particle is not simulated until the current one has stopped moving.
- The sand falls **straight down** until blocked, then it attempts to move diagonally: first down-left and then down-right.
- If the three possible moves are blocked, the it stops moving and the next sand particle is generated.

**How many units of sand are poured into the map before the sand starts flowing into the void?**

#### Problem B

Now, you add an infinitely horizontal floor two units bellow the deepest part of the map. **How many units of sand can be poured into the map until the sand entry point is blocked?**.


