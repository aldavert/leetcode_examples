#include <iostream>
#include <fstream>
#include <string>
#include <utility>

#if 1
int main(int /*argc*/, char * * /*argv*/)
{
    std::ifstream file("data.txt");
    std::string line;
    int result[3] = {}, current = 0;
    auto update = [&](void) -> void
    {
        if (current > result[0])
            result[2] = result[1],
            result[1] = result[0],
            result[0] = current;
        else if (current > result[1])
            result[2] = result[1],
            result[1] = current;
        else if (current > result[2])
            result[2] = current;
    };
    while (std::getline(file, line))
    {
        if (line.empty())
        {
            update();
            current = 0;
        }
        else current += std::stoi(line);
    }
    file.close();
    update();
    std::cout << "Maximum number of calories: "
              << result[0] << '\n';
    std::cout << "Sum of the calories carried by the three Elves carrying the "
                 "most calories: " << result[0] + result[1] + result[2] << '\n';
}
#else
int main(int /*argc*/, char * * /*argv*/)
{
    std::ifstream file("data.txt");
    std::string line;
    int result = 0, current = 0;
    while (std::getline(file, line))
    {
        if (line.empty())
            result = std::max(result, std::exchange(current, 0));
        else current += std::stoi(line);
    }
    file.close();
    result = std::max(result, current);
    std::cout << "Maximum number of calories: " << result << '\n';
}
#endif
