# [Day 1: Calorie Counting](https://adventofcode.com/2022/day/1)

#### Problem A

You are given a list of the food's calories carried by various people. The list separates the entries carried by different people by a blank space. For example, the entries for five people would be:

```
1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
```

Return the amount of calories carried by the person that carries the most calories. In previous example, the answer is `24000`, as the fourth person carries `7000+8000+9000=24000`.

#### Problem B

Return instead the amount of calories carried by the three people that carry the most calories, in the previous example the fourth, third and fifth persons carry the most calories, so the answer would be `24000+11000+10000=45000`.

