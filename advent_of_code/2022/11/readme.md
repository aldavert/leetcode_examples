# [Day 11: Monkey in the Middle](https://adventofcode.com/2022/day/11)

#### Problem A

You have some monkeys that *inspect* a list of items and depending on your *worry level* they throw to one or another of their companions. Each monkey of the pack processes all its items at once by:

    1. Applying an update function which increases *worry level* of an item.
    2. Dampening the *worry level* by dividing it by 3.
    3. Selecting a monkey depending if the *worry level* value is divisible by a certain number (each monkey has its own number).
    4. Throwing the package to the selected monkey.
    5. Go back to 1, until they do not have any package left.

Once a monkey has processed all its items, the next one does the same. An iteration ends when each monkey has applied the algorithm to the packages that its holding.

**Find the number of items inspected by each monkey and return the product of the two largest numbers after 20 iterations**.

#### Problem B

Same as the *Problem A* but removing the dampening step (i.e. the worry level is not divided by 3). You have to find a method to keep the numbers reasonable as without the division they'll grow without control. Like before, **find the number of items inspected by each monkey and return the product of the two largest numbers after 10'000 iterations**.

