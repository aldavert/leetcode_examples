#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <set>
#include <numeric>

struct Monkey
{
    std::queue<long> items;
    long update_addition = 0;
    long update_multiplication = 1;
    long test_mod = 0;
    std::string monkey_true = "";
    std::string monkey_false = "";
    long inspected = 0;
    long worry_mod = std::numeric_limits<long>::max();
    long worry_dampening = 3;
    bool valid(void) const { return test_mod != 0 && monkey_true != "" && monkey_false != ""; }
    std::pair<int, std::string> update(void)
    {
        long worry_level = items.front();
        items.pop();
        if (update_addition != 0)
            worry_level += (update_addition < 0)?worry_level:update_addition;
        if (update_multiplication != 0)
            worry_level *= (update_multiplication < 0)?worry_level:update_multiplication;
        worry_level = (worry_level / worry_dampening) % worry_mod;
        ++inspected;
        return { worry_level, (worry_level % test_mod == 0)?monkey_true:monkey_false };
    }
};

std::ostream& operator<<(std::ostream &out, const std::queue<auto> &q)
{
    auto copy = q;
    out << '{';
    for (bool next = false; !copy.empty(); copy.pop())
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << copy.front();
    }
    out << '}';
    return out;
}
std::ostream& operator<<(std::ostream &out, const Monkey &m)
{
    out << " · Items: " << m.items << '\n';
    out << " · Operation: new  = old ";
    if (m.update_multiplication != 1)
    {
        out << "* ";
        if (m.update_multiplication < 0) out << "old ";
        else out << m.update_multiplication << " ";
    }
    if (m.update_addition != 0)
    {
        out << "+ ";
        if (m.update_addition < 0) out << "old";
        else out << m.update_addition;
    }
    out << '\n';
    out << " · Test: divisible by " << m.test_mod << "?<throw to " << m.monkey_true;
    out << ">:<throw to " << m.monkey_false << ">\n";
    out << " · Inspected: " << m.inspected << '\n';
    return out;
}
    
std::pair<long, long> mostInspectedFrequency(const std::vector<std::string> &labels,
                                             std::unordered_map<std::string, Monkey> monkeys,
                                             int number_of_iterations,
                                             bool display = false)
{
    long maximum_inspected[2] = {};
    for (int iteration = 0; iteration < number_of_iterations; ++iteration)
    {
        for (std::string identifier : labels)
        {
            Monkey &active = monkeys[identifier];
            while (!active.items.empty())
            {
                auto [worry, next] = active.update();
                monkeys[next].items.push(worry);
            }
        }
    }
    for (const auto &[identifier, monkey] : monkeys)
    {
        if (monkey.inspected > maximum_inspected[0])
            maximum_inspected[1] = std::exchange(maximum_inspected[0], monkey.inspected);
        else if (monkey.inspected > maximum_inspected[1])
            maximum_inspected[1] = monkey.inspected;
    }
    if (display)
        for (auto identifier : labels)
            std::cout << "Monkey " << identifier << '\n' << monkeys[identifier] << '\n';
    return { maximum_inspected[0], maximum_inspected[1] };
}

int main(int argc, char * * argv)
{
    auto trim = [](std::string text)
    {
        int spaces = 0;
        for (char c : text)
        {
            if (c == ' ') ++spaces;
            else break;
        }
        return text.substr(spaces);
    };
    std::unordered_map<std::string, Monkey> monkeys;
    auto current = monkeys.end();
    std::string line;
    
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    while (std::getline(file, line))
    {
        if (trim(line).substr(0, 6) == "Monkey")
        {
            std::istringstream input(line);
            std::string tag;
            std::string identifier;
            input >> tag >> identifier;
            if (identifier[identifier.size() - 1] == ':')
                identifier = identifier.substr(0, identifier.size() - 1);
            auto [iter, insertion] = monkeys.insert({identifier, Monkey()});
            if (!insertion)
            {
                std::cout << "ERROR: Duplicated monkey\n";
                return EXIT_FAILURE;
            }
            current = iter;
        }
        else if (trim(line).substr(0, 15) == "Starting items:")
        {
            if (current == monkeys.end())
            {
                std::cout << "ERROR: No active monkey\n";
                return EXIT_FAILURE;
            }
            std::istringstream input(trim(line).substr(16));
            int value;
            std::string comma;
            while (input)
            {
                input >> value >> comma;
                current->second.items.push(value);
            }
        }
        else if (trim(line).substr(0, 10) == "Operation:")
        {
            if (current == monkeys.end())
            {
                std::cout << "ERROR: No active monkey\n";
                return EXIT_FAILURE;
            }
            std::istringstream input(trim(line).substr(21));
            std::string op, value;
            input >> op >> value;
            if (op == "+")
                current->second.update_addition = (value == "old")?-1:stoi(value);
            else if (op == "*")
                current->second.update_multiplication = (value == "old")?-1:stoi(value);
            else
            {
                std::cout << "ERROR: Unknown monkey operator\n";
                return EXIT_FAILURE;
            }
        }
        else if (trim(line).substr(0, 5) == "Test:")
        {
            if (current == monkeys.end())
            {
                std::cout << "ERROR: No active monkey\n";
                return EXIT_FAILURE;
            }
            current->second.test_mod = std::stoi(trim(line).substr(19));
        }
        else if (trim(line).substr(0, 8) == "If true:")
        {
            if (current == monkeys.end())
            {
                std::cout << "ERROR: No active monkey\n";
                return EXIT_FAILURE;
            }
            current->second.monkey_true = trim(line).substr(25);
        }
        else if (trim(line).substr(0, 9) == "If false:")
        {
            if (current == monkeys.end())
            {
                std::cout << "ERROR: No active monkey\n";
                return EXIT_FAILURE;
            }
            current->second.monkey_false = trim(line).substr(26);
        }
    }
    for (auto [identifier, monkey] : monkeys)
    {
        if (!monkey.valid())
        {
            std::cout << "ERROR: Monkey not initialized properly\n";
            return EXIT_FAILURE;
        }
    }
    std::vector<std::string> labels;
    for (const auto &[identifier, monkey] : monkeys)
        labels.push_back(identifier);
    std::sort(labels.begin(), labels.end());
    
    // -[ TEST A ]--------------------------------------------------------------
    auto [most_firstA, most_secondA] = mostInspectedFrequency(labels, monkeys, 20);
    std::cout << "Test A: " << most_firstA << "*" << most_secondA << "=";
    std::cout << most_firstA * most_secondA << '\n';
    
    // -[ TEST B ]--------------------------------------------------------------
    long common_lcm = 1;
    for (const auto &[identifier, monkey] : monkeys)
        common_lcm = std::lcm(common_lcm, monkey.test_mod);
    for (auto &[identifier, monkey] : monkeys)
    {
        monkey.worry_mod = common_lcm;
        monkey.worry_dampening = 1;
    }
    auto [most_firstB, most_secondB] = mostInspectedFrequency(labels, monkeys, 10'000);
    std::cout << "Test B: " << most_firstB << "*" << most_secondB << "=";
    std::cout << most_firstB * most_secondB << '\n';
    
    return EXIT_SUCCESS;
}

