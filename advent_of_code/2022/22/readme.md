# [Day 21: Monkey Map](https://adventofcode.com/2022/day/22)

You are given a map and a route. In the map, your starting position corresponds to the **top-left-most** free cell and you are facing **right**. The route is series of numbers of numbers, corresponding to the number steps that you must take, and `R` or `L` indicating that you turn *clockwise* or *counter-clockwise*, respectively. While moving, you only access to a new cell if this cell is free. So, when the destination is an obstacle, you don't move from the original cell but you count a step.

The solution of the problem is computed as `1000 * row + 4 * column + facing` where facing is the numeric value associated to the direction: `{ 'RIGHT':0, 'DOWN':1, 'LEFT':2, 'UP':3 }`.

#### Problem A
In this variation of the problem, when you reach a border, you **teleport** to opposite cell of the same row or column. For example, if you hit the right wall while moving right, you appear to the left-most free cell of the same row and you are still facing right.<br>

*Route on the small problem:*<br>
![Image with the problem A route on the small sample problem](images/map_problemA_small.png)

#### Problem B
In this variation of the problem, we assume that the map corresponds to a flatten cube, so when you hit a wall, you have to **teleport** to the appropriate side of the cube and change your heading accordingly.<br>

*Route on the small problem:*<br>
![Image with the problem B route on the small sample problem](images/map_problemB_small.png)

