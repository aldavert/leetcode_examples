#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <vector>
#include <cctype>
#include <algorithm>
#include <numeric>
#include <fmt/core.h>
#include "cube.hpp"
#include <set>

#define DEBUG 0

std::ostream& operator<<(std::ostream &out, const std::tuple<int, int> &tup)
{
    out << '[' << std::get<0>(tup) << ", " << std::get<1>(tup) << ']';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<auto> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::string> &vec)
{
    for (const auto &v : vec)
        out << v << '\n';;
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<char> &vec)
{
    for (const auto &v : vec) out << v;
    return out;
}

void save(const char * filename, const std::vector<std::vector<unsigned char> > &image)
{
    const int height = static_cast<int>(image.size());
    if (height == 0) return;
    const int width  = static_cast<int>(image[0].size()) / 3;
    if (width == 0) return;
    std::ofstream file(filename, std::ios::binary);
    
    unsigned char fileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
    unsigned char infoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
    unsigned char pad[3] = {0, 0, 0};
    const int filesize = 54 + 3 * height * width;
    fileheader[ 2] = static_cast<unsigned char>(filesize      );
    fileheader[ 3] = static_cast<unsigned char>(filesize >>  8);
    fileheader[ 4] = static_cast<unsigned char>(filesize >> 16);
    fileheader[ 5] = static_cast<unsigned char>(filesize >> 24);
    infoheader[ 4] = static_cast<unsigned char>(   width      );
    infoheader[ 5] = static_cast<unsigned char>(   width >>  8);
    infoheader[ 6] = static_cast<unsigned char>(   width >> 16);
    infoheader[ 7] = static_cast<unsigned char>(   width >> 24);
    infoheader[ 8] = static_cast<unsigned char>(  height      );
    infoheader[ 9] = static_cast<unsigned char>(  height >>  8);
    infoheader[10] = static_cast<unsigned char>(  height >> 16);
    infoheader[11] = static_cast<unsigned char>(  height >> 24);
    file.write(reinterpret_cast<char *>(fileheader), 14);
    file.write(reinterpret_cast<char *>(infoheader), 40);
    for (int row = height - 1; row >= 0; --row)
    {
        file.write(reinterpret_cast<const char *>(image[row].data()), 3 * width);
        file.write(reinterpret_cast<char *>(pad), (4 - (3 * width) % 4) % 4);
    }
    file.close();
}

std::vector<std::vector<unsigned char> >
convertMap2Image(std::vector<std::string> text_map,
                 std::tuple<int, int> start,
                 std::tuple<int, int> end)
{
    if ((text_map.size() == 0) || (text_map[0].size() == 0)) return {};
    const int height = static_cast<int>(text_map.size()) - 10;
    const int width  = static_cast<int>(text_map[0].size()) - 10;
    for (int h = 0; h < height; ++h)
        if (static_cast<int>(text_map[h].size()) != width + 10)
            return {};
    int scale = 20;
    std::vector<std::vector<unsigned char> > image((scale + 2) * height, std::vector<unsigned char>((scale + 2) * width * 3, 32));
    auto square = [&](int h, int w, unsigned char red, unsigned char green, unsigned char blue)
    {
        for (int yb = h * (scale + 2) + 1, ye = h * (scale + 2) + scale;
             yb < ye; ++yb)
        {
            for (int xb = w * (scale + 2) + 1, xe = w * (scale + 2) + scale;
                 xb < xe; ++xb)
            {
                image[yb][3 * xb + 0] = blue;
                image[yb][3 * xb + 1] = green;
                image[yb][3 * xb + 2] = red;
            }
        }
    };
    for (int h = 0; h < height; ++h)
    {
        for (int w = 0; w < width; ++w)
        {
            char current = text_map[h + 5][w + 5];
            if (current == '.')
                square(h, w, 255, 255, 255);
            else if (current == '#')
                square(h, w, 128, 0, 0);
            else if ((current == ' ') || ((current >= '0') && (current <= '9')))
                square(h, w, 32, 32, 32);
            else square(h, w, 255, 180, 0);
        }
    }
    square(std::get<1>(start) - 1, std::get<0>(start) - 1, 255, 0, 0);
    square(std::get<1>(end) - 1, std::get<0>(end) - 1, 0, 255, 0);
    return image;
}

//------------------------------------------------------------------------------

int mod(int value, int M) { return ((value % M) + M) % M; }
enum class DIRECTION { RIGHT = 0, DOWN, LEFT, UP, UNDEFINED };
struct Position
{
    Position(void) = default;
    Position(int w, int h, DIRECTION direction) :
        x(w),
        y(h),
        heading(direction) {}
    Position(size_t w, size_t h, DIRECTION direction) :
        x(static_cast<int>(w)),
        y(static_cast<int>(h)),
        heading(direction) {}
    int x = -1;
    int y = -1;
    DIRECTION heading = DIRECTION::UNDEFINED;
    void turnRight(void)
    {
        heading = static_cast<DIRECTION>(mod(static_cast<int>(heading) + 1, 4));
    }
    void turnLeft(void) 
    {
        heading = static_cast<DIRECTION>(mod(static_cast<int>(heading) - 1, 4));
    }
    bool isUndefined(void) const { return heading == DIRECTION::UNDEFINED; }
    void set(size_t w, size_t h, DIRECTION direction)
    {
        x = static_cast<int>(w);
        y = static_cast<int>(h);
        heading = direction;
    }
    Position next(void)
    {
        constexpr static int dx[5] = {  1,  0, -1,  0,  0};
        constexpr static int dy[5] = {  0,  1,  0, -1,  0};
        return { x + dx[static_cast<int>(heading)],
                 y + dy[static_cast<int>(heading)],
                 heading };
    }
};

class Map
{
public:
    Map(void) = default;
    void initPlain(const std::vector<std::string> &text_map);
    void initCube(const std::vector<std::string> &text_map);
    std::vector<std::string> draw(void) const;
    Position traverse(const std::string &instructions) const;
private:
    size_t m_start_w = -1;
    size_t m_start_h = -1;
    std::vector<std::vector<char> > m_cells;
    std::vector<Position> m_loop[4];
    static constexpr int RIGHT = static_cast<int>(DIRECTION::RIGHT);
    static constexpr int DOWN  = static_cast<int>(DIRECTION::DOWN );
    static constexpr int LEFT  = static_cast<int>(DIRECTION::LEFT );
    static constexpr int UP    = static_cast<int>(DIRECTION::UP   );
};

void Map::initPlain(const std::vector<std::string> &text_map)
{
    size_t width = 0, height = text_map.size() + 2;
    for (const auto &line : text_map)
        width = std::max(width, line.size());
    width += 2;
    m_cells = std::vector<std::vector<char> >(height, std::vector<char>(width, 0));
    m_loop[RIGHT].assign(height, {});
    m_loop[DOWN ].assign(width,  {});
    m_loop[LEFT ].assign(height, {});
    m_loop[UP   ].assign(width,  {});
    for (size_t h = 1; const auto &line : text_map)
    {
        for (size_t w = 1; char value : line)
        {
            if ((value == '.') || (value == '#'))
            {
                if (m_loop[RIGHT][h].isUndefined())
                    m_loop[RIGHT][h].set(w, h, DIRECTION::RIGHT);
                if (m_loop[DOWN][w].isUndefined())
                    m_loop[DOWN][w].set(w, h, DIRECTION::DOWN);
                m_cells[h][w] = value;
                m_loop[LEFT][h].set(w, h, DIRECTION::LEFT);
                m_loop[UP][w].set(w, h, DIRECTION::UP);
            }
            ++w;
        }
        ++h;
    }
    for (size_t w = 1; w < width; ++w)
    {
        if (m_cells[1][w] >= 32)
        {
            m_start_w = w;
            m_start_h = 1;
            break;
        }
    }
    for (size_t h = 0; h < height; ++h)
    {
        if (!m_loop[RIGHT][h].isUndefined()) m_cells[h][m_loop[RIGHT][h].x - 1] += 1;
        if (!m_loop[LEFT][h].isUndefined()) m_cells[h][m_loop[LEFT][h].x + 1] += 4;
    }
    for (size_t w = 0; w < width; ++w)
    {
        if (!m_loop[DOWN][w].isUndefined()) m_cells[m_loop[DOWN][w].y - 1][w] += 2;
        if (!m_loop[UP][w].isUndefined()) m_cells[m_loop[UP][w].y + 1][w] += 8;
    }
}

void Map::initCube(const std::vector<std::string> &text_map)
{
    size_t width = 0, height = text_map.size() + 2;
    for (const auto &line : text_map)
        width = std::max(width, line.size());
    width += 2;
    
    // 1) Find flatten cube geometry.
    size_t face_size = std::gcd(width - 2, height - 2);
    size_t face_width = (width - 2) / face_size;
    size_t face_height = (height - 2) / face_size;
    [[maybe_unused]] const int side_width = static_cast<int>((width - 2) / face_width);
    [[maybe_unused]] const int side_height = static_cast<int>((height - 2) / face_height);
    std::vector<std::vector<char> > face_map(face_height, std::vector<char>(face_width, '\0'));
    char face_current = 'a';
    m_cells = std::vector<std::vector<char> >(height, std::vector<char>(width, 0));
    m_loop[RIGHT].assign(height, {});
    m_loop[DOWN ].assign(width,  {});
    m_loop[LEFT ].assign(height, {});
    m_loop[UP   ].assign(width,  {});
    for (size_t h = 1; const auto &line : text_map)
    {
        for (size_t w = 1; char value : line)
        {
            if ((value == '.') || (value == '#'))
            {
                m_cells[h][w] = value;
                if (face_map[(h - 1) / face_size][(w - 1) / face_size] == '\0')
                {
                    face_map[(h - 1) / face_size][(w - 1) / face_size] = face_current;
                    ++face_current;
                }
            }
            ++w;
        }
        ++h;
    }
    for (size_t w = 1; w < width; ++w)
    {
        if (m_cells[1][w] >= 32)
        {
            m_start_w = w;
            m_start_h = 1;
            break;
        }
    }
    std::vector<Side> cube;
    for (size_t y = 0; y < face_height; ++y)
        for (size_t x = 0; x < face_width; ++x)
            if (face_map[y][x] != '\0')
                cube.push_back({static_cast<int>(x), static_cast<int>(y), face_map[y][x]});
    auto folded_cube = fold(cube);
    auto [cube_graph, cube_edges] = relationships(cube);
    std::set<std::tuple<size_t, size_t> > inner_edges;
    for (auto [id_a, id_b, correspondences] : cube_edges)
        inner_edges.insert({id_a, id_b});
    auto [folded_cube_graph, folded_cube_edges] = relationships(folded_cube);
    constexpr DIRECTION position_direction[] = {
        DIRECTION::UP, DIRECTION::RIGHT, DIRECTION::LEFT, DIRECTION::DOWN };
    DIRECTION invert_direction[4];
    invert_direction[static_cast<int>(DIRECTION::UP)] = DIRECTION::DOWN;
    invert_direction[static_cast<int>(DIRECTION::DOWN)] = DIRECTION::UP;
    invert_direction[static_cast<int>(DIRECTION::LEFT)] = DIRECTION::RIGHT;
    invert_direction[static_cast<int>(DIRECTION::RIGHT)] = DIRECTION::LEFT;
    struct Heading
    {
        int x;
        int y;
        int dx;
        int dy;
        DIRECTION heading;
    };
    auto fillMoves = [&](const Heading &in, const Heading &out, int n)
    {
        Position current = { out.x, out.y, invert_direction[static_cast<int>(out.heading)] };
        for (int i = 0, idx = in.x * (in.dx != 0) + in.y * (in.dy != 0), dir = in.dx + in.dy;
             i < n; ++i, idx += dir)
        {
            m_loop[static_cast<int>(in.heading)][idx + 1] = {
                current.x + (current.heading != DIRECTION::LEFT),
                current.y + (current.heading != DIRECTION::UP),
                current.heading};
            current.x += out.dx;
            current.y += out.dy;
        }
    };
    for (auto [id_a, id_b, correspondences] : folded_cube_edges)
    {
        if (inner_edges.find({id_a, id_b}) == inner_edges.end())
        {
            const int corner_begin_first  = std::get<0>(correspondences[0]);
            const int corner_end_first    = std::get<0>(correspondences[1]);
            const int corner_begin_second = std::get<1>(correspondences[0]);
            const int corner_end_second   = std::get<1>(correspondences[1]);
            const int dir_idx_first = (1 << corner_begin_first)
                                    | (1 << corner_end_first);
            const int dir_idx_second = (1 << corner_begin_second)
                                     | (1 << corner_end_second);
            Position first[2] = {
                { side_width  * cube[id_a].corners[corner_begin_first].x,
                  side_height * cube[id_a].corners[corner_begin_first].y,
                  position_direction[dir_idx_first / 3 - 1] },
                { side_width  * cube[id_a].corners[corner_end_first].x,
                  side_height * cube[id_a].corners[corner_end_first].y,
                  position_direction[dir_idx_first / 3 - 1] } };
            Position second[2] = {
                { side_width  * cube[id_b].corners[corner_begin_second].x,
                  side_height * cube[id_b].corners[corner_begin_second].y,
                 position_direction[dir_idx_second / 3 - 1] },
                { side_width  * cube[id_b].corners[corner_end_second].x,
                  side_height * cube[id_b].corners[corner_end_second].y,
                 position_direction[dir_idx_second / 3 - 1] } };
            const int n = std::abs(first[0].x - first[1].x)
                        + std::abs(first[0].y - first[1].y);
            Heading it_first, it_second;
            if (first[0].x == first[1].x)
            {
                it_first = { first[0].x, first[0].y - (first[0].y > first[1].y),
                             0, 2 * (first[0].y < first[1].y) - 1,
                             first[0].heading };
            }
            else
            {
                it_first = { first[0].x - (first[0].x > first[1].x), first[0].y,
                             2 * (first[0].x < first[1].x) - 1, 0,
                             first[0].heading };
            }
            if (second[0].x == second[1].x)
            {
                it_second = { second[0].x, second[0].y - (second[0].y > second[1].y),
                              0, 2 * (second[0].y < second[1].y) - 1,
                              second[0].heading };
            }
            else
            {
                it_second = { second[0].x - (second[0].x > second[1].x), second[0].y,
                              2 * (second[0].x < second[1].x) - 1, 0,
                              second[0].heading };
            }
            fillMoves(it_first, it_second, n);
            fillMoves(it_second, it_first, n);
        }
    }
}

std::vector<std::string> Map::draw(void) const
{
    const size_t width = m_cells[0].size();
    std::vector<std::string> text_map;
    std::string line;
    for (size_t factor = 100; factor > 0; factor /= 10)
    {
        line = "    ";
        for (size_t w = 0; w < width; ++w)
            line += std::to_string((m_loop[3][w].y / factor) % 10);
        line += "    ";
        text_map.push_back(std::exchange(line, ""));
    }
    line = std::string(text_map.back().size(), ' ');
    text_map.push_back(std::exchange(line, ""));
    for (size_t h = 0; const auto &row : m_cells)
    {
        line = fmt::format("{:03d} ", m_loop[2][h].x);
        for (size_t w = 0; char value : row)
        {
            if (value < 32) line += fmt::format("{:0X}", static_cast<int>(value));
            else line += value;
            ++w;
        }
        line += fmt::format(" {:03d}", m_loop[0][h].x);
        text_map.push_back(std::exchange(line, ""));
        ++h;
    }
    line = std::string(text_map.back().size(), ' ');
    text_map.push_back(std::exchange(line, ""));
    for (size_t factor = 100; factor > 0; factor /= 10)
    {
        line = "    ";
        for (size_t w = 0; w < width; ++w)
            line += std::to_string((m_loop[1][w].y / factor) % 10);
        line += "    ";
        text_map.push_back(std::exchange(line, ""));
    }
    return text_map;
}

Position Map::traverse(const std::string &instructions) const
{
    Position current(m_start_w, m_start_h, DIRECTION::RIGHT);
#if DEBUG // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    auto text_map = draw();
    auto updateDraw = [&]()
    {
        if (current.heading == DIRECTION::RIGHT) text_map[4 + current.y][4 + current.x] = '>';
        if (current.heading == DIRECTION::DOWN ) text_map[4 + current.y][4 + current.x] = 'v';
        if (current.heading == DIRECTION::LEFT ) text_map[4 + current.y][4 + current.x] = '<';
        if (current.heading == DIRECTION::UP   ) text_map[4 + current.y][4 + current.x] = '^';
    };
#endif    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    auto move = [&](int value)
    {
        for (int i = 0; i < value; ++i)
        {
            auto next = current.next();
            if (m_cells[next.y][next.x] < 32)
            {
                if ((current.heading == DIRECTION::LEFT)
                ||  (current.heading == DIRECTION::RIGHT))
                    next = m_loop[static_cast<int>(current.heading)][next.y];
                else // Up-down
                    next = m_loop[static_cast<int>(current.heading)][next.x];
            }
            if (m_cells[next.y][next.x] == '#') break;
            else if (m_cells[next.y][next.x] == '.')
                current = next;
#if DEBUG // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
            updateDraw();
#endif    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
        }
    };
#if DEBUG // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    updateDraw();
#endif    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    int value = 0;
    for (char symbol : instructions)
    {
        switch (symbol)
        {
        case 'R':
            move(value);
            current.turnRight();
#if DEBUG // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
            updateDraw();
#endif    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
            value = 0;
            break;
        case 'L':
            move(value);
            current.turnLeft();
#if DEBUG // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
            updateDraw();
#endif    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
            value = 0;
            break;
        default:
            value = value * 10 + symbol - '0';
        };
    }
    move(value);
#if DEBUG // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    std::cout << text_map << '\n'; 
    save("debug_map.bmp", convertMap2Image(text_map, {m_start_w, m_start_h}, {current.x, current.y}));
#endif    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    return current;
}

//------------------------------------------------------------------------------

int main(int argc, char * * argv)
{
    Map map;
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line;
    std::vector<std::string> text_map;
    while (std::getline(file, line))
        if (line.size() > 0)
            text_map.push_back(line);
    std::string instructions = text_map.back();
    text_map.pop_back();
    
    // -[ TEST A ]--------------------------------------------------------------
    map.initPlain(text_map);
    auto [xa, ya, ha] = map.traverse(instructions);
    std::cout << "[TEST A] Password: " << 1000 * ya + 4 * xa + static_cast<int>(ha) << '\n';
    
    // -[ TEST B ]--------------------------------------------------------------
    map.initCube(text_map);
    auto [xb, yb, hb] = map.traverse(instructions);
    std::cout << "[TEST B] Password: " << 1000 * yb + 4 * xb + static_cast<int>(hb) << '\n';
    
    return EXIT_SUCCESS;
}


