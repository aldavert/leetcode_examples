#ifndef __CUBE_HEADER_FILE__
#define __CUBE_HEADER_FILE__
#pragma once

#include <tuple>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <limits>

struct Point
{
    int x = 0;
    int y = 0;
    int z = 0;
    inline Point& operator+=(const Point &other)
    {
        x += other.x;
        y += other.y;
        z += other.z;
        return *this;
    }
    inline Point& operator-=(const Point &other)
    {
        x -= other.x;
        y -= other.y;
        z -= other.z;
        return *this;
    }
    inline Point operator-(const Point &other) const
    {
        return {x - other.x, y - other.y, z - other.z};
    }
};

struct Side
{
    Point corners[4] = {};
    char label = '\0';
    Side(void) = default;
    Side(int x, int y, char l)  :
        label(l)
    {
        corners[0] = { x    , y    , 0 };
        corners[1] = { x + 1, y    , 0 };
        corners[2] = { x + 1, y + 1, 0 };
        corners[3] = { x    , y + 1, 0 };
    }
    std::vector<std::tuple<int, int> > isNeighbor(const Side &other) const
    {
        std::vector<std::vector<int> > closest(4, std::vector<int>(4));
        for (int i = 0; i < 4; ++i)
            for (int j = 0; j < 4; ++j)
                closest[i][j] = std::abs(corners[i].x - other.corners[j].x)
                              + std::abs(corners[i].y - other.corners[j].y)
                              + std::abs(corners[i].z - other.corners[j].z);
        std::vector<std::tuple<int, int> > edges;
        while (true)
        {
            int closest_value = std::numeric_limits<int>::max();
            int closest_i = 0;
            int closest_j = 0;
            for (int i = 0; i < 4; ++i)
                for (int j = 0; j < 4; ++j)
                    if (closest[i][j] < closest_value)
                        closest_value = closest[i][j],
                        closest_i = i,
                        closest_j = j;
            if (closest_value > 0)
                break;
            edges.push_back({closest_i, closest_j});
            for (int i = 0; i < 4; ++i)
                closest[closest_i][i] = std::numeric_limits<int>::max(),
                closest[i][closest_j] = std::numeric_limits<int>::max();
        }
        if (edges.size() != 2) return {};
        return edges;
    }
    void transform(Point p0, Point p1, bool clockwise)
    {
        auto dp = p0 - p1;
        for (int i = 0; i < 4; ++i)
            corners[i] -= p0;
        if (dp.x != 0)
        {
            for (int i = 0; i < 4; ++i)
            {
                std::swap(corners[i].y, corners[i].z);
                if (clockwise) corners[i].z = -corners[i].z;
                else corners[i].y = -corners[i].y;
            }
        }
        else if (dp.y != 0)
        {
            for (int i = 0; i < 4; ++i)
            {
                std::swap(corners[i].x, corners[i].z);
                if (clockwise) corners[i].x = -corners[i].x;
                else corners[i].z = -corners[i].z;
            }
        }
        else if (dp.z != 0)
        {
            for (int i = 0; i < 4; ++i)
            {
                std::swap(corners[i].x, corners[i].y);
                if (clockwise) corners[i].y = -corners[i].y;
                else corners[i].x = -corners[i].x;
            }
        }
        for (int i = 0; i < 4; ++i)
            corners[i] += p0;
    }
};

struct Edge
{
    size_t cube_i = std::numeric_limits<size_t>::max();
    size_t cube_j = std::numeric_limits<size_t>::max();
    std::vector<std::tuple<int, int> > correspondences;
};

typedef std::unordered_map<size_t, std::vector<size_t> > GRAPH;
typedef std::vector<Edge> EDGES;
std::tuple<GRAPH, EDGES> relationships(const std::vector<Side> &current_cube);
std::vector<Side> fold(std::vector<Side> &cube);

#endif

