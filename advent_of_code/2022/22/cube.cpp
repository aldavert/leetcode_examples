#include "cube.hpp"
#include <unordered_set>
#include <queue>

std::tuple<GRAPH, EDGES> relationships(const std::vector<Side> &current_cube)
{
    const size_t n = current_cube.size();
    GRAPH graph;
    EDGES edges;
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = i + 1; j < n; ++j)
        {
            auto edge = current_cube[i].isNeighbor(current_cube[j]);
            if (edge.size() == 2)
            {
                graph[i].push_back(j);
                graph[j].push_back(i);
                edges.push_back({i, j, edge});
            }
        }
    }
    return {graph, edges};
}

std::vector<Side> fold(std::vector<Side> &cube)
{
    auto graphSplit = [&](const GRAPH &graph, size_t source, size_t avoid) -> std::vector<size_t>
    {
        std::vector<size_t> result;
        std::unordered_set<size_t> visited = {avoid};
        std::queue<size_t> search;
        search.push(source);
        while (!search.empty())
        {
            size_t node = search.front();
            search.pop();
            if (visited.find(node) != visited.end()) continue;
            visited.insert(node);
            result.push_back(node);
            for (size_t neighbor : graph.at(node))
                if (visited.find(neighbor) == visited.end())
                    search.push(neighbor);
        }
        return result;
    };
    auto valid = [&](const std::vector<Side> &c) -> bool
    {
        auto [graph, edges] = relationships(c);
        int min_x = std::numeric_limits<int>::max(),
            min_y = std::numeric_limits<int>::max(),
            min_z = std::numeric_limits<int>::max(),
            max_x = std::numeric_limits<int>::lowest(),
            max_y = std::numeric_limits<int>::lowest(),
            max_z = std::numeric_limits<int>::lowest();
        for (const auto &[side, neighbors] : graph)
        {
            if (neighbors.size() != 4)
                return false;
            const std::vector<size_t> * opposite_neighbors = nullptr;
            bool present[6] = {};
            present[side] = true;
            for (size_t nindex : neighbors)
                present[nindex] = true;
            for (size_t i = 0; i < 6; ++i)
            {
                if (!present[i])
                {
                    opposite_neighbors = &graph.at(i);
                    break;
                }
            }
            if ((opposite_neighbors == nullptr)
            ||  (std::find(opposite_neighbors->begin(), opposite_neighbors->end(), side) != opposite_neighbors->end()))
                return false;
            for (size_t i = 0; i < 4; ++i)
            {
                min_x = std::min(min_x, c[side].corners[i].x);
                min_y = std::min(min_y, c[side].corners[i].y);
                min_z = std::min(min_z, c[side].corners[i].z);
                max_x = std::max(max_x, c[side].corners[i].x);
                max_y = std::max(max_y, c[side].corners[i].y);
                max_z = std::max(max_z, c[side].corners[i].z);
            }
        }
        if ((max_x - min_x != 1) || (max_y - min_y != 1) || (max_z - min_z != 1))
            return false;
        return true;
    };
    auto [graph, edges] = relationships(cube);
    size_t number_of_edges = edges.size();
    std::vector<Side> work;
    bool found = false;
    for (size_t code = 0, n = 1 << number_of_edges; code < n; ++code)
    {
        work = cube;
        for (size_t j = 0, bits = code; j < number_of_edges; ++j, bits >>= 1)
        {
            auto [l, r, matches] = edges[j];
            auto first = graphSplit(graph, l, r);
            Point p0 = work[l].corners[std::get<0>(matches[0])];
            Point p1 = work[l].corners[std::get<0>(matches[1])];
            for (size_t idx : first)
                work[idx].transform(p0, p1, (bits & 1));
        }
        if (valid(work))
        {
            found = true;
            break;
        }
    }
    if (found) return work;
    else return {};
}

