import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import copy

class Side:
    def __init__(self, x, y, label):
        self._x = [x, x + 1, x + 1, x]
        self._y = [y, y, y + 1, y + 1]
        self._z = [0] * 4
        self._label = label
    def coordinates(self):
        coord = []
        for i in range(4):
            coord.append((self._x[i], self._y[i], self._z[i]))
        return tuple(coord)
    def isNeighbor(self, other):
        closest = [[0] * 4 for x in range(4)]
        for i in range(4):
            for j in range(4):
                closest[i][j] = (abs(self._x[i] - other._x[j])
                              +  abs(self._y[i] - other._y[j])
                              +  abs(self._z[i] - other._z[j]))
        edges = []
        while True:
            closest_value = float('inf')
            closest_i = 0
            closest_j = 0
            for i in range(4):
                for j in range(4):
                    if closest[i][j] < closest_value:
                        closest_value = closest[i][j]
                        closest_i = i
                        closest_j = j
            if closest_value >= 0.05:
                break
            edges.append((closest_i, closest_j))
            for i in range(4):
                closest[closest_i][i] = float('inf')
                closest[i][closest_j] = float('inf')
        return edges if len(edges) == 2 else None
    def transform(self, p0, p1, clockwise):
        dp = list(map(lambda x, y: x - y, p0, p1))
        for i in range(4):
            self._x[i] -= p0[0]
            self._y[i] -= p0[1]
            self._z[i] -= p0[2]
        for i in range(3):
            if abs(p0[i] - p1[i]) > 1e-5:
                if i == 0:
                    # Clockwise  Anti-clockwise
                    #  1  0  0       1  0  0
                    #  0  0  1       0  0 -1
                    #  0 -1  0       0  1  0
                    self._y, self._z = self._z, self._y
                    if clockwise:
                        self._z = list(map(lambda v: -v, self._z))
                    else:
                        self._y = list(map(lambda v: -v, self._y))
                elif i == 1:
                    # Clockwise  Anti-clockwise
                    #  0  0  1       0  0 -1
                    #  0  1  0       0  1  0
                    # -1  0  0       1  0  0
                    self._x, self._z = self._z, self._x
                    if clockwise:
                        self._x = list(map(lambda v: -v, self._x))
                    else:
                        self._z = list(map(lambda v: -v, self._z))
                elif i == 2:
                    # Clockwise  Anti-clockwise
                    #  0  1  0       0 -1  0
                    # -1  0  0       1  0  0
                    #  0  0  1       0  0  1
                    self._x, self._y = self._y, self._x
                    if clockwise:
                        self._y = list(map(lambda v: -v, self._y))
                    else:
                        self._x = list(map(lambda v: -v, self._x))
                break
        for i in range(4):
            self._x[i] += p0[0]
            self._y[i] += p0[1]
            self._z[i] += p0[2]

def graphSplit(graph, edge):
    def split(start, stop):
        result = set()
        queue = [start]
        while len(queue) != 0:
            current = queue.pop(0)
            if current in result:
                continue
            result.add(current)
            for n in graph[current]:
                if n != stop and not n in result:
                    queue.append(n)
        return list(result)
    return split(edge[0], edge[1]), split(edge[1], edge[0])

def binary(number, number_of_bits):
    result = []
    while number:
        result.append(number % 2)
        number //= 2
    result.reverse()
    return [0] * (number_of_bits - len(result)) + result

def relationships(cube):
    graph = {}
    edges = []
    for i in range(len(cube)):
        for j in range(i + 1, len(cube)):
            edge = cube[i].isNeighbor(cube[j])
            if edge != None:
                if not i in graph: graph[i] = []
                if not j in graph: graph[j] = []
                graph[i].append(j)
                graph[j].append(i)
                edges.append((i, j, edge))
    return graph, edges

def valid(cube):
    graph, edges = relationships(cube)
    min_x, min_y, min_z = float( 'inf'), float( 'inf'), float( 'inf')
    max_x, max_y, max_z = float('-inf'), float('-inf'), float('-inf')
    for i in graph:
        if len(graph[i]) != 4:
            return False
        contrary = list(set(range(6)).difference(set(graph[i] + [i])))[0]
        if i in graph[contrary]:
            return False
        min_x = min(min_x, min(cube[i]._x))
        min_y = min(min_y, min(cube[i]._y))
        min_z = min(min_z, min(cube[i]._z))
        max_x = max(max_x, max(cube[i]._x))
        max_y = max(max_y, max(cube[i]._y))
        max_z = max(max_z, max(cube[i]._z))
    if (abs(1 - max_x + min_x) > 1e-5) or (abs(1 - max_y + min_y) > 1e-5) or (abs(1 - max_z + min_z) > 1e-5):
        return False
    for i in range(len(cube)):
        cube[i]._x = list(map(lambda x: x - min_x, cube[i]._x))
        cube[i]._y = list(map(lambda x: x - min_y, cube[i]._y))
        cube[i]._z = list(map(lambda x: x - min_z, cube[i]._z))
    return True

if __name__ == '__main__':
    model = 0
    if model == 0:
        model_text = ['  A ',
                      'BCD ',
                      '  EF']
    elif model == 1:
        model_text = [' AB',
                      ' C ',
                      'DE ',
                      'F  ']
    elif model == 2:
        model_text = [' A',
                      ' B',
                      'CD',
                      'E ',
                      'F ']
    number_of_rows = len(model_text)
    number_of_columns = len(model_text[0])
    colors = ['red', 'blue', 'green', 'black', 'darkorange', 'magenta', 'greenyellow']
    cube = []
    for h in range(number_of_rows):
        for w in range(number_of_columns):
            if model_text[h][w] != ' ':
                cube.append(Side(w, h, model_text[h][w]))
    graph, edges = relationships(cube)
    number_of_edges = len(edges)
    for i in range(2 ** number_of_edges):
        code = binary(i, number_of_edges)
        work = copy.deepcopy(cube)
        for j in range(len(edges)):
            l, r, matches = edges[j]
            first, second = graphSplit(graph, (l, r))
            p0 = (work[l]._x[matches[0][0]], work[l]._y[matches[0][0]], work[l]._z[matches[0][0]])
            p1 = (work[l]._x[matches[1][0]], work[l]._y[matches[1][0]], work[l]._z[matches[1][0]])
            for idx in first:
                work[idx].transform(p0, p1, code[j])
        if valid(work):
            print(code)
            print(sum(map(lambda x: x[1] * 2**x[0], enumerate(code))))
            break
    ##aux = copy.deepcopy(cube)
    ##for j in range(len(edges)):
    ##    l, r, matches = edges[j]
    ##    first, second = graphSplit(graph, (l, r))
    ##    p0 = (aux[l]._x[matches[0][0]], aux[l]._y[matches[0][0]], aux[l]._z[matches[0][0]])
    ##    p1 = (aux[l]._x[matches[1][0]], aux[l]._y[matches[1][0]], aux[l]._z[matches[1][0]])
    ##    for idx in first:
    ##        aux[idx].transform(p0, p1, code[j])
    ##    fig_debug = plt.figure()
    ##    ax_debug = fig_debug.add_subplot(projection='3d')
    ##    for face_index in range(len(aux)):
    ##        ax_debug.add_collection(Poly3DCollection([aux[face_index].coordinates()], facecolors=colors[face_index]))
    ##    ax_debug.set_xlim([0, 5])
    ##    ax_debug.set_ylim([0, 5])
    ##    ax_debug.set_zlim([0, 3])
    ##    plt.show()
    # What are the relationships between the unrolled sides and the rolled sides.
    # To what side each segment is related to and what is the direction of the relation.
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    for face_index in range(len(cube)):
        ax.add_collection(Poly3DCollection([cube[face_index].coordinates()], facecolors=colors[face_index]))
    for face_index in range(len(work)):
        ax.add_collection(Poly3DCollection([work[face_index].coordinates()], facecolors=colors[face_index]))
    ax.set_xlim([0, 5])
    ax.set_ylim([0, 5])
    ax.set_zlim([0, 3])
    plt.show()



