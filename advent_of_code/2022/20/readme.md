# [Day 20: Grove Positioning System](https://adventofcode.com/2022/day/20)

#### Problem A

You are given a list of numbers. These numbers are put inside a circular queue and moved according to their value. For example, element `1` of list `[1, 2, -3, 3, -2, 0, 4]` move forward one position: `[2, 1, -3, 3, -2, 0, 4]`. In the previous list `-3` move three positions backwards: `[2, 1, 3, -2, 0, -3, 4]`. You have to displace all the elements in the circular queue **in the original order** that they where added inside. For example, you first move `1`, then `2`, after `-3`, next `3`, ... and finally `4`, regardless of their order in the queue at a given moment:

```
Original: [ 1,  2, -3,  3, -2,  0,  4]
 Move  1: [ 2,  1, -3,  3, -2,  0,  4]
 Move  2: [ 1, -3,  2,  3, -2,  0,  4]
 Move -3: [ 1,  2,  3, -2, -3,  0,  4]
 Move  3: [ 1,  2, -2, -3,  0,  3,  4]
 Move -2: [ 1,  2, -3,  0,  3,  4, -2]
 Move  0: [ 1,  2, -3,  0,  3,  4, -2]
 Move  4: [ 1,  2, -3,  4,  0,  3, -2]
```

After a cycle, **return the sum of the** `1000th`, `2000th` **and** `3000th` **elements in the circular queue.**

#### Problem B

Same as before, but now list elements are first multiplied by `811589153` and then you have to run 10 cycles. Numbers are always processed in the order that they appear in the original list. Once the circular queue has been scrambled, **return the sum of the** `1000th`, `2000th` **and** `3000th` **elements in the circular queue.**

