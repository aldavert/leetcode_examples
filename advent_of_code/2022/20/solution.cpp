#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <queue>
#include <limits>
#include <deque>
#include <set>
#include <fmt/core.h>

std::ostream& operator<<(std::ostream &out, const std::vector<long> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}
std::ostream& operator<<(std::ostream &out, const std::deque<const long *> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << *v;
    }
    out << '}';
    return out;
}

//------------------------------------------------------------------------------

long decipher(std::vector<long> numbers, size_t iterations)
{
    const size_t n = numbers.size();
    std::deque<const long *> queue(n);
    const long * zero = nullptr;
    for (size_t i = 0; i < n; ++i)
    {
        if (numbers[i] == 0) zero = &numbers[i];
        queue[i] = &numbers[i];
    }
    auto mod = [&](long value) -> size_t
    {
        return static_cast<size_t>(std::abs(value) % (n - 1));
    };
    for (size_t iter = 0; iter < iterations; ++iter)
    {
        for (size_t i = 0; i < n; ++i)
        {
            if (numbers[i] > 0)
            {
                auto position = std::find(queue.begin(), queue.end(), &numbers[i]);
                for (size_t m = 0, jumps = mod(numbers[i]); m < jumps; ++m)
                {
                    if (std::next(position) == queue.end())
                    {
                        queue.push_front(queue.back());
                        queue.pop_back();
                        position = queue.begin();
                    }
                    std::swap(*position, *std::next(position));
                    position = std::next(position);
                    if (std::next(position) == queue.end())
                    {
                        queue.push_front(queue.back());
                        queue.pop_back();
                        position = queue.begin();
                    }
                }
            }
            else if (numbers[i] < 0)
            {
                auto position = std::find(queue.begin(), queue.end(), &numbers[i]);
                for (size_t m = 0, jumps = mod(numbers[i]); m < jumps; ++m)
                {
                    if (position == queue.begin())
                    {
                        queue.push_back(queue.front());
                        queue.pop_front();
                        position = std::prev(queue.end());
                    }
                    std::swap(*position, *std::prev(position));
                    position = std::prev(position);
                    if (position == queue.begin())
                    {
                        queue.push_back(queue.front());
                        queue.pop_front();
                        position = std::prev(queue.end());
                    }
                }
            }
        }
    }
    size_t position = std::distance(queue.begin(), std::find(queue.begin(), queue.end(), zero));
    return *queue[(position + 1000) % n]
         + *queue[(position + 2000) % n]
         + *queue[(position + 3000) % n];
}

//------------------------------------------------------------------------------

int main(int argc, char * * argv)
{
    std::ifstream file((argc == 1)?"data_test.txt":argv[1]);
    std::string line;
    std::vector<long> numbers;
    while (std::getline(file, line))
        numbers.push_back(std::atoi(line.c_str()));
    std::cout << "[TEST A] Code: " << decipher(numbers, 1) << '\n';
    for (auto &number : numbers)
        number *= 811589153;
    std::cout << "[TEST B] Code: " << decipher(numbers, 10) << '\n';
    
    return EXIT_SUCCESS;
}


