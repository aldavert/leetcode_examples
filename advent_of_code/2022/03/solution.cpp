#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <cctype>
#include <cstring>

int main(int /*argc*/, char * * /*argv*/)
{
    std::ifstream file("data.txt");
    std::string line;
    int result = 0, file_line = 0, result_group = 0;
    int group_histogram[3][52];
    while (std::getline(file, line))
    {
        // -[ PART ONE ]--------------------------------------------------------
        int histogram[2][52] = {};
        const size_t half = line.size() / 2, n = line.size();
        for (size_t i = 0; i < n; ++i)
        {
            int position = (std::tolower(line[i]) - 'a') + 26 * (line[i] <= 'Z');
            ++histogram[i >= half][position];
        }
        for (int i = 0; i < 52; ++i)
            if ((histogram[0][i]) && (histogram[1][i]))
                result += i + 1;
        // -[ PART TWO ]--------------------------------------------------------
        if (file_line % 3 == 0)
            std::memset(group_histogram, 0, sizeof(group_histogram));
        for (size_t i = 0; i < n; ++i)
            ++group_histogram[file_line % 3][position];
        if (file_line % 3 == 2)
        {
            for (int i = 0; i < 52; ++i)
                if ((group_histogram[0][i]) && (group_histogram[1][i]) && (group_histogram[2][i]))
                    result_group += i + 1;
        }
        ++file_line;
    }
    std::cout << "Priority sum: " << result << '\n';
    std::cout << "Group priority sum: " << result_group << '\n';
    file.close();
}

