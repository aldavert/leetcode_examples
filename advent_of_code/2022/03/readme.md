# [Day 3: Rucksack Reorganization](https://adventofcode.com/2022/day/3)

#### Problem A

The puzzle input is a set of strings where the first and second half of the string have a single character in common. **Find these characters and convert then into** ***priority value*** **by:**

- Lowercase item types `a` through `z` have priorities `1` through `26`.
- Uppercase item types `A` through `Z` have priorities `27` through `52`.

**Return the sum of all priority values.**

#### Problem B

Now, group the lines into groups of three and find the **single character** that all three strings have in common. **Convert this characters into a priority** (as in *Problem A*) **and return the sum of all the priorities.**

