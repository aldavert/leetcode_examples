# [Day 17: Two Steps Forward](https://adventofcode.com/2016/day/17)

#### Problem A

You are in a maze of `4 * 4` rooms. You start at the top-left corner and you want to reach the bottom-right corner. The door are locked or opened based on the path that you are following.

Initially, you have a starting state string. As you move around the maze, you concatenate the direction that you have taken to the state string (`U` for up, `D` for down, `L` for left and `R` for right). This state string determines which doors are open or close at the current room.

The state of the room is obtained by computing the MD5 has on the state string and getting the four first characters. These characters correspond to the `up`, `down`, `left` and `right` doors. If the hash hexadecimal value is lower than `b`, then the door is closed. Otherwise the door is opened.

What is the shortest path to reach the exit?

#### Problem B

What is the longest path to reach the exit?

**Note:** The first time you reach the bottom-right cell, you have to stop. This cell can be entered, but cannot be exited.

