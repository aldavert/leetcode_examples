#include <iostream>
#include <queue>
#include <cstring>

class MD5
{
    static constexpr uint32_t blocksize = 64;
    static constexpr uint32_t S11 = 7;
    static constexpr uint32_t S12 = 12;
    static constexpr uint32_t S13 = 17;
    static constexpr uint32_t S14 = 22;
    static constexpr uint32_t S21 = 5;
    static constexpr uint32_t S22 = 9;
    static constexpr uint32_t S23 = 14;
    static constexpr uint32_t S24 = 20;
    static constexpr uint32_t S31 = 4;
    static constexpr uint32_t S32 = 11;
    static constexpr uint32_t S33 = 16;
    static constexpr uint32_t S34 = 23;
    static constexpr uint32_t S41 = 6;
    static constexpr uint32_t S42 = 10;
    static constexpr uint32_t S43 = 15;
    static constexpr uint32_t S44 = 21;
    uint8_t buffer[blocksize];
    uint32_t count[2];
    uint32_t state[4];
    uint8_t digest[16];
public:
    MD5(const std::string &text)
    {
        count[0] = count[1] = 0;
        state[0] = 0x67452301;
        state[1] = 0xefcdab89;
        state[2] = 0x98badcfe;
        state[3] = 0x10325476;
        update(reinterpret_cast<const unsigned char *>(text.c_str()),
               static_cast<uint32_t>(text.length()));
        finalize();
    }
    void update(const unsigned char * buf, uint32_t length);
    void finalize();
    std::string hexdigest() const
    {
        char buf[33] = {};
        for (int i = 0; i < 16; ++i)
        sprintf(buf + i * 2, "%02x", digest[i]);
        return std::string(buf);
    }
private:
    void init(void);
    void transform(const uint8_t block[blocksize]);
    inline void decode(uint32_t output[], const uint8_t input[], uint32_t len) const
    {
        for (unsigned int i = 0, j = 0; j < len; ++i, j += 4)
            output[i] =  static_cast<uint32_t>(input[j    ])
                      | (static_cast<uint32_t>(input[j + 1]) << 8)
                      | (static_cast<uint32_t>(input[j + 2]) << 16)
                      | (static_cast<uint32_t>(input[j + 3]) << 24);
    }
    inline void encode(uint8_t output[], const uint32_t input[], uint32_t len) const
    {
        for (uint32_t i = 0, j = 0; j < len; ++i, j += 4)
        {
            output[j    ] = static_cast<uint8_t>(input[i]       & 0xff);
            output[j + 1] = static_cast<uint8_t>(input[i] >>  8 & 0xff);
            output[j + 2] = static_cast<uint8_t>(input[i] >> 16 & 0xff);
            output[j + 3] = static_cast<uint8_t>(input[i] >> 24 & 0xff);
        }
    }
    inline uint32_t F(uint32_t x, uint32_t y, uint32_t z) const { return (x & y) | (~x & z); }
    inline uint32_t G(uint32_t x, uint32_t y, uint32_t z) const { return (x & z) | (y & ~z); }
    inline uint32_t H(uint32_t x, uint32_t y, uint32_t z) const { return x ^ y ^ z; }
    inline uint32_t I(uint32_t x, uint32_t y, uint32_t z) const { return y ^ (x | ~z); }
    inline uint32_t rotate_left(uint32_t x, int n) const { return (x << n) | (x >> (32 - n)); }
    inline void FF(uint32_t &a, uint32_t b, uint32_t c, uint32_t d, uint32_t x, uint32_t s, uint32_t ac) const
    {
        a = rotate_left(a + F(b, c, d) + x + ac, s) + b;
    }
    inline void GG(uint32_t &a, uint32_t b, uint32_t c, uint32_t d, uint32_t x, uint32_t s, uint32_t ac) const
    {
        a = rotate_left(a + G(b, c, d) + x + ac, s) + b;
    }
    inline void HH(uint32_t &a, uint32_t b, uint32_t c, uint32_t d, uint32_t x, uint32_t s, uint32_t ac) const
    {
        a = rotate_left(a + H(b, c, d) + x + ac, s) + b;
    }
    inline void II(uint32_t &a, uint32_t b, uint32_t c, uint32_t d, uint32_t x, uint32_t s, uint32_t ac) const
    {
        a = rotate_left(a + I(b, c, d) + x + ac, s) + b;
    }
};

void MD5::transform(const uint8_t block[blocksize])
{
    uint32_t a = state[0], b = state[1], c = state[2], d = state[3], x[16];
    decode (x, block, blocksize);
    
    // Round 1
    FF (a, b, c, d, x[ 0], S11, 0xd76aa478); /* 1 */
    FF (d, a, b, c, x[ 1], S12, 0xe8c7b756); /* 2 */
    FF (c, d, a, b, x[ 2], S13, 0x242070db); /* 3 */
    FF (b, c, d, a, x[ 3], S14, 0xc1bdceee); /* 4 */
    FF (a, b, c, d, x[ 4], S11, 0xf57c0faf); /* 5 */
    FF (d, a, b, c, x[ 5], S12, 0x4787c62a); /* 6 */
    FF (c, d, a, b, x[ 6], S13, 0xa8304613); /* 7 */
    FF (b, c, d, a, x[ 7], S14, 0xfd469501); /* 8 */
    FF (a, b, c, d, x[ 8], S11, 0x698098d8); /* 9 */
    FF (d, a, b, c, x[ 9], S12, 0x8b44f7af); /* 10 */
    FF (c, d, a, b, x[10], S13, 0xffff5bb1); /* 11 */
    FF (b, c, d, a, x[11], S14, 0x895cd7be); /* 12 */
    FF (a, b, c, d, x[12], S11, 0x6b901122); /* 13 */
    FF (d, a, b, c, x[13], S12, 0xfd987193); /* 14 */
    FF (c, d, a, b, x[14], S13, 0xa679438e); /* 15 */
    FF (b, c, d, a, x[15], S14, 0x49b40821); /* 16 */
    // Round 2
    GG (a, b, c, d, x[ 1], S21, 0xf61e2562); /* 17 */
    GG (d, a, b, c, x[ 6], S22, 0xc040b340); /* 18 */
    GG (c, d, a, b, x[11], S23, 0x265e5a51); /* 19 */
    GG (b, c, d, a, x[ 0], S24, 0xe9b6c7aa); /* 20 */
    GG (a, b, c, d, x[ 5], S21, 0xd62f105d); /* 21 */
    GG (d, a, b, c, x[10], S22,  0x2441453); /* 22 */
    GG (c, d, a, b, x[15], S23, 0xd8a1e681); /* 23 */
    GG (b, c, d, a, x[ 4], S24, 0xe7d3fbc8); /* 24 */
    GG (a, b, c, d, x[ 9], S21, 0x21e1cde6); /* 25 */
    GG (d, a, b, c, x[14], S22, 0xc33707d6); /* 26 */
    GG (c, d, a, b, x[ 3], S23, 0xf4d50d87); /* 27 */
    GG (b, c, d, a, x[ 8], S24, 0x455a14ed); /* 28 */
    GG (a, b, c, d, x[13], S21, 0xa9e3e905); /* 29 */
    GG (d, a, b, c, x[ 2], S22, 0xfcefa3f8); /* 30 */
    GG (c, d, a, b, x[ 7], S23, 0x676f02d9); /* 31 */
    GG (b, c, d, a, x[12], S24, 0x8d2a4c8a); /* 32 */
    // Round 3
    HH (a, b, c, d, x[ 5], S31, 0xfffa3942); /* 33 */
    HH (d, a, b, c, x[ 8], S32, 0x8771f681); /* 34 */
    HH (c, d, a, b, x[11], S33, 0x6d9d6122); /* 35 */
    HH (b, c, d, a, x[14], S34, 0xfde5380c); /* 36 */
    HH (a, b, c, d, x[ 1], S31, 0xa4beea44); /* 37 */
    HH (d, a, b, c, x[ 4], S32, 0x4bdecfa9); /* 38 */
    HH (c, d, a, b, x[ 7], S33, 0xf6bb4b60); /* 39 */
    HH (b, c, d, a, x[10], S34, 0xbebfbc70); /* 40 */
    HH (a, b, c, d, x[13], S31, 0x289b7ec6); /* 41 */
    HH (d, a, b, c, x[ 0], S32, 0xeaa127fa); /* 42 */
    HH (c, d, a, b, x[ 3], S33, 0xd4ef3085); /* 43 */
    HH (b, c, d, a, x[ 6], S34,  0x4881d05); /* 44 */
    HH (a, b, c, d, x[ 9], S31, 0xd9d4d039); /* 45 */
    HH (d, a, b, c, x[12], S32, 0xe6db99e5); /* 46 */
    HH (c, d, a, b, x[15], S33, 0x1fa27cf8); /* 47 */
    HH (b, c, d, a, x[ 2], S34, 0xc4ac5665); /* 48 */
    // Round 4
    II (a, b, c, d, x[ 0], S41, 0xf4292244); /* 49 */
    II (d, a, b, c, x[ 7], S42, 0x432aff97); /* 50 */
    II (c, d, a, b, x[14], S43, 0xab9423a7); /* 51 */
    II (b, c, d, a, x[ 5], S44, 0xfc93a039); /* 52 */
    II (a, b, c, d, x[12], S41, 0x655b59c3); /* 53 */
    II (d, a, b, c, x[ 3], S42, 0x8f0ccc92); /* 54 */
    II (c, d, a, b, x[10], S43, 0xffeff47d); /* 55 */
    II (b, c, d, a, x[ 1], S44, 0x85845dd1); /* 56 */
    II (a, b, c, d, x[ 8], S41, 0x6fa87e4f); /* 57 */
    II (d, a, b, c, x[15], S42, 0xfe2ce6e0); /* 58 */
    II (c, d, a, b, x[ 6], S43, 0xa3014314); /* 59 */
    II (b, c, d, a, x[13], S44, 0x4e0811a1); /* 60 */
    II (a, b, c, d, x[ 4], S41, 0xf7537e82); /* 61 */
    II (d, a, b, c, x[11], S42, 0xbd3af235); /* 62 */
    II (c, d, a, b, x[ 2], S43, 0x2ad7d2bb); /* 63 */
    II (b, c, d, a, x[ 9], S44, 0xeb86d391); /* 64 */
    
    state[0] += a;
    state[1] += b;
    state[2] += c;
    state[3] += d;
    std::memset(x, 0, sizeof(x));
}

void MD5::update(const unsigned char input[], uint32_t length)
{
    uint32_t index = count[0] / 8 % blocksize;

    if ((count[0] += (length << 3)) < (length << 3)) ++count[1];
    count[1] += (length >> 29);

    uint32_t firstpart = 64 - index;
    uint32_t i;
    if (length >= firstpart)
    {
        memcpy(&buffer[index], input, firstpart);
        transform(buffer);
        for (i = firstpart; i + blocksize <= length; i += blocksize)
        transform(&input[i]);
        index = 0;
    }
    else i = 0;
    memcpy(&buffer[index], &input[i], length-i);
}

void MD5::finalize()
{
    static unsigned char padding[64] = {
        0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };
    unsigned char bits[8];
    encode(bits, count, 8);
    uint32_t index = count[0] / 8 % 64;
    uint32_t padLen = (index < 56) ? (56 - index) : (120 - index);
    update(padding, padLen);
    update(bits, 8);
    encode(digest, state, 16);
}
 
std::string md5(const std::string str)
{
    MD5 md5 = MD5(str);
    return md5.hexdigest();
}

std::string shortestPath(std::string state)
{
    constexpr int dirs[] = { -1, 0, 1, 0, -1 };
    constexpr char dirs_text[] = "LDRU";
    constexpr int reshuffle[] = { 2, 1, 3, 0 };
    std::queue<std::tuple<int, int, std::string> > q;
    q.push({0, 0, ""});
    while (!q.empty())
    {
        for (size_t i = 0, n = q.size(); i < n; ++i)
        {
            auto [x, y, current_path] = q.front();
            q.pop();
            if ((x == 3) && (y == 3))
                return current_path;
            std::string h = md5(state + current_path);
            for (size_t k = 0; k < 4; ++k)
            {
                char hex_value = h[reshuffle[k]];
                if (((hex_value >= '0') && (hex_value <= '9')) || (hex_value == 'a'))
                    continue;
                int nx = x + dirs[k], ny = y + dirs[k + 1];
                if ((nx < 0) || (nx >= 4) || (ny < 0) || (ny >= 4))
                    continue;
                q.push({nx, ny, current_path + dirs_text[k]});
            }
        }
    }
    return "";
}

int longestPath(std::string state)
{
    constexpr int dirs[] = { -1, 0, 1, 0, -1 };
    constexpr char dirs_text[] = "LDRU";
    constexpr int reshuffle[] = { 2, 1, 3, 0 };
    std::queue<std::tuple<int, int, std::string> > q;
    q.push({0, 0, ""});
    size_t longest_path = 0;
    while (!q.empty())
    {
        for (size_t i = 0, n = q.size(); i < n; ++i)
        {
            auto [x, y, current_path] = q.front();
            q.pop();
            if ((x == 3) && (y == 3))
            {
                longest_path = std::max(longest_path, current_path.size());
                continue;
            }
            std::string h = md5(state + current_path);
            for (size_t k = 0; k < 4; ++k)
            {
                char hex_value = h[reshuffle[k]];
                if (((hex_value >= '0') && (hex_value <= '9')) || (hex_value == 'a'))
                    continue;
                int nx = x + dirs[k], ny = y + dirs[k + 1];
                if ((nx < 0) || (nx >= 4) || (ny < 0) || (ny >= 4))
                    continue;
                q.push({nx, ny, current_path + dirs_text[k]});
            }
        }
    }
    return static_cast<int>(longest_path);
}

int main(int argc, char * * argv)
{
    std::string input = "qljzarfv";
    std::string expected_result = "";
    int expected_length = 0;
    if (argc == 2) input = argv[1];
    else if ((argc == 3) && (std::string(argv[1]) == "test"))
    {
        switch (std::stoi(argv[2]))
        {
        case 1:
            input = "ihgpwlah";
            expected_result = "DDRRRD";
            expected_length = 370;
            break;
        case 2:
            input = "kglvqrro";
            expected_result = "DDUDRLRRUDRD";
            expected_length = 492;
            break;
        case 3:
            input = "ulqzkmiv";
            expected_result = "DRURDRUDDLLDLUURRDULRLDUUDDDRR";
            expected_length = 830;
            break;
        default:
            std::cerr << "[ERROR] Test case doesn't exist.\n";
            return EXIT_FAILURE;
        }
    }
    std::string route = shortestPath(input);
    if (expected_result != "")
    {
        std::cout << "Problem A: " << route << ' ';
        if (route == expected_result) std::cout << "[CORRECT]\n";
        else std::cout << "[FAILURE]\n";
    }
    else std::cout << "Problem A: " << route << '\n';
    int longest_path = longestPath(input);
    if (expected_result != "")
    {
        std::cout << "Problem B: " << longest_path << ' ';
        if (longest_path == expected_length) std::cout << "[CORRECT]\n";
        else std::cout << "[FAILURE]\n";
    }
    else std::cout << "Problem A: " << longest_path << '\n';
    return EXIT_SUCCESS;
}

