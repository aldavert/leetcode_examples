#include <iostream>
#include <vector>
#include <string>

int countFreeSpace(std::string row, int number_of_rows)
{
    constexpr char cells[2] = {'.', '^'};
    const int n = static_cast<int>(row.size());
    int result = static_cast<int>(std::count(row.begin(), row.end(), '.'));
    row += ".";
    for (int r = 1; r < number_of_rows; ++r)
    {
        char top = row[0] == '^';
        for (int c = 0; c < n; ++c)
        {
            top = ((top << 1) | (row[c + 1] == '^')) & 0x7;
            row[c] = cells[(top == 0b100) || (top == 0b110)
                        || (top == 0b011) || (top == 0b001)];
            result += row[c] == '.';
        }
    }
    return result;
}

int main(int argc, char * * argv)
{
    std::string initial_row = "......^.^^.....^^^^^^^^^...^.^..^^.^^^..^.^..^.^"
                              "^^.^^^^..^^.^.^.....^^^^^..^..^^^..^^.^.^..^^..^"
                              "^^..";
    int number_of_rows = 40;
    int expected_result = -1;
    if (argc == 3)
    {
        if (std::string(argv[1]) == "test")
        {
            switch (std::stoi(argv[2]))
            {
            case 0:
                break;
            case 1:
                number_of_rows = 400'000;
                break;
            case 2:
                initial_row = "..^^.";
                number_of_rows = 3;
                expected_result = 6;
                break;
            case 3:
                initial_row = ".^^.^.^^^^";
                number_of_rows = 10;
                expected_result = 38;
                break;
            default:
                std::cerr << "[ERROR] Unexpected test index.\n";
                return EXIT_FAILURE;
            }
        }
        else
        {
            initial_row = argv[1];
            number_of_rows = std::stoi(argv[2]);
        }
    }
    int problem_a = countFreeSpace(initial_row, number_of_rows);
    if (expected_result != -1)
    {
        std::cout << "Problem A: " << problem_a << ' ';
        if (expected_result == problem_a) std::cout << "[CORRECT]\n";
        else std::cout << "[FAILURE]\n";
    }
    else std::cout << "Problem A: " << problem_a << '\n';
    
    return EXIT_SUCCESS;
}

