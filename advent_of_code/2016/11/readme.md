# [Day 11: Radioisotope Thermoelectric Generators](https://adventofcode.com/2016/day/11)

#### Problem A

You are given a tower with four floors and an elevator. You can use the elevator to move one or two items between the floors. The items that you can move are **heaters** and **shields**. The **heater** can be neutralized by their corresponding **shield**, otherwise they damage the other elements present in the floor.

Given an specific configuration, what is the minimum number of steps that you need to take to move all the items to the top floor?

**Note:** You start at the bottom floor.


