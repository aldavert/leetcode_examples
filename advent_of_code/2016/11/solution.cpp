#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <string>
#include <unordered_map>
#include <unordered_set>

int main(int argc, char * * argv)
{
    constexpr int number_of_levels = 4;
    std::set<std::string> generator[number_of_levels],
                          microchip[number_of_levels];
    auto addItem = [&](int floor_number,
                       std::string line,
                       std::string::size_type search,
                       std::string::size_type next) -> void
    {
        search = line.find(' ', search) + 1;
        std::string item_info = line.substr(search, next - search);
        auto middle = item_info.find(' ');
        std::string element = item_info.substr(0, middle),
                    type = item_info.substr(middle + 1);
        if (type == "generator")
            generator[floor_number].insert(element);
        else
        {
            element = element.substr(0, element.find('-'));
            microchip[floor_number].insert(element);
        }
    };
    
    // LOAD DATA ...............................................................
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        auto search = line.find("floor");
        if (search == std::string::npos) continue;
        if (line.substr(search) == "floor contains nothing relevant.") continue;
        auto next = line.rfind(' ', search - 2);
        std::string floor_text = line.substr(next + 1, search - next - 2);
        int floor_number;
        if      (floor_text == "first" ) floor_number = 0;
        else if (floor_text == "second") floor_number = 1;
        else if (floor_text == "third" ) floor_number = 2;
        else if (floor_text == "fourth") floor_number = 3;
        else
        {
            std::cerr << "[ERROR] Unexpected floor number: " << floor_text << '\n';
            return EXIT_FAILURE;
        }
        search += 15;
        next = line.find(',', search);
        while (next != std::string::npos)
        {
            if (next > search)
                addItem(floor_number, line, search, next);
            search = next + 2;
            next = line.find(',', search);
        }
        next = line.find("and", search);
        if (next != std::string::npos)
        {
            if (next > search)
                addItem(floor_number, line, search, next);
            search = next + 4;
        }
        next = line.find('.', search);
        if (next > search)
            addItem(floor_number, line, search, next);
    }
    file.close();
    
    // FORMAT DATA .............................................................
    std::map<std::string, std::pair<std::string, size_t> > element_order;
    for (int i = number_of_levels - 1; i >= 0; --i)
    {
        for (std::string element : generator[i])
        {
            if (element_order.find(element) != element_order.end())
                continue;
            std::string short_name;
            short_name += static_cast<char>(element[0] - 'a' + 'A');
            short_name += static_cast<char>(element[1] - 'a' + 'A');
            element_order[element] = {short_name, 0};
        }
        for (std::string element : microchip[i])
        {
            if (element_order.find(element) != element_order.end())
                continue;
            std::string short_name;
            short_name += static_cast<char>(element[0] - 'a' + 'A');
            short_name += static_cast<char>(element[1] - 'a' + 'A');
            element_order[element] = {short_name, 0};
        }
    }
    std::vector<std::pair<std::string, std::string> > order_element;
    for (auto &[element, short_index] : element_order)
    {
        short_index.second = order_element.size();
        order_element.push_back({element, short_index.first});
    }
    std::vector<int> element_floor(order_element.size() * 2);
    for (int i = number_of_levels - 1; i >= 0; --i)
    {
        for (std::string element : generator[i])
            element_floor[2 * element_order[element].second] = i;
        for (std::string element : microchip[i])
            element_floor[2 * element_order[element].second + 1] = i;
    }
    const size_t number_of_elements = element_floor.size();
    
    auto displayState = [&](long state) -> void
    {
        for (size_t i = 0; i < number_of_elements; ++i)
            std::cout << ' ' << i << ((i < 10)?"  ":" ");
        std::cout << '\n';
        for (int level = 3; level >= 0; --level)
        {
            long current = state;
            for (size_t i = 0; i < number_of_elements; ++i)
            {
                bool present = (current & 0x3) == level;
                current = current >> 2;
                if (present)
                    std::cout << order_element[(number_of_elements - i - 1) / 2].second
                              << (((i & 1) == 0)?'C':'G') << ' ';
                else std::cout << "... ";
            }
            std::cout << '\n';
        }
    };
    
    std::unordered_set<long> memo;
    std::queue<std::pair<long, int> > q;
    long goal_state = 0; // This is the start state.
    for (size_t i = 0; i < number_of_elements; ++i)
        goal_state = (goal_state << 2) | (element_floor[i] & 0x3);
    q.push({goal_state, 0});
    goal_state = number_of_levels - 1;
    for (size_t i = 0; i < number_of_elements; ++i)
        goal_state = (goal_state << 2) | (number_of_levels - 1);
    
    auto pushElement = [](long state, size_t element, int inc) -> long
    {
        long modified = (((state >> (2 * element)) & 0x3) + inc) << (2 * element);
        return (state & (~(0x3 << (2 * element)))) | modified;
    };
    auto moveFloor = [&](long state, int inc) -> long
    {
        long floor = ((state >> (2 * number_of_elements)) & 0x3) + inc;
        return (state & (~(0x3 << (2 * number_of_elements))))
             | (floor << (2 * number_of_elements));
    };
    auto isValid = [&](long state) -> bool
    {
        for (int level = 0; level < number_of_levels; ++level)
        {
            long current_state = state;
            int generators = 0, unpaired = 0;
            for (size_t j = 0; j < number_of_elements; j += 2)
            {
                bool has_microchip = (current_state & 0x3) == level;
                current_state = current_state >> 2;
                bool has_generator = (current_state & 0x3) == level;
                current_state = current_state >> 2;
                generators += has_generator;
                unpaired += has_microchip && !has_generator;
            }
            if ((generators > 0) && (unpaired > 0)) return false;
        }
        return true;
    };
    auto hash = [&](long state) -> long
    {
        long n_elements[4] = {}, n_generators[4] = {};
        for (size_t j = 0; j < number_of_elements; j += 2)
        {
            ++n_elements[state & 0x3];
            state = state >> 2;
            ++n_elements[state & 0x3];
            ++n_generators[state & 0x3];
            state = state >> 2;
        }
        long result = (state & 0x3);
        for (int i = 0; i < 4; ++i)
        {
            result = (result << 6) | n_elements[i];
            result = (result << 6) | n_generators[i];
        }
        return result;
    };
    int number_of_calls = 0;
    std::cout << "Initial state:\n";
    displayState(q.front().first);
    while (!q.empty())
    {
        auto [current_state, n_moves] = q.front();
        q.pop();
        ++number_of_calls;
        if (memo.find(hash(current_state)) != memo.end()) continue;
        memo.insert(hash(current_state));
        if (current_state == goal_state)
        {
            std::cout << "Final state:\n";
            displayState(current_state);
            std::cout << "Number of Moves: " << n_moves << " (in "
                      << number_of_calls << " calls).\n";
            break;
        }
        long level = current_state >> (number_of_elements * 2);
        for (size_t i = 0; i < number_of_elements; ++i)
        {
            if (((current_state >> (2 * i)) & 0x3) != level)
                continue;
            if (level < 3)
            {
                long push_one = moveFloor(pushElement(current_state, i, 1), 1);
                for (size_t j = i + 1; j < number_of_elements; ++j)
                {
                    if (((current_state >> (2 * j)) & 0x3) != level)
                        continue;
                    long push_two = pushElement(push_one, j, 1);
                    if (isValid(push_two) && (memo.find(hash(push_two)) == memo.end()))
                        q.push({push_two, n_moves + 1});
                }
                if (isValid(push_one) && (memo.find(hash(push_one)) == memo.end()))
                    q.push({push_one, n_moves + 1});
            }
            if (level > 0)
            {
                long push_one = moveFloor(pushElement(current_state, i, -1), -1);
                for (size_t j = i + 1; j < number_of_elements; ++j)
                {
                    if (((current_state >> (2 * j)) & 0x3) != level) continue;
                    long push_two = pushElement(push_one, j, -1);
                    if (isValid(push_two) && (memo.find(hash(push_two)) == memo.end()))
                        q.push({push_two, n_moves + 1});
                }
                if (isValid(push_one) && (memo.find(hash(push_one)) == memo.end()))
                    q.push({push_one, n_moves + 1});
            }
        }
    }
    
    return EXIT_SUCCESS;
}

