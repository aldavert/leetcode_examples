#include <iostream>
#include <fstream>
#include <vector>
#include <string>

long expansionSize(std::string line, bool is_recursive)
{
    long length = static_cast<long>(line.size());
    for (auto begin = line.find('(');
         begin != std::string::npos;
         begin = line.find('(', begin + 1))
    {
        auto middle = line.find('x', begin + 1);
        auto end = line.find(')', middle + 1);
        if ((middle == std::string::npos)
        ||  (end == std::string::npos)) break;
        std::string text_of_characters =
            line.substr(begin + 1, middle - begin - 1),
                    text_of_repetitions =
            line.substr(middle + 1, end - middle - 1);
        bool valid = true;
        for (char symbol : text_of_characters)
            valid = valid && std::isdigit(symbol);
        for (char symbol : text_of_repetitions)
            valid = valid && std::isdigit(symbol);
        if (!valid) continue;
        long number_of_characters = std::stoi(text_of_characters),
             number_of_repetitions = std::stoi(text_of_repetitions);
        long marker_size = static_cast<long>(end - begin + 1);
        if (!is_recursive)
            length += number_of_characters * (number_of_repetitions - 1)
                   -  marker_size;
        else
        {
            length += expansionSize(line.substr(end + 1, number_of_characters), true)
                                  * number_of_repetitions
                    - marker_size - number_of_characters;
        }
        begin = end + number_of_characters;
    }
    return length;
}

int main(int argc, char * * argv)
{
    std::vector<std::string> documents;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        documents.push_back(line);
    file.close();
    
    long problem_a = 0, problem_b = 0;
    for (std::string line : documents)
    {
        problem_a += expansionSize(line, false);
        problem_b += expansionSize(line, true);
    }
    std::cout << "Problem A: " << problem_a << '\n';
    std::cout << "Problem B: " << problem_b << '\n';
    // Expected size (A - little1): 6+7+9+11+6+18=57
    // Expected size (B - little2): 9+20+241920+445=242394
    
    return EXIT_SUCCESS;
}

