# [Day 9: Explosives in Cyberspace](https://adventofcode.com/2016/day/9)

#### Problem A

You are given a set of strings where each string has compression markers like `(<number_of_characters>X<number_of_repetitions>)` that means that you have to select the `<number_of_charecters>` after the maker and repeat it `<number_of_repetitions>`. Any marker that is inside a previous marker `<number_of_characters>` range is ignored.

What is the decompressed length of the strings? Don't count whitespace.

#### Problem B

Now, you take into account all the markers, even the ones that are inside of a previous marker, i.e. markers are expanded *recursively*.

What is the decompressed length of the strings? Don't count whitespace.
