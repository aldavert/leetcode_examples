#include <iostream>
#include <fstream>
#include <vector>
#include <string>

int main(int argc, char * * argv)
{
    std::vector<std::string> codes;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        codes.push_back(line);
    file.close();
    
    const size_t codeword_size = codes[0].size();
    std::cout << codes.size() << ' ' << codeword_size << '\n';
    std::vector<std::vector<int> > histogram(codeword_size, std::vector<int>(26));
    for (size_t i = 0; i < codes.size(); ++i)
        for (size_t j = 0; j < codeword_size; ++j)
            ++histogram[j][codes[i][j] - 'a'];
    std::string codeword_a, codeword_b;
    for (size_t i = 0; i < codeword_size; ++i)
    {
        int max_frequency = histogram[i][0], max_selected = 0;
        for (int j = 1; j < 26; ++j)
        {
            if (histogram[i][j] > max_frequency)
            {
                max_frequency = histogram[i][j];
                max_selected = j;
            }
        }
        codeword_a += static_cast<char>('a' + max_selected);
        int min_frequency = max_frequency, min_selected = 0;
        for (int j = 0; j < 26; ++j)
        {
            if ((histogram[i][j] > 0) && (histogram[i][j] < min_frequency))
            {
                min_frequency = histogram[i][j];
                min_selected = j;
            }
        }
        codeword_b += static_cast<char>('a' + min_selected);
    }
    std::cout << "Problem A: " << codeword_a << '\n';
    std::cout << "Problem B: " << codeword_b << '\n';
    return EXIT_SUCCESS;
}

