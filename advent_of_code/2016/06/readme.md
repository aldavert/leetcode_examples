# [Day 6: Signals and Noise](https://adventofcode.com/2016/day/6)

#### Problem A

You are given a matrix of characters where its columns encode a single character using a repetition code. What is the codeword?

#### Problem B

Now, you select the less frequent character of the characters present at each code. What is the codeword now?

