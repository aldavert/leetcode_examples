# [Day 23: Safe Cracking](https://adventofcode.com/2016/day/23)

#### Problem A

You have the computer from problem 2016-12. The computer has 4 registers (`a`, `b`, `c` and `d`) and the following instructions:
- `cpy x y`: copies `x` (value or register) into register `y`.
- `inc x` increase value of register `x` by one.
- `dec x` decrease value of register `x` by one.
- `jnz x y` jumps an instruction `y` away (relative), but only if `x` is *not zero*.

Now the computer has the extra instruction toggle `tgl x`, that modifies the instruction that is `x` instructions away as follows:
- `inc` becomes `dec`
- `dec` becomes `inc`
- `tlg` becomes `inc`. When `tlg` toggles itself, the instruction is not executed until next time it is reached.
- `jnz` becomes `cpy`.
- `cpy` becomes `jnz`.
- If the instruction toggled is outside the program, nothing is done.

Instruction that are not valid, e.g. an `inc` of a value or a `cpy` where the second parameter is also a value, the instruction is ignored.

Given a program, set the register `a` to 7 and execute the program. What is the value at this register once the code is executed?

#### Problem B

Now, re-execute the program but resetting all registers and setting register `a` to 12. What is the value at this register once the code is executed?


