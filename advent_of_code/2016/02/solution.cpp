#include <iostream>
#include <fstream>
#include <string>
#include <vector>

std::string code(const std::vector<std::string> &codes,
                 const std::vector<std::string> &keypad,
                 int start_x,
                 int start_y)
{
    std::string result;
    for (int y = start_y, x = start_x; const std::string &code : codes)
    {
        for (char dir : code)
        {
            if      ((dir == 'L') && (keypad[y][x - 1] != '.')) --x;
            else if ((dir == 'R') && (keypad[y][x + 1] != '.')) ++x;
            else if ((dir == 'U') && (keypad[y - 1][x] != '.')) --y;
            else if ((dir == 'D') && (keypad[y + 1][x] != '.')) ++y;
        }
        result += keypad[y][x];
    }
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<std::string> keypadA = {
        ".....",
        ".123.",
        ".456.",
        ".789.",
        "....."
    };
    std::vector<std::string> keypadB = {
        ".......",
        "...1...",
        "..234..",
        ".56789.",
        "..ABC..",
        "...D...",
        "......."
    };
    std::vector<std::string> codes;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        codes.push_back(line);
    file.close();
    
    std::cout << "Problem A: " << code(codes, keypadA, 2, 2) << '\n';
    std::cout << "Problem B: " << code(codes, keypadB, 1, 3) << '\n';
    
    return EXIT_SUCCESS;
}

