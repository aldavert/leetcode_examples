# [Day 2: Bathroom Security](https://adventofcode.com/2016/day/2)

#### Problem A

You have a keypad like this:
```
1 2 3
4 5 6
7 8 9
```
And a set of instructions to generate a series of numbers. You start at `5`, and the move your pointer `L` left, `U` up, `R` right and `D` down. Movements that lead outside the keypad are to be ignored. For example,

```
ULL
RRDDD
LURDL
UUUUD
```

has 4 lines, so it will generate the 4 digit number `1985`.

What number generates the problem input data?

#### Problem B

Now you have the exact same conditions but, the keypad is like:

```
  1
 234
56789
 ABC
  D
```

What number generates the problem input data?

