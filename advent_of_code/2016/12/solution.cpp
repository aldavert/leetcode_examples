#include <iostream>
#include <fstream>
#include <vector>
#include <string>

enum class ID { CPY, INC, DEC, JNZ };
struct Instruction
{
    ID id = ID::CPY;
    bool is_register = false;
    long value_first = 0;
    long value_second = 0;
};

void run(const std::vector<Instruction> &code, std::vector<long> &registers)
{
    const long n = static_cast<long>(code.size());
    long pc = 0;
    while ((pc >= 0) && (pc < n))
    {
        switch (code[pc].id)
        {
        case ID::CPY:
            if (code[pc].is_register)
                registers[code[pc].value_second] = registers[code[pc].value_first];
            else registers[code[pc].value_second] = code[pc].value_first;
            ++pc;
            break;
        case ID::INC:
            ++registers[code[pc].value_first];
            ++pc;
            break;
        case ID::DEC:
            --registers[code[pc].value_first];
            ++pc;
            break;
        case ID::JNZ:
            if ((code[pc].is_register && (registers[code[pc].value_first] != 0))
            ||  (!code[pc].is_register && (code[pc].value_first != 0)))
                pc += code[pc].value_second;
            else ++pc;
            break;
        default:
            std::cerr << "[ERROR] UNEXPECTED INSTRUCTION (pc = " << pc << ")\n";
            return;
        }
    }
}

void display(const std::vector<Instruction> &code)
{
    for (const auto &instruction : code)
    {
        if      (instruction.id == ID::CPY) std::cout << "cpy ";
        else if (instruction.id == ID::INC) std::cout << "inc ";
        else if (instruction.id == ID::DEC) std::cout << "dec ";
        else if (instruction.id == ID::JNZ) std::cout << "jnz ";
        if (instruction.is_register)
            std::cout << static_cast<char>('a' + instruction.value_first);
        else std::cout << instruction.value_first;
        if (instruction.id == ID::CPY)
            std::cout << ' ' << static_cast<char>('a' + instruction.value_second);
        if (instruction.id == ID::JNZ)
            std::cout << ' ' << instruction.value_second;
        std::cout << '\n';
    }
}

int main(int argc, char * * argv)
{
    std::vector<Instruction> code;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.size() < 5) continue;
        Instruction current;
        auto instr = line.substr(0, 3);
        if      (instr == "cpy")
        {
            current.id = ID::CPY;
            auto space = line.find(' ', 4);
            std::string value_first = line.substr(4, space - 4);
            if ((value_first[0] >= 'a') && (value_first[0] <= 'd'))
            {
                current.is_register = true;
                current.value_first = value_first[0] - 'a';
            }
            else
            {
                current.is_register = false;
                current.value_first = std::stol(value_first);
            }
            std::string reg_value = line.substr(space + 1);
            if ((reg_value[0] >= 'a') && (reg_value[0] <= 'd'))
                current.value_second = reg_value[0] - 'a';
            else
            {
                std::cerr << "[ERROR] MALFORMED INSTRUCTION: " << line << '\n';
                continue;
            }
        }
        else if (instr == "inc")
        {
            current.id = ID::INC;
            std::string reg_value = line.substr(4);
            current.is_register = true;
            if ((reg_value[0] >= 'a') && (reg_value[0] <= 'd'))
                current.value_first = reg_value[0] - 'a';
            else
            {
                std::cerr << "[ERROR] MALFORMED INSTRUCTION: " << line << '\n';
                continue;
            }
            current.value_second = -1;
        }
        else if (instr == "dec")
        {
            current.id = ID::DEC;
            std::string reg_value = line.substr(4);
            current.is_register = true;
            if ((reg_value[0] >= 'a') && (reg_value[0] <= 'd'))
                current.value_first = reg_value[0] - 'a';
            else
            {
                std::cerr << "[ERROR] MALFORMED INSTRUCTION: " << line << '\n';
                continue;
            }
            current.value_second = -1;
        }
        else if (instr == "jnz")
        {
            current.id = ID::JNZ;
            auto space = line.find(' ', 4);
            std::string value_first = line.substr(4, space - 4);
            if ((value_first[0] >= 'a') && (value_first[0] <= 'd'))
            {
                current.is_register = true;
                current.value_first = value_first[0] - 'a';
            }
            else
            {
                current.is_register = false;
                current.value_first = std::stol(value_first);
            }
            std::string jmp_value = line.substr(space + 1);
            current.value_second = std::stol(jmp_value);
        }
        else
        {
            std::cerr << "[ERROR] UNKNOWN INSTRUCTION: " << line << '\n';
            continue;
        }
        code.push_back(current);
    }
    file.close();
    
    std::cout << "Loading:\n";
    display(code);
    std::cout << "DONE\n";
    std::vector<long> registers(4);
    run(code, registers);
    std::cout << "Problem A: " << registers[0] << '\n';
    for (long &reg : registers) reg = 0;
    registers[2] = 1;
    run(code, registers);
    std::cout << "Problem B: " << registers[0] << '\n';
    
    return EXIT_SUCCESS;
}

