# [Day 12: Leonardo's Monorail](https://adventofcode.com/2016/day/12)

#### Problem A

You have a computer with 4 registers (`a`, `b`, `c` and `d`) and the following instructions:
- `cpy x y`: copies `x` (value or register) into register `y`.
- `inc x` increase value of register `x` by one.
- `dec x` decrease value of register `x` by one.
- `jnz x y` jumps an instruction `y` away (relative), but only if `x` is *not zero*.

Given a program, what is the value left at the register `a` once the code is executed?

#### Problem B

Now, rerun the program but start with register `c = 1`. What is the value of register `a` once the code is executed?

