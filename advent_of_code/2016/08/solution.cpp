#include <iostream>
#include <fstream>
#include <vector>

struct Instruction
{
    enum class ID { RECT, ROW, COLUMN };
    Instruction::ID id = Instruction::ID::RECT;
    int a = -1;
    int b = -1;
};

int main(int argc, char * * argv)
{
    std::vector<Instruction> instructions;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        Instruction current;
        if (line.size() < 5) continue;
        if (line.substr(0, 5) == "rect ")
        {
            current.id = Instruction::ID::RECT;
            auto search = line.find('x', 5);
            current.a = std::stoi(line.substr(5, search - 5));
            current.b = std::stoi(line.substr(search + 1));
        }
        else if (line.substr(0, 14) == "rotate column ")
        {
            current.id = Instruction::ID::COLUMN;
            auto left = line.find("x=");
            auto right = line.find(" by ", left);
            current.a = std::stoi(line.substr(left + 2, right - left - 2));
            current.b = std::stoi(line.substr(right + 4));
        }
        else if (line.substr(0, 11) == "rotate row ")
        {
            current.id = Instruction::ID::ROW;
            auto left = line.find("y=");
            auto right = line.find(" by ", left);
            current.a = std::stoi(line.substr(left + 2, right - left - 2));
            current.b = std::stoi(line.substr(right + 4));
        }
        else continue;
        instructions.push_back(current);
    }
    file.close();
    const int n_rows = (instructions.size() > 20)?6:3,
              n_cols = (instructions.size() > 20)?50:7;
    std::vector<std::string> display(n_rows, std::string(n_cols, '.'));
    for (auto [id, a, b] : instructions)
    {
        if (id == Instruction::ID::RECT)
        {
            for (int row = 0; row < std::min(b, n_rows); ++row)
                for (int col = 0; col < std::min(a, n_cols); ++col)
                    display[row][col] = '#';
        }
        else if (id == Instruction::ID::ROW)
        {
            std::string buffer(n_cols, '.');
            for (int c = 0; c < n_cols; ++c)
                buffer[(c + b) % n_cols] = display[a][c];
            display[a] = buffer;
        }
        else if (id == Instruction::ID::COLUMN)
        {
            std::string buffer(n_rows, '.');
            for (int r = 0; r < n_rows; ++r)
                buffer[(r + b) % n_rows] = display[r][a];
            for (int r = 0; r < n_rows; ++r)
                display[r][a] = buffer[r];
        }
    }
    int problem_a = 0;
    for (int row = 0; row < n_rows; ++row)
        for (int col = 0; col < n_cols; ++col)
            problem_a += display[row][col] == '#';
    std::cout << "Problem A: " << problem_a << '\n';
    std::cout << "Problem B:\n";
    for (const auto &row : display)
        std::cout << row << '\n';
    
    return EXIT_SUCCESS;
}
