# [Day 8: Two-Factor Authentication](https://adventofcode.com/2016/day/8)

#### Problem A

You are given a display of 50 by 6 pixels and you can perform the following operations:
- `rect AxB`turns on all the pixels in the rectangle that starts at the top-left corner of the display up to the coordinates `(A, B)`.
- `rotate row y=A by B` shift right all the pixels of row `A` by `B` pixels. Pixels that fall off the right end appear at the left end of the same row.
- `rotate column y=A by B` shift down all the pixels of column `A` by `B` pixels. Pixels that fall off the bottom end appear at the top end of the same row.

Given a list of instructions, how many pixels would turn on after all instructions are processed?


