#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <queue>
#include <string>

struct Node
{
    std::string name;
    std::vector<int> inputs;
    std::vector<std::string> outputs;
};

int main(int argc, char * * argv)
{
    std::vector<Node> graph;
    size_t number_of_inputs = 0;
    
    int queries[2] = { 17, 61 };
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    if (argc > 3)
    {
        queries[0] = std::stoi(argv[2]);
        queries[1] = std::stoi(argv[3]);
    }
    if (queries[0] < queries[1])
        std::swap(queries[0], queries[1]);
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.size() < 5) continue;
        std::istringstream iss(line);
        std::string token;
        iss >> token;
        if (token == "value")
        {
            std::string tok1, tok2, output, id;
            int value;
            iss >> value >> tok1 >> tok2 >> output >> id;
            graph.push_back({"I" + std::to_string(number_of_inputs++),
                             {value},
                             {((output == "bot")?"B":"O") + id}});
            if (output == "output")
                graph.push_back({"O" + id, {}, {"O" + id}});
        }
        else if (token == "bot")
        {
            std::string bot_id, gives, op1, to, out1, out1_id, an_, op2, out2, out2_id;
            iss >> bot_id >> gives >> op1 >> to >> out1 >> out1_id;
            out1_id = ((out1 == "bot")?"B":"O") + out1_id;
            if (out1 == "output")
                graph.push_back({out1_id, {}, {out1_id}});
            iss >> an_ >> op2 >> to >> out2 >> out2_id;
            out2_id = ((out2 == "bot")?"B":"O") + out2_id;
            if (out2 == "output")
                graph.push_back({out2_id, {}, {out2_id}});
            graph.push_back({"B" + bot_id,
                             {},
                             {out1_id, out2_id}});
            if (op1 == "low")
                std::swap(graph.back().outputs[0], graph.back().outputs[1]);
        }
        else std::cerr << "[WARNING] Unknown command: " << line << '\n';
    }
    file.close();
    std::unordered_map<std::string, Node *> nodes;
    std::queue<std::string> active;
    for (auto &node : graph)
    {
        if (nodes.find(node.name) != nodes.end())
        {
            std::cerr << "[ERROR] Multiple definitions of node '" << node.name
                      << "' in the graph.\n";
            return EXIT_FAILURE;
        }
        nodes[node.name] = &node;
        active.push(node.name);
    }
    while (!active.empty())
    {
        Node &current = *nodes[active.front()];
        active.pop();
        if (current.inputs.size() == current.outputs.size())
        {
            if (current.outputs.size() == 1)
                nodes[current.outputs[0]]->inputs.push_back(current.inputs[0]);
            else
            {
                if (current.inputs[0] < current.inputs[1])
                    std::swap(current.inputs[0], current.inputs[1]);
                nodes[current.outputs[0]]->inputs.push_back(current.inputs[0]);
                nodes[current.outputs[1]]->inputs.push_back(current.inputs[1]);
                if ((current.inputs[0] == queries[0])
                &&  (current.inputs[1] == queries[1]))
                {
                    std::cout << "Problem A: " << current.name << '\n';
                }
            }
        }
        else active.push(current.name);
    }
    int problem_b = 1;
    for (auto &node : graph)
    {
        if ((node.name == "O0") || (node.name == "O1") || (node.name == "O2"))
            problem_b *= node.inputs[0];
    }
    std::cout << "Problem B: " << problem_b << '\n';
    
    return EXIT_SUCCESS;
}

