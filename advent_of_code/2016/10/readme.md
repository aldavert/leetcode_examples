# [Day 10: Balance Bots](https://adventofcode.com/2016/day/10)

#### Problem A

You are given a set of instructions that define a graph of inputs, outputs and processing nodes. The processing nodes select the two inputs, when available, and distribute the max and min value to the defined nodes.

What is the ID of the processing node which compares and distributes values `61` and `17`?

#### Problem B

What do you get if you multiply together the values of the chip's ID in the outputs `0`, `1` and `2`?

