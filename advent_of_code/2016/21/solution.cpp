#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <memory>

class Instruction
{
public:
    Instruction(void) {}
    virtual ~Instruction(void) {}
    virtual std::string apply(const std::string &input) const = 0;
    virtual std::string display(void) const = 0;
};

class SwapPosition : public Instruction
{
    int m_x = -1;
    int m_y = -1;
public:
    SwapPosition(int x, int y) : Instruction(), m_x(x), m_y(y) {};
    std::string apply(const std::string &input) const
    {
        std::string result = input;
        std::swap(result[m_x], result[m_y]);
        return result;
    }
    std::string display(void) const
    {
        return std::string("swap position ") + std::to_string(m_x)
             + " with position " + std::to_string(m_y) + "";
    }
};

class SwapLetter : public Instruction
{
    char m_x = '\0';
    char m_y = '\0';
public:
    SwapLetter(char x, char y) : Instruction(), m_x(x), m_y(y) {};
    std::string apply(const std::string &input) const
    {
        auto p_x = input.find(m_x), p_y = input.find(m_y);
        if ((p_x == std::string::npos) || (p_y == std::string::npos))
            return input;
        std::string result = input;
        std::swap(result[p_x], result[p_y]);
        return result;
    }
    std::string display(void) const
    {
        return std::string("swap letter ") + m_x
             + " with letter " + m_y;
    }
};

class RotateLeft : public Instruction
{
    int m_steps = 0;
public:
    RotateLeft(int steps) : Instruction(), m_steps(steps) {};
    std::string apply(const std::string &input) const
    {
        int steps = m_steps % static_cast<int>(input.size());
        if (steps == 0) return input;
        return input.substr(steps) + input.substr(0, steps);
    }
    std::string display(void) const
    {
        return std::string("rotate left ") + std::to_string(m_steps) + " step"
             + ((m_steps != 1)?"s":"");
    }
};

class RotateRight : public Instruction
{
    int m_steps = 0;
public:
    RotateRight(int steps) : Instruction(), m_steps(steps) {};
    std::string apply(const std::string &input) const
    {
        int steps = m_steps % static_cast<int>(input.size());
        if (steps == 0) return input;
        return input.substr(input.size() - steps)
             + input.substr(0, input.size() - steps);
    }
    std::string display(void) const
    {
        return std::string("rotate right ") + std::to_string(m_steps) + " step"
             + ((m_steps != 1)?"s":"");
    }
};

class RotateLetter : public Instruction
{
    char m_letter = '\0';
public:
    RotateLetter(char letter) : Instruction(), m_letter(letter) {};
    std::string apply(const std::string &input) const
    {
        auto position = input.find(m_letter);
        if (position == std::string::npos) return input;
        int steps = (static_cast<int>(position) + 1 + (position >= 4))
                  % static_cast<int>(input.size());
        if (steps == 0) return input;
        return input.substr(input.size() - steps)
             + input.substr(0, input.size() - steps);
    }
    std::string display(void) const
    {
        return std::string("rotate based on position of letter ") + m_letter;
    }
};

class UnRotateLetter : public Instruction
{
    char m_letter = '\0';
public:
    UnRotateLetter(char letter) : Instruction(), m_letter(letter) {};
    std::string apply(const std::string &input) const
    {
        RotateLetter rotate_forth(m_letter);
        for (int i = 1, n = static_cast<int>(input.size()); i < n; ++i)
        {
            RotateLeft rotate_back(i);
            std::string result = rotate_back.apply(input);
            if (rotate_forth.apply(result) == input)
                return result;
        }
        return input;
    }
    std::string display(void) const
    {
        return std::string("rotate based on position of letter ") + m_letter;
    }
};

class Reverse : public Instruction
{
    int m_x = -1;
    int m_y = -1;
public:
    Reverse(int x, int y) : Instruction(), m_x(std::min(x, y)), m_y(std::max(x, y)) {};
    std::string apply(const std::string &input) const
    {
        std::string result = input;
        for (int a = m_x, b = m_y; a < b; ++a, --b)
            std::swap(result[a], result[b]);
        return result;
    }
    std::string display(void) const
    {
        return std::string("reverse positions ") + std::to_string(m_x)
             + " through " + std::to_string(m_y);
    }
};

class Move : public Instruction
{
    int m_x = -1;
    int m_y = -1;
public:
    Move(int x, int y) : Instruction(), m_x(x), m_y(y) {};
    std::string apply(const std::string &input) const
    {
        if (m_x < m_y)
            return input.substr(0, m_x)
                 + input.substr(m_x + 1, m_y - m_x)
                 + input[m_x]
                 + input.substr(m_y + 1);
        else
        {
            return input.substr(0, m_y)
                 + input[m_x]
                 + input.substr(m_y, m_x - m_y)
                 + input.substr(m_x + 1);
        }
        return input;
    }
    std::string display(void) const
    {
        return std::string("move position ") + std::to_string(m_x)
             + " to position " + std::to_string(m_y);
    }
};

std::ostream& operator<<(std::ostream &out,
                         const std::unique_ptr<Instruction> &instruction)
{
    out << instruction->display();
    return out;
};

int main(int argc, char * * argv)
{
    std::vector<std::unique_ptr<Instruction> > scramble, unscramble;
    std::string scramble_string = "abcdefgh";
    std::string unscramble_string = "fbgdceah";
    std::string filename = "data.txt";
    if (argc == 2) filename = argv[1];
    else if (argc == 4)
    {
        filename = argv[1];
        scramble_string = argv[2];
        unscramble_string = argv[3];
    }
    else if (argc != 1)
    {
        std::cerr << "[ERROR] Unexpected number of parameters.\n";
        return EXIT_FAILURE;
    }
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.size() < 14) continue;
        if (line.substr(0, 4) == "swap")
        {
            if (line.substr(5, 8) == "position")
            {
                std::string with, position;
                int x, y;
                std::istringstream iss(line.substr(13));
                iss >> x >> with >> position >> y;
                scramble.push_back(std::make_unique<SwapPosition>(x, y));
                unscramble.push_back(std::make_unique<SwapPosition>(y, x));
            }
            else if (line.substr(5, 6) == "letter")
            {
                std::string with, letter;
                char x, y;
                std::istringstream iss(line.substr(11));
                iss >> x >> with >> letter >> y;
                scramble.push_back(std::make_unique<SwapLetter>(x, y));
                unscramble.push_back(std::make_unique<SwapLetter>(y, x));
            }
            else
            {
                std::cerr << "[ERROR] Unexpected scrambling instruction: "
                          << line << '\n';
                return EXIT_FAILURE;
            }
        }
        else if (line.substr(0, 4) == "move")
        {
            std::string position1, to, position2;
            int x, y;
            std::istringstream iss(line.substr(4));
            iss >> position1 >> x >> to >> position2 >> y;
            scramble.push_back(std::make_unique<Move>(x, y));
            unscramble.push_back(std::make_unique<Move>(y, x));
        }
        else if (line.substr(0, 6) == "rotate")
        {
            if (line.substr(7, 4) == "left")
            {
                std::string steps;
                int x;
                std::istringstream iss(line.substr(11));
                iss >> x >> steps;
                scramble.push_back(std::make_unique<RotateLeft>(x));
                unscramble.push_back(std::make_unique<RotateRight>(x));
            }
            else if (line.substr(7, 5) == "right")
            {
                std::string steps;
                int x;
                std::istringstream iss(line.substr(12));
                iss >> x >> steps;
                scramble.push_back(std::make_unique<RotateRight>(x));
                unscramble.push_back(std::make_unique<RotateLeft>(x));
            }
            else if (line.substr(7, 5) == "based")
            {
                std::string on, position, of, letter;
                char x;
                std::istringstream iss(line.substr(12));
                iss >> on >> position >> of >> letter >> x;
                scramble.push_back(std::make_unique<RotateLetter>(x));
                unscramble.push_back(std::make_unique<UnRotateLetter>(x));
            }
            else
            {
                std::cerr << "[ERROR] Unexpected scrambling instruction: "
                          << line << '\n';
                return EXIT_FAILURE;
            }
        }
        else if (line.substr(0, 7) == "reverse")
        {
            std::string positions, through;
            int x, y;
            std::istringstream iss(line.substr(7));
            iss >> positions >> x >> through >> y;
            scramble.push_back(std::make_unique<Reverse>(x, y));
            unscramble.push_back(std::make_unique<Reverse>(x, y));
        }
        else
        {
            std::cerr << "[ERROR] Unexpected scrambling instruction: " << line << '\n';
            return EXIT_FAILURE;
        }
    }
    file.close();
    std::reverse(unscramble.begin(), unscramble.end());
    
    std::string problem_a = scramble_string;
    for (const auto &operation : scramble)
        problem_a = operation->apply(problem_a);
    std::cout << "Problem A: " << problem_a << '\n';
    
    std::string problem_b = unscramble_string;
    for (const auto &operation : unscramble)
        problem_b = operation->apply(problem_b);
    std::cout << "Problem B: " << problem_b << '\n';
    
    return EXIT_SUCCESS;
}

