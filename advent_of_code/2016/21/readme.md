# [Day 21: Scrambled Letters and Hash](https://adventofcode.com/2016/day/21)

#### Problem A

You are given a string and a set of scrambling operations that you have to apply on the string. The operations are:

- `swap position X with position Y`: swap elements at position `X` with the element of position `Y`.
- `swap letter X with letter Y`: swap the letter `X` with letter `Y`.
- `rotate left/right X steps`: rotate the whole string to the left/right `X` steps.
- `rotate based on position of letter X`: search letter `X` in the string and rotate the string to the right `position + 1 + (position >= 4)` positions.
- `reverse positions X through Y`: reverse the content of the string between elements at position `X` and `Y` (both included).
- `move position X to position Y`: eliminate the element at position `X` and insert it to position `Y` of the string.

What is the resulting string after applying all the scrambling operations?

#### Problem B

Given a scrambled string, return the original unscrambled string.

