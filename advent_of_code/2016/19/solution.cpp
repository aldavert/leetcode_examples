#include <iostream>
#include <vector>

int main(int argc, char * * argv)
{
    int number_of_people = 3017957;
    if (argc > 1)
        number_of_people = std::stoi(argv[1]);
    auto step = [&](int idx) -> int { return (idx + 1 < number_of_people)?idx + 1:0; };
    
    std::vector<bool> active(number_of_people, true);
    for (int count = number_of_people, pass = true, i = 0; count > 0; pass = !pass)
    {
        while (!active[i]) i = step(i);
        active[i] = pass;
        count -= !pass;
        if (pass && (count == 1))
            std::cout << "Problem A: " << i + 1 << '\n';
        i = step(i);
    }
    int w = 1;
    for (int i = 1; i < number_of_people; ++i)
    {
        w = w % i + 1;
        w += (w > (i + 1) / 2);
    }
    std::cout << "Problem B: " << w << '\n';
}

