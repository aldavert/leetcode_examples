# [Day 19: An Elephant Named Joseph](https://adventofcode.com/2016/day/19)

#### Problem A

You have a **round table** with people with tokens. Starting with the first position, the active person gets all the tokens of the person at its left. Then, the next active person is the one that still has tokens left. After several round, one person will have all the tokens.

Given the number of people who sits on the table, what is the number of the person accumulating all the tokens?

**Note:** The first person in the table has index `1` (not `0`).

#### Problem B

Now instead of taking the tokens from the person at your left, you take them from the person in front of you.

Given the number of people who sits on the table, what is the number of the person accumulating all the tokens?


