# [Day 16: Dragon Checksum](https://adventofcode.com/2016/day/16)

#### Problem A

You are given a string (made of `1` and `0`) and a desired length. You have to grow the string until it has the desired length. The algorithm to grow the string is as follow:
1. Make a copy of the current string.
2. Reverse the order of the copy.
3. Negate the values of the copy (`0` becomes `1` while `1` becomes `0`).
4. Modify the original string by concatenating a `0` and adding the modified copy of the string.
5. Repeat this process until the string has desired length. If its longer, only keep the first characters up to the desired length and discard the remaining characters.

After you have obtained the string, you have to compute its checksum. The checksum is computed by grouping all characters in groups of two and generating `1` if both characters are the same and `0` if they are different. This checksum, haves the length of the original string. If the length of the checksum is **odd** we stop, otherwise we apply the same algorithm on the obtained checksum. We repeat this process until the length of the string is **odd**.

Given an input string and length, what is the correct checksum?

#### Problem B

Same as before, but now the expected length is `35651584`. What is the checksum?

