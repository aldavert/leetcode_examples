#include <iostream>
#include <string>

std::string checksum(std::string text)
{
    std::string result;
    for (size_t i = 0; i < text.size(); i += 2)
        result += '0' + (text[i] == text[i + 1]);
    return (result.size() % 2 == 1)?result:checksum(result);
}

int main(int argc, char * * argv)
{
    std::string input = "10111011111001111", expected_checksum = "";
    size_t expected_length = 272;
    if ((argc == 2) && (std::string(argv[1]) == "test"))
    {
        input = "10000";
        expected_length = 20;
        expected_checksum = "01100";
    }
    else if ((argc == 2) && (std::string(argv[1]) == "second"))
    {
        expected_length = 35'651'584;
    }
    else if (argc > 2)
    {
        input = argv[1];
        expected_length = std::stoi(argv[2]);
    }
    
    std::string generated = input;
    while (generated.size() < expected_length)
    {
        std::string copy = generated;
        std::reverse(copy.begin(), copy.end());
        for (char &bit : copy)
            bit = '0' + !(bit - '0');
        generated += '0' + copy;
    }
    generated = generated.substr(0, expected_length);
    if (expected_checksum != "")
    {
        std::string output = checksum(generated);
        std::cout << "Checksum: " << output << ' ';
        if (expected_checksum == output) std::cout << "[CORRECT]";
        else std::cout << "[FAILURE]";
        std::cout << '\n';
    }
    else std::cout << "Checksum: " << checksum(generated) << '\n';
}

