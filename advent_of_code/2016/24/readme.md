# [Day 24: Air Duct Spelunking](https://adventofcode.com/2016/day/24)

#### Problem A

You are given the map of a maze and there are multiple locations that you have to visit. You start at location `0` but you can visit the other locations in any order.

What is the distance of the shortest path that connects all the locations?

#### Problem B

What is the distance of the shortest path that connects all locations **and returns to the starting location**?

