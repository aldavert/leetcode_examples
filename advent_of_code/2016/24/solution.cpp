#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <queue>
#include <limits>

std::vector<int> calculateDistances(std::vector<std::string> grid,
                                    const std::map<char, int> &locations,
                                    char start)
{
    constexpr int dirs[] = { -1, 0, 1, 0, -1 };
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>(grid[0].size());
    std::queue<std::pair<int, int> > q;
    std::vector<int> distances(locations.size());
    
    for (int row = 0; q.empty() && (row < n_rows); ++row)
        for (int col = 0; q.empty() && (col < n_cols); ++col)
            if (grid[row][col] == start)
                q.push({row, col});
    for (int distance = 0; !q.empty(); ++distance)
    {
        for (size_t i = 0, n = q.size(); i < n; ++i)
        {
            auto [row, col] = q.front();
            q.pop();
            if (grid[row][col] == '#') continue;
            if (std::isdigit(grid[row][col]))
                distances[locations.at(grid[row][col])] = distance;
            grid[row][col] = '#';
            for (int d = 0; d < 4; ++d)
            {
                int next_row = row + dirs[d],
                    next_col = col + dirs[d + 1];
                if ((next_row >= 0) && (next_col >= 0)
                &&  (next_row < n_rows) && (next_col < n_cols)
                &&  (grid[next_row][next_col] != '#'))
                    q.push({next_row, next_col});
            }
        }
    }
    return distances;
}

int minimumDistance(const std::vector<std::vector<int> > &distances, bool back)
{
    const int n = static_cast<int>(distances.size());
    std::vector<bool> available(n, true);
    int result = std::numeric_limits<int>::max();
    auto search = [&](auto &&self, int current, int distance, int remaining) -> void
    {
        if (remaining == 0)
        {
            if (back) result = std::min(result, distance + distances[current][0]);
            else result = std::min(result, distance);
            return;
        }
        for (int i = 0; i < n; ++i)
        {
            if (available[i] && (i != current))
            {
                available[i] = false;
                self(self, i, distance + distances[current][i], remaining - 1);
                available[i] = true;
            }
        }
    };
    available[0] = false;
    search(search, 0, 0, n - 1);
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<std::string> maze;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        if (line.size() > 5)
            maze.push_back(line);
    file.close();
    std::map<char, int> locations;
    for (const auto &row : maze)
        for (char loc : row)
            if (std::isdigit(loc))
                locations[loc] = -1;
    for (int i = 0; auto &[start, idx] : locations)
        idx = i++;
    std::vector<std::vector<int> > distances;
    for (auto [start, _] : locations)
        distances.push_back(calculateDistances(maze, locations, start));
    std::cout << "Problem A: " << minimumDistance(distances, false) << '\n';
    std::cout << "Problem B: " << minimumDistance(distances, true) << '\n';
    
    return EXIT_SUCCESS;
}

