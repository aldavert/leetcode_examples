# [Day 4: Security Through Obscurity](https://adventofcode.com/2016/day/4)

#### Problem A

You are given a set of string codes of the form `<name>-<id>[<checksum>]` where name consists of lowercase letters separated by dashes, the ID is a number and the checksum is the five most common letters in the name, in order, with ties broken by alphabetization.

A string code is valid if the checksum is correct. Compute the sum of the IDs all valid string codes.

#### Problem B

The `<name>` of the string code is encoded using *caesar* encryption. Search the ID of the string that contains the text `north`.

