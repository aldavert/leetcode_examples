#include <iostream>
#include <fstream>
#include <vector>
#include <string>

struct Room
{
    std::string name = "";
    int id = 0;
    char checksum[5] = {};
};

int main(int argc, char * * argv)
{
    auto cmp =
        [](const std::pair<int, int> &left, const std::pair<int, int> &right) -> bool
    {
        return (left.first < right.first)
            || ((left.first == right.first) && (left.second > right.second));
    };
    std::vector<Room> rooms;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        rooms.push_back({});
        size_t position = 0;
        while (!std::isdigit(line[position])) ++position;
        rooms.back().name = line.substr(0, position - 1);
        rooms.back().id = std::stoi(line.substr(position, line.size() - 7));
        for (size_t i = 0; i < 5; ++i)
            rooms.back().checksum[i] = line[line.size() - 6 + i];
    }
    file.close();
    
    int problem_a = 0, problem_b = -1;
    for (const Room &r : rooms)
    {
        std::pair<int, int> histogram[26] = {};
        for (int i = 0; i < 26; ++i) histogram[i].second = i;
        for (char letter : r.name)
            histogram[letter - 'a'].first += 1;
        std::sort(&histogram[0], &histogram[26], cmp);
        bool valid = true;
        for (int i = 0; i < 5; ++i)
            valid = valid && ((histogram[25 - i].second + 'a') == r.checksum[i]);
        if (valid)
        {
            problem_a += r.id;
            for (int o = 0; o < 26; ++o)
            {
                std::string text;
                for (char letter : r.name)
                {
                    if (letter != '-')
                        text += static_cast<char>((letter - 'a' + o) % 26 + 'a');
                    else text += ' ';
                }
                if (text.find("north") != std::string::npos)
                    problem_b = r.id;
            }
        }
    }
    std::cout << "Problem A: " << problem_a << '\n';
    std::cout << "Problem B: " << problem_b << '\n';
    
    return EXIT_SUCCESS;
}

