# [Day 13: A Maze of Twisty Little Cubicles](https://adventofcode.com/2016/day/13)

#### Problem A

You are in a maze where you can determine if a cells are free space or an obstacle by:
1. Compute `a = x * x + 3 * x + 2 * x * y + y + y * y`.
2. Add the maze key (puzzle input) to `a`, so `b = a + key`.
3. If the number of ones in the binary representation of `b` is even, it is **open space**, otherwise it is an **obstacle**.

You start at cell `(1, 1)` and you want to reach cell `(31, 39)`. What is the minimum number of steps required?

#### Problem B

How many cells can you visit if you take at most 50 steps?

