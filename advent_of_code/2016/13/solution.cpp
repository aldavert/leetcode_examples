#include <iostream>
#include <vector>
#include <unordered_set>
#include <queue>
#include <bit>

int main(int argc, char * * argv)
{
    constexpr int dirs[] = { -1, 0, 1, 0, -1 };
    int key = 1352;
    if (argc > 1) key = std::stoi(argv[1]);
    std::unordered_set<long> visited;
    std::queue<std::pair<int, int> > q;
    q.push({1, 1});
    int problem_b = 0;
    for (int steps = 0; !q.empty(); ++steps)
    {
        for (size_t i = 0, n = q.size(); i < n; ++i)
        {
            auto [x, y] = q.front();
            q.pop();
            if ((x == 31) && (y == 39))
            {
                std::cerr << "Problem A: " << steps << '\n';
                while (!q.empty()) q.pop();
                break;
            }
            size_t obstacle = x * x + 3 * x + 2 * x * y + y + y * y + key;
            if (std::popcount(obstacle) % 2 != 0)
                continue;
            long id = static_cast<long>(x) << 31 | static_cast<long>(y);
            if (visited.find(id) != visited.end())
                continue;
            visited.insert(id);
            if ((x < 0) || (y < 0)) continue;
            for (size_t k = 0; k < 4; ++k)
                q.push({x + dirs[k], y + dirs[k + 1]});
        }
        if (steps == 49)
            problem_b = static_cast<int>(visited.size());
    }
    std::cout << "Problem B: " << problem_b << '\n';
    return EXIT_SUCCESS;
}

