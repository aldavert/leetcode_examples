#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <queue>

struct Grid
{
    std::vector<std::vector<int> > used;
    std::vector<std::vector<int> > avail;
    std::vector<std::vector<int> > size;
    int n_rows = 0;
    int n_cols = 0;
};

int search(std::pair<int, int> goal, std::pair<int, int> start, const Grid &grid)
{
    const int dirs[] = { -1, 0, 1, 0, -1 };
    std::pair<int, int> empty = {-1, -1};
    long average_size = 0;
    for (int row = 0; row < grid.n_rows; ++row)
    {
        for (int col = 0; col < grid.n_cols; ++col)
        {
            if (grid.used[row][col] == 0)
                empty = {row, col};
            average_size += grid.size[row][col];
        }
    }
    average_size /= (grid.n_rows * grid.n_cols);
    if (empty == std::pair<int, int>({-1, -1})) return -1;
    std::vector<std::string> obstacles(grid.n_rows, std::string(grid.n_cols, '.'));
    for (int row = 0; row < grid.n_rows; ++row)
        for (int col = 0; col < grid.n_cols; ++col)
            if (grid.size[row][col] > average_size)
                obstacles[row][col] = '#';
    std::vector<std::string> work = obstacles;
    std::queue<std::pair<int, int> > q;
    q.push(empty);
    int result = 0;
    while (!q.empty())
    {
        for (size_t i = 0, n = q.size(); i < n; ++i)
        {
            auto [r, c] = q.front();
            q.pop();
            if (work[r][c] != '.') continue;
            if (start == std::make_pair(r, c))
            {
                while (!q.empty()) q.pop();
                break;
            }
            work[r][c] = '#';
            for (int d = 0; d < 4; ++d)
            {
                int nr = r + dirs[d], nc = c + dirs[d + 1];
                if ((nr >= 0) && (nr < grid.n_rows) && (nc >= 0) && (nc < grid.n_cols))
                    q.push({nr, nc});
            }
        }
        ++result;
    }
    work = obstacles;
    q.push(start);
    int second_trail = 0;
    while (!q.empty())
    {
        for (size_t i = 0, n = q.size(); i < n; ++i)
        {
            auto [r, c] = q.front();
            q.pop();
            if (work[r][c] != '.') continue;
            if (goal == std::make_pair(r, c))
            {
                while (!q.empty()) q.pop();
                break;
            }
            work[r][c] = '#';
            for (int d = 0; d < 4; ++d)
            {
                int nr = r + dirs[d], nc = c + dirs[d + 1];
                if ((nr >= 0) && (nr < grid.n_rows) && (nc >= 0) && (nc < grid.n_cols))
                    q.push({nr, nc});
            }
        }
        ++second_trail;
    }
    return result - 1 + (second_trail - 2) * 5;
}

int main(int argc, char * * argv)
{
    Grid grid;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.find("/dev/grid/node-") == std::string::npos) continue;
        auto start = line.find('-');
        auto location = line.substr(start + 1, line.find(' ', start) - start - 1);
        int x = stoi(location.substr(1, location.find('-') - 1)),
            y = stoi(location.substr(location.find('-') + 2));
        while (y >= grid.n_rows)
        {
            grid.used.push_back(std::vector<int>(grid.n_cols, -1));
            grid.avail.push_back(std::vector<int>(grid.n_cols, -1));
            grid.size.push_back(std::vector<int>(grid.n_cols, -1));
            ++grid.n_rows;
        }
        while (x >= grid.n_cols)
        {
            for (int row = 0; row < grid.n_rows; ++row)
            {
                grid.used[row].push_back(-1);
                grid.avail[row].push_back(-1);
                grid.size[row].push_back(-1);
            }
            ++grid.n_cols;
        }
        auto pos_t = line.find('T');
        start = line.rfind(' ', pos_t) + 1;
        grid.size[y][x] = std::stoi(line.substr(start, pos_t - start));
        pos_t = line.find('T', pos_t + 1);
        start = line.rfind(' ', pos_t) + 1;
        grid.used[y][x] = std::stoi(line.substr(start, pos_t - start));
        pos_t = line.find('T', pos_t + 1);
        start = line.rfind(' ', pos_t) + 1;
        grid.avail[y][x] = std::stoi(line.substr(start, pos_t - start));
        if (grid.avail[y][x] + grid.used[y][x] != grid.size[y][x])
        {
            std::cerr << "[ERROR] Current node has an incorrect disk size: "
                      << line << '\n';
            return EXIT_FAILURE;
        }
    }
    file.close();
    std::vector<std::pair<int, int> > nodes;
    for (int i = 0, n = static_cast<int>(grid.used.size()); i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            if (grid.used[i][j] == -1)
            {
                std::cout << "[ERROR] Node '" << i << "'-'" << j
                          << "' is uninitialized.\n";
                return EXIT_FAILURE;
            }
            nodes.push_back({i, j});
        }
    }
    int problem_a = 0;
    for (auto node_a : nodes)
    {
        for (auto node_b : nodes)
        {
            if (node_a == node_b) continue;
            if (grid.used[node_a.first][node_a.second] == 0) continue;
            problem_a += grid.used[node_a.first][node_a.second]
                      <= grid.avail[node_b.first][node_b.second];
        }
    }
    std::cout << "Problem A: " << problem_a << '\n';
    std::cout << "Problem B: " << search({0, 0}, {0, grid.n_cols - 1}, grid) << '\n';
    return EXIT_SUCCESS;
}

