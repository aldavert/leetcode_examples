# [Day 22: Grid Computing](https://adventofcode.com/2016/day/22)

#### Problem A

You have a grid of storage drives. These drives are 4-connected and can move all its content to one of its four neighbors. You are given a listing of the free space in the storage grid (similar to the output of the `df` command).

A **viable pair** is any pair of nodes `(A, B)`, regardless if they are neighbors or not, where
- `A` is not empty.
- `A != B`
- The data on `A` fits in `B`.

Return the number of **viable pairs** in the grid.

#### Problem B

You want to obtain the content from then node at `y=0` and the highest `x` value. To access this content, you have to move it to node `x=0` and `y=0`.

What is the minimum number of steps you need to achieve this?


