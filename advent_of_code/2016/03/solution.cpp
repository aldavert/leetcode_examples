#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

int main(int argc, char * * argv)
{
    std::vector<std::vector<int> > triangles;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        std::istringstream iss(line);
        triangles.push_back({});
        for (std::string token; iss >> token;)
            triangles.back().push_back(std::stoi(token));
    }
    file.close();
    
    int problem_a = 0;
    std::vector<int> current(3);
    for (size_t i = 0; i < triangles.size(); ++i)
    {
        current = triangles[i];
        std::sort(current.begin(), current.end());
        problem_a += (current[0] + current[1]) > current[2];
    }
    std::cout << "Problem A: " << problem_a << '\n';
    
    int problem_b = 0;
    for (size_t i = 0; i < triangles.size(); i += 3)
    {
        for (size_t c = 0; c < 3; ++c)
        {
            for (size_t j = 0; j < 3; ++j)
                current[j] = triangles[i + j][c];
            std::sort(current.begin(), current.end());
            problem_b += (current[0] + current[1]) > current[2];
        }
    }
    std::cout << "Problem B: " << problem_b << '\n';
    return EXIT_SUCCESS;
}

