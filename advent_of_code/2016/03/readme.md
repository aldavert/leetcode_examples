# [Day 3: Squares With Three Sides](https://adventofcode.com/2016/day/3)

#### Problem A

You are given a list of triplets corresponding to the lengths of the triangle sides. Find how many triangles are valid triangles.

#### Problem B

Do the same, but now consider that the triangles are stored in stacked in columns, not in rows (i.e. the matrix is transposed). How many triangles are valid?

