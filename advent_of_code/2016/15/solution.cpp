#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

struct Disk
{
    int number_of_positions = 0;
    int position_index = 0;
};

int allAligned(const std::vector<Disk> &disks)
{
    for (int t = 0, n = static_cast<int>(disks.size()); t < 2'000'000'000; ++t)
    {
        bool all_zero = true;
        for (int i = 0; all_zero && (i < n); ++i)
            all_zero = all_zero && ((t + disks[i].position_index + i + 1) % disks[i].number_of_positions == 0);
        if (all_zero)
            return t;
    }
    return -1;
}

int main(int argc, char * * argv)
{
    std::vector<Disk> disks;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        std::istringstream iss(line);
        std::string _disk, _has, _at, _it, _is, _position;
        std::string t_disk_number, t_number_of_positions, t_time, t_position_index;
        iss >> _disk >> t_disk_number >> _has >> t_number_of_positions >> _position
            >> _at >> t_time >> _it >> _is;
        iss >> _at >> _position >> t_position_index;
        size_t disk_number = std::stoi(t_disk_number.substr(1));
        while (disks.size() < disk_number) disks.push_back(Disk());
        disks[disk_number - 1] = { std::stoi(t_number_of_positions),
                                   std::stoi(t_position_index) };
    }
    file.close();
    std::cout << "Problem A: " << allAligned(disks) << '\n';
    disks.push_back({11, 0});
    std::cout << "Problem B: " << allAligned(disks) << '\n';
    
    return EXIT_SUCCESS;
}

