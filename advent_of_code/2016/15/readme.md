# [Day 15: Timing is Everything](https://adventofcode.com/2016/day/15)

#### Problem A

You are given a set of disks with a different amount of positions. All the positions are blocked and only position `0` is open. The disks rotate one position at each second.

A ball falls from the top and can only pass if there is an open position in the disk. Otherwise, it bounces back and disappears. The ball moves a ball one position per second.

How many seconds you have to wait to drop the ball so it can pass all the disks and reach the other side?

#### Problem B

You add a new disk at the bottom with `11` positions which starts at position `0`.

How many seconds you have to wait now to drop the ball so it can pass all the disks and reach the other side?

