# [Day 7: Internet Protocol Version 7](https://adventofcode.com/2016/day/7)

#### Problem A

You are given a set of strings. A valid string is a string that contains a substring containing a pair of different characters followed by the same pair in reverse. This substring however cannot be between contained square brackets.

How many valid strings has the set?

#### Problem B

Now a valid string is any substring of three characters where the first and the third characters are the same while the middle character is a different character. This substring must also be found in reverse (i.e. the inner character outside and the outside characters in the middle) inside the square brackets.

How many valid strings has now the set?

