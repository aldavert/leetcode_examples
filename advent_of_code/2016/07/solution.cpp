#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_set>

bool isValidA(const std::string &address)
{
    bool inside = false, valid = false;
    for (char substring[4] = {}; char symbol : address)
    {
        substring[0] = substring[1];
        substring[1] = substring[2];
        substring[2] = substring[3];
        substring[3] = symbol;
        if      (symbol == '[') inside = true;
        else if (symbol == ']') inside = false;
        else
        {
            if ((substring[0] == substring[3])
            &&  (substring[1] == substring[2])
            &&  (substring[0] != substring[1]))
            {
                if (inside) return false;
                valid = true;
            }
        }
    }
    return valid;
}

bool isValidB(const std::string &address)
{
    std::unordered_set<std::string> pattern_outside, pattern_inside;
    bool inside = false;
    for (std::string substring(3, '\0'); char symbol : address)
    {
        substring[0] = substring[1];
        substring[1] = substring[2];
        substring[2] = symbol;
        if      (symbol == '[') inside = true;
        else if (symbol == ']') inside = false;
        else
        {
            if ((substring[0] == substring[2])
            &&  (substring[0] != substring[1]))
            {
                if (inside)
                {
                    std::string reverse(3, substring[1]);
                    reverse[1] = substring[0];
                    pattern_inside.insert(reverse);
                }
                else pattern_outside.insert(substring);
            }
        }
    }
    for (std::string pattern : pattern_inside)
        if (pattern_outside.find(pattern) != pattern_outside.end())
            return true;
    return false;
}

int main(int argc, char * * argv)
{
    std::vector<std::string> addresses;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        addresses.push_back(line);
    file.close();
    
    int problem_a = 0;
    for (const auto &address : addresses)
        problem_a += isValidA(address);
    std::cout << "Problem A: " << problem_a << '\n';
    int problem_b = 0;
    for (const auto &address : addresses)
        problem_b += isValidB(address);
    std::cout << "Problem B: " << problem_b << '\n';
    
    return EXIT_SUCCESS;
}

