#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>

enum class ID { CPY, INC, DEC, JNZ, TGL, OUT };
struct Instruction
{
    ID id = ID::CPY;
    bool is_register[2] = { false, true };
    long value[2] = {0, 0};
};

std::vector<long> run(const std::vector<Instruction> &source,
                      std::vector<long> &registers,
                      size_t maximum_output_size)
{
    std::vector<long> output;
    std::unordered_map<ID, ID> replace;
    replace[ID::CPY] = ID::JNZ;
    replace[ID::JNZ] = ID::CPY;
    replace[ID::INC] = ID::DEC;
    replace[ID::DEC] = ID::INC;
    replace[ID::TGL] = ID::INC;
    std::vector<Instruction> code = source;
    const long n = static_cast<long>(code.size());
    long pc = 0;
    while ((pc >= 0) && (pc < n))
    {
        switch (code[pc].id)
        {
        case ID::CPY:
            if (code[pc].is_register[1])
            {
                if (code[pc].is_register[0])
                    registers[code[pc].value[1]] = registers[code[pc].value[0]];
                else registers[code[pc].value[1]] = code[pc].value[0];
            }
            ++pc;
            break;
        case ID::INC:
            if (code[pc].is_register[0])
                ++registers[code[pc].value[0]];
            ++pc;
            break;
        case ID::DEC:
            if (code[pc].is_register[0])
                --registers[code[pc].value[0]];
            ++pc;
            break;
        case ID::JNZ:
            {
                long step = (code[pc].is_register[1])
                          ? registers[code[pc].value[1]]
                          : code[pc].value[1];
                if ((code[pc].is_register[0] && (registers[code[pc].value[0]] != 0))
                ||  (!code[pc].is_register[0] && (code[pc].value[0] != 0)))
                    pc += step;
                else ++pc;
            }
            break;
        case ID::TGL:
            {
                long step = code[pc].is_register[0]
                          ? registers[code[pc].value[0]]
                          : code[pc].value[0];
                step += pc;
                if ((step >= 0) && (step < static_cast<int>(code.size())))
                    code[step].id = replace[code[step].id];
            }
            ++pc;
            break;
        case ID::OUT:
            if (code[pc].is_register[0])
                output.push_back(registers[code[pc].value[0]]);
            else output.push_back(code[pc].value[0]);
            if (output.size() >= maximum_output_size)
                return output;
            ++pc;
            break;
        default:
            std::cerr << "[ERROR] UNEXPECTED INSTRUCTION (pc = " << pc << ")\n";
            return output;
        }
    }
    return output;
}

void display(const std::vector<Instruction> &code)
{
    for (const auto &instruction : code)
    {
        if      (instruction.id == ID::CPY) std::cout << "cpy ";
        else if (instruction.id == ID::INC) std::cout << "inc ";
        else if (instruction.id == ID::DEC) std::cout << "dec ";
        else if (instruction.id == ID::JNZ) std::cout << "jnz ";
        else if (instruction.id == ID::TGL) std::cout << "tgl ";
        if (instruction.is_register[0])
            std::cout << static_cast<char>('a' + instruction.value[0]);
        else std::cout << instruction.value[0];
        if ((instruction.id == ID::CPY) || (instruction.id == ID::JNZ))
        {
            std::cout << ' ';
            if (instruction.is_register[1])
                std::cout << static_cast<char>('a' + instruction.value[1]);
            else std::cout << instruction.value[1];
        }
        std::cout << '\n';
    }
}

int main(int argc, char * * argv)
{
    std::vector<Instruction> code;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.size() < 5) continue;
        Instruction current;
        auto instr = line.substr(0, 3);
        if      (instr == "cpy")
        {
            current.id = ID::CPY;
            current.is_register[1] = true;
            auto space = line.find(' ', 4);
            std::string first_param = line.substr(4, space - 4);
            if ((first_param[0] >= 'a') && (first_param[0] <= 'd'))
            {
                current.is_register[0] = true;
                current.value[0] = first_param[0] - 'a';
            }
            else
            {
                current.is_register[0] = false;
                current.value[0] = std::stol(first_param);
            }
            std::string reg_value = line.substr(space + 1);
            if ((reg_value[0] >= 'a') && (reg_value[0] <= 'd'))
                current.value[1] = reg_value[0] - 'a';
            else
            {
                std::cerr << "[ERROR] MALFORMED INSTRUCTION: " << line << '\n';
                continue;
            }
        }
        else if (instr == "inc")
        {
            current.id = ID::INC;
            current.is_register[1] = true;
            current.value[1] = 0;
            std::string reg_value = line.substr(4);
            current.is_register[0] = true;
            if ((reg_value[0] >= 'a') && (reg_value[0] <= 'd'))
                current.value[0] = reg_value[0] - 'a';
            else
            {
                std::cerr << "[ERROR] MALFORMED INSTRUCTION: " << line << '\n';
                continue;
            }
            current.value[1] = -1;
        }
        else if (instr == "dec")
        {
            current.id = ID::DEC;
            current.is_register[1] = true;
            current.value[1] = 0;
            std::string reg_value = line.substr(4);
            current.is_register[0] = true;
            if ((reg_value[0] >= 'a') && (reg_value[0] <= 'd'))
                current.value[0] = reg_value[0] - 'a';
            else
            {
                std::cerr << "[ERROR] MALFORMED INSTRUCTION: " << line << '\n';
                continue;
            }
            current.value[1] = -1;
        }
        else if (instr == "jnz")
        {
            current.id = ID::JNZ;
            auto space = line.find(' ', 4);
            std::string first_param = line.substr(4, space - 4);
            if ((first_param[0] >= 'a') && (first_param[0] <= 'd'))
            {
                current.is_register[0] = true;
                current.value[0] = first_param[0] - 'a';
            }
            else
            {
                current.is_register[0] = false;
                current.value[0] = std::stol(first_param);
            }
            std::string second_param = line.substr(space + 1);
            if ((second_param[0] >= 'a') && (second_param[0] <= 'd'))
            {
                current.is_register[1] = true;
                current.value[1] = second_param[0] - 'a';
            }
            else
            {
                current.is_register[1] = false;
                current.value[1] = std::stol(second_param);
            }
        }
        else if (instr == "tgl")
        {
            current.id = ID::TGL;
            current.is_register[1] = true;
            current.value[1] = 0;
            std::string first_param = line.substr(4);
            if ((first_param[0] >= 'a') && (first_param[0] <= 'd'))
            {
                current.is_register[0] = true;
                current.value[0] = first_param[0] - 'a';
            }
            else
            {
                current.is_register[0] = false;
                current.value[0] = std::stol(first_param);
            }
        }
        else if (instr == "out")
        {
            current.id = ID::OUT;
            current.is_register[1] = true;
            current.value[1] = 0;
            std::string first_param = line.substr(4);
            if ((first_param[0] >= 'a') && (first_param[0] <= 'd'))
            {
                current.is_register[0] = true;
                current.value[0] = first_param[0] - 'a';
            }
            else
            {
                current.is_register[0] = false;
                current.value[0] = std::stol(first_param);
            }
        }
        else
        {
            std::cerr << "[ERROR] UNKNOWN INSTRUCTION: " << line << '\n';
            continue;
        }
        code.push_back(current);
    }
    file.close();
    
    //std::cout << "Loading:\n";
    //display(code);
    //std::cout << "DONE\n";
    std::vector<long> registers(4);
    std::vector<long> expected_output(20);
    for (size_t i = 0; i < expected_output.size(); ++i) expected_output[i] = i & 1;
    for (int value = 0; value < 1000; ++value)
    {
        registers.clear();
        registers[0] = value;
        auto output = run(code, registers, expected_output.size());
        if (output == expected_output)
        {
            std::cout << "Problem A: " << value << '\n';
            break;
        }
    }
    
    return EXIT_SUCCESS;
}

