# [Day 25: Clock Signal](https://adventofcode.com/2016/day/25)

#### Problem A

You have the computer from problems 2016-12 and 2016-23, again. The computer has 4 registers (`a`, `b`, `c` and `d`) and the following instructions:
- `cpy x y`: copies `x` (value or register) into register `y`.
- `inc x` increase value of register `x` by one.
- `dec x` decrease value of register `x` by one.
- `jnz x y` jumps an instruction `y` away (relative), but only if `x` is *not zero*.
- `tgl x` modifies the instruction that is at `x` positions: `dec<->inc`, `cpy<->jnz` and `tgl->inc`.

Now, the computer has a new instruction called `out x` which *outputs* the integer `x` or the content of the register `x`.

What is the initial integer value of register `a` that makes the given program generate the sequence `01010101010...` indefinitely? 

