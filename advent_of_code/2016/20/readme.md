# [Day 20: Firewall Rules](https://adventofcode.com/2016/day/20)

#### Problem A

You are given a set of ranges of blocked integers. Return the lowest integer that is not blocked by any range of the set.

#### Problem B

How many numbers are not blocked?

