#include <iostream>
#include <fstream>
#include <vector>
#include <string>

int main(int argc, char * * argv)
{
    std::vector<std::pair<long, long> > blocked;
    std::string filename = "data.txt";
    if (argc > 1)
        filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        auto separator = line.find('-');
        blocked.push_back({std::stol(line.substr(0, separator)),
                           std::stol(line.substr(separator + 1))});
    }
    file.close();
    std::sort(blocked.begin(), blocked.end());
    long problem_a = -1, problem_b = 0;
    for (long previous = 0; auto [min, max] : blocked)
    {
        if ((problem_a == -1) && (min - previous > 1))
            problem_a = previous + 1;
        problem_b += std::max(0l, min - previous - 1);
        previous = std::max(previous, max);
    }
    std::cout << "Problem A: " << problem_a << '\n';
    std::cout << "Problem B: " << problem_b << '\n';
    return EXIT_SUCCESS;
}

