# [Day 1: No Time for a Taxicab](https://adventofcode.com/2016/day/1)

#### Problem A

You are on a map facing north. You are given a set of instruction with the format `<R|L><distance>` that indicates that you have to rotate 90 degrees right `R` or left `L` and move `distance` blocks forward.

What is the Manhattan distance between the start and destination locations?

