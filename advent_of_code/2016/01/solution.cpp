#include <iostream>
#include <fstream>
#include <vector>

struct Instruction
{
    int turn = 0;
    int distance = 0;
};

int main(int argc, char * * argv)
{
    constexpr int movement[] = {-1, 0, 1, 0, -1};
    std::vector<Instruction> instructions;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string token; file >> token; )
    {
        if (token.back() == ',') token.pop_back();
        instructions.push_back({2 * (token[0] == 'R') - 1, std::stoi(token.substr(1))});
    }
    file.close();
    int orientation = 1, x = 0, y = 0, max_x = 0, min_x = 0, max_y = 0, min_y = 0;
    for (auto [turn, distance] : instructions)
    {
        orientation += turn;
        if (orientation < 0) orientation = 3;
        if (orientation > 3) orientation = 0;
        x += distance * movement[orientation];
        y += distance * movement[orientation + 1];
        max_x = std::max(x, max_x);
        max_y = std::max(y, max_y);
        min_x = std::min(x, min_x);
        min_y = std::min(y, min_y);
    }
    std::cout << "Problem A: " << std::abs(x) + std::abs(y) << '\n';
    
    std::vector<std::vector<bool> > visited(max_y - min_y + 1,
                                            std::vector<bool>(max_x - min_x + 1, false));
    orientation = 1, x = 0, y = 0;
    visited[0 - min_y][0 - min_x] = true;
    for (auto [turn, distance] : instructions)
    {
        bool done = false;
        orientation += turn;
        if (orientation < 0) orientation = 3;
        if (orientation > 3) orientation = 0;
        for (; distance > 0; --distance)
        {
            x += movement[orientation];
            y += movement[orientation + 1];
            if (visited[y - min_y][x - min_x])
            {
                done = true;
                break;
            }
            visited[y - min_y][x - min_x] = true;
        }
        if (done) break;
    }
    std::cout << "Problem B: " << std::abs(x) + std::abs(y) << '\n';
    return EXIT_SUCCESS;
}

