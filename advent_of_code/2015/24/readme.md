# [Day 24: It Hangs in the Balance](https://adventofcode.com/2015/day/24)

#### Problem A

You are given the weights of group of packages. You have to divide them into three groups that have exactly the same weight.

The first group must have as few packages as possible. Additionally, you'll choose the configuration which has the smallest product between the package's weights when there is a tie between the amount of packages.

What is the product of 1st group packages weights in the ideal configuration?

#### Product B

Now, you divide the packages in four groups but you still have the same constrains as before. What is the product of 1st group packages weights in the ideal configuration?

