#include <iostream>
#include <fstream>
#include <vector>
#include <numeric>
#include <limits>

long long bestScore(const std::vector<int> &weights, int ng)
{
    struct Result
    {
        int n_elements = std::numeric_limits<int>::max();
        long long prod = std::numeric_limits<long long>::max();
        bool operator<(const Result &other) const
        {
            return (n_elements < other.n_elements)
                || ((n_elements == other.n_elements) && (prod < other.prod));
        }
    };
    const int n = static_cast<int>(weights.size());
    int best_n_elements = n;
    long long best_prod = std::accumulate(weights.begin(), weights.end(), 1,
                                          std::multiplies<int>());
    int expected_weight = std::accumulate(weights.begin(), weights.end(), 0) / ng;
    auto process =
        [&](auto &&self, int i, int ne, int sum, long long prod) -> Result
    {
        if ((ne > best_n_elements)
        ||  ((ne == best_n_elements) && (prod > best_prod))) return Result();
        if (sum == expected_weight)
        {
            if (ne < best_n_elements)
            {
                best_n_elements = ne;
                best_prod = prod;
            }
            else if (prod < best_prod) best_prod = prod;
            return { ne, prod };
        }
        if ((i >= n) || (sum > expected_weight)) return Result();
        return std::min(self(self, i + 1, ne, sum, prod),
                        self(self, i + 1, ne + 1, sum + weights[i], prod * weights[i]));
    };
    return process(process, 0, 0, 0, 1).prod;
}

int main(int argc, char * * argv)
{
    std::vector<int> weights;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; file >> line; )
        weights.push_back(std::stoi(line));
    file.close();
    
    std::cout << "Problem A: " << bestScore(weights, 3) << '\n';
    std::cout << "Problem B: " << bestScore(weights, 4) << '\n';
    return EXIT_SUCCESS;
}

