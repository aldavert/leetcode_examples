#include <iostream>
#include <fstream>
#include <vector>
#include <array>

enum class ID { NOPE, HLF, TPL, INC, JMP, JIE, JIO };
struct Instruction
{
    ID id = ID::NOPE;
    int reg = 0;
    int offset = 0;
    friend std::ostream& operator<<(std::ostream &out, const Instruction &ins)
    {
        switch (ins.id)
        {
        case ID::NOPE:
            out << "----";
            break;
        case ID::HLF:
            out << "HLF " << ins.reg;
            break;
        case ID::TPL:
            out << "TPL " << ins.reg;
            break;
        case ID::INC:
            out << "INC " << ins.reg;
            break;
        case ID::JMP:
            out << "JMP " << ins.offset;
            break;
        case ID::JIE:
            out << "JIE " << ins.reg << ", " << ins.offset;
            break;
        case ID::JIO:
            out << "JIO " << ins.reg << ", " << ins.offset;
            break;
        default:
            out << "----";
        }
        return out;
    }
};


void run(const std::vector<Instruction> &program, std::array<long, 2> &registers)
{
    const int n = static_cast<int>(program.size());
    int pc = 0;
    while ((pc >= 0) && (pc < n))
    {
        switch (program[pc].id)
        {
        case ID::NOPE:
            break;
        case ID::HLF:
            registers[program[pc].reg] /= 2;
            ++pc;
            break;
        case ID::TPL:
            registers[program[pc].reg] *= 3;
            ++pc;
            break;
        case ID::INC:
            registers[program[pc].reg] += 1;
            ++pc;
            break;
        case ID::JMP:
            pc += program[pc].offset;
            break;
        case ID::JIE:
            pc += ((registers[program[pc].reg] % 2) == 0)?program[pc].offset:1;
            break;
        case ID::JIO:
            pc += (registers[program[pc].reg] == 1)?program[pc].offset:1;
            break;
        default:
            break;
        }
    }
}

int main(int argc, char * * argv)
{
    std::array<long, 2> registers = {0, 0};
    std::vector<Instruction> program;
    
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        std::string current = line.substr(0, 3);
        if (current.empty()) continue;
        if (current == "hlf")
            program.push_back({ID::HLF, line[4] - 'a', 0});
        else if (current == "tpl")
            program.push_back({ID::TPL, line[4] - 'a', 0});
        else if (current == "inc")
            program.push_back({ID::INC, line[4] - 'a', 0});
        else if (current == "jmp")
            program.push_back({ID::JMP, -1, std::stoi(line.substr(4))});
        else if (current == "jie")
            program.push_back({ID::JIE, line[4] - 'a', std::stoi(line.substr(7))});
        else if (current == "jio")
            program.push_back({ID::JIO, line[4] - 'a', std::stoi(line.substr(7))});
        else
        {
            std::cerr << "[ERROR] Instruction '" << current << "' is unknown.\n";
            return EXIT_FAILURE;
        }
    }
    run(program, registers);
    std::cout << "Program A: " << registers[1] << '\n';
    registers[0] = 1;
    registers[1] = 0;
    run(program, registers);
    std::cout << "Program B: " << registers[1] << '\n';
    
    return EXIT_SUCCESS;
}

