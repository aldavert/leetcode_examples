# [Day 23: Opening the Turing Lock](https://adventofcode.com/2015/day/23)

#### Problem A

You have a computer with two registers, `a` and `b`, and the following instructions:

- `hlf r` halves the values of register `r`.
- `tpl r` triples the value of register `r`.
- `inc r` increases by 1 the value of register `r`.
- `jmp offset` jumps `offset` instructions relative to itself. `offset` can be negative.
- `jie r, offset` like `jmp` but only jumps if `r` is even.
- `jio r, offset` like `jmp` but only jumps if `r` is `1`.

The program stops executing when it reaches an undefined instruction (out of memory). Both registers start at 0.

What is the value of register `b` after executing the program?

#### Program B

If register `a` starts at `1`, what is now the value of register `b` after executing the program?

