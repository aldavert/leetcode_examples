#include <iostream>
#include <fstream>
#include <sstream>
#include <cctype>
#include <unordered_map>
#include <vector>
#include <limits>

int main([[maybe_unused]] int argc, [[maybe_unused]] char * * argv)
{
    std::unordered_map<std::string, size_t> attribute_to_id;
    std::vector<std::vector<int> > descriptors;
    std::vector<std::string> descriptor_id;
    std::ifstream file("data.txt");
    auto filter = [](auto &&func, std::string &text) -> void
    {
        while (!text.empty() && (!func(text.back()))) text.pop_back();
    };
    auto identifier = [&](std::string attribute)
    {
        if (auto search = attribute_to_id.find(attribute);
            search == attribute_to_id.end())
        {
            size_t id = attribute_to_id.size();
            for (auto &descriptor : descriptors)
                while (descriptor.size() <= id)
                    descriptor.push_back(-1);
            attribute_to_id[attribute] = id;
            return id;
        }
        else return search->second;
    };
    descriptor_id.push_back("QUERY");
    descriptors.push_back({});
    descriptors.back()[identifier("children")] = 3;
    descriptors.back()[identifier("cats")] = 7;
    descriptors.back()[identifier("samoyeds")] = 2;
    descriptors.back()[identifier("pomeranians")] = 3;
    descriptors.back()[identifier("akitas")] = 0;
    descriptors.back()[identifier("vizslas")] = 0;
    descriptors.back()[identifier("goldfish")] = 5;
    descriptors.back()[identifier("trees")] = 3;
    descriptors.back()[identifier("cars")] = 2;
    descriptors.back()[identifier("perfumes")] = 1;
    for (std::string line; std::getline(file, line); )
    {
        std::istringstream parser(line);
        std::string attribute, value;
        parser >> attribute >> value;
        filter([](char val) -> bool { return std::isalpha(val); }, attribute);
        filter([](char val) -> bool { return std::isdigit(val); }, value);
        descriptor_id.push_back(value);
        descriptors.push_back(std::vector<int>(attribute_to_id.size(), -1));
        while (parser >> attribute >> value)
        {
            filter([](char val) -> bool { return std::isalpha(val); }, attribute);
            filter([](char val) -> bool { return std::isdigit(val); }, value);
            descriptors.back()[identifier(attribute)] = std::stoi(value);
        }
    }
    file.close();
    size_t selected = 0;
    int min_distance = std::numeric_limits<int>::max();
    for (size_t i = 1; i < descriptors.size(); ++i)
    {
        int distance = 0;
        for (size_t j = 0; j < attribute_to_id.size(); ++j)
            if (descriptors[i][j] != -1)
                distance += std::abs(descriptors[0][j] - descriptors[i][j]);
        if (distance < min_distance)
        {
            min_distance = distance;
            selected = i;
        }
    }
    std::cout << "Problem A: " << descriptor_id[selected] << '\n';
    selected = 0;
    min_distance = std::numeric_limits<int>::max();
    size_t greater[] = {attribute_to_id["trees"], attribute_to_id["cats"]};
    size_t lower[] = {attribute_to_id["pomeranians"], attribute_to_id["goldfish"]};
    for (size_t i = 1; i < descriptors.size(); ++i)
    {
        int distance = 0;
        for (size_t j = 0; j < attribute_to_id.size(); ++j)
        {
            if (descriptors[i][j] != -1)
            {
                if ((j == greater[0]) || (j == greater[1]))
                {
                    if (descriptors[i][j] <= descriptors[0][j])
                        distance += 100'000;
                }
                else if ((j == lower[0]) || (j == lower[1]))
                {
                    if (descriptors[i][j] >= descriptors[0][j])
                        distance += 100'000;
                }
                else distance += std::abs(descriptors[0][j] - descriptors[i][j]);
            }
        }
        if (distance < min_distance)
        {
            min_distance = distance;
            selected = i;
        }
    }
    std::cout << "Problem B: " << descriptor_id[selected] << '\n';
#if 0
    std::cout << "# Descriptors: " << descriptor_id.size() << '\n';
    std::cout << "# Attributes: " << attribute_to_id.size() << '\n';
    std::cout << "Attributes:";
    for (auto [name, idx] : attribute_to_id)
        std::cout << ' ' << name;
    std::cout << '\n';
    for (size_t i = 0; i < 10; ++i)
    {
        std::cout << descriptor_id[i];
        for (size_t j = 0; j < attribute_to_id.size(); ++j)
            std::cout << ' ' << descriptors[i][j];
        std::cout << '\n';
    }
#endif
    return EXIT_SUCCESS;
}

