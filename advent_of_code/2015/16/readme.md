# [Day 16: Aunt Sue](https://adventofcode.com/2015/day/16)

#### Problem A

You are given a list of objects with their categorical description. You are also given the categorical description of a query. Return the number of the more similar object in the list.




