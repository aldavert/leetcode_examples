#include <iostream>
#include <fstream>
#include <limits>
#include <vector>
#include <unordered_set>
#include <functional>

int solve(int boss_hitpoints, int boss_damage, bool constant_drain)
{
    constexpr static short COST[5] = {53, 73, 113, 173, 229};
    struct Game
    {
        Game(int boss_hp, int boss_dmg) :
            boss_hitpoints(static_cast<short>(boss_hp)),
            boss_damage(static_cast<char>(boss_dmg)) {}
        short cost = 0;
        short mana = 500;
        short hitpoints  = 50;
        short boss_hitpoints { 0 };
        char armor = 0;
        char boss_damage = 0;
        char time_shield = 0;
        char time_poison = 0;
        char time_recharge = 0;
    };
    auto hashFunc = [](const Game &g) -> int64_t
    {
        int64_t * ptr = const_cast<int64_t *>(reinterpret_cast<const int64_t *>(&g));
        return *ptr ^ static_cast<int64_t>(*reinterpret_cast<int *>(ptr + 1));
    };
    std::function<size_t(const Game &, const Game &)> equalFunc =
        [&](const Game &g1, const Game &g2)
    {
        return hashFunc(g1) == hashFunc(g2);
    };
    short best = std::numeric_limits<short>::max();
    std::unordered_set<Game, decltype(hashFunc), decltype(equalFunc)> states(10, hashFunc, equalFunc);
    auto effect = [](Game &g, short &b) -> void
    {
        g.mana += static_cast<short>(101 * (--g.time_recharge >= 0));
        g.boss_hitpoints -= static_cast<short>(3 * (--g.time_poison >= 0));
        g.armor = static_cast<char>(7 * (--g.time_shield >= 0));
        if (g.boss_hitpoints <= 0)
        {
            b = std::min(b, g.cost);
            return;
        }
    };
    
    auto process = [&](auto &&self, Game g, int spell) -> void
    {
        if ((g.mana < COST[spell]) || (g.cost + COST[spell] > best))
            return;
        g.mana -= COST[spell];
        g.cost += COST[spell];
        if      (spell == 0) g.boss_hitpoints -= 4;
        else if (spell == 1) g.boss_hitpoints -= 2, g.hitpoints += 2;
        else if (spell == 2)
        {
            if (g.time_shield <= 0) g.time_shield = 6;
            else return;
        }
        else if (spell == 3)
        {
            if (g.time_poison <= 0) g.time_poison = 6;
            else return;
        }
        else
        {
            if (g.time_recharge <= 0) g.time_recharge = 5;
            else return;
        }
        effect(g, best);
        if ((g.hitpoints -= (constant_drain)) <= 0) return;
        effect(g, best);
        g.hitpoints -= static_cast<short>(std::max(1, g.boss_damage - g.armor));
        if (g.hitpoints <= 0) return;
        
        if (!states.insert(g).second) return;
        for (int i = 0; i < 5; ++i)
            self(self, g, i);
    };
    for (int spell = 0; spell < 5; ++spell)
        process(process, {boss_hitpoints, boss_damage}, spell);
    return best;
}

int main(int argc, char * * argv)
{
    int boss_hitpoints = 0, boss_damage = 0;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.substr(0, 11) == "Hit Points:")
            boss_hitpoints = std::stoi(line.substr(12));
        else if (line.substr(0, 7) == "Damage:")
            boss_damage = std::stoi(line.substr(8));
    }
    file.close();
    
    std::cout << "Problem A: " << solve(boss_hitpoints, boss_damage, false) << '\n';
    std::cout << "Problem B: " << solve(boss_hitpoints, boss_damage, true) << '\n';
    return EXIT_SUCCESS;
}

