# [Day 22: Wizard Simulator 20XX](https://adventofcode.com/2015/day/22)

#### Problem A

You are playing an RPG game where you are a wizard that is fighting a boss monster. The wizard has `500` mana and `50` hit points. You have the following spells:

- **Magic missile** costs `53` mana. It instantly does `4` damage.
- **Drain** costs `73` mana. It instantly does `2` damage and heals you for `2` hit points.
- **Shield** costs `113` mana. It starts an effect that lasts for `6` turns. While it is active, your armor is increased by `7`.
- **Poison** costs `173` mana. It starts an effect that lasts for `6` turns. At the start of each turn while it is active, it deals the boss `3` damage.
- **Recharge** costs `229` mana. It starts an effect that lasts for `5` turns. As the start of each turn while it is active, it gives you `101` new mana.

If you run out of hit points or you cannot cast any spell, you lose.

What is the minimum amount of mana needed to win the fight?

#### Problem B

Same setting as before but now the player loses 1 hit point at every turn.

What is the now minimum amount of mana needed to win the fight?


