#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

int main(int argc, char * * argv)
{
    struct Reindeer
    {
        std::string name;
        int speed;
        int time;
        int rest;
    };
    std::vector<Reindeer> reindeer;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line);)
    {
        std::string name, token;
        int speed, time, rest;
        std::istringstream iss(line);
        iss >> name;
        iss >> token >> token;
        iss >> speed;
        iss >> token >> token;
        iss >> time;
        iss >> token >> token >> token >> token >> token >> token;
        iss >> rest;
        iss >> token;
        reindeer.push_back({name, speed, time, rest});
    }
    file.close();
    constexpr int threshold = 2'503;
    int problem_a = 0;
    for (const auto &[name, speed, time, rest] : reindeer)
    {
        int runtime = 0, distance = 0;
        runtime = threshold / (time + rest);
        distance = runtime * time * speed;
        runtime *= time + rest;
        distance += speed * std::min(time, threshold - runtime);
        problem_a = std::max(problem_a, distance);
    }
    // -----------------------------------------------------------
    struct Status
    {
        bool running = false;
        int remaining_time = 0;
    };
    std::vector<int> points(reindeer.size()), distance(reindeer.size());
    std::vector<Status> status(reindeer.size());
    for (int t = 0; t < threshold; ++t)
    {
        int max_distance = 0;
        for (size_t i = 0; i < reindeer.size(); ++i)
        {
            if (!status[i].remaining_time)
            {
                status[i].running = !status[i].running;
                status[i].remaining_time = (status[i].running)
                                         ?reindeer[i].time
                                         :reindeer[i].rest;
            }
            distance[i] += (status[i].running) * reindeer[i].speed;
            --status[i].remaining_time;
            max_distance = std::max(max_distance, distance[i]);
        }
        for (size_t i = 0; i < reindeer.size(); ++i)
            points[i] += (distance[i] == max_distance);
    }
    int problem_b = 0;
    for (size_t i = 0; i < reindeer.size(); ++i)
        problem_b = std::max(problem_b, points[i]);
    std::cout << "Problem A: " << problem_a << '\n';
    std::cout << "Problem B: " << problem_b << '\n';
    return EXIT_SUCCESS;
}

