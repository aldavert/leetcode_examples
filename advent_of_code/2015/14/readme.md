# [Day 14: Reindeer Olympics](https://adventofcode.com/2015/day/14)

#### Problem A

You are given a list with the speed, flight time and rest time of different reindeer. Compute the **maximum distance** traveled by the any reindeer after `2503` seconds.

#### Problem B

Now, each reindeer receives a point per second when they are on the lead (it can be multiple). What is the **maximum amount of points** accumulated by a reindeer after `2503` seconds?


