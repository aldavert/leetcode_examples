#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <limits>

int main(int argc, char * * argv)
{
    std::unordered_map<std::string, size_t> city_to_id;
    std::vector<std::string> id_to_city;
    std::vector<std::vector<int> > costs;
    auto increase = [&](std::string city) -> size_t
    {
        if (auto search = city_to_id.find(city);
            search == city_to_id.end())
        {
            size_t id = city_to_id.size();
            city_to_id[city] = id;
            costs.push_back(std::vector<int>(id + 1));
            for (size_t i = 0; i < id; ++i)
                costs[i].push_back(0);
            return id;
        }
        return city_to_id[city];
    };
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.size() < 10) continue;
        std::string destination, source, token, is_gain;
        int gain;
        std::istringstream iss(line);
        iss >> destination >> token >> is_gain >> gain;
        if (is_gain == "lose") gain = - gain;
        iss >> token >> token >> token >> token >> token >> token >> source;
        if (source.back() == '.') source.pop_back();
        size_t destination_id = increase(destination),
               source_id = increase(source);
        costs[destination_id][source_id] = gain;
    }
    file.close();
    auto maxHappiness = [&](void) -> int
    {
        const int n = static_cast<int>(city_to_id.size());
        std::vector<int> available(n, true);
        std::vector<int> selected(n, -1);
        auto score = [&](void) -> int
        {
            int result = costs[selected[0    ]][selected[n - 1]]
                       + costs[selected[0    ]][selected[1    ]]
                       + costs[selected[n - 1]][selected[n - 2]]
                       + costs[selected[n - 1]][selected[0    ]];
            for (int i = 1; i < n - 1; ++i)
                result += costs[selected[i]][selected[i - 1]]
                       +  costs[selected[i]][selected[i + 1]];
            return result;
        };
        auto populate = [&](auto &&self, int idx) -> int
        {
            int result = std::numeric_limits<int>::lowest();
            if (idx == n - 2)
            {
                for (int i = 0; i < n; ++i)
                {
                    if (available[i])
                    {
                        selected[n - 2] = i;
                        break;
                    }
                }
                return score();
            }
            for (int i = 0; i < n; ++i)
            {
                if (!available[i]) continue;
                available[i] = false;
                selected[idx] = i;
                result = std::max(result, self(self, idx + 1));
                available[i] = true;
            }
            return result;
        };
        int result = std::numeric_limits<int>::lowest();
        available[0] = false;
        selected[0] = 0;
        for (int i = 1; i < n; ++i)
        {
            selected[1] = i;
            available[i] = false;
            for (int j = 2; j < n; ++j)
            {
                if (!available[j]) continue;
                selected[n - 1] = j;
                available[j] = false;
                result = std::max(result, populate(populate, 2));
                available[j] = true;
            }
            available[i] = true;
        }
        return result;
    };
    std::cout << "Problem A: " << maxHappiness() << '\n';
    increase("YOU");
    std::cout << "Problem B: " << maxHappiness() << '\n';
    return EXIT_SUCCESS;
}

