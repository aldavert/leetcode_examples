# [Day 13: Knights of the Dinner Table](https://adventofcode.com/2015/day/13)

#### Problem A

You are given a list of relationships information where it specifies the amount of happiness lost or gained depending *who sits next to who*. The table is circular, so every person exactly has two neighbors.

What is the **maximum amount of happiness** that can be gained by seating people the optimal configuration.

#### Problem B

You add yourself into the relationships list and you have a 0 happiness gain respect all other participants. What is the maximum happiness now?

