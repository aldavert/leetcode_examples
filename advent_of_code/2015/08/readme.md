# [Day 8: Matchsticks](https://adventofcode.com/2015/day/8)

# Problem A

You are given a list of strings. Return the difference between the number of bytes needed to represent the strings in *code* and the bytes needed to represent the strings in *memory*.

# Problem B

Now, first rewrite each string as if they were code strings (i.e. replace each `"` with a `\"` and `\` with a `\\`). What is now the difference between the number of bytes needed to represents the *new strings* and the number of bytes needed to represent the strings in *code*?


