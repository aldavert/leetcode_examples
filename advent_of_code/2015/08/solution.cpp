#include <iostream>
#include <fstream>

int main([[maybe_unused]] int argc, [[maybe_unused]] char * * argv)
{
    std::string filename = "data.txt";
    if (argc > 1)
        filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    int new_size = 0, code_size = 0, string_size = 0;
    for (std::string line; std::getline(file, line);)
    {
        code_size += static_cast<int>(line.size());
        new_size += static_cast<int>(line.size()) + 4;
        size_t n = line.size() - 1;
        for (; line[n] != '"'; --n);
        for (size_t i = 1; i < n; ++i)
        {
            ++string_size;
            if (line[i] == '\\')
            {
                new_size += 1 + (line[i + 1] != 'x');
                i += 1 + (line[i + 1] == 'x') * 2;
            }
        }
    }
    file.close();
    std::cout << "Problem A: " << code_size - string_size << '\n';
    std::cout << "Problem B: " << new_size - code_size << '\n';
    return EXIT_SUCCESS;
}


