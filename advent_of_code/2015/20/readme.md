# [Day 20: Infinite Elves and Infinite Houses](https://adventofcode.com/2015/day/20)

#### Problem A

You have a set of agents that visit locations based on their number. For example,
- Agent `1` visits locations `1, 2, 3, 4, ...`
- Agent `2` visits locations `2, 4, 6, 8, ...`
- Agent `3` visits locations `3, 6, 9, 12, ...`

The score at a certain location corresponds to the sum of the agents that visited each location multiplied by `10`.

What is the first location that reaches the score `29000000`?

#### Problems B

Now agents only visit the first `50` locations and multiply by `11` instead of by `10`.

What is the first location that reaches the score `29000000`?


