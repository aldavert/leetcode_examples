#include <iostream>
#include <vector>

int main([[maybe_unused]] int argc, [[maybe_unused]] char * * argv)
{
    constexpr int LIMIT = 1'000'000;
    int goal = 29'000'000;
    std::vector<int> sieveA(LIMIT);
    for (int i = 1; i < LIMIT; ++i)
    {
        for (int k = i; k < LIMIT; k += i)
            sieveA[k] += i * 10;
        if (sieveA[i] >= goal)
        {
            std::cout << "Problem A: " << i << '\n';
            break;
        }
    }
    std::vector<int> sieveB(LIMIT);
    {
        for (int i = 1; i < LIMIT; ++i)
        {
            for (int k = i, m = 0; (k < LIMIT) && (m < 50); ++m, k += i)
                sieveB[k] += i * 11;
            if (sieveB[i] >= goal)
            {
                std::cout << "Problem B: " << i << '\n';
                break;
            }
        }
    }
    return EXIT_FAILURE;
}

