# [Day 11: Corporate Policy](https://adventofcode.com/2015/day/11)

#### Problem A

You are given a `8` character string containing a lowercase code with the following conditions:
- It cannot contain the characters `i`, `o` or `l`.
- At least two different, non-overlapping pairs of letters, like `aa`, `bb` or `zz`.
- Include one increasing straight of at least three letters.

Given a string code, the next valid string code is computed by increasing the values as if its a number: `xx -> xy -> xz -> ya -> yb`.

What is the following valid code of `hepxcrrq`.



