#include <iostream>
#include <unordered_set>

std::string next(std::string input)
{
    for (int i = 7; i >= 0; --i)
    {
        if (input[i] == 'z') input[i] = 'a';
        else
        {
            ++input[i];
            return input;
        }
    }
    return input;
}

bool valid(std::string input)
{
    for (int i = 0; i < 8; ++i)
        if ((input[i] == 'i') || (input[i] == 'o') || (input[i] == 'l'))
            return false;
    bool available_pairs[26] = {};
    int streak = 1, pairs = 0;
    for (int i = 0; i < 7; ++i)
    {
        if (input[i] == input[i + 1])
            ++streak;
        else
        {
            if (streak == 2)
            {
                pairs += !available_pairs[input[i] - 'a'];
                available_pairs[input[i] - 'a'] = true;
            }
            streak = 1;
        }
    }
    if (streak == 2)
    {
        pairs += !available_pairs[input[7] - 'a'];
        available_pairs[input[7] - 'a'] = true;
    }
    int increase = 0, progress = 1;
    for (int i = 1; i < 8; ++i)
    {
        if ((input[i] == input[i - 1] + 1)
        ||  ((input[i] == 'j') && (input[i - 1] == 'h'))
        ||  ((input[i] == 'm') && (input[i - 1] == 'k'))
        ||  ((input[i] == 'p') && (input[i - 1] == 'n')))
            ++progress;
        else
        {
            increase += (progress > 2);
            progress = 1;
        }
    }
    increase += (progress > 2);
    return (pairs >= 2) && (increase >= 1);
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char * * argv)
{
    std::string code = "hepxcrrq", problem_a;
    if (argc > 1) code = argv[1];
#if 1
    for (problem_a = next(code); !valid(problem_a); problem_a = next(problem_a));
    std::cout << "Problem A: " << code << " -> " << problem_a << '\n';
#else
    std::cout << code << ' ' << valid(code) << '\n';
#endif
    
    return EXIT_SUCCESS;
}

