#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <vector>
#include <limits>

int tsp(std::vector<std::vector<int> > costs)
{
    const int n = static_cast<int>(costs.size());
    std::vector<std::vector<int> > memo(n, std::vector<int>(1 << n, -1));
    auto dp = [&](auto &&self, int mask, int current) -> int
    {
        if (mask == (1 << n) - 1) return costs[current][0];
        if (memo[current][mask] != -1) return memo[current][mask];
        int result = std::numeric_limits<int>::max();
        for (int i = 0; i < n; ++i)
            if ((mask & (1 << i)) == 0)
                result = std::min(result, costs[current][i]
                                        + self(self, mask | (1 << i), i));
        return memo[current][mask] = result;
    };
    return dp(dp, 1, 0);
}

int main(int argc, char * * argv)
{
    std::unordered_map<std::string, size_t> city_to_id;
    std::vector<std::vector<int> > costs;
    auto increase = [&](std::string city) -> size_t
    {
        if (auto search = city_to_id.find(city);
            search == city_to_id.end())
        {
            size_t id = city_to_id.size();
            city_to_id[city] = id;
            costs.push_back(std::vector<int>(id + 1));
            for (size_t i = 0; i < id; ++i)
                costs[i].push_back(0);
            return id;
        }
        return city_to_id[city];
    };
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line);)
    {
        std::istringstream iss(line);
        std::string source, destination, distance, token;
        iss >> source;
        iss >> token;
        iss >> destination;
        iss >> token;
        iss >> distance;
        size_t source_id = increase(source),
               destination_id = increase(destination);
        int distance_value = std::stoi(distance);
        costs[source_id][destination_id] = distance_value;
        costs[destination_id][source_id] = distance_value;
    }
    file.close();
    increase("DUMMY");
    std::cout << "Problem A: " << tsp(costs) << '\n';
    for (auto &row : costs)
        for (int &cell : row)
            cell = -cell;
    std::cout << "Problem B: " << -tsp(costs) << '\n';
    return EXIT_SUCCESS;
}

