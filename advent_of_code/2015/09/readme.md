# [Day 9: All in a Single Night](https://adventofcode.com/2015/day/9)

#### Problem A

You are given a list of distances between two locations. **Find the shortest route that passes through all the locations.**

#### Problem B

Now, **find the longest route that passes through all the locations.**


