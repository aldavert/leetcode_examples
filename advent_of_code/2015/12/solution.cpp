#include <iostream>
#include <fstream>
#include <stack>

int main(int argc, char * * argv)
{
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    int problem_a = 0, problem_b = 0;
    std::stack<std::pair<bool, int> > nested;
    int value = 0;
    bool negative = false; 
    auto update = [&]() -> void
    {
        value = (1 - 2 * negative) * value;
        nested.top().second += value;
        problem_a += value;
        value = 0;
        negative = false;
    };
    nested.push({false, 0});
    for (std::string line; std::getline(file, line);)
    {
        for (size_t i = 0; i < line.size(); ++i)
        {
            if ((line[i] == '{') || (line[i] == '['))
            {
                update();
                nested.push({false, 0});
            }
            else if (line[i] == ']')
            {
                update();
                int aux = nested.top().second;
                nested.pop();
                nested.top().second += aux;
            }
            else if (line[i] == '}')
            {
                update();
                int aux = (!nested.top().first)?nested.top().second:0;
                nested.pop();
                nested.top().second += aux;
            }
            else if (line[i] == '"')
            {
                update();
                nested.top().first = nested.top().first
                                  || (line.substr(i, 5) == "\"red\"");
            }
            else if (line[i] == '-')
            {
                update();
                negative = true;
            }
            else if ((line[i] >= '0') && (line[i] <= '9'))
            {
                value = value * 10 + (line[i] - '0');
            }
            else update();
        }
        problem_a += (1 - 2 * negative) * value;
    }
    update();
    if (!nested.top().first)
        problem_b += nested.top().second;
    file.close();
    std::cout << "Problem A: " << problem_a << '\n';
    std::cout << "Problem B: " << problem_b << '\n';
    return EXIT_SUCCESS;
}

