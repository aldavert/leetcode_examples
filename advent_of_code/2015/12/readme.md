# [Day 12: JSAbacusFramework.io](https://adventofcode.com/2015/day/12)

#### Problem A

Return the addition of all the numbers (integers, can be negative) that are present in a JSON document.

#### Problem B

Same as before, but do not take into account all the numbers present in JSON objects containing an attribute with the value `"red"`.

