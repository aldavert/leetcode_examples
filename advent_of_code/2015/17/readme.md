# [Day 17: No Such Thing as Too Much](https://adventofcode.com/2015/day/17)

#### Problem A

You are given a list of values. How many different combinations of these numbers add up to `150`?

#### Problem B

How many combinations will add up to `150` using the minimum number possible of containers?

