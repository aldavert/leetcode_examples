#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>

long combinations(const std::vector<int> containers, long target)
{
    auto traverse = [&](auto &&self, size_t idx, long sum) -> long
    {
        if (idx == containers.size())
            return target == sum;
        if (sum > target) return 0;
        return self(self, idx + 1, sum + containers[idx])
             + self(self, idx + 1, sum);
    };
    return traverse(traverse, 0, 0);
}

long minimumCombinations(const std::vector<int> containers, long target)
{
    std::vector<long> solutions_per_ncontainers(containers.size());
    auto traverse = [&](auto &&self, size_t idx, int ncontainers, long sum) -> void
    {
        if (idx == containers.size())
        {
            solutions_per_ncontainers[ncontainers] += target == sum;
            return;
        }
        if (sum > target) return;
        self(self, idx + 1, ncontainers + 1, sum + containers[idx]);
        self(self, idx + 1, ncontainers, sum);
    };
    traverse(traverse, 0, 0, 0);
    for (long ncontainers : solutions_per_ncontainers)
        if (ncontainers > 0)
            return ncontainers;
    return 0;
}

int main(int argc, char * * argv)
{
    long target = 150;
    std::vector<int> containers;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    if (argc > 2) target = std::stol(argv[2]);
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string token; file >> token; )
        containers.push_back(std::stoi(token));
    file.close();
    std::cout << "Problem A: " << combinations(containers, target) << '\n';
    std::cout << "Problem B: " << minimumCombinations(containers, target) << '\n';
    return EXIT_SUCCESS;
}

