#include <iostream>
#include <fstream>
#include <sstream>
#include <cctype>
#include <vector>
#include <unordered_map>

struct Ingredient
{
    void set(size_t index, std::string value)
    {
        while (!std::isdigit(value.back()))
            value.pop_back();
        while (index >= properties.size()) properties.push_back(0);
        properties[index] = std::stoi(value);
    }
    std::string name;
    std::vector<int> properties;
};

long score(const std::vector<Ingredient> &ingredients,
           size_t calories_idx,
           int expected_calories,
           const int total_units)
{
    const size_t n = static_cast<int>(ingredients.size()),
                 m = static_cast<int>(ingredients[0].properties.size());
    std::vector<int> units(n);
    auto compute = [&](auto &&self, size_t idx, const int sum) -> long
    {
        if (idx == n - 1)
        {
            units.back() = total_units - sum;
            if (expected_calories != -1)
            {
                long total_calories = 0;
                for (size_t i = 0; i < n; ++i)
                    total_calories += units[i] * ingredients[i].properties[calories_idx];
                if (total_calories != expected_calories)
                    return 0;
            }
            long current_score = 1;
            for (size_t j = 0; j < m; ++j)
            {
                if (j == calories_idx) continue;
                long dot = 0;
                for (size_t i = 0; i < n; ++i)
                    dot += units[i] * ingredients[i].properties[j];
                current_score *= std::max(0l, dot);
            }
            return current_score;
        }
        long result = 0;
        for (int s = 0; s < total_units - sum; ++s)
        {
            units[idx] = s;
            result = std::max(result, self(self, idx + 1, sum + s));
        }
        return result;
    };
    return compute(compute, 0, 0);
}

int main(int argc, char * * argv)
{
    constexpr int total_units = 100;
    std::vector<Ingredient> ingredients;
    std::unordered_map<std::string, size_t> property_to_id;
    auto index = [&](std::string property) -> size_t
    {
        if (auto search = property_to_id.find(property); search == property_to_id.end())
        {
            size_t id = property_to_id.size();
            property_to_id[property] = id;
            return id;
        }
        else return search->second;
    };
    
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be openend.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        Ingredient ingredient;
        std::istringstream iss(line);
        std::string property_name, property_value;
        iss >> ingredient.name;
        while (!std::isalpha(ingredient.name.back()))
            ingredient.name.pop_back();
        for (int i = 0; i < 5; ++i)
        {
            iss >> property_name >> property_value;
            ingredient.set(index(property_name), property_value);
        }
        ingredients.push_back(ingredient);
    }
    file.close();
    std::cout << "Problem A: "
              << score(ingredients, index("calories"), -1, total_units) << '\n';
    std::cout << "Problem B: "
              << score(ingredients, index("calories"), 500, total_units) << '\n';
    return EXIT_SUCCESS;
}

