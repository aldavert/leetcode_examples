# [Day 15: Science for Hungry People](https://adventofcode.com/2015/day/15)

#### Problem A

You are given a list of ingredients and its associated properties. You have to combine 100 units of these ingredients and **find the maximum score possible**. The score is computed as the product of the individual property scores. The property score is computed as the maximum between 0 and the sum of the products between the amount of ingredient units and the property value.

**Note:** in this problem, ignore the `calories` property.



