#include <iostream>
#include <fstream>

int main([[maybe_unused]] int argc, [[maybe_unused]] char * * argv)
{
    std::ifstream file("data.txt");
    int total_area = 0, length = 0;
    for (std::string data; file >> data;)
    {
        int side[3] = {};
        for (size_t i = 0, current = 0; i < data.size(); ++i)
        {
            if (data[i] == 'x') ++current;
            else side[current] = side[current] * 10 + data[i] - '0';
        }
        std::sort(&side[0], &side[3]);
        total_area += 2 * side[0] * side[1]
                   +  2 * side[0] * side[2]
                   +  2 * side[1] * side[2]
                   + side[0] * side[1];
        length += 2 * (side[0] + side[1]) + side[0] * side[1] * side[2];
    }
    std::cout << "Problem A: " << total_area << '\n';
    std::cout << "Problem B: " << length << '\n';
    return EXIT_SUCCESS;
}


