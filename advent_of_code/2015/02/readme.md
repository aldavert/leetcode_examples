# [Day 2: I Was Told There Would Be No Math](https://adventofcode.com/2015/day/2)

#### Problem A

You are given a list of prisms. You have to compute the sum of the areas of all prisms surfaces plus the areas of their smallest side.



