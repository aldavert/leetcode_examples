#include <iostream>
#include <fstream>
#include <cctype>
#include <fmt/core.h>

int main(int argc, char * * argv)
{
    int row = -1, column = -1;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string token; file >> token; )
    {
        if (token == "row")
        {
            file >> token;
            while (!(token.empty() || std::isdigit(token.back()))) token.pop_back();
            row = std::stoi(token);
        }
        else if (token == "column")
        {
            file >> token;
            while (!(token.empty() || std::isdigit(token.back()))) token.pop_back();
            column = std::stoi(token);
        }
    }
    file.close();
    auto calculateValue = [](long r, long c) -> long
    {
        long row_start = 1 + (r - 1) * r / 2;
        return row_start + (r) * (c - 1) + (c - 1) * c / 2;
    };
    long value = 20151125;
    for (long i = 0, n = calculateValue(row, column) - 1; i < n; ++i)
        value = (value * 252533) % 33554393;
    std::cout << "Problem A: " << value << '\n';
    
    return EXIT_SUCCESS;
}

