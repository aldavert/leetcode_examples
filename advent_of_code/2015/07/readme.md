# [Day 7: Some Assembly Required](https://adventofcode.com/2015/day/7)

#### Problem A

You are given the definitions that describe a boolean logic system. The system signals are represented by 16 bits numbers and the operations are:
- `AND`: Bitwise AND.
- `OR`: Bitwise OR.
- `NOT`: Bitwise complement
- `LSHIFT`: Shifts the signal `n` positions to the left.
- `RSHIFT`: Shifts the signal `n` positions to the right.

The syntax of the commands is:
- `<a> AND <b> -> <c>` and `<a> OR <b> -> <c>`: the outcome of the bitwise operator between the signals from wires `<a>` and `<b>` is stored in wire `<c>`.
- `NOT <a> -> <b>`: the complement of the signal on `<a>` is stored on `<b>`.
- `<a> LSHIFT <n> -> <b>` and `<a> RSHIFT <n> -> <b>`: the signal `<a>` shifted `<n>` positions to the left/right is stored on `<b>`.
- `<number> -> <wire>`: stores the value `<number>` into the wire `<wire>`.

For example, the circuit:
```
123 -> x
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT x -> h
NOT y -> i
```
after running the values on the wires are:
```
d: 72
e: 507
f: 492
g: 114
h: 65412
i: 65079
x: 123
y: 456
```

**Return the signal ultimately provided to wire** `a`.

#### Problem B

Replace the value of wire `b` with the value obtained by wire `a` in the previous problem, rerun the examination and **return the signal ultimately provided to wire** `a`.


