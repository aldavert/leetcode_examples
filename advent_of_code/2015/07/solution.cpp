#include <iostream>
#include <fstream>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <exception>
#include <memory>

class Component
{
protected:
    std::unordered_map<std::string, unsigned short> m_input;
    std::unordered_set<std::string> m_active_input;
    std::string m_destination;
public:
    Component(const std::vector<std::string> &input, const std::string &dst) :
        m_destination(dst)
    {
        for (auto wire : input)
            m_input[wire] = 0;
    }
    virtual ~Component(void) {}
    void update(const std::unordered_map<std::string, unsigned short> &signal_values)
    {
        for (auto [wire, value] : signal_values)
        {
            if (auto search = m_input.find(wire); search != m_input.end())
            {
                search->second = value;
                m_active_input.insert(wire);
            }
        }
    }
    bool canCompute(void) const { return m_input.size() == m_active_input.size(); }
    void setInput(std::string wire, unsigned short value)
    {
        if (auto search = m_input.find(wire); search != m_input.end())
        {
            m_active_input.insert(wire);
            search->second = value;
        }
        else throw std::out_of_range("Wrong input wire");
    }
    inline std::string destination(void) const { return m_destination; }
    virtual unsigned short compute(void) const = 0;
};

class ComponentAND : public Component
{
public:
    ComponentAND(const std::vector<std::string> &input, const std::string &dst) :
        Component(input, dst) {}
    unsigned short compute(void) const
    {
        unsigned short result = 0xFFFF;
        for (auto [wire, value] : m_input)
            result = result & value;
        return result;
    }
};

class ComponentOR : public Component
{
public:
    ComponentOR(const std::vector<std::string> &input, const std::string dst) :
        Component(input, dst) {}
    unsigned short compute(void) const
    {
        unsigned short result = 0;
        for (auto [wire, value] : m_input)
            result = result | value;
        return result;
    }
};

class ComponentNOT : public Component
{
public:
    ComponentNOT(const std::string &input, const std::string &dst) :
        Component({input}, dst) {}
    unsigned short compute(void) const
    {
        return ~m_input.begin()->second;
    }
};

class ComponentLSHIFT : public Component
{
    int m_shift = 0;
public:
    ComponentLSHIFT(const std::string &input, int shift, const std::string &dst) :
        Component({input}, dst),
        m_shift(shift) {}
    unsigned short compute(void) const
    {
        return m_input.begin()->second << m_shift;
    }
};

class ComponentRSHIFT : public Component
{
    int m_shift = 0;
public:
    ComponentRSHIFT(const std::string &input, int shift, const std::string &dst) :
        Component({input}, dst),
        m_shift(shift) {}
    unsigned short compute(void) const
    {
        return m_input.begin()->second >> m_shift;
    }
};

class ComponentWIRE : public Component
{
public:
    ComponentWIRE(const std::string &input, const std::string &dst) :
        Component({input}, dst) {}
    unsigned short compute(void) const
    {
        return m_input.begin()->second;
    }
};

int main([[maybe_unused]] int argc, [[maybe_unused]] char * * argv)
{
    std::unordered_map<std::string, unsigned short> signal_values;
    std::vector<std::unique_ptr<Component> > components;
    [[maybe_unused]] unsigned int number_of_numbers = 0;
    
    std::string filename = "data.txt";
    if (argc > 1)
    {
        filename = argv[1];
    }
    std::ifstream file(filename);
    for (std::string line; std::getline(file, line);)
    {
        std::string dest_op = line.substr(line.find('>') + 2);
        std::string::size_type pos = line.find(' ');
        if ((pos == 3) && (line.substr(0, 3) == "NOT"))
        {
            std::string::size_type next = line.find(' ', 4);
            std::string left_op = line.substr(pos + 1, next - pos - 1);
            std::cout << "NOT " << dest_op << "=!" << left_op << " ";
            components.push_back(std::make_unique<ComponentNOT>(left_op, dest_op));
        }
        else
        {
            std::string::size_type next = line.find(' ', pos + 1);
            std::string command = line.substr(pos + 1, next - pos - 1);
            bool enable_right_op = true;
            int command_id = -1;
            if ((command == "AND") || (command == "OR"))
            {
                if (command == "AND") command_id = 0;
                else command_id = 1;
            }
            else if ((command == "LSHIFT") || (command == "RSHIFT"))
            {
                if (command == "LSHIFT") command_id = 2;
                else command_id = 3;
            }
            else
            {
                if (command != "->")
                {
                    std::cout << "UNKNOWN COMMAND: " << command << "\n";
                    return EXIT_FAILURE;
                }
                enable_right_op = false;
                std::cout << "NUMBER ";
            }
            std::string left_op = line.substr(0, pos), right_op = "";
            if (enable_right_op)
            {
                pos = next + 1;
                next = line.find(' ', pos);
                right_op = line.substr(pos, next - pos);
                try
                {
                    if (unsigned short value = static_cast<unsigned short>(stoi(left_op));
                        std::to_string(value) == left_op)
                    {
                        left_op = "#value" + std::to_string(++number_of_numbers);
                        signal_values[left_op] = value;
                    }
                    if (unsigned short value = static_cast<unsigned short>(stoi(right_op));
                        ((command_id == 0) || (command_id == 1)) && (std::to_string(value) == right_op))
                    {
                        right_op = "#value" + std::to_string(++number_of_numbers);
                        signal_values[right_op] = value;
                    }
                }
                catch (std::invalid_argument &e)
                {
                }
                switch (command_id)
                {
                    case 0:
                    {
                        std::vector<std::string> input = {left_op, right_op};
                        components.push_back(std::make_unique<ComponentAND>(input, dest_op));
                        break;
                    }
                    case 1:
                    {
                        std::vector<std::string> input = {left_op, right_op};
                        components.push_back(std::make_unique<ComponentOR>(input, dest_op));
                        break;
                    }
                    case 2:
                        components.push_back(std::make_unique<ComponentLSHIFT>(left_op, std::stoi(right_op), dest_op));
                        break;
                    case 3:
                        components.push_back(std::make_unique<ComponentRSHIFT>(left_op, std::stoi(right_op), dest_op));
                        break;
                    default:
                        std::cout << "UNKNOWN??";
                        throw std::out_of_range("Wrong input wire");
                        break;
                }
                std::cout << "#" << dest_op << "=(" << left_op << ", " << right_op << ")  ";
            }
            else
            {
                try
                {
                    signal_values[dest_op] = static_cast<unsigned short>(std::stoi(left_op));
                }
                catch (std::invalid_argument &e)
                {
                        components.push_back(std::make_unique<ComponentWIRE>(left_op, dest_op));
                }
            }
        }
        std::cout << line << '\n';
    }
    file.close();
    while (!components.empty())
    {
        std::vector<decltype(components.begin())> to_remove;
        for (auto begin = components.begin(), end = components.end(); begin != end; ++begin)
        {
            (*begin)->update(signal_values);
            if ((*begin)->canCompute())
            {
                signal_values[(*begin)->destination()] = (*begin)->compute();
                to_remove.push_back(begin);
            }
        }
        std::reverse(to_remove.begin(), to_remove.end());
        for (auto rm : to_remove)
            components.erase(rm);
    }
    std::cout << "===========================\n";
    std::vector<std::string> set_variables;
    for (auto [variable, signal] : signal_values)
        set_variables.push_back(variable);
    std::sort(set_variables.begin(), set_variables.end());
    for (auto variable : set_variables)
        std::cout << variable << " -> " << signal_values[variable] << '\n';
    return EXIT_SUCCESS;
}

