# [Day 10: Elves Look, Elves Say](https://adventofcode.com/2015/day/10)

#### Problem A

Given a number you have to play a game where you change the representation of the number with its run length representation at each iteration:
1. `1` becomes `11`: 1 instance of 1.
2. `11` becomes `21`: 2 instances of 1.
3. `21` becomes `1211`: 1 instance of 2, 1 instance of 1.
4. `1211` becomes `111221`: 1 instance of 1, 1 instance of 2, 2 instances of 1.
5. `111221` becomes `312211`: 3 instances of 1, 2 instances of 2, 1 instance of 1.

Return the length of the code when starting with `1113222113` after 40 iterations.






