#include <iostream>

int main([[maybe_unused]] int argc, [[maybe_unused]] char * * argv)
{
    std::string input = "1113222113";
    std::string process[2] = { input, "" };
    bool second = false;
    for (int iter = 0; iter < 40; ++iter, second = !second)
    {
        char previous = process[second][0];
        int count = 1;
        process[!second] = "";
        for (size_t i = 1; i < process[second].size(); ++i)
        {
            if (process[second][i] == previous) ++count;
            else
            {
                process[!second] += std::to_string(count);
                process[!second] += previous;
                count = 1;
                previous = process[second][i];
            }
        }
        process[!second] += std::to_string(count);
        process[!second] += previous;
    }
    std::cout << "Problem A: " << process[second].size() << '\n';
    for (int iter = 0; iter < 10; ++iter, second = !second)
    {
        char previous = process[second][0];
        int count = 1;
        process[!second] = "";
        for (size_t i = 1; i < process[second].size(); ++i)
        {
            if (process[second][i] == previous) ++count;
            else
            {
                process[!second] += std::to_string(count);
                process[!second] += previous;
                count = 1;
                previous = process[second][i];
            }
        }
        process[!second] += std::to_string(count);
        process[!second] += previous;
    }
    std::cout << "Problem B: " << process[second].size() << '\n';
    return EXIT_SUCCESS;
}

