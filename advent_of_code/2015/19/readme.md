# [Day 19: Medicine for Rudolph](https://adventofcode.com/2015/day/19)

#### Problem A

You are given a set of replacement strings (e.g. `<source> => <destination>`) that define different ways that the substrings can be replaced. If you can only do one replacement, how many unique strings can generate after doing a single replacement?

#### Problem B

Starting with a `e`, what is the minimum number of steps (replacements) needed to generate the query string?

