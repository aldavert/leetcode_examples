#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <limits>

std::string replace(const std::string &source,
                    std::string::size_type pos,
                    size_t size,
                    const std::string &substr)
{
    std::string result;
    if (pos > 0)
        result = source.substr(0, pos);
    result += substr;
    result += source.substr(pos + size);
    return result;
}

int generateCompound(const std::unordered_map<std::string,
                                              std::vector<std::string> > &replacement,
                     std::string goal)
{
    auto cmp = [](const auto &left, const auto &right)
    {
        return left.first.size() > right.first.size();
    };
    std::vector<std::pair<std::string, std::string> > reverse;
    bool can_combine_e = false;
    for (auto [source, next] : replacement)
    {
        for (std::string r : next)
        {
            reverse.push_back({r, source});
            can_combine_e = can_combine_e || (r.find("e") != std::string::npos);
        }
    }
    std::sort(reverse.begin(), reverse.end(), cmp);
    int result = std::numeric_limits<int>::max();
    std::unordered_map<size_t, int> memo;
    auto process = [&](auto &&self, std::string current, int steps) -> void
    {
        if (steps >= result) return;
        if (current.size() > goal.size() + 20) return;
        if (current == "e")
        {
            result = std::min(result, steps);
            return;
        }
        if (!can_combine_e && (current.find("e") != std::string::npos)) return;
        if (auto search = memo.find(current.size()); search != memo.end())
        {
            if (search->second >= steps) return;
            search->second = steps;
        }
        else memo[current.size()] = steps;
        for (auto [src, dst] : reverse)
        {
            for (auto search = current.find(src);
                 search != std::string::npos;
                 search = current.find(src, search + 1))
            {
                self(self, replace(current, search, src.size(), dst), steps + 1);
            }
        }
    };
    process(process, goal, 0);
    return result;
}

int main(int argc, char * * argv)
{
    std::unordered_map<std::string, std::vector<std::string> > replacements;
    std::string compound;
    
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cout << "[ERROR] File '" << filename << "' cannot be loaded.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.empty()) continue;
        if (auto search = line.find('='); search != std::string::npos)
        {
            std::string left = line.substr(0, search);
            while (!(left.empty() || std::isalpha(left.back()))) left.pop_back();
            std::string right = line.substr(line.find(' ', search) + 1);
            while (!(right.empty() || std::isalpha(right.front()))) right.erase(0, 1);
            replacements[left].push_back(right);
        }
        else compound = line;
    }
    file.close();
    std::unordered_set<std::string> unique;
    for (const auto &[left, right] : replacements)
    {
        for (auto search = compound.find(left);
             search != std::string::npos;
             search = compound.find(left, search + 1))
            for (std::string new_string : right)
                unique.insert(replace(compound, search, left.size(), new_string));
    }
    std::cout << "Problem A: " << unique.size() << '\n';
    std::cout << "Problem B: " << generateCompound(replacements, compound) << '\n';
    return EXIT_SUCCESS;
}

