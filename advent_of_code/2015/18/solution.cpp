#include <iostream>
#include <fstream>
#include <vector>

std::vector<std::string> iteration(std::vector<std::string> grid,
                                   bool borders = false)
{
    constexpr int direction[] = {-1, -1, 0, -1, 1, 0, 1, 1, -1};
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>(grid[0].size());
    std::vector<std::string> result(n_rows, std::string(n_cols, '.'));
    if (borders)
    {
        grid[0][0] = grid[n_rows - 1][0] = '#';
        grid[0][n_cols - 1] = grid[n_rows - 1][n_cols - 1] = '#';
        result[0][0] = result[n_rows - 1][0] = '#';
        result[0][n_cols - 1] = result[n_rows - 1][n_cols - 1] = '#';
    }
    for (int row = 0; row < n_rows; ++row)
    {
        for (int col = 0; col < n_cols; ++col)
        {
            int count = 0;
            for (int d = 0; d < 8; ++d)
            {
                int y = row + direction[d],
                    x = col + direction[d + 1];
                if ((x < 0) || (y < 0) || (x >= n_cols) || (y >= n_rows))
                    continue;
                count += grid[y][x] == '#';
            }
            if (((grid[row][col] == '#') && ((count == 2) || (count == 3)))
            ||  ((count == 3) && (grid[row][col] == '.')))
                result[row][col] = '#';
        }
    }
    return result;
}

int count(const std::vector<std::string> &grid)
{
    int result = 0;
    for (const std::string &line : grid)
        for (char cell : line)
            result += cell == '#';
    return result;
}

int main(int argc, char * * argv)
{
    std::vector<std::string> grid;
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "[ERROR] File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
        grid.push_back(line);
    file.close();
    std::vector<std::string> work = grid;
    for (int iter = 0; iter < 100; ++iter)
        work = iteration(work);
    std::cout << "Problem A: " << count(work) << '\n';
    work = grid;
    for (int iter = 0; iter < 100; ++iter)
        work = iteration(work, true);
    std::cout << "Problem B: " << count(work) << '\n';
    return EXIT_SUCCESS;
}

