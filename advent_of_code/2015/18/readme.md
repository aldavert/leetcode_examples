# [Day 18: Like a GIF For Your Yard](https://adventofcode.com/2015/day/18)

#### Problem A

You are given a text map indicating activated/deactivated cells. You have to compute how many cells are activated after 100 iterations of following the Conway game of life instructions:

- A cell which is **on** stays `on` when `2` or `3` neighbors are `on`, and turns `off` otherwise.
- A cell which is **off** stays `on` if exactly `3` neighbors are `on`, and stays `off` otherwise.

#### Problem B

Same as before, but now the corners of the grid are always **on**. How many cells are active after 100 iterations.


