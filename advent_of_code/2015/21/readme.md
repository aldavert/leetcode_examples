# [Day 21: RPG Simulator 20XX](https://adventofcode.com/2015/day/21)

#### Problem A

You are simulating an RPG fight with the boss. Your character has `100` hit points, an attack makes at least `1` damage (regardless of armor) and, you can buy items from a store to increase your damage and armor. You can pick a single weapon, a single piece of armor and up to 2 rings. 

What is the minimum amount of gold that you can to spend in order to win against the boss?

#### Problem B

Now, what is the maximum amount of gold that you can spend and still lose?

