#include <iostream>
#include <fstream>
#include <vector>
#include <limits>

struct Object
{
    std::string name;
    int cost = 0;
    int damage = 0;
    int armor = 0;
};

int main(int argc, char * * argv)
{
    std::vector<Object> objects[3];
    objects[0].push_back({"Dagger",        8,     4,       0});
    objects[0].push_back({"Shortsword",   10,     5,       0});
    objects[0].push_back({"Warhammer",    25,     6,       0});
    objects[0].push_back({"Longsword",    40,     7,       0});
    objects[0].push_back({"Greataxe",     74,     8,       0});
    objects[1].push_back({"None",          0,     0,       0});
    objects[1].push_back({"Leather",      13,     0,       1});
    objects[1].push_back({"Chainmail",    31,     0,       2});
    objects[1].push_back({"Splintmail",   53,     0,       3});
    objects[1].push_back({"Bandedmail",   75,     0,       4});
    objects[1].push_back({"Platemail",   102,     0,       5});
    objects[2].push_back({"None",          0,     0,       0});
    objects[2].push_back({"None",          0,     0,       0});
    objects[2].push_back({"Damage +1",    25,     1,       0});
    objects[2].push_back({"Damage +2",    50,     2,       0});
    objects[2].push_back({"Damage +3",   100,     3,       0});
    objects[2].push_back({"Defense +1",   20,     0,       1});
    objects[2].push_back({"Defense +2",   40,     0,       2});
    objects[2].push_back({"Defense +3",   80,     0,       3});
    
    int hit_points[2] = {100, 0}, damage[2] = {0, 0}, armor[2] = {0, 0};
    std::string filename = "data.txt";
    if (argc > 1) filename = argv[1];
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cout << "File '" << filename << "' cannot be opened.\n";
        return EXIT_FAILURE;
    }
    for (std::string line; std::getline(file, line); )
    {
        if (line.substr(0, 11) == "Hit Points:")
            hit_points[1] = std::stoi(line.substr(12));
        else if (line.substr(0, 7) == "Damage:")
            damage[1] = std::stoi(line.substr(8));
        else if (line.substr(0, 6) == "Armor:")
            armor[1] = std::stoi(line.substr(7));
    }
    file.close();
    auto fight = [&](void) -> bool
    {
        int hpoints[2] = { hit_points[0], hit_points[1] };
        for (bool boss = false; (hpoints[0] > 0) && (hpoints[1] > 0); boss = !boss)
        {
            hpoints[!boss] -= std::max(1, damage[boss] - armor[!boss]);
        }
        return hpoints[0] > hpoints[1];
    };
    
    auto calculateCost = [&](bool boss_wins) -> int
    {
        int cost = 0, best_cost = (boss_wins)
                                ? std::numeric_limits<int>::lowest()
                                : std::numeric_limits<int>::max();
        for (int i = 0, n = static_cast<int>(objects[0].size()); i < n; ++i)
        {
            cost      += objects[0][i].cost;
            damage[0] += objects[0][i].damage;
            armor[0]  += objects[0][i].armor;
            for (int j = 0, m = static_cast<int>(objects[1].size()); j < m; ++j)
            {
                cost      += objects[1][j].cost;
                damage[0] += objects[1][j].damage;
                armor[0]  += objects[1][j].armor;
                for (int k = 0, o = static_cast<int>(objects[2].size()); k < o; ++k)
                {
                    cost      += objects[2][k].cost;
                    damage[0] += objects[2][k].damage;
                    armor[0]  += objects[2][k].armor;
                    for (int l = k + 1; l < o; ++l)
                    {
                        cost      += objects[2][l].cost;
                        damage[0] += objects[2][l].damage;
                        armor[0]  += objects[2][l].armor;
                        if (boss_wins)
                        {
                            if ((cost > best_cost) && !fight())
                                best_cost = cost;
                        }
                        else
                        {
                            if ((cost < best_cost) && fight())
                                best_cost = cost;
                        }
                        cost      -= objects[2][l].cost;
                        damage[0] -= objects[2][l].damage;
                        armor[0]  -= objects[2][l].armor;
                    }
                    cost      -= objects[2][k].cost;
                    damage[0] -= objects[2][k].damage;
                    armor[0]  -= objects[2][k].armor;
                    if (k == 0) ++k;
                }
                cost      -= objects[1][j].cost;
                damage[0] -= objects[1][j].damage;
                armor[0]  -= objects[1][j].armor;
            }
            cost      -= objects[0][i].cost;
            damage[0] -= objects[0][i].damage;
            armor[0]  -= objects[0][i].armor;
        }
        return best_cost;
    };
    
    std::cout << "Problem A: " << calculateCost(false) << '\n';
    std::cout << "Problem B: " << calculateCost(true) << '\n';
    
    return EXIT_SUCCESS;
}

