# [Day 3: Perfectly Spherical Houses in a Vacuum](https://adventofcode.com/2015/day/3)

#### Problem A

You are given the instructions to follow a path. Return the total amount of cells that are visited by following the path.

#### Problem B

Same as before but now the path is followed by two agents and the instructions are interlaced i.e., instructions in even positions are for agent `A` while instruction in odd positions are for agent `B`. Return the number of cells visited by both agents.


