#include <iostream>
#include <fstream>
#include <unordered_map>

int main([[maybe_unused]] int argc, [[maybe_unused]] char * * argv)
{
    std::ifstream file("data.txt");
    std::string path;
    file >> path;
    file.close();
    const int n = static_cast<int>(path.size());
    auto toKey = [&n](std::pair<int, int> position) -> long
    {
        return static_cast<long>(position.first  + n) << 32
             | static_cast<long>(position.second + n);
    };
    std::unordered_map<long, int> cells_single, cells_double;
    std::pair<int, int> agent_single = {0, 0},
                        agent_double[2] = {{0, 0}, {0, 0}};
    ++cells_single[toKey(agent_single)];
    ++cells_double[toKey(agent_double[0])];
    ++cells_double[toKey(agent_double[1])];
    for (bool robot = false; char symbol : path)
    {
        if (symbol == '^')
        {
            agent_single.second += 1;
            agent_double[robot].second += 1;
        }
        else if (symbol == 'v')
        {
            agent_single.second -= 1;
            agent_double[robot].second -= 1;
        }
        else if (symbol == '<')
        {
            agent_single.first -= 1;
            agent_double[robot].first -= 1;
        }
        else if (symbol == '>')
        {
            agent_single.first += 1;
            agent_double[robot].first += 1;
        }
        else
        {
            std::cerr << "Unexpected symbol '" << symbol << "' found!\n";
            return EXIT_FAILURE;
        }
        ++cells_single[toKey(agent_single)];
        ++cells_double[toKey(agent_double[robot])];
        robot = !robot;
    }
    std::cout << "Problem A: " << cells_single.size() << '\n';
    std::cout << "Problem A: " << cells_double.size() << '\n';
    return EXIT_SUCCESS;
}

