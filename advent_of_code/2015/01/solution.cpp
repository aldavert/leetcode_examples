#include <iostream>
#include <fstream>

int main([[maybe_unused]] int argc, [[maybe_unused]] char * * argv)
{
    std::fstream file("data.txt");
    std::string data;
    file >> data;
    file.close();
    int imbalance = 0, first_negative = 0;
    bool not_set = true;
    for (int position = 1; char symbol : data)
    {
        imbalance += 1 - 2 * (symbol == ')');
        if (not_set && (imbalance < 0))
        {
            not_set = false;
            first_negative = position;
        }
        ++position;
    }
    std::cout << "Problem A: " << imbalance << '\n';
    std::cout << "Problem B: " << first_negative << '\n';
    return EXIT_SUCCESS;
}


