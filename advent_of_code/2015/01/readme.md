# [Day 1: Not Quite Lisp](https://adventofcode.com/2015/day/1)

#### Problem A

You are given a set of parenthesis and you have to count its imbalance. Each opening parenthesis `(` adds 1 to the count while, each closing parenthesis `)` subtracts 1 to the count. Return the parentheses imbalance.

#### Problem B

Find the first position in the array (which starts at 1) where the parenthesis imbalance is negative.


