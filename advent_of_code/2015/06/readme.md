# [Day 6: Probably a Fire Hazard](https://adventofcode.com/2015/day/6)

#### Problem A

You are given a set of instruction to turn on, turn of or toggle lights in a grid `1000 * 1000` grid.

Return number of lights that are **on** after processing all the instructions.

#### Problem B

Using the same instruction, now `turn on` instruction increase the intensity of the lights, `turn off` decrease the intensity of the lights and `toggle` increases the intensity of the lights by two levels.

Returns the total intensity of the lights.

