#include <iostream>
#include <fstream>
#include <vector>

int main([[maybe_unused]] int argc, [[maybe_unused]] char * * argv)
{
    auto coordinates = [](std::string text) -> std::tuple<int, int, int, int>
    {
        std::string::size_type pos = text.find(",");
        int x0 = std::stoi(text.substr(0, pos));
        std::string::size_type next = text.find(" ", pos);
        int y0 = std::stoi(text.substr(pos + 1, next - pos - 1));
        pos = text.find(" ", next + 2);
        next = text.find(",", pos);
        int x1 = std::stoi(text.substr(pos + 1, next - pos - 1));
        int y1 = std::stoi(text.substr(next + 1));
        return {x0, y0, x1, y1};
    };
    std::ifstream file("data.txt");
    std::vector<std::vector<bool> > lights_a(1000, std::vector<bool>(1000, false));
    std::vector<std::vector<int> > lights_b(1000, std::vector<int>(1000, 0));
    for (std::string line; std::getline(file, line); )
    {
        if (line.substr(0, 7) == "turn on")
        {
            auto [x0, y0, x1, y1] = coordinates(line.substr(8));
            for (int x = x0; x <= x1; ++x)
            {
                for (int y = y0; y <= y1; ++y)
                {
                    lights_a[x][y] = true;
                    ++lights_b[x][y];
                }
            }
        }
        else if (line.substr(0, 8) == "turn off")
        {
            auto [x0, y0, x1, y1] = coordinates(line.substr(9));
            for (int x = x0; x <= x1; ++x)
            {
                for (int y = y0; y <= y1; ++y)
                {
                    lights_a[x][y] = false;
                    lights_b[x][y] = std::max(0, lights_b[x][y] - 1);
                }
            }
        }
        else if (line.substr(0, 6) == "toggle")
        {
            auto [x0, y0, x1, y1] = coordinates(line.substr(7));
            for (int x = x0; x <= x1; ++x)
            {
                for (int y = y0; y <= y1; ++y)
                {
                    lights_a[x][y] = !lights_a[x][y];
                    lights_b[x][y] += 2;
                }
            }
        }
    }
    int number_of_lights_on = 0, lights_intensity = 0;
    for (int x = 0; x < 1000; ++x)
    {
        for (int y = 0; y < 1000; ++y)
        {
            number_of_lights_on += lights_a[x][y];
            lights_intensity += lights_b[x][y];
        }
    }
    std::cout << "Problem A: " << number_of_lights_on << '\n';
    std::cout << "Problem B: " << lights_intensity << '\n';
    return EXIT_SUCCESS;
}

