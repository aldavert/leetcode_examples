# [Day 5: Doesn't He Have Intern-Elves For This?](https://adventofcode.com/2015/day/5)

#### Problem A

Given a list of words, detect the amount of nice strings. A nice string has the following properties:
- It contains at least three vowels (`aeiou` only).
- It contains at least one letter that appears twice in a row.
- It does **not** contain the string: `ab`, `cd`, `pq` or `xy`.

Return the amount of nice strings in the list.

#### Problem B
Same as before but now with a different set of rules. Nice strings now must:
- Contain a pair of any two letters that appear at least twice in the string but without overlapping (i.e. the distance between the two substrings is at least of two characters).
- It constant at least one letter which repeats with exactly one letter between them.

Return the amount of nice strings in the list.

