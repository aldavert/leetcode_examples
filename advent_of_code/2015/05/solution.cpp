#include <iostream>
#include <fstream>
#include <unordered_map>

int main([[maybe_unused]] int argc, [[maybe_unused]] char * * argv)
{
    std::ifstream file("data.txt");
    int problem_a = 0, problem_b = 0;
    for (std::string line; file >> line;)
    {
        bool repeated = false, invalid = false;
        int vowels = 0;
        for (char previous = '\0'; auto current : line)
        {
            vowels += (current == 'a')
                   || (current == 'e')
                   || (current == 'i') 
                   || (current == 'o') 
                   || (current == 'u');
            repeated = repeated || current == previous;
            invalid = invalid || ((previous == 'a') && (current == 'b'))
                              || ((previous == 'c') && (current == 'd'))
                              || ((previous == 'p') && (current == 'q'))
                              || ((previous == 'x') && (current == 'y'));
            previous = current;
        }
        problem_a += repeated && (vowels >= 3) && !invalid;
        
        std::unordered_map<std::string, int> histogram;
        bool repeated_vowel = false, repeated_substring = false;
        std::string previous = line.substr(0, 2);
        ++histogram[previous];
        for (int i = 1, n = static_cast<int>(line.size()) - 1; i < n; ++i)
        {
            repeated_vowel = repeated_vowel || (line[i - 1] == line[i + 1]);
            std::string current = line.substr(i, 2);
            if (current != previous)
            {
                repeated_substring = repeated_substring || (++histogram[current] >= 2);
                previous = current;
            }
            else previous = "";
        }
        problem_b += repeated_vowel && repeated_substring;
    }
    file.close();
    std::cout << "Problem A: " << problem_a << '\n';
    std::cout << "Problem B: " << problem_b << '\n';
    // Expected: 55
    return EXIT_SUCCESS;
}

