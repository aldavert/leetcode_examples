#ifndef __UNZIP_HEADER_FILE__
#define __UNZIP_HEADER_FILE__

#include <vector>
#include <tuple>
#include <type_traits>

namespace utls
{
    template <size_t idx, size_t size, typename ...ARGS>
    void unzip(const std::vector<std::tuple<ARGS...> > &container,
               std::tuple<std::vector<ARGS>...> &unzipped)
    {
        for (size_t i = 0, n = container.size(); i < n; ++i)
            std::get<idx>(unzipped)[i] = std::get<idx>(container[i]);
        if constexpr (idx + 1 < size) unzip<idx + 1, size>(container, unzipped);
    }
    
    template <typename ...ARGS>
    auto unzip([[maybe_unused]] const std::vector<std::tuple<ARGS...> > &container)
    {
        std::tuple<std::vector<ARGS>...> result = std::make_tuple(std::vector<ARGS>(container.size())...);
        unzip<0, sizeof...(ARGS)>(container, result);
        return result;
    }
    
}

#endif

