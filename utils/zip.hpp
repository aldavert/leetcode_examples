#ifndef __ZIP_HEADER_FILE__
#define __ZIP_HEADER_FILE__

#include <vector>
#include <tuple>

namespace utls
{
    
    template <typename ...ARGS>
    auto zip(std::vector<ARGS> ...containers)
    {
        auto min = []<typename ...VALUES>(VALUES... values)
        {
            auto inner = []<typename ...VALUES2>(auto &&self, auto v1, auto v2, VALUES2 ...values2)
            {
                if constexpr (sizeof...(values2) == 0)
                    return (v1 < v2)?v1:v2;
                else return self(self, (v1 < v2)?v1:v2, values2...);
            };
            return inner(inner, values...);
        };
        size_t number_of_elements = min(std::size(containers)...);
        std::vector<std::tuple<ARGS...> > result(number_of_elements);
        for (size_t i = 0; i < number_of_elements; ++i)
            result[i] = std::make_tuple(containers[i]...);
        return result;
    }
    
}

#endif

