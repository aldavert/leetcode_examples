#ifndef __SPLIT_HEADER_FILE__
#define __SPLIT_HEADER_FILE__

#include <vector>
#include <algorithm>
#include "definitions.hpp"

namespace utls
{
    
    template <typename STRING>
    std::vector<STRING> split(const STRING &text,
                              std::vector<STRING> separators = {" ", "\n", "\t"},
                              REPLACE policy = REPLACE::OVERLAP)
    {
        if (separators.empty())
            return {text};
        const size_t n = text.size();
        std::vector<STRING> result;
        
        if (policy == REPLACE::OVERLAP)
        {
            std::vector<bool> active(n, true);
            for (size_t i = 0; i < n; ++i)
            {
                for (const auto &sep : separators)
                {
                    if ((i + sep.size() <= n)
                    &&  (text.substr(i, sep.size()) == sep))
                    {
                        for (size_t j = 0; j < sep.size(); ++j)
                            active[i + j] = false;
                    }
                }
            }
            bool previous = false;
            for (size_t i = 0; i < n; ++i)
            {
                if (active[i])
                {
                    if (previous != active[i])
                        result.push_back("");
                    result.back() += text[i];
                }
                previous = active[i];
            }
        }
        else
        {
            if (policy == REPLACE::LONGEST_TO_SHORTEST)
            {
                std::sort(separators.begin(), separators.end(),
                        [](const STRING &left, const STRING &right)
                        { return left.size() > right.size(); });
            }
            else if (policy == REPLACE::SHORTEST_TO_LONGEST)
            {
                std::sort(separators.begin(), separators.end(),
                        [](const STRING &left, const STRING &right)
                        { return left.size() < right.size(); });
            }
            size_t left = 0, right = 0;
            while (right < n)
            {
                for (auto sep : separators)
                {
                    if ((right + sep.size() <= n)
                    &&  (text.substr(right, sep.size()) == sep))
                    {
                        if (right > left)
                            result.push_back(text.substr(left, right - left));
                        left = right + sep.size();
                        right = left - 1;
                        break;
                    }
                }
                ++right;
            }
            if (right > left)
                result.push_back(text.substr(left, right - left));
        }
        return result;
    }
    
}

#endif

