#ifndef __ENUMERATE_HEADER_FILE__
#define __ENUMERATE_HEADER_FILE__

#include <vector>
#include <tuple>

namespace utls
{
    
    template <typename T>
    auto enumerate(std::vector<T> container)
    {
        std::vector<std::tuple<size_t, T> > result(container.size());
        for (size_t i = 0, n = container.size(); i < n; ++i)
            result[i] = { i, container[i] };
        return result;
    }
    
}

#endif
