#ifndef __TRIM_HEADER_FILE__
#define __TRIM_HEADER_FILE__

#include <vector>
#include <algorithm>
#include "definitions.hpp"

namespace utls
{
    template <typename STRING>
    STRING ltrim(const STRING &text, std::vector<STRING> separators = {" ", "\n", "\t"}, REPLACE policy = REPLACE::OVERLAP)
    {
        if (separators.empty()) return text;
        const size_t n = text.size();
        
        if (policy == REPLACE::OVERLAP)
        {
            std::vector<bool> active(n, true);
            size_t i;
            for (i = 0; i < n; ++i)
            {
                for (const auto &sep : separators)
                {
                    if ((i + sep.size() <= n)
                    &&  (text.substr(i, sep.size()) == sep))
                    {
                        for (size_t j = 0; j < sep.size(); ++j)
                            active[i + j] = false;
                    }
                }
                if (active[i])
                    break;
            }
            if (i == n) return "";
            return text.substr(i);
        }
        else
        {
            if (policy == REPLACE::LONGEST_TO_SHORTEST)
            {
                std::sort(separators.begin(), separators.end(),
                        [](const STRING &left, const STRING &right)
                        { return left.size() > right.size(); });
            }
            else if (policy == REPLACE::SHORTEST_TO_LONGEST)
            {
                std::sort(separators.begin(), separators.end(),
                        [](const STRING &left, const STRING &right)
                        { return left.size() < right.size(); });
            }
            size_t left = 0, right = 0;
            while (right < n)
            {
                for (auto sep : separators)
                {
                    if ((right + sep.size() <= n)
                    &&  (text.substr(right, sep.size()) == sep))
                    {
                        if (right > left)
                            return text.substr(left);
                        left = right + sep.size();
                        right = left - 1;
                        break;
                    }
                }
                ++right;
            }
            if (left == n) return "";
            return text.substr(left);
        }
    }
    
    template <typename STRING>
    STRING rtrim(const STRING &text, std::vector<STRING> separators = {" ", "\n", "\t"}, REPLACE policy = REPLACE::OVERLAP)
    {
        if (separators.empty()) return text;
        for (auto &separator : separators)
            std::reverse(separator.begin(), separator.end());
        auto result = text;
        std::reverse(result.begin(), result.end());
        result = ltrim(result, separators, policy);
        std::reverse(result.begin(), result.end());
        return result;
    }
    
    template <typename STRING>
    STRING trim(const STRING &text, std::vector<STRING> separators = {" ", "\n", "\t"}, REPLACE policy = REPLACE::OVERLAP)
    {
        return rtrim(ltrim(text, separators, policy), separators, policy);
    }
    
}

#endif

