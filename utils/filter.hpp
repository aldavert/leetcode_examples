#ifndef __FILTER_HEADER_FILE__
#define __FILTER_HEADER_FILE__

#include <vector>

namespace utls
{
    
    template <typename T>
    auto filter(auto &&function, std::vector<T> container)
    {
        std::vector<T> result;
        for (const auto &value : container)
            if (function(value))
                result.push_back(value);
        return result;
    }
    
}

#endif
