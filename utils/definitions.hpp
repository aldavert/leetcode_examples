#ifndef __DEFINITIONS_HEADER_FILE__
#define __DEFINITIONS_HEADER_FILE__

namespace utls
{
    enum class REPLACE {
        IN_ORDER,
        SHORTEST_TO_LONGEST,
        LONGEST_TO_SHORTEST,
        OVERLAP
    };
}

#endif

