#ifndef __MAP_HEADER_FILE__
#define __MAP_HEADER_FILE__

#include <vector>

namespace utls
{
    
    template <typename ...ARGS>
    auto map(auto &&function, std::vector<ARGS> ...containers)
    {
        auto min = []<typename ...VALUES>(VALUES... values)
        {
            auto inner = []<typename ...VALUES2>(auto &&self, auto v1, auto v2, VALUES2 ...values2)
            {
                if constexpr (sizeof...(values2) == 0)
                    return (v1 < v2)?v1:v2;
                else return self(self, (v1 < v2)?v1:v2, values2...);
            };
            auto single = []<typename ...VALUES2>(auto v, VALUES2 ...values2)
            {
                return v;
            };
            if constexpr (sizeof...(values) == 1) return single(values...);
            else return inner(inner, values...);
        };
        typedef decltype(function(containers.back()...)) TYPE;
        if (sizeof...(containers) == 0) return std::vector<TYPE>();
        size_t number_of_elements = min(std::size(containers)...);
        std::vector<TYPE> result(number_of_elements);
        for (size_t i = 0; i < number_of_elements; ++i)
            result[i] = function(containers[i]...);
        return result;
    }
    
}

#endif

