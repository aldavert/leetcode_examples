import json

def remove_tag(text):
    open_tag = 0
    output = ''
    for i in range(len(text)):
        if text[i] == '<':
            open_tag += 1
        elif text[i] == '>':
            open_tag -= 1
        elif open_tag == 0:
            output += text[i]
    return output
def extractCode(value_str):
    value = 0
    try:
        if ';' in value_str:
            value_str = value_str.split(';')[0]
        for code in value_str.split(','):
            value = (value << 16) + int(code.split('(')[1].split(')')[0])
    except:
        print(value_str)
    return value
if __name__ == '__main__':
    use_code = set(["shy", "lrm", "rlm", "ThickSpace", "nvlt", "bne", "nvgt",
                    "fjlig", "nrarrw", "npart", "nang", "caps", "cups", "nvsim",
                    "race", "acE", "nesim", "NotEqualTilde", "napid", "nvap",
                    "nbump", "NotHumpDownHump", "nbumpe", "NotHumpEqual", "nedot",
                    "bnequiv", "nvle", "nvge", "nlE", "nleqq", "ngE", "ngeqq",
                    "NotGreaterFullEqual", "lvertneqq", "lvnE", "gvertneqq", "gvnE",
                    "nLtv", "NotLessLess", "nLt", "nGtv", "NotGreaterGreater", "nGt",
                    "NotSucceedsTilde", "NotSubset", "nsubset", "vnsub", "NotSuperset",
                    "nsupset", "vnsup", "varsubsetneq", "vsubne", "varsupsetneq",
                    "vsupne", "NotSquareSubset", "NotSquareSuperset", "sqcaps",
                    "sqcups", "nvltrie", "nvrtrie", "nLl", "nGg", "lesg", "gesl",
                    "notindot", "notinE", "nrarrc", "NotLeftTriangleBar",
                    "NotRightTriangleBar", "ncongdot", "napE", "nleqslant",
                    "nles", "NotLessSlantEqual", "ngeqslant", "nges",
                    "NotGreaterSlantEqual", "NotNestedLessLess",
                    "NotNestedGreaterGreater", "smtes", "lates", "NotPrecedesEqual",
                    "npre", "npreceq", "NotSucceedsEqual", "nsce", "nsucceq",
                    "nsubE", "nsubseteqq", "nsupE", "nsupseteqq", "varsubsetneqq",
                    "vsubnE", "varsupsetneqq", "vsupnE", "nparsl", "DownBreve"])
    f = open('data/html_entities_table.txt', 'r')
    d = f.read()
    f.close()
    d = d.replace('\n', ' ')
    symbols = {}
    entities = {}
    for line in d.split('<tr>'):#[43:44]:
        if not '</tr>' in line:
            continue
        line = line.replace('</tr>', '')
        line = line.split('<td>')
        if len(line) < 7:
            continue
        line = list(map(lambda x: remove_tag(x.replace('</td>', '').replace('</a>', '')).strip(), line))
        code = extractCode(line[3])
        line = [line[1], line[2], line[-1]]
        if len(line[1]) != 1:
            if line[1] == '&lt;':
                line[1] = '<'
            elif line[1] == '&gt;':
                line[1] = '>'
            elif line[1] == '&amp;':
                line[1] = '&'
            elif line[1] == '&#x7c;':
                line[1] = '|'
            elif line[1][:2] == '&#' and line[1][-1] == ';':
                try:
                    line[1] = int(line[1][2:-1])
                except:
                    line[1] = None
            #else:
            #    if 'varsubsetneq' in line[0]:
            #        print("HOOOLLAA", line)
            #    line[1] = None
        else:
            if len(repr(line[1])) > 4:
                line[1] = ord(line[1])
            #elif len(repr(line[1])) > 2:
            #    print(line, repr(line[1]), len(repr(line[1])), ord(line[1]))
        if line[1] != None:
            for e in line[0].split(','):
                e = e.replace('&#91;a&#93;', '').strip()
                if e in use_code:
                    line[1] = code
                if not line[1] in symbols:
                    symbols[line[1]] = {'definition' : line[2], 'entities' : []}
                symbols[line[1]]['entities'].append(e)
                entities[e] = line[1]
    #for key in symbols:
    #    #if 'copy' in symbols[key]['entities']:
    #    print(key, symbols[key])
    f = open('data/entities.json', 'r')
    d = f.read()
    f.close()
    for entity, data in json.loads(d).items():
        if entity[-1] == ';':
            entity = entity[:-1]
        if entity[0] == '&':
            entity = entity[1:]
        #if len(data['codepoints']) != 1:
        #    print(entity, data['codepoints'], data['characters'])
        if not entity in entities:
            if data['characters'] in symbols:
                symbols[data['characters']]['entities'].append(entity)
            else:
                symbols[data['characters']] = {'definition' : '', 'entities' : [entity]}
    idx = 0
    for symbol, data in symbols.items():
        definition = data['definition']
        entities = data['entities']
        if symbol != "'":
            if type(symbol) == type(int()):
                symbol_show = f"{symbol}"
            else:
                if symbol != '\\':
                    symbol_show = "L'" + symbol + "'"
                else:
                    symbol_show = "L'\\\\'"
        else:
            symbol_show = "L'\\''"
        for entity in entities:
            length = 9 + (len(str(symbol_show)) == 5)
            #line = f"""{{{symbol_show:<{length}} , "", {'"' + entity + '"':<40}, "{definition}"}},"""
            #line = f"""m_entries[{idx:>4}] = {{{symbol_show:<{length}} , "", {'"' + entity + '"':<40}, "{definition}"}};"""
            line = f"""m_entries[{idx:>4}] = {{{symbol_show:<{length}} , "", {'"' + entity + '"'}}};"""
            idx += 1
            print(line)


