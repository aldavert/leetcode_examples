#include <vector>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
//#include <concepts>
//#include <numeric>
//#include <functional>
#include <random>
//#include <mutex>
//#include <ranges>
//#include <list>
//#include <unordered_map>
//#include <map>
//#include <unordered_set>
//#include <set>
//#include <bitset>
#include "xml.hpp"
#include "entities.hpp"

void testProxy(void)
{
    std::string data = "12.2";
    std::cout << "DATA is " << data << '\n';
    {
        short value = Proxy(data);
        std::cout << "Short: " << value << '\n';
    }
    {
        double value = Proxy(data);
        std::cout << "Double: " << value << '\n';
    }
    {
        float value = Proxy(data);
        std::cout << "Float: " << value << '\n';
    }
    data = "2039393054";
    std::cout << "DATA is " << data << '\n';
    {
        short value = Proxy(data);
        std::cout << "Short: " << value << '\n';
    }
    {
        int value = Proxy(data);
        std::cout << "Int: " << value << '\n';
    }
    {
        unsigned int value = Proxy(data);
        std::cout << "UInt: " << value << '\n';
    }
    data = "3039393054";
    std::cout << "DATA is " << data << '\n';
    {
        short value = Proxy(data);
        std::cout << "Short: " << value << '\n';
    }
    {
        int value = Proxy(data);
        std::cout << "Int: " << value << '\n';
    }
    {
        unsigned int value = Proxy(data);
        std::cout << "UInt: " << value << '\n';
    }
    data = "5039393054";
    std::cout << "DATA is " << data << '\n';
    {
        short value = Proxy(data);
        std::cout << "Short: " << value << '\n';
    }
    {
        int value = Proxy(data);
        std::cout << "Int: " << value << '\n';
    }
    {
        unsigned int value = Proxy(data);
        std::cout << "UInt: " << value << '\n';
    }
    {
        long value = Proxy(data);
        std::cout << "Long: " << value << '\n';
    }
    data = "(10,3)";
    std::cout << "DATA is " << data << '\n';
    {
        short value = Proxy(data);
        std::cout << "Short: " << value << '\n';
    }
    {
        std::complex<double> value = Proxy(data);
        std::cout << "Short: " << value << '\n';
    }
}

// ############################################################################
// ############################################################################



// ############################################################################
// ############################################################################

int main(int /***argc*/, char * * /***argv*/)
{
    //testProxy();
    //std::locale::global(std::locale("en_US.UTF-8"));
#if 0
    std::locale::global(std::locale("ca_ES.UTF-8"));
    std::cout << fmt::format("{}", 3.141592637428743) << '\n';
    std::cout.imbue(std::locale("ca_ES.UTF-8"));
    std::cout << 3.1419626 << '\n';
    auto value = u8'>';
    fmt::print("wchar_t: {}\n", value);
#endif
#if 0
    std::ifstream file("binary/dblp_mini.xml");
    std::ostringstream output;
    
    XmlParser parser(file);
#endif
#if 1
    HTMLEntities html;
    std::string original = "&rarr; Symbol: &clubs; &nopd; &error    &vsupne;   &vsupnE;   &NotSquareSubset;    &eth;";
    std::string decoded = html.decode(original);
    std::cout << "   ORIGINAL: " << original << '\n';
    std::cout << "HTML->UTF-8: " << decoded << '\n';
    std::cout << "UTF-8->HTML: " << html.encode(decoded) << '\n';
    std::cout << sizeof(wchar_t) << '\n';
#endif
    return 0;
}

