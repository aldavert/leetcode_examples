#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minPushBox(std::vector<std::vector<char> > grid)
{
    int directions[] = {0, 1, 0, -1, 0};
    struct Position
    {
        Position(void) = default;
        Position(int r, int c) : row(r), col(c) {} // Needed for CLANG.
        int row = -1;
        int col = -1;
        bool operator==(const Position &o) const
        {
            return (row == o.row) && (col == o.col);
        }
        Position operator+(const Position &o) const
        {
            return Position(row + o.row, col + o.col);
        }
    };
    const int n_rows = static_cast<int>(grid.size());
    const int n_cols = static_cast<int>(grid[0].size());
    bool seen[21][21][21][21] = {};
    Position target;
    int result = 0;
    auto isInvalid = [&](Position pos) -> bool
    {
        return (pos.row < 0) || (pos.row == n_rows)
            || (pos.col < 0) || (pos.col == n_cols)
            || grid[pos.row][pos.col] == '#';
    };
    auto canGoTo = [&](Position player, Position from, Position box) -> bool
    {
        std::queue<Position> q{{player}};
        bool visited[21][21] = {};
        visited[player.row][player.col] = true;
        while (!q.empty())
        {
            auto current = q.front();
            q.pop();
            if (current == from) return true;
            for (int k = 0; k < 4; ++k)
            {
                Position next = current + Position(directions[k], directions[k + 1]);
                if (isInvalid(next)) continue;
                if (visited[next.row][next.col]) continue;
                if (next == box) continue;
                q.push(next);
                visited[next.row][next.col] = true;
            }
        }
        return false;
    };
    Position init_box, init_player;
    for (int row = 0; row < n_rows; ++row)
        for (int col = 0; col < n_cols; ++col)
            if (grid[row][col] == 'B')
                init_box = {row, col};
            else if (grid[row][col] == 'S')
                init_player = {row, col};
            else if (grid[row][col] == 'T')
                target = {row, col};
    std::queue<std::tuple<Position, Position> > q{{{init_box, init_player}}};
    seen[init_box.row][init_box.col][init_player.row][init_player.col] = true;
    while (!q.empty())
    {
        for (int sz = static_cast<int>(q.size()); sz > 0; --sz)
        {
            const auto [box, player] = q.front();
            q.pop();
            if (box == target) return result;
            for (int k = 0; k < 4; ++k)
            {
                Position next_box = box + Position(directions[k], directions[k + 1]);
                if (isInvalid(next_box))
                    continue;
                if (seen[next_box.row][next_box.col][box.row][box.col])
                    continue;
                Position from = box + Position(directions[(k + 2) % 4],
                                               directions[(k + 3) % 4]);
                if (isInvalid(from))
                    continue;
                if (canGoTo(player, from, box))
                {
                    seen[next_box.row][next_box.col][box.row][box.col] = true;
                    q.emplace(next_box, box);
                }
            }
        }
        ++result;
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minPushBox(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'#', '#', '#', '#', '#', '#'}, 
          {'#', 'T', '#', '#', '#', '#'}, 
          {'#', '.', '.', 'B', '.', '#'}, 
          {'#', '.', '#', '#', '.', '#'}, 
          {'#', '.', '.', '.', 'S', '#'}, 
          {'#', '#', '#', '#', '#', '#'}}, 3, trials);
    test({{'#', '#', '#', '#', '#', '#'}, 
          {'#', 'T', '#', '#', '#', '#'}, 
          {'#', '.', '.', 'B', '.', '#'}, 
          {'#', '#', '#', '#', '.', '#'}, 
          {'#', '.', '.', '.', 'S', '#'}, 
          {'#', '#', '#', '#', '#', '#'}}, -1, trials);
    test({{'#', '#', '#', '#', '#', '#'}, 
          {'#', 'T', '.', '.', '#', '#'}, 
          {'#', '.', '#', 'B', '.', '#'}, 
          {'#', '.', '.', '.', '.', '#'}, 
          {'#', '.', '.', '.', 'S', '#'}, 
          {'#', '#', '#', '#', '#', '#'}}, 5, trials);
    return 0;
}



