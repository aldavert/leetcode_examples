#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countDays(int days, std::vector<std::vector<int> > meetings)
{
    int free_days = 0, prev_end = 0;
    std::sort(meetings.begin(), meetings.end());
    for (const auto &meeting : meetings)
    {
        if (meeting[0] > prev_end)
            free_days += meeting[0] - prev_end - 1;
        prev_end = std::max(prev_end, meeting[1]);
    }
    return free_days + std::max(0, days - prev_end);
}

// ############################################################################
// ############################################################################

void test(int days,
          std::vector<std::vector<int> > meetings,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countDays(days, meetings);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, {{5, 7}, {1, 3}, {9, 10}}, 2, trials);
    test(5, {{2, 4}, {1, 3}}, 1, trials);
    test(6, {{1, 6}}, 0, trials);
    return 0;
}


