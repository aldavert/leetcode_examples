#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isOneBitCharacter(std::vector<int> bits)
{
    const int n = static_cast<int>(bits.size()) - 1;
    int i = 0;
    while (i < n) i += 1 + (bits[i] == 1);
    return i == n;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> bits, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isOneBitCharacter(bits);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(   {1, 0, 0},  true, trials);
    test({1, 1, 1, 0}, false, trials);
    test(   {0, 1, 0}, false, trials);
    return 0;
}


