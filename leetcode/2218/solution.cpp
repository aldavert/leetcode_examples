#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
int maxValueOfCoins(std::vector<std::vector<int> > piles, int k)
{
    int dp[1001][2001] = {}, before[1001] = {}, after[1001] = {};
    const int n = static_cast<int>(piles.size());
    std::sort(piles.begin(), piles.end(),
              [](auto &x, auto &y) { return x.size() < y.size(); });
    piles.emplace_back();
    for (int i = 0; i <= n; ++i)
    {
        std::partial_sum(piles[i].begin(), piles[i].end(), piles[i].begin());
        before[i] = static_cast<int>(piles[i].size());
        after[i]  = static_cast<int>(piles[i].size());
    }
    for (int i = 1; i <= n; ++i)
        before[i] += before[i - 1],
        after[n - i] += after[n - i + 1];
    for (int i = n - 1; i >= 0; --i)
    {
        for(int j = std::max(1, k - before[i + 1]); j <= k; ++j)
        {
            dp[i][j] = dp[i + 1][j];
            int x = std::max(1, j - after[i + 1]);
            for(; (x <= j) && (x <= static_cast<int>(piles[i].size())); ++x)
                dp[i][j] = std::max(dp[i][j], piles[i][x - 1] + dp[i + 1][j - x]);
        }
    }
    return dp[0][k];
}
#else
int maxValueOfCoins(std::vector<std::vector<int> > piles, int k)
{
    const int n = static_cast<int>(piles.size());
    int dp_table[1001][2001] = {};
    auto dpProcess = [&](auto &&self, int i, int r) -> int
    {
        if ((i == n) || (r == 0)) return 0;
        if (dp_table[i][r]) return dp_table[i][r];
        int result = self(self, i + 1, r);
        const int m = std::min(static_cast<int>(piles[i].size()), r);
        for (int j = 0, val = 0; j < m; ++j)
            val += piles[i][j],
            result = std::max(result, val + self(self, i + 1, r - j - 1));
        return dp_table[i][r] = result;
    };
    return dpProcess(dpProcess, 0, k);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > piles,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxValueOfCoins(piles, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 100, 3}, {7, 8, 9}}, 2, 101, trials);
    test({{100}, {100}, {100}, {100}, {100}, {100}, {1, 1, 1, 1, 1, 1, 700}},
         7, 706, trials);
    test({{37, 88}, {51, 64, 65, 20, 95, 30, 26}, {9, 62, 20}, {44}}, 9, 494, trials);
    return 0;
}


