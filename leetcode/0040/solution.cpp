#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > combinationSum2(std::vector<int> candidates, int target)
{
    std::vector<std::vector<int> > result;
    std::vector<int> backtrace;
    int histogram[31] = {};
    for (int c : candidates)
        if (c <= 30)
            ++histogram[c];
    auto backtrack = [&](auto &&self, int index, int goal) -> void
    {
        if (goal < 0) return;
        if (goal == 0)
        {
            result.push_back(backtrace);
            return;
        }
        if (index > 30) return;
        for (int val = 1; val <= histogram[index]; ++val)
        {
            backtrace.push_back(index);
            self(self, index + 1, goal - val * index);
        }
        for (int val = 1; val <= histogram[index]; ++val)
            backtrace.pop_back();
        self(self, index + 1, goal);
    };
    backtrack(backtrack, 1, target);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> candidates,
          int target,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = combinationSum2(candidates, target);
    std::sort(solution.begin(), solution.end());
    std::sort(result.begin(), result.end());
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 1, 2, 7, 6, 1, 5}, 8, {{1, 1, 6}, {1, 2, 5}, {1, 7}, {2, 6}}, trials);
    test({2, 5, 2, 1, 2}, 5, {{1, 2, 2}, {5}}, trials);
    test({1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1}, 30,
         {{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
           1, 1, 1, 1, 1, 1, 1}}, trials);
    return 0;
}


