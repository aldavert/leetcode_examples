#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string convertToTitle(int columnNumber)
{
    std::string result = "";
    for (; columnNumber > 0; columnNumber = (columnNumber - 1) / 26)
        result = static_cast<char>('A' + (columnNumber - 1) % 26) + result;
    return result;
}
  

// ############################################################################
// ############################################################################

void test(int columnNumber, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = convertToTitle(columnNumber);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, "A", trials);
    test(28, "AB", trials);
    test(701, "ZY", trials);
    return 0;
}


