#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

#if 1
long long countGoodIntegers(int n, int k)
{
    if (n == 1) return 9 / k;
    std::vector<int> values;
    long long result = 0, count[10] = {}, fact[11] = {};
    auto process = [&](auto &&self, int length, int last) -> void
    {
        if (length == 0)
        {
            bool active[10] = {}, can = false;
            do
            {
                if (values[0] > 0)
                {
                    const int nv = static_cast<int>(values.size());
                    long long x = 0, y = 0;
                    for (int i = 0; i < nv; ++i)
                        x = x * 10 + values[i];
                    long long p = 1;
                    for (int i = nv - 1; i >= 0; --i, x *= 10, p *= 10)
                        y = y * 10 + values[i];
                    if (n % 2 == 1)
                        for (int i = 0; i < 10; i++)
                            active[i] = active[i] || ((x * 10 + i * p + y) % k == 0);
                    else can = can || ((x += y) % k == 0);
                }
            } while (next_permutation(values.begin(), values.end()));
            
            auto permutation = [&]
            {
                long long res = fact[n];
                for (int i = 0; i < 10; i++)
                    if (count[i] != 0)
                        res /= fact[count[i]];
                if (count[0] > 0) res = res - res * count[0] / n;
                return res;
            };
            if (n % 2 == 1)
            {
                for (int i = 0; i < 10; i++)
                {
                    if (!active[i]) continue;
                    ++count[i];
                    result += permutation();
                    --count[i];
                }
            }
            else if (can) result += permutation();
        }
        else
        {
            for (int i = last; i < 10; i++)
            {
                values.push_back(i);
                count[i] += 2;
                self(self, length - 1, i);
                count[i] -= 2;
                values.pop_back();
            }
        }
    };
    
    fact[0] = 1;
    for (int i = 1; i <= 10; ++i) fact[i] = i * fact[i - 1];
    process(process, n / 2, 0);
    return result;
}
#else
long long countGoodIntegers(int n, int k)
{
    auto factorial = [](int value) -> long
    {
        long fact = 1;
        for (int i = 2; i <= value; ++i) fact *= i;
        return fact;
    };
    const int half_length = (n + 1) / 2,
              min_half = static_cast<int>(std::pow(10, half_length - 1)),
              max_half = static_cast<int>(std::pow(10, half_length));
    long result = 0;
    std::unordered_set<std::string> seen;
    for (int num = min_half; num < max_half; ++num)
    {
        const std::string first_half = std::to_string(num),
                          second_half = {first_half.rbegin(), first_half.rend()},
                          palindrome = first_half + second_half.substr(n % 2);
        if (std::stol(palindrome) % k != 0) continue;
        std::string sorted_digits = palindrome;
        std::sort(sorted_digits.begin(), sorted_digits.end());
        if (seen.contains(sorted_digits)) continue;
        seen.insert(sorted_digits);
        int digit_count[10] = {};
        for (char c : palindrome) ++digit_count[c - '0'];
        long permutations = (n - digit_count[0]) * factorial(n - 1);
        for (int i = 0; i < 10; ++i)
            if (digit_count[i] > 1)
                permutations /= factorial(digit_count[i]);
        result += permutations;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countGoodIntegers(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 5, 27, trials);
    test(1, 4, 2, trials);
    test(5, 6, 2468, trials);
    return 0;
}


