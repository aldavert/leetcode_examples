#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
long long countOperationsToEmptyArray(std::vector<int> nums)
{
    std::pair<int, int> num_to_index[100'001];
    const int n = static_cast<int>(nums.size());
    long long result = n;
    for (int i = 0; i < n; ++i)
        num_to_index[i] = { nums[i], i };
    std::sort(num_to_index, num_to_index + n);
    for (int i = 0, j = 1, round = 0; j < n; ++i, ++j)
        result += (round += num_to_index[j].second < num_to_index[i].second);
    return result;
}
#else
long long countOperationsToEmptyArray(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    long long result = n;
    std::unordered_map<int, int> num_to_index;
    
    for (int i = 0; i < n; ++i)
        num_to_index[nums[i]] = i;
    std::sort(nums.begin(), nums.end());
    for (int i = 1; i < n; ++i)
        if (num_to_index[nums[i]] < num_to_index[nums[i - 1]])
            result += n - i;
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countOperationsToEmptyArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 4, -1}, 5, trials);
    test({1, 2, 4, 3}, 5, trials);
    test({1, 2, 3}, 3, trials);
    return 0;
}


