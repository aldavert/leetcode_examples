#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<int> minimumTime(int n,
                             std::vector<std::vector<int> > edges,
                             std::vector<int> disappear)
{
    std::vector<std::vector<std::pair<int, int> > > graph(n);
    for (const auto &e : edges)
    {
        graph[e[0]].push_back({e[1], e[2]});
        graph[e[1]].push_back({e[0], e[2]});
    }
    std::vector<int> dist(n, 1 << 30);
    dist[0] = 0;
    std::priority_queue<std::pair<int, int>,
                        std::vector<std::pair<int, int> >,
                        std::greater<std::pair<int, int> > > pq;
    pq.push({0, 0});
    while (!pq.empty())
    {
        auto [du, u] = pq.top();
        pq.pop();
        if (du > dist[u]) continue;
        for (auto [v, w] : graph[u])
        {
            if ((dist[v] > dist[u] + w) && (dist[u] + w < disappear[v]))
            {
                dist[v] = dist[u] + w;
                pq.push({dist[v], v});
            }
        }
    }
    std::vector<int> result(n);
    for (int i = 0; i < n; ++i)
        result[i] = (dist[i] < disappear[i])?dist[i]:-1;
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<int> disappear,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumTime(n, edges, disappear);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{0, 1, 2}, {1, 2, 1}, {0, 2, 4}}, {1, 1, 5}, {0, -1, 4}, trials);
    test(3, {{0, 1, 2}, {1, 2, 1}, {0, 2, 4}}, {1, 3, 5}, {0, 2, 3}, trials);
    test(2, {{0, 1, 1}}, {1, 1}, {0, -1}, trials);
    return 0;
}


