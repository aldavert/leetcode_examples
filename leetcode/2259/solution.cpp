#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string removeDigit(std::string number, char digit)
{
    const int n = static_cast<int>(number.size());
    std::string result;
    for (int i = 0; i < n; ++i)
    {
        if (number[i] == digit)
        {
            std::string putative = number.substr(0, i) + number.substr(i + 1, n - i - 1);
            if (putative > result)
                result = putative;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string number, char digit, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = removeDigit(number, digit);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("123", '3', "12", trials);
    test("1231", '1', "231", trials);
    test("551", '5', "51", trials);
    return 0;
}


