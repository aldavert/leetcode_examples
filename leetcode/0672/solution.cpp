#include "../common/common.hpp"

// ############################################################################
// ############################################################################

// Numbers obtained using leetcode's test-case test.
// n | p r | p r | p r | p r | p r
// 1 | 0 1 | 1 2 | 2 2 | 3 2 | 4 2
// 2 | 0 1 | 1 3 | 2 4 | 3 4 | 4 4
// 3 | 0 1 | 1 4 | 2 7 | 3 8 | 4 8
// 4 | 0 1 | 1 4 | 2 7 | 3 8 | 4 8
// 5 | 0 1 | 1 4 | 2 7 | 3 8 | 4 8
// 6 | 0 1 | 1 4 | 2 7 | 3 8 | 4 8
// 7 | 0 1 | 1 4 | 2 7 | 3 8 | 4 8
// 8 | 0 1 | 1 4 | 2 7 | 3 8 | 4 8

int flipLights(int n, int presses)
{
    n = std::min(n, 3);
    if (presses == 0) return 1;
    if (presses == 1) return std::min(n + 1, 4);
    if (presses == 2) return std::min(1 << n, 7);
    return 1 << n;
}

// ############################################################################
// ############################################################################

void test(int n, int presses, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = flipLights(n, presses);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 1, 2, trials);
    test(2, 1, 3, trials);
    test(3, 1, 4, trials);
    return 0;
}


