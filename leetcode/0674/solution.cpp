#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int findLengthOfLCIS(std::vector<int> nums)
{
    int previous = std::numeric_limits<int>::max();
    int result = 0, counter = 0;
    for (int n : nums)
    {
        if (previous < n) ++counter;
        else
        {
            result = std::max(result, counter);
            counter = 1;
        }
        previous = n;
    }
    return std::max(result, counter);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLengthOfLCIS(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 5, 4, 7}, 3, trials);
    test({2, 2, 2, 2, 2}, 1, trials);
    return 0;
}


