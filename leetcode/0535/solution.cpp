#include "../common/common.hpp"
#include <unordered_map>
#include <random>

// ############################################################################
// ############################################################################

class Solution
{
    std::unordered_map<std::string, std::string> m_url2code;
    std::unordered_map<std::string, std::string> m_code2url;
    constexpr static char m_base64[] = "abcdefghijklmnopqrstuvwxyz"
                                       "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                       "0123456789-=";
    std::random_device m_rd;
    std::mt19937 m_generator;
    std::uniform_int_distribution<> m_distribution;
public:
    Solution(void) :
        m_generator(m_rd()),
        m_distribution(0, 63) {}
    std::string encode(std::string longUrl)
    {
        auto generate = [&](void) -> std::string
        {
            std::string code = "        ";
            for (int i = 0; i < 8; ++i)
                code[i] = m_base64[m_distribution(m_generator)];
            return code;
        };
        if (auto search = m_url2code.find(longUrl); search != m_url2code.end())
            return search->second;
        std::string code;
        for (code = generate(); m_code2url.count(code); code = generate());
        std::string shortUrl = "http://tinyurl.com/" + code;
        m_code2url[shortUrl] = longUrl;
        m_url2code[longUrl] = shortUrl;
        return shortUrl;
    }
    std::string decode(std::string shortUrl)
    {
        return m_code2url[shortUrl];
    }
};

// ############################################################################
// ############################################################################

void test(std::string solution, unsigned int trials = 1)
{
    std::string result, tiny;
    for (unsigned int i = 0; i < trials; ++i)
    {
        Solution obj;
        
        tiny = obj.encode(solution);
        result = obj.decode(tiny);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("https://leetcode.com/problems/design-tinyurl", trials);
    return 0;
}


