#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int subtractProductAndSum(int n)
{
    int product = 1, sum = 0;
    while (n)
    {
        int digit = n % 10;
        sum += digit;
        product *= digit;
        n /= 10;
    }
    return product - sum;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = subtractProductAndSum(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test( 234, 15, trials);
    test(4421, 21, trials);
    return 0;
}


