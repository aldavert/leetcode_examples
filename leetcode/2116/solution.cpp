#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 1
bool canBeValid(std::string s, std::string locked)
{
    if (s.size() % 2) return false;
    
    std::stack<int> open, unlocked;
    for(int i = 0, n = static_cast<int>(s.size()); i < n; ++i)
    {
        if (locked[i] == '0') unlocked.push(i);
        else if(s[i] == '(') open.push(i);
        else if(s[i] == ')')
        {
            if (!open.empty()) open.pop();
            else if (!unlocked.empty()) unlocked.pop();
            else return false;
        }
    }
    while (!open.empty() && !unlocked.empty() && (open.top() < unlocked.top()))
    {
        open.pop();
        unlocked.pop();
    }
    return open.empty() && (unlocked.size() % 2 == 0);
}
#else
bool canBeValid(std::string s, std::string locked)
{
    const int n = static_cast<int>(s.size());
    auto check = [&](int direction) -> bool
    {
        for (int i = 0, changeable = 0, l = 0, r = 0; i < n; ++i)
        {
            char c = s[i], lock = locked[i];
            if (lock == '0') ++changeable;
            else if (c == '(') ++l;
            else ++r;
            if (changeable + direction * (l - r) < 0)
                return false;
        }
        return true;
    };
    if (n % 2 == 1)
        return false;
    bool left_to_right = check(1);
    std::reverse(s.begin(), s.end());
    std::reverse(locked.begin(), locked.end());
    return left_to_right && check(-1);
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, std::string locked, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canBeValid(s, locked);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("))()))", "010100", true, trials);
    test("()()", "0000", true, trials);
    test(")", "0", false, trials);
    return 0;
}


