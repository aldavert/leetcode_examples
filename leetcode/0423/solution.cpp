#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string originalDigits(std::string s)
{
    std::string result;
    int histogram[10] = {};
    for (char letter : s)
    {
        switch (letter)
        {
        /* Characters found only in a single number: */
        case 'z': ++histogram[0]; break;
        case 'w': ++histogram[2]; break;
        case 'u': ++histogram[4]; break;
        case 'x': ++histogram[6]; break;
        case 'g': ++histogram[8]; break;
        /* Characters found only in a single number once removed the previous set: */
        case 'h': ++histogram[3]; break;
        case 'f': ++histogram[5]; break;
        case 's': ++histogram[7]; break;
        /* Characters found only in a single number once removed the previous sets: */
        case 'o': ++histogram[1]; break;
        case 'i': ++histogram[9]; break;
        default: continue;
        }
    }
    // Remove duplicated characters.
    histogram[3] -= histogram[8]; // 'h' both in 3 and 8, remove 8 instances.
    histogram[5] -= histogram[4]; // 'f' both in 4 and 5, remove 4 instances.
    histogram[7] -= histogram[6]; // 's' both in 6 and 7, remove 6 instances.
    histogram[1] -= histogram[0] + histogram[2] + histogram[4]; // 'o' also in 0, 2, 4
    histogram[9] -= histogram[6] + histogram[8] + histogram[5]; // 'i' also in 5, 6, 8
    for (size_t i = 0; i < 10; ++i)
        for (int j = 0; j < histogram[i]; ++j)
            result += static_cast<char>('0' + i);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = originalDigits(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("owoztneoer", "012", trials);
    test("fviefuro", "45", trials);
    test("ereht", "3", trials);
    test("zeroonetwothreefourfivesixseveneightnine", "0123456789", trials);
    return 0;
}


