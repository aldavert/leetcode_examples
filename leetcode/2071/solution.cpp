#include "../common/common.hpp"
#include <deque>

// ############################################################################
// ############################################################################

#if 1
int maxTaskAssign(std::vector<int> tasks,
                  std::vector<int> workers,
                  int pills,
                  int strength)
{
    const int nw = static_cast<int>(workers.size());
    std::vector<int> sorted_tasks(tasks), sorted_workers(workers);
    std::sort(sorted_tasks.begin(), sorted_tasks.end());
    std::sort(sorted_workers.begin(), sorted_workers.end());
    
    int left = -1, right = static_cast<int>(std::min(tasks.size(), workers.size())) - 1;
    while (left < right)
    {
        int mid = (left + right + 1) >> 1, t = mid;
        std::deque<int> dq;
        
        for (int w = nw - 1, free_pills = pills; t >= 0; --t)
        {
            if ((!dq.empty()) && (dq.front() >= sorted_tasks[t]))
                dq.pop_front();
            else if (w >= 0 && sorted_workers[w] >= sorted_tasks[t])
                --w;
            else if (free_pills > 0)
            {
                while ((w >= 0) && (sorted_workers[w] + strength >= sorted_tasks[t]))
                    dq.push_back(sorted_workers[w--]);
                if (dq.size() == 0) break;
                dq.pop_back(), free_pills--;
            }
            else break;
        }
        t == -1?(left = mid):(right = mid - 1);
    }
    
    return left + 1;
}
#else
int maxTaskAssign(std::vector<int> tasks,
                  std::vector<int> workers,
                  int pills,
                  int strength)
{
    const int nw = static_cast<int>(workers.size());
    int result = 0, left = 0,
                    right = static_cast<int>(std::min(tasks.size(), workers.size()));
    std::vector<int> sorted_tasks(tasks), sorted_workers(workers);
    std::sort(sorted_tasks.begin(), sorted_tasks.end());
    std::sort(sorted_workers.begin(), sorted_workers.end());
    
    auto canComplete = [&](int k)
    {
        std::map<int, int> workers_histogram;
        int pills_left = pills;
        for (int i = nw - k; i < nw; ++i)
            ++workers_histogram[sorted_workers[i]];
        
        for (int i = k - 1; i >= 0; --i)
        {
            auto it = workers_histogram.lower_bound(sorted_tasks[i]);
            if (it != workers_histogram.end())
            {
                if (--(it->second) == 0) workers_histogram.erase(it);
            }
            else if (pills_left > 0)
            {
                it = workers_histogram.lower_bound(sorted_tasks[i] - strength);
                if (it != workers_histogram.end())
                {
                    if (--(it->second) == 0) workers_histogram.erase(it);
                    --pills_left;
                }
                else return false;
            }
            else return false;
        }
        return true;
    };
    
    while (left <= right)
    {
        if (int m = (left + right) / 2; canComplete(m))
            left = m + 1, result = m;
        else right = m - 1;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> tasks,
          std::vector<int> workers,
          int pills,
          int strength,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxTaskAssign(tasks, workers, pills, strength);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 1}, {0, 3, 3}, 1, 1, 3, trials);
    test({5, 4}, {0, 0, 0}, 1, 5, 1, trials);
    test({10, 15, 30}, {0, 10, 10, 10, 10}, 3, 10, 2, trials);
    return 0;
}


