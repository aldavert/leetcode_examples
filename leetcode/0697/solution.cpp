#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findShortestSubArray(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    struct Info
    {
        int first = 0;
        int frequency = 0;
        int last = 0;
        int length(void) const { return last - first + 1; }
        bool operator>(const Info &other) const
        {
            return (frequency > other.frequency)
                || ((frequency == other.frequency) && (length() < other.length()));
        }
    };
    Info lut[50'000] = {};
    int selected = 0;
    for (int i = 0; i < n; ++i)
    {
        Info &current = lut[nums[i]];
        Info &max = lut[selected];
        if (current.frequency == 0) current.first = i;
        ++current.frequency;
        current.last = i;
        if (current > max)
            selected = nums[i];
    }
    return lut[selected].length();
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findShortestSubArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 3, 1}, 2, trials);
    test({1, 2, 2, 3, 1, 4, 2}, 6, trials);
    return 0;
}


