#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> removeAnagrams(std::vector<std::string> words)
{
    auto isAnagram = [](std::string first, std::string second)
    {
        if (first.size() != second.size()) return false;
        int histogram[26] = {};
        for (char letter : first) ++histogram[letter - 'a'];
        for (char letter : second)
            if (--histogram[letter - 'a'] < 0)
                return false;
        return true;
    };
    std::vector<std::string> result;
    result.push_back(words[0]);
    for (auto word : words)
        if (!isAnagram(word, result.back()))
            result.push_back(word);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = removeAnagrams(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"abba", "baba", "bbaa", "cd", "cd"}, {"abba", "cd"}, trials);
    test({"a", "b", "c", "d", "e"}, {"a", "b", "c", "d", "e"}, trials);
    return 0;
}


