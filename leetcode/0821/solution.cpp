#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> shortestToChar(std::string s, char c)
{
    const int n = static_cast<int>(s.size());
    std::vector<int> result(n);
    int previous = n;
    for (int i = 0; i < n; ++i, ++previous)
    {
        if (s[i] == c)
            previous = 0;
        result[i] = previous;
    }
    previous = n;
    for (int i = n - 1; i >= 0; --i, ++previous)
    {
        if (s[i] == c)
            previous = 0;
        result[i] = std::min(result[i], previous);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          char c,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestToChar(s, c);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("loveleetcode", 'e', {3, 2, 1, 0, 1, 0, 0, 1, 2, 2, 1, 0}, trials);
    test("aaab", 'b', {3, 2, 1, 0}, trials);
    return 0;
}


