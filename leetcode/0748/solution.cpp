#include "../common/common.hpp"
#include <cstring>
#include <limits>

// ############################################################################
// ############################################################################

std::string shortestCompletingWord(std::string licensePlate,
                                   std::vector<std::string> words)
{
    char histogram[26] = {}, current[26];
    std::string result;
    size_t length = std::numeric_limits<size_t>::max();
    for (char c : licensePlate)
    {
        if ((c >= 'a') && (c <= 'z'))
            ++histogram[c - 'a'];
        else if ((c >= 'A') && (c <= 'Z'))
            ++histogram[c - 'A'];
    }
    auto check = [&](void) -> bool
    {
        for (int i = 0; i < 26; ++i)
            if (current[i] > 0) return false;
        return true;
    };
    for (const auto &word : words)
    {
        std::memcpy(current, histogram, sizeof(histogram));
        for (char c : word)
            --current[c - 'a'];
        if (check() && (word.size() < length))
        {
            length = word.size();
            result = word;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string licensePlate,
          std::vector<std::string> words,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestCompletingWord(licensePlate, words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1s3 PSt", {"step", "steps", "stripe", "stepple"}, "steps", trials);
    test("1s3 456", {"looks", "pest", "stew", "show"}, "pest", trials);
    return 0;
}


