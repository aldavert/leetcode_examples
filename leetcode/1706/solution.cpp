#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findBall(std::vector<std::vector<int> > grid)
{
    const int m = static_cast<int>(grid.size());
    const int n = static_cast<int>(grid[0].size());
    std::vector<int> result(n);
    for (int c = 0; c < n; ++c)
    {
        int &cur = result[c] = c;
        for (int r = 0; r < m; ++r)
        {
            if (grid[r][cur] == 1)
            {
                if ((cur + 1 == n) || (grid[r][cur + 1] == -1))
                {
                    cur = -1;
                    break;
                }
                else ++cur;
            }
            else 
            {
                if ((cur == 0) || (grid[r][cur - 1] == 1))
                {
                    cur = -1;
                    break;
                }
                else --cur;
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findBall(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{ 1,  1,  1, -1, -1},
          { 1,  1,  1, -1, -1},
          {-1, -1, -1,  1,  1},
          { 1,  1,  1,  1, -1},
          {-1, -1, -1, -1, -1}}, {1, -1, -1, -1, -1}, trials);
    test({{-1}}, {-1}, trials);
    test({{ 1,  1,  1,  1,  1,  1},
          {-1, -1, -1, -1, -1, -1},
          { 1,  1,  1,  1,  1,  1},
          {-1, -1, -1, -1, -1, -1}}, {0, 1, 2, 3, 4, -1}, trials);
    return 0;
}


