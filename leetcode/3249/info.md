# Count the Number of Good Nodes

There is an **undirected** tree with `n` nodes labeled from `0` to `n - 1`, and rooted at node `0`. You are given a 2D integer array edges of length `n - 1`, where `edges[i] = [a_i, b_i]` indicates that there is an edge between nodes `a_i` and `b_i` in the tree.

A node is **good** if all the *subtrees* rooted at its children have the same size.

Return the number of **good** nodes in the given tree.

A **subtree** of `treeName` is a tree consisting of a node in `treeName` and all of its descendants.

**Note:** A *subtree* of `treeName` is a tree consisting of a node in `treeName` and all of its descendants.

#### Example 1:
> *Input:* `edges = [[0, 1], [0, 2], [1, 3], [1, 4], [2, 5], [2, 6]]`  
> *Output:* `7`  
> *Explanation:*
> ```mermaid 
> graph TD;
> A((0))---B((1))
> A---C((2))
> B---D((4))
> B---E((3))
> C---F((5))
> C---G((6))
> classDef default fill:#FFF,stroke:#00A,stroke-width:3px;
> ```
> All of the nodes of the given tree are good.

#### Example 2:
> *Input:* `edges = [[0, 1], [1, 2], [2, 3], [3, 4], [0, 5], [1, 6], [2, 7], [3, 8]]`  
> *Output:* `6`  
> *Explanation:*
> ```mermaid 
> graph TD;
> A((0))---B((1))
> A---C((5))
> B---D((2))
> B---E((6))
> D---F((3))
> D---G((7))
> F---H((4))
> F---I((8))
> classDef default fill:#FFF,stroke:#00A,stroke-width:3px;
> classDef unselected fill:#FFF,stroke:#000,stroke-width:2px;
> class A,B,D unselected;
> ```
> There are `6` good nodes in the given tree. They are colored in the image above.

#### Example 3:
> *Input:* `edges = [[0, 1], [1, 2], [1, 3], [1, 4], [0, 5], [5, 6], [6, 7], [7, 8], [0, 9], [9, 10], [9, 12], [10, 11]]`  
> *Output:* `12`  
> *Explanation:*
> ```mermaid 
> graph TD;
> A((0))---B((9))
> A---C((5))
> A---D((1))
> B---E((12))
> B---F((10))
> F---G((11))
> C---H((6))
> H---I((7))
> I---K((8))
> D---M((4))
> D---N((3))
> D---O((2))
> classDef default fill:#FFF,stroke:#00A,stroke-width:3px;
> classDef unselected fill:#FFF,stroke:#000,stroke-width:2px;
> class B unselected;
> ```
> All nodes except node 9 are good.

#### Constraints:
- `2 <= n <= 10^5`
- `edges.length == n - 1`
- `edges[i].length == 2`
- `0 <= a_i, b_i < n`
- The input is generated such that `edges` represents a valid tree.


