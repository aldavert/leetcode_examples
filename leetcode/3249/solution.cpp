#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countGoodNodes(std::vector<std::vector<int> > edges)
{
    const int n = static_cast<int>(edges.size()) + 1;
    std::vector<std::vector<int> > graph(n);
    int result = 0;
    auto dfs = [&](auto &&self, int u, int prev) -> int
    {
        std::vector<int> children_sizes;
        int size = 1;
        for (const int v : graph[u])
        {
            if (v == prev) continue;
            const int child_size = self(self, v, u);
            size += child_size;
            children_sizes.push_back(child_size);
        }
        bool all_same_size = true;
        for (size_t i = 1; all_same_size && (i < children_sizes.size()); ++i)
            if (children_sizes[i] != children_sizes[0])
                all_same_size = false;
        result += (children_sizes.empty() || all_same_size);
        return size;
    };
    for (const auto &edge : edges)
    {
        graph[edge[0]].push_back(edge[1]);
        graph[edge[1]].push_back(edge[0]);
    }
    dfs(dfs, 0, -1);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countGoodNodes(edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {0, 2}, {1, 3}, {1, 4}, {2, 5}, {2, 6}}, 7, trials);
    test({{0, 1}, {1, 2}, {2, 3}, {3, 4}, {0, 5}, {1, 6}, {2, 7}, {3, 8}}, 6, trials);
    test({{0, 1}, {1, 2}, {1, 3}, {1, 4}, {0, 5}, {5, 6}, {6, 7}, {7, 8},
          {0, 9}, {9, 10}, {9, 12}, {10, 11}}, 12, trials);
    return 0;
}


