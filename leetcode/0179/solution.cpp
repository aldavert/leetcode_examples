#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

std::string largestNumber(std::vector<int> nums)
{
    std::vector<std::string> nums_str;
    bool all_zero = true;
    for (int value : nums)
    {
        all_zero = all_zero && (value == 0);
        nums_str.push_back(std::to_string(value));
    }
    if (all_zero) return "0";
    auto criteria = [](std::string left, std::string right)
    {
        return left + right > right + left;
    };
    std::sort(nums_str.begin(), nums_str.end(), criteria);
    return std::accumulate(nums_str.begin(), nums_str.end(), std::string());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestNumber(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 2}, "210", trials);
    test({3, 30, 34, 5, 9}, "9534330", trials);
    test({111311, 1113}, "1113111311", trials);
    return 0;
}


