#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int longestCommonPrefix(std::vector<int> arr1, std::vector<int> arr2)
{
    std::unordered_set<int> s;
    for (int x : arr1)
        for (; x; x /= 10)
            s.insert(x);
    int result = 0;
    for (int x : arr2)
    {
        for (; x; x /= 10)
        {
            if (s.count(x))
            {
                result = std::max(result, static_cast<int>(std::log10(x) + 1));
                break;
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr1,
          std::vector<int> arr2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestCommonPrefix(arr1, arr2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 10, 100}, {1000}, 3, trials);
    test({1, 2, 3}, {4, 4, 4}, 0, trials);
    return 0;
}


