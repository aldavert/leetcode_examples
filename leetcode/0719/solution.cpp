#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int smallestDistancePair(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::sort(nums.begin(), nums.end());
    int l = 0, r = nums.back() - nums.front();
    while (l < r)
    {
        const int m = (l + r) / 2;
        int count = 0;
        auto it_first = std::upper_bound(nums.begin(), nums.end(), nums[0] + m);
        auto initial = std::distance(nums.begin(), it_first);
        for (int i = 0, j = static_cast<int>(initial); i < n; ++i)
        {
            while ((j < n) && (nums[j] <= nums[i] + m))
                ++j;
            count += j - i - 1;
        }
        if (count >= k) r = m;
        else l = m + 1;
    }
    return l;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestDistancePair(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 1}, 1, 0, trials);
    test({1, 1, 1}, 2, 0, trials);
    test({1, 6, 1}, 3, 5, trials);
    return 0;
}


