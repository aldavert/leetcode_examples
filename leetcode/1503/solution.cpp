#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int getLastMoment(int n, std::vector<int> left, std::vector<int> right)
{
    return std::max((right.size())?(n - *std::min_element(right.begin(), right.end())):0,
                    (left.size())?*std::max_element(left.begin(), left.end()):0);
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<int> left,
          std::vector<int> right,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getLastMoment(n, left, right);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {4, 3}, {0, 1}, 4, trials);
    test(7, {}, {0, 1, 2, 3, 4, 5, 6, 7}, 7, trials);
    test(7, {0, 1, 2, 3, 4, 5, 6, 7}, {}, 7, trials);
    return 0;
}


