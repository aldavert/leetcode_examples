#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * lowestCommonAncestor(TreeNode * root, TreeNode * p, TreeNode * q)
{
    if (root == nullptr) return nullptr;
    if ((root == p) || (root == q)) return root;
    TreeNode * search_left = ((root->val > p->val) || (root->val > q->val))?
        lowestCommonAncestor(root->left, p, q):nullptr;
    TreeNode * search_right = ((root->val < p->val) || (root->val < q->val))?
        lowestCommonAncestor(root->right, p, q):nullptr;
    if (((search_left == p) || (search_right == p))
    &&  ((search_left == q) || (search_right == q))) return root;
    return (search_left)?search_left:search_right;
}

// ############################################################################
// ############################################################################

TreeNode * search(TreeNode * root, int value)
{
    if (root == nullptr) return nullptr;
    if (root->val == value) return root;
    TreeNode * result = search(root->left, value);
    if (result != nullptr) return result;
    return search(root->right, value);
}

void test(std::vector<int> tree,
          int p,
          int q,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        TreeNode * node_p = search(root, p);
        TreeNode * node_q = search(root, q);
        TreeNode * node_result = lowestCommonAncestor(root, node_p, node_q);
        result = (node_result != nullptr)?node_result->val:null;
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({6, 2, 8, 0, 4, 7, 9, null, null, 3, 5}, 2, 8, 6, trials);
    test({6, 2, 8, 0, 4, 7, 9, null, null, 3, 5}, 2, 4, 2, trials);
    test({2, 1}, 2, 1, 2, trials);
    return 0;
}

