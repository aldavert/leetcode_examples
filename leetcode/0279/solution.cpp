#include "../common/common.hpp"
#include <array>
#include <limits>
#include <unordered_map>
#include <cmath>

// ############################################################################
// ############################################################################

#if 1
int numSquares(int n)
{
    constexpr int N = 101;
    static constexpr auto PS = []{
        std::array<int, N> result{};
        for (int i = 0; i < N; ++i) result[i] = i * i;
        return result;
    }();
    
    std::vector<int> lut(n + 1);
    lut[0] = 0;
    for (int idx = 1; idx <= n;)
    {
        int current_target = idx;
        int current_res = current_target;
        for (int i = 1; PS[i] <= current_target; ++i)
            current_res = std::min(current_res, lut[current_target - PS[i]] + 1);
        lut[idx++] = current_res;
    }
    return lut[n];
}
#elif 0
int numSquares(int n)
{
    constexpr int MAX = std::numeric_limits<int>::max();
    constexpr int N = 101;
    static constexpr auto PS = []{
        std::array<int, N> result{};
        for (int i = 0; i < N; ++i) result[i] = i * i;
        return result;
    }();
    const std::array<int, N> &perfect_squares = PS;
    int result = MAX;
    
    auto squares = [&](auto &&self, int position, int remainder, int operations) -> int
    {
        if (operations >= result) return MAX;
        if (remainder <  0) return MAX;
        if (remainder == 0) return operations;
        if (remainder == 1) return operations + 1;
        for (int p = position; p > 0; --p)
        {
            int current = self(self, p, remainder - perfect_squares[p], operations + 1);
            result = std::min(result, current);
        }
        return result;
    };
    return squares(squares, static_cast<int>(std::sqrt(n)), n, 0);
}
#else
int numSquares(int n)
{
    constexpr int N = 101;
    static constexpr auto perfect_squares = []{
        std::array<int, N> result{};
        for (int i = 0; i < N; ++i) result[i] = i * i;
        return result;
    }();
    std::unordered_map<long, int> memo;
    
    auto squares = [&](auto &&self, int position, int remainder) -> int
    {
        if (remainder <= 1) return 1;
        long key = static_cast<long>(position) << 32 | remainder;
        if (auto it = memo.find(key); it != memo.end()) return it->second;
        int result = std::numeric_limits<int>::max();
        for (int p = position; p > 0; --p)
        {
            if (perfect_squares[p] > remainder)
                continue;
            if (perfect_squares[p] == remainder) return 1;
            result = std::min(result, self(self, p, remainder - perfect_squares[p]) + 1);
        }
        return memo[key] = result;
    };
    return squares(squares, N - 1, n);
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numSquares(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(12, 3, trials);
    test(13, 2, trials);
    test(20, 2, trials);
    test(493, 2, trials); // 20 * 20 + 3 * 3 = 493
    test(494, 3, trials); // 20 * 20 + 3 * 3 = 493
    test(6175, 4, trials);
    test(9312, 3, trials);
    test(9007, 4, trials);
    test(7, 4, trials);
    return 0;
}


