#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countSubarrays(std::vector<int> nums)
{
    int result = 0;
    for (size_t i = 0, n = nums.size() - 2; i < n; ++i)
        result += ((nums[i] + nums[i + 2]) * 2) == nums[i + 1];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSubarrays(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1, 4, 1}, 1, trials);
    test({1, 1, 1}, 0, trials);
    return 0;
}


