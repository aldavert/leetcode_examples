#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sumOfPower(std::vector<int> nums)
{
    constexpr int MOD = 1'000'000'007;
    long result = 0, sum = 0;
    
    std::sort(nums.begin(), nums.end());
    for (int num : nums)
    {
        result = (result + (num + sum) * num % MOD * num % MOD) % MOD;
        sum = (sum * 2 + num) % MOD;
    }
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOfPower(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 4}, 141, trials);
    test({1, 1, 1}, 7, trials);
    return 0;
}


