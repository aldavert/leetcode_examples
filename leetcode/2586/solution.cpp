#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int vowelStrings(std::vector<std::string> words, int left, int right)
{
    auto isVowel = [](char letter)
    {
        return (letter == 'a') || (letter == 'e') || (letter == 'i')
            || (letter == 'o') || (letter == 'u');
    };
    int result = 0;
    for (int i = left; i <= right; ++i)
        result += isVowel(words[i].front()) && isVowel(words[i].back());
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          int left,
          int right,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = vowelStrings(words, left, right);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"are", "amy", "u"}, 0, 2, 2, trials);
    test({"hey", "aeo", "mu", "ooo", "artro"}, 1, 4, 3, trials);
    return 0;
}



