#include "../common/common.hpp"
#include <unordered_map>
#include <queue>
#include <stack>
#include <numeric>
#include <map>

// ############################################################################
// ############################################################################

std::vector<std::string> basicCalculatorIV(
        std::string expression,
        std::vector<std::string> evalvars,
        std::vector<int> evalints)
{
    struct RPNElement
    {
        RPNElement(void) = default;
        RPNElement(int v) : operation(false), value(v), variable("") {}
        RPNElement(std::string v) : operation(false), value(1), variable(v) {}
        RPNElement(bool product, bool addition) :
            operation(true), value(0), variable(product?"*":(addition?"+":"-")) {}
        bool operation = false;
        int value = 0;
        std::string variable = "";
    };
    struct Polynomial
    {
        // {variables : {order, coefficient}}
        std::map<std::vector<std::string>, int> coefficients;
        Polynomial(void) = default;
        Polynomial(int value) { coefficients[{}] = value; }
        Polynomial(std::string variable)
        {
            coefficients[std::vector<std::string>(1, variable)] = 1;
        }
        void add(const Polynomial &other, bool addition)
        {
            for (const auto &[variable, coefficient] : other.coefficients)
            {
                auto search = coefficients.find(variable);
                int value = (2 * addition - 1) * coefficient;
                if (search == coefficients.end())
                    coefficients[variable] = value;
                else search->second += value;
            }
        }
        void product(const Polynomial &other)
        {
            auto backup = coefficients;
            coefficients.clear();
            for (const auto &[var_left, coef_left] : backup)
            {
                for (const auto &[var_right, coef_right] : other.coefficients)
                {
                    std::vector<std::string> new_var = var_left;
                    new_var.insert(new_var.end(), var_right.begin(), var_right.end());
                    std::sort(new_var.begin(), new_var.end());
                    if (auto search = coefficients.find(new_var);
                        search == coefficients.end())
                        coefficients[new_var] = coef_left * coef_right;
                    else search->second += coef_left * coef_right;
                }
            }
        }
    };
    std::vector<std::string> result;
    std::unordered_map<std::string, int> evaluations;
    for (size_t i = 0, m = evalvars.size(); i < m; ++i)
        evaluations[evalvars[i]] = evalints[i];
    std::queue<RPNElement> rpn_expression;
    std::stack<char> s;
    auto parser = [&](std::string token) -> void
    {
        if (token.size() == 0) return;
        else if (token.size() == 1)
        {
            if ((token[0] == '+') || (token[0] == '-'))
            {
                while ((!s.empty()) && (s.top() != '('))
                {
                    rpn_expression.push({s.top() == '*', s.top() == '+'});
                    s.pop();
                }
                s.push(token[0]);
                return;
            }
            else if (token[0] == '*')
            {
                while ((!s.empty()) && (s.top() != '(') && (s.top() == '*'))
                {
                    rpn_expression.push({s.top() == '*', s.top() == '+'});
                    s.pop();
                }
                s.push('*');
                return;
            }
            else if (token[0] == '(')
            {
                s.push('(');
                return;
            }
            else if (token[0] == ')')
            {
                while ((!s.empty()) && (s.top() != '('))
                {
                    rpn_expression.push({s.top() == '*', s.top() == '+'});
                    s.pop();
                }
                s.pop();
                return;
            }
        }
        size_t number_of_digits = std::accumulate(token.begin(), token.end(), 0,
                    [](int v, char letter) { return std::isdigit(letter) + v; });
        if (number_of_digits == token.size())
            rpn_expression.push({std::stoi(token)});
        else
        {
            if (auto search = evaluations.find(token); search != evaluations.end())
                rpn_expression.push({search->second});
            else rpn_expression.push({token});
        }
    };
    for (size_t i = 0, p = 0, n = expression.size(); i < n; ++i)
    {
        if (expression[i] == ' ')
        {
            parser(expression.substr(p, i - p));
            p = i + 1;
        }
        else if (expression[i] == '(')
        {
            parser(expression.substr(p, i - p));
            parser("(");
            p = i + 1;
        }
        else if (expression[i] == ')')
        {
            parser(expression.substr(p, i - p));
            parser(")");
            p = i + 1;
        }
        else if (i + 1 == n)
            parser(expression.substr(p, i - p + 1));
    }
    for (; !s.empty(); s.pop())
        if (s.top() != '(') rpn_expression.push({s.top() == '*', s.top() == '+'});
    std::stack<Polynomial> evaluation;
    while (!rpn_expression.empty())
    {
        auto value = rpn_expression.front();
        rpn_expression.pop();
        if (value.operation)
        {
            auto top = evaluation.top();
            evaluation.pop();
            if (value.variable == "*")
                evaluation.top().product(top);
            else evaluation.top().add(top, value.variable == "+");
        }
        else if (value.variable.empty())
            evaluation.push({value.value});
        else evaluation.push({value.variable});
    }
    int maximum_degree = 0;
    for (const auto &[variables, coefficients] : evaluation.top().coefficients)
        maximum_degree = std::max(maximum_degree, static_cast<int>(variables.size()));
    std::vector<std::tuple<int, std::string, int> > sorted_output;
    for (const auto &[variables, coefficient] : evaluation.top().coefficients)
    {
        if (coefficient == 0) continue;
        std::string current;
        for (std::string v : variables)
            current += "*" + v;
        sorted_output.emplace_back(maximum_degree - static_cast<int>(variables.size()),
                                   current,
                                   coefficient);
    }
    std::sort(sorted_output.begin(), sorted_output.end());
    for (auto [order, current, coefficient] : sorted_output)
        result.push_back(std::to_string(coefficient) + current);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string expression,
          std::vector<std::string> evalvars,
          std::vector<int> evalints,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = basicCalculatorIV(expression, evalvars, evalints);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("e + 8 - a + 5", {"e"}, {1}, {"-1*a", "14"}, trials);
    test("e - 8 + temperature - pressure", {"e", "temperature"}, {1, 12},
         {"-1*pressure", "5"}, trials);
    test("(e + 8) * (e - 8)", {}, {}, {"1*e*e", "-64"}, trials);
    test("a * b * c + b * a * c * 4", {}, {}, {"5*a*b*c"}, trials);
    test("((a - b) * (b - c) + (c - a)) * ((a - b) + (b - c) * (c - a))", {}, {},
         {"-1*a*a*b*b", "2*a*a*b*c", "-1*a*a*c*c", "1*a*b*b*b", "-1*a*b*b*c",
          "-1*a*b*c*c", "1*a*c*c*c", "-1*b*b*b*c", "2*b*b*c*c", "-1*b*c*c*c",
          "2*a*a*b", "-2*a*a*c", "-2*a*b*b", "2*a*c*c", "1*b*b*b", "-1*b*b*c",
          "1*b*c*c", "-1*c*c*c", "-1*a*a", "1*a*b", "1*a*c", "-1*b*c"},  trials);
    return 0;
}


