#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

void reorderList(ListNode * head)
{
    if (!head) return;
    ListNode * nodes_even = nullptr, * ptr;
    int n = 0;
    for (ptr = head; ptr; ptr = ptr->next) ++n;
    n /= 2;
    for (ptr = head; n; --n, ptr = ptr->next);
    while (ptr)
    {
        ListNode * next = ptr->next;
        ptr->next = nodes_even;
        nodes_even = ptr;
        ptr = next;
    }
    
    for (ptr = head; ptr; )
    {
        ListNode * next_odd = ptr->next;
        ListNode * next_even = (nodes_even)?nodes_even->next:nullptr;
        ptr->next = nodes_even;
        if(nodes_even)
        {
            nodes_even->next = next_odd;
            nodes_even = next_even;
        }
        ptr = next_odd;
    }
}

// ############################################################################
// ############################################################################

void test(std::vector<int> input, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * l = vec2list(input);
        reorderList(l);
        result = list2vec(l);
        delete l;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, {1, 4, 2, 3}, trials);
    test({1, 2, 3, 4, 5}, {1, 5, 2, 4, 3}, trials);
    return 0;
}


