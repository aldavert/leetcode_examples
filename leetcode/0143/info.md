# Reorder List

You are given the head of a singly linked-list. The list can be represented as:
```
L_0 -> L_1 -> ... -> L_{N - 1} -> L_n
```

*Reorder the list to be on the following form:*
```
L_0 -> L_n -> L_1 -> L_{n - 1} -> L_2 -> L_{n - 2} -> ...
```

You may not modify the values in the list's nodes. Only nodes themselves may be changed.
 

#### Example 1:
> *The list*
> ```mermaid 
> graph LR;
> A((1))-->B((2))
> B-->C((3))
> C-->D((4))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *is transformed into*
> ```mermaid 
> graph LR;
> A((1))-->D((4))
> D-->B((2))
> B-->C((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `head = [1, 2, 3, 4]`  
> *Output:* `[1, 4, 2, 3]`

#### Example 2:
> *The list*
> ```mermaid 
> graph LR;
> A((1))-->B((2))
> B-->C((3))
> C-->D((4))
> D-->E((5))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *is transformed into*
> ```mermaid 
> graph LR;
> A((1))-->E((5))
> E-->B((2))
> B-->D((4))
> D-->C((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `head = [1, 2, 3, 4, 5]`  
> *Output:* `[1, 5, 2, 4, 3]`

#### Constraints:
- The number of nodes in the list is in the range `[1, 5 * 10^4]`.
- `1 <= Node.val <= 1000`


