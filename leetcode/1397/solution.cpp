#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findGoodStrings(int n, std::string s1, std::string s2, std::string evil)
{
    constexpr int MOD = 1'000'000'007;
    const int ne = static_cast<int>(evil.size());
    int dp[501][51][2][2], next_matched_count[51][26], longest_prefix_sufix[51] = {};
    
    auto next = [&](int j, char current) -> int
    {
        int &result = next_matched_count[j][current - 'a'];
        if (result != -1) return result;
        while ((j > 0) && (evil[j] != current)) j = longest_prefix_sufix[j - 1];
        return result = (evil[j] == current)?j + 1:j;
    };
    auto find = [&](auto &&self, int i, int matched_evil_count,
                    bool is_s1_prefix, bool is_s2_prefix) -> int
    {
        if (matched_evil_count == ne) return 0;
        if (i == static_cast<int>(s1.size())) return 1;
        int &result = dp[i][matched_evil_count][is_s1_prefix][is_s2_prefix];
        if (result != -1) return result;
        result = 0;
        char min_char = is_s1_prefix?s1[i]:'a',
             max_char = is_s2_prefix?s2[i]:'z';
        for (char c = min_char; c <= max_char; ++c)
            result = (result + self(self, i + 1, next(matched_evil_count, c),
                                    is_s1_prefix && (c == s1[i]),
                                    is_s2_prefix && (c == s2[i]))) % MOD;
        return result;
    };
    
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < ne; ++j)
            dp[i][j][0][0] = dp[i][j][0][1] = dp[i][j][1][0] = dp[i][j][1][1] = -1;
    for (int i = 0; i < ne; ++i)
        for (int j = 0; j < 26; ++j)
            next_matched_count[i][j] = -1;
    for (int i = 1, j = 0; i < ne; ++i)
    {
        while ((j > 0) && (evil[j] != evil[i])) j = longest_prefix_sufix[j - 1];
        if (evil[i] == evil[j]) longest_prefix_sufix[i] = ++j;
    }
    return find(find, 0, 0, true, true);
}

// ############################################################################
// ############################################################################

void test(int n,
          std::string s1,
          std::string s2,
          std::string evil,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findGoodStrings(n, s1, s2, evil);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, "aa", "da", "b", 51, trials);
    test(8, "leetcode", "leetgoes", "leet", 0, trials);
    test(2, "gx", "gz", "x", 2, trials);
    return 0;
}


