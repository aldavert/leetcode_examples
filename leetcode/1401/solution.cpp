#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkOverlap(int radius, int xCenter, int yCenter, int x1, int y1, int x2, int y2)
{
    auto clamp = [](int center, int min_i, int max_i) -> int
    {
        return std::max(min_i, std::min(max_i, center));
    };
    int distanceX = xCenter - clamp(xCenter, x1, x2),
        distanceY = yCenter - clamp(yCenter, y1, y2);
    return (distanceX * distanceX + distanceY * distanceY <= radius * radius);
}

// ############################################################################
// ############################################################################

void test(int radius, int xCenter, int yCenter, int x1, int y1, int x2, int y2,
          bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkOverlap(radius, xCenter, yCenter, x1, y1, x2, y2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 0, 0,  1, -1, 3,  1,  true, trials);
    test(1, 1, 1,  1, -3, 2, -1, false, trials);
    test(1, 0, 0, -1,  0, 0,  1,  true, trials);
    return 0;
}


