#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * mergeTrees(TreeNode * root1, TreeNode * root2)
{
    auto duplicate = [&](auto &&self, TreeNode * ptr) -> TreeNode *
    {
        if (!ptr) return nullptr;
        return new TreeNode(ptr->val, self(self, ptr->left), self(self, ptr->right));
    };
    if (root1 && root2)
    {
        TreeNode * current = new TreeNode(root1->val + root2->val);
        current->left = mergeTrees(root1->left, root2->left);
        current->right = mergeTrees(root1->right, root2->right);
        return current;
    }
    else if (root1 && !root2)
        return duplicate(duplicate, root1);
    else if (!root1 && root2)
        return duplicate(duplicate, root2);
    else return nullptr;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree1,
          std::vector<int> tree2,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root1 = vec2tree(tree1);
        TreeNode * root2 = vec2tree(tree2);
        TreeNode * output = mergeTrees(root1, root2);
        result = tree2vec(output);
        delete root1;
        delete root2;
        delete output;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 5}, {2, 1, 3, null, 4, null, 7}, {3, 4, 5, 5, 4, null, 7}, trials);
    test({1}, {1, 2}, {2, 2}, trials);
    return 0;
}


