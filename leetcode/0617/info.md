# Merge Two Binary Trees

You are given two binary trees `root1` and `root2`.

Imagine that when you put one of them to cover the other, some nodes of the two trees are overlapped while the others are not. You need to merge the two trees into a new binary tree. The merge rule is that if two nodes overlap, then sum node values up as the new value of the merged node. Otherwise, the NOT null node will be used as the node of the new tree. Return *the merged tree*.

**Note:** The merging process must start from the root nodes of both trees.

#### Example 1:
> ```mermaid
> flowchart LR;
> subgraph SA [ ]
> A((1))---B((3))
> A---C((2))
> B---D((5))
> B---EMPTY1(( ))
> E((2))---F((1))
> E---G((3))
> F---EMPTY2(( ))
> F---H((4))
> G---EMPTY3(( ))
> G---I((7))
> end
> subgraph SB [ ]
> J((3))---K((4))
> J---L((5))
> K---M((5))
> K---N((4))
> L---EMPTY4(( ))
> L---O((7))
> end
> SA-->SB
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class SA,EMPTY1,EMPTY2,EMPTY3,SB,EMPTY4 empty;
> linkStyle 3,6,8,14 stroke-width:0px;
> ```
> *Input:* `root1 = [1, 3, 2, 5], root2 = [2, 1, 3, null, 4, null, 7]`  
> *Output:* `[3, 4, 5, 5, 4, null, 7]`

#### Example 2:
> *Input:* `root1 = [1], root2 = [1, 2]`  
> *Output:* `[2, 2]`

#### Constraints:
- The number of nodes in both trees is in the range `[0, 2000]`.
- `-10^4 <= Node.val <= 10^4`


