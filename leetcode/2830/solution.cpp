#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximizeTheProfit(int n, std::vector<std::vector<int> > offers)
{
    std::vector<int> dp(n + 1);
    std::vector<std::vector<std::pair<int, int> > > end_to_start_and_golds(n);
    
    for (const auto &offer : offers)
        end_to_start_and_golds[offer[1]].emplace_back(offer[0], offer[2]);
    for (int end = 1; end <= n; ++end)
    {
        dp[end] = dp[end - 1];
        for (auto& [start, gold] : end_to_start_and_golds[end - 1])
            dp[end] = std::max(dp[end], dp[start] + gold);
    }
    return dp[n];
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > offers,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximizeTheProfit(n, offers);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{0, 0, 1}, {0, 2, 2}, {1, 3, 2}}, 3, trials);
    test(5, {{0, 0, 1}, {0, 2, 10}, {1, 3, 2}}, 10, trials);
    return 0;
}


