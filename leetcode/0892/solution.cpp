#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int surfaceArea(std::vector<std::vector<int> > grid)
{
    int xy = 0, xz = 0, yz = 0, buffer[51] = {};
    const size_t n = grid.size();
    for (const auto &x : grid)
    {
        int cy = 0;
        for (int idx = 0; int y : x)
        {
            xy += y > 0;
            xz += std::abs(cy - y);
            yz += std::abs(buffer[idx] - y);
            cy = y;
            buffer[idx++] = y;
        }
        xz += cy;
    }
    for (size_t i = 0; i < n; ++i)
        yz += buffer[i];
    return 2 * xy + xz + yz;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = surfaceArea(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {3, 4}}, 34, trials);
    test({{1, 1, 1}, {1, 0, 1}, {1, 1, 1}}, 32, trials);
    test({{2, 2, 2}, {2, 1, 2}, {2, 2, 2}}, 46, trials);
    return 0;
}


