#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int findMaxLength(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = 0, accum = 2 * (nums[0] == 0) - 1;
    std::unordered_map<int, int> position;
    position[0] = -1;
    position[accum] = 0;
    for (int i = 1; i < n; ++i)
    {
        accum += 2 * (nums[i] == 0) - 1;
        if (auto it = position.find(accum); it != position.end())
            result = std::max(result, i - it->second);
        else position.insert({accum, i});
    }
    return result;
}
#else
int findMaxLength(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = 0, accum_zro = nums[0] == 0, accum_one = nums[0] == 1;
    std::unordered_map<int, int> position;
    position[0] = -1;
    position[accum_zro - accum_one] = 0;
    for (int i = 1; i < n; ++i)
    {
        accum_zro += (nums[i] == 0);
        accum_one += (nums[i] == 1);
        int difference = accum_zro - accum_one;
        if (auto it = position.find(difference); it != position.end())
            result = std::max(result, i - it->second);
        else position[difference] = i;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMaxLength(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1}, 2, trials);
    test({0, 1, 0}, 2, trials);
    test({1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1,
          0, 1, 0, 1}, 10, trials);
    test({0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0,
          1, 0, 1, 0}, 10, trials);
    test({0, 0, 1}, 2, trials);
    return 0;
}


