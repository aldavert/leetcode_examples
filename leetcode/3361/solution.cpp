#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long shiftDistance(std::string s,
                        std::string t,
                        std::vector<int> nextCost,
                        std::vector<int> previousCost)
{
    long long cost[2][54] = {}, accum[2] = {};
    for (int i = 0; i < 26; ++i)
    {
        accum[1] += previousCost[i];
        cost[0][i] = accum[0];
        cost[1][i] = accum[1];
        accum[0] += nextCost[i];
    }
    for (int i = 0; i < 26; ++i)
    {
        accum[1] += previousCost[i];
        cost[0][i + 26] = accum[0];
        cost[1][i + 26] = accum[1];
        accum[0] += nextCost[i];
    }
    long long result = 0;
    for (int i = 0, n = static_cast<int>(s.size()); i < n; ++i)
    {
        const int ti = t[i] - 'a', si = s[i] - 'a';
        result += std::min(cost[0][ti + 26 * (s[i] > t[i])] - cost[0][si],
                           cost[1][si + 26 * (s[i] < t[i])] - cost[1][ti]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::string t,
          std::vector<int> nextCost,
          std::vector<int> previousCost,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = shiftDistance(s, t, nextCost, previousCost);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abab", "baba", {100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 2, trials);
    test("leet", "code", {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 31, trials);
    return 0;
}


