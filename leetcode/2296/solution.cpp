#include "../common/common.hpp"
#include <stack>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class TextEditor
{
    std::string m_str;
    std::stack<char> m_stack;
    inline std::string getString(void) const
    {
        return (m_str.size() < 10)?m_str:m_str.substr(m_str.size() - 10);
    }
public:
    TextEditor(void)
    {
    }
    inline void addText(std::string text) { m_str += text; }
    int deleteText(int k)
    {
        int n_deleted = std::min(k, static_cast<int>(m_str.size()));
        for (int i = 0; i < n_deleted; ++i)
            m_str.pop_back();
        return n_deleted;
    }
    std::string cursorLeft(int k)
    {
        while (!m_str.empty() && k--)
            m_stack.push(m_str.back()),
            m_str.pop_back();
        return getString();
    }
    std::string cursorRight(int k)
    {
        while (!m_stack.empty() && k--)
            m_str += m_stack.top(),
            m_stack.pop();
        return getString();
    }
};

// ############################################################################
// ############################################################################

enum class OP { DELETE, ADD, RIGHT, LEFT };

void test(std::vector<OP> operation,
          std::vector<std::variant<std::string, int> > input,
          std::vector<std::variant<std::string, int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::variant<std::string, int> > result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        TextEditor object;
        result.clear();
        for (size_t i = 0; i < operation.size(); ++i)
        {
            switch (operation[i])
            {
            case OP::DELETE:
                result.push_back(object.deleteText(std::get<int>(input[i])));
                break;
            case OP::ADD:
                result.push_back(null);
                object.addText(std::get<std::string>(input[i]));
                break;
            case OP::RIGHT:
                result.push_back(object.cursorRight(std::get<int>(input[i])));
                break;
            case OP::LEFT:
                result.push_back(object.cursorLeft(std::get<int>(input[i])));
                break;
            default:
                std::cerr << "[ERROR] Unknown operation code!!\n";
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::ADD, OP::DELETE, OP::ADD, OP::RIGHT, OP::LEFT, OP::DELETE,
          OP::LEFT, OP::RIGHT},
         {"leetcode", 4, "practice", 3, 8, 10, 2, 6},
         {null, 4, null, "etpractice", "leet", 4, "", "practi"}, trials);
    return 0;
}


