#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int singleNonDuplicate(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    if (n == 1) return nums[0];
    int begin = 0, end = n - 1;
    while (end - begin > 3)
    {
        int mid = ((begin + end) / 2) / 2 * 2;
        if (nums[mid] != nums[mid + 1])
            end = mid;
        else begin = mid;
    }
    return (nums[begin] == nums[begin + 1])?nums[end]:nums[begin];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = singleNonDuplicate(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    //    0  1  2  3  4  5  6  7  8
    test({1, 2, 2, 3, 3, 4, 4, 8, 8}, 1, trials);
    //    0  1  2  3  4  5  6  7  8
    test({1, 1, 2, 2, 3, 3, 5, 8, 8}, 5, trials);
    //    0  1  2  3  4  5  6  7  8
    test({1, 1, 2, 3, 3, 4, 4, 8, 8}, 2, trials);
    //    0  1  2  3   4   5   6
    test({3, 3, 7, 7, 10, 11, 11}, 10, trials);
    return 0;
}


