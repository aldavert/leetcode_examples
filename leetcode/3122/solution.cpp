#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumOperations(std::vector<std::vector<int> > grid)
{
    const int m = static_cast<int>(grid.size());
    const int n = static_cast<int>(grid[0].size());
    std::vector<int> count[10], mem[10];
    for (int i = 0; i < 10; ++i)
        count[i] = std::vector<int>(n),
        mem[i] = std::vector<int>(n, -1);
    auto process = [&](auto &&self, int j, int prev) -> int
    {
        if (j == n) return 0;
        if (mem[prev][j] != -1) return mem[prev][j];
        int result = std::numeric_limits<int>::max();
        for (int num = 0; num < 10; ++num)
            if ((j == 0) || (num != prev))
                result = std::min(result, m - count[num][j] + self(self, j + 1, num));
        return mem[prev][j] = result;
    };
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            ++count[grid[i][j]][j];
    return process(process, 0, 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumOperations(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0, 2}, {1, 0, 2}}, 0, trials);
    test({{1, 1, 1}, {0, 0, 0}}, 3, trials);
    test({{1}, {2}, {3}}, 2, trials);
    return 0;
}


