#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int waysToReachTarget(int target, std::vector<std::vector<int> > types)
{
    constexpr int MOD = 1'000'000'007;
    int dp[1001] = {};
    dp[0] = 1;
    for (const auto &type : types)
        for (int j = target, count = type[0], mark = type[1]; j >= 0; --j)
            for (int solved = 1; solved <= count; ++solved)
                if (j - solved * mark >= 0)
                    dp[j] = (dp[j] + dp[j - solved * mark]) % MOD;
    return dp[target];
}

// ############################################################################
// ############################################################################

void test(int target,
          std::vector<std::vector<int> > types,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = waysToReachTarget(target, types);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, {{6, 1}, {3, 2}, {2, 3}}, 7, trials);
    test(5, {{50, 1}, {50, 2}, {50, 5}}, 4, trials);
    return 0;
}


