#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> constructArray(int n, int k)
{
    std::vector<int> result;
    for (int i = 0; i < n - k; ++i) result.push_back(i + 1);
    for (int i = 0; i < k; ++i)
    {
        if ((i & 1) == 0) result.push_back(n - i / 2);
        else result.push_back(n - k + (i + 1) / 2);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int k, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = constructArray(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 1, {1, 2, 3}, trials);
    test(3, 2, {1, 3, 2}, trials);
    return 0;
}


