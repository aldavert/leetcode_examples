#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestCommonSubpath(int /*n*/, std::vector<std::vector<int> > paths)
{
    const int n_paths = static_cast<int>(paths.size());
    constexpr long MAX_SIZE = 100'001, MAX_VALUE = 100'000'000'019;
    auto hash = [](const std::vector<int> &path,
                   std::unordered_set<long> &subpath,
                   int length,
                   bool first) -> void
    {
        const int n_path = static_cast<int>(path.size());
        long t = 1, h = path[0];
        for (int i = 1; i < length; ++i)
        {
            h = (h * MAX_SIZE + path[i]) % MAX_VALUE;
            t = (t * MAX_SIZE) % MAX_VALUE;
        }
        std::unordered_set<long> current;
        if (first || (subpath.find(h) != subpath.end())) current.insert(h);
        for (int i = length; i < n_path; ++i)
        {
            h = (h - path[i - length] * t % MAX_VALUE + MAX_VALUE) % MAX_VALUE;
            h = (h * MAX_SIZE + path[i]) % MAX_VALUE;
            if (first || (subpath.find(h) != subpath.end())) current.insert(h);
        }
        subpath = current;
    };
    int left = 0, right = std::numeric_limits<int>::max();
    for (const auto &path : paths)
        right = std::min<int>(right, static_cast<int>(path.size()));
    while (left < right)
    {
        int mid = (left + right + 1) / 2;
        std::unordered_set<long> subpath;
        hash(paths[0], subpath, mid, true);
        for (int i = 1; (i < n_paths) && (subpath.size() > 0); ++i)
            hash(paths[i], subpath, mid, false);
        if (subpath.size()) left = mid;
        else           right = mid - 1;
    }
    return left;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > paths,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestCommonSubpath(n, paths);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{0, 1, 2, 3, 4}, {2, 3, 4}, {4, 0, 1, 2, 3}}, 2, trials);
    test(3, {{0}, {1}, {2}}, 0, trials);
    test(5, {{0, 1, 2, 3, 4}, {4, 3, 2, 1, 0}}, 1, trials);
    return 0;
}


