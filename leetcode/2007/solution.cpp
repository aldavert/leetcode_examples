#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findOriginalArray(std::vector<int> changed)
{
    if ((changed.size() & 1) == 1) return {};
    std::vector<int> sorted_changed(changed.begin(), changed.end());
    int histogram[100'001] = {};
    for (int c : changed)
        ++histogram[c];
    std::sort(sorted_changed.begin(), sorted_changed.end());
    std::vector<int> result;
    for (int v : sorted_changed)
    {
        if (histogram[v])
        {
            if (int next = v * 2; (next <= 100'000) && histogram[next])
            {
                --histogram[v];
                --histogram[next];
                result.push_back(v);
            }
            else return {};
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const size_t n = left.size();
    std::vector<int> sort_left(left), sort_right(right);
    std::sort(sort_left.begin(), sort_left.end());
    std::sort(sort_right.begin(), sort_right.end());
    for (size_t i = 0; i < n; ++i)
        if (sort_left[i] != sort_right[i])
            return false;
    return true;
}

void test(std::vector<int> changed,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findOriginalArray(changed);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 4, 2, 6, 8}, {1, 3, 4}, trials);
    test({6, 3, 0, 1}, {}, trials);
    test({1}, {}, trials);
    test({0, 0, 0, 0}, {0, 0}, trials);
    test({2, 1, 2, 4, 2, 4}, {1, 2, 2}, trials);
    return 0;
}


