#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int peakIndexInMountainArray(std::vector<int> arr)
{
    int begin = 0;
    for (int end = static_cast<int>(arr.size()) - 1; begin != end;)
    {
        int mid = (begin + end) / 2;
        if (arr[mid] < arr[mid + 1])
            begin = mid + 1;
        if (arr[mid] > arr[mid + 1])
            end = mid;
    }
    return begin;
}
#else
int peakIndexInMountainArray(std::vector<int> arr)
{
    for (int begin = 0, end = static_cast<int>(arr.size()) - 1; begin != end;)
    {
        if (begin == end)
            return begin;
        else if (end - begin == 1)
        {
            return begin + (arr[begin] < arr[end]);
        }
        else
        {
            int mid = (begin + end) / 2;
            if (arr[mid] > arr[mid - 1]) begin = mid;
            else end = mid;
        }
    }
    return 0;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = peakIndexInMountainArray(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 0}, 1, trials);
    test({0, 2, 1, 0}, 1, trials);
    test({0, 10, 5, 2}, 1, trials);
    return 0;
}


