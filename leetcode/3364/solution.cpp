#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumSumSubarray(std::vector<int> nums, int l, int r)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> accum(n);
    for (int w = 1; w < l; ++w)
        for (int i = w - 1, j = 0; i < n; ++i, ++j)
            accum[j] += nums[i];
    int result = std::numeric_limits<int>::max();
    for (int w = l; w <= r; ++w)
    {
        for (int i = w - 1, j = 0; i < n; ++i, ++j)
        {
            accum[j] += nums[i];
            if (accum[j] > 0) result = std::min(result, accum[j]);
        }
    }
    return (result < std::numeric_limits<int>::max())?result:-1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int l, int r, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumSumSubarray(nums, l, r);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, -2, 1, 4}, 2, 3, 1, trials);
    test({-2, 2, -3, 1}, 2, 3, -1, trials);
    test({1, 2, 3, 4}, 2, 4, 3, trials);
    test({4, -10}, 1, 1, 4, trials);
    return 0;
}


