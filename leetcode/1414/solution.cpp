#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int findMinFibonacciNumbers(int k)
{
    int result = 0, a = 1, b = 1;
    
    while (b <= k)
        a = std::exchange(b, b + a);
    while (a > 0)
    {
        if (a <= k)
        {
            k -= a;
            ++result;
        }
        b = std::exchange(a, b - a);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMinFibonacciNumbers(k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test( 7, 2, trials);
    test(10, 2, trials);
    test(19, 3, trials);
    return 0;
}


