#include "../common/common.hpp"

// ############################################################################
// ############################################################################

class NumMatrix {
public:
    NumMatrix(std::vector<std::vector<int> > &matrix)
    {
        const int n = static_cast<int>(matrix.size()),
                  m = static_cast<int>(matrix[0].size());
        ncols = (matrix.size() > 0)?(m + 1):0;
        integral.resize((n + 1) * ncols, 0);
        int idx = ncols + 1;
        for (int row = 0; row < n; ++row)
        {
            for (int col = 0; col < ncols - 1; ++col)
            {
                integral[idx] = matrix[row][col]
                              + integral[idx - 1]
                              + integral[idx - ncols]
                              - integral[idx - ncols - 1];
                ++idx;
            }
            ++idx;
        }
    }
    
    int sumRegion(int row1, int col1, int row2, int col2)
    {
        ++row2;
        ++col2;
        return integral[col2 + row2 * ncols]
             + integral[col1 + row1 * ncols]
             - integral[col2 + row1 * ncols]
             - integral[col1 + row2 * ncols];
    }
protected:
    std::vector<int> integral;
    int ncols = 0;
};

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix,
          int row1,
          int col1,
          int row2,
          int col2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        NumMatrix nm(matrix);
        result = nm.sumRegion(row1, col1, row2, col2);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 0, 1, 4, 2},
          {5, 6, 3, 2, 1},
          {1, 2, 0, 1, 5},
          {4, 1, 0, 1, 7},
          {1, 0, 3, 0, 5}}, 2, 1, 4, 3, 8, trials);
    test({{3, 0, 1, 4, 2},
          {5, 6, 3, 2, 1},
          {1, 2, 0, 1, 5},
          {4, 1, 0, 1, 7},
          {1, 0, 3, 0, 5}}, 1, 1, 2, 2, 11, trials);
    test({{3, 0, 1, 4, 2},
          {5, 6, 3, 2, 1},
          {1, 2, 0, 1, 5},
          {4, 1, 0, 1, 7},
          {1, 0, 3, 0, 5}}, 1, 2, 2, 4, 12, trials);
    return 0;
}


