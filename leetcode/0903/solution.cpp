#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numPermsDISequence(std::string s)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(s.length());
    std::vector<std::vector<int> > dp(n + 1, std::vector<int>(n + 1));
    
    for (int j = 0; j <= n; ++j) dp[0][j] = 1;
    for (int i = 1; i <= n; ++i)
    {
        if (s[i - 1] == 'I')
            for (int j = n - i, postfixsum = 0; j >= 0; --j)
                postfixsum = (postfixsum + dp[i - 1][j + 1]) % MOD,
                dp[i][j] = postfixsum;
        else
            for (int j = 0, prefix = 0; j <= n - i; ++j)
                prefix = (prefix + dp[i - 1][j]) % MOD,
                dp[i][j] = prefix;
    }
    return dp[n][0];
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numPermsDISequence(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("DID", 5, trials);
    test("D", 1, trials);
    return 0;
}


