#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
bool hasValidPath(std::vector<std::vector<int> > grid)
{
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    int direction[] = {-1, 0, 1, 0, -1};
    if ((m == 1) && (n == 1)) return true;
    auto visited = grid;
    std::vector<std::vector<int> > possible(7);
    possible[1] = {0, 1, 0, 1};
    possible[2] = {1, 0, 1, 0};
    possible[3] = {0, 0, 1, 1};
    possible[4] = {0, 1, 1, 0};
    possible[5] = {1, 0, 0, 1};
    possible[6] = {1, 1, 0, 0};
    std::queue<std::tuple<int, int> > q;
    q.push({0, 0});
    visited[0][0] = -1;
    while (!q.empty())
    {
        auto [x, y] = q.front();
        q.pop();
        for (int d = 0; d < 4; ++d)
        {
            if (possible[grid[x][y]][d])
            {
                int tx = x + direction[d], ty = y + direction[d + 1];
                if ((tx >= 0) && (tx < m) && (ty >= 0) && (ty < n)
                &&  (visited[tx][ty] != -1)
                &&  (possible[grid[tx][ty]][(d + 2) % 4]))
                {
                    q.push({tx, ty});
                    if ((tx + 1 == m) && (ty + 1 == n)) return true;
                    visited[tx][ty] = -1;
                }
            }
        }
    }
    return false;
}
#else
bool hasValidPath(std::vector<std::vector<int> > grid)
{
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    std::vector<std::vector<bool> > g(m * 3, std::vector<bool>(n * 3));
    auto process = [&g](void) -> int
    {
        const int mp = static_cast<int>(g.size()),
                  np = static_cast<int>(g[0].size());
        auto inner = [&](auto &&self, int i, int j) -> bool
        {
            if ((i < 0) || (i == mp) || (j < 0) || (j == np))
                return false;
            if (!g[i][j])
                return false;
            if ((i == mp - 2) && (j == np - 2))
                return true;
            g[i][j] = false;
            return self(self, i + 1, j)
                || self(self, i - 1, j)
                || self(self, i, j + 1) 
                || self(self, i, j - 1);
        };
        return inner(inner, 1, 1);
    };
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            switch (grid[i][j])
            {
            case 1:
                g[i * 3 + 1][j * 3 + 0] = true;
                g[i * 3 + 1][j * 3 + 1] = true;
                g[i * 3 + 1][j * 3 + 2] = true;
                break;
            case 2:
                g[i * 3 + 0][j * 3 + 1] = true;
                g[i * 3 + 1][j * 3 + 1] = true;
                g[i * 3 + 2][j * 3 + 1] = true;
                break;
            case 3:
                g[i * 3 + 1][j * 3 + 0] = true;
                g[i * 3 + 1][j * 3 + 1] = true;
                g[i * 3 + 2][j * 3 + 1] = true;
                break;
            case 4:
                g[i * 3 + 1][j * 3 + 1] = true;
                g[i * 3 + 1][j * 3 + 2] = true;
                g[i * 3 + 2][j * 3 + 1] = true;
                break;
            case 5:
                g[i * 3 + 0][j * 3 + 1] = true;
                g[i * 3 + 1][j * 3 + 0] = true;
                g[i * 3 + 1][j * 3 + 1] = true;
                break;
            case 6:
                g[i * 3 + 0][j * 3 + 1] = true;
                g[i * 3 + 1][j * 3 + 1] = true;
                g[i * 3 + 1][j * 3 + 2] = true;
                break;
            default:
                break;
            }
        }
    }
    return process();
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = hasValidPath(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 4, 3}, {6, 5, 2}}, true, trials);
    test({{1, 2, 1}, {1, 2, 1}}, false, trials);
    test({{1, 1, 2}}, false, trials);
    return 0;
}


