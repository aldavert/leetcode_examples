#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int maxOperations(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<std::vector<int> > mem(n, std::vector<int>(n, -1));
    auto dfs = [&](int x, int y, int s) -> int
    {
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                mem[i][j] = -1;
        auto run = [&](auto &&self, int i, int j) -> int
        {
            if (j - i < 1) return 0;
            if (mem[i][j] != -1) return mem[i][j];
            int result = 0;
            if (nums[i] + nums[i + 1] == s)
                result = std::max(result, 1 + self(self, i + 2, j));
            if (nums[i] + nums[j] == s)
                result = std::max(result, 1 + self(self, i + 1, j - 1));
            if (nums[j - 1] + nums[j] == s)
                result = std::max(result, 1 + self(self, i, j - 2));
            return mem[i][j] = result;
        };
        return run(run, x, y);
    };
    return 1 + std::max({dfs(2, n - 1, nums[0] + nums[1]),
                         dfs(0, n - 3, nums[n - 2] + nums[n - 1]),
                         dfs(1, n - 2, nums[0] + nums[n - 1])});
}
#else
int maxOperations(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::unordered_map<std::string, int> mem;
    auto hash = [](int i, int j, int score)
    {
        return std::to_string(i) + "," + std::to_string(j) + "," + std::to_string(score);
    };
    auto operations = [&](auto &&self, int i, int j, int score)
    {
        if (i >= j) return 0;
        const std::string key = hash(i, j, score);
        if (const auto it = mem.find(key); it != mem.end())
            return it->second;
        const int delete_first_two = (nums[i] + nums[i + 1] == score)
                                   ? 1 + self(self, i + 2, j, score)
                                   : 0;
        const int delete_last_two = (nums[j] + nums[j - 1] == score)
                                  ? 1 + self(self, i, j - 2, score)
                                  : 0;
        const int delete_first_and_last = (nums[i] + nums[j] == score)
                                        ? 1 + self(self, i + 1, j - 1, score)
                                        : 0;
        return mem[key] = std::max({delete_first_two,
                                    delete_last_two,
                                    delete_first_and_last});
    };
    return std::max({operations(operations, 0, n - 1, nums[0] + nums[1]),
                     operations(operations, 0, n - 1, nums[n - 1] + nums[n - 2]),
                     operations(operations, 0, n - 1, nums[0] + nums[n - 1])});
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 1, 2, 3, 4}, 3, trials);
    test({3, 2, 6, 1, 4}, 2, trials);
    return 0;
}


