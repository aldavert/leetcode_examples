# Insert into a Binary Search Tree

You are given the `root` node of a binary search tree (BST) and a `value` to insert into the tree. Return *the root node of the BST after the insertion*. It is **guaranteed** that the new value does not exist in the original BST.

**Notice** that there may exist multiple valid ways for the insertion, as long as the tree remains a BST after insertion. You can return **any of them**.

#### Example 1:
> ```mermaid
> graph TD;
> A1((4))---B1((2))
> A1---C1((7))
> B1---D1((1))
> B1---E1((3))
> A2((4))---B2((2))
> A2---C2((7))
> B2---D2((1))
> B2---E2((3))
> C2---F2((5))
> C2---EMPTY(( ))
> linkStyle 9 stroke-width:0px;
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef marked fill:#FDA,stroke:#000,stroke-width:2px;
> class EMPTY empty;
> class F2 marked;
> ```
> *Input:* `root = [4, 2, 7, 1, 3],  val = 5`  
> *Output:* `[4, 2, 7, 1, 3, 5]`  
> *Explanation:* Another accepted tree is:
> ```mermaid
> graph TD;
> A((5))---B((2))
> A---C((7))
> B---D((1))
> B---E((3))
> E---EMPTY(( ))
> E---F((4))
> linkStyle 4 stroke-width:0px;
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef marked fill:#FDA,stroke:#000,stroke-width:2px;
> class EMPTY empty;
> class A marked;
> ```

#### Example 2:
> *Input:* `root = [40, 20, 60, 10, 30, 50, 70],  val = 25`  
> *Output:* `[40, 20, 60, 10, 30, 50, 70, null, null, 25]`

#### Example 3:
> *Input:* `root = [4, 2, 7, 1, 3, null, null, null, null, null, null],  val = 5`  
> *Output:* `[4, 2, 7, 1, 3, 5]`
 
#### Constraints:
- The number of nodes in the tree will be in the range `[0, 10^4]`.
- `-10^8 <= Node.val <= 10^8`
- All the values `Node.val` are **unique**.
- `-10^8 <= val <= 10^8`
- It's **guaranteed** that `val` does not exist in the original BST.


