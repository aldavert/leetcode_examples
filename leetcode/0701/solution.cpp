#include "../common/common.hpp"
#include "../common/tree.hpp"


// ############################################################################
// ############################################################################

TreeNode * insertIntoBST(TreeNode * root, int val)
{
    if (root == nullptr)
        return new TreeNode(val);
    for (TreeNode * current = root; true; )
    {
        if (current->val > val)
        {
            if (current->left == nullptr)
            {
                current->left = new TreeNode(val);
                break;
            }
            else current = current->left;
        }
        else
        {
            if (current->right == nullptr)
            {
                current->right = new TreeNode(val);
                break;
            }
            else current = current->right;
        }
    }
    return root;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          int val,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        root = insertIntoBST(root, val);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 7, 1, 3}, 5, {4, 2, 7, 1, 3, 5}, trials);
    test({40, 20, 60, 10, 30, 50, 70}, 25,
         {40, 20, 60, 10, 30, 50, 70, null, null, 25}, trials);
    test({4, 2, 7, 1, 3, null, null, null, null, null, null}, 5,
         {4, 2, 7, 1, 3, 5}, trials);
    return 0;
}


