#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool strongPasswordCheckerII(std::string password)
{
    std::unordered_set<char> special = {
        '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '+'};
    if (password.size() < 8) return false;
    int status = 0;
    for (char previous = '\0'; char letter : password)
    {
        if (letter == previous) return false;
        previous = letter;
        if      ((letter >= 'a') && (letter <= 'z')) status = status | 1;
        else if ((letter >= 'A') && (letter <= 'Z')) status = status | 2;
        else if ((letter >= '0') && (letter <= '9')) status = status | 4;
        else if (special.find(letter) != special.end()) status = status | 8;
    }
    return status == 15;
}

// ############################################################################
// ############################################################################

void test(std::string password, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = strongPasswordCheckerII(password);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("IloveLe3tcode!", true, trials);
    test("Me+You--IsMyDream", false, trials);
    test("1aB!", false, trials);
    return 0;
}


