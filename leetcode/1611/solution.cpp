#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumOneBitOperations(int n)
{
    if (n <= 1) return n;
    int bit = 0;
    while (1 << bit <= n) ++bit;
    --bit;
    return ((1 << (bit + 1)) - 1) - minimumOneBitOperations(n - (1 << bit));
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumOneBitOperations(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 2, trials);
    test(6, 4, trials);
    return 0;
}


