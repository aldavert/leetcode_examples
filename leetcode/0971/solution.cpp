#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

std::vector<int> flipMatchVoyage(TreeNode * root, std::vector<int> voyage)
{
    auto process = [&]() -> std::vector<int>
    {
        const int n = static_cast<int>(voyage.size());
        std::vector<int> result, done = {-1};
        int i = 0;
        auto inner = [&](auto &&self, TreeNode * node) -> void
        {
            if (node == nullptr) return;
            if (result == done) return;
            if (node->val != voyage[i++])
            {
                result.clear();
                result.push_back(-1);
                return;
            }
            if ((i < n) && node->left && (node->left->val != voyage[i]))
            {
                result.push_back(node->val);
                self(self, node->right);
                self(self, node->left);
            }
            else
            {
                self(self, node->left);
                self(self, node->right);
            }
        };
        inner(inner, root);
        return result;
    };
    return process();
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<int> work_left(left), work_right(right);
    std::sort(work_left.begin(), work_left.end());
    std::sort(work_right.begin(), work_right.end());
    for (size_t i = 0; i < left.size(); ++i)
        if (work_left[i] != work_right[i])
            return false;
    return true;
}

void test(std::vector<int> tree,
          std::vector<int> voyage,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = flipMatchVoyage(root, voyage);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2}, {2, 1}, {-1}, trials);
    test({1, 2, 3}, {1, 3, 2}, {1}, trials);
    test({1, 2, 3}, {1, 2, 3}, {}, trials);
    test({4, 5, 3, 2, 7, 1, 6, null, null, null, null, null, null, null, null},
         {4, 1, 5, 6, 2, 6, 3}, {-1}, trials);
    return 0;
}


