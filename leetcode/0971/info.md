# Flip Binary Tree to Match Preorder Traversal

You are given the `root` of a binary tree with `n` nodes, where each node is uniquely assigned a value from `1` to `n`. You are also given a sequence of `n` values `voyage`, which is the **desired pre-order traversal** of the binary tree.

Any node in the binary tree can be **flipped** by swapping its left and right subtrees. For example, flipping node `1` will have the following effect:
```mermaid
flowchart LR;
subgraph S1 [ ]
direction TB;
A1((1))---B1((2))
A1---C1((3))
C1---D1((4))
C1---E1((5))
end
subgraph S2 [ ]
direction TB;
A2((1))---B2((3))
A2---C2((2))
B2---D2((4))
B2---E2((5))
end
S1-->S2
classDef default fill:#FFF,stroke:#000,stroke-width:2px;
classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
class S1,S2 empty;
```
Flip the **smallest** number of nodes so that the **pre-order traversal** of the tree **matches** `voyage`.

Return *a list of the values of all* ***flipped*** *nodes*.* *You may return the answer in* ***any order***. *If it is* ***impossible*** *to flip the nodes in the tree to make the pre-order traversal match* `voyage`, *return the list* `[-1]`.

#### Example 1:
> ```mermaid 
> graph TD;
> A((1))---B((2))
> A---E(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class E empty;
> linkStyle 1 stroke-width:0px
> ```
> *Input:* `root = [1, 2], voyage = [2, 1]`  
> *Output:* `[-1]`  
> *Explanation:* It is impossible to flip the nodes such that the pre-order traversal matches voyage.

#### Example 2:
> ```mermaid 
> graph TD;
> A((1))---B((2))
> A---C((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [1, 2, 3], voyage = [1, 3, 2]`  
> *Output:* `[1]`  
> *Explanation:* Flipping node `1` swaps nodes `2` and `3`, so the pre-order traversal matches voyage.

#### Example 3:
> ```mermaid 
> graph TD;
> A((1))---B((2))
> A---C((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [1, 2, 3], voyage = [1, 2, 3]`  
> *Output:* `[]`  
> *Explanation:* The tree's pre-order traversal already matches voyage, so no nodes need to be flipped.

#### Constraints:
- The number of nodes in the tree is `n`.
- `n == voyage.length`
- `1 <= n <= 100`
- `1 <= Node.val, voyage[i] <= n`
- All the values in the tree are **unique**.
- All the values in `voyage` are **unique**.


