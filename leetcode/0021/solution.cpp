#include "../common/common.hpp"
#include "../common/list.hpp"
#include <utility>

// ############################################################################
// ############################################################################

#if 0
ListNode * mergeTwoLists(ListNode * list1, ListNode * list2)
{
    if (list1 == nullptr) return list2;
    if (list2 == nullptr) return list1;
    auto assign = [](ListNode * &ln) -> ListNode *
    {
        ListNode * out = ln;
        ln = ln->next;
        return out;
    };
    ListNode * result = (list1->val < list2->val)?assign(list1):assign(list2);
    ListNode * last = result;
    while (list1 && list2)
    {
        last->next = (list1->val < list2->val)?assign(list1):assign(list2);
        last = last->next;
    }
    last->next = (list1)?list1:list2;
    return result;
}
#else
ListNode * mergeTwoLists(ListNode * list1, ListNode * list2)
{
    if (list1 == nullptr) return list2;
    if (list2 == nullptr) return list1;
    ListNode * result = (list1->val < list2->val)?
                            std::exchange(list1, list1->next):
                            std::exchange(list2, list2->next);
    ListNode * last = result;
    while (list1 && list2)
    {
        last->next = (list1->val < list2->val)?
                            std::exchange(list1, list1->next):
                            std::exchange(list2, list2->next);
        last = last->next;
    }
    last->next = (list1)?list1:list2;
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> list1,
          std::vector<int> list2,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head1 = vec2list(list1);
        ListNode * head2 = vec2list(list2);
        ListNode * headm = mergeTwoLists(head1, head2);
        result = list2vec(headm);
        delete headm;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 4}, {1, 3, 4}, {1, 1, 2, 3, 4, 4}, trials);
    test({}, {}, {}, trials);
    test({}, {0}, {0}, trials);
    test({1, 2, 4, 9, 11}, {1, 3, 4}, {1, 1, 2, 3, 4, 4, 9, 11}, trials);
    test({1, 2, 4}, {1, 3, 4, 9, 11}, {1, 1, 2, 3, 4, 4, 9, 11}, trials);
    return 0;
}

