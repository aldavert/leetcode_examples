#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long minimumFuelCost(std::vector<std::vector<int> > roads,
                          [[maybe_unused]] int seats)
{
    std::vector<std::vector<int> > neighbors(roads.size() + 1);
    for (const auto &road : roads)
        neighbors[road[0]].push_back(road[1]),
        neighbors[road[1]].push_back(road[0]);
    long long result = 0;
    auto traverse = [&](auto &&self, int node, int parent) -> int
    {
        int people = 1;
        for (int neighbor : neighbors[node])
            if (neighbor != parent)
                people += self(self, neighbor, node);
        if (node != 0)
            result += people / seats + (people % seats != 0);
        return people;
    };
    traverse(traverse, 0, -1);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > roads,
          int seats,
          int solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumFuelCost(roads, seats);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {0, 2}, {0, 3}}, 5, 3, trials);
    test({{3, 1}, {3, 2}, {1, 0}, {0, 4}, {0, 5}, {4, 6}}, 2, 7, trials);
    test({}, 1, 0, trials);
    return 0;
}


