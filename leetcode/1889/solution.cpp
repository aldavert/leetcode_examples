#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int minWastedSpace(std::vector<int> packages, std::vector<std::vector<int> > boxes)
{
    constexpr int MOD = 1'000'000'007;
    constexpr long INF = 1e11;
    const long packagesSum = std::accumulate(packages.begin(), packages.end(), 0L);
    long min_boxes_sum = INF;
    
    std::sort(packages.begin(), packages.end());
    for (auto &box : boxes)
    {
        std::sort(box.begin(), box.end());
        if (box.back() < packages.back()) continue;
        long accumulate = 0, i = 0;
        for (const int b : box)
        {
            long j = std::lower_bound(packages.begin(), packages.end(), b + 1)
                   - packages.begin();
            accumulate += b * (j - i);
            i = j;
        }
        min_boxes_sum = std::min(min_boxes_sum, accumulate);
    }
    
    return (min_boxes_sum == INF)?-1
                                 :static_cast<int>((min_boxes_sum - packagesSum) % MOD);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> packages,
          std::vector<std::vector<int> > boxes,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minWastedSpace(packages, boxes);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 5}, {{4, 8}, {2, 8}}, 6, trials);
    test({2, 3, 5}, {{1, 4}, {2, 3}, {3, 4}}, -1, trials);
    test({3, 5, 8, 10, 11, 12}, {{12}, {11, 9}, {10, 5, 14}}, 9, trials);
    return 0;
}


