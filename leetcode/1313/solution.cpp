#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> decompressRLElist(std::vector<int> nums)
{
    std::vector<int> result;
    for (size_t i = 0, n = nums.size(); i < n; i += 2)
        for (int j = 0; j < nums[i]; ++j)
            result.push_back(nums[i + 1]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = decompressRLElist(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, {2, 4, 4, 4}, trials);
    test({1, 1, 2, 3}, {1, 3, 3}, trials);
    return 0;
}


