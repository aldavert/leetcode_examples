#include "../common/common.hpp"
#include <array>
#include <queue>

// ############################################################################
// ############################################################################

class WordDictionary
{
    class Trie
    {
    public:
        Trie(void) : m_word(false)
        {
            for (int i = 0; i < m_degree; ++i) m_children[i] = nullptr;
        }
        ~Trie(void)
        {
            for (int i = 0; i < m_degree; ++i) delete m_children[i];
        }
        Trie(const Trie &) = delete;
        Trie(const Trie &&) = delete;
        Trie& operator=(const Trie &) = delete;
        Trie& operator=(const Trie &&) = delete;
        void insert(const std::string &word)
        {
            Trie * ptr = this;
            for (char c : word)
            {
                c -= 'a';
                if (ptr->m_children[c] == nullptr)
                    ptr->m_children[c] = new Trie();
                ptr = ptr->m_children[c];
            }
            ptr->m_word = true;
        }
        const Trie * search(char c) const { return m_children[c - 'a']; }
        short word(void) const { return m_word; }
        const auto& children(void) const { return m_children; }
    private:
        static constexpr int m_degree = 'z' - 'a' + 1;
        std::array<Trie *, m_degree> m_children;
        bool m_word = false;
    };
    Trie m_words;
public:
    WordDictionary(void)
    {
    }
    void addWord(std::string word)
    {
        m_words.insert(word);
    }
    bool search(std::string word)
    {
        const int n = static_cast<int>(word.size());
        std::queue<const Trie *> q;
        
        auto update = [&](const Trie &current, int idx)
        {
            if (word[idx] != '.')
            {
                if (const Trie * ptr = current.search(word[idx]); ptr)
                    q.push(ptr);
            }
            else
            {
                for (auto ptr : current.children())
                    if (ptr)
                        q.push(ptr);
            }
        };
        
        update(m_words, 0);
        if (q.empty()) return false;
        q.push(nullptr);
        bool result = false;
        for (int idx = 1; !q.empty();)
        {
            auto ptr = q.front();
            q.pop();
            if (ptr == nullptr)
            {
                ++idx;
                if (idx > n) break;
                result = false;
                q.push(nullptr);
            }
            else
            {
                if (idx < n)
                    update(*ptr, idx);
                else
                    result = result || ptr->word();
            }
        }
        return result;
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<bool> add,
          std::vector<std::string> word,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    const int n = static_cast<int>(add.size());
    std::vector<bool> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        WordDictionary wd;
        result.clear();
        for (int i = 0; i < n; ++i)
        {
            if (add[i])
                wd.addWord(word[i]);
            else
                result.push_back(wd.search(word[i]));
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({ true,  true,  true,   false, false, false, false},
         {"bad", "dad", "mad",   "pad", "bad", ".ad", "b.."},
         {false, true, true, true}, trials);
    return 0;
}


