#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string largestOddNumber(std::string num)
{
    while (!num.empty() && !((num.back() - '0') & 1)) num.pop_back();
    return num;
}

// ############################################################################
// ############################################################################

void test(std::string num, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestOddNumber(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("52", "5", trials);
    test("4206", "", trials);
    test("35427", "35427", trials);
    return 0;
}


