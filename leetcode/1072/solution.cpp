#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxEqualRowsAfterFlips(std::vector<std::vector<int> > matrix)
{
    const int m = static_cast<int>(matrix.size()),
              n = static_cast<int>(matrix[0].size());
    int result = 0;
    std::vector<int> flip(n);
    std::unordered_set<int> seen;
    
    for (int i = 0; i < m; ++i)
    {
        if (seen.count(i)) continue;
        int count = 0;
        for (int j = 0; j < n; ++j)
            flip[j] = 1 ^ matrix[i][j];
        for (int k = 0; k < m; ++k)
        {
            if ((matrix[k] == matrix[i]) || (matrix[k] == flip))
            {
                seen.insert(k);
                ++count;
            }
        }
        result = std::max(result, count);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxEqualRowsAfterFlips(matrix);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {1, 1}}, 1, trials);
    test({{0, 1}, {1, 0}}, 2, trials);
    test({{0, 0, 0}, {0, 0, 1}, {1, 1, 0}}, 2, trials);
    return 0;
}


