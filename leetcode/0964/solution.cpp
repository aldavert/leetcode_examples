#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int leastOpsExpressTarget(int x, int target)
{
    std::unordered_map<long, long> memo;
    auto process = [&](auto &&self, long value, long goal) -> long
    {
        if (const auto it = memo.find(goal); it != memo.end()) return it->second;
        if (value > goal) return std::min(2 * goal - 1, 2 * (value - goal));
        if (value == goal) return 0;
        long prod = value, n = 0;
        while (prod < goal)
            prod *= value,
            ++n;
        if (prod == goal) return memo[goal] = n;
        long result = self(self, value, goal - prod / value) + n;
        if (prod < 2 * goal)
            result = std::min(result, self(self, value, prod - goal) + n + 1);
        return memo[goal] = result;
    };
    return static_cast<int>(process(process, x, target));
}

// ############################################################################
// ############################################################################

void test(int x, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = leastOpsExpressTarget(x, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 19, 5, trials);
    test(5, 501, 8, trials);
    test(100, 100000000, 3, trials);
    return 0;
}


