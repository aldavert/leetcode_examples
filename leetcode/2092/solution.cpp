#include "../common/common.hpp"
#include <map>

// ############################################################################
// ############################################################################

std::vector<int> findAllPeople(int n,
                               std::vector<std::vector<int> > meetings,
                               int firstPerson)
{
    std::vector<int> result;
    std::map<int, std::vector<std::pair<int, int> > > time_to_pairs;
    std::vector<int> id(n), rank(n);
    for (int i = 0; i < n; ++i) id[i] = i;
    auto find = [&](auto &&self, int i) -> int
    {
        return (id[i] == i)?i:(id[i] = self(self, id[i]));
    };
    auto merge = [&](int u, int v)
    {
        int i = find(find, u), j = find(find, v);
        if (i == j) return;
        if (rank[i] < rank[j]) id[i] = id[j], rank[j] += (rank[i] == rank[j]);
        else id[j] = id[i];
    };
    
    merge(0, firstPerson);
    for (const auto &m : meetings)
        time_to_pairs[m[2]].push_back({m[0], m[1]});
    for (const auto& [_, pairs] : time_to_pairs)
    {
        std::unordered_set<int> people_united;
        for (const auto &[x, y] : pairs)
        {
            merge(x, y);
            people_united.insert(x);
            people_united.insert(y);
        }
        for (const int person : people_united)
            if (find(find, person) != find(find, 0))
                id[person] = person;
    }
    for (int i = 0; i < n; ++i)
        if (find(find, i) == find(find, 0))
        result.push_back(i);
    
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > meetings,
          int firstPerson,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findAllPeople(n, meetings, firstPerson);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, {{1, 2, 5}, {2, 3, 8}, {1, 5, 10}}, 1, {0, 1, 2, 3, 5}, trials);
    test(4, {{3, 1, 3}, {1, 2, 2}, {0, 3, 3}}, 3, {0, 1, 3}, trials);
    test(5, {{3, 4, 2}, {1, 2, 1}, {2, 3, 1}}, 1, {0, 1, 2, 3, 4}, trials);
    return 0;
}


