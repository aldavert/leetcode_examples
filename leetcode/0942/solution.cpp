#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> diStringMatch(std::string s)
{
    std::vector<int> result;
    int left = 0, right = static_cast<int>(s.size());
    for (char c : s)
    {
        if (c == 'I') result.push_back(left++);
        else result.push_back(right--);
    }
    result.push_back(left);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = diStringMatch(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("IDID", {0, 4, 1, 3, 2}, trials);
    test("III", {0, 1, 2, 3}, trials);
    test("DDI", {3, 2, 0, 1}, trials);
    return 0;
}


