#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool validSquare(std::vector<int> p1,
                 std::vector<int> p2,
                 std::vector<int> p3,
                 std::vector<int> p4)
{
    std::vector<int> * p[] = {&p1, &p2, &p3, &p4};
    auto squaredEuclidean = [&p](int i, int j) -> int
    {
        return ((*p[i])[0] - (*p[j])[0]) * ((*p[i])[0] - (*p[j])[0])
             + ((*p[i])[1] - (*p[j])[1]) * ((*p[i])[1] - (*p[j])[1]);
    };
    int square_distance[2] = {-1, -1};
    for (int i = 0; i < 4; ++i)
    {
        for (int j = i + 1; j < 4; ++j)
        {
            int distance = squaredEuclidean(i, j);
            if (distance == 0) return false;
            if      (square_distance[0] == -1) square_distance[0] = distance;
            else if (square_distance[0] == distance) continue;
            else if (square_distance[1] == -1) square_distance[1] = distance;
            else if (square_distance[1] != distance) return false;
        }
    }
    return true;
}
#else
bool validSquare(std::vector<int> p1,
                 std::vector<int> p2,
                 std::vector<int> p3,
                 std::vector<int> p4)
{
    std::vector<int> * points[] = {&p1, &p2, &p3, &p4};
    auto squaredEuclidean = [](const std::vector<int> &l,
                               const std::vector<int> &r) -> int
    {
        return (l[0] - r[0]) * (l[0] - r[0]) + (l[1] - r[1]) * (l[1] - r[1]);
    };
    std::unordered_set<int> distances;
    for (int i = 0; i < 4; ++i)
        for (int j = i + 1; j < 4; ++j)
            distances.insert(squaredEuclidean(*points[i], *points[j]));
    return (distances.find(0) == distances.end()) && (distances.size() == 2);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> p1,
          std::vector<int> p2,
          std::vector<int> p3,
          std::vector<int> p4,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = validSquare(p1, p2, p3, p4);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 0}, {1, 1}, {1, 0}, {0, 1}, true, trials);
    test({0, 0}, {1, 1}, {1, 0}, {0, 12}, false, trials);
    test({1, 0}, {-1, 0}, {0, 1}, {0, -1}, true, trials);
    return 0;
}


