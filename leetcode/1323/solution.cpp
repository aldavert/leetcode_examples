#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximum69Number(int num)
{
    int result = num, factor = 1;
    for (int rest = 0; num; num /= 10, factor *= 10)
    {
        int value = num % 10;
        result = std::max(result, (num + (value == 6) * 3) * factor + rest);
        rest += value * factor;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int num, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximum69Number(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(9669, 9969, trials);
    test(9969, 9999, trials);
    test(9999, 9999, trials);
    return 0;
}


