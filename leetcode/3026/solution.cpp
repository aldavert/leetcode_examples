#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

long long maximumSubarraySum(std::vector<int> nums, int k)
{
    long result = std::numeric_limits<long>::lowest();
    std::unordered_map<int, long> lut;
    for (long prefix = 0; const int num : nums)
    {
        if (auto it = lut.find(num); (it == lut.end()) || (it->second > prefix))
            lut[num] = prefix;
        prefix += num;
        if (auto it = lut.find(num + k); it != lut.end())
            result = std::max(result, prefix - it->second);
        if (auto it = lut.find(num - k); it != lut.end())
            result = std::max(result, prefix - it->second);
    }
    return (result == std::numeric_limits<long>::lowest())?0:result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumSubarraySum(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6}, 1, 11, trials);
    test({-1, 3, 2, 4, 5}, 3, 11, trials);
    test({-1, -2, -3, -4}, 2, -6, trials);
    return 0;
}


