#include "../common/common.hpp"
#include <set>

// ############################################################################
// ############################################################################

std::string strWithout3a3b(int a, int b, char symbol_a = 'a', char symbol_b = 'b')
{
    if (a < b) return strWithout3a3b(b, a, symbol_b, symbol_a);
    if (b == 0) return std::string(std::min(a, 2), symbol_a);
    int use_a = std::min(a, 2), use_b = (a - use_a >= b)?1:0;
    return std::string(use_a, symbol_a) + std::string(use_b, symbol_b)
         + strWithout3a3b(a - use_a, b - use_b, symbol_a, symbol_b);
}

// ############################################################################
// ############################################################################

void test(int a, int b, std::set<std::string> solutions, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = strWithout3a3b(a, b);
    showResult(solutions.find(result) != solutions.end(), solutions, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 2, {"aab", "bab", "bba"}, trials);
    test(4, 1, {"aabaa"}, trials);
    return 0;
}


