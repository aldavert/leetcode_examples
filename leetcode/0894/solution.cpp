#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<TreeNode *> allPossibleFBT(int n)
{
    std::unordered_map<int, std::vector<TreeNode *> > memo;
    auto process = [&](auto &&self, int count) -> std::vector<TreeNode *>
    {
        if (count % 2 == 0) return {};
        if (count == 1) return { new TreeNode(0) };
        if (auto it = memo.find(count); it != memo.end()) return it->second;
        
        std::vector<TreeNode *> result;
        for (int left_count = 0; left_count < count; ++left_count)
        {
            const int right_count = count - 1 - left_count;
            for (TreeNode * left : self(self, left_count))
            {
                for (TreeNode * right : self(self, right_count))
                {
                    result.push_back(new TreeNode(0));
                    result.back()->left = left;
                    result.back()->right = right;
                }
            }
        }
        return memo[count] = result;
    };
    return process(process, n);
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        std::vector<TreeNode *> roots = allPossibleFBT(n);
        result.clear();
        for (auto &root : roots)
        {
            result.push_back(tree2vec(root));
            //delete root; // This is a mess, cannot be deleted without crashing.
                           // Copying the tree nodes correctly will make the code
                           // too slow.
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, {{0, 0, 0, null, null, 0, 0, null, null, 0, 0},
             {0, 0, 0, null, null, 0, 0, 0, 0},
             {0, 0, 0, 0, 0, 0, 0},
             {0, 0, 0, 0, 0, null, null, null, null, 0, 0},
             {0, 0, 0, 0, 0, null, null, 0, 0}}, trials);
    test(3, {{0, 0, 0}}, trials);
    return 0;
}


