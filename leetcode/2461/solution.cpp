#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

long long maximumSubarraySum(std::vector<int> nums, int k)
{
    std::unordered_map<int, int> count;
    long result = 0, sum = 0;
    for (int i = 0, distinct = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        sum += nums[i];
        if (++count[nums[i]] == 1)
            ++distinct;
        if (i >= k)
        {
            if (--count[nums[i - k]] == 0)
                --distinct;
            sum -= nums[i - k];
        }
        if ((i >= k - 1) && (distinct == k))
            result = std::max(result, sum);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumSubarraySum(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 5, 4, 2, 9, 9, 9}, 3, 15, trials);
    test({4, 4, 4}, 3, 0, trials);
    return 0;
}


