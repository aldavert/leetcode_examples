#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> sumOfDistancesInTree(int n, std::vector<std::vector<int> > edges)
{
    std::vector<std::vector<int> > adjacent(n);
    std::vector<int> result(n), subtree(n, 1);
    // Deep-first search (first pass)
    auto dfs = [&](auto &&self, int node, int parent) -> int
    {
        int curr = 0;
        for (int neighbor : adjacent[node])
        {
            if (neighbor != parent)
            {
                curr += self(self, neighbor, node);
                curr += subtree[neighbor];
                subtree[node] += subtree[neighbor];
            }
        }
        return curr;
    };
    // Deep-first search (second pass)
    auto dfs2 = [&](auto &&self, int node, int parent, int now) -> void
    {
        result[node] = now;
        for (int neighbor : adjacent[node])
            if (neighbor != parent)
                self(self, neighbor, node, now - subtree[neighbor]
                                         + subtree[0]
                                         - subtree[neighbor]);
    };
    
    for (const auto &node : edges)
    {
        adjacent[node[0]].push_back(node[1]);
        adjacent[node[1]].push_back(node[0]);
    }
    dfs2(dfs2, 0, -1, dfs(dfs, 0, -1));
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOfDistancesInTree(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, {{0, 1}, {0, 2}, {2, 3}, {2, 4}, {2, 5}}, {8, 12, 6, 10, 10, 10}, trials);
    test(1, {}, {0}, trials);
    test(2, {{1, 0}}, {1, 1}, trials);
    return 0;
}


