#include "../common/common.hpp"
#include <unordered_map>
#include <numeric>

// ############################################################################
// ############################################################################

int subarrayGCD(std::vector<int> nums, int k)
{
    int result = 0;
    std::unordered_map<int, int> gcds;
    
    for (int num : nums)
    {
        if (num % k == 0)
        {
            std::unordered_map<int, int> next{{num, 1}};
            for (const auto& [prev, count] : gcds)
                next[std::gcd(prev, num)] += count;
            result += next[k];
            gcds = next;
        }
        else gcds.clear();
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = subarrayGCD(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({9, 3, 1, 2, 6, 3}, 3, 4, trials);
    test({4}, 7, 0, trials);
    return 0;
}


