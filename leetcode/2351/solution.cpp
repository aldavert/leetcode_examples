#include "../common/common.hpp"

// ############################################################################
// ############################################################################

char repeatedCharacter(std::string s)
{
    bool present[26] = {};
    for (char letter : s)
        if (present[letter - 'a']) return letter;
        else present[letter - 'a'] = true;
    return '\0';
}

// ############################################################################
// ############################################################################

void test(std::string s, char solution, unsigned int trials = 1)
{
    char result = '\n';
    for (unsigned int i = 0; i < trials; ++i)
        result = repeatedCharacter(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abccbaacz", 'c', trials);
    test("abcdd", 'd', trials);
    return 0;
}


