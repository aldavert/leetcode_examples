#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkInclusion(std::string s1, std::string s2)
{
    const int n1 = static_cast<int>(s1.size());
    const int n2 = static_cast<int>(s2.size());
    if (n1 > n2) return false;
    int histogram_s1[26] = {}, histogram_s2[26] = {}, same = 0;
    for (int i = 0; i < n1; ++i)
    {
        ++histogram_s1[s1[i] - 'a'];
        ++histogram_s2[s2[i] - 'a'];
    }
    for (int i = 0; i < 26; ++i)
        if (histogram_s1[i] == histogram_s2[i])
            ++same;
    if (same == 26)
        return true;
    for (int i = n1; i < n2; ++i)
    {
        const int left = s2[i - n1] - 'a';
        const int right = s2[i] - 'a';
        --histogram_s2[left];
        if (histogram_s2[left] == histogram_s1[left])
            ++same;
        else if (histogram_s2[left] + 1 == histogram_s1[left])
            --same;
        ++histogram_s2[right];
        if (histogram_s2[right] == histogram_s1[right])
            ++same;
        else if (histogram_s2[right] - 1 == histogram_s1[right])
            --same;
        if (same == 26) return true;
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::string s1, std::string s2, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkInclusion(s1, s2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ab", "eidbaooo", true , trials);
    test("ab", "eidboaoo", false, trials);
    return 0;
}


