#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
long long largestPerimeter(std::vector<int> nums)
{
    std::sort(nums.begin(), nums.end());
    long long perimeter = nums[0], result = -1;
    for (size_t i = 1, n = nums.size(); i + 1 < n; ++i)
    {
        perimeter += nums[i];
        if (perimeter > nums[i + 1])
            result = std::max(result, perimeter + nums[i + 1]);
    }
    return result;
}
#else
long long largestPerimeter(std::vector<int> nums)
{
    std::sort(nums.begin(), nums.end());
    long long perimeter = std::accumulate(nums.begin(), nums.end(), 0L);
    for (int i = static_cast<int>(nums.size()) - 1; i >= 2; --i)
    {
        perimeter -= nums[i];
        if (perimeter > nums[i])
            return perimeter + nums[i];
    }
    return -1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestPerimeter(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 5, 5}, 15, trials);
    test({1, 12, 1, 2, 5, 50, 3}, 12, trials);
    test({5, 5, 50}, -1, trials);
    test({1, 1, 2}, -1, trials);
    return 0;
}


