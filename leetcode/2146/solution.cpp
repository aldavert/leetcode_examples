#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> >
highestRankedKItems(std::vector<std::vector<int> > grid,
                    std::vector<int> pricing,
                    std::vector<int> start,
                    int k)
{
    constexpr int dirs[4][2] = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
    const int m = static_cast<int>(grid.size());
    const int n = static_cast<int>(grid[0].size());
    const int low = pricing[0];
    const int high = pricing[1];
    std::vector<std::vector<int> > result;
    if ((low <= grid[start[0]][start[1]]) && (grid[start[0]][start[1]] <= high))
    {
        result.push_back({start[0], start[1]});
        if (k == 1)
            return result;
    }
    std::queue<std::pair<int, int> > q{{{start[0], start[1]}}};
    std::vector<std::vector<bool> > seen(m, std::vector<bool>(n));
    seen[start[0]][start[1]] = true;
    auto cmp = [&](const std::vector<int> &a, const std::vector<int> &b) -> bool
    {
        if (grid[a[0]][a[1]] != grid[b[0]][b[1]])
            return grid[a[0]][a[1]] < grid[b[0]][b[1]];
        return (a[0] == b[0])?(a[1] < b[1]):(a[0] < b[0]);
    };
    
    while (!q.empty())
    {
        std::vector<std::vector<int> > neighbors;
        for (int sz = static_cast<int>(q.size()); sz > 0; --sz)
        {
            const auto [i, j] = q.front();
            q.pop();
            for (const auto& [dx, dy] : dirs)
            {
                const int x = i + dx, y = j + dy;
                if ((x < 0) || (x == m) || (y < 0) || (y == n))
                    continue;
                if (!grid[x][y] || seen[x][y])
                    continue;
                if ((low <= grid[x][y]) && (grid[x][y] <= high))
                    neighbors.push_back({x, y});
                seen[x][y] = true;
                q.emplace(x, y);
            }
        }
        std::sort(neighbors.begin(), neighbors.end(), cmp);
        for (const auto &neighbor : neighbors)
        {
            if (static_cast<int>(result.size())  < k) result.push_back(neighbor);
            if (static_cast<int>(result.size()) == k) return result;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<int> pricing,
          std::vector<int> start,
          int k,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = highestRankedKItems(grid, pricing, start, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 0, 1}, {1, 3, 0, 1}, {0, 2, 5, 1}}, {2, 5}, {0, 0}, 3,
         {{0, 1}, {1, 1}, {2, 1}}, trials);
    test({{1, 2, 0, 1}, {1, 3, 3, 1}, {0, 2, 5, 1}}, {2, 3}, {2, 3}, 2,
         {{2, 1}, {1, 2}}, trials);
    test({{1, 1, 1}, {0, 0, 1}, {2, 3, 4}}, {2, 3}, {0, 0}, 3,
         {{2, 1}, {2, 0}}, trials);
    return 0;
}


