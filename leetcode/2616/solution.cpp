#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimizeMax(std::vector<int> nums, int p)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> work(nums);
    std::sort(work.begin(), work.end());
    auto numPairs = [&](int max_difference) -> int
    {
        int pairs = 0;
        for (int i = 1; i < n; ++i)
            if (work[i] - work[i - 1] <= max_difference)
                ++pairs, ++i;
        return pairs;
    };
    
    int left = 0, right = work.back() - work.front();
    while (left < right)
    {
        const int mid = (left + right) / 2;
        if (numPairs(mid) >= p) right = mid;
        else left = mid + 1;
    }
    return left;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int p, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimizeMax(nums, p);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 1, 2, 7, 1, 3}, 2, 1, trials);
    test({4, 2, 1, 2}, 1, 0, trials);
    return 0;
}


