#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string nearestPalindromic(std::string n)
{
    const long long nl = std::stoll(n);
    auto generatePalindrome = [](long long value, bool odd) -> long long
    {
        std::string back = std::to_string(value);
        std::string front = back;
        if (odd) back.pop_back();
        for (size_t i = 0, j = back.size() - 1; i < j; ++i, --j)
            std::swap(back[i], back[j]);
        return std::stoll(front + back);
    };
    auto is10x = [&](void) -> bool
    {
        if (n[0] != '1') return false;
        for (size_t i = 1; i < n.size(); ++i)
            if (n[i] != '0')
                return false;
        return true;
    };
    auto is1x1 = [&](void) -> bool
    {
        if ((n[0] != '1') || (n.back() != '1'))
            return false;
        for (size_t i = 1; i < n.size() - 1; ++i)
            if (n[i] != '0')
                return false;
        return true;
    };
    auto is9x = [&](void) -> bool
    {
        for (size_t i = 0; i < n.size(); ++i)
            if (n[i] != '9')
                return false;
        return true;
    };
    if (n.size() == 1)
    {
        if (n[0] == '0') return "1";
        --n[0];
        return n;
    }
    if (is10x() || is1x1())  return std::string(n.size() - 1, '9');
    else if (is9x()) return std::string("1")
                          + std::string(n.size() - 1, '0')
                          + std::string("1");
    long long half = (n.size() & 1)?std::stoll(n.substr(0, n.size() / 2 + 1))
                                   :std::stoll(n.substr(0, n.size() / 2));
    long long great = generatePalindrome(half + 1, n.size() & 1),
              small = generatePalindrome(half - 1, n.size() & 1),
              equal = generatePalindrome(half    , n.size() & 1);
    long long min_diff = nl - small;
    long long result = small;
    if ((nl != equal) && (std::abs(nl - equal) < min_diff))
    {
        min_diff = std::abs(nl - equal);
        result = equal;
    }
    if (great - nl < min_diff)
    {
        min_diff = great - nl;
        result = great;
    }
    return std::to_string(result);
}

// ############################################################################
// ############################################################################

void test(std::string n, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = nearestPalindromic(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("123", "121", trials);
    test("1", "0", trials);
    return 0;
}


