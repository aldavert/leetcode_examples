# Middle of the Linked List

Given the `head` of a singly linked list, return *the middle node of the linked list*.

If there are two middle nodes, return **the second middle** node.

#### Example 1:
> ```mermaid 
> graph LR;
> A((1))-->B((2))
> B-->C((3))
> C-->D((4))
> D-->E((5))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#F50,stroke:#000,stroke-width:2px;
> class C selected;
> ```
> *Input:* `head = [1, 2, 3, 4, 5]`  
> *Output:* `[3, 4, 5]`  
> *Explanation:* The middle node of the list is node `3`.

#### Example 2:
> ```mermaid 
> graph LR;
> A((1))-->B((2))
> B-->C((3))
> C-->D((4))
> D-->E((5))
> E-->F((6))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#F50,stroke:#000,stroke-width:2px;
> class D selected;
> ```
> *Input:* `head = [1, 2, 3, 4, 5, 6]`  
> *Output:* `[4, 5, 6]`  
> *Explanation:* Since the list has two middle nodes with values `3` and `4`, we return the second one.
 
#### Constraints:
- The number of nodes in the list is in the range `[1, 100]`.
- `1 <= Node.val <= 100`


