#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

ListNode * middleNode(ListNode * head)
{
    ListNode * result;
    int n;
    
    for (result = head, n = 0; result; result = result->next, ++n);
    n /= 2;
    for (result = head; n; result = result->next, --n);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * root = vec2list(tree);
        ListNode * middle = middleNode(root);
        result = list2vec(middle);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, {3, 4, 5}, trials);
    test({1, 2, 3, 4, 5, 6}, {4, 5, 6}, trials);
    return 0;
}


