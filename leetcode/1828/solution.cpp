#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> countPoints(std::vector<std::vector<int> > points,
                             std::vector<std::vector<int> > queries)
{
    auto squared = [](int value) -> int { return value * value; };
    std::vector<int> result;
    
    for (const auto &q : queries)
    {
        int count = 0;
        for (const auto &p : points)
            count += squared(p[0] - q[0]) + squared(p[1] - q[1]) <= squared(q[2]);
        result.push_back(count);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPoints(points, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 3}, {3, 3}, {5, 3}, {2, 2}},
         {{2, 3, 1}, {4, 3, 1}, {1, 1, 2}}, {3, 2, 2}, trials);
    test({{1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}},
         {{1, 2, 2}, {2, 2, 2}, {4, 3, 2}, {4, 3, 3}}, {2, 3, 2, 4}, trials);
    return 0;
}


