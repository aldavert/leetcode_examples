#include "../common/common.hpp"
#include <limits>

// #############################################################################
// #############################################################################

double findMedianSortedArrays(std::vector<int> nums1, std::vector<int> nums2)
{
    constexpr int MINIMUM = std::numeric_limits<int>::lowest();
    constexpr int MAXIMUM = std::numeric_limits<int>::max();
    const std::vector<int> &vec_long  = (nums1.size() > nums2.size())?nums1:nums2;
    const std::vector<int> &vec_short = (nums1.size() > nums2.size())?nums2:nums1;
    const int n = static_cast<int>(vec_long.size());
    const int m = static_cast<int>(vec_short.size());
    if (n == 0)
        return (m % 2 == 1)?
                    vec_short[m / 2]:
                    static_cast<double>(vec_short[m / 2] + vec_short[m / 2 - 1]) / 2.0;
    if (m == 0)
        return (n % 2 == 1)?
                    vec_long[n / 2]:
                    static_cast<double>(vec_long[n / 2] + vec_long[n / 2 - 1]) / 2.0;

    int l = 0, r = m * 2;
    int mid1, mid2, low_long, low_short, high_long, high_short;
    auto set = [&]()
    {
        mid2 = (l + r) >> 1;
        mid1 = n + m - mid2;
        low_long  = (mid1 == 0)?MINIMUM:vec_long[(mid1 - 1) >> 1];
        low_short = (mid2 == 0)?MINIMUM:vec_short[(mid2 - 1) >> 1];
        high_long  = (mid1 == 2 * n)?MAXIMUM:vec_long[mid1 >> 1];
        high_short = (mid2 == 2 * m)?MAXIMUM:vec_short[mid2 >> 1];
    };
    set();
    while (l <= r)
    {
        set();
        if      (low_long  > high_short) l = mid2 + 1;
        else if (low_short > high_long)  r = mid2 - 1;
        else return (std::max(low_long, low_short)
                  +  std::min(high_long, high_short)) / 2.0;
    }
    return (std::max(low_long, low_short) + std::min(high_long, high_short)) / 2.0;
}

// #############################################################################
// #############################################################################

void test(std::vector<int> num1,
          std::vector<int> num2,
          double solution,
          unsigned int trials = 1)
{
    double result = 1.0 - solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMedianSortedArrays(num1, num2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 3, 3, 3, 3, 3, 3, 4, 4, 5, 5}, {6, 6, 6, 7, 8, 8, 9, 9}, 4, trials);
    test({1, 2, 2, 3, 3, 3, 3}, {3, 3, 4, 4, 5, 5, 6, 6, 6, 7, 8, 8, 9, 9}, 4, trials);
    test({2, 3, 3, 3, 4, 5, 6, 6, 8, 9}, {1, 2, 3, 3, 3, 4, 5, 6, 7, 8, 9}, 4, trials);
    test({1, 3}, {2}, 2.0, trials);
    test({1, 2}, {3, 4}, 2.5, trials);
    test({0, 0}, {0, 0}, 0.0, trials);
    test({}, {1}, 1.0, trials);
    test({2}, {}, 2.0, trials);
    test({}, {1, 2}, 1.5, trials);
    test({2, 3}, {}, 2.5, trials);
    return 0;
}


