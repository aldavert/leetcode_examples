#include "../common/common.hpp"

// ############################################################################
// ############################################################################


int maximumRemovals(std::string s, std::string p, std::vector<int> removable)
{
    auto remove = [&](int k) -> std::string
    {
        std::string removed(s);
        for (int i = 0; i < k; ++i)
            removed[removable[i]] = '*';
        return removed;
    };
    auto isSubsequence = [&](const std::string &str) -> bool
    {
        for (size_t j = 0, i = 0; j < str.length(); ++j)
            if (p[i] == str[j])
                if (++i == p.length())
                    return true;
        return false;
    };
    
    int l = 0, r = static_cast<int>(removable.size()) + 1;
    while (l < r)
    {
        int m = (l + r) / 2;
        std::string removed = remove(m);
        if (isSubsequence(removed)) l = m + 1;
        else r = m;
    }
    return l - 1;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::string p,
          std::vector<int> removable,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumRemovals(s, p, removable);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcacb", "ab", {3, 1, 0}, 2, trials);
    test("abcbddddd", "abcd", {3, 2, 1, 4, 5, 6}, 1, trials);
    test("abcab", "abc", {0, 1, 2, 3, 4}, 0, trials);
    return 0;
}


