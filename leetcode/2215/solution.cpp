#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > findDifference(std::vector<int> nums1,
                                              std::vector<int> nums2)
{
    int histogram1[2002] = {}, histogram2[2002] = {};
    for (int number : nums1) ++histogram1[number + 1000];
    for (int number : nums2) ++histogram2[number + 1000];
    std::vector<std::vector<int> > result(2);
    for (int i = 0; i <= 2001; ++i)
    {
        if (histogram1[i] && !histogram2[i]) result[0].push_back(i - 1000);
        if (!histogram1[i] && histogram2[i]) result[1].push_back(i - 1000);
    }
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<int> left_copy(left), right_copy(right);
    std::sort(left_copy.begin(), left_copy.end());
    std::sort(right_copy.begin(), right_copy.end());
    for (size_t i = 0, n = left.size(); i < n; ++i)
        if (left_copy[i] != right_copy[i]) return false;
    return true;
}

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findDifference(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, {2, 4, 6}, {{1, 3}, {4, 6}}, trials);
    test({1, 2, 3, 3}, {1, 1, 2, 2}, {{3}, {}}, trials);
    return 0;
}


