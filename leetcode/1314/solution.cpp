#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > matrixBlockSum(std::vector<std::vector<int> > mat, int k)
{
    const int m = static_cast<int>(mat.size()),
              n = static_cast<int>(mat[0].size());
    std::vector<std::vector<int> > result(m, std::vector<int>(n)),
                                   prefix(m + 1, std::vector<int>(n + 1));
    
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            prefix[i + 1][j + 1] = mat[i][j] + prefix[i][j + 1] + prefix[i + 1][j]
                                 - prefix[i][j];
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            const int r1 = std::max(0, i - k) + 1;
            const int c1 = std::max(0, j - k) + 1;
            const int r2 = std::min(m - 1, i + k) + 1;
            const int c2 = std::min(n - 1, j + k) + 1;
            result[i][j] = prefix[r2][c2] - prefix[r2][c1 - 1] - prefix[r1 - 1][c2]
                         + prefix[r1 - 1][c1 - 1];
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > mat,
          int k,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = matrixBlockSum(mat, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, 1,
         {{12, 21, 16}, {27, 45, 33}, {24, 39, 28}}, trials);
    test({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, 2,
         {{45, 45, 45}, {45, 45, 45}, {45, 45, 45}}, trials);
    return 0;
}


