#include "../common/common.hpp"
#include <deque>

// ############################################################################
// ############################################################################

#if 1
int getMoneyAmount(int n)
{
    int dp[201][201] = {};
    for (int upper = 2; upper <= n; ++upper)
    {
        int k0 = upper - 1;
        std::deque<std::pair<int, int> > costs;
        
        for (int lower = upper - 1; lower > 0; --lower)
        {
            int right_cost = lower + dp[lower + 1][upper];
            for (; dp[lower][k0 - 1] > dp[k0 + 1][upper]; --k0)
                if (!costs.empty() && (costs.front().second == k0))
                    costs.pop_front();
            while (!costs.empty() && (right_cost < costs.back().first))
                costs.pop_back();
            costs.push_back(std::make_pair(right_cost, lower));
            dp[lower][upper] = std::min(dp[lower][k0] + k0 + 1, costs.front().first);
        }
    }
    return dp[1][n];
}
#else
int getMoneyAmount(int n)
{
    constexpr int INF = 1'000'000'000;
    int cache[202][202];
    for (int i = 0; i < n + 2; ++i)
        for (int j = 0; j < n + 2; ++j)
            cache[i][j] = INF;
    auto dp = [&](auto &&self, int l, int r) -> int
    {
        if (l >= r) return 0;
        if (cache[l][r] != INF) return cache[l][r];
        for (int x = l; x <= r; ++x)
            cache[l][r] = std::min(cache[l][r],
                                   x + std::max(self(self, l, x - 1),
                                                self(self, x + 1, r)));
        return cache[l][r];
    };
    return dp(dp, 1, n);
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getMoneyAmount(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, 16, trials);
    test( 1,  0, trials);
    test( 2,  1, trials);
    return 0;
}


