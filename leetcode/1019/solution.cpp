#include "../common/common.hpp"
#include "../common/list.hpp"
#include <stack>

// ############################################################################
// ############################################################################

std::vector<int> nextLargerNodes(ListNode * head)
{
    std::vector<int> result;
    std::stack<size_t> s;
    
    for (; head; head = head->next)
    {
        while (!s.empty() && (head->val > result[s.top()]))
        {
            result[s.top()] = head->val;
            s.pop();
        }
        s.push(result.size());
        result.push_back(head->val);
    }
    for (; !s.empty(); s.pop()) result[s.top()] = 0;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> list, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(list);
        result = nextLargerNodes(head);
        delete head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 5}, {5, 5, 0}, trials);
    test({2, 7, 4, 3, 5}, {7, 0, 5, 5, 0}, trials);
    test({1, 7, 5, 1, 9, 2, 5, 1}, {7, 9, 9, 9, 0, 5, 0, 0}, trials);
    return 0;
}


