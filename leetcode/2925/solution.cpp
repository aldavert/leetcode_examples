#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

long long maximumScoreAfterOperations(std::vector<std::vector<int> > edges,
                                      std::vector<int> values)
{
    std::vector<std::vector<int> > tree(values.size());
    auto dfs = [&](auto &&self, int u, int prev) -> long
    {
        if ((u > 0) && (tree[u].size() == 1))
            return values[u];
        long children_sum = 0;
        for (const int v : tree[u])
            if (v != prev)
                children_sum += self(self, v, u);
        return std::min(children_sum, static_cast<long>(values[u]));
    };
    for (const auto &edge : edges)
    {
        tree[edge[0]].push_back(edge[1]);
        tree[edge[1]].push_back(edge[0]);
    }
    return std::accumulate(values.begin(), values.end(), 0L)
         - dfs(dfs, 0, -1);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges,
          std::vector<int> values,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumScoreAfterOperations(edges, values);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {0, 2}, {0, 3}, {2, 4}, {4, 5}}, {5, 2, 5, 2, 1, 1}, 11, trials);
    test({{0, 1}, {0, 2}, {1, 3}, {1, 4}, {2, 5}, {2, 6}},
         {20, 10, 9, 7, 4, 3, 5}, 40, trials);
    return 0;
}


