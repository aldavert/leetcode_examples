#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <iostream>

// ############################################################################
// ############################################################################

int sumNumbers(TreeNode* root)
{
    int result = 0;
    auto search = [&](auto &&self, TreeNode * node, int number) -> void
    {
        number = number * 10 + node->val;
        if (!node->left && !node->right) result += number;
        else
        {
            if (node->left ) self(self, node->left , number);
            if (node->right) self(self, node->right, number);
        }
    };
    search(search, root, 0);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = sumNumbers(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 25, trials);
    test({4, 9, 0, 5, 1}, 1026, trials);
    return 0;
}


