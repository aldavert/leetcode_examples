#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

#if 1
std::string smallestGoodBase(std::string n)
{
    const long num = std::stol(n);
    for (int i = static_cast<int>(std::log2(num + 1)); i >= 2; --i)
    {
        long left = 2, right = static_cast<long>(std::pow(num, 1.0 / (i - 1))) + 1;
        while (left < right)
        {
            long mid = left + (right - left) / 2;
            long sum = 0;
            for (int j = 0; j < i; ++j) sum = sum * mid + 1;
            if (sum == num) return std::to_string(mid);
            else if (sum < num) left = mid + 1;
            else right = mid;
        }
    }
    return std::to_string(num - 1);
}
#else
std::string smallestGoodBase(std::string n)
{
    const long num = std::stol(n);
    for (int m = static_cast<int>(std::log2(num)); m >= 2; --m)
    {
        const int k = static_cast<int>(std::pow(num, 1.0 / m));
        long sum = 1, prod = 1;
        for (int i = 0; i < m; ++i, prod *= k)
            sum += prod;
        if (sum == num) return std::to_string(k);
    }
    return std::to_string(num - 1);
}
#endif

// ############################################################################
// ############################################################################

void test(std::string n, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestGoodBase(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("13", "3", trials);
    test("4681", "8", trials);
    test("1000000000000000000", "999999999999999999", trials);
    return 0;
}


