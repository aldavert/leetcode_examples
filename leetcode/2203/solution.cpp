#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

long long minimumWeight(int n,
                        std::vector<std::vector<int> > edges,
                        int src1,
                        int src2,
                        int dest)
{
    constexpr long KMAX = 1e10;
    std::vector<std::vector<std::pair<int, int> > > graph1(n), graph2(n);
    auto dijkstra = [&](const std::vector<std::vector<std::pair<int, int> > > &graph,
                       int src) -> std::vector<long>
    {
        std::vector<long> distance(graph.size(), KMAX);
        std::priority_queue<std::pair<long, int>,
                            std::vector<std::pair<long, int> >,
                            std::greater<> > min_heap;
        
        min_heap.push({0, src});
        while (!min_heap.empty())
        {
            auto [d, u] = min_heap.top();
            min_heap.pop();
            if (distance[u] != KMAX) continue;
            distance[u] = d;
            for (auto [v, w] : graph[u])
                min_heap.push({d + w, v});
        }
        return distance;
    };
    
    for (const auto &edge : edges)
    {
        graph1[edge[0]].push_back({edge[1], edge[2]});
        graph2[edge[1]].push_back({edge[0], edge[2]});
    }
    
    std::vector<long> from_src1 = dijkstra(graph1, src1),
                      from_src2 = dijkstra(graph1, src2),
                      from_dest = dijkstra(graph2, dest);
    long result = KMAX;
    for (int i = 0; i < n; ++i)
    {
        if (from_src1[i] == KMAX || from_src2[i] == KMAX || from_dest[i] == KMAX)
            continue;
        result = std::min(result, from_src1[i] + from_src2[i] + from_dest[i]);
    }
    return (result == KMAX)?-1:result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          int src1,
          int src2,
          int dest,
          int solution,
          unsigned int trials = 1)
{
    long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumWeight(n, edges, src1, src2, dest);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, {{0, 2, 2}, {0, 5, 6}, {1, 0, 3}, {1, 4, 5}, {2, 1, 1}, {2, 3, 3},
             {2, 3, 4}, {3, 4, 2}, {4, 5, 1}}, 0, 1, 5, 9, trials);
    test(3, {{0, 1, 1}, {2, 1, 1}}, 0, 1, 2, -1, trials);
    return 0;
}


