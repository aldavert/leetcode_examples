#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findPeakElement(std::vector<int> nums)
{
    for (int begin = 0, end = static_cast<int>(nums.size()) - 1; begin != end;)
    {
        if (begin == end)
            return begin;
        else if (end - begin == 1)
            return begin + (nums[begin] < nums[end]);
        else
        {
            int mid = (begin + end) / 2;
            if (nums[mid] > nums[mid - 1]) begin = mid;
            else end = mid;
        }
    }
    return 0;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, const std::unordered_set<int> &solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findPeakElement(nums);
    showResult(solution.find(result) != solution.end(), solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 1}, {2}, trials);
    test({1, 2, 1, 3, 5, 6, 4}, {1, 5}, trials);
    test({1, 6, 5, 4, 3, 2, 1}, {1}, trials);
    return 0;
}


