#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool reachingPoints(int sx, int sy, int tx, int ty)
{
    while ((sx < tx) && (sy < ty))
    {
        if (tx > ty) tx %= ty;
        else         ty %= tx;
    }
    return ((sx == tx) && (sy <= ty) && ((ty - sy) % sx == 0))
        || ((sy == ty) && (sx <= tx) && ((tx - sx) % sy == 0));
}

// ############################################################################
// ############################################################################

void test(int sx, int sy, int tx, int ty, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = reachingPoints(sx, sy, tx, ty);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 1, 3, 5,  true, trials);
    test(1, 1, 2, 2, false, trials);
    test(1, 1, 1, 1,  true, trials);
    return 0;
}


