# Reaching Points

Given four integers `s_x`, `s_y`, `t_x`, and `t_y`, return `true` *if it is possible to convert the point* `(s_x, s_y)` *to the point* `(t_x, t_y)` *through some operations, or* `false` *otherwise.*

The allowed operation on some point `(x, y)` is to convert it to either `(x, x + y)` or `(x + y, y)`.

#### Example 1:
> *Input:* `s_x = 1, s_y = 1, t_x = 3, t_y = 5`  
> *Output:* `true`  
> *Explanation:* One series of moves that transforms the starting point to the target is:
> 1. `(1, 1) -> (1, 2)`
> 2. `(1, 2) -> (3, 2)`
> 3. `(3, 2) -> (3, 5)`

#### Example 2:
> *Input:* `s_x = 1, s_y = 1, t_x = 2, t_y = 2`  
> *Output:* `false`

#### Example 3:
> *Input:* `s_x = 1, s_y = 1, t_x = 1, t_y = 1`  
> *Output:* `true`

#### Constraints:
- `1 <= s_x, s_y, t_x, t_y <= 10^9`


