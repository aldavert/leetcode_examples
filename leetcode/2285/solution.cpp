#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maximumImportance(int n, std::vector<std::vector<int> > roads)
{
    std::vector<long> count(n);
    long result = 0;
    
    for (const auto &road : roads)
    {
        ++count[road[0]];
        ++count[road[1]];
    }
    std::sort(count.begin(), count.end());
    for (int i = 0; i < n; ++i)
        result += (i + 1) * count[i];
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > roads,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumImportance(n, roads);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{0, 1}, {1, 2}, {2, 3}, {0, 2}, {1, 3}, {2, 4}}, 43, trials);
    test(5, {{0, 3}, {2, 4}, {1, 3}}, 20, trials);
    return 0;
}


