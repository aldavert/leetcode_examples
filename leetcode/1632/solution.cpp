#include "../common/common.hpp"
#include <map>
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
std::vector<std::vector<int> > matrixRankTransform(std::vector<std::vector<int> > matrix)
{
    const int m = static_cast<int>(matrix.size());        // Row
    const int n = static_cast<int>(matrix[0].size());     // Column
    const int S = m + n;
    std::vector<int> parent(S), parent_rank(S), rank_row(m), rank_column(n);
    
    auto search = [&](auto &&self, int x) -> int
    {
        if (parent[x] == x) return x;
        return parent[x] = self(self, parent[x]);
    };
    std::vector<std::vector<int>> result(m, std::vector<int>(n, 0));
    std::map<int, std::vector<std::pair<int, int> > > groups;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            groups[matrix[i][j]].push_back({i, j});
    
    for (const auto &[_, group] : groups)
    {
        for (auto [row, col] : group)
        {
            parent[row] = row;
            parent_rank[row] = rank_row[row];
            parent[col + m] = col + m;
            parent_rank[col + m] = rank_column[col];
        }
        for (auto [row, col] : group)
        {
            int a = search(search, row), b = search(search, col + m);
            if (a != b)
            {
                parent[a] = b;
                parent_rank[b] = std::max(parent_rank[a], parent_rank[b]);
            }
        }
        for (auto [row, col] : group)
        {
            int rank = parent_rank[search(search, row)] + 1;
            result[row][col] = rank;
            rank_row[row] = rank;
            rank_column[col] = rank;
        }
    }
    
    return result;
}
#elif 0
std::vector<std::vector<int> > matrixRankTransform(std::vector<std::vector<int> > matrix)
{
    class UnionFind
    {
    public:
        std::unordered_map<int, int> parent;
        int find(int u)
        {
            if (u == parent[u]) return u;
            return parent[u] = find(parent[u]);
        }
        void add(int u, int v)
        {
            if (parent.find(u) == parent.end()) parent[u] = u;
            if (parent.find(v) == parent.end()) parent[v] = v;
            int pu = find(u), pv = find(v);
            if (pu != pv) parent[pu] = pv;
        }
    };
    const int m = static_cast<int>(matrix.size());
    const int n = static_cast<int>(matrix[0].size());
    const int T = m + n;
    std::map<int, std::vector<std::pair<int, int> > > group_by_value;
    std::vector<std::vector<int> > result(m, std::vector<int>(n, 0));
    
    for (int r = 0; r < m; ++r)
        for (int c = 0; c < n; ++c)
            group_by_value[matrix[r][c]].push_back({r, c});
    std::vector<int> rank(T);
    for (auto const& [_1, cells] : group_by_value)
    {
        std::unordered_map<int, std::vector<int> > groups;
        
        UnionFind uf;
        for (auto [r, c] : cells)
            uf.add(r, c + m);
        for (auto const& [u, _2] : uf.parent)
            groups[uf.find(u)].push_back(u);
        
        for (auto const& [_, group] : groups)
        {
            int max_rank = 0;
            for (int i : group) max_rank = std::max(max_rank, rank[i]);
            for (int i : group) rank[i] = max_rank + 1;
        }
        for (auto const& [r, c] : cells) result[r][c] = rank[r];
    }
    return result;
}
#else
std::vector<std::vector<int> > matrixRankTransform(std::vector<std::vector<int> > matrix)
{
    struct Values
    {
        int value;
        short row;
        short col;
        bool operator<(const Values &other) { return (value < other.value); }
    };
    const int m = static_cast<int>(matrix.size());        // Row
    const int n = static_cast<int>(matrix[0].size());     // Column
    const int T = m * n;
    const int S = m + n;
    std::vector<int> parent(S), parent_rank(S), rank_row(m), rank_column(n);
    
    auto search = [&](auto &&self, int x) -> int
    {
        if (parent[x] == x) return x;
        return parent[x] = self(self, parent[x]);
    };
    std::vector<std::vector<int>> result(m, std::vector<int>(n, 0));
    std::vector<Values> values(T);
    for (int j = 0, k = 0; j < m; ++j)
        for (int i = 0; i < n; ++i, ++k)
            values[k] = { matrix[j][i], static_cast<short>(j), static_cast<short>(i) };
    std::sort(&values[0], &values[T]);

    auto update = [&](int begin, int end)
    {
        for (int k = begin; k < end; ++k)
        {
            auto [_, row, col] = values[k];
            parent[row] = row;
            parent_rank[row] = rank_row[row];
            parent[col + m] = col + m;
            parent_rank[col + m] = rank_column[col];
        }
        for (int k = begin; k < end; ++k)
        {
            auto [_, row, col] = values[k];
            int a = search(search, row), b = search(search, col + m);
            if (a != b)
            {
                parent[a] = b;
                parent_rank[b] = std::max(parent_rank[a], parent_rank[b]);
            }
        }
        for (int k = begin; k < end; ++k)
        {
            auto [_, row, col] = values[k];
            int rank = parent_rank[search(search, row)] + 1;
            result[row][col] = rank;
            rank_row[row] = rank;
            rank_column[col] = rank;
        }
    };
    
    int previous = values[0].value;
    int begin = 0;
    for (int i = 1; i < T; ++i)
    {
        if (values[i].value != previous)
        {
            update(begin, i);
            previous = values[i].value;
            begin = i;
        }
    }
    update(begin, T);
    
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = matrixRankTransform(matrix);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {3, 4}}, {{1, 2}, {2, 3}}, trials);
    test({{7, 7}, {7, 7}}, {{1, 1}, {1, 1}}, trials);
    test({{ 20, -21,  14},
          {-19,   4,  19},
          { 22, -47,  24},
          {-19,   4,  19}},
        //-------------------
          {{4, 2, 3},
           {1, 3, 4},
           {5, 1, 6},
           {1, 3, 4}},trials);
    test({{7, 3, 6},
          {1, 4, 5},
          {9, 8, 2}},
        //------------
         {{5, 1, 4},
          {1, 2, 3},
          {6, 3, 1}}, trials);
    test({{-37, -50,  -3,  44},
          {-37,  46,  13, -32},
          { 47, -42,  -3, -40},
          {-17, -22, -39,  24}},
        //------------
         {{2, 1, 4, 6},
          {2, 6, 5, 4},
          {5, 2, 4, 3},
          {4, 3, 1, 5}}, trials);
    test({{-37, -26, -47, -40, -13},
          { 22, -11, -44,  47,  -6},
          {-35,   8, -45,  34, -31},
          {-16,  23,  -6, -43, -20},
          { 47,  38, -27,  -8,  43}},
        //------------
         {{ 3,  4,  1,  2,  7},
          { 9,  5,  3, 10,  8},
          { 4,  6,  2,  7,  5},
          { 7,  9,  8,  1,  6},
          {12, 10,  4,  5, 11}}, trials);
    test({{-23,  20, -49, -30, -39, -28,  -5, -14},
          {-19,   4, -33,   2, -47,  28,  43,  -6},
          {-47,  36, -49,   6,  17,  -8, -21, -30},
          {-27,  44,  27,  10,  21,  -8,   3,  14},
          {-19,  12, -25,  34, -27, -48, -37,  14},
          {-47,  40,  23,  46, -39,  48, -41,  18},
          {-27,  -4,   7, -10,   9,  36,  43,   2},
          { 37,  44,  43, -38,  29, -44,  19,  38}},
         {{  7,  13,   1,   5,   4,   6,   9,  8},
          {  8,  11,   2,  10,   1,  12,  14,  9},
          {  2,  14,   1,  11,  13,   7,   5,  3},
          {  3,  19,  16,  12,  14,   7,  10, 13},
          {  8,  12,   6,  14,   5,   1,   4, 13},
          {  2,  16,  15,  17,   4,  18,   3, 14},
          {  3,   7,  11,   6,  12,  13,  14, 10},
          { 16,  19,  18,   3,  15,   2,  11, 17}}, trials);
    test({{  -5, -42, -39, -44,  31, -46,  13, -48,  -5,  30,   1,  48},
          {  31,  42,  33, -44,  -5,  14, -47,  48,   7, -34, -19, -20},
          {  11,   2,  37,  24,  39,  -6, -27,  20,  -1,  14,   5,   8},
          {   7,  38,  29, -28,  -1,  10,  13,  12,   3,  26,   9,  16},
          { -49,  38, -47,  40,  35, -42, -43,  -8, -21,  22, -39,  -8},
          { -17, -38, -15,  24, -41, -46, -19,  16,   3, -42, -39,  24},
          {  -9, -14,  37, -40,  -9,  26,  17, -28,   3,  38, -15, -28},
          { -21, -34,   1,  36, -37,  18,  25,  16,  39,   6,  -3, -20},
          {  -9,  46,  25,  24, -45, -26, -39, -16, -29,  -2, -35,  28},
          {  43,  18,  37,   4, -49,  18,  37,  24,  -9, -46,  49,  48},
          {  43, -46, -39,  -8,  35,  26,  49,  48, -29, -42,   5, -44},
          {  -5,  18,   9, -20,  35, -18, -47,  -4,  -5, -14, -47,  44}},
         //------------------------------------------------------------
         {{  14,   4,   5,   3,  25,   2,  21,   1,  14,  24,  15,  29},
          {  21,  28,  26,   3,  14,  20,   1,  29,  17,   4,  10,   9},
          {  19,  16,  27,  23,  28,  10,   5,  22,  15,  20,  17,  18},
          {  17,  27,  25,   5,  15,  19,  21,  20,  16,  23,  18,  22},
          {   1,  27,   2,  28,  26,   4,   3,  10,   8,  21,   5,  10},
          {   9,   6,  10,  23,   4,   2,   7,  21,  16,   3,   5,  23},
          {  13,  12,  27,   4,  13,  23,  22,   5,  16,  28,  11,   5},
          {   8,   7,  13,  24,   5,  22,  23,  21,  25,  15,  12,   9},
          {  13,  29,  24,  23,   2,   8,   4,   9,   7,  14,   6,  25},
          {  28,  22,  27,  10,   1,  22,  27,  23,   9,   2,  30,  29},
          {  28,   1,   5,   8,  26,  23,  30,  29,   7,   3,  17,   2},
          {  14,  22,  16,   6,  26,   9,   1,  15,  14,  10,   1,  27}}, trials);
    return 0;
}


