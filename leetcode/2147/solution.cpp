#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfWays(std::string corridor)
{
    const int n = static_cast<int>(corridor.size());
    constexpr int MOD = 1'000'000'007;
    long result = 1;
    int n_seats = 0;
    
    for (int i = 0, previous_seat = -1; i < n; ++i)
    {
        if (corridor[i] == 'S')
        {
            if ((++n_seats > 2) && (n_seats & 1))
                result = result * (i - previous_seat) % MOD;
            previous_seat = i;
        }
    }
    return ((n_seats > 1) && (n_seats % 2 == 0))?static_cast<int>(result):0;
}

// ############################################################################
// ############################################################################

void test(std::string corridor, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfWays(corridor);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("SSPPSPS", 3, trials);
    test("PPSPSP", 1, trials);
    test("S", 0, trials);
    return 0;
}


