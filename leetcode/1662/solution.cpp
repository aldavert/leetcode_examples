#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool arrayStringsAreEqual(std::vector<std::string> word1,
                          std::vector<std::string> word2)
{
    size_t w1 = 0, w2 = 0, c1 = 0, c2 = 0;
    while (true)
    {
        if (c1 == word1[w1].size()) { c1 = 0; ++w1; }
        if (c2 == word2[w2].size()) { c2 = 0; ++w2; }
        if ((w1 < word1.size()) && (w2 < word2.size()))
        {
            if (word1[w1][c1] != word2[w2][c2])
                return false;
            ++c1;
            ++c2;
        }
        else break;
    }
    return (w1 == word1.size()) && (w2 == word2.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> word1,
          std::vector<std::string> word2,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = arrayStringsAreEqual(word1, word2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"ab", "c"}, {"a", "bc"}, true, trials);
    test({"a", "cb"}, {"ab", "c"}, false, trials);
    test({"abc", "d", "defg"}, {"abcddefg"}, true, trials);
    return 0;
}


