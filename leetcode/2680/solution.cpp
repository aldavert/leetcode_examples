#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maximumOr(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::vector<long> prefix(n), suffix(n);
    long result = 0;
    
    for (int i = 1; i < n; ++i)
        prefix[i] = prefix[i - 1] | nums[i - 1];
    for (int i = n - 2; i >= 0; --i)
        suffix[i] = suffix[i + 1] | nums[i + 1];
    for (int i = 0; i < n; ++i)
        result = std::max(result,
                          prefix[i] | static_cast<long>(nums[i]) << k | suffix[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumOr(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({12, 9}, 1, 30, trials);
    test({8, 1, 2}, 2, 35, trials);
    return 0;
}


