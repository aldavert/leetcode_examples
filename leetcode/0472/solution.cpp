#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<std::string> findAllConcatenatedWordsInADict(std::vector<std::string> words)
{
    std::vector<std::string> result;
    std::unordered_set<std::string> word_set{words.begin(), words.end()};
    std::unordered_map<std::string, bool> lut;
    auto concat = [&](auto &&self, std::string s) -> bool
    {
        if (auto it = lut.find(s); it != lut.end())
            return it->second;
        const int n = static_cast<int>(s.size());
        for (int i = 1; i < n; ++i)
        {
            const std::string prefix = s.substr(0, i);
            const std::string suffix = s.substr(i);
            if (word_set.count(prefix)
            && (word_set.count(suffix) || self(self, suffix)))
            return lut[s] = true;
        }
        return lut[s] = false;
    };
    for (const std::string &word : words)
        if (concat(concat, word))
            result.push_back(word);
    return result;
}

// ############################################################################
// ############################################################################


void test(std::vector<std::string> words,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findAllConcatenatedWordsInADict(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"cat", "cats", "catsdogcats", "dog", "dogcatsdog", "hippopotamuses", "rat", "ratcatdogcat"}, {"catsdogcats", "dogcatsdog", "ratcatdogcat"}, trials);
    test({"cat","dog","catdog"}, {"catdog"}, trials);
    return 0;
}


