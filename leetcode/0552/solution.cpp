#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int checkRecord(int n)
{
    constexpr int MOD = 1'000'000'007;
    long dp[2][3] = {};
    dp[0][0] = 1;
    for (int i = 0; i < n; ++i)
    {
        long next = (dp[0][0] + dp[0][1] + dp[0][2]) % MOD;
        dp[0][2] = dp[0][1];
        dp[0][1] = dp[0][0];
        dp[0][0] = next;
        next = (next + dp[1][0] + dp[1][1] + dp[1][2]) % MOD;
        dp[1][2] = dp[1][1];
        dp[1][1] = dp[1][0];
        dp[1][0] = next;
    }
    return static_cast<int>((dp[0][0] + dp[0][1] + dp[0][2]
                           + dp[1][0] + dp[1][1] + dp[1][2]) % MOD);
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkRecord(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 8, trials);
    test(1, 3, trials);
    test(10'101, 183'236'316, trials);
    return 0;
}


