#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool findSubarrays(std::vector<int> nums)
{
    std::unordered_set<long> values;
    for (size_t i = 1, n = nums.size(); i < n; ++i)
    {
        long current = static_cast<long>(nums[i]) + static_cast<long>(nums[i - 1]);
        auto [it, inserted] = values.insert(current);
        if (!inserted) return true;
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = findSubarrays(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 4}, true, trials);
    test({1, 2, 3, 4, 5}, false, trials);
    test({0, 0, 0}, true, trials);
    return 0;
}



