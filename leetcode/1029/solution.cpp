#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int twoCitySchedCost(std::vector<std::vector<int> > costs)
{
    constexpr int N = 101;
    const int n = static_cast<int>(costs.size());
    int result = 0;
    int diffB[N];
    for (int i = 0; i < n; ++i)
    {
        result += costs[i][0];
        diffB[i] = costs[i][1] - costs[i][0];
    }
    std::sort(diffB, diffB + n);
    for (int i = 0; i < n / 2; ++i)
        result += diffB[i];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > costs, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = twoCitySchedCost(costs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{10, 20},{30, 200},{400, 50},{30, 20}}, 110, trials);
    test({{259, 770}, {448, 54}, {926, 667}, {184, 139}, {840, 118}, {577, 469}},
         1859, trials);
    test({{515, 563}, {451, 713}, {537, 709}, {343, 819}, {855, 779}, {457, 60},
         {650, 359}, {631, 42}}, 3086, trials);
    return 0;
}


