#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minTimeToVisitAllPoints(std::vector<std::vector<int> > points)
{
    int result = 0;
    for (size_t i = 1, n = points.size(); i < n; ++i)
    {
        const int dx = std::abs(points[i - 1][0] - points[i][0]);
        const int dy = std::abs(points[i - 1][1] - points[i][1]);
        result += std::max(dx, dy);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minTimeToVisitAllPoints(points);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1}, {3, 4}, {-1, 0}}, 7, trials);
    test({{3, 2}, {-2, 2}}, 5, trials);
    return 0;
}


