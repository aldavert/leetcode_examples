#include "../common/common.hpp"
#include <limits>
#include <set>

// ############################################################################
// ############################################################################

#if 1
int maxSumSubmatrix(std::vector<std::vector<int> > matrix, int k)
{
    const int n = static_cast<int>(matrix.size()),
              m = static_cast<int>(matrix[0].size());
    int result = std::numeric_limits<int>::lowest();
    for (int i = 0; i < n; ++i)
    {
        int sum[101] = {};
        for (int j = i; j < n; ++j)
        {
            int max_val = std::numeric_limits<int>::lowest();
            for (int cur = 0, p = 0; p < m; ++p)
            {
                cur += sum[p] += matrix[j][p];
                max_val = std::max(max_val, cur);
                cur = std::max(cur, 0);
            }
            if (max_val == k) return k;
            if (max_val > k)
            {
                std::set<int> cumset{0};
                for (int p = 0, cur = 0; p < m; ++p)
                {
                    cur += sum[p];
                    auto it = cumset.lower_bound(cur - k);
                    if (it != cumset.end())
                    {
                        result = std::max(result, cur - *it);
                        if (result == k) return k;
                    }
                    cumset.insert(cur);
                }
            }
            else result = std::max(result, max_val);
        }
    }
    return result;
}
#else
int maxSumSubmatrix(std::vector<std::vector<int>> matrix, int k)
{
    const int n = static_cast<int>(matrix.size()),
              m = static_cast<int>(matrix[0].size());
    int accum[100][101] = {};
    for (int i = 0; i < n; ++i)
    {
        accum[i][1] = matrix[i][0];
        for (int j = 1; j < m; ++j)
            accum[i][j + 1] = matrix[i][j] + accum[i][j];
    }
    int result = std::numeric_limits<int>::lowest();
    for (int i = 1; i <= m; ++i)
    {
        for (int j = i; j <= m; ++j)
        {
            std::set<int> cumset{0};
            for (int p = 0, cum = 0; p < n; ++p)
            {
                cum += accum[p][j] - accum[p][i - 1];
                auto it = cumset.lower_bound(cum - k);
                if (it != cumset.end())
                {
                    result = std::max(result, cum - *it);
                    if (result == k) return k;
                }
                cumset.insert(cum);
            }
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSumSubmatrix(matrix, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0, 1}, {0, -2, 3}}, 2, 2, trials);
    test({{0, 1}, {-2, 3}}, 3, 3, trials);
    return 0;
}

