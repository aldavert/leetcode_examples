#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numSplits(std::string s)
{
    const int n = static_cast<int>(s.size());
    int result = 0;
    std::vector<int> prefix(n), suffix(n);
    std::unordered_set<int> seen;
    
    for (int i = 0; i < n; ++i)
    {
        seen.insert(s[i]);
        prefix[i] = static_cast<int>(seen.size());
    }
    seen.clear();
    for (int i = n - 1; i >= 0; --i)
    {
        seen.insert(s[i]);
        suffix[i] = static_cast<int>(seen.size());
    }
    for (int i = 0; i + 1 < n; ++i)
        result += (prefix[i] == suffix[i + 1]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numSplits(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aacaba", 2, trials);
    test("abcd", 1, trials);
    return 0;
}


