#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int minimumDistance(std::vector<std::vector<int> > points)
{
    const int n = static_cast<int>(points.size());
    auto manhattan = [&](int i, int j) -> int
    {
        return std::abs(points[i][0] - points[j][0])
             + std::abs(points[i][1] - points[j][1]);
    };
    auto maxDistance = [&](int excluded_index) -> std::pair<int, int>
    {
        int min_sum = std::numeric_limits<int>::max();
        int max_sum = std::numeric_limits<int>::lowest();
        int min_diff = std::numeric_limits<int>::max();
        int max_diff = std::numeric_limits<int>::lowest();
        int min_sum_index = -1;
        int max_sum_index = -1;
        int min_diff_index = -1;
        int max_diff_index = -1;
        for (int i = 0; i < n; ++i)
        {
            if (i == excluded_index) continue;
            const int sum  = points[i][0] + points[i][1],
                      diff = points[i][0] - points[i][1];
            if (sum < min_sum) min_sum = sum, min_sum_index = i;
            if (sum > max_sum) max_sum = sum, max_sum_index = i;
            if (diff < min_diff) min_diff = diff, min_diff_index = i;
            if (diff > max_diff) max_diff = diff, max_diff_index = i;
        }
        return (max_sum - min_sum >= max_diff - min_diff)
             ? std::pair<int, int>(min_sum_index, max_sum_index)
             : std::pair<int, int>(min_diff_index, max_diff_index);
    };
    const auto [i , j ] = maxDistance(-1);
    const auto [xi, yi] = maxDistance(i);
    const auto [xj, yj] = maxDistance(j);
    return std::min(manhattan(xi, yi), manhattan(xj, yj));
}
#else
int minimumDistance(std::vector<std::vector<int> > points)
{
    std::multiset<int> st1, st2;
    for (auto &p : points)
    {
        st1.insert(p[0] + p[1]);
        st2.insert(p[0] - p[1]);
    }
    int result = std::numeric_limits<int>::max();
    for (auto& p : points)
    {
        int v1 = p[0] + p[1], v2 = p[0] - p[1];
        st1.erase(st1.find(v1));
        st2.erase(st2.find(v2));
        result = std::min(result,
                std::max(*st1.rbegin() - *st1.begin(), *st2.rbegin() - *st2.begin()));
        st1.insert(v1);
        st2.insert(v2);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumDistance(points);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 10}, {5, 15}, {10, 2}, {4, 4}}, 12, trials);
    test({{1, 1}, {1, 1}, {1, 1}}, 0, trials);
    return 0;
}


