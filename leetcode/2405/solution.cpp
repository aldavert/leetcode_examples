#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int partitionString(std::string s)
{
    int number_of_substrings = 1;
    for (int mask = 0; char letter : s)
    {
        if (int bit = 1 << (letter - 'a'); bit & mask)
            mask = bit,
            ++number_of_substrings;
        else mask = mask | bit;
    }
    return number_of_substrings;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = partitionString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abacaba", 4, trials);
    test("ssssss", 6, trials);
    return 0;
}


