#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minOperations(int n)
{
    int result = 0;
    while (n > 0)
    {
        if ((n & 3) == 3)
        {
            ++n;
            ++result;
        }
        else if (n % 2 == 1)
        {
            --n;
            ++result;
        }
        else n >>= 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(39, 3, trials);
    test(54, 3, trials);
    return 0;
}


