#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

void flatten(TreeNode * root)
{
    // In-place substitution.
    while (root != nullptr)
    {
        if ((root->left != nullptr) && (root->right != nullptr)) // Rotation
        {
            TreeNode * aux = root->right;
            root->right = std::exchange(root->left, nullptr);
            TreeNode * s = root->right;
            while (s->right != nullptr) s = s->right;
            s->right = aux;
        }
        else if (root->left != nullptr)
            std::swap(root->left, root->right);
        root = root->right;
    }
}

// ############################################################################
// ############################################################################

void test(const std::vector<int> &tree, const std::vector<int> &solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        flatten(root);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 5, 3, 4, null, 6}, {1, null, 2, null, 3, null, 4, null, 5, null, 6}, trials);
    //        1
    //    2       9
    //  3   6   A   D
    // 4 5 7 8 B C E F 
    test({1, 2, 9, 3, 6, 10, 13, 4, 5, 7, 8, 11, 12, 14, 15},
         {1, null, 2, null, 3, null, 4, null, 5, null, 6, null, 7, null, 8, null,
          9, null, 10, null, 11, null, 12, null, 13, null, 14, null, 15}, trials);
    test({}, {}, trials);
    test({0}, {0}, trials);
    return 0;
}


