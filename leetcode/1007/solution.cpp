#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minDominoRotations(std::vector<int> tops, std::vector<int> bottoms)
{
    const int n = static_cast<int>(tops.size());
    int histogram_top[7] = { 0, 0, 0, 0, 0, 0, 0 };
    int histogram_bottom[7] = { 0, 0, 0, 0, 0, 0, 0 };
    int histogram_common[7] = { 0, 0, 0, 0, 0, 0, 0 };
    for (int i = 0; i < n; ++i)
    {
        ++histogram_top[tops[i]];
        ++histogram_bottom[bottoms[i]];
        if (tops[i] == bottoms[i]) ++histogram_common[tops[i]];
    }
    for (int i = 1; i <= 6; ++i)
        if (histogram_top[i] + histogram_bottom[i] - histogram_common[i] == n)
            return n - std::max(histogram_top[i], histogram_bottom[i]);
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tops,
          std::vector<int> bottoms,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDominoRotations(tops, bottoms);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 2, 4, 2, 2}, {5, 2, 6, 2, 3, 2}, 2, trials);
    test({3, 5, 1, 2, 3}, {3, 6, 3, 3, 4}, -1, trials);
    return 0;
}


