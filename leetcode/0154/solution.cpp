#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findMin(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    if (n == 1) return nums[0];
    else if (n == 2) return std::min(nums[0], nums[1]);
    auto search = [&](auto &&self, int begin, int end) -> int
    {
        for (int iter = 0; iter < n; ++iter)
        {
            if (nums[begin] < nums[end])
                return nums[begin];
            else if (end - begin <= 1)
                return std::min(nums[begin], nums[end]);
            int aux = (begin + end) / 2;
            if (nums[aux] < nums[begin])
                end = aux;
            else if (nums[aux] > nums[end])
                begin = aux;
            else
                return std::min(self(self, begin, aux - 1), self(self, aux + 1, end));
        }
        return std::numeric_limits<int>::lowest();
    };
    return search(search, 0, n - 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMin(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 5}, 1, trials);
    test({2, 2, 2, 0, 1}, 0, trials);
    test({4, 5, 6, 7, 0, 1, 4}, 0, trials);
    test({1, 4, 4, 5, 6, 7, 0}, 0, trials);
    test({4, 4, 5, 6, 7, 0, 1}, 0, trials);
    test({5, 6, 7, 0, 1, 4, 4}, 0, trials);
    test({0, 1, 4, 4, 5, 6, 7}, 0, trials);
    test({4, 4, 4, 0, 4, 4, 4}, 0, trials);
    test({4, 4, 0, 4, 4, 4, 4}, 0, trials);
    test({4, 4, 4, 4, 0, 4, 4}, 0, trials);
    return 0;
}


