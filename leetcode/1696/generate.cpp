#include <vector>
#include <cstring>
#include <iostream>
#include <fstream>
#include <stack>
#include <queue>
#include <utility>
#include <limits>
#include <random>

int maxResult(std::vector<int> nums, int k)
{
    const int n = nums.size();
    for (int i = 1; i < n; ++i)
    {
        int max_value = std::numeric_limits<int>::lowest();
        for (int j = 1; j <= k; ++j)
            if (i - j >= 0) max_value = std::max(max_value, nums[i - j]);
        nums[i] += max_value;
    }
    return nums[n - 1];
}

int main(int, char **)
{
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> distrib(-10'000, 10'000);
    std::vector<int> nums(50'000);
    const int k = 10'000;
    for (int &n : nums)
        n = distrib(gen);
    std::ofstream file("values.hpp");
    file << "#include <vector>\n";
    file << "namespace testA\n";
    file << "{\n";
    file << "    const std::vector<int> nums = {";
    bool next = false;
    for (int &n : nums)
    {
        if (next) [[likely]] file << ", ";
        next = true;
        file << n;
    }
    file << "};\n";
    file << "    constexpr int k = " << k << ";\n";
    file << "    constexpr int solution = " << maxResult(nums, k) << ";\n";
    file << "}\n";
    file << "\n";
    
    return 0;
}
