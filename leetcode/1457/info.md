# Pseudo-Palindromic Paths in a Binary Tree

Given a binary tree where node values are digits from `1` to `9`. A path in the binary tree is said to be **pseudo-palindromic** if at least one permutation of the node values in the path is a palindrome.

*Return the number of* ***pseudo-palindromic*** *paths going from the root node to leaf nodes.*

#### Example 1:
> ```mermaid 
> graph TD;
> A((2))---B((3))
> A---C((1))
> B---D((3))
> B---E((1))
> C---EMPTY(( ))
> C---F((1))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef delete fill:#FFF, stroke-width:0px
> class EMPTY delete;
> linkStyle 4 stroke-width:0px
> linkStyle 1,5 stroke:#2B2,stroke-width:5px
> linkStyle 0,2 stroke:#B22,stroke-width:5px
> ```
> *Input:* `root = [2, 3, 1, 3, 1, null, 1]`  
> *Output:* `2`   
> *Explanation:* There are three paths going from the root node to leaf nodes: the left path `[2, 3, 3]`, the right path `[2, 1, 1]`, and the middle path `[2, 3, 1]`. Among these paths only left path and right path are pseudo-palindromic paths since the left path `[2, 3, 3]` can be rearranged in `[3, 2, 3]` (palindrome) and the right path `[2, 1, 1]` can be rearranged in [1,2,1] (palindrome).

#### Example 2:
> ```mermaid 
> graph TD;
> A((2))---B((1))
> A---C((1))
> B---D((1))
> B---E((3))
> E---EMPTY(( ))
> E---F((1))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef delete fill:#FFF, stroke-width:0px
> class EMPTY delete;
> linkStyle 4 stroke-width:0px
> linkStyle 0,2 stroke:#2B2,stroke-width:5px
> ```
> *Input:* `root = [2, 1, 1, 1, 3, null, null, null, null, null, 1]`  
> *Output:* `1`  
> *Explanation:* There are three paths going from the root node to leaf nodes: the left path `[2, 1, 1]`, the middle path `[2, 1, 3, 1]`, and the right path `[2, 1]`. Among these paths only the left path is pseudo-palindromic since `[2, 1, 1]` can be rearranged in `[1, 2, 1]` (palindrome).

#### Example 3:
> *Input:* `root = [9]`  
> *Output:* `1`

#### Constraints:
- The number of nodes in the tree is in the range `[1, 10^5]`.
- `1 <= Node.val <= 9`


