#include "../common/common.hpp"
#include "../common/tree.hpp"

// ###########################################################################
// ###########################################################################

int pseudoPalindromicPaths(TreeNode * root)
{
    int histogram[10] = {}, result = 0;
    auto backtracking = [&histogram,&result](const TreeNode * root_node) -> void
    {
        auto inner = [&histogram,&result](auto &&self, const TreeNode * node) -> void
        {
            ++histogram[node->val];
            if (!node->left && !node->right)
            {
                int number_of_evens = 0;
                for (int i = 1; i <= 9; ++i)
                    number_of_evens += (histogram[i] & 1);
                result += (number_of_evens <= 1);
            }
            else
            {
                if (node->left)  self(self, node->left);
                if (node->right) self(self, node->right);
            }
            --histogram[node->val];
        };
        inner(inner, root_node);
    };
    backtracking(root);
    return result;
}

// ###########################################################################
// ###########################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = pseudoPalindromicPaths(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 1, 3, 1, null, 1}, 2, trials);
    test({2, 1, 1, 1, 3, null, null, null, null, null, 1}, 1, trials);
    test({9}, 1, trials);
    return 0;
}

