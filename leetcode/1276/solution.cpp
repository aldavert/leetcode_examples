#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> numOfBurgers(int tomatoSlices, int cheeseSlices)
{
    if ((tomatoSlices % 2 == 1)
    ||  (tomatoSlices < 2 * cheeseSlices)
    ||  (tomatoSlices > cheeseSlices * 4))
        return {};
    
    int jumbo_burgers = (tomatoSlices - 2 * cheeseSlices) / 2;
    return {jumbo_burgers, cheeseSlices - jumbo_burgers};
}

// ############################################################################
// ############################################################################

void test(int tomatoSlices,
          int cheeseSlices,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = numOfBurgers(tomatoSlices, cheeseSlices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(16, 7, {1, 6}, trials);
    test(17, 4, {}, trials);
    test(4, 17, {}, trials);
    return 0;
}


