#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countKDifference(std::vector<int> nums, int k)
{
    int present[101] = {};
    int result = 0;
    for (int n : nums)
    {
        int value = n - k;
        result += present[((value >= 1) && (value <= 100)) * value];
        value = k + n;
        result += present[((value >= 1) && (value <= 100)) * value];
        ++present[n];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countKDifference(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 1}, 1, 4, trials);
    test({1, 3}, 3, 0, trials);
    test({3, 2, 1, 5, 4}, 2, 3, trials);
    return 0;
}


