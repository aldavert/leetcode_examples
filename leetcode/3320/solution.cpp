#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countWinningSequences(std::string s)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(s.size());
    std::vector<std::vector<std::vector<int> > > memo(n,
            std::vector<std::vector<int> >(3, std::vector<int>(2 * n, -1)));
    auto count = [&](auto &&self, int i, int prev, int bob) -> long
    {
        if (i == n) return bob > 0;
        int &result = memo[i][prev][bob + s.length()];
        if (result != -1) return result;
        long dragon = 0, serpent = 0, golem = 0;
        switch (s[i])
        {
        case 'E':
            if (prev != 0) dragon  = self(self, i + 1, 0, bob + 1) % MOD;
            if (prev != 1) serpent = self(self, i + 1, 1, bob - 1) % MOD;
            if (prev != 2) golem   = self(self, i + 1, 2, bob    ) % MOD;
            break;
        case 'F':
            if (prev != 0) dragon  = self(self, i + 1, 0, bob    ) % MOD;
            if (prev != 1) serpent = self(self, i + 1, 1, bob + 1) % MOD;
            if (prev != 2) golem   = self(self, i + 1, 2, bob - 1) % MOD;
            break;
        case 'W':
            if (prev != 0) dragon  = self(self, i + 1, 0, bob - 1) % MOD;
            if (prev != 1) serpent = self(self, i + 1, 1, bob    ) % MOD;
            if (prev != 2) golem   = self(self, i + 1, 2, bob + 1) % MOD;
            break;
        default:
            break;
        }
        return result = static_cast<int>(dragon + serpent + golem);
    };
    return static_cast<int>(((count(count, 0, 0, 0)
                            + count(count, 0, 1, 0)
                            + count(count, 0, 2, 0)) / 2) % MOD);
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countWinningSequences(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("FFF", 3, trials);
    test("FWEFW", 18, trials);
    return 0;
}


