#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int splitNum(int num)
{
    int histogram[10] = {}, number_of_digits = 0, sum_nums[2] = {};
    for (; num > 0; num /= 10, ++number_of_digits) ++histogram[num % 10];
    if (number_of_digits & 1) ++histogram[0];
    for (int i = 0, k = 0; i < 10; ++i)
        for (int j = 0; j < histogram[i]; ++j, k = !k)
            sum_nums[k] = sum_nums[k] * 10 + i;
    return sum_nums[0] + sum_nums[1];
}

// ############################################################################
// ############################################################################

void test(int num, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = splitNum(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4325, 59, trials);
    test(687, 75, trials);
    return 0;
}


