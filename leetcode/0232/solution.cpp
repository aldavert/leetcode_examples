#include "../common/common.hpp"
#include <stack>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class MyQueue {
    std::stack<int> m_data;
public:
    MyQueue() {}
    void push(int x)
    {
        std::stack<int> aux;
        while (!m_data.empty())
        {
            aux.push(m_data.top());
            m_data.pop();
        }
        aux.push(x);
        while (!aux.empty())
        {
            m_data.push(aux.top());
            aux.pop();
        }
    }
    int pop()
    {
        int element = m_data.top();
        m_data.pop();
        return element;
    }
    int peek()
    {
        return m_data.top();
    }
    bool empty()
    {
        return m_data.empty();
    }
};

// ############################################################################
// ############################################################################

enum class OP { PUSH, POP, PEEK, EMPTY };

void test(std::vector<OP> operations,
          std::vector<int> input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    const size_t n = operations.size();
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        MyQueue obj;
        for (size_t i = 0; i < n; ++i)
        {
            switch (operations[i])
            {
            case OP::PUSH:
                obj.push(input[i]);
                result.push_back(null);
                break;
            case OP::POP:
                result.push_back(obj.pop());
                break;
            case OP::PEEK:
                result.push_back(obj.peek());
                break;
            case OP::EMPTY:
                result.push_back(obj.empty());
                break;
            default:
                std::cerr << "[ERROR] Unknown operation.\n";
                return;
            };
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::PUSH, OP::PUSH, OP::PEEK, OP::POP, OP::EMPTY},
         {       1,        2,     null,    null,      null},
         {    null,     null,        1,       1,         0}, trials);
    return 0;
}


