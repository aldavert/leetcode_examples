#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 1
int reachableNodes(int n,
                   std::vector<std::vector<int> > edges,
                   std::vector<int> restricted)
{
    std::vector<std::vector<int> > connected(n);
    std::vector<char> blocked(n), visited(n);
    for (int x : restricted)
        blocked[x] = 1;
    for (int i = 0, m = static_cast<int>(edges.size()); i < m; ++i)
    {
        if (!blocked[edges[i][0]] && !blocked[edges[i][1]])
        {
            connected[edges[i][0]].push_back(edges[i][1]);
            connected[edges[i][1]].push_back(edges[i][0]);                
        }            
    }
    std::stack<int> s;
    int result = 0;
    s.push(0);
    while (s.size())
    {
        int next = s.top();
        s.pop();
        if (visited[next]) continue;
        for (int x : connected[next])
            s.push(x);
        visited[next] = 1;
        ++result;
    }
    return result;
}
#else
int reachableNodes(int n,
                   std::vector<std::vector<int> > edges,
                   std::vector<int> restricted)
{
    std::vector<std::vector<int> > tree(n);
    std::vector<bool> seen(n);
    auto dfs = [&](auto &&self, int u) -> int
    {
        if (seen[u]) return 0;
        seen[u] = true;
        int result = 1;
        for (int v : tree[u])
            result += self(self, v);
        return result;
    };
    
    for (auto &edge : edges)
    {
        tree[edge[0]].push_back(edge[1]);
        tree[edge[1]].push_back(edge[0]);
    }
    for (int r : restricted)
        seen[r] = true;
    return dfs(dfs, 0);
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<int> restricted,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = reachableNodes(n, edges, restricted);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, {{0, 1}, {1, 2}, {3, 1}, {4, 0}, {0, 5}, {5, 6}}, {4, 5}, 4, trials);
    test(7, {{0, 1}, {0, 2}, {0, 5}, {0, 4}, {3, 2}, {6, 5}}, {4, 2, 1}, 3, trials);
    return 0;
}


