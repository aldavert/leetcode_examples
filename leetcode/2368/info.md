# Reachable Nodes with Restrictions

There is an undirected tree with `n` nodes labeled from `0` to `n - 1` and `n - 1` edges.

You are given a 2D integer array `edges` of length `n - 1` where `edges[i] = [a_i, b_i]` indicates that there is an edge between nodes `a_i` and `b_i` in the tree. You are also given an integer array `restricted` which represents **restricted** nodes.

Return *the* ***maximum*** *number of nodes you can reach from node* `0` *without visiting a restricted node.*

Note that node `0` will **not** be a restricted node.

#### Example 1:
> ```mermaid 
> graph TD;
> A((0))---B((1))
> A---C((4))
> A---D((5))
> B---E((2))
> B---F((3))
> D---G((6))
> classDef default fill:#FFF,stroke:#000;
> classDef GREEN fill:#AFA,stroke:#040;
> classDef RED fill:#FAA,stroke:#400;
> class A,B,E,F GREEN;
> class C,D RED;
> ```
> *Input:* `n = 7, edges = [[0, 1], [1, 2], [3, 1], [4, 0], [0, 5], [5, 6]], restricted = [4, 5]`  
> *Output:* `4`  
> *Explanation:* The diagram above shows the tree. We have that `[0, 1, 2, 3]` are the only nodes that can be reached from node `0` without visiting a restricted node.

#### Example 2:
> ```mermaid 
> graph TD;
> A((1))---B((0))
> B---C((2))
> B---D((5))
> C---E((3))
> B---F((4))
> D---H((6))
> classDef default fill:#FFF,stroke:#000;
> classDef GREEN fill:#AFA,stroke:#040;
> classDef RED fill:#FAA,stroke:#400;
> class A,C,F RED;
> class B,D,H GREEN;
> ```
> *Input:* `n = 7, edges = [[0, 1], [0, 2], [0, 5], [0, 4], [3, 2], [6, 5]], restricted = [4, 2, 1]`  
> *Output:* `3`  
> *Explanation:* The diagram above shows the tree. We have that `[0, 5, 6]` are the only nodes that can be reached from node `0` without visiting a restricted node.

#### Constraints:
- `2 <= n <= 10^5`
- `edges.length == n - 1`
- `edges[i].length == 2`
- `0 <= a_i, b_i < n`
- `a_i != b_i`
- `edges` represents a valid tree.
- `1 <= restricted.length < n`
- `1 <= restricted[i] < n`
- All the values of `restricted` are unique.


