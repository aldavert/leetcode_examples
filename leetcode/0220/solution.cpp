#include "../common/common.hpp"
#include <set>

// ############################################################################
// ############################################################################

#if 1
bool containsNearbyAlmostDuplicate(std::vector<int> nums,
                                   [[maybe_unused]] int indexDiff,
                                   [[maybe_unused]] int valueDiff)
{
    const int n = static_cast<int>(nums.size());
    std::set<int> window;
    auto update = [&](int i) -> bool
    {
        auto [it, inserted] = window.insert(nums[i]);
        if (!inserted) return true;
        auto nx = std::next(it), pr = std::prev(it);
        return ((it != window.begin()) && (*it - *pr <= valueDiff))
            || ((nx != window.end()) && (*nx - *it <= valueDiff));
    };
    for (int i = 0; i < indexDiff; ++i)
        if (update(i)) return true;
    for (int i = indexDiff; i < n; ++i)
    {
        if (update(i)) return true;
        window.erase(nums[i - indexDiff]);
    }
    return false;
}
#else
bool containsNearbyAlmostDuplicate(std::vector<int> nums,
                                   [[maybe_unused]] int indexDiff,
                                   [[maybe_unused]] int valueDiff)
{
    std::set<long> window;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        const auto it = window.lower_bound(static_cast<long>(nums[i]) - valueDiff);
        if ((it != cend(window)) && (*it - nums[i] <= valueDiff))
            return true;
        window.insert(nums[i]);
        if (i >= indexDiff)
            window.erase(nums[i - indexDiff]);
    }
    return false;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int indexDiff,
          int valueDiff,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = containsNearbyAlmostDuplicate(nums, indexDiff, valueDiff);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 1}, 3, 0, true, trials);
    test({1, 5, 9, 1, 5, 9}, 2, 3, false, trials);
    std::vector<int> testA(200'000);
    for (int i = 0; i < 100'000; ++i)
    {
        testA[i] = 2 * i;
        testA[200'000 - i - 1] = 2 * i + 1;
    }
    test(testA, 1, 1, true, trials);
    return 0;
}


