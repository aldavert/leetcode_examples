#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long makeIntegerBeautiful(long long n, int target)
{
    auto sum = [](long long value) -> int
    {
        int result = 0;
        for (; value > 0; value /= 10) result += static_cast<int>(value % 10);
        return result;
    };
    long result = 0;
    for (long power = 1; sum(n) > target; power *= 10)
    {
        result += power * (10 - n % 10);
        n = n / 10 + 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(long long n, int target, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = makeIntegerBeautiful(n, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(16, 6, 4, trials);
    test(467, 6, 33, trials);
    test(1, 1, 0, trials);
    test(2415682970, 36, 30, trials);
    return 0;
}


