#include "../common/common.hpp"
#include <unordered_map>

constexpr int null = -1000;
constexpr int MAX_SIZE = 1001;

class Node
{
public:
    int val;
    Node * next;
    Node * random;
    Node(int _val)
    {
        val = _val;
        next = nullptr;
        random = nullptr;
    }
    ~Node(void) { delete next; }
};

Node * vec2list(const std::vector<std::pair<int, int> > &vec)
{
    const unsigned int n = static_cast<int>(vec.size());
    Node * buffer[MAX_SIZE];
    for (unsigned int i = 0; i < n; ++i)
        buffer[i] = new Node(vec[i].first);
    buffer[n] = nullptr;
    for (unsigned int i = 0; i < n; ++i)
    {
        buffer[i]->next = buffer[i + 1];
        if (vec[i].second != null)
            buffer[i]->random = buffer[vec[i].second];
    }
    return buffer[0];
}

std::vector<std::pair<int, int> > list2vec(const Node * head)
{
    std::unordered_map<const Node *, unsigned int> lut;
    unsigned int size = 0;
    for (const Node * ptr = head; ptr; ptr = ptr->next, ++size)
        lut[ptr] = size;
    std::vector<std::pair<int, int> > result(size);
    size = 0;
    for (const Node * ptr = head; ptr; ptr = ptr->next, ++size)
        result[size] = { ptr->val, (ptr->random)?lut[ptr->random]:null };
    return result;
}

// ############################################################################
// ############################################################################

Node * copyRandomList(Node * head)
{
    if (head == nullptr) return nullptr;
    std::unordered_map<Node *, unsigned int> lut;
    Node * buffer[MAX_SIZE], * ptr;
    unsigned int size = 0;
    for (ptr = head; ptr; ++size, ptr = ptr->next)
    {
        buffer[size] = new Node(ptr->val);
        lut[ptr] = size;
    }
    buffer[size] = nullptr;
    ptr = head;
    for (unsigned int i = 0; i < size; ++i, ptr = ptr->next)
    {
        buffer[i]->next = buffer[i + 1];
        if (auto it = lut.find(ptr->random); it != lut.end())
            buffer[i]->random = buffer[it->second];
    }
    return buffer[0];
}

// ############################################################################
// ############################################################################

void test(const std::vector<std::pair<int, int> > &solution, unsigned int trials = 1)
{
    std::vector<std::pair<int, int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        Node * head = vec2list(solution);
        Node * copy = copyRandomList(head);
        result = list2vec(copy);
        delete head;
        delete copy;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{7, null}, {13, 0}, {11, 4}, {10, 2}, {1, 0}}, trials);
    test({{1, 1}, {2, 1}}, trials);
    test({{3, null}, {3, 0}, {3, null}}, trials);
    return 0;
}


