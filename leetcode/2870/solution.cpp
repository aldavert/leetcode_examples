#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> nums)
{
    std::unordered_map<int, int> histogram;
    for (size_t i = 0, n = nums.size(); i < n; ++i)
        ++histogram[nums[i]];
    int result = 0;
    for (auto begin = histogram.begin(), end = histogram.end(); begin != end; ++begin)
    {
        if (begin->second == 1) return -1;
        result += begin->second / 3 + (begin->second % 3 > 0);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 3, 2, 2, 4, 2, 3, 4}, 4, trials);
    test({2, 1, 2, 2, 3, 3}, -1, trials);
    return 0;
}


