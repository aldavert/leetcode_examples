#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

std::vector<double> getCollisionTimes(std::vector<std::vector<int> > cars)
{
    const int n = static_cast<int>(cars.size());
    struct Car
    {
        int pos = -1;
        int speed = -1;
        double collision_time = -1;
        inline double collisionTime(int p, int s) const
        {
            return (pos - p) / static_cast<double>(s - speed);
        }
    };
    std::vector<double> result(n);
    std::stack<Car> stack;
    
    for (int i = n - 1; i >= 0; --i)
    {
        const int pos = cars[i][0], speed = cars[i][1];
        while (!stack.empty()
           && ((speed <= stack.top().speed)
           ||  (stack.top().collisionTime(pos, speed) >= stack.top().collision_time)))
            stack.pop();
        if (stack.empty())
        {
            stack.push({pos, speed, std::numeric_limits<int>::max()});
            result[i] = -1;
        }
        else stack.push({pos, speed,
                         result[i] = stack.top().collisionTime(pos, speed)});
    }
    
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<double> &left, const std::vector<double> &right)
{
    if (left.size() != right.size()) return false;
    for (size_t i = 0, n = left.size(); i < n; ++i)
        if (std::abs(left[i] - right[i]) > 1e-5)
            return false;
    return true;
}

void test(std::vector<std::vector<int> > cars,
          std::vector<double> solution,
          unsigned int trials = 1)
{
    std::vector<double> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getCollisionTimes(cars);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 1}, {4, 3}, {7, 2}},
         {1.00000, -1.00000, 3.00000, -1.00000}, trials);
    test({{3, 4}, {5, 4}, {6, 3}, {9, 1}},
         {2.00000, 1.00000, 1.50000, -1.00000}, trials);
    return 0;
}


