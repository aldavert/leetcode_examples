#include "../common/common.hpp"
#include <map>

// ############################################################################
// ############################################################################

bool isPossibleDivide(std::vector<int> nums, int k)
{
    std::map<int, int> count;
    
    for (int number : nums) ++count[number];
    for (auto [start, value] : count)
    {
        if (value <= 0) continue;
        for (int i = start; i < start + k; ++i)
        {
            count[i] -= value;
            if (count[i] < 0)
                return false;
        }
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPossibleDivide(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 3, 4, 4, 5, 6}, 4, true, trials);
    test({3, 2, 1, 2, 3, 4, 3, 4, 5, 9, 10, 11}, 3, true, trials);
    test({1, 2, 3, 4}, 3, false, trials);
    return 0;
}


