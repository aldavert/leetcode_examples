#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minFallingPathSum(std::vector<std::vector<int> > grid)
{
    auto accumulated = grid;
    const int n_rows = static_cast<int>(grid.size());
    const int n_cols = static_cast<int>(grid[0].size());
    for (int i = 1; i < n_rows; ++i)
    {
        std::vector<std::pair<int, int> > sorted(n_cols);
        for (int col = 0; col < n_cols; ++col)
            sorted[col] = { accumulated[i - 1][col], col };
        std::sort(sorted.begin(), sorted.end());
        for (int j = 0; j < n_rows; ++j)
            accumulated[i][j] += (j == std::get<1>(sorted[0]))
                               ? std::get<0>(sorted[1])
                               : std::get<0>(sorted[0]);
    }
    int result = accumulated[n_rows - 1][0];
    for (int col = 1; col < n_cols; ++col)
        result = std::min(result, accumulated[n_rows - 1][col]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minFallingPathSum(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, 13, trials);
    test({{7}}, 7, trials);
    return 0;
}


