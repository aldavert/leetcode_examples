#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int beautySum(std::string s)
{
    const int n = static_cast<int>(s.size());
    int result = 0;
    
    for (int i = 0; i < n; ++i)
    {
        int count[26] = {}, max_frequency = 1, min_frequency = 1,
            min_frequency_counter = 1;
        ++count[s[i] - 'a'];
        for (int j = i + 1; j < n; ++j)
        {
            int current_index = s[j] - 'a',
                current_frequency = ++count[current_index];
            if (current_frequency > max_frequency)
                max_frequency = current_frequency;
            if (current_frequency < min_frequency)
            {
                min_frequency = current_frequency;
                min_frequency_counter = 1;
            }
            else if (current_frequency == min_frequency) ++min_frequency_counter;
            else if ((current_frequency - 1 == min_frequency)
                 &&  (--min_frequency_counter == 0))
            {
                ++min_frequency;
                for (int k = 0; k < 26; ++k)
                    min_frequency_counter += count[k] == min_frequency;
            }
            result += max_frequency - min_frequency;
        }
    }
    return result;
}
#else
int beautySum(std::string s)
{
    const int n = static_cast<int>(s.size());
    int result = 0;
    
    for (int i = 0; i < n; ++i)
    {
        int count[26] = {};
        for (int j = i; j < n; ++j)
        {
            ++count[s[j] - 'a'];
            int max_frequency = 0, min_frequency = std::numeric_limits<int>::max();
            for (int k = 0; k < 26; ++k)
            {
                if (count[k] == 0) continue;
                min_frequency = std::min(min_frequency, count[k]);
                max_frequency = std::max(max_frequency, count[k]);
            }
            result += max_frequency - min_frequency;
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = beautySum(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aabcb", 5, trials);
    test("aabcbaa", 17, trials);
    return 0;
}


