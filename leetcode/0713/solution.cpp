#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numSubarrayProductLessThanK(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    int left = 0, right = 0, result = 0, prod = 1;
    while (right < n)
    {
        if (prod < k)
        {
            result += right - left;
            prod *= nums[right];
            ++right;
        }
        else
        {
            if (left == right)
            {
                ++left;
                ++right;
            }
            else
            {
                prod /= nums[left];
                ++left;
            }
        }
    }
    while ((left < n) && (prod >= k))
        prod /= nums[left++];
    result += right - left;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numSubarrayProductLessThanK(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 5, 2, 6}, 100, 8, trials);
    test({1, 2, 3}, 0, 0, trials);
    test({10, 9, 10, 4, 3, 8, 3, 3, 6, 2, 10, 10, 9, 3}, 19, 18, trials);
    test({10, 10, 9, 3}, 19, 4, trials);
    return 0;
}

