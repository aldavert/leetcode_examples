#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long validSubstringCount(std::string word1, std::string word2)
{
    long result = 0;
    int required = static_cast<int>(word2.length()), count[26] = {};
    
    for (const char c : word2) ++count[c - 'a'];
    for (int l = 0, r = 0, n = static_cast<int>(word1.size()); r < n; ++r)
    {
        required -= (--count[word1[r] - 'a'] >= 0);
        while (required == 0)
        {
            result += n - r;
            required += (++count[word1[l++] - 'a'] > 0);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word1,
          std::string word2,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = validSubstringCount(word1, word2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("bcca", "abc", 1, trials);
    test("abcabc", "abc", 10, trials);
    test("abcabc", "aaabc", 0, trials);
    return 0;
}


