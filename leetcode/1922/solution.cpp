#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countGoodNumbers(long long n)
{
    static constexpr int MOD = 1'000'000'007;
    auto modPow = [&](auto self, long x, long nn) -> long
    {
        if (nn == 0) return 1;
        if (nn % 2 == 1) return x * self(self, x, nn - 1) % MOD;
        return self(self, x * x % MOD, nn / 2);
    };
    return static_cast<int>(modPow(modPow, 4 * 5, n / 2) * (n & 1 ? 5 : 1) % MOD);
}

// ############################################################################
// ############################################################################

void test(long long n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countGoodNumbers(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 5, trials);
    test(4, 400, trials);
    test(50, 564908303, trials);
    return 0;
}


