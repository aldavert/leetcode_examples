#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumSum(std::vector<int> nums)
{
    std::vector<std::vector<int> > count(9 * 9 + 1);
    for (int num : nums)
    {
        int digit_sum = 0;
        for (int value = num; value > 0; value /= 10) digit_sum += value % 10;
        count[digit_sum].push_back(num);
    }
    int result = -1;
    for (auto &group_nums : count)
    {
        if (group_nums.size() < 2)
            continue;
        std::sort(group_nums.begin(), group_nums.end(), std::greater<>());
        result = std::max(result, group_nums[0] + group_nums[1]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({18, 43, 36, 13, 7}, 54, trials);
    test({10, 12, 19, 14}, -1, trials);
    return 0;
}


