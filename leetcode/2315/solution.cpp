#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countAsterisks(std::string s)
{
    int result = 0;
    for (bool active = true; char letter : s)
    {
        if (letter == '|') active = !active;
        result += active * (letter == '*');
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countAsterisks(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("l|*e*et|c**o|*de|", 2, trials);
    test("iamprogrammer", 0, trials);
    test("yo|uar|e**|b|e***au|tifu|l", 5, trials);
    return 0;
}


