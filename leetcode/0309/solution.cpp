#include "../common/common.hpp"
#include <unordered_map>

#if 1
int maxProfit(std::vector<int> prices)
{
    const int n = static_cast<int>(prices.size());
    if (n == 1) return 0;
    int a = 0, c = 0;
    for (int i = 0, b = -prices[0]; i < n; ++i)
    {
        int aux = a;
        a = std::max(a, c);
        c = b + prices[i];
        b = std::max(b, aux - prices[i]);
    }
    return std::max(a, c);
}
#elif 0
int maxProfit(std::vector<int> prices)
{
    // Tabulation solution.
    const int n = static_cast<int>(prices.size());
    if (n == 1) return 0;
    std::vector<int> dp(n, 0);
    int accumulated = 0;
    for (int i = 0; i < n; ++i)
    {
        if (i >= 2) accumulated = std::max(dp[i - 2], accumulated);
        if (i >= 1) dp[i] = std::max(dp[i], dp[i - 1]);
        for (int j = i + 1; j < n; ++j)
            dp[j] = std::max(prices[j] - prices[i] + accumulated, dp[j]);
        //for (int j = 0; j < n; ++j)
        //    std::cout << dp[j] << ' ';
        //std::cout << '\n';
    }
    return std::max(dp[n - 1], dp[n - 2]);
    //  |  1  |  2  |  3  |  0  |  2  |  9  |  3  |
    // 1|  -  |0,1  |0,2  |0,-1 |0,1  |0,8  |0,2  |
    // 2|  -  |  -  |2,1  |0,-2 |1,0  |8,7  |2,1  |
    // 3|  -  |  -  |  -  |0,-3 |1,-1 |8,6  |2,0  |
    // 0|  -  |  -  |  -  |  -  |1,2  |8,9  |2,3  |
    // 2|  -  |  -  |  -  |  -  |  -  |9,7  |3,2  |
    // 9|  -  |  -  |  -  |  -  |  -  |  -  |3,-6 |
    // 3|  -  |  -  |  -  |  -  |  -  |  -  |  -  |
}
#else
int maxProfit(std::vector<int> prices)
{
    // Memoization solution
    const int n = static_cast<int>(prices.size());
    std::unordered_map<int, int> memo;
    auto recursive = [&](auto &&self, int day, int price) -> int
    {
        if (day >= n) return 0;
        const int key = static_cast<int>(day) << 16 | (0x0000FFFF & static_cast<int>(price));
        if (auto it = memo.find(key); it != memo.end()) return it->second;
        if (price < 0) // BUYING PHASE ...
            return memo[key] = std::max(self(self, day + 1, prices[day]), // BUY
                                        self(self, day + 1, -1));         // PASS
        else // SELLING PHASE ...
            return memo[key] = std::max(self(self, day + 2, -1) + prices[day] - price, // SELL
                                        self(self, day + 1, price));                   // PASS
    };
    return recursive(recursive, 0, -1);
}
#endif

void test(std::vector<int> prices, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxProfit(prices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 0, 2}, 3, trials);
    test({1, 2, 3, 0, 2, 9, 3}, 10, trials);
    test({1, 2, 3, 0, 2, 9, 3, 1}, 10, trials);
    test({6, 1, 6, 4, 3, 0, 2}, 7, trials);
    test({1, 2, 3, 0, 2, 9, 3, 2, 4, 19, 1, 5, 5, 2, 1, 5, 12, 2, 2, 0}, 38, trials);
    test({1}, 0, trials);
    return 0;
}


