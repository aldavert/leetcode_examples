#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int countNicePairs(std::vector<int> nums)
{
    auto rev = [](int v) -> long
    {
        long result = 0;
        for (; v; v /= 10, result *= 10)
            result += v % 10;
        return result / 10;
    };
    std::unordered_map<long, int> histogram;
    long result = 0;
    for (int value : nums)
    {
        long key = static_cast<long>(value) - rev(value);
        if (auto search = histogram.find(key); search != histogram.end())
        {
            result += search->second;
            ++search->second;
        }
        else histogram[key] = 1;
    }
    return static_cast<int>(result % 1'000'000'007);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countNicePairs(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({42, 11, 1, 97}, 2, trials);
    test({13, 10, 35, 24, 76}, 4, trials);
    return 0;
}


