#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int countUnguarded(int m,
                   int n,
                   std::vector<std::vector<int> > guards,
                   std::vector<std::vector<int> > walls)
{
    constexpr int dirs[5] = {-1, 0, 1, 0, -1};
    std::vector<std::vector<int> > grid(m, std::vector<int>(n));
    for (const auto &element : guards)
        grid[element[0]][element[1]] = 2;
    for (const auto &element : walls)
        grid[element[0]][element[1]] = 2;
    for (auto &element : guards)
    {
        for (int k = 0; k < 4; ++k)
        {
            int x = element[0], y = element[1];
            int dx = dirs[k], dy = dirs[k + 1];
            while ((x + dx >= 0) && (x + dx < m) && (y + dy >= 0) && (y + dy < n)
               &&  (grid[x + dx][y + dy] < 2))
            {
                x += dx;
                y += dy;
                grid[x][y] = 1;
            }
        }
    }
    int result = 0;
    for (const auto &row : grid)
        result += static_cast<int>(std::count(row.begin(), row.end(), 0));
    return result;
}
#else
int countUnguarded(int m,
                   int n,
                   std::vector<std::vector<int> > guards,
                   std::vector<std::vector<int> > walls)
{
    auto recordOrFill = [&](char curr_cell, char &last_cell, char &info_cell) -> void
    {
        if ((curr_cell == 'G') || (curr_cell == 'W'))
            last_cell = curr_cell;
        else info_cell = last_cell;
    };
    std::vector<std::vector<char> > grid(m, std::vector<char>(n)),
        left(m, std::vector<char>(n)), right(m, std::vector<char>(n)),
        up(m, std::vector<char>(n)), down(m, std::vector<char>(n));
    
    for (const auto &guard : guards)
        grid[guard[0]][guard[1]] = 'G';
    for (const auto &wall : walls)
        grid[wall[0]][wall[1]] = 'W';
    for (int i = 0; i < m; ++i)
    {
        char last_cell = 0;
        for (int j = 0; j < n; ++j)
            recordOrFill(grid[i][j], last_cell, left[i][j]);
        last_cell = 0;
        for (int j = n - 1; j >= 0; --j)
            recordOrFill(grid[i][j], last_cell, right[i][j]);
    }
    for (int j = 0; j < n; ++j)
    {
        char last_cell = 0;
        for (int i = 0; i < m; ++i)
            recordOrFill(grid[i][j], last_cell, up[i][j]);
        last_cell = 0;
        for (int i = m - 1; i >= 0; --i)
            recordOrFill(grid[i][j], last_cell, down[i][j]);
    }
    int result = 0;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            result += ((grid[i][j] == 0) && (left[i][j] != 'G')
                   && (right[i][j] != 'G') && (up[i][j] != 'G') && (down[i][j] != 'G'));
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int m,
          int n,
          std::vector<std::vector<int> > guards,
          std::vector<std::vector<int> > walls,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countUnguarded(m, n, guards, walls);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 6, {{0, 0}, {1, 1}, {2, 3}}, {{0, 1}, {2, 2}, {1, 4}}, 7, trials);
    test(3, 3, {{1, 1}}, {{0, 1}, {1, 0}, {2, 1}, {1, 2}}, 4, trials);
    return 0;
}


