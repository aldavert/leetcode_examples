#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string multiply(std::string num1, std::string num2)
{
    if ((num1 == "0") || (num2 == "0")) return "0";
    const int first_size = static_cast<int>(std::max(num1.size(), num2.size()));
    const int second_size = static_cast<int>(std::min(num1.size(), num2.size()));
    char first[201], second[201];
    char multiplication[402 + 20] = {};
    if (num1.size() > num2.size())
    {
        for (int i = 0; i < first_size; ++i)
            first[first_size - i - 1] = num1[i] - '0';
        for (int i = 0; i < second_size; ++i)
            second[second_size - i - 1] = num2[i] - '0';
    }
    else
    {
        for (int i = 0; i < first_size; ++i)
            first[first_size - i - 1] = num2[i] - '0';
        for (int i = 0; i < second_size; ++i)
            second[second_size - i - 1] = num1[i] - '0';
    }
    int length = 0;
    for (int i = 0; i < second_size; ++i)
    {
        int k = i;
        char carry = 0;
        for (int j = 0; j < first_size; ++j, ++k)
        {
            char product = static_cast<char>(multiplication[i + j]
                                           + first[j] * second[i] + carry);
            carry = product / 10;
            multiplication[k] = static_cast<char>(product - carry * 10);
        }
        for (; carry > 0; ++k)
        {
            char aux = carry / 10;
            multiplication[k] = static_cast<char>(carry - aux * 10);
            carry = aux;
        }
        length = std::max(length, k);
    }
    std::string result(length, '\0');
    for (int i = 0; i < length; ++i)
        result[i] = multiplication[length - i - 1] + '0';
    return result;
    ////std::cout << "-----------------------\n";
    ////std::cout << num1 << ' ' << num2 << '\n';
    ////std::cout << "First: ";
    ////for (int i = first_size - 1; i >= 0; --i)
    ////    std::cout << static_cast<int>(first[i]);
    ////std::cout << '\n';
    ////std::cout << "Second: ";
    ////for (int i = second_size - 1; i >= 0 ; --i)
    ////    std::cout << static_cast<int>(second[i]);
    ////std::cout << '\n';
    ////std::cout << "-----------------------\n";
}

// ############################################################################
// ############################################################################

void test(std::string num1,
          std::string num2,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = multiply(num1, num2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("2", "3", "6", trials);
    test("123", "456", "56088", trials);
    test("1024", "32", "32768", trials);
    test("32", "1024", "32768", trials);
    return 0;
}


