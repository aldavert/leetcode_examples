#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
int minSizeSubarray(std::vector<int> nums, int target)
{
    const int n = static_cast<int>(nums.size());
    int current_sum = 0, min_len = std::numeric_limits<int>::max();
    long long total_sum = std::accumulate(nums.begin(), nums.end(), 0L),
              to_add = n * (target / total_sum);
    target = static_cast<int>(target % total_sum);
    for (int start = 0, end = 0; start < n;)
    {
        do
        {
            current_sum += nums[end % n];
            ++end;
        }
        while (current_sum < target);
        while (current_sum > target)
        {
            current_sum -= nums[start % n];
            ++start;
        }
        if (current_sum == target) min_len = std::min(min_len, end - start);
    }
    return (min_len == std::numeric_limits<int>::max())?-1
           :static_cast<int>(min_len + to_add);
}
#else
int minSizeSubarray(std::vector<int> nums, int target)
{
    const long sum = std::accumulate(nums.begin(), nums.end(), 0L);
    const int n = static_cast<int>(nums.size()),
              remaining_target = static_cast<int>(target % sum),
              repeat_length = static_cast<int>((target / sum) * n);
    if (remaining_target == 0) return repeat_length;
    int suffix_plus_prefix_length = n;
    long prefix = 0;
    std::unordered_map<long, int> prefix_to_index{{0, -1}};
    
    for (int i = 0; i < 2 * n; ++i)
    {
        prefix += nums[i % n];
        if (const auto it = prefix_to_index.find(prefix - remaining_target);
            it != prefix_to_index.cend())
            suffix_plus_prefix_length = std::min(suffix_plus_prefix_length,
                                                 i - it->second);
        prefix_to_index[prefix] = i;
    }
    return (suffix_plus_prefix_length == n)?-1:suffix_plus_prefix_length + repeat_length;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSizeSubarray(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 5, 2, trials);
    test({1, 1, 1, 2, 3}, 4, 2, trials);
    test({2, 4, 6, 8}, 3, -1, trials);
    return 0;
}


