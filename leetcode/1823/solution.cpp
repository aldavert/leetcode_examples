#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findTheWinner(int n, int k)
{
    int result = 0;
    for (int i = 2; i <= n; ++i)
        result = (result + k) % i;
    return result + 1;
}

// ############################################################################
// ############################################################################

void test(int n, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findTheWinner(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 2, 3, trials);
    test(6, 5, 1, trials);
    return 0;
}


