#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int balancedStringSplit(std::string s)
{
    int result = 0;
    for (int left = 0, right = 0; char c : s)
    {
        if (c == 'L') ++left;
        else ++right;
        if (left == right)
        {
            ++result;
            left = right = 0;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = balancedStringSplit(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("RLRRLLRLRL", 4, trials);
    test("RLRRRLLRLL", 2, trials);
    test("LLLLRRRR", 1, trials);
    test("RLRLRRLLRL", 4, trials);
    return 0;
}


