#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> stringIndices(std::vector<std::string> wordsContainer,
                               std::vector<std::string> wordsQuery)
{
    class Trie
    {
        Trie * children[26] = {};
        int length = 1 << 30;
        int idx = 1 << 30;
    public:
        Trie(void) = default;
        ~Trie(void) { for (size_t i = 0; i < 26; ++i) delete children[i]; }
        void insert(std::string w, int i)
        {
            const int n = static_cast<int>(w.size());
            Trie * node = this;
            if (node->length > n)
            {
                node->length = n;
                node->idx = i;
            }
            for (int k = n - 1; k >= 0; --k)
            {
                int j= w[k] - 'a';
                if (node->children[j] == nullptr)
                    node->children[j] = new Trie();
                node = node->children[j];
                if (node->length > n)
                {
                    node->length = n;
                    node->idx = i;
                }
            }
        }
        int query(std::string w)
        {
            Trie * node = this;
            for (int k = static_cast<int>(w.size()) - 1; k >= 0; --k)
            {
                int j = w[k] - 'a';
                if (node->children[j] == nullptr)
                    break;
                node = node->children[j];
            }
            return node->idx;
        }
    };
    
    Trie * trie = new Trie();
    for (int i = 0, m = static_cast<int>(wordsContainer.size()); i < m; ++i)
        trie->insert(wordsContainer[i], i);
    int n = static_cast<int>(wordsQuery.size());
    std::vector<int> result(n);
    for (int i = 0; i < n; ++i)
        result[i] = trie->query(wordsQuery[i]);
    delete trie;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> wordsContainer,
          std::vector<std::string> wordsQuery,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = stringIndices(wordsContainer, wordsQuery);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"abcd", "bcd", "xbcd"}, {"cd", "bcd", "xyz"}, {1, 1, 1}, trials);
    test({"abcdefgh", "poiuygh", "ghghgh"}, {"gh", "acbfgh", "acbfegh"},
         {2, 0, 2}, trials);
    return 0;
}


