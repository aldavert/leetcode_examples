#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

double separateSquares(std::vector<std::vector<int> > squares)
{
    struct Square
    {
        double y0;
        double size;
        std::pair<double, double> partition(double thr) const
        {
            double above = 0.0, below = 0.0;
            if (thr >= y0 + size)
                below = size * size;
            else if (thr <= y0)
                above = size * size;
            else
            {
                below = (thr - y0) * size;
                above = (y0 + size - thr) * size;
            }
            return {above, below};
        }
    };
    std::vector<Square> info;
    double lowest = std::numeric_limits<double>::max(),
           highest = std::numeric_limits<double>::lowest();
    for (const auto &square : squares)
    {
        info.push_back({static_cast<double>(square[1]),
                        static_cast<double>(square[2])});
        lowest = std::min(lowest, info.back().y0);
        highest = std::max(highest, info.back().y0 + info.back().size);
    }
    while (std::abs(lowest - highest) > 1e-6)
    {
        double mid = (highest + lowest) / 2.0;
        double area_above = 0.0, area_below = 0.0;
        for (const auto &square : info)
        {
            auto [above, below] = square.partition(mid);
            area_above += above;
            area_below += below;
        }
        if (area_above > area_below) lowest = mid;
        else highest = mid;
    }
    return lowest;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > squares,
          double solution,
          unsigned int trials = 1)
{
    double result = -1.0;
    for (unsigned int i = 0; i < trials; ++i)
        result = separateSquares(squares);
    showResult(std::abs(solution - result) < 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0, 1}, {2, 2, 1}}, 1.00000, trials);
    test({{0, 0, 2}, {1, 1, 1}}, 1.16667, trials);
    test({{12, 25, 3}, {3, 14, 2}}, 25.83333, trials);
    return 0;
}


