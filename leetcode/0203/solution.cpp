#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

ListNode * removeElements(ListNode * head, int val)
{
    ListNode * new_head = head;
    while ((new_head != nullptr) && (new_head->val == val))
    {
        ListNode * aux = new_head;
        new_head = new_head->next;
        aux->next = nullptr;
        delete aux;
    }
    ListNode * current = (new_head != nullptr)?new_head->next:nullptr;
    ListNode * previous = new_head;
    while (current != nullptr)
    {
        if (current->val == val)
        {
            previous->next = current->next;
            current->next = nullptr;
            delete current;
            current = previous->next;
        }
        else
        {
            previous = current;
            current = current->next;
        }
    }
    return new_head;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> input,
          int val,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * linput = vec2list(input);
        linput = removeElements(linput, val);
        result = list2vec(linput);
        delete linput;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 6, 3, 4, 5, 6}, 6, {1, 2, 3, 4, 5}, trials);
    test({}, 1, {}, trials);
    test({7, 7, 7, 7}, 7, {}, trials);
    return 0;
}


