# Remove Linked List Elements

Given the `head` of a linked list and an integer `val`, remove all the nodes of the linked list that has `Node.val == val`, and return the *new head*.
 
#### Example 1:
> ```mermaid
> graph TD;
> subgraph SA [ ]
> direction LR;
> A1((1))-->B1((2))
> B1-->C1((6))
> C1-->D1((3))
> D1-->E1((4))
> E1-->F1((5))
> F1-->G1((6))
> end
> subgraph SB [ ]
> direction LR;
> A2((1))-->B2((2))
> B2-->D2((3))
> D2-->E2((4))
> E2-->F2((5))
> end
> SA-->SB
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#FAA,stroke:#000,stroke-width:2px;
> class C1,G1 selected;
> ```
> *Input:* `head = [1, 2, 6, 3, 4, 5, 6], val = 6`  
> *Output:* `[1, 2, 3, 4, 5]`

#### Example 2:
> *Input:* `head = [], val = 1`  
> *Output:* `[]`

#### Example 3:
> *Input:* `head = [7, 7, 7, 7], val = 7`  
> *Output:* `[]`
 
#### Constraints:
- The number of nodes in the list is in the range `[0, 10^4]`.
- `1 <= Node.val <= 50`
- `0 <= val <= 50`


