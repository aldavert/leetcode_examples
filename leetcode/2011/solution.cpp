#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int finalValueAfterOperations(std::vector<std::string> operations)
{
    int result = 0;
    for (const std::string &op : operations)
        result += 2 * ((op.front() == '+') || (op.back() == '+')) - 1;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> operations, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = finalValueAfterOperations(operations);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"--X", "X++", "X++"}, 1, trials);
    test({"++X", "++X", "X++"}, 3, trials);
    test({"X++", "++X", "--X", "X--"}, 0, trials);
    return 0;
}


