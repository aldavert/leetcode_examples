#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<std::string> subdomainVisits(std::vector<std::string> cpdomains)
{
    std::unordered_map<std::string, size_t> domain_histogram;
    for (std::string domain : cpdomains)
    {
        auto space = domain.find(' ');
        if (space == std::string::npos) continue;
        size_t number_of_visits = std::atoi(domain.substr(0, space).c_str());
        domain_histogram[domain.substr(++space)] += number_of_visits;
        while ((space = domain.find('.', space)) != std::string::npos)
            domain_histogram[domain.substr(++space)] += number_of_visits;
    }
    std::vector<std::string> result;
    for (auto [domain, visits] : domain_histogram)
        result.push_back(std::to_string(visits) + " " + domain);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<std::string> left_copy(left), right_copy(right);
    std::sort(left_copy.begin(), left_copy.end());
    std::sort(right_copy.begin(), right_copy.end());
    for (size_t i = 0, n = left.size(); i < n; ++i)
        if (left_copy[i] != right_copy[i])
            return false;
    return true;
}

void test(std::vector<std::string> cpdomains,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = subdomainVisits(cpdomains);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"9001 discuss.leetcode.com"},
         {"9001 leetcode.com", "9001 discuss.leetcode.com", "9001 com"}, trials);
    test({"900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"},
         {"901 mail.com", "50 yahoo.com", "900 google.mail.com", "5 wiki.org",
          "5 org", "1 intel.mail.com", "951 com"}, trials);
    return 0;
}


