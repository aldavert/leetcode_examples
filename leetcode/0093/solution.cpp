#include "../common/common.hpp"

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    const size_t n = left.size();
    std::vector<std::string> left_copy = left, right_copy = right;
    std::sort(left_copy.begin(), left_copy.end());
    std::sort(right_copy.begin(), right_copy.end());
    for (size_t i = 0; i < n; ++i)
        if (left_copy[i] != right_copy[i])
            return false;
    return true;
}

// ############################################################################
// ############################################################################

std::vector<std::string> restoreIpAddresses(std::string s)
{
    struct Address
    {
        std::string text = "";
        size_t number_of_digits = 0;
        bool update(std::string digit, bool last)
        {
            if ((number_of_digits < 3) || (last && (number_of_digits == 3)))
            {
                if (!text.empty()) [[likely]] text += ".";
                text += digit;
                ++number_of_digits;
                return true;
            }
            return false;
        }
    };
    auto isValid = [](std::string digit) -> bool
    {
        if (digit.empty()) return false;
        int value = std::stoi(digit);
        return (value >= 0) && (value <= 255) && (std::to_string(value) == digit);
    };
    std::vector<std::string> result;
    auto backtrace =
        [&](auto &&self, std::string digit, Address address, std::string input) -> void
    {
        if (input.empty())
        {
            if ((address.number_of_digits == 4) && (digit.empty()))
                result.push_back(address.text);
            return;
        }
        digit += input[0];
        if (!isValid(digit))
            return;
        input = input.substr(1);
        self(self, digit, address, input);
        if (address.update(digit, input.empty()))
            self(self, "", address, input);
    };
    backtrace(backtrace, "", {}, s);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = restoreIpAddresses(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("25525511135", {"255.255.11.135", "255.255.111.35"}, trials);
    test("0000", {"0.0.0.0"}, trials);
    test("101023", {"1.0.10.23", "1.0.102.3", "10.1.0.23", "10.10.2.3", "101.0.2.3"}, trials);
    return 0;
}


