#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countSymmetricIntegers(int low, int high)
{
    auto isSymmetric = [](int value) -> bool
    {
        if      (    value < 10) return false;
        else if (   value < 100) return (value / 10) == (value % 10);
        else if ( value < 1'000) return false;
        else if (value < 10'000)
        {
            int left = 0, right = 0;
            right += value % 10;
            value /= 10;
            right += value % 10;
            value /= 10;
            left += value % 10;
            value /= 10;
            left += value % 10;
            return left == right;
        }
        return false;
    };
    int result = 0;
    for (int number = low; number <= high; ++number)
        result += isSymmetric(number);
    return result;
}

// ############################################################################
// ############################################################################

void test(int low, int high, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSymmetricIntegers(low, high);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 100, 9, trials);
    test(1200, 1230, 4, trials);
    return 0;
}


