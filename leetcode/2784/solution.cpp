#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isGood(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int histogram[100] = {};
    for (int number : nums)
    {
        if (number >= n) return false;
        if (++histogram[number] > 1 + (number == n - 1)) return false;
    }
    return histogram[n - 1] == 2;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isGood(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3}, false, trials);
    test({1, 3, 3, 2}, true, trials);
    test({1, 1}, true, trials);
    test({3, 4, 4, 1, 2, 1}, false, trials);
    return 0;
}


