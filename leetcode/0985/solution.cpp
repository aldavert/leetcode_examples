#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> sumEvenAfterQueries(std::vector<int> nums,
                                     std::vector<std::vector<int>> queries)
{
    std::vector<int> result;
    result.reserve(nums.size());
    int sum_even = 0;
    for (int n : nums)
        if ((n & 1) == 0)
            sum_even += n;
    for (const auto &query : queries)
    {
        const int val_i = query[0], index_i = query[1];
        if ((nums[index_i] & 1) == 0)
            sum_even -= nums[index_i];
        nums[index_i] += val_i;
        if ((nums[index_i] & 1) == 0)
            sum_even += nums[index_i];
        result.push_back(sum_even);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumEvenAfterQueries(nums, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, {{1, 0}, {-3, 1}, {-4, 0}, {2, 3}}, {8, 6, 2, 4}, trials);
    test({1}, {{4, 0}}, {0}, trials);
    return 0;
}


