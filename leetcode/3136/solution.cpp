#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isValid(std::string word)
{
    auto isVowel = [](char c) -> bool
    {
        constexpr std::string_view VOWELS = "aeiouAEIOU";
        return VOWELS.find(c) != std::string_view::npos;
    };
    auto isConsonant = [&](char c) -> bool
    {
        return std::isalpha(c) && !isVowel(c);
    };
    return (word.length() >= 3)
        && std::all_of(word.begin(), word.end(), [](char c) { return std::isalnum(c); })
        && std::any_of(word.begin(), word.end(), isVowel)
        && std::any_of(word.begin(), word.end(), isConsonant);
}

// ############################################################################
// ############################################################################

void test(std::string word, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isValid(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("234Adas", true, trials);
    test("b3", false, trials);
    test("a3$e", false, trials);
    return 0;
}


