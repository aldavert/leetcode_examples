#include "../common/common.hpp"
#include <functional>
#include <semaphore.h>
#include <thread>

// ############################################################################
// ############################################################################

std::string output;
void printFizz(void) { output += "fizz "; }
void printBuzz(void) { output += "buzz "; }
void printFizzBuzz(void) { output += "fizzbuzz "; }
void printNumber(int value) { output += std::to_string(value) + ' '; }

class FizzBuzz
{
public:
    FizzBuzz(int _n) : n(_n)
    {
        sem_init(&fizzSemaphore    , 0, 0);
        sem_init(&buzzSemaphore    , 0, 0);
        sem_init(&fizzbuzzSemaphore, 0, 0);
        sem_init(&numberSemaphore  , 0, 1);
    }
    ~FizzBuzz(void)
    {
        sem_destroy(&fizzSemaphore);
        sem_destroy(&buzzSemaphore);
        sem_destroy(&fizzbuzzSemaphore);
        sem_destroy(&numberSemaphore);
    }
    void fizz(std::function<void(void)> printFizz)
    {
        for (int i = 1; i <= n; ++i)
        {
            if (i % 3 == 0 && i % 15 != 0)
            {
                sem_wait(&fizzSemaphore);
                printFizz();
                sem_post(&numberSemaphore);
            }
        }
    }
    void buzz(std::function<void(void)> printBuzz)
    {
        for (int i = 1; i <= n; ++i)
        {
            if (i % 5 == 0 && i % 15 != 0)
            {
                sem_wait(&buzzSemaphore);
                printBuzz();
                sem_post(&numberSemaphore);
            }
        }
    }
    void fizzbuzz(std::function<void(void)> printFizzBuzz)
    {
        for (int i = 1; i <= n; ++i)
        {
            if (i % 15 == 0)
            {
                sem_wait(&fizzbuzzSemaphore);
                printFizzBuzz();
                sem_post(&numberSemaphore);
            }
        }
    }
    void number(std::function<void(int)> printNumber)
    {
        for (int i = 1; i <= n; ++i)
        {
            sem_wait(&numberSemaphore);
            if (i % 15 == 0) sem_post(&fizzbuzzSemaphore);
            else if (i % 3 == 0) sem_post(&fizzSemaphore);
            else if (i % 5 == 0) sem_post(&buzzSemaphore);
            else
            {
                printNumber(i);
                sem_post(&numberSemaphore);
            }
        }
    }
private:
    int n;
    sem_t fizzSemaphore;
    sem_t buzzSemaphore;
    sem_t fizzbuzzSemaphore;
    sem_t numberSemaphore;
};

// ############################################################################
// ############################################################################

void test(int n, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        FizzBuzz obj(n);
        output.clear();
        std::thread t1([&]() { obj.fizz(printFizz); }),
                    t2([&]() { obj.buzz(printBuzz); }),
                    t3([&]() { obj.fizzbuzz(printFizzBuzz); }),
                    t4([&]() { obj.number(printNumber); });
        t1.join();
        t2.join();
        t3.join();
        t4.join();
        result = output;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(15, "1 2 fizz 4 buzz fizz 7 8 fizz buzz 11 fizz 13 14 fizzbuzz ", trials);
    test(5, "1 2 fizz 4 buzz ", trials);
    return 0;
}


