#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumArea(std::vector<std::vector<int> > grid)
{
    const int n = static_cast<int>(grid.size()),
              m = static_cast<int>(grid[0].size());
    int x1 = std::numeric_limits<int>::max(), y1 = std::numeric_limits<int>::max(),
        x2 = std::numeric_limits<int>::lowest(), y2 = std::numeric_limits<int>::lowest();
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            if (grid[i][j])
            {
                x1 = std::min(x1, i);
                y1 = std::min(y1, j);
                x2 = std::max(x2, i);
                y2 = std::max(y2, j);
            }
        }
    }
    return (x2 - x1 + 1) * (y2 - y1 + 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumArea(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 0}, {1, 0, 1}}, 6, trials);
    test({{1, 0}, {0, 0}}, 1, trials);
    return 0;
}


