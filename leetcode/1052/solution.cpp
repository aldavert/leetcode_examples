#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxSatisfied(std::vector<int> customers, std::vector<int> grumpy, int minutes)
{
    const int n = static_cast<int>(customers.size());
    int satisfied = 0, made_satisfied = 0;
    
    for (int i = 0, window_satisfied = 0; i < n; ++i)
    {
        if (grumpy[i] == 0) satisfied += customers[i];
        else window_satisfied += customers[i];
        if ((i >= minutes) && (grumpy[i - minutes] == 1))
            window_satisfied -= customers[i - minutes];
        made_satisfied = std::max(made_satisfied, window_satisfied);
    }
    return satisfied + made_satisfied;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> customers,
          std::vector<int> grumpy,
          int minutes,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSatisfied(customers, grumpy, minutes);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 1, 2, 1, 1, 7, 5}, {0, 1, 0, 1, 0, 1, 0, 1}, 3, 16, trials);
    test({1}, {0}, 1, 1, trials);
    return 0;
}


