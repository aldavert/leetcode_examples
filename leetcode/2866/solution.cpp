#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 1
long long maximumSumOfHeights(std::vector<int> maxHeights)
{
    const int n = static_cast<int>(maxHeights.size());
    std::vector<long long> left(n), right(n);
    left[0] = maxHeights[0];
    std::vector<int> mist(1);
    for (int i = 1; i < n; ++i)
    {
        while (!mist.empty() && (maxHeights[mist.back()] >= maxHeights[i]))
            mist.pop_back();
        if (mist.empty())
            left[i] = static_cast<long long>(maxHeights[i]) * (i + 1);
        else
            left[i] = static_cast<long long>(maxHeights[i]) * (i - mist.back())
                    + left[mist.back()];
        mist.push_back(i);
    }
    right[n - 1] = maxHeights[n - 1];
    mist.clear();
    mist.push_back(n - 1);
    for (int i = n - 2; i >= 0; --i)
    {
        while (!mist.empty() && (maxHeights[mist.back()] >= maxHeights[i]))
            mist.pop_back();
        if (mist.empty())
            right[i] = static_cast<long long>(maxHeights[i]) * (n - i);
        else
            right[i] = static_cast<long long>(maxHeights[i]) * (mist.back() - i)
                     + right[mist.back()];
        mist.push_back(i);
    }
    long long result = 0;
    for (int i = 0; i < n; ++i)
        result = std::max(result, left[i] + right[i] - maxHeights[i]);
    return result;
}
#else
long long maximumSumOfHeights(std::vector<int> maxHeights)
{
    const int n = static_cast<int>(maxHeights.size());
    std::vector<long> max_sum(n);
    std::stack<int> stack{{-1}};
    long summ = 0;
    auto process = [&](int i) -> void
    {
        while ((stack.size() > 1) && (maxHeights[stack.top()] > maxHeights[i]))
        {
            int j = stack.top();
            stack.pop();
            summ -= std::abs(j - stack.top()) * static_cast<long>(maxHeights[j]);
        }
        summ += std::abs(i - stack.top()) * static_cast<long>(maxHeights[i]);
        stack.push(i);
    };
    
    for (int i = 0; i < n; ++i)
    {
        process(i);
        max_sum[i] = summ;
    }
    stack = std::stack<int>{{n}};
    summ = 0;
    for (int i = n - 1; i >= 0; --i)
    {
        process(i);
        max_sum[i] += summ - maxHeights[i];
    }
    return *std::max_element(max_sum.begin(), max_sum.end());
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> maxHeights, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumSumOfHeights(maxHeights);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 3, 4, 1, 1}, 13, trials);
    test({6, 5, 3, 9, 2, 7}, 22, trials);
    test({3, 2, 5, 5, 2, 3}, 18, trials);
    return 0;
}


