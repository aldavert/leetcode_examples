#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> grayCode(int n)
{
    int N = (2 << n) - 1;
    std::vector<int> result;
    int size = 1;
    result.reserve(N);
    result.push_back(0);
    for (int i = 0; i < n; ++i)
    {
        const int mask = 1 << i;
        for (int j = size - 1; j >= 0; --j, ++size)
            result.push_back(result[j] | mask);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = grayCode(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {0, 1, 3, 2}, trials);
    test(1, {0, 1}, trials);
    return 0;
}


