#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkIfPangram(std::string sentence)
{
    size_t different = 0;
    if (sentence.size() >= 26)
    {
        std::vector<bool> enabled(26, false);
        for (char c : sentence)
        {
            c -= 'a';
            different += !enabled[c];
            enabled[c] = true;
        }
    }
    return different == 26;
}

// ############################################################################
// ############################################################################

void test(std::string sentence, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkIfPangram(sentence);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("thequickbrownfoxjumpsoverthelazydog", true, trials);
    test("leetcode", false, trials);
    return 0;
}


