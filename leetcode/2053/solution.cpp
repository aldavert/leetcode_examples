#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::string kthDistinct(std::vector<std::string> arr, int k)
{
    std::unordered_map<std::string, int> histogram;
    for (std::string str : arr)
        ++histogram[str];
    for (int count = 0; std::string str : arr)
    {
        count += histogram.find(str)->second == 1;
        if (count == k)
            return str;
    }
    return "";
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> arr,
          int k,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = kthDistinct(arr, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"d", "b", "c", "b", "c", "a"}, 2, "a", trials);
    test({"aaa", "aa", "a"}, 1, "aaa", trials);
    test({"a", "b", "a"}, 3, "", trials);
    return 0;
}


