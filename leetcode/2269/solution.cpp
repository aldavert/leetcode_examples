#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int divisorSubstrings(int num, int k)
{
    long exponent = 1;
    for (int i = 0; i < k; ++i) exponent *= 10;
    long value = num % exponent;
    long current = num / exponent;
    int result = (value > 0)?num % value == 0:0;
    exponent /= 10;
    while (current)
    {
        long digit = current % 10;
        current /= 10;
        value = value / 10 + digit * exponent;
        result += (value > 0)?num % value == 0:0;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int num, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = divisorSubstrings(num, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(240, 2, 2, trials);
    test(430043, 2, 2, trials);
    test(10, 1, 1, trials);
    test(10, 2, 1, trials);
    test(1000000000, 10, 1, trials);
    return 0;
}


