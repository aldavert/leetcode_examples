#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfGoodSubarraySplits(std::vector<int> nums)
{
    if (std::count(nums.begin(), nums.end(), 1) == 0)
        return 0;
    
    constexpr int MOD = 1'000'000'007;
    int result = 1;
    for (int i = 0, n = static_cast<int>(nums.size()), prev = -1; i < n; ++i)
    {
        if (nums[i] == 1)
        {
            if (prev != -1)
                result = static_cast<int>(result * static_cast<long>(i - prev) % MOD);
            prev = i;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfGoodSubarraySplits(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 0, 0, 1}, 3, trials);
    test({0, 1, 0}, 1, trials);
    return 0;
}


