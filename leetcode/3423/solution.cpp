#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxAdjacentDistance(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = 0;
    for (int i = 0, previous = nums.back(); i < n; ++i)
    {
        result = std::max(result, std::abs(nums[i] - previous));
        previous = nums[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxAdjacentDistance(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 4}, 3, trials);
    test({-5, -10, -5}, 5, trials);
    return 0;
}


