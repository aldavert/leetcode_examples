#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <stack>

// ############################################################################
// ############################################################################

bool leafSimilar(TreeNode * root1, TreeNode * root2)
{
    std::stack<TreeNode *> s1, s2;
    auto insert = [](std::stack<TreeNode *> &s, TreeNode * ptr)
    {
        for (TreeNode * next = ptr; next; )
        {
            if (next->right && next->left)
            {
                s.push(next);
                next = next->left;
            }
            else if (next->right) next = next->right;
            else if (next->left) next = next->left;
            else { s.push(next); next = nullptr; }
        }
    };
    auto next = [](std::stack<TreeNode *> &s) -> TreeNode *
    {
        s.pop();
        if (s.empty()) return nullptr;
        TreeNode * next_node = s.top()->right;
        s.pop();
        return next_node;
    };
    insert(s1, root1);
    insert(s2, root2);
    while (!s1.empty() && !s2.empty())
    {
        if (s1.top()->val != s2.top()->val) return false;
        insert(s1, next(s1));
        insert(s2, next(s2));
    }
    return s1.empty() == s2.empty();
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree1,
          std::vector<int> tree2,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root1 = vec2tree(tree1);
        TreeNode * root2 = vec2tree(tree2);
        result = leafSimilar(root1, root2);
        delete root1;
        delete root2;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 5, 1, 6, 2, 9, 8, null, null, 7, 4},
         {3, 5, 1, 6, 7, 4, 2, null, null, null, null, null, null, 9, 8},
         true, trials);
    test({1, 2, 3}, {1, 3, 2}, false, trials);
    test({3, 5, 1, 6, 7, 4, 2, null, null, null, null, null, null, 9, 11, null,
          null, 8, 10}, {3, 5, 1, 6, 2, 9, 8, null, null, 7, 4}, false, trials);
    return 0;
}


