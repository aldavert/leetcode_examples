#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

class CBTInserter
{
    std::vector<TreeNode *> m_tree;
public:
    CBTInserter(TreeNode * root)
    {
        m_tree.push_back(root);
        for (size_t i = 0; i < m_tree.size(); ++i)
        {
            TreeNode * node = m_tree[i];
            if (node->left)
                m_tree.push_back(node->left);
            if (node->right)
                m_tree.push_back(node->right);
        }
    }
    int insert(int val)
    {
        const int n = static_cast<int>(m_tree.size());
        m_tree.push_back(new TreeNode(val));
        auto &parent = m_tree[(n - 1) / 2];
        if (n % 2 == 1) parent->left = m_tree.back();
        else parent->right = m_tree.back();
        return parent->val;
    }
    TreeNode * get_root(void)
    {
        return m_tree[0];
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          std::vector<bool> insert,
          std::vector<int> value,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        TreeNode * root = vec2tree(tree);
        CBTInserter object(root);
        for (size_t i = 0; i < insert.size(); ++i)
        {
            if (insert[i]) result.push_back({object.insert(value[i])});
            else result.push_back(tree2vec(object.get_root()));
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2}, {true, true, false}, {3, 4, null}, {{1}, {2}, {1, 2, 3, 4}}, trials);
    return 0;
}


