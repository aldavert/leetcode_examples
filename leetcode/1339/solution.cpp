#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int maxProduct(TreeNode * root)
{
    struct FlatNode
    {
        int left;
        int right;
        int val;
    };
    
    FlatNode flat[50'001];
    int index = -1;
    auto localSum = [&](auto &&self, TreeNode * node) -> int
    {
        if (node != nullptr)
        {
            int current = ++index;
            int &sum = flat[current].val = node->val;
            if (node->left != nullptr)
            {
                flat[current].left = index + 1;
                sum += self(self, node->left);
            }
            else flat[current].left = -1;
            if (node->right != nullptr)
            {
                flat[current].right = index + 1;
                sum += self(self, node->right);
            }
            else flat[current].right = -1;
            return sum;
        }
        else return 0;
    };
    auto maxProduct = [&](auto &&self, TreeNode * node, int idx, long sum) -> long
    {
        if (node == nullptr) return 0;
        else
        {
            long sum_left  = (flat[idx].left  != -1)?(flat[flat[idx].left ].val):0;
            long sum_right = (flat[idx].right != -1)?(flat[flat[idx].right].val):0;
            sum += node->val;
            long product = std::max((sum + sum_left ) * sum_right,
                                    (sum + sum_right) * sum_left );
            product = std::max(product,
                               self(self, node->left, flat[idx].left, sum + sum_right));
            product = std::max(product,
                               self(self, node->right, flat[idx].right, sum + sum_left));
            return product;
        }
    };
    localSum(localSum, root);
    return static_cast<int>(maxProduct(maxProduct, root, 0, 0) % 1'000'000'007L);
}

// ############################################################################
// ############################################################################

void test(const std::vector<int> &tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = maxProduct(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6}, 110, trials);
    test({1, null, 2, 3, 4, null, null, 5, 6}, 90, trials);
    test({2, 3, 9, 10, 7, 8, 6, 5, 4, 11, 1}, 1025, trials);
    test({1, 1}, 1, trials);
    test({5, 6, 6, null, null, 8, 6, 10, null, 5}, 504, trials);
    return 0;
}


