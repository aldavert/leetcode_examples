# Maximum Product of Splitted Binary Tree

Given the `root` of a binary tree, split the binary tree into two subtrees by removing one edge such that the product of the sums of the subtrees is maximized.

Return *the maximum product of the sums of the two subtrees*. Since the answer may be too large, return it **modulo** `10^9 + 7`.

**Note** that you need to maximize the answer before taking the mod and not after taking it.

#### Example 1:
> ```mermaid
> flowchart LR;
> subgraph S1 [ ]
> direction TB;
> A((1))---B((2))
> A---C((3))
> B---D((4))
> B---E((5))
> C---F((6))
> C---EMPTY1(( ))
> end
> subgraph S2 [sum = 11]
> direction TB;
> G((2))---H((4))
> G---I((5))
> end
> subgraph S3 [sum = 10]
> direction TB;
> J((1))---EMPTY2(( ))
> J---K((3))
> K---L((6))
> K---EMPTY3(( ))
> end
> S1 --> S2
> S2 --- S3
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY1,EMPTY2,EMPTY3,S1,S2,S3 empty;
> linkStyle 0 stroke-width:2pt,fill:none,stroke:red;
> linkStyle 5,8,11,13 stroke-width:0pt;
> ```
> *Input:* `root = [1, 2, 3, 4, 5, 6]`  
> *Output:* `110`  
> *Explanation:* Remove the red edge and get `2` binary trees with sum `11` and `10`. Their product is `110` (`11 * 10`)

#### Example 2:
> ```mermaid
> flowchart LR;
> subgraph S1 [ ]
> direction TB;
> A((1))---EMPTY1(( ))
> A---B((2))
> B---C((3))
> B---D((4))
> D---E((5))
> D---F((6))
> end
> subgraph S2 [ sum = 6 ]
> G((1))---EMPTY2(( ))
> G---H((2))
> H---I((3))
> H---EMPTY3(( ))
> end
> subgraph S3 [ sum = 15 ]
> J((4))---L((5))
> J---M((6))
> end
> S1-->S2
> S2---S3
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY1,EMPTY2,EMPTY3,S1,S2,S3 empty;
> linkStyle 3 stroke-width:2pt,fill:none,stroke:red;
> linkStyle 0,6,9,13 stroke-width:0pt;
> ```
> *Input:* `root = [1, null, 2, 3, 4, null, null, 5, 6]`  
> *Output:* `90`  
> *Explanation:* Remove the red edge and get `2` binary trees with sum `15` and `6`. Their product is `90` (`15 * 6`).

#### Example 3:
> *Input:* `root = [2, 3, 9, 10, 7, 8, 6, 5, 4, 11, 1]`  
> *Output:* `1025`

#### Example 4:
> *Input:* `root = [1, 1]`  
> *Output:* `1`

#### Constraints:
- The number of nodes in the tree is in the range `[2, 5 * 10^4]`.
- `1 <= Node.val <= 10^4`


