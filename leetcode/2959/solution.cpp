#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfSets(int n, int maxDistance, std::vector<std::vector<int> > roads)
{
    std::vector<std::vector<int> > dist(n, std::vector<int>(n));
    auto floydWarshall = [&](int mask) -> int
    {
        int max_distance = 0;
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                dist[i][j] = maxDistance + 1;
        for (int i = 0; i < n; ++i)
            if (mask >> i & 1)
                dist[i][i] = 0;
        for (const auto &road : roads)
        {
            const int u = road[0];
            const int v = road[1];
            const int w = road[2];
            if (((mask >> u) & 1) && ((mask >> v) & 1))
            {
                dist[u][v] = std::min(dist[u][v], w);
                dist[v][u] = std::min(dist[v][u], w);
            }
        }
        for (int k = 0; k < n; ++k)
        {
            if (((mask >> k) & 1) == 0) continue;
            for (int i = 0; i < n; ++i)
            {
                if (((mask >> i) & 1) == 0) continue;
                for (int j = 0; j < n; ++j)
                    if ((mask >> j) & 1)
                        dist[i][j] = std::min(dist[i][j], dist[i][k] + dist[k][j]);
            }
        }
        for (int i = 0; i < n; ++i)
            if (mask >> i & 1)
                for (int j = i + 1; j < n; ++j)
                    if (mask >> j & 1)
                        max_distance = std::max(max_distance, dist[i][j]);
        return max_distance;
    };
    const int max_mask = 1 << n;
    int result = 0;
    for (int mask = 0; mask < max_mask; ++mask)
        result += (floydWarshall(mask) <= maxDistance);
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          int maxDistance,
          std::vector<std::vector<int> > roads,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfSets(n, maxDistance, roads);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 5, {{0, 1, 2}, {1, 2, 10}, {0, 2, 10}}, 5, trials);
    test(3, 5, {{0, 1, 20}, {0, 1, 10}, {1, 2, 2}, {0, 2, 2}}, 7, trials);
    return 0;
}



