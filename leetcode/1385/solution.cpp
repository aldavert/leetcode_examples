#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findTheDistanceValue(std::vector<int> arr1, std::vector<int> arr2, int d)
{
    std::sort(arr2.begin(), arr2.end());
    int result = 0;
    for (int value : arr1)
    {
        int min_distance = 0;
        auto search = std::lower_bound(arr2.begin(), arr2.end(), value);
        if (search == arr2.end())
            min_distance = std::abs(arr2.back() - value);
        else if (search == arr2.begin())
            min_distance = std::abs(arr2.front() - value);
        else min_distance = std::min(std::abs(*search - value),
                                     std::abs(*std::prev(search) - value));
        if (min_distance > d) ++result;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr1,
          std::vector<int> arr2,
          int d,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findTheDistanceValue(arr1, arr2, d);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 5, 8}, {10, 9, 1, 8}, 2, 2, trials);
    test({1, 4, 2, 3}, {-4, -3, 6, 10, 20, 30}, 3, 2, trials);
    test({2, 1, 100, 3}, {-5, -2, 10, -3, 7}, 6, 1, trials);
    return 0;
}

