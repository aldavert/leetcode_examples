#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minTimeToType(std::string word)
{
    int result = 0;
    for (char previous = 'a'; char letter : word)
    {
        const int distance = std::abs(letter - previous);
        result += std::min(distance, 26 - distance) + 1;
        previous = letter;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minTimeToType(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", 5, trials);
    test("bza", 7, trials);
    test("zjpc", 34, trials);
    return 0;
}


