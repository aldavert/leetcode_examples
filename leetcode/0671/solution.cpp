#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int findSecondMinimumValue(TreeNode* root)
{
    auto traverse = [&](auto &&self, TreeNode * node, int minimum) -> int
    {
        if (!node) return -1;
        if (node->val > minimum) return node->val;
        int left = self(self, node->left, minimum);
        int right = self(self, node->right, minimum);
        if ((left == -1) || (right == -1)) return std::max(left, right);
        else return std::min(left, right);
    };
    return traverse(traverse, root, root->val);
}

// ############################################################################
// ############################################################################

void test(const std::vector<int> &tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = findSecondMinimumValue(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 2, 5, null, null, 5, 7}, 5, trials);
    test({2, 2, 2}, -1, trials);
    test({1, 1, 3, 1, 1, 3, 4, 3, 1, 1, 1, 3, 8, 4, 8, 3, 3, 1, 6, 2, 1}, 2, trials);
    return 0;
}


