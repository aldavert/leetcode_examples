#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minIncrements(int n, std::vector<int> cost)
{
    int result = 0;
    for (int i = n / 2 - 1; i >= 0; --i)
    {
        const int l = i * 2 + 1, r = i * 2 + 2;
        result += std::abs(cost[l] - cost[r]);
        cost[i] += std::max(cost[l], cost[r]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<int> cost, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minIncrements(n, cost);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, {1, 5, 2, 2, 3, 3, 1}, 6, trials);
    test(3, {5, 3, 3}, 0, trials);
    return 0;
}


