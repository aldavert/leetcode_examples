#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int countPairs(std::vector<int> nums, int low, int high)
{
    constexpr int N = 1 << 15;
    int f[N + 1] = {}, g[N + 1] = {};
    auto lowbit = [](int i) -> int { return i & (-i); };
    auto xorpairs = [&](int y, int x) -> int
    {
        int sum = 0;
        while (y)
        {
            int z = y & (y - 1), lb = y - z;
            x &= ~(lb - 1);
            sum += x & lb ? g[x ^ z] : f[x ^ y];
            y = z;
        }
        return sum;
    };
    int result = 0;
    for (auto x : nums)
    {
        result += xorpairs(high + 1, x) - xorpairs(low, x); 
        for (int i = x; i > 0; i &= i - 1) ++g[i];
        for (++x; x <= N; x += lowbit(x)) ++f[x];
    }
    return result;
}
#else
int countPairs(std::vector<int> nums, int low, int high)
{
    struct Trie
    {
    public:
        Trie(void) : m_children(2), m_count(0) {}
        ~Trie(void) { for (auto &child : m_children) delete child; } 
        void insert(int x)
        {
            Trie * node = this;
            for (int i = 15; ~i; --i)
            {
                int v = x >> i & 1;
                if (!node->m_children[v])
                    node->m_children[v] = new Trie();
                node = node->m_children[v];
                ++node->m_count;
            }
        }
        int search(int x, int limit)
        {
            Trie * node = this;
            int result = 0;
            for (int i = 15; ~i && node; --i)
            {
                int v = x >> i & 1;
                if (limit >> i & 1)
                {
                    if (node->m_children[v])
                        result += node->m_children[v]->m_count;
                    node = node->m_children[v ^ 1];
                }
                else node = node->m_children[v];
            }
            return result;
        }
    private:
        std::vector<Trie *> m_children;
        int m_count;
    };
    
    Trie trie;
    int result = 0;
    for (int x : nums)
    {
        result += trie.search(x, high + 1) - trie.search(x, low);
        trie.insert(x);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int low,
          int high,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPairs(nums, low, high);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 2, 7}, 2, 6, 6, trials);
    test({9, 8, 4, 2, 1}, 5, 14, 8, trials);
    return 0;
}


