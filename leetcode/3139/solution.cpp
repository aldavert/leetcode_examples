#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int minCostToEqualizeArray(std::vector<int> nums, int cost1, int cost2)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(nums.size());
    const int min_num = *std::min_element(nums.begin(), nums.end());
    const int max_num = *std::max_element(nums.begin(), nums.end());
    const long sum = std::accumulate(nums.begin(), nums.end(), 0L);
    long result = std::numeric_limits<long>::max();
    
    if ((cost1 * 2 <= cost2) || (n < 3))
        return static_cast<int>((cost1 * (static_cast<long>(max_num) * n - sum)) % MOD);
    for (int target = max_num; target < 2 * max_num; ++target)
    {
        const long total_gap = static_cast<long>(target) * n - sum;
        const long pairs = std::min(total_gap / 2, total_gap - (target - min_num));
        result = std::min(result, cost1 * (total_gap - 2 * pairs) + cost2 * pairs);
    }
    return static_cast<int>(result % MOD);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int cost1,
          int cost2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCostToEqualizeArray(nums, cost1, cost2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 1}, 5, 2, 15, trials);
    test({2, 3, 3, 3, 5}, 2, 1, 6, trials);
    return 0;
}


