#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

bool isValid(std::string s)
{
    std::stack<char> openings;
    for (char c : s)
    {
        if ((c == '(') || (c == '[') || (c == '{'))
            openings.push(c);
        else
        {
            if (openings.size() == 0) return false;
            if (((c == ')') && (openings.top() != '('))
             || ((c == '}') && (openings.top() != '{'))
             || ((c == ']') && (openings.top() != '[')))
                return false;
            openings.pop();
        }
    }
    return openings.empty();
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isValid(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("()", true, trials);
    test("()[]{}", true, trials);
    test("(]", false, trials);
    test("([)]", false, trials);
    test("{[]}", true, trials);
    return 0;
}


