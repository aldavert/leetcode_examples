#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int nearestValidPoint(int x, int y, std::vector<std::vector<int> > points)
{
    int distance = std::numeric_limits<int>::max();
    int index = -1;
    for (int i = 0, n = static_cast<int>(points.size()); i < n; ++i)
    {
        if ((points[i][0] == x) || (points[i][1] == y))
        {
            int current = std::abs(x - points[i][0]) + std::abs(y - points[i][1]);
            if (current < distance)
            {
                distance = current;
                index = i;
            }
        }
    }
    return index;
}

// ############################################################################
// ############################################################################

void test(int x,
          int y,
          std::vector<std::vector<int> > points,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = nearestValidPoint(x, y, points);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 4, {{1, 2}, {3, 1}, {2, 4}, {2, 3}, {4, 4}}, 2, trials);
    test(3, 4, {{3, 4}}, 0, trials);
    test(3, 4, {{2, 3}}, -1, trials);
    return 0;
}


