#include "../common/common.hpp"
#include <memory>

// ############################################################################
// ############################################################################

#if 1
int lengthOfLIS(std::vector<int> nums, int k)
{
    int max_lenght = 0, f[100'001] = {}, a[100'001] = {};
    auto renew = [&](int pos, int x) -> void
    {
        if (x > a[pos]) a[pos] = x;
        else return;
        for (int i = pos; i <= 100'000; i += (i & (-i)))
        {
            if (x > f[i]) f[i] = x; 
            else return;
        }
    };
    auto segmentSearch = [&](int l, int r) -> int
    {
        int max = 0;
        while (r >= l)
        {
            for (; r - (r & (-r)) >= l; r -= r&(-r))
            {
                if (f[r] > max)
                max = f[r];
            }
            if (r >= l)
            {
                if (a[r] > max) max = a[r];
                --r;
            }
        }
        return max;
    };
    for (int x : nums)
    {
        auto len = segmentSearch(std::max(x - k, 1), x - 1);
        if (++len > max_lenght) max_lenght = len;
        renew(x, len);
    }
    return max_lenght;
}
#else
int lengthOfLIS(std::vector<int> nums, int k)
{
    class SegmentTree
    {
        struct SegmentTreeNode;
        using NODE = std::unique_ptr<SegmentTreeNode>;
        struct SegmentTreeNode
        {
            int lo;
            int hi;
            int max_length;
            NODE left;
            NODE right;
            SegmentTreeNode(int pl, int ph, int ml, NODE l = nullptr, NODE r = nullptr) :
                lo(pl), hi(ph), max_length(ml), left(std::move(l)), right(std::move(r))
            {}
        };
    public:
        SegmentTree(void) :
            root(std::make_unique<SegmentTreeNode>(0, 100'001, 0)) {}
        inline void updateRange(int i, int j, int max_length)
        {
            update(root, i, j, max_length);
        }
        inline int queryRange(int i, int j)
        {
            return query(root, i, j);
        }
    private:
        NODE root;
        
        NODE makeNode(int pl, int ph, int ml) const
        {
            return std::make_unique<SegmentTreeNode>(pl, ph, ml);
        }
        void update(NODE &node, int i, int j, int max_length)
        {
            if (node->lo == i && node->hi == j)
            {
                node->max_length = max_length;
                node->left = nullptr;
                node->right = nullptr;
                return;
            }
            const int mid = node->lo + (node->hi - node->lo) / 2;
            if (node->left == nullptr)
            {
                node->left  = makeNode(node->lo,      mid, node->max_length);
                node->right = makeNode(mid + 1 , node->hi, node->max_length);
            }
            if      (j <= mid) update(node->left , i, j, max_length);
            else if (i >  mid) update(node->right, i, j, max_length);
            else
            {
                update(node->left ,       i, mid, max_length);
                update(node->right, mid + 1,   j, max_length);
            }
            node->max_length = std::max(node->left->max_length, node->right->max_length);
        }
        int query(NODE &node, int i, int j)
        {
            if (node->left == nullptr) return node->max_length;
            if ((node->lo == i) && (node->hi == j))
                return node->max_length;
            int mid = node->lo + (node->hi - node->lo) / 2;
            if (j <= mid) return query(node->left, i, j);
            if (i >  mid) return query(node->right, i, j);
            return std::max(query(node->left, i, mid), query(node->right, mid + 1, j));
        }
    };
    int result = 1;
    for (SegmentTree tree; int num : nums)
    {
        int left = std::max(1, num - k), right = num - 1,
            max_length = tree.queryRange(left, right) + 1;
        result = std::max(result, max_length);
        tree.updateRange(num, num, max_length);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = lengthOfLIS(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 1, 4, 3, 4, 5, 8, 15}, 3, 5, trials);
    test({7, 4, 5, 1, 8, 12, 4, 7}, 5, 4, trials);
    test({1, 5}, 1, 1, trials);
    return 0;
}


