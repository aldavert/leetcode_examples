#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string solveEquation(std::string equation)
{
    int variable[2] = {}, coefficient[2] = {}, value = 0, sign = 1, position = 0;
    bool digit = false;
    for (char symbol : equation)
    {
        if (symbol == '=')
        {
            coefficient[position] += value * sign;
            value = 0;
            digit = false;
            position = 1;
            sign = 1;
        }
        else if (symbol == '+')
        {
            coefficient[position] += value * sign;
            value = 0;
            digit = false;
            sign = 1;
        }
        else if (symbol == '-')
        {
            coefficient[position] += value * sign;
            value = 0;
            digit = false;
            sign = -1;
        }
        else if (symbol == 'x')
        {
            variable[position] += sign * (value + !digit);
            digit = false;
            value = 0;
        }
        else
        {
            value = 10 * value + static_cast<int>(symbol - '0');
            digit = true;
        }
    }
    coefficient[position] += value * sign;
    int numerator = coefficient[1] - coefficient[0],
        denominator = variable[0] - variable[1];
    if (denominator == 0)
    {
        if (numerator == 0) return "Infinite solutions";
        else return "No solution";
    }
    return "x=" + std::to_string(numerator / denominator);
}

// ############################################################################
// ############################################################################

void test(std::string equation, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = solveEquation(equation);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("x+5-3+x=6+x-2", "x=2", trials);
    test("x=x", "Infinite solutions", trials);
    test("2x=x", "x=0", trials);
    test("x-x=1", "No solution", trials);
    test("-x=-1", "x=1", trials);
    test("-x=1", "x=-1", trials);
    test("0x=0", "Infinite solutions", trials);
    return 0;
}


