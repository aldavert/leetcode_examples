#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

bool canFormArray(std::vector<int> arr, std::vector<std::vector<int> > pieces)
{
    std::unordered_map<int, size_t> lut;
    for (size_t i = 0, n = pieces.size(); i < n; ++i)
        lut[pieces[i][0]] = i;
    for (size_t i = 0, n = arr.size(); i < n;)
    {
        auto search = lut.find(arr[i]);
        if (search == lut.end()) return false;
        const std::vector<int> &selected = pieces[search->second];
        const size_t m = selected.size();
        if (m + i > n) return false;
        for (size_t j = 0; j < m; ++j, ++i)
            if (arr[i] != selected[j]) return false;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr,
          std::vector<std::vector<int> > pieces,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canFormArray(arr, pieces);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({15, 88}, {{88}, {15}}, true, trials);
    test({49, 18, 16}, {{16, 18, 49}}, false, trials);
    test({91, 4, 64, 78}, {{78}, {4, 64}, {91}}, true, trials);
    return 0;
}


