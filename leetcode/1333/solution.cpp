#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> filterRestaurants(std::vector<std::vector<int> > restaurants,
                                   int veganFriendly,
                                   int maxPrice,
                                   int maxDistance)
{
    struct Selected
    {
        int id = 0;
        int rating = 0;
        bool operator<(const Selected &other) const
        {
            return (rating == other.rating)?id > other.id:rating > other.rating;
        }
    };
    std::vector<Selected> filtered;
    for (auto &r : restaurants)
        if ((r[2] >= veganFriendly)
        &&  (r[3] <= maxPrice)
        &&  (r[4] <= maxDistance))
            filtered.push_back({r[0], r[1]});
    std::sort(filtered.begin(), filtered.end());
    std::vector<int> result;
    for (const auto &f : filtered)
        result.push_back(f.id);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > restaurants,
          int veganFriendly,
          int maxPrice,
          int maxDistance,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = filterRestaurants(restaurants, veganFriendly, maxPrice, maxDistance);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{ 1,  4,  1, 40, 10},
          { 2,  8,  0, 50,  5},
          { 3,  8,  1, 30,  4},
          { 4, 10,  0, 10,  3},
          { 5,  1,  1, 15,  1}}, 1, 50, 10, {3, 1, 5}, trials);
    test({{ 1,  4,  1, 40, 10},
          { 2,  8,  0, 50,  5},
          { 3,  8,  1, 30,  4},
          { 4, 10,  0, 10,  3},
          { 5,  1,  1, 15,  1}}, 0, 50, 10, {4, 3, 2, 1, 5}, trials);
    test({{ 1,  4,  1, 40, 10},
          { 2,  8,  0, 50,  5},
          { 3,  8,  1, 30,  4},
          { 4, 10,  0, 10,  3},
          { 5,  1,  1, 15,  1}}, 0, 30, 3, {4, 5}, trials);
    return 0;
}


