#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumEnergy(std::vector<int> energy, int k)
{
    int result = std::numeric_limits<int>::lowest();
    for (int i = 0, n = static_cast<int>(energy.size()); i < k; ++i)
    {
        int sum = 0;
        for (int j = n - 1 - i; j >= 0; j -= k)
        {
            sum += energy[j];
            result = std::max(result, sum);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> energy, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumEnergy(energy, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 2, -10, -5, 1}, 3, 3, trials);
    test({-2, -3, -1}, 2, -1, trials);
    return 0;
}


