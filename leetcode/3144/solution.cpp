#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumSubstringsInPartition(std::string s)
{
    auto isBalanced = [](const int * count) -> bool
    {
        int min_freq = 1001, max_freq = 0;
        for (int i = 0; i < 26; ++i)
        {
            if (count[i] > 0)
            {
                min_freq = std::min(min_freq, count[i]);
                max_freq = std::max(max_freq, count[i]);
            }
        }
        return min_freq == max_freq;
    };
    const int n = static_cast<int>(s.size());
    std::vector<int> dp(n, n);
    for (int i = 0; i < n; ++i)
    {
        int count[26] = {};
        for (int j = i; j >= 0; --j)
        {
            ++count[s[j] - 'a'];
            if (isBalanced(count))
                dp[i] = (j > 0)?std::min(dp[i], 1 + dp[j - 1]):1;
        }
    }
    return dp.back();
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumSubstringsInPartition(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("fabccddg", 3, trials);
    test("abababaccddb", 2, trials);
    return 0;
}


