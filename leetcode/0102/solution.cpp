#include "../common/common.hpp"
#include "../common/tree.hpp"

// ################################################################################################
// ################################################################################################

std::vector<std::vector<int> > levelOrder(TreeNode * root)
{
    if (root == nullptr) return {};
    std::queue<std::pair<TreeNode *, int> > queue;
    queue.push({root, 0});
    int current_level = 0;
    std::vector<std::vector<int> > result;
    std::vector<int> partial_result;
    while (!queue.empty())
    {
        auto current = queue.front();
        queue.pop();
        if (current_level == current.second)
            partial_result.push_back(current.first->val);
        else
        {
            result.emplace_back(std::move(partial_result));
            partial_result = {current.first->val};
            current_level = current.second;
        }
        if (current.first->left != nullptr)
            queue.push({current.first->left, current.second + 1});
        if (current.first->right != nullptr)
            queue.push({current.first->right, current.second + 1});
    }
    result.emplace_back(std::move(partial_result));
    return result;
}

// ################################################################################################
// ################################################################################################

void test(const std::vector<int> &tree, const std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = levelOrder(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 9, 20, null, null, 15, 7}, {{3}, {9, 20}, {15, 7}}, trials);
    test({1}, {{1}}, trials);
    test({}, {}, trials);
    return 0;
}


