#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int equalSubstring(std::string s, std::string t, int maxCost)
{
    const int n = static_cast<int>(s.size());
    int j = 0;
    for (int i = 0; i < n; ++i)
    {
        maxCost -= abs(s[i] - t[i]);
        if (maxCost < 0)
            maxCost += std::abs(s[j] - t[j]), ++j;
    }
    return n - j;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::string t,
          int maxCost,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = equalSubstring(s, t, maxCost);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcd", "bcdf", 3, 3, trials);
    test("abcd", "cdef", 3, 1, trials);
    test("abcd", "acde", 0, 1, trials);
    return 0;
}


