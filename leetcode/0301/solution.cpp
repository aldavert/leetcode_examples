#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<std::string> removeInvalidParentheses(std::string s)
{
    std::vector<std::string> result;
    
    auto isValidString = [](std::string str) -> bool
    {
        const int n = static_cast<int>(str.size());
        int cnt = 0;
        for (int i = 0; i < n; ++i)
        {
            if (str[i] == '(') ++cnt;
            else if (str[i] == ')') --cnt;
            if (cnt < 0) return false;
        }
        return (cnt == 0);
    };
    
    std::unordered_set<std::string> visit;
    std::queue<std::string> q;
    q.push(s);
    visit.insert(s);
    for (bool level = false; !q.empty(); q.pop())
    {
        s = q.front();
        if (isValidString(s))
        {
            result.push_back(s);
            level = true;
        }
        if (level) continue;
        const int n = static_cast<int>(s.size());
        for (int i = 0; i < n; ++i)
        {
            if ((s[i] == '(') || (s[i] == ')'))
            {
                if (auto t = s.substr(0, i) + s.substr(i + 1); visit.find(t) == visit.end())
                {
                    q.push(t);
                    visit.insert(t);
                }
            }
        }
    }
    if (result.size() == 0) return {""};
    return result;
}
 
// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_set<std::string> lut(left.begin(), left.end());
    for (const auto &s : right)
    {
        if (auto search = lut.find(s); search != lut.end())
            lut.erase(search);
        else return false;
    }
    return lut.empty();
}

void test(std::string s, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = removeInvalidParentheses(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("()())()", {"(())()", "()()()"}, trials);
    test("(a)())()", {"(a())()", "(a)()()"}, trials);
    test(")(", {""}, trials);
    return 0;
}


