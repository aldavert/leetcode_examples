#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

long long minEnd(int n, int x)
{
    const int max_bit = static_cast<int>(std::log2(n) + std::log2(x)) + 2;
    const long k = n - 1;
    long result = x;
    for (int i = 0, k_binary_index = 0; i < max_bit; ++i)
    {
        if ((result >> i & 1) == 0)
        {
            if (k >> k_binary_index & 1)
                result |= 1L << i;
            ++k_binary_index;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int x, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minEnd(n, x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 4, 6, trials);
    test(2, 7, 15, trials);
    return 0;
}


