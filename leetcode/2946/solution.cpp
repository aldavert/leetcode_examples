#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool areSimilar(std::vector<std::vector<int> > mat, int k)
{
    const int m = (mat.size() > 0)?static_cast<int>(mat[0].size()):0;
    int start[2] = { k % m, (m + (-k % m)) % m };
    for (bool even = false; const auto &row : mat)
    {
        for (int i = 0, j = start[even]; i < m; ++i, ++j)
        {
            j *= (j != m);
            if (row[i] != row[j]) return false;
        }
        even = !even;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > mat,
          int k,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = areSimilar(mat, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 1, 2}, {5, 5, 5, 5}, {6, 3, 6, 3}}, 2, true, trials);
    test({{2, 2}, {2, 2}}, 3, true, trials);
    test({{1, 2}}, 1, false, trials);
    return 0;
}


