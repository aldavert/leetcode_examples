#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > divideArray(std::vector<int> nums, int k)
{
    std::vector<std::vector<int> > result;
    std::vector<int> current(3);
    std::sort(nums.begin(), nums.end());
    for (size_t i = 0; i < nums.size(); i += 3)
    {
        if (nums[i + 2] - nums[i + 0] > k) return {};
        current[0] = nums[i + 0];
        current[1] = nums[i + 1];
        current[2] = nums[i + 2];
        result.push_back(current);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = divideArray(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 4, 8, 7, 9, 3, 5, 1}, 2, {{1, 1, 3}, {3, 4, 5}, {7, 8, 9}}, trials);
    test({1, 3, 3, 2, 7, 3}, 3, {}, trials);
    return 0;
}


