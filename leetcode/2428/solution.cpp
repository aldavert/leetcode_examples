#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxSum(std::vector<std::vector<int> > grid)
{
    int result = 0;
    for (size_t i = 1; i + 1 < grid.size(); ++i)
        for (size_t j = 1; j + 1 < grid[0].size(); ++j)
            result = std::max(result, grid[i - 1][j - 1] + grid[i - 1][j]
                   + grid[i - 1][j + 1] + grid[i][j] + grid[i + 1][j - 1]
                   + grid[i + 1][j] + grid[i + 1][j + 1]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSum(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{6, 2, 1, 3}, {4, 2, 1, 5}, {9, 2, 8, 7}, {4, 1, 2, 9}}, 30, trials);
    test({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, 35, trials);
    return 0;
}


