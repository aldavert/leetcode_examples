#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > resultGrid(std::vector<std::vector<int> > image,
                                          int threshold)
{
    const int m = static_cast<int>(image.size()),
              n = static_cast<int>(image[0].size());
    std::vector<std::vector<int> > sums(m, std::vector<int>(n)),
                                   counts(m, std::vector<int>(n));
    auto isRegion = [&](int i, int j) -> bool
    {
        for (int x = i; x < i + 3; ++x)
            for (int y = j; y < j + 3; ++y)
                if (((x > i) && (std::abs(image[x][y] - image[x - 1][y]) > threshold))
                ||  ((y > j) && (std::abs(image[x][y] - image[x][y - 1]) > threshold)))
                    return false;
        return true;
    };
    for (int i = 0; i < m - 2; ++i)
    {
        for (int j = 0; j < n - 2; ++j)
        {
            if (isRegion(i, j))
            {
                int subgrid_sum = 0;
                for (int x = i; x < i + 3; ++x)
                    for (int y = j; y < j + 3; ++y)
                        subgrid_sum += image[x][y];
                subgrid_sum /= 9;
                for (int x = i; x < i + 3; ++x)
                {
                    for (int y = j; y < j + 3; ++y)
                    {
                        sums[x][y] += subgrid_sum;
                        counts[x][y] += 1;
                    }
                }
            }
        }
    }
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (counts[i][j] > 0)
                image[i][j] = sums[i][j] / counts[i][j];
    return image;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > image,
          int threshold,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = resultGrid(image, threshold);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{5, 6, 7, 10}, {8, 9, 10, 10}, {11, 12, 13, 10}}, 3,
         {{9, 9, 9, 9}, {9, 9, 9, 9}, {9, 9, 9, 9}}, trials);
    test({{10, 20, 30}, {15, 25, 35}, {20, 30, 40}, {25, 35, 45}}, 12,
         {{25, 25, 25}, {27, 27, 27}, {27, 27, 27}, {30, 30, 30}}, trials);
    return 0;
}


