#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool validPartition(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<bool> dp(n + 1);
    dp[0] = true;
    dp[2] = nums[0] == nums[1];
    for (int i = 3; i <= n; ++i)
    {
        int diff32 = nums[i - 2] - nums[i - 3], diff21 = nums[i - 1] - nums[i - 2];
        dp[i] = (dp[i - 2] && (nums[i - 2] == nums[i - 1]))
             || (dp[i - 3] && (diff32 == diff21) && ((diff32 & 0x7ffffffe) == 0));
    }
    return dp[n];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = validPartition(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 4, 4, 5, 6}, true, trials);
    test({1, 1, 1, 2}, false, trials);
    return 0;
}


