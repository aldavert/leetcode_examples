#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int jump(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size()) - 1;
    int result = 0, end = 0, farthest = 0;
    for (int i = 0; i < n; ++i)
    {
        farthest = std::max(farthest, i + nums[i]);
        if (farthest >= n)
            return ++result;
        if (i == end)
        {
            ++result;
            end = farthest;
        }
    }
    return result;
}
#else
int jump(std::vector<int> nums)
{
    int n = static_cast<int>(nums.size());
    std::vector<int> steps(nums.size(), std::numeric_limits<int>::max());
    steps[0] = 0;
    for (int i = 0; i < n; ++i)
        for (int j = 0, k = i + 1; (k < n) && (j < nums[i]); ++j, ++k)
            steps[k] = std::min(steps[i] + 1, steps[k]);
    return steps.back();
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = jump(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 1, 1, 4}, 2, trials);
    test({2, 3, 0, 1, 4}, 2, trials);
    return 0;
}


