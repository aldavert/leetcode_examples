#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> partitionLabels(std::string s)
{
    const int n = static_cast<int>(s.size());
    const int MAX = std::numeric_limits<int>::max();
    const int MIN = std::numeric_limits<int>::lowest();
    std::vector<int> result;
    std::pair<int, int> chr_range[26];
    for (int i = 0; i < 26; ++i)
        chr_range[i] = { MAX, MIN };
    for (int i = 0; i < n; ++i)
    {
        const int idx = static_cast<int>(s[i] - 'a');
        chr_range[idx].first  = std::min(chr_range[idx].first , i);
        chr_range[idx].second = std::max(chr_range[idx].second, i);
    }
    std::sort(&chr_range[0], &chr_range[26]);
    int previous = 0, largest = 0;
    for (int i = 0; (i < 26) && (chr_range[i].first < MAX); ++i)
    {
        if (chr_range[i].first <= largest)
        {
            largest = std::max(largest, chr_range[i].second);
        }
        else
        {
            result.push_back(largest - previous + 1);
            previous = chr_range[i].first;
            largest = chr_range[i].second;
        }
    }
    result.push_back(largest - previous + 1);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = partitionLabels(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ababcbacadefegdehijhklij", {9, 7, 8}, trials);
    test("eccbbbbdec", {10}, trials);
    return 0;
}


