#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findMinDifference(std::vector<std::string> timePoints)
{
    std::sort(timePoints.begin(), timePoints.end());
    auto toMinutes = [](std::string time) -> int
    {
        return (static_cast<int>(time[0] - '0') * 10
             +  static_cast<int>(time[1] - '0')) * 60
             + (static_cast<int>(time[3] - '0') * 10
             +  static_cast<int>(time[4] - '0'));
    };
    int previous = toMinutes(timePoints.front()),
        result = 23 * 60 + 60 - (toMinutes(timePoints.back()) - previous);
    for (size_t i = 1; i < timePoints.size(); ++i)
    {
        int minutes = toMinutes(timePoints[i]);
        result = std::min(result, minutes - previous);
        previous = minutes;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> timePoints, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMinDifference(timePoints);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"23:59", "00:00"}, 1, trials);
    test({"23:00", "00:00"}, 60, trials);
    test({"00:00", "23:59", "00:00"}, 0, trials);
    return 0;
}


