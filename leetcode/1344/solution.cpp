#include "../common/common.hpp"

// ############################################################################
// ############################################################################

double angleClock(int hour, int minutes)
{
    const double hour_hand_angle = (hour % 12 + minutes / 60.0) * 30;
    const double minute_hand_angle = minutes * 6;
    const double diff = std::abs(hour_hand_angle - minute_hand_angle);
    return std::min(diff, 360 - diff);
}

// ############################################################################
// ############################################################################

void test(int hour, int minutes, double solution, unsigned int trials = 1)
{
    double result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = angleClock(hour, minutes);
    showResult(std::abs(solution - result) < 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(12, 30, 165  , trials);
    test( 3, 30,  75  , trials);
    test( 3, 15,   7.5, trials);
    return 0;
}


