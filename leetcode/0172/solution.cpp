#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int trailingZeroes(int n)
{
    int result = 0;
    for (int factor = 5; factor <= n; factor *= 5)
        result += n / factor;
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = trailingZeroes(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 0, trials);
    test(5, 1, trials);
    test(0, 0, trials);
    return 0;
}


