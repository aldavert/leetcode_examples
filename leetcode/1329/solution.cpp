#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > diagonalSort(std::vector<std::vector<int> > mat)
{
    const int m = static_cast<int>(mat.size());
    const int n = static_cast<int>(mat[0].size());
    std::vector<std::vector<int> > result(m, std::vector<int>(n));
    std::vector<int> buffer;
    for (int row = m - 1; row >= 0; --row)
    {
        buffer.clear();
        for (int r = row, c = 0; (r < m) && (c < n); ++r, ++c)
            buffer.push_back(mat[r][c]);
        std::sort(buffer.begin(), buffer.end());
        for (int r = row, c = 0; (r < m) && (c < n); ++r, ++c)
            result[r][c] = buffer[c];
    }
    for (int col = 1; col < n; ++col)
    {
        buffer.clear();
        for (int c = col, r = 0; (r < m) && (c < n); ++r, ++c)
            buffer.push_back(mat[r][c]);
        std::sort(buffer.begin(), buffer.end());
        for (int c = col, r = 0; (r < m) && (c < n); ++r, ++c)
            result[r][c] = buffer[r];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > mat,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = diagonalSort(mat);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 3, 1, 1}, {2, 2, 1, 2}, {1, 1, 1, 2}},
         {{1, 1, 1, 1}, {1, 2, 2, 2}, {1, 2, 3, 3}}, trials);
    test({{11, 25, 66, 1, 69, 7}, {23, 55, 17, 45, 15, 52}, {75, 31, 36, 44, 58, 8},
          {22, 27, 33, 25, 68, 4}, {84, 28, 14, 11, 5, 50}},
         {{5, 17, 4, 1, 52, 7}, {11, 11, 25, 45, 8, 69}, {14, 23, 25, 44, 58, 15},
          {22, 27, 31, 36, 50, 66}, {84, 28, 75, 33, 55, 68}}, trials);
    return 0;
}


