#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int profitableSchemes(int n,
                      int minProfit,
                      std::vector<int> group,
                      std::vector<int> profit)
{
    constexpr int MOD = 1'000'000'007;
    std::vector<std::vector<int> > dp(n + 1, std::vector<int>(minProfit + 1));
    for (int i = 0; i <= n; ++i) dp[i][0] = 1;
    for (int k = 1, m = static_cast<int>(group.size()); k <= m; ++k)
        for (int i = n; i >= group[k - 1]; --i)
            for (int j = minProfit; j >= 0; --j)
                dp[i][j] += dp[i - group[k - 1]][std::max(0, j - profit[k - 1])],
                dp[i][j] %= MOD;
    return dp[n][minProfit];
}

// ############################################################################
// ############################################################################

void test(int n,
          int minProfit,
          std::vector<int> group,
          std::vector<int> profit,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = profitableSchemes(n, minProfit, group, profit);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 3, {2, 2}, {2, 3}, 2, trials);
    test(10, 5, {2, 3, 5}, {6, 7, 8}, 7, trials);
    return 0;
}


