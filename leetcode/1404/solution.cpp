#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numSteps(std::string s)
{
    int result = 0;
    for (; s != "1"; ++result)
    {
        if (s.back() == '0') s.pop_back();
        else
        {
            int i = static_cast<int>(s.size()) - 1;
            for ( ; i > 0; --i)
            {
                if (s[i] == '1') s[i] = '0';
                else
                {
                    s[i] = '1';
                    break;
                }
            }
            if (i == 0) s.push_back('0');
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numSteps(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1101", 6, trials);
    test("1111", 5, trials);
    test("10", 1, trials);
    test("1", 0, trials);
    return 0;
}


