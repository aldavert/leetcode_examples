#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

bool wordPattern(std::string pattern, std::string s)
{
    const int np = static_cast<int>(pattern.size());
    const std::string_view sv = s;
    std::unordered_map<std::string_view, int> word_id;
    int lut_pattern[32] = {}, lut_word[32] = {}, begin = 0, end = 0, pos = 0;
    auto check = [&]() -> bool
    {
        const auto word = sv.substr(begin, end - begin);
        int letter = pattern[pos] - 'a' + 1, id;
        
        if (auto search = word_id.find(word); search != word_id.end())
            id = search->second;
        else word_id[word] = (id = static_cast<int>(word_id.size()) + 1);
        
        if (!(lut_pattern[letter] || lut_word[id]))
        {
            lut_pattern[letter] = id;
            lut_word[id] = letter;
        }
        else if ((lut_pattern[letter] != id) || (lut_word[id] != letter))
            return false;
        return true;
    };
    for (char c : sv)
    {
        if (c == ' ')
        {
            if (!check()) return false;
            begin = end + 1;
            ++pos;
            if (pos == np) return false;
        }
        ++end;
    }
    if (!check()) return false;
    return (pos + 1) == np;
}

// ############################################################################
// ############################################################################

void test(std::string pattern, std::string s, bool solution, unsigned int trials = 1)
{
    bool result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = wordPattern(pattern, s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abba", "dog cat cat dog", true, trials);
    test("abba", "dog dog dog dog", false, trials);
    test("abba", "dog cat cat fish", false, trials);
    test("aaaa", "dog cat cat dog", false, trials);
    test("abc", "dog cat dog", false, trials);
    return 0;
}


