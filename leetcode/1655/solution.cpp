#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canDistribute(std::vector<int> nums, std::vector<int> quantity)
{
    unsigned int frequency[1001] = {};
    int sums[1024] = {};
    std::vector<int> count;
    
    for (int x : nums) ++frequency[x]; 
    for (int i = 0; i <= 1000; ++i)
        if (frequency[i])
            count.push_back(frequency[i]);
    const int n = static_cast<int>(count.size());
    const int m = static_cast<int>(quantity.size());
    const int o = 1 << m;
    for (int mask = 0; mask < o; ++mask)
        for (int i = 0; i < m; ++i)
            if (mask & (1 << i))
                sums[mask] += quantity[i];
    
    std::vector<std::vector<int> > dp(o, std::vector<int>(n + 1));
    dp[0][0] = 1;    
    for (int mask = 0; mask < o; ++mask)
    {
        for (int i = 0; i < n; ++i)
        {
            dp[mask][i + 1] |= dp[mask][i];
            for (int current = mask; current; current = (current - 1) & mask)
                if (sums[current] <= count[i] && dp[mask ^ current][i])
                    dp[mask][i + 1] = 1;
        }
    }
    return dp[o - 1][n];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> quantity,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canDistribute(nums, quantity);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, {2}, false, trials);
    test({1, 2, 3, 3}, {2}, true, trials);
    test({1, 1, 2, 2}, {2, 2}, true, trials);
    return 0;
}


