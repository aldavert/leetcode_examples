#include "../common/common.hpp"
#include "test_a.hpp"

// ############################################################################
// ############################################################################

int arrayNesting(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> jumps(n);
    int result = 0;
    for (int i = 0; i < n; ++i)
    {
        if (!jumps[i])
        {
            int current = 0, pos;
            for (pos = i; !jumps[pos]; ++current, pos = nums[pos])
                jumps[pos] = -1;
            if (jumps[pos] > 0)
                current += jumps[pos];
            result = std::max(result, current);
            for (pos = i; jumps[pos] == -1; --current, pos = nums[pos])
                jumps[pos] = current;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = arrayNesting(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 10'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 0, 3, 1, 6, 2}, 4, trials);
    test({0, 1, 2}, 1, trials);
    test({1, 2, 0, 0, 3, 4, 5}, 7, trials);
    test(testA::nums, testA::solution, trials);
    return 0;
}


