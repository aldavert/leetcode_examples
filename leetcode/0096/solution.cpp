#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numTrees(int n)
{
    int dp[20] = {};
    dp[0] = 1;
    dp[1] = 1;
    for (int i = 2; i <= n; ++i)
        for (int j = 1; j <= i; ++j)
            dp[i] += dp[i - j] * dp[j - 1];
    return dp[n];
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numTrees(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
#if 0
    for (int i = 1; i < 20; ++i)
    {
        int result = numTrees(i);
        int diff = result - numTrees(i - 1);
        std::cout << i << ' ' << result << ' ' << diff;
        int approx = static_cast<int>(std::pow(3, i - 2));
        int error = diff - approx;
        std::cout << ' ' << approx << ' ' << std::cbrt(error) << '\n';
    }
#else
    test(1, 1, trials);
    test(2, 2, trials);
    test(3, 5, trials);
    test(4, 14, trials);
    test(5, 42, trials);
    test(6, 132, trials);
    test(7, 429, trials);
    test(8, 1430, trials);
    test(9, 4862, trials);
    test(10, 16796, trials);
    test(11, 58786, trials);
    test(12, 208012, trials);
    test(13, 742900, trials);
    test(14, 2674440, trials);
    test(15, 9694845, trials);
    test(16, 35357670, trials);
    test(17, 129644790, trials);
    test(18, 477638700, trials);
    test(19, 1767263190, trials);
#endif
    return 0;
}


