#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int maxFreq(std::string s, int maxLetters, int minSize, int /*maxSize*/)
{
    int result = 0, letters = 0, count[26] = {};
    std::unordered_map<std::string, int> substring_count;
    
    for (int l = 0, r = 0, n = static_cast<int>(s.size()); r < n; ++r)
    {
        letters += (++count[s[r] - 'a'] == 1);
        while ((letters > maxLetters) || (r - l + 1 > minSize))
            if (--count[s[l++] - 'a'] == 0)
                --letters;
        if (r - l + 1 == minSize)
            result = std::max(result, ++substring_count[s.substr(l, minSize)]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          int maxLetters,
          int minSize,
          int maxSize,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxFreq(s, maxLetters, minSize, maxSize);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aababcaab", 2, 3, 4, 2, trials);
    test("aaaa", 1, 3, 3, 2, trials);
    return 0;
}


