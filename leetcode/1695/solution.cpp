#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int maximumUniqueSubarray(std::vector<int> &nums)
{
    const int n = static_cast<int>(nums.size());
    bool present[10'001] = {};
    int result = 0, active_result = 0;
    int begin = 0;
    for (int i = 0; i < n; ++i)
    {
        if (!present[nums[i]])
        {
            present[nums[i]] = true;
            active_result += nums[i];
        }
        else
        {
            if (active_result > result)
                result = active_result;
            for (; nums[begin] != nums[i]; ++begin)
            {
                active_result -= nums[begin];
                present[nums[begin]] = false;
            }
            ++begin;
        }
    }
    if (active_result > result)
        result = active_result;
    return result;
}
#else
int maximumUniqueSubarray(std::vector<int> &nums)
{
    std::unordered_set<int> present;
    int result = 0, active_result = 0;
    int begin = 0;
    for (const auto &n : nums)
    {
        if (present.find(n) == present.end())
        {
            present.insert(n);
            active_result += n;
        }
        else
        {
            if (active_result > result)
                result = active_result;
            for (; nums[begin] != n; ++begin)
            {
                active_result -= nums[begin];
                present.erase(nums[begin]);
            }
            ++begin;
        }
    }
    if (active_result > result)
        result = active_result;
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumUniqueSubarray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 4, 5, 6}, 17, trials);
    test({5, 2, 1, 2, 5, 2, 1, 2, 5}, 8, trials);
    test({187, 470,  25, 436, 538, 809, 441, 167, 477, 110, 275, 133, 666, 345,
          411, 459, 490, 266, 987, 965, 429, 166, 809, 340, 467, 318, 125, 165,
          809, 610,  31, 585, 970, 306,  42, 189, 169, 743,  78, 810,  70, 382,
          367, 490, 787, 670, 476, 278, 775, 673, 299,  19, 893, 817, 971, 458,
          409, 886, 434}, 16911, trials);
    return 0;
}


