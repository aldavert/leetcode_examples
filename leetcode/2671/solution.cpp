#include "../common/common.hpp"
#include <unordered_map>

constexpr int null = -1'000;

// ############################################################################
// ############################################################################

class FrequencyTracker
{
    std::unordered_map<int, int> m_count;
    std::unordered_map<int, int> m_frequency_count;
public:
    FrequencyTracker(void) = default;
    void add(int number)
    {
        if (m_count[number] > 0)
            --m_frequency_count[m_count[number]];
        ++m_count[number];
        ++m_frequency_count[m_count[number]];
    }
    void deleteOne(int number)
    {
        if (m_count[number] == 0)
            return;
        --m_frequency_count[m_count[number]];
        --m_count[number];
        ++m_frequency_count[m_count[number]];
    }
    bool hasFrequency(int frequency)
    {
        return m_frequency_count[frequency] > 0;
    }
};

// ############################################################################
// ############################################################################

enum class OP { ADD, DELETE, HAS };

void test(std::vector<OP> operations,
          std::vector<int> parameter,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        FrequencyTracker obj;
        result.clear();
        for (size_t i = 0; i < operations.size(); ++i)
        {
            switch (operations[i])
            {
            case OP::ADD:
                obj.add(parameter[i]);
                result.push_back(null);
                break;
            case OP::DELETE:
                obj.deleteOne(parameter[i]);
                result.push_back(null);
                break;
            case OP::HAS:
                result.push_back(obj.hasFrequency(parameter[i]));
                break;
            default:
                std::cerr << "[ERROR] Unknown parameter\n.";
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::ADD, OP::ADD, OP::HAS}, {3, 3, 2}, {null, null, true}, trials);
    test({OP::ADD, OP::DELETE, OP::HAS}, {1, 1, 1}, {null, null, false}, trials);
    test({OP::HAS, OP::ADD, OP::HAS}, {2, 3, 1}, {false, null, true}, trials);
    return 0;
}


