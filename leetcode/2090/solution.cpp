#include "../common/common.hpp"

// ############################################################################
// ############################################################################

// 0 1 2 3 4 5 6 7 8
// 7 4 3 9 1 8 5 2 6
//       ^     ^

std::vector<int> getAverages(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> result(n, -1);
    long sum = 0;
    if (2 * k < n)
        for (int i = 0; i < 2 * k; ++i)
            sum += nums[i];
    for (int i = k; i + k < n; ++i)
    {
        sum += nums[i + k];
        result[i] = static_cast<int>(sum / (2 * k + 1));
        sum -= nums[i - k];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getAverages(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({7, 4, 3, 9, 1, 8, 5, 2, 6}, 3, {-1, -1, -1, 5, 4, 4, -1, -1, -1}, trials);
    test({100'000}, 0, {100'000}, trials);
    test({8}, 100'000, {-1}, trials);
    return 0;
}


