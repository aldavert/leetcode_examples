#include "../common/common.hpp"
#include <numeric>
#include <limits>
#include <unordered_map>

// ############################################################################
// ############################################################################

int getLargestOutlier(std::vector<int> nums)
{
    int total_sum = std::accumulate(nums.begin(), nums.end(), 0);
    std::unordered_map<int, int> histogram;
    for (int number : nums)
        ++histogram[number];
    int result = std::numeric_limits<int>::lowest();
    for (int number : nums)
    {
        int sum = total_sum - number;
        if (sum & 1) continue;
        if (auto search = histogram.find(sum / 2);
            (search != histogram.end()) && (search->second > (search->first == number)))
            result = std::max(result, number);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getLargestOutlier(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 5, 10}, 10, trials);
    test({-2, -1, -3, -6, 4}, 4, trials);
    test({1, 1, 1, 1, 1, 5, 5}, 5, trials);
    test({6, -31, 50, -35, 41, 37, -42, 13}, -35, trials);
    return 0;
}


