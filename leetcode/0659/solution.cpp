#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

bool isPossible(std::vector<int> nums)
{
    struct info
    {
        int last;
        int size;
        bool operator<(const info &other) const
        {
            return (last > other.last)
                || ((last == other.last) && (size > other.size));
        }
    };
    std::priority_queue<info> queue;
    for (int n : nums)
    {
        while (!queue.empty() && (n - queue.top().last > 1))
        {
            if (queue.top().size < 3) return false;
            queue.pop();
        }
        if (queue.empty() || (queue.top().last == n))
            queue.push({n, 1});
        else
        {
            int size = queue.top().size;
            queue.pop();
            queue.push({n, size + 1});
        }
    }
    while (!queue.empty())
    {
        if (queue.top().size < 3) return false;
        queue.pop();
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPossible(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 3, 4, 5}, true, trials);
    test({1, 2, 3, 3, 4, 4, 5, 5}, true, trials);
    test({1, 2, 3, 4, 4, 5}, false, trials);
    test({1, 2, 3, 4, 4, 5, 6}, true, trials);
    test({1, 2, 3, 7, 8, 9}, true, trials);
    return 0;
}


