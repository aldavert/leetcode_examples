#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int xorBeauty(std::vector<int> nums)
{
    return std::accumulate(nums.begin(), nums.end(), 0, std::bit_xor<>());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = xorBeauty(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4}, 5, trials);
    test({15, 45, 20, 2, 34, 35, 5, 44, 32, 30}, 34, trials);
    return 0;
}


