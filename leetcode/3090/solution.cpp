#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumLengthSubstring(std::string s)
{
    int histogram[128] = {};
    int result = 0;
    for (int l = 0, r = 0, n = static_cast<int>(s.size()); r < n; )
    {
        if (histogram[static_cast<int>(s[r])] < 2)
            ++histogram[static_cast<int>(s[r++])];
        else --histogram[static_cast<int>(s[l++])];
        result = std::max(result, r - l);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumLengthSubstring(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("bcbbbcba", 4, trials);
    test("aaaa", 2, trials);
    return 0;
}


