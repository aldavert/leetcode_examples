#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maxMatrixSum(std::vector<std::vector<int> > matrix)
{
    long abs_sum = 0;
    int min_abs = std::numeric_limits<int>::max(), odd_neg = 0;
    
    for (auto &row : matrix)
    {
        for (int num : row)
        {
            abs_sum += std::abs(num);
            min_abs = std::min(min_abs, std::abs(num));
            if (num < 0)
                odd_neg ^= 1;
        }
    }
    return abs_sum - odd_neg * min_abs * 2;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxMatrixSum(matrix);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, -1}, {-1, 1}}, 4, trials);
    test({{1, 2, 3}, {-1, -2, -3}, {1, 2, 3}}, 16, trials);
    return 0;
}


