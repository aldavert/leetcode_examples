#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int countArrangement(int n)
{
    auto dfs = [&n](void) -> int
    {
        int mask = 0;
        std::unordered_map<int, int> lut;
        auto inner = [&](auto &&self, int num) -> int
        {
            if (num == n + 1) return 1;
            if (auto search = lut.find(mask); search != lut.end()) return search->second;
            int result = 0;
            for (int i = 1; i <= n; ++i)
            {
                if ((!((mask >> (i - 1)) & 1)) && ((num % i == 0) || (i % num == 0)))
                {
                    int previous_mask = mask;
                    mask = mask | (1 << (i - 1));
                    result += self(self, num + 1);
                    mask = previous_mask;
                }
            }
            return lut[mask] = result;
        };
        return inner(inner, 1);
    };
    return dfs();
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countArrangement(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test( 2,     2, trials);
    test( 1,     1, trials);
    test( 3,     3, trials);
    test( 4,     8, trials);
    test( 5,    10, trials);
    test( 6,    36, trials);
    test( 7,    41, trials);
    test( 8,   132, trials);
    test( 8,   132, trials);
    test( 9,   250, trials);
    test(10,   700, trials);
    test(11,   750, trials);
    test(12,  4010, trials);
    test(13,  4237, trials);
    test(14, 10680, trials);
    test(15, 24679, trials);
    return 0;
}


