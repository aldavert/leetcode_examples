#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkStrings(std::string s1, std::string s2)
{
    int count[2][26] = {};
    for (int i = 0, n = static_cast<int>(s1.size()); i < n; ++i)
    {
        ++count[i % 2][s1[i] - 'a'];
        --count[i % 2][s2[i] - 'a'];
    }
    for (int i = 0; i < 26; ++i)
        if ((count[0][i] > 0) || (count[1][i] > 0))
            return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string s1, std::string s2, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkStrings(s1, s2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcdba", "cabdab", true, trials);
    test("abe", "bea", false, trials);
    return 0;
}


