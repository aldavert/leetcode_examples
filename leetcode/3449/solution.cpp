#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
long long maxScore(std::vector<int> points, int m)
{
    const int n = static_cast<int>(points.size());
    long left = 1,
         right = static_cast<long>(*std::max_element(points.begin(), points.end())) * m;
    while (left <= right)
    {
        long p = 0, c = m;
        const long mid = (left + right) / 2;
        for (int i = 0; i < n; ++i)
        {
            const long need = std::max((mid + points[i] - 1L) / points[i] - p, 0L);
            if (need)
            {
                p = need - 1;
                c -= need + p;
            }
            else if (i + 1 < n)
            {
                p = 0;
                --c;
            }
            if (c < 0) break;
        }
        if (c >= 0) left = mid + 1;
        else right = mid - 1;
    }
    return left - 1;
}
#else
long long maxScore(std::vector<int> points, int m)
{
    const int n = static_cast<int>(points.size());
    std::vector<long> need(n);
    auto good = [&](long target) -> bool
    {
        for (int i = 0; i < n; ++i)
            need[i] = (points[i] + target - 1) / points[i];
        if (std::accumulate(need.begin(), need.end(), 0) > static_cast<long>(m))
            return false;
        long turns = 0;
        for (int i = 0; i < n - 1; ++i)
        {
            if (need[i] == 0) { ++turns; continue; }
            turns += need[i] * 2 - 1;
            long delta = need[i] - 1 - need[i + 1];
            if (delta > 0) need[i + 1] += delta;
            need[i + 1] -= need[i] - 1;
            if (need[i + 1] < 0) need[i + 1] = 0;
        }
        if (need[n - 1] > 0) turns += need[n - 1] * 2 - 1;
        return turns <= static_cast<long>(m);
    };
    long left = 0, right = 1e15;
    while (left < right)
    {
        long mid = (left + right + 1) / 2;
        if (good(mid)) left = mid;
        else right = mid - 1;
    }
    return left;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> points, int m, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxScore(points, m);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4}, 3, 4, trials);
    test({1, 2, 3}, 5, 2, trials);
    return 0;
}


