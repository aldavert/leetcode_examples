#include "../common/common.hpp"

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class LUPrefix
{
    std::unordered_set<int> m_seen;
    int m_longest_prefix = 0;
public:
    LUPrefix([[maybe_unused]] int n) {};
    void upload(int video)
    {
        m_seen.insert(video);
        while (m_seen.contains(m_longest_prefix + 1))
            ++m_longest_prefix;
    }
    int longest(void) { return m_longest_prefix; }
};

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<int> input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        LUPrefix obj(n);
        result.clear();
        for (int parameter : input)
        {
            if (parameter == null)
                result.push_back(obj.longest());
            else
            {
                obj.upload(parameter);
                result.push_back(null);
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {3, null, 1, null, 2, null}, {null, 0, null, 1, null, 3}, trials);
    return 0;
}


