#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string getSmallestString(std::string s, int k)
{
    std::string result = s;
    for (char &c : result)
    {
        if (k == 0) break;
        if (int dist_to_a = std::min(c - 'a', 'z' - c + 1); k >= dist_to_a)
        {
            k -= dist_to_a;
            c = 'a';
        }
        else
        {
            c -= static_cast<char>(k);
            k = 0;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getSmallestString(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("zbbz", 3, "aaaz", trials);
    test("xaxcd", 4, "aawcd", trials);
    test("lol", 0, "lol", trials);
    return 0;
}


