#include "../common/common.hpp"
#include <unordered_map>
#include <queue>

// ############################################################################
// ############################################################################

#if 1
std::vector<std::string>
watchedVideosByFriends(std::vector<std::vector<std::string> > watchedVideos,
                       std::vector<std::vector<int> > friends,
                       int id,
                       int level)
{
    int queue[101] = {};
    char visited[101] = {};
    int queue_begin = 0, queue_end = 0;
    queue[queue_end++] = id;
    visited[id] = 1;
    while (level && (queue_begin != queue_end))
    {
        --level;
        for (int queue_index = queue_end; queue_begin != queue_index;)
            for (int value = queue[queue_begin++]; int f : friends[value])
                if (!visited[f])
                    visited[f] = 1,
                    queue[queue_end++] = f;
    }
    std::unordered_map<std::string_view, int> vc;
    while (queue_begin != queue_end)
    {
        int value = queue[queue_begin++];
        for (const auto &wv : watchedVideos[value]) ++vc[wv];
    }
    std::vector<std::pair<int, std::string_view> > srt;
    for (const auto &it : vc) srt.push_back({it.second, it.first});
    std::sort(srt.begin(), srt.end());
    std::vector<std::string> result;
    for (const auto &s : srt) result.push_back(std::string(s.second));
    return result; 
}
#else
std::vector<std::string>
watchedVideosByFriends(std::vector<std::vector<std::string> > watchedVideos,
                       std::vector<std::vector<int> > friends,
                       int id,
                       int level)
{
    std::vector<bool> visited(friends.size());
    std::queue<int> queue{{id}};
    std::unordered_map<std::string, int> count;
    std::set<std::pair<int, std::string> > frequency_and_video;
    
    visited[id] = true;
    for (int i = 0; i < level; ++i)
    {
        for (int j = static_cast<int>(queue.size()); j > 0; --j)
        {
            for (int f : friends[queue.front()])
            {
                if (visited[f] == false)
                {
                    visited[f] = true;
                    queue.push(f);
                }
            }
            queue.pop();
        }
    }
    for (int i = static_cast<int>(queue.size()); i > 0; --i)
    {
        for (const auto &video : watchedVideos[queue.front()])
            ++count[video];
        queue.pop();
    }
    for (const auto& [video, frequency] : count)
        frequency_and_video.insert({frequency, video});
    std::vector<std::string> result;
    for (const auto &[_, video] : frequency_and_video)
        result.push_back(video);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<std::string> > watchedVideos,
          std::vector<std::vector<int> > friends,
          int id,
          int level,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = watchedVideosByFriends(watchedVideos, friends, id, level);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{"A", "B"}, {"C"}, {"B", "C"}, {"D"}},
         {{1, 2}, {0, 3}, {0, 3}, {1, 2}}, 0, 1, {"B", "C"}, trials);
    test({{"A", "B"}, {"C"}, {"B", "C"}, {"D"}},
         {{1, 2}, {0, 3}, {0, 3}, {1, 2}}, 0, 2, {"D"}, trials);
    return 0;
}


