#include "../common/common.hpp"
#include <map>

// ############################################################################
// ############################################################################

bool isNStraightHand(std::vector<int> hand, int groupSize)
{
    std::map<int, int> histogram;
    for (int card : hand) ++histogram[card];
    for (auto [value, frequency] : histogram)
    {
        if (frequency == 0) continue;
        for (int i = value; i < value + groupSize; ++i)
            if ((histogram[i] -= frequency) < 0)
                return false;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> hand, int groupSize, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isNStraightHand(hand, groupSize);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 6, 2, 3, 4, 7, 8}, 3, true, trials);
    test({1, 2, 3, 4, 5}, 3, false, trials);
    return 0;
}


