#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int countPairs(std::vector<int> deliciousness)
{
    constexpr int MOD = 1'000'000'007;
    constexpr int MAXPOWER = 1 << 21;;
    std::unordered_map<int, int> count;
    int result = 0;
    
    for (const int d : deliciousness)
    {
        for (int power = 1; power <= MAXPOWER; power *= 2)
            if (const auto it = count.find(power - d); it != count.cend())
                result = (result + it->second) % MOD;
        ++count[d];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> deliciousness, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPairs(deliciousness);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 5, 7, 9}, 4, trials);
    test({1, 1, 1, 3, 3, 3, 7}, 15, trials);
    return 0;
}


