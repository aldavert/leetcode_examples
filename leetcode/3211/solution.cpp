#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> validStrings(int n)
{
    std::vector<std::string> result;
    auto dfs = [&](auto &&self, int m, std::string &s)
    {
        if (m == 0)
        {
            result.push_back(s);
            return;
        }
        if (s.empty() || (s.back() == '1'))
        {
            s.push_back('0');
            self(self, m - 1, s);
            s.pop_back();
        }
        s.push_back('1');
        self(self, m - 1, s);
        s.pop_back();
    };
    std::string substring;
    dfs(dfs, n, substring);
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = validStrings(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {"010", "011", "101", "110", "111"}, trials);
    test(1, {"0", "1"}, trials);
    return 0;
}


