#include "../common/common.hpp"
#include <deque>

// ############################################################################
// ############################################################################

#if 1
long long minimumTotalDistance(std::vector<int> robot,
                               std::vector<std::vector<int> > factory)
{
    const int n = static_cast<int>(robot.size()),
              m = static_cast<int>(factory.size());
    std::sort(robot.begin(), robot.end());
    std::sort(factory.begin(), factory.end());
    long accum_robot[102] = {}, s[102] = {}, count[102] = {};
    for (int i = 1; i <= n; ++i) accum_robot[i] = accum_robot[i - 1] + robot[i - 1];
    auto calc = [&](int i, int x) -> long
    {
        return 2 * s[i] - 2 * x * count[i] + static_cast<long>(x) * i - accum_robot[i];
    };
    std::vector<long> dp[] = {std::vector<long>(n + 1, 1e13),
                              std::vector<long>(n + 1, 1e13)};
    std::deque<std::pair<long, int> > deque;
    dp[0][0] = 0;
    int cur = 0;
    for (int j = 0; j < m; ++j)
    {
        int position = factory[j][0], limit = factory[j][1];
        if (!limit) continue;
        deque.clear();
        deque.push_back({0L, 0});
        for (int i = 1; i <= n; ++i)
        {
            s[i] = s[i - 1] + (robot[i - 1] > position ? robot[i - 1] : 0);
            count[i] = count[i - 1] + (robot[i - 1] > position);
            while (!deque.empty() && (deque.front().second < i - limit))
                deque.pop_front();
            long val = calc(i, position);
            dp[cur ^ 1][i] = std::min(dp[cur][i], deque.front().first + val);
            while (!deque.empty() && (deque.back().first >= dp[cur][i] - val))
                deque.pop_back();
            deque.push_back({dp[cur][i] - val, i});
        }
        cur ^= 1;
    }
    return dp[cur][n];
}
#else
long long minimumTotalDistance(std::vector<int> robot,
                               std::vector<std::vector<int> > factory)
{
    const int n_robot = static_cast<int>(robot.size()),
              n_factory = static_cast<int>(factory.size());
    std::sort(robot.begin(), robot.end());
    std::sort(factory.begin(), factory.end());
    
    long long dp[101][101][101] = {};
    auto minimumTotalDistance = [&](auto &&self, int i, int j, int k) -> long
    {
        if (i == n_robot) return 0;
        if (j == n_factory) return std::numeric_limits<long>::max();
        if (dp[i][j][k] > 0) return dp[i][j][k];
        long skip_factory = self(self, i, j + 1, 0);
        int position = factory[j][0], limit = factory[j][1];
        long use_factory = (limit > k)?self(self, i + 1, j, k + 1)
                                      + std::abs(robot[i] - position)
                                      :std::numeric_limits<long>::max() / 2;
        return dp[i][j][k] = std::min(skip_factory, use_factory);
    };
    return minimumTotalDistance(minimumTotalDistance, 0, 0, 0);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> robot,
          std::vector<std::vector<int> > factory,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumTotalDistance(robot, factory);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 4, 6}, {{2, 2}, {6, 2}}, 4, trials);
    test({1, -1}, {{-2, 1}, {2, 1}}, 2, trials);
    return 0;
}


