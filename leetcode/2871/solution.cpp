#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxSubarrays(std::vector<int> nums)
{
    int result = 0;
    for (int score = 0; const int num : nums)
    {
        score = (score == 0)?num:score & num;
        result += (score == 0);
    }
    return result + (result == 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSubarrays(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 2, 0, 1, 2}, 3, trials);
    test({5, 7, 1, 3}, 1, trials);
    return 0;
}


