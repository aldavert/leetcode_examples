#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maximumValueSum(std::vector<std::vector<int> > board)
{
    struct Selected
    {
        long row = -1;
        long col = -1;
        long value = std::numeric_limits<int>::lowest();
    };
    const int n_rows = static_cast<int>(board.size()),
              n_cols = static_cast<int>(board[0].size());
    Selected m1, m2;
    for (int r = 0; r < n_rows; ++r)
        for (int c = 0; c < n_cols; ++c)
            if (board[r][c] > m1.value)
                m1 = { r, c, board[r][c] };
    for (int r = 0; r < n_rows; ++r)
    {
        if (r == m1.row) continue;
        for (int c = 0; c < n_cols; ++c)
            if ((c != m1.col) && (board[r][c] > m2.value))
                m2 = { r, c, board[r][c] };
    }
    long r1 = 0, mx = std::numeric_limits<long>::lowest();
    for (int r = 0; r < n_rows; ++r)
        if ((r != m1.row) && (r != m2.row) && (board[r][m1.col] > mx))
            mx = board[r][m1.col], r1 = r;
    long c1 = 0;
    mx = std::numeric_limits<long>::lowest();
    for (int c = 0; c < n_cols; ++c)
        if ((c != m1.col) && (c != m2.col) && (board[m1.row][c] > mx))
            mx = board[m1.row][c], c1 = c;
    
    long r0 = (board[m2.row][m1.col] > board[r1][m1.col])?m2.row:r1;
    long c0 = (board[m1.row][m2.col] > board[m1.row][c1])?m2.col:c1;
    long m3 = std::numeric_limits<long>::lowest();
    for (int r = 0; r < n_rows; ++r)
    {
        if ((r == m1.row) || (r == m2.row) || (r == r0)) continue;
        for (int c = 0; c < n_cols; ++c)
            if ((c != m1.col) && (c != m2.col) && (c != c0))
                m3 = std::max<long>(m3, board[r][c]);
    }

    long r0m = std::numeric_limits<long>::lowest(),
         c0m = std::numeric_limits<long>::lowest(), mv, mh;
    {            
        long r2m = std::numeric_limits<long>::lowest(),
             c2m = std::numeric_limits<long>::lowest();
        for (int r = 0; r < n_rows; ++r)
        {
            if ((r != m1.row) && (r != m2.row) && (r != r0))
            {
                c0m = std::max<long>(c0m, board[r][c0]);
                c2m = std::max<long>(c2m, board[r][m2.col]);
            }
        }
        for (int c = 0; c < n_cols; ++c)
        {
            if ((c != m1.col) && (c != m2.col) && (c != c0))
            {
                r0m = std::max<long>(r0m, board[r0][c]);
                r2m = std::max<long>(r2m, board[m2.row][c]);
            }
        }
        mv = mh = m3;
        if (r0 != m2.row)
            mv = std::max(mv, r0m),
            mh = std::max(mh, r2m),
            m3 = std::max(m3, r0m);
        if (c0 != m2.col)
            mv = std::max(mv, c2m),
            mh = std::max(mh, c0m),
            m3 = std::max(m3, c0m);
        if ((r0 != m2.row) && (c0 != m2.col))
        {
            m3 = std::max<long>(m3, board[r0][c0]);
            mv = std::max<long>(mv, board[r0][m2.col]);
            mh = std::max<long>(mh, board[m2.row][c0]);
        }
    }
    long s = std::max(m1.value + m2.value + m3,
                      board[m1.row][m2.col] + board[m2.row][m1.col] + m3);
    {
        long mr = std::numeric_limits<long>::lowest(),
             mc = std::numeric_limits<long>::lowest();
        for (int r = 0; r < n_rows; ++r)
            if ((r != m2.row) && (r != m1.row))
                mr = std::max<long>(mr, board[r][m2.col]);
        for (int c = 0; c < n_cols; ++c)
            if ((c != m2.col) && (c != m1.col))
                mc = std::max<long>(mc, board[m2.row][c]);
        s = std::max(s, m1.value + mr + mc);
    }
    s = std::max(s, m2.value + board[r1][m1.col] + board[m1.row][c1]);
    {
        long u = mv + board[m1.row][c0];
        long mx1 = c0m;
        if (r0 != m2.row) mx1 = std::max<long>(mx1, board[r0][c0]);
        if (mx1 > mv)
        {
            long mx2 = std::numeric_limits<long>::lowest();
            for (int c = 0; c < n_cols; ++c)
                if ((c != m1.col) && (c != c0))
                    mx2 = std::max<long>(mx2, board[m1.row][c]);
            u = std::max(u, mx1 + mx2);
        }
        s = std::max(s, u + board[m2.row][m1.col]);
    }
    {
        long u = mh + board[r0][m1.col], mx1 = r0m;
        if (c0 != m2.col)
            mx1 = std::max<long>(mx1, board[r0][c0]);
        if (mx1 > mh)
        {
            long mx2 = std::numeric_limits<long>::lowest();
            for (int r = 0; r < n_rows; ++r)
                if ((r != m1.row) && (r != r0))
                    mx2 = std::max<long>(mx2, board[r][m1.col]);
            u = std::max(u, mx1 + mx2);
        }
        s = std::max(s, u + board[m1.row][m2.col]);
    }
    return s;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > board,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumValueSum(board);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{-3, 1, 1, 1}, {-3, 1, -3, 1}, {-3, 2, 1, 1}}, 4, trials);
    test({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, 15, trials);
    test({{1, 1, 1}, {1, 1, 1}, {1, 1, 1}}, 3, trials);
    test({{-2, -2, 10,  0,  0, -2},
          { 0, 10, -2, -2, -2,  0},
          {-2,  0,  0, -2, -2, -2},
          {-2,  0,  0, -2, -2, -2},
          {-2, -2, -2, -2, -2, -1},
          {-2, -2, -2, -2, -2, -2}}, 19, trials);
    return 0;
}


