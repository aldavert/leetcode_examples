#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

long long beautifulSubarrays(std::vector<int> nums)
{
    long result = 0;
    
    std::unordered_map<int, int> prefix_count{{0, 1}};
    for (int prefix = 0; const int num : nums)
    {
        prefix ^= num;
        result += prefix_count[prefix]++;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = beautifulSubarrays(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 3, 1, 2, 4}, 2, trials);
    test({1, 10, 4}, 0, trials);
    return 0;
}


