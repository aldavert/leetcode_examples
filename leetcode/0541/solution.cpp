#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string reverseStr(std::string s, int k)
{
    const int n = static_cast<int>(s.size());
    for (int i = 0; i < n; i += 2 * k)
        std::reverse(&s[i], &s[std::min(n, i + k)]);
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = reverseStr(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcdefg", 2, "bacdfeg", trials);
    test("abcd", 2, "bacd", trials);
    test("abcd", 4, "dcba", trials);
    return 0;
}


