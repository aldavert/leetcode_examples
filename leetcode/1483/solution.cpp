#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
class TreeAncestor
{
    std::vector<int> anc[16];
public:
    TreeAncestor(int n, std::vector<int> &parent)
    {
        anc[0] = parent;
        for (int i = 1; i < 16; i++)
        {
            anc[i].resize(n);
            for (int v = 0; v < n; v++)
            {
                if (anc[i - 1][v] == -1) anc[i][v] = -1;
                else anc[i][v] = anc[i - 1][anc[i - 1][v]];
            }
        }
    }
    int getKthAncestor(int node, int k)
    {
        for (int i = 0; i < 16; i++)
        {
            if (node == -1) return -1;
            if ((1 << i) & k) node = anc[i][node];
        }
        return node;
    }
};
#elif 0
class TreeAncestor
{
public:
    TreeAncestor(int n, std::vector<int> &parent) : dp(n)
    {
        for (int i = 0; i < n; i ++)
            dp[i].push_back(parent[i]);
        for (int j = 1; ; ++j)
        {
            bool allminus = true;
            for (int i = 0; i < n; ++i)
            {
                int t = (dp[i][j - 1] != -1)?dp[dp[i][j - 1]][j - 1]:-1;
                dp[i].push_back(t);
                if (t != -1) allminus = false;
            }
            if (allminus) break;
        }
    }
    int getKthAncestor(int node, int k)
    {
        int result = node, pos = 0;
        while (k && (result != -1))
        {
            if (pos >= static_cast<int>(dp[result].size())) return -1;
            if (k & 1) result = dp[result][pos];
            k >>= 1;
            ++pos;
        }
        return result;
    }
private:
    std::vector<std::vector<int> > dp;
};
#else
class TreeAncestor
{
public:
    TreeAncestor(int n, std::vector<int> &parent)
      : m_max_level(32 - __builtin_clz(n)), m_dp(n, std::vector<int>(m_max_level))
    {
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m_max_level; ++j)
                if (j == 0)
                    m_dp[i][j] = parent[i];
                else if (m_dp[i][j - 1] == -1)
                    m_dp[i][j] = -1;
                else 
                    m_dp[i][j] = m_dp[m_dp[i][j - 1]][j - 1];
    }
    int getKthAncestor(int node, int k)
    {
        for (int j = 0; (j < m_max_level) && (node != -1); ++j)
            if (k & 1 << j)
                node = m_dp[node][j];
        return node;
    }
private:
    int m_max_level;
    std::vector<std::vector<int> > m_dp;
};
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<int> parent,
          std::vector<std::pair<int, int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        TreeAncestor object(n, parent);
        for (size_t i = 0; i < input.size(); ++i)
            result.push_back(object.getKthAncestor(input[i].first, input[i].second));
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, {-1, 0, 0, 1, 1, 2, 2}, {{3, 1}, {5, 2}, {6, 3}}, {1, 0, -1}, trials);
    test(7, {-1, 0, 0, 5, 1, 1, 5}, {{3, 2}, {5, 2}, {6, 3}}, {1, 0, 0}, trials);
    return 0;
}


