#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

int getLengthOfOptimalCompression(std::string s, int k)
{
    const int n = static_cast<int>(s.size());
    constexpr int KMAX = 101;
    int dp[KMAX][KMAX];
    std::memset(dp, KMAX, sizeof(dp));
    auto length = [](int frequency) -> int
    {
        return 1 + (frequency > 1) + (frequency >= 10) + (frequency >= 100);
    };
    auto process = [&](auto &&self, int i, int m) -> int
    {
        if (m < 0) return KMAX;
        if ((i == n) || (n - i <= m)) return 0;
        if ((dp[i][m] < KMAX)) return dp[i][m];
        int max_frequency = 0;
        int count[128] = {};
        for (int j = i; j < n; ++j)
        {
            max_frequency = std::max(max_frequency, ++count[static_cast<int>(s[j])]);
            dp[i][m] = std::min(dp[i][m],
                                length(max_frequency)
                                + self(self, j + 1, m - (j - i + 1 - max_frequency)));
        }
        return dp[i][m];
    };
    return process(process, 0, k);
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getLengthOfOptimalCompression(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aaabcccd", 2, 4, trials);
    test("aabbaa", 2, 2, trials);
    test("aaaaaaaaaaa", 0, 3, trials);
    return 0;
}


