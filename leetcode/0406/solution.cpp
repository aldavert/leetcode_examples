#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > reconstructQueue(std::vector<std::vector<int>> people)
{
    std::vector<std::vector<int> > result;
    auto cmp = [](const auto &a, const auto &b) -> bool
        { return (a[0] == b[0])?a[1] < b[1]:a[0] > b[0]; };
    std::sort(people.begin(), people.end(), cmp);
    for (const auto &p : people)
        result.insert(result.begin() + p[1], p);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > people,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = reconstructQueue(people);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{7, 0}, {4, 4}, {7, 1}, {5, 0}, {6, 1}, {5, 2}},
         {{5, 0}, {7, 0}, {5, 2}, {6, 1}, {4, 4}, {7, 1}}, trials);
    test({{6, 0}, {5, 0}, {4, 0}, {3, 2}, {2, 2}, {1, 4}},
         {{4, 0}, {5, 0}, {2, 2}, {3, 2}, {1, 4}, {6, 0}}, trials);
    return 0;
}


