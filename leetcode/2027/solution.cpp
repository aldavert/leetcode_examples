#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumMoves(std::string s)
{
    const size_t n = s.size();
    int result = 0;
    for (size_t i = 0; i < n; ++i)
    {
        if (s[i] == 'X')
        {
            i += 2;
            ++result;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumMoves(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("XXX", 1, trials);
    test("XXOX", 2, trials);
    test("OOOO", 0, trials);
    return 0;
}


