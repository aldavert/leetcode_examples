#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> stableMountains(std::vector<int> height, int threshold)
{
    std::vector<int> result;
    for (int i = 1, n = static_cast<int>(height.size()); i < n; ++i)
        if (height[i - 1] > threshold)
            result.push_back(i);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> height,
          int threshold,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = stableMountains(height, threshold);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 2, {3, 4}, trials);
    test({10, 1, 10, 1, 10}, 3, {1, 3}, trials);
    test({10, 1, 10, 1, 10}, 10, {}, trials);
    return 0;
}


