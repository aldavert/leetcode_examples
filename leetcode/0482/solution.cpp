#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string licenseKeyFormatting(std::string s, int k)
{
    std::string result;
    for (int i = static_cast<int>(s.size()) - 1, j = 0; i >= 0; --i)
    {
        if (s[i] == '-') continue;
        if (j == k) { result += '-'; j = 1; }
        else ++j;
        if ((s[i] >= 'a') && (s[i] <= 'z'))
            result += s[i] - 'a' + 'A';
        else result += s[i];
    }
    std::reverse(result.begin(), result.end());
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = licenseKeyFormatting(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("5F3Z-2e-9-w", 4, "5F3Z-2E9W", trials);
    test("2-5g-3-J", 2, "2-5G-3J", trials);
    return 0;
}


