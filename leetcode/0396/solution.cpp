#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxRotateFunction(std::vector<int> nums)
{
    int sum = 0, f = 0;
    for (size_t i = 0; i < nums.size(); ++i)
        sum += nums[i],
        f += static_cast<int>(i) * nums[i];
    int result = f;
    for (size_t i = 1; i < nums.size(); ++i)
    {
        f = f
          - (sum - nums[i - 1])
          + static_cast<int>(nums.size() - 1) * nums[i - 1];
        result = std::max(result, f);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxRotateFunction(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 3, 2, 6}, 26, trials);
    test({100}, 0, trials);
    return 0;
}


