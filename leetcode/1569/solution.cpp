#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int numOfWays(std::vector<int> nums)
{
    struct TreeNode
    {
        TreeNode(void) = default;
        TreeNode(int v) : value(v), left(nullptr), right(nullptr) {}
        int value = 0;
        TreeNode * left = nullptr;
        TreeNode * right = nullptr;
        ~TreeNode(void) { delete left; delete right; }
        void insert(int num)
        {
            if (num < value)
            {
                if (left) left->insert(num);
                else left = new TreeNode(num);
            }
            else
            {
                if (right) right->insert(num);
                else right = new TreeNode(num);
            }
        }
    };
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(nums.size());
    int factors[1001] = {};
    auto fastExponential = [](int base, int exponent)
    {
        int result = 1;
        while (exponent)
        {
            if (exponent % 2)
                result = static_cast<int>(static_cast<long>(result) * base % MOD);
            base = static_cast<int>(static_cast<long>(base) * base % MOD);
            exponent /= 2;
        }
        return result;
    };
    auto solve = [&](auto &&self, TreeNode * node) -> std::pair<int, int>
    {
        if (!node) return {1, 0};
        auto l = self(self, node->left), r = self(self, node->right);
        long result = static_cast<long>(l.first) * r.first % MOD;
        result = result * factors[l.second + r.second] % MOD;
        long base = static_cast<long>(factors[l.second]) * factors[r.second] % MOD;
        base = fastExponential(static_cast<int>(base), MOD - 2) % MOD;
        return { static_cast<int>(result * base % MOD), l.second + r.second + 1 };
    };
    
    TreeNode * head = new TreeNode(nums[0]);
    for (int i = 1; i < n; ++i) head->insert(nums[i]);
    factors[0] = 1;
    for (int i = 1; i < 1001; ++i)
        factors[i] = static_cast<int>(static_cast<long>(factors[i - 1]) * i % MOD);
    auto [result, _] = solve(solve, head);
    delete head;
    return result - 1;
}
#else
int numOfWays(std::vector<int> nums)
{
    constexpr int MOD = 1'000'000'007;
    const int n_rows = static_cast<int>(nums.size());
    std::vector<std::vector<int> > comb;
    for (int i = 0; i <= n_rows; ++i)
        comb.push_back(std::vector<int>(i + 1, 1));
    for (int i = 2; i <= n_rows; ++i)
        for (int j = 1, m = static_cast<int>(comb[i].size()); j < m - 1; ++j)
            comb[i][j] = (comb[i - 1][j - 1] + comb[i - 1][j]) % MOD;
    auto ways = [&](auto &&self, std::vector<int> &current) -> int
    {
        if (current.size() <= 2) return 1;
        std::vector<int> side[2];
        for (int i = 1, m = static_cast<int>(current.size()); i < m; ++i)
            side[(current[i] >= current[0])].push_back(current[i]);
        return static_cast<int>((((static_cast<long>(comb[current.size() - 1][side[0].size()])
                                 * self(self, side[0])) % MOD) * self(self, side[1])) % MOD);
    };
    return ways(ways, nums) - 1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numOfWays(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3}, 1, trials);
    test({3, 4, 5, 1, 2}, 5, trials);
    test({1, 2, 3}, 0, trials);
    return 0;
}


