#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<int> minInterval(std::vector<std::vector<int> > intervals,
                             std::vector<int> queries)
{
    struct Interval
    {
      int size;
      int right;
    };
    std::vector<int> result(queries.size(), -1);
    auto compare = [](const Interval& a, const Interval& b) { return a.size > b.size; };
    std::priority_queue<Interval,
                        std::vector<Interval>,
                        decltype(compare)> min_heap(compare);
    std::vector<std::vector<int> > qs;
    
    for (int i = 0, n = static_cast<int>(queries.size()); i < n; ++i)
        qs.push_back({queries[i], i});
    std::sort(intervals.begin(), intervals.end());
    std::sort(qs.begin(), qs.end());
    
    for (int i = 0, n = static_cast<int>(intervals.size()); const auto &q : qs)
    {
        while ((i < n) && (intervals[i][0] <= q[0]))
        {
            min_heap.push({intervals[i][1] - intervals[i][0] + 1, intervals[i][1]});
            ++i;
        }
        while (!min_heap.empty() && (min_heap.top().right < q[0]))
            min_heap.pop();
        if (!min_heap.empty())
            result[q[1]] = min_heap.top().size;
    }
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > intervals,
          std::vector<int> queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minInterval(intervals, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 4}, {2, 4}, {3, 6}, {4, 4}}, {2, 3, 4, 5}, {3, 3, 1, 4}, trials);
    test({{2, 3}, {2, 5}, {1, 8}, {20, 25}}, {2, 19, 5, 22}, {2, -1, 4, 6}, trials);
    return 0;
}


