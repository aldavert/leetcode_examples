#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string>
findHighAccessEmployees(std::vector<std::vector<std::string> > access_times)
{
    std::unordered_set<std::string> result;
    std::sort(access_times.begin(), access_times.end());
    for (int i = 0, n = static_cast<int>(access_times.size()); i + 2 < n; ++i)
    {
        const auto &name = access_times[i][0];
        if (result.contains(name)) continue;
        if (name != access_times[i + 2][0]) continue;
        if (std::stoi(access_times[i + 2][1]) - std::stoi(access_times[i][1]) < 100)
            result.insert(name);
    }
    return {result.begin(), result.end()};
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<std::string> left_copy = left, right_copy = right;
    std::sort(left_copy.begin(), left_copy.end());
    std::sort(right_copy.begin(), right_copy.end());
    for (size_t i = 0; i < left_copy.size(); ++i)
        if (left_copy[i] != right_copy[i])
            return false;
    return true;
}

void test(std::vector<std::vector<std::string> > access_times,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findHighAccessEmployees(access_times);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{"a", "0549"}, {"b", "0457"}, {"a", "0532"}, {"a", "0621"},
          {"b", "0540"}}, {"a"}, trials);
    test({{"d", "0002"}, {"c", "0808"}, {"c", "0829"}, {"e", "0215"}, {"d", "1508"},
          {"d", "1444"}, {"d", "1410"}, {"c", "0809"}}, {"c", "d"}, trials);
    test({{"cd", "1025"}, {"ab", "1025"}, {"cd", "1046"}, {"cd", "1055"},
          {"ab", "1124"}, {"ab", "1120"}}, {"ab", "cd"}, trials);
    return 0;
}


