#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int distinctPrimeFactors(std::vector<int> nums)
{
    std::unordered_set<int> primes;
    for (int num : nums)
    {
        for (int divisor = 2; divisor <= num; ++divisor)
        {
            if (num % divisor == 0)
            {
                primes.insert(divisor);
                while (num % divisor == 0) num /= divisor;
            }
        }
    }
    return static_cast<int>(primes.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = distinctPrimeFactors(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4, 3, 7, 10, 6}, 4, trials);
    test({2, 4, 8, 16}, 1, trials);
    return 0;
}


