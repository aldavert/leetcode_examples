#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canPartitionKSubsets(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    int sum = 0;
    for (int val : nums)
        sum += val;
    if ((n < k) || (sum % k)) return false;
    std::vector<bool> visited(nums.size(), false);
    auto backtrack = [&](auto &&self, int target, int curr_sum, int i, int l) -> bool
    {
        if (l == 0)  return true;
        if (curr_sum == target) return self(self, target, 0, 0, l - 1);
        for (int j = i; j < n; ++j)
        {
            if (visited[j] || (curr_sum + nums[j] > target)) continue;
            visited[j] = true;
            if (self(self, target, curr_sum + nums[j], j + 1, l)) return true;
            visited[j] = false;
        }
        return false;
    };
    return backtrack(backtrack, sum / k, 0, 0, k);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canPartitionKSubsets(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 3, 2, 3, 5, 2, 1}, 4, true, trials);
    test({1, 2, 3, 4}, 3, false, trials);
    return 0;
}


