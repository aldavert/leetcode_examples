# Odd Even Linked List

Given the `head` of a singly linked list, group all the nodes with odd indices together followed by the nodes with even indices, and return *the reordered list*.

The first node is considered **odd**, and the second node is **even**, and so on.

Note that the relative order inside both the even and odd groups should remain as it was in the input.

You must solve the problem in `O(1)` extra space complexity and `O(n)` time complexity.

 
#### Example 1:
> ```mermaid
> flowchart TD;
> subgraph SA [ ]
> direction LR;
> A1((1))-->B1((2))
> B1-->C1((3))
> C1-->D1((4))
> D1-->E1((5))
> end
> subgraph SB [ ]
> direction LR;
> A2((1))-->C2((3))
> C2-->E2((5))
> E2-->B2((2))
> B2-->D2((4))
> end
> SA-->SB
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef odd fill:#FA8;
> classDef white fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class B1,D1,B2,D2 odd;
> class SA,SB white;
> ```
> *Input:* `head = [1, 2, 3, 4, 5]`  
> *Output:* `[1, 3, 5, 2, 4]`

#### Example 2:
> ```mermaid
> flowchart TD;
> subgraph SA [ ]
> direction LR;
> A1((2))-->B1((1))
> B1-->C1((3))
> C1-->D1((5))
> D1-->E1((6))
> E1-->F1((4))
> F1-->G1((7))
> end
> subgraph SB [ ]
> direction LR;
> A2((2))-->C2((3))
> C2-->E2((6))
> E2-->G2((7))
> G2-->B2((1))
> B2-->D2((5))
> D2-->F2((4))
> end
> SA-->SB
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef odd fill:#FA8;
> classDef white fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class B1,D1,F1,B2,D2,F2 odd;
> class SA,SB white;
> ```
> *Input:* `head = [2, 1, 3, 5, 6, 4, 7]`  
> *Output:* `[2, 3, 6, 7, 1, 5, 4]`
 
#### Constraints:
- `n ==` number of nodes in the linked list
- `0 <= n <= 10^4`
- `-10^6 <= Node.val <= 10^6`


