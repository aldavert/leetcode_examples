#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

ListNode * oddEvenList(ListNode * head)
{
    if (!head) return nullptr;
    ListNode * even = head, * odd = head->next;
    if ((odd == nullptr) || (odd->next == nullptr)) return head;
    bool current = true;
    ListNode * ptr[2] = {odd, even};
    for (ListNode * p = odd->next; p != nullptr; p = p->next, current = !current)
        ptr[current] = ptr[current]->next = p;
    ptr[0]->next = nullptr;
    ptr[1]->next = odd;
    return even;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> input, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * linput = vec2list(input);
        linput = oddEvenList(linput);
        result = list2vec(linput);
        delete linput;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, {1, 3, 5, 2, 4}, trials);
    test({2, 1, 3, 5, 6, 4, 7}, {2, 3, 6, 7, 1, 5, 4}, trials);
    return 0;
}
