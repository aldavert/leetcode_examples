#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestIncreasingPath(std::vector<std::vector<int> > matrix)
{
    const int n = static_cast<int>(matrix.size()),
              m = static_cast<int>(matrix[0].size());
    int dp[201][201] = {}, result = 0;
    auto dfs = [&](auto &&sf, int y, int x) -> int
    {
        if (dp[y][x]) return dp[y][x];
        const int val = matrix[y][x];
        int r = 0;
        if ((y < n - 1) && (matrix[y + 1][x] < val)) r =             sf(sf, y + 1, x);
        if ((y >     0) && (matrix[y - 1][x] < val)) r = std::max(r, sf(sf, y - 1, x));
        if ((x < m - 1) && (matrix[y][x + 1] < val)) r = std::max(r, sf(sf, y, x + 1));
        if ((x >     0) && (matrix[y][x - 1] < val)) r = std::max(r, sf(sf, y, x - 1));
        return dp[y][x] = r + 1;
    };
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < m; ++j)
            result = std::max(result, dfs(dfs, i, j));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestIncreasingPath(matrix);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{9, 9, 4},
          {6, 6, 8},
          {2, 1, 1}}, 4, trials);
    test({{3, 4, 5},
          {3, 2, 6},
          {2, 2, 1}}, 4, trials);
    test({{1}}, 1, trials);
    test({{1, 2},
          {3, 4}}, 3, trials);
    test({{3, 4, 5},
          {3, 2, 6},
          {2, 2, 1}}, 4, trials);
    test({{1, 2, 3, 4},
          {2, 2, 3, 4},
          {3, 2, 3, 4},
          {4, 5, 6, 7}}, 7, trials);
    test({{9, 9},
          {6, 6},
          {2, 1}}, 4, trials);

    return 0;
}
