#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string removeTrailingZeros(std::string num)
{
    size_t zeros = 0;
    for (size_t i = num.size() - 1; (i > 0) && (num[i] == '0'); --i, ++zeros);
    return num.substr(0, num.size() - zeros);
}

// ############################################################################
// ############################################################################

void test(std::string num, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = removeTrailingZeros(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("51230100", "512301", trials);
    test("123", "123", trials);
    return 0;
}


