#include "../common/common.hpp"
#include <unordered_map>
#include <numeric>

// ############################################################################
// ############################################################################

int minSubarray(std::vector<int> nums, int p)
{
    int remainder = static_cast<int>(std::accumulate(nums.begin(), nums.end(), 0L) % p),
        n = static_cast<int>(nums.size());
    if (remainder == 0)
        return 0;
    
    std::unordered_map<int, int> prefix_to_index{{0, -1}};
    int result = n, prefix = 0;
    for (int i = 0; i < n; ++i)
    {
        prefix = (prefix + nums[i]) % p;
        if (auto it = prefix_to_index.find((prefix - remainder + p) % p);
            it != prefix_to_index.end())
            result = std::min(result, i - it->second);
        prefix_to_index[prefix] = i;
    }
    return (result == n)?-1:result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int p, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSubarray(nums, p);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1, 4, 2}, 6, 1, trials);
    test({6, 3, 5, 2}, 9, 2, trials);
    test({1, 2, 3}, 3, 0, trials);
    return 0;
}


