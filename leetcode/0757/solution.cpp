#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int intersectionSizeTwo(std::vector<std::vector<int> > intervals)
{
    int result = 0, max = -1, second_max = -1;
    auto compare = [](const auto& a, const auto& b)
    {
        return (a[1] == b[1])?(a[0] > b[0]):(a[1] < b[1]);
    };
    std::sort(intervals.begin(), intervals.end(), compare);
    for (const auto &interval : intervals)
    {
        const int a = interval[0];
        const int b = interval[1];
        if ((max >= a) && (second_max >= a)) continue;
        if (max >= a) second_max = max;
        else          second_max = b - 1, ++result;
        max = b;
        ++result;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > intervals, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = intersectionSizeTwo(intervals);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 3}, {3, 7}, {8, 9}}, 5, trials);
    test({{1, 3}, {1, 4}, {2, 5}, {3, 5}}, 3, trials);
    test({{1, 2}, {2, 3}, {2, 4}, {4, 5}}, 5, trials);
    return 0;
}


