#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countEven(int num)
{
    int digits_sum = num / 100 + (num / 10) % 10 + num % 10;
    return (num - digits_sum % 2) / 2 - (num == 1000);
}

// ############################################################################
// ############################################################################

void test(int num, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countEven(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 2, trials);
    test(30, 14, trials);
    test(1000, 499, trials);
    return 0;
}


