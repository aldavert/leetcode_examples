#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int subarraySum(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::unordered_map<int, int> previous;
    int result = 0;
    for (int i = 0, sum = 0; i < n; ++i)
    {
        sum += nums[i];
        if (sum == k) ++result;
        if (auto it = previous.find(sum - k); it != previous.end())
            result += it->second;
        ++previous[sum];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = subarraySum(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 1}, 2, 2, trials);
    test({1, 2, 3}, 3, 2, trials);
    test({1, 2, 3, 57}, 3, 2, trials);
    test({1}, 0, 0, trials);
    test({-1, -1, 1}, 0, 1, trials);
    return 0;
}


