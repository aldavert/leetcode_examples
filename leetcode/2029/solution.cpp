#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool stoneGameIX(std::vector<int> stones)
{
    int count[3] = {};
    
    for (int stone : stones)
        ++count[stone % 3];
    if (count[0] % 2 == 0)
        return std::min(count[1], count[2]) > 0;
    return std::abs(count[1] - count[2]) > 2;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> stones, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = stoneGameIX(stones);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1}, true, trials);
    test({2}, false, trials);
    test({5, 1, 2, 4, 3}, false, trials);
    return 0;
}


