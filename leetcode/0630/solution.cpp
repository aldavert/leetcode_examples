#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int scheduleCourse(std::vector<std::vector<int> > courses)
{
    struct Info
    {
        int duration = 0;
        int last_day = 0;
        bool operator<(const Info &other)
        {
            return last_day < other.last_day;
        }
    };
    std::vector<Info> courses_sorted;
    for (const auto &c : courses)
        courses_sorted.push_back({c[0], c[1]});
    std::sort(courses_sorted.begin(), courses_sorted.end());
    std::priority_queue<int> queue;
    int time = 0;
    for (auto [duration, last_day] : courses_sorted)
    {
        queue.push(duration);
        time += duration;
        if (time > last_day)
        {
            time -= queue.top();
            queue.pop();
        }
    }
    return static_cast<int>(queue.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > courses,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = scheduleCourse(courses);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{100, 200}, {200, 1300}, {1000, 1250}, {2000, 3200}}, 3, trials);
    test({{1, 2}}, 1, trials);
    test({{3, 2}, {4, 3}}, 0, trials);
    return 0;
}


