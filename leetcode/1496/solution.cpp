#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isPathCrossing(std::string path)
{
    std::unordered_set<long> visited({(100'000l << 30) | 100'000l});
    for (long x = 100'000l, y = 100'000l; char c : path)
    {
        x += (-(c == 'E')) + (c == 'W');
        y += (-(c == 'S')) + (c == 'N');
        if (long position = (x << 30) | y;
            visited.find(position) != visited.end())
            return true;
        else visited.insert(position);
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::string path, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPathCrossing(path);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("NES", false, trials);
    test("NESWW", true, trials);
    return 0;
}


