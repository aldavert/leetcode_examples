#include "../common/common.hpp"
#include <list>
#include <unordered_map>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class LFUCache
{
public:
    LFUCache(int capacity) : m_capacity(capacity), m_minimum(0) {}
    int get(int key)
    {
        if (auto it = m_nodes.find(key); it != m_nodes.end())
        {
            auto &node = it->second;
            touch(node);
            return node.value;
        }
        else return -1;
    }
    void put(int key, int value)
    {
        if (m_capacity == 0) return;
        if (auto it = m_nodes.find(key); it != m_nodes.end())
        {
            auto &node = it->second;
            node.value = value;
            touch(node);
        }
        else
        {
            if (static_cast<int>(m_nodes.size()) == m_capacity)
            {
                int key_remove = m_frequency_lut[m_minimum].back();
                m_frequency_lut[m_minimum].pop_back();
                m_nodes.erase(key_remove);
            }
            m_minimum = 1;
            m_frequency_lut[1].push_front(key);
            m_nodes[key] = {key, value, 1, m_frequency_lut[1].cbegin()};
        }
    }
private:
    struct Node
    {
        int key;
        int value;
        int freq;
        std::list<int>::const_iterator it;
    };
    void touch(Node &node)
    {
        const int frequency_prev = node.freq;
        const int frequency_new = ++node.freq;
        m_frequency_lut[frequency_prev].erase(node.it);
        if (m_frequency_lut[frequency_prev].empty())
        {
            m_frequency_lut.erase(frequency_prev);
            if (frequency_prev == m_minimum) ++m_minimum;
        }
        m_frequency_lut[frequency_new].push_front(node.key);
        node.it = m_frequency_lut[frequency_new].cbegin();
    }
    int m_capacity;
    int m_minimum;
    std::unordered_map<int, Node> m_nodes;
    std::unordered_map<int, std::list<int> > m_frequency_lut;
};

// ############################################################################
// ############################################################################

void test(unsigned int capacity,
          std::vector<char> operations,
          std::vector<std::vector<int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    const int n = static_cast<int>(operations.size());
    std::vector<int> result(n, null);
    for (unsigned int t = 0; t < trials; ++t)
    {
        LFUCache lfu(capacity);
        for (int i = 0; i < n; ++i)
        {
            if (operations[i] == 'p')
                lfu.put(input[i].front(), input[i].back());
            else if (operations[i] == 'g')
                result[i] = lfu.get(input[i].front());
            else
            {
                std::cout << "[ERROR] Unknown operation\n";
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {'p', 'p', 'g', 'p', 'g', 'g', 'p', 'g', 'g', 'g'},
         {{1, 1}, {2, 2}, {1}, {3, 3}, {2}, {3}, {4, 4}, {1}, {3}, {4}},
         {null, null, 1, null, -1, 3, null, -1, 3, 4}, trials);
    return 0;
}


