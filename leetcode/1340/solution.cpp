#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

int maxJumps(std::vector<int> arr, int d)
{
    const int n = static_cast<int>(arr.size());
    std::vector<int> dp_table(n, 1);
    std::stack<int> s;
    for (int i = 0; i <= n; ++i)
    {
        while (!s.empty() && ((i == n) || (arr[s.top()] < arr[i])))
        {
            std::vector<int> indices{s.top()};
            s.pop();
            while (!s.empty() && (arr[s.top()] == arr[indices[0]]))
                indices.push_back(s.top()),
                s.pop();
            for (int j : indices)
            {
                if ((i < n) && (i - j <= d))
                    dp_table[i] = std::max(dp_table[i], dp_table[j] + 1);
                if (!s.empty() && (j - s.top() <= d))
                    dp_table[s.top()] = std::max(dp_table[s.top()], dp_table[j] + 1);
            }
        }
        s.push(i);
    }
    int result = dp_table[0];
    for (int i = 1; i < n; ++i)
        result = std::max(result, dp_table[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int d, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxJumps(arr, d);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({6, 4, 14, 6, 8, 13, 9, 7, 10, 6, 12}, 2, 4, trials);
    test({3, 3, 3, 3, 3}, 3, 1, trials);
    test({7, 6, 5, 4, 3, 2, 1}, 1, 7, trials);
    return 0;
}


