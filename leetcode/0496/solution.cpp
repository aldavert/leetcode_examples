#include "../common/common.hpp"
#include <unordered_map>
#include <set>

// ############################################################################
// ############################################################################

std::vector<int> nextGreaterElement(std::vector<int> nums1, std::vector<int> nums2)
{
    const int n = static_cast<int>(nums1.size());
    std::vector<int> result(n, -1);
    std::set<int> previous;
    std::unordered_map<int, int> lut;
    for (int v : nums2)
    {
        previous.insert(v);
        auto end = previous.lower_bound(v);
        for (auto begin = previous.begin(); begin != end; ++begin)
            lut[*begin] = v;
        previous.erase(previous.begin(), end);
    }
    for (int i = 0; i < n; ++i)
        if (auto it = lut.find(nums1[i]); it != lut.end())
            result[i] = it->second;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = nextGreaterElement(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 1, 2}, {1, 3, 4, 2}, {-1, 3, -1}, trials);
    test({2, 4}, {1, 2, 3, 4}, {3, -1}, trials);
    return 0;
}


