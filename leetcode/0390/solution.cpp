#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 0
int lastRemaining(int n)
{
    return (n == 1)?1:2 * (n / 2 + 1 - lastRemaining(n / 2));
}
#elif 1
int lastRemaining(int n)
{
    int left = 1, right = n, size = n, gap = 1;
    bool rightwards = true;
    for (; left < right; rightwards = !rightwards, gap *= 2, size /= 2)
    {
        left  += gap * ( rightwards || (size & 1));
        right -= gap * (!rightwards || (size & 1));
    }
    return (rightwards)?left:right;
}
#else
int lastRemaining(int n)
{
    int left = 1, right = n, size = n, gap = 1;
    bool rightwards = true;
    for (; left < right; rightwards = !rightwards, gap *= 2, size /= 2)
    {
        if (rightwards)
        {
            left += gap;
            if (size & 1) right -= gap;
        }
        else
        {
            right -= gap;
            if (size & 1) left += gap;
        }
    }
    return (rightwards)?left:right;
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = lastRemaining(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(9, 6, trials);
    test(1, 1, trials);
    std::vector<std::tuple<int, int> > solutions = {
        { 1,  1}, { 2,  2}, { 3,  2}, { 4,  2}, { 5,  2}, { 6,  4}, { 7,  4},
        { 8,  6}, { 9,  6}, {10,  8}, {11,  8}, {12,  6}, {13,  6}, {14,  8},
        {15,  8}, {16,  6}, {17,  6}, {18,  8}, {19,  8}, {20,  6}, {21,  6},
        {22,  8}, {23,  8}, {24, 14}, {25, 14}, {26, 16}, {27, 16}, {28, 14},
        {29, 14}, {30, 16}, {31, 16}, {32, 22}, {33, 22}, {34, 24}, {35, 24},
        {36, 22}, {37, 22}, {38, 24}, {39, 24}, {40, 30}, {41, 30}, {42, 32},
        {43, 32}, {44, 30}, {45, 30} };
    for (auto [n, solution] : solutions)
        test(n, solution, trials);
    return 0;
}


