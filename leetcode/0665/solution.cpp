#include "../common/common.hpp"
#include <limits>
#include <utility>

// ############################################################################
// ############################################################################

#if 0
bool checkPossibility(std::vector<int> nums)
{
    bool modified = false;
    for (int i = 1; i < static_cast<int>(nums.size()); ++i)
    {
        if (nums[i] < nums[i - 1])
        {
            if (modified) return false;
            if ((i == 1) || (nums[i] >= nums[i - 2]))
                nums[i - 1] = nums[i];
            else nums[i] = nums[i - 1];
            modified = true;
        }
    }
    return true;
}
#else
bool checkPossibility(std::vector<int> nums)
{
    bool no_change = true;
    int n_1 = std::numeric_limits<int>::lowest();
    int n_2 = std::numeric_limits<int>::lowest();
    for (int n : nums)
    {
        if (n < n_1)
        {
            if (no_change)
            {
                if (n >= n_2)
                {
                    n_2 = n_1;
                    n_1 = n;
                }
                else n_2 = n_1;
                no_change = false;
            }
            else return false;
        }
        else n_2 = std::exchange(n_1, n);
    }
    return true;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkPossibility(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 3},  true, trials);
    test({4, 2, 1}, false, trials);
    test({3, 4, 2, 3}, false, trials);
    return 0;
}


