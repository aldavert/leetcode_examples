#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
long long incremovableSubarrayCount(std::vector<int> nums)
{
    long result = 0;
    int n = static_cast<int>(nums.size()), i = 0, j = n - 1;
    for (; (j > 0) && (nums[j] > nums[j - 1]); --j);
    for (; i < n; ++i)
    {
        j = std::max(j, i + 1);
        result += n - j + 1;
        for (; (j < n) && (nums[j] <= nums[i]); ++j);
        if ((i > 0) && (nums[i] <= nums[i - 1]))
            break;
    }
    return result;
}
#else
long long incremovableSubarrayCount(std::vector<int> nums)
{
    const long n = static_cast<int>(nums.size());
    long start_index = 0;
    for (long i = n - 2; i >= 0; --i)
    {
        if (nums[i] >= nums[i + 1])
        {
            start_index = i + 1;
            break;
        }
    }
    if (start_index == 0) return n * (n + 1) / 2;
    long long result = n - start_index + 1;
    for (long i = 0, j = start_index; i < start_index; ++i)
    {
        if ((i > 0) && (nums[i] <= nums[i - 1])) break;
        while ((j < n) && (nums[i] >= nums[j])) ++j;
        result += n - j + 1;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = incremovableSubarrayCount(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 10, trials);
    test({6, 5, 7, 8}, 7, trials);
    test({8, 7, 6, 6}, 3, trials);
    return 0;
}


