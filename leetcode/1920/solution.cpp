#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> buildArray(std::vector<int> nums)
{
    std::vector<int> result(nums.size());
    for (size_t i = 0, n = nums.size(); i < n; ++i)
        result[i] = nums[nums[i]];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = buildArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 2, 1, 5, 3, 4}, {0, 1, 2, 4, 5, 3}, trials);
    test({5, 0, 1, 2, 3, 4}, {4, 5, 0, 1, 2, 3}, trials);
    return 0;
}


