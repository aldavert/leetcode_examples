#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

#if 0
int minOperations(std::vector<int> nums, int k)
{
    int partial = 0;
    for (size_t i = 0; i < nums.size(); ++i) partial = partial xor nums[i];
    return __builtin_popcount(partial xor k);
}
#else
int minOperations(std::vector<int> nums, int k)
{
    return __builtin_popcount(std::accumulate(nums.begin(), nums.end(), 0,
                              std::bit_xor<>()) xor k);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3, 4}, 1, 2, trials);
    test({2, 0, 2, 0}, 0, 0, trials);
    return 0;
}


