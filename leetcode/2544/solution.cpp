#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int alternateDigitSum(int n)
{
    int result = 0;
    bool positive = true;
    for (; n; positive = !positive, n /= 10)
    {
        int digit = n % 10;
        result += (2 * positive - 1) * digit;
    }
    return (1 - 2 * positive) * result;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = alternateDigitSum(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, 1, trials);
    test(521, 4, trials);
    test(251, -2, trials);
    test(111, 1, trials);
    test(886996, 0, trials);
    return 0;
}


