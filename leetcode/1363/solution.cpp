#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string largestMultipleOfThree(std::vector<int> digits)
{
    const size_t n = digits.size();
    const int module_values[2][6] = {{1, 4, 7, 2, 5, 8}, {2, 5, 8, 1, 4, 7}};
    int accumulate = 0, histogram[10] = {};
    for (size_t i = 0; i < n; ++i)
        ++histogram[digits[i]],
        accumulate += digits[i];
    for (int residual = accumulate % 3; residual != 0; residual = accumulate % 3)
    {
        for (int i = 0; i < 6; ++i)
        {
            if (histogram[module_values[residual - 1][i]])
            {
                --histogram[module_values[residual - 1][i]];
                accumulate -= module_values[residual - 1][i];
                break;
            }
        }
    }
    std::string result;
    for (int i = 9; i >= 0; --i)
        result += std::string(histogram[i], static_cast<char>('0' + i));
    return (result.size() && (result[0] == '0'))?"0":result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> digits, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestMultipleOfThree(digits);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 1, 9}, "981", trials);
    test({8, 6, 7, 1, 0}, "8760", trials);
    test({1}, "", trials);
    test({0, 0, 0, 0, 0, 0}, "0", trials);
    test({0, 0, 1, 0, 0, 0}, "0", trials);
    return 0;
}


