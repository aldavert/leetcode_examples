#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

std::vector<int> closestDivisors(int num)
{
    for (int root = static_cast<int>(std::sqrt(num + 2)); root > 0; --root)
        for (int cand : {num + 1, num + 2})
            if (cand % root == 0)
                return {root, cand / root};
    return {};
}

// ############################################################################
// ############################################################################

void test(int num, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = closestDivisors(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(  8, { 3,  3}, trials);
    test(123, { 5, 25}, trials);
    test(999, {25, 40}, trials);
    return 0;
}


