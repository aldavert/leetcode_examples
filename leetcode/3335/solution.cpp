#include "../common/common.hpp"
#include <cstring>
#include <numeric>

// ############################################################################
// ############################################################################

int lengthAfterTransformations(std::string s, int t)
{
    constexpr int MOD = 1'000'000'007;
    int count[26] = {};
    
    for (const char c : s) ++count[c - 'a'];
    while (t-- > 0)
    {
        int next_count[26] = {};
        for (int i = 0; i < 25; ++i)
            next_count[i + 1] = count[i];
        next_count[0] = count[25];
        next_count[1] = (next_count[1] + count[25]) % MOD;
        std::memcpy(count, next_count, sizeof(count));
    }
    return static_cast<int>(std::accumulate(&count[0], &count[26], 0L) % MOD);
}

// ############################################################################
// ############################################################################

void test(std::string s, int t, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = lengthAfterTransformations(s, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcyy", 2, 7, trials);
    test("azbk", 1, 5, trials);
    return 0;
}


