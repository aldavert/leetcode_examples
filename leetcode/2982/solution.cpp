#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumLength(std::string s)
{
    const int n = static_cast<int>(s.size());
    auto calcMaxFreq = [&](const std::vector<int> &count, int max_freq) -> int
    {
        int times = 0;
        for (int freq = max_freq; freq >= 1; --freq)
        {
            times += count[freq];
            if (times >= 3)
                return freq;
        }
        return -1;
    };
    int result = -1, running_len = 0;
    std::vector<std::vector<int> > counts(26, std::vector<int>(n + 1));
    
    for (char prev_letter = '\n'; const char c : s)
    {
        if (c == prev_letter) ++counts[c - 'a'][++running_len];
        else
        {
            running_len = 1;
            ++counts[c - 'a'][running_len];
            prev_letter = c;
        }
    }
    for (const auto &count : counts)
        result = std::max(result, calcMaxFreq(count, n));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumLength(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aaaa", 2, trials);
    test("abcdef", -1, trials);
    test("abcaba", 1, trials);
    return 0;
}


