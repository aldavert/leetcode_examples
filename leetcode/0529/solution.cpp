#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<std::vector<char> > updateBoard(
        std::vector<std::vector<char> > board,
        std::vector<int> click)
{
    struct Position
    {
        int row = 0;
        int col = 0;
        Position operator+(const Position &other)
        {
            return {row + other.row, col + other.col};
        }
    };
    Position directions[] = {
        {-1, -1}, { 0, -1}, { 1, -1},
        {-1,  0},           { 1,  0},
        {-1,  1}, { 0,  1}, { 1,  1}};
    const int n_rows = static_cast<int>(board.size()),
              n_cols = static_cast<int>(board[0].size());
    auto inside = [&n_rows, &n_cols](Position p) -> bool
    {
        return (p.row >= 0)
            && (p.col >= 0)
            && (p.row <  n_rows)
            && (p.col <  n_cols);
    };
    auto result = board;
    int row = click[0], col = click[1];
    if ((row < 0) || (col < 0) || (row >= n_rows) || (col >= n_cols))
        return result;
    if (result[row][col] == 'M') result[row][col] = 'X';
    else if (result[row][col] == 'E')
    {
        std::queue<Position> q;
        q.push({row, col});
        while (!q.empty())
        {
            auto current = q.front();
            q.pop();
            if (!inside(current)
            ||  (result[current.row][current.col] != 'E')) continue;
            int n_mines = 0;
            for (int d = 0; d < 8; ++d)
            {
                auto next = current + directions[d];
                n_mines += (inside(next) && (result[next.row][next.col] == 'M'));
            }
            if (n_mines == 0)
            {
                result[current.row][current.col] = 'B';
                for (int d = 0; d < 8; ++d)
                    q.push(current + directions[d]);
            }
            else result[current.row][current.col] = static_cast<char>('0' + n_mines);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > board,
          std::vector<int> click,
          std::vector<std::vector<char> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<char> > result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result = updateBoard(board, click);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'E', 'E', 'E', 'E', 'E'},
          {'E', 'E', 'M', 'E', 'E'},
          {'E', 'E', 'E', 'E', 'E'},
          {'E', 'E', 'E', 'E', 'E'}}, {3, 0},
         {{'B', '1', 'E', '1', 'B'},
          {'B', '1', 'M', '1', 'B'},
          {'B', '1', '1', '1', 'B'},
          {'B', 'B', 'B', 'B', 'B'}}, trials);
    test({{'B', '1', 'E', '1', 'B'},
          {'B', '1', 'M', '1', 'B'},
          {'B', '1', '1', '1', 'B'},
          {'B', 'B', 'B', 'B', 'B'}}, {1, 2},
         {{'B', '1', 'E', '1', 'B'},
          {'B', '1', 'X', '1', 'B'},
          {'B', '1', '1', '1', 'B'},
          {'B', 'B', 'B', 'B', 'B'}}, trials);
    return 0;
}


