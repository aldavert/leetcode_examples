#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumJumps(std::vector<int> nums, int target)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> dp(n, -1);
    dp[0] = 0;
    for (int j = 1; j < n; ++j)
        for (int i = 0; i < j; ++i)
            if ((dp[i] != -1) && (std::abs(nums[j] - nums[i]) <= target))
                dp[j] = std::max(dp[j], dp[i] + 1);
    return dp[n - 1];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumJumps(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 6, 4, 1, 2}, 2, 3, trials);
    test({1, 3, 6, 4, 1, 2}, 3, 5, trials);
    test({1, 3, 6, 4, 1, 2}, 0, -1, trials);
    return 0;
}


