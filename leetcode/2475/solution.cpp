#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int unequalTriplets(std::vector<int> nums)
{
    const size_t n = nums.size();
    int offset[1002] = {}, histogram[1002] = {}, count = 0, result = 0;
    for (size_t i = 0; i < n; ++i)
    {
        if (!offset[nums[i]])
            offset[nums[i]] = ++count;
        ++histogram[offset[nums[i]]];
    }
    for (int i = 1, prev = 0, next = static_cast<int>(n); i <= count; ++i)
    {
        next -= histogram[i];
        result  += prev * histogram[i] * next;
        prev += histogram[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = unequalTriplets(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 4, 2, 4, 3}, 3, trials);
    test({1, 1, 1, 1, 1}, 0, trials);
    test({4, 4, 2, 4, 3, 5, 7}, 22, trials);
    test({4, 2, 3, 5, 7}, 10, trials);
    test({4, 4, 2, 4, 3, 2, 3}, 12, trials);
    return 0;
}


