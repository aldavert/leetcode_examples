#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int singleNumber(std::vector<int> nums)
{
    int histogram[32] = {};
    for (int number : nums)
        for (int i = 0; i < 32; ++i)
            histogram[i] += ((number >> i) & 1);
    int result = 0;
    for (int i = 0; i < 32; ++i)
        if (histogram[i] % 3) result = result | (1 << i);
    return result;
}
#else
int singleNumber(std::vector<int> nums)
{
    int once = 0;
    for (int twice = 0; int number : nums)
    {
        once = (~twice) & (once ^ number);
        twice = (~once) & (twice ^ number);
    }
    return once;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = singleNumber(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 2, 3, 2}, 3, trials);
    test({0, 1, 0, 1, 0, 1, 99}, 99, trials);
    test({0, 1, 0, 1, 0, 1, 2, 2, 2, 3}, 3, trials);
    return 0;
}


