#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

std::vector<int> answerQueries(std::vector<int> nums, std::vector<int> queries)
{
    std::vector<int> result;
    std::sort(nums.begin(), nums.end());
    std::inclusive_scan(nums.begin(), nums.end(), nums.begin());
    for (int query : queries)
    {
        auto search = std::upper_bound(nums.begin(), nums.end(), query);
        if      (search == nums.end())
            result.push_back(static_cast<int>(nums.size()));
        else if (search == nums.begin())
            result.push_back(0);
        else result.push_back(static_cast<int>(std::distance(nums.begin(), search)));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = answerQueries(nums, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 5, 2, 1}, {3, 10, 21}, {2, 3, 4}, trials);
    test({2, 3, 4, 5}, {1}, {0}, trials);
    return 0;
}


