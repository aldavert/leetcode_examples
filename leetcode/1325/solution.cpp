#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * removeLeafNodes(TreeNode * root, int target)
{
    if (root == nullptr) return nullptr;
    root->left = removeLeafNodes(root->left, target);
    root->right = removeLeafNodes(root->right, target);
    if ((root->val == target) && (root->left == nullptr) && (root->right == nullptr))
    {
        //delete root; // Leetcode crashes is you clean the memory ¯\_(ツ)_/¯.
        return nullptr;
    }
    else return root;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          int target,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        root = removeLeafNodes(root, target);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 2, null, 2, 4}, 2, {1, null, 3, null, 4}, trials);
    test({1, 3, 3, 3, 2}, 3, {1, 3, null, null, 2}, trials);
    test({1, 2, null, 2, null, 2}, 2, {1}, trials);
    test({1, 1, 1}, 1, {}, trials);
    return 0;
}


