# Delete Leaves with a Given Value

Given a binary tree `root` and an integer `target`, delete all the **leaf nodes** with value `target`.

Note that once you delete a leaf node with value `target`, if its parent node becomes a leaf node and has the value `target`, it should also be deleted (you need to continue doing that until you cannot).

#### Example 1:
> ```mermaid 
> flowchart LR;
> subgraph S3 ["Step 3"]
> direction TB
> A3((1))---EMP4(( ))
> A3---C3((3))
> C3---EMP3(( ))
> C3---F3((4))
> end
> subgraph S2 ["Step 2"]
> direction TB
> A2((1))---B2((2))
> A2---C2((3))
> C2---EMP2(( ))
> C2---F2((4))
> end
> subgraph S1 ["Step 1"]
> direction TB
> A1((1))---B1((2))
> A1---C1((3))
> B1---D1((2))
> B1---EMP1(( ))
> C1---E1((2))
> C1---F1((4))
> end
> S1-->S2-->S3
> linkStyle 0,2,6,11 stroke-width:0px;
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef selected fill:#FA0,stroke:#000,stroke-width:2px;
> class S1,S2,S3,EMP1,EMP2,EMP3,EMP4 empty;
> class B2,D1,E1 selected;
> ```
> *Input:* `root = [1, 2, 3, 2, null, 2, 4], target = 2`  
> *Output:* `[1, null, 3, null, 4]`  
> *Explanation:* Leaf nodes in green with value (`target = 2`) are removed (Picture in left). After removing, new nodes become leaf nodes with value (`target = 2`) (Picture in center).

#### Example 2:
> ```mermaid 
> flowchart LR;
> subgraph S2 ["Step 2"]
> direction TB
> A2((1))---B2((3))
> A2---EMP1(( ))
> B2---EMP2(( ))
> B2---D2((2))
> end
> subgraph S1 ["Step 1"]
> direction TB
> A1((1))---B1((3))
> A1---C1((3))
> B1---D1((3))
> B1---E1((2))
> end
> S1-->S2
> linkStyle 1,2 stroke-width:0px;
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef selected fill:#FA0,stroke:#000,stroke-width:2px;
> class S1,S2,EMP1,EMP2 empty;
> class D1,B1 selected;
> ```
> *Input:* `root = [1, 3, 3, 3, 2], target = 3`  
> *Output:* `[1, 3, null, null, 2]`

#### Example 3:
> ```mermaid 
> flowchart LR;
> subgraph S4 ["Step 4"]
> direction TB
> A4((1))
> end
> subgraph S3 ["Step 3"]
> direction TB
> A3((1))---B3((2))
> A3---EMP1(( ))
> linkStyle 1 stroke-width:0px;
> end
> subgraph S2 ["Step 2"]
> direction TB
> A2((1))---B2((2))
> A2---EMP2(( ))
> B2---C2((2))
> B2---EMP3(( ))
> linkStyle 1,3 stroke-width:0px;
> end
> subgraph S1 ["Step 1"]
> direction TB
> A1((1))---B1((2))
> A1---EMP4(( ))
> B1---C1((2))
> B1---EMP5(( ))
> C1---D1((2))
> C1---EMP6(( ))
> linkStyle 1,3,5,7,9,11 stroke-width:0px;
> end
> S1-->S2-->S3-->S4
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef selected fill:#FA0,stroke:#000,stroke-width:2px;
> class S1,S2,S3,S4,EMP1,EMP2,EMP3,EMP4,EMP5,EMP6 empty;
> class B3,C2,D1 selected;
> ```
> *Input:* `root = [1, 2, null, 2, null, 2], target = 2`  
> *Output:* `[1]`  
> *Explanation:* Leaf nodes in green with value (`target = 2`) are removed at each step.

#### Constraints:
- The number of nodes in the tree is in the range `[1, 3000]`.
- `1 <= Node.val, target <= 1000`


