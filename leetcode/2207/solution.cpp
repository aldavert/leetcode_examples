#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maximumSubsequenceCount(std::string text, std::string pattern)
{
    long result = 0, count[2] = {};
    
    for (const char c : text)
    {
        if (c == pattern[1])
        {
            result += count[0];
            ++count[1];
        }
        if (c == pattern[0])
            ++count[0];
    }
    return result + std::max(count[0], count[1]);
}

// ############################################################################
// ############################################################################

void test(std::string text,
          std::string pattern,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumSubsequenceCount(text, pattern);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abdcdbc", "ac", 4, trials);
    test("aabb", "ab", 6, trials);
    return 0;
}


