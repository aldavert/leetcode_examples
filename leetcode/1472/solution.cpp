#include "../common/common.hpp"

// ############################################################################
// ############################################################################

class BrowserHistory
{
    std::vector<std::string> memory;
    int position = -1;
public:
    BrowserHistory(std::string homepage) :
        memory({homepage}),
        position(0) {}
    void visit(std::string url)
    {
        while (position + 1 < static_cast<int>(memory.size()))
            memory.pop_back();
        memory.push_back(url);
        ++position;
    }
    std::string back(int steps)
    {
        position = std::max(position - steps, 0);
        return memory[position];
    }
    std::string forward(int steps)
    {
        position = std::min(position + steps, static_cast<int>(memory.size()) - 1);
        return memory[position];
    }
};

// ############################################################################
// ############################################################################

enum class OP { VISIT, BACK, FORWARD };

void test(std::string homepage,
          std::vector<OP> operations,
          std::vector<std::string> values,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        BrowserHistory bh(homepage);
        const size_t n = operations.size();
        for (size_t i = 0; i < n; ++i)
        {
            std::string output;
            switch (operations[i])
            {
            case OP::VISIT:
                bh.visit(values[i]);
                break;
            case OP::BACK:
                output = bh.back(std::stoi(values[i]));
                break;
            case OP::FORWARD:
                output = bh.forward(std::stoi(values[i]));
                break;
            default:
                std::cerr << "[ERROR] UNKNOWN/UNIMPLEMENTED OPERATION\n";
                return;
            };
            result.push_back(output);
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leetcode.com",
         {OP::VISIT, OP::VISIT, OP::VISIT, OP::BACK, OP::BACK, OP::FORWARD,
          OP::VISIT, OP::FORWARD, OP::BACK, OP::BACK},
         {"google.com", "facebook.com", "youtube.com", "1", "1", "1",
          "linkedin.com", "2", "2", "7"},
         {"", "", "", "facebook.com", "google.com", "facebook.com", "",
          "linkedin.com", "google.com", "leetcode.com"}, trials);
    return 0;
}


