#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> twoSum(std::vector<int> numbers, int target)
{
    int l = 0;
    int r = static_cast<int>(numbers.size()) - 1;
    while (numbers[l] + numbers[r] != target)
    {
        if (numbers[l] + numbers[r] < target) ++l;
        else --r;
    }
    return {l + 1, r + 1};
}

// ############################################################################
// ############################################################################

void test(std::vector<int> numbers,
          int target,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = twoSum(numbers, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 7, 11, 15}, 9, {1, 2}, trials);
    test({2, 3, 4}, 6, {1, 3}, trials);
    test({-1, 0}, -1, {1, 2}, trials);
    return 0;
}


