#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int numWaterBottles(int numBottles, int numExchange)
{
    return numBottles + (numBottles - 1) / (numExchange - 1);
}
#else
int numWaterBottles(int numBottles, int numExchange)
{
    int result = numBottles;
    while (numBottles >= numExchange)
    {
        result += numBottles / numExchange;
        numBottles = numBottles / numExchange + numBottles % numExchange;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int numBottles, int numExchange, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numWaterBottles(numBottles, numExchange);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(9, 3, 13, trials);
    test(15, 4, 19, trials);
    return 0;
}



