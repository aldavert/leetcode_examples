# Search in a Binary Search Tree

You are given the `root` of a binary search tree (BST) and an integer `val`.

Find the node in the BST that the node's value equals `val` and return the subtree rooted with that node. If such a node does not exist, return `null`.

#### Example 1:
> ```mermaid
> graph TD;
> A((4))---B((2))
> A---C((7))
> B---D((1))
> B---E((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#FDA,stroke:#000,stroke-width:2px;
> class B,D,E selected;
> ```
> *Input:* `root = [4, 2, 7, 1, 3], val = 2`  
> *Output:* `[2, 1, 3]`

#### Example 2:
> ```mermaid
> graph TD;
> A((4))---B((2))
> A---C((7))
> B---D((1))
> B---E((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [4, 2, 7, 1, 3], val = 5`  
> *Output:* `[]`
 
#### Constraints:
- The number of nodes in the tree is in the range `[1, 5000]`.
- `1 <= Node.val <= 10^7`
- `root` is a binary search tree.
- `1 <= val <= 10^7`


