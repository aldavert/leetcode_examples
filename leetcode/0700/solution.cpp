#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * searchBST(TreeNode * root, int val)
{
    if (!root) return root;
    else if (root->val == val) return root;
    else if (root->val > val) return searchBST(root->left, val);
    else return searchBST(root->right, val);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          int val,
          std::tuple<int, int, int> solution,
          unsigned int trials = 1)
{
    std::tuple<int, int, int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        TreeNode * search = searchBST(root, val);
        if (search == nullptr) result = { null, null, null };
        else result = { search->val,
                        (search->left)?search->left->val:null,
                        (search->right)?search->right->val:null };
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 7, 1, 3}, 2, {2, 1, 3}, trials);
    test({4, 2, 7, 1, 3}, 5, {null, null, null}, trials);
    return 0;
}


