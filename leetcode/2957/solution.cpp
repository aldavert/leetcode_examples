#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int removeAlmostEqualCharacters(std::string word)
{
    int result = 0;
    for (int i = 1, n = static_cast<int>(word.size()); i < n;)
    {
        if (std::abs(word[i] - word[i - 1]) <= 1)
        {
            ++result;
            i += 2;
        }
        else i += 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = removeAlmostEqualCharacters(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aaaaa", 2, trials);
    test("abddez", 2, trials);
    test("zyxyxyz", 3, trials);
    return 0;
}


