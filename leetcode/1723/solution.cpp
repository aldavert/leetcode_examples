#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int minimumTimeRequired(std::vector<int> jobs, int k)
{
    const int n = static_cast<int>(jobs.size());
    std::vector<int> count(k);
    int result = std::numeric_limits<int>::max();
    std::sort(jobs.rbegin(), jobs.rend());
    auto solve = [&](auto &&self, int index, int m) -> void
    {
        if (index >= n)
        {
            result = std::min(result, m);
            return;
        }
        if (m >= result) return;
        for (int i = 0; i < k; ++i)
        {
            if ((i > 0) && (count[i] == count[i - 1])) continue;
            count[i] += jobs[index];
            int t = m;
            m = std::max(m, count[i]);
            self(self, index + 1, m);
            m = t;
            count[i] -= jobs[index];
        }
    };
    solve(solve, 0, std::numeric_limits<int>::lowest());
    return result;
}
#else
int minimumTimeRequired(std::vector<int> jobs, int k)
{
    const int n = static_cast<int>(jobs.size());
    std::vector<int> count(k);
    int result = std::numeric_limits<int>::max();
    std::sort(jobs.begin(), jobs.end(), std::greater<int>());
    auto dfs = [&](auto &&self, int i) -> void
    {
        if (i == n)
        {
            result = std::min(result, *std::max_element(count.begin(), count.end()));
            return;
        }
        for (int j = 0; j < k; ++j)
        {
            if (count[j] + jobs[i] >= result) continue;
            count[j] += jobs[i];
            self(self, i + 1);
            count[j] -= jobs[i];
            if (count[j] == 0) break;
        }
    };
    dfs(dfs, 0);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> jobs, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumTimeRequired(jobs, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 3}, 3, 3, trials);
    test({1, 2, 4, 7, 8}, 2, 11, trials);
    return 0;
}


