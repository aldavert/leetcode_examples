#include "../common/common.hpp"
#include <unordered_map>
#include <queue>
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
long long minSumSquareDiff(std::vector<int> nums1,
                           std::vector<int> nums2,
                           int k1,
                           int k2)
{
    const int n = static_cast<int>(nums1.size());
    std::vector<int> diff(n);
    int m = -1;
    for (int i = 0; i < n; ++i)
    {
        diff[i] = std::abs(nums1[i] - nums2[i]);
        if (diff[i] > m)
            m = diff[i];
    }
    std::vector<int> f(m + 1);
    for (int i = 0; i < n; i++)
        ++f[diff[i]];
    for (int i = m, k = k1 + k2; i && k; --i)
    {
        if (f[i] <= k)
        {
            f[i - 1] += f[i];
            k -= f[i];
            f[i] = 0;
        }
        else
        {
            f[i] -= k;
            f[i - 1] += k;
            k = 0;
        }
    }
    long long result = 0;
    for (long long i = 0; i < m + 1; ++i)
        result += f[i] * i * i;
    return result;
}
#else
long long minSumSquareDiff(std::vector<int> nums1,
                           std::vector<int> nums2,
                           int k1,
                           int k2)
{
    std::vector<int> diff;
    for (size_t i = 0; i < nums1.size(); ++i)
        diff.push_back(std::abs(nums1[i] - nums2[i]));
    int k = k1 + k2;
    if (std::accumulate(diff.begin(), diff.end(), 0L) <= k)
        return 0;
    std::unordered_map<int, int> count;
    std::priority_queue<std::pair<int, int> > heap;
    
    for (int d : diff)
        if (d != 0)
            ++count[d];
    for (auto &[num, freq] : count)
        heap.emplace(num, freq);
    while (k > 0)
    {
        const auto [max_num, max_num_freq] = heap.top();
        heap.pop();
        const int num_decreased = std::min(k, max_num_freq);
        k -= num_decreased;
        if (max_num_freq > num_decreased)
        heap.emplace(max_num, max_num_freq - num_decreased);
        if (!heap.empty() && heap.top().first + 1 == max_num)
        {
            const auto [second_max_num, second_max_num_freq] = heap.top();
            heap.pop();
            heap.emplace(second_max_num, second_max_num_freq + num_decreased);
        }
        else if (max_num > 1)
            heap.emplace(max_num - 1, num_decreased);
    }
    long result = 0;
    while (!heap.empty())
    {
        auto [num, freq] = heap.top();
        heap.pop();
        result += static_cast<long>(num) * num * freq;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int k1,
          int k2,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSumSquareDiff(nums1, nums2, k1, k2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, {2, 10, 20, 19}, 0, 0, 579, trials);
    test({1, 4, 10, 12}, {5, 8, 6, 9}, 1, 1, 43, trials);
    return 0;
}


