#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int hardestWorker(int n, std::vector<std::vector<int> > logs)
{
    int result_time = 0, result_id = n;
    for (int time = 0; const auto &task : logs)
    {
        int task_time = task[1] - time;
        time = task[1];
        if (task_time > result_time)
            result_time = task_time,
            result_id = task[0];
        else if (task_time == result_time)
            result_id = std::min(result_id, task[0]);
    }
    return result_id;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > logs,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = hardestWorker(n, logs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, {{0, 3}, {2, 5}, {0, 9}, {1, 15}}, 1, trials);
    test(26, {{1, 1}, {3, 7}, {2, 12}, {7, 17}}, 3, trials);
    test(2, {{0, 10}, {1, 20}}, 0, trials);
    return 0;
}


