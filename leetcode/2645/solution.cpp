#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int addMinimum(std::string word)
{
    const std::string letters = "abc";
    int result = 0;
    for (int i = 0, n = static_cast<int>(word.size()); i < n; )
    {
        for (char c : letters)
        {
            if ((i < n) && (word[i] == c)) ++i;
            else ++result;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = addMinimum(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("b", 2, trials);
    test("aaa", 6, trials);
    test("abc", 0, trials);
    return 0;
}


