#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

#if 1
int calculateMinimumHP(std::vector<std::vector<int> > dungeon)
{
    const int m = static_cast<int>(dungeon.size()),
              n = static_cast<int>(dungeon[0].size());
    int health[201] = {};
    health[n - 1] = dungeon[m - 1][n - 1];
    for (int i = n - 2; i >= 0; --i)
        health[i] = dungeon[m - 1][i] + std::min(0, health[i + 1]);
    for (int i = m - 2; i >= 0; --i)
    {
        health[n - 1] = dungeon[i][n - 1] + std::min(0, health[n - 1]);
        for (int j = n - 2; j >= 0; --j)
            health[j] = dungeon[i][j] + std::min(0, std::max(health[j], health[j + 1]));
    }
    return 1 - std::min(0, health[0]);
}
#elif 0
int calculateMinimumHP(std::vector<std::vector<int> > dungeon)
{
    const int m = static_cast<int>(dungeon.size()),
              n = static_cast<int>(dungeon[0].size());
    int health[201][201] = {};
    health[m - 1][n - 1] = dungeon[m - 1][n - 1];
    for (int i = m - 2; i >= 0; --i)
        health[i][n - 1] = dungeon[i][n - 1] + std::min(0, health[i + 1][n - 1]);
    for (int i = n - 2; i >= 0; --i)
        health[m - 1][i] = dungeon[m - 1][i] + std::min(0, health[m - 1][i + 1]);
    for (int i = m - 2; i >= 0; --i)
        for (int j = n - 2; j >= 0; --j)
            health[i][j] = dungeon[i][j]
                         + std::min(0, std::max(health[i + 1][j], health[i][j + 1]));
    return 1 - std::min(0, health[0][0]);
}
#else
int calculateMinimumHP(std::vector<std::vector<int> > dungeon)
{
    const int m = static_cast<int>(dungeon.size()),
              n = static_cast<int>(dungeon[0].size());
    int result_health = std::numeric_limits<int>::lowest();
    auto cost = [&](auto &&self, int x, int y, int min_health, int health) -> int
    {
        health += dungeon[x][y];
        min_health = std::min(min_health, health);
        if ((x >= m - 1) && (y >= n - 1))
        {
            result_health = std::max(result_health, min_health);
            return min_health;
        }
        int result = min_health;
        if (x < m - 1)
            result = std::min(result, self(self, x + 1, y, min_health, health));
        if (y < n - 1)
            result = std::min(result, self(self, x, y + 1, min_health, health));
        return result;
    };
    cost(cost, 0, 0, 0, 0);
    return 1 - std::min(0, result_health);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > dungeon, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = calculateMinimumHP(dungeon);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{ -2, -3,  3},
          { -5,-10,  1},
          { 10, 30, -5}}, 7, trials);
    test({{ -2, -3,  3, -4},
          { -5,-10, -4,  1},
          { 10, 30,  5, -2}}, 7, trials);
    test({{ -2, -3,  3, -4},
          { -5,-10, -4,  1},
          { 10, 30,  0, -2}}, 8, trials);
    test({{ -2, -5, 10},
          { -3,-10, 30},
          {  3, -4,  0},
          { -4,  1, -2}}, 8, trials);
    test({{0}}, 1, trials);
    return 0;
}


