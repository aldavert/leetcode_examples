#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > generateMatrix(int n)
{
    const std::pair<int, int> direction[] = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
    const int n2 = n * n;
    std::vector<std::vector<int> > result(n);
    for (int i = 0; i < n; ++i)
        result[i].resize(n, 0);
    for (int i = 0, j = 0, count = 1, dir = 0; count <= n2; ++count)
    {
        result[i][j] = count;
        i += direction[dir].first;
        j += direction[dir].second;
        if ((i < 0) || (j < 0) || (i >= n) || (j >= n) || (result[i][j]))
        {
            i -= direction[dir].first;
            j -= direction[dir].second;
            dir = (dir + 1) % 4;
            i += direction[dir].first;
            j += direction[dir].second;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = generateMatrix(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{1, 2, 3}, {8, 9, 4}, {7, 6, 5}}, trials);
    test(1, {{1}}, trials);
    return 0;
}


