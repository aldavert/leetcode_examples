#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minOperations(std::vector<std::string> logs)
{
    int depth = 0;
    for (auto folder : logs)
    {
        if (folder == "../") depth = std::max(0, depth - 1);
        else if (folder == "./");
        else ++depth;
    }
    return depth;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> logs, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(logs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"d1/", "d2/", "../", "d21/", "./"}, 2, trials);
    test({"d1/", "d2/", "./", "d3/", "../", "d31/"}, 3, trials);
    test({"d1/", "../", "../", "../"}, 0, trials);
    return 0;
}


