#include "../common/common.hpp"

// ############################################################################
// ############################################################################

double calculateTax(std::vector<std::vector<int> > brackets, int income)
{
    const int n = static_cast<int>(brackets.size());
    double result = 0.0;
    for (int i = 0, lower = 0; (i < n) && (lower < income); lower = brackets[i++][0])
        result += static_cast<double>(std::min(income, brackets[i][0]) - lower)
               *  (static_cast<double>(brackets[i][1]) / 100.0);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > brackets,
          int income,
          double solution,
          unsigned int trials = 1)
{
    double result = -1.0;
    for (unsigned int i = 0; i < trials; ++i)
        result = calculateTax(brackets, income);
    showResult(std::abs(solution - result) < 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 50}, {7, 10}, {12, 25}}, 10, 2.65000, trials);
    test({{1, 0}, {4, 25}, {5, 50}}, 2, 0.25000, trials);
    test({{2, 50}}, 0, 0.0, trials);
    return 0;
}


