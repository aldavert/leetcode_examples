#include "../common/common.hpp"
#include <unordered_map>
#include <list>

// ############################################################################
// ############################################################################

#if 1
class AllOne
{
private:
    struct Node
    {
        int count;
        std::unordered_set<std::string> keys;
    };
    std::list<Node> m_elements;
    std::unordered_map<std::string, std::list<Node>::iterator> m_lut;
public:
    void inc(std::string key)
    {
        if (auto it = m_lut.find(key); it == cend(m_lut))
        {
            if (m_elements.empty() || (m_elements.front().count > 1))
                m_elements.push_front({1, {key}});
            else m_elements.front().keys.insert(key);
            m_lut[key] = m_elements.begin();
        }
        else
        {
            const auto it_curr = it->second;
            auto it_next = std::next(it_curr);
            if ((it_next == m_elements.end())
             || (it_next->count > it_curr->count + 1))
                it_next = m_elements.insert(it_next, {it_curr->count + 1, {key}});
            else it_next->keys.insert(key);
            it->second = it_next;
            it_curr->keys.erase(key);
            if (it_curr->keys.empty()) m_elements.erase(it_curr);
        }
    }
    void dec(std::string key)
    {
        if (auto it = m_lut.find(key); it != m_lut.end())
        {
            auto it_curr = it->second;
            if (it_curr->count == 1) m_lut.erase(it);
            else
            {
                auto it_prev = std::prev(it_curr);
                if ((it_curr == m_elements.begin())
                 || (it_prev->count < it_curr->count - 1))
                    it_prev = m_elements.insert(it_curr, {it_curr->count - 1, {key}});
                else it_prev->keys.insert(key);
                it->second = it_prev;
            }
            it_curr->keys.erase(key);
            if (it_curr->keys.empty()) m_elements.erase(it_curr);
        }
    }
    std::string getMaxKey(void)
    {
        return m_elements.empty()?"":*m_elements.back().keys.begin();
    }
    std::string getMinKey(void)
    {
        return m_elements.empty()?"":*m_elements.front().keys.begin();
    }
};
#else
class AllOne
{
    struct Node
    {
        int count;
        std::string key;
        bool operator<(const Node &other) const { return count < other.count; }
    };
    typedef std::list<Node> LIST;
    LIST m_words;
    std::unordered_map<std::string, LIST::iterator> m_iterators;
public:
    AllOne()
    {
    }
    void inc(std::string key)
    {
        if (auto it = m_iterators.find(key); it != m_iterators.end())
        {
            ++it->second->count;
            m_words.sort();
        }
        else
        {
            m_words.push_front({1, key});
            m_iterators[key] = m_words.begin();
        }
    }
    void dec(std::string key)
    {
        if (auto it = m_iterators.find(key); it != m_iterators.end())
        {
            --it->second->count;
            if (it->second->count == 0)
            {
                m_words.erase(it->second);
                m_iterators.erase(it);
            }
            else m_words.sort();
        }
    }
    std::string getMaxKey(void)
    {
        return (m_words.empty())?"":m_words.rbegin()->key;
    }
    std::string getMinKey(void)
    {
        return (m_words.empty())?"":m_words.begin()->key;
    }
};
#endif

// ############################################################################
// ############################################################################

void test(std::vector<char> operation,
          std::vector<std::string> input,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    const int n = static_cast<int>(operation.size());
    std::vector<std::string> result(n);
    for (unsigned int i = 0; i < trials; ++i)
    {
        AllOne allOne;
        for (int c = 0; c < n; ++c)
        {
            switch (operation[c])
            {
            case '+':
                allOne.inc(input[c]);
                break;
            case '-':
                allOne.dec(input[c]);
                break;
            case 'M':
                result[c] = allOne.getMaxKey();
                break;
            case 'm':
                result[c] = allOne.getMinKey();
                break;
            default:
                std::cerr << "[ERROR] Unknown operation.\n";
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({    '+',     '+',     'M',     'm',    '+',     'M',    'm'},
         {"hello", "hello",      "",      "", "leet",      "",     ""},
         {""     ,      "", "hello", "hello",     "", "hello", "leet"}, trials);
    test({    'M',     'm'},
         {     "",      ""},
         {     "",      ""}, trials);
    test({'+', '+', '+', '+', '+', '+', 'M', '+', '-', 'M', '-', '+', 'M',
          '+', '+', '-', '-', '-', '-', 'M', '+', '+', '+', '+', '+', '+',
          'M', 'm'},
         {"hello", "world", "leet", "code", "ds", "leet", "", "ds", "leet",
          "", "ds", "hello", "", "hello", "hello", "world", "leet", "code",
          "ds", "", "new", "new", "new", "new", "new", "new", "", ""},
         {"", "", "", "", "", "", "leet", "", "", "ds", "", "", "hello",
          "", "", "", "", "", "", "hello", "", "", "", "", "", "", "new",
          "hello"}, trials);
    return 0;
}


