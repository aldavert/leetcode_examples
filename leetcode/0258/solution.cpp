#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int addDigits(int num)
{
    if (num == 0) return 0;
    int result = 0;
    while (num > 0)
    {
        int aux = num / 10;
        result += num - aux * 10;
        num = aux;
    }
    result = result % 9;
    if (result == 0) result = 9;
    return result;
}

// ############################################################################
// ############################################################################

void test(int nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = addDigits(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(38, 2, trials);
    test(23'426, 8, trials);
    test(23'427, 9, trials);
    test(23'428, 1, trials);
    test(0, 0, trials);
    return 0;
}


