#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
bool isSolvable(std::vector<std::string> words, std::string result)
{
    const int n = static_cast<int>(words.size());
    int ptrs[5] = {}, m = 0;
    for(int i = 0; i < n; ++i)
        ptrs[i] = static_cast<int>(words[i].size()) - 1,
        m += static_cast<int>(words[i].size());
    std::unordered_map<char, int> c2i;
    std::unordered_map<int, char> i2c;
    
    auto check = [&](int i, char c)
    {
        auto sc = c2i.find(c);
        auto si = i2c.find(i);
        if ((sc != c2i.end()) && (si != i2c.end())
        &&  (sc->second == i) && (si->second == c))
            return 1;
        else return -1 + ((sc == c2i.end()) && (si == i2c.end()));
    };
    auto recursion = [&](auto &&self, int l_idx, int r_idx, int count, int sum) -> bool
    {
        if (l_idx == n)
        {
            const int d = sum % 10, carry = sum / 10;
            const int res = check(d, result[r_idx]);
            bool solved = false;
            if (res >= 0)
            {
                if (res == 0)
                    c2i[result[r_idx]] = d,
                    i2c[d] = result[r_idx];
                if (count == m)
                {
                    if      (carry == 0) solved = r_idx == 0;
                    else if (r_idx == 1) solved = check(carry, result[0]) >= 0;
                }
                else if (r_idx > 0)
                    solved = self(self, 0, r_idx - 1, count, carry);
                if (res == 0)
                {
                    c2i.erase(result[r_idx]);
                    i2c.erase(d);
                }
            }
            return solved;
        }
        if (ptrs[l_idx] < 0)
            return self(self, l_idx + 1, r_idx, count, sum);
        char c = words[l_idx][ptrs[l_idx]];
        bool solved = false;
        --ptrs[l_idx];
        if (auto sc = c2i.find(c); sc != c2i.end())
        {
            if ((words[l_idx].size() > 1) && (ptrs[l_idx] == -1) && (sc->second == 0))
                solved = false;
            else solved = self(self, l_idx + 1, r_idx, count + 1, sum + sc->second);
        }
        else
        {
            for (int i = 0; i <= 9 && !solved; ++i)
            {
                if (i2c.find(i) != i2c.end()) continue;
                if ((words[l_idx].size() > 1) && (ptrs[l_idx] == -1) && (i == 0))
                    continue;
                c2i[c] = i;
                i2c[i] = c;
                solved = self(self, l_idx + 1, r_idx, count + 1, sum + i);
                c2i.erase(c);
                i2c.erase(i);
            }
        }
        ++ptrs[l_idx];
        return solved;
    };
    return recursion(recursion, 0, static_cast<int>(result.size()) - 1, 0, 0);
}
#else
bool isSolvable(std::vector<std::string> words, std::string result)
{
    std::unordered_map<char, int> lut;
    auto work = words;
    bool used_digit[10] = {};
    work.push_back(result);
    int rows = static_cast<int>(work.size());
    int cols = 0;
    for (std::string word : work)
        cols = std::max(cols, static_cast<int>(word.size()));
    auto search = [&](auto &&self, int row, int col, int sum)
    {
        if (col == cols) return sum == 0;
        if (row == rows) return (sum % 10 == 0) && self(self, 0, col + 1, sum / 10);
        
        std::string word = work[row];
        const int n = static_cast<int>(word.size());
        if (col >= n)
            return self(self, row + 1, col, sum);
        char letter = word[n - col - 1];
        int sign = 1 - 2 * (row == rows - 1);
        
        if (auto it = lut.find(letter); (it != lut.end())
        && ((it->second > 0) || (col < n - 1)))
            return self(self, row + 1, col, sum + sign * it->second);

        for (int digit = 0; digit < 10; ++digit)
        if (!used_digit[digit] && ((digit > 0) || (col + 1 < n)))
        {
            lut[letter] = digit;
            used_digit[digit] = true;
            if (self(self, row + 1, col, sum + sign * digit))
                return true;
            used_digit[digit] = false;
            lut.erase(letter);
        }
        return false;
    };
    return search(search, 0, 0, 0);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::string param_result,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isSolvable(words, param_result);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"SEND", "MORE"}, "MONEY", true, trials);
    test({"SIX", "SEVEN", "SEVEN"}, "TWENTY", true, trials);
    test({"LEET", "CODE"}, "POINT", false, trials);
    test({"A", "B"}, "A", true, trials);
    return 0;
}


