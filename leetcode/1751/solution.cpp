#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxValue(std::vector<std::vector<int> > events, int k)
{
    const int n = static_cast<int>(events.size());
    std::vector<std::vector<int> > dp(n, std::vector<int>(k + 1, -1));
    std::sort(events.begin(), events.end());
    auto maxValue = [&](auto &&self, int i, int current_k) -> int
    {
        if ((current_k == 0) || (i == n)) return 0;
        if (dp[i][current_k] != -1) return dp[i][current_k];

        const auto it = std::upper_bound(events.begin() + i, events.end(), events[i][1],
                                    [](int end, const auto &a) { return a[0] > end; });
        int dist = static_cast<int>(std::distance(events.begin(), it));
        return dp[i][current_k] = std::max(events[i][2] + self(self, dist, current_k - 1),
                                           self(self, i + 1, current_k));
    };
    return maxValue(maxValue, 0, k);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > events,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxValue(events, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 4}, {3, 4, 3}, {2, 3, 1}}, 2, 7, trials);
    test({{1, 2, 4}, {3, 4, 3}, {2, 3, 10}}, 2, 10, trials);
    test({{1, 1, 1}, {2, 2, 2}, {3, 3, 3}, {4, 4, 4}}, 3, 9, trials);
    return 0;
}



