#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int findKthLargest(std::vector<int> nums, int k)
{
    std::priority_queue<int, std::vector<int>, std::greater<int> > queue;
    for (int n : nums)
    {
        if (static_cast<int>(queue.size()) < k)
            queue.push(n);
        else if (queue.top() < n)
        {
            queue.pop();
            queue.push(n);
        }
    }
    return queue.top();
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findKthLargest(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 1, 5, 6, 4}, 2, 5, trials);
    test({3, 2, 3, 1, 2, 4, 5, 5, 6}, 4, 4, trials);
    return 0;
}


