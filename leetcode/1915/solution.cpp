#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long wonderfulSubstrings(std::string word)
{
    long long result = 0;
    int prefix = 0, count[1024] = {};
    count[0] = 1;
    for (char c : word)
    {
        prefix ^= 1 << (c - 'a');
        result += count[prefix];
        for (int i = 0; i < 10; ++i)
            result += count[prefix ^ 1 << i];
        ++count[prefix];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = wonderfulSubstrings(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aba", 4, trials);
    test("aabb", 9, trials);
    test("he", 2, trials);
    return 0;
}


