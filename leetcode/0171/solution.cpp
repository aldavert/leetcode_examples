#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int titleToNumber(std::string columnTitle)
{
    const int n = static_cast<int>(columnTitle.size());
    int result = 0;
    for (int i = 0; i < n; ++i)
        result = result * static_cast<int>('Z' - 'A' + 1) + (columnTitle[i] - 'A' + 1);
    return result;
}

// ############################################################################
// ############################################################################


void test(std::string columnTitle, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = titleToNumber(columnTitle);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("A", 1, trials);
    test("AB", 28, trials);
    test("ZY", 701, trials);
    test("BZY", 2053, trials);
    test("FXSHRXW", 2147483647, trials);
    return 0;
}


