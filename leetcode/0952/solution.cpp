#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int largestComponentSize(std::vector<int> nums)
{
    constexpr int NR = 100'001;
    int high = nums[0];
    std::vector<bool> contains(NR, false);
    std::vector<int> visited(NR, false), node_parent(NR), node_size(NR, 1);
    int result = 1;
    for (int i = 0; i < NR; ++i)
        node_parent[i] = i;
    for (int i : nums)
    {
        contains[i] = true;
        high = std::max(high, i);
    }
    auto parent = [&](auto &&self, int x) -> int
    {
        if (node_parent[x] == x) return x;
        return node_parent[x] = self(self, node_parent[x]);
    };
    auto merge = [&](int x, int y) -> void
    {
        int parX = parent(parent, x);
        int parY = parent(parent, y);
        if (parX == parY) return;
        if (node_size[parX] >= node_size[parY])
            std::swap(parX, parY);
        node_size[node_parent[parX] = parY] += node_size[parX];
        result = std::max(result, node_size[parY]);
    };
    
    for (int i = 2; 2 * i <= high; ++i)
    {
        if (!visited[i])
        {
            int last = -1;
            for (int j = i; j <= high; j += i)
            {
                visited[j] = true;
                if (last == -1)
                {
                    if (contains[j])
                        last = j;
                }
                else if (contains[j]) merge(last, j);
            }
        }
    }
    return result;
}
#else
int largestComponentSize(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> node_parent(n, -1), node_rank(n, 1);
    std::unordered_map<int, int> m;
    int ret = 0;
    
    auto parent = [&](auto &&self, int x) -> int
    {
        if (node_parent[x] == -1) return x;
        return node_parent[x] = self(self, node_parent[x]);
    };
    auto merge = [&](int x, int y) -> void
    {
        int parX = parent(parent, x);
        int parY = parent(parent, y);
        if (parX == parY) return;
        if (node_rank[parX] >= node_rank[parY])
            node_rank[node_parent[parY] = parX] += node_rank[parY];
        else
            node_rank[node_parent[parX] = parY] += node_rank[parX];
    };
    
    for (int i = 0; i < n; ++i)
    {
        int x = nums[i];
        for (int j = 2; j * j <= x; ++j)
        {
            if (x % j == 0)
            {
                if (auto it = m.find(j); it != m.end())
                    merge(it->second, i);
                else m[j] = i;
                if (auto it = m.find(x / j); it != m.end())
                    merge(it->second, i);
                else m[x / j] = i;
            }
        }
        if (auto it = m.find(x); it != m.end())
            merge(it->second, i);
        else m[x] = i;
        ret = std::max(ret, node_rank[parent(parent, i)]);
    }
    
    return ret;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestComponentSize(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 6, 15, 35}, 4, trials);
    test({20, 50, 9, 63}, 2, trials);
    test({2, 3, 6, 7, 4, 12, 21, 39}, 8, trials);
    return 0;
}


