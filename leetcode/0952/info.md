=Largest Component Size by Common Factor=


You are given an integer array of unique positive integers `nums`. Consider the following graph:

- There are `nums.length` nodes, labeled `nums[0]` to `nums[nums.length - 1]`,
- There is an undirected edge between `nums[i]` and `nums[j]` if `nums[i]` and `nums[j]` share a common factor greater than 1.

Return the size of the largest connected component in the graph.

 
#### Example 1:
> ```mermaid
> graph LR;
> A((4))---B((6))
> B---C((15))
> C---D((35))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `nums = [4, 6, 15, 35]`  
> *Output:* `4`

#### Example 2:
> ```mermaid
> graph LR;
> A((20))---B((50))
> C((9))---D((63))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `nums = [20, 50, 9, 63]`  
> *Output:* `2`

**Example 3:**
> ```mermaid
> graph LR
> A((2))---B((6))
> A---C((4))
> A---D((12))
> B---C
> B---D
> B---E((21))
> B---F((3))
> B---G((39))
> C---D
> D---F
> D---E
> D---G
> E---H((7))
> E---G
> F---E
> F---G
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `nums = [2, 3, 6, 7, 4, 12, 21, 39]`  
> *Output:* `8`
 
#### Constraints:
- `1 <= nums.length <= 2 * 10^4`
- `1 <= nums[i] <= 10^5`
- All the values of `nums` are **unique**.


