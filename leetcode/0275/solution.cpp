#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int hIndex(std::vector<int> citations)
{
    if (citations.back() == 0) return 0;
    const int n = static_cast<int>(citations.size());
    int left = 0, right = n - 1;
    while (left < right)
    {
        int mid = (left + right) / 2;
        if (citations[mid] < n - mid) left = mid + 1;
        else right = mid;
    }
    return n - left;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> citations, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = hIndex(citations);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 3, 5, 6}, 3, trials);
    test({1, 2, 100}, 2, trials);
    test({1, 1, 3}, 1, trials);
    test({100}, 1, trials);
    test({0}, 0, trials);
    test({100, 102, 105}, 3, trials);
    test({0, 0, 4, 4}, 2, trials);
    test({3, 4, 5, 8, 10}, 4, trials);
    test({3, 3, 5, 8, 25}, 3, trials);
    return 0;
}


