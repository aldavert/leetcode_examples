#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int minSumOfLengths(std::vector<int> arr, int target)
{
    int result = std::numeric_limits<int>::max(), sum = 0;
    std::vector<int> best(arr.size(), std::numeric_limits<int>::max());
    
    for (int l = 0, r = 0; r < static_cast<int>(arr.size()); ++r)
    {
        sum += arr[r];
        while (sum > target)
            sum -= arr[l++];
        if (sum == target)
        {
            if ((l > 0) && (best[l - 1] != std::numeric_limits<int>::max()))
            result = std::min(result, best[l - 1] + r - l + 1);
            best[r] = std::min(best[r], r - l + 1);
        }
        if (r > 0)
            best[r] = std::min(best[r], best[r - 1]);
    }
    return (result == std::numeric_limits<int>::max())?-1:result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSumOfLengths(arr, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 2, 4, 3}, 3, 2, trials);
    test({7, 3, 4, 7}, 7, 2, trials);
    test({4, 3, 2, 6, 2, 3, 4}, 6, -1, trials);
    return 0;
}


