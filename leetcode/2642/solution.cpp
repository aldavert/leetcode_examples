#include "../common/common.hpp"
#include <queue>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class Graph
{
    struct Edge
    {
        int node = -1;
        int distance = -1;
        bool operator<(const Edge &other) const
        {
            return distance > other.distance
                || ((distance == other.distance) && (node > other. node));
        }
    };
    std::vector<std::vector<Edge> > m_graph;
public:
    Graph(int n, std::vector<std::vector<int> > edges) :
        m_graph(n)
    {
        for (const auto &edge : edges)
            m_graph[edge[0]].push_back({edge[1], edge[2]});
    }
    inline void addEdge(std::vector<int> edge)
    {
        m_graph[edge[0]].push_back({edge[1], edge[2]});
    }
    int shortestPath(int node1, int node2)
    {
        std::vector<int> distance(m_graph.size(), std::numeric_limits<int>::max());
        std::priority_queue<Edge> heap;

        distance[node1] = 0;
        heap.push({node1, 0});
        while (!heap.empty())
        {
            auto [u, d] = heap.top();
            heap.pop();
            if (u == node2) return d;
            for (auto [v, w] : m_graph[u])
            if (d + w < distance[v])
            {
                distance[v] = d + w;
                heap.push({v, distance[v]});
            }
        }
        return -1;
    }
};

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<bool> add_edge,
          std::vector<std::vector<int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        Graph graph(n, edges);
        result.clear();
        for (size_t i = 0; i < add_edge.size(); ++i)
        {
            if (add_edge[i])
            {
                graph.addEdge(input[i]);
                result.push_back(null);
            }
            else result.push_back(graph.shortestPath(input[i][0], input[i][1]));
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{0, 2, 5}, {0, 1, 2}, {1, 2, 1}, {3, 0, 3}},
         {false, false, true, false},
         {{3, 2}, {0, 3}, {1, 3, 4}, {0, 3}},
         {6, -1, null, 6}, trials);
    return 0;
}
