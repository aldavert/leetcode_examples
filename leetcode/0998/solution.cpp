#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * insertIntoMaxTree(TreeNode * root, int val)
{
    if (root == nullptr) return new TreeNode(val);
    if (root->val < val)
        return new TreeNode(val, root, nullptr);
    root->right = insertIntoMaxTree(root->right, val);
    return root;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          int val,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        root = insertIntoMaxTree(root, val);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 1, 3, null, null, 2}, 5, {5, 4, null, 1, 3, null, null, 2}, trials);
    test({5, 2, 4, null, 1}, 3, {5, 2, 4, null, 1, null, 3}, trials);
    test({5, 2, 3, null, 1}, 4, {5, 2, 4, null, 1, 3}, trials);
    return 0;
}


