# Maximum Binary Tree II

A **maximum tree** is a tree where every node has a value greater than any other value in its subtree.

You are given the `root` of a maximum binary tree and an integer `val`.

Just as in problem 654, the given tree was constructed from a list `a` (`root = Construct(a)`) recursively with the following Construct(a) routine:
- If `a` is empty, return `null`.
- Otherwise, let `a[i]` be the largest element of `a`. Create a `root` node with the value `a[i]`.
- The left child of `root` will be `Construct([a[0], a[1], ..., a[i - 1]])`.
- The right child of `root` will be `Construct([a[i + 1], a[i + 2], ..., a[a.length - 1]])`.
- Return `root`.

Note that we were not given `a` directly, only a root node `root = Construct(a)`.

Suppose `b` is a copy of `a` with the value `val` appended to it. It is guaranteed that `b` has unique values.

Return `Construct(b)`.

#### Example 1:
> ```mermaid 
> graph TD;
> A1((4))---B1((1))
> A1---C1((3))
> C1---D1((2))
> C1---EMPTY1(( ))
> A2((5))---B2((4))
> A2---EMPTY2(( ))
> B2---C2((1))
> B2---D2((3))
> D2---E2((2))
> D2---EMPTY3(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class EMPTY1,EMPTY2,EMPTY3 empty;
> linkStyle 3,5,9 stroke-width:0px
> ```
> *Input:* `root = [4, 1, 3, null, null, 2], val = 5`  
> *Output:* `[5, 4, null, 1, 3, null, null, 2]`  
> *Explanation:* `a = [1, 4, 2, 3], b = [1, 4, 2, 3, 5]`

#### Example 2:
> ```mermaid 
> graph TD;
> A1((5))---B1((2))
> A1---C1((4))
> B1---EMPTY1(( ))
> B1---D1((1))
> A2((5))---B2((2))
> A2---C2((4))
> B2---EMPTY2(( ))
> B2---D2((1))
> C2---EMPTY3(( ))
> C2---E2((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class EMPTY1,EMPTY2,EMPTY3 empty;
> linkStyle 2,6,8 stroke-width:0px
> ```
> *Input:* `root = [5, 2, 4, null, 1], val = 3`  
> *Output:* `[5, 2, 4, null, 1, null, 3]`  
> *Explanation:* `a = [2, 1, 5, 4], b = [2, 1, 5, 4, 3]`

#### Example 3:
> ```mermaid 
> graph TD;
> A1((5))---B1((2))
> A1---C1((3))
> B1---EMPTY1(( ))
> B1---D1((1))
> A2((5))---B2((2))
> A2---C2((4))
> B2---EMPTY2(( ))
> B2---D2((1))
> C2---E2((3))
> C2---EMPTY3(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class EMPTY1,EMPTY2,EMPTY3 empty;
> linkStyle 2,6,9 stroke-width:0px
> ```
> *Input:* `root = [5, 2, 3, null, 1], val = 4`  
> *Output:* `[5, 2, 4, null, 1, 3]`  
> *Explanation:* `a = [2, 1, 5, 3], b = [2, 1, 5, 3, 4]`

#### Constraints:
- The number of nodes in the tree is in the range `[1, 100]`.
- `1 <= Node.val <= 100`
- All the values of the tree are **unique**.
- `1 <= val <= 100`


