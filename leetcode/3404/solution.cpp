#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

long long numberOfSubsequences(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size()),
              max_value = *std::max_element(nums.begin(), nums.end());
    long long result = 0;
    std::vector<std::vector<int> > count(max_value + 1, std::vector<int>(max_value + 1));
    
    for (int r = 4; r <= n - 1 - 2; ++r)
    {
        int q = r - 2;
        for (int p = 0; p <= q - 2; ++p)
        {
            int _gcd = std::gcd(nums[p], nums[q]);
            ++count[nums[p] / _gcd][nums[q] / _gcd];
        }
        for (int s = r + 2; s < n; ++s)
        {
            int _gcd = std::gcd(nums[s], nums[r]);
            result += count[nums[s] / _gcd][nums[r] / _gcd];
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfSubsequences(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 3, 6, 1}, 1, trials);
    test({3, 4, 3, 4, 3, 4, 3, 4}, 3, trials);
    return 0;
}


