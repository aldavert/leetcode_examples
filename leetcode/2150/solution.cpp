#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> findLonely(std::vector<int> nums)
{
    std::unordered_map<int, int> count;
    std::vector<int> result;
    for (int num : nums) ++count[num];
    for (auto& [num, freq] : count)
        if ((freq == 1) && !count.contains(num - 1) && !count.contains(num + 1))
            result.push_back(num);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<int> left_work(left), right_work(right);
    std::sort(left_work.begin(), left_work.end());
    std::sort(right_work.begin(), right_work.end());
    for (size_t i = 0; i < left.size(); ++i)
        if (left_work[i] != right_work[i]) return false;
    return true;
}

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLonely(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 6, 5, 8}, {10, 8}, trials);
    test({1, 3, 5, 3}, {1, 5}, trials);
    return 0;
}


