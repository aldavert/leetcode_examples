#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int maxLength(std::vector<std::string> arr)
{
    const int N = static_cast<int>(arr.size());
    int n = 0;
    int arr_bits[17], arr_idx[17];
    for (int i = 0; i < N; ++i)
    {
        arr_idx[n] = i;
        arr_bits[n] = 0;
        bool valid = true;
        for (char c : arr[i])
        {
            int bit = 1 << (c - 'a');
            if ((arr_bits[n] & bit) == 0)
                arr_bits[n] = arr_bits[n] | bit;
            else
            {
                valid = false;
                break;
            }
        }
        if (valid) ++n;
    }
    std::unordered_map<long, int> memo;
    auto dp = [&](auto &&self, int mask, int idx) -> int
    {
        int length = 0;
        const long call_id = static_cast<long>(mask) << 32 | static_cast<long>(idx);
        if (auto it = memo.find(call_id); it != memo.end()) return it->second;
        for (int i = idx; i < n; ++i)
            if ((mask & arr_bits[i]) == 0)
                length = std::max<int>(length, static_cast<int>(arr[arr_idx[i]].size())
                                             + self(self, mask | arr_bits[i], idx + 1));
        return memo[call_id] = length;
    };
    return dp(dp, 0, 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxLength(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"un", "iq", "ue"}, 4, trials);
    test({"cha", "r", "act", "ers"}, 6, trials);
    test({"abcdefghijklmnopqrstuvwxyz"}, 26, trials);
    test({"yy", "bkhwmpbiisbldzknpm"}, 0, trials);
    test({"zog","nvwsuikgndmfexxgjtkb","nxko"}, 4, trials);
    return 0;
}


