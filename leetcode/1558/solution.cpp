#include "../common/common.hpp"
#include <numeric>
#include <cmath>

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> nums)
{
    auto op = [](int subtotal, unsigned num) { return subtotal + std::popcount(num); };
    const int max_num = *std::max_element(nums.begin(), nums.end());
    return std::accumulate(nums.begin(), nums.end(), 0, op)
         + ((max_num == 0)?0:static_cast<int>(std::log2(max_num)));
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 5}, 5, trials);
    test({2, 2}, 3, trials);
    test({4, 2, 5}, 6, trials);
}


