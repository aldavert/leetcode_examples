#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int subsetXORSum(std::vector<int> nums)
{
    const size_t n = nums.size();
    int result = 0;
    auto recursive_subsets = [&](auto &&self, int value, size_t index) -> void
    {
        if (index >= n)
            result += value;
        else
        {
            self(self, value, index + 1);
            self(self, value^nums[index], index + 1);
        }
    };
    recursive_subsets(recursive_subsets, 0, 0);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = subsetXORSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3}, 6, trials);
    test({5, 1, 6}, 28, trials);
    test({3, 4, 5, 6, 7, 8}, 480, trials);
    return 0;
}


