#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<int> maxSubsequence(std::vector<int> nums, int k)
{
    std::priority_queue<std::tuple<int, int>,
                        std::vector<std::tuple<int, int> >,
                        std::greater<std::tuple<int, int> > > q;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        if (static_cast<int>(q.size()) == k)
        {
            if (std::get<0>(q.top()) < nums[i])
            {
                q.pop();
                q.push({nums[i], i});
            }
        }
        else q.push({nums[i], i});
    }
    std::vector<std::tuple<int, int> > selection;
    for (; !q.empty(); q.pop())
        selection.push_back({std::get<1>(q.top()), std::get<0>(q.top())});
    std::sort(selection.begin(), selection.end());
    std::vector<int> result;
    for (auto [index, value] : selection)
        result.push_back(value);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSubsequence(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3, 3}, 2, {3, 3}, trials);
    test({-1, -2, 3, 4}, 3, {-1, 3, 4}, trials);
    test({3, 4, 3, 3}, 2, {3, 4}, trials);
    return 0;
}


