#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> getMaximumXor(std::vector<int> nums, int maximumBit)
{
    const int max = (1 << maximumBit) - 1;
    std::vector<int> result;
    int xors = 0;
    
    for (int num : nums)
    {
        xors ^= num;
        result.push_back(xors ^ max);
    }
    std::reverse(result.begin(), result.end());
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int maximumBit,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getMaximumXor(nums, maximumBit);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 1, 3}, 2, {0, 3, 2, 3}, trials);
    test({2, 3, 4, 7}, 3, {5, 2, 6, 5}, trials);
    test({0, 1, 2, 2, 5, 7}, 3, {4, 3, 6, 4, 6, 7}, trials);
    return 0;
}


