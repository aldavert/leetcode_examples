#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minCapability(std::vector<int> nums, int k)
{
    int l = nums[0], r = nums[0];
    for (size_t i = 1; i < nums.size(); ++i)
        l = std::min(l, nums[i]),
        r = std::max(r, nums[i]);
    while (l < r)
    {
        const int m = (l + r) / 2;
        int stolen_houses = 0;
        for (size_t i = 0; i < nums.size(); ++i)
        {
            if (nums[i] <= m)
            {
                ++stolen_houses;
                ++i;
            }
        }
        if (stolen_houses >= k) r = m;
        else l = m + 1;
    }
    return l;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCapability(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 5, 9}, 2, 5, trials);
    test({2, 7, 9, 3, 1}, 2, 2, trials);
    return 0;
}


