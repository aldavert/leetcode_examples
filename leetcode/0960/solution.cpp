#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minDeletionSize(std::vector<std::string> strs)
{
    const int m = static_cast<int>(strs.size());
    const int n = static_cast<int>(strs[0].size());
    std::vector<int> dp_table(n, 1);
    for (int i = 1; i < n; ++i)
    {
        for (int j = 0; j < i; ++j)
        {
            bool all_of = true;
            for (int k = 0; k < m; ++k)
                all_of = all_of && (strs[k][j] <= strs[k][i]);
            if (all_of)
                dp_table[i] = std::max(dp_table[i], dp_table[j] + 1);
        }
    }
    int max_element = dp_table[0];
    for (int i = 1; i < n; ++i) max_element = std::max(max_element, dp_table[i]);
    return n - max_element;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> strs, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDeletionSize(strs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"babca", "bbazb"}, 3, trials);
    test({"edcba"}, 4, trials);
    test({"ghi", "def", "abc"}, 0, trials);
    return 0;
}


