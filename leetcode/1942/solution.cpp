#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int smallestChair(std::vector<std::vector<int> > times, int targetFriend)
{
    const int n = static_cast<int>(times.size());
    std::priority_queue<int, std::vector<int>, std::greater<> > empty_chairs;
    std::priority_queue<std::pair<int, int>, std::vector<std::pair<int, int> >,
                        std::greater<>> occupied;
    int next_empty_chair = 0;
    for (int i = 0; i < n; ++i)
        times[i].push_back(i);
    std::sort(times.begin(), times.end());
    for (const auto &time : times)
    {
        while (!occupied.empty() && (occupied.top().first <= time[0]))
            empty_chairs.push(occupied.top().second), occupied.pop();
        if (time[2] == targetFriend)
            return empty_chairs.empty()?next_empty_chair:empty_chairs.top();
        if (empty_chairs.empty())
            occupied.emplace(time[1], next_empty_chair++);
        else
            occupied.emplace(time[1], empty_chairs.top()),
            empty_chairs.pop();
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > times,
          int targetFriend,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestChair(times, targetFriend);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 4}, {2, 3}, {4, 6}}, 1, 1, trials);
    test({{3, 10}, {1, 5}, {2, 6}}, 0, 2, trials);
    return 0;
}


