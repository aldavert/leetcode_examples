#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int compress(std::vector<char> &chars)
{
    std::string result;
    int count = 0;
    auto update = [&](char previous)->void
    {
        if      (count == 1) result += previous;
        else if (count >  1) result += previous + std::to_string(count);
    };
    for (char previous = '\0'; char letter : chars)
    {
        if (letter == previous) ++count;
        else
        {
            update(previous);
            previous = letter;
            count = 1;
        }
    }
    update(chars.back());
    chars = std::vector<char>(result.begin(), result.end());
    return static_cast<int>(chars.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<char> chars, std::vector<char> solution, unsigned int trials = 1)
{
    int solution_length = static_cast<int>(solution.size());
    std::vector<char> result;
    int result_length = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = chars;
        result_length = compress(result);
    }
    showResult((result_length == solution_length) && (solution == result),
               solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({'a', 'a', 'b', 'b', 'c', 'c', 'c'}, {'a', '2', 'b', '2', 'c', '3'}, trials);
    test({'a'}, {'a'}, trials);
    test({'a', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b'},
         {'a', 'b', '1', '2'}, trials);
    return 0;
}


