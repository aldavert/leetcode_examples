#include "../common/common.hpp"
#include <limits>
#include <unordered_map>

// ############################################################################
// ############################################################################

int integerReplacement(int n)
{
    auto dp = [](int start) -> int
    {
        int result = std::numeric_limits<int>::max();
        std::unordered_map<long, long> visited;
        auto search = [&](auto &&self, long value, int noperations) -> void
        {
            if (value <= 1)
            {
                if (value == 1) result = std::min(result, noperations);
                return;
            }
            if (noperations >= result) return;
            if (auto find = visited.find(value);
                (find != visited.end()) && (find->second <= noperations)) return;
            visited[value] = noperations;
            if (value & 1)
            {
                self(self, value + 1, noperations + 1);
                self(self, value - 1, noperations + 1);
            }
            else self(self, value / 2, noperations + 1);
        };
        search(search, start, 0);
        return result;
    };
    return dp(n);
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = integerReplacement(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 10'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(8, 3, trials);
    test(7, 4, trials);
    test(4, 2, trials);
    test(12, 4, trials);
    test(123, 9, trials);
    test(1234, 14, trials);
    test(1234567, 28, trials);
    test(123456789, 37, trials);
    test(1234567890, 41, trials);
    test(2147483647, 32, trials);
    return 0;
}


