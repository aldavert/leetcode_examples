#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int closestToTarget(std::vector<int> arr, int target)
{
    int v[20], sz = 1, result = std::abs(target - arr[0]);
    v[0] = arr[0];
    for (int i = 1, n = static_cast<int>(arr.size()); i < n; ++i)
    {
        int u = arr[i], p = 1; v[0] &= u;
        for (int j = 1; j < sz; ++j)
        {
            int w = v[j] & u;
            if (w != v[p - 1]) v[p++] = w;
        }
        if (u != v[p - 1]) v[p++] = u;
        sz = p;
        
        for (p = 0; (p < sz) && (v[p] <= target); ++p);
        int r = v[p - ((p > 0) && ((p == sz) || (v[p - 1] + v[p] > 2 * target)))];
        result = std::min(result, std::abs(target - r));
    }
    return result;
}
#else
int closestToTarget(std::vector<int> arr, int target)
{
    int result = std::numeric_limits<int>::max();
    std::unordered_set<int> s;
    
    for (size_t i = 0, n = arr.size(); i < n; ++i)
    {
        std::unordered_set<int> current{arr[i]};
        for (int b : s)
            current.insert(arr[i] & b);
        for (int c : s = current)
            result = std::min(result, std::abs(c - target));
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = closestToTarget(arr, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({9, 12, 3, 7, 15}, 5, 2, trials);
    test({1000000, 1000000, 1000000}, 1, 999999, trials);
    test({1, 2, 4, 8, 16}, 0, 0, trials);
    return 0;
}


