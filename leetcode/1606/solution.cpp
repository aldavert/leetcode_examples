#include "../common/common.hpp"
#include <queue>
#include <map>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> busiestServers(int k, std::vector<int> arrival, std::vector<int> load)
{
    const int n = static_cast<int>(arrival.size());
    std::vector<int> count(k), available(k), result;
    std::map<int, int> server_load_lut;
    
    for (int i = 0, last_i = 0; ; ++i)
    {
        if      (i < n) server_load_lut[arrival[i]] = load[i];
        else if (i - last_i > k) break;
        auto it = server_load_lut.lower_bound(available[i % k]);
        while (it != server_load_lut.end())
        {
            last_i = i;
            ++count[i % k];
            available[i % k] = it->first + it->second;
            server_load_lut.erase(it);
            it = server_load_lut.lower_bound(available[i % k]);
        }
    }
    int max_req = *max_element(count.begin(), count.end());
    for (int i = 0; i < k; ++i)
        if (count[i] == max_req)
            result.push_back(i);
    return result;
}
#else
std::vector<int> busiestServers(int k, std::vector<int> arrival, std::vector<int> load)
{
    const int n = static_cast<int>(arrival.size());
    std::vector<int> result, times(k);
    std::set<int> idle_servers;
    std::priority_queue<std::pair<int, int>,
                        std::vector<std::pair<int, int> >,
                        std::greater<> > heap;
    auto nextAvailable = [&](int request)
    {
        if (idle_servers.empty())
            return -1;
        const auto it = idle_servers.lower_bound(request % k);
        return (it == idle_servers.cend())?*idle_servers.begin():*it;
    };
    
    for (int i = 0; i < k; ++i)
        idle_servers.insert(i);
    for (int i = 0; i < n; ++i)
    {
        while (!heap.empty() && (heap.top().first <= arrival[i]))
        {
            idle_servers.insert(heap.top().second);
            heap.pop();
        }
        const int server = nextAvailable(i);
        if (server == -1) continue;
        ++times[server];
        heap.emplace(arrival[i] + load[i], server);
        idle_servers.erase(server);
    }
    const int busiest = *std::max_element(times.begin(), times.end());
    for (int i = 0; i < k; ++i)
        if (times[i] == busiest)
            result.push_back(i);
    return result;
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<int> left_work(left), right_work(right);
    std::sort(left_work.begin(), left_work.end());
    std::sort(right_work.begin(), right_work.end());
    for (size_t i = 0, n = left.size(); i < n; ++i)
        if (left_work[i] != right_work[i])
            return false;
    return true;
}

void test(int k,
          std::vector<int> arrival,
          std::vector<int> load,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = busiestServers(k, arrival, load);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {1, 2, 3, 4, 5}, {5, 2, 3, 3, 3}, {1}, trials);
    test(3, {1, 2, 3, 4}, {1, 2, 1, 2}, {0}, trials);
    test(3, {1, 2, 3}, {10, 12, 11}, {0, 1, 2}, trials);
    return 0;
}



