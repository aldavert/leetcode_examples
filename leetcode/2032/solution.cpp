#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> twoOutOfThree(std::vector<int> nums1,
                               std::vector<int> nums2,
                               std::vector<int> nums3)
{
    char unique1[101] = {}, unique2[101] = {}, unique3[101] = {};
    for (int value : nums1) unique1[value] = 1;
    for (int value : nums2) unique2[value] = 1;
    for (int value : nums3) unique3[value] = 1;
    std::vector<int> result;
    for (int i = 0; i <= 100; ++i)
        if (unique1[i] + unique2[i] + unique3[i] > 1)
            result.push_back(i);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &first, const std::vector<int> &second)
{
    if (first.size() != second.size()) return false;
    auto copy_first = first;
    auto copy_second = second;
    std::sort(copy_first.begin(), copy_first.end());
    std::sort(copy_second.begin(), copy_second.end());
    for (size_t i = 0, n = first.size(); i < n; ++i)
        if (copy_first[i] != copy_second[i])
            return false;
    return true;
}

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          std::vector<int> nums3,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = twoOutOfThree(nums1, nums2, nums3);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 3, 2}, {2, 3}, {3}, {3, 2}, trials);
    test({3, 1}, {2, 3}, {1, 2}, {2, 3, 1}, trials);
    test({1, 2, 2}, {4, 3, 3}, {5}, {}, trials);
    return 0;
}


