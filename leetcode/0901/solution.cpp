#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

class StockSpanner
{
    std::stack<std::pair<int, int> > stack;
public:
    int next(int price)
    {
        int span = 1;
        while (!stack.empty() && (stack.top().first <= price))
            span += stack.top().second, stack.pop();
        stack.emplace(price, span);
        return span;
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<int> prices,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result.clear();
        StockSpanner stock_spanner;
        for (int price : prices)
            result.push_back(stock_spanner.next(price));
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({100, 80, 60, 70, 60, 75, 85}, {1, 1, 1, 2, 1, 4, 6}, trials);
    return 0;
}


