#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int prefixCount(std::vector<std::string> words, std::string pref)
{
    int result = 0;
    for (const std::string &word : words)
        result += (word.size() >= pref.size()) && (word.substr(0, pref.size()) == pref);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::string pref,
          int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = prefixCount(words, pref);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"pay", "attention", "practice", "attend"}, "at", 2, trials);
    test({"leetcode", "win", "loops", "success"}, "code", 0, trials);
    return 0;
}


