# Insertion Sort List

Given the `head` of a singly linked list, sort the list using **insertion sort**, and return *the sorted list's head*.

The steps of the **insertion sort** algorithm:

1. Insertion sort iterates, consuming one input element each repetition and growing a sorted output list.
2. At each iteration, insertion sort removes one element from the input data, finds the location it belongs within the sorted list and inserts it there.
3. It repeats until no input elements remain.

#### Example 1:
> ```mermaid
> graph TD;
> subgraph A [ ]
> direction LR;
> A1((4))-->B1((2))-->C1((1))-->D1((3))
> end
> subgraph B [ ]
> direction LR;
> A2((1))-->B2((2))-->C2((3))-->D2((4))
> end
> A-->B
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef white fill:#FFF,stroke:#FFF,stroke-width:0px,color:red;
> class A,B white;
> ```
> *Input:* `head = [4, 2, 1, 3]`  
> *Output:* `[1, 2, 3, 4]`

#### Example 2:
> ```mermaid
> graph TD;
> subgraph A [ ]
> direction LR;
> A1(( -1))-->B1((5))-->C1((3))-->D1((4))-->E1((0))
> end
> subgraph B [ ]
> direction LR;
> A2(( -1))-->B2((0))-->C2((3))-->D2((4))-->E2((5))
> end
> A-->B
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef white fill:#FFF,stroke:#FFF,stroke-width:0px,color:red;
> class A,B white;
> ```
> *Input:* `head = [-1, 5, 3, 4, 0]`  
> *Output:* `[-1, 0, 3, 4, 5]`

#### Constraints:
- The number of nodes in the list is in the range `[1, 5000]`.
- `-5000 <= Node.val <= 5000`


