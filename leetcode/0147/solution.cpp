#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

ListNode * insertionSortList(ListNode * head)
{
    if (head == nullptr) return nullptr;
    ListNode * result = head;
    head = head->next;
    result->next = nullptr; // **
    while (head)
    {
        // Remove it from list.
        ListNode * current = head;
        head = head->next;
        current->next = nullptr; // **
        
        // Add it to the result list.
        if (current->val < result->val)
        {
            current->next = result;
            result = current;
        }
        else
        {
            ListNode * search = result;
            while ((search->next != nullptr) && (current->val > search->next->val))
                search = search->next;
            current->next = search->next;
            search->next = current;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> input, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * linked_list = vec2list(input);
        linked_list = insertionSortList(linked_list);
        result = list2vec(linked_list);
        delete linked_list;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 1, 3}, {1, 2, 3, 4}, trials);
    test({-1, 5, 3, 4, 0}, {-1, 0, 3, 4, 5}, trials);
    
    return 0;
}


