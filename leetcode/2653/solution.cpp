#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> getSubarrayBeauty(std::vector<int> nums, int k, int x)
{
    int count[50] = {};
    std::vector<int> result;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        if (nums[i] < 0) ++count[nums[i] + 50];
        if ((i - k >= 0) && (nums[i - k] < 0)) --count[nums[i - k] + 50];
        if (i + 1 >= k)
        {
            int value = 0;
            for (int j = 0, prefix = 0; j < 50; ++j)
            {
                prefix += count[j];
                if (prefix >= x)
                {
                    value = j - 50;
                    break;
                }
            }
            result.push_back(value);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          int x,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getSubarrayBeauty(nums, k, x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, -1, -3, -2, 3}, 3, 2, {-1, -2, -2}, trials);
    test({-1, -2, -3, -4, -5}, 2, 2, {-1, -2, -3, -4}, trials);
    test({-3, 1, 2, -3, 0, -3}, 2, 1, {-3, 0, -3, -3, -3}, trials);
    return 0;
}


