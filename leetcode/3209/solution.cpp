#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
long long countSubarrays(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    long long result = 0;
    for (int i = 0, j = 0; i < n; ++i)
    {
        if ((nums[i] & k) != k) continue;
        if (j <= i)
            for (j = i + 1; (j < n) && ((nums[j] & k) == k); ++j);
        int right = i, current = nums[i];
        while ((current != k) && (++right < j)) current &= nums[right];
        if (current != k) { i = j; continue; }
        int left = right;
        current = nums[right];
        while (current != k) current &= nums[--left];
        result += (j - right) * (left - i + 1ll);
        i = left;
    }
    return result;
}
#else
long long countSubarrays(std::vector<int> nums, int k)
{
    long long result = 0;
    for (std::unordered_map<int, int> prev; const int num : nums)
    {
        std::unordered_map<int, int> curr{{num, 1}};
        for (const auto &[val, freq] : prev)
            curr[val & num] += freq;
        result += curr.contains(k) ? curr[k] : 0;
        prev = curr;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSubarrays(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 1}, 1, 6, trials);
    test({1, 1, 2}, 1, 3, trials);
    test({1, 2, 3}, 2, 2, trials);
    return 0;
}


