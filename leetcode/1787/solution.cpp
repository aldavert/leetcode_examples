#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int minChanges(std::vector<int> nums, int k)
{
    int n = static_cast<int>(nums.size());
    std::vector<std::vector<int> > dp(k, std::vector<int>(1024)),
                                   count(k, std::vector<int>(1024));
    std::vector<int> total_count(k);
    
    for (int i = 0; i < k; ++i)
    { 
        total_count[i] = n / k  + (i < n % k);
        for (int j = i; j < n; j += k)
            ++count[i][nums[j]];
    }

    for (int d = 0; d < 1024; ++d)
        dp[0][d] = total_count[0] - count[0][d];
    
    for (int i = 1; i < k; ++i)
    {
        int pre_min = std::numeric_limits<int>::max(), x = 0;
        for (int d = 0; d < 1024; ++d)
        {
            if (dp[i - 1][d] < pre_min)
            {
                pre_min =  dp[i-1][d];
                x = d;                    
            }
        }
        
        for (int d = 0; d < 1024; ++d)
        {
            dp[i][d] = pre_min + total_count[i] - count[i][d ^ x];
            for (int j = i; j < n; j += k)
            dp[i][d] = std::min(dp[i][d], dp[i - 1][d ^ nums[j]]
                                        + total_count[i]
                                        - count[i][nums[j]]);
        }
    }
    
    return dp[k - 1][0];
}
#else
int minChanges(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::vector<std::unordered_map<int, int> > counts(k);
    for (int i = 0; i < n; ++i)
        ++counts[i % k][nums[i]];
    
    int tot_mx = 0, mn_mx = std::numeric_limits<int>::max();
    for (const auto &cnt : counts)
    {
        int mx = max_element(cnt.cbegin(), cnt.cend(),
                    [](auto x, auto y) { return x.second < y.second; })->second;
        tot_mx += mx;
        mn_mx = std::min(mn_mx, mx);
    }
    int one_are_not_from_nums = n - (tot_mx - mn_mx);
    
    std::unordered_map<int, int> dp{{0, 0}};
    for (const auto &cnt : counts)
    {
        std::unordered_map<int, int> new_dp;
        for (auto [x, dp_x] : dp)
            for (auto [y, cnt_y] : cnt)
                new_dp[x ^ y] = std::max(new_dp[x ^ y], dp_x + cnt_y);
        dp = std::move(new_dp);
    }
    int all_are_from_nums = n - dp[0];
    
    return std::min(one_are_not_from_nums, all_are_from_nums);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minChanges(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 0, 3, 0}, 1, 3, trials);
    test({3, 4, 5, 2, 1, 7, 3, 4, 7}, 3, 3, trials);
    test({1, 2, 4, 1, 2, 5, 1, 2, 6}, 3, 3, trials);
    return 0;
}


