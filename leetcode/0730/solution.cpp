#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countPalindromicSubsequences(std::string s)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(s.size());
    auto negmod = [&](auto first, auto second) -> auto
    {
        if (first > second) return first - second;
        else return MOD - second + first;
    };
    int dp[1000][1000] = {};
    for (int i = 0; i < n; ++i) dp[i][i] = 1;
    for (int d = 1; d < n; ++d)
    {
        for (int i = 0; i + d < n; ++i)
        {
            const int j = i + d;
            if (s[i] == s[j])
            {
                int lo = i + 1;
                int hi = j - 1;
                while ((lo <= hi) && (s[lo] != s[i])) ++lo;
                while ((lo <= hi) && (s[hi] != s[i])) --hi;
                if      (lo  > hi) dp[i][j] = (dp[i + 1][j - 1] * 2 + 2) % MOD;
                else if (lo == hi) dp[i][j] = (dp[i + 1][j - 1] * 2 + 1) % MOD;
                else               dp[i][j] = negmod((dp[i + 1][j - 1] * 2) % MOD,
                                                      dp[lo + 1][hi - 1]);
            }
            else dp[i][j] = negmod((dp[i][j - 1] + dp[i + 1][j]) % MOD, dp[i + 1][j - 1]);
        }
    }
    return static_cast<int>(dp[0][n - 1]);
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPalindromicSubsequences(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("bccb", 6, trials);
    test("abcdabcdabcdabcdabcdabcdabcdabcddcbadcbadcbadcbadcbadcbadcbadcba", 104860361, trials);
    return 0;
}


