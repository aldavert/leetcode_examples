#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumPushes(std::string word)
{
    int result = 0, letters = static_cast<int>(word.size());
    for (int pushes = 1; letters > 0; letters -= 8, ++pushes)
        result += std::min(letters, 8) * pushes;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumPushes(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcde", 5, trials);
    test("xycdefghij", 12, trials);
    return 0;
}


