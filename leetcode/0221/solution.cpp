#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int maximalSquare(std::vector<std::vector<char> > matrix)
{
    const int n = static_cast<int>(matrix.size()),
              m = static_cast<int>(matrix[0].size());
    int result = 0, dp[302] = {};
    for (int i = 1; i <= n; ++i)
    {
        for (int j = 1, previous = dp[0]; j <= m; ++j)
        {
            int current = dp[j];
            if (matrix[i - 1][j - 1] == '1')
                dp[j] = std::min(previous, std::min(dp[j], dp[j - 1])) + 1;
            else dp[j] = 0;
            result = std::max(result, dp[j]);
            previous = current;
        }
    }
    return result * result; 
}
#else
int maximalSquare(std::vector<std::vector<char> > matrix)
{
    const int n = static_cast<int>(matrix.size()),
              m = static_cast<int>(matrix[0].size());
    int result = 0, dp[302][302] = {};
    for (int i = 1; i <= n; ++i)
    {
        for (int j = 1; j <= m; ++j)
        {
            if (matrix[i - 1][j - 1] == '1')
            {
                int cost = dp[i - 1][j - 1];
                cost = std::min(cost, std::min(dp[i - 1][j], dp[i][j - 1]));
                dp[i][j] = cost + 1;
            }
            result = std::max(result, dp[i][j]);
        }
    }
    return result * result; 
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > matrix, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int t = 0; t < trials; ++t)
        result = maximalSquare(matrix);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'1', '0', '1', '0', '0'},
          {'1', '0', '1', '1', '1'},
          {'1', '1', '1', '1', '1'},
          {'1', '0', '0', '1', '0'}}, 4, trials);
    test({{'0', '1'}, {'1', '0'}}, 1, trials);
    test({{'0'}}, 0, trials);
    test({{'1', '1', '1', '1', '0'},
          {'1', '1', '1', '1', '1'},
          {'1', '1', '1', '1', '1'},
          {'1', '0', '1', '0', '0'}}, 9, trials);
    test({{'0', '0', '0', '0', '1', '1'},
          {'0', '1', '1', '1', '1', '0'},
          {'0', '1', '1', '1', '1', '0'},
          {'0', '1', '1', '1', '1', '0'},
          {'1', '1', '1', '1', '1', '1'},
          {'1', '0', '0', '0', '0', '0'}}, 16, trials);
    return 0;
}


