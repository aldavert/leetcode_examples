#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int deleteString(std::string s)
{
    constexpr int base = 31;
    const int n = static_cast<int>(s.size());
    std::vector<int> dp(n + 1), chr(n);
    for (int i = n - 1; i >= 0; --i)
        chr[n - i - 1] = s[i] - 'a';
    dp[1] = 1;
    for(int i = 2; i <= n; ++i)
    {
        dp[i] = 1;
        unsigned long a = chr[i - 1], b = chr[i - 2], pow = 1;
        if (a == b) dp[i] = std::max(dp[i], dp[i - 1] + 1);
        for (int j = 2; j <= i / 2; ++j)
        {
            a = a * base + chr[i - j];
            b = (b - chr[i - j] * pow) * base * base
              + chr[i - 2 * j + 1] * base + chr[i - 2 * j];
            pow *= base;
            if (a == b) dp[i] = std::max(dp[i], dp[i - j] + 1);
        }
    }
    return dp[n];
}
#else
int deleteString(std::string s)
{
    const int n = static_cast<int>(s.size());
    std::vector<std::vector<int> > lcs(n + 1, std::vector<int>(n + 1));
    std::vector<int> dp(n, 1);
    
    for (int i = n - 1; i >= 0; --i)
    {
        for (int j = i + 1; j < n; ++j)
        {
            if (s[i] == s[j]) lcs[i][j] = lcs[i + 1][j + 1] + 1;
            if (lcs[i][j] >= j - i) dp[i] = std::max(dp[i], dp[j] + 1);
        }
    }
    return dp[0];
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = deleteString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcabcdabc", 2, trials);
    test("aaabaab", 4, trials);
    test("aaaaa", 5, trials);
    return 0;
}


