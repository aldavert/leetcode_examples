#include "../common/common.hpp"
#include <random>

// ############################################################################
// ############################################################################

class Solution
{
    std::random_device m_random_device;
    std::mt19937 m_generator;
    std::discrete_distribution<> m_distribution;
public:
    Solution(std::vector<int> w) :
        m_generator(m_random_device()),
        m_distribution(w.begin(), w.end())
    {
    }
    int pickIndex()
    {
        return m_distribution(m_generator);
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<int> w, size_t number_of_points, unsigned int trials = 1)
{
    std::vector<double> expected(w.size());
    double total = static_cast<double>(std::accumulate(w.begin(), w.end(), 0));
    for (size_t i = 0; i < w.size(); ++i)
        expected[i] = static_cast<double>(w[i]) / total;
    for (unsigned int t = 0; t < trials; ++t)
    {
        Solution solution(w);
        std::vector<int> histogram(w.size(), 0);
        for (size_t p = 0; p < number_of_points; ++p)
        {
            int index = solution.pickIndex();
            if ((index < 0) || (index >= static_cast<int>(w.size())))
            {
                std::cout << showResult(false) << " Index '" << index << "' is "
                          << "out of range [0, " << w.size() - 1 << "].\n";
                return;
            }
            ++histogram[index];
        }
        double standard_deviation = 0.0;
        for (size_t index = 0; int frequency : histogram)
        {
            double difference = static_cast<double>(frequency)
                              / static_cast<double>(number_of_points)
                              - expected[index++];
            standard_deviation += difference * difference;
        }
        standard_deviation = std::sqrt(standard_deviation
                                     / static_cast<double>(w.size()));
        if (standard_deviation > 0.01)
        {
            std::cout << showResult(false) << " The random distribution doesn't "
                      << "follow the given index weights.\n";
            return;
        }
    }
    std::cout << showResult(true) << " The object generates the correct "
              << "distribution.\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(   {1}, 10'000, trials);
    test({1, 3}, 10'000, trials);
    return 0;
}


