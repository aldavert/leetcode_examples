#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumStrongPairXor(std::vector<int> nums)
{
    int result = 0;
    std::sort(nums.begin(), nums.end());
    for (size_t i = 0; i < nums.size(); ++i)
        for (size_t j = i + 1; j < nums.size(); ++j)
            if (nums[j] - nums[i] <= nums[i])
                result = std::max(result, nums[i] ^ nums[j]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumStrongPairXor(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 7, trials);
    test({10, 100}, 0, trials);
    test({5, 6, 25, 30}, 7, trials);
    return 0;
}


