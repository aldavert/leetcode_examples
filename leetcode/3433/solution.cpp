#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> countMentions(int numberOfUsers,
                               std::vector<std::vector<std::string> > events)
{
    struct Info
    {
        Info(void) = default;
        Info(const std::vector<std::string> &event)
        {
            message = event[0] == "MESSAGE";
            time = std::stoi(event[1]);
            info = event[2];
        }
        bool message = false;
        int time = -1;
        std::string info;
        bool operator<(const Info &other) const
        {
            return (time < other.time)
                || ((time == other.time) && (message < other.message));
        };
    };
    std::vector<Info> information;
    for (const auto &event : events)
        information.push_back({event});
    std::sort(information.begin(), information.end());
    
    std::vector<int> mentions(numberOfUsers, 0), time_active(numberOfUsers, 0);
    auto process = [&](int time, const std::string &msg) -> std::vector<int>
    {
        std::vector<int> result;
        if (msg == "ALL")
        {
            result.reserve(numberOfUsers);
            for (int i = 0; i < numberOfUsers; ++i)
                result.push_back(i);
        }
        else if (msg == "HERE")
        {
            for (int i = 0; i < numberOfUsers; ++i)
                if (time >= time_active[i])
                    result.push_back(i);
        }
        else
        {
            std::string::size_type start = 0, stop = msg.find(' ');
            while (stop != std::string::npos)
            {
                result.push_back(std::stoi(msg.substr(start + 2, stop - start)));
                start = stop + 1;
                stop = msg.find(' ', start);
            }
            result.push_back(std::stoi(msg.substr(start + 2)));
        }
        return result;
    };
    for (const auto &current : information)
    {
        if (current.message)
            for (int id : process(current.time, current.info))
                ++mentions[id];
        else time_active[std::stoi(current.info)] = current.time + 60;
    }
    return mentions;
}

// ############################################################################
// ############################################################################

void test(int numberOfUsers,
          std::vector<std::vector<std::string> > events,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countMentions(numberOfUsers, events);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {{"MESSAGE", "10", "id0 id1"},
             {"OFFLINE", "11", "0"},
             {"MESSAGE", "50", "ALL"},
             {"MESSAGE", "51", "HERE"},
             {"MESSAGE", "71", "id1 id0"}}, {3, 4}, trials);
    test(2, {{"MESSAGE", "10", "id1 id0"},
             {"OFFLINE", "11", "0"},
             {"MESSAGE", "71", "HERE"}}, {2, 2}, trials);
    test(2, {{"MESSAGE", "10", "id1 id0"},
             {"OFFLINE", "11", "0"},
             {"MESSAGE", "12", "ALL"}}, {2, 2}, trials);
    test(2, {{"OFFLINE", "10", "0"},
             {"MESSAGE", "12", "HERE"}}, {0, 1}, trials);
    test(3, {{"MESSAGE", "2", "HERE"},
             {"OFFLINE", "2", "1"},
             {"OFFLINE", "1", "0"},
             {"MESSAGE", "61", "HERE"}}, {1, 0, 2}, trials);
    test(5, {{"OFFLINE", "28", "1"},
             {"OFFLINE", "14", "2"},
             {"MESSAGE", "2", "ALL"},
             {"MESSAGE", "28", "HERE"},
             {"OFFLINE", "66", "0"},
             {"MESSAGE", "34", "id2"},
             {"MESSAGE", "83", "HERE"},
             {"MESSAGE", "40", "id3 id3 id2 id4 id4"}}, {2, 1, 4, 5, 5}, trials);
    test(5, {{"MESSAGE", "2", "ALL"},
             {"OFFLINE", "14", "2"},
             {"OFFLINE", "28", "1"},
             {"MESSAGE", "28", "HERE"},
             {"MESSAGE", "34", "id2"},
             {"MESSAGE", "40", "id3 id3 id2 id4 id4"},
             {"OFFLINE", "66", "0"},
             {"MESSAGE", "83", "HERE"},}, {2, 1, 4, 5, 5}, trials);
    return 0;
}


