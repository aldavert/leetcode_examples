#include "../common/common.hpp"
#include <queue>
#include <utility>

constexpr int null = -1000;

class Node
{
public:
    bool val = false;
    bool isLeaf = false;
    Node * topLeft = nullptr;
    Node * topRight = nullptr;
    Node * bottomLeft = nullptr;
    Node * bottomRight = nullptr;
    
    Node(void) = default;
    Node(bool _val, bool _isLeaf) :
        val(_val),
        isLeaf(_isLeaf),
        topLeft(nullptr),
        topRight(nullptr),
        bottomLeft(nullptr),
        bottomRight(nullptr) {}
    Node(bool _val,
         bool _isLeaf,
         Node * _topLeft,
         Node * _topRight,
         Node * _bottomLeft,
         Node * _bottomRight) :
        val(_val),
        isLeaf(_isLeaf),
        topLeft(_topLeft),
        topRight(_topRight),
        bottomLeft(_bottomLeft),
        bottomRight(_bottomRight) {}
    Node(const Node &other) :
        val(other.val),
        isLeaf(other.isLeaf),
        topLeft(other.topLeft?new Node(*topLeft):nullptr),
        topRight(other.topRight?new Node(*topRight):nullptr),
        bottomLeft(other.bottomLeft?new Node(*bottomLeft):nullptr),
        bottomRight(other.bottomRight?new Node(*bottomRight):nullptr) {}
    Node(Node &&other) :
        val(other.val),
        isLeaf(other.isLeaf),
        topLeft(std::exchange(other.topLeft, nullptr)),
        topRight(std::exchange(other.topRight, nullptr)),
        bottomLeft(std::exchange(other.bottomLeft, nullptr)),
        bottomRight(std::exchange(other.bottomRight, nullptr)) {}
    ~Node(void)
    {
        delete topLeft;
        delete topRight;
        delete bottomLeft;
        delete bottomRight;
    }
    Node& operator=(const Node &other)
    {
        if (this != &other)
        {
            delete topLeft;
            delete topRight;
            delete bottomLeft;
            delete bottomRight;
            val = other.val;
            isLeaf = other.isLeaf;
            topLeft = other.topLeft?new Node(*topLeft):nullptr;
            topRight = other.topRight?new Node(*topRight):nullptr;
            bottomLeft = other.bottomLeft?new Node(*bottomLeft):nullptr;
            bottomRight = other.bottomRight?new Node(*bottomRight):nullptr;
        }
        return *this;
    }
    Node& operator=(Node &&other)
    {
        val = other.val;
        isLeaf = other.isLeaf;
        topLeft = std::exchange(other.topLeft, nullptr);
        topRight = std::exchange(other.topRight, nullptr);
        bottomLeft = std::exchange(other.bottomLeft, nullptr);
        bottomRight = std::exchange(other.bottomRight, nullptr);
        return *this;
    }
};

struct SerialNode
{
    SerialNode(int) : leaf(false), value(false), nothing(true) {}
    SerialNode(bool _leaf, bool _value) : leaf(_leaf), value(_value), nothing(false) {}
    bool leaf = false;
    bool value = false;
    bool nothing = true;
    bool operator==(const SerialNode &other) const
    {
        if (nothing != other.nothing) return false;
        if (nothing) return true;
        if (leaf != other.leaf) return false;
        if (!leaf) return true;
        return value == other.value;
    }
};

Node * vec2quad(std::vector<SerialNode> &vec)
{
    Node * result = new Node(vec[0].value, vec[0].leaf);
    std::queue<Node *> previous;
    previous.push(result);
    for (size_t i = 1; i < vec.size(); i += 4)
    {
        if (vec[i].nothing) continue;
        Node * current = previous.front();
        previous.pop();
        current->topLeft     = new Node(vec[i + 0].value, vec[i + 0].leaf);
        current->topRight    = new Node(vec[i + 1].value, vec[i + 1].leaf);
        current->bottomLeft  = new Node(vec[i + 2].value, vec[i + 2].leaf);
        current->bottomRight = new Node(vec[i + 3].value, vec[i + 3].leaf);
        if (!vec[i + 0].leaf) previous.push(current->topLeft);
        if (!vec[i + 1].leaf) previous.push(current->topRight);
        if (!vec[i + 2].leaf) previous.push(current->bottomLeft);
        if (!vec[i + 3].leaf) previous.push(current->bottomRight);
    }
    return result;
}

std::vector<SerialNode> quad2vec(const Node * root)
{
    std::queue<const Node *> nodes;
    nodes.push(root);
    std::vector<SerialNode> result;
    while (!nodes.empty())
    {
        const Node * node = nodes.front();
        nodes.pop();
        if (!node) result.push_back(null);
        else
        {
            result.push_back({node->isLeaf, node->val});
            nodes.push(node->topLeft);
            nodes.push(node->topRight);
            nodes.push(node->bottomLeft);
            nodes.push(node->bottomRight);
        }
    }
    while (result.back().nothing) result.pop_back();
    return result;
}

std::ostream& operator<<(std::ostream &out, const SerialNode &node)
{
    if (node.nothing) out << "null";
    else out << '{' << node.leaf << ", " << node.value << '}';
    return out;
}

// ############################################################################
// ############################################################################

Node * intersect(Node * quadTree1, Node * quadTree2)
{
    auto copy = [&](auto &&self, Node * node) -> Node *
    {
        if (node == nullptr) return nullptr;
        Node * result = new Node(node->val, node->isLeaf);
        result->topLeft     = self(self, node->topLeft    );
        result->topRight    = self(self, node->topRight   );
        result->bottomLeft  = self(self, node->bottomLeft );
        result->bottomRight = self(self, node->bottomRight);
        return result;
    };
    if (!(quadTree1->isLeaf || quadTree2->isLeaf))
    {
        Node * result = new Node(true, false);
        result->topLeft     = intersect(quadTree1->topLeft    , quadTree2->topLeft    );
        result->topRight    = intersect(quadTree1->topRight   , quadTree2->topRight   );
        result->bottomLeft  = intersect(quadTree1->bottomLeft , quadTree2->bottomLeft );
        result->bottomRight = intersect(quadTree1->bottomRight, quadTree2->bottomRight);
        if (result->topLeft->isLeaf
        &&  result->topRight->isLeaf
        &&  result->bottomLeft->isLeaf
        &&  result->bottomRight->isLeaf
        &&  (result->topLeft->val == result->topRight->val)
        &&  (result->bottomLeft->val == result->bottomRight->val)
        &&  (result->topLeft->val == result->bottomRight->val))
        {
            result->val = result->topLeft->val;
            result->isLeaf = true;
            delete result->topLeft;
            delete result->topRight;
            delete result->bottomLeft;
            delete result->bottomRight;
            result->topLeft = result->topRight =
                result->bottomLeft = result->bottomRight = nullptr;
        }
        return result;
    }
    if (quadTree1->isLeaf && quadTree2->isLeaf)
        return new Node(quadTree1->val || quadTree2->val, true);
    if (quadTree1->isLeaf)
    {
        if (quadTree1->val) return new Node(true, true);
        return copy(copy, quadTree2);
    }
    else
    {
        if (quadTree2->val) return new Node(true, true);
        return copy(copy, quadTree1);
    }
}

// ############################################################################
// ############################################################################

void test(std::vector<SerialNode> quadTree1,
          std::vector<SerialNode> quadTree2,
          std::vector<SerialNode> solution,
          unsigned int trials = 1)
{
    std::vector<SerialNode> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        Node * tree1 = vec2quad(quadTree1);
        Node * tree2 = vec2quad(quadTree2);
        Node * output = intersect(tree1, tree2);
        result = quad2vec(output);
        delete output;
        delete tree1;
        delete tree2;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {1, 1}, {1, 1}, {1, 0}, {1, 0}},
         {{0, 1}, {1, 1}, {0, 1}, {1, 1}, {1, 0}, null, null, null, null,
          {1, 0}, {1, 0}, {1, 1}, {1, 1}},
         {{0, 0}, {1, 1}, {1, 1}, {1, 1}, {1, 0}}, trials);
    test({{1, 0}}, {{1, 0}}, {{1, 0}}, trials);
    test({{0, 0}, {1, 0}, {1, 0}, {1, 1}, {1, 1}},
         {{0, 0}, {1, 1}, {1, 1}, {1, 0}, {1, 1}},
         {{1, 1}}, trials);
    return 0;
}


