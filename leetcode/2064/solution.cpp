#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int minimizedMaximum(int n, std::vector<int> quantities)
{
    int l = 1, r = *std::max_element(quantities.begin(), quantities.end());
    while (l < r)
    {
        int m = (l + r) / 2;
        auto cmp = [&](int subtotal, int q) { return subtotal + (q - 1) / m + 1; };
        if (std::accumulate(quantities.begin(), quantities.end(), 0, cmp) <= n)
            r = m;
        else l = m + 1;
    }
    return l;
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<int> quantities, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimizedMaximum(n, quantities);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, {11, 6}, 3, trials);
    test(7, {15, 10, 10}, 5, trials);
    test(1, {100000}, 100000, trials);
    return 0;
}


