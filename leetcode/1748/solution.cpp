#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sumOfUnique(std::vector<int> nums)
{
    int histogram[101] = {};
    for (int value : nums)
        ++histogram[value];
    int result = 0;
    for (int i = 1; i <= 100; ++i)
        result += (histogram[i] == 1) * i;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOfUnique(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 2}, 4, trials);
    test({1, 1, 1, 1, 1}, 0, trials);
    test({1, 2, 3, 4, 5}, 15, trials);
    return 0;
}


