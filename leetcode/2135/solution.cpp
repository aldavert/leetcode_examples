#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int wordCount(std::vector<std::string> startWords, std::vector<std::string> targetWords)
{
    auto calcMask = [](const std::string &s) -> int
    {
        int mask = 0;
        for (char c : s) mask ^= 1 << static_cast<int>(c - 'a');
        return mask;
    };
    std::unordered_set<int> seen;
    int result = 0;
    for (const auto &w : startWords)
        seen.insert(calcMask(w));
    for (const auto &w : targetWords)
    {
        for (int mask = calcMask(w); const char c : w)
        {
            if (seen.contains(mask ^ 1 << static_cast<int>(c - 'a')))
            {
                ++result;
                break;
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> startWords,
          std::vector<std::string> targetWords,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = wordCount(startWords, targetWords);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"ant", "act", "tack"}, {"tack", "act", "acti"}, 2, trials);
    test({"ab", "a"}, {"abc", "abcd"}, 1, trials);
    return 0;
}


