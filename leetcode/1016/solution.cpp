#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

bool queryString(std::string s, int n)
{
    if (n > 1511) return false;
    for (int i = n; i > n / 2; --i)
    {
        std::string binary = std::bitset<32>(i).to_string();
        binary = binary.substr(binary.find("1"));
        if (s.find(binary) == std::string::npos)
            return false;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string s, int n, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = queryString(s, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("0110", 3, true, trials);
    test("0110", 4, false, trials);
    return 0;
}


