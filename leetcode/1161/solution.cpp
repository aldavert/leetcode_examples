#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int maxLevelSum(TreeNode * root)
{
    std::queue<TreeNode *> levels;
    int max_sum = std::numeric_limits<int>::lowest(), result = 0;
    levels.push(root);
    for (int level = 1; !levels.empty(); ++level)
    {
        int sum = 0;
        for (size_t i = 0, n = levels.size(); i < n; ++i)
        {
            TreeNode * current = levels.front();
            levels.pop();
            if (current->left) levels.push(current->left);
            if (current->right) levels.push(current->right);
            sum += current->val;
        }
        if (sum > max_sum)
        {
            max_sum = sum;
            result = level;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = maxLevelSum(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 7, 0, 7, -8, null, null}, 2, trials);
    test({989, null, 10250, 98693, -89388, null, null, null, -32127}, 2, trials);
    return 0;
}


