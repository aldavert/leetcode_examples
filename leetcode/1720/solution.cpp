#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> decode(std::vector<int> encoded, int first)
{
    std::vector<int> result;
    result.push_back(first);
    for (int value : encoded)
        result.push_back(result.back() ^ value);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> encoded,
          int first,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = decode(encoded, first);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 1, {1, 0, 2, 1}, trials);
    test({6, 2, 7, 3}, 4, {4, 2, 0, 7, 4}, trials);
    return 0;
}


