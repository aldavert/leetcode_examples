#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 0
int uniquePaths(int m, int n)
{
    auto fact = [](int v) -> int
    {
        int result = 1;
        for (int i = 2; i <= v; ++i)
            result *= i;
        return result;
    };
    return fact(n + m - 2) / (fact(n - 1) * fact(m - 1));
}
#elif 1
int uniquePaths(int m, int n)
{
    int dp[101] = {};
    dp[0] = 1;
    for (int i = 0; i < m; ++i)
        for (int j = 1; j < n; ++j)
            dp[j] += dp[j - 1];
    return dp[n - 1];
}
#else
int uniquePaths(int m, int n)
{
    if ((m == 1) || (n == 1)) return 1;
    return uniquePaths(m - 1, n) + uniquePaths(m, n - 1);
}
#endif

// ############################################################################
// ############################################################################

void test(int m, int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = uniquePaths(m, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    //7 4 3 5/2 11/5 2
    //70/10 40/10 30/10 25/10 22/10 20/10
    //    30 -> 10 ->  5 ->  3  -> 2
    //6 7/2 8/3 9/4 2 11/6
    //72/12 42/12 32/12 27/12 24/12 22/12
    //    30 -> 10 ->  5 ->  3  -> 2
    // (m + n – 2)! / (n – 1)! * (m – 1)! which is nothing

    test(3, 7, 28, trials);
    test(3, 2, 3, trials);
    test(7, 3, 28, trials);
    test(3, 3, 6, trials);
    test(7, 7, 924, trials);
    return 0;
}


