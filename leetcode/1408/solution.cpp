#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<std::string> stringMatching(std::vector<std::string> words)
{
    const size_t n = words.size();
    std::vector<std::string> result;
    std::vector<bool> available(n, true);
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = 0; j < n; ++j)
        {
            if (available[j]
            &&  (words[i].size() > words[j].size())
            &&  (words[i].find(words[j]) != std::string::npos))
            {
                result.push_back(words[j]);
                available[j] = false;
            }
        }
    }
    return result;
}
#elif 0
std::vector<std::string> stringMatching(std::vector<std::string> words)
{
    std::vector<std::string> result;
    for (std::string w1 : words)
        for (std::string w2 : words)
            if ((w1 != w2) && (w2.find(w1) != std::string::npos))
            {
                result.push_back(w1);
                break;
            }
    return result;
}
#else
std::vector<std::string> stringMatching(std::vector<std::string> words)
{
    auto isSubstring = [](std::string word, std::string substring) -> bool
    {
        const size_t n = word.size();
        const size_t m = substring.size();
        for (size_t i = 0; i <= n - m; ++i)
        {
            if (word[i] == substring[0])
            {
                bool same = true;
                size_t j;
                for (j = 1; j < m; ++j)
                    if (!(same = (word[i + j] == substring[j])))
                        break;
                if (same)
                    return true;
            }
        }
        return false;
    };
    const size_t n = words.size();
    std::vector<std::string> result;
    std::vector<bool> available(n, true);
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = 0; j < n; ++j)
        {
            if (available[j]
            &&  (words[i].size() > words[j].size())
            &&  isSubstring(words[i], words[j]))
            {
                result.push_back(words[j]);
                available[j] = false;
            }
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_set<std::string> lut(left.begin(), left.end());
    for (std::string word : right)
    {
        if (auto search = lut.find(word); search != lut.end())
            lut.erase(search);
        else return false;
    }
    return lut.empty();
}

void test(std::vector<std::string> words,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = stringMatching(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"mass", "as", "hero", "superhero"}, {"as", "hero"}, trials);
    test({"leetcode", "et", "code"}, {"et", "code"}, trials);
    test({"blue", "green", "bu"}, {}, trials);
    test({"evl", "evlat", "kes", "lwkesz", "ckk", "eylwkesz", "efuw", "ickkw",
          "xnc", "evlon", "qsmd", "nmlwkeszk", "uyh", "xncme", "auze", "ixncmeqc"}, 
         {"evl", "kes", "lwkesz", "ckk", "xnc", "xncme"},  trials);
    return 0;
}



