#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool judgePoint24(std::vector<int> cards) // This version is dumb, but is the fastest.
{
    auto bcktr1 = [](double a) -> bool
    {
        return std::abs(a - 24) < 1e-4;
    };
    auto bcktr2 = [&](double a, double b) -> bool
    {
        return bcktr1(a + b)
            || bcktr1(a * b)
            || bcktr1(a - b)
            || bcktr1(b - a)
            || bcktr1(a / b)
            || bcktr1(b / a);
    };
    auto bcktr3 = [&](double a, double b, double c) -> bool
    {
        return bcktr2(a + b, c) || bcktr2(a + c, b)
            || bcktr2(a, b + c)
            || bcktr2(a * b, c) || bcktr2(a * c, b)
            || bcktr2(a, b * c)
            || bcktr2(a - b, c) || bcktr2(a - c, b)
            || bcktr2(b - a, c) || bcktr2(a, b - c)
            || bcktr2(c - a, b) || bcktr2(a, c - b)
            || bcktr2(a / b, c) || bcktr2(a / c, b)
            || bcktr2(b / a, c) || bcktr2(a, b / c)
            || bcktr2(c / a, b) || bcktr2(a, c / b);
    };
    auto bcktr4 = [&](double a, double b, double c, double d) -> bool
    {
        return bcktr3(a + b, c, d) || bcktr3(a + c, b, d) || bcktr3(a + d, b, c)
            || bcktr3(a, b + c, d) || bcktr3(a, b + d, c)
            || bcktr3(a, b, c + d)
            || bcktr3(a * b, c, d) || bcktr3(a * c, b, d) || bcktr3(a * d, b, c)
            || bcktr3(a, b * c, d) || bcktr3(a, b * d, c)
            || bcktr3(a, b, c * d)
            || bcktr3(a - b, c, d) || bcktr3(a - c, b, d) || bcktr3(a - d, b, c)
            || bcktr3(b - a, c, d) || bcktr3(a, b - c, d) || bcktr3(a, b - d, c)
            || bcktr3(c - a, b, d) || bcktr3(a, c - b, d) || bcktr3(a, b, c - d)
            || bcktr3(d - a, b, c) || bcktr3(d - b, a, c) || bcktr3(d - c, a, b)
            || bcktr3(a / b, c, d) || bcktr3(a / c, b, d) || bcktr3(a / d, b, c)
            || bcktr3(b / a, c, d) || bcktr3(a, b / c, d) || bcktr3(a, b / d, c)
            || bcktr3(c / a, b, d) || bcktr3(a, c / b, d) || bcktr3(a, b, c / d)
            || bcktr3(d / a, b, c) || bcktr3(d / b, a, c) || bcktr3(d / c, a, b);
    };
    return bcktr4(cards[0], cards[1], cards[2], cards[3]);
}
#elif 0
bool judgePoint24(std::vector<int> cards)
{
    double values[5][4] = {};
    values[4][0] = cards[0];
    values[4][1] = cards[1];
    values[4][2] = cards[2];
    values[4][3] = cards[3];
    auto backtrace = [&](auto &&self, int level) -> bool
    {
        if (level == 1)
            return std::abs(values[1][0] - 24) < 1e-4;
        for (int i = 0; i < level; ++i)
        {
            for (int j = 0; j < level; ++j)
            {
                if (i == j) continue;
                int n = 0;
                for (int k = 0; k < level; ++k)
                    if ((k != i) && (k != j))
                        values[level - 1][n++] = values[level][k];
                values[level - 1][n] = values[level][i] + values[level][j];
                if (self(self, level - 1)) return true;
                values[level - 1][n] = values[level][i] - values[level][j];
                if (self(self, level - 1)) return true;
                values[level - 1][n] = values[level][i] * values[level][j];
                if (self(self, level - 1)) return true;
                values[level - 1][n] = values[level][i] / values[level][j];
                if (self(self, level - 1)) return true;
            }
        }
        return false;
    };
    return backtrace(backtrace, 4);
}
#else
bool judgePoint24(std::vector<int> cards)
{
    auto backtrace = [&](auto &&self, std::vector<double> values) -> bool
    {
        if (values.size() == 1)
            return std::abs(values[0] - 24) < 1e-4;
        for (size_t i = 0; i < values.size(); ++i)
        {
            for (size_t j = 0; j < values.size(); ++j)
            {
                if (i == j) continue;
                std::vector<double> next;
                for (size_t k = 0; k < values.size(); ++k)
                    if ((k != i) && (k != j))
                        next.push_back(values[k]);
                next.push_back(values[i] + values[j]);
                if (self(self, next)) return true;
                next.pop_back();
                next.push_back(values[i] - values[j]);
                if (self(self, next)) return true;
                next.pop_back();
                next.push_back(values[i] / values[j]);
                if (self(self, next)) return true;
                next.pop_back();
                next.push_back(values[i] * values[j]);
                if (self(self, next)) return true;
                next.pop_back();
            }
        }
        return false;
    };
    return backtrace(backtrace, std::vector<double>(cards.begin(), cards.end()));
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> cards, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = judgePoint24(cards);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 1, 8, 7}, true, trials);
    test({1, 2, 1, 2}, false, trials);
    test({1, 5, 9, 1}, false, trials);
    test({3, 3, 8, 8}, true, trials);
    return 0;
}


