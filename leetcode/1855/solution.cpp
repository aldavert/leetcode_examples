#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxDistance(std::vector<int> nums1, std::vector<int> nums2)
{
    int i = 0, j = 0;
    for (int n = static_cast<int>(nums1.size()), m = static_cast<int>(nums2.size());
            (i < n) && (j < m); ++j)
        i += (nums1[i] > nums2[j]);
    return (i != j) * (j - i - 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1, std::vector<int> nums2, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxDistance(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({55, 30, 5, 4, 2}, {100, 20, 10, 10, 5}, 2, trials);
    test({2, 2, 2}, {10, 10, 1}, 1, trials);
    test({30, 29, 19, 5}, {25, 25, 25, 25, 25}, 2, trials);
    return 0;
}


