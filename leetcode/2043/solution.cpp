#include "../common/common.hpp"

// ############################################################################
// ############################################################################

class Bank
{
    std::vector<long long> m_balance;
    inline bool isValid(int account) const
    {
        return (1 <= account) && (account <= static_cast<int>(m_balance.size()));
    }
public:
    Bank(std::vector<long long> &balance) : m_balance(balance) {}
    bool transfer(int account1, int account2, long long money)
    {
        if (!isValid(account2)) return false;
        return withdraw(account1, money) && deposit(account2, money);
    }
    bool deposit(int account, long long money)
    {
        if (!isValid(account)) return false;
        m_balance[account - 1] += money;
        return true;
    }
    bool withdraw(int account, long long money)
    {
        if (!isValid(account)) return false;
        if (m_balance[account - 1] < money) return false;
        m_balance[account - 1] -= money;
        return true;
    }
};

// ############################################################################
// ############################################################################

enum class OP { TRANSFER, DEPOSIT, WITHDRAW };

void test(std::vector<long long> balance,
          std::vector<OP> operations,
          std::vector<std::vector<int> > account,
          std::vector<long long> money,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        Bank obj(balance);
        for (size_t i = 0; i < operations.size(); ++i)
        {
            switch (operations[i])
            {
            case OP::TRANSFER:
                result.push_back(obj.transfer(account[i][0], account[i][1], money[i]));
                break;
            case OP::DEPOSIT:
                result.push_back(obj.deposit(account[i][0], money[i]));
                break;
            case OP::WITHDRAW:
                result.push_back(obj.withdraw(account[i][0], money[i]));
                break;
            default:
                std::cerr << "[ERROR] Unexpected operation.\n";
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 100, 20, 50, 30},
         {OP::WITHDRAW, OP::TRANSFER, OP::DEPOSIT, OP::TRANSFER, OP::WITHDRAW},
         {{3}, {5, 1}, {5}, {3, 4}, {10}},
         {10, 20, 20, 15, 50},
         {true, true, true, false, false}, trials);
    return 0;
}


