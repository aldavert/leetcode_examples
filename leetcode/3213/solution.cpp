#include "../common/common.hpp"
#include <unordered_map>
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int minimumCost(std::string target,
                std::vector<std::string> words,
                std::vector<int> costs)
{
    class Trie
    {
        struct Node
        {
            Node(void) = default;
            ~Node(void) { for (int i = 0; i < 26; ++i) delete children[i]; }
            Node * children[26] = {};
            Node * fail = nullptr;
            Node * dict = nullptr;
            int word_id = -1;
            int cost = std::numeric_limits<int>::max();
        };
        std::vector<std::string> m_words;
        std::vector<int> m_costs;
        Node root;
    public:
        Trie(const std::vector<std::string> &words, const std::vector<int> &costs) :
            m_words(words),
            m_costs(costs)
        {
            for (size_t i = 0; i < words.size(); ++i)
            {
                Node * current = &root;
                for (int j = 0; j < (int)words[i].size(); ++j)
                {
                    int idx = words[i][j] - 'a';
                    if (current->children[idx] == nullptr)
                        current->children[idx] = new Node();
                    current = current->children[idx];
                }
                if (costs[i] < current->cost)
                {
                    current->cost = costs[i];
                    current->word_id = static_cast<int>(i);
                }
            }
            std::queue<Node *> q;
            q.push(&root);
            while (!q.empty())
            {
                Node * parent = q.front();
                q.pop();
                for (int idx = 0; idx < 26; ++idx)
                {
                    if (parent->children[idx] == nullptr) continue;
                    Node * child = parent->children[idx];
                    q.push(child);
                    Node * current = parent->fail;
                    while (current && !current->children[idx])
                        current = current->fail;
                    child->fail = (!current)?&root:current->children[idx];
                    child->dict = (child->fail->word_id > -1)?child->fail
                                                             :child->fail->dict;
                }
            }
        }
        int search(std::string target)
        {
            int n = static_cast<int>(target.size());
            std::vector<int> dp(n + 1, std::numeric_limits<int>::max());
            dp[0] = 0;
            Node * current = &root;
            
            for (int i = 0; i < n; ++i)
            {
                while (current && !current->children[target[i] - 'a'])
                    current = current->fail;
                if (!current) { current = &root; continue; }
                current = current->children[target[i] - 'a'];
                int min_cost = std::numeric_limits<int>::max();
                Node * output = current;
                while (output)
                {
                    if (output->word_id == -1) { output = output->dict; continue; }
                    int word_idx = output->word_id;
                    int len_word = static_cast<int>(m_words[word_idx].size());
                    int cost_current = m_costs[word_idx];
                    int cost_preceding =
                                (i + 1 >= len_word)?dp[i + 1 - len_word]
                                                   :std::numeric_limits<int>::max();
                    if (cost_preceding != std::numeric_limits<int>::max())
                        cost_current += cost_preceding;
                    else cost_current = std::numeric_limits<int>::max();
                    min_cost = std::min(min_cost, cost_current);
                    output = output->dict;
                }
                dp[i + 1] = min_cost;
            }
            return (dp[n] != std::numeric_limits<int>::max())?dp[n]:-1;
        }
    };
    Trie trie(words, costs);
    return trie.search(target);
}
#else
int minimumCost(std::string target,
                std::vector<std::string> words,
                std::vector<int> costs)
{
    class Hashing
    {
        std::vector<long> p, h;
        long mod;
    public:
        Hashing(const std::string &word, long base, long _mod)
            : p(word.size() + 1, 1)
            , h(word.size() + 1, 0)
            , mod(_mod)
        {
            for (size_t i = 1; i <= word.size(); ++i)
            {
                p[i] = p[i - 1] * base % _mod;
                h[i] = (h[i - 1] * base + word[i - 1]) % _mod;
            }
        }
        long query(int l, int r)
        {
            return (h[r] - h[l - 1] * p[r - l + 1] % mod + mod) % mod;
        }
    };
    constexpr int BASE = 13331;
    constexpr int MOD = 998244353;
    constexpr int INF = std::numeric_limits<int>::max() / 2;
    const int n = static_cast<int>(target.size());
    Hashing hashing(target, BASE, MOD);
    std::vector<int> f(n + 1, INF);
    f[0] = 0;
    std::set<int> ss;
    for (const auto &w : words)
        ss.insert(static_cast<int>(w.size()));
    std::unordered_map<long, int> d;
    for (size_t i = 0; i < words.size(); ++i)
    {
        long x = 0;
        for (char c : words[i])
            x = (x * BASE + c) % MOD;
        d[x] = (d.find(x) == d.end())?costs[i]:std::min(d[x], costs[i]);
    }
    for (int i = 1; i <= n; ++i)
    {
        for (int j : ss)
        {
            if (j > i) break;
            long x = hashing.query(i - j + 1, i);
            if (d.contains(x))
                f[i] = std::min(f[i], f[i - j] + d[x]);
        }
    }
    return (f[n] >= INF)?-1:f[n];
}
#endif

// ############################################################################
// ############################################################################

void test(std::string target,
          std::vector<std::string> words,
          std::vector<int> costs,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumCost(target, words, costs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcdef", {"abdef", "abc", "d", "def", "ef"}, {100, 1, 1, 10, 5}, 7, trials);
    test("aaaa", {"z", "zz", "zzz"}, {1, 10, 100}, -1, trials);
    return 0;
}


