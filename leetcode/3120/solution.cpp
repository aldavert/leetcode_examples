#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfSpecialChars(std::string word)
{
    char check[26] = {};
    for (char letter : word)
    {
        bool lower = letter >= 'a';
        char start = static_cast<char>('A' + lower * ('a' - 'A'));
        check[letter - start] = check[letter - start] | (1 + lower);
    }
    int result = 0;
    for (int i = 0; i < 26; ++i)
        result += check[i] == 3;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfSpecialChars(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aaAbcBC", 3, trials);
    test("abc", 0, trials);
    test("abBCab", 1, trials);
    return 0;
}


