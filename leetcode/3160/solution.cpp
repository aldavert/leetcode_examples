#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> queryResults([[maybe_unused]] int limit,
                              std::vector<std::vector<int> > queries)
{
    std::unordered_map<int, int> ball_to_color, color_count;
    std::vector<int> result;
    for (const auto &query : queries)
    {
        if (const auto it = ball_to_color.find(query[0]); it != ball_to_color.cend())
        {
            const int prev_color = it->second;
            if (--color_count[prev_color] == 0)
                color_count.erase(prev_color);
        }
        ball_to_color[query[0]] = query[1];
        ++color_count[query[1]];
        result.push_back(static_cast<int>(color_count.size()));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int limit,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = queryResults(limit, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{1, 4}, {2, 5}, {1, 3}, {3, 4}}, {1, 2, 2, 3}, trials);
    test(4, {{0, 1}, {1, 2}, {2, 2}, {3, 4}, {4, 5}}, {1, 2, 2, 3, 4}, trials);
    return 0;
}


