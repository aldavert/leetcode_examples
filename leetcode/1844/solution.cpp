#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string replaceDigits(std::string s)
{
    auto shift = [](char c, int v) -> char
    {
        return static_cast<char>(((c - 'a') + v) % 26 + 'a');
    };
    std::string result;
    const size_t n = s.size();
    size_t i;
    for (i = 0; i < n - 1; i += 2)
    {
        result += s[i];
        result += shift(s[i], static_cast<int>(s[i + 1] - '0'));
    }
    for (; i < n; ++i) result += s[i];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = replaceDigits(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("a1c1e1", "abcdef", trials);
    test("a1b2c3d4e", "abbdcfdhe", trials);
    return 0;
}


