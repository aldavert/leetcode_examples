#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

long long maximizeSumOfWeights(std::vector<std::vector<int> > edges, int k)
{
    const int n = static_cast<int>(edges.size()) + 1;
    std::vector<std::vector<std::pair<int, int> > > graph(n);
    for (const auto &edge : edges)
    {
        graph[edge[0]].emplace_back(edge[1], edge[2]);
        graph[edge[1]].emplace_back(edge[0], edge[2]);
    }
    auto dfs = [&](auto &&self, int u, int prev) -> std::pair<long, long>
    {
        std::priority_queue<long> diffs;
        long weight_sum = 0;
        
        for (const auto& [v, w] : graph[u])
        {
            if (v == prev) continue;
            const auto [sub_k1, sub_k] = self(self, v, u);
            weight_sum += sub_k;
            diffs.push(std::max(0L, sub_k1 - sub_k + w));
        }
        long top_k1 = 0, top_k = 0;
        for (int i = 0; (i < k) && (!diffs.empty()); ++i)
        {
            if (i < k - 1) top_k1 += diffs.top();
            top_k += diffs.top();
            diffs.pop();
        }
        return { weight_sum + top_k1, weight_sum + top_k};
    };
    return dfs(dfs, 0, -1).second;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges,
          int k,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximizeSumOfWeights(edges, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 4}, {0, 2, 2}, {2, 3, 12}, {2, 4, 6}}, 2, 22, trials);
    test({{0, 1, 5}, {1, 2, 10}, {0, 3, 15}, {3, 4, 20}, {3, 5, 5}, {0, 6, 10}},
           3, 65, trials);
    return 0;
}



