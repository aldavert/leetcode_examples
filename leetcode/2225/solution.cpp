#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > findWinners(std::vector<std::vector<int> > matches)
{
    std::vector<std::vector<int> > result(2);
    std::unordered_map<int, int> lossers;
    for (const auto &value : matches)
    {
        ++lossers[value[1]];
        if (auto search = lossers.find(value[0]); search == lossers.end())
            lossers[value[0]] = 0;
    }
    for (auto [team, freq] : lossers)
    {
        if (freq == 0) result[0].push_back(team);
        else if (freq == 1) result[1].push_back(team);
    }
    std::sort(result[0].begin(), result[0].end());
    std::sort(result[1].begin(), result[1].end());
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matches,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findWinners(matches);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 3}, {2, 3}, {3, 6}, {5, 6}, {5, 7}, {4, 5}, {4, 8}, {4, 9}, {10, 4},
          {10, 9}}, {{1, 2, 10}, {4, 5, 7, 8}}, trials);
    test({{2, 3}, {1, 3}, {5, 4}, {6, 4}}, {{1, 2, 5, 6}, {}}, trials);
    return 0;
}


