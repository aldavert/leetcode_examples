#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int maxCoins(std::vector<int> nums)
{
    if (nums.size() == 1)
        return nums[0];
    else if (nums.size() == 2)
        return nums[0] * nums[1] + std::max(nums[0], nums[1]);
    int values[502] = {}, dp[502][502] = {}, n = 0;
    for (int v : nums)
        if (v != 0) ++n;
    values[0] = values[n + 1] = 1;
    for (int idx = 1; int v : nums)
        if (v != 0)
            values[idx++] = v;
     
    auto update = [&](int left, int right, int last) -> void
    {
        int new_value = dp[left][last - 1]
                      + values[left - 1] * values[last] * values[right + 1]
                      + dp[last + 1][right];
        dp[left][right] = std::max(dp[left][right], new_value);
    };
    for (int length = 1; length < n + 1; ++length)
        for (int left = 1; left < n - length + 2; ++left)
            for (int last = left, right = left + length - 1; last < right + 1; ++last)
                update(left, right, last);
    return dp[1][n];
}
#else
int maxCoins(std::vector<int> nums)
{
    if (nums.size() == 1)
        return nums[0];
    else if (nums.size() == 2)
        return nums[0] * nums[1] + std::max(nums[0], nums[1]);
    
    int n = 0;
    for (int v : nums)
        if (v != 0) ++n;
    std::vector<int> values(n + 2);
    values[0] = values[n + 1] = 1;
    for (int idx = 1; int v : nums)
        if (v != 0)
            values[idx++] = v;
     
    std::vector<std::vector<int> > dp(n + 2, std::vector<int>(n + 2, 0));
    auto update = [&](int left, int right, int last) -> void
    {
        int new_value = dp[left][last - 1]
                      + values[left - 1] * values[last] * values[right + 1]
                      + dp[last + 1][right];
        dp[left][right] = std::max(dp[left][right], new_value);
    };
    for (int length = 1; length < n + 1; ++length)
        for (int left = 1; left < n - length + 2; ++left)
            for (int last = left, right = left + length - 1; last < right + 1; ++last)
                update(left, right, last);
    return dp[1][n];
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxCoins(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1, 5, 8}, 167, trials);
    test({1, 5}, 10, trials);
    test({7, 9, 8, 0, 7, 1, 3, 5}, 1358, trials);
    return 0;
}


