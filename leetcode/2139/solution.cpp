#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minMoves(int target, int maxDoubles)
{
    int steps = 0;
    while ((target > 1) && maxDoubles)
    {
        if (target % 2 == 1) --target;
        else
        {
            target /= 2;
            --maxDoubles;
        }
        ++steps;
    }
    return steps + target - 1;
}

// ############################################################################
// ############################################################################

void test(int target, int maxDoubles, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minMoves(target, maxDoubles);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 0, 4, trials);
    test(19, 2, 7, trials);
    test(10, 4, 4, trials);
    return 0;
}


