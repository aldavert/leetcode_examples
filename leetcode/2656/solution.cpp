#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int maximizeSum(std::vector<int> nums, int k)
{
    int m = nums[0];
    for (size_t i = 1, n = nums.size(); i < n; ++i)
        if (nums[i] > m)
            m = nums[i];
    return ((m + k) * (m + k - 1) - m * (m - 1)) / 2;
}
#else
int maximizeSum(std::vector<int> nums, int k)
{
    int m = *std::max_element(nums.begin(), nums.end());
    return ((m + k) * (m + k - 1) - m * (m - 1)) / 2;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximizeSum(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 3, 18, trials);
    test({5, 5, 5}, 2, 11, trials);
    return 0;
}


