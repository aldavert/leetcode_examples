#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countHillValley(std::vector<int> nums)
{
    const size_t n = nums.size() - 1;
    int result = 0;
    for (size_t left = 1, right = 1; left < n; )
    {
        while ((right < n) && (nums[left] == nums[right])) ++right;
        result += ((nums[left - 1] < nums[left]) && (nums[right] < nums[left]))
               || ((nums[left - 1] > nums[left]) && (nums[right] > nums[left]));
        left = right;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countHillValley(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4, 1, 1, 6, 5}, 3, trials);
    test({6, 6, 5, 5, 4, 1}, 0, trials);
    return 0;
}


