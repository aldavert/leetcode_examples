#include "../common/common.hpp"
#include <bitset>
#include <queue>

// ############################################################################
// ############################################################################

int cutOffTree(std::vector<std::vector<int> > forest)
{
    constexpr int WMAX = 51;
    struct Destination
    {
        int height = 0;
        int y = -1;
        int x = -1;
        bool operator<(const Destination &other) const { return height < other.height; }
    };
    const int h = static_cast<int>(forest.size());
    const int w = static_cast<int>(forest[0].size());
    std::vector<std::bitset<WMAX> > visited(h), obstacles(h);
    for (int y = 0; y < h; ++y)
        for (int x = 0; x < w; ++x)
            obstacles[y][x] = forest[y][x] == 0;
    std::vector<Destination> trees;
    auto findPath = [&](Destination src, Destination dst) -> int
    {
        visited = obstacles;
        std::queue<Destination> search;
        search.push(src);
        for (int distance = 0; !search.empty(); ++distance)
        {
            for (int i = 0, n = static_cast<int>(search.size()); i < n; ++i)
            {
                auto [_, y, x] = search.front();
                search.pop();
                if (visited[y][x]) continue;
                visited[y][x] = true;
                if ((y == dst.y) && (x == dst.x)) return distance;
                if ((y - 1 >= 0) && (!visited[y - 1][x]))
                    search.push({0, y - 1, x});
                if ((y + 1 <  h) && (!visited[y + 1][x]))
                    search.push({0, y + 1, x});
                if ((x - 1 >= 0) && (!visited[y][x - 1]))
                    search.push({0, y, x - 1});
                if ((x + 1 <  w) && (!visited[y][x + 1]))
                    search.push({0, y, x + 1});
            }
        }
        return -1;
    };
    int steps = 0;
    for (int y = 0; y < h; ++y)
        for (int x = 0; x < w; ++x)
            if (forest[y][x] > 1)
                trees.push_back({forest[y][x], y, x});
    std::sort(trees.begin(), trees.end());
    for (Destination previous = {0, 0, 0}; Destination current : trees)
    {
        int solution = findPath(previous, current);
        if (solution < 0) return -1;
        steps += solution;
        previous = current;
    }
    
    return steps;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > forest, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = cutOffTree(forest);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {0, 0, 4}, {7, 6, 5}}, 6, trials);
    test({{1, 2, 3}, {0, 0, 0}, {7, 6, 5}}, -1, trials);
    test({{2, 3, 4}, {0, 0, 5}, {8, 7, 6}}, 6, trials);
    return 0;
}


