# Construct Binary Tree form Preorder and Postorder Traversal

Given two integer arrays, `preorder` and `postorder` where `preorder` is the preorder traversal of a binary tree of **distinct** values and `postorder` is the postorder traversal of the same tree, reconstruct and return *the binary tree*.

If there exist multiple answers, you can **return any** of them.

#### Example 1:
> ```mermaid 
> graph TD;
> A((1))-->B((2))
> A-->C((3))
> B-->D((4))
> B-->E((5))
> C-->F((6))
> C-->G((7))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `preorder = [1, 2, 4, 5, 3, 6, 7], postorder = [4, 5, 2, 6, 7, 3, 1]`  
> *Output:* `[1, 2, 3, 4, 5, 6, 7]`

#### Example 2:
> *Input:* `preorder = [1], postorder = [1]`  
> *Output:* `[1]`

#### Constraints:
- `1 <= preorder.length <= 30`
- `1 <= preorder[i] <= preorder.length`
- All the values of `preorder` are unique.
- `postorder.length == preorder.length`
- `1 <= postorder[i] <= postorder.length`
- All the values of `postorder` are unique.
- It is guaranteed that `preorder` and `postorder` are the preorder traversal and postorder traversal of the same binary tree.


