#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

TreeNode * constructFromPrePost(std::vector<int> preorder, std::vector<int> postorder)
{
    auto build = [&](void) -> TreeNode *
    {
        const int n = static_cast<int>(preorder.size());
        std::unordered_map<int, int> post_to_index;
        for (int i = 0; i < n; ++i)
            post_to_index[postorder[i]] = i;
        auto inner =
            [&](auto &&self, int pre_s, int pre_e, int post_s, int post_e) -> TreeNode *
        {
            if (pre_s > pre_e) return nullptr;
            if (pre_s == pre_e) return new TreeNode(preorder[pre_s]);
            int partition = post_to_index[preorder[pre_s + 1]],
                size = partition - post_s + 1;
            TreeNode * root = new TreeNode(preorder[pre_s]);
            root->left = self(self, pre_s + 1, pre_s + size, post_s, partition);
            root->right = self(self, pre_s + size + 1, pre_e, partition + 1, post_e - 1);
            return root;
        };
        return inner(inner, 0, n - 1, 0, n - 1);
    };
    return build();
}

// ############################################################################
// ############################################################################

void test(std::vector<int> preorder,
          std::vector<int> postorder,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = constructFromPrePost(preorder, postorder);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 4, 5, 3, 6, 7}, {4, 5, 2, 6, 7, 3, 1}, {1, 2, 3, 4, 5, 6, 7}, trials);
    test({1}, {1}, {1}, trials);
    return 0;
}


