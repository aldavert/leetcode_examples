#include "../common/common.hpp"

// #############################################################################
// #############################################################################

#if 1
int longestStrChain(std::vector<std::string> &words)
{
    auto isPredecessor =
        [](const std::string &str_long, const std::string &str_int) -> bool
    {
        bool skip = false;
        for (unsigned int i = 0, j = 0; i < str_long.size(); ++i)
        {
            if (str_long[i] != str_int[j])
            {
                if (skip) return false;
                skip = true;
            }
            else ++j;
        }
        return true;
    };
    std::vector<std::pair<int, int> > length(words.size());
    int lhist[17] = {};
    for (size_t i = 0; i < words.size(); ++i)
    {
        const int s = static_cast<int>(words[i].size());
        length[i] = {s, i};
        ++lhist[s];
    }
    int max_hist = 0;
    for (int i = 0; i < 17; ++i)
        max_hist = std::max(max_hist, lhist[i]);
    std::vector<int> chain_length[2] = {
        std::vector<int>(max_hist, 0),
        std::vector<int>(max_hist, 0) };
    std::sort(length.begin(), length.end());
    int max_size = length[words.size() - 1].first;
    int min_size = length[0].first;
    
    int longest_chain = 0;
    bool current_chain = false;
    for (int s = max_size, p = static_cast<int>(words.size()) - 1; s > min_size; --s)
    {
        int n = p - lhist[s];
        int e = n - lhist[s - 1];
        for (int j = n; j > e; --j)
        {
            chain_length[current_chain][n - j] = 0;
            for (int i = p; i > n; --i)
                if (isPredecessor(words[length[i].second], words[length[j].second]))
                    chain_length[current_chain][n - j] =
                        std::max(chain_length[!current_chain][p - i] + 1,
                                 chain_length[current_chain][n - j]);
        }
        for (int j = n; j > e; --j)
            longest_chain = std::max(longest_chain, chain_length[current_chain][n - j]);
        p -= lhist[s];
        current_chain = !current_chain;
    }
    
    return longest_chain + 1;
}
#else
bool isPredecessor(const std::string &str_long, const std::string &str_int)
{
    bool skip = false;
    for (unsigned int i = 0, j = 0; i < str_long.size(); ++i)
    {
        if (str_long[i] != str_int[j])
        {
            if (skip) return false;
            skip = true;
        }
        else ++j;
    }
    return true;
}
// words.length <= 1000
// words[i].length <= 16
int longestStrChain(std::vector<std::string> &words)
{
    const int n = static_cast<int>(words.size());
    std::vector<std::pair<int, int> > length(words.size());
    int lhist[17] = {};
    for (int i = 0; i < n; ++i)
    {
        const int s = static_cast<int>(words[i].size());
        length[i] = {s, i};
        ++lhist[s];
    }
    int max_hist = 0;
    for (int i = 0; i < 17; ++i)
        if (lhist[i] > max_hist)
            max_hist = lhist[i];
    std::vector<int> chain_length[2] = {
        std::vector<int>(max_hist), std::vector<int>(max_hist) };
    for (int i = 0; i < max_hist; ++i)
        chain_length[0][i] = chain_length[1][i] = 0;
    std::sort(length.begin(), length.end());
    int max_size = length[words.size() - 1].first;
    int min_size = length[0].first;
    
    int longest_chain = 0;
    bool current_chain = false;
    for (int s = max_size, p = n - 1; s > min_size; --s)
    {
        int m = p - lhist[s];
        int e = m - lhist[s - 1];
        for (int j = m; j > e; --j)
        {
            chain_length[current_chain][m - j] = 0;
            for (int i = p; i > m; --i)
                if (isPredecessor(words[length[i].second], words[length[j].second]))
                    chain_length[current_chain][m - j] =
                        std::max(chain_length[!current_chain][p - i] + 1,
                                 chain_length[current_chain][m - j]);
        }
        for (int j = m; j > e; --j)
            longest_chain = std::max(longest_chain, chain_length[current_chain][m - j]);
        p -= lhist[s];
        current_chain = !current_chain;
    }
    
    return longest_chain + 1;
}
#endif

// #############################################################################
// #############################################################################

void test(std::vector<std::string> words, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestStrChain(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"a", "b", "ba", "bca", "bda", "bdca"}, 4, trials);
    test({"xbc", "pcxbcf", "xb", "cxbc", "pcxbc"}, 5, trials);
    test({"sanfrancisca", "bcela", "bacelona", "sanfranciscosa", "barcelona",
          "sanfrancisco", "bacelna", "sanfranciscos", "l", "cela", "cla", "cl",
          "bcelna", "fadslkajfld"}, 9, trials);
    return 0;
}


