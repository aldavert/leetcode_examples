#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > mergeArrays(std::vector<std::vector<int> > nums1,
                                           std::vector<std::vector<int> > nums2)
{
    const size_t n1 = nums1.size(), n2 = nums2.size();
    size_t i1 = 0, i2 = 0;
    std::vector<std::vector<int> > result;
    while ((i1 < n1) && (i2 < n2))
    {
        if (nums1[i1][0] < nums2[i2][0])
            result.push_back(nums1[i1++]);
        else if (nums1[i1][0] > nums2[i2][0])
            result.push_back(nums2[i2++]);
        else
        {
            result.push_back({nums1[i1][0], nums1[i1][1] + nums2[i2][1]});
            ++i1;
            ++i2;
        }
    }
    for (; i1 < n1; ++i1) result.push_back(nums1[i1]);
    for (; i2 < n2; ++i2) result.push_back(nums2[i2]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > nums1,
          std::vector<std::vector<int> > nums2,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = mergeArrays(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 3}, {4, 5}}, {{1, 4}, {3, 2}, {4, 1}},
         {{1, 6}, {2, 3}, {3, 2}, {4, 6}}, trials);
    test({{2, 4}, {3, 6}, {5, 5}}, {{1, 3}, {4, 3}},
         {{1, 3}, {2, 4}, {3, 6}, {4, 3}, {5, 5}}, trials);
    return 0;
}


