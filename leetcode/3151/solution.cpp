#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isArraySpecial(std::vector<int> nums)
{
    for (size_t i = 1; i < nums.size(); ++i)
        if (nums[i] % 2 == nums[i - 1] % 2)
            return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isArraySpecial(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1}, true, trials);
    test({2, 1, 4}, true, trials);
    test({4, 3, 1, 6}, false, trials);
    return 0;
}


