#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberWays(std::vector<std::vector<int> > hats)
{
    constexpr int MOD = 1'000'000'007;
    constexpr int HATS = 40;
    const int n = static_cast<int>(hats.size());
    const int n_assignments = 1 << n;
    std::vector<std::vector<int> > hat_to_people(HATS + 1);
    std::vector<int> dp(n_assignments);
    dp[0] = 1;
    
    for (int i = 0; i < n; ++i)
        for (int hat : hats[i])
            hat_to_people[hat].push_back(i);
    for (int h = 1; h <= HATS; ++h)
        for (int j = n_assignments - 1; j >= 0; --j)
            for (int p : hat_to_people[h])
                if (j & 1 << p)
                dp[j] = (dp[j] + dp[j ^ 1 << p]) % MOD;
    return dp[n_assignments - 1];
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > hats, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberWays(hats);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 4}, {4, 5}, {5}}, 1, trials);
    test({{3, 5, 1}, {3, 5}}, 4, trials);
    test({{1, 2, 3, 4}, {1, 2, 3, 4}, {1, 2, 3, 4}, {1, 2, 3, 4}}, 24, trials);
    return 0;
}


