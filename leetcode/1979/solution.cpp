#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int findGCD(std::vector<int> nums)
{
    int minimum = nums[0], maximum = nums[0];
    for (size_t i = 1, n = nums.size(); i < n; ++i)
    {
        minimum = std::min(minimum, nums[i]);
        maximum = std::max(maximum, nums[i]);
    }
    return std::gcd(minimum, maximum);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findGCD(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 5, 6, 9, 10}, 2, trials);
    test({7, 5, 6, 8, 3}, 1, trials);
    test({3, 3}, 3, trials);
    return 0;
}


