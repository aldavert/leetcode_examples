#include "../common/common.hpp"
#include <variant>
#include <unordered_map>
#include <random>

constexpr int null = -1'000;

// ############################################################################
// ############################################################################

class RandomizedSet
{
    std::unordered_map<int, int> m_lut;
    std::vector<int> m_data;
    std::random_device rd;
    std::mt19937 gen;
public:
    RandomizedSet() :
        gen(rd())
    {
    }
    bool insert(int val)
    {
        if (m_lut.find(val) != m_lut.end())
            return false;
        m_lut[val] = static_cast<int>(m_data.size());
        m_data.push_back(val);
        return true;
    }
    bool remove(int val)
    {
        if (auto it = m_lut.find(val); it != m_lut.end())
        {
            m_data[it->second] = m_data.back();
            m_lut[m_data.back()] = it->second;
            m_lut.erase(it);
            m_data.pop_back();
            return true;
        }
        return false;
    }
    int getRandom()
    {
        const int max = static_cast<int>(m_data.size()) - 1;
        std::uniform_int_distribution<> distribution(0, max);
        return m_data[distribution(gen)];
    }
};

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::variant<bool, int> > &left,
                const std::vector<std::variant<bool, int> > &right)
{
    if (left.size() == 0) return right.size() == 0;
    if (std::holds_alternative<bool>(left[0]))
    {
        if ((left.size() != 1) || (right.size() != 1))
            return false;
        return left[0] == right[0];
    }
    else
    {
        if (left.size() == 1)
        {
            bool found = false;
            int element = std::get<int>(left[0]);
            for (const auto &v : right)
            {
                if (element == std::get<int>(v))
                {
                    found = true;
                    break;
                }
            }
            return found;
        }
        else if (right.size() == 1)
        {
            bool found = false;
            int element = std::get<int>(right[0]);
            for (const auto &v : left)
            {
                if (element == std::get<int>(v))
                {
                    found = true;
                    break;
                }
            }
            return found;
        }
        else return false;
    }
}

bool operator==(const std::vector<std::vector<std::variant<bool, int> > > &left,
                const std::vector<std::vector<std::variant<bool, int> > > &right)
{
    if (left.size() != right.size()) return false;
    const int n = static_cast<int>(left.size());
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

enum class OP { RANDOMIZEDSET, INSERT, REMOVE, GETRANDOM };

void test(std::vector<OP> operation,
          std::vector<int> values,
          std::vector<std::vector<std::variant<bool, int> > > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<std::variant<bool, int> > > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        RandomizedSet * rs = nullptr;
        std::vector<std::vector<std::variant<bool, int> > > current_result;
        const int n_op = static_cast<int>(operation.size());
        for (int j = 0; j < n_op; ++j)
        {
            switch (operation[j])
            {
            case OP::RANDOMIZEDSET:
                current_result.push_back({});
                delete rs;
                rs = new RandomizedSet();
                break;
            case OP::INSERT:
                current_result.push_back({rs->insert(values[j])});
                break;
            case OP::REMOVE:
                current_result.push_back({rs->remove(values[j])});
                break;
            case OP::GETRANDOM:
                current_result.push_back({rs->getRandom()});
                break;
            default:
                std::cerr << "[ERROR] AN UNKNOWN OPERATION HAS BEEN FOUND!!\n";
                return;
            }
        }
        delete rs;
        result = current_result;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::RANDOMIZEDSET, OP::INSERT, OP::REMOVE, OP::INSERT, OP::GETRANDOM,
          OP::REMOVE, OP::INSERT, OP::GETRANDOM},
         {null, 1, 2, 2, null, 1, 2, null},
         {{}, {true}, {false}, {true}, {1, 2}, {true}, {false}, {2}}, trials);
    return 0;
}


