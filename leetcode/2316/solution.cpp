#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long countPairs(int n, std::vector<std::vector<int> > edges)
{
    std::vector<int> id(n), lut(n + 1, n);
    auto find = [&](auto &&self, int idx) -> int
    {
        return (id[idx] != idx)?id[idx] = self(self, id[idx]):idx;
    };
    for (int i = 0; i < n; ++i) id[i] = i;
    for (const auto &edge : edges)
    {
        int u = find(find, edge[0]), v = find(find, edge[1]);
        id[u] = id[v] = std::min(u, v);
    }
    std::vector<int> histogram;
    for (int i = 0; i < n; ++i)
    {
        if (id[i] == i)
            lut[i] = static_cast<int>(histogram.size()),
            histogram.push_back(1);
        else ++histogram[lut[find(find, i)]];
    }
    const int m = static_cast<int>(histogram.size());
    long long result = 0;
    std::vector<int> accumulate(m);
    for (int previous = 0, i = m - 1; i >= 0; --i)
        accumulate[i] = previous,
        previous += histogram[i];
    for (int i = 0; i < m; ++i)
        result += static_cast<long long>(accumulate[i])
                * static_cast<long long>(histogram[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPairs(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{0, 1}, {0, 2}, {1, 2}}, 0, trials);
    test(7, {{0, 2}, {0, 5}, {2, 4}, {1, 6}, {5, 4}}, 14, trials);
    return 0;
}


