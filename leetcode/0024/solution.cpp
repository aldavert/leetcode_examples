#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

ListNode * swapPairs(ListNode * head)
{
    if ((head == nullptr) || (head->next == nullptr))
        return head;
    ListNode * current = head;
    head = head->next;
    for (ListNode * previous = nullptr; current && current->next;)
    {
        ListNode * following = current->next;
        if (previous)
            previous->next = following;
        current->next = following->next;
        following->next = current;
        
        previous = current;
        current = current->next;
    }
    return head;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> input, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * root = vec2list(input);
        root = swapPairs(root);
        result = list2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, {2, 1, 4, 3}, trials);
    test({}, {}, trials);
    test({1}, {1}, trials);
    return 0;
}


