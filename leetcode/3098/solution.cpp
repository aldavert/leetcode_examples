#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int sumOfPowers(std::vector<int> nums, int k)
{
    std::unordered_map<long long, int> mem;
    const int MOD = 1'000'000'007;
    int n = static_cast<int>(nums.size());
    sort(nums.begin(), nums.end());
    auto dfs = [&](auto &&self, int i, int j, int nk, int mi) -> int {
        if (i >= n) return (nk == 0)?mi:0;
        if (n - i < nk) return 0;
        long long key = (1LL * mi) << 18 | (i << 12) | (j << 6) | nk;
        if (mem.contains(key)) return mem[key];
        long long result = (self(self, i + 1, j, nk, mi)
                          + self(self, i + 1, i, nk - 1,
                              (j == n)?mi:std::min(mi, nums[i] - nums[j]))) % MOD;
        return mem[key] = static_cast<int>(result);
    };
    return dfs(dfs, 0, n, k, std::numeric_limits<int>::max());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOfPowers(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 3, 4, trials);
    test({2, 2}, 2, 0, trials);
    test({4, 3, -1}, 2, 10, trials);
    return 0;
}


