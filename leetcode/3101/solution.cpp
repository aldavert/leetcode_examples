#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

long long countAlternatingSubarrays(std::vector<int> nums)
{
    std::vector<long> dp(nums.size(), 1);
    for (size_t i = 1; i < nums.size(); ++i)
        if (nums[i] != nums[i - 1])
            dp[i] += dp[i - 1];
    return std::accumulate(dp.begin(), dp.end(), 0L);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countAlternatingSubarrays(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 1, 1}, 5, trials);
    test({1, 0, 1, 0}, 10, trials);
    return 0;
}


