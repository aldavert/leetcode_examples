#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<bool> prefixesDivBy5(std::vector<int> nums)
{
    std::vector<bool> result;
    for (int value = 0; int bit : nums)
        result.push_back((value = ((value << 1) | (bit == 1)) % 5) == 0);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = prefixesDivBy5(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 1}, {true, false, false}, trials);
    test({1, 1, 1}, {false, false, false}, trials);
    test({0, 1, 1, 1, 1, 1}, {true, false, false, false, true, false}, trials);
    return 0;
}


