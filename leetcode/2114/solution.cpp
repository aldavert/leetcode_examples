#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int mostWordsFound(std::vector<std::string> sentences)
{
    int result = 0;
    for (size_t i = 0, n = sentences.size(); i < n; ++i)
    {
        int spaces = 0;
        for (size_t j = 0, m = sentences[i].size(); j < m; ++j)
            spaces += sentences[i][j] == ' ';
        result = std::max(result, spaces + 1);
    }
    return result;
}
#else
int mostWordsFound(std::vector<std::string> sentences)
{
    int result = 0;
    for (const auto &sentence : sentences)
    {
        int spaces = 0;
        for (char letter : sentence)
            spaces += letter == ' ';
        result = std::max(result, spaces + 1);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> sentences,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = mostWordsFound(sentences);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"alice and bob love leetcode", "i think so too", "this is great thanks very much"}, 6, trials);
    test({"please wait", "continue to fight", "continue to win"}, 3, trials);
    return 0;
}



