# Binary Tree Cameras

Given a binary tree, we install cameras on the nodes of the tree. 
Each camera at a node can monitor **its parent, itself, and its immediate children**.
Calculate the minimum number of cameras needed to monitor all nodes of the tree.

#### Example 1:
> ```mermaid
> graph TD;
> A(( &nbsp ))---B(( C ))
> A---EMPTY(( ))
> B---C(( &nbsp ))
> B---D(( &nbsp ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY empty;
> linkStyle 1 stroke-width:0px;
> ```
> *Input:* `[0, 0, null, 0, 0]`  
> *Output:* `1`  
> *Explanation:* One camera is enough to monitor all nodes if placed as show.

#### Example 2:
> ```mermaid
> graph TD
> A(( &nbsp  ))---B(( C ))
> A---EMPTY1(( ))
> B---C(( &nbsp ))
> B---EMPTY2(( ))
> C---D(( C ))
> C---EMPTY3(( ))
> D---EMPTY4(( ))
> D---E(( &nbsp ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY1,EMPTY2,EMPTY3,EMPTY4 empty;
> linkStyle 1,3,5,6 stroke-width:0px;
> ```
> *Input:* `[0, 0, null, 0, null, 0, null, null, 0]`  
> *Output:* `2`  
> *Explanation:* At least two cameras are needed to monitor all nodes of the tree. The above image shows one of the valid configurations of camera placement.

#### Note:
- The number of nodes in the given tree will be in the range `[1, 1000]`.
- **Every** node has value 0.


