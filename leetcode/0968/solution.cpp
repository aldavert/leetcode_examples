#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int minCameraCover(TreeNode* root)
{
    auto search = [&](auto &&self, TreeNode * current, int &count) -> char
    {
        if (current == nullptr) return 0;
        int status = self(self, current->left, count)
                   + self(self, current->right, count);
        if (status == 0) return 3;
        if (status < 3) return 0;
        ++count;
        return 1;
    };
    int count = 0;
    int status = search(search, root, count);
    return count + (status > 2);
}

// ############################################################################
// ############################################################################

void test(const std::vector<int> &tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = minCameraCover(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 0, null, 0, 0}, 1, trials);
    test({0,0,null,0,null,0,null,null,0}, 2, trials);
    //        0
    //    C       C
    //  0   0   0   0
    test({0, 0, 0, 0, 0, 0, 0}, 2, trials);
    //        C
    //    0       0
    //  C   C   C   C
    // 0 0 0 0 0 0 0 0
    test({0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 5, trials);
    return 0;
}


