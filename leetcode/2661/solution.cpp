#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int firstCompleteIndex(std::vector<int> arr, std::vector<std::vector<int> > mat)
{
    const int m = static_cast<int>(mat.size());
    const int n = static_cast<int>(mat[0].size());
    std::vector<int> rows(m), cols(n), num_to_row(m * n + 1), num_to_col(m * n + 1);
    
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            num_to_row[mat[i][j]] = i,
            num_to_col[mat[i][j]] = j;
    for (int i = 0, end = static_cast<int>(arr.size()); i < end; ++i)
    {
        if (++rows[num_to_row[arr[i]]] == n) return i;
        if (++cols[num_to_col[arr[i]]] == m) return i;
    }
    throw;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr,
          std::vector<std::vector<int> > mat,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = firstCompleteIndex(arr, mat);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 4, 2}, {{1, 4}, {2, 3}}, 2, trials);
    test({2, 8, 7, 4, 1, 3, 5, 6, 9}, {{3, 2, 5}, {1, 4, 6}, {8, 7, 9}}, 3, trials);
    return 0;
}


