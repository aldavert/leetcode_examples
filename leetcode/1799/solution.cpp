#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int maxScore(std::vector<int> nums)
{
    const int l = static_cast<int>(nums.size());
    int dp[16'384] = {};
    for (int mask = 0; mask < 1 << l; ++mask)
    {
        int c = __builtin_popcount(mask);
        if (c & 1) continue;
        int k = c / 2 + 1;
        for (int i = 0; i < l; ++i)
        {
            for (int j = i + 1; j < l; ++j)
            {
                if ((mask & (1 << i)) + (mask & (1 << j)) == 0)
                {
                    int new_mask = mask | (1 << i) | (1 << j);            
                    dp[new_mask] = std::max(dp[new_mask],
                    k * std::gcd(nums[i], nums[j]) + dp[mask]);
                }
            }
        }
    }
    return dp[(1 << l) - 1];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxScore(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2}, 1, trials);
    test({3, 4, 6, 8}, 11, trials);
    test({1, 2, 3, 4, 5, 6}, 14, trials);
    return 0;
}


