#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canWinNim(int n)
{
    return n % 4 != 0;
}

// ############################################################################
// ############################################################################

void test(int n, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = canWinNim(n);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, false, trials);
    test(1,  true, trials);
    test(2,  true, trials);
    return 0;
}


