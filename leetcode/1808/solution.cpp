#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxNiceDivisors(int primeFactors)
{
    constexpr int MOD = 1'000'000'007;
    auto pow = [&](auto &&self, long x, int n) -> int
    {
        if (n == 0) return 1;
        if (n & 1) return static_cast<int>(x * self(self, x, n - 1) % MOD);
        return self(self, x * x % MOD, n / 2);
    };
    if (primeFactors     <= 3) return primeFactors;
    long result;
    if      (primeFactors % 3 == 0)
        result =      pow(pow, 3 , primeFactors      / 3) % MOD;
    else if (primeFactors % 3 == 1)
        result = 4L * pow(pow, 3, (primeFactors - 4) / 3) % MOD;
    else
        result = 2L * pow(pow, 3, (primeFactors - 2) / 3) % MOD;
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(int primeFactors, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxNiceDivisors(primeFactors);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 6, trials);
    test(8, 18, trials);
    return 0;
}


