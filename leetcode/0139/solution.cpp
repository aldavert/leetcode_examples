#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool wordBreak(std::string s, std::vector<std::string> wordDict)
{
    const int n = static_cast<int>(s.size());
    std::unordered_set<std::string> wordSet{wordDict.begin(), wordDict.end()};
    std::vector<bool> dp(n + 1);
    dp[0] = true;
    
    for (int i = 1; i <= n; ++i)
    {
        for (int j = 0; j < i; ++j)
        {
            if (dp[j] && (wordSet.find(s.substr(j, i - j)) != wordSet.end()))
            {
                dp[i] = true;
                break;
            }
        }
    }
    return dp[n];
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<std::string> wordDict,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = wordBreak(s, wordDict);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leetcode", {"leet", "code"}, true, trials);
    test("applepenapple", {"apple", "pen"}, true, trials);
    test("catsandog", {"cats", "dog", "sand", "and", "cat"}, false, trials);
    return 0;
}


