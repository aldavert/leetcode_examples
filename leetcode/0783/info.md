# Minimum Distance Between BST Nodes

Given the `root` of a Binary Search Tree (BST), return *the minimum difference between the values of any two different nodes in the tree.*

#### Example 1:
> ```mermaid
> graph TD;
> A((4))---B((2))
> A---C((6))
> B---D((1))
> B---E((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [4, 2, 6, 1, 3]`  
> *Output:* `1`

#### Example 2:
> ```mermaid
> graph TD;
> A((1))---B((0))
> A---C((48))
> C---D((12))
> C---E((49))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [1, 0, 48, null, null, 12, 49]`  
> *Output:* `1`

#### Constraints:
- The number of nodes in the tree is in the range `[2, 100]`.
- `0 <= Node.val <= 10^5`

**Note:** This question is the same as `530`.


