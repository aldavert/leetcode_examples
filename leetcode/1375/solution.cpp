#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numTimesAllBlue(std::vector<int> flips)
{
    int result = 0;
    for (int i = 0, rightmost = 0, n = static_cast<int>(flips.size()); i < n; ++i)
    {
        rightmost = std::max(rightmost, flips[i]);
        result += (rightmost == i + 1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> flips, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numTimesAllBlue(flips);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 4, 1, 5}, 2, trials);
    test({4, 1, 2, 3}, 1, trials);
    return 0;
}


