#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int expressiveWords(std::string s, std::vector<std::string> words)
{
    const int n = static_cast<int>(s.size());
    auto stretchy = [&](const std::string &word) -> bool
    {
        const int m = static_cast<int>(word.size());
        int j = 0;
        for (int i = 0; i < n; ++i)
        {
            if ((j < m) && (s[i] == word[j]))
                ++j;
            else if ((i > 1) && (s[i] == s[i - 1]) && (s[i - 1] == s[i - 2]))
                continue;
            else if ((i > 0) && (i + 1 < n) && (s[i - 1] == s[i]) && (s[i] == s[i + 1]))
                ++i;
            else return false;
        }
        return j == m;
    };
    int result = 0;
    for (const auto &word : words)
        result += stretchy(word);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<std::string> words,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = expressiveWords(s, words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("heeellooo", {"hello", "hi", "helo"}, 1, trials);
    test("zzzzzyyyyy", {"zzyy", "zy", "zyy"}, 3, trials);
    return 0;
}


