#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<int> shortestDistanceAfterQueries(int n,
                                              std::vector<std::vector<int> > queries)
{
    std::vector<std::vector<int> > graph(n);
    std::vector<int> result, dist(n);
    auto bfs = [&](int start) -> void
    {
        std::queue<int> q{{start}};
        while (!q.empty())
        {
            const int u = q.front();
            q.pop();
            for (const int v : graph[u])
            {
                if (dist[u] + 1 < dist[v])
                {
                    dist[v] = dist[u] + 1;
                    q.push(v);
                }
            }
        }
    };
    for (int i = 0; i < n; ++i) dist[i] = i;
    for (int i = 0; i < n - 1; ++i)
        graph[i].push_back(i + 1);
    for (const auto &query : queries)
    {
        graph[query[0]].push_back(query[1]);
        if (dist[query[0]] + 1 < dist[query[1]])
        {
            dist[query[1]] = dist[query[0]] + 1;
            bfs(query[1]);
        }
        result.push_back(dist[n - 1]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestDistanceAfterQueries(n, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{2, 4}, {0, 2}, {0, 4}}, {3, 2, 1}, trials);
    test(4, {{0, 3}, {0, 2}}, {1, 1}, trials);
    return 0;
}


