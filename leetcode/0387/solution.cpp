#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int firstUniqChar(std::string s)
{
    std::vector<std::pair<size_t, size_t> > histogram(26, {0, 0});
    const size_t n = s.size();
    for (size_t i = 0; i < n; ++i)
    {
        if (size_t idx = s[i] - 'a'; histogram[idx].first == 0)
            histogram[idx] = {1, i};
        else ++histogram[idx].first;
    }
    size_t min_position = n;
    for (size_t i = 0; i < 26; ++i)
        if (histogram[i].first == 1)
            min_position = std::min(min_position, histogram[i].second);
    return (min_position < n)?static_cast<int>(min_position):-1;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = firstUniqChar(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(    "leetcode",  0, trials);
    test("loveleetcode",  2, trials);
    test(        "aabb", -1, trials);
    return 0;
}


