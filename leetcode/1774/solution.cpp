#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

#if 1
int closestCost(std::vector<int> baseCosts, std::vector<int> toppingCosts, int target)
{
    std::bitset<20001> f(1), g;
    for (int x : toppingCosts) f |= f << x | f << x * 2;
    for (int x : baseCosts) g |= f << x;
    if (g.test(target)) return target;
    for (int offsite = 1;; ++offsite)
    {
        if ((target - offsite > 0)
        &&  g.test(target - offsite))
            return target - offsite;
        if ((target + offsite) <= 20000
        &&  g.test(target + offsite))
            return target + offsite;
    }
}
#elif 0
int closestCost(std::vector<int> baseCosts, std::vector<int> toppingCosts, int target)
{
    std::unordered_set<int> combinations = {0};
    for (int cost : toppingCosts)
    {
        std::unordered_set<int> new_combs_set;
        for (int c : combinations)
            for (int i = 0; i <= 2; ++i)
                new_combs_set.emplace(c + i * cost);
        combinations = new_combs_set;
    }
    int result = std::numeric_limits<int>::max();
    for (int base : baseCosts)
        for (int c : combinations)
            if (std::pair(std::abs(base + c - target), base + c)
              < std::pair(std::abs(  result - target), result))
            result = base + c;
    return result;
}
#else
int closestCost(std::vector<int> baseCosts, std::vector<int> toppingCosts, int target)
{
    int result = std::numeric_limits<int>::max();
    auto dfs = [&](auto &&self, int i, int current_cost) -> void
    {
        if (std::abs(current_cost - target) < std::abs(result - target))
        result = current_cost;
        if ((i == static_cast<int>(toppingCosts.size())) || (current_cost >= target))
            return;
        for (int k = 0; k < 3; ++k)
            self(self, i + 1, current_cost + k * toppingCosts[i]);
    };
    for (const int base_cost : baseCosts)
        dfs(dfs, 0, base_cost);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> baseCosts,
          std::vector<int> toppingCosts,
          int target,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = closestCost(baseCosts, toppingCosts, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 7}, {3, 4}, 10, 10, trials);
    test({2, 3}, {4, 5, 100}, 18, 17, trials);
    test({3, 10}, {2, 5}, 9, 8, trials);
    test({1}, {8, 10}, 10, 9, trials);
    return 0;
}


