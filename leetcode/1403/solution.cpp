#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

std::vector<int> minSubsequence(std::vector<int> nums)
{
    std::vector<int> result;
    std::sort(nums.begin(), nums.end());
    int sum = std::accumulate(nums.begin(), nums.end(), 0);
    for (int current = 0, j = static_cast<int>(nums.size()) - 1; current <= sum; --j)
    {
        result.push_back(nums[j]);
        current += nums[j];
        sum -= nums[j];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSubsequence(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 3, 10, 9, 8}, {10, 9}, trials);
    test({4, 4, 7, 6, 7}, {7, 7, 6}, trials);
    return 0;
}


