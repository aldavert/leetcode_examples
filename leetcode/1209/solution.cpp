#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 1
std::string removeDuplicates(std::string s, int k)
{
    std::stack<std::pair<char, int> > pool;
    for (char c : s)
    {
        if (pool.empty() || (pool.top().first != c))
            pool.push({c, 1});
        else
        {
            pool.top().second += 1;
            if (pool.top().second == k)
                pool.pop();
        }
    }
    std::string result;
    while (!pool.empty())
    {
        auto [chr, frequency] = pool.top();
        pool.pop();
        for (int f = 0; f < frequency; ++f)
            result += chr;
    }
    std::reverse(result.begin(), result.end());
    return result;
}
#else
std::string removeDuplicates(std::string s, int k)
{
    if (k <= 0) return s;
    if (k == 1) return "";
    std::string result = s;
    for (size_t rs = 0, n = s.size(); rs != n;)
    {
        n = s.size();
        rs = 0;
        char previous = '\0';
        int counter = 0;
        for (size_t i = 0; i < n; ++i)
        {
            if (s[i] == previous) ++counter;
            else
            {
                for (int c = 0; c < counter; ++c, ++rs)
                    result[rs] = previous;
                previous = s[i];
                counter = 1;
            }
            if (counter == k)
            {
                counter = 0;
                previous = '\0';
            }
        }
        for (int c = 0; c < counter; ++c, ++rs)
            result[rs] = previous;
        if (rs != n) [[likely]]
        {
            result = result.substr(0, rs);
            s = result;
        }
    };
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, int k, std::string solution, unsigned int trials = 1)
{
    std::string result = "";
    for (unsigned int i = 0; i < trials; ++i)
        result = removeDuplicates(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcd", 2, "abcd", trials);
    test("deeedbbcccbdaa", 3, "aa", trials);
    test("pbbcggttciiippooaais", 2, "ps", trials);
    return 0;
}


