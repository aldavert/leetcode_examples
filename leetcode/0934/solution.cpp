#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int shortestBridge(std::vector<std::vector<int> > grid)
{
    const int n = static_cast<int>(grid.size());
    std::vector<std::vector<bool> > visited(n, std::vector<bool>(n));
    std::queue<std::pair<int, int> > queue_distance, queue_island;
    auto searchLand = [&](void) -> std::pair<int, int>
    {
        for (int row = 0; row < n; ++row)
            for (int col = 0; col < n; ++col)
                if (grid[row][col])
                    return {row, col};
        return {0, 0};
    };
    // Search island.
    queue_island.push(searchLand());
    // Find island border.
    while (!queue_island.empty())
    {
        auto [r, c] = queue_island.front();
        queue_island.pop();
        if (visited[r][c]) continue;
        visited[r][c] = true;
        if (!grid[r][c])
        {
            queue_distance.push({r, c});
            continue;
        }
        if ((r     > 0) && (!visited[r - 1][c])) queue_island.push({r - 1, c});
        if ((r + 1 < n) && (!visited[r + 1][c])) queue_island.push({r + 1, c});
        if ((c     > 0) && (!visited[r][c - 1])) queue_island.push({r, c - 1});
        if ((c + 1 < n) && (!visited[r][c + 1])) queue_island.push({r, c + 1});
    }
    // Reset border.
    for (size_t n_border = queue_distance.size(); n_border > 0; --n_border)
    {
        auto [r, c] = queue_distance.front();
        queue_distance.pop();
        visited[r][c] = false;
        queue_distance.push({r, c});
    }
    // Breadth-first search the other island to find the shortest path.
    for (int distance = 0; !queue_distance.empty(); ++distance)
    {
        for (size_t n_border = queue_distance.size(); n_border > 0; --n_border)
        {
            auto [r, c] = queue_distance.front();
            queue_distance.pop();
            if (visited[r][c]) continue;
            visited[r][c] = true;
            if (grid[r][c]) return distance;
            if ((r     > 0) && (!visited[r - 1][c])) queue_distance.push({r - 1, c});
            if ((r + 1 < n) && (!visited[r + 1][c])) queue_distance.push({r + 1, c});
            if ((c     > 0) && (!visited[r][c - 1])) queue_distance.push({r, c - 1});
            if ((c + 1 < n) && (!visited[r][c + 1])) queue_distance.push({r, c + 1});
        }
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestBridge(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1},
          {1, 0}}, 1, trials);
    test({{0, 1, 0},
          {0, 0, 0},
          {0, 0, 1}}, 2, trials);
    test({{1, 1, 1, 1, 1},
          {1, 0, 0, 0, 1},
          {1, 0, 1, 0, 1},
          {1, 0, 0, 0, 1},
          {1, 1, 1, 1, 1}}, 1, trials);
    return 0;
}


