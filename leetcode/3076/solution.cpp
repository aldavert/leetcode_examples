#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> shortestSubstrings(std::vector<std::string> arr)
{
    int n = static_cast<int>(arr.size());
    std::vector<std::string> result(n);
    for (int i = 0; i < n; ++i)
    {
        int m = static_cast<int>(arr[i].size());
        for (int j = 1; (j <= m) && result[i].empty(); ++j)
        {
            for (int l = 0; l <= m - j; ++l)
            {
                std::string sub = arr[i].substr(l, j);
                if (result[i].empty() || (sub < result[i]))
                {
                    bool ok = true;
                    for (int k = 0; (k < n) && ok; ++k)
                        ok = !((k != i) && (arr[k].find(sub) != std::string::npos));
                    if (ok)
                        result[i] = sub;
                }
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> arr,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestSubstrings(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"cab", "ad", "bad", "c"}, {"ab", "", "ba", ""}, trials);
    test({"abc", "bcd", "abcd"}, {"", "", "abcd"}, trials);
    return 0;
}


