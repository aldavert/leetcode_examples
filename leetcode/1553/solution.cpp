#include "../common/common.hpp"
#include <cstring>
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int minDays(int n)
{
    constexpr int buffer_size = 50'000;
    int buffer[buffer_size];
    std::memset(buffer, -1, sizeof(buffer));
    auto traverse = [&](auto &&self, int value) -> int
    {
        if (value < 2) return value;
        if ((value < buffer_size) && (buffer[value] != -1)) return buffer[value];
        int result = 1 + std::min(value % 2 + self(self, value / 2),
                                  value % 3 + self(self, value / 3));
        if (value < buffer_size) buffer[value] = result;
        return result;
    };
    return traverse(traverse, n);
}
#else
int minDays(int n)
{
    std::queue<int> q{{n}};
    std::unordered_set<int> seen;
    
    for (int steps = 0; !q.empty(); ++steps)
    {
        for (int size = static_cast<int>(q.size()); size; --size)
        {
            int x = q.front(); q.pop();        
            if (x == 0) return steps;
            if ((x % 2 == 0) && seen.insert(x / 2).second)
                q.push(x / 2);                  
            if ((x % 3 == 0) && seen.insert(x / 3).second)
                q.push(x / 3);        
            if (seen.insert(x - 1).second)
                q.push(x - 1);        
        }
    }
    return -1;
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDays(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, 4, trials);
    test(6, 3, trials);
    return 0;
}



