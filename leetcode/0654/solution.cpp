#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

#if 1
TreeNode * constructMaximumBinaryTree(std::vector<int> nums)
{
    auto process = [](const std::vector<int> &data) -> TreeNode *
    {
        auto maximum = [&](int left, int right) -> int
        {
            int max_value = data[left], max_index = left;
            for (int i = left + 1; i <= right; ++i)
            {
                if (data[i] > max_value)
                {
                    max_value = data[i];
                    max_index = i;
                }
            }
            return max_index;
        };
        auto inner = [&](auto &&self, int left, int right) -> TreeNode *
        {
            if (left > right) return nullptr;
            const int threshold = maximum(left, right);
            return new TreeNode(data[threshold],
                                self(self, left, threshold - 1),
                                self(self, threshold + 1, right));
        };
        return inner(inner, 0, static_cast<int>(data.size()) - 1);
    };
    return (!nums.empty())?process(nums):nullptr;
}
#else
TreeNode * constructMaximumBinaryTree(std::vector<int> nums)
{
    auto process = [](const std::vector<int> &data) -> TreeNode *
    {
        std::vector<std::tuple<int, size_t> > sorted_nums(data.size());
        for (size_t i = 0; i < data.size(); ++i)
            sorted_nums[i] = { data[i], i };
        std::sort(sorted_nums.begin(), sorted_nums.end());
        auto inner = [](auto &&self, const auto &values) -> TreeNode *
        {
            if (values.empty()) return nullptr;
            const size_t threshold = std::get<1>(values.back());
            std::vector<std::tuple<int, size_t> > left, right;
            for (size_t i = 0; i < values.size() - 1; ++i)
            {
                if (std::get<1>(values[i]) < threshold)
                    left.push_back(values[i]);
                else right.push_back(values[i]);
            }
            return new TreeNode(std::get<0>(values.back()),
                                self(self, left),
                                self(self, right));
        };
        return inner(inner, sorted_nums);
    };
    return process(nums);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = constructMaximumBinaryTree(nums);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 1, 6, 0, 5}, {6, 3, 5, null, 2, 0, null, null, 1}, trials);
    test({3, 2, 1}, {3, null, 2, null, 1}, trials);
    return 0;
}


