#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> >
mergeSimilarItems(std::vector<std::vector<int> > items1,
                  std::vector<std::vector<int> > items2)
{
    int weights[1001] = {};
    for (const auto &element : items1)
        weights[element[0]] += element[1];
    for (const auto &element : items2)
        weights[element[0]] += element[1];
    std::vector<std::vector<int> > result;
    for (int i = 1; i <= 1000; ++i)
        if (weights[i])
            result.push_back({i, weights[i]});
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > items1,
          std::vector<std::vector<int> > items2,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = mergeSimilarItems(items1, items2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1}, {4, 5}, {3, 8}}, {{3, 1}, {1, 5}},
         {{1, 6}, {3, 9}, {4, 5}}, trials);
    test({{1, 1}, {3, 2}, {2, 3}}, {{2, 1}, {3, 2}, {1, 3}},
         {{1, 4}, {2, 4}, {3, 4}}, trials);
    test({{1, 3}, {2, 2}}, {{7, 1}, {2, 2}, {1, 4}},
         {{1, 7}, {2, 4}, {7, 1}}, trials);
    return 0;
}


