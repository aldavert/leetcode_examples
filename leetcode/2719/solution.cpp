#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

#if 0
const int MOD = 1e9 + 7;
const int PREV_IS_MAX_DIGIT = 1, PREV_LESS_MAX_DIGIT = 0;
class Solution {
    public:
    long long memo[25][420][2];
    long long _count(std::string &num, int min_sum, int max_sum)
    {
        // count the number of digit sum in [min_sum, max_sum] for nums in [0, num]
        std::memset(memo, 0, sizeof memo);
        const int n = static_cast<int>(num.size());
        // 1 means the previous digit reaches the largest digit value as the previous position is 0 for example, 19, 
        // it can be written as 019, so the previous position is 0. It reaches its maximum value. So we set it as 1.
        memo[0][0][PREV_IS_MAX_DIGIT] = 1;
        for (int i = 0;  i < n; ++i)
        {
            for (int j = 0; j < 9*n; ++j)
            {
                // the third position of the dp memo
                // PREV_LESS_MAX_DIGIT means not reach the largest digit value
                // PREV_IS_MAX_DIGIT means the previous digit reaches the largest digit value

                //PREV_IS_MAX_DIGIT: previous digit reach the largest digit value
                for (int digit = 0; digit < num[i] - '0'; ++digit)
                {
                    memo[i+1][j + digit][PREV_LESS_MAX_DIGIT] +=  memo[i][j][PREV_IS_MAX_DIGIT];
                    memo[i+1][j + digit][PREV_LESS_MAX_DIGIT] %= MOD;
                }
                memo[i+1][j +  num[i] - '0'][PREV_IS_MAX_DIGIT] +=  memo[i][j][PREV_IS_MAX_DIGIT];
                memo[i+1][j +  num[i] - '0'][PREV_IS_MAX_DIGIT] %= MOD;
                // PREV_LESS_MAX_DIGIT previous digit does not reach the largest digit value, for this digit, we can pick up 0 to 9.
                for (int digit = 0; digit < 10; ++digit)
                {
                    memo[i+1][j + digit][PREV_LESS_MAX_DIGIT] +=  memo[i][j][PREV_LESS_MAX_DIGIT];
                    memo[i+1][j + digit][PREV_LESS_MAX_DIGIT] %= MOD;
                }
            }
        }
        long long ret = 0;
        for (int i = min_sum; i <= max_sum; ++i){
            ret += memo[n][i][PREV_LESS_MAX_DIGIT];
            ret += memo[n][i][PREV_IS_MAX_DIGIT];
        }
        return ret % MOD;


    }
    int get_digit_sum(std::string &num)
    {
        int ret = 0;
        for (auto c: num) ret += c - '0';
        return ret;
    }
    int count(std::string num1, std::string num2, int min_sum, int max_sum)
    {
        long long ret = _count(num2, min_sum, max_sum) -  _count(num1, min_sum, max_sum);
        int digit_sum = get_digit_sum(num1);
        ret += digit_sum >= min_sum && digit_sum <= max_sum;
        return static_cast<int>((ret % MOD + MOD) % MOD);
    }
};
int count(std::string num1, std::string num2, int min_sum, int max_sum)
{
    Solution sol;
    return sol.count(num1, num2, min_sum, max_sum);
}
#else
int count(std::string num1, std::string num2, int min_sum, int max_sum)
{
    constexpr long MOD = 1'000'000'007;
    using DPVECTOR = std::vector<std::vector<long> >;
    DPVECTOR dp[2][2] = {
        { DPVECTOR(num2.size(), std::vector<long>(max_sum + 1, -1)),
          DPVECTOR(num2.size(), std::vector<long>(max_sum + 1, -1)) },
        { DPVECTOR(num2.size(), std::vector<long>(max_sum + 1, -1)),
          DPVECTOR(num2.size(), std::vector<long>(max_sum + 1, -1)) }};
    std::string num1_with_zeros = std::string(num2.size() - num1.size(), '0') + num1;
    auto compute =
        [&](auto &&self, long i, long sum, bool is_tight1, bool is_tight2) -> long
    {
        if (sum < 0) return 0;
        if (i == static_cast<long>(num2.size())) return 1;
        if (dp[is_tight1][is_tight2][i][sum] != -1)
            return dp[is_tight1][is_tight2][i][sum];
        long result = 0,
             min_digit = is_tight1?num1_with_zeros[i] - '0':0,
             max_digit = is_tight2?num2[i] - '0':9;
        for (long d = min_digit; d <= max_digit; ++d)
        {
            bool next_tight1 = is_tight1 && (d == min_digit),
                 next_tight2 = is_tight2 && (d == max_digit);
            result = (result
                   + self(self, i + 1, sum - d, next_tight1, next_tight2)) % MOD;
        }
        return dp[is_tight1][is_tight2][i][sum] = result;
    };
    long result = compute(compute, 0, max_sum, true, true)
                - compute(compute, 0, min_sum - 1, true, true);
    return static_cast<int>((result % MOD + MOD) % MOD);
}
#endif

// ############################################################################
// ############################################################################

void test(std::string num1,
          std::string num2,
          int min_sum,
          int max_sum,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = count(num1, num2, min_sum, max_sum);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1", "12", 1, 8, 11, trials);
    test("1", "5", 1, 5, 5, trials);
    test("1479192516", "5733987233", 36, 108, 519488312, trials);
    return 0;
}


