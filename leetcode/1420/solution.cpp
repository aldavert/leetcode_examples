#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numOfArrays(int n, int m, int k)
{
    constexpr int MOD = 1'000'000'007;
    int dp[51][101][51] = {}, prefix[51][101][51] = {};
    
    for (int j = 1; j <= m; ++j)
        dp[1][j][1] = 1,
        prefix[1][j][1] = j;
    for (int i = 2; i <= n; ++i)
        for (int j = 1; j <= m; ++j)
            for (int cost = 1; cost <= k; ++cost)
                dp[i][j][cost] = static_cast<int>((static_cast<long>(j)
                               * dp[i - 1][j][cost]
                               +  prefix[i - 1][j - 1][cost - 1]) % MOD),
                prefix[i][j][cost] = (dp[i][j][cost] + prefix[i][j - 1][cost]) % MOD;
    int result = 0;
    for (int j = 1; j <= m; ++j)
        result = (result + dp[n][j][k]) % MOD;
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int m, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numOfArrays(n, m, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 3, 1, 6, trials);
    test(5, 2, 3, 0, trials);
    test(9, 1, 1, 1, trials);
    return 0;
}


