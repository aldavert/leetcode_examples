#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int closestMeetingNode(std::vector<int> edges, int node1, int node2)
{
    const size_t n = edges.size();
    const int UNREACHABLE = std::numeric_limits<int>::max();
    auto computeDistances = [&](int node) -> std::vector<int>
    {
        std::vector<int> distances(edges.size(), UNREACHABLE);
        for (int d = 0; ((node != -1) && (distances[node] == UNREACHABLE)); ++d)
            distances[std::exchange(node, edges[node])] = d;
        return distances;
    };
    std::vector<int> distance_first = computeDistances(node1),
                     distance_second = computeDistances(node2);
    int closest_node = -1;
    int closest_distance = UNREACHABLE;
    for (size_t i = 0; i < n; ++i)
    {
        int distance = std::max(distance_first[i], distance_second[i]);
        if (distance < closest_distance)
        {
            closest_distance = distance;
            closest_node = static_cast<int>(i);
        }
    }
    return closest_node;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> edges,
          int node1,
          int node2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = closestMeetingNode(edges, node1, node2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 2, 3, -1}, 0, 1, 2, trials);
    test({1, 2, -1}, 0, 2, 2, trials);
    return 0;
}


