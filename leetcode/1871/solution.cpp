#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canReach(std::string s, int minJump, int maxJump)
{
    std::vector<bool> dp(s.size());
    
    dp[0] = true;
    for (int i = minJump, count = 0, n = static_cast<int>(s.size()); i < n; ++i)
    {
        count += dp[i - minJump];
        if (i - maxJump > 0)
            count -= dp[i - maxJump - 1];
        dp[i] = count && s[i] == '0';
    }
    return dp.back();
}

// ############################################################################
// ############################################################################

void test(std::string s,
          int minJump,
          int maxJump,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canReach(s, minJump, maxJump);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("011010", 2, 3, true, trials);
    test("01101110", 2, 3, false, trials);
    return 0;
}


