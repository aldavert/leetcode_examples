#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkDistances(std::string s, std::vector<int> distance)
{
    int expected[26] = {};
    for (int i = 0, n = static_cast<int>(s.size()); i < n; ++i)
    {
        int idx = s[i] - 'a';
        if ((expected[idx] > 0) && (expected[idx] != i))
            return false;
        expected[idx] = i + distance[idx] + 1;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<int> distance,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkDistances(s, distance);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abaccb", {1, 3, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, true, trials);
    test("aa", {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, false, trials);
    test("abbccddeeffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxyyzza", {49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, false, trials);
    return 0;
}


