#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

#if 0
ListNode * swapNodes(ListNode * head, int k)
{
    ListNode * left = nullptr, * right;
    int list_size = 0;
    for (ListNode * ptr = head; ptr; ptr = ptr->next)
        if (++list_size == k)
            left = ptr;
    for (right = head; list_size - k > 0; right = right->next, --list_size);
    std::swap(left->val, right->val);
    return head;
}
#elif 1
ListNode * swapNodes(ListNode * head, int k)
{
    ListNode * left, * right, *ptr;
    for (left = head; --k; left = left->next);
    for (ptr = left, right = head; ptr->next; right = right->next, ptr = ptr->next);
    std::swap(left->val, right->val);
    return head;
}
#else
ListNode * swapNodes(ListNode * head, int k)
{
    if (head)
    {
        ListNode * right;
        int n = 1;
        for (right = head; right->next; right = right->next, ++n);
        if ((k == 1) || (k == n))
            std::swap(head->val, right->val);
        else
        {
            ListNode * left;
            int goal_left = std::min(k, n - k + 1);
            int goal_right = n - goal_left + 1;
            left = head;
            for (int i = 1; i < goal_left; ++i, left = left->next);
            right = left;
            for (int i = goal_left; i < goal_right; ++i, right = right->next);
            std::swap(left->val, right->val);
        }
    }
    return head;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> input,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(input);
        ListNode * new_head = swapNodes(head, k);
        result = list2vec(new_head);
        delete new_head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 2, {1, 4, 3, 2, 5}, trials);
    test({1, 2, 3, 4, 5}, 1, {5, 2, 3, 4, 1}, trials);
    test({1, 2, 3, 4, 5}, 5, {5, 2, 3, 4, 1}, trials);
    test({7, 9, 6, 6, 7, 8, 3, 0, 9, 5}, 5, {7, 9, 6, 6, 8, 7, 3, 0, 9, 5}, trials);
    test({7, 9, 6, 6, 7, 8, 3, 0, 9, 5}, 6, {7, 9, 6, 6, 8, 7, 3, 0, 9, 5}, trials);
    return 0;
}

