#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> maxDepthAfterSplit(std::string seq)
{
    std::vector<int> result;
    for (int depth = 1; char c : seq)
    {
        if (c == '(') result.push_back(++depth % 2);
        else          result.push_back(depth-- % 2);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string seq, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxDepthAfterSplit(seq);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("(()())", {0, 1, 1, 1, 1, 0}, trials);
    test("()(())()", {0, 0, 0, 1, 1, 0, 0, 0}, trials);
    return 0;
}


