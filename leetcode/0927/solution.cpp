#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> threeEqualParts(std::vector<int> arr)
{
    int number_of_ones = std::accumulate(arr.begin(), arr.end(), 0);
    const int n = static_cast<int>(arr.size());
    if (number_of_ones == 0)
        return {0, n - 1};
    else if (number_of_ones % 3 != 0)
        return {-1, -1};
    number_of_ones /= 3;
    std::vector<int>::const_iterator begin[3];
    int count = 1;
    for (int i = 0, sequence_number = 0; sequence_number < 3; ++i)
    {
        if (arr[i])
        {
            if (count == 1)
            {
                begin[sequence_number] = arr.begin() + i;
                ++sequence_number;
                count = number_of_ones;
            }
            else --count;
        }
    }
    for (const auto end = arr.end(); begin[2] != end; ++begin[0], ++begin[1], ++begin[2])
        if ((*begin[0] != *begin[1]) || (*begin[0] != *begin[2]))
            return {-1, -1};
    return {n + static_cast<int>(std::distance(begin[2], begin[0])) - 1,
            n + static_cast<int>(std::distance(begin[2], begin[1]))};
}
#else
std::vector<int> threeEqualParts(std::vector<int> arr)
{
    int number_of_ones = std::accumulate(arr.begin(), arr.end(), 0);
    const int n = static_cast<int>(arr.size());
    if (number_of_ones == 0)
        return {0, n - 1};
    else if (number_of_ones % 3 != 0)
        return {-1, -1};
    number_of_ones /= 3;
    int begin[3];
    int count = 1;
    for (int i = 0, sequence_number = 0; sequence_number < 3; ++i)
    {
        if (arr[i])
        {
            if (count == 1)
            {
                begin[sequence_number] = i;
                ++sequence_number;
                count = number_of_ones;
            }
            else --count;
        }
    }
    for (int i = 0; begin[2] < n; ++i)
    {
        if ((arr[begin[0]] != arr[begin[1]]) || (arr[begin[0]] != arr[begin[2]]))
            return {-1, -1};
        else
        {
            ++begin[0];
            ++begin[1];
            ++begin[2];
        }
    }
    if (begin[2] != n) return {-1, -1};
    return {begin[0] - 1, begin[1]};
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = threeEqualParts(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 1, 0, 1}, {0, 3}, trials);
    test({1, 1, 0, 1, 1}, {-1, -1}, trials);
    test({1, 1, 0, 0, 1}, {0, 2}, trials);
    test({1, 0, 1, 1, 0}, {-1, -1}, trials);
    test({0, 0, 0, 0, 0}, {0, 4}, trials);
    test({0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1}, {4, 12}, trials);
    test({0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1}, {4, 12}, trials);
    test({0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1}, {4, 14}, trials);
    test({1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1,
          0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1,
          0, 0}, {15, 32}, trials);
    return 0;
}


