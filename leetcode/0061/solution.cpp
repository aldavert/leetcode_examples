#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

ListNode * rotateRight(ListNode * head, int k)
{
    if (head)
    {
        unsigned int size = 0;
        ListNode * ptr, * last;
        for (ptr = head; ptr; ++size)
        {
            if (ptr->next == nullptr) last = ptr;
            ptr = ptr->next;
        }
        k = k % size;
        if (k == 0) return head;
        ptr = head;
        ListNode * previous = nullptr;
        for (int i = size - k; i; --i)
        {
            previous = ptr;
            ptr = ptr->next;
        }
        previous->next = nullptr;
        last->next = head;
        head = ptr;
    }
    return head;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> input,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(input);
        ListNode * new_head = rotateRight(head, k);
        result = list2vec(new_head);
        delete new_head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 2, {4, 5, 1, 2, 3}, trials);
    test({0, 1, 2}, 4, {2, 0, 1}, trials);
    return 0;
}


