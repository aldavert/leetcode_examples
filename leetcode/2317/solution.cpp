#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int maximumXOR(std::vector<int> nums)
{
    return std::reduce(nums.begin(), nums.end(), 0, std::bit_or());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumXOR(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 4, 6}, 7, trials);
    test({1, 2, 3, 9, 2}, 11, trials);
    return 0;
}


