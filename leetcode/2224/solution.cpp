#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int convertTime(std::string current, std::string correct)
{
    int correct_hours   = (correct[0] - '0') * 10 + (correct[1] - '0'),
        correct_minutes = (correct[3] - '0') * 10 + (correct[4] - '0'),
        current_hours   = (current[0] - '0') * 10 + (current[1] - '0'),
        current_minutes = (current[3] - '0') * 10 + (current[4] - '0');
    int diff = 0;
    if (current_minutes > correct_minutes)
        diff = correct_minutes + 60 - current_minutes,
        ++current_hours;
    else diff = correct_minutes - current_minutes;
    int result = diff / 15;
    diff = diff % 15;
    return correct_hours - current_hours + result + diff / 5 + diff % 5;
}

// ############################################################################
// ############################################################################

void test(std::string current,
          std::string correct,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = convertTime(current, correct);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("02:30", "04:35", 3, trials);
    test("11:00", "11:01", 1, trials);
    return 0;
}


