#include "../common/common.hpp"
#include <unordered_map>
#include <cstring>

// ############################################################################
// ############################################################################

#if 1
int maxSizeSlices(std::vector<int> slices)
{
    const int n = static_cast<int>(slices.size());
    const int k = n / 3;
    auto process = [&](int s, int e) -> int
    {
        int length = e - s + 1;
        int f[200] = {}, f1[200] = {}, f2[200] = {}; 
        for (int i = 0; i < length; ++i)
        {
            std::memset(f, 0, sizeof(f));
            for (int j = 1, m = std::min(i + 1, k); j <= m; ++j)
                f[j] = std::max(f2[j - 1] + slices[s + i], f1[j]);
            std::memcpy(f2, f1, sizeof(f));
            std::memcpy(f1, f, sizeof(f));
        }
        return f1[k];
    };
    return std::max(process(0, n - 2), process(1, n - 1));
}
#else
int maxSizeSlices(std::vector<int> slices)
{
    std::unordered_map<int, int> lut;
    auto dp = [&](auto &&self, int i, int j, int k) -> int
    {
        int id = ((i & 0x1FF) << 18) | ((j & 0x1FF) << 9) | (k & 0x1FF);
        if (auto search = lut.find(id); search != lut.end())
            return search->second;
        if (k == 1)
        {
            int result = slices[i];
            for (int m = i + 1; m < j; ++m)
                result = std::max(result, slices[m]);
            return lut[id] = result;
        }
        if (j - i < 2 * k - 1)
            return lut[id] = std::numeric_limits<int>::lowest();
        return lut[id] = std::max(slices[i] + self(self, i + 2, j, k - 1),
                                              self(self, i + 1, j, k));
    };
    const int n = static_cast<int>(slices.size());
    const int k = n / 3;
    return std::max(dp(dp, 0, n - 1, k), dp(dp, 1, n, k));
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> slices, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSizeSlices(slices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6}, 10, trials);
    test({8, 9, 8, 6, 1, 1}, 16, trials);
    test({7, 8, 5, 6, 9, 10, 1, 6, 5, 10, 8, 10, 5, 4, 7, 2, 8, 5, 9, 7, 5, 9,
          3, 7, 7, 2, 2, 10, 7, 6, 4, 6, 5, 7, 7, 9, 6, 8, 10, 7, 5, 7, 2, 5,
          4, 9, 6, 10, 10, 2, 10}, 150, trials);
    return 0;
}


