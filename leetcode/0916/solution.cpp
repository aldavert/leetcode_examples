#include "../common/common.hpp"
#include <unordered_map>
#include <cstring>

// ############################################################################
// ############################################################################

#if 1
std::vector<std::string> wordSubsets(std::vector<std::string> words1,
                                     std::vector<std::string> words2)
{
    size_t histogram_merged[26] = {}, histogram_word[26];
    auto histogram = [&](std::string word)
    {
        std::memset(histogram_word, 0, sizeof(histogram_word));
        for (char l : word)
            ++histogram_word[l - 'a'];
    };
    std::vector<std::string> result;
    for (const auto &word : words2)
    {
        histogram(word);
        for (int i = 0; i < 26; ++i)
            histogram_merged[i] = std::max(histogram_merged[i], histogram_word[i]);
    }
    for (const auto &word : words1)
    {
        bool valid = true;
        histogram(word);
        for (int i = 0; i < 26; ++i)
        {
            if (histogram_word[i] < histogram_merged[i])
            {
                valid = false;
                break;
            }
        }
        if (valid) result.push_back(word);
    }
    return result;
}
#else
std::vector<std::string> wordSubsets(std::vector<std::string> words1,
                                     std::vector<std::string> words2)
{
    const size_t n = words1.size();
    std::vector<std::string> result;
    std::vector<std::pair<size_t, size_t> > lut[26];
    std::vector<size_t> counter(n);
    std::vector<bool> valid(n, true);
    size_t current[26];
    auto histogram = [&](std::string word)
    {
        std::memset(current, 0, sizeof(current));
        for (char l : word)
            ++current[l - 'a'];
    };
    for (size_t i = 0; i < n; ++i)
    {
        histogram(words1[i]);
        for (int j = 0; j < 26; ++j)
            if (current[j])
                lut[j].push_back({current[j], i});
    }
    for (const auto &word : words2)
    {
        for (size_t i = 0; i < n; ++i)
            counter[i] = 0;
        histogram(word);
        for (int i = 0; i < 26; ++i)
            for (auto [freq, idx] : lut[i])
                counter[idx] += std::min(freq, current[i]);
        for (size_t i = 0; i < n; ++i)
            valid[i] = valid[i] && (counter[i] == word.size());
    }
    for (size_t i = 0; i < n; ++i)
        if (valid[i])
            result.push_back(words1[i]);
    return result;
}
#endif

// ############################################################################
// ############################################################################

template <typename T>
bool operator==(const std::vector<T> &left, const std::vector<T> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_map<T, size_t> histogram;
    for (const auto &l : left)
        ++histogram[l];
    for (const auto &r : right)
    {
        if (auto search = histogram.find(r); search != histogram.end())
        {
            if (search->second == 1)
                histogram.erase(search);
            else --search->second;
        }
        else return false;
    }
    return histogram.empty();
}

void test(std::vector<std::string> words1,
          std::vector<std::string> words2,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = wordSubsets(words1, words2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"amazon", "apple", "facebook", "google", "leetcode"},
         {"e", "o"}, {"facebook", "google", "leetcode"}, trials);
    test({"amazon", "apple", "facebook", "google", "leetcode"},
         {"l", "e"}, {"apple", "google", "leetcode"}, trials);
    return 0;
}


