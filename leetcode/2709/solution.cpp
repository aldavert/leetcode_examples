#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

bool canTraverseAllPairs(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    const int max_number = *std::max_element(nums.begin(), nums.end());
    std::vector<int> minimum_prime_factors(max_number + 2);
    for (int i = 2; i < max_number + 2; ++i) minimum_prime_factors[i] = i;
    for (int i = 2; i * i <= max_number; ++i)
        if (minimum_prime_factors[i] == i)
            for (int j = i * i; j <= max_number; j += i)
                minimum_prime_factors[j] = std::min(minimum_prime_factors[j], i);
    auto primeFactors = [&](int number) -> std::vector<int>
    {
        std::vector<int> result;
        while (number > 1)
        {
            int divisor = minimum_prime_factors[number];
            result.push_back(divisor);
            while (number % divisor == 0) number /= divisor;
        }
        return result;
    };
    
    std::unordered_map<int, int> prime_to_first_index;
    std::vector<int> id(n), sz(n, 1);
    for (int i = 0; i < n; ++i) id[i] = i;
    auto find = [&](auto &&self, int u) -> int
    {
        return (id[u] == u)?u:id[u] = self(self, id[u]);
    };
    auto merge = [&](int u, int v) -> void
    {
        int i = find(find, u), j = find(find, v);
        if (i == j) return;
        if (sz[i] < sz[j])
        {
            sz[j] += sz[i];
            id[i] = j;
        }
        else
        {
            sz[i] += sz[j];
            id[j] = i;
        }
    };
    
    for (int i = 0; i < n; ++i)
    {
        for (int prime_factor : primeFactors(nums[i]))
        {
            auto it = prime_to_first_index.find(prime_factor);
            if (it != prime_to_first_index.end()) merge(it->second, i);
            else prime_to_first_index[prime_factor] = i;
        }
    }
    for (int i = 0; i < n; ++i)
        if (sz[i] == n)
            return true;
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    int result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canTraverseAllPairs(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 6}, true, trials);
    test({3, 9, 5}, false, trials);
    test({4, 3, 12, 8}, true, trials);
    return 0;
}


