#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

class MajorityChecker
{
    std::vector<int> m_values;
    std::unordered_map<int, std::vector<int> > m_value_to_index;
public:
    MajorityChecker(std::vector<int> arr) : m_values(arr)
    {
        for (size_t i = 0, n = arr.size(); i < n; ++i)
            m_value_to_index[arr[i]].push_back(static_cast<int>(i));
    }
    int query(int left, int right, int threshold)
    {
        for (int i = 0; i < 5; ++i)
        {
            int num = m_values[left + rand() % (right - left + 1)];
            std::vector<int> &indices = m_value_to_index[num];
            auto lit = std::lower_bound(indices.begin(), indices.end(), left);
            auto rit = std::upper_bound(indices.begin(), indices.end(), right);
            if (rit - lit >= threshold)
                return num;
        }
        return -1;
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<int> arr,
          std::vector<std::tuple<int, int, int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        MajorityChecker obj(arr);
        result.clear();
        for (auto [left, right, threshold] : input)
            result.push_back(obj.query(left, right, threshold));
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2, 2, 1, 1}, {{0, 5, 4}, {0, 3, 3}, {2, 3, 2}}, {1, -1, 2}, trials);
    return 0;
}


