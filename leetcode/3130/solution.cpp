#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfStableArrays(int zero, int one, int limit)
{
    constexpr int MOD = 1'000'000'007;
    std::vector<std::vector<long> > dp[2] = {
        std::vector<std::vector<long> >(zero + 1, std::vector<long>(one + 1)),
        std::vector<std::vector<long> >(zero + 1, std::vector<long>(one + 1)) };
    for (int i = 0, n = std::min(zero, limit); i <= n; ++i) dp[0][i][0] = 1;
    for (int j = 0, n = std::min( one, limit); j <= n; ++j) dp[1][0][j] = 1;
    for (int i = 1; i <= zero; ++i)
    {
        for (int j = 1; j <= one; ++j)
        {
            dp[0][i][j] = (dp[0][i - 1][j] + dp[1][i - 1][j]
                        - ((i - limit < 1)?0:dp[1][i - limit - 1][j])
                        + MOD) % MOD;
            dp[1][i][j] = (dp[0][i][j - 1] + dp[1][i][j - 1]
                        - ((j - limit < 1)?0:dp[0][i][j - limit - 1])
                        + MOD) % MOD;
        }
    }
    return static_cast<int>((dp[0][zero][one] + dp[1][zero][one]) % MOD);
}

// ############################################################################
// ############################################################################

void test(int zero, int one, int limit, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfStableArrays(zero, one, limit);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 1, 2, 2, trials);
    test(1, 2, 1, 1, trials);
    test(3, 3, 2, 14, trials);
    return 0;
}


