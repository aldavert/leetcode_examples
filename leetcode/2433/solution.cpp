#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<int> findArray(std::vector<int> pref)
{
    std::vector<int> result(pref.size());
    result[0] = pref[0];
    for (size_t i = 1, n = pref.size(); i < n; ++i)
        result[i] = pref[i - 1] ^ pref[i];
    return result;
}
#else
std::vector<int> findArray(std::vector<int> pref)
{
    std::vector<int> result;
    for (int prev = 0; int value : pref)
    {
        result.push_back(prev ^ value);
        prev = value;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> pref, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findArray(pref);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 2, 0, 3, 1}, {5, 7, 2, 3, 2}, trials);
    test({13}, {13}, trials);
    return 0;
}


