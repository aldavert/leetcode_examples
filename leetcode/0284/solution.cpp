#include "../common/common.hpp"
#include <memory>

class Iterator
{
	struct Data
    {
        Data(const int * v, size_t ns) :
            values(v), i(0), n(ns) {}
        Data(const Data &other) :
            values(other.values),
            i(other.i),
            n(other.n) {}
        const int * values = nullptr;
        size_t i = 0;
        size_t n = 0;
    };
    std::unique_ptr<Data> data;
public:
	Iterator(const std::vector<int> &nums) :
        data(std::make_unique<Data>(nums.data(), nums.size())) {}
	Iterator(const Iterator &iter) :
        data(std::make_unique<Data>(*iter.data)) {}
	int next()
    {
        int value = data->values[data->i];
        ++(data->i);
        return value;
    }
	bool hasNext() const { return data->i < data->n; }
};

// ############################################################################
// ############################################################################

class PeekingIterator : public Iterator
{
public:
	PeekingIterator(const std::vector<int>& nums) : Iterator(nums) {}
	int peek(void)
    {
        return Iterator(*this).next();
	}
	int next(void)
    {
        return Iterator::next();
	}
	
	bool hasNext(void) const
    {
        return Iterator::hasNext();
	}
};

// ############################################################################
// ############################################################################

enum class OP { NEXT = 1, PEEK = 2, HAS  = 4 };

void test(std::vector<int> input,
          std::vector<OP> operations,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    size_t n = solution.size();
    std::vector<int> result(n);
    for (unsigned int t = 0; t < trials; ++t)
    {
        PeekingIterator iter(input);
        for (size_t i = 0; i < n; ++i)
        {
            if      (operations[i] == OP::NEXT)
                result[i] = iter.next();
            else if (operations[i] == OP::PEEK)
                result[i] = iter.peek();
            else if (operations[i] == OP::HAS)
                result[i] = iter.hasNext();
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3},
         {OP::NEXT, OP::PEEK, OP::NEXT, OP::NEXT, OP::HAS},
         {1, 2, 2, 3, false}, trials);
    return 0;
}


