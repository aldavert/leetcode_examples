#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int smallestNumber(int n, int t)
{
    for (int i = n; true; ++i)
    {
        int product = 1;
        for (int j = i; j > 0; j /= 10)
            product *= j % 10;
        if (product % t == 0) return i;
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(int n, int t, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int trial = 0; trial < trials; ++trial)
        result = smallestNumber(n, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, 2, 10, trials);
    test(15, 3, 16, trials);
    return 0;
}


