#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxHeight(std::vector<std::vector<int> > cuboids)
{
    const int n = static_cast<int>(cuboids.size());
    for (auto &cuboid : cuboids) std::sort(cuboid.begin(), cuboid.end());
    std::sort(cuboids.begin(), cuboids.end());
    std::vector<int> dp(n);
    for (int i = 0; i < n; ++i) dp[i] = cuboids[i][2];
    for (int i = 1; i < n; ++i)
        for (int j = 0; j < i; ++j)
            if ((cuboids[j][0] <= cuboids[i][0])
            &&  (cuboids[j][1] <= cuboids[i][1])
            &&  (cuboids[j][2] <= cuboids[i][2]))
                dp[i] = std::max(dp[i], dp[j] + cuboids[i][2]);
    return *std::max_element(dp.begin(), dp.end());
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > cuboids,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxHeight(cuboids);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{50, 45, 20}, {95, 37, 53}, {45, 23, 12}}, 190, trials);
    test({{38, 25, 45}, {76, 35, 3}}, 76, trials);
    test({{7, 11, 17}, {7, 17, 11}, {11, 7, 17},
          {11, 17, 7}, {17, 7, 11}, {17, 11, 7}}, 102, trials);
    return 0;
}



