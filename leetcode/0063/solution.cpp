#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int uniquePathsWithObstacles(std::vector<std::vector<int> > obstacleGrid)
{
    const int m = static_cast<int>(obstacleGrid.size());
    const int n = static_cast<int>(obstacleGrid[0].size());
    std::vector<unsigned int> dp(n + 1, 0);
    dp[1] = 1;
    for (int i = 0; i < m; ++i)
        for (int j = 1; j <= n; ++j)
            dp[j] = (!obstacleGrid[i][j - 1]) * (dp[j] + dp[j - 1]);
    return dp[n];
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > obstacleGrid,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = uniquePathsWithObstacles(obstacleGrid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0, 0}, {0, 1, 0}, {0, 0, 0}}, 2, trials);
    test({{0, 1}, {0, 0}}, 1, trials);
    return 0;
}


