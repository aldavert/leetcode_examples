#include "../common/common.hpp"

// ############################################################################
// ############################################################################

void reverseString(std::vector<char> &s)
{
    const int n = static_cast<int>(s.size());
    for (int left = 0, right = n - 1; left < right; ++left, --right)
        std::swap(s[left], s[right]);
}

// ############################################################################
// ############################################################################

void test(std::vector<char> s, std::vector<char> solution, unsigned int trials = 1)
{
    std::vector<char> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = s;
        reverseString(result);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({'h', 'e', 'l', 'l', 'o'}, {'o', 'l', 'l', 'e', 'h'}, trials);
    test({'H', 'a', 'n', 'n', 'a', 'h'}, {'h', 'a', 'n', 'n', 'a', 'H'}, trials);
    return 0;
}


