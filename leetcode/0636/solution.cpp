#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

std::vector<int> exclusiveTime(int n, std::vector<std::string> logs)
{
    struct Info { int time = -1, id = -1; };
    std::vector<int> result(n);
    std::stack<Info> callstack;
    for (const std::string &log : logs)
    {
        const size_t m = log.size();
        size_t i = 0;
        int id = 0;
        for (; (i < m) && (log[i] != ':'); ++i)
            id = id * 10 + static_cast<int>(log[i] - '0');
        bool start = log[++i] == 's';
        for (; (i < m) && (log[i] != ':'); ++i);
        int time = 0;
        for (++i; i < m; ++i)
            time = time * 10 + static_cast<int>(log[i] - '0');
        if (start)
        {
            if (!callstack.empty())
                result[callstack.top().id] += time - callstack.top().time;
            callstack.push({time, id});
        }
        else
        {
            result[id] += time - callstack.top().time + 1;
            callstack.pop();
            if (!callstack.empty()) callstack.top().time = time + 1;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::string> logs,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = exclusiveTime(n, logs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {"0:start:0", "1:start:2", "1:end:5", "0:end:6"}, {3, 4}, trials);
    test(1, {"0:start:0", "0:start:2", "0:end:5", "0:start:6",
             "0:end:6", "0:end:7"}, {8}, trials);
    test(2, {"0:start:0", "0:start:2", "0:end:5", "1:start:6",
             "1:end:6", "0:end:7"}, {7, 1}, trials);
    return 0;
}


