#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> maximumBeauty(std::vector<std::vector<int> > items,
                               std::vector<int> queries)
{
    std::vector<int> prices, result, max_beauty_so_far(items.size() + 1);
    std::sort(items.begin(), items.end());
    for (const auto &item : items)
        prices.push_back(item[0]);
    for (size_t i = 0; i < items.size(); ++i)
        max_beauty_so_far[i + 1] = std::max(max_beauty_so_far[i], items[i][1]);
    for (int query : queries)
        result.push_back(max_beauty_so_far[std::upper_bound(prices.begin(),
                    prices.end(), query) - prices.begin()]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > items,
          std::vector<int> queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumBeauty(items, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {3, 2}, {2, 4}, {5, 6}, {3, 5}},
         {1, 2, 3, 4, 5, 6}, {2, 4, 5, 5, 6, 6}, trials);
    test({{1, 2}, {1, 2}, {1, 3}, {1, 4}}, {1}, {4}, trials);
    test({{10, 1000}}, {5}, {0}, trials);
    return 0;
}


