#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int palindromePartition(std::string s, int k)
{
    const int n = static_cast<int>(s.size());
    int dp[101][101] = {}, cost[101][101] = {};
    for (int i = 0; i <= n; ++i)
        for (int j = 0; j <= k; ++j)
            dp[i][j] = n;
    for (int d = 1; d < n; ++d)
        for (int i = 0, j = d; j < n; ++i, ++j)
            cost[i][j] = (s[i] != s[j]) + cost[i + 1][j - 1];
    for (int i = 1; i <= n; ++i)
        dp[i][1] = cost[0][i - 1];
    for (int p = 2; p <= k; ++p)
        for (int i = p; i <= n; ++i)
            for (int j = p - 1; j < i; ++j)
                dp[i][p] = std::min(dp[i][p], dp[j][p - 1] + cost[j][i - 1]);
    return dp[n][k];
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = palindromePartition(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", 2, 1, trials);
    test("aabbc", 3, 0, trials);
    test("leetcode", 8, 0, trials);
    return 0;
}


