#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

int minimumChanges(std::string s, int k)
{
    const int n = static_cast<int>(s.size());
    int g[201][201];
    int f[201][101];
    std::memset(g, 0x3f, sizeof(g));
    std::memset(f, 0x3f, sizeof(f));
    f[0][0] = 0;
    for (int i = 1; i <= n; ++i)
    {
        for (int j = i; j <= n; ++j)
        {
            int m = j - i + 1;
            for (int d = 1; d < m; ++d)
            {
                if (m % d == 0)
                {
                    int cnt = 0;
                    for (int l = 0; l < m; ++l)
                    {
                        int r = (m / d - 1 - l / d) * d + l % d;
                        if (l >= r)
                            break;
                        cnt += (s[i - 1 + l] != s[i - 1 + r]);
                    }
                    g[i][j] = std::min(g[i][j], cnt);
                }
            }
        }
    }
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= k; ++j)
            for (int h = 0; h < i - 1; ++h)
                f[i][j] = std::min(f[i][j], f[h][j - 1] + g[h + 1][i]);
    return f[n][k];
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumChanges(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcac", 2, 1, trials);
    test("abcdef", 2, 2, trials);
    test("aabbaa", 3, 0, trials);
    return 0;
}


