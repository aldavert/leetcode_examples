#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int nthUglyNumber(int n)
{
    std::vector<int> result = {0, 1};
    for(int i = 2, idx_2 = 1, idx_3 = 1, idx_5 = 1; i <= n; ++i)
    {
        int val_i2 = result[idx_2] * 2;
        int val_i3 = result[idx_3] * 3;
        int val_i5 = result[idx_5] * 5;
        result.push_back(std::min({val_i2, val_i3, val_i5}));
        if (result.back() == val_i2) ++idx_2;
        if (result.back() == val_i3) ++idx_3;
        if (result.back() == val_i5) ++idx_5;
    }
    return result.back();
}
#elif 0
int nthUglyNumber(int n)
{
    // This is a dumb solution to increase the speed of the code. Since the number
    // of ugly numbers is rather small, you can create a static vector that will be
    // computed just once in the first call and, the remaining test calls will
    // read the result from this vector instead of computing it. This is a trick
    // to make the runtime faster in Leetcode.
    static std::vector<long> values = []()
    {
        std::vector<long> result;
        std::priority_queue<long, std::vector<long>, std::greater<long> > q;
        std::unordered_set<long> present;
        std::array<long, 3> primes = {2, 3, 5};
        q.push(1);
        result.push_back(0);
        for (int i = 0; i <= 1690; ++i)
        {
            result.push_back(q.top());
            q.pop();
            for (long p : primes)
            {
                if (long nvalue = result.back() * p;
                    present.find(nvalue) == present.end())
                {
                    present.insert(nvalue);
                    q.push(nvalue);
                }
            }
        }
        return result;
    }();
    return static_cast<int>(values[n]);
}
#else
int nthUglyNumber(int n)
{
    std::priority_queue<long, std::vector<long>, std::greater<long> > q;
    std::unordered_set<long> present;
    std::array<long, 3> primes = {2, 3, 5};
    q.push(1);
    long result = 0;
    for (int i = 0; i < n; ++i)
    {
        result = q.top();
        q.pop();
        for (long p : primes)
        {
            if (long nvalue = result * p;
                present.find(nvalue) == present.end())
            {
                present.insert(nvalue);
                q.push(nvalue);
            }
        }
    }
    return static_cast<int>(result);
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = nthUglyNumber(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, 12, trials);
    test(1, 1, trials);
    test(1690, 2123366400, trials);
    return 0;
}


