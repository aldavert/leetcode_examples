#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int minimumIndex(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::unordered_map<int, int> mp;
    for (int num : nums) ++mp[num];
    int dominant = -1;
    for (const auto &it : mp)
    {
        if (it.second > n / 2)
        {
            dominant = it.first;
            break;
        }
    }
    std::vector<int> prefix(n);
    if (nums[0] == dominant)
        prefix[0] = 1;
    for (int i = 1; i < n; ++i)
    {
        prefix[i] = prefix[i - 1];
        if (nums[i] == dominant)
            prefix[i]++;
    }
    for (int i = 0; i < n; ++i)
        if ((prefix[i] > (i + 1) / 2) && (prefix[n - 1] - prefix[i] > (n - i - 1) / 2))
            return i;
    return -1;
}
#else
int minimumIndex(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::unordered_map<int, int> count[2];
    for (int num : nums) ++count[1][num];
    for (int i = 0; i < n; ++i)
    {
        int freq[2] = { ++count[0][nums[i]], --count[1][nums[i]] };
        if ((freq[0] * 2 > i + 1) && (freq[1] * 2 > n - 1 - i))
            return i;
    }
    return -1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumIndex(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 2}, 2, trials);
    test({2, 1, 3, 1, 1, 1, 7, 1, 2, 1}, 4, trials);
    test({3, 3, 3, 3, 7, 2, 2}, -1, trials);
    return 0;
}


