#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> smallerNumbersThanCurrent(std::vector<int> nums)
{
    const size_t n = nums.size();
    if (n <= 1) return nums;
    std::vector<std::tuple<int, size_t> > sorted_nums(n);
    std::vector<int> result(n);
    for (size_t i = 0; i < n; ++i)
        sorted_nums[i] = { nums[i], i };
    std::sort(sorted_nums.begin(), sorted_nums.end());
    auto [previous_value, previous_index] = sorted_nums[0];
    result[previous_index] = 0;
    for (size_t i = 1; i < n; ++i)
    {
        auto [value, index] = sorted_nums[i];
        if (value == previous_value)
            result[index] = result[previous_index];
        else result[index] = static_cast<int>(i);
        previous_value = value;
        previous_index = index;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallerNumbersThanCurrent(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 1, 2, 2, 3}, {4, 0, 1, 1, 3}, trials);
    test({6, 5, 4, 8}, {2, 1, 0, 3}, trials);
    test({7, 7, 7, 7}, {0, 0, 0, 0}, trials);
    return 0;
}


