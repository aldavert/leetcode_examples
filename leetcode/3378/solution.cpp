#include "../common/common.hpp"
#include <unordered_map>
#include <numeric>

// ############################################################################
// ############################################################################

int countComponents(std::vector<int> nums, [[maybe_unused]] int threshold)
{
    std::sort(nums.begin(), nums.end());
    int result = 0;
    while (!nums.empty() && (nums.back() > threshold))
    {
        ++result;
        nums.pop_back();
    }
    const int n = static_cast<int>(nums.size());
    std::vector<int> id(n), rank(n, 1);
    for (int i = 0; i < n; ++i) id[i] = i;
    auto find = [&](auto &&self, int u) -> int
    {
        return ((id[u] == u)?u:(id[u] = self(self, id[u])));
    };
    auto merge = [&](int u, int v) -> bool
    {
        const int i = find(find, u), j = find(find, v);
        if (i == j) return false;
        if (rank[i] >= rank[j])
        {
            id[j] = i;
            rank[i] += rank[j];
        }
        else
        {
            id[i] = j;
            rank[j] += rank[i];
        }
        return true;
    };
    std::unordered_map<int, int> min_idx;
    auto tryMerge = [&](int i, int divisor)
    {
        if (auto search = min_idx.find(divisor); search != min_idx.end())
        {
            if (std::lcm(nums[i], nums[search->second]) <= threshold)
                merge(i, search->second);
        }
        else min_idx[divisor] = i;
    };
    for (int i = 0; i < n; ++i)
        for (int divisor = 1; divisor * divisor <= nums[i]; ++divisor)
            if (nums[i] % divisor == 0)
                tryMerge(i, divisor),
                tryMerge(i, nums[i] / divisor);
    std::unordered_set<int> components;
    for (int i = 0; i < n; ++i)
        components.insert(find(find, i));
    return result + static_cast<int>(components.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int threshold, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countComponents(nums, threshold);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4, 8, 3, 9}, 5, 4, trials);
    test({2, 4, 8, 3, 9, 12}, 10, 2, trials);
    return 0;
}


