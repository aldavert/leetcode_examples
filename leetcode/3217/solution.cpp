#include "../common/common.hpp"
#include "../common/list.hpp"
#include <utility>

// ############################################################################
// ############################################################################

ListNode * modifiedList(std::vector<int> nums, ListNode * head)
{
    ListNode start(0, head);
    std::unordered_set<int> to_remove{nums.begin(), nums.end()};
    for (ListNode * current = head, * prev = &start; current;)
    {
        if (to_remove.contains(current->val))
        {
            prev->next = current->next;
            current->next = nullptr;
            delete current;
            current = prev->next;
        }
        else prev = std::exchange(current, current->next);
    }
    head = start.next;
    start.next = nullptr;
    return head;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> list,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(list);
        ListNode * result_head = modifiedList(nums, head);
        result = list2vec(result_head);
        delete result_head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, {1, 2, 3, 4, 5}, {4, 5}, trials);
    test({1}, {1, 2, 1, 2, 1, 2}, {2, 2, 2}, trials);
    test({1}, {2, 1, 2, 1, 2, 1, 2}, {2, 2, 2, 2}, trials);
    test({5}, {1, 2, 3, 4}, {1, 2, 3, 4}, trials);
    return 0;
}


