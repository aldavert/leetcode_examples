#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

std::string greatestLetter(std::string s)
{
    std::bitset<26> upper, lower;
    for (char letter : s)
    {
        if ((letter >= 'a') && (letter <= 'z')) lower[letter - 'a'] = true;
        else upper[letter - 'A'] = true;
    }
    for (int i = 25; i >= 0; --i)
        if (upper[i] && lower[i]) return std::string(1, static_cast<char>('A' + i));
    return "";
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = greatestLetter(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("lEeTcOdE", "E", trials);
    test("arRAzFif", "R", trials);
    test("AbCdEfGhIjK", "", trials);
    return 0;
}


