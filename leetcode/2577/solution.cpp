#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minimumTime(std::vector<std::vector<int> > grid)
{
    struct Info
    {
        int time = 0;
        int i = 0;
        int j = 0;
        bool operator<(const Info &other) const { return time > other.time; }
    };
    constexpr int directions[] = {0, 1, 0, -1, 0};
    if ((grid[0][1] > 1) && (grid[1][0] > 1))
        return -1;
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    std::priority_queue<Info> heap;
    std::vector<std::vector<bool> > seen(m, std::vector<bool>(n));
    
    heap.push({0, 0, 0});
    seen[0][0] = true;
    while (!heap.empty())
    {
        auto [time, i, j] = heap.top();
        heap.pop();
        if ((i == m - 1) && (j == n - 1))
            return time;
        for (int k = 0; k < 4; ++k)
        {
            int x = i + directions[k], y = j + directions[k + 1];
            if ((x < 0) || (x == m) || (y < 0) || (y == n))
                continue;
            if (seen[x][y])
                continue;
            int extra_wait = (grid[x][y] - time) % 2 == 0 ? 1 : 0;
            int next_time = std::max(time + 1, grid[x][y] + extra_wait);
            heap.push({next_time, x, y});
            seen[x][y] = true;
        }
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumTime(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 3, 2}, {5, 1, 2, 5}, {4, 3, 8, 6}}, 7, trials);
    test({{0, 2, 4}, {3, 2, 1}, {1, 0, 4}}, -1, trials);
    return 0;
}


