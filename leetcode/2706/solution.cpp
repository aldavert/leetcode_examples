#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int buyChoco(std::vector<int> prices, int money)
{
    int cheapest[2] = { std::numeric_limits<int>::max(),
                        std::numeric_limits<int>::max() };
    for (int price : prices)
    {
        if (price < cheapest[0]) cheapest[1] = std::exchange(cheapest[0], price);
        else if (price < cheapest[1]) cheapest[1] = price;
    }
    int result = money - cheapest[0] - cheapest[1];
    return (result >= 0)?result:money;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> prices, int money, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = buyChoco(prices, money);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2}, 3, 0, trials);
    test({3, 2, 3}, 3, 3, trials);
    test({1, 1, 3, 2, 5, 2}, 5, 3, trials);
    return 0;
}


