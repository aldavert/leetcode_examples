#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string maximumNumber(std::string num, std::vector<int> change)
{
    for (bool mutated = false; char &c : num)
    {
        int d = c - '0';
        c = static_cast<char>('0' + std::max(d, change[d]));
        if (mutated && (d > change[d]))
            return num;
        if (d < change[d])
            mutated = true;
    }
    return num;
}

// ############################################################################
// ############################################################################

void test(std::string num,
          std::vector<int> change,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumNumber(num, change);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("132", {9, 8, 5, 0, 3, 6, 4, 2, 6, 8}, "832", trials);
    test("021", {9, 4, 3, 5, 7, 2, 1, 9, 0, 6}, "934", trials);
    test("5", {1, 4, 7, 5, 3, 2, 5, 6, 9, 4}, "5", trials);
    return 0;
}


