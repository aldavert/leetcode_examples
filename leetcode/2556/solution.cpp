#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isPossibleToCutPath(std::vector<std::vector<int> > grid)
{
    const int n = static_cast<int>(grid.size()),
              m = static_cast<int>(grid[0].size());
    auto hasPath = [&](auto &&self, int i, int j) -> bool
    {
        if ((i == n) || (j == m)) return false;
        if ((i == n - 1) && (j == m - 1)) return true;
        if (grid[i][j] == 0) return false;
        grid[i][j] = 0;
        return self(self, i + 1, j) || self(self, i, j + 1);
    };
    if (!hasPath(hasPath, 0, 0)) return true;
    grid[0][0] = 1;
    return !hasPath(hasPath, 0, 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPossibleToCutPath(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 1}, {1, 0, 0}, {1, 1, 1}}, true, trials);
    test({{1, 1, 1}, {1, 0, 1}, {1, 1, 1}}, false, trials);
    return 0;
}


