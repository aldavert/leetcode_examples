#include "../common/common.hpp"
#include <queue>
#include <limits>

// ############################################################################
// ############################################################################

int minTimeToReach(std::vector<std::vector<int> > moveTime)
{
    constexpr int dirs[] = { -1, 0, 1, 0, -1 };
    const int n_rows = static_cast<int>(moveTime.size()),
              n_cols = static_cast<int>(moveTime[0].size());
    struct Position
    {
        int time = 0;
        int row = 0;
        int column = 0;
        bool operator<(const Position &other) const { return (time > other.time); }
    };
    
    std::vector<std::vector<int> > travel_time(n_rows, std::vector<int>(n_cols, -1));
    std::priority_queue<Position> q;
    q.push({0, 0, 0});
    while (!q.empty())
    {
        auto [t, r, c] = q.top();
        q.pop();
        if (travel_time[r][c] != -1) continue;
        travel_time[r][c] = t;
        if ((r == n_rows - 1) && (c == n_cols - 1))
            continue;
        for (int i = 0; i < 4; ++i)
        {
            int row = r + dirs[i],
                col = c + dirs[i + 1];
            if ((row >= 0) && (col >= 0) && (row < n_rows) && (col < n_cols)
            &&  (travel_time[row][col] == -1))
                q.push({std::max(moveTime[row][col], t) + 1, row, col});
        }
    }
    return travel_time[n_rows - 1][n_cols - 1];
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > moveTime,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int t = 0; t < trials; ++t)
        result = minTimeToReach(moveTime);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 4}, {4, 4}}, 6, trials);
    test({{0, 0, 0}, {0, 0, 0}}, 3, trials);
    test({{0, 1}, {1, 2}}, 3, trials);
    test({{0, 2}, {4, 4}}, 5, trials);
    return 0;
}


