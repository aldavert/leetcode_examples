#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = n;
    std::vector<int> work(nums);
    std::sort(work.begin(), work.end());
    work.erase(std::unique(work.begin(), work.end()), work.end());
    
    for (int i = 0; i < n; ++i)
    {
        auto unique_length = std::upper_bound(work.begin(), work.end(), work[i] + n - 1)
                           - work.begin();
        result = std::min(result, n - static_cast<int>(unique_length - i));
    }
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 5, 3}, 0, trials);
    test({1, 2, 3, 5, 6}, 1, trials);
    test({1, 10, 100, 1000}, 3, trials);
    return 0;
}


