#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> getCoprimes(std::vector<int> nums,
                             std::vector<std::vector<int> > edges)
{
    const int n = static_cast<int>(nums.size());
    std::vector<std::vector<int> > g(n), vt(51, {0});
    std::vector<int> cp[51], path{-1}, result(n, -1);
    
    for (auto &e : edges)
    {
        g[e[0]].push_back(e[1]);
        g[e[1]].push_back(e[0]);
    }
    for (int i = 1; i <= 50; ++i)
    {
        for (int j = i + 1; j <= 50; ++j) 
        {
            if (std::gcd(i, j) == 1)
            {
                cp[i].push_back(j);
                cp[j].push_back(i);
            }
        }
    }
    cp[1].push_back(1);
    auto dfs = [&](auto &&self, int c, int p, int d) -> void
    {
        int r = 0;
        for (int v : cp[nums[c]])
            r = std::max(r, vt[v].back());
        result[c] = path[r];
        path.push_back(c);
        vt[nums[c]].push_back(d);
        for (int x : g[c])
        {
            if (x == p) continue;
            self(self, x, c, d + 1);
        }
        path.pop_back();
        vt[nums[c]].pop_back();
    };
    dfs(dfs, 0, -1, 1);
    return result;
}
#else
std::vector<int> getCoprimes(std::vector<int> nums,
                             std::vector<std::vector<int> > edges)
{
    const int n = static_cast<int>(nums.size());
    std::vector<std::pair<int, int> > recordings_val_index[51];
    std::vector<std::vector<int> > tree(n);
    std::vector<int> result(n);
    int visited[100010] = {};
    
    for (auto edge : edges)
    {
        int u = edge[0], v = edge[1];
        tree[u].push_back(v);
        tree[v].push_back(u);
    }
    auto dfs = [&](auto &&self, int curr, int depth) -> void
    {
        int ret = -1, level = -1;
        for (int val = 1; val <= 50; ++val)
        {
            if (!recordings_val_index[val].empty() && std::gcd(val, nums[curr]) == 1)
            {
                auto [idx, thisLevel] = recordings_val_index[val].back();
                if (thisLevel > level)
                {
                    level = thisLevel;
                    ret = idx;
                }
            }
        }
        result[curr] = ret;
        visited[curr] = 1;
        recordings_val_index[nums[curr]].emplace_back(curr, depth);
        for (auto neighbor : tree[curr])
        {
            if (visited[neighbor]) continue;
            self(self, neighbor, depth + 1);
        }
        visited[curr] = 0;
        recordings_val_index[nums[curr]].pop_back();
    };
    dfs(dfs, 0, 0);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::vector<int> > edges,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getCoprimes(nums, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 3, 2}, {{0, 1}, {1, 2}, {1, 3}}, {-1, 0, 0, 1}, trials);
    test({5, 6, 10, 2, 3, 6, 15},
         {{0, 1}, {0, 2}, {1, 3}, {1, 4}, {2, 5}, {2, 6}},
         {-1, 0, -1, 0, 0, 0, -1}, trials);
    return 0;
}


