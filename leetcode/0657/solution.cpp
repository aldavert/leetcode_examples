#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool judgeCircle(std::string moves)
{
    int x = 0, y = 0;
    for (char m : moves)
    {
        if      (m == 'L') ++x;
        else if (m == 'R') --x;
        else if (m == 'U') ++y;
        else if (m == 'D') --y;
    }
    return (x == 0) && (y == 0);
}

// ############################################################################
// ############################################################################

void test(std::string moves, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = judgeCircle(moves);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("UD",  true, trials);
    test("LL", false, trials);
    return 0;
}


