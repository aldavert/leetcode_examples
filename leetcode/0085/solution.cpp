#include "../common/common.hpp"
#include "test_large.hpp"

// ############################################################################
// ############################################################################

#if 1
int maximalRectangle(std::vector<std::vector<char> > matrix)
{
    if (matrix.empty()) return 0;
    const int cols = static_cast<int>(matrix[0].size());
    int left[201], right[201], height[201], result = 0;
    for (int i = 0; i < cols; ++i)
    {
        left[i] = height[i] = 0;
        right[i] = cols;
    }
    for (const auto &row : matrix)
    {
        int right_smallest_zero = cols;
        
        for (int j = cols - 1; j >= 0; --j)
        {
            if (row[j] == '1')
                right[j] = std::min(right[j], right_smallest_zero);
            else
            {
                right[j] = cols;
                right_smallest_zero = j;
            }
        }
        int left_largest_one = 0;
        for (int j = 0; j < cols; ++j)
        {
            if (row[j] == '1')
            {
                ++height[j];
                left[j] = std::max(left[j], left_largest_one);
            }
            else
            {
                height[j] = 0;
                left[j] = 0;
                left_largest_one = j + 1;
            }
            result = std::max(result, height[j] * (right[j] - left[j]));
        }
    }
    return result;
}
#else
int maximalRectangle(std::vector<std::vector<char> > matrix)
{
    if (matrix.size() == 0) return 0;
    const int cols = static_cast<int>(matrix[0].size());
    int result = 0;
    if (cols == 1)
    {
        int count = 0;
        for (const auto &row : matrix)
        {
            for (int i = 0; i < cols; ++i)
                count = (row[i] == '1') * (count + 1);
            result = std::max(result, count);
        }
    }
    else
    {
        int count[201] = {};
        
        for (const auto &row : matrix)
        {
            for (int i = 0; i < cols; ++i)
                count[i] = (row[i] == '1') * (count[i] + 1);
            
            for (int i = 0; i < cols - 1; ++i)
            {
                int min = count[i];
                result = std::max(result, count[i]);
                for (int j = i + 1; j < cols; ++j)
                {
                    min = std::min(min, count[j]);
                    result = std::max(count[j], std::max(result, min * (j - i + 1)));
                }
            }
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > matrix, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximalRectangle(matrix);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'1', '0', '1', '0', '0'},
          {'1', '0', '1', '1', '1'},
          {'1', '1', '1', '1', '1'},
          {'1', '0', '0', '1', '0'}}, 6, trials);
    test({}, 0, trials);
    test({{'0'}}, 0, trials);
    test({{'1'}}, 1, trials);
    test({{'0', '0'}}, 0, trials);
    test(testA::matrix, testA::solution, trials);
    return 0;
}


