#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int stoneGameVIII(std::vector<int> stones)
{
    const int n = static_cast<int>(stones.size());
    std::vector<int> prefix(n), dp(n, std::numeric_limits<int>::lowest());
    
    std::partial_sum(stones.begin(), stones.end(), prefix.begin());
    dp[n - 2] = prefix.back();
    for (int i = n - 3; i >= 0; --i)
        dp[i] = std::max(dp[i + 1], prefix[i + 1] - dp[i + 1]);
    
    return dp[0];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> stones, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = stoneGameVIII(stones);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, 2, -3, 4, -5}, 5, trials);
    test({7, -6, 5, 10, 5, -2, -6}, 13, trials);
    test({-10, -12}, -22, trials);
    return 0;
}


