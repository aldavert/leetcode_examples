#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int minimumSeconds(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = n;
    std::unordered_map<int, std::vector<int> > num_to_indices;
    
    for (int i = 0; i < n; ++i)
        num_to_indices[nums[i]].push_back(i);
    for (const auto& [_, indices] : num_to_indices)
    {
        int seconds = (indices.front() + n - indices.back()) / 2;
        for (int i = 1, m = static_cast<int>(indices.size()); i < m; ++i)
            seconds = std::max(seconds, (indices[i] - indices[i - 1]) / 2);
        result = std::min(result, seconds);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumSeconds(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1, 2}, 1, trials);
    test({2, 1, 3, 3, 2}, 2, trials);
    return 0;
}


