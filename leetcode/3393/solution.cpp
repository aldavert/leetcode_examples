#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countPathsWithXorValue(std::vector<std::vector<int> > grid, int k)
{
    constexpr int MOD = 1'000'000'007,
                  MAX = 15;
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    using DPGRID = std::vector<std::array<int, MAX + 1> >;
    DPGRID dp[2] = { DPGRID(n), DPGRID(n) };
    bool current = false;
    for (int i = 0; i < m; ++i, current = !current)
    {
        for (int j = 0; j < n; ++j)
        {
            dp[!current][j].fill(0);
            if ((i == 0) && (j == 0)) dp[0][0][grid[0][0]] = 1;
            for (int xors = 0; xors <= MAX; ++xors)
            {
                if (i + 1 < m)
                {
                    const int new_xor = xors ^ grid[i + 1][j];
                    dp[!current][j][new_xor] += dp[current][j][xors];
                    dp[!current][j][new_xor] %= MOD;
                }
                if (j + 1 < n)
                {
                    const int new_xor = xors ^ grid[i][j + 1];
                    dp[current][j + 1][new_xor] += dp[current][j][xors];
                    dp[current][j + 1][new_xor] %= MOD;
                }
            }
        }
    }
    return dp[!current][n - 1][k];
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPathsWithXorValue(grid, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 1, 5}, {7, 10, 0}, {12, 6, 4}}, 11, 3, trials);
    test({{1, 3, 3, 3}, {0, 3, 3, 2}, {3, 0, 1, 1}}, 2, 5, trials);
    test({{1, 1, 1, 2}, {3, 0, 3, 2}, {3, 0, 2, 2}}, 10, 0, trials);
    return 0;
}


