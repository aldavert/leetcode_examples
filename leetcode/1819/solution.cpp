#include "../common/common.hpp"
#include <numeric>
#include <bitset>

// ############################################################################
// ############################################################################

#if 0
int countDifferentSubsequenceGCDs(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int max_element = std::numeric_limits<int>::lowest(), result = 0;
    std::bitset<2'000'001> present;
    
    for (int i = 0; i < n; ++i)
    {
        max_element = std::max(max_element, nums[i]);
        present[nums[i]] = true;
    }
    for (int i = 1; i <= max_element; ++i)
    {
        for (int j = i, current_gcd = 0; j <= max_element; j += i)
        {
            if (present[j])
                current_gcd = std::gcd(current_gcd, j);
            if (current_gcd == i)
            {
                ++result;
                break;
            }
        }
    }
    return result;
}
#elif 1
int countDifferentSubsequenceGCDs(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int max_element = std::numeric_limits<int>::lowest(), result = 0;
    char present[2'000'001] = {};
    
    for (int i = 0; i < n; ++i)
    {
        max_element = std::max(max_element, nums[i]);
        present[nums[i]] = true;
    }
    for (int i = 1; i <= max_element; ++i)
    {
        for (int j = i, current_gcd = 0; j <= max_element; j += i)
        {
            if (present[j])
                current_gcd = std::gcd(current_gcd, j);
            if (current_gcd == i)
            {
                ++result;
                break;
            }
        }
    }
    return result;
}
#else
int countDifferentSubsequenceGCDs(std::vector<int> nums)
{
    int max_number = *std::max_element(nums.begin(), nums.end());
    std::vector<int> factor(max_number + 1);
    int result = 0;
    
    for (int num : nums)
    {
        for (int i = 1; i * i <= num; ++i)
        {
            if (num % i == 0)
            {
                int j = num / i;
                factor[i] = std::gcd(factor[i], num);
                factor[j] = std::gcd(factor[j], num);
            }
        }
    }
    for (int i = 1; i <= max_number; ++i)
        result += (factor[i] == i);
    
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countDifferentSubsequenceGCDs(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({6, 10, 3}, 5, trials);
    test({5, 15, 40, 5, 6}, 7, trials);
    return 0;
}


