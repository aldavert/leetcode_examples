#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
long long findScore(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    long long result = 0;
    for (int i = 0; i < n; i += 2)
    {
        int s = i;
        while ((i + 1 < n) && (nums[i] > nums[i + 1]))
            ++i;
        for (int j = i; j >= s; j -= 2)
            result += nums[j];
    }
    return result;
}
#else
long long findScore(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::set<std::pair<int, int> > num_and_indices;
    std::vector<bool> seen(nums.size());
    long result = 0;
    
    for (int i = 0; i < n; ++i)
        num_and_indices.insert({nums[i], i});
    for (const auto& [num, i] : num_and_indices)
    {
        if (seen[i]) continue;
        if (i > 0) seen[i - 1] = true;
        if (i + 1 < n) seen[i + 1] = true;
        seen[i] = true;
        result += num;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findScore(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3, 4, 5, 2}, 7, trials);
    test({2, 3, 5, 1, 3, 2}, 5, trials);
    return 0;
}


