#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int characterReplacement(std::string s, int k)
{
    const int n = static_cast<int>(s.size());
    int max_count = 0, count[26] = {}, l = 0, r = 0;
    for (r = 0; r < n; ++r)
    {
        max_count = std::max(max_count, ++count[s[r] - 'A']);
        if (max_count + k < r - l + 1)
            --count[s[l++] - 'A'];
    }
    return r - l;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = characterReplacement(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ABAB", 2, 4, trials);
    test("AABABBA", 1, 4, trials);
    return 0;
}


