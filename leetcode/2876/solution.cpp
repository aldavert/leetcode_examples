#include "../common/common.hpp"
#include <queue>
#include <stack>

// ############################################################################
// ############################################################################

std::vector<int> countVisitedNodes(std::vector<int>& edges)
{
    const int n = static_cast<int>(edges.size());
    std::vector<int> result(n), in_degrees(n);
    std::vector<bool> seen(n);
    std::queue<int> q;
    std::stack<int> stack;
    
    for (const int v : edges) ++in_degrees[v];
    for (int i = 0; i < n; ++i)
        if (in_degrees[i] == 0)
            q.push(i);
    while (!q.empty())
    {
        const int u = q.front();
        q.pop();
        if (--in_degrees[edges[u]] == 0)
            q.push(edges[u]);
        stack.push(u);
        seen[u] = true;
    }
    for (int i = 0; i < n; ++i)
    {
        if (!seen[i])
        {
            int cycle_length = 0;
            for (int u = i; !seen[u]; u = edges[u])
            {
                ++cycle_length;
                seen[u] = true;
            }
            result[i] = cycle_length;
            for (int u = edges[i]; u != i; u = edges[u])
                result[u] = cycle_length;
        }
    }
    while (!stack.empty())
    {
        const int u = stack.top();
        stack.pop();
        result[u] = result[edges[u]] + 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> edges, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countVisitedNodes(edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 0, 0}, {3, 3, 3, 4}, trials);
    test({1, 2, 3, 4, 0}, {5, 5, 5, 5, 5}, trials);
    return 0;
}


