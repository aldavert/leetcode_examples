#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int maxSubarraySumCircular(std::vector<int> nums)
{
    int sum_max = std::numeric_limits<int>::lowest();
    int sum_min = std::numeric_limits<int>::max();
    int sum_total = 0;
    for (int current_max = 0, current_min = 0; int value : nums)
    {
        sum_total += value;
        current_max = std::max(current_max + value, value);
        current_min = std::min(current_min + value, value);
        sum_max = std::max(sum_max, current_max);
        sum_min = std::min(sum_min, current_min);
    }
    return (sum_max < 0)?sum_max:std::max(sum_max, sum_total - sum_min);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSubarraySumCircular(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, -2, 3, -2}, 3, trials);
    test({5, -3, 5}, 10, trials);
    test({-3, -2, -3}, -2, trials);
    return 0;
}


