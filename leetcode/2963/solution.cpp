#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int numberOfGoodPartitions(std::vector<int> nums)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(nums.size());
    long result = 1;
    std::unordered_map<int, int> last_seen;
    
    for (int i = 0; i < n; ++i)
        last_seen[nums[i]] = i;
    for (int i = 0, max_right = 0; i < n; ++i)
    {
        if (i > max_right)
            result = (result * 2L) % MOD;
        max_right = std::max(max_right, last_seen[nums[i]]);
    }
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfGoodPartitions(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 8, trials);
    test({1, 1, 1, 1}, 1, trials);
    test({1, 2, 1, 3}, 2, trials);
    return 0;
}


