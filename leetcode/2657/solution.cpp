#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findThePrefixCommonArray(std::vector<int> A, std::vector<int> B)
{
    const int n = static_cast<int>(A.size());
    int prefix_common = 0;
    std::vector<int> result, count(n + 1);
    for (int i = 0; i < n; ++i)
    {
        if (++count[A[i]] == 2) ++prefix_common;
        if (++count[B[i]] == 2) ++prefix_common;
        result.push_back(prefix_common);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> A,
          std::vector<int> B,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findThePrefixCommonArray(A, B);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 4}, {3, 1, 2, 4}, {0, 2, 3, 4}, trials);
    test({2, 3, 1}, {3, 1, 2}, {0, 1, 3}, trials);
    return 0;
}


