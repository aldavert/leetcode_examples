#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

#if 1
int minArraySum(std::vector<int> nums, int k, int op1, int op2)
{
    auto m = [] (auto &a, auto b) -> void  { a = std::min(a, b); };
    auto f1 = [](int a) -> int { return (a + 1) >> 1; };
    auto f2 = [k](int a){ return a - k; };
    constexpr int MAX = 100'000'000;
    int dp[2][101][101];
    for (int i = 0, n = static_cast<int>(sizeof(dp) / sizeof(int)); i < n; ++i)
        *(reinterpret_cast<int *>(dp) + i) = MAX;
    dp[1][op1][op2] = 0;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        for (int i1 = op1; i1 >= std::max(0, op1 - i - 1); --i1)
        {
            for (int i2 = op2; i2 >= std::max(0, op2 - i - 1); --i2)
            {
                dp[i & 1][i1][i2] = std::min(MAX, nums[i] + dp[!(i & 1)][i1][i2]);
                if (i1 != op1)
                    m(dp[i & 1][i1][i2], f1(nums[i]) + dp[!(i&1)][i1+1][i2]);
                if ((i2 != op2) && (nums[i] >= k))
                {
                    m(dp[i & 1][i1][i2], f2(nums[i]) + dp[!(i & 1)][i1][i2 + 1]);
                    if (i1 != op1)
                    {
                        m(dp[i & 1][i1][i2],
                          f1(f2(nums[i])) + dp[!(i & 1)][i1 + 1][i2 + 1]);
                        if (f1(nums[i]) >= k)
                            m(dp[i & 1][i1][i2],
                              f2(f1(nums[i])) + dp[!(i & 1)][i1 + 1][i2 + 1]);
                    }
                }
            }
        }
    }
    int result = MAX;
    for (int i1 = 0; i1 <= op1; ++i1)
        for (int i2 = 0; i2 <= op2; ++i2)
            m(result, dp[!(nums.size() & 1)][i1][i2]);
    return result;
}
#else
int minArraySum(std::vector<int> nums, int k, int op1, int op2)
{
    const int n = static_cast<int>(nums.size());
    std::vector<std::vector<std::vector<int> > > dp(n + 1,
            std::vector<std::vector<int> >(op1 + 1,
            std::vector<int>(op2 + 1, std::numeric_limits<int>::max())));
    
    for (int i = 0; i <= op1; ++i)
        for (int j = 0; j <= op2; ++j)
            dp[n][i][j] = 0;
    for (int i = n - 1; i >= 0; --i)
    {
        for (int o1 = 0; o1 <= op1; ++o1)
        {
            for (int o2 = 0; o2 <= op2; ++o2)
            {
                int sum = nums[i] + dp[i + 1][o1][o2];
                if (o1 > 0)
                    sum = std::min(sum, (nums[i] + 1) / 2 + dp[i + 1][o1 - 1][o2]);
                if ((o2 > 0) && (nums[i] >= k))
                    sum = std::min(sum, nums[i] - k + dp[i + 1][o1][o2 - 1]);
                if ((o1 > 0) && (o2 > 0))
                {
                    if ((nums[i] + 1) / 2 >= k)
                        sum = std::min(sum, (nums[i] + 1) / 2
                                          - k + dp[i + 1][o1 - 1][o2 - 1]);
                    if (nums[i] >= k)
                        sum = std::min(sum, (nums[i] - k + 1) / 2
                                          + dp[i + 1][o1 - 1][o2 - 1]);
                }
                dp[i][o1][o2] = sum;
            }
        }
    }
    return dp[0][op1][op2];
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          int op1,
          int op2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minArraySum(nums, k, op1, op2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 8, 3, 19, 3}, 3, 1, 1, 23, trials);
    test({2, 4, 3}, 3, 2, 1, 3, trials);
    test({3, 8}, 0, 0, 2, 11, trials);
    return 0;
}


