#include "../common/common.hpp"
#include <map>

// ############################################################################
// ############################################################################

class MyCalendarThree
{
    std::map<int, int> m_timeline;
public:
    MyCalendarThree() {}
    int book(int start, int end)
    {
        ++m_timeline[start];
        --m_timeline[end];
        int result = 0;
        for (int active_events = 0; const auto &[_, count] : m_timeline)
            result = std::max(result, active_events += count);
        return result;
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<std::pair<int, int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        MyCalendarThree object;
        result.clear();
        for (auto [start, end] : input)
            result.push_back(object.book(start, end));
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{10, 20}, {50, 60}, {10, 40}, {5, 15}, {5, 10}, {25, 55}},
         {1, 1, 2, 3, 3, 3},
         trials);
    return 0;
}


