#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countGoodArrays(int n, int m, int k)
{
    constexpr int MOD = 1'000'000'007;
    std::vector<long> fact(n + 1), inv_fact(n + 1), inv(n + 1);
    auto modPow = [&](auto &&self, long x, long nn) -> long
    {
        if (nn == 0) return 1;
        if (nn % 2 == 1) return x * self(self, x % MOD, (nn - 1)) % MOD;
        return self(self, x * x % MOD, (nn / 2)) % MOD;
    };
    auto nCk = [&](int nn, int nk) -> int
    {
        return static_cast<int>(fact[nn] * inv_fact[nk] % MOD
                              * inv_fact[nn - nk] % MOD);
    };
    
    fact[0] = inv_fact[0] = 1;
    inv[0] = inv[1] = 1;
    for (int i = 1; i <= n; ++i)
    {
        if (i >= 2) inv[i] = MOD - MOD / i * inv[MOD % i] % MOD;
        fact[i] = fact[i - 1] * i % MOD;
        inv_fact[i] = inv_fact[i - 1] * inv[i] % MOD;
    }
    return static_cast<int>(m
                          * modPow(modPow, m - 1, n - k - 1) % MOD
                          * nCk(n - 1, k) % MOD);
}

// ############################################################################
// ############################################################################

void test(int n, int m, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countGoodArrays(n, m, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 2, 1, 4, trials);
    test(4, 2, 2, 6, trials);
    test(5, 2, 0, 2, trials);
    return 0;
}


