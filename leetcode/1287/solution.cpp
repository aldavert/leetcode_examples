#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findSpecialInteger(std::vector<int> arr)
{
    const int thr = static_cast<int>(arr.size()) / 4;
    int maximum_value = -1, maximum_frequency = 0;
    int current_value = -1, current_frequency = 0;
    for (int value : arr)
    {
        if (value == current_value) ++current_frequency;
        else
        {
            if (current_frequency > maximum_frequency)
            {
                maximum_frequency = current_frequency;
                maximum_value = current_value;
                if (current_frequency > thr)
                    return current_value;
            }
            current_value = value;
            current_frequency = 1;
        }
    }
    return (maximum_frequency > current_frequency)?maximum_value:current_value;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findSpecialInteger(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 6, 6, 6, 6, 7, 10}, 6, trials);
    test({1, 1}, 1, trials);
    return 0;
}


