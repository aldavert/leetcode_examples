#include "../common/common.hpp"
#include <numeric>
#include <set>
#include <queue>

// ############################################################################
// ############################################################################

#if 1
bool canMeasureWater(int jug1Capacity, int jug2Capacity, int targetCapacity)
{
    return (targetCapacity == 0)
        || ((jug1Capacity + jug2Capacity >= targetCapacity)
         && (targetCapacity % std::gcd(jug1Capacity, jug2Capacity) == 0));
}
#elif 0
bool canMeasureWater(int jug1Capacity, int jug2Capacity, int targetCapacity)
{
    if (targetCapacity == 0) return true;
    struct Status
    {
        int jug1 = 0;
        int jug2 = 0;
        bool operator<(const Status &other) const
        {
            return  (jug1 < other.jug1)
                || ((jug1 == other.jug1) && (jug2 < other.jug2));
        }
    };
    enum class Action { FILL1, FILL2, EMPTY1, EMPTY2, POUR1TO2, POUR2TO1 };
    std::string action_text[] =
        { "Fill 1", "Fill 2", "Empty 1", "Empty 2", "Pour 1 to 2", "Pour 2 to 1" };
    struct Trace
    {
        std::vector<std::tuple<Action, Status> > trace;
    };
    std::queue<std::tuple<Status, Trace> > q;
    auto push = [&](Status s, Trace t, Action a) -> void
    {
        t.trace.push_back({a, s});
        q.push({s, t});
    };
    q.push({{0, 0}, {}});
    std::set<Status> done;
    Trace result;
    while (!q.empty())
    {
        auto [status, trace] = q.front();
        q.pop();
        if (done.find(status) != done.end()) continue;
        done.insert(status);
        if ((status.jug1 == targetCapacity)
        ||  (status.jug2 == targetCapacity)
        ||  (status.jug1 + status.jug2 == targetCapacity))
        {
            if (result.trace.empty()
            || (trace.trace.size() < result.trace.size()))
                result = trace;
        }
        if (status.jug1 < jug1Capacity)
            push({jug1Capacity, status.jug2}, trace, Action::FILL1);
        if (status.jug2 < jug2Capacity)
            push({status.jug1, jug2Capacity}, trace, Action::FILL2);
        if (status.jug1 != 0)
            push({0, status.jug2}, trace, Action::EMPTY1);
        if (status.jug2 != 0)
            push({status.jug1, 0}, trace, Action::EMPTY2);
        push({std::max(0, status.jug1 - (jug2Capacity - status.jug2)),
              std::min(status.jug2 + status.jug1, jug2Capacity)},
              trace, Action::POUR1TO2);
        push({std::min(status.jug1 + status.jug2, jug1Capacity),
              std::max(0, status.jug2 - (jug1Capacity - status.jug1))},
              trace, Action::POUR2TO1);
    }
    for (auto [action, stat] : result.trace)
    {
        std::cout << action_text[static_cast<int>(action)] << ": [";
        std::cout << stat.jug1 << ", " << stat.jug2 << "]\n";
    }
    return !result.trace.empty();
}
#else
bool canMeasureWater(int jug1Capacity, int jug2Capacity, int targetCapacity)
{
    struct Status
    {
        int jug1 = 0;
        int jug2 = 0;
        bool operator<(const Status &other) const
        {
            return  (jug1 < other.jug1)
                || ((jug1 == other.jug1) && (jug2 < other.jug2));
        }
    };
    std::queue<Status> q;
    q.push({0, 0});
    std::set<Status> done;
    while (!q.empty())
    {
        auto status = q.front();
        q.pop();
        if (done.find(status) != done.end()) continue;
        done.insert(status);
        if ((status.jug1 == targetCapacity)
        ||  (status.jug2 == targetCapacity)
        ||  (status.jug1 + status.jug2 == targetCapacity))
            return true;
        if (status.jug1 < jug1Capacity)
            q.push({jug1Capacity, status.jug2});
        if (status.jug2 < jug2Capacity)
            q.push({status.jug1, jug2Capacity});
        if (status.jug1 != 0)
            q.push({0, status.jug2});
        if (status.jug2 != 0)
            q.push({status.jug1, 0});
        q.push({std::max(0, status.jug1 - (jug2Capacity - status.jug2)),
                std::min(status.jug2 + status.jug1, jug2Capacity)});
        q.push({std::min(status.jug1 + status.jug2, jug1Capacity),
                std::max(0, status.jug2 - (jug1Capacity - status.jug1))});
    }
    return false;
}
#endif

// ############################################################################
// ############################################################################

void test(int jug1Capacity,
          int jug2Capacity,
          int targetCapacity,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canMeasureWater(jug1Capacity, jug2Capacity, targetCapacity);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 5, 4,  true, trials);
    test(2, 6, 5, false, trials);
    test(1, 2, 3,  true, trials);
    test(19, 31, 1,  true, trials);
    return 0;
}


