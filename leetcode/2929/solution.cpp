#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long distributeCandies(int n, int limit)
{
    auto ways = [](long value) -> long
    {
        if (value < 0) return 0;
        return (value + 2) * (value + 1) / 2;
    };
    return ways(n) - 3 * ways(n - (limit + 1)) + 3 * ways(n - 2 * (limit + 1))
         - ways(n - 3 * (limit + 1));
}

// ############################################################################
// ############################################################################

void test(int n, int limit, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = distributeCandies(n, limit);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 2, 3, trials);
    test(3, 3, 10, trials);
    return 0;
}


