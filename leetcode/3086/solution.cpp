#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long minimumMoves(std::vector<int> nums, int k, int maxChanges)
{
    constexpr int LIMIT = 3;
    long result = std::numeric_limits<long>::max();
    std::vector<int> one_indices;
    std::vector<long> prefix{0};
    
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        if (nums[i] == 1)
            one_indices.push_back(i);
    for (int one_index : one_indices)
        prefix.push_back(prefix.back() + one_index);
    const int min_ones_by_two = std::max(0, k - maxChanges);
    const int max_ones_by_two =
        std::min({k, min_ones_by_two + LIMIT, static_cast<int>(one_indices.size())});
    
    for (int m = min_ones_by_two, p = static_cast<int>(prefix.size());
         m <= max_ones_by_two; ++m)
    {
        for (int l = 0; l + m < p; ++l)
        {
            const int r = l + m;
            const long cost2 = (prefix[r] - prefix[(l + r) / 2])
                             - (prefix[(l + r + 1) / 2] - prefix[l]);
            result = std::min(result, 2 * (k - m) + cost2);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          int maxChanges,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumMoves(nums, k, maxChanges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 0, 0, 0, 1, 1, 0, 0, 1}, 3, 1, 3, trials);
    test({0, 0, 0, 0}, 2, 3, 4, trials);
    return 0;
}


