#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> minimumCost(int n,
                             std::vector<std::vector<int> > edges,
                             std::vector<std::vector<int> > query)
{
    class UnionFind
    {
    public:
        UnionFind(int n) : m_id(n), m_rank(n), m_weight(n, (1 << 17) - 1)
        {
            for (int i = 0; i < n; ++i) m_id[i] = i;
        }
        void unionByRank(int u, int v, int w)
        {
            const int i = find(u), j = find(v);
            const int new_weight = m_weight[i] & m_weight[j] & w;
            m_weight[j] = m_weight[i] = new_weight;
            if (i == j) return;
            if      (m_rank[i] < m_rank[j]) m_id[i] = j;
            else if (m_rank[i] > m_rank[j]) m_id[j] = i;
            else
            {
                m_id[i] = j;
                ++m_rank[j];
            }
        }
        int getMinCost(int u, int v)
        {
            if (u == v) return 0;
            const int i = find(u), j = find(v);
            return (i == j)?m_weight[i]:-1;
        }
    private:
        std::vector<int> m_id;
        std::vector<int> m_rank;
        std::vector<int> m_weight;
        int find(int u)
        {
            return (m_id[u] == u)?u:m_id[u] = find(m_id[u]);
        }
    };
    std::vector<int> result;
    UnionFind uf(n);
    
    for (const auto &edge : edges)
        uf.unionByRank(edge[0], edge[1], edge[2]);
    for (const auto &q : query)
        result.push_back(uf.getMinCost(q[0], q[1]));
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<std::vector<int> > query,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumCost(n, edges, query);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{0, 1, 7}, {1, 3, 7}, {1, 2, 1}}, {{0, 3}, {3, 4}}, {1, -1}, trials);
    test(3, {{0, 2, 7}, {0, 1, 15}, {1, 2, 6}, {1, 2, 1}}, {{1, 2}}, {0}, trials);
    return 0;
}


