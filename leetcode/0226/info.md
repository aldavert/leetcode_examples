# Invert Binary Tree

Given the `root` of a binary tree, invert the tree, and return *its root*.

#### Example 1:
> ```mermaid 
> flowchart LR;
> subgraph SA [ ]
> direction TB;
> A1((4))---B1((2))
> A1---C1((7))
> B1---D1((1))
> B1---E1((3))
> C1---F1((6))
> C1---G1((9))
> end
> subgraph SB [ ]
> direction TB;
> A2((4))---B2((7))
> A2---C2((2))
> B2---D2((9))
> B2---E2((6))
> C2---F2((3))
> C2---G2((1))
> end
> SA-->SB
> style A1 fill:#FFF
> style B1 fill:#FFF
> style C1 fill:#BDF
> style D1 fill:#FCA
> style E1 fill:#CAF
> style F1 fill:#FAA
> style G1 fill:#9C9
> style A2 fill:#FFF
> style C2 fill:#FFF
> style B2 fill:#BDF
> style G2 fill:#FCA
> style F2 fill:#CAF
> style E2 fill:#FAA
> style D2 fill:#9C9
> classDef white fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class SA,SB white;
> ```
> *Input:* `root = [4, 2, 7, 1, 3, 6, 9]`  
> *Output:* `[4, 7, 2, 9, 6, 3, 1]`

#### Example 2:
> ```mermaid 
> flowchart LR;
> subgraph SA [ ]
> direction TB;
> A1((2))---B1((1))
> A1---C1((3))
> end
> subgraph SB [ ]
> direction TB;
> A2((2))---B2((3))
> A2---C2((1))
> end
> SA-->SB
> style A1 fill:#FFF
> style B1 fill:#FFF
> style C1 fill:#BDF
> style A2 fill:#FFF
> style C2 fill:#FFF
> style B2 fill:#BDF
> classDef white fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class SA,SB white;
> ```
> *Input:* `root = [2, 1, 3]`  
> *Output:* `[2, 3, 1]`

#### Example 3:
> *Input:* `root = []`  
> *Output:* `[]`
 
#### Constraints:
- The number of nodes in the tree is in the range `[0, 100]`.
- `-100 <= Node.val <= 100`


