#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

std::string smallestNumber(std::string pattern)
{
    std::stack<char> s{{'1'}};
    std::string result;
    
    for (char c : pattern)
    {
        char max_so_far = s.top();
        if (c == 'I')
        {
            while (!s.empty())
            {
                max_so_far = std::max(max_so_far, s.top());
                result += s.top();
                s.pop();
            }
        }
        s.push(max_so_far + 1);
    }
    while (!s.empty())
        result += s.top(),
        s.pop();
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string pattern, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestNumber(pattern);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("IIIDIDDD", "123549876", trials);
    test("DDD", "4321", trials);
    return 0;
}


