#include "../common/common.hpp"
#include "../common/list.hpp"
#include <queue>

// ############################################################################
// ############################################################################

ListNode * mergeKLists(const std::vector<ListNode *> &lists)
{
    if (lists.empty()) return nullptr;
    
    auto cmp = [](const ListNode * left, const ListNode * right) -> bool
    {
        return (left == nullptr) || ((right != nullptr) && (left->val > right->val));
    };
    std::priority_queue<ListNode *, std::vector<ListNode *>, decltype(cmp)>
        queue(lists.begin(), lists.end(), cmp);
    if (queue.top())
    {
        ListNode * result, * ptr;
        ptr = queue.top();
        result = new ListNode(ptr->val);
        queue.pop();
        queue.push(ptr->next);
        ptr = result;
        while (queue.top())
        {
            ListNode * current = queue.top();
            queue.pop();
            
            ptr->next = new ListNode(current->val);
            ptr = ptr->next;
            queue.push(current->next);
        }
        return result;
    }
    else return nullptr;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        std::vector<ListNode *> lists;
        for (const auto &i : input)
            lists.push_back(vec2list(i));
        ListNode * lresult = mergeKLists(lists);
        result = list2vec(lresult);
        for (auto &li : lists)
            delete li;
        delete lresult;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 4, 5}, {1, 3, 4}, {2, 6}}, {1, 1, 2, 3, 4, 4, 5, 6}, trials);
    test({}, {}, trials);
    test({{}}, {}, trials);
    
    return 0;
}


