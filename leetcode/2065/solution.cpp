#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximalPathQuality(std::vector<int> values,
                       std::vector<std::vector<int> > edges,
                       int maxTime)
{
    const int n = static_cast<int>(values.size());
    int result = 0;
    std::vector<std::vector<std::pair<int, int> > > graph(n);
    std::vector<int> seen(n);
    auto dfs = [&](auto &&self, int u, int quality, int remaining_time) -> void
    {
        if (u == 0) result = std::max(result, quality);
        for (const auto& [v, time] : graph[u])
        {
            if (time > remaining_time) continue;
            int new_quality = quality + values[v] * (seen[v] == 0);
            ++seen[v];
            self(self, v, new_quality, remaining_time - time);
            --seen[v];
        }
    };
    
    seen[0] = 1;
    for (const auto &edge : edges)
    {
        graph[edge[0]].push_back({edge[1], edge[2]});
        graph[edge[1]].push_back({edge[0], edge[2]});
    }
    dfs(dfs, 0, values[0], maxTime);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> values,
          std::vector<std::vector<int> > edges,
          int maxTime,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximalPathQuality(values, edges, maxTime);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 32, 10, 43}, {{0, 1, 10}, {1, 2, 15}, {0, 3, 10}}, 49, 75, trials);
    test({5, 10, 15, 20}, {{0, 1, 10}, {1, 2, 10}, {0, 3, 10}}, 30, 25, trials);
    test({1, 2, 3, 4}, {{0, 1, 10}, {1, 2, 11}, {2, 3, 12}, {1, 3, 13}}, 50, 7, trials);
    return 0;
}


