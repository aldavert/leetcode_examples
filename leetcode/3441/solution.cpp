#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string minCostGoodCaption(std::string caption)
{
    const int n = static_cast<int>(caption.size());
    if (n < 3) return "";
    std::vector<std::array<std::pair<int, int>, 26> > dp(n - 3 + 1);
    std::vector<std::pair<int, int> > mn(n - 3 + 1);
    for (int i = n - 3; i >= 0; --i)
    {
        for (int j = 0; j < 26; ++j)
        {
            if (i == n - 3)
            {
                for (int k = i; k < i + 3; ++k)
                    dp[i][j].first += std::abs((caption[k] - 'a') - j);
                dp[i][j].second = 3;
                continue;
            }
            dp[i][j] = std::make_pair(dp[i + 1][j].first
                     + std::abs((caption[i] - 'a') - j), 1);
            if (i + 3 < n - 2)
            {
                auto [curr, c] = mn[i + 3];
                for (int k = i; k < i + 3; ++k)
                    curr += std::abs((caption[k] - 'a') - j);
                if ((curr < dp[i][j].first) || ((curr == dp[i][j].first) && (c < j)))
                    dp[i][j] = std::make_pair(curr, 3);
            }
        }
        mn[i] = std::make_pair(dp[i][0].first, 0);
        for (int j = 1; j < 26; ++j)
            mn[i] = std::min(mn[i], std::make_pair(dp[i][j].first, j));
    }
    std::string result;
    for (int i = 0, j = mn[0].second, l = 1; i != n; i += l)
    {
        if (l == 3) j = mn[i].second;
        l = dp[i][j].second;
        result.append(l, static_cast<char>('a' + j));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string caption, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCostGoodCaption(caption);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("cdcd", "cccc", trials);
    test("aca", "aaa", trials);
    test("bc", "", trials);
    return 0;
}


