#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::string reformatDate(std::string date)
{
    const std::unordered_map<std::string, std::string> months = {
        {"Jan", "01"}, {"Feb", "02"}, {"Mar", "03"}, {"Apr", "04"}, {"May", "05"},
        {"Jun", "06"}, {"Jul", "07"}, {"Aug", "08"}, {"Sep", "09"}, {"Oct", "10"},
        {"Nov", "11"}, {"Dec", "12"} };
    return date.substr(date.size() - 4, 4) + "-"
         + months.at(date.substr(date.size() - 8, 3)) + "-"
         + ((date.size() == 12)?("0" + date.substr(0, 1)):date.substr(0, 2));
}

// ############################################################################
// ############################################################################

void test(std::string date, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = reformatDate(date);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("20th Oct 2052", "2052-10-20", trials);
    test("6th Jun 1933", "1933-06-06", trials);
    test("26th May 1960", "1960-05-26", trials);
    return 0;
}


