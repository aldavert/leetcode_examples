#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumSum(int n, int k)
{
    const int mid = k / 2;
    if (n <= mid) return (n + 1) * n / 2;
    return (mid + 1) * mid / 2 + (2 * k + n - mid - 1) * (n - mid) / 2;
}

// ############################################################################
// ############################################################################

void test(int n, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumSum(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 4, 18, trials);
    test(2, 6, 3, trials);
    return 0;
}


