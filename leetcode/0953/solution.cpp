#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isAlienSorted(std::vector<std::string> words, std::string order)
{
    int position[26] = {};
    for (int idx = 0; char c : order)
        position[static_cast<int>(c - 'a')] = idx++;
    auto greater = [&](std::string left, std::string right)
    {
        const size_t m = std::min(left.size(), right.size());
        for (size_t i = 0; i < m; ++i)
        {
            const int idx_left  = static_cast<int>(left[i]  - 'a');
            const int idx_right = static_cast<int>(right[i] - 'a');
            if (position[idx_left] > position[idx_right])
                return true;
            else if (position[idx_left] < position[idx_right])
                return false;
        }
        return left.size() > right.size();
    };
    const size_t n = words.size();
    for (size_t i = 1; i < n; ++i)
        if (greater(words[i - 1], words[i]))
            return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::string order,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isAlienSorted(words, order);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"hello", "leetcode"}, "hlabcdefgijkmnopqrstuvwxyz", true, trials);
    test({"word", "world", "row"}, "worldabcefghijkmnpqstuvxyz", false, trials);
    test({"apple", "app"}, "abcdefghijklmnopqrstuvwxyz", false, trials);
    return 0;
}


