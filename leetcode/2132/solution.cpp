#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool possibleToStamp(std::vector<std::vector<int> > grid,
                     int stampHeight,
                     int stampWidth)
{
    const int m = static_cast<int>(grid.size());
    const int n = static_cast<int>(grid[0].size());
    std::vector<std::vector<int> > integral_occupied(m + 1, std::vector<int>(n + 1));
    std::vector<std::vector<int> > integral_fit(m + 1, std::vector<int>(n + 1));
    
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            integral_occupied[i + 1][j + 1] = integral_occupied[i + 1][j]
                                            + integral_occupied[i][j + 1]
                                            - integral_occupied[i][j]
                                            + grid[i][j];
            bool fit = false;
            if ((i + 1 >= stampHeight) && (j + 1 >= stampWidth))
            {
                const int x = i - stampHeight + 1;
                const int y = j - stampWidth + 1;
                int area_sum = integral_occupied[i + 1][j + 1]
                             - integral_occupied[x][j + 1]
                             - integral_occupied[i + 1][y]
                             + integral_occupied[x][y];
                if (area_sum == 0) fit = true;
            }
            integral_fit[i + 1][j + 1] = integral_fit[i + 1][j]
                                       + integral_fit[i][j + 1]
                                       - integral_fit[i][j]
                                       + fit;
        }
    }
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            if (!grid[i][j])
            {
                int x = std::min(i + stampHeight, m);
                int y = std::min(j + stampWidth, n);
                int sum_fit = integral_fit[x][y]
                            - integral_fit[i][y]
                            - integral_fit[x][j]
                            + integral_fit[i][j];
                if (sum_fit == 0) return false;
            }
        }
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          int stampHeight,
          int stampWidth,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = possibleToStamp(grid, stampHeight, stampWidth);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0, 0, 0},
          {1, 0, 0, 0},
          {1, 0, 0, 0},
          {1, 0, 0, 0},
          {1, 0, 0, 0}}, 4, 3, true, trials);
    test({{1, 0, 0, 0},
          {0, 1, 0, 0},
          {0, 0, 1, 0},
          {0, 0, 0, 1}}, 2, 2, false, trials);
    return 0;
}


