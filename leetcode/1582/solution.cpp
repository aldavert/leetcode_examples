#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numSpecial(std::vector<std::vector<int> > mat)
{
    const size_t n = mat[0].size();
    int column_ones[101] = {};
    bool column_valid[101] = {};
    int result = 0;
    for (const auto &row : mat)
    {
        bool valid = false;
        int number_of_ones = 0;
        size_t position = 0;
        for (size_t i = 0; i < n; ++i)
        {
            if (row[i])
            {
                if (column_valid[i]) --result, column_valid[i] = false;
                valid = !number_of_ones && !column_ones[i];
                ++column_ones[i];
                ++number_of_ones;
                position = i;
            }
        }
        result += valid;
        if (valid) column_valid[position] = true;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > mat, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numSpecial(mat);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0, 0}, {0, 0, 1}, {1, 0, 0}}, 1, trials);
    test({{1, 0, 0}, {0, 1, 0}, {0, 0, 1}}, 3, trials);
    test({{1, 1, 1}, {1, 1, 1}, {1, 1, 1}}, 0, trials);
    test({{1, 0, 0}, {0, 0, 1}, {1, 0, 1}}, 0, trials);
    test({{0, 0, 0, 0},
          {1, 0, 1, 0},
          {0, 1, 0, 0},
          {0, 1, 0, 0},
          {0, 0, 0, 0},
          {0, 0, 0, 0},
          {0, 0, 0, 0},
          {0, 1, 0, 0}}, 0, trials);
    return 0;
}


