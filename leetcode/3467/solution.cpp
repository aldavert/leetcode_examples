#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> transformArray(std::vector<int> nums)
{
    std::vector<int> result(nums.size(), 0);
    size_t n_even = 0;
    for (size_t i = 0; i < nums.size(); ++i)
        n_even += (nums[i] & 1) == 0;
    for (size_t i = n_even; i < nums.size(); ++i)
        result[i] = 1;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = transformArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 3, 2, 1}, {0, 0, 1, 1}, trials);
    test({1, 5, 1, 4, 2}, {0, 0, 1, 1, 1}, trials);
    return 0;
}


