#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int mostProfitablePath(std::vector<std::vector<int> > edges,
                       int bob,
                       std::vector<int> amount)
{
    const int n = static_cast<int>(amount.size());
    std::vector<std::vector<int> > tree(n);
    std::vector<int> parent(n), dist(n, -1);
    auto calcMoney = [&](auto &&self, int u, int prev) -> int
    {
        if ((tree[u].size() == 1) && (tree[u][0] == prev))
            return amount[u];
        int max_path = std::numeric_limits<int>::lowest();
        for (const int v : tree[u])
            if (v != prev)
                max_path = std::max(max_path, self(self, v, u));
        return amount[u] + max_path;
    };
    auto dfs = [&](auto &&self, int u, int prev, int d) -> void
    {
        parent[u] = prev;
        dist[u] = d;
        for (int v : tree[u])
            if (dist[v] == -1)
                self(self, v, u, d + 1);
    };
    
    for (const auto &edge : edges)
    {
        tree[edge[0]].push_back(edge[1]);
        tree[edge[1]].push_back(edge[0]);
    }
    dfs(dfs, 0, -1, 0);
    for (int u = bob, bob_dist = 0; u != 0; u = parent[u], ++bob_dist)
    {
        if (bob_dist < dist[u])
            amount[u] = 0;
        else if (bob_dist == dist[u])
            amount[u] /= 2;
    }
    return calcMoney(calcMoney, 0, -1);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges,
          int bob,
          std::vector<int> amount,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = mostProfitablePath(edges, bob, amount);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {1, 2}, {1, 3}, {3, 4}}, 3, {-2, 4, 2, -4, 6}, 6, trials);
    test({{0, 1}}, 1, {-7280, 2350}, -7280, trials);
    return 0;
}


