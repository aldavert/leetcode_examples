#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> longestRepeating(std::string s,
                                  std::string queryCharacters,
                                  std::vector<int> queryIndices)
{
    struct Node
    {
        int max_length = 1;
        int length_left = 1;
        int length_right = 1;
        int length = 1;
        char char_left = '\0';
        char char_right = '\0';
    };
    std::vector<Node> tree(s.size() * 4);
    std::vector<int> result;
    auto refresh = [&](int i) ->void
    {
        const Node &node_left = tree[i * 2], &node_right = tree[i * 2 + 1];
        Node &node = tree[i];
        
        node.max_length = std::max(node_left.max_length, node_right.max_length);
        node.length_left = node_left.length_left;
        node.length_right = node_right.length_right;
        node.char_left = node_left.char_left;
        node.char_right = node_right.char_right;
        if ((node_left.length_left == node_left.length)
        &&  (node_left.char_left == node_right.char_left))
            node.length_left += node_right.length_left;
        if ((node_right.length_right == node_right.length)
        &&  (node_right.char_right == node_left.char_right))
            node.length_right += node_left.length_right;
        
        if (node_left.char_right == node_right.char_left)
            node.max_length = std::max(node.max_length,
                    node_left.length_right + node_right.length_left);
    };
    auto build = [&](auto &&self, int index, int left, int right) -> void
    {
        tree[index].length = right - left + 1;
        if (left == right)
            tree[index].char_left = tree[index].char_right = s[left];
        else
        {
            int m = (left + right) / 2;
            self(self, index * 2    ,  left,     m);
            self(self, index * 2 + 1, m + 1, right);
            refresh(index);
        }
    };
    auto update =
        [&](auto &&self, int index, int left, int right, int pos, char character) -> void
    {
        if (left == right) tree[index].char_left = tree[index].char_right = character;
        else
        {
            int m = (left + right) / 2;
            if (pos <= m)
                self(self, index * 2, left, m, pos, character);
            else
                self(self, index * 2 + 1, m + 1, right, pos, character);
            refresh(index);
        }
    };
    
    const int n_s = static_cast<int>(s.size()),
              n_qc = static_cast<int>(queryCharacters.size());
    build(build, 1, 0, n_s - 1);
    for (int i = 0; i < n_qc; ++i)
    {
        update(update, 1, 0, n_s - 1, queryIndices[i], queryCharacters[i]);
        result.push_back(tree[1].max_length);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::string queryCharacters,
          std::vector<int> queryIndices,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestRepeating(s, queryCharacters, queryIndices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("babacc", "bcb", {1, 3, 3}, {3, 3, 4}, trials);
    test("abyzz", "aa", {2, 1}, {2, 3}, trials);
    return 0;
}


