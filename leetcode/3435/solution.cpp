#include "../common/common.hpp"
#include <unordered_set>
#include <set>
#include <unordered_map>
#include <numeric>
#include <bitset>

// ############################################################################
// ############################################################################

#if 1
std::vector<std::vector<int> > supersequences(std::vector<std::string> words)
{
    auto notZero = [](int x) -> bool { return x != 0; };
    int chr_to_idx[26] = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, idx_to_chr[26] = {};
    std::array<int, 26> base_in_degree = {};
    int n = 0;
    std::vector<int> adjacent[26];
    for (const auto &word : words)
    {
        const int idx0 = word[0] - 'a', idx1 = word[1] - 'a';
        if (chr_to_idx[idx0] == -1)
        {
            chr_to_idx[idx0] = n;
            idx_to_chr[n++] = idx0;
        }
        if (chr_to_idx[idx1] == -1)
        {
            chr_to_idx[idx1] = n;
            idx_to_chr[n++] = idx1;
        }
        adjacent[chr_to_idx[idx0]].push_back(chr_to_idx[idx1]);
        ++base_in_degree[chr_to_idx[idx1]];
    }
    std::array<int, 26> count = {};
    int min_length = std::numeric_limits<int>::max();
    std::vector<std::array<int, 26> > frequencies;
    const auto& topologicalSort = [&](void) -> void
    {
        const auto& total = std::accumulate(count.begin(), count.end(), 0);
        if (total > min_length) return;
        std::array<int, 26> in_degree = base_in_degree, new_count = count;
        std::bitset<26> lookup;
        std::array<int, 26> present_chrs;
        int present_size = 0;
        
        for (int u = 0; u < n; ++u)
        {
            if (!in_degree[u] || (new_count[u] == 2))
            {
                --new_count[u];
                lookup[u] = true;
                present_chrs[present_size++] = u;
            }
        }
        while (present_size > 0)
        {
            std::array<int, 26> current_chrs;
            int current_size = 0;
            for (int i = 0; i < present_size; ++i)
            {
                for (int v : adjacent[present_chrs[i]])
                {
                    if (--in_degree[v]) continue;
                    --new_count[v];
                    if (lookup[v]) continue;
                    lookup[v] = true;
                    current_chrs[current_size++] = v;
                }
            }
            present_size = current_size;
            present_chrs = current_chrs;
        }
        
        if (std::any_of(new_count.begin(), new_count.end(), notZero))
            return;
        if (min_length > total)
        {
            min_length = total;
            frequencies.clear();
        }
        frequencies.push_back(count);
    };
    
    for (int mask = 0; mask < (1 << n); ++mask)
    {
        for (int i = 0; i < n; ++i)
            count[i] = 1 + ((mask & (1 << i)) != 0);
        topologicalSort();
    }
    std::vector<std::vector<int> > result(frequencies.size(), std::vector<int>(26));
    for (size_t j = 0; j < frequencies.size(); ++j)
        for (int i = 0; i < n; ++i)
            result[j][idx_to_chr[i]] = frequencies[j][i];
    return result;
}
#else
std::vector<std::vector<int> > supersequences(std::vector<std::string> words)
{
    constexpr int INF = 100;
    int before[26] = {}, avoid[26] = {}, lookup[26] = { -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1 }, unique[26] = {};
    int n = 0;
    for (auto word : words)
    {
        const int idx0 = word[0] - 'a', idx1 = word[1] - 'a';
        if (lookup[idx0] == -1)
        {
            lookup[idx0] = n;
            unique[n++] = idx0;
        }
        if (lookup[idx1] == -1)
        {
            lookup[idx1] = n;
            unique[n++] = idx1;
        }
        if (word[0] == word[1]) avoid[lookup[idx0]] = 1;
        else before[lookup[idx1]] |= (1 << lookup[idx0]);
    }
    std::unordered_map<int, std::pair<int, std::set<int> > > dp;
    dp[0] = {0, {0}};
    auto go2 = [&](auto &&self, int available) -> std::pair<int, std::set<int> >
    {
        if (auto search = dp.find(available); search != dp.end())
            return search->second;
        int counter = INF;
        std::set<int> bg;
        for (int i = 0; i < n; ++i)
        {
            if ((available & (1 << i)) == 0) continue;
            auto [r, g] = self(self, available ^ (1 << i));
            r += 1;
            int mask = 0;
            if (((before[i] & available) == 0) && !avoid[i])
                mask = 1 << i;
            else r += 1;
            if (r > counter) continue;
            if (r < counter)
            {
                counter = r;
                bg.clear();
            }
            for (auto x : g) bg.insert(x | mask);
        }
        return dp[available] = {counter, bg};
    };
    auto [a, g] = go2(go2, (1 << n) - 1);
    std::vector<std::vector<int> > result;
    for (auto x : g)
    {
        result.push_back(std::vector<int>(26));
        for (int i = 0; i < n; ++i)
            result.back()[unique[i]] = 1 + ((x & (1 << i)) == 0);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = supersequences(words);
    std::sort(solution.begin(), solution.end());
    std::sort(result.begin(), result.end());
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"ab", "ba"}, {{1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0}, {2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},  trials);
    test({"aa", "ac"}, {{2, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0}}, trials);
    test({"aa", "bb", "cc"}, {{2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0}}, trials);
    test({"ql", "kf", "lf", "ks", "ko", "zb", "rq", "ky", "ze", "yh", "lr", "lq",
          "hf", "kl", "ys", "xo", "yf", "lz", "fh", "kd", "bf", "ef", "hl", "xn",
          "xz", "nb", "nr", "bk", "er", "el", "oh", "xk", "fq", "kr", "bh", "so",
          "ox", "qe", "yn", "fr", "fk", "eb", "ho", "hk", "nq", "bd", "zx", "yr",
          "no", "eh", "rn", "xd", "dk", "bs", "qx", "sf", "bl", "sk", "bx", "zr",
          "oy", "yb", "zh", "rd", "fd", "nl", "ek", "kx", "dy", "ds", "xs", "qn",
          "yl", "hy", "oe", "ke", "hd", "ly", "ro", "sq", "sd", "xy", "sr", "ey",
          "xr", "od", "eo", "xf", "xq", "zl", "fo", "rk", "sh", "zn", "hz", "fb"},
          {{0, 1, 0, 1, 1, 2, 0, 1, 0, 0, 2, 1, 0, 2, 2, 0, 2, 1, 2, 0, 0, 0, 0,
          1, 2, 2}, {0, 1, 0, 2, 1, 2, 0, 1, 0, 0, 2, 1, 0, 2, 2, 0, 2, 1, 1, 0,
          0, 0, 0, 1, 2, 2}}, trials);
    test({"rx", "rh", "rj", "rv", "rq", "ri", "rz", "re", "rm", "rc", "rb", "rn",
          "ry", "ro", "rd", "xr", "xh", "xj", "xv", "xq", "xi", "xz", "xe", "xm",
          "xc", "xb", "xn", "xy", "xo", "xd", "hr", "hx", "hj", "hv", "hq", "hi",
          "hz", "he", "hm", "hc", "hb", "hn", "hy", "ho", "hd", "jr", "jx", "jh",
          "jv", "jq", "ji", "jz", "je", "jm", "jc", "jb", "jn", "jy", "jo", "jd",
          "vr", "vx", "vh", "vj", "vq", "vi", "vz", "ve", "vm", "vc", "vb", "vn",
          "vy", "vo", "vd", "qr", "qx", "qh", "qj", "qv", "qi", "qz", "qe", "qm",
          "qc", "qb", "qn", "qy", "qo", "qd", "ir", "ix", "ih", "ij", "iv", "iq",
          "iz", "ie", "im", "ic", "ib", "in", "iy", "io", "id", "zr", "zx", "zh",
          "zj", "zv", "zq", "zi", "ze", "zm", "zc", "zb", "zn", "zy", "zo", "zd",
          "er", "ex", "eh", "ej", "ev", "eq", "ei", "ez", "em", "ec", "eb", "en",
          "ey", "eo", "ed", "mr", "mx", "mh", "mj", "mv", "mq", "mi", "mz", "me",
          "mc", "mb", "mn", "my", "mo", "md", "cr", "cx", "ch", "cj", "cv", "cq",
          "ci", "cz", "ce", "cm", "cb", "cn", "cy", "co", "cd", "br", "bx", "bh",
          "bj", "bv", "bq", "bi", "bz", "be", "bm", "bc", "bn", "by", "bo", "bd",
          "nr", "nx", "nh", "nj", "nv", "nq", "ni", "nz", "ne", "nm", "nc", "nb",
          "ny", "no", "nd", "yr", "yx", "yh", "yj", "yv", "yq", "yi", "yz", "ye",
          "ym", "yc", "yb", "yn", "yo", "yd", "or", "ox", "oh", "oj", "ov", "oq",
          "oi", "oz", "oe", "om", "oc", "ob", "on", "oy", "od", "dr", "dx", "dh",
          "dj", "dv", "dq", "di", "dz", "de", "dm", "dc", "db", "dn", "dy", "do"},
          {{0, 1, 2, 2, 2, 0, 0, 2, 2, 2, 0, 0, 2, 2, 2, 0, 2, 2, 0, 0, 0, 2, 0,
            2, 2, 2}, {0, 2, 1, 2, 2, 0, 0, 2, 2, 2, 0, 0, 2, 2, 2, 0, 2, 2, 0, 0,
            0, 2, 0, 2, 2, 2}, {0, 2, 2, 1, 2, 0, 0, 2, 2, 2, 0, 0, 2, 2, 2, 0, 2,
            2, 0, 0, 0, 2, 0, 2, 2, 2}, {0, 2, 2, 2, 1, 0, 0, 2, 2, 2, 0, 0, 2, 2,
            2, 0, 2, 2, 0, 0, 0, 2, 0, 2, 2, 2}, {0, 2, 2, 2, 2, 0, 0, 1, 2, 2, 0,
            0, 2, 2, 2, 0, 2, 2, 0, 0, 0, 2, 0, 2, 2, 2}, {0, 2, 2, 2, 2, 0, 0, 2,
            1, 2, 0, 0, 2, 2, 2, 0, 2, 2, 0, 0, 0, 2, 0, 2, 2, 2}, {0, 2, 2, 2, 2,
            0, 0, 2, 2, 1, 0, 0, 2, 2, 2, 0, 2, 2, 0, 0, 0, 2, 0, 2, 2, 2}, {0, 2,
            2, 2, 2, 0, 0, 2, 2, 2, 0, 0, 1, 2, 2, 0, 2, 2, 0, 0, 0, 2, 0, 2, 2,
            2}, {0, 2, 2, 2, 2, 0, 0, 2, 2, 2, 0, 0, 2, 1, 2, 0, 2, 2, 0, 0, 0, 2,
            0, 2, 2, 2}, {0, 2, 2, 2, 2, 0, 0, 2, 2, 2, 0, 0, 2, 2, 1, 0, 2, 2, 0,
            0, 0, 2, 0, 2, 2, 2}, {0, 2, 2, 2, 2, 0, 0, 2, 2, 2, 0, 0, 2, 2, 2, 0,
            1, 2, 0, 0, 0, 2, 0, 2, 2, 2}, {0, 2, 2, 2, 2, 0, 0, 2, 2, 2, 0, 0, 2,
            2, 2, 0, 2, 1, 0, 0, 0, 2, 0, 2, 2, 2}, {0, 2, 2, 2, 2, 0, 0, 2, 2, 2,
            0, 0, 2, 2, 2, 0, 2, 2, 0, 0, 0, 1, 0, 2, 2, 2}, {0, 2, 2, 2, 2, 0, 0,
            2, 2, 2, 0, 0, 2, 2, 2, 0, 2, 2, 0, 0, 0, 2, 0, 1, 2, 2}, {0, 2, 2, 2,
            2, 0, 0, 2, 2, 2, 0, 0, 2, 2, 2, 0, 2, 2, 0, 0, 0, 2, 0, 2, 1, 2}, {0,
            2, 2, 2, 2, 0, 0, 2, 2, 2, 0, 0, 2, 2, 2, 0, 2, 2, 0, 0, 0, 2, 0, 2,
            2, 1}}, trials);
    return 0;
}


