#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int maximumsSplicedArray(std::vector<int> nums1, std::vector<int> nums2)
{
    auto kadane = [](const std::vector<int> &vec1, const std::vector<int> &vec2)
    {
        int gain = 0, max_gain = 0;
        for (int i = 0, n = static_cast<int>(vec1.size()); i < n; ++i)
        {
            gain = std::max(0, gain + vec2[i] - vec1[i]);
            max_gain = std::max(max_gain, gain);
        }
        return max_gain + std::accumulate(vec1.begin(), vec1.end(), 0);
    };
    return std::max(kadane(nums1, nums2), kadane(nums2, nums1));
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumsSplicedArray(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({60, 60, 60}, {10, 90, 10}, 210, trials);
    test({20, 40, 20, 70, 30}, {50, 20, 50, 40, 20}, 220, trials);
    test({7, 11, 13}, {1, 1, 1}, 31, trials);
    return 0;
}


