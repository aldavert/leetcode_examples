#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> findRelativeRanks(std::vector<int> score)
{
    const size_t n = score.size();
    std::vector<std::tuple<int, int> > rank(n);
    std::vector<std::string> result(n);
    for (size_t i = 0; i < n; ++i)
        rank[i] = {score[i], i};
    std::sort(rank.begin(), rank.end(), std::greater<std::tuple<int, int> >());
    if (n > 0) result[std::get<1>(rank[0])] = "Gold Medal";
    if (n > 1) result[std::get<1>(rank[1])] = "Silver Medal";
    if (n > 2) result[std::get<1>(rank[2])] = "Bronze Medal";
    for (size_t i = 3; i < n; ++i)
        result[std::get<1>(rank[i])] = std::to_string(i + 1);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> score,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findRelativeRanks(score);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 3, 2, 1},
         {"Gold Medal", "Silver Medal", "Bronze Medal", "4", "5"}, trials);
    test({10, 3, 8, 9, 4},
         {"Gold Medal", "5", "Bronze Medal", "Silver Medal", "4"}, trials);
    return 0;
}


