#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumDifference(std::vector<int> nums)
{
    int minimum = nums[0], difference = 0;
    for (int value : nums)
        difference = std::max(difference, value - minimum),
        minimum = std::min(minimum, value);
    return (difference > 0)?difference:-1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumDifference(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({7, 1, 5, 4}, 4, trials);
    test({9, 4, 3, 2}, -1, trials);
    test({1, 5, 2, 10}, 9, trials);
    return 0;
}


