#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool squareIsWhite(std::string coordinates)
{
    return ((coordinates[0] - 'a') & 1) xor ((coordinates[1] - '1') & 1);
}

// ############################################################################
// ############################################################################

void test(std::string coordinates, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = squareIsWhite(coordinates);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("a1", false, trials);
    test("h3",  true, trials);
    test("c7", false, trials);
    return 0;
}


