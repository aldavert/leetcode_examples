#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countCompleteSubarrays(std::vector<int>& nums)
{
    constexpr int MAX = 2000;
    const int total_distinct =
        static_cast<int>(std::unordered_set<int>(nums.begin(), nums.end()).size());
    int result = 0;
    std::vector<int> count(MAX + 1);
    for (int l = 0, distinct = 0; int num : nums)
    {
        if (++count[num] == 1) ++distinct;
        while (distinct == total_distinct)
            if (--count[nums[l++]] == 0)
                --distinct;
        result += l;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countCompleteSubarrays(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 1, 2, 2}, 4, trials);
    test({5, 5, 5, 5}, 10, trials);
    return 0;
}


