#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int maximumLength(std::vector<int> nums, int k)
{
    auto max_element = [](int acc, const std::vector<int> &vec) -> int
    {
        return std::max(acc, *std::max_element(vec.begin(), vec.end()));
    };
    std::vector<std::vector<int> > dp(k, std::vector<int>(k));
    for (const int x : nums)
        for (int y = 0; y < k; ++y)
            dp[x % k][y] = dp[y][x % k] + 1;
    return std::accumulate(dp.begin(), dp.end(), 0, max_element);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumLength(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 2, 5, trials);
    test({1, 4, 2, 3, 1, 4}, 3, 4, trials);
    return 0;
}


