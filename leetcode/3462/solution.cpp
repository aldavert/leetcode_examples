#include "../common/common.hpp"
#include <queue>
#include <fstream>
#include <sstream>

// ############################################################################
// ############################################################################

long long maxSum(std::vector<std::vector<int> > grid, std::vector<int> limits, int k)
{
    if (k == 0) return 0;
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>(grid[0].size());
    std::priority_queue<int, std::vector<int>, std::greater<> > total;
    for (int row = 0; row < n_rows; ++row)
    {
        if (limits[row] == 0) continue;
        std::priority_queue<int, std::vector<int>, std::greater<> > q;
        for (int col = 0; col < n_cols; ++col)
        {
            if (static_cast<int>(q.size()) < limits[row])
                q.push(grid[row][col]);
            else if (grid[row][col] > q.top())
            {
                q.pop();
                q.push(grid[row][col]);
            }
        }
        while (!q.empty())
        {
            if (static_cast<int>(total.size()) < k)
                total.push(q.top());
            else if (q.top() > total.top())
            {
                total.pop();
                total.push(q.top());
            }
            q.pop();
        }
    }
    long long result = 0;
    while (!total.empty())
    {
        result += total.top();
        total.pop();
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<int> limits,
          int k,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSum(grid, limits, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {3, 4}}, {1, 2}, 2, 7, trials);
    test({{5, 3, 7}, {8, 2, 6}}, {2, 2}, 3, 21, trials);
    test({{0, 1}}, {0}, 0, 0, trials);
    std::ifstream file("data.txt");
    if (file.is_open())
    {
        std::vector<std::vector<int> > grid;
        std::vector<int> limits;
        int k;
        long long expected_result;
        for (std::string line; std::getline(file, line); )
        {
            std::istringstream iss(line);
            grid.push_back({});
            int value = -1;
            while (iss >> value)
                grid.back().push_back(value);
            if (grid.back().size() == 1) break;
        }
        k = grid.back().back();
        grid.pop_back();
        limits = grid.back();
        grid.pop_back();
        file >> expected_result;
        file.close();
        test(grid, limits, k, expected_result, trials);
    }
    return 0;
}


