#include "../common/common.hpp"

#if 1
int trap(std::vector<int> height)
{
    const int n = static_cast<int>(height.size());
    if (n < 3) return 0;
    int left = -1, left_height = 0, right = n, right_height = 0, water = 0;
    while (left < right)
    {
        if (left_height < right_height)
        {
            ++left;
            if (height[left] >= left_height)
                left_height = height[left];
            else
                water += left_height - height[left];
        }
        else
        {
            --right;
            if (height[right] >= right_height)
                right_height = height[right];
            else
                water += right_height - height[right];
        }
    }
    return water;
}
#else
int trap(std::vector<int> height)
{
    const int n = static_cast<int>(height.size());
    if (n < 3) return 0;
    int left = -1, left_height = 0, right = n, right_height = 0, water = 0;
    while (left < right)
    {
        //for (int v : height)
        //    std::cout << v << ' ';
        //std::cout << '\n';
        if (left_height < right_height)
        {
            ++left;
            //for (int k = 0; k < left; ++k)
            //    std::cout << "  ";
            //std::cout << '*';
            if (height[left] >= left_height)
                left_height = height[left];
            else
                water += left_height - height[left];
            //std::cout << " => " << left_height << ' ' << height[left] << ' ' << water << '\n';
        }
        else
        {
            --right;
            //for (int k = 0; k < right; ++k)
            //    std::cout << "  ";
            //std::cout << '*';
            if (height[right] >= right_height)
                right_height = height[right];
            else
                water += right_height - height[right];
            //std::cout << " => " << right_height << ' ' << height[right] << ' ' << water << '\n';
        }
        //std::cout << '\n';
    }
    return water;
}
#endif

void test(std::vector<int> height, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = trap(height);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}, 6, trials);
    test({4, 2, 0, 3, 2, 5}, 9, trials);
    return 0;
}


