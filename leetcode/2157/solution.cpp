#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> groupStrings(std::vector<std::string> words)
{
    const int n = static_cast<int>(words.size());
    std::unordered_map<int, int> mask_to_index;
    std::unordered_map<int, int> deleted_mask_to_index;
    std::vector<int> id(n), size(n);
    int count = n, max_size = 1;
    for (int i = 0; i < n; ++i) id[i] = i, size[i] = 1;
    auto find = [&](auto &&self, int i) -> int
    {
        return (i == id[i])?i:id[i] = self(self, id[i]);
    };
    auto merge = [&](int u, int v) -> void
    {
        int i = find(find, u), j = find(find, v);
        if (i == j) return;
        if (size[i] < size[j])
        {
            max_size = std::max(max_size, size[j] += size[i]);
            id[i] = j;
        }
        else
        {
            max_size = std::max(max_size, size[i] += size[j]);
            id[j] = i;
        }
        --count;
    };
    auto calculateMask = [](std::string word)
    {
        int mask = 0;
        for (char c : word) mask |= 1 << (c - 'a');
        return mask;
    };
    
    for (int i = 0; i < n; ++i)
    {
        int mask = calculateMask(words[i]);
        for (int j = 0; j < 26; ++j)
        {
            if ((mask >> j) & 1)
            {
                int m = mask ^ 1 << j;
                if (auto it = mask_to_index.find(m); it != mask_to_index.end())
                    merge(i, it->second);
                if (auto it = deleted_mask_to_index.find(m);
                    it != deleted_mask_to_index.end())
                    merge(i, it->second);
                else deleted_mask_to_index[m] = i;
            }
            else
            {
                int m = mask | 1 << j;
                if (auto it = mask_to_index.find(m); it != mask_to_index.end())
                    merge(i, it->second);
            }
        }
        mask_to_index[mask] = i;
    }
    return {count, max_size};
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = groupStrings(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"a", "b", "ab", "cde"}, {2, 3}, trials);
    test({"a", "ab", "abc"}, {1, 3}, trials);
    return 0;
}


