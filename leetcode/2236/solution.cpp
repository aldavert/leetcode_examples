#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

bool checkTree(TreeNode * root)
{
    return root->val == root->left->val + root->right->val;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = checkTree(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 4, 6}, true, trials);
    test({5, 3, 1}, false, trials);
    return 0;
}


