#include "../common/common.hpp"
#include <unordered_map>
#include <cmath>

// ############################################################################
// ############################################################################

long long numberOfPairs(std::vector<int> nums1,
                        std::vector<int> nums2,
                        int k)
{
    std::unordered_map<int, int> count;
    long result = 0;
    for (int num : nums2) ++count[num * k];
    for (int num : nums1)
    {
        for (int div = 1, m = static_cast<int>(std::sqrt(num)); div <= m; ++div)
        {
            if (num % div == 0)
            {
                result += count.contains(div)?count[div]:0;
                if (num / div != div)
                    result += count.contains(num / div)?count[num / div]:0;
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int k,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfPairs(nums1, nums2, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 4}, {1, 3, 4}, 1, 5, trials);
    test({1, 2, 4, 12}, {2, 4}, 3, 2, trials);
    return 0;
}


