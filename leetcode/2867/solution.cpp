#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long countPaths(int n, std::vector<std::vector<int> > edges)
{
    auto sieveEratosthenes = [](int value) -> std::vector<bool>
    {
        std::vector<bool> sieve(value, true);
        sieve[0] = sieve[1] = false;
        for (int i = 2; i * i < value; ++i)
            if (sieve[i])
                for (int j = i * i; j < value; j += i)
                    sieve[j] = false;
        return sieve;
    };
    long result = 0;
    const std::vector<bool> is_prime = sieveEratosthenes(n + 1);
    std::vector<std::vector<int> > graph(n + 1);
    auto dfs = [&](auto &&self, int u, int prev) -> std::pair<long, long>
    {
        long count_zero = !is_prime[u], count_one = is_prime[u];
        for (const int v : graph[u])
        {
            if (v == prev) continue;
            const auto& [count_zero_child, count_one_child] = self(self, v, u);
            result += count_zero * count_one_child + count_one * count_zero_child;
            if (is_prime[u])
                count_one += count_zero_child;
            else
            {
                count_zero += count_zero_child;
                count_one += count_one_child;
            }
        }
        return {count_zero, count_one};
    };
    for (const auto &edge : edges)
    {
        graph[edge[0]].push_back(edge[1]);
        graph[edge[1]].push_back(edge[0]);
    }
    dfs(dfs, 1, -1);
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPaths(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{1, 2}, {1, 3}, {2, 4}, {2, 5}}, 4, trials);
    test(6, {{1, 2}, {1, 3}, {2, 4}, {3, 5}, {3, 6}}, 6, trials);
    return 0;
}


