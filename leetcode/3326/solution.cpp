#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> nums)
{
    auto gd = [](int n) -> int
    {
        for (int i = 2, m = static_cast<int>(std::sqrt(n)); i <= m; ++i)
            if (n % i == 0) return i;
        return 1;
    };
    int result = 0;
    for (int i = static_cast<int>(nums.size()) - 1; i > 0; --i)
    {
        while (nums[i] < nums[i - 1])
        {
            int g = gd(nums[i - 1]);
            if (g == 1) return -1;
            nums[i - 1] = g;
            ++result;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({25, 7}, 1, trials);
    test({7, 7, 6}, -1, trials);
    test({1, 1, 1, 1}, 0, trials);
    return 0;
}


