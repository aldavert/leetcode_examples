#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isRectangleOverlap(std::vector<int> rec1, std::vector<int> rec2)
{
    return ((std::min(rec1[2], rec2[2]) - std::max(rec1[0], rec2[0])) > 0)
        && ((std::min(rec1[3], rec2[3]) - std::max(rec1[1], rec2[1])) > 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> rec1,
          std::vector<int> rec2,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isRectangleOverlap(rec1, rec2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 0, 2, 2}, {1, 1, 3, 3},  true, trials);
    test({0, 0, 1, 1}, {1, 0, 2, 1}, false, trials);
    test({0, 0, 1, 1}, {2, 2, 3, 3}, false, trials);
    return 0;
}


