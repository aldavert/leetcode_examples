#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <stack>

// ############################################################################
// ############################################################################

TreeNode * increasingBST(TreeNode * root)
{
    if (!root) return root;
    std::stack<TreeNode *> backtrace;
    backtrace.push(root);
    while (backtrace.top()->left)
        backtrace.push(std::exchange(backtrace.top()->left, nullptr));
    TreeNode * new_root = backtrace.top();
    TreeNode * last = new_root;
    backtrace.pop();
    if (new_root->right)
        backtrace.push(new_root->right);
    new_root->right = nullptr;
    while (backtrace.size())
    {
        if (backtrace.top()->left)
            backtrace.push(std::exchange(backtrace.top()->left, nullptr));
        else
        {
            TreeNode * current = backtrace.top();
            backtrace.pop();
            if (current->right)
                backtrace.push(current->right);
            current->right = nullptr;
            last->right = current;
            last = current;
        }
    }
    return new_root;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        root = increasingBST(root);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 3, 6, 2, 4, null, 8, 1, null, null, null, 7, 9},
         {1, null, 2, null, 3, null, 4, null, 5, null, 6, null, 7, null, 8, null, 9},
         trials);
    test({5, 1, 7}, {1, null, 5, null, 7}, trials);
    test({212}, {212}, trials);
    return 0;
}


