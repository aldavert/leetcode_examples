#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<bool> camelMatch(std::vector<std::string> queries, std::string pattern)
{
    const int n = static_cast<int>(pattern.size());
    auto isMatch = [&pattern,&n](const std::string &query) -> bool
    {
        int j = 0;
        for (char c : query)
        {
            if ((j < n) && (c == pattern[j]))
                ++j;
            else if (std::isupper(c))
                return false;
        }
        return j == n;
    };
    std::vector<bool> result;
    for (const auto &query : queries)
        result.push_back(isMatch(query));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> queries,
          std::string pattern,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = camelMatch(queries, pattern);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"FooBar", "FooBarTest", "FootBall", "FrameBuffer", "ForceFeedBack"}, "FB",
         {true, false, true, true, false}, trials);
    test({"FooBar", "FooBarTest", "FootBall", "FrameBuffer", "ForceFeedBack"}, "FoBa",
         {true, false, true, false, false}, trials);
    test({"FooBar", "FooBarTest", "FootBall", "FrameBuffer", "ForceFeedBack"}, "FoBaT",
         {false, true, false, false, false}, trials);
    return 0;
}


