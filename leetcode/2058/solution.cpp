#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

std::vector<int> nodesBetweenCriticalPoints(ListNode * head)
{
    if (!head || !head->next) return {-1, -1};
    std::vector<int> result = {std::numeric_limits<int>::max(), -1};
    int previous = head->val, first_critical = -1, last_critical = -1, index = 0;
    for (ListNode * node = head->next; node->next; node = node->next, ++index)
    {
        if (((previous < node->val) && (node->next->val < node->val))
        ||  ((previous > node->val) && (node->next->val > node->val)))
        {
            if (first_critical == -1)
                first_critical = index;
            else
            {
                result[0] = std::min(result[0], index - last_critical);
                result[1] = std::max(result[1], index - first_critical);
            }
            last_critical = index;
        }
        previous = node->val;
    }
    if (result[1] == -1) result[0] = -1;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> list, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(list);
        result = nodesBetweenCriticalPoints(head);
        delete head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1}, {-1, -1}, trials);
    test({5, 3, 1, 2, 5, 1, 2}, {1, 3}, trials);
    test({1, 3, 2, 2, 3, 2, 2, 2, 7}, {3, 3}, trials);
    return 0;
}


