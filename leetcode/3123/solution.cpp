#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<bool> findAnswer(int n, std::vector<std::vector<int> > edges)
{
    constexpr int MAX = 1'000'000'000;
    std::vector<std::vector<std::pair<int, int> > > graph(n);
    std::vector<bool> result;
    
    auto dijkstra = [&](int src) -> std::vector<int>
    {
        std::vector<int> dist(graph.size(), MAX);
        std::priority_queue<std::pair<int, int>,
                            std::vector<std::pair<int, int> >,
                            std::greater<> > heap;
        dist[src] = 0;
        heap.emplace(dist[src], src);
        while (!heap.empty())
        {
            const auto [d, u] = heap.top();
            heap.pop();
            if (d > dist[u]) continue;
            for (const auto& [v, w] : graph[u])
            {
                if (d + w < dist[v])
                {
                    dist[v] = d + w;
                    heap.emplace(dist[v], v);
                }
            }
        }
        return dist;
    };
    for (const auto &edge : edges)
    {
        graph[edge[0]].emplace_back(edge[1], edge[2]);
        graph[edge[1]].emplace_back(edge[0], edge[2]);
    }
    const std::vector<int> from0 = dijkstra(0),
                           from1 = dijkstra(n - 1);
    for (const auto &edge : edges)
        result.push_back((from0[edge[0]] + edge[2] + from1[edge[1]] == from0[n - 1])
                      || (from0[edge[1]] + edge[2] + from1[edge[0]] == from0[n - 1]));
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findAnswer(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, {{0, 1, 4}, {0, 2, 1}, {1, 3, 2}, {1, 4, 3}, {1, 5, 1}, {2, 3, 1},
         {3, 5, 3}, {4, 5, 2}},
         {true, true, true, false, true, true, true, false}, trials);
    test(4, {{2, 0, 1}, {0, 1, 1}, {0, 3, 4}, {3, 2, 2}},
         {true, false, false, true}, trials);
    return 0;
}


