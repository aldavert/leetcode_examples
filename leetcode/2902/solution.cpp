#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int countSubMultisets(std::vector<int>& nums, int l, int r)
{
    constexpr int MOD = 1'000'000'007;
    std::vector<long> dp(r + 1);
    dp[0] = 1;
    std::unordered_map<int, int> count;
    for (const int num : nums) ++count[num];
    int zeros = count[0];
    count.erase(0);
    
    for (const auto& [num, freq] : count)
    {
        std::vector<long> stride = dp;
        for (int i = num; i <= r; ++i)
            stride[i] += stride[i - num];
        for (int i = r; i > 0; --i)
        {
            if (i >= num * (freq + 1))
                dp[i] = (stride[i] - stride[i - num * (freq + 1)]) % MOD;
            else dp[i] = stride[i] % MOD;
        }
    }
    long result = 0;
    for (int i = l; i <= r; ++i)
    result = (result + dp[i]) % MOD;
    return static_cast<int>(((zeros + 1) * result) % MOD);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int l, int r, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSubMultisets(nums, l, r);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 3}, 6, 6, 1, trials);
    test({2, 1, 4, 2, 7}, 1, 5, 7, trials);
    test({1, 2, 1, 3, 5, 2}, 3, 5, 9, trials);
    return 0;
}


