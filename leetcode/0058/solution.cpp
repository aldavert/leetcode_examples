#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int lengthOfLastWord(std::string s)
{
    const int n = static_cast<int>(s.size());
    int i;
    for (i = n - 1; (i >= 0) && (s[i] == ' '); --i);
    int end = i;
    for (; (i >= 0) && (s[i] != ' '); --i);
    return end - i;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = lengthOfLastWord(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("Hello World", 5, trials);
    test("   fly me   to   the moon  ", 4, trials);
    test("luffy is still joyboy", 6, trials);
    return 0;
}


