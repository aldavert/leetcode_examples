# Cousins in Binary Tree

Given the `root` of a binary tree with unique values and the values of two different nodes of the tree `x` and `y`, return `true` *if the nodes corresponding to the values* `x` *and* `y` *in the tree are* ***cousins,*** *or* `false` *otherwise*.

Two nodes of a binary tree are ***cousins*** if they have the same depth with different parents.

Note that in a binary tree, the root node is at the depth `0`, and children of each depth `k` node are at the depth `k + 1`.

#### Example 1:
> ```mermaid
> graph TD;
> A((1))---B((2))
> A---C((3))
> B---D((4))
> B---EMPTY(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY empty;
> linkStyle 3 stroke-width:0px;
> ```
> *Input:* `root = [1, 2, 3, 4], x = 4, y = 3`  
> *Output:* `false`

#### Example 2:
> ```mermaid
> graph TD;
> A((1))---B((2))
> A---C((3))
> B---EMPTY1(( ))
> B---D((4))
> C---EMPTY2(( ))
> C---E((5))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY1,EMPTY2 empty;
> linkStyle 2,4 stroke-width:0px;
> ```
> *Input:* `root = [1, 2, 3, null, 4, null, 5], x = 5, y = 4`  
> *Output:* `true`

#### Example 3:
> ```mermaid
> graph TD;
> A((1))---B((2))
> A---C((3))
> B---EMPTY(( ))
> B---D((4))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY empty;
> linkStyle 2 stroke-width:0px;
> ```
> *Input:* `root = [1, 2, 3, null, 4], x = 2, y = 3`  
> *Output:* `false`
 
#### Constraints:
- The number of nodes in the tree is in the range `[2, 100]`.
- `1 <= Node.val <= 100`
- Each node has a **unique** value.
- `x != y`
- `x` and `y` are exist in the tree.


