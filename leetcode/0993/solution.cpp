#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

bool isCousins(TreeNode * root, int x, int y)
{
    int depth_x = -1, depth_y = -2, parent_x = -1, parent_y = -1;
    auto search = [&](auto &&self, TreeNode * node, int depth, int parent) -> void
    {
        if (node == nullptr) return;
        if (node->val == x)
        {
            depth_x = depth;
            parent_x = parent;
        }
        else if (node->val == y)
        {
            depth_y = depth;
            parent_y = parent;
        }
        if ((depth_x >= 0) && (depth_y >= 0)) return;
        self(self, node->left, depth + 1, node->val);
        self(self, node->right, depth + 1, node->val);
    };
    search(search, root, 0, -1);
    return (depth_x == depth_y) && (parent_x != parent_y);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int x, int y, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = isCousins(root, x, y);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 4, 3, false, trials);
    test({1, 2, 3, null, 4, null, 5}, 5, 4, true, trials);
    test({1, 2, 3, null, 4}, 2, 3, false, trials);
    return 0;
}


