#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minOperations(int n)
{
    auto arr = [](int i) -> int { return (i - 1) * 2 + 1; };
    const int half_size = n / 2,
              median = (arr(n) + arr(1)) / 2,
              diff_first = median - arr(1),
              diff_last = median - arr(half_size);
    return (diff_first + diff_last) * half_size / 2;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 2, trials);
    test(6, 9, trials);
    return 0;
}


