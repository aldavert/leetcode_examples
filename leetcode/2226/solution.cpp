#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int maximumCandies(std::vector<int> candies, long long k)
{
    auto numChildren = [&](int m) -> long
    {
        return std::accumulate(candies.begin(), candies.end(), 0L,
                               [&](long st, int c) { return st + c / m; });
    };
    int l = 1,
        r = static_cast<int>(std::accumulate(candies.begin(), candies.end(), 0L) / k);
    while (l < r)
    {
        if (int m = (l + r) / 2; numChildren(m) < k) r = m;
        else l = m + 1;
    }
    return l - (numChildren(l) < k);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> candies, long long k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumCandies(candies, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 8, 6}, 3, 5, trials);
    test({2, 5}, 11, 0, trials);
    return 0;
}


