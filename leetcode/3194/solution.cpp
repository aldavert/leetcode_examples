#include "../common/common.hpp"

// ############################################################################
// ############################################################################

double minimumAverage(std::vector<int> nums)
{
    double result = std::numeric_limits<int>::max();
    std::sort(nums.begin(), nums.end());
    for (int i = 0, j = static_cast<int>(nums.size()) - 1; i < j; ++i, --j)
        result = std::min(result, (nums[i] + nums[j]) / 2.0);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, double solution, unsigned int trials = 1)
{
    double result = -1.0;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumAverage(nums);
    showResult(std::abs(solution - result) < 1e5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({7, 8, 3, 4, 15, 13, 4, 1}, 5.5, trials);
    test({1, 9, 8, 3, 10, 5}, 5.5, trials);
    test({1, 2, 3, 7, 8, 9}, 5.0, trials);
    return 0;
}


