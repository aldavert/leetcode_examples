# Even Odd Tree

A binary tree is named **Even-Odd** if it meets the following conditions:
- The root of the binary tree is at level index `0`, its children are at level index `1`, their children are at level index `2`, etc.
- For every **even-indexed** level, all nodes at the level have **odd** integer values in **strictly increasing** order (from left to right).
- For every **odd-indexed** level, all nodes at the level have **even** integer values in **strictly decreasing** order (from left to right).

Given the `root` of a binary tree, *return* `true` *if the binary tree is* ***Even-Odd***, *otherwise return* `false`.

#### Example 1:
> ```mermaid
> graph TD;
> A((1))---B(( 10 ))
> A---C((4))
> B---D((3))
> B---E1(( ))
> D---E((12))
> D---F((8))
> C---G((7))
> C---H((9))
> G---I((6))
> G---E2(( ))
> H---E3(( ))
> H---J((2))
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class E1,E2,E3 empty;
> classDef odd  fill:#77F,stroke:#000,stroke-width:2px,color:#EEF;
> classDef even fill:#DDF,stroke:#000,stroke-width:2px,color:#447;
> class A,D,G,H odd;
> class B,C,E,F,I,J even;
> linkStyle 3,9,10 stroke-width:0px;
> ```
> *Input:* `root = [1, 10, 4, 3, null, 7, 9, 12, 8, 6, null, null, 2]`  
> *Output:* `true`  
> *Explanation:* The node values on each level are:
> - Level 0: `[1]`
> - Level 1: `[10, 4]`
> - Level 2: `[3, 7, 9]`
> - Level 3: `[12, 8, 6, 2]`
> Since levels `0` and `2` are all **odd** and **increasing** and levels `1` and `3` are all **even** and **decreasing**, the tree is *Even-Odd*.

#### Example 2:
> ```mermaid
> graph TD;
> A((5))---B((4))
> A---C((2))
> B---D((3))
> B---E((3))
> C---F((7))
> C---E1(( ))
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class E1 empty;
> classDef odd  fill:#77F,stroke:#000,stroke-width:2px,color:#EEF;
> classDef even fill:#DDF,stroke:#000,stroke-width:2px,color:#447;
> class A,D,E,F odd;
> class B,C even;
> linkStyle 5 stroke-width:0px;
> ```
> *Input:* `root = [5, 4, 2, 3, 3, 7]`  
> *Output:* `false`  
> *Explanation:* The node values on each level are:
> - Level 0: `[5]`
> - Level 1: `[4, 2]`
> - Level 2: `[3, 3, 7]`
> Node values in level `2` must be in strictly increasing order, so the tree is not *Even-Odd*.

#### Example 3:
> ```mermaid 
> graph TD;
> A((5))---B((9))
> A---C((1))
> B---D((3))
> B---E((5))
> C---F((7))
> C---E1(( ))
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class E1 empty;
> classDef odd  fill:#77F,stroke:#000,stroke-width:2px,color:#EEF;
> classDef even fill:#DDF,stroke:#000,stroke-width:2px,color:#447;
> class A,D,E,F odd;
> class B,C even;
> linkStyle 5 stroke-width:0px;
> ```
> *Input:* `root = [5, 9, 1, 3, 5, 7]`  
> *Output:* `false`  
> *Explanation:* Node values in the level `1` should be even integers.

#### Constraints:
- The number of nodes in the tree is in the range `[1, 10^5]`.
- `1 <= Node.val <= 10^6`


