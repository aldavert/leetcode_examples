#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

bool isEvenOddTree(TreeNode * root)
{
    std::queue<TreeNode *> q;
    if (root) q.push(root);
    for (bool odd = true; !q.empty(); odd = !odd)
    {
        int previous = 0;
        for (size_t i = 0, n = q.size(); i < n; ++i)
        {
            TreeNode * node = q.front();
            q.pop();
            if (node->left) q.push(node->left);
            if (node->right) q.push(node->right);
            if (odd)
            {
                if ((node->val & 1) != 1) return false;
                if ((i > 0) && (node->val <= previous)) return false;
            }
            else
            {
                if ((node->val & 1) != 0) return false;
                if ((i > 0) && (node->val >= previous)) return false;
            }
            previous = node->val;
        }
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = isEvenOddTree(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 10, 4, 3, null, 7, 9, 12, 8, 6, null, null, 2},  true, trials);
    test({5, 4, 2, 3, 3, 7}, false, trials);
    test({5, 9, 1, 3, 5, 7}, false, trials);
    return 0;
}


