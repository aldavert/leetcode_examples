#include "../common/common.hpp"
#include <queue>
#include <cstring>

// ############################################################################
// ############################################################################

int maxMoves(int kx, int ky, std::vector<std::vector<int> > positions)
{
    const int n = static_cast<int>(positions.size()),
              m = 50,
              dx[8] = {1, 1, 2, 2, -1, -1, -2, -2},
              dy[8] = {2, -2, 1, -1, 2, -2, 1, -1};
    int dist[17][50][50];
    std::memset(dist, -1, sizeof(dist));
    for (int i = 0; i <= n; ++i)
    {
        int x = (i < n) ? positions[i][0] : kx;
        int y = (i < n) ? positions[i][1] : ky;
        std::queue<std::pair<int, int> > q;
        q.push({x, y});
        dist[i][x][y] = 0;
        for (int step = 1; !q.empty(); ++step)
        {
            for (int k = static_cast<int>(q.size()); k > 0; --k)
            {
                auto [x1, y1] = q.front();
                q.pop();
                for (int j = 0; j < 8; ++j)
                {
                    int x2 = x1 + dx[j], y2 = y1 + dy[j];
                    if ((x2 >= 0) && (x2 < m)
                    &&  (y2 >= 0) && (y2 < m)
                    &&  (dist[i][x2][y2] == -1))
                    {
                        dist[i][x2][y2] = step;
                        q.push({x2, y2});
                    }
                }
            }
        }
    }
    std::vector<std::vector<int> > dp[2] = {
        std::vector<std::vector<int> >(n + 1, std::vector<int>(1 << n, -1)),
        std::vector<std::vector<int> >(n + 1, std::vector<int>(1 << n, -1)) };
    auto dfs = [&](auto &&self, int last, int state, int k) -> int
    {
        if (state == 0) return 0;
        if (dp[k][last][state] != -1) return dp[k][last][state];
        int result = (k == 1)?0:std::numeric_limits<int>::max();
        for (int i = 0; i < n; ++i)
        {
            int x = positions[i][0], y = positions[i][1];
            if ((state >> i) & 1)
            {
                int t = self(self, i, state ^ (1 << i), k ^ 1) + dist[last][x][y];
                if (k == 1) result = std::max(result, t);
                else result = std::min(result, t);
            }
        }
        return dp[k][last][state] = result;
    };
    return dfs(dfs, n, (1 << n) - 1, 1);
}

// ############################################################################
// ############################################################################

void test(int kx,
          int ky,
          std::vector<std::vector<int> > position,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxMoves(kx, ky, position);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 1, {{0, 0}}, 4, trials);
    test(0, 2, {{1, 1}, {2, 2}, {3, 3}}, 8, trials);
    test(0, 0, {{1, 2}, {2, 4}}, 3, trials);
    return 0;
}


