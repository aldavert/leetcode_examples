#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int sumOfEncryptedInt(std::vector<int> nums)
{
    auto encrypt = [](int sum, int value)
    {
        int number_of_digits = 0, max_value = 0;
        for (; value > 0; value /= 10, ++number_of_digits)
            max_value = std::max(max_value, value % 10);
        for (int i = 0; i < number_of_digits; ++i)
            value = value * 10 + max_value;
        return sum + value;
    };
    return std::accumulate(nums.begin(), nums.end(), 0, encrypt);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOfEncryptedInt(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 6, trials);
    test({10, 21, 31}, 66, trials);
    return 0;
}


