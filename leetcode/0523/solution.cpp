#include "../common/common.hpp"
#include <unordered_map>
#include "data.hpp"

// ############################################################################
// ############################################################################

#if 1
bool checkSubarraySum(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::unordered_map<int, int> lut{{0, -1}};
    for (int prefix = 0, i = 0; i < n; ++i)
    {
        prefix += nums[i];
        if (k != 0) prefix %= k;
        if (auto search = lut.find(prefix); search != lut.end())
        {
            if (i - search->second > 1) return true;
        }
        else lut[prefix] = i;
    }
    return false;
}
#else
bool checkSubarraySum(std::vector<int> nums, int k)
{
    std::vector<long> integral(nums.size() + 1, 0);
    for (size_t i = 0; i < nums.size(); ++i)
        integral[i + 1] = integral[i] + nums[i];
    for (size_t i = 0; i <= nums.size(); ++i)
        for (size_t j = i + 2; j <= nums.size(); ++j)
            if ((integral[j] - integral[i]) % k == 0)
                return true;
    return false;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkSubarraySum(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({23, 2, 4, 6, 7}, 6, true, trials);
    test({23, 2, 6, 4, 7}, 6, true, trials);
    test({23, 2, 6, 4, 7}, 13, false, trials);
    test({0, 0}, 1, true, trials);
    test(TESTS::large, 2'000'000'000, false, trials);
    return 0;
}

