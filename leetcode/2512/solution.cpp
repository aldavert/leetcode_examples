#include "../common/common.hpp"
#include <sstream>

// ############################################################################
// ############################################################################

std::vector<int> topStudents(std::vector<std::string> positive_feedback,
                             std::vector<std::string> negative_feedback,
                             std::vector<std::string> report,
                             std::vector<int> student_id,
                             int k)
{
    std::vector<std::pair<int, int> > score_and_ids;
    std::unordered_set<std::string> pos{positive_feedback.begin(),
                                        positive_feedback.end()},
                                    neg{negative_feedback.begin(),
                                        negative_feedback.end()};

    for (int i = 0, n = static_cast<int>(report.size()); i < n; ++i)
    {
        int score = 0;
        std::istringstream iss(report[i]);
        for (std::string word; iss >> word;)
        {
            if (pos.contains(word)) score += 3;
            if (neg.contains(word)) score -= 1;
        }
        score_and_ids.emplace_back(-score, student_id[i]);
    }
    
    std::partial_sort(score_and_ids.begin(), score_and_ids.begin() + k,
                      score_and_ids.end());
    std::vector<int> result;
    std::transform(score_and_ids.begin(), score_and_ids.begin() + k, 
                   back_inserter(result), [](const std::pair<int, int> &score_and_id)
                   { return score_and_id.second; });
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> positive_feedback,
          std::vector<std::string> negative_feedback,
          std::vector<std::string> report,
          std::vector<int> student_id,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = topStudents(positive_feedback, negative_feedback, report, student_id, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"smart", "brilliant", "studious"}, {"not"},
         {"this student is studious", "the student is smart"},
         {1, 2}, 2, {1, 2}, trials);
    test({"smart", "brilliant", "studious"}, {"not"},
         {"this student is not studious", "the student is smart"},
         {1, 2}, 2, {2, 1}, trials);
    return 0;
}


