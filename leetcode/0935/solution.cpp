#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

#if 1
int knightDialer(int n)
{
    constexpr int MOD = 1'000'000'007;
    std::vector<int> neighbors[10] = {
        /*0*/ {4, 6},
        /*1*/ {8, 6},
        /*2*/ {7, 9},
        /*3*/ {4, 8},
        /*4*/ {0, 3, 9},
        /*5*/ {},
        /*6*/ {0, 1, 7},
        /*7*/ {2, 6},
        /*8*/ {1, 3},
        /*9*/ {2, 4}};
    int counter[2][10] = {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                          {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
    bool second = false;
    for (int i = 1; i < n; ++i, second = !second)
    {
        std::memcpy(counter[second], counter[!second], 10 * sizeof(int));
        for (int j = 0; j < 10; ++j)
        {
            counter[second][j] = 0;
            for (int next : neighbors[j])
                counter[second][j] = (counter[second][j] + counter[!second][next]) % MOD;
        }
    }
    
    int result = 0;
    for (int i = 0; i < 10; ++i)
        result = (result + (counter[!second][i] % MOD)) % MOD;
    return result;
}
#else
int knightDialer(int n)
{
    constexpr int MOD = 1'000'000'007;
    std::vector<int> neighbors[10] = {
        /*0*/ {4, 6},
        /*1*/ {8, 6},
        /*2*/ {7, 9},
        /*3*/ {4, 8},
        /*4*/ {0, 3, 9},
        /*5*/ {},
        /*6*/ {0, 1, 7},
        /*7*/ {2, 6},
        /*8*/ {1, 3},
        /*9*/ {2, 4}};
    std::vector<long> counter(10, 1), previous(10);
    for (int i = 1; i < n; ++i)
    {
        std::copy(counter.begin(), counter.end(), previous.begin());
        for (int j = 0; j < 10; ++j)
        {
            counter[j] = 0;
            for (int vei : neighbors[j])
                counter[j] = (counter[j] + previous[vei]) % MOD;
        }
    }
    
    int result = 0;
    for (auto value : counter)
        result = (result + static_cast<int>(value % MOD)) % MOD;
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = knightDialer(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(   1,        10, trials);
    test(   2,        20, trials);
    test(   3,        46, trials);
    test(   4,       104, trials);
    test(   5,       240, trials);
    test(   6,       544, trials);
    test(   7,      1256, trials);
    test(3131, 136006598, trials);
    return 0;
}


