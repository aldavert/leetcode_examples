#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<std::string> readBinaryWatch(int turnedOn)
{
    std::vector<std::string> result;
    int bits[10] = {};
    auto backtrack = [&](auto &&self, int idx, int on) -> void
    {
        if (on == 0)
        {
            int hour = bits[0] * 8 + bits[1] * 4 + bits[2] * 2 + bits[3];
            int minutes = bits[4] * 32 + bits[5] * 16 + bits[6] * 8 + bits[7] * 4
                        + bits[8] * 2  + bits[9];
            if ((hour >= 0) && (hour <= 11) && (minutes >= 0) && (minutes <= 59))
            {
                std::string time;
                time += std::to_string(hour) + ":";
                if (minutes < 10) time += "0";
                time += std::to_string(minutes);
                result.push_back(time);
            }
        }
        else if (idx >= 10) return;
        else
        {
            bits[idx] = 1;
            self(self, idx + 1, on - 1);
            bits[idx] = 0;
            self(self, idx + 1, on);
        }
    };
    backtrack(backtrack, 0, turnedOn);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_map<std::string, unsigned int> histogram;
    for (const auto &l : left)
        ++histogram[l];
    for (const auto &r : right)
    {
        if (auto search = histogram.find(r); search != histogram.end())
        {
            if (search->second > 1)
                --search->second;
            else histogram.erase(search);
        }
    }
    return histogram.empty();
}

void test(int turnedOn, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = readBinaryWatch(turnedOn);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, {"0:01", "0:02", "0:04", "0:08", "0:16", "0:32", "1:00", "2:00", "4:00", "8:00"}, trials);
    test(9, {}, trials);
    return 0;
}


