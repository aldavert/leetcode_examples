#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int repeatedNTimes(std::vector<int> nums)
{
    std::vector<bool> lut(100'001, false);
    for (int n : nums)
    {
        if (lut[n]) return n;
        lut[n] = true;
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = repeatedNTimes(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 3}, 3, trials);
    test({2, 1, 2, 5, 3, 2}, 2, trials);
    test({5, 1, 5, 2, 5, 3, 5, 4}, 5, trials);
    return 0;
}


