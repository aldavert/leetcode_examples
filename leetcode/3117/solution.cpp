#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int minimumValueSum(std::vector<int> nums, std::vector<int> andValues)
{
    const int INF = 1'000'000'000;
    const int ALLMASK = (1 << 17) - 1;
    const int n = static_cast<int>(nums.size());
    const int m = static_cast<int>(andValues.size());
    std::vector<std::vector<std::unordered_map<int, int> > > mem(n,
            std::vector<std::unordered_map<int, int> >(m));
    auto process = [&](auto &&self, int i, int j, int mask) -> int
    {
        if ((i == n) && (j == m)) return 0;
        if ((i == n) || (j == m)) return INF;
        if (const auto it = mem[i][j].find(mask); it != mem[i][j].cend())
            return it->second;
        mask &= nums[i];
        if (mask < andValues[j])
            return mem[i][j][mask] = INF;
        if (mask == andValues[j])
            return mem[i][j][mask] = std::min(self(self, i + 1, j, mask),
                                          nums[i] + self(self, i + 1, j + 1, ALLMASK));
        return mem[i][j][mask] = self(self, i + 1, j, mask);
    };
    int result = process(process, 0, 0, ALLMASK);
    return (result == INF)?-1:result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> andValues,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumValueSum(nums, andValues);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 3, 3, 2}, {0, 3, 3, 2}, 12, trials);
    test({2, 3, 5, 7, 7, 7, 5}, {0, 7, 5}, 17, trials);
    return 0;
}


