#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> assignElements(std::vector<int> groups, std::vector<int> elements)
{
    const int n = *std::max_element(groups.begin(), groups.end()) + 1;
    std::vector<int> selected(n, -1);
    std::unordered_set<int> done;
    for (int i = 0, m = static_cast<int>(elements.size()); i < m; ++i)
    {
        if (done.find(elements[i]) != done.end())
            continue;
        done.insert(elements[i]);
        for (int j = elements[i]; j < n; j += elements[i])
            if (selected[j] == -1)
                selected[j] = i;
    }
    std::vector<int> result;
    for (int group : groups)
        result.push_back(selected[group]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> groups,
          std::vector<int> elements,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = assignElements(groups, elements);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 4, 3, 2, 4}, {4, 2}, {0, 0, -1, 1, 0}, trials);
    test({2, 3, 5, 7}, {5, 3, 3}, {-1, 1, 0, -1}, trials);
    test({10, 21, 30, 41}, {2, 1}, {0, 1, 0, 1}, trials);
    std::vector<int> test_groups, test_elements, test_expected;
    for (int i = 0; i < 100'000; ++i)
        test_groups.push_back(i + 1),
        test_elements.push_back(100'000 - i),
        test_expected.push_back(100'000 - i - 1);
    test(test_groups, test_elements, test_expected, trials);
    return 0;
}


