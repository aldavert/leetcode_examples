#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 0
int kthSmallest(TreeNode * root, int k)
{
    int count = 0, result = 0;
    auto traverse = [&](auto &&self, TreeNode * current) -> void
    {
        if (current->left) self(self, current->left);
        ++count;
        if (count == k)
            result = current->val;
        if (count >= k)
            return;
        if (current->right) self(self, current->right);
    };
    traverse(traverse, root);
    return result;
}
#else
int kthSmallest(TreeNode * root, int k)
{
    std::stack<TreeNode *> backtrace;
    int count = 0;
    for (TreeNode * current = root; true; )
    {
        while (current)
            backtrace.push(std::exchange(current, current->left));
        current = backtrace.top();
        backtrace.pop();
        if (++count == k) return current->val;
        current = current->right;
    }
    return -1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = kthSmallest(root, k);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    for (int t = 1; t <= 4; ++t)
        test({3, 1, 4, null, 2}, t, t, trials);
    for (int t = 1; t <= 6; ++t)
        test({5, 3, 6, 2, 4, null, null, 1}, t, t, trials);
    for (int t = 1; t <= 7; ++t)
        test({6, 3, 7, 2, 5, null, null, 1, null, 4}, t, t, trials);
    //              +---6---+
    //              |       |
    //           +--3--+    7
    //           |     |
    //         +-2   +-5
    //         |     |
    //         1     4
    //
    return 0;
}


