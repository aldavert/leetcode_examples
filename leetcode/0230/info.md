# Kth Smallest Element in a BST

Given the `root` of a binary search tree, and an integer `k`, return the `k^th` *smallest value* ***(1-indexed)*** *of all the values of the nodes in the tree*.

#### Example 1:
> ```mermaid
> graph TD;
> A((3))---B((1))
> A---C((4))
> B---EMPTY(( ))
> B---D((2))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef white fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY white;
> linkStyle 2 stroke-width:0px;
> ```
> *Input:* `root = [3, 1, 4, null, 2], k = 1`  
> *Output:* `1`

#### Example 2:
> ```mermaid
> graph TD;
> A((5))---B((3))
> A---C((6))
> B---D((2))
> B---E((4))
> D---F((1))
> D---EMPTY(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef white fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY white;
> linkStyle 5 stroke-width:0px;
> ```
> *Input:* `root = [5, 3, 6, 2, 4, null, null, 1], k = 3`  
> *Output:* `3`

#### Constraints:
- The number of nodes in the tree is `n`.
- `1 <= k <= n <= 10^4`
- `0 <= Node.val <= 10^4`
 
**Follow up:** If the BST is modified often (i.e., we can do insert and delete operations) and you need to find the `k^th` smallest frequently, how would you optimize?


