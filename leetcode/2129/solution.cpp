#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string capitalizeTitle(std::string title)
{
    const size_t n = title.size();
    size_t start = 0, current = 0;
    std::string result;
    auto addCapitalized = [&](std::string word) -> std::string
    {
        if (!word.empty())
        {
            if (!result.empty()) result += " ";
            word[0] = static_cast<char>((word.size() > 2)?
                                        std::toupper(word[0]):
                                        std::tolower(word[0]));
            for (size_t i = 1, m = word.size(); i < m; ++i)
                word[i] = static_cast<char>(std::tolower(word[i]));
            result += word;
        }
        return word;
    };
    for (; current < n; ++current)
    {
        if (title[current] == ' ')
        {
            addCapitalized(title.substr(start, current - start));
            start = current + 1;
        }
    }
    addCapitalized(title.substr(start, current - start));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string title, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = capitalizeTitle(title);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("capiTalIze tHe titLe", "Capitalize The Title", trials);
    test("First leTTeR of EACH Word", "First Letter of Each Word", trials);
    test("i lOve leetcode", "i Love Leetcode", trials);
    test("  i lOve   leetcode   ", "i Love Leetcode", trials);
    return 0;
}


