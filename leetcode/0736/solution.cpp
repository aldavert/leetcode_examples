#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int evaluate(std::string expression)
{
    enum class OP { LET, ADD, MUL };
    const std::unordered_map<std::string, OP> str2op = {
        { "let", OP::LET }, { "add", OP::ADD }, { "mult", OP::MUL } };
    struct Scope
    {
        OP operation = OP::LET;
        std::unordered_map<std::string, int> variables;
        int count = 0;
        std::string variable = "";
        int value = 0;
    };
    std::vector<Scope> scopes({{}});
    auto get = [&](const std::string &token) -> int
    {
        if (!token.empty())
        {
            if (std::isalpha(token.front()))
            {
                for (int i = static_cast<int>(scopes.size()) - 1; i >= 0; --i)
                    if (auto search = scopes[i].variables.find(token);
                        search != scopes[i].variables.end())
                        return search->second;
            }
            else return std::stoi(token);
        }
        return 0;
    };
    std::string token;
    auto update = [&](void) -> void
    {
        if (token.empty()) return;
        if (scopes.back().count == 0)
            scopes.back().operation = str2op.at(token);
        else if ((scopes.back().count & 1) == 1)
        {
            if (scopes.back().operation == OP::LET)
                scopes.back().variable = token,
                scopes.back().value = get(token);
            else scopes.back().value = get(token);
        }
        else
        {
            if (scopes.back().operation == OP::LET)
                scopes.back().variables[scopes.back().variable] = get(token);
            else if (scopes.back().operation == OP::ADD)
                scopes.back().value += get(token);
            else scopes.back().value *= get(token);
        }
        ++scopes.back().count;
        token = "";
    };
    for (char letter : expression)
    {
        if (letter == '(')
        {
            update();
            scopes.push_back({});
        }
        else if (letter == ')')
        {
            update();
            int value = scopes.back().value;
            scopes.pop_back();
            if (scopes.back().operation == OP::LET)
            {
                if ((scopes.size() == 1) || ((scopes.back().count & 1) != 0))
                    scopes.back().value = value;
                else scopes.back().variables[scopes.back().variable] = value;
            }
            else
            {
                if ((scopes.back().count & 1) == 1)
                    scopes.back().value = value;
                else
                {
                    if (scopes.back().operation == OP::ADD)
                        scopes.back().value += value;
                    else scopes.back().value *= value;
                }
            }
            ++scopes.back().count;
        }
        else if (letter == ' ')
            update();
        else token += letter;
    }
    return scopes.back().value;
}

// ############################################################################
// ############################################################################

void test(std::string expression, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = evaluate(expression);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("(let x 2 (mult x (let x 3 y 4 (add x y))))", 14, trials);
    test("(let x 3 x 2 x)", 2, trials);
    test("(let x 1 y 2 x (add x y) (add x y))", 5, trials);
    test("(let x 2 x)", 2, trials);
    test("(add (mult 2 4) (add 2 7))", 8+9, trials);
    test("(add 7 (mult (mult 2 4) (add 2 7)))", 7+8*9, trials);
    test("(add (mult (mult 2 4) (add 2 7)) 7)", 7+8*9, trials);
    test("(mult 7 (mult (mult 2 4) (add 2 7)))", 7*8*9, trials);
    test("(mult (mult (mult 2 4) (add 2 7)) 7)", 7*8*9, trials);
    test("(mult 7 (add (mult 2 4) (add 2 7)))", 7*(8+9), trials);
    test("(mult (add (mult 2 4) (add 2 7)) 7)", 7*(8+9), trials);
    return 0;
}


