#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int miceAndCheese(std::vector<int> reward1, std::vector<int> reward2, int k)
{
    std::vector<int> diffs;
    for (int i = 0, n = static_cast<int>(reward1.size()); i < n; ++i)
        diffs.push_back(reward1[i] - reward2[i]);
    std::nth_element(diffs.begin(), diffs.begin() + k, diffs.end(), std::greater<>());
    return std::accumulate(reward2.begin(), reward2.end(), 0)
         + std::accumulate(diffs.begin(), diffs.begin() + k, 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> reward1,
          std::vector<int> reward2,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = miceAndCheese(reward1, reward2, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 3, 4}, {4, 4, 1, 1}, 2, 15, trials);
    test({1, 1}, {1, 1}, 2, 2, trials);
    return 0;
}


