#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minMaxSums(std::vector<int> nums, int k)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(nums.size());

    std::vector<long long> v(n + 2, 1);
    for (int i = 2; i <= n; ++i)
        v[i] = MOD - (MOD / i) * v[MOD % i] % MOD;
    
    int p = k - 1;
    std::vector<long long> c(n + 1);

    if (p >= 1)
    {
        c[p] = 1;
        for (int i = p + 1; i <= n; ++i)
            c[i] = (c[i - 1] * i % MOD) * v[i - p] % MOD;
    }
    std::vector<long long> s(n + 1);
    if (p < 1) std::fill(s.begin(), s.end(), 1);
    else
    {
        s[0] = 1;
        for (int i = 1; i <= n; ++i)
        {
            s[i] = 2 * s[i - 1] % MOD;
            if (i - 1 >= p)
                s[i] = (s[i] - c[i - 1] + MOD) % MOD;
        }
    }
    std::vector<int> x = nums;
    std::sort(x.begin(), x.end());
    std::vector<int> y = x;
    std::reverse(y.begin(), y.end());
    
    long long min_value = 0, max_value = 0;
    for (int i = 0; i < n; ++i)
    {
        int j = n - i - 1;
        if (j < 0) continue;
        min_value = (min_value + static_cast<long long>(x[i]) * s[j]) % MOD;
        max_value = (max_value + static_cast<long long>(y[i]) * s[j]) % MOD;
    }
    return static_cast<int>((min_value + max_value) % MOD);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minMaxSums(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 2, 24, trials);
    test({5, 0, 6}, 1, 22, trials);
    test({1, 1, 1}, 2, 12, trials);
    return 0;
}


