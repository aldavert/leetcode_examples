#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfSubarrays(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    auto numberOfSubarraysAtMost = [&](int m) -> int
    {
        int result = 0;
        
        for (int l = 0, r = 0; r <= n;)
        {
            if (m >= 0)
            {
                result += r - l;
                if (r == n) break;
                if (nums[r] & 1) --m;
                ++r;
            }
            else
            {
                if (nums[l] & 1) ++m;
                ++l;
            }
        }
        return result;
    };
    return numberOfSubarraysAtMost(k) - numberOfSubarraysAtMost(k - 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfSubarrays(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2, 1, 1}, 3, 2, trials);
    test({2, 4, 6}, 1, 0, trials);
    test({2, 2, 2, 1, 2, 2, 1, 2, 2, 2}, 2, 16, trials);
    return 0;
}


