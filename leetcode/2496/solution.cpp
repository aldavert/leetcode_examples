#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumValue(std::vector<std::string> strs)
{
    int result = 0;
    for (std::string token : strs)
    {
        bool number = true;
        int value = 0;
        for (char letter : token)
            number = number && (letter >= '0') && (letter <= '9'),
            value = number * (value * 10 + letter - '0');
        result = std::max(result, (number)?value:static_cast<int>(token.size()));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> strs, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumValue(strs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"alic3", "bob", "3", "4", "00000"}, 5, trials);
    test({"1", "01", "001", "0001"}, 1, trials);
    return 0;
}


