#include "../common/common.hpp"
#include "../common/list.hpp"
#include <unordered_map>
#include <utility>

// ############################################################################
// ############################################################################

ListNode * removeZeroSumSublists(ListNode * head)
{
    ListNode anchor_head(0, head);
    int prefix = 0;
    std::unordered_map<int, ListNode *> prefix_to_node;
    
    prefix_to_node[0] = &anchor_head;
    for (ListNode * node = head; node; node = node->next)
    {
        prefix += node->val;
        prefix_to_node[prefix] = node;
    }
    prefix = 0;
    ListNode * node = prefix_to_node[0]->next;
    ListNode anchor_result(0, nullptr);
    for (ListNode * result = &anchor_result; node; result = result->next)
    {
        result->next = new ListNode(node->val);
        prefix += node->val;
        node = prefix_to_node[prefix]->next;
    }
    anchor_head.next = nullptr;
    return std::exchange(anchor_result.next, nullptr);;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> vec,
          std::set<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(vec);
        ListNode * output = removeZeroSumSublists(head);
        result = list2vec(output);
        delete head;
        delete output;
    }
    showResult(solution.find(result) != solution.end(), solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, -3, 3, 1}, {{3, 1}, {1, 2, 1}}, trials);
    test({1, 2, 3, -3, 4}, {{1, 2, 4}}, trials);
    test({1, 2, 3, -3, -2}, {{1}}, trials);
    test({1, -1}, {{}}, trials);
    return 0;
}


