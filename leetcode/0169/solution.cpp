#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int majorityElement(std::vector<int> nums)
{
    const unsigned int thr = static_cast<unsigned int>(nums.size()) / 2;
    unsigned int histogram[32] = {};
    for (int n : nums)
        for (int i = 0; i < 32; ++i, n >>= 1)
            if (n & 1) ++histogram[i];
    int result = 0;
    for (int i = 31; i >= 0; --i)
    {
        result <<= 1;
        if (histogram[i] > thr)
            result |= 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = majorityElement(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 3}, 3, trials);
    test({2, 2, 1, 1, 1, 2, 2}, 2, trials);
    return 0;
}


