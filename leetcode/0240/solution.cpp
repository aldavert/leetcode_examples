#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool searchMatrix(std::vector<std::vector<int> > matrix, int target)
{
    int r = 0;
    int c = static_cast<int>(matrix[0].size()) - 1;
    while ((c >= 0) && (r < static_cast<int>(matrix.size())))
    {
        if (matrix[r][c] == target) return true;
        else if (matrix[r][c] < target) ++r;
        else --c;
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix,
          int target,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = searchMatrix(matrix, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{ 1,  4,  7, 11, 15},
          { 2,  5,  8, 12, 19},
          { 3,  6,  9, 16, 22},
          {10, 13, 14, 17, 24},
          {18, 21, 23, 26, 30}}, 5, true, trials);
    test({{ 1,  4,  7, 11, 15},
          { 2,  5,  8, 12, 19},
          { 3,  6,  9, 16, 22},
          {10, 13, 14, 17, 24},
          {18, 21, 23, 26, 30}}, 20, false, trials);
    return 0;
}


