#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxBuilding(int n, std::vector<std::vector<int> > restrictions)
{
    struct Restriction
    {
        int id;
        int height;
        bool operator<(const Restriction &other) const { return id < other.id; };
        void update(const Restriction &other)
        {
            int distance = std::abs(id - other.id);
            height = std::min(height, other.height + distance);
        }
        int heightUpdate(const Restriction &other) const
        {
            return std::max(height, other.height)
                 + (id - other.id - std::abs(other.height - height)) / 2;
        }
    };
    std::vector<Restriction> sort_rest;
    
    for (const auto &restriction : restrictions)
        sort_rest.push_back({restriction[0], restriction[1]});
    sort_rest.push_back({1, 0});
    sort_rest.push_back({n, n - 1});
    std::sort(sort_rest.begin(), sort_rest.end());
    const int m = static_cast<int>(sort_rest.size());
    
    for (int i = 1; i < m; ++i)
        sort_rest[i].update(sort_rest[i - 1]);
    for (int i = m - 2; i >= 0; --i)
        sort_rest[i].update(sort_rest[i + 1]);
    
    int result = 0;
    for (int i = 1; i < m; ++i)
        result = std::max(result, sort_rest[i].heightUpdate(sort_rest[i - 1]));
    
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > restrictions,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxBuilding(n, restrictions);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{2, 1}, {4, 1}}, 2, trials);
    test(6, {}, 5, trials);
    test(10, {{5, 3}, {2, 5}, {7, 4}, {10, 3}}, 5, trials);
    return 0;
}


