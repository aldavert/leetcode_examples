#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int maxSelectedElements(std::vector<int> nums)
{
    int result = 1;
    std::sort(nums.begin(), nums.end());
    for (int dp[2] = {1, 1}, prev = std::numeric_limits<int>::lowest(); int num : nums)
    {
        if (num == prev) dp[1] = dp[0] + 1;
        else if (num == prev + 1) ++dp[0], ++dp[1];
        else if (num == prev + 2) dp[0] = std::exchange(dp[1], 1) + 1;
        else dp[0] = dp[1] = 1;
        result = std::max({result, dp[0], dp[1]});
        prev = num;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSelectedElements(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 5, 1, 1}, 3, trials);
    test({1, 4, 7, 10}, 1, trials);
    return 0;
}


