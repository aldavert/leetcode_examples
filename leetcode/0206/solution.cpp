#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

ListNode * reverseList(ListNode * head)
{
    ListNode * previous = nullptr;
    for (ListNode * current = head; current != nullptr;)
    {
        ListNode * next = current->next;
        current->next = previous;
        previous = current;
        current = next;
    }
    return previous;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> input, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * linput = vec2list(input);
        ListNode * lr = reverseList(linput);
        result = list2vec(lr);
        delete lr;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, {5, 4, 3, 2, 1}, trials);
    test({1, 2}, {2, 1}, trials);
    test({}, {}, trials);
    return 0;
}


