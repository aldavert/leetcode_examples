# Reverse Linked List

Given the `head` of a singly linked list, reverse the list, and return *the reversed list*.

#### Example 1:
> ```mermaid 
> flowchart TD;
> 
> subgraph TOP
> direction LR;
> A((1))-->B((2))
> B-->C((3))
> C-->D((4))
> D-->E((5))
> end
> 
> subgraph BOTTOM
> direction LR;
> F((5))-->G((4))
> G-->H((3))
> H-->I((2))
> I-->J((1))
> end
> TOP-->BOTTOM
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px,color:#FFF;
> class TOP,BOTTOM empty;
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `head = [1, 2, 3, 4, 5]`  
> *Output:* `[5, 4, 3, 2, 1]`

#### Example 2:
> ```mermaid 
> flowchart TD;
> 
> subgraph TOP
> direction LR;
> A((1))-->B((2))
> end
> 
> subgraph BOTTOM
> direction LR;
> F((2))-->G((1))
> end
> TOP-->BOTTOM
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px,color:#FFF;
> class TOP,BOTTOM empty;
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `head = [1, 2]`  
> *Output:* `[2, 1]`

#### Example 3:
> *Input:* `head = []`  
> *Output:* `[]`
 
#### Constraints:
- The number of nodes in the list is the range `[0, 5000]`.
- `-5000 <= Node.val <= 5000`
 
**Follow up:** A linked list can be reversed either iteratively or recursively. Could you implement both?


