#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string minimizeResult(std::string expression)
{
    const size_t plus_index = expression.find('+');
    const std::string left = expression.substr(0, plus_index),
                      right = expression.substr(plus_index + 1);
    std::string result;
    int mn = std::numeric_limits<int>::max();
    
    for (size_t i = 0; i < left.size(); ++i)
    {
        for (size_t j = 0; j < right.size(); ++j)
        {
            const int a = (i == 0)?1:std::stoi(left.substr(0, i));
            const int b = std::stoi(left.substr(i));
            const int c = std::stoi(right.substr(0, j + 1));
            const int d = (j == right.size() - 1)?1:std::stoi(right.substr(j + 1));
            const int val = a * (b + c) * d;
            if (val < mn)
            {
                mn = val;
                result = ((i == 0)?"":std::to_string(a))
                       + '(' + std::to_string(b) + '+' + std::to_string(c) + ')'
                       + ((j == right.size() - 1)?"":std::to_string(d));
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string expression, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimizeResult(expression);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("247+38", "2(47+38)", trials);
    test("12+34", "1(2+3)4", trials);
    test("999+999", "(999+999)", trials);
    return 0;
}


