#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> getLongestSubsequence(std::vector<std::string> words,
                                               std::vector<int> groups)
{
    std::vector<std::string> result;
    for (int next = groups[0], i = 0, n = static_cast<int>(words.size()); i < n; ++i)
    {
        if (groups[i] == next)
        {
            result.push_back(words[i]);
            next = !next;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::vector<int> groups,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getLongestSubsequence(words, groups);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"e", "a", "b"}, {0, 0, 1}, {"e", "b"}, trials);
    test({"a", "b", "c", "d"}, {1, 0, 1, 1}, {"a", "b", "c"}, trials);
    test({"c"}, {0}, {"c"}, trials);
    test({"d"}, {1}, {"d"}, trials);
    return 0;
}


