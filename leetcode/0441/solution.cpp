#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int arrangeCoins(int n)
{
    // 1 + 2 + 3 + 4 + 5 = 15;
    // n = n * (n + 1) / 2 = 5 * 6 / 2 = 15;
    // 1 + 2 + 3 + 4 + 5 + 6 + 7 = 28;
    // n = n * (n + 1) / 2 = 7 * 8 / 2 = 28;
    //M = n * (n + 1) / 2 => 2 * M = n * (n + 1) = n^2 + n -> n^2 + n - 2 * M = 0
    //n = (-b +- sqrt(b^2 - 4ac)) / 2a =
    //    (-1 +- sqrt(1 - 4 * 1 * -2 * M)) / 2 =
    //    (-1 +- sqrt(1 + 8 * M) / 2 =
    //    (sqrt(1 + 8 * M) - 1) / 2
    //M = 28 -> (sqrt(1 + 224) - 1) / 2 = (15 - 1) / 2 = 14 / 2 = 7;
    return static_cast<int>((std::sqrt(1 + 8.0 * n) - 1.0) / 2.0);
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = arrangeCoins(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 2, trials);
    test(8, 3, trials);
    return 0;
}


