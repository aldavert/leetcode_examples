#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findPermutationDifference(std::string s, std::string t)
{
    int result = 0, indices[26] = {};
    for (int i = 0, n = static_cast<int>(s.size()); i < n; ++i)
        indices[s[i] - 'a'] = i;
    for (int i = 0, n = static_cast<int>(t.size()); i < n; ++i)
        result += std::abs(indices[t[i] - 'a'] - i);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string t, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findPermutationDifference(s, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", "bac", 2, trials);
    test("abcde", "edbac", 12, trials);
    return 0;
}


