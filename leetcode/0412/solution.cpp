#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> fizzBuzz(int n)
{
    std::vector<std::string> result;
    for (int i = 1; i <= n; ++i)
    {
        std::string current;
        if (i % 3 == 0) current = "Fizz";
        if (i % 5 == 0) current += "Buzz";
        if (current.empty()) result.push_back(std::to_string(i));
        else result.push_back(current);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = fizzBuzz(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {"1", "2", "Fizz"}, trials);
    test(5, {"1", "2", "Fizz", "4", "Buzz"}, trials);
    test(15, {"1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz",
              "11", "Fizz", "13", "14", "FizzBuzz"}, trials);
    return 0;
}


