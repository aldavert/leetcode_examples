#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

std::vector<std::string> simplifiedFractions(int n)
{
    std::vector<std::string> result;
    for (int denominator = 2; denominator <= n; ++denominator)
        for (int numerator = 1; numerator < denominator; ++numerator)
            if (std::gcd(denominator, numerator) == 1)
                result.push_back(std::to_string(numerator) + "/"
                               + std::to_string(denominator));
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<std::string> left_work(left), right_work(right);
    std::sort(left_work.begin(), left_work.end());
    std::sort(right_work.begin(), right_work.end());
    for (size_t i = 0; i < left.size(); ++i)
        if (left_work[i] != right_work[i])
            return false;
    return true;
}

void test(int n, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = simplifiedFractions(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {"1/2"}, trials);
    test(3, {"1/2", "1/3", "2/3"}, trials);
    test(4, {"1/2", "1/3", "1/4", "2/3", "3/4"}, trials);
    return 0;
}


