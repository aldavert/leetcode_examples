#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> xorQueries(std::vector<int> arr,
                            std::vector<std::vector<int> > queries)
{
    std::vector<int> result, xors(arr.size() + 1);
    
    for (size_t i = 0; i < arr.size(); ++i)
        xors[i + 1] = xors[i] ^ arr[i];
    for (const auto &query : queries)
        result.push_back(xors[query[0]] ^ xors[query[1] + 1]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = xorQueries(arr, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 4, 8}, {{0, 1}, {1, 2}, {0, 3}, {3, 3}}, {2, 7, 14, 8}, trials);
    test({4, 8, 2, 10}, {{2, 3}, {1, 3}, {0, 0}, {0, 3}}, {8, 0, 4, 4}, trials);
    return 0;
}


