#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

class RangeFreqQuery
{
    std::unordered_map<int, std::vector<int> > m_value_to_indices;
public:
    RangeFreqQuery(std::vector<int> arr)
    {
        for (int i = 0, n = static_cast<int>(arr.size()); i < n; ++i)
            m_value_to_indices[arr[i]].push_back(i);
    }
    int query(int left, int right, int value)
    {
        auto search = m_value_to_indices.find(value);
        if (search == m_value_to_indices.end()) return 0;
        const std::vector<int> &indices = search->second;
        auto i = std::lower_bound(indices.begin(), indices.end(), left);
        auto j = std::upper_bound(indices.begin(), indices.end(), right);
        return static_cast<int>(std::distance(i, j));
    }
};

// ############################################################################
// ############################################################################

void test(const std::vector<int> &arr,
          std::vector<std::tuple<int, int, int> > parameters,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        RangeFreqQuery obj(arr);
        result.clear();
        for (auto [left, right, value] : parameters)
            result.push_back(obj.query(left, right, value));
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({12, 33, 4, 56, 22, 2, 34, 33, 22, 12, 34, 56},
         {{1, 2, 4}, {0, 11, 33}}, {1, 2}, trials);
    test({8, 4, 2, 5, 4, 5, 8, 6, 2, 3}, 
         {{0, 3, 5}, {5, 6, 2}, {6, 8, 4}, {2, 8, 3}, {4, 5, 1}, {2, 4, 2}}, 
         {1, 0, 0, 0, 0, 1}, trials);
    return 0;
}


