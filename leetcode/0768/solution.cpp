#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxChunksToSorted(std::vector<int> arr)
{
    const int n = static_cast<int>(arr.size());
    if (n <= 1) return 1;
    std::vector<int> minimum(arr);
    minimum[n - 1] = arr[n - 1];
    for (int i = n - 2; i >= 0; --i)
        minimum[i] = std::min(arr[i], minimum[i + 1]);
    int maximum = arr[0];
    int result = (maximum <= minimum[1]) + 1;
    for (int i = 2; i < n; ++i)
    {
        maximum = std::max(maximum, arr[i - 1]);
        result += maximum <= minimum[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxChunksToSorted(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 3, 2, 1}, 1, trials);
    test({2, 1, 3, 4, 4}, 4, trials);
    return 0;
}


