#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumDifference(std::vector<int> nums, int k)
{
    if (k <= 0) return 0;
    std::sort(nums.begin(), nums.end());
    int result = std::numeric_limits<int>::max();
    for (size_t i = 0, n = static_cast<int>(nums.size()) - k + 1; i < n; ++i)
        result = std::min(result, nums[i + k - 1] - nums[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumDifference(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({90}, 1, 0, trials);
    test({9, 4, 1, 7}, 2, 2, trials);
    return 0;
}


