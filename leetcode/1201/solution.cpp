#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int nthUglyNumber(int n, int a, int b, int c)
{
    const long ab = static_cast<long>(a) * b / std::gcd(a, b),
               ac = static_cast<long>(a) * c / std::gcd(a, c),
               bc = static_cast<long>(b) * c / std::gcd(b, c),
               abc = static_cast<long>(a) * bc / std::gcd(a, bc);
    long l = 1, r = 2'000'000'000;
    while (l < r)
    {
        long m = l + (r - l) / 2;
        if (m / a + m / b + m / c - m / ab - m / ac - m / bc + m / abc >= n)
            r = m;
        else l = m + 1;
    }
    return static_cast<int>(l);
}

// ############################################################################
// ############################################################################

void test(int n, int a, int b, int c, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = nthUglyNumber(n, a, b, c);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 2, 3, 5, 4, trials);
    test(4, 2, 3, 4, 6, trials);
    test(5, 2, 11, 13, 10, trials);
    return 0;
}


