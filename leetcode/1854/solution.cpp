#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int maximumPopulation(std::vector<std::vector<int> > logs)
{
    struct Range
    {
        int begin = 0;
        int end = 0;
        bool operator<(const Range &other) const
        {
            return (begin < other.begin)
                || ((begin == other.begin) && (end < other.end));
        }
    };
    std::vector<Range> ordered_logs;
    for (const auto &year : logs)
        ordered_logs.push_back({year[0], year[1]});
    std::sort(ordered_logs.begin(), ordered_logs.end());
    size_t maximum_population = 0;
    int maximum_year = 0;
    std::priority_queue<int, std::vector<int>, std::greater<int> > population;
    for (auto [begin, end] : ordered_logs)
    {
        while (!population.empty() && (begin >= population.top()))
            population.pop();
        population.push(end);
        if (population.size() > maximum_population)
        {
            maximum_population = population.size();
            maximum_year = begin;
        }
    }
    return maximum_year;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > logs, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumPopulation(logs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1993, 1999}, {2000, 2010}}, 1993, trials);
    test({{1950, 1961}, {1960, 1971}, {1970, 1981}}, 1960, trials);
    test({{2033, 2034}, {2039, 2047}, {1998, 2042}, {2047, 2048}, {2025, 2029},
          {2005, 2044}, {1990, 1992}, {1952, 1956}, {1984, 2014}}, 2005, trials);
    return 0;
}


