#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> countRectangles(std::vector<std::vector<int> > rectangles,
                                 std::vector<std::vector<int> > points)
{
    std::vector<int> y_to_xs[101], result;
    
    for (const auto &r : rectangles)
        y_to_xs[r[1]].push_back(r[0]);
    for (int i = 0; i < 101; ++i)
        std::sort(y_to_xs[i].begin(), y_to_xs[i].end());
    for (const auto &p : points)
    {
        int count = 0;
        for (int y = p[1]; y < 101; ++y)
        {
            const auto &xs = y_to_xs[y];
            count += static_cast<int>(xs.end()
                   - std::lower_bound(xs.begin(), xs.end(), p[0]));
        }
        result.push_back(count);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > rectangles,
          std::vector<std::vector<int> > points,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countRectangles(rectangles, points);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 3}, {2, 5}}, {{2, 1}, {1, 4}}, {2, 1}, trials);
    test({{1, 1}, {2, 2}, {3, 3}}, {{1, 3}, {1, 1}}, {1, 3}, trials);
    return 0;
}


