#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int idealArrays(int n, int maxValue)
{
    constexpr int MOD = 1'000'000'007;
    const int m = std::max(n, maxValue) + 1;
    auto power = [&](long a, long b) -> long
    {
        long result = 1;
        for (; b > 0; a = (a * a) % MOD, b = b / 2)
            if (b % 2 == 1)
                result = (result * a) % MOD;
        return result;
    };
    std::vector<long> fact(m), invfact(m);
    invfact[0] = fact[0] = 1;
    for (int i = 1; i < m; ++i)
        fact[i] = (i * fact[i-1]) % MOD,
        invfact[i] = power(fact[i], MOD - 2);
    auto nCr = [&](int r) -> long
    {
        if (r > n - 1) return 0;
        return (((fact[n - 1] * invfact[n - r - 1]) % MOD) * invfact[r]) % MOD;
    };
    std::vector<std::vector<int> > dp(maxValue + 1, std::vector<int>(15));
    for (int i = 1; i <= maxValue; ++i)
        dp[i][1] = 1;
    for (int i = 1; i <= maxValue; ++i)
        for (int len = 1; len < 14; ++len)
            for (int j = 2 * i; j <= maxValue; j += i)
                dp[j][len + 1] += dp[i][len];
    
    long long result = 0;
    for (int last = 1; last <= maxValue; ++last)
        for (int uc = 1; uc < 15; ++uc)
            result = (result + (nCr(uc - 1) * dp[last][uc]) % MOD) % MOD;
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(int n, int maxValue, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = idealArrays(n, maxValue);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 5, 10, trials);
    test(5, 3, 11, trials);
    test(282, 297, 383'367'638, trials);
    return 0;
}


