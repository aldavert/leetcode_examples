#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countSubstrings(std::string s)
{
    const int n = static_cast<int>(s.size());
    int result = 0;
    for (int i = 0; i < n; ++i)
    {
        for (int l = i, r = i    ; (l >= 0) && (r < n) && (s[l] == s[r]); --l, ++r)
            ++result;
        for (int l = i, r = i + 1; (l >= 0) && (r < n) && (s[l] == s[r]); --l, ++r)
            ++result;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSubstrings(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", 3, trials);
    test("aaa", 6, trials);
    return 0;
}


