#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int maximumProduct(std::vector<int> nums, int k)
{
    constexpr long MOD = 1'000'000'007;
    const long n = static_cast<long>(nums.size());
    long x = 0;
    std::sort(nums.begin(), nums.end());
    for (long i = 0, pref = 0; i < n; ++i)
    {
        pref += nums[i];
        x = (pref + k) / (i + 1);
        if ((nums[i] <= x) && ((i + 1 == n) || (x <= nums[i + 1])))
            break;
    }
    for (long i = 0; i < n; ++i)
    {
        if (nums[i] >= x) break;
        k -= static_cast<int>(x - nums[i]);
        nums[i] = static_cast<int>(x);
    }
    long result = 1;
    for(long i = 0; i < n; ++i)
        result = (result * (nums[i] + (i < k))) % MOD;
    return static_cast<int>(result);
}
#else
int maximumProduct(std::vector<int> nums, int k)
{
    constexpr int MOD = 1'000'000'007;
    long result = 1;
    std::priority_queue<int, std::vector<int>, std::greater<>> heap;
    
    for (int num : nums)
        heap.push(num);
    for (int i = 0; i < k; ++i)
    {
        int min_num = heap.top();
        heap.pop();
        heap.push(min_num + 1);
    }
    for (; !heap.empty(); heap.pop())
        result = (result * heap.top()) % MOD;
    return static_cast<int>(result);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumProduct(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 4}, 5, 20, trials);
    test({6, 3, 3, 2}, 2, 216, trials);
    return 0;
}


