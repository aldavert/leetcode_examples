#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxValue(std::vector<int> nums, int k)
{
    auto isSubMask = [](int a,int b) -> bool { return (a | b) == b; };
    const int n = static_cast<int>(nums.size());
    int prefix[130], suffix[130], dp[128], count[128] = {};
    for (int i = 0; i < 130; ++i) prefix[i] = suffix[i] = -1;
    for (int i = 0; i < 128; ++i) dp[i] = 1e9;
    
    for (int i = 0; i < n; ++i)
    {
        dp[nums[i]] = 1;
        for (int mask = 0; mask < 128; ++mask)
        {
            dp[mask | nums[i]] = std::min(dp[mask | nums[i]], 1 + dp[mask]);
            count[mask] += (isSubMask(nums[i], mask) != 0);
        }
        for (int mask = 0; mask < 128; ++mask)
            if ((dp[mask] <= k) && (count[mask] >= k) && (prefix[mask] == -1))
                prefix[mask] = i;
    }
    for (int i = 0; i < 128; ++i) dp[i] = 1e9, count[i] = 0;
    for (int i = n - 1; i >= 0; --i)
    {
        dp[nums[i]] = 1;
        for (int mask = 0; mask < 128; ++mask)
        {
            dp[mask | nums[i]] = std::min(dp[mask | nums[i]], 1 + dp[mask]);
            count[mask] += (isSubMask(nums[i], mask) != 0);
        }
        for (int mask = 0; mask < 128; ++mask)
            if ((dp[mask] <= k) && (count[mask] >= k) && (suffix[mask] == -1))
                suffix[mask] = i;
    }
    for (int mask = 127; mask >= 0; --mask)
        for (int l = 127; l >= 0; --l)
            if (int r = mask ^ l; (prefix[l] != -1) && (suffix[r] != -1)
                               && (prefix[l] < suffix[r]))
                return mask;
    return 0;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxValue(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 6, 7}, 1, 5, trials);
    test({4, 2, 5, 6, 7}, 2, 2, trials);
    test({13, 95, 33, 26}, 2, 100, trials);
    return 0;
}


