#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
long long minimumOperations(std::vector<int> nums, std::vector<int> target)
{
    long long result = 0;
    for (int i = 0, n = static_cast<int>(nums.size()), previous = 0; i < n; ++i)
    {
        int current = target[i] - nums[i];
        if ((current ^ previous) >= 0)
        {
            int d = (current >= 0)?current - previous:previous - current;
            if (d > 0) result += d;
        }
        else result += std::abs(current);
        previous = current;
    }
    return result;
}
#else
long long minimumOperations(std::vector<int> nums, std::vector<int> target)
{
    long result = std::abs(nums[0] - target[0]);
    for (int i = 1, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        const int curr_diff = target[i] - nums[i],
                  prev_diff = target[i - 1] - nums[i - 1];
        if ((curr_diff >= 0) && (prev_diff >= 0))
            result += std::max(0, curr_diff - prev_diff);
        else if ((curr_diff <= 0) && (prev_diff <= 0))
            result += std::max(0, std::abs(curr_diff) - std::abs(prev_diff));
        else
            result += std::abs(curr_diff);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> target,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumOperations(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 5, 1, 2}, {4, 6, 2, 4}, 2, trials);
    test({1, 3, 2}, {2, 1, 4}, 5, trials);
    return 0;
}


