#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkTwoChessboards(std::string coordinate1, std::string coordinate2)
{
    return (((coordinate1[0] - 'a') & 1) ^ ((coordinate1[1] - '1') & 1))
        == (((coordinate2[0] - 'a') & 1) ^ ((coordinate2[1] - '1') & 1));
}

// ############################################################################
// ############################################################################

void test(std::string coordinate1,
          std::string coordinate2,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkTwoChessboards(coordinate1, coordinate2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("a1", "c3", true, trials);
    test("a1", "h3", false, trials);
    return 0;
}


