#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int minTrioDegree(int n, std::vector<std::vector<int> > edges)
{
    bool adj[401][401] = {};
    std::vector<short> deg(n + 1);
    
    for (const auto &v : edges)
    {
        adj[v[0]][v[1]] = adj[v[1]][v[0]] = true;
        ++deg[v[0]];
        ++deg[v[1]];
    }
    
    int result = std::numeric_limits<int>::max();
    for (short i = 1; i != n + 1; ++i)
    {
        auto &ai = adj[i];
        int di = deg[i];
        for (short j = i + 1; j != n + 1; ++j)
        {
            if (!ai[j]) continue;
            auto &aj = adj[j];
            int dj = deg[j];
            for (short k = j + 1; k != n + 1; ++k)
            {
                if (!ai[k] || !aj[k]) continue;
                result = std::min(result, di + dj + deg[k] - 6);
            }
        }
    }
    
    return (result == std::numeric_limits<int>::max())?-1:result;
}
#else
int minTrioDegree(int n, std::vector<std::vector<int> > edges)
{
    std::bitset<401> g[401];
    for (const auto& e : edges)
    {
        g[e[0] - 1].set(e[1] - 1);
        g[e[1] - 1].set(e[0] - 1);
    }
    size_t result = std::numeric_limits<size_t>::max();
    for (int i = 0; i < n; ++i)
        for (int j = i + 1; j < n; ++j)
            if (g[i][j])
                for (int k = j + 1; k < n; ++k)
                    if (g[i][k] && g[j][k])
                        result = std::min(result, g[i].count() + g[j].count()
                                                + g[k].count() - 6);
    return (result == std::numeric_limits<size_t>::max())?-1:static_cast<int>(result);
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minTrioDegree(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, {{1, 2}, {1, 3}, {3, 2}, {4, 1}, {5, 2}, {3, 6}}, 3, trials);
    test(7, {{1, 3}, {4, 1}, {4, 3}, {2, 5}, {5, 6}, {6, 7}, {7, 5}, {2, 6}}, 0, trials);
    return 0;
}


