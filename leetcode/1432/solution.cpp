#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxDiff(int num)
{
    const std::string s = std::to_string(num);
    int firstNot9  = static_cast<int>(s.find_first_not_of('9'));
    int firstNot01 = static_cast<int>(s.find_first_not_of("01"));
    if (static_cast<size_t>(firstNot9)  == std::string::npos) firstNot9 = 0;
    if (static_cast<size_t>(firstNot01) == std::string::npos) firstNot01 = 0;
    
    std::string a = s, b = s;
    std::replace(a.begin(), a.end(), s[firstNot9], '9');
    std::replace(b.begin(), b.end(), s[firstNot01], (firstNot01 == 0)?'1':'0');
    return std::stoi(a) - std::stoi(b);
}

// ############################################################################
// ############################################################################

void test(int num, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxDiff(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(555, 888, trials);
    test(9, 8, trials);
    return 0;
}


