# Subtree of Another Tree

Given the roots of two binary trees `root` and `subRoot`, return `true` if there is a subtree of `root` with the same structure and node values of `subRoot` and `false` otherwise.

A subtree of a binary tree `tree` is a tree that consists of a node in `tree` and all of this node's descendants. The tree `tree` could also be considered as a subtree of itself.

#### Example 1:
> ```mermaid
> graph TD;
> subgraph SB [subRoot]
> A2((4))---B2((1))
> A2---C2((2))
> end
> subgraph SA [root]
> A1((3))---B1
> subgraph SSA [ ]
> B1((4))
> B1---D1((1))
> B1---E1((2))
> end
> A1---C1((5))
> end
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef white fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef black fill:#FFF,stroke:#000,stroke-width:2px;
> class SA,SB white;
> class SSA black;
> ```
> *Input:* `root = [3, 4, 5, 1, 2], subRoot = [4, 1, 2]`  
> *Output:* `true`

#### Example 2:
> ```mermaid
> graph TD;
> subgraph SB [subRoot]
> A2((4))---B2((1))
> A2---C2((2))
> end
> subgraph SA [root]
> A1((3))---B1((4))
> B1---D1((1))
> B1---E1((2))
> A1---C1((5))
> E1---G1((0))
> E1---EMPTY(( ))
> end
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef white fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef black fill:#FFF,stroke:#000,stroke-width:2px;
> class SA,SB,EMPTY white;
> linkStyle 7 stroke-width:0px;
> ```
> *Input:* `root = [3, 4, 5, 1, 2, null, null, null, null, 0], subRoot = [4, 1, 2]`  
> *Output:* `false`
 
#### Constraints:
- The number of nodes in the `root` tree is in the range `[1, 2000]`.
- The number of nodes in the `subRoot` tree is in the range `[1, 1000]`.
- `-10^4 <= root.val <= 10^4`
- `-10^4 <= subRoot.val <= 10^4`


