#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

bool isSubtree(TreeNode * root, TreeNode * subRoot)
{
    if (root)
    {
        auto check = [&](auto &&self, TreeNode * node, TreeNode * sub_node) -> bool
        {
            if (node && sub_node && (node->val == sub_node->val)
            && (self(self, node->left, sub_node->left)
            &&  self(self, node->right, sub_node->right)))
                return true;
            else return !node && !sub_node;
        };
        if ((root->val == subRoot->val) && check(check, root, subRoot))
            return true;
        return isSubtree(root->left, subRoot) || isSubtree(root->right, subRoot);
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          std::vector<int> subtree,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        TreeNode * subroot = vec2tree(subtree);
        result = isSubtree(root, subroot);
        delete root;
        delete subroot;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 4, 5, 1, 2}, {4, 1, 2}, true, trials);
    test({3, 4, 5, 1, 2, null, null, null, null, 0}, {4, 1, 2}, false, trials);
    test({3, 4, 5, 1, null, 2}, {3, 1, 2}, false, trials);
    return 0;
}


