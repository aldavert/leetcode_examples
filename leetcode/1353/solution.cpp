#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int maxEvents(std::vector<std::vector<int> > events)
{
    const int n = static_cast<int>(events.size());
    int result = 0, day = 0, idx = 0;
    std::priority_queue<int, std::vector<int>, std::greater<> > min_heap;
    
    std::sort(events.begin(), events.end());
    while (!min_heap.empty() || (idx < n))
    {
        if (min_heap.empty()) day = events[idx][0];
        while ((idx < n) && (events[idx][0] == day))
            min_heap.push(events[idx++][1]);
        min_heap.pop();
        ++result;
        ++day;
        while (!min_heap.empty() && min_heap.top() < day)
            min_heap.pop();
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > events, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxEvents(events);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 3}, {3, 4}}, 3, trials);
    test({{1, 2}, {2, 3}, {3, 4}, {1, 2}}, 4, trials);
    return 0;
}


