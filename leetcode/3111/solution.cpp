#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minRectanglesToCoverPoints(std::vector<std::vector<int> > points, int w)
{
    int result = 0;
    std::vector<int> x_coordinates;
    for (const auto &point : points)
        x_coordinates.push_back(point[0]);
    std::sort(x_coordinates.begin(), x_coordinates.end());
    for (int prev_x = -w - 1; int x : x_coordinates)
    {
        if (x > prev_x + w)
        {
            ++result;
            prev_x = x;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points,
          int w,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minRectanglesToCoverPoints(points, w);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 1}, {1, 0}, {1, 4}, {1, 8}, {3, 5}, {4, 6}}, 1, 2, trials);
    test({{0, 0}, {1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}, {6, 6}}, 2, 3, trials);
    test({{2, 3}, {1, 2}}, 0, 2, trials);
    return 0;
}


