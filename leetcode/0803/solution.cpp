#include "../common/common.hpp"
#include <fstream>
#include <queue>
#include <stack>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> hitBricks(std::vector<std::vector<int> > grid,
                           std::vector<std::vector<int> > hits)
{
    struct UnionFind
    {
        UnionFind(int n) : id(n), sz(n)
        {
            for (int i = 0; i < n; ++i) id[i] = i, sz[i] = 1;
        }
        void merge(int u, int v)
        {
            const int i = find(u);
            const int j = find(v);
            if (i == j) return;
            if (sz[i] < sz[j])
                sz[j] += sz[i],
                id[i] = j;
            else
                sz[i] += sz[j],
                id[j] = i;
        }
        inline int size(void) { return sz[find(0)]; }
    private:
        std::vector<int> id;
        std::vector<int> sz;
        inline int find(int u) { return (id[u] == u)?u:id[u] = find(id[u]); }
    };
    constexpr int directions[] = {0, 1, 0, -1, 0};
    const int m = static_cast<int>(grid.size());
    const int n = static_cast<int>(grid[0].size());
    UnionFind uf(m * n + 1);
    std::vector<int> result(hits.size());
    auto computeOffset = [&](int i, int j) -> int { return i * n + j + 1; };
    auto unionNeighbors = [&](int i, int j)
    {
        const int offset = computeOffset(i, j);
        for (int k = 0; k < 4; ++k)
        {
            const int x = i + directions[k];
            const int y = j + directions[k + 1];
            if ((x < 0) || (x == m) || (y < 0) || (y == n)) continue;
            if (grid[x][y] != 1) continue;
            uf.merge(offset, computeOffset(x, y));
        }
        if (i == 0) uf.merge(offset, 0);
    };
    for (const auto &hit : hits)
    {
        const int i = hit[0];
        const int j = hit[1];
        if (grid[i][j] == 1) grid[i][j] = 2;
    }
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (grid[i][j] == 1)
                unionNeighbors(i, j);
    int stable_size = uf.size();
    for (int i = static_cast<int>(hits.size()) - 1; i >= 0; --i)
    {
        const int x = hits[i][0], y = hits[i][1];
        if (grid[x][y] == 2)
        {
            grid[x][y] = 1;
            unionNeighbors(x, y);
            const int new_stable_size = uf.size();
            if (new_stable_size > stable_size)
                result[i] = new_stable_size - stable_size - 1;
            stable_size = new_stable_size;
        }
    }
    return result;
}
#else
std::vector<int> hitBricks(std::vector<std::vector<int> > grid,
                           std::vector<std::vector<int> > hits)
{
    const int h = static_cast<int>(grid.size());
    const int w = static_cast<int>(grid[0].size());
    std::vector<std::vector<int> > labels(h, std::vector<int>(w, -1));
    std::vector<std::vector<bool> > visited(h, std::vector<bool>(w, false));
    auto paint = [&](int y0, int x0, int label) -> bool
    {
        std::queue<std::tuple<int, int> > q;
        q.push({y0, x0});
        bool top_row = false;
        while (!q.empty())
        {
            auto [y, x] = q.front();
            q.pop();
            if (labels[y][x] == label) continue;
            labels[y][x] = label;
            top_row = top_row || (y == 0);
            if ((x     > 0) && grid[y][x - 1] && (labels[y][x - 1] != label))
                q.push({y, x - 1});
            if ((x + 1 < w) && grid[y][x + 1] && (labels[y][x + 1] != label))
                q.push({y, x + 1});
            if ((y     > 0) && grid[y - 1][x] && (labels[y - 1][x] != label))
                q.push({y - 1, x});
            if ((y + 1 < h) && grid[y + 1][x] && (labels[y + 1][x] != label))
                q.push({y + 1, x});
        }
        return top_row;
    };
    auto check = [&](int y0, int x0) -> bool
    {
        for (int y = 0; y < h; ++y)
            for (int x = 0; x < w; ++x)
                visited[y][x] = false;
        std::stack<std::tuple<int, int> > s;
        s.push({y0, x0});
        while (!s.empty())
        {
            auto [y, x] = s.top();
            s.pop();
            if (y == 0) return false;
            if (visited[y][x]) continue;
            visited[y][x] = true;
            if ((y     > 0) && grid[y - 1][x]) s.push({y - 1, x});
            if ((x     > 0) && grid[y][x - 1]) s.push({y, x - 1});
            if ((x + 1 < w) && grid[y][x + 1]) s.push({y, x + 1});
            if ((y + 1 < h) && grid[y + 1][x]) s.push({y + 1, x});
        }
        return true;
    };
    auto remove = [&](int y0, int x0) -> int
    {
        std::queue<std::tuple<int, int> > q;
        q.push({y0, x0});
        int counter = 0;
        while (!q.empty())
        {
            auto [y, x] = q.front();
            q.pop();
            if (!grid[y][x]) continue;
            labels[y][x] = -1;
            grid[y][x] = 0;
            ++counter;
            if ((x     > 0) && grid[y][x - 1]) q.push({y, x - 1});
            if ((x + 1 < w) && grid[y][x + 1]) q.push({y, x + 1});
            if ((y     > 0) && grid[y - 1][x]) q.push({y - 1, x});
            if ((y + 1 < h) && grid[y + 1][x]) q.push({y + 1, x});
        }
        return counter;
    };
    int number_of_regions = 0;
    for (int x = 0; x < w; ++x)
    {
        if (grid[0][x] && (labels[0][x] == -1))
            paint(0, x, number_of_regions++);
    }
    // Probably not needed, but the description doesn't guarantee that the
    // initial bricks are stable.
    for (int y = 0; y < h; ++y)
        for (int x = 0; x < w; ++x)
            if (grid[y][x] && (labels[y][x] == -1))
                grid[y][x] = 0;
    std::vector<int> result;
    for (const auto &hit : hits)
    {
        int y = hit[0], x = hit[1];
        if (grid[y][x])
        {
            int fall_bricks = 0;
            grid[y][x] = 0;
            if ((x > 0) && grid[y][x - 1] && check(y, x - 1))
                fall_bricks += remove(y, x - 1);
            if ((x + 1 < w) && grid[y][x + 1] && check(y, x + 1))
                fall_bricks += remove(y, x + 1);
            if ((y > 0) && grid[y - 1][x] && check(y - 1, x))
                fall_bricks += remove(y - 1, x);
            if ((y + 1 < h) && grid[y + 1][x] && check(y + 1, x))
                fall_bricks += remove(y + 1, x);
            result.push_back(fall_bricks);
        }
        else result.push_back(0);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<std::vector<int> > hits,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = hitBricks(grid, hits);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0, 0, 0},
          {1, 1, 1, 0}}, {{1, 0}}, {2}, trials);
    test({{1, 0, 0, 0},
          {1, 1, 0, 0}}, {{1, 1}, {1, 0}}, {0, 0}, trials);
    test({{1, 0, 0, 1},
          {1, 1, 1, 1}}, {{1, 0}, {1, 3}}, {0, 2}, trials);
    test({{1, 0, 1, 0, 1},
          {1, 0, 1, 0, 1},
          {1, 0, 1, 0, 1},
          {1, 1, 1, 1, 1}}, {{3, 2}, {0, 2}}, {0, 2}, trials);
    test({{1, 0, 1, 0, 1},
          {1, 0, 1, 0, 1},
          {1, 0, 1, 0, 1},
          {1, 1, 1, 1, 1}}, {{0, 0}, {0, 2}, {0, 4}}, {0, 0, 11}, trials);
    test({{1, 0, 1},
          {1, 1, 1}}, {{0, 0}, {0, 2}, {1, 1}}, {0, 3, 0}, trials);
    if (1)
    {
        int height, width, number_of_hits;
        std::ifstream file("data.txt");
        file >> height >> width;
        std::vector<std::vector<int> > grid(height, std::vector<int>(width));
        for (int h = 0; h < height; ++h)
        {
            std::string line;
            file >> line;
            for (int w = 0; w < width; ++w)
                grid[h][w] = line[w] == '1';
        }
        file >> number_of_hits;
        std::vector<std::vector<int> > hits(number_of_hits, std::vector<int>(2, 0));
        std::vector<int> solution(number_of_hits);
        for (int i = 0; i < number_of_hits; ++i)
            file >> hits[i][0] >> hits[i][1] >> solution[i];
        test(grid, hits, solution, trials);
    }
    return 0;
}


