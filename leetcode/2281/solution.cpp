#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

int totalStrength(std::vector<int> strength)
{
    constexpr int KMOD = 1'000'000'007;
    const int n = static_cast<int>(strength.size());
    std::vector<long> prefix(n), prefix_of_prefix(n + 1);
    std::vector<int> left(n, -1), right(n, n);
    std::stack<int> stack;
    
    for (int i = 0; i < n; ++i)
        prefix[i] = (i == 0)?strength[0]:((strength[i] + prefix[i - 1]) % KMOD);
    for (int i = 0; i < n; ++i)
        prefix_of_prefix[i + 1] = (prefix_of_prefix[i] + prefix[i]) % KMOD;
    
    for (int i = n - 1; i >= 0; --i)
    {
        while (!stack.empty() && (strength[stack.top()] >= strength[i]))
            left[stack.top()] = i,
            stack.pop();
        stack.push(i);
    }
    stack = {};
    for (int i = 0; i < n; ++i)
    {
        while (!stack.empty() && (strength[stack.top()] > strength[i]))
            right[stack.top()] = i,
            stack.pop();
        stack.push(i);
    }
    
    long result = 0;
    for (int i = 0; i < n; ++i)
        result = (result + strength[i]
               * ((prefix_of_prefix[right[i]] - prefix_of_prefix[i])
               * (i - left[i]) % KMOD
               - (prefix_of_prefix[i] - prefix_of_prefix[std::max(0, left[i])])
               * (right[i] - i) % KMOD + KMOD) % KMOD) % KMOD;
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> strength, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = totalStrength(strength);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 1, 2}, 44, trials);
    test({5, 4, 6}, 213, trials);
    return 0;
}


