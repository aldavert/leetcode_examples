#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

class RecentCounter
{
    std::queue<int> m_requests;
public:
    RecentCounter(void) = default;
    int ping(int t)
    {
        m_requests.push(t);
        while (m_requests.back() - m_requests.front() > 3'000)
            m_requests.pop();
        return static_cast<int>(m_requests.size());
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<int> pings, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        RecentCounter object;
        result.clear();
        for (int t : pings)
            result.push_back(object.ping(t));
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 100, 3001, 3002}, {1, 2, 3, 3}, trials);
    return 0;
}


