#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

#if 1
bool canIWin(int maxChoosableInteger, int desiredTotal)
{
    if (desiredTotal <= maxChoosableInteger)
        return true;
    int max_sum = maxChoosableInteger * (maxChoosableInteger + 1) / 2;
    if (max_sum < desiredTotal)
        return false;
    else if (max_sum == desiredTotal)
        return maxChoosableInteger % 2;
    auto solution = [](int _total, int _max) -> int
    {
        std::bitset<(1 << 21) + 1> win, lose;
        auto inner = [&win,&lose,&_max](auto &&self, int used, int total) -> bool
        {
            if (total <= 0) return false;
            if (win[used])  return true;
            if (lose[used]) return false;
            for (int i = 1; i <= _max; ++i)
            {
                if ((used >> i) & 1) continue;
                if (!self(self, used | (1 << i), total - i))
                    return win[used] = true;
            }
            return !(lose[used] = true);
        };
        return inner(inner, 0, _total);
    };
    return solution(desiredTotal, maxChoosableInteger);
}
#else
bool canIWin(int maxChoosableInteger, int desiredTotal)
{
    if (desiredTotal <= maxChoosableInteger)
        return true;
    if (maxChoosableInteger * (maxChoosableInteger + 1) / 2 < desiredTotal)
        return false;
    auto solution = [](int _total, int _max) -> int
    {
        std::vector<bool> lut((1 << (_max + 1)) + 1, false);
        auto inner = [&lut,&_max](auto &&self, int used, int total) -> bool
        {
            if (total <= 0)
                return false;
            if (lut[used]) return false;
            for (int i = 1; i <= _max; ++i)
            {
                if ((used >> i) & 1) continue;
                if (!self(self, used | (1 << i), total - i)) return true;
            }
            lut[used] = true;
            return false;
        };
        return inner(inner, 0, _total);
    };
    return solution(desiredTotal, maxChoosableInteger);
}
#endif

// ############################################################################
// ############################################################################

void test(int maxChoosableInteger,
          int desiredTotal,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canIWin(maxChoosableInteger, desiredTotal);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10,  11, false, trials);
    test(10,   0, true, trials);
    test(10,   1, true, trials);
    test(10, 110, false, trials);
    test(10, 300, false, trials);
    test(10,  97, false, trials);
    test( 3,  94, false, trials);
    test( 7, 210, false, trials);
    test( 5,   0,  true, trials);
    test( 5,   1,  true, trials);
    test( 5,   2,  true, trials);
    test( 5,   3,  true, trials);
    test( 5,   4,  true, trials);
    test( 5,   5,  true, trials);
    test( 5,   6, false, trials);
    test( 5,   7,  true, trials);
    test( 5,   8,  true, trials);
    test( 5,   9,  true, trials);
    test( 5,  10,  true, trials);
    test( 5,  11, false, trials);
    test( 5,  12,  true, trials);
    test( 5,  13,  true, trials);
    test( 5,  14,  true, trials);
    test( 5,  15,  true, trials);
    test( 5,  16, false, trials);
    test( 5,  17, false, trials);
    test( 4,   0,  true, trials);
    test( 4,   1,  true, trials);
    test( 4,   2,  true, trials);
    test( 4,   3,  true, trials);
    test( 4,   4,  true, trials);
    test( 4,   5, false, trials);
    test( 4,   6,  true, trials);
    test( 4,   7,  true, trials);
    test( 4,   8,  true, trials);
    test( 4,   9, false, trials);
    test( 4,  10, false, trials);
    test( 4,  11, false, trials);
    test(20, 152, false, trials);
    return 0;
}


