#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int deepestLeavesSum(TreeNode * root)
{
    int result = 0, max_depth = 0;
    auto process = [&](auto &&self, TreeNode * node, int depth) -> void
    {
        if (node == nullptr) return;
        if (depth > max_depth)
        {
            max_depth = depth;
            result = node->val;
        }
        else if (depth == max_depth)
            result += node->val;
        self(self, node->left, depth + 1);
        self(self, node->right, depth + 1);
    };
    process(process, root, 1);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = deepestLeavesSum(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, null, 6, 7, null, null, null, null, 8}, 15, trials);
    test({6, 7, 8, 2, 7, 1, 3, 9, null, 1, 4, null, null, null, 5}, 19, trials);
    return 0;
}


