#include "../common/common.hpp"
#include <unordered_map>
#include <map>

// ############################################################################
// ############################################################################

int numberOfGoodPaths(std::vector<int> vals, std::vector<std::vector<int> > edges)
{
    const int n = static_cast<int>(vals.size());
    int result = n;
    std::vector<std::vector<int> > graph(n);
    std::map<int, std::vector<int> > value_to_nodes;
    std::vector<int> node_id(n);
    std::vector<int> node_rank(n);
    auto find = [&](auto &&self, int u) -> int
    {
        return (node_id[u] == u)?u:node_id[u] = self(self, node_id[u]);
    };
    auto unionByRank = [&](int u, int v)
    {
        const int i = find(find, u);
        const int j = find(find, v);
        if (i == j) return;
        if      (node_rank[i] < node_rank[j]) node_id[i] = node_id[j];
        else if (node_rank[i] > node_rank[j]) node_id[j] = node_id[i];
        else
        {
            node_id[i] = node_id[j];
            ++node_rank[j];
        }
    };

    for (int i = 0; i < n; ++i)
    {
        node_id[i] = i;
        value_to_nodes[vals[i]].push_back(i);
    }
    for (const auto &edge : edges)
    {
        const int u = edge[0];
        const int v = edge[1];
        if (vals[v] <= vals[u]) graph[u].push_back(v);
        if (vals[u] <= vals[v]) graph[v].push_back(u);
    }
    for (const auto& [val, nodes] : value_to_nodes)
    {
        for (const int u : nodes)
            for (const int v : graph[u])
                unionByRank(u, v);
        std::unordered_map<int, int> root_count;
        for (const int u : nodes)
            ++root_count[find(find, u)];
        for (const auto& [_, count] : root_count)
            result += count * (count - 1) / 2;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> vals,
          std::vector<std::vector<int> > edges,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfGoodPaths(vals, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 1, 3}, {{0, 1}, {0, 2}, {2, 3}, {2, 4}}, 6, trials);
    test({1, 1, 2, 2, 3}, {{0, 1}, {1, 2}, {2, 3}, {2, 4}}, 7, trials);
    test({1}, {}, 1, trials);
    return 0;
}


