#include "../common/common.hpp"
#include <map>
#include <queue>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class MKAverage
{
    struct Map
    {
        std::map<int, int> map;
        int size = 0;
        long sum = 0;
    };
    const int m_m;
    const int m_k;
    const int m_mid_size;
    std::queue<int> m_queue;
    Map m_top;
    Map m_middle;
    Map m_bottom;
    
public:
    MKAverage(int m, int k) : m_m(m), m_k(k), m_mid_size(m - 2 * k) {}
    void addElement(int num)
    {
        m_queue.push(num);
        add(num);
        if (static_cast<int>(m_queue.size()) > m_m)
        {
            const int removed = m_queue.front();
            m_queue.pop();
            remove(removed);
        }
    }
    int calculateMKAverage()
    {
        return (static_cast<int>(m_queue.size()) == m_m)?
            static_cast<int>(m_middle.sum / m_mid_size):-1;
    }
private:
    void add(int num)
    {
        add(m_bottom, num);
        if (m_bottom.size > m_k)
            add(m_middle, remove(m_bottom, rbegin(m_bottom.map)->first));
        if (m_middle.size > m_mid_size)
            add(m_top, remove(m_middle, rbegin(m_middle.map)->first));
    }
    void remove(int num)
    {
        if      (m_bottom.map.count(num)) remove(m_bottom, num);
        else if (m_middle.map.count(num)) remove(m_middle, num);
        else                              remove(m_top   , num);
        if (m_bottom.size < m_k)
            add(m_bottom, remove(m_middle, begin(m_middle.map)->first));
        if (m_middle.size < m_mid_size)
            add(m_middle, remove(m_top, begin(m_top.map)->first));
    }
    void add(Map &obj, int num)
    {
        ++obj.map[num];
        ++obj.size;
        obj.sum += num;
    }
    int remove(Map &obj, int num)
    {
        if (--obj.map[num] == 0) obj.map.erase(num);
        --obj.size;
        obj.sum -= num;
        return num;
    }
};

// ############################################################################
// ############################################################################



void test(int m,
          int k,
          std::vector<bool> add,
          std::vector<int> input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    const int n = static_cast<int>(input.size());
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        MKAverage obj(m, k);
        result.clear();
        for (int i = 0; i < n; ++i)
        {
            if (add[i])
            {
                obj.addElement(input[i]);
                result.push_back(null);
            }
            else result.push_back(obj.calculateMKAverage());
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 1, {true, true, false, true, false, true, true, true, false},
         {3, 1, null, 10, null, 5, 5, 5, null},
         {null, null, -1, null, 3, null, null, null, 5}, trials);
    return 0;
}


