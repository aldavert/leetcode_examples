#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int matchPlayersAndTrainers(std::vector<int> players, std::vector<int> trainers)
{
    const int n = static_cast<int>(players.size());
    int result = 0;
    std::sort(players.begin(), players.end());
    std::sort(trainers.begin(), trainers.end());
    for (int i = 0, m = static_cast<int>(trainers.size()); i < m; ++i)
        if ((players[result] <= trainers[i]) &&  (++result == n))
            return result;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> players,
          std::vector<int> trainers,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = matchPlayersAndTrainers(players, trainers);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 7, 9}, {8, 2, 5, 8}, 2, trials);
    test({1, 1, 1}, {10}, 1, trials);
    return 0;
}


