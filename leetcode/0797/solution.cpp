#include "../common/common.hpp"
#include <set>

// ############################################################################
// ############################################################################

#if 1
std::vector<std::vector<int> >
allPathsSourceTarget(std::vector<std::vector<int> > graph)
{
    const int n = static_cast<int>(graph.size());
    std::vector<std::vector<int> > result;
    std::vector<int> trace;
    trace.reserve(n);
    trace.push_back(0);
    auto search = [&](auto &&self, int position) -> void
    {
        for (int idx : graph[position])
        {
            trace.push_back(idx);
            if (idx == n - 1)
                result.push_back(trace);
            else 
                self(self, idx);
            trace.pop_back();
        }
    };
    search(search, 0);
    return result;
}
#else
std::vector<std::vector<int> >
allPathsSourceTarget(std::vector<std::vector<int> > graph)
{
    const int n = static_cast<int>(graph.size());
    std::vector<std::vector<int> > result;
    std::vector<bool> visited(n, false);
    std::vector<int> trace;
    visited[0] = true;
    trace.reserve(n);
    trace.push_back(0);
    
    auto search = [&](auto &&self, int position) -> void
    {
        for (int idx : graph[position])
        {
            if (idx == n - 1)
            {
                trace.push_back(idx);
                result.push_back(trace);
                trace.pop_back();
            }
            else if (!visited[idx])
            {
                visited[idx] = true;
                trace.push_back(idx);
                self(self, idx);
                trace.pop_back();
                visited[idx] = false;
            }
        }
    };
    search(search, 0);
    return result;
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    std::set<std::vector<int> > unique;
    for (const auto &vec : left)
        unique.insert(vec);
    for (const auto &vec : right)
    {
        if (auto it = unique.find(vec); it != unique.end())
            unique.erase(it);
        else return false;
    }
    return unique.empty();
}

void test(std::vector<std::vector<int> > graph,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = allPathsSourceTarget(graph);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {3}, {3}, {}}, {{0, 1, 3}, {0, 2, 3}}, trials);
    test({{4, 3, 1}, {3, 2, 4}, {3}, {4}, {}},
         {{0, 4}, {0, 3, 4}, {0, 1, 3, 4}, {0, 1, 2, 3, 4}, {0, 1, 4}}, trials);
    test({{1}, {}}, {{0, 1}}, trials);
    test({{1, 2, 3}, {2}, {3}, {}}, {{0, 1, 2, 3}, {0, 2, 3}, {0, 3}}, trials);
    test({{1, 3}, {2}, {3}, {}}, {{0, 1, 2, 3}, {0, 3}}, trials);
    return 0;
}


