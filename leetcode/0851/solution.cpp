#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> loudAndRich(std::vector<std::vector<int> > richer,
                             std::vector<int> quiet)
{
    auto process = [&](void) -> std::vector<int>
    {
        const int n = static_cast<int>(quiet.size());
        std::vector<std::vector<int> > graph(n);
        std::vector<int> result(n, -1);
        
        auto inner = [&](auto &&self, int u) -> int
        {
            if (result[u] != -1) return result[u];
            result[u] = u;
            for (int v : graph[u])
                if (int res = self(self, v); quiet[res] < quiet[result[u]])
                    result[u] = res;
            return result[u];
        };
        for (const auto &r : richer)
            graph[r[1]].push_back(r[0]);
        for (int i = 0; i < n; ++i)
            inner(inner, i);
        return result;
    };
    return process();
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > richer,
          std::vector<int> quiet,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = loudAndRich(richer, quiet);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0}, {2, 1}, {3, 1}, {3, 7}, {4, 3}, {5, 3}, {6, 3}},
         {3, 2, 5, 4, 6, 1, 7, 0}, {5, 5, 2, 5, 4, 5, 6, 7}, trials);
    test({}, {0}, {0}, trials);
    return 0;
}


