#include "../common/common.hpp"
#include <unordered_map>
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
long long interchangeableRectangles(std::vector<std::vector<int> > rectangles)
{
    std::unordered_map<double, int> nums;
    long result = 0;
    for (const auto &v : rectangles) result += nums[double(v[0])/ v[1]]++;
    return result;
}
#else
long long interchangeableRectangles(std::vector<std::vector<int> > rectangles)
{
    struct PairHash
    {
        size_t operator()(const std::pair<int, int> &p) const
        {
            return p.first ^ p.second;
        }
    };
    std::unordered_map<std::pair<int, int>, int, PairHash> ratio_count;
    long result = 0;
    
    for (const auto &rectangle : rectangles)
    {
        int d = std::gcd(rectangle[0], rectangle[1]);
        ++ratio_count[{rectangle[0] / d, rectangle[1] / d}];
    }
    for (const auto& [_, count] : ratio_count)
        result += static_cast<long>(count) * (count - 1) / 2;
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > rectangles,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = interchangeableRectangles(rectangles);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{4, 8}, {3, 6}, {10, 20}, {15, 30}}, 6, trials);
    test({{4, 5}, {7, 8}}, 0, trials);
    return 0;
}


