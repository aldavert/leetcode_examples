#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

std::vector<long long> kthPalindrome(std::vector<int> queries, int intLength)
{
    const int start = (int)std::pow(10, (intLength + 1) / 2 - 1),
              end = start * 10,
              mul = (int)std::pow(10, intLength / 2);
    std::vector<long long> result;
    
    for (bool odd = intLength & 1; int query : queries)
    {
        if (long long prefix = start + query - 1; prefix >= end)
            result.push_back(-1);
        else
        {
            long long num = prefix / (1 + 9 * odd), current = 0;
            for (; num; current = current * 10 + num % 10, num /= 10);
            result.push_back(prefix * mul + current);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> queries,
          int intLength,
          std::vector<long long> solution,
          unsigned int trials = 1)
{
    std::vector<long long> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = kthPalindrome(queries, intLength);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 90}, 3, {101, 111, 121, 131, 141, 999}, trials);
    test({2, 4, 6}, 4, {1111, 1331, 1551}, trials);
    return 0;
}


