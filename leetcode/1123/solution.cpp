#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * lcaDeepestLeaves(TreeNode * root)
{
    auto process = [&](void) -> TreeNode *
    {
        struct Data
        {
            TreeNode * lca = nullptr;
            int depth = 0;
        };
        auto inner = [](auto &&self, TreeNode * node) -> Data
        {
            if (node == nullptr)
            return {nullptr, 0};
            Data left  = self(self, node->left),
                 right = self(self, node->right);
            if (left.depth > right.depth)
                return { left.lca, left.depth + 1 };
            if (left.depth < right.depth)
                return { right.lca, right.depth + 1 };
            return {node, left.depth + 1};
        };
        return inner(inner, root).lca;
    };
    return process();
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = tree2vec(lcaDeepestLeaves(root));
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 5, 1, 6, 2, 0, 8, null, null, 7, 4}, {2, 7, 4}, trials);
    test({1}, {1}, trials);
    test({0, 1, 3, null, 2}, {2}, trials);
    return 0;
}


