#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int largestInteger(int num)
{
    int histogram[10] = {}, result = 0, digit[16] = {}, n = 0;
    for (int work = num; work; work /= 10) ++histogram[digit[n++] = work % 10];
    for (int odd = 9, even = 8; n;)
    {
        int value = (digit[--n] & 1)?odd:even;
        while (!histogram[value]) value -= 2;
        result  = result * 10 + value;
        --histogram[value];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int num, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestInteger(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1234, 3412, trials);
    test(65875, 87655, trials);
    return 0;
}


