#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<std::string> >
suggestedProducts(std::vector<std::string> products, std::string searchWord)
{
    std::vector<std::vector<std::string> > result;
    
    result.reserve(searchWord.size());
    std::sort(products.begin(), products.end());
    
    const int n_c = static_cast<int>(searchWord.size());
    const int n_p = static_cast<int>(products.size());
    int begin = 0, end = n_p - 1;
    for (int i = 0; i < n_c; ++i)
    {
        std::vector<std::string> current;
        for (; (begin < n_p)
            && !((i < static_cast<int>(products[begin].size()))
            && (products[begin][i] == searchWord[i]));
             ++begin);
        for (; (end >= begin)
            && !((i < static_cast<int>(products[end].size()))
            && (products[end][i] == searchWord[i]));
             --end);
        for (int j = begin, k = 0; (j <= end) && (k < 3); ++j, ++k)
            current.push_back(products[j]);
        result.push_back(current);
    }
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<std::string> > &first,
                const std::vector<std::vector<std::string> > &second)
{
    if (first.size() != second.size()) return false;
    const int n = static_cast<int>(first.size());
    for (int i = 0; i < n; ++i)
    {
        if (first[i].size() != second[i].size()) return false;
        const int nr = static_cast<int>(first[i].size());
        for (int j = 0; j < nr; ++j)
        {
            if (first[i][j] != second[i][j]) return false;
        }
    }
    return true;
}

void test(std::vector<std::string> products,
          std::string searchWord,
          std::vector<std::vector<std::string> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<std::string> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = suggestedProducts(products, searchWord);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"mobile", "mouse", "moneypot", "monitor", "mousepad"}, "mouse",
         {{"mobile", "moneypot", "monitor"},
          {"mobile", "moneypot", "monitor"},
          {"mouse", "mousepad"},
          {"mouse", "mousepad"},
          {"mouse", "mousepad"}}, trials);
    test({"havana"}, "havana",
         {{"havana"}, {"havana"}, {"havana"}, {"havana"}, {"havana"}, {"havana"}},
          trials);
    test({"bags", "baggage", "banner", "box", "cloths"}, "bags",
         {{"baggage", "bags", "banner"}, {"baggage", "bags", "banner"},
          {"baggage", "bags"}, {"bags"}}, trials);
    test({"havana"}, "tatiana", {{}, {}, {}, {}, {}, {}, {}}, trials);
    std::vector<std::string> productsA = {
        "oqjrtcokaagakchrwrdbrlpnqivwcpzwuxbdkpkhndevouwyrtmbokxolhbvrencthmyplqi"
        "xnhnokbhtbstmslfbinsypubqjckiqujvmknxuomdwqkfudgiqmpzkvnshrmnoeonzyfaipd"
        "cfdwhekrazfkdlfluyvoevahsyhvqjfcizxjhucbpqdjgmqqalqnvfyrtfkyrxlavfbagxkd"
        "loetaiedqbrmtzxwwrpyewrcbntsnrdzurzlfokcxolkltouozdobqvtoldjuincumspqpuj"
        "ynxxetbflfnkrscxgrvennmczurjvnoalxcfcrnddfckavedjrvewmawxazviumzpudgolof"
        "sxllhgsdrnvjbipcsqsdoosfdkhavhicsfbyavwyxziefmycknhzqujqkbqhacuaebwqpkfn"
        "kovxchczohhirczixikuoktkaamchcghynclonujiyzkcghjjjtzckjxmpssfqjirnfvrddv"
        "oqvdeteegupzevydcywjsoybsflpgpzkcoztcayffscwxkofwibguysdjtmddgevjhbxrfiq"
        "liqyiczdunhosdctrvlagcugpsskpvlsdhcbpdtdwjbhchymlptjrmzmezwljxemgzecptlu"
        "xbwtgoyvqwrvxkartgrebdegyqdibemnficwpgsywlzczvwujmykkyhkkxhvrtmjxcyidawf"
        "xjcagsgutndplixpepddiosflfoujxndtgxsbzarlwacjtydorlrtukbyibaktiphojmrcbw"
        "jppnuwwihuerqswjpmyblrdslnwpdnzovpslsrfbzhjywbimwdiqhxmdssjovufgbqqykozf"
        "jbylihfwanzjdtgeoimwowocykeskvivwuybdrowolstbrbenioagkgwednhymqisdvjuycj"
        "vtrurzqssxrisozysizxeoyelhglnppsjzggjunmyabzptlfktzphlmhvpoqlnrwgxauepum"
        "ssyecjpwfozhxhvodsasomhtgbasijrpphbijnxcyrajypogighdxacjxqyxagfuenwrdajq"
        "suzrjtrqjebjnbmdzbqdgmbuqofbyegnxvgsegbaqxsdjjffdxiqgqtlgclqtmjqfmbrsccn"
        "oidkugbwscamrqfqmpbfuvvoxrodoccusixfehzetsbiflonazsmacvwilarpgpwjkexykqk"
        "mklqcuhdrhhgrzofbsjvzwdaghqxajgwqmuhyyervscyiyggbqyhpvotnmaubapigegatgtq"
        "eazgcytubsuhjzipbyxnkyqveeetgecxwinwdjuawpzdieizlzqplzajafeernecfkpiilnu"
        "qdjnipjybqishinomemdxfwabhemxecuyailbogehxwwtzvaueonxkjmdvojpjjxaftvnbbs"
        "fcjjyzwartrbmxgktbhrqeynwoziddoh",
        "kfnyprlqjosvbkvfvanhqsxpfezfafuzxchbwsdagvgtmtwdjsgnodkahyfkpyfmrbrcinei"
        "chemlomrgaydmjnzbqxhzpciwkwbcsrpeafwkitjibviipnhvcqntjuwnccylnqdwxmirvng"
        "gcdwfbldnhxsjjhfqcxlggcpldamlyttzibadxzcpwecjjxekeoucanprljnsgvhpxjydazz"
        "lxofypaxcnwhhkhrtwbecouqvoxnzsitdzyrxokhdtynjgygqarnhyaqsdrgdqdmpfwdhlay"
        "ltvrjalfxxctwgqtmuwgdwzxyuiictaul",
        "kfnwqytfhroyewirkaofdwdjtzyaeamrqkbdzganjfqfykfwizgtxjhrbvnmbwyfivwrbbjr"
        "alxuptonwhlvztgefnggltqekduxdhznowjsayyaieacjvewhfookivjuwmwmhlobrgummkv"
        "jwclbipmkwrpbwqlbthbokenmcaupqbtuglgqtpuognhfachuscdsztjbuffgdzajlzhmnmk"
        "sdnyrhahnnhilwnqohxyjsiatzshinquevymllusyqzkjwovmgtwwfztpbcpvyjdkqcdkscp"
        "imchxdjdrxvropgbgrrccfqnatlqjlojfczeggnanpuqtebjcyezdqpxsfkegxqfeplcmfxy"
        "qmlruapeywuftbbosmpegfpcepukcsvpgmfundbvbzazwqnefmbnnxntixtormwfbnuwheah"
        "zddwksodogedymeujvxacdbguthxhlcmmblfwjrnurpfuawoghyakqkccxcgxzdraasdivjg"
        "vtjrulhgktdxjuzpdyewckrpmjrttusqubhwntpchscprcdddsbyxrqrnplgazwjvojrtxog"
        "qdvqarcspvkfljlswpktqxlndidtnhzryohyyujfncunynxkblsdpjlaifdzsjxpuapwjzwz"
        "eahrnfkgtaehwjqsligfjnmsivinygzoeaejagsvzkyicbsyfxhqaotexdsvtxuexnmqsjhm"
        "ojexevbgozlkbmuuexgpklnsetscrceuvtfhrxwvloiucqbzpxwtodopesljmisfxqmxhjle"
        "hpyaodfwqbnfvjtplauiyiyrddbjdpkvznmvpbzxgemfhgpwligjwznbztyaenziwooceebf"
        "wppfpgmzczoenqtmqcjdgvdlhjpwwlqcyjmaqnjicyrocrvxxrnrhpsqvrndurviilufvwbs"
        "swtjlmefydjechqrgeqjucsjyaaudocxgpvtbfekgynmnywltxvqddtjdaunvfqjclrjsohf"
        "enjxqjpvcklfdjoaavgoukzqjijycwqipkzpcvaqzqwgwnolrqudrcqqymlhelskpvrlobsj"
        "xlgsunkdlvqdgpnnrhcgdxasbdmrcrauswiticdiwkhdvpkduqgqpujcyjkrksmhjswvqccp"
        "xfacszfdsdvoxjnlphqawjlxryprgoexjqxryuxadviwdebxjyzolpqerfkxhklgpeffahcl"
        "nfyfnwudwnjjqbcazsuzekwfsprbiwztqsxtxpiemfucwgcamdowvoymuwkstyqggmtknzez"
        "acoklmiaanvsttqknedeoayakomzzzntfmyfaunsnvimvkboqcfprizsofhauflvmetrwfoi"
        "gjcxjavspnaqwpagfwslxolbjgglknrcacqntfcizfkmcrdbdidvojdritvnvgnsvrhjcuto"
        "jfvjaspzdodnisyuirklwcxjitakdpxaclthbxgooxmqslftctxopfencxtzktckpkpkhlkb"
        "oueylshbztlvkbtkpjdcxakldxmnjnqjyscgvydlmnpfnaxaicylivtesvtoqimovynmntii"
        "jxnwinlirbmiubpwlmwdscynsywgswsklxaxjwoculminicuphgtepjxmlhaolzzxuqqawjm"
        "vlniknniwexrelrvxagbtcqnzhdmiqfkyvwnsfuzskzlcvcyluzjtesuvzdknipreueyqgzv"
        "gbeqtmcnzwtfdgmihuzwtsdxahawfiwnpzzwpnzawfzyobyriuxbnlojvkfycwprgngluhgy"
        "irhutknuvdyebwrmaarbelkhzoqpilrneitzzuysmlpczdepdngeuwcpylcysafnmzulcrzl"
        "kaskbiexjikebbrwlbcpgvmiumsafuzqcuxnpwtjgqmlmnbeauljmyrxykugovjfazsrkzsy"
        "onecfejokeiwtxvdjawbkgvlqegxcoxwhldzoblzvhydopqpatwxllccdnlmxjypjiimtczq"
        "hubuloozyiwjmcsooqvufuamaopwxsexaaohazzctezrqahkdsnkqspkjqcvocnqirwzxocb"
        "xibrwrlrhzcuxlfqsgwoqyagkmcrdedasvrzjgugbrvvbsjtmjfbdxhchbkfomefrfrwdiyh"
        "hskrqstnnwrclsphqfrbwjvlitjmwtlmx",
        "kfnyprlqjosvbkvfvanhqsxpfezfafuzxchbwsdagvgtmtwdjsgnodkahyfkpyfmrbrcinei"
        "chemlomrgaydmjnzbqxhzpciwkwbcsrpeafwkitjibviipnhvcqntjuwnccylnqdwxmirvng"
        "gcdwfbldnhxsjjhfqcxlggcpldamlyttzibadxzcpwecjjxekeoucanprljnsgvhpxjydazz"
        "lxofypaxcnwhhkhrtwbecouqvoxnzsitdzyuoyzfkyseiohccpdtnjhqlrkgpcifvatradjf"
        "urxmwfssmbpbvxeoialjeyxujpgqdunhrthidhizzqddvuqzmoenmjzunulkrjyxfugrpvkw"
        "oiwyxwgrweakhbswllbyziranhxkleggegegdailjgyteaghdqnjqdjfhyrapqmckvxgxmas"
        "nweej"};

    std::string searchWordA =
        "kfnyprlqjosvbkvfvanhqsxpfezfafuzxchbwsdagvgtmtwdjsgnodkahyfkpyfmrbrcinei"
        "chemlomrgaydmjnzbqxhzpciwkwbcsrpeafwkitjibviipnhvcqntjuwnccylnqdwxmirvng"
        "gcdwfbldnhxsjjhfqcxlggcpldamlyttzibadxzcpwecjjxekeoucanprljnsgvhpxjydazz"
        "lxofypaxcnwhhkhrtwbecouqvoxnzsitdzyuoyzfkyseiohccpdtnjhqlrkgpcifvatradjf"
        "urxmwfssmbpbvxeoialjeyxujpgqdunhrthidhizzqddvuqzmoyrnqunojmtporeofgldjnt"
        "qvlngobvtpbhmmdrkosxlkvmivonldjr";

    std::vector<std::vector<std::string> > resultA;
    for (int i = 0; i < 3; ++i)
        resultA.push_back({
                "kfnwqytfhroyewirkaofdwdjtzyaeamrqkbdzganjfqfykfwizgtxjhr"
                "bvnmbwyfivwrbbjralxuptonwhlvztgefnggltqekduxdhznowjsayya"
                "ieacjvewhfookivjuwmwmhlobrgummkvjwclbipmkwrpbwqlbthboken"
                "mcaupqbtuglgqtpuognhfachuscdsztjbuffgdzajlzhmnmksdnyrhah"
                "nnhilwnqohxyjsiatzshinquevymllusyqzkjwovmgtwwfztpbcpvyjd"
                "kqcdkscpimchxdjdrxvropgbgrrccfqnatlqjlojfczeggnanpuqtebj"
                "cyezdqpxsfkegxqfeplcmfxyqmlruapeywuftbbosmpegfpcepukcsvp"
                "gmfundbvbzazwqnefmbnnxntixtormwfbnuwheahzddwksodogedymeu"
                "jvxacdbguthxhlcmmblfwjrnurpfuawoghyakqkccxcgxzdraasdivjg"
                "vtjrulhgktdxjuzpdyewckrpmjrttusqubhwntpchscprcdddsbyxrqr"
                "nplgazwjvojrtxogqdvqarcspvkfljlswpktqxlndidtnhzryohyyujf"
                "ncunynxkblsdpjlaifdzsjxpuapwjzwzeahrnfkgtaehwjqsligfjnms"
                "ivinygzoeaejagsvzkyicbsyfxhqaotexdsvtxuexnmqsjhmojexevbg"
                "ozlkbmuuexgpklnsetscrceuvtfhrxwvloiucqbzpxwtodopesljmisf"
                "xqmxhjlehpyaodfwqbnfvjtplauiyiyrddbjdpkvznmvpbzxgemfhgpw"
                "ligjwznbztyaenziwooceebfwppfpgmzczoenqtmqcjdgvdlhjpwwlqc"
                "yjmaqnjicyrocrvxxrnrhpsqvrndurviilufvwbsswtjlmefydjechqr"
                "geqjucsjyaaudocxgpvtbfekgynmnywltxvqddtjdaunvfqjclrjsohf"
                "enjxqjpvcklfdjoaavgoukzqjijycwqipkzpcvaqzqwgwnolrqudrcqq"
                "ymlhelskpvrlobsjxlgsunkdlvqdgpnnrhcgdxasbdmrcrauswiticdi"
                "wkhdvpkduqgqpujcyjkrksmhjswvqccpxfacszfdsdvoxjnlphqawjlx"
                "ryprgoexjqxryuxadviwdebxjyzolpqerfkxhklgpeffahclnfyfnwud"
                "wnjjqbcazsuzekwfsprbiwztqsxtxpiemfucwgcamdowvoymuwkstyqg"
                "gmtknzezacoklmiaanvsttqknedeoayakomzzzntfmyfaunsnvimvkbo"
                "qcfprizsofhauflvmetrwfoigjcxjavspnaqwpagfwslxolbjgglknrc"
                "acqntfcizfkmcrdbdidvojdritvnvgnsvrhjcutojfvjaspzdodnisyu"
                "irklwcxjitakdpxaclthbxgooxmqslftctxopfencxtzktckpkpkhlkb"
                "oueylshbztlvkbtkpjdcxakldxmnjnqjyscgvydlmnpfnaxaicylivte"
                "svtoqimovynmntiijxnwinlirbmiubpwlmwdscynsywgswsklxaxjwoc"
                "ulminicuphgtepjxmlhaolzzxuqqawjmvlniknniwexrelrvxagbtcqn"
                "zhdmiqfkyvwnsfuzskzlcvcyluzjtesuvzdknipreueyqgzvgbeqtmcn"
                "zwtfdgmihuzwtsdxahawfiwnpzzwpnzawfzyobyriuxbnlojvkfycwpr"
                "gngluhgyirhutknuvdyebwrmaarbelkhzoqpilrneitzzuysmlpczdep"
                "dngeuwcpylcysafnmzulcrzlkaskbiexjikebbrwlbcpgvmiumsafuzq"
                "cuxnpwtjgqmlmnbeauljmyrxykugovjfazsrkzsyonecfejokeiwtxvd"
                "jawbkgvlqegxcoxwhldzoblzvhydopqpatwxllccdnlmxjypjiimtczq"
                "hubuloozyiwjmcsooqvufuamaopwxsexaaohazzctezrqahkdsnkqspk"
                "jqcvocnqirwzxocbxibrwrlrhzcuxlfqsgwoqyagkmcrdedasvrzjgug"
                "brvvbsjtmjfbdxhchbkfomefrfrwdiyhhskrqstnnwrclsphqfrbwjvl"
                "itjmwtlmx",
                "kfnyprlqjosvbkvfvanhqsxpfezfafuzxchbwsdagvgtmtwdjsgnodka"
                "hyfkpyfmrbrcineichemlomrgaydmjnzbqxhzpciwkwbcsrpeafwkitj"
                "ibviipnhvcqntjuwnccylnqdwxmirvnggcdwfbldnhxsjjhfqcxlggcp"
                "ldamlyttzibadxzcpwecjjxekeoucanprljnsgvhpxjydazzlxofypax"
                "cnwhhkhrtwbecouqvoxnzsitdzyrxokhdtynjgygqarnhyaqsdrgdqdm"
                "pfwdhlayltvrjalfxxctwgqtmuwgdwzxyuiictaul",
                "kfnyprlqjosvbkvfvanhqsxpfezfafuzxchbwsdagvgtmtwdjsgnodka"
                "hyfkpyfmrbrcineichemlomrgaydmjnzbqxhzpciwkwbcsrpeafwkitj"
                "ibviipnhvcqntjuwnccylnqdwxmirvnggcdwfbldnhxsjjhfqcxlggcp"
                "ldamlyttzibadxzcpwecjjxekeoucanprljnsgvhpxjydazzlxofypax"
                "cnwhhkhrtwbecouqvoxnzsitdzyuoyzfkyseiohccpdtnjhqlrkgpcif"
                "vatradjfurxmwfssmbpbvxeoialjeyxujpgqdunhrthidhizzqddvuqz"
                "moenmjzunulkrjyxfugrpvkwoiwyxwgrweakhbswllbyziranhxklegg"
                "egegdailjgyteaghdqnjqdjfhyrapqmckvxgxmasnweej"});
    for (int i = 3; i < 251; ++i)
        resultA.push_back({
                "kfnyprlqjosvbkvfvanhqsxpfezfafuzxchbwsdagvgtmtwdjsgnodka"
                "hyfkpyfmrbrcineichemlomrgaydmjnzbqxhzpciwkwbcsrpeafwkitj"
                "ibviipnhvcqntjuwnccylnqdwxmirvnggcdwfbldnhxsjjhfqcxlggcp"
                "ldamlyttzibadxzcpwecjjxekeoucanprljnsgvhpxjydazzlxofypax"
                "cnwhhkhrtwbecouqvoxnzsitdzyrxokhdtynjgygqarnhyaqsdrgdqdm"
                "pfwdhlayltvrjalfxxctwgqtmuwgdwzxyuiictaul",
                "kfnyprlqjosvbkvfvanhqsxpfezfafuzxchbwsdagvgtmtwdjsgnodka"
                "hyfkpyfmrbrcineichemlomrgaydmjnzbqxhzpciwkwbcsrpeafwkitj"
                "ibviipnhvcqntjuwnccylnqdwxmirvnggcdwfbldnhxsjjhfqcxlggcp"
                "ldamlyttzibadxzcpwecjjxekeoucanprljnsgvhpxjydazzlxofypax"
                "cnwhhkhrtwbecouqvoxnzsitdzyuoyzfkyseiohccpdtnjhqlrkgpcif"
                "vatradjfurxmwfssmbpbvxeoialjeyxujpgqdunhrthidhizzqddvuqz"
                "moenmjzunulkrjyxfugrpvkwoiwyxwgrweakhbswllbyziranhxklegg"
                "egegdailjgyteaghdqnjqdjfhyrapqmckvxgxmasnweej"});
    for (int i = 251; i < 338; ++i)
        resultA.push_back({
                "kfnyprlqjosvbkvfvanhqsxpfezfafuzxchbwsdagvgtmtwdjsgnodka"
                "hyfkpyfmrbrcineichemlomrgaydmjnzbqxhzpciwkwbcsrpeafwkitj"
                "ibviipnhvcqntjuwnccylnqdwxmirvnggcdwfbldnhxsjjhfqcxlggcp"
                "ldamlyttzibadxzcpwecjjxekeoucanprljnsgvhpxjydazzlxofypax"
                "cnwhhkhrtwbecouqvoxnzsitdzyuoyzfkyseiohccpdtnjhqlrkgpcif"
                "vatradjfurxmwfssmbpbvxeoialjeyxujpgqdunhrthidhizzqddvuqz"
                "moenmjzunulkrjyxfugrpvkwoiwyxwgrweakhbswllbyziranhxklegg"
                "egegdailjgyteaghdqnjqdjfhyrapqmckvxgxmasnweej"});
    for (int i = 338; i < 392; ++i)
        resultA.push_back({});
    test(productsA, searchWordA, resultA, trials);
    return 0;
}


