#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int maxSubArray(std::vector<int> nums)
{
    int result = nums[0], sum = nums[0];
    for (size_t i = 1, n = nums.size(); i < n; ++i)
        result = std::max(result, sum = nums[i] + (sum >= 0) * sum);
    return result;
}
#else
int maxSubArray(std::vector<int> nums)
{
    int result = -1'000'000;
    int current_sum = -1'000'000;
    for (int v : nums)
    {
        current_sum = std::max(v, current_sum + v);
        result = std::max(result, current_sum);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSubArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-2, 1, -3, 4, -1, 2, 1, -5, 4}, 6, trials);
    test({1}, 1, trials);
    test({5, 4, -1, 7, 8}, 23, trials);
    return 0;
}


