#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int minimumAverageDifference(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    long total = std::accumulate(nums.begin(), nums.end(), 0l);
    long accumulate = 0;
    long minimum_value = std::numeric_limits<long>::max();
    int minimum_position = -1;
    for (int i = 0; i < n - 1; ++i)
    {
        accumulate += nums[i];
        total -= nums[i];
        long current_value = std::abs(accumulate / (i + 1) - total / (n - i - 1));
        if (current_value < minimum_value)
        {
            minimum_value = current_value;
            minimum_position = i;
        }
    }
    accumulate = (accumulate + nums[n - 1]) / n;
    return (minimum_value <= accumulate)?minimum_position:n - 1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumAverageDifference(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 5, 3, 9, 5, 3}, 3, trials);
    test({0}, 0, trials);
    test({0, 0, 0, 0}, 0, trials);
    test({5, 4, 3, 2, 1}, 1, trials);
    return 0;
}


