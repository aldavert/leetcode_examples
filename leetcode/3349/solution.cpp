#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

bool hasIncreasingSubarrays(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    for (int i = 1, inc = 1, prev = 0; i < n; ++i)
    {
        if (nums[i] > nums[i - 1]) ++inc;
        else prev = std::exchange(inc, 1);
        if ((inc / 2 >= k) || (std::min(prev, inc) >= k))
            return true;
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = hasIncreasingSubarrays(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 5, 7, 8, 9, 2, 3, 4, 3, 1}, 3, true, trials);
    test({1, 2, 3, 4, 4, 4, 4, 5, 6, 7}, 5, false, trials);
    return 0;
}


