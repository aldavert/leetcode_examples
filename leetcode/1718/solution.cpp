#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> constructDistancedSequence(int n)
{
    auto process = [&]() -> std::vector<int>
    {
        const int m = 2 * n - 1;
        std::vector<int> result(m);
        auto inner = [&](auto &&self, int i, int mask) -> bool
        {
            if (i == m) return true;
            if (result[i] > 0)
                return self(self, i + 1, mask);
            for (int num = n; num >= 1; --num)
            {
                if (mask >> num & 1) continue;
                if (num == 1)
                {
                    result[i] = num;
                    if (self(self, i + 1, mask | 1 << num))
                        return true;
                    result[i] = 0;
                }
                else
                {
                    if ((i + num >= m) || (result[i + num] > 0))
                        continue;
                    result[i] = num;
                    result[i + num] = num;
                    if (self(self, i + 1, mask | 1 << num))
                        return true;
                    result[i + num] = 0;
                    result[i] = 0;
                }
            }
            return false;
        };
        inner(inner, 0, 0);
        return result;
    };
    return process();
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = constructDistancedSequence(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {3, 1, 2, 3, 2}, trials);
    test(5, {5, 3, 1, 4, 3, 5, 2, 4, 2}, trials);
    test(10, {10, 8, 6, 9, 3, 1, 7, 3, 6, 8, 10, 5, 9, 7, 4, 2, 5, 2, 4}, trials);
    test(15, {15, 13, 14, 10, 8, 12, 5, 3, 11, 9, 3, 5, 8, 10, 13, 15, 14, 12, 9,
              11, 7, 4, 6, 1, 2, 4, 2, 7, 6}, trials);
    test(20, {20, 18, 19, 15, 13, 17, 10, 16, 7, 5, 3, 14, 12, 3, 5, 7, 10, 13,
              15, 18, 20, 19, 17, 16, 12, 14, 11, 9, 4, 6, 8, 2, 4, 2, 1, 6, 9,
              11, 8}, trials);
    test(1, {1}, trials);
    test(2, {2, 1, 2}, trials);
    test(13, {13, 11, 12, 8, 6, 4, 9, 10, 1, 4, 6, 8, 11, 13, 12, 9, 7, 10, 3, 5,
              2, 3, 2, 7, 5}, trials);
    return 0;
}


