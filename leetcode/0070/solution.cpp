#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 0
int climbStairs(int n)
{
    if (n <= 2) return n;
    int aux = 0;
    for (int i = 0, a = 0, b = 1; i < n; ++i)
    {
        aux = a + b;
        a = b;
        b = aux;
    }
    return aux;
}
#elif 0
int climbStairs(int n)
{
    if (n <= 2) return n;
    int result = 2;
    for (int i = 2, a = 0, b = 1, inc = 1; i < n; ++i)
    {
        int aux = a + b;
        a = b;
        result += inc;
        inc += b;
        b = aux;
    }
    return result;
    // 1 2 3 4 5  6  7  8  9
    // 1 2 3 5 8 13 21 34 55
    //     1 2 3  5  8 13 21
    //     1 1 1  2  3  5  8
}
#else
int climbStairs(int n)
{
    std::unordered_map<int, int> memo;
    auto climb = [&](auto &&self, int steps) -> int
    {
        if (steps <= 1) return 1;
        if (steps == 2) return 2;
        if (auto it = memo.find(steps); it != memo.end()) return it->second;
        int result = self(self, steps - 1);
        if (steps > 1)
            result += self(self, steps - 2);
        return memo[steps] = result;
    };
    return climb(climb, n);
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = climbStairs(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    // n = 2
    // 1. 1s + 1s
    // 2. 2s
    test(2, 2, trials);
    // n = 3
    // 1. 1s + 1s + 1s
    // 2. 1s + 2s
    // 3. 2s + 1s
    test(3, 3, trials);
    // n = 4
    // 1. 1s + 1s + 1s + 1s
    // 2. 1s + 1s + 2s
    // 3. 1s + 2s + 1s
    // 4. 2s + 1s + 1s
    // 5. 2s + 2s
    test(4, 5, trials);
    // n = 5
    // 1. 1s + 1s + 1s + 1s + 1s
    // 2. 1s + 1s + 1s + 2s
    // 3. 1s + 1s + 2s + 1s
    // 4. 1s + 2s + 1s + 1s
    // 5. 1s + 2s + 2s
    // 6. 2s + 1s + 1s + 1s
    // 7. 2s + 1s + 2s
    // 8. 2s + 2s + 1s
    test(5, 8, trials);
    test(6, 13, trials);
    test(7, 21, trials);
    test(8, 34, trials);
    test(9, 55, trials);
    // 2 3 5 8 13 21 34 55
    //   1 2 3  5  8 13 21
    //   1 1 1  2  3  5  8
    return 0;
}


