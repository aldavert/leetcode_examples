#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string decodeMessage(std::string key, std::string message)
{
    char lut[26];
    for (int i = 0; i < 26; ++i) lut[i] = -1;
    for (char current = 'a'; char letter : key)
        if ((letter != ' ') && (lut[letter - 'a'] == -1))
            lut[letter - 'a'] = current++;
    std::string result;
    for (char letter : message)
        result.push_back((letter != ' ')?lut[letter - 'a']:' ');
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string key,
          std::string message,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = decodeMessage(key, message);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("the quick brown fox jumps over the lazy dog",
         "vkbs bs t suepuv",
         "this is a secret", trials);
    test("eljuxhpwnyrdgtqkviszcfmabo",
         "zwx hnfx lqantp mnoeius ycgk vcnjrdb",
         "the five boxing wizards jump quickly", trials);
    return 0;
}


