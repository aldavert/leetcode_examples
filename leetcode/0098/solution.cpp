#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <iostream>

// ############################################################################
// ############################################################################

bool isValidBST(TreeNode * root)
{
    auto traverse = [&](auto &&self, TreeNode * node, long min_value, long max_value)
    {
        if (!node) return true;
        if ((node->val >= min_value) && (node->val <= max_value))
            return self(self, node->left, min_value, static_cast<long>(node->val> - 1))
                && self(self, node->right, static_cast<long>(node->val) + 1, max_value);
        else return false;
    };
    return traverse(traverse,
                    root,
                    std::numeric_limits<int>::lowest(),
                    std::numeric_limits<int>::max());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = isValidBST(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3}, true, trials);
    test({5, 1, 4, null, null, 3, 6}, false, trials);
    test({2147483647}, true, trials);
    return 0;
}

