#include "../common/common.hpp"

// ############################################################################
// ############################################################################

void rotate(std::vector<std::vector<int> > &matrix)
{
    const size_t n = matrix.size();
    for (size_t i = 0; i < n; ++i)  // Transpose
        for (size_t j = i + 1; j < n; ++j)
            std::swap(matrix[i][j], matrix[j][i]);
    for (size_t i = 0; i < n; ++i)  // Flip columns
        for (size_t l = 0, r = n - 1; l < r; ++l, --r)
            std::swap(matrix[i][l], matrix[i][r]);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = matrix;
        rotate(result);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}},
         {{7, 4, 1}, {8, 5, 2}, {9, 6, 3}}, trials);
    test({{5, 1, 9, 11}, {2, 4, 8, 10}, {13, 3, 6, 7}, {15, 14, 12, 16}},
         {{15, 13, 2, 5}, {14, 3, 4, 1}, {12, 6, 8, 9}, {16, 7, 10, 11}}, trials);
    test({{7}}, {{7}}, trials);
    return 0;
}


