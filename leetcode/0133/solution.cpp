#include "../common/common.hpp"
#include <unordered_map>

class Node
{
public:
    int val = 0;
    std::vector<Node*> neighbors;
    Node(int _val) : val(_val) {}
};

void deleteGraph(Node * current)
{
    if (current)
    {
        std::unordered_map<int, Node *> graph;
        auto traverse = [&](auto &&self, Node * c) -> void
        {
            if (graph.find(c->val) == graph.end())
            {
                graph[c->val] = c;
                for (auto n : c->neighbors)
                    self(self, n);
            }
        };
        traverse(traverse, current);
        for (auto [val, n] : graph)
            delete n;
    }
}

Node * vec2node(std::vector<std::vector<int> > neighbors)
{
    const int n = static_cast<int>(neighbors.size());
    if (n > 0)
    {
        std::vector<Node *> graph(n);
        for (int i = 0; i < n; ++i)
            graph[i] = new Node(i + 1);
        for (int i = 0; i < n; ++i)
            for (int val : neighbors[i])
                graph[i]->neighbors.push_back(graph[val - 1]);
        return graph[0];
    }
    else return nullptr;
}

std::vector<std::vector<int> > node2vec(Node * node)
{
    if (node)
    {
        std::unordered_map<int, Node *> graph;
        auto traverse = [&](auto &&self, Node * current) -> void
        {
            if (graph.find(current->val) == graph.end())
            {
                graph[current->val] = current;
                for (auto n : current->neighbors)
                    self(self, n);
            }
        };
        traverse(traverse, node);
        std::unordered_map<Node *, int> lut;
        for (auto [v, n] : graph)
            lut[n] = v;
        std::vector<std::vector<int> > result(graph.size());
        for (auto [v, n] : graph)
            for (auto nn : n->neighbors)
                result[v - 1].push_back(lut[nn]);
        for (auto &r : result)
            std::sort(r.begin(), r.end());
        return result;
    }
    else return {};
}

// ############################################################################
// ############################################################################

Node * cloneGraph(Node * node)
{
    if (node)
    {
        std::unordered_map<int, Node *> graph;
        auto traverse = [&](auto &&self, Node * current) -> void
        {
            if (graph.find(current->val) == graph.end())
            {
                graph[current->val] = current;
                for (auto n : current->neighbors)
                    self(self, n);
            }
        };
        traverse(traverse, node);
        std::unordered_map<Node *, int> lut;
        for (auto [v, n] : graph)
            lut[n] = v - 1;
        std::vector<Node *> new_graph(graph.size());
        for (int i = 0; i < static_cast<int>(graph.size()); ++i)
            new_graph[i] = new Node(i + 1);
        for (auto [v, n] : graph)
            for (auto nn : n->neighbors)
                new_graph[v - 1]->neighbors.push_back(new_graph[lut[nn]]);
        return new_graph[0];
    }
    else return nullptr;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        Node * node = vec2node(solution);
        Node * copy = cloneGraph(node);
        result = node2vec(copy);
        deleteGraph(node);
        deleteGraph(copy);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 4}, {1, 3}, {2, 4}, {1, 3}}, trials);
    test({{}}, trials);
    test({}, trials);
    return 0;
}


