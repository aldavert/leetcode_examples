#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<std::string> wordBreak(std::string s, std::vector<std::string> wordDict)
{
    std::unordered_map<std::string, int> dictionary;
    const int n = static_cast<int>(s.size());
    int m = 0;
    for (short idx = 0; const auto &wd : wordDict)
    {
        dictionary[wd] = idx++;
        m = std::max(m, static_cast<int>(wd.size()));
    }
    
    auto process = [&](auto &&self, int start_idx) -> std::vector<std::vector<short> >
    {
        std::vector<std::vector<short> > result;
        if (start_idx == n)
            return {{}};
        std::string current;
        
        for (int i = start_idx; (i < n) && (static_cast<int>(current.size()) < m); ++i)
        {
            current += s[i];
            if (auto search = dictionary.find(current); search != dictionary.end())
            {
                for (auto res = self(self, i + 1); auto &r : res)
                {
                    r.push_back(static_cast<short>(search->second));
                    result.push_back(r);
                }
            }
        }
        return result;
    };
    
    std::vector<std::string> result;
    for (auto num_result = process(process, 0); auto &partition : num_result)
    {
        std::string current = wordDict[partition.back()];
        partition.pop_back();
        for (; !partition.empty(); partition.pop_back())
            current += " " + wordDict[partition.back()];
        result.push_back(current);
    }
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    std::unordered_set<std::string> lut(left.begin(), left.end());
    for (const std::string &s : right)
    {
        if (auto search = lut.find(s); search != lut.end())
            lut.erase(search);
        else return false;
    }
    return lut.empty();
}

void test(std::string s,
          std::vector<std::string> wordDict,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = wordBreak(s, wordDict);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("catsanddog", {"cat", "cats", "and", "sand", "dog"},
         {"cats and dog", "cat sand dog"}, trials);
    test("pineapplepenapple", {"apple", "pen", "applepen", "pine", "pineapple"},
         {"pine apple pen apple", "pineapple pen apple", "pine applepen apple"},
         trials);
    test("catsandog", {"cats", "dog", "sand", "and", "cat"}, {}, trials);
    return 0;
}


