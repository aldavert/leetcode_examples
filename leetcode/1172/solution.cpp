#include "../common/common.hpp"
#include <stack>
#include <queue>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class DinnerPlates
{
    size_t m_capacity;
    std::vector<std::stack<int> > m_stacks;
    std::priority_queue<int, std::vector<int>, std::greater<> > m_min_heap;
public:
    DinnerPlates(int capacity) : m_capacity(capacity)
    {
        m_min_heap.push(0);
    }
    void push(int val)
    {
        const int index = m_min_heap.top();
        if (index == static_cast<int>(m_stacks.size()))
            m_stacks.push_back({});
        m_stacks[index].push(val);
        if (m_stacks[index].size() == m_capacity)
        {
            m_min_heap.pop();
            if (m_min_heap.empty())
                m_min_heap.push(static_cast<int>(m_stacks.size()));
        }
    }
    int pop(void)
    {
        while (!m_stacks.empty() && m_stacks.back().empty())
            m_stacks.pop_back();
        if (m_stacks.empty()) return -1;
        return popAtStack(static_cast<int>(m_stacks.size()) - 1);
    }
    int popAtStack(int index)
    {
        if (index >= static_cast<int>(m_stacks.size()) || m_stacks[index].empty())
            return -1;
        if (m_stacks[index].size() == m_capacity)
            m_min_heap.push(index);
        int val = m_stacks[index].top();
        m_stacks[index].pop();
        return val;
    }
};

// ############################################################################
// ############################################################################

enum class OP { PUSH, POP, POPATSTACK };

void test(int capacity,
          std::vector<OP> input,
          std::vector<int> values,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        DinnerPlates obj(capacity);
        for (int i = 0, n = static_cast<int>(input.size()); i < n; ++i)
        {
            switch (input[i])
            {
            case OP::PUSH:
                obj.push(values[i]);
                result.push_back(null);
                break;
            case OP::POP:
                result.push_back(obj.pop());
                break;
            case OP::POPATSTACK:
                result.push_back(obj.popAtStack(values[i]));
                break;
            default:
                break;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {OP::PUSH, OP::PUSH, OP::PUSH, OP::PUSH, OP::PUSH, OP::POPATSTACK,
             OP::PUSH, OP::PUSH, OP::POPATSTACK, OP::POPATSTACK, OP::POP, OP::POP,
             OP::POP, OP::POP, OP::POP},
        {1, 2, 3, 4, 5, 0, 20, 21, 0, 2, null, null, null, null, null},
        {null, null, null, null, null, 2, null, null, 20, 21, 5, 4, 3, 1, -1}, trials);
    return 0;
}


