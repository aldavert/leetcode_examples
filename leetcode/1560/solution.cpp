#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<int> mostVisited(int n, std::vector<int> rounds)
{
    std::vector<int> result;
    if (rounds.front() <= rounds.back())
        for (int i = rounds.front(); i <= rounds.back(); ++i)
            result.push_back(i);
    else
    {
        for (int i = 1; i <= rounds.back(); ++i)
            result.push_back(i);
        for (int i = rounds.front(); i <= n; ++i)
            result.push_back(i);
    }
    return result;
}
#else
std::vector<int> mostVisited(int n, std::vector<int> rounds)
{
    std::vector<std::tuple<int, int> > frequency(n + 1);
    for (int i = 0; i <= n; ++i)
        frequency[i] = {0, i};
    ++std::get<0>(frequency[rounds[0]]);
    for (int pos = rounds[0]; auto next : rounds)
    {
        while (pos != next)
        {
            if (++pos > n) pos = 1;
            ++std::get<0>(frequency[pos]);
        }
    }
    auto selected = std::get<0>(*std::max_element(frequency.begin(), frequency.end()));
    std::vector<int> result;
    for (auto [freq, bin] : frequency)
        if (freq == selected)
            result.push_back(bin);
    std::sort(result.begin(), result.end());
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<int> rounds,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = mostVisited(n, rounds);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {1, 3, 1, 2}, {1, 2}, trials);
    test(2, {2, 1, 2, 1, 2, 1, 2, 1, 2}, {2}, trials);
    test(7, {1, 3, 5, 7}, {1, 2, 3, 4, 5, 6, 7}, trials);
    return 0;
}


