#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool matchReplacement(std::string s,
                      std::string sub,
                      std::vector<std::vector<char> > mappings)
{
    const int n_s = static_cast<int>(s.size());
    const int n_sub = static_cast<int>(sub.size());
    std::vector<std::vector<bool> > is_mapped(128, std::vector<bool>(128));
    auto canTransform = [&](int start) -> bool
    {
        if (start + sub.size() > s.size())
            return false;
        for (int i = 0; i < n_sub; ++i)
            if (char a = sub[i], b = s[start + i]; (a != b) && !is_mapped[a][b])
                return false;
        return true;
    };
    
    for (const auto &m : mappings)
        is_mapped[m[0]][m[1]] = true;
    for (int i = 0; i < n_s; ++i)
        if (canTransform(i))
            return true;
    return false;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::string sub,
          std::vector<std::vector<char> > mappings,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = matchReplacement(s, sub, mappings);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("fool3e7bar", "leet", {{'e', '3'}, {'t', '7'}, {'t', '8'}}, true, trials);
    test("fooleetbar", "f00l", {{'o', '0'}}, false, trials);
    test("Fool33tbaR", "leetd",
         {{'e', '3'}, {'t', '7'}, {'t', '8'}, {'d', 'b'}, {'p', 'b'}}, true, trials);
    return 0;
}


