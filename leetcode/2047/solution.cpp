#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countValidWords(std::string sentence)
{
    int result = 0;
    std::string token;
    auto update = [&](void)
    {
        const int n = static_cast<int>(token.size());
        if (n)
        {
            if ((token.front() == '-')
            ||  (token.back()  == '-')
            ||   std::isdigit(token.back())
            ||  ((n > 1) && !std::islower(token.back()) && (token[n - 2] == '-')))
                return;
            for (int i = n - 2, counter = 0; i >= 0; --i)
            {
                counter += token[i] == '-';
                if ((counter > 1) || !(std::isalpha(token[i]) || token[i] == '-'))
                    return;
            }
            ++result;
        }
    };
    for (char letter : sentence)
    {
        if (letter == ' ')
        {
            update();
            token = "";
        }
        else token += letter;
    }
    update();
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string sentence, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countValidWords(sentence);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("cat and  dog", 3, trials);
    test("!this  1-s b8d!", 0, trials);
    test("alice and  bob are playing stone-game10", 5, trials);
    test("he bought 2 pencils, 3 erasers, and 1  pencil-sharpener.", 6, trials);
    return 0;
}


