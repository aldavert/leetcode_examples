#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int myAtoi(std::string s)
{
    const int n = static_cast<int>(s.size());
    bool negative = false;
    long result = 0;
    int idx;
    
    for (idx = 0; (idx < n) && (s[idx] == ' '); ++idx);
    if (s[idx] == '-')
    {
        negative = true;
        ++idx;
    }
    else if (s[idx] == '+') ++idx;
    for (; (idx < n) && (s[idx] >= '0') && (s[idx] <= '9'); ++idx)
    {
        result = result * 10 + s[idx] - '0';
        if (!negative && (result > std::numeric_limits<int>::max()))
            return std::numeric_limits<int>::max();
        if (negative && (-result < std::numeric_limits<int>::lowest()))
            return std::numeric_limits<int>::lowest();
    }
    return static_cast<int>((1 - 2 * negative) * result);
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = myAtoi(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("42", 42, trials);
    test("   -42", -42, trials);
    test("4193 with words", 4193, trials);
    test("-91283472332", -2147483648, trials);
    return 0;
}


