#include "../common/common.hpp"

// ############################################################################
// ############################################################################

class Trie
{
    struct TrieNode
    {
        std::vector<TrieNode *> children;
        int weight = -1;
        TrieNode(void) : children(27) {}
        ~TrieNode(void)
        {
            for (TrieNode * child : children)
                delete child;
        }
    };
public:
    void insert(const std::string& word, int weight)
    {
        TrieNode * node = &root;
        for (const char c : word)
        {
            if (!node->children[c - 'a'])
                node->children[c - 'a'] = new TrieNode();
            node = node->children[c - 'a'];
            node->weight = weight;
        }
    }
    int startsWith(const std::string &word)
    {
        TrieNode * node = &root;
        for (const char c : word)
        {
            if (!node->children[c - 'a'])
                return -1;
            node = node->children[c - 'a'];
        }
        return node->weight;
    }
private:
    TrieNode root;
};

class WordFilter
{
    Trie trie;
public:
    WordFilter(std::vector<std::string> &words)
    {
        for (size_t i = 0; i < words.size(); ++i)
            for (size_t j = 0; j <= words[i].size(); ++j)
                trie.insert(words[i].substr(j) + '{' + words[i], static_cast<int>(i));
    }
    int f(std::string prefix, std::string suffix)
    {
        return trie.startsWith(suffix + '{' + prefix);
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::vector<std::pair<std::string, std::string> > search,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        WordFilter wf(words);
        result.clear();
        for (auto [prefix, suffix] : search)
            result.push_back(wf.f(prefix, suffix));
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"apple"}, {{"a", "e"}}, {0}, trials);
    return 0;
}


