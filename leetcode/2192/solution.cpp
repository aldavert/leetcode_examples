#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> >
getAncestors(int n, std::vector<std::vector<int> > edges)
{
    auto process = [&](void) -> std::vector<std::vector<int> >
    {
        std::vector<std::vector<int> > result(n), graph(n);
        std::vector<bool> seen(n);
        auto inner = [&](auto &&self, int u, int ancestor) -> void
        {
            seen[u] = true;
            for (int v : graph[u])
            {
                if (seen[v]) continue;
                result[v].push_back(ancestor);
                self(self, v, ancestor);
            }
        };
        for (const auto &edge : edges)
            graph[edge[0]].push_back(edge[1]);
        for (int i = 0; i < n; ++i)
        {
            seen = std::vector<bool>(n);
            inner(inner, i, i);
        }
        return result;
    };
    return process();
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getAncestors(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(8, {{0, 3}, {0, 4}, {1, 3}, {2, 4}, {2, 7}, {3, 5}, {3, 6}, {3, 7}, {4, 6}},
         {{}, {}, {}, {0, 1}, {0, 2}, {0, 1, 3}, {0, 1, 2, 3, 4}, {0, 1, 2, 3}}, trials);
    test(5, {{0, 1}, {0, 2}, {0, 3}, {0, 4}, {1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 4},
         {3, 4}}, {{}, {0}, {0, 1}, {0, 1, 2}, {0, 1, 2, 3}}, trials);
    return 0;
}


