#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> divisibilityArray(std::string word, int m)
{
    std::vector<int> result;
    for (long prev_remainder = 0; char c : word)
    {
        long remainder = (prev_remainder * 10 + (c - '0')) % m;
        result.push_back(remainder == 0);
        prev_remainder = remainder;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word, int m, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = divisibilityArray(word, m);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("998244353", 3, {1, 1, 0, 0, 0, 1, 1, 0, 0}, trials);
    test("1010", 10, {0, 1, 0, 1}, trials);
    return 0;
}


