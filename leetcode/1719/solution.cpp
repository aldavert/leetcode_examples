#include "../common/common.hpp"
#include <bitset>
#include <unordered_map>

// ############################################################################
// ############################################################################

int checkWays(std::vector<std::vector<int> > pairs)
{
    std::unordered_map<int, std::bitset<501> > m;
    
    for (const auto &e : pairs) 
      m[e[0]][e[0]] = m[e[1]][e[1]] = m[e[0]][e[1]] = m[e[1]][e[0]] = true;
    const size_t n = m.size();
    if (!std::any_of(m.begin(), m.end(),
                [&n](const auto &kv) { return kv.second.count() == n;}))
        return 0;
    int multiple = 0;
    for (const auto& e : pairs)
    {
      const auto &all = m[e[0]] | m[e[1]];
      const int r0 = m[e[0]] == all;
      const int r1 = m[e[1]] == all;
      if (r0 + r1 == 0) return 0;
      multiple |= r0 * r1;
    }
    return 1 + multiple;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > pairs, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkWays(pairs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 3}}, 1, trials);
    test({{1, 2}, {2, 3}, {1, 3}}, 2, trials);
    test({{1, 2}, {2, 3}, {2, 4}, {1, 5}}, 0, trials);
    return 0;
}


