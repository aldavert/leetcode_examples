#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfAlternatingGroups(std::vector<int> colors, int k)
{
    int result = 0;
    for (int i = 0, alt = 1, n = static_cast<int>(colors.size()); i < n + k - 2; ++i)
    {
        alt = (colors[i % n] == colors[(i - 1 + n) % n])?1:alt + 1;
        result += (alt >= k);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> colors, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfAlternatingGroups(colors, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 0, 1, 0}, 3, 3, trials);
    test({0, 1, 0, 0, 1, 0, 1}, 6, 2, trials);
    test({1, 1, 0, 1}, 4, 0, trials);
    return 0;
}


