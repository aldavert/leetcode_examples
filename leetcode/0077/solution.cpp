#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > combine(int n, int k)
{
    std::vector<std::vector<int> > result;
    std::vector<int> current;
    auto backtracking =  [&](auto &&self, int i) -> void
    {
        if (static_cast<int>(current.size()) == k)
        {
            result.push_back(current);
            return;
        }
        if (i > n) return;
        self(self, i + 1);
        current.push_back(i);
        self(self, i + 1);
        current.pop_back();
    };
    backtracking(backtracking, 1);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    auto copy_left = left, copy_right = right;
    for (auto &element : copy_left)
        std::sort(element.begin(), element.end());
    for (auto &element : copy_right)
        std::sort(element.begin(), element.end());
    std::sort(copy_left.begin(), copy_left.end());
    std::sort(copy_right.begin(), copy_right.end());
    for (size_t i = 0, n = copy_left.size(); i < n; ++i)
        if (copy_left[i] != copy_right[i])
            return false;
    return true;
}

void test(int n, int k, std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = combine(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 2, {{1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 4}, {3, 4}}, trials);
    test(1, 1, {{1}}, trials);
    return 0;
}


