#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool validMountainArray(std::vector<int> arr)
{
    const int n = static_cast<int>(arr.size());
    if (n < 3) return false;
    if (arr[0] >= arr[1]) return false;
    int idx = 2;
    for (; idx < n - 1; ++idx)
    {
        if (arr[idx] == arr[idx - 1]) return false;
        if (arr[idx] <  arr[idx - 1])
            break;
    }
    for (; idx < n; ++idx)
        if (arr[idx] >= arr[idx - 1])
            return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = validMountainArray(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1}, false, trials);
    test({3, 5, 5}, false, trials);
    test({0, 3, 2, 1}, true, trials);
    test({9, 8, 7, 6, 5, 4, 3, 2, 1, 0}, false, trials);
    return 0;
}


