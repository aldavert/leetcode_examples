#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> colorTheArray(int n, std::vector<std::vector<int> > queries)
{
    std::vector<int> result, arr(n);
    for (int same_colors = 0; const auto &query : queries)
    {
        const int i = query[0], color = query[1];
        if (i + 1 < n)
        {
            if ((arr[i + 1] > 0) && (arr[i + 1] == arr[i]))
                --same_colors;
            if (arr[i + 1] == color)
                ++same_colors;
        }
        if (i > 0)
        {
            if (arr[i - 1] > 0 && arr[i - 1] == arr[i])
                --same_colors;
            if (arr[i - 1] == color)
                ++same_colors;
        }
        arr[i] = color;
        result.push_back(same_colors);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = colorTheArray(n, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{0, 2}, {1, 2}, {3, 1}, {1, 1}, {2, 1}}, {0, 1, 1, 0, 2}, trials);
    test(1, {{0, 100'000}}, {0}, trials);
    return 0;
}


