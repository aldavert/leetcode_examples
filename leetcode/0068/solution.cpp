#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> fullJustify(std::vector<std::string> words, int maxWidth)
{
    const int n = static_cast<int>(words.size());
    std::vector<std::string> result;
    
    for (int word_idx = 0; word_idx < n;)
    {
        std::string current_line(maxWidth, ' ');
        auto copy = [&](int idx, int pos) -> void
        {
            const int m = static_cast<int>(words[idx].size());
            for (int i = pos, j = 0; j < m; ++i, ++j)
                current_line[i] = words[idx][j];
        };
        int length = static_cast<int>(words[word_idx].size()), end;
        for (end = word_idx + 1; end < n; ++end)
        {
            if (length + static_cast<int>(words[end].size()) + 1 <= maxWidth)
                length += static_cast<int>(words[end].size()) + 1;
            else break;
        }
        if (end == n)
        {
            length = 0;
            for (int i = word_idx; i < end; ++i)
            {
                copy(i, length);
                length += static_cast<int>(words[i].size()) + 1;
            }
        }
        else
        {
            copy(word_idx, 0);
            
            int nwords = end - word_idx;
            if (nwords > 1)
            {
                int available_space = maxWidth - (length - (nwords - 1));
                int min_space = available_space / (nwords - 1);
                int big_spaces = available_space % (nwords - 1);
                int current_idx = word_idx + 1, gap;
                
                length = static_cast<int>(words[word_idx].size());
                for (gap = 0; gap < big_spaces; ++gap, ++current_idx)
                {
                    copy(current_idx, length + min_space + 1);
                    length += static_cast<int>(words[current_idx].size())
                           +  min_space + 1;
                }
                for (; current_idx < end; ++current_idx)
                {
                    copy(current_idx, length + min_space);
                    length += static_cast<int>(words[current_idx].size()) + min_space;
                }
            }
        }
        result.push_back(current_line);
        word_idx = end;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          int maxWidth,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = fullJustify(words, maxWidth);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"This", "is", "an", "example", "of", "text", "justification."}, 16, {"This    is    an", "example  of text", "justification.  "}, trials);
    test({"What", "must", "be", "acknowledgment", "shall","be"}, 16, {"What   must   be", "acknowledgment  ", "shall be        "}, trials);
    test({"Science", "is", "what", "we", "understand", "well", "enough", "to", "explain", "to", "a", "computer.", "Art", "is", "everything", "else", "we", "do"}, 20, {"Science  is  what we", "understand      well", "enough to explain to", "a  computer.  Art is", "everything  else  we", "do                  "}, trials);
    
    return 0;
}


