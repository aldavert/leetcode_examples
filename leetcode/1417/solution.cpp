#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string reformat(std::string s)
{
    std::vector<char> lowercase, digits;
    for (char c : s)
    {
        if ((c >= '0') && (c <= '9')) digits.push_back(c);
        else lowercase.push_back(c);
    }
    if (const int ndigits = static_cast<int>(digits.size()),
                  nletters = static_cast<int>(lowercase.size());
        std::abs(ndigits - nletters) > 1)
        return "";
    bool digit = digits.size() > lowercase.size();
    for (size_t i = 0, id = 0, il = 0; i < s.size(); ++i)
    {
        s[i] = (digit)?digits[id++]:lowercase[il++];
        digit = !digit;
    }
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s, unsigned int trials = 1)
{
    auto valid = [s](std::string test)
    {
        int ndigits = 0, nletters = 0;
        for (char c : s)
        {
            if ((c >= '0') && (c <= '9')) ++ndigits;
            else ++nletters;
        }
        if (test.size() < 1) return std::abs(ndigits - nletters) > 1;
        bool digit = ((test[0] >= '0') && (test[0] <= '9'));
        for (size_t i = 1; i < test.size(); ++i)
        {
            bool current = (test[i] >= '0') && (test[i] <= '9');
            if (digit == current) return false;
            digit = current;
        }
        return true;
    };
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = reformat(s);
    std::cout << showResult(valid(result)) << " reformat(" << s << ")='"
              << result << "' is a valid solution\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("a0b1c2", trials);
    test("leetcode", trials);
    test("1229857369", trials);
    return 0;
}


