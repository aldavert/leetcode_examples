#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool rotateString(std::string s, std::string goal)
{
    if (s.size() != goal.size()) return false;
    const size_t n = s.size();
    auto equal = [&](size_t shift)
    {
        size_t j = 0;
        for (size_t i = shift; i < n; ++i, ++j)
            if (s[i] != goal[j])
                return false;
        for (size_t i = 0; i < shift; ++i, ++j)
            if (s[i] != goal[j])
                return false;
        return true;
    };
    for (size_t t = 0; t < n; ++t)
        if (equal(t)) return true;
    return false;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::string goal,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = rotateString(s, goal);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcde", "cdeab",  true, trials);
    test("abcde", "abced", false, trials);
    return 0;
}


