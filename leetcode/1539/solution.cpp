#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int findKthPositive(std::vector<int> arr, int k)
{
    int left = -1, right = static_cast<int>(arr.size());
    while (left < right - 1)
    {
        int mid = left + (right - left) / 2;
        if (arr[mid] - mid - 1 >= k)
            right = mid;
        else left = mid;
    }
    return right + k;
}
#else
int findKthPositive(std::vector<int> arr, int k)
{
    const int n = static_cast<int>(arr.size());
    int i;
    for (i = 0; i < n; ++i)
        if (arr[i] - i > k)
            break;
    return i + k;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findKthPositive(arr, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 4, 7, 11}, 5, 9, trials);
    test({2, 3, 4, 7, 11}, 1, 1, trials);
    test({2, 3, 4, 7, 11}, 2, 5, trials);
    test({1, 2, 3, 4}, 2, 6, trials);
    return 0;
}


