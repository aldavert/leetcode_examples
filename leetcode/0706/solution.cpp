#include "../common/common.hpp"
#include <list>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class MyHashMap
{
    //std::array<std::list<int>, 65'536> m_buckets;
    //std::hash<unsigned short> m_hash;
    std::array<std::list<std::pair<int, int> >, 256> m_buckets;
    std::hash<int> m_hash;
    inline unsigned char hash(int key) const
        { return static_cast<unsigned char>(m_hash(key) % 256); }
public:
    MyHashMap()
    {
    }
    void put(int key, int value)
    {
        auto &current = m_buckets[hash(key)];
        bool not_found = true;
        for (auto begin = current.begin(), end = current.end(); begin != end; ++begin)
        {
            if (begin->first == key)
            {
                begin->second = value;
                not_found = false;
            }
        }
        if (not_found)
            current.push_back({key, value});
    }
    void remove(int key)
    {
        auto &current = m_buckets[hash(key)];
        for (auto begin = current.begin(), end = current.end(); begin != end; ++begin)
        {
            if (begin->first == key)
            {
                current.erase(begin);
                break;
            }
        }
    }
    int get(int key)
    {
        auto &current = m_buckets[hash(key)];
        for (auto begin = current.begin(), end = current.end(); begin != end; ++begin)
            if (begin->first == key)
                return begin->second;
        return -1;
    }
};

// ############################################################################
// ############################################################################

enum class OP { PUT = 1, GET = 2, REMOVE = 4 };

void test(std::vector<OP> ops,
          std::vector<std::vector<int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    const size_t n = input.size();
    std::vector<int> result(n);
    for (unsigned int t = 0; t < trials; ++t)
    {
        MyHashMap hash;
        
        for (size_t i = 0; i < n; ++i)
        {
            result[i] = null;
            if      (ops[i] == OP::PUT)
                hash.put(input[i][0], input[i][1]);
            else if (ops[i] == OP::GET)
                result[i] = hash.get(input[i][0]);
            else if (ops[i] == OP::REMOVE)
                hash.remove(input[i][0]);
            else throw std::invalid_argument("Unknown operation!");
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::PUT, OP::PUT, OP::GET, OP::GET, OP::PUT, OP::GET, OP::REMOVE, OP::GET},
         {{1, 1}, {2, 2}, {1}, {3}, {2, 1}, {2}, {2}, {2}},
         {null, null, 1, -1, null, 1, null, -1},
         trials);
    return 0;
}


