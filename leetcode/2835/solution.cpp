#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> nums, int target)
{
    constexpr int MAXBIT = 31;
    int result = 0;
    int min_missing_bit = MAXBIT;
    int count[MAXBIT + 1] = {};
    for (int num : nums)
        ++count[static_cast<int>(std::log2(num))];
    for (int bit = 0; bit < MAXBIT; ++bit)
    {
        if ((target >> bit) & 1)
        {
            if (count[bit] > 0) --count[bit];
            else min_missing_bit = std::min(min_missing_bit, bit);
        }
        if ((min_missing_bit != MAXBIT) && (count[bit] > 0))
        {
            --count[bit];
            result += bit - min_missing_bit;
            min_missing_bit = MAXBIT;
        }
        count[bit + 1] += count[bit] / 2;
    }
    return (min_missing_bit == MAXBIT)?result:-1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 8}, 7, 1, trials);
    test({1, 32, 1, 2}, 12, 2, trials);
    test({1, 32, 1}, 35, -1, trials);
    return 0;
}


