#include "../common/common.hpp"
#include "../common/list.hpp"
#include <iostream>

// ############################################################################
// ############################################################################

bool hasCycle(ListNode * head)
{
    ListNode * slow = head, * fast = head;
    while (fast && fast->next)
    {
        fast = fast->next;
        if (slow == fast) return true;
        if (fast) fast = fast->next;
        if (slow == fast) return true;
        slow = slow->next;
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> list,
          int pos,
          bool solution,
          unsigned int trials = 1)
{
    bool result = false;
    for (unsigned int t = 0; t < trials; ++t)
    {
        ListNode * head = vec2list(list);
        ListNode * tail = nullptr;
        if (head != nullptr)
        {
            ListNode * loop = nullptr;
            tail = head;
            for (int i = 0; tail->next != nullptr; ++i)
            {
                if (i == pos)
                    loop = tail;
                tail = tail->next;
            }
            tail->next = loop;
        }
        result = hasCycle(head);
        if (tail) tail->next = nullptr;
        delete head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 0, -4}, 1, true, trials);
    test({1, 2}, 0, true, trials);
    test({1}, -1, false, trials);
    test({1, 2}, -1, false, trials);
    return 0;
}


