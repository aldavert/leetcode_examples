#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > largestLocal(std::vector<std::vector<int> > grid)
{
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>(grid[0].size());
    std::vector<std::vector<int> > result(n_rows - 2, std::vector<int>(n_cols - 2));
    for (int row = 1; row < n_rows - 1; ++row)
    {
        for (int col = 1; col < n_cols - 1; ++col)
        {
            int current = 0;
            for (int i = -1; i <= 1; ++i)
                for (int j = -1; j <= 1; ++j)
                    current = std::max(current, grid[row + i][col + j]);
            result[row - 1][col - 1] = current;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestLocal(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{9, 9, 8, 1},
          {5, 6, 2, 6},
          {8, 2, 6, 4},
          {6, 2, 2, 2}}, {{9, 9}, {8, 6}}, trials);
    test({{1, 1, 1, 1, 1},
          {1, 1, 1, 1, 1},
          {1, 1, 2, 1, 1},
          {1, 1, 1, 1, 1},
          {1, 1, 1, 1, 1}}, {{2, 2, 2}, {2, 2, 2}, {2, 2, 2}}, trials);
    return 0;
}


