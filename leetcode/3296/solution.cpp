#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

long long minNumberOfSeconds(int mountainHeight, std::vector<int> workerTimes)
{
    auto check = [&](long long seconds)
    {
        long height = 0;
        for (int time : workerTimes)
            height += static_cast<long>((-1 + std::sqrt(1 + 8 * seconds / time)) / 2);
        return height >= mountainHeight;
    };
    long long left = 0, right = std::numeric_limits<long long>::max() / 10;
    while (left < right)
    {
        long long middle = (right - left) / 2 + left;
        if (check(middle)) right = middle;
        else left = middle + 1;
    }
    return left;
}

// ############################################################################
// ############################################################################

void test(int mountainHeight,
          std::vector<int> workerTimes,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minNumberOfSeconds(mountainHeight, workerTimes);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {2, 1, 1}, 3, trials);
    test(10, {3, 2, 2, 4}, 12, trials);
    test(5, {1}, 15, trials);
    return 0;
}


