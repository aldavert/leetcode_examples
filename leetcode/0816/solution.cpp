#include "../common/common.hpp"
#include <set>

// ############################################################################
// ############################################################################

#if 1
std::vector<std::string> ambiguousCoordinates(std::string s)
{
    auto generateNumbers = [&](const char * data, int length) -> std::vector<std::string>
    {
        if      (length == 0) return {};
        else if (length == 1) return { std::string(&data[0], 1) };
        else
        {
            if (data[0] == '0')
            {
                if (data[length - 1] == '0') return {};
                return {"0." + std::string(&data[1], length - 1)};
            }
            else
            {
                std::vector<std::string> result{std::string(data, length)};
                if (data[length - 1] != '0')
                    for (int i = 1; i < length; ++i)
                        result.push_back(std::string(data, i) + "."
                                       + std::string(&data[i], length - i));
                return result;
            }
        }
    };
    std::vector<std::string> result;
    char numbers[14] = {};
    int idx = 0;
    for (const auto &c : s)
        if ((c >= '0') && (c <= '9'))
            numbers[idx++] = c;
    for (int i = 1; i < idx; ++i)
        for (const auto &left : generateNumbers(&numbers[0], i))
            for (const auto &right : generateNumbers(&numbers[i], idx - i))
                result.push_back("(" + left + ", " + right + ")");
    return result;
}
#else
std::vector<std::string> ambiguousCoordinates(std::string s)
{
    auto generate =
        [](const char * data, int length, int right) -> std::vector<std::string>
    {
        if (length == 1) return { std::string(&data[0], 1) };
        if (length - right >= 1)
        {
            if (right > 0)
                return {std::string(data, length)};
            std::vector<std::string> result{std::string(data, length)};
            for (int i = 1; i < length; ++i)
                result.push_back(std::string(data, i) + "."
                               + std::string(&data[i], length - i));
            return result;
        }
        else return {};
    };
    std::vector<std::string> result;
    char hist[14] = {};
    int left = 0, right = 0, idx = 0;
    
    // Gather information.
    for (const auto &c : s)
        if ((c >= '0') && (c <= '9'))
            hist[idx++] = c;
    for (left = 0; (left < idx) && (hist[left] == '0'); ++left);
    if (left == idx)
    {
        if (idx == 2) return {"(0, 0)"};
        else return {};
    }
    for (right = idx - 1; (right >= 0) && (hist[right] == '0'); --right);
    right = idx - right - 1;
    int nz = idx - left - right;
    
    if (left > 0)
    {
        if (left > 1)
        {
            if (right == 0) // When no right zeros are present, first possible result.
                result.push_back("(0, 0." + std::string(&hist[2], idx - 2) + ")");
            if (nz == 1)
            {
                if (right > 0) // Only possible result with a single non-zero element.
                    result.push_back("(0." + std::string(&hist[1], idx - 2) + ", 0)");
            }
            else
            {
                // Generate multiple responses.
                for (int i = 1; i < nz; ++i)
                {
                    std::string l = "0." + std::string(&hist[1], left - 1 + i);
                    for (const auto &r : generate(&hist[left + i],
                                                  right + nz - i, right))
                        result.push_back("(" + l + ", " + r + ")");
                }
            }
        }
        else
        {
            for (int i = 0; i <= nz; ++i)
            {
                std::string l = (i == 0)?("0"):("0." + std::string(&hist[1], i));
                for (const auto &r : generate(&hist[i + 1], idx - i - 1, right))
                    result.push_back("(" + l + ", " + r + ")");
            }
        }
    }
    else
    {
        for (int i = 1; i < idx; ++i)
            for (const auto &l : generate(&hist[0], i, std::max(i - nz, 0)))
                for (const auto &r : generate(&hist[i], idx - i, right))
                    result.push_back("(" + l + ", " + r + ")");
    }
    
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = ambiguousCoordinates(s);
    bool valid = std::set<std::string>(result.begin(), result.end())
              == std::set<std::string>(solution.begin(), solution.end());
    showResult(valid, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("(123)", {"(1, 23)", "(12, 3)", "(1.2, 3)", "(1, 2.3)"}, trials);
    test("(00011)", {"(0.001, 1)", "(0, 0.011)"}, trials);
    test("(0123)", {"(0, 123)", "(0, 12.3)", "(0, 1.23)", "(0.1, 23)",
         "(0.1, 2.3)", "(0.12, 3)"}, trials);
    test("(100)", {"(10, 0)"}, trials);
    
    test("(0001)", {"(0, 0.01)"}, trials);
    test("(000123)", {"(0.001, 23)", "(0.001, 2.3)", "(0.0012, 3)", "(0, 0.0123)"}, trials);
    test("(00012300)", {"(0.001, 2300)", "(0.0012, 300)"}, trials);
    test("(0123)", {"(0, 123)", "(0, 12.3)", "(0, 1.23)", "(0.1, 23)",
         "(0.1, 2.3)", "(0.12, 3)"}, trials);
    test("(100)", {"(10, 0)"}, trials);
    test("(1000)", {"(100, 0)"}, trials);
    test("(010)", {"(0, 10)", "(0.1, 0)"}, trials);
    test("(0100)", {"(0, 100)"}, trials);
    test("(01230)", {"(0, 1230)", "(0.1, 230)", "(0.12, 30)", "(0.123, 0)"}, trials);
    test("(101)", {"(10, 1)", "(1, 0.1)"}, trials);
    return 0;
}


