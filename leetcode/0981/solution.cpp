#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

class TimeMap
{
    struct Info
    {
        std::string value = "";
        int timestamp = 0;
        bool operator<(const Info &other) const { return timestamp < other.timestamp; }
    };
    std::unordered_map<std::string, std::vector<Info> > m_lut;
public:
    TimeMap(void) {}
    void set(std::string key, std::string value, int timestamp)
    {
        m_lut[key].push_back({value, timestamp});
    }
    std::string get(std::string key, int timestamp)
    {
        const auto search = m_lut.find(key);
        if (search == m_lut.end()) return "";
        const auto &info = search->second;
        auto ptr = --std::upper_bound(info.begin(), info.end(), Info{"", timestamp});
        return (distance(info.begin(), ptr) == -1)?"":ptr->value;
    }
};

// ############################################################################
// ############################################################################

struct Parameters
{
    Parameters(std::string k, std::string v, int t) :
        key(k), value(v), timestamp(t) {}
    Parameters(std::string k, int t) :
        key(k), timestamp(t) {}
    std::string key;
    std::string value;
    int timestamp;
};

void test(std::vector<char> op,
          std::vector<Parameters> params,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    const size_t n = op.size();
    std::vector<std::string> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        TimeMap object;
        for (size_t i = 0; i < n; ++i)
        {
            if (op[i] == 'g')
            {
                result.push_back(object.get(params[i].key, params[i].timestamp));
            }
            else if (op[i] == 's')
            {
                object.set(params[i].key, params[i].value, params[i].timestamp);
                result.push_back("");
            }
            else break;
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({'s', 'g', 'g', 's', 'g', 'g'},
         {{"foo", "bar", 1}, {"foo", 1}, {"foo", 3}, {"foo", "bar2", 4},
          {"foo", 4}, {"foo", 5}},
         {"", "bar", "bar", "", "bar2", "bar2"},
         trials);
    test({'s', 'g', 'g', 's', 'g', 'g', 'g'},
         {{"foo", "bar", 1}, {"foo", 1}, {"foo", 3}, {"foo", "bar2", 4},
          {"foo", 4}, {"foo", 5}, {"foo", 0}},
         {"", "bar", "bar", "", "bar2", "bar2", ""},
         trials);
    return 0;
}


