#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long numberOfRightTriangles(std::vector<std::vector<int> > grid)
{
    const int m = static_cast<int>(grid.size());
    const int n = static_cast<int>(grid[0].size());
    std::vector<int> rows(m), cols(n);
    long result = 0;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (grid[i][j] == 1)
                ++rows[i],
                ++cols[j];
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (grid[i][j] == 1)
                result += (rows[i] - 1) * (cols[j] - 1);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfRightTriangles(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 0}, {0, 1, 1}, {0, 1, 0}}, 2, trials);
    test({{1, 0, 0, 0}, {0, 1, 0, 1}, {1, 0, 0, 0}}, 0, trials);
    test({{1, 0, 1}, {1, 0, 0}, {1, 0, 0}}, 2, trials);
    return 0;
}


