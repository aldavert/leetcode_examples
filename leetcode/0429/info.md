# N-ary Tree Level Order Traversal

Given an n-ary tree, return the *level order* traversal of its nodes' values.

*Nary-Tree input serialization is represented in their level order traversal, each group of children is separated by the null value (See examples)*.

#### Example 1:
> ```mermaid
> graph TD;
> A((1))-->B((3))
> A-->C((2))
> A-->D((4))
> B-->E((5))
> B-->F((6))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [1, null, 3, 2, 4, null, 5, 6]`  
> *Output:* `[[1], [3, 2, 4], [5, 6]]`

#### Example 2:
> ```mermaid
> graph TD;
> A((1))-->B((2))
> A-->C((3))
> A-->D((4))
> A-->E((5))
> C-->F((6))
> C-->G((7))
> D-->H((8))
> E-->I((9))
> E-->J((10))
> G-->K((11))
> H-->L((12))
> I-->M((13))
> K-->N((14))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [1, null, 2, 3, 4, 5, null, null, 6, 7, null, 8, null, 9, 10, null, null, 11, null, 12, null, 13, null, null, 14]`  
> *Output:* `[[1], [2, 3, 4, 5], [6, 7, 8, 9, 10], [11, 12, 13], [14]]`

#### Constrains:
- The height of the n-ary tree is less or equal to `1000`.
- The total number of nodes is between `[0, 10^4]`.


