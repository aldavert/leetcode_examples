#include "../common/common.hpp"
#include <queue>

constexpr int null = -1000;

class Node
{
public:
    int val = 0;
    std::vector<Node *> children;

    Node() = default;
    Node(int _val) : val(_val) {}
    Node(int _val, std::vector<Node *> _children) : val(_val), children(_children) {}
    ~Node(void)
    {
        for (auto &n : children) delete n;
    }
};

Node * vec2tree(const std::vector<int> &tree)
{
    const int n = static_cast<int>(tree.size());
    if (n == 0) return nullptr;
    Node * root = new Node(tree[0]);
    std::queue<Node *> queue;
    queue.push(root);
    for (int i = 2; i < n; ++i)
    {
        Node * current = queue.front();
        queue.pop();
        int j;
        for (j = i; (j < n) && (tree[j] != null); ++j);
        current->children.resize(j - i, nullptr);
        for (int k = i; k < j; ++k)
            queue.push(current->children[k - i] = new Node(tree[k]));
        i = j;
    }
    return root;
}

// ############################################################################
// ############################################################################

#if 1
std::vector<std::vector<int> > levelOrder(Node* root)
{
    if (root == nullptr) return {};
    else
    {
        std::vector<std::vector<int> > result;
        std::queue<Node *> queue;
        std::vector<int> current_level;
        queue.push(root);
        queue.push(nullptr);
        while (!queue.empty())
        {
            Node * current = queue.front();
            queue.pop();
            if (current == nullptr)
            {
                if (!queue.empty()) queue.push(nullptr);
                result.push_back(current_level);
                current_level.resize(0);
            }
            else
            {
                current_level.push_back(current->val);
                for (auto &n : current->children)
                    queue.push(n);
            }
        }
        return result;
    }
}
#else
std::vector<std::vector<int> > levelOrder(Node* root)
{
    if (root != nullptr)
    {
        std::cout << '{';
        bool next = false;
        for (auto &c : root->children)
        {
            if (next) [[likely]] std::cout << ", ";
            next = true;
            std::cout << ((c != nullptr)?c->val:null);
        }
        std::cout << "};\n";
        for (auto &c : root->children)
            levelOrder(c);
    }
    return {};
}
#endif
// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        Node * root = vec2tree(tree);
        result = levelOrder(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, null, 3, 2, 4, null, 5, 6}, {{1}, {3, 2, 4}, {5, 6}}, trials);
    test({1, null, 2, 3, 4, 5, null, null, 6, 7, null, 8, null, 9, 10, null,
          null, 11, null, 12, null, 13, null, null, 14},
          {{1}, {2, 3, 4, 5}, {6, 7, 8, 9, 10}, {11, 12, 13}, {14}}, trials);
    return 0;
}


