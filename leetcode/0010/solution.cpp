#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isMatch(std::string s, std::string p)
{
    const int n = static_cast<int>(s.size());
    const int m = static_cast<int>(p.size());
    std::unordered_set<int> lut;
    auto match = [&](auto &&self, int idx_s, int idx_p) -> bool
    {
        const int call_id = idx_s << 16 | idx_p;
        if (lut.find(call_id) != lut.end()) return false;
        lut.insert(call_id);
        if ((idx_s == n) && (idx_p == m))
            return true;
        else if ((idx_s < n) && (idx_p >= m))
            return false;
        
        if ((idx_p + 1 < m) && (p[idx_p + 1] == '*')) // Repeat
        {
            // Avoid the repetition pattern (0 repetitions).
            if (self(self, idx_s, idx_p + 2))
                return true;
            // Rest of the cases.
            for (int repeat_s = idx_s; repeat_s < n; ++repeat_s)
            {
                if ((p[idx_p] == '.') || (s[repeat_s] == p[idx_p]))
                {
                    if (self(self, repeat_s + 1, idx_p + 2))
                        return true;
                }
                else break;
            }
            return false;
        }
        else
        {
            if (idx_s == n)
                return false;
            if ((p[idx_p] == '.') || (s[idx_s] == p[idx_p]))
                return self(self, idx_s + 1, idx_p + 1);
            else return false;
        }
    };
    return match(match, 0, 0);
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string p, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isMatch(s, p);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aa", "a", false, trials);
    test("aa", "a*", true, trials);
    test("ab", ".*", true, trials);
    test("aab", "c*a*b", true, trials);
    test("mississippi", "mis*is*p*.", false, trials);
    test("a", "ab*", true, trials);
    test("bccbbabcaccacbcacaa", ".*b.*c*.*.*.c*a*.c", false, trials);
    return 0;
}

