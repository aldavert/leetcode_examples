#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int colorTheGrid(int m, int n)
{
    constexpr int MAX = 1'024;
    constexpr int MOD = 1'000'000'007;
    
    int dp[2][MAX] = {};
    std::vector<int> mask, adj[MAX];
    auto add = [&](int a, int b) { int c = a + b; return (c < MOD)?c:c - MOD; };
    auto check = [](int o, int a, int b)
    {
        for (int i = 0; i < o; ++i)
            if (((a >> (2 * i)) & 3) == ((b >> (2 * i)) & 3))
                return false;
        return true;
    };
    auto dfs = [&](auto &&self, int o, int k, int pre, int cur) -> void
    {
        if (k == o) mask.push_back(cur);
        else for (int i = 0; i < 3; ++i)
            if (i != pre)
                self(self, o, k + 1, i, (cur | (i << (2 * k))));
    };
    
    dfs(dfs, m, 0, -1, 0);
    std::sort(mask.begin(), mask.end());
    for (int u : mask)
        for (int v : mask)
            if (check(m, u, v))
                adj[u].push_back(v);
    int k = static_cast<int>(mask.size()), x = 0;
    for (int u : mask)
        dp[x][u] = 1;
    for (int i = 1; i < n; ++i)
    {
        x ^= 1;
        for (int j = k - 1; j >= 0; --j)
        {
            int u = mask[j];
            dp[x][u] = 0;
            for (int v : adj[u])
                dp[x][u] = add(dp[x][u], dp[x ^ 1][v]);
        }
    }
    int result = 0;
    for (int u : mask)
        result = add(result, dp[x][u]);
    return result;
}
#else
int colorTheGrid(int m, int n)
{
    constexpr int MOD = 1'000'000'007;
    std::vector<std::vector<int> > memo(1000, std::vector<int>(1024));
    auto getColor = [](int mask, int r) { return mask >> r * 2 & 3; };
    auto setColor = [](int mask, int r, int color) { return mask | color << r * 2; };
    auto dp =
        [&](auto &&self, int r, int c, int prev_column_mask, int curr_column_mask) -> int
    {
        if (c == n) return 1;
        if (memo[c][prev_column_mask]) return memo[c][prev_column_mask];
        if (r == m) return self(self, 0, c + 1, curr_column_mask, 0);
        
        int result = 0;
        for (int color = 1; color <= 3; ++color)
        {
            if (getColor(prev_column_mask, r) == color) continue;
            if ((r > 0) && (getColor(curr_column_mask, r - 1) == color)) continue;
            int next_color = setColor(curr_column_mask, r, color);
            result = (result + self(self, r + 1, c, prev_column_mask, next_color)) % MOD;
        }
        
        if (r == 0) memo[c][prev_column_mask] = result;
        return result;
    };
    return dp(dp, 0, 0, 0, 0);
}
#endif

// ############################################################################
// ############################################################################

void test(int m, int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = colorTheGrid(m, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 1, 3, trials);
    test(1, 2, 6, trials);
    test(5, 5, 580'986, trials);
    return 0;
}



