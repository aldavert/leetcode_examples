#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> cellsInRange(std::string s)
{
    std::vector<std::string> result;
    for (char column = s[0]; column <= s[3]; ++column)
        for (char row = s[1]; row <= s[4]; ++row)
            result.push_back({column, row});
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = cellsInRange(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("K1:L2", {"K1", "K2", "L1", "L2"}, trials);
    test("A1:F1", {"A1", "B1", "C1", "D1", "E1", "F1"}, trials);
    return 0;
}


