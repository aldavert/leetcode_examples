#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > findSubsequences(std::vector<int> nums)
{
    auto vec2string = [](const std::vector<int> &vec) -> std::string
    {
        const size_t n = vec.size();
        std::string result(2 * n + 1, '\0');
        for (size_t i = 0; i < n; ++i)
        {
            int value = vec[i] + 100;
            result[2 * i + 0] = static_cast<char>('a' + ( value       & 0xF));
            result[2 * i + 1] = static_cast<char>('a' + ((value >> 4) & 0xF));
        }
        return result;
    };
    std::unordered_map<std::string, std::vector<int> > lut;
    const size_t n = nums.size();
    std::vector<std::vector<int> > result;
    auto create = [&](auto &&self, size_t index, std::vector<int> &current) -> void
    {
        if (index >= n)
        {
            std::string id = vec2string(current);
            if ((current.size() > 1) && (lut.find(id) == lut.end()))
                lut[id] = current;
        }
        else
        {
            if (nums[index] >= current.back())
            {
                current.push_back(nums[index]);
                self(self, index + 1, current);
                current.pop_back();
            }
            self(self, index + 1, current);
        }
    };
    for (size_t i = 0; i < n; ++i)
    {
        std::vector<int> current(1, nums[i]);
        create(create, i + 1, current);
    }
    for (auto [id, vec] : lut)
        result.push_back(vec);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    auto vec2string = [](const std::vector<int> &vec) -> std::string
    {
        const size_t n = vec.size();
        std::string result(2 * n + 1, '\0');
        for (size_t i = 0; i < n; ++i)
        {
            int value = vec[i] + 100;
            result[2 * i + 0] = static_cast<char>('a' + ( value       & 0xF));
            result[2 * i + 1] = static_cast<char>('a' + ((value >> 4) & 0xF));
        }
        return result;
    };
    std::unordered_set<std::string> histogram;
    for (const auto &vec : left)
        histogram.insert(vec2string(vec));
    if (left.size() != histogram.size()) return false;
    for (const auto &vec : right)
    {
        if (auto search = histogram.find(vec2string(vec)); search != histogram.end())
            histogram.erase(search);
        else return false;
    }
    return histogram.empty();
}

void test(std::vector<int> nums,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findSubsequences(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 6, 7, 7}, {{4, 6}, {4, 6, 7}, {4, 6, 7, 7}, {4, 7}, {4, 7, 7}, {6, 7}, {6, 7, 7}, {7, 7}}, trials);
    test({4, 4, 3, 2, 1}, {{4, 4}}, trials);
    test({1, 3, 5, 7},  {{1, 3}, {1, 3, 5}, {1, 3, 5, 7}, {1, 3, 7}, {1, 5}, {1, 5, 7}, {1, 7}, {3, 5}, {3, 5, 7}, {3, 7}, {5, 7}},  trials);
    return 0;
}


