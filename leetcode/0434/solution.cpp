#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countSegments(std::string s)
{
    int result = 0;
    for (bool space = true; char c : s)
    {
        if (c == ' ')
            space = true;
        else if (space)
            ++result, space = false;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSegments(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("Hello, my name is John", 5, trials);
    test("Hello", 1, trials);
    return 0;
}


