#include "../common/common.hpp"
#include <queue>
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
int collectTheCoins(std::vector<int> coins, std::vector<std::vector<int> > edges)
{
    int n = static_cast<int>(coins.size());
    std::vector<std::vector<int> > graph(n);
    for(const auto &edge : edges)
    {
        graph[edge[0]].push_back(edge[1]);
        graph[edge[1]].push_back(edge[0]);
    }
    
    std::vector<int> count(n), distance(n);
    std::queue<int> q;
    for (int i = 0; i < n; ++i)
    {
        distance[i] = -(coins[i] <= 0);
        count[i] = static_cast<int>(graph[i].size());
        if (count[i] == 1)
            q.push(i);
    }
    while(!q.empty())
    {
        int current = q.front();
        q.pop();
        
        if (distance[current] == 2) continue;
        --n;
        for (int next : graph[current])
        {
            if (distance[current] != -1)
                distance[next] = std::max(distance[next], distance[current] + 1);
            if (--count[next] == 1)
                q.push(next);
        }
    }
    return std::max(0, 2 * (n - 1));
}
#else
int collectTheCoins(std::vector<int> coins, std::vector<std::vector<int> > edges)
{
    const int n = static_cast<int>(coins.size());
    std::vector<std::unordered_set<int> > tree(n);
    std::queue<int> leaves_to_be_removed;
    auto update = [&](int u) -> int
    {
        int v = *tree[u].begin();
        tree[u].clear();
        tree[v].erase(u);
        return v;
    };
    
    for (const auto &edge : edges)
    {
        tree[edge[0]].insert(edge[1]);
        tree[edge[1]].insert(edge[0]);
    }
    for (int i = 0; i < n; ++i)
    {
        int u = i;
        while ((tree[u].size() == 1) && (coins[u] == 0))
            u = update(u);
        if (tree[u].size() == 1) leaves_to_be_removed.push(u);
    }
    for (int i = 0; i < 2; ++i)
    {
        for (int sz = static_cast<int>(leaves_to_be_removed.size()); sz > 0; --sz)
        {
            int u = leaves_to_be_removed.front();
            leaves_to_be_removed.pop();
            if (!tree[u].empty())
                if (int v = update(u); tree[v].size() == 1)
                    leaves_to_be_removed.push(v);
        }
    }
    
    return std::accumulate(tree.begin(), tree.end(), 0,
            [](int subtotal, const std::unordered_set<int>& children) {
            return subtotal + children.size(); });
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> coins,
          std::vector<std::vector<int> > edges,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = collectTheCoins(coins, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 0, 0, 0, 1}, {{0, 1}, {1, 2}, {2, 3}, {3, 4}, {4, 5}}, 2, trials);
    test({0, 0, 0, 1, 1, 0, 0, 1},
         {{0, 1}, {0, 2}, {1, 3}, {1, 4}, {2, 5}, {5, 6}, {5, 7}}, 2, trials);
    return 0;
}


