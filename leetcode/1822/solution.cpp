#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int arraySign(std::vector<int> nums)
{
    int negatives = 0;
    for (int n : nums)
    {
        if (n == 0) return 0;
        negatives += (n < 0);
    }
    return 1 - 2 * (negatives & 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = arraySign(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, -2, -3, -4, 3, 2, 1}, 1, trials);
    test({1, 5, 0, 2, -3}, 0, trials);
    test({-1, 1, -1, 1, -1}, -1, trials);
    return 0;
}


