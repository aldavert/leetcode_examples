#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int findPairs(std::vector<int> nums, int k)
{
    int result = 0;
    if (k > 0)
    {
        std::unordered_set<int> lut;
        for (int n : nums)
            lut.insert(n);
        for (int v : lut)
            if (lut.find(v + k) != lut.end())
                ++result;
    }
    else
    {
        std::unordered_map<int, int> lut;
        for (int n : nums)
        {
            int current = ++lut[n];
            if (current == 2) ++result;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findPairs(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1, 4, 1, 5}, 2, 2, trials);
    test({1, 2, 3, 4, 5}, 1, 4, trials);
    test({1, 3, 1, 5, 4}, 0, 1, trials);
    return 0;
}


