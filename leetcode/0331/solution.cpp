#include "../common/common.hpp"
#include <sstream>
#include <stack>

// ############################################################################
// ############################################################################

bool isValidSerialization(std::string preorder)
{
    std::istringstream ss(preorder);
    std::string token;
    
    std::stack<bool> s;
    if (std::getline(ss, token, ','))
    {
        if (token != "#")
            s.push(false);
    }
    while (std::getline(ss, token, ','))
    {
        if (s.empty()) return false;
        if (!s.top()) s.top() = true;
        else s.pop();
        if (token != "#")
            s.push(false);
    }
    return s.empty();
}

// ############################################################################
// ############################################################################

void test(std::string preorder, bool solution, unsigned int trials = 1)
{
    bool result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = isValidSerialization(preorder);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("9,3,4,#,#,1,#,#,2,#,6,#,#", true, trials);
    test("1,#", false, trials);
    test("9,#,#,1", false, trials);
    test("#", true, trials);
    return 0;
}


