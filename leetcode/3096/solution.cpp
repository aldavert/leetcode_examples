#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int minimumLevels(std::vector<int> possible)
{
    const int n = static_cast<int>(possible.size());
    int sum = 0;
    for (int num : possible) sum += (2 * (num == 1) - 1);
    for (int i = 0, first_player = 0; i < n - 1; ++i)
    {
        first_player += (2 * (possible[i] == 1) - 1);
        if (first_player > sum - first_player) return i + 1;
    }
    return -1;
}
#else
int minimumLevels(std::vector<int> possible)
{
    const int n = static_cast<int>(possible.size());
    std::vector<int> nums, prefix(n + 1);
    for (int num : possible) nums.push_back(2 * (num == 1) - 1);
    std::partial_sum(nums.begin(), nums.end(), prefix.begin() + 1);
    for (int i = 1; i < n; ++i)
        if (prefix[i] > prefix[n] - prefix[i])
            return i;
    return -1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> possible, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumLevels(possible);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 1, 0}, 1, trials);
    test({1, 1, 1, 1, 1}, 3, trials);
    test({0, 0}, -1, trials);
    return 0;
}


