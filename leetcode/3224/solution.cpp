#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int minChanges(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    const int pair_size = n / 2;
    int result = n;
    std::unordered_map<int, int> diff_count;
    std::vector<int> one_change_count(k + 1);
    
    for (int i = 0; i < pair_size; ++i)
    {
        const int a = nums[i], b = nums[n - i - 1];
        ++diff_count[std::abs(a - b)];
        ++one_change_count[std::max({a, b, k - a, k - b})];
    }
    std::vector<int> prefix_one_change_count{one_change_count};
    for (int i = k - 1; i >= 0; --i)
        prefix_one_change_count[i] += prefix_one_change_count[i + 1];
    for (const auto& [diff, freq] : diff_count)
    {
        const int one_change = prefix_one_change_count[diff] - freq;
        const int two_changes = (pair_size - prefix_one_change_count[diff]) * 2;
        result = std::min(result, one_change + two_changes);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minChanges(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 1, 2, 4, 3}, 4, 2, trials);
    test({0, 1, 2, 3, 3, 6, 5, 4}, 6, 2, trials);
    return 0;
}


