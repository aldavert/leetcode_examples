#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int findMinArrowShots(std::vector<std::vector<int> > points)
{
    if (points.size() == 0) return 0;
    struct Segment
    {
        int begin = 0;
        int end = 0;
        inline bool operator<(const Segment &other) const { return end < other.end; }
    };
    std::vector<Segment> sorted_points;
    sorted_points.reserve(points.size());
    int result = 0;
    for (const auto &p : points)
        sorted_points.push_back({p[0], p[1]});
    std::sort(sorted_points.begin(), sorted_points.end());
    for (int active = 0, previous = sorted_points[0].end; Segment s : sorted_points)
    {
        if (s.begin > previous)
        {
            previous = s.end;
            active = 1;
            ++result;
        }
        else ++active;
    }
    return result + 1;
}
#else
int findMinArrowShots(std::vector<std::vector<int> > points)
{
    if (points.size() == 0) return 0;
    struct Segment
    {
        int begin = 0;
        int end = 0;
        bool operator<(const Segment &other) const { return end < other.end; }
    };
    std::vector<Segment> sorted_points;
    int result = 0;
    for (const auto &p : points)
        sorted_points.push_back({p[0], p[1]});
    std::sort(sorted_points.begin(), sorted_points.end());
    for (int active = 0, previous = sorted_points[0].end; Segment s : sorted_points)
    {
        if (s.begin > previous)
        {
            previous = s.end;
            active = 1;
            ++result;
        }
        else ++active;
    }
    return result + 1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMinArrowShots(points);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{10, 16}, {2, 8}, {1, 6}, {7, 12}}, 2, trials);
    test({{10, 16}, {2, 8}, {1, 6}, {7, 12}, {6, 7}}, 2, trials);
    test({{1, 2}, {3, 4}, {5, 6}, {7, 8}}, 4, trials);
    test({{1, 2}, {2, 3}, {3, 4}, {4, 5}}, 2, trials);
    return 0;
}


