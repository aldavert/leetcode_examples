#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minFlips(int a, int b, int c)
{
    int result = 0;
    for (int i = 0; i < 30; ++i, c >>= 1, a >>= 1, b >>= 1)
        result += (((c & 1) == 1) && ((a & 1) == 0) && ((b & 1) == 0))
               +  (((c & 1) == 0) && ((a & 1) == 1))
               +  (((c & 1) == 0) && ((b & 1) == 1));
    return result;
}

// ############################################################################
// ############################################################################

void test(int a, int b, int c, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minFlips(a, b, c);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 6, 5, 3, trials);
    test(4, 2, 7, 1, trials);
    test(1, 2, 3, 0, trials);
    test(8, 3, 5, 3, trials);
    return 0;
}


