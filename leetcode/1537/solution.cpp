#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxSum(std::vector<int> nums1, std::vector<int> nums2)
{
    constexpr int MOD = 1'000'000'007;
    const int n1 = static_cast<int>(nums1.size()),
              n2 = static_cast<int>(nums2.size());
    
    long result = 0, sum1 = 0, sum2 = 0;
    int i = 0, j = 0;

    while ((i < n1) && (j < n2))
    {
        if (nums1[i] < nums2[j])
            sum1 += nums1[i++];
        else if (nums1[i] > nums2[j])
            sum2 += nums2[j++];
        else
        {
            result += std::max(sum1, sum2) + nums1[i];
            sum1 = 0;
            sum2 = 0;
            ++i;
            ++j;
        }
    }
    while (i < n1) sum1 += nums1[i++];
    while (j < n2) sum2 += nums2[j++];
    
    return static_cast<int>((result + std::max(sum1, sum2)) % MOD);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSum(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4, 5, 8, 10}, {4, 6, 8, 9}, 30, trials);
    test({1, 3, 5, 7, 9}, {3, 5, 100}, 109, trials);
    test({1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}, 40, trials);
    return 0;
}


