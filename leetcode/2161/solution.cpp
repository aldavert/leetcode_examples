#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> pivotArray(std::vector<int> nums, int pivot)
{
    std::vector<int> result;
    
    for (int num : nums)
        if (num < pivot)
            result.push_back(num);
    for (int num : nums)
        if (num == pivot)
            result.push_back(num);
    for (int num : nums)
        if (num > pivot)
            result.push_back(num);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int pivot,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = pivotArray(nums, pivot);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({9, 12, 5, 10, 14, 3, 10}, 10, {9, 5, 3, 10, 10, 12, 14}, trials);
    test({-3, 4, 3, 2}, 2, {-3, 2, 4, 3}, trials);
    return 0;
}


