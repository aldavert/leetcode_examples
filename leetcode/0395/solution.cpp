#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestSubstring(std::string s, int k)
{
    const int ns = static_cast<int>(s.size());
    int result = 0;
    auto process = [&](int n) -> void
    {
        int unique_chr = 0, no_less_than_k = 0, histogram[26] = {};
        for (int l = 0, r = 0; r < ns; ++r)
        {
            unique_chr += (histogram[s[r] - 'a'] == 0);
            no_less_than_k += (++histogram[s[r] - 'a'] == k);
            for (; unique_chr > n; ++l)
            {
                no_less_than_k -= (histogram[s[l] - 'a'] == k);
                unique_chr -= (--histogram[s[l] - 'a'] == 0);
            }
            if (no_less_than_k == n)
                result = std::max(result, r - l + 1);
        }
    };
    for (int i = 1; i <= 26; ++i)
        process(i);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestSubstring(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aaabb", 3, 3, trials);
    test("ababbc", 2, 5, trials);
    test("zxytrsvwababbc", 2, 5, trials);
    return 0;
}


