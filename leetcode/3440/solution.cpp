#include "../common/common.hpp"
#include <map>

// ############################################################################
// ############################################################################

#if 1
int maxFreeTime(int eventTime,
                std::vector<int> startTime,
                std::vector<int> endTime)
{
    const int n = static_cast<int>(startTime.size());
    std::vector<int> free_times(n + 1);
    free_times[0] = startTime[0];
    for (int i = 1; i < n; ++i)
        free_times[i] = startTime[i] - endTime[i - 1];
    free_times[n] = eventTime - endTime[n - 1];
    
    int result = 0;
    for (int i = 0, max_free = 0; i < n; ++i)
    {
        int time = endTime[i] - startTime[i],
            free_left = free_times[i],
            free_right = free_times[i + 1];
        result = std::max(result, free_left + free_right + ((time <= max_free)?time:0));
        max_free = std::max(max_free, free_left);
    }
    for (int i = n - 1, max_free = 0; 0 <= i; --i)
    {
        int time = endTime[i] - startTime[i],
            free_left = free_times[i],
            free_right = free_times[i + 1];
        result = std::max(result, free_left + free_right + ((time <= max_free)?time:0));
        max_free = std::max(max_free, free_right);
    }
    return result;
}
#else
int maxFreeTime(int eventTime,
                std::vector<int> startTime,
                std::vector<int> endTime)
{
    if (startTime.size() == 0) return eventTime;
    if (endTime.empty() || (endTime.back() != eventTime)) // Add a stopper.
    {
        startTime.push_back(eventTime);
        endTime.push_back(eventTime);
    }
    std::vector<int> free_time;
    std::multimap<int, size_t> available_time;
    free_time.push_back(startTime[0]);
    available_time.insert({startTime[0], 0});
    int previous = endTime[0], result = 0;
    for (size_t i = 1; i < startTime.size(); ++i)
    {
        int time = startTime[i] - previous;
        result = std::max(result, time + free_time.back());
        free_time.push_back(time);
        available_time.insert({time, i});
        previous = endTime[i];
    }
    free_time.push_back(0);
    for (size_t i = 0; i < startTime.size(); ++i)
    {
        int time = endTime[i] - startTime[i];
        auto search = available_time.lower_bound(time);
        while ((search != available_time.end())
           &&  ((search->second == i) || (search->second == i + 1))) ++search;
        if (search != available_time.end())
            result = std::max(result, free_time[i] + free_time[i + 1] + time);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int eventTime,
          std::vector<int> startTime,
          std::vector<int> endTime,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxFreeTime(eventTime, startTime, endTime);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {1, 3}, {2, 5}, 2, trials);
    test(10, {0, 7, 9}, {1, 8, 10}, 7, trials);
    test(10, {0, 3, 7, 9}, {1, 4, 8, 10}, 6, trials);
    test(5, {0, 1, 2, 3, 4}, {1, 2, 3, 4, 5}, 0, trials);
    test(10, {0, 2, 9}, {1, 4, 10}, 6, trials);
    test(23, {0, 3, 5, 7, 11, 16, 19}, {1, 4, 6, 8, 14, 17, 21}, 6, trials);
    //      2        1     1     3                 2        2           2
    //      -----    --    --    --------          -----    -----       -----
    //  ####     ####  ####  ####        ##########     ####     #######
    // -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+->
    //  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
    test(21, {7, 10, 16}, {10, 14, 18}, 10, trials);
    //  7                                          2           3
    //  ---------------------                      ----        --------
    //                       #########|OOOOOOOOOOOO     #######
    // -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+->
    //  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
    test(10, {0, 3, 7}, {1, 4, 10}, 5, trials);
    test(99, {7, 21, 25}, {13, 25, 78}, 21, trials);
    return 0;
}


