#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long putMarbles(std::vector<int> weights, int k)
{
    const int n = static_cast<int>(weights.size());
    std::vector<int> marbles_score;
    for (int i = 1; i < n; ++i)
        marbles_score.push_back(weights[i] + weights[i - 1]);
    std::sort(marbles_score.begin(), marbles_score.end());
    
    long min = 0, max = 0;
    for (int i = 0; i < k - 1; ++i)
        min += marbles_score[i],
        max += marbles_score[n - 2 - i];
    return max - min;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> weights, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = putMarbles(weights, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 5, 1}, 2, 4, trials);
    test({1, 3}, 2, 0, trials);
    return 0;
}


