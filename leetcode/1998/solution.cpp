#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool gcdSort(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> sorted_nums(nums); 
    std::sort(sorted_nums.begin(), sorted_nums.end()); 
    int max_num = sorted_nums.back() + 1;
    std::vector<int> id(max_num);
    std::vector<bool> visited(max_num); 
    for (int v : sorted_nums)
        id[v] = v;
    auto find = [&](auto &&self, int u) -> int
    {
        return (id[u] == u)?u:id[u] = self(self, id[u]);
    };
    auto merge = [&](int i, int j) { id[find(find, i)] = id[find(find, j)]; };
    for (int i = 2; i < max_num; ++i)
    {
        if (!visited[i])
        {
            int first = 0;
            for(int k = i; k < max_num; k += i)
            {
                if (id[k])
                {
                    if (first) merge(first, k);
                    else first = k;
                }
                visited[k] = true;
            }
        }
    }
    for (int i = 0; i < n; ++i)
        if (find(find, nums[i]) != find(find, sorted_nums[i]))
            return false;
    return true;
}
#else
bool gcdSort(std::vector<int> nums)
{
    std::vector<int> sorted_nums(nums);
    std::sort(sorted_nums.begin(), sorted_nums.end());
    const int max_num = sorted_nums.back() + 1;
    std::vector<int> min_prime_factor(max_num + 1), id(max_num), rank(max_num);
    for (int i = 2; i <= max_num; ++i) min_prime_factor[i] = i;
    for (int i = 2; i * i <= max_num; ++i)
        if (min_prime_factor[i] == i)
            for (int j = i * i; j <= max_num; j += i)
                min_prime_factor[j] = std::min(min_prime_factor[j], i);
    for (int i = 0; i < max_num; ++i)
        id[i] = i;
    std::function<int(int)> find = [&](int u)
    {
        return (id[u] == u)?u:id[u] = find(id[u]);
    };
    auto merge = [&](int u, int v)
    {
        int i = find(u), j = find(v);
        if (i == j) return;
        if      (rank[i] < rank[j]) id[i] = id[j];
        else if (rank[i] > rank[j]) id[j] = id[i];
        else                        id[i] = id[j], ++rank[j];
    };
    std::function<std::vector<int>(int)> primeFactors =
        [&](int num) -> std::vector<int>
    {
        std::vector<int> prime_factors;
        while (num > 1)
        {
            int divisor = min_prime_factor[num];
            prime_factors.push_back(divisor);
            while (num % divisor == 0) num /= divisor;
        }
        return prime_factors;
    };
    
    for (int num : nums)
        for (auto factors = primeFactors(num); int pfactor : factors)
            merge(num, pfactor);
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        if (find(nums[i]) != find(sorted_nums[i]))
            return false;
    return true;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = gcdSort(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({7, 21, 3}, true, trials);
    test({5, 2, 6, 2}, false, trials);
    test({10, 5, 9, 3, 15}, true, trials);
    return 0;
}


