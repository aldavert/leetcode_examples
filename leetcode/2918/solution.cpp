#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

long long minSum(std::vector<int> nums1, std::vector<int> nums2)
{
    const long sum1 = std::accumulate(nums1.begin(), nums1.end(), 0L),
               sum2 = std::accumulate(nums2.begin(), nums2.end(), 0L),
               zero1 = std::count(nums1.begin(), nums1.end(), 0),
               zero2 = std::count(nums2.begin(), nums2.end(), 0);
    if ((zero1 == 0) && (sum1 < sum2 + zero2))
        return -1;
    if ((zero2 == 0) && (sum2 < sum1 + zero1))
        return -1;
    return std::max(sum1 + zero1, sum2 + zero2);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSum(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 0, 1, 0}, {6, 5, 0}, 12, trials);
    test({2, 0, 2, 0}, {1, 4}, -1, trials);
    return 0;
}


