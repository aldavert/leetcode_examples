#include "../common/common.hpp"
#include <set>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > permute(std::vector<int> nums)
{
    const size_t n = nums.size();
    std::vector<std::vector<int> > result;
    std::vector<bool> available(n, true);
    std::vector<int> trace;
    trace.reserve(n);
    auto backtrace = [&](auto &&self) -> void
    {
        if (trace.size() == nums.size())
            result.push_back(trace);
        else
        {
            for (size_t i = 0; i < n; ++i)
            {
                if (available[i])
                {
                    trace.push_back(nums[i]);
                    available[i] = false;
                    self(self);
                    available[i] = true;
                    trace.pop_back();
                }
            }
        }
    };
    backtrace(backtrace);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    std::set<std::vector<int> > unique(left.begin(), left.end());
    if ((unique.size() != right.size())
    ||  (unique.size() !=  left.size())) return false;
    for (const auto &r : right)
    {
        if (auto search = unique.find(r); search != unique.end())
            unique.erase(search);
        else return false;
    }
    return unique.empty();
}

void test(std::vector<int> nums,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = permute(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3},
         {{1, 2, 3}, {1, 3, 2}, {2, 1, 3}, {2, 3, 1}, {3, 1, 2}, {3, 2, 1}}, trials);
    test({0, 1}, {{0, 1}, {1, 0}}, trials);
    test({1}, {{1}}, trials);
    return 0;
}


