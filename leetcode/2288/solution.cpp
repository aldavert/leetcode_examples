#include "../common/common.hpp"
#include <sstream>

// ############################################################################
// ############################################################################

std::string discountPrices(std::string sentence, int discount)
{
    constexpr int PRECISION = 2;
    std::istringstream iss(sentence);
    std::string result;
    
    for (std::string word; iss >> word;)
    {
        if ((word[0] == '$') && (word.length() > 1))
        {
            const std::string digits = word.substr(1);
            if (std::all_of(digits.begin(), digits.end(),
                    [](const char digit) { return isdigit(digit); }))
            {
                const long double val = std::stold(digits) * (100 - discount) / 100;
                const std::string s = std::to_string(val);
                const std::string trimmed = s.substr(0, s.find(".") + PRECISION + 1);
                result += "$" + trimmed + " ";
            }
            else result += word + " ";
        }
        else
        result += word + " ";
    }
    result.pop_back();
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string sentence,
          int discount,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = discountPrices(sentence, discount);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("there are $1 $2 and 5$ candies in the shop", 50,
         "there are $0.50 $1.00 and 5$ candies in the shop", trials);
    test("1 2 $3 4 $5 $6 7 8$ $9 $10$", 100,
         "1 2 $0.00 4 $0.00 $0.00 7 8$ $0.00 $10$", trials);
    return 0;
}


