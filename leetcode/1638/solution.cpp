#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countSubstrings(std::string s, std::string t)
{
    auto count = [&](size_t i, size_t j) -> int
    {
        int result = 0, dp[2] = {};
        for (; (i < s.size()) && (j < t.size()); ++i, ++j)
        {
            if (s[i] == t[j]) ++dp[0];
            else
            {
                dp[1] = dp[0] + 1;
                dp[0] = 0;
            }
            result += dp[1];
        }
        return result;
    };
    int result = 0;
    for (size_t i = 0; i < s.size(); ++i)
        result += count(i, 0);
    for (size_t j = 1; j < t.size(); ++j)
        result += count(0, j);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string t, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSubstrings(s, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aba", "baba", 6, trials);
    test("ab", "bb", 3, trials);
    return 0;
}


