#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int largest1BorderedSquare(std::vector<std::vector<int> > grid)
{
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    std::vector<std::vector<int> > left_ones(m, std::vector<int>(n));
    std::vector<std::vector<int> > top_ones(m, std::vector<int>(n));
    
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (grid[i][j] == 1)
                left_ones[i][j] = (j == 0)?1:1 + left_ones[i][j - 1],
                top_ones[i][j] = (i == 0)?1:1 + top_ones[i - 1][j];
    for (int sz = std::min(m, n); sz > 0; --sz)
        for (int i = 0; i + sz - 1 < m; ++i)
            for (int j = 0; j + sz - 1 < n; ++j)
                if (int x = i + sz - 1, y = j + sz - 1;
                    (std::min(left_ones[i][y], left_ones[x][y]) >= sz)
                &&  (std::min( top_ones[x][j],  top_ones[x][y]) >= sz))
                    return sz * sz;
    return 0;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largest1BorderedSquare(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 1}, {1, 0, 1}, {1, 1, 1}}, 9, trials);
    test({{1, 1, 0, 0}}, 1, trials);
    return 0;
}


