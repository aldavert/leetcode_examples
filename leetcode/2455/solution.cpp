#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int averageValue(std::vector<int> nums)
{
    int number_of_values = 0, result = 0;
    for (int value : nums)
        if ((value % 2 == 0) && (value % 3 == 0))
            result += value,
            ++number_of_values;
    return (number_of_values > 0)?result / number_of_values:0;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = averageValue(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 6, 10, 12, 15}, 9, trials);
    test({1, 2, 4, 7, 10}, 0, trials);
    test({4, 4, 9, 10}, 0, trials);
    return 0;
}


