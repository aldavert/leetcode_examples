#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestValidParentheses(std::string s)
{
    // (  (  (  )  (  )  )  (  (  )  )  (  )  )
    // ·  ·  ·  2  ·  4  6  ·  ·  2 10  · 12 14
    //          |<----+  |        |<-+     |  |
    //                |<-+           |<----+  |
    //                   |<----------+     |<-+
    // (  (  (  )  (  )  )  (  )  )  (  )
    // ·  ·  ·  2  ·  4  6  ·  8 10  · 12
    //          |<----+  |     |  |     |
    //                |<-+     |  |     |
    //                   |<----+  |     |
    //                         |<-+     |
    //                            |<----+
    const int n = static_cast<int>(s.size());
    if (n == 0) return 0;
    std::vector<int> valid(n);
    int stack = 0, result = 0;
    for (int i = 0; i < n; ++i)
    {
        if (s[i] == '(')
        {
            valid[i] = 0;
            ++stack;
        }
        else
        {
            if (stack > 0)
            {
                valid[i] = valid[i - 1] + 2;
                int back = i - valid[i];
                if (back >= 0)
                    valid[i] += valid[back];
                --stack;
                result = std::max(result, valid[i]);
            }
            else valid[i] = 0;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int t = 0; t < trials; ++t)
        result = longestValidParentheses(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("(()", 2, trials);
    test(")()())", 4, trials);
    test("", 0, trials);
    test("((()())(())())", 14, trials);
    test("((()())())()", 12, trials);
    return 0;
}


