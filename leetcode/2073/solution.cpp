#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int timeRequiredToBuy(std::vector<int> tickets, int k)
{
    const int n = static_cast<int>(tickets.size());
    int excess_ahead = 0, excess_behind = 0;
    for (int i = 0; i < k; ++i)
        excess_ahead  += std::max(0, tickets[k] - tickets[i]);
    for (int i = k + 1; i < n; ++i)
        excess_behind += std::max(0, tickets[k] - tickets[i] - 1);
    return k + 1 + (tickets[k] - 1) * n - excess_ahead - excess_behind;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tickets, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = timeRequiredToBuy(tickets, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 2}, 0, 4, trials);
    test({2, 3, 2}, 2, 6, trials);
    test({5, 1, 1, 1}, 0, 8, trials);
    test({1, 5, 1, 1}, 1, 8, trials);
    test({1, 1, 5, 1}, 2, 8, trials);
    test({1, 1, 1, 5}, 3, 8, trials);
    test({5, 2, 2, 2}, 0, 11, trials);
    test({2, 5, 2, 2}, 1, 11, trials);
    test({2, 2, 5, 2}, 2, 11, trials);
    test({2, 2, 2, 5}, 3, 11, trials);
    return 0;
}


