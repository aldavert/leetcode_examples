#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
bool findSafeWalk(std::vector<std::vector<int> > grid, int health)
{
    const int n = static_cast<int>(grid.size()),
              m = static_cast<int>(grid[0].size());
    std::vector<std::vector<int> > distance(n, std::vector<int>(m, 1e9));
    std::queue<std::pair<int, int> > q;
    q.push({0, 0});
    distance[0][0] = grid[0][0] == 1;
    while (!q.empty())
    {
        auto [x, y] = q.front();
        q.pop();
        if ((x + 1 < n) && (distance[x + 1][y] > distance[x][y] + grid[x + 1][y]))
        {
            q.push({x + 1, y});
            distance[x + 1][y] = distance[x][y] + grid[x + 1][y];
        }
        if ((x - 1 >= 0) && (distance[x - 1][y] > distance[x][y] + grid[x - 1][y]))
        {
            q.push({x - 1, y});
            distance[x - 1][y] = distance[x][y] + grid[x - 1][y];
        }
        if ((y + 1 < m) && (distance[x][y + 1] > distance[x][y] + grid[x][y + 1]))
        {
            q.push({x, y + 1});
            distance[x][y + 1] = distance[x][y] + grid[x][y + 1];
        }
        if ((y - 1 >= 0) && (distance[x][y - 1] > distance[x][y] + grid[x][y - 1]))
        {
            q.push({x, y - 1});
            distance[x][y - 1] = distance[x][y] + grid[x][y - 1];
        }
    }
    return distance[n - 1][m - 1] < health;
}
#else
bool findSafeWalk(std::vector<std::vector<int> > grid, int health)
{
    constexpr int dirs[4][2] = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    const int initial_health = health - grid[0][0];
    std::queue<std::tuple<int, int, int>> q{{{0, 0, initial_health}}};
    std::vector<std::vector<std::vector<bool> > > seen(m,
            std::vector<std::vector<bool> >(n, std::vector<bool>(health + 1)));
    seen[0][0][initial_health] = true;
    while (!q.empty())
    {
        for (int sz = static_cast<int>(q.size()); sz > 0; --sz)
        {
            const auto [i, j, h] = q.front();
            q.pop();
            if ((i == m - 1) && (j == n - 1) && (h > 0)) return true;
            for (const auto& [dx, dy] : dirs)
            {
                const int x = i + dx, y = j + dy;
                if ((x < 0) || (x == m) || (y < 0) || (y == n)) continue;
                const int next_health = h - grid[x][y];
                if ((next_health <= 0) || seen[x][y][next_health]) continue;
                q.emplace(x, y, next_health);
                seen[x][y][next_health] = true;
            }
        }
    }
    return false;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          int health,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = findSafeWalk(grid, health);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 0, 0, 0},
          {0, 1, 0, 1, 0},
          {0, 0, 0, 1, 0}}, 1, true, trials);
    test({{0, 1, 1, 0, 0, 0},
          {1, 0, 1, 0, 0, 0},
          {0, 1, 1, 1, 0, 1},
          {0, 0, 1, 0, 1, 0}}, 3, false, trials);
    test({{1, 1, 1},
          {1, 0, 1},
          {1, 1, 1}}, 5, true, trials);
    return 0;
}


