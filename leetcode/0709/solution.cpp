#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string toLowerCase(std::string s)
{
    for (char &c : s)
        if ((c >= 'A') && (c <= 'Z'))
            c = c - 'A' + 'a';
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = toLowerCase(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("Hello", "hello", trials);
    test("here", "here", trials);
    test("LOVELY", "lovely", trials);
    return 0;
}


