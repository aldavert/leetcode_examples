#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::string evaluate(std::string s, std::vector<std::vector<std::string> > knowledge)
{
    std::unordered_map<std::string, std::string> dictionary;
    std::string result;
    
    for (const auto &entry : knowledge)
        dictionary["(" + entry[0] + ")"] = entry[1];
    for (size_t i = 0, n = s.size(); i < n; ++i)
    {
        if (s[i] == '(')
        {
            size_t j = s.find_first_of(')', i);
            auto search = dictionary.find(s.substr(i, j - i + 1));
            result += (search != dictionary.end())?search->second:"?";
            i = j;
        }
        else result += s[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<std::vector<std::string> > knowledge,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = evaluate(s, knowledge);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("(name)is(age)yearsold", {{"name", "bob"}, {"age", "two"}},
         "bobistwoyearsold", trials);
    test("hi(name)", {{"a", "b"}}, "hi?", trials);
    test("(a)(a)(a)aaa", {{"a", "yes"}}, "yesyesyesaaa", trials);
    return 0;
}


