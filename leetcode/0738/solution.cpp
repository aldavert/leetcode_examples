#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int monotoneIncreasingDigits(int n)
{
    std::string s = std::to_string(n);
    const size_t nchars = s.size();
    if (nchars == 0) return 0;
    size_t k = nchars;
    
    for (size_t i = nchars - 1; i > 0; --i)
        if (s[i] < s[i - 1])
            --s[i - 1],
            k = i;
    for (size_t i = k; i < nchars; ++i)
        s[i] = '9';
    
    return std::stoi(s);
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = monotoneIncreasingDigits(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, 9, trials);
    test(1234, 1234, trials);
    test(332, 299, trials);
    return 0;
}


