#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 0
long long appealSum(std::string s)
{
    long last_occurrence[] = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                               -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                               -1, -1, -1, -1, -1, -1 }, dp = 0, result = 0;
    for (size_t i = 0; i < s.size(); ++i)
    {
        dp += i - last_occurrence[s[i] - 'a'];
        result += dp;
        last_occurrence[s[i] - 'a'] = i;
    }
    return result;
}
#else
long long appealSum(std::string s)
{
    long last_occurrence[] = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                               -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                               -1, -1, -1, -1, -1, -1 }, result = 0;
    for (size_t i = 0; i < s.size(); ++i)
    {
        result += (i - last_occurrence[s[i] - 'a']) * (s.size() - i);
        last_occurrence[s[i] - 'a'] = i;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = appealSum(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abbca", 28, trials);
    test("code", 20, trials);
    return 0;
}


