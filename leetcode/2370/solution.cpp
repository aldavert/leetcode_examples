#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestIdealString(std::string s, int k)
{
    int longest[26] = {};
    for (char letter : s)
    {
        int idx = static_cast<int>(letter - 'a'), current = 0;
        for (int j = std::max(0, idx - k), n = std::min(25, idx + k); j <= n; ++j)
            current = std::max(current, longest[j]);
        longest[idx] = 1 + current;
    }
    return *std::max_element(&longest[0], &longest[26]);
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestIdealString(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("acfgbd", 2, 4, trials);
    test("abcd", 3, 4, trials);
    test("azaza", 25, 5, trials);
    return 0;
}


