#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

int maximumScore(std::vector<int> nums, int k)
{
    struct Info
    {
        int index = -1;
        int n_primes = -1;
        int num = -1;
        bool operator<(const Info &other) const { return num > other.num; }
    };
    constexpr int MOD = 1'000'000'007;
    int n = static_cast<int>(nums.size());
    std::vector<Info> values(n);
    for (int i = 0; i < n; ++i)
    {
        std::unordered_set<int> primes;
        int n_primes = nums[i];
        for (int j = 2; j <= n_primes / j; ++j)
            for (; n_primes % j == 0; n_primes /= j)
                primes.insert(j);
        values[i] = {i, static_cast<int>(primes.size()) + (n_primes > 1), nums[i]};
    }
    std::vector<int> left(n, -1), right(n, n);
    std::stack<int> stack;
    for (auto [index, n_primes, _] : values)
    {
        while (!stack.empty() && (values[stack.top()].n_primes < n_primes))
            stack.pop();
        if (!stack.empty())
            left[index] = stack.top();
        stack.push(index);
    }
    stack = std::stack<int>();
    for (int i = n - 1; ~i; --i)
    {
        while (!stack.empty() && (values[stack.top()].n_primes <= values[i].n_primes))
            stack.pop();
        if (!stack.empty())
            right[i] = stack.top();
        stack.push(i);
    }
    std::sort(values.begin(), values.end());
    long long result = 1;
    auto qpow = [&](long long a, long long m)
    {
        long long out = 1;
        for (; m; m >>= 1, a = a * a % MOD)
            if (m & 1)
                out = out * a % MOD;
        return out;
    };
    for (auto [index, _, num] : values)
    {
        int l = left[index], r = right[index];
        long long count = static_cast<long long>(index - l) * (r - index);
        if (count <= k)
        {
            result = result * qpow(num, count) % MOD;
            k -= static_cast<int>(count);
        }
        else
        {
            result = result * qpow(num, k) % MOD;
            break;
        }
    }
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumScore(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 3, 9, 3, 8}, 2, 81, trials);
    test({19, 12, 14, 6, 10, 18}, 3, 4788, trials);
    return 0;
}


