#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkString(std::string s)
{
    for (bool b = false; char letter : s)
    {
        if (letter == 'a')
        {
            if (b) return false;
        }
        else b = true;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aaabbb",  true, trials);
    test(  "abab", false, trials);
    test(   "bbb",  true, trials);
    return 0;
}


