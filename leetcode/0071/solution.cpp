#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

std::string simplifyPath(std::string path)
{
    const int n = static_cast<int>(path.size());
    std::stack<std::string> folders;
    int begin, end;
    auto process = [&](void) -> void
    {
        if (begin < end)
        {
            auto folder_name = path.substr(begin, end - begin);
            if (folder_name == "..")
            {
                if (!folders.empty())
                    folders.pop();
            }
            else if (folder_name != ".")
                folders.push(folder_name);
        }
    };
    for (begin = end = 0; end < n; ++end)
    {
        if (path[end] == '/')
        {
            process();
            begin = end + 1;
        }
    }
    process();
    std::string result;
    while (!folders.empty())
    {
        result = '/' + folders.top() + result;
        folders.pop();
    }
    return (result.empty())?"/":result;
}

// ############################################################################
// ############################################################################

void test(std::string path, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = simplifyPath(path);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("/home/", "/home", trials);
    test("/../", "/", trials);
    test("/home//foo/", "/home/foo", trials);
    test("/home/../foo/", "/foo", trials);
    test("/a//b////c/d//././/..", "/a/b/c", trials);
    return 0;
}


