#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int countSubarrays(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    int index = static_cast<int>(std::find(nums.begin(), nums.end(), k) - nums.begin()),
        result = 0;
    std::unordered_map<int, int> count;
    
    for (int i = index, balance = 0; i >= 0; --i)
        ++count[balance += 2 * (nums[i] > k) - 1 + (nums[i] == k)];
    for (int i = index, balance = 0; i < n; ++i)
    {
        balance += 2 * (nums[i] > k) - 1 + (nums[i] == k);
        result += count[-balance] + count[1 - balance];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSubarrays(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 1, 4, 5}, 4, 3, trials);
    test({2, 3, 1}, 3, 1, trials);
    return 0;
}


