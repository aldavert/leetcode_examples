#include "../common/common.hpp"
#include <map>

#if 1
std::vector<int> dailyTemperatures(std::vector<int> temperatures)
{
    const int n = static_cast<int>(temperatures.size());
    std::vector<int> result(n, 0), stack(n);
    for (int i = n - 1; i >= 0; --i)
    {
        int j = i + 1;
        while ((j < n) && (temperatures[j] <= temperatures[i]))
            j = (result[j] > 0)?result[j] + j:n;
        if (j < n) result[i] = j - i;
    }
    return result;
}
#elif 0
std::vector<int> dailyTemperatures(std::vector<int> temperatures)
{
    const int n = static_cast<int>(temperatures.size());
    const int Tm = 100 - 30 + 1;
    std::vector<int> result(n, 0);
    std::vector<int> positions(Tm, n);
    for (int i = n - 1; i >= 0; --i)
    {
        const int idx = temperatures[i] - 30;
        int closest = n;
        for (int j = idx + 1; j < Tm; ++j)
            closest = std::min(closest, positions[j]);
        if (closest < n)
            result[i] = closest - i;
        positions[idx] = i;
    }
    return result;
}
#else
std::vector<int> dailyTemperatures(std::vector<int> temperatures)
{
    std::vector<int> result(temperatures.size(), 0);
    std::map<int, int> positions;
    const int n = static_cast<int>(temperatures.size());
    for (int i = n - 1; i >= 0; --i)
    {
        if (auto it = positions.upper_bound(temperatures[i]); it != positions.end())
        {
            int closest = n;
            for (auto search = it; search != positions.end(); ++search)
                closest = std::min(closest, search->second);
            result[i] = closest - i;
            if (it->first == temperatures[i])
                it->second = i;
            else positions[temperatures[i]] = i;
        }
        else positions[temperatures[i]] = i;
    }
    return result;
}
#endif

void test(std::vector<int> temperatures, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = dailyTemperatures(temperatures);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({73, 74, 75, 71, 69, 72, 76, 73}, {1, 1, 4, 2, 1, 1, 0, 0}, trials);
    test({30, 40, 50, 60}, {1, 1, 1, 0}, trials);
    test({30, 60, 90}, {1, 1, 0}, trials);
    test({89, 62, 70, 58, 47, 47, 46, 76, 100, 70},  {8, 1, 5, 4, 3, 2, 1, 1, 0, 0},  trials);
    return 0;
}


