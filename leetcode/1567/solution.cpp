#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int getMaxLen(std::vector<int> nums)
{
    int result = 0;
    
    for (int negative = 0, positive = 0; int num : nums)
    {
        positive = (num != 0) * (positive + 1);
        negative = ((num != 0) && (negative != 0)) * (negative + 1);
        if (num < 0) std::swap(positive, negative);
        result = std::max(result, positive);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getMaxLen(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, -2, -3, 4}, 4, trials);
    test({0, 1, -2, -3, -4}, 3, trials);
    test({-1, -2, -3, 0, 1}, 2, trials);
    return 0;
}


