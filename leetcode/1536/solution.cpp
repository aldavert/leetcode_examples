#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minSwaps(std::vector<std::vector<int> > grid)
{
    const int n = static_cast<int>(grid.size());
    int result = 0;
    std::vector<int> suffix_zeros;
    
    for (const auto &row : grid)
        suffix_zeros.push_back(static_cast<int>(distance(row.rbegin(),
                                        std::find(row.rbegin(), row.rend(), 1))));
    for (int i = 0; i < n; ++i)
    {
        int needed_zeros = n - 1 - i;
        auto it = std::find_if(suffix_zeros.begin() + i, suffix_zeros.end(),
                                [&](int count) { return count >= needed_zeros; });
        if (it == suffix_zeros.end())
            return -1;
        int j = static_cast<int>(distance(suffix_zeros.begin(), it));
        for (int k = j; k > i; --k)
            suffix_zeros[k] = suffix_zeros[k - 1];
        result += j - i;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSwaps(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0, 1}, {1, 1, 0}, {1, 0, 0}}, 3, trials);
    test({{0, 1, 1, 0}, {0, 1, 1, 0}, {0, 1, 1, 0}, {0, 1, 1, 0}}, -1, trials);
    test({{1, 0, 0}, {1, 1, 0}, {1, 1, 1}}, 0, trials);
    return 0;
}


