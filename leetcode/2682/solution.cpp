#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> circularGameLosers(int n, int k)
{
    bool visited[50] = {};
    for (int position = 0, i = 1; true; ++i)
    {
        if (visited[position]) break;
        visited[position] = true;
        position = (position + i * k) % n;
    }
    std::vector<int> result;
    for (int i = 0; i < n; ++i)
        if (!visited[i])
            result.push_back(i + 1);
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int k, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = circularGameLosers(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 2, {4, 5}, trials);
    test(4, 4, {2, 3, 4}, trials);
    return 0;
}


