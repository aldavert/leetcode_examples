#include "../common/common.hpp"

// ############################################################################
// ############################################################################

class RLEIterator
{
    size_t m_index;
    std::vector<int> m_encoding;
public:
    RLEIterator(std::vector<int> encoding) : m_index(0), m_encoding(encoding) {}
    int next(int n)
    {
        while ((m_index < m_encoding.size()) && (m_encoding[m_index] < n))
            n -= m_encoding[m_index],
            m_index += 2;
        if (m_index == m_encoding.size()) return -1;
        m_encoding[m_index] -= n;
        return m_encoding[m_index + 1];
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<int> encoding,
          std::vector<int> input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        RLEIterator object(encoding);
        result.clear();
        for (int in : input)
            result.push_back(object.next(in));
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 8, 0, 9, 2, 5}, {2, 1, 1, 2}, {8, 8, 5, -1}, trials);
    return 0;
}


