#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long waysToBuyPensPencils(int total, int cost1, int cost2)
{
    long result = 0;
    for (int i = 0, max_pen = total / cost1; i <= max_pen; ++i)
        result += (total - i * cost1) / cost2 + 1;
    return result;
}

// ############################################################################
// ############################################################################

void test(int total, int cost1, int cost2, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = waysToBuyPensPencils(total, cost1, cost2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(20, 10, 5, 9, trials);
    test(5, 10, 10, 1, trials);
    return 0;
}


