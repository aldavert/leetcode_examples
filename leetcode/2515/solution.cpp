#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int closetTarget(std::vector<std::string> words, std::string target, int startIndex)
{
    const int n = static_cast<int>(words.size());
    int min_distance = std::numeric_limits<int>::max();
    for (int i = 0; i < n; ++i)
        if (words[i] == target)
        {
            int diff = std::abs(i - startIndex);
            min_distance = std::min(min_distance, ((diff > n / 2)?(n - diff):diff));
        }
    return (min_distance < n)?min_distance:-1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::string target,
          int startIndex,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = closetTarget(words, target, startIndex);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"hello", "i", "am", "leetcode", "hello"}, "hello", 1, 1, trials);
    test({"a", "b", "leetcode"}, "leetcode", 0, 1, trials);
    test({"i", "eat", "leetcode"}, "ate", 0, -1, trials);
    test({"hsdqinnoha", "mqhskgeqzr", "zemkwvqrww", "zemkwvqrww", "daljcrktje",
          "fghofclnwp", "djwdworyka", "cxfpybanhd", "fghofclnwp", "fghofclnwp"},
          "zemkwvqrww", 8, 4);
    return 0;
}


