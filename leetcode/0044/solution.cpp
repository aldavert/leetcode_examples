#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool isMatch(std::string s, std::string p)
{
    const int n = static_cast<int>(s.size());
    const int m = static_cast<int>(p.size());
    int p_idx = 0;
    
    for (int s_idx = 0, star = -1, count = 0; s_idx < n; )
    {
        if (p[p_idx] == '*')
        {
            count = s_idx;
            star = p_idx++;
        }
        else if ((p[p_idx] == s[s_idx]) || (p[p_idx] == '?'))
        {
            ++p_idx;
            ++s_idx;
        }
        else if (star >= 0)
        {
            p_idx = star + 1;
            s_idx = ++count;
        }
        else return false;
    }
    
    while ((p_idx < m) && (p[p_idx] == '*')) ++p_idx;
    return p_idx == m;
}
#else
bool isMatch(std::string s, std::string p)
{
    // Code adapted from problem 10.
    const int n = static_cast<int>(s.size()),
              m = static_cast<int>(p.size());
    std::vector<bool> lut((n + 1) * (m + 1), false);
    auto match = [&](auto &&self, int idx_s, int idx_p) -> bool
    {
        const int call_id = idx_s * m + idx_p;
        if (lut[call_id]) return false;
        lut[call_id] = true;
        while (true)
        {
            if ((idx_s == n) && (idx_p == m))
                return true;
            else if ((idx_s < n) && (idx_p >= m))
                return false;
            
            if (p[idx_p] == '*') // Any
            {
                // Avoid the repetition pattern (0 repetitions).
                if (self(self, idx_s, idx_p + 1))
                    return true;
                // Rest of the cases.
                for (int repeat_s = idx_s; repeat_s < n; ++repeat_s)
                    if (self(self, repeat_s + 1, idx_p + 1))
                        return true;
                return false;
            }
            else
            {
                if (idx_s == n)
                    return false;
                if ((p[idx_p] == '?') || (s[idx_s] == p[idx_p]))
                {
                    ++idx_s;
                    ++idx_p;
                }
                else return false;
            }
        }
    };
    return match(match, 0, 0);
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, std::string p, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isMatch(s, p);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aa", "a", false, trials);
    test("aa", "*", true, trials);
    test("cb", "?a", false, trials);
    test("adceb", "*a*b", true, trials);
    test("acdcb", "a*c?b", false, trials);
    test("", "******", true, trials);
    return 0;
}


