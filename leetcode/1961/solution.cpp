#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isPrefixString(std::string s, std::vector<std::string> words)
{
    const size_t n = words.size();
    const size_t m = s.size();
    for (size_t i = 0, j = 0; j < m; ++i)
    {
        if (i == n) return false;
        if (j + words[i].size() > m) return false;
        if (s.substr(j, words[i].size()) != words[i]) return false;
        j += words[i].size();
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<std::string> words,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPrefixString(s, words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("iloveleetcode", {"i", "love", "leetcode", "apples"}, true, trials);
    test("iloveleetcode", {"apples", "i", "love", "leetcode"}, false, trials);
    return 0;
}


