#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int atMostNGivenDigitSet(std::vector<std::string> digits, int n)
{
    const int m = static_cast<int>(digits.size());
    std::string str_n = std::to_string(n);
    int ndigits = static_cast<int>(str_n.size());
    int result = 0, prod = 1;
    for (int i = 1; i < ndigits; ++i)
        result += prod *= m;
    for (int i = 0; i < ndigits; ++i)
    {
        char c = str_n[i];
        auto search = std::lower_bound(digits.begin(), digits.end(), std::string(1, c));
        int d = static_cast<int>(std::distance(digits.begin(), search));
        result += prod * d;
        if ((d == m) || (digits[d][0] != c))
            return result;
        prod /= m;
    }
    return result + 1;
}
#else
int atMostNGivenDigitSet(std::vector<std::string> digits, int n)
{
    const int m = static_cast<int>(digits.size());
    std::string str_n = std::to_string(n);
    int ndigits = static_cast<int>(str_n.size());
    int result = 0, prod = 1;
    for (int i = 1; i < ndigits; ++i)
        result += prod *= m;
    for (int i = 0; i < ndigits; ++i)
    {
        char c = str_n[i];
        bool has_same = false;
        for (const auto &x : digits)
        {
            if (x[0] < c)
                result += prod;
            else if (x[0] == c)
                has_same = true;
        }
        prod /= m;
        if (!has_same) return result;
    }
    return result + 1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> digits, int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = atMostNGivenDigitSet(digits, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"1", "3", "5", "7"}, 36, 11, trials);
    test({"1", "3", "5", "7"}, 100, 20, trials);
    test({"1", "4", "9"}, 1000000000, 29523, trials);
    test({"7"}, 8, 1, trials);
    test({"3", "4", "8"}, 4, 2, trials);
    test({"1", "2", "3", "4", "5", "6", "7", "9"}, 1, 1, trials);
    test({"5", "6"}, 19, 2, trials);
    test({"5", "7", "8"}, 59, 6, trials);
    return 0;
}


