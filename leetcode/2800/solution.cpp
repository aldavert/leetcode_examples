#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string minimumString(std::string a, std::string b, std::string c)
{
    auto merge = [](std::string l, std::string r) -> std::string 
    {
        if (l.find(r) != std::string::npos) return l;
        if (r.find(l) != std::string::npos) return r;
        int m = static_cast<int>(l.size()), n = static_cast<int>(r.size());
        for (int i = std::min(m, n); i > 0; --i)
            if (l.substr(m - i) == r.substr(0, i))
                return l + r.substr(i);
        return l + r;
    };
    std::vector<std::string> s = {a, b, c};
    std::vector<std::vector<int> > perm = {
        {0, 1, 2}, {0, 2, 1}, {1, 0, 2}, {1, 2, 0}, {2, 1, 0}, {2, 0, 1}};
    std::string result = "";
    for (auto& p : perm)
    {
        int i = p[0], j = p[1], k = p[2];
        std::string t = merge(merge(s[i], s[j]), s[k]);
        if ((result == "")
        ||  (t.size() < result.size())
        ||  ((t.size() == result.size()) && (t < result)))
            result = t;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string a,
          std::string b,
          std::string c,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumString(a, b, c);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", "bca", "aaa", "aaabca", trials);
    test("ab", "ba", "aba", "aba", trials);
    return 0;
}


