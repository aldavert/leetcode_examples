#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long countSubarrays(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    int maximum = nums[0];
    for (size_t i = 1; i < nums.size(); ++i)
        maximum = std::max(maximum, nums[i]);
    long long result = 0;
    for (int l = 0, r = 0, count = 0; r < n; ++r)
    {
        count += (nums[r] == maximum);
        while (count == k)
            count -= (nums[l++] == maximum);
        result += l;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSubarrays(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 3, 3}, 2, 6, trials);
    test({1, 4, 2, 1}, 3, 0, trials);
    return 0;
}


