#include "../common/common.hpp"
#include <fstream>
#include <iterator>

// ############################################################################
// ############################################################################

#if 1
int totalHammingDistance(std::vector<int> nums)
{
    int bits[32] = {};
    for (size_t i = 0, n = nums.size(); i < n; ++i)
        for (size_t j = 0, value = nums[i]; value; ++j, value >>= 1)
            bits[j] += value & 1;
    int result = 0;
    for (size_t i = 0, n = nums.size(); i < 30; ++i)
        result += static_cast<int>(bits[i] * (n - bits[i]));
    return result;
}
#else
int totalHammingDistance(std::vector<int> nums)
{
    int result = 0;
    for (size_t i = 0, n = nums.size(); i < n; ++i)
        for (size_t j = i + 1; j < n; ++j)
            result += std::popcount(static_cast<size_t>(nums[i] ^ nums[j]));
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = totalHammingDistance(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 14, 2}, 6, trials);
    test({4, 14, 4}, 4, trials);
    test({1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 95, trials);
    std::vector<int> data;
    std::ifstream file("data.txt");
    for (std::istream_iterator<std::string> begin(file), end; begin != end; ++begin)
        data.push_back(std::atoi(begin->c_str()));
    file.close();
    test(data, 748'454'872, trials);
    return 0;
}


