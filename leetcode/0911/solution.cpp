#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

class TopVotedCandidate
{
    std::vector<int> m_times;
    std::unordered_map<int, int> m_time_to_lead;
public:
    TopVotedCandidate(std::vector<int> persons, std::vector<int> times) :
        m_times(times)
    {
        std::unordered_map<int, int> frequency;
        int lead = -1;
        for (size_t i = 0; i < persons.size(); ++i)
        {
            if (++frequency[persons[i]] >= frequency[lead])
                lead = persons[i];
            m_time_to_lead[times[i]] = lead;
        }
    }
    int q(int t)
    {
        return m_time_to_lead[*(--std::upper_bound(m_times.begin(), m_times.end(), t))];
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<int> persons,
          std::vector<int> times,
          std::vector<int> input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        TopVotedCandidate object(persons, times);
        result.clear();
        for (int value : input)
            result.push_back(object.q(value));
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 1, 0, 0, 1, 0},
         {0, 5, 10, 15, 20, 25, 30},
         {3, 12, 25, 15, 24, 8},
         {0, 1, 1, 0, 0, 1}, trials);
    return 0;
}


