#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minimumJumps(std::vector<int> forbidden, int a, int b, int x)
{
    enum class Direction { FORWARD, BACKWARD };
    int furthest = x + a + b;
    std::unordered_set<int> seen_forward, seen_backward;
    std::queue<std::pair<Direction, int> > q{{{Direction::FORWARD, 0}}};
    
    for (int pos : forbidden)
    {
        seen_forward.insert(pos);
        seen_backward.insert(pos);
        furthest = std::max(furthest, pos + a + b);
    }
    for (int result = 0; !q.empty(); ++result)
    {
        for (int sz = static_cast<int>(q.size()); sz > 0; --sz)
        {
            const auto [dir, pos] = q.front();
            q.pop();
            if (pos == x) return result;
            const int forward = pos + a, backward = pos - b;
            if ((forward <= furthest) && seen_forward.insert(forward).second)
                q.emplace(Direction::FORWARD, forward);
            if ((dir == Direction::FORWARD) && (backward >= 0)
            &&  seen_backward.insert(backward).second)
                q.emplace(Direction::BACKWARD, backward);
        }
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> forbidden,
          int a,
          int b,
          int x,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumJumps(forbidden, a, b, x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({14, 4, 18, 1, 15}, 3, 15, 9, 3, trials);
    test({8, 3, 16, 6, 12, 20}, 15, 13, 11, -1, trials);
    test({1, 6, 2, 14, 5, 17, 4}, 16, 9, 7, 2, trials);
    return 0;
}


