#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findRightInterval(std::vector<std::vector<int> > intervals)
{
    std::vector<std::tuple<int, int> > indexes;
    for (int i = 0, n = static_cast<int>(intervals.size()); i < n; ++i)
        indexes.push_back({intervals[i][0], i});
    std::sort(indexes.begin(), indexes.end());
    std::vector<int> result;
    for (const auto &interval : intervals)
    {
        auto search = std::upper_bound(indexes.begin(), indexes.end(), interval[1],
                [](int value, const std::tuple<int, int> &element)
                { return std::get<0>(element) >= value; });
        if (search == indexes.end()) { result.push_back(-1); continue; }
        result.push_back(std::get<1>(*search));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > intervals,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findRightInterval(intervals);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}}, {-1}, trials);
    test({{3, 4}, {2, 3}, {1, 2}}, {-1, 0, 1}, trials);
    test({{1, 4}, {2, 3}, {3, 4}}, {-1, 2, -1}, trials);
    return 0;
}


