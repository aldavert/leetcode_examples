#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int partitionDisjoint(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> min_value(n);
    for (int i = n - 1, previous = nums[n - 1]; i >= 0; --i)
        previous = min_value[i] = std::min(nums[i], previous);
    int max = nums[0];
    for (int i = 1; i < n; ++i)
    {
        if (max <= min_value[i]) return i;
        max = std::max(max, nums[i]);
    }
    return 1;
}
#else
int partitionDisjoint(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> max_value(n), min_value(n);
    for (int i = n - 1, previous = nums[n - 1]; i >= 0; --i)
        previous = min_value[i] = std::min(nums[i], previous);
    for (int i = 0, previous = nums[0]; i < n; ++i)
        previous = max_value[i] = std::max(nums[i], previous);
    int threshold = min_value[0];
    int idx = 0;
    if (min_value[0] < min_value[1]) return 1;
    while (true)
    {
        int search = idx;
        while ((search < n - 1) && (min_value[search] <= threshold)) ++search;
        --search;
        if (max_value[search] > threshold)
            threshold = max_value[search];
        else break;
    }
    return idx + 1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = partitionDisjoint(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 0, 3, 8, 6}, 3, trials);
    test({1, 1, 1, 0, 6, 12}, 4, trials);
    test({0, 1, 1, 0, 5, -1, 4, 6, 12}, 7, trials);
    test({0, 0, 0, 0, 0, 0, 0, 0, 0}, 1, trials);
    test({1, 1}, 1, trials);
    test({6, 0, 8, 30, 37, 6, 75, 98, 39, 90, 63, 74, 52, 92, 64}, 2, trials);
    test({1, 0, 2, 0, 2}, 4, trials);
    test({92, 6, 0, 8, 30, 37, 6, 75, 39, 90, 63, 74, 52, 64, 98}, 14, trials);
    return 0;
}


