#include "../common/common.hpp"
#include <stack>

constexpr int null = -1'000;

// ############################################################################
// ############################################################################

class CustomStack
{
    int m_max_size;
    std::stack<int> m_stack;
    std::vector<int> m_pending_increments;
public:
    CustomStack(int maxSize) : m_max_size(maxSize) {}
    void push(int x)
    {
        if (static_cast<int>(m_stack.size()) == m_max_size)
            return;
        m_stack.push(x);
        m_pending_increments.push_back(0);
    }
    int pop(void)
    {
        if (m_stack.empty())
            return -1;
        int i = static_cast<int>(m_stack.size()) - 1;
        if (i > 0)
            m_pending_increments[i - 1] += m_pending_increments[i];
        const int val = m_stack.top() + m_pending_increments[i];
        m_stack.pop();
        m_pending_increments.pop_back();
        return val;
    }
    void increment(int k, int val)
    {
        if (m_stack.empty())
            return;
        int i = std::min(k - 1, static_cast<int>(m_stack.size()) - 1);
        m_pending_increments[i] += val;
    }
};

// ############################################################################
// ############################################################################

enum class OP { PUSH, POP, INCREMENT };

void test(int maxSize,
          std::vector<OP> operation,
          std::vector<std::vector<int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        CustomStack obj(maxSize);
        result.clear();
        for (size_t i = 0; i < operation.size(); ++i)
        {
            switch (operation[i])
            {
            case OP::PUSH:
                result.push_back(null);
                obj.push(input[i][0]);
                break;
            case OP::POP:
                result.push_back(obj.pop());
                break;
            case OP::INCREMENT:
                result.push_back(null);
                obj.increment(input[i][0], input[i][1]);
                break;
            default:
                std::cerr << "[ERROR] Unknown operation at: " << i << '\n';
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {OP::PUSH, OP::PUSH, OP::POP, OP::PUSH, OP::PUSH, OP::PUSH,
             OP::INCREMENT, OP::INCREMENT, OP::POP, OP::POP, OP::POP, OP::POP},
         {{1}, {2}, {}, {2}, {3}, {4}, {5, 100}, {2, 100}, {}, {}, {}, {}},
         {null, null, 2, null, null, null, null, null, 103, 202, 201, -1}, trials);
    return 0;
}


