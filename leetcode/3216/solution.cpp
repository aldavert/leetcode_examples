#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string getSmallestString(std::string s)
{
    for (int i = 1, n = static_cast<int>(s.size()); i < n; ++i)
    {
        if ((s[i - 1] % 2 == s[i] % 2) && (s[i - 1] > s[i]))
        {
            std::swap(s[i - 1], s[i]);
            break;
        }
    }
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getSmallestString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("45320", "43520", trials);
    test("001", "001", trials);
    return 0;
}


