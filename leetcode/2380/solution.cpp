#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int secondsToRemoveOccurrences(std::string s)
{
    int result = 0;
    for (int zeros = 0; char c : s)
    {
        if (c == '0') ++zeros;
        else if (zeros > 0) result = std::max(result + 1, zeros);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = secondsToRemoveOccurrences(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("0110101", 4, trials);
    test("11100", 0, trials);
    return 0;
}


