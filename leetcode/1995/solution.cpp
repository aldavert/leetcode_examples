#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int countQuadruplets(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int present[5001] = {};
    int result = 0;
    for (int k = n - 2; k >= 0; --k)
    {
        ++present[nums[k + 1]];
        for (int j = k - 1; j >= 0; --j)
            for (int i = j - 1; i >= 0; --i)
                result += present[nums[i] + nums[j] + nums[k]];
    }
    return result;
}
#else
int countQuadruplets(std::vector<int> nums)
{
    const size_t n = nums.size();
    int result = 0;
    for (size_t i = 0; i < n - 3; ++i)
        for (size_t j = i + 1; j < n - 2; ++j)
            for (size_t k = j + 1; k < n - 1; ++k)
                for (size_t m = k + 1; m < n; ++m)
                    result += (nums[i] + nums[j] + nums[k] == nums[m]);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countQuadruplets(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 6}, 1, trials);
    test({3, 3, 6, 4, 5}, 0, trials);
    test({1, 1, 1, 3, 5}, 4, trials);
    test({9, 6, 8, 23, 39, 23}, 2, trials);
    return 0;
}


