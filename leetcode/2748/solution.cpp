#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int countBeautifulPairs(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<std::pair<int, int> > digits;
    for (int i = 0; i < n; ++i)
    {
        int last_digit = nums[i] % 10, first_digit = 0;
        for (int number = nums[i]; number > 0; number /= 10)
            first_digit = number % 10;
        digits.push_back({first_digit, last_digit});
    }
    int result = 0;
    for (int i = 0; i < n; ++i)
        for (int j = i + 1; j < n; ++j)
            result += std::gcd(digits[i].first, digits[j].second) == 1;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countBeautifulPairs(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 5, 1, 4}, 5, trials);
    test({11, 21, 12}, 2, trials);
    test({31, 25, 72, 79, 74}, 7, trials);
    return 0;
}


