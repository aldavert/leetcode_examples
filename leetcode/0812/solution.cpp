#include "../common/common.hpp"

// ############################################################################
// ############################################################################

double largestTriangleArea(std::vector<std::vector<int> > points)
{
    const size_t n = points.size();
    auto area = [&](size_t a, size_t b, size_t c) -> double
    {
        //return std::abs(points[a][0] * (points[b][1] - points[c][1])
        //              + points[b][0] * (points[c][1] - points[a][1])
        //              + points[c][0] * (points[a][1] - points[b][1])) / 2.0;
        return std::abs((points[b][0] - points[a][0])
                      * (points[c][1] - points[a][1])
                      - (points[c][0] - points[a][0])
                      * (points[b][1] - points[a][1])) / 2.0;

    };
    double result = 0.0;
    for (size_t i = 0; i < n - 2; ++i)
        for (size_t j = 0; j < n - 1; ++j)
            for (size_t k = 0; k < n; ++k)
                result = std::max(result, area(i, j, k));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points,
          double solution,
          unsigned int trials = 1)
{
    double result = -1.0;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestTriangleArea(points);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0}, {0, 1}, {1, 0}, {0, 2}, {2, 0}}, 2, trials);
    test({{1, 0}, {0, 0}, {0, 1}}, 0.5, trials);
    return 0;
}


