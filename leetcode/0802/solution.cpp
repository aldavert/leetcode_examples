#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> eventualSafeNodes(std::vector<std::vector<int> > graph)
{
    const int n = static_cast<int>(graph.size());
    std::vector<char> state(n, -1);
    int number_of_terminal_nodes = 0;
    for (int i = 0; i < n; ++i)
        if (graph[i].size() == 0)
            ++number_of_terminal_nodes,
            state[i] = 1;
    if (number_of_terminal_nodes == 0) return {};
    
    auto search = [&](auto &&self, int node) -> bool
    {
        if (state[node] >= 0) return state[node] == 1;
        state[node] = -2;
        bool terminal = true;
        for (int next : graph[node])
            terminal = (state[next] != -2) && self(self, next) && terminal;
        state[node] = terminal;
        return terminal;
    };
    for (int i = 0; i < n; ++i)
        if (state[i] == -1)
            search(search, i);
    
    std::vector<int> result;
    for (int i = 0; i < n; ++i)
        if (state[i] == 1)
            result.push_back(i);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > graph,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = eventualSafeNodes(graph);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 3}, {5}, {0}, {5}, {}, {}}, {2, 4, 5, 6}, trials);
    test({{1, 2, 3, 4}, {1, 2}, {3, 4}, {0, 4}, {}}, {4}, trials);
    return 0;
}


