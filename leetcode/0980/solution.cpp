#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int uniquePathsIII(std::vector<std::vector<int> > grid)
{
    struct coordinate
    {
        int y = 0;
        int x = 0;
        coordinate operator+(const coordinate &other) const
        {
            return { y + other.y, x + other.x };
        }
        bool inside(int h, int w) const
        {
            return (y >= 0) && (x >= 0) && (y < h) && (x < w);
        }
    };
    constexpr coordinate displacement[] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
    const int height = static_cast<int>(grid.size());
    const int width = static_cast<int>(grid[0].size());
    coordinate start;
    std::vector<std::vector<bool> > visited(height, std::vector<bool>(width, false));
    int count = 0, result = 0;
    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            if (grid[y][x] == 1) start = {y, x};
            else if (grid[y][x] == 0) ++count;
        }
    }
    visited[start.y][start.x] = true;
    auto search = [&](auto &&self, coordinate position, int remaining) -> void
    {
        if ((grid[position.y][position.x] == 2) && (remaining == 0))
            ++result;
        else
        {
            for (int d = 0; d < 4; ++d)
            {
                coordinate position_new = position + displacement[d];
                if (position_new.inside(height, width)
                 && (!visited[position_new.y][position_new.x])
                 && (grid[position_new.y][position_new.x] != -1))
                {
                    visited[position_new.y][position_new.x] = true;
                    self(self, position_new, remaining - 1);
                    visited[position_new.y][position_new.x] = false;
                }
            }
        }
    };
    search(search, start, count + 1);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = uniquePathsIII(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 2, -1}}, 2, trials);
    test({{1, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 2}}, 4, trials);
    test({{0, 1}, {2, 0}}, 0, trials);
    return 0;
}


