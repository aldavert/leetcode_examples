#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int lenLongestFibSubseq(std::vector<int> arr)
{
    const int n = static_cast<int>(arr.size());
    std::vector<std::vector<int> > dp(n, std::vector<int>(n, 2));
    std::unordered_map<int, int> number_to_index;
    int result = 0;
    
    for (int i = 0; i < n; ++i)
        number_to_index[arr[i]] = i;
    for (int j = 0; j < n; ++j)
    {
        for (int k = j + 1; k < n; ++k)
        {
            int diff = arr[k] - arr[j];
            if (diff >= arr[j]) continue;
            auto search = number_to_index.find(diff);
            if (search != number_to_index.end())
                result = std::max(result, dp[j][k] = dp[search->second][j] + 1);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = lenLongestFibSubseq(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6, 7, 8}, 5, trials);
    test({1, 3, 7, 11, 12, 14, 18}, 3, trials);
    return 0;
}


