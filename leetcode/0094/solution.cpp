#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> inorderTraversal(TreeNode * root)
{
    std::stack<TreeNode *> stack;
    std::vector<int> result;
    for (TreeNode * ptr = root;;)
    {
        for (; ptr != nullptr; ptr = ptr->left)
            stack.push(ptr);
        if (!stack.empty())
        {
            ptr = stack.top();
            stack.pop();
            result.push_back(ptr->val);
            ptr = ptr->right;
        }
        else break;
    }
    return result;
}
#else
std::vector<int> inorderTraversal(TreeNode * root)
{
    std::vector<int> result;
    auto traverse = [&](auto &&self, TreeNode * ptr)
    {
        if (ptr == nullptr) return;
        self(self, ptr->left);
        result.push_back(ptr->val);
        self(self, ptr->right);
    };
    traverse(traverse, root);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = inorderTraversal(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, null, 2, 3}, {1, 3, 2}, trials);
    test({}, {}, trials);
    test({1}, {1}, trials);
    return 0;
}


