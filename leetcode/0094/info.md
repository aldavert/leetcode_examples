# Binary Tree Inorder Traversal

Given the `root` of a binary tree, return *the inorder traversal of its nodes' values*.

#### Example 1:
> ```mermaid
> graph TD;
> A((1))---EMPTY1(( ))
> A---B((2))
> B---C((3))
> B---EMPTY2(( ))
> linkStyle 0,3 stroke-width:0px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class EMPTY1,EMPTY2 empty;
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [1, null, 2, 3]`  
> *Output:* `[1, 3, 2]`

#### Example 2:
> *Input:* `root = []`  
> *Output:* `[]`

#### Example 3:
> *Input:* `root = [1]`  
> *Output:* `[1]`

#### Constraints:
- The number of nodes in the tree is in the range `[0, 100]`.
- `-100 <= Node.val <= 100`

**Follow up:** Recursive solution is trivial, could you do it iteratively?

