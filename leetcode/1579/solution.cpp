#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxNumEdgesToRemove(int n, std::vector<std::vector<int> > edges)
{
    struct UnionFind
    {
    private:
        std::vector<int> m_id;
        std::vector<int> m_size;
        int m_max_size;
    public:
        UnionFind(int nelements) : m_id(nelements), m_size(nelements, 1), m_max_size(1)
        {
            for (int i = 0; i < nelements; ++i) m_id[i] = i;
        }
        int find(int u) { return (m_id[u] == u)?u:m_id[u] = find(m_id[u]); }
        bool merge(int x, int y)
        {
            int u = find(x), v = find(y);
            if (u == v) return true;
            m_id[u] = v;
            m_size[v] += m_size[u];
            m_max_size = std::max(m_max_size, m_size[v]);
            return false;
        }
        int maxSize(void) const { return m_max_size; }
    };
    UnionFind uf1(n), uf2(n);
    int result = 0, nedges = static_cast<int>(edges.size());
    for (int i = 0; i < nedges; ++i)
    {
        if (edges[i][0] != 3) continue;
        int u = edges[i][1] - 1, v = edges[i][2] - 1;
        bool aux = uf1.merge(u, v);
        uf2.merge(u, v);
        result += aux;
    }
    for (int i = 0; i < nedges; ++i)
    {
        if (edges[i][0] == 3) continue;
        int u = edges[i][1] - 1, v = edges[i][2] - 1;
        if      (edges[i][0] == 1) result += uf1.merge(u, v);
        else if (edges[i][0] == 2) result += uf2.merge(u, v);
    }
    return ((uf1.maxSize() != n) || (uf2.maxSize() != n))?-1:result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxNumEdgesToRemove(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{3, 1, 2}, {3, 2, 3}, {1, 1, 3}, {1, 2, 4}, {1, 1, 2}, {2, 3, 4}}, 2, trials);
    test(4, {{3, 1, 2}, {3, 2, 3}, {1, 1, 4}, {2, 1, 4}}, 0, trials);
    test(4, {{3, 2, 3}, {1, 1, 2}, {2, 3, 4}}, -1, trials);
    return 0;
}


