#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumTime(std::string s)
{
    const int n = static_cast<int>(s.size());
    int result = n;
    
    for (int i = 0, left = 0; i < n; ++i)
    {
        left = std::min(left + (s[i] - '0') * 2, i + 1);
        result = std::min(result, left + n - 1 - i);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumTime(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1100101", 5, trials);
    test("0010", 2, trials);
    return 0;
}


