# Path Sum II

Given the `root` of a binary tree and an integer `targetSum`, return all **root-to-leaf** paths where each path's sum equals `targetSum`.

A **leaf** is a node with no children.

#### Example 1:
> ```mermaid 
> graph TD;
> A((5))---B((4))
> A---C((8))
> B---D((11))
> B---EMPTY1(( ))
> C---E((13))
> C---F((4))
> D---G((7))
> D---H((2))
> F---I((5))
> F---J((1))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#DDF,stroke:#44A,stroke-width:2px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class A,B,D,H,C,F,I selected;
> class EMPTY1 empty;
> linkStyle 3 stroke-width:0px
> ```
> *Input:* `root = [5, 4, 8, 11, null, 13, 4, 7, 2, null, null, 5, 1], targetSum = 22`  
> *Output:* `[[5, 4, 11, 2], [5, 8, 4, 5]]`

#### Example 2:
> ```mermaid 
> graph TD;
> A((1))---B((2))
> A---C((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [1, 2, 3], targetSum = 5`  
> *Output:* `[]`

#### Example 3:
> *Input:* `root = [1, 2], targetSum = 0`  
> *Output:* `[]`

#### Constrains:
- The number of nodes in the tree is in the range `[0, 5000]`.
- `-1000 <= Node.val <= 1000`
- `-1000 <= targetSum <= 1000`


