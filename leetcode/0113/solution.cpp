#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > pathSum(TreeNode* root, int targetSum)
{
    std::vector<std::vector<int> > result;
    auto process = [&](auto &&self, TreeNode * current, int sum, std::vector<int> path) -> void
    {
        if (current != nullptr)
        {
            int diff = sum - current->val;
            if ((current->left == nullptr) && (current->right == nullptr))
            {
                if (diff == 0)
                {
                    path.push_back(current->val);
                    result.emplace_back(path);
                }
            }
            else
            {
                path.push_back(current->val);
                self(self, current->left , diff, path);
                self(self, current->right, diff, std::move(path));
            }
        }
    };
    process(process, root, targetSum, {});
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    const int n = static_cast<int>(left.size());
    std::unordered_set<std::string> left_hash;
    for (int i = 0; i < n; ++i)
    {
        std::string text;
        for (int v : left[i])
            text += std::to_string(v) + ",";
        left_hash.insert(text);
    }
    for (int i = 0; i < n; ++i)
    {
        std::string text;
        for (int v : right[i])
            text += std::to_string(v) + ",";
        if (left_hash.find(text) == left_hash.end()) return false;
    }
    return true;
}

void test(std::vector<int> tree,
          int targetSum,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = pathSum(root, targetSum);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 8, 11, null, 13, 4, 7, 2, null, null, 5, 1}, 22, {{5, 4, 11, 2}, {5, 8, 4, 5}}, trials);
    test({1, 2, 3}, 5, {}, trials);
    test({1, 2}, 0, {}, trials);
    //         1
    //    -2       -3
    //   1   3  -2    -
    //-1
    test({1, -2, -3, 1, 3, -2, null, -1}, -1, {{1, -2, 1, -1}}, trials);
    return 0;
}


