#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int lengthOfLongestSubsequence(std::vector<int> nums, int target)
{
    int dp[1001] = {};
    for (const int num : nums)
        for (int i = target; i >= num; --i)
            if ((i == num) || (dp[i - num] > 0))
                dp[i] = std::max(dp[i], 1 + dp[i - num]);
    return (dp[target] > 0)?dp[target]:-1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = lengthOfLongestSubsequence(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 9, 3, trials);
    test({4, 1, 3, 2, 1, 5}, 7, 4, trials);
    test({1, 1, 5, 4, 5}, 3, -1, trials);
    return 0;
}


