#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool findRotation(std::vector<std::vector<int> > mat,
                  std::vector<std::vector<int> > target)
{
    const int n = static_cast<int>(mat.size());
    auto work_copy = mat;
    auto rotate = [&](const std::vector<std::vector<int> > &m)
    {
        std::vector<std::vector<int> > result(n, std::vector<int>(n));
        for (int r = 0; r < n; ++r)
            for (int c = 0; c < n; ++c)
                result[r][c] = m[n - c - 1][r];
        return result;
    };
    if (work_copy == target)
        return true;
    for (int i = 0; i < 3; ++i)
    {
        work_copy = rotate(work_copy);
        if (work_copy == target)
            return true;
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > mat,
          std::vector<std::vector<int> > target,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = findRotation(mat, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {1, 0}}, {{1, 0}, {0, 1}}, true, trials);
    test({{0, 1}, {1, 1}}, {{1, 0}, {0, 1}}, false, trials);
    test({{0, 0, 0}, {0, 1, 0}, {1, 1, 1}},
         {{1, 1, 1}, {0, 1, 0}, {0, 0, 0}}, true, trials);
    return 0;
}


