#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

int findMaxK(std::vector<int> nums)
{
    std::bitset<1001> positive, negative;
    int result = -1;
    for (size_t i = 0, n = nums.size(); i < n; ++i)
    {
        int current;
        if (nums[i] > 0) positive[current =  nums[i]] = true;
        else             negative[current = -nums[i]] = true;
        if ((current > result) && positive[current] && negative[current])
            result = current;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMaxK(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, 2, -3, 3}, 3, trials);
    test({-1, 10, 6, 7, -7, 1}, 7, trials);
    test({-10, 8, 6, 7, -2, -3}, -1, trials);
    return 0;
}


