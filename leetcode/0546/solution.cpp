#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int removeBoxes(std::vector<int> boxes)
{
    int dp[102][101][101] = {};
    std::string s;
    auto valOf = [&](int i) -> char { return s[i * 2]; };
    auto countOf = [&](int i) -> char { return s[i * 2 + 1]; };
    auto pointsOf = [&](int i) { i = countOf(i); return i * i; };
    auto recurse = [&](auto &&self, int i, int j, int k) -> int
    {
        if (i == j) return 0;
        int &cache = dp[i][j][k];
        if (cache != -1) { return cache; }
        k += countOf(i);
        int p = k * k + self(self, i + 1, j, 0);
        char target = valOf(i);
        for (int m = i + 1; m < j; ++m)
        {
            if (valOf(m) != target) continue;
            p = std::max(p, self(self, i + 1, m, 0) + self(self, m, j, k));
        }
        return cache = p;
    };
    
    int start = 0;
    for (int i = 0, n = static_cast<int>(boxes.size()); i <= n; ++i)
    {
        if ((i == n) || (boxes[i] != boxes[start]))
        {
            s.push_back(static_cast<char>(boxes[start]));
            s.push_back(static_cast<char>(i - start));
            start = i;
        }
    }
    const int n = static_cast<int>(s.size()) / 2;
    if (n == 0) return 0;
    if (n == 1) return pointsOf(0);
    if (n == 2) return pointsOf(0) + pointsOf(1);
    for (int i = 0; i < n; ++i)
        for (int j = 0; j <= n; ++j)
            for (int k = 0; k < 101; ++k)
                dp[i][j][k] = -1;
    return recurse(recurse, 0, n, 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> boxes, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = removeBoxes(boxes);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 2, 2, 3, 4, 3, 1}, 23, trials);
    test({1, 1, 1}, 9, trials);
    test({1}, 1, trials);
    test({1, 2, 2, 1, 1, 1, 2, 1, 1, 2, 1, 2, 1, 1, 2, 2, 1, 1, 2, 2, 1, 1, 1, 2,
          2, 2, 2, 1, 2, 1, 1, 2, 2, 1, 2, 1, 2, 2, 2, 2, 2, 1, 2, 1, 2, 2, 1, 1,
          1, 2, 2, 1, 2, 1, 2, 2, 1, 2, 1, 1, 1, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2,
          1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2,
          1, 2, 2, 1}, 2758, trials);
    return 0;
}


