#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumDeletions(std::string s)
{
    int dp = 0;
    
    for (int count_b = 0; char c : s)
    {
        if (c == 'a') dp = std::min(dp + 1, count_b);
        else ++count_b;
    }
    return dp;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumDeletions(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aababbab", 2, trials);
    test("bbaaaaabb", 2, trials);
    return 0;
}


