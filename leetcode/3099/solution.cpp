#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sumOfTheDigitsOfHarshadNumber(int x)
{
    int sum = 0;
    for (int number = x; number; number /= 10) sum += number % 10;
    return (x % sum == 0)?sum:-1;
}

// ############################################################################
// ############################################################################

void test(int x, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOfTheDigitsOfHarshadNumber(x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(18,  9, trials);
    test(23, -1, trials);
    return 0;
}


