#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canMakeSubsequence(std::string str1, std::string str2)
{
    for (size_t i = 0; char c : str1)
        if ((c == str2[i]) || (('a' + ((c - 'a' + 1) % 26)) == str2[i]))
            if (++i == str2.length())
                return true;
    return false;
}

// ############################################################################
// ############################################################################

void test(std::string str1, std::string str2, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canMakeSubsequence(str1, str2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", "ad", true, trials);
    test("zc", "ad", true, trials);
    test("ab", "d", false, trials);
    return 0;
}


