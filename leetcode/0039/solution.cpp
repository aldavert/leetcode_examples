#include "../common/common.hpp"
#include <unordered_map>
#include <sstream>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > combinationSum(std::vector<int> candidates, int target)
{
    const int n = static_cast<int>(candidates.size());
    std::vector<std::vector<int> > result;
    std::vector<int> histogram(n, 0);
    std::sort(candidates.begin(), candidates.end());
    auto process = [&](auto &&self, int idx, int sum) -> void
    {
        if (idx >= n) return;
        int max_histogram = sum / candidates[idx];
        for (int k = 0; k < max_histogram; ++k)
        {
            self(self, idx + 1, sum);
            sum -= candidates[idx];
            ++histogram[idx];
        }
        if (sum == 0)
        {
            result.push_back({});
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < histogram[i]; ++j)
                    result.back().push_back(candidates[i]);
        }
        histogram[idx] = 0;
    };
    process(process, 0, target);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    std::unordered_map<std::string, int> histogram;
    for (const auto &l : left)
    {
        std::ostringstream oss;
        oss << l;
        ++histogram[oss.str()];
    }
    for (const auto &r : right)
    {
        std::ostringstream oss;
        oss << r;
        if (auto it = histogram.find(oss.str()); it != histogram.end())
        {
            --(it->second);
            if (!it->second)
                histogram.erase(it);
        }
        else return false;
    }
    return histogram.empty();
}

void test(std::vector<int> candidates,
          int target,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = combinationSum(candidates, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 7, 6, 3, 5, 1}, 9,
         {{1, 1, 1, 1, 1, 1, 1, 1, 1},
          {1, 1, 1, 1, 1, 1, 1, 2},
          {1, 1, 1, 1, 1, 1, 3},
          {1, 1, 1, 1, 1, 2, 2},
          {1, 1, 1, 1, 2, 3},
          {1, 1, 1, 1, 5},
          {1, 1, 1, 2, 2, 2},
          {1, 1, 1, 3, 3},
          {1, 1, 1, 6},
          {1, 1, 2, 2, 3},
          {1, 1, 2, 5},
          {1, 1, 7},
          {1, 2, 2, 2, 2},
          {1, 2, 3, 3},
          {1, 2, 6},
          {1, 3, 5},
          {2, 2, 2, 3},
          {2, 2, 5},
          {2, 7},
          {3, 3, 3},
          {3, 6}}, trials);
    return 0;
}


