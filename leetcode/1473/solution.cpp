#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

#if 1
int minCost(std::vector<int> &houses,
            std::vector<std::vector<int> > &cost,
            int m,
            int n,
            int target)
{
    static constexpr int MAXVALUE = 10'000'001;
    int dp[100][101][21];
    std::memset(dp, -1, sizeof(dp));
    auto recursive = [&](auto &&self, int pstn, int prev, int ctar) -> int
    {
        if (pstn == m)
            return (ctar == 0)?0:MAXVALUE;
        if (ctar < 0) return MAXVALUE;
        if (ctar > m - pstn + 1) return MAXVALUE;
        if (dp[pstn][ctar][prev] != -1)
            return dp[pstn][ctar][prev];
        int rslt = MAXVALUE;
        if (houses[pstn] == 0)
        {
            for (int j = 0; j < n; ++j)
            {
                rslt = std::min(rslt, cost[pstn][j] +
                                self(self, pstn + 1, j + 1, ctar - (j + 1 != prev)));
            }
        }
        else rslt = self(self, pstn + 1, houses[pstn], ctar - (houses[pstn] != prev));
        dp[pstn][ctar][prev] = rslt;
        return rslt;
    };
    int rslt = recursive(recursive, 0, 0, target);
    return (rslt < MAXVALUE)?rslt:-1;
}
#else
int minCost(int dp[100][101][21],
            int pstn,
            int prev,
            std::vector<int> &houses,
            std::vector<std::vector<int> > &cost,
            int m,
            int n,
            int target)
{
    static constexpr int MAXVALUE = 10'000'001;
    if (pstn == m)
        return (target == 0)?0:MAXVALUE;
    if (target < 0) return MAXVALUE;
    if (target > m - pstn + 1) return MAXVALUE;
    if (dp[pstn][target][prev] != -1)
        return dp[pstn][target][prev];
    int rslt = MAXVALUE;
    if (houses[pstn] == 0)
    {
        for (int j = 0; j < n; ++j)
        {
            rslt = std::min(rslt, cost[pstn][j] +
                            minCost(dp,
                                    pstn + 1,
                                    j + 1,
                                    houses, cost, m, n,
                                    target - (j + 1 != prev)));
        }
    }
    else rslt = minCost(dp,
                        pstn + 1,
                        houses[pstn],
                        houses, cost, m, n,
                        target - (houses[pstn] != prev));
    dp[pstn][target][prev] = rslt;
    return rslt;
}

int minCost(std::vector<int> houses,
            std::vector<std::vector<int> > cost,
            int m,
            int n,
            int target)
{
    static constexpr int MAXVALUE = 10'000'001;
    // This dp is to avoid recomputing the 'tails' when at a certaint house index, we
    // already have the cost for a certain target and previous color (the algorithm
    // will follow the same path).
    int dp[100][101][21];
    std::memset(dp, -1, sizeof(dp));
    //for (unsigned int i = 0; i < m; ++i)
    //    for (unsigned int j = 0; j <= target; ++j)
    //        for (unsigned int k = 0; k <= n; ++k)
    //            dp[i][j][k] = -1;
    int rslt = minCost(dp, 0, 0, houses, cost, m, n, target);
    return (rslt < MAXVALUE)?rslt:-1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> houses,
          std::vector<std::vector<int> > cost,
          int m,
          int n,
          int target,
          int solution)
{
    int result = minCost(houses, cost, m, n, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
    test({0, 0, 0, 0, 0}, {{1, 10}, {10, 1}, {10, 1}, {1, 10}, {5, 1}}, 5, 2, 3, 9);
    test({0, 2, 1, 2, 0}, {{1, 10}, {10, 1}, {10, 1}, {1, 10}, {5, 1}}, 5, 2, 3, 11);
    test({0, 0, 0, 0, 0}, {{1, 10}, {10, 1}, {1, 10}, {10, 1}, {1, 10}}, 5, 2, 5, 5);
    test({3, 1, 2, 3}, {{1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}}, 4, 3, 3, -1);
    return 0;
}


