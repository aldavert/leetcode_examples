#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

#if 1
long long countSubstrings(std::string s)
{
    const int n = static_cast<int>(s.size());
    int three[3] = {}, seven[7] = {}, nine[9] = {}, last[3] = {};
    three[0] = nine[0] = 1;
    int mod_three = 0, mod_nine = 0;
    long long result = 0;
    for (int i = 0; i < n; ++i)
    {
        int digit = s[i] - '0';
        last[2] = last[1];
        last[1] = last[0];
        last[0] = digit;
        mod_three = (mod_three + digit) % 3;
        mod_nine = (mod_nine + digit) % 9;
        if      (digit == 0) { }
        else if (digit <= 2) result += i + 1;
        else if (digit == 3) result += three[mod_three];
        else if (digit == 4)
        {
            ++result;
            if ((last[1] * 10 + last[0]) % 4 == 0) result += i;
        }
        else if (digit == 5) result += i + 1;
        else if (digit == 6) result += three[mod_three];
        else if (digit == 7) ++result;
        else if (digit == 8)
        {
            ++result;
            if ((last[1] * 10 + last[0]) % 8 == 0) ++result;
            if ((last[2] * 100 + last[1] * 10 + last[0]) % 8 ==0) result += i-1;
        }
        else result += nine[mod_nine];
        ++three[mod_three];
        ++nine[mod_nine];
    }
    std::vector<int> mod_pow(n + 1, 1);
    for (int i = 1; i <= n; ++i) mod_pow[i] = 10 * mod_pow[i - 1] % 7;
    for (int i = n - 1, rem = 0; i >= 0; --i)
    {
        int digit = s[i] - '0';
        rem = ((digit * mod_pow[n - i]) % 7 + rem) % 7;
        result += seven[rem];
        if (s[i] == '7') ++seven[rem];
    }
    return result;
}
#else
long long countSubstrings(std::string s)
{
    const int n = static_cast<int>(s.size());
    std::vector<int> digits(n);
    long long result = 0;
    for (int i = 0; i < n; ++i) digits[i] = s[i] - '0';
    long r[2][10] = {};
    for (int i = 1; i < 10; ++i)
    {
        std::memset(r, 0, sizeof(r));
        for (bool next = true; int x : digits)
        {
            std::memset(r[next], 0, sizeof(r[0]));
            for (int j = 0; j < i; ++j)
                r[next][(j * 10 + x) % i] += r[!next][j];
            ++r[next][x % i];
            if (x == i) result += r[next][0];
            next = !next;
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSubstrings(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("12936", 11, trials);
    test("5701283", 18, trials);
    test("1010101010", 25, trials);
    return 0;
}


