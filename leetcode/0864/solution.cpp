#include "../common/common.hpp"
#include <bitset>
#include <queue>

// ############################################################################
// ############################################################################

int shortestPathAllKeys(std::vector<std::string> grid)
{
    struct Position
    {
        int row = 0;
        int column = 0;
        int keys = 0;
        inline int bin(void) const { return row * 63 * 31 + column * 63 + keys; }
    };
    const int n_rows = static_cast<int>(grid.size());
    const int n_cols = static_cast<int>(grid[0].length());
    const int dirs[] = {0, 1, 0, -1, 0};
    std::queue<Position> q;
    int key_mask = 1, result = 0;
    for (int r = 0; r < n_rows; ++r)
    {
        for (int c = 0; c < n_cols; ++c)
        {
            key_mask <<= (grid[r][c] >= 'a') && (grid[r][c] <= 'f');
            if (grid[r][c] == '@')
                q.push({r, c, 0});
        }
    }
    --key_mask;
    if (q.size() != 1) return -1;
    std::bitset<31 * 31 * 63> seen;
    
    while (!q.empty())
    {
        ++result;
        for (size_t e = 0, l = q.size(); e < l; ++e)
        {
            Position current = q.front();
            q.pop();
            if (seen[current.bin()]) continue;
            seen[current.bin()] = true;
            for (int k = 0; k < 4; ++k)
            {
                const int row = current.row + dirs[k];
                const int col = current.column + dirs[k + 1];
                if ((row < 0) || (row == n_rows) || (col < 0) || (col == n_cols))
                    continue; // Out of bounds
                const char c = grid[row][col];
                if (c == '#') continue; // Wall
                Position next({row, col, current.keys
                            | (((c >= 'a') && (c <= 'f'))?(1 << (c - 'a')):0)});
                if (next.keys == key_mask)
                    return result;
                if ((c >= 'A') && (c <= 'F') && ((next.keys >> (c - 'A')) & 1) == 0)
                    continue; // Locked lock
                if (!seen[next.bin()])
                    q.push(next);
            }
        }
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestPathAllKeys(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"@.a..", "###.#", "b.A.B"}, 8, trials);
    test({"@..aA", "..B#.", "....b"}, 6, trials);
    test({"@Aa"}, -1, trials);
    return 0;
}


