#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkAlmostEquivalent(std::string word1, std::string word2)
{
    int histogram1[26] = {}, histogram2[26] = {};
    for (size_t i = 0, n = word1.size(); i < n; ++i)
        ++histogram1[word1[i] - 'a'],
        ++histogram2[word2[i] - 'a'];
    for (size_t i = 0; i < 26; ++i)
        if (std::abs(histogram1[i] - histogram2[i]) > 3)
            return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string word1,
          std::string word2,
          bool solution,
          unsigned int trials = 1)
{
    bool result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkAlmostEquivalent(word1, word2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aaaa", "bccb", false, trials);
    test("abcdeef", "abaaacc", true, trials);
    test("cccddabba", "babababab", true, trials);
    return 0;
}


