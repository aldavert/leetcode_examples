#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int maxProfit(std::vector<int> prices, int fee)
{
    int sell = 0, hold = std::numeric_limits<int>::lowest();
    
    for (int price : prices)
    {
        sell = std::max(sell, hold + price);
        hold = std::max(hold, sell - price - fee);
    }
    return sell;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> prices, int fee, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxProfit(prices, fee);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 8, 4, 9}, 2, 8, trials);
    test({1, 3, 7, 5, 10, 3}, 3, 6, trials);
    return 0;
}


