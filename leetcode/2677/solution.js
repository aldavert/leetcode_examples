var chunk = function(arr, size) {
    if ((size <= 0) || (arr.length === 0)) return [];
    var chunked_arr = [];
    var i = 0;
    while (i < arr.length)
    {
        chunked_arr.push(arr.slice(i, i + size));
        i += size;
    }
    return chunked_arr;
};
