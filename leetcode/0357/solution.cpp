#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countNumbersWithUniqueDigits(int n)
{
    if      (n == 0) return 1;
    else if (n == 1) return 10;
    auto f = [](int k) -> int
    {
        int result = 9;
        for (int i = 0; i < k - 1; ++i)
            result *= (9 - i);
        return result;
    };
    int result = 10;
    for (int i = 2; i <= n; ++i)
        result += f(i);
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countNumbersWithUniqueDigits(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 91, trials);
    test(0, 1, trials);
    test(1, 10, trials);
    test(3, 739, trials);
    test(4, 5275, trials);
    test(5, 32491, trials);
    test(6, 168571, trials);
    test(7, 712891, trials);
    test(8, 2345851, trials);
    return 0;
}


