#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findEvenNumbers(std::vector<int> digits)
{
    int histogram[10] = {};
    for (int digit : digits)
        ++histogram[digit];
    std::vector<int> result;
    for (int i = 1; i < 10; ++i)            // No leading zeros.
    {
        if (!histogram[i]) continue;
        --histogram[i];
        for (int j = 0; j < 10; ++j)
        {
            if (!histogram[j]) continue;
            --histogram[j];
            for (int k = 0; k < 10; k += 2)    // Even numbers.
            {
                if (histogram[k])
                    result.push_back(i * 100 + j * 10 + k);
            }
            ++histogram[j];
        }
        ++histogram[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const size_t n = left.size();
    std::vector<int> copy_left(left), copy_right(right);
    std::sort(copy_left.begin(), copy_left.end());
    std::sort(copy_right.begin(), copy_right.end());
    for (size_t i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

void test(std::vector<int> digits,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findEvenNumbers(digits);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3, 0}, {102, 120, 130, 132, 210, 230, 302, 310, 312, 320}, trials);
    test({2, 2, 8, 8, 2}, {222, 228, 282, 288, 822, 828, 882}, trials);
    test({3, 7, 5}, {}, trials);
    return 0;
}


