#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> treeQueries(TreeNode * root, std::vector<int> &queries)
{
    int max_height[100'001] = {}, current_max_height = 0;
    auto traverse =
        [&](auto &&self, TreeNode * node, int height, bool left_to_right) -> void
    {
        if (node == nullptr) return;
        max_height[node->val] =
            (left_to_right)?current_max_height
                           :std::max(max_height[node->val], current_max_height);
        current_max_height = std::max(current_max_height, height);
        if (left_to_right)
        {
            self(self, node->left , height + 1, left_to_right);
            self(self, node->right, height + 1, left_to_right);
        }
        else
        {
            self(self, node->right, height + 1, left_to_right);
            self(self, node->left , height + 1, left_to_right);
        }
    };
    traverse(traverse, root, 0, true);
    current_max_height = 0;
    traverse(traverse, root, 0, false);
    int query_count = static_cast<int>(queries.size());
    std::vector<int> results(query_count);
    for (int i = 0; i < query_count; i++)
        results[i] = max_height[queries[i]];
    return results;
}
#elif 0
std::vector<int> treeQueries(TreeNode * root, std::vector<int> &queries)
{
    int dp_left[100'001], dp_right[100'001], max_height;
    auto pre = [&](auto &&self, TreeNode* node, int h) -> void
    {
        if (!node) return;
        dp_left[node->val] = max_height;
        max_height = std::max(max_height, h);
        self(self, node->left , h + 1);
        self(self, node->right, h + 1);
    };
    auto post = [&](auto &&self, TreeNode* node, int h) -> void
    {
        if (!node) return;
        dp_right[node->val] = max_height;
        max_height = std::max(max_height, h);
        self(self, node->right, h + 1);
        self(self, node->left , h + 1);
    };
    max_height = 0; pre(pre, root, 0);
    max_height = 0; post(post, root, 0);
    std::vector<int> result;
    for (int query : queries)
        result.push_back(std::max(dp_left[query], dp_right[query]));
    return result;
}
#else
std::vector<int> treeQueries(TreeNode * root, std::vector<int> &queries)
{
    std::unordered_map<int, int> value_to_max_height;
    std::unordered_map<int, int> value_to_height;
    auto height = [&](auto &&self, TreeNode * node) -> int
    {
        if (node == nullptr) return 0;
        if (auto it = value_to_height.find(node->val); it != value_to_height.end())
            return it->second;
        return value_to_height[node->val] = 1
            + std::max(self(self, node->left), self(self, node->right));
    };
    auto dfs = [&](auto &&self, TreeNode * node, int depth, int max_height) -> void
    {
        if (node == nullptr) return;
        value_to_max_height[node->val] = max_height;
        int height_right = depth + height(height, node->right),
            height_left  = depth + height(height, node->left);
        self(self, node->left, depth + 1, std::max(max_height, height_right));
        self(self, node->right, depth + 1, std::max(max_height, height_left));
    };
    
    dfs(dfs, root, 0, 0);
    std::vector<int> result;
    for (int query : queries)
        result.push_back(value_to_max_height[query]);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          std::vector<int> queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = treeQueries(root, queries);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 4, 2, null, 6, 5, null, null, null, null, null, 7}, {4}, {2}, trials);
    test({5, 8, 9, 2, 1, 3, 7, 4, 6}, {3, 2, 4, 8}, {3, 2, 3, 2}, trials);
    return 0;
}


