# Height of Binary Tree After Subtree Removal Queries

You are given the `root` of a **binary tree** with `n` nodes. Each node is assigned a unique value from `1` to `n`. You are also given an array `queries` of size `m`.

You have to perform `m` **independent** queries on the tree where in the `i^{th}` query you do the following:
- **Remove** the subtree rooted at the node with the value `queries[i]` from the tree. It is **guaranteed** that `queries[i]` will **not** be equal to the value of the root.
Return *an array* `answer` *of size* `m` *where* `answer[i]` *is the height of the tree after performing the* `i^{th}` *query.*

**Note:**
- The queries are independent, so the tree returns to its **initial** state after each query.
- The height of a tree is the **number of edges in the longest simple path** from the root to some node in the tree.

#### Example 1:
> ```mermaid
> flowchart LR;
> subgraph S1 [ ]
> direction TB;
> A1((1))---B1((3))
> B1---C1((2))
> B1---EMPTY1(( ))
> A1---D1((4))
> D1---E1((6))
> D1---F1((5))
> F1---EMPTY2(( ))
> F1---G1((7))
> end
> subgraph S2 [ ]
> direction TB;
> A2((1))---B2((3))
> A2---EMPTY3(( ))
> B2---C2((2))
> B2---EMPTY4(( ))
> end
> S1-->S2
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef delete fill:#FFF0,stroke:#FFF0,stroke-width:0px
> class EMPTY1,EMPTY2,EMPTY3,EMPTY4,S1,S2 delete;
> linkStyle 2,6,9,11 stroke-width:0px
> ```
> *Input:* `root = [1, 3, 4, 2, null, 6, 5, null, null, null, null, null, 7], queries = [4]`  
> *Output:* `[2]`  
> *Explanation:* The diagram above shows the tree after removing the subtree rooted at node with value `4`. The height of the tree is `2` (The path `1 -> 3 -> 2`).

#### Example 2:
> ```mermaid 
> graph TD;
> A((5))---B((8))
> B---C((2))
> C---D((4))
> C---E((6))
> B---F((1))
> A---G((9))
> G---H((3))
> G---I((7))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [5, 8, 9, 2, 1, 3, 7, 4, 6], queries = [3, 2, 4, 8]`  
> *Output:* `[3, 2, 3, 2]`  
> *Explanation:* We have the following queries:
> - Removing the subtree rooted at node with value `3`. The height of the tree becomes `3` (The path `5 -> 8 -> 2 -> 4`).
> - Removing the subtree rooted at node with value `2`. The height of the tree becomes `2` (The path `5 -> 8 -> 1`).
> - Removing the subtree rooted at node with value `4`. The height of the tree becomes `3` (The path `5 -> 8 -> 2 -> 6`).
> - Removing the subtree rooted at node with value `8`. The height of the tree becomes `2` (The path `5 -> 9 -> 3`).

#### Constraints:
- The number of nodes in the tree is `n`.
- `2 <= n <= 10^5`
- `1 <= Node.val <= n`
- All the values in the tree are unique.
- `m == queries.length`
- `1 <= m <= min(n, 10^4)`
- `1 <= queries[i] <= n`
- `queries[i] != root.val`


