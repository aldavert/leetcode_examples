#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

char kthCharacter(long long k, std::vector<int> operations)
{
    const int operations_count = static_cast<int>(std::ceil(std::log2(k)));
    int increases = 0;
    for (int i = operations_count - 1; i >= 0; --i)
    {
        const long half_size = 1L << i;
        if (k > half_size)
        {
            k -= half_size;
            increases += operations[i];
        }
    }
    return static_cast<char>('a' + increases % 26);
}

// ############################################################################
// ############################################################################

void test(long long k,
          std::vector<int> operations,
          char solution,
          unsigned int trials = 1)
{
    char result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = kthCharacter(k, operations);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {0, 0, 0}, 'a', trials);
    test(10, {0, 1, 0, 1}, 'b', trials);
    return 0;
}


