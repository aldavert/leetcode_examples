#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> maximumBobPoints(int numArrows, std::vector<int> aliceArrows)
{
    auto getShotableAndPoint = [&](int mask, int left_arrows) -> std::pair<bool, int>
    {
        int point = 0;
        for (int i = 0; i < 12; ++i)
        {
            if (mask >> i & 1)
            {
                left_arrows -= aliceArrows[i] + 1;
                point += i;
            }
        }
        return {left_arrows >= 0, point};
    };
    int max_mask = 0;
    for (int mask = 0, max_point = 0, all_mask = (1 << 12) - 1; mask < all_mask; ++mask)
    {
        auto [shotable, point] = getShotableAndPoint(mask, numArrows);
        if (shotable && (point > max_point))
        {
            max_point = point;
            max_mask = mask;
        }
    }
    std::vector<int> result(12);
    for (int i = 0; i < 12; ++i)
    {
        if (max_mask >> i & 1)
        {
            result[i] = aliceArrows[i] + 1;
            numArrows -= aliceArrows[i] + 1;
        }
    }
    result[0] = numArrows;
    return result;
}

// ############################################################################
// ############################################################################

void test(int numArrows,
          std::vector<int> aliceArrows,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumBobPoints(numArrows, aliceArrows);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(9, {1, 1, 0, 1, 0, 0, 2, 1, 0, 1, 2, 0},
            {0, 0, 0, 0, 1, 1, 0, 0, 1, 2, 3, 1}, trials);
    test(3, {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 2},
            {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0}, trials);
    return 0;
}


