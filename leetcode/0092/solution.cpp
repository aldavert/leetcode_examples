#include "../common/common.hpp"
#include "../common/list.hpp"
#include <utility>

// ############################################################################
// ############################################################################

ListNode * reverseBetween(ListNode * head, int left, int right)
{
    ListNode * current = head->next;
    ListNode * begin = new ListNode(head->val);
    ListNode * end = begin, * it = nullptr;
    int idx = 1;
    for (; idx < left; ++idx, current = current->next)
    {
        it = end;
        end = (end->next = new ListNode(current->val));
    }
    if (it == nullptr) // At the beginning of the list.
    {
        for (; idx < right; ++idx, current = current->next)
            begin = new ListNode(current->val, begin);
    }
    else // In the middle of the list.
    {
        for (; idx < right; ++idx, current = current->next)
            it->next = new ListNode(current->val, it->next);
    }
    for (; current != nullptr; current = current->next)
        end = (end->next = new ListNode(current->val));
    return begin;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> list,
          int left,
          int right,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(list);
        ListNode * reversed = reverseBetween(head, left, right);
        result = list2vec(reversed);
        delete head;
        delete reversed;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 2, 4, {1, 4, 3, 2, 5}, trials);
    test({1, 2, 3, 4, 5}, 1, 4, {4, 3, 2, 1, 5}, trials);
    test({5}, 1, 1, {5}, trials);
    return 0;
}


