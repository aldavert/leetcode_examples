#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string maximumTime(std::string time)
{
    if (time[0] == '?')
        time[0] = ((time[1] >= '4') && (time[1] <= '9'))?'1':'2';
    if (time[1] == '?') time[1] = (time[0] == '2')?'3':'9';
    if (time[3] == '?') time[3] = '5';
    if (time[4] == '?') time[4] = '9';
    return time;
}

// ############################################################################
// ############################################################################

void test(std::string time, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumTime(time);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("0?:3?", "09:39", trials);
    test("1?:22", "19:22", trials);
    test("?4:03", "14:03", trials);
    return 0;
}


