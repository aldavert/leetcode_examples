#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int differenceOfSums(int n, int m)
{
    int result = n * (n + 1) / 2;
    for (int i = 1; i <= n; ++i)
        result -= 2 * i * (i % m == 0);
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int m, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = differenceOfSums(n, m);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, 3, 19, trials);
    test(5, 6, 15, trials);
    test(5, 1, -15, trials);
    return 0;
}


