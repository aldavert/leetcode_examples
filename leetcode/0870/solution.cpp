#include "../common/common.hpp"
#include <set>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> advantageCount(std::vector<int> nums1, std::vector<int> nums2)
{
    int n = static_cast<int>(nums1.size());
    std::sort(nums1.begin(), nums1.end());
    std::vector<std::pair<int, int> > index_value;
    for (int i = 0; i < n; ++i)
        index_value.push_back({nums2[i], i});
    std::sort(index_value.begin(), index_value.end());
    
    std::vector<int> permutation(n);
    for (int index = n - 1, first_p = n - 1, second_p = 0; index >= 0; --index)
    {
        auto [current_value, current_index] = index_value[index];
        if (current_value >= nums1[first_p])
        {
            permutation[current_index] = nums1[second_p];
            ++second_p;
        }
        else
        {
            permutation[current_index] = nums1[first_p];
            --first_p;
        }
    } 
    return permutation;
}
#else
std::vector<int> advantageCount(std::vector<int> nums1, std::vector<int> nums2)
{
    std::multiset<int> s(nums1.begin(), nums1.end());
    for (size_t i = 0, n = nums2.size(); i < n; ++i)
    {
        const auto p = (*s.rbegin() <= nums2[i])?s.begin():s.upper_bound(nums2[i]);
        nums1[i] = *p;
        s.erase(p);
    }
    return nums1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = advantageCount(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 7, 11, 15}, {1, 10, 4, 11}, {2, 11, 7, 15}, trials);
    test({12, 24, 8, 32}, {13, 25, 32, 11}, {24, 32, 8, 12}, trials);
    return 0;
}


