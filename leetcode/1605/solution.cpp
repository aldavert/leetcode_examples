#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> >
restoreMatrix(std::vector<int> rowSum, std::vector<int> colSum)
{
    const int m = static_cast<int>(rowSum.size()),
              n = static_cast<int>(colSum.size());
    std::vector<std::vector<int> > result(m, std::vector<int>(n));
    
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            result[i][j] = std::min(rowSum[i], colSum[j]);
            rowSum[i] -= result[i][j];
            colSum[j] -= result[i][j];
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> rowSum,
          std::vector<int> colSum,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = restoreMatrix(rowSum, colSum);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 8}, {4, 7}, {{3, 0}, {1, 7}}, trials);
    test({5, 7, 10}, {8, 6, 8}, {{5, 0, 0}, {3, 4, 0}, {0, 2, 8}}, trials);
    return 0;
}


