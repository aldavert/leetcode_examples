#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int latestDayToCross(int row, int col, std::vector<std::vector<int> > cells)
{
    constexpr int directions[] = {0, 1, 0, -1, 0};
    int result = 0;
    auto canWalk = [&](int day) -> bool
    {
        std::vector<std::vector<bool> > matrix(row, std::vector<bool>(col));
        std::queue<std::pair<int, int> > q;
        
        for (int i = 0; i < day; ++i)
            matrix[cells[i][0] - 1][cells[i][1] - 1] = true;
        for (int j = 0; j < col; ++j)
        {
            if (!matrix[0][j])
            {
                q.emplace(0, j);
                matrix[0][j] = true;
            }
        }

        while (!q.empty())
        {
            auto [i, j] = q.front();
            q.pop();
            for (int k = 0; k < 4; ++k)
            {
                const int x = i + directions[k];
                const int y = j + directions[k + 1];
                if ((x < 0) || (x == row) || (y < 0) || (y == col)) continue;
                if (matrix[x][y]) continue;
                if (x == row - 1) return true;
                q.emplace(x, y);
                matrix[x][y] = true;
            }
        }
        
        return false;
    };
    
    for (int l = 1, r = static_cast<int>(cells.size()) - 1; l <= r; )
    {
        if (int m = (l + r) / 2; canWalk(m))
            result = m,
            l = m + 1;
        else r = m - 1;
    }
    
    return result;
}

// ############################################################################
// ############################################################################

void test(int row,
          int col,
          std::vector<std::vector<int> > cells,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = latestDayToCross(row, col, cells);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 2, {{1, 1}, {2, 1}, {1, 2}, {2, 2}}, 2, trials);
    test(2, 2, {{1, 1}, {1, 2}, {2, 1}, {2, 2}}, 1, trials);
    test(3, 3, {{1, 2}, {2, 1}, {3, 3}, {2, 2}, {1, 1}, {1, 3}, {2, 3}, {3, 2},
                {3, 1}}, 3, trials);
    return 0;
}


