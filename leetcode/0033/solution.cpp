#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int search(std::vector<int> nums, int target)
{
    int left = 0, right = static_cast<int>(nums.size()) - 1;
    while (left < right)
    {
        int mid = (left + right) / 2;
        if (nums[mid] == target) return mid;
        if (nums[mid] < nums[left])
        {
            if ((nums[mid] <= target) && (target <= nums[right]))
                left = mid;
            else right = mid - 1;
        }
        else
        {
            if ((nums[left] <= target) && (target <= nums[mid]))
                right = mid;
            else left = mid + 1;
        }
    }
    return (nums[left] == target)?left:-1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = search(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 5, 6, 7, 0, 1, 2}, 0,  4, trials);
    test({4, 5, 6, 7, 0, 1, 2}, 3, -1, trials);
    test({1}, 0, -1, trials);
    test({5, 1, 3}, 3, 2, trials);
    return 0;
}


