#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumGood(std::vector<std::vector<int> > statements)
{
    const int n = static_cast<int>(statements.size()),
              max_mask = 1 << n;
    int result = 0;
    auto isValid = [&](int mask) -> bool
    {
        for (int i = 0; i < n; ++i)
        {
            if (!(mask >> i & 1)) continue;
            for (int j = 0; j < n; ++j)
            {
                if (statements[i][j] == 2) continue;
                if (statements[i][j] != (mask >> j & 1))
                    return false;
            }
        }
        return true;
    };
    
    for (int mask = 0; mask < max_mask; ++mask)
        if (isValid(mask))
            result = std::max(result, __builtin_popcount(mask));
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > statements,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumGood(statements);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 1, 2}, {1, 2, 2}, {2, 0, 2}}, 2, trials);
    test({{2, 0}, {0, 2}}, 1, trials);
    return 0;
}


