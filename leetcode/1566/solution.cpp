#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool containsPattern(std::vector<int> arr, int m, int k)
{
    const int n = static_cast<int>(arr.size());
    for (int i = 0; i <= k * (n - m); ++i)
    {
        int repetitions = 1;
        for (int j = i + m; j <= n - m; j += m)
        {
            bool same = true;
            for (int o = 0; same && (o < m); ++o)
                same = arr[i + o] == arr[j + o];
            if (same) ++repetitions;
            else break;
        }
        if (repetitions == k) return true;
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr,
          int m,
          int k,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = containsPattern(arr, m, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 4, 4, 4, 4}, 1, 3, true, trials);
    test({1, 2, 1, 2, 1, 1, 1, 3}, 2, 2, true, trials);
    test({1, 2, 1, 2, 1, 3}, 2, 3, false, trials);
    test({2, 2}, 1, 2, true, trials);
    test({1, 2, 3, 1, 2}, 2, 2, false, trials);
    test({2, 2, 1, 2, 2, 1, 1, 1, 2, 1}, 2, 2, false, trials);
    return 0;
}


