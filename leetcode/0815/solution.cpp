#include "../common/common.hpp"
#include <unordered_map>
#include <queue>
#include <bitset>

// ############################################################################
// ############################################################################

#if 0
int numBusesToDestination(std::vector<std::vector<int> > routes,
                          int source,
                          int target)
{
    const int n = static_cast<int>(routes.size());
    std::vector<int> distance(1'000'000, 501);
    distance[source] = 0;
    std::unordered_set<int> visited;
    size_t count = 0;
    while ((visited.size() < routes.size()) && (count < routes.size()))
    {
        for (int bus = 0; bus < n; ++bus)
        {
            if (visited.find(bus) != visited.end()) continue;
            int min_distance = 501;
            for (auto i : routes[bus])
                min_distance = std::min(min_distance, distance[i] + 1);
            for (auto i : routes[bus])
                distance[i] = std::min(distance[i], min_distance);
            if (min_distance < 501) visited.emplace(bus);
        }
        ++count;
    }
    return (distance[target] == 501)?-1:distance[target];
}
#elif 0
int numBusesToDestination(std::vector<std::vector<int> > routes,
                          int source,
                          int target)
{
    if (target == source) return 0;
    struct Bus
    {
        bool target = false;
        bool source = false;
        std::vector<int> neighbors;
    };
    const int n = static_cast<int>(routes.size());
    std::unordered_map<int, std::vector<int> > buses_at_stop;
    std::vector<Bus> graph(n);
    std::vector<int> starting_buses;
    bool target_found = false;
    for (int bus_id = 0; bus_id < n; ++bus_id)
    {
        for (int stop : routes[bus_id])
        {
            graph[bus_id].source = graph[bus_id].source || (stop == source);
            graph[bus_id].target = graph[bus_id].target || (stop == target);
            target_found = target_found || (stop == target);
            buses_at_stop[stop].push_back(bus_id);
            if (stop == source)
                starting_buses.push_back(bus_id);
        }
        if (graph[bus_id].source && graph[bus_id].target)
            return 1;
    }
    for (auto &[stop, current] : buses_at_stop)
    {
        if (current.size() == 1) continue;
        const int m = static_cast<int>(current.size());
        for (int i = 0; i < m; ++i)
            for (int j = 0; j < m; ++j)
                if (i != j)
                    graph[current[i]].neighbors.push_back(current[j]);
    }
    if (!target_found || (starting_buses.size() == 0)) return -1;
    int result = std::numeric_limits<int>::max();
    std::bitset<501> visited;
    std::queue<int> q;
    for (int start : starting_buses)
        q.push(start);
    [&](){
        for (int number_of_buses = 1; !q.empty(); ++number_of_buses)
        {
            for (int i = 0, m = static_cast<int>(q.size()); i < m; ++i)
            {
                int bus = q.front();
                q.pop();
                if (visited[bus]) continue;
                visited[bus] = true;
                if (graph[bus].target)
                {
                    result = std::min(result, number_of_buses);
                    return;
                }
                for (int neighbor : graph[bus].neighbors)
                    if (!visited[neighbor])
                        q.push(neighbor);
            }
        }
    }();
    return (result <= n)?result:-1;
}
#elif 0 // THIS VERSION IS TOO SLOW AND EXCEEDS THE TIME LIMIT
int numBusesToDestination(std::vector<std::vector<int> > routes,
                          int source,
                          int target)
{
    struct Edge
    {
        int node_dst;
        int bus_id;
    };
    struct Position
    {
        int node;
        int bus_id;
        int cost;
        bool operator<(const Position &other) const { return cost < other.cost; }
    };
    const int n = static_cast<int>(routes.size());
    std::vector<std::vector<Edge> > graph(1'000'000);
    std::vector<int> visit_cost(1'000'000, std::numeric_limits<int>::max());
    for (int i = 0; i < n; ++i)
        for (int j = 0, m = static_cast<int>(routes[i].size()); j < m; ++j)
            for (int k = 0; k < m; ++k)
                if (j != k)
                    graph[routes[i][j]].push_back({routes[i][k], i});
    std::priority_queue<Position> q;
    q.push({source, n, 0});
    while (!q.empty())
    {
        auto [stop, bus, cost] = q.top();
        q.pop();
        if (cost >= visit_cost[stop])
            continue;
        visit_cost[stop] = cost;
        for (auto [dst_stop, dst_bus] : graph[stop])
        {
            int new_cost = cost + (dst_bus != bus);
            if (new_cost < visit_cost[dst_stop])
            {
                q.push({dst_stop, dst_bus, new_cost});
            }
        }
    }
    return (visit_cost[target] < std::numeric_limits<int>::max())?visit_cost[target]:-1;
}
#else // THIS VERSION IS TOO SLOW AND EXCEEDS THE TIME LIMIT
int numBusesToDestination(std::vector<std::vector<int> > routes,
                          int source,
                          int target)
{
    struct Edge
    {
        int node_dst;
        int bus_id;
    };
    struct Position
    {
        int node;
        int bus_id;
        int cost;
        bool operator<(const Position &other) const
        {
            return (((node == target()) ^ (other.node == target()))
                &&  (cost < other.cost))
                ||  ((node == target()) && (other.node != target()))
                || !((node != target()) && (other.node == target()));
        }
        static int& target(void) { static int target = -1; return target; }
    };
    Position::target() = target;
    const int n = static_cast<int>(routes.size());
    std::vector<std::vector<Edge> > graph(1'000'000);
    std::vector<int> visit_cost(1'000'000, std::numeric_limits<int>::max());
    for (int i = 0; i < n; ++i)
        for (int j = 0, m = static_cast<int>(routes[i].size()); j < m; ++j)
            for (int k = 0; k < m; ++k)
                if (j != k)
                    graph[routes[i][j]].push_back({routes[i][k], i});
    int result = std::numeric_limits<int>::max();
    std::priority_queue<Position> q;
    q.push({source, n, 0});
    while (!q.empty())
    {
        auto [stop, bus, cost] = q.top();
        q.pop();
        if ((cost > result) || (cost >= visit_cost[stop]))
            continue;
        if (stop == target)
        {
            result = std::min(result, cost);
            continue;
        }
        visit_cost[stop] = cost;
        for (auto [dst_stop, dst_bus] : graph[stop])
            if (int ncost = cost + (dst_bus != bus); ncost < visit_cost[dst_stop])
                q.push({dst_stop, dst_bus, ncost});
    }
    return (result < std::numeric_limits<int>::max())?result:-1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > routes,
          int source,
          int target,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numBusesToDestination(routes, source, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 7}, {3, 6, 7}}, 1, 6, 2, trials);
    test({{7, 12}, {4, 5, 15}, {6}, {15, 19}, {9, 12, 13}}, 15, 12, -1, trials);
#if 1
    test({{24}, {3, 6, 11, 14, 22}, {1, 23, 24}, {0, 6, 14}, {1, 3, 8, 11, 20}},
         20, 8, 1, trials);
    test({{12, 24, 62, 75, 81, 83, 84, 85, 91, 102, 127, 141, 145, 153, 170,
           184, 192, 211, 228, 241, 254, 262, 286, 288, 311, 335, 336, 358,
           364}, {1, 2, 3, 6, 11, 14, 19, 20, 26, 40, 44, 45, 70, 71, 84, 88,
           98, 102, 115, 120, 122, 127, 133, 139, 140, 141, 149, 151, 168, 169,
           182, 198, 199, 201, 205, 206, 210, 220, 221, 223, 226, 231, 237, 240,
           242, 247, 264, 266, 267, 283, 302, 327, 332, 351, 355, 357, 362, 363,
           364}, {3, 19, 47, 55, 56, 108, 118, 134, 158, 164, 178, 188, 237,
           240, 245, 266, 288, 305, 334}, {35, 57, 61, 91, 131, 143, 169, 174,
           178, 179, 181, 195, 227, 237, 251, 263, 264, 286, 342, 347}, {9, 18,
           21, 29, 34, 46, 76, 103, 104, 111, 145, 152, 170, 176, 179, 189, 191,
           193, 194, 197, 202, 204, 207, 208, 209, 221, 224, 258, 261, 284, 294,
           329, 338, 345, 364}, {1, 3, 6, 35, 57, 58, 69, 74, 84, 92, 104, 108,
           110, 153, 156, 173, 177, 179, 195, 198, 203, 215, 216, 223, 229, 236,
           241, 253, 280, 282, 283, 285, 297, 312, 314, 323, 334, 358}, {5, 8,
           11, 16, 31, 34, 68, 69, 77, 92, 95, 100, 104, 114, 121, 122, 126,
           127, 140, 150, 154, 158, 160, 161, 174, 186, 191, 194, 198, 201,
           207, 218, 225, 237, 240, 245, 249, 256, 261, 266, 270, 277, 278,
           284, 305, 307, 312, 323, 327, 335, 345, 351}, {4, 8, 10, 11, 12, 24,
           25, 29, 36, 39, 40, 47, 53, 56, 86, 89, 91, 98, 101, 102, 113, 116,
           120, 121, 126, 128, 129, 141, 151, 153, 165, 167, 169, 186, 187, 196,
           199, 202, 208, 210, 222, 227, 235, 249, 251, 271, 275, 281, 311, 313,
           322, 324, 327, 335, 342, 343, 352, 356}, {30, 35, 79, 181, 219, 230,
           232, 250, 332, 335}, {11, 13, 31, 57, 74, 112, 176, 201, 204, 246,
           251, 268, 275, 284, 288, 327, 335, 339}, {21, 25, 26, 33, 34, 62,
           102, 105, 107, 117, 138, 143, 168, 171, 176, 199, 201, 205, 214,
           224, 226, 231, 253, 254, 258, 268, 269, 277, 284, 304, 326, 336,
           351}, {5, 11, 15, 23, 24, 27, 44, 45, 51, 57, 73, 84, 109, 111, 116,
           127, 142, 144, 155, 167, 169, 179, 180, 182, 186, 188, 190, 191, 196,
           201, 203, 204, 208, 213, 214, 227, 229, 233, 240, 243, 244, 245, 254,
           255, 257, 258, 276, 280, 281, 291, 300, 305, 311, 313, 315, 320, 329,
           338, 340, 347, 349, 351, 353, 360}, {126}, {16, 19, 30, 47, 50, 57,
           69, 72, 93, 111, 113, 137, 169, 188, 200, 201, 209, 213, 227, 229,
           231, 237, 272, 278, 284, 313, 322, 337, 338, 342, 345, 358}, {89,
           121, 126, 204}, {0, 1, 4, 24, 25, 38, 43, 46, 47, 54, 55, 60, 62, 72,
           81, 82, 95, 97, 102, 107, 109, 113, 116, 118, 125, 126, 134, 167,
           174, 178, 179, 180, 192, 200, 202, 217, 219, 224, 247, 279, 281, 288,
           291, 293, 303, 308, 314, 347, 349, 356, 362, 363, 364}, {32, 45, 63,
           69, 132, 196, 220, 249, 255, 269}, {155, 202}, {2, 5, 23, 28, 43, 56,
           70, 71, 72, 95, 111, 116, 130, 156, 165, 166, 170, 219, 225, 226,
           282, 286, 288, 295, 300, 322, 346, 362}, {5, 18, 40, 71, 108, 112,
           114, 131, 183, 195, 204, 232, 240, 241, 256, 263, 289, 293, 301, 306,
           311, 331, 349, 363}, {3, 6, 13, 14, 23, 30, 31, 52, 57, 60, 64, 67,
           68, 90, 97, 106, 110, 113, 124, 125, 133, 141, 147, 148, 152, 162,
           164, 165, 167, 174, 179, 186, 187, 195, 197, 199, 215, 225, 227, 228,
           245, 247, 254, 266, 273, 274, 276, 279, 282, 284, 291, 292, 295, 297,
           304, 311, 314, 319, 325, 327, 328, 335, 348, 353, 359}, {3, 5, 20,
           37, 42, 56, 58, 60, 61, 65, 66, 68, 74, 82, 124, 128, 145, 149, 154,
           156, 157, 158, 171, 177, 186, 192, 196, 221, 230, 231, 234, 238, 251,
           256, 259, 260, 264, 289, 292, 314, 319, 323, 329, 343, 344, 346},
           {105, 147, 220, 252, 294, 323}, {0, 49, 170, 231, 324, 330, 338, 345},
           {1, 3, 7, 19, 27, 35, 50, 59, 65, 66, 75, 77, 80, 85, 92, 97, 109,
           118, 121, 130, 131, 147, 148, 163, 176, 179, 184, 196, 208, 221, 247,
           249, 254, 259, 266, 267, 280, 284, 300, 303, 319, 321, 338, 342,
           364}, {11, 26, 45, 51, 52, 59, 62, 64, 72, 76, 108, 118, 121, 145,
           156, 163, 167, 170, 202, 204, 205, 210, 211, 215, 223, 230, 238, 244,
           257, 279, 298, 303, 306, 321, 330, 349, 352}, {9, 19, 20, 46, 48, 50,
           51, 52, 54, 70, 72, 76, 83, 92, 93, 95, 111, 112, 115, 132, 133, 134,
           150, 152, 160, 170, 171, 180, 184, 185, 189, 197, 198, 201, 211, 213,
           216, 217, 223, 224, 232, 233, 236, 237, 247, 264, 273, 278, 280, 286,
           293, 296, 297, 314, 316, 318, 343, 348, 353}, {42, 54, 63, 82, 92,
           109, 116, 136, 138, 143, 151, 152, 166, 170, 181, 189, 190, 206, 210,
           213, 228, 235, 237, 239, 240, 245, 249, 259, 266, 281, 286, 291, 292,
           293, 299, 311, 315, 322, 324, 325, 342, 346, 352}, {0, 17, 26, 32,
           42, 48, 58, 99, 102, 106, 118, 146, 159, 165, 191, 198, 203, 227,
           232, 259, 265, 278, 352, 354}, {0, 2, 42, 69, 86, 106, 110, 113, 142,
           149, 154, 176, 183, 188, 199, 205, 214, 226, 229, 243, 244, 250, 252,
           274, 287, 307, 318, 324, 330, 332, 354, 358, 360, 361}, {10, 18, 22,
           30, 43, 86, 91, 110, 113, 143, 151, 166, 178, 187, 235, 281, 315,
           341, 345, 346, 355}, {13, 48, 52, 55, 58, 69, 83, 102, 106, 114, 128,
           131, 172, 173, 174, 185, 213, 216, 226, 242, 258, 267, 268, 269, 276,
           282, 283, 287, 292, 298, 311, 312, 313, 324, 326, 332, 333, 354, 358,
           363}, {97}, {4, 17, 26, 32, 40, 45, 47, 48, 54, 57, 63, 65, 66, 74,
           77, 79, 88, 95, 102, 106, 114, 117, 127, 133, 141, 151, 159, 160,
           161, 169, 171, 174, 175, 177, 196, 205, 217, 225, 228, 230, 233, 240,
           244, 252, 270, 274, 278, 282, 283, 295, 300, 303, 312, 314, 325, 327,
           341, 347, 350, 357}, {1, 3, 12, 27, 36, 37, 42, 48, 55, 56, 77, 81,
           91, 98, 99, 108, 114, 118, 121, 162, 169, 180, 201, 204, 263, 273,
           275, 279, 287, 322, 343}, {32, 47, 60, 68, 104, 114, 120, 150, 169,
           174, 180, 182, 200, 223, 227, 232, 233, 258, 286, 289, 293, 294, 301,
           303, 317, 338, 353}, {10, 20, 28, 29, 33, 43, 50, 53, 54, 67, 83,
           111, 112, 114, 115, 119, 140, 174, 181, 194, 197, 199, 207, 216, 233,
           236, 277, 285, 331, 338, 349}, {17, 37, 49, 54, 58, 62, 74, 76, 79,
           93, 103, 106, 120, 126, 134, 163, 170, 178, 203, 214, 226, 232, 238,
           240, 263, 266, 275, 278, 296, 320, 321, 325, 332}, {17, 34, 55, 76,
           116, 117, 118, 120, 121, 122, 126, 131, 154, 233, 234, 247, 255, 258,
           285, 301, 314, 319, 323, 332, 336, 344, 352, 356, 360}, {5, 7, 9, 16,
           26, 29, 35, 38, 41, 45, 95, 103, 113, 117, 127, 135, 136, 158, 163,
           197, 207, 225, 282, 306, 317, 339, 348}, {1, 48, 147, 307}, {6, 7,
           17, 30, 38, 40, 47, 71, 95, 101, 103, 118, 166, 168, 181, 204, 220,
           227, 259, 265, 273, 307, 316, 318, 337, 341}, {9, 15, 16, 37, 45, 58,
           59, 68, 70, 86, 92, 103, 105, 115, 119, 124, 125, 129, 152, 154, 190,
           191, 196, 200, 203, 208, 217, 220, 231, 232, 245, 254, 255, 264, 269,
           272, 275, 276, 283, 288, 295, 299, 306, 310, 319, 339, 361}, {15, 46,
           72, 78, 85, 151, 165, 181, 199, 206, 223, 226, 242, 288, 289, 300,
           307, 315, 342, 344, 346, 352}, {0, 3, 4, 8, 15, 29, 43, 50, 52, 54,
           65, 66, 78, 107, 114, 117, 127, 136, 162, 166, 167, 173, 179, 197,
           209, 215, 222, 224, 232, 237, 238, 240, 244, 245, 250, 264, 270, 271,
           272, 297, 302, 331, 333, 334, 351, 357, 360}, {80, 121, 149, 150, 178,
           205, 240, 297}, {11, 59, 62, 127, 161, 176, 181, 247, 274, 286, 288,
           317, 320}, {23, 61, 96, 147, 217, 243, 259, 265, 279, 304}, {0, 22,
           43, 44, 45, 60, 68, 77, 90, 109, 123, 128, 142, 145, 150, 154, 197,
           201, 203, 216, 219, 221, 229, 234, 246, 248, 273, 301, 339, 345, 355,
           362}, {0, 6, 10, 14, 17, 22, 28, 37, 78, 93, 96, 100, 103, 105, 110,
           122, 123, 128, 129, 131, 138, 147, 153, 158, 161, 163, 164, 193, 204,
           205, 209, 228, 229, 240, 247, 252, 255, 260, 270, 275, 285, 293, 301,
           307, 308, 312, 314, 324, 336, 342, 360, 364}, {7, 13, 22, 37, 43, 84,
           87, 104, 107, 120, 158, 169, 171, 215, 223, 234, 247, 256, 264, 286,
           308, 310, 322, 352, 356}, {10, 22, 25, 30, 66, 97, 107, 160, 201, 211,
           248, 288, 322, 326, 332}, {3, 15, 23, 26, 34, 53, 60, 74, 76, 81, 84,
           85, 88, 90, 91, 95, 113, 117, 121, 128, 140, 141, 148, 154, 164, 168,
           178, 179, 180, 181, 184, 187, 205, 209, 218, 219, 223, 224, 230, 234,
           239, 240, 250, 258, 259, 262, 273, 274, 301, 304, 311, 315, 321, 326,
           327, 335, 336, 341, 343, 345, 346, 354, 360}, {9, 33, 44, 66, 77, 96,
           105, 106, 111, 118, 136, 140, 156, 162, 171, 173, 175, 179, 181, 193,
           202, 220, 229, 236, 244, 247, 274, 294, 295, 320, 346, 348, 355}, {9,
           27, 30, 64, 66, 73, 75, 76, 78, 90, 91, 109, 124, 128, 132, 137, 161,
           165, 168, 202, 218, 220, 230, 236, 240, 264, 266, 268, 270, 272, 273,
           277, 289, 311, 319, 336, 343, 345, 356, 364}, {8, 12, 15, 23, 30, 43,
           67, 75, 78, 84, 93, 97, 99, 109, 115, 132, 134, 141, 161, 176, 180,
           193, 199, 200, 207, 215, 220, 224, 231, 245, 248, 249, 252, 258, 264,
           266, 268, 270, 285, 288, 293, 294, 295, 300, 307, 309, 311, 320, 323,
           327, 338, 345, 350, 353, 359}, {7, 38, 45, 59, 64, 99, 114, 124, 135,
           155, 167, 178, 193, 195, 198, 225, 249, 251, 264, 279, 287, 294, 305,
           314, 326, 342, 363}, {8, 57, 90, 109, 126, 150, 184, 239, 247, 250,
           362}, {104, 154, 204, 233}, {20, 75, 131, 150, 170, 207, 212, 227,
           281, 315, 355}, {109, 221, 273}, {0, 10, 12, 32, 35, 44, 67, 74, 75,
           81, 82, 88, 90, 98, 111, 130, 149, 153, 167, 179, 183, 185, 188, 189,
           190, 203, 205, 215, 232, 236, 241, 242, 251, 260, 263, 264, 265, 269,
           270, 274, 286, 295, 302, 315, 316, 320, 325, 327, 330, 349, 362},
           {20, 32, 43, 49, 58, 123, 128, 188, 190, 193, 213, 245, 307, 345},
           {13, 42, 111, 114, 193, 236, 258, 260, 275, 332, 349}, {1, 41, 44,
           57, 64, 77, 88, 108, 109, 112, 123, 131, 161, 174, 182, 199, 201,
           214, 225, 243, 250, 254, 261, 294, 305, 321, 323, 324, 335, 343,
           361}, {208, 319}, {11, 12, 13, 24, 25, 28, 29, 31, 41, 42, 47, 48,
           57, 65, 66, 69, 70, 71, 76, 78, 81, 82, 87, 93, 101, 105, 110, 111,
           131, 134, 152, 177, 179, 180, 186, 203, 207, 208, 213, 217, 228, 241,
           247, 254, 268, 269, 272, 282, 288, 289, 295, 304, 309, 313, 320, 333,
           349, 357, 358, 359, 361}, {10, 19, 52, 55, 68, 85, 101, 187, 244,
           260, 292, 299, 322, 328, 336}, {1, 3, 4, 6, 21, 33, 37, 39, 40, 64,
           70, 72, 85, 93, 106, 114, 118, 121, 123, 143, 152, 188, 190, 194,
           199, 202, 206, 215, 234, 237, 246, 252, 260, 261, 271, 280, 293, 303,
           308, 319, 321, 323, 325, 330, 348, 354, 362}, {209}, {2, 14, 19, 25,
           32, 36, 43, 47, 48, 61, 65, 74, 78, 79, 84, 85, 88, 91, 98, 100, 103,
           106, 107, 118, 124, 136, 144, 185, 191, 203, 204, 208, 212, 217, 231,
           238, 262, 267, 274, 305, 307, 311, 315, 316, 323, 333, 336, 338, 343},
           {15, 25, 31, 74, 102, 130, 134, 173, 175, 215, 276, 319, 325, 330,
           361, 363}, {10, 15, 28, 34, 39, 40, 43, 53, 56, 85, 90, 100, 108,
           112, 135, 140, 152, 160, 162, 164, 165, 167, 181, 195, 201, 209, 215,
           217, 219, 223, 225, 228, 255, 264, 271, 275, 284, 286, 289, 291, 293,
           295, 302, 314, 323, 329, 331, 345, 356, 363}}, 4, 359, 2, trials);
#endif
    return 0;
}


