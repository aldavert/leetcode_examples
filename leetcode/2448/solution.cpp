#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long minCost(std::vector<int> nums, std::vector<int> cost)
{
    const int n = static_cast<int>(nums.size());
    long result = 0;
    int l = *std::min_element(nums.begin(), nums.end());
    int r = *std::max_element(nums.begin(), nums.end());
    auto getCost = [&](int target) -> long 
    {
        long current_cost = 0;
        for (int i = 0; i < n; ++i)
            current_cost += std::abs(nums[i] - target) * static_cast<long>(cost[i]);
        return current_cost;
    };
    
    while (l < r)
    {
        int m = (l + r) / 2;
        long cost1 = getCost(m), cost2 = getCost(m + 1);
        result = std::min(cost1, cost2);
        if (cost1 < cost2) r = m;
        else l = m + 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> cost,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCost(nums, cost);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 5, 2}, {2, 3, 1, 14}, 8, trials);
    test({2, 2, 2, 2, 2}, {4, 2, 8, 1, 3}, 0, trials);
    return 0;
}


