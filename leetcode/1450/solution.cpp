#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int busyStudent(std::vector<int> startTime,
                std::vector<int> endTime,
                int queryTime)
{
    int result = 0;
    for (size_t i = 0, n = startTime.size(); i < n; ++i)
        result += (queryTime >= startTime[i]) && (queryTime <= endTime[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> startTime,
          std::vector<int> endTime,
          int queryTime,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = busyStudent(startTime, endTime, queryTime);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, {3, 2, 7}, 4, 1, trials);
    test({4}, {4}, 4, 1, trials);
    return 0;
}


