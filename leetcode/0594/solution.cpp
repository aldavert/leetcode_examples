#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int findLHS(std::vector<int> nums)
{
    int result = 0;
    std::unordered_map<int, int> histogram;
    for (int n : nums)
        ++histogram[n];
    for (auto [value, frequency] : histogram)
    {
        if (auto search = histogram.find(value - 1); search != histogram.end())
            result = std::max(result, frequency + search->second);
        if (auto search = histogram.find(value + 1); search != histogram.end())
            result = std::max(result, frequency + search->second);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLHS(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 2, 5, 2, 3, 7}, 5, trials);
    test({1, 2, 3, 4}, 2, trials);
    test({1, 1, 1, 1}, 0, trials);
    return 0;
}


