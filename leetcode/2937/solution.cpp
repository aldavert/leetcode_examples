#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findMinimumOperations(std::string s1, std::string s2, std::string s3)
{
    size_t same = 0;
    for (size_t n = std::min({s1.size(), s2.size(), s3.size()});
         (same < n) && (s1[same] == s2[same]) && (s1[same] == s3[same]); ++same);
    if (same == 0) return -1;
    return static_cast<int>(s1.size() - same)
         + static_cast<int>(s2.size() - same)
         + static_cast<int>(s3.size() - same);
}

// ############################################################################
// ############################################################################

void test(std::string s1,
          std::string s2,
          std::string s3,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMinimumOperations(s1, s2, s3);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", "abb", "ab", 2, trials);
    test("dac", "bac", "cac", -1, trials);
    return 0;
}


