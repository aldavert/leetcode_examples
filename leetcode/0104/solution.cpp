#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <iostream>

// ############################################################################
// ############################################################################

int maxDepth(TreeNode * root)
{
    if (root) return std::max(maxDepth(root->left), maxDepth(root->right)) + 1;
    else return 0;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = maxDepth(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 9, 20, null, null, 15, 7}, 3, trials);
    test({1, null, 2}, 2, trials);
    return 0;
}


