#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > spiralMatrix(int m, int n, ListNode * head)
{
    constexpr int dirs[] = {0, 1, 0, -1, 0};
    std::vector<std::vector<int> > result(m, std::vector<int>(n, -1));
    int x = 0, y = 0, d = 0;
    for (ListNode * curr = head; curr; curr = curr->next)
    {
        result[x][y] = curr->val;
        if ((x + dirs[d] < 0) || (x + dirs[d] == m) || (y + dirs[d + 1] < 0)
        ||  (y + dirs[d + 1] == n) || (result[x + dirs[d]][y + dirs[d + 1]] != -1))
            d = (d + 1) % 4;
        x += dirs[d];
        y += dirs[d + 1];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int m,
          int n,
          std::vector<int> list,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(list);
        result = spiralMatrix(m, n, head);
        delete head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 5, {3, 0, 2, 6, 8, 1, 7, 9, 4, 2, 5, 5, 0},
         {{3, 0, 2, 6, 8}, {5, 0, -1, -1, 1}, {5, 2, 4, 9, 7}}, trials);
    test(1, 4, {0, 1, 2}, {{0, 1, 2, -1}}, trials);
    return 0;
}


