#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

class Codec {
public:
    // Encodes a tree to a single string.
    std::string serialize(TreeNode* root)
    {
        if (root == nullptr) return "";
        std::string result;
        std::queue<TreeNode *> active_nodes;
        active_nodes.push(root);
        result += std::to_string(root->val);
        int null_pointers = 0;
        while (!active_nodes.empty())
        {
            TreeNode * current = active_nodes.front();
            active_nodes.pop();
            if (current->left != nullptr)
            {
                active_nodes.push(current->left);
                for (int i = 0; i < null_pointers; ++i)
                    result += " " + std::to_string(null);
                null_pointers = 0;
                result += " " + std::to_string(current->left->val);
            }
            else ++null_pointers;
            if (current->right != nullptr)
            {
                active_nodes.push(current->right);
                for (int i = 0; i < null_pointers; ++i)
                    result += " " + std::to_string(null);
                null_pointers = 0;
                result += " " + std::to_string(current->right->val);
            }
            else ++null_pointers;
        }
        return result;
    }

    // Decodes your encoded data to tree.
    TreeNode * deserialize(std::string data)
    {
        if (data.empty()) return nullptr;
        std::queue<TreeNode *> active_nodes;
        
        TreeNode * result = nullptr;
        bool left = true;
        auto process = [&](int v) -> void
        {
            if (v != null)
            {
                if (result == nullptr)
                {
                    result = new TreeNode(v);
                    active_nodes.push(result);
                    active_nodes.push(result);
                }
                else
                {
                    TreeNode * current = new TreeNode(v);
                    if (left) active_nodes.front()->left = current;
                    else active_nodes.front()->right = current;
                    active_nodes.pop();
                    active_nodes.push(current);
                    active_nodes.push(current);
                    left = !left;
                }
            }
            else
            {
                active_nodes.pop();
                left = !left;
            }
        };
        int sign = 1, numero = 0;
        for (char c : data)
        {
            if (c == '-') sign = -1;
            else if (c == ' ')
            {
                process(sign * numero);
                sign = 1;
                numero = 0;
            }
            else numero = numero * 10 + c - '0';
        }
        process(sign * numero);
        return result;
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    Codec ser, deser;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(solution);
        TreeNode * output = deser.deserialize(ser.serialize(root));
        result = tree2vec(output);
        delete root;
        delete output;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, null, null, 4, 5}, trials);
    test({}, trials);
    return 0;
}


