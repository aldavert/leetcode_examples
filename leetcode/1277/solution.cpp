#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int countSquares(std::vector<std::vector<int> > matrix)
{
    int result = std::accumulate(matrix[0].begin(), matrix[0].end(), 0);;
    for (size_t i = 1; i < matrix.size(); ++i)
    {
        result += matrix[i][0];
        for (size_t j = 1; j < matrix[0].size(); ++j)
            if (matrix[i][j] == 1)
                result += matrix[i][j] += std::min({matrix[i - 1][j - 1],
                                                    matrix[i - 1][j    ],
                                                    matrix[i    ][j - 1]});
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSquares(matrix);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 1, 1}, {1, 1, 1, 1}, {0, 1, 1, 1}}, 15, trials);
    test({{1, 0, 1}, {1, 1, 0}, {1, 1, 0}}, 7, trials);
    return 0;
}


