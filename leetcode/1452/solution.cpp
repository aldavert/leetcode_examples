#include "../common/common.hpp"
#include <unordered_map>
#include <bitset>
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> peopleIndexes(std::vector<std::vector<std::string> > favoriteCompanies)
{
    const int n = static_cast<int>(favoriteCompanies.size());
    std::vector<int> result, idx(n);
    std::iota(idx.begin(), idx.end(), 0);
    std::sort(idx.begin(), idx.end(), [&favoriteCompanies](int lhs, int rhs)
            { return favoriteCompanies[lhs].size() > favoriteCompanies[rhs].size(); });
    std::unordered_map<std::string, std::bitset<100> > um;
    for (int i : idx)
    {
        std::bitset<100> mask;
        mask.set();
        for (const auto &comp : favoriteCompanies[i])
        {
            mask &= um[comp];
            um[comp].set(i);
        }
        if (mask.none())
            result.push_back(i);
    }
    std::sort(result.begin(), result.end());
    return result;
}
#else
std::vector<int> peopleIndexes(std::vector<std::vector<std::string> > favoriteCompanies)
{
    std::vector<int> result;
    std::vector<std::unordered_set<std::string> > sets(favoriteCompanies.size());
    for (int index = 0; auto &companies : favoriteCompanies)
    {
        for (auto &company : companies)
            sets[index].insert(company);
        ++index;
    }
    for (size_t i = 0; i < favoriteCompanies.size(); ++i)
    {
        bool found = false;
        for (size_t j = 0; !found && (j < favoriteCompanies.size()); ++j)
        {
            if (i == j) continue;
            size_t count = 0;
            for (auto &company : sets[i])
                count += (sets[j].find(company) != sets[j].end());
            if (count == sets[i].size())
                found = true;
        }
        if (!found) result.push_back(static_cast<int>(i));
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<std::string> > favoriteCompanies,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = peopleIndexes(favoriteCompanies);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{"leetcode", "google", "facebook"}, {"google", "microsoft"},
          {"google", "facebook"}, {"google"}, {"amazon"}}, {0, 1, 4}, trials);
    test({{"leetcode", "google", "facebook"},
          {"leetcode", "amazon"}, {"facebook", "google"}}, {0, 1}, trials);
    test({{"leetcode"}, {"google"}, {"facebook"}, {"amazon"}}, {0, 1, 2, 3}, trials);
    return 0;
}


