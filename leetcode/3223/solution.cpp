#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumLength(std::string s)
{
    int result = 0, count[26] = {};
    for (char c : s)
        ++count[c - 'a'];
    for (int i = 0; i < 26; ++i)
        if (count[i] > 0)
            result += 1 + (count[i] % 2 == 0);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumLength(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abaacbcbb", 5, trials);
    test("aa", 2, trials);
    return 0;
}


