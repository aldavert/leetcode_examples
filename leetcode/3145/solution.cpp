#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findProductsOfElements(std::vector<std::vector<long long> > queries)
{
    auto bitLength = [](long x) -> int
    {
        return (x == 0)?0: 64 - __builtin_clzl(x);
    };
    auto modPow = [&](auto &&self, long x, long n, int mod) -> long
    {
        if (n == 0) return 1 % mod;
        if (n % 2 == 1) return x * self(self, x % mod, (n - 1), mod) % mod;
        return static_cast<int>(self(self, x * x % mod, (n / 2), mod) % mod);
    };
    auto sumBitsTill = [](long x) -> long
    {
        long sum_bits = 0;
        for (long power_of_two = 1; power_of_two <= x; power_of_two *= 2)
            sum_bits += (x / (2L * power_of_two)) * power_of_two
                     +  std::max(0L, x % (2L * power_of_two) + 1 - power_of_two);
        return sum_bits;
    };
    auto sumPowersFirstKBigNums = [&](long k) -> long
    {
        long num = 1, r = k;
        while (num < r)
        {
            long m = (num + r) / 2;
            if (sumBitsTill(m) < k) num = m + 1;
            else r = m;
        }
        long sum_powers = 0;
        for (long p = 0, p_two = 1; p < bitLength(num - 1); ++p, p_two *= 2)
            sum_powers += ((num - 1) / (2L * p_two)) * p_two * p
                       +  std::max(0L, (num - 1) % (2L * p_two) + 1 - p_two) * p;
        long remaining_count = k - sumBitsTill(num - 1);
        for (int p = 0; p < bitLength(num); ++p)
        {
            if (num >> p & 1)
            {
                sum_powers += p;
                --remaining_count;
                if (remaining_count == 0) break;
            }
        }
        return sum_powers;
    };
    std::vector<int> result;
    for (const auto &query : queries)
    {
        long diff = sumPowersFirstKBigNums(query[1] + 1)
                  - sumPowersFirstKBigNums(query[0]);
        result.push_back(static_cast<int>(modPow(modPow, 2, diff, (int)query[2])));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<long long> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findProductsOfElements(queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 3, 7}}, {4}, trials);
    test({{2, 5, 3}, {7, 7, 4}}, {2, 2}, trials);
    return 0;
}


