#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sumDistance(std::vector<int> nums, std::string s, int d)
{
    constexpr int MOD = 1'000'000'007;
    const long n = static_cast<long>(nums.size());
    long result = 0, prefix = 0;
    std::vector<long> pos;
    
    for (long i = 0; i < n; ++i)
    {
        if (s[i] == 'L') pos.push_back(static_cast<long>(nums[i]) - d);
        else pos.push_back(static_cast<long>(nums[i]) + d);
    }
    std::sort(pos.begin(), pos.end());
    for (long i = 0; i < n; ++i)
    {
        result = ((result + i * pos[i] - prefix) % MOD + MOD) % MOD;
        prefix = ((prefix + pos[i]) % MOD + MOD) % MOD;
    }
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::string s,
          int d,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumDistance(nums, s, d);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-2, 0, 2}, "RLL", 3, 8, trials);
    test({1, 0}, "RL", 2, 5, trials);
    return 0;
}


