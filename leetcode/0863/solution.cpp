#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

std::vector<int> distanceK(TreeNode * root, TreeNode * target, int k)
{
    std::queue<TreeNode * > parents;
    auto searchTarget = [&](auto &&self, TreeNode * node) -> TreeNode *
    {
        if (!node) return nullptr;
        if (node == target)
        {
            parents.push(node);
            return target;
        }
        else
        {
            TreeNode * left = self(self, node->left),
                     * right = self(self, node->right);
            if (left || right)
            {
                parents.push(node);
                return (left)?left:right;
            }
            else return nullptr;
        }
    };
    std::vector<int> result;
    auto searchDistance =
        [&](auto &&self, TreeNode * node, TreeNode * avoid, int distance) -> void
    {
        if (!node || (node == avoid)) return;
        if (distance == 0) result.push_back(node->val);
        self(self, node->left , avoid, distance - 1);
        self(self, node->right, avoid, distance - 1);
    };
    
    searchTarget(searchTarget, root);
    for (TreeNode * previous = nullptr; !parents.empty(); --k)
    {
        TreeNode * node = parents.front();
        parents.pop();
        searchDistance(searchDistance, node, previous, k);
        previous = node;
    }
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<int> copy_left = left, copy_right = right;
    std::sort(copy_left.begin(), copy_left.end());
    std::sort(copy_right.begin(), copy_right.end());
    for (size_t i = 0; i < copy_left.size(); ++i)
        if (copy_left[i] != copy_right[i])
            return false;
    return true;
}

void test(std::vector<int> tree,
          int target,
          int k,
          std::vector<int> solution, unsigned int trials = 1)
{
    auto search = [&](auto &&self, TreeNode * node, int value) -> TreeNode *
    {
        if (!node) return nullptr;
        if (node->val == value) return node;
        auto left = self(self, node->left, value);
        auto right = self(self, node->right, value);
        return (left != nullptr)?left:right;
    };
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = distanceK(root, search(search, root, target), k);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 5, 1, 6, 2, 0, 8, null, null, 7, 4}, 5, 2, {7, 4, 1}, trials);
    test({1}, 1, 3, {}, trials);
    return 0;
}


