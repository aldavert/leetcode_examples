#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countDistinctIntegers(std::vector<int> nums)
{
    std::unordered_set<int> nums_set{nums.begin(), nums.end()};
    for (int num : nums)
    {
        int value = 0;
        for (; num > 0; num /= 10) value = value * 10 + num % 10;
        nums_set.insert(value);
    }
    return static_cast<int>(nums_set.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countDistinctIntegers(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 13, 10, 12, 31}, 6, trials);
    test({2, 2, 2}, 1, trials);
    return 0;
}


