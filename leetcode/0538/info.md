# Convert BST to Greater Tree

Given the `root` of a Binary Search Tree (BST), convert it to a Greater Tree such that every key of the original BST is changed to the original key plus the sum of all keys greater than the original key in BST.

As a reminder, a binary search tree is a tree that satisfies these constraints:
- The left subtree of a node contains only nodes with keys **less than** the node's key.
- The right subtree of a node contains only nodes with keys **greater than** the node's key.
- Both the left and right subtrees must also be binary search trees.

#### Example 1:
> ```mermaid 
> graph TD;
> subgraph A2 [30]
> A1((4))
> end
> subgraph B2 ["<div style='margin:0 0ex 0 0;'>36</div>"]
> B1((1))
> end
> subgraph C2 ["<div style='margin:0 0ex 0 0;'>21</div>"]
> C1((6))
> end
> subgraph D2 ["<div style='margin:0 4ex 0 0;'>36</div>"]
> D1((0))
> end
> subgraph E2 ["<div style='margin:0 4ex 0 0;'>35</div>"]
> E1((2))
> end
> subgraph F2 ["<div style='margin:0 4ex 0 0;'>26</div>"]
> F1((5))
> end
> subgraph G2 ["<div style='margin:0 4ex 0 0;'>15</div>"]
> G1((7))
> end
> subgraph H2 ["<div style='margin:0 4ex 0 0;'>33</div>"]
> H1((3))
> end
> subgraph I2 ["<div style='margin:0 4ex 0 0;'>8</div>"]
> I1((8))
> end
> A1---B1
> A1---C1
> B1---D1
> B1---E1
> C1---F1
> C1---G1
> E1---EMPTY1(( ))
> E1---H1
> G1---EMPTY2(( ))
> G1---I1
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px,color:#00F;
> class EMPTY1,EMPTY2,A2,B2,C2,D2,E2,F2,G2,H2,I2 empty;
> linkStyle 6,8 stroke-width:0px
> ```
> *Input:* `root = [4, 1, 6, 0, 2, 5, 7, null, null, null, 3, null, null, null, 8]`  
> *Output:* `[30, 36, 21, 36, 35, 26, 15, null, null, null, 33, null, null, null, 8]`

#### Example 2:
> *Input:* `root = [0, null, 1]`
> *Output:* `[1, null, 1]`
 
#### Constraints:
- The number of nodes in the tree is in the range `[0, 10^4]`.
- `-10^4 <= Node.val <= 10^4`
- All the values in the tree are **unique**.
- `root` is guaranteed to be a valid binary search tree.
 
**Note:** This question is the same as **1038**


