#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int preimageSizeFZF(int k)
{
    auto trailingZeroes = [&](auto &&self, long n) -> long
    {
        return (n == 0)?0:n / 5 + self(self, n / 5);
    };
    long l = 0;
    long r = 5L * k;
    while (l < r)
    {
        const long m = (l + r) / 2;
        if (trailingZeroes(trailingZeroes, m) >= k) r = m;
        else l = m + 1;
    }
    return (trailingZeroes(trailingZeroes, l) == k)?5:0;
}

// ############################################################################
// ############################################################################

void test(int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = preimageSizeFZF(k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(0, 5, trials);
    test(5, 0, trials);
    test(3, 5, trials);
    test(1'000'000'000, 5, trials);
    return 0;
}


