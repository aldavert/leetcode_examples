#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minProcessingTime(std::vector<int> processorTime, std::vector<int> tasks)
{
    int result = 0;
    std::sort(processorTime.begin(), processorTime.end());
    std::sort(tasks.begin(), tasks.end(), std::greater<>());
    for (size_t i = 0; i < processorTime.size(); ++i)
        result = std::max(result, processorTime[i] + tasks[i * 4]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> processorTime,
          std::vector<int> tasks,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minProcessingTime(processorTime, tasks);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 10}, {2, 2, 3, 1, 8, 7, 4, 5}, 16, trials);
    test({10, 20}, {2, 3, 1, 2, 5, 8, 4, 3}, 23, trials);
    return 0;
}


