#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int minDifference(std::vector<int> nums)
{
    int n = static_cast<int>(nums.size()),
        max_gap = 0,
        min_n = std::numeric_limits<int>::max(),
        max_n = 0;
    for (int i = 0; i + 1 < n; ++i)
    {
        if ((std::min(nums[i], nums[i + 1]) == -1)
        &&  (std::max(nums[i], nums[i + 1]) != -1))
        {
            min_n = std::min(min_n, std::max(nums[i], nums[i + 1]));
            max_n = std::max(max_n, std::max(nums[i], nums[i + 1]));
        }
        else max_gap = std::max(max_gap, std::abs(nums[i] - nums[i + 1]));
    }
    int result = 0, min_2r = (max_n - min_n + 2) / 3 * 2;
    for (int i = 0; i < n; ++i)
    {
        if (((i > 0) && (nums[i - 1] == -1)) || (nums[i] > 0)) continue;
        int j = i;
        while ((j < n) && (nums[j] == -1)) ++j;
        int a = std::numeric_limits<int>::max(), b = 0;
        if (i > 0)
        {
            a = std::min(a, nums[i - 1]);
            b = std::max(b, nums[i - 1]);
        }
        if (j < n)
        {
            a = std::min(a, nums[j]);
            b = std::max(b, nums[j]);
        }
        if(j - i == 1)
            result = std::max(result, std::min(max_n - a, b - min_n));
        else result = std::max(result, std::min({max_n -a, b-min_n, min_2r}));
    }
    return std::max(max_gap, (result + 1) / 2);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDifference(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, -1, 10, 8}, 4, trials);
    test({-1, -1, -1}, 0, trials);
    return 0;
}


