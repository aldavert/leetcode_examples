#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> >
spiralMatrixIII(int rows, int cols, int rStart, int cStart)
{
    constexpr int dx[] = {1, 0, -1, 0},
                  dy[] = {0, 1, 0, -1};
    std::vector<std::vector<int> > result{{rStart, cStart}};
    
    for (int i = 0; static_cast<int>(result.size()) < rows * cols; ++i)
    {
        for (int step = 0; step < i / 2 + 1; ++step)
        {
            rStart += dy[i % 4];
            cStart += dx[i % 4];
            if ((0 <= rStart) && (rStart < rows) && (0 <= cStart) && (cStart < cols))
                result.push_back({rStart, cStart});
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int rows,
          int cols,
          int rStart,
          int cStart,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = spiralMatrixIII(rows, cols, rStart, cStart);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 4, 0, 0, {{0, 0}, {0, 1}, {0, 2}, {0, 3}}, trials);
    test(5, 6, 1, 4, {{1, 4}, {1, 5}, {2, 5}, {2, 4}, {2, 3}, {1, 3}, {0, 3},
                      {0, 4}, {0, 5}, {3, 5}, {3, 4}, {3, 3}, {3, 2}, {2, 2},
                      {1, 2}, {0, 2}, {4, 5}, {4, 4}, {4, 3}, {4, 2}, {4, 1},
                      {3, 1}, {2, 1}, {1, 1}, {0, 1}, {4, 0}, {3, 0}, {2, 0},
                      {1, 0}, {0, 0}}, trials);
    return 0;
}


