#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

long long findMaximumElegance(std::vector<std::vector<int> > items, int k)
{
    std::unordered_set<int> seen_categories;
    std::stack<int> decreasing_duplicate_profits;
    auto elegance = [](long profit, long distinct_categories) -> long
    {
        return profit + distinct_categories * distinct_categories;
    };
    
    std::sort(items.begin(), items.end(), std::greater<>());
    long total_profit = 0;
    for (int i = 0; i < k; ++i)
    {
        total_profit += items[i][0];
        if (seen_categories.contains(items[i][1]))
            decreasing_duplicate_profits.push(items[i][0]);
        else seen_categories.insert(items[i][1]);
    }
    long result = elegance(total_profit, seen_categories.size());
    for (int i = k, n = static_cast<int>(items.size()); i < n; ++i)
    {
        if (!seen_categories.contains(items[i][1])
        && !decreasing_duplicate_profits.empty())
        {
            total_profit -= decreasing_duplicate_profits.top(),
            decreasing_duplicate_profits.pop();
            total_profit += items[i][0];
            seen_categories.insert(items[i][1]);
            result = std::max(result, elegance(total_profit, seen_categories.size()));
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > items,
          int k,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMaximumElegance(items, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 2}, {5, 1}, {10, 1}}, 2, 17, trials);
    test({{3, 1}, {3, 1}, {2, 2}, {5, 3}}, 3, 19, trials);
    test({{1, 1}, {2, 1}, {3, 1}}, 3, 7, trials);
    return 0;
}


