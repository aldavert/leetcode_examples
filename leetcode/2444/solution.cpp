#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long countSubarrays(std::vector<int> nums, int minK, int maxK)
{
    long long result = 0;
    int j = -1, previous_min = -1, previous_max = -1;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        if ((nums[i] < minK) || (nums[i] > maxK)) j = i;
        if (nums[i] == minK) previous_min = i;
        if (nums[i] == maxK) previous_max = i;
        result += std::max(0, std::min(previous_min, previous_max) - j);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int minK,
          int maxK,
          long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSubarrays(nums, minK, maxK);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 5, 2, 7, 5}, 1, 5, 2, trials);
    test({1, 1, 1, 1}, 1, 1, 10, trials);
    return 0;
}


