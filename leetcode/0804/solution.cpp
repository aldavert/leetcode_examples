#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int uniqueMorseRepresentations(std::vector<std::string> words)
{
    static const std::string morse[] = {".-", "-...", "-.-.", "-..", ".", "..-.",
        "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.",
        "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
    std::unordered_set<std::string> unique;
    for (const std::string &word : words)
    {
        std::string current;
        for (char c : word)
            current += morse[c - 'a'];
        unique.insert(current);
    }
    return static_cast<int>(unique.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = uniqueMorseRepresentations(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"gin", "zen", "gig", "msg"}, 2, trials);
    test({"a"}, 1, trials);
    return 0;
}


