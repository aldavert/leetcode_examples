#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int minImpossibleOR(std::vector<int> nums)
{
    int bitmask = 0, result = 1;
    for (int x : nums)
        if ((x & (x - 1)) == 0)
            bitmask |= x;
    while ((result & bitmask) > 0)
        result <<= 1;
    return result;
}
#else
int minImpossibleOR(std::vector<int> nums)
{
    int result = 1;
    std::unordered_set<int> nums_set{nums.begin(), nums.end()};
    while (nums_set.contains(result))
        result <<= 1;
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minImpossibleOR(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1}, 4, trials);
    test({5, 3, 2}, 1, trials);
    return 0;
}


