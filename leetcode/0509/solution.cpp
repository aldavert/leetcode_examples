#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int fib(int n)
{
    if (n < 1) return 0;
    int previous = 1, sum = 1;
    for (int i = 2; i < n; ++i)
        sum += std::exchange(previous, sum);
    return sum;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = fib(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 1, trials);
    test(2, 1, trials);
    test(3, 2, trials);
    test(4, 3, trials);
    return 0;
}


