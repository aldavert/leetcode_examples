#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <iostream>

// ############################################################################
// ############################################################################

bool isSymmetric(TreeNode * root)
{
    if (!root) return true;
    auto process = [&](auto &&self, TreeNode * p, TreeNode * q)
    {
        if (!p && !q)
            return true;
        if ((!p && q) || (p && !q))
            return false;
        if (p->val != q->val)
            return false;
        return self(self, p->left, q->right) && self(self, p->right, q->left);
    };
    return process(process, root->left, root->right);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = isSymmetric(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 3, 4, 4, 3}, true, trials);
    test({1, 2, 2, null, 3, null, 3}, false, trials);
    return 0;
}


