#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumTotal(std::vector<std::vector<int> > triangle)
{
    for (int i = static_cast<int>(triangle.size()) - 2; i >= 0; --i)
        for (int j = 0; j <= i; ++j)
            triangle[i][j] += std::min(triangle[i + 1][j], triangle[i + 1][j + 1]);
    return triangle[0][0];
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > triangle,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumTotal(triangle);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2}, {3, 4}, {6, 5, 7}, {4, 1, 8, 3}}, 11, trials);
    test({{-10}}, -10, trials);
    return 0;
}


