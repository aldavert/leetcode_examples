#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> longestObstacleCourseAtEachPosition(std::vector<int> obstacles)
{
    std::vector<int> result, tail;
    for (const int obstacle : obstacles)
    {
        if (tail.empty() || (obstacle >= tail.back()))
        {
            tail.push_back(obstacle);
            result.push_back(static_cast<int>(tail.size()));
        }
        else
        {
            int index = static_cast<int>(std::upper_bound(tail.begin(), tail.end(),
                                         obstacle) - tail.begin());
            tail[index] = obstacle;
            result.push_back(index + 1);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> obstacles, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestObstacleCourseAtEachPosition(obstacles);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 2}, {1, 2, 3, 3}, trials);
    test({2, 2, 1}, {1, 2, 1}, trials);
    test({3, 1, 5, 6, 4, 2}, {1, 1, 2, 3, 2, 2}, trials);
    return 0;
}


