# Remove Nth Node From End of List

Given the `head` of a linked list, remove the `n^th` node from the end of the list and return its head.

#### Example 1:
> ```mermaid 
> graph LR;
> A((1))-->B((2))-->C((3))-->D((4))-->E((5))
> F((1))-->G((2))-->H((3))-->I((5))
> classDef remove  fill:#F77,stroke:#744,stroke-width:2px,color:#FEE;
> class D remove;
> ```
> *Input:* `head = [1, 2, 3, 4, 5], n = 2`  
> *Output:* `[1, 2, 3, 5]`

#### Example 2:
> *Input:* `head = [1], n = 1`  
> *Output:* `[]`

#### Example 3:
> *Input:* `head = [1, 2], n = 1`  
> *Output:* `[1]`

#### Constraints:
- The number of nodes in the list is `sz`.
- `1 <= sz <= 30`
- `0 <= Node.val <= 100`
- `1 <= n <= sz`

**Follow up:** Could you do this in one pass?



