#include "../common/common.hpp"
#include "../common/list.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

ListNode * removeNthFromEnd(ListNode * head, int n)
{
    auto traverse = [&](auto &&self, ListNode * ptr, int pos, ListNode * prev) -> int
    {
        if (ptr == nullptr) return pos;
        int size = self(self, ptr->next, pos + 1, ptr);
        if (n == size - pos)
        {
            prev->next = ptr->next;
            ptr->next = nullptr;
            delete ptr;
        }
        else prev->next = ptr;
        return size;
    };
    ListNode new_head;
    traverse(traverse, head, 0, &new_head);
    ListNode * result = new_head.next;
    new_head.next = nullptr;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> input,
          int n,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(input);
        ListNode * new_head = removeNthFromEnd(head, n);
        result = list2vec(new_head);
        delete new_head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 2, {1, 2, 3, 5}, trials);
    test({1}, 1, {}, trials);
    test({1, 2}, 1, {1}, trials);
    test({1, 2}, 2, {2}, trials);
    return 0;
}


