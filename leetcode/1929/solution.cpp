#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> getConcatenation(std::vector<int> nums)
{
    std::vector<int> result = nums;
    result.insert(result.end(), nums.begin(), nums.end());
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getConcatenation(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1}, {1, 2, 1, 1, 2, 1}, trials);
    test({1, 3, 2, 1}, {1, 3, 2, 1, 1, 3, 2, 1}, trials);
    return 0;
}


