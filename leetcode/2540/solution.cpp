#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int getCommon(std::vector<int> nums1, std::vector<int> nums2)
{
    const int n1 = static_cast<int>(nums1.size()),
              n2 = static_cast<int>(nums2.size());
    for (int i = 0, j = 0; (i < n1) && (j < n2);)
    {
        if (nums1[i] == nums2[j]) return nums1[i];
        else if (nums1[i] < nums2[j]) ++i;
        else ++j;
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getCommon(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, {2, 4}, 2, trials);
    test({1, 2, 3, 6}, {2, 3, 4, 5}, 2, trials);
    return 0;
}



