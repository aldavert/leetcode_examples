#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int maximumMinutes(std::vector<std::vector<int> > grid)
{
    const int n = static_cast<int>(grid.size()),
              m = static_cast<int>(grid[0].size());
    constexpr int directions[] = {0, 1, 0, -1, 0};
    const int KMAX = n * m;
    std::vector<std::vector<int> > fire_minute(n, std::vector<int>(m, -1));
    auto canStayFor = [&](int minute) -> bool
    {
        std::queue<std::pair<int, int> > q{{{0, 0}}};
        std::vector<std::vector<bool> > seen(n, std::vector<bool>(m));
        seen[0][0] = true;
        while (!q.empty())
        {
            ++minute;
            for (size_t sz = q.size(); sz > 0; --sz)
            {
                auto [i, j] = q.front(); q.pop();
                for (int k = 0; k < 4; ++k)
                {
                    int x = i + directions[k], y = j + directions[k + 1];
                    if ((x < 0) || (x == n) || (y < 0) || (y == m) || (grid[x][y] == 2))
                        continue;
                    if ((x == n - 1) && (y == m - 1))
                    {
                        if ((fire_minute[x][y] != -1) && (fire_minute[x][y] < minute))
                            continue;
                        return true;
                    }
                    if ((fire_minute[x][y] != -1) && (fire_minute[x][y] <= minute))
                        continue;
                    if (seen[x][y])
                        continue;
                    q.push({x, y});
                    seen[x][y] = true;
                }
            }
        }
        return false;
    };
    
    std::queue<std::pair<int, int> > q;
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < m; ++j)
            if (grid[i][j] == 1) { q.push({i, j}); fire_minute[i][j] = 0; }
    for (int minute_from_fire = 1; !q.empty(); ++minute_from_fire)
    {
        for (size_t sz = q.size(); sz > 0; --sz)
        {
            auto [i, j] = q.front(); q.pop();
            for (int k = 0; k < 4; ++k)
            {
                int x = i + directions[k], y = j + directions[k + 1];
                if ((x < 0) || (x == n) || (y < 0) || (y == m)
                ||  (grid[x][y] == 2) || (fire_minute[x][y] != -1))
                    continue;
                fire_minute[x][y] = minute_from_fire;
                q.push({x, y});
            }
        }
    }
    int result = -1;
    for (int l = 0, r = KMAX; l <= r; )
    {
        int mid = (l + r) / 2;
        if (canStayFor(mid))
             l = mid + 1, result = mid;
        else r = mid - 1;
    }
    return (result == KMAX)?1'000'000'000:result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumMinutes(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 2, 0, 0, 0, 0, 0},
          {0, 0, 0, 2, 2, 1, 0},
          {0, 2, 0, 0, 1, 2, 0},
          {0, 0, 2, 2, 2, 0, 2},
          {0, 0, 0, 0, 0, 0, 0}}, 3, trials);
    test({{0, 0, 0, 0},
          {0, 1, 2, 0},
          {0, 2, 0, 0}}, -1, trials);
    test({{0, 0, 0},
          {2, 2, 0},
          {1, 2, 0}}, 1'000'000'000, trials);
    return 0;
}


