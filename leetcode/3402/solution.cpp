#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumOperations(std::vector<std::vector<int> > grid)
{
    const size_t m = grid[0].size();
    std::vector<int> row = grid[0];
    int result = 0;
    for (size_t i = 1; i < grid.size(); ++i)
    {
        for (size_t c = 0; c < m; ++c)
        {
            int next = std::max(grid[i][c], row[c] + 1);
            result += std::max(0, next - grid[i][c]);
            row[c] = next;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumOperations(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 2}, {1, 3}, {3, 4}, {0, 1}}, 15, trials);
    test({{3, 2, 1}, {2, 1, 0}, {1, 2, 3}}, 12, trials);
    return 0;
}


