#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

long long maxSum(std::vector<int> nums, int m, int k)
{
    std::unordered_map<int, int> count;
    long result = 0, sum = 0;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        sum += nums[i];
        ++count[nums[i]];
        if (i >= k)
        {
            const int num_to_remove = nums[i - k];
            sum -= num_to_remove;
            if (--count[num_to_remove] == 0)
                count.erase(num_to_remove);
        }
        if (static_cast<int>(count.size()) >= m)
            result = std::max(result, sum);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int m,
          int k,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSum(nums, m, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 6, 7, 3, 1, 7}, 3, 4, 18, trials);
    test({5, 9, 9, 2, 4, 5, 4}, 1, 3, 23, trials);
    test({1, 2, 1, 2, 1, 2, 1}, 3, 3, 0, trials);
    return 0;
}


