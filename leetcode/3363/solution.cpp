#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int maxCollectedFruits(std::vector<std::vector<int> > fruits)
{
    const int n = static_cast<int>(fruits.size()) - 1;
    int result = 0;
    for (int i = 0; i <= n; ++i) result += fruits[i][i];
    for (int i = 1, j = n, t = 0; i < n; ++i, j = t)
    {
        std::vector<int> &v = fruits[i], &w = fruits[i - 1];
        for(int k = (t = std::max(i + 1, j - 1)), ii = i - 1; k <= n; ++k)
        {
            int q = 0, p = 0, kk = k - 1;
            if (kk >= j)
            {
                q = std::max(w[kk], q);
                p = std::max(fruits[kk][ii], p);
            }
            if (++kk >= j)
            {
                q = std::max(w[kk], q);
                p = std::max(fruits[kk][ii], p);
            }
            if ((++kk <= n) && (kk >= j))
            {
                q = std::max(w[kk], q);                
                p = std::max(fruits[kk][ii], p);
            }
            v[k] += q; fruits[k][i] += p;
        }
    }       
    result += fruits[n - 1][n] + fruits[n][n - 1]; 
    return result;        
}
#else
int maxCollectedFruits(std::vector<std::vector<int> > fruits)
{
    const int n = static_cast<int>(fruits.size());
    const std::vector<std::pair<int, int> > directions = {{1, -1}, {1, 0}, {1, 1}};
    auto topLeft = [&](void) -> int
    {
        int result = 0;
        for (int i = 0; i < n; ++i)
            result += fruits[i][i];
        return result;
    };
    auto topRight = [&](void) -> int
    {
        std::vector<std::vector<int> > dp(n, std::vector<int>(n));
        dp[0][n - 1] = fruits[0][n - 1];
        for (int x = 0; x < n; ++x)
        {
            for (int y = 0; y < n; ++y)
            {
                if (x >= y && !(x == n - 1 && y == n - 1)) continue;
                for (auto [dx, dy] : directions)
                {
                    const int i = x - dx, j = y - dy;
                    if ((i < 0) || (i == n) || (j < 0) || (j == n)) continue;
                    if ((i < j) && (j < n - 1 - i)) continue;
                    dp[x][y] = std::max(dp[x][y], dp[i][j] + fruits[x][y]);
                }
            }
        }
        return dp[n - 1][n - 1];
    };
    auto bottomLeft = [&](void) -> int
    {
        std::vector<std::vector<int> > dp(n, std::vector<int>(n));
        dp[n - 1][0] = fruits[n - 1][0];
        for (int y = 0; y < n; ++y)
        {
            for (int x = 0; x < n; ++x)
            {
                if ((x <= y) && !((x == n - 1) && (y == n - 1))) continue;
                for (auto [dy, dx] : directions)
                {
                    const int i = x - dx, j = y - dy;
                    if ((i < 0) || (i == n) || (j < 0) || (j == n)) continue;
                    if ((j < i) && (i < n - 1 - j)) continue;
                    dp[x][y] = std::max(dp[x][y], dp[i][j] + fruits[x][y]);
                }
            }
        }
        return dp[n - 1][n - 1];
    };
    return topLeft() + topRight() + bottomLeft() - 2 * fruits.back().back();
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > fruits,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxCollectedFruits(fruits);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}}, 100, trials);
    test({{1, 1}, {1, 1}}, 4, trials);
    return 0;
}


