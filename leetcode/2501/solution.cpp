#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestSquareStreak(std::vector<int> nums)
{
    nums.erase(std::unique(nums.begin(), nums.end()), nums.end());
    std::sort(nums.begin(), nums.end(), std::greater<>());
    const int max_num = *std::max_element(nums.begin(), nums.end());
    std::vector<int> dp(max_num + 1);
    for (int num : nums)
    {
        dp[num] = 1;
        long squared_num = static_cast<long>(num) * num;
        if (squared_num <= max_num)
        dp[num] += dp[squared_num];
    }
    int result = *std::max_element(dp.begin(), dp.end());
    return (result < 2)?-1:result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestSquareStreak(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 3, 6, 16, 8, 2}, 3, trials);
    test({2, 3, 5, 6, 7}, -1, trials);
    return 0;
}


