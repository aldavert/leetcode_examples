#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findChampion(int n, std::vector<std::vector<int> > edges)
{
    int result = -1, count = 0;
    std::vector<int> in_degrees(n);
    
    for (const auto &edge : edges)
        ++in_degrees[edge[1]];
    for (int i = 0; i < n; ++i)
    {
        if (in_degrees[i] == 0)
        {
            ++count;
            result = i;
        }
    }
    return (count > 1)?-1:result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findChampion(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{0, 1}, {1, 2}}, 0, trials);
    test(4, {{0, 2}, {1, 3}, {1, 2}}, -1, trials);
    return 0;
}


