#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

std::string stoneGameIII(std::vector<int> stoneValue)
{
    const int n = static_cast<int>(stoneValue.size());
    std::vector<int> dp(n + 1, std::numeric_limits<int>::lowest() / 2);
    dp[n] = 0;
    
    for (int i = n - 1; i >= 0; --i)
        for (int j = i, sum = 0; j < i + 3 && j < n; ++j)
            sum += stoneValue[j],
            dp[i] = std::max(dp[i], sum - dp[j + 1]);
    return (dp[0] > 0)?"Alice":((dp[0] < 0)?"Bob":"Tie");
}

// ############################################################################
// ############################################################################

void test(std::vector<int> stoneValue, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = stoneGameIII(stoneValue);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 7}, "Bob", trials);
    test({1, 2, 3, -9}, "Alice", trials);
    test({1, 2, 3, 6}, "Tie", trials);
    return 0;
}


