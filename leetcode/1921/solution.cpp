#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int eliminateMaximum(std::vector<int> dist, std::vector<int> speed)
{
    const int n = static_cast<int>(dist.size());
    std::vector<int> time_to_city(n);
    for (int i = 0; i < n; ++i)
        time_to_city[i] = dist[i] / speed[i] + (dist[i] % speed[i] != 0);
    std::sort(time_to_city.begin(), time_to_city.end());
    for (int i = 1; i < n; ++i)
        if (time_to_city[i] <= i)
            return i;
    return n;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> dist,
          std::vector<int> speed,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = eliminateMaximum(dist, speed);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 4}, {1, 1, 1}, 3, trials);
    test({1, 1, 2, 3}, {1, 1, 1, 1}, 1, trials);
    test({3, 2, 4}, {5, 3, 2}, 1, trials);
    test({3, 5, 7, 4, 5}, {2, 3, 6, 3, 2}, 2, trials);
    return 0;
}


