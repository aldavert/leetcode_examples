#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
int distributeCookies(std::vector<int> cookies, int k)
{
    const int n = static_cast<int>(cookies.size());
    std::sort(cookies.begin(), cookies.end(), std::greater<int>());
    int fair = std::accumulate(cookies.begin(), cookies.end(), 0) / k;
    int childs[8] = {}, result = std::numeric_limits<int>::max();
    auto process = [&](auto &&self, int id) -> void
    {
        if (id == n)
        {
            result = std::min(result, *std::max_element(&childs[0], &childs[k]));
            return;
        }
        for (int i = 0; i < k; ++i)
        {
            if ((i > 0) && (childs[i - 1] == childs[i])) continue;
            if (childs[i] >= fair) continue;
            childs[i] += cookies[id];
            self(self, id + 1);
            childs[i] -= cookies[id];
        }
    }; 
    process(process, 0);
    return result;
}
#else
int distributeCookies(std::vector<int> cookies, int k)
{
    const int n = static_cast<int>(cookies.size());
    int result = std::numeric_limits<int>::max();
    std::vector<int> childs(k);
    auto process = [&](auto &&self, int s) -> void
    {
        if (s == n)
        {
            result = std::min(result, *std::max_element(childs.begin(), childs.end()));
            return;
        }
        for (int i = 0; i < k; ++i)
        {
            childs[i] += cookies[s];
            self(self, s + 1);
            childs[i] -= cookies[s];
        }
    };
    process(process, 0);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> cookies, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = distributeCookies(cookies, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 15, 10, 20, 8}, 2, 31, trials);
    test({6, 1, 3, 2, 2, 4, 1, 2}, 3, 7, trials);
    return 0;
}


