#include "../common/common.hpp"
#include <map>

// ############################################################################
// ############################################################################

std::vector<int> fallingSquares(std::vector<std::vector<int> > positions)
{
    struct Segment
    {
        int begin = -1;
        int end = -1;
        bool operator<(const Segment &other) const
        {
            return (begin < other.begin)
                || ((begin == other.begin) && (end < other.end));
        }
        void intersect(const Segment &other,
                       std::vector<std::tuple<Segment, int> > &buffer,
                       int height) const
        {
            if (other.begin < begin)
                buffer.emplace_back(Segment{other.begin, begin}, height);
            if (end < other.end)
                buffer.emplace_back(Segment{end, other.end}, height);
        }
    };
    std::vector<int> result;
    std::map<Segment, int> elevation;
    int maximum_elevation = 0;
    for (const auto &current : positions)
    {
        Segment segment = {current[0], current[0] + current[1]};
        auto search = elevation.upper_bound(segment);
        if ((search != elevation.begin()) && ((--search)->first.end <= segment.begin))
            ++search;
        int segment_elevation = 0;
        std::vector<std::tuple<Segment, int> > buffer;
        while ((search != elevation.end()) && (search->first.begin < segment.end))
        {
            auto [previous, current_height] = *search;
            segment.intersect(previous, buffer, current_height);
            segment_elevation = std::max(segment_elevation, current_height);
            search = elevation.erase(search);
        }
        segment_elevation += current[1];
        elevation[segment] = segment_elevation;
        for (const auto &[intersected, h] : buffer)
            elevation[intersected] = h;
        maximum_elevation = std::max(maximum_elevation, segment_elevation);
        result.push_back(maximum_elevation);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > positions,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = fallingSquares(positions);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 3}, {6, 1}}, {2, 5, 5}, trials);
    test({{1, 2}, {3, 3}, {6, 1}}, {2, 3, 3}, trials);
    test({{1, 2}, {3, 2}, {5, 2}}, {2, 2, 2}, trials);
    test({{100, 100}, {200, 100}}, {100, 100}, trials);
    return 0;
}


