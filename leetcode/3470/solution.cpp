#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> permute(int n, long long k)
{
    std::set<int> odd, even;
    for (int i = 1; i <= n; ++i)
    {
        if (i & 1) odd.insert(i);
        else even.insert(i);
    }
    std::vector<int> result;
    auto get = [](std::set<int> &s, int kp) -> int
    {
        if (static_cast<int>(s.size()) < kp) return -1;
        int output = 0;
        for (auto &x : s)
        {
            if (--kp == 0)
            {
                output = x;
                break;
            }
        }
        s.erase(output);
        return output;
    };
    auto nth = [&](int a, int b) -> long long
    {
        long long output = k;
        for(int i = 1; i <= a; ++i) output /= i;
        for(int i = 1; i < b; ++i) output /= i;
        return output + 1;
    };
    auto nextK = [&](int base, int a, int b) -> long long
    {
        if (base == 1) return k;
        long long op = 1;
        for (int i = 1; i <= a; ++i) op *= i;
        for (int i = 1; i < b; ++i) op *= i;
        return k - (base - 1) * op;
    };
    auto process = [&](auto &&self, int pos) -> void
    {
        if (even.size() + odd.size() == 1)
        {
            if (even.size()) result.push_back(*even.begin());
            if (odd.size()) result.push_back(*odd.begin());
            return;
        }
        std::set<int> &now = (result.back() & 1)?even:odd,
                      &prv = (result.back() & 1)?odd:even;
        int _n = (int)now.size(), p = (int)prv.size(), base = (int)nth(p, _n);
        result.push_back(get(now, base));
        k = nextK(base, p, _n);
        self(self, pos + 1);
    };
    
    --k;
    if (even.size() + odd.size() == 1)
    {
        if (even.size()) result.push_back(*even.begin());
        if (odd.size()) result.push_back(*odd.begin());
    }
    else
    {
        int e = static_cast<int>(even.size()),
            o = static_cast<int>(odd.size()),
            base = static_cast<int>(nth(e, o));
        if (e == o)
        {
            if (e + o >= base)
            {
                result.push_back(get((base & 1)?odd:even, (base + 1) / 2));
                if (result.back() >= 0)
                {
                    k = nextK(base, e, o);
                    process(process, 1);
                }
            }
        }
        else
        {
            result.push_back(get(odd, base));
            if (result.back() >= 0)
            {
                k = nextK(base, e, o);
                process(process, 1);
            }
        }
    }
    if ((k != 0) || (*min_element(result.begin(), result.end()) != 1)) return {};
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, long long k, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = permute(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 6, {3, 4, 1, 2}, trials);
    test(3, 2, {3, 2, 1}, trials);
    test(2, 3, {}, trials);
    return 0;
}


