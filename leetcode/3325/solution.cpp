#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfSubstrings(std::string s, int k)
{
    int result = 0;
    for (int left = 0, histogram[26] = {}; char c : s)
    {
        ++histogram[c - 'a'];
        while (histogram[c - 'a'] >= k)
            --histogram[s[left++] - 'a'];
        result += left;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfSubstrings(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abacb", 2, 4, trials);
    test("abcde", 1, 15, trials);
    return 0;
}


