#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maximumHappinessSum(std::vector<int> happiness, int k)
{
    long long result = 0;
    std::sort(happiness.begin(), happiness.end());
    for (int i = static_cast<int>(happiness.size() - 1), j = 0; j < k; ++j, --i)
        result += std::max(happiness[i] - j, 0);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> happiness, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumHappinessSum(happiness, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 2, 4, trials);
    test({1, 1, 1, 1}, 2, 1, trials);
    test({1, 1, 1, 1}, 3, 1, trials);
    test({2, 3, 4, 5}, 1, 5, trials);
    return 0;
}


