#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> numsSameConsecDiff(int n, int k)
{
    std::vector<int> result;
    auto process = [&](auto &&self, int value, int pos) -> void
    {
        if (pos == n)
            result.push_back(value);
        else
        {
            int last = value % 10;
            for (int i = 0; i <= 9; ++i)
                if (std::abs(last - i) == k)
                    self(self, value * 10 + i, pos + 1);
        }
    };
    for (int i = 1; i <= 9; ++i)
        process(process, i, 1);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(std::vector<int> left, std::vector<int> right)
{
    if (left.size() != right.size()) return false;
    std::sort(left.begin(), left.end());
    std::sort(right.begin(), right.end());
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

void test(int n, int k, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = numsSameConsecDiff(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 7, {181, 292, 707, 818, 929}, trials);
    test(2, 1, {10, 12, 21, 23, 32, 34, 43, 45, 54, 56, 65, 67, 76, 78, 87, 89,
         98}, trials);
    return 0;
}


