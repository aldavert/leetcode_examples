#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string reformatNumber(std::string number)
{
    size_t number_of_digits = 0, i, count;
    for (char chr : number)
        number_of_digits += ((chr >= '0') && (chr <= '9'));
    size_t last_digits = number_of_digits % 3;
    if      (last_digits == 1) last_digits = 4;
    else if (last_digits == 0) last_digits = 3;
    number_of_digits -= last_digits;
    std::string result;
    for (i = 0, count = 0; count < number_of_digits; ++i)
    {
        if ((number[i] >= '0') && (number[i] <= '9'))
        {
            result += number[i];
            ++count;
            if (count % 3 == 0) result += '-';
        }
    }
    count = 0;
    if (last_digits == 4)
    {
        for (; count < 2; ++i)
        {
            if ((number[i] >= '0') && (number[i] <= '9'))
            {
                result += number[i];
                ++count;
            }
        }
        result += '-';
    }
    for (; count < last_digits; ++i)
    {
        if ((number[i] >= '0') && (number[i] <= '9'))
        {
            result += number[i];
            ++count;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string number, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = reformatNumber(number);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1-23-45 6", "123-456", trials);
    test("123 4-567", "123-45-67", trials);
    test("123 4-5678", "123-456-78", trials);
    return 0;
}


