#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int snakesAndLadders(std::vector<std::vector<int> > board)
{
    const int n = static_cast<int>(board.size());
    const int threshold = n * n - 1;
    auto lookup = [&](int position) -> int
    {
        const int y = position / n;
        const int x = (n - 1) * (y & 1) + (1 - 2 * (y & 1)) * position % n;
        return board[(n - 1) - y][x] - 1;
    };
    std::vector<bool> active(n * n, true);
    std::queue<int> search;
    search.push(0);
    for (int iteration = 0; !search.empty(); ++iteration)
    {
        for (int elements = static_cast<int>(search.size()); elements > 0; --elements)
        {
            int index = search.front();
            search.pop();
            if (!active[index]) continue;
            active[index] = false;
            if (index == threshold) return iteration;
            for (int dice = 1; dice <= 6; ++dice)
            {
                int new_position = std::min(threshold, index + dice);
                if (int aux = lookup(new_position); aux != -2) new_position = aux;
                search.push(new_position);
            }
        }
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > board, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = snakesAndLadders(board);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{-1, -1, -1, -1, -1, -1},
          {-1, -1, -1, -1, -1, -1},
          {-1, -1, -1, -1, -1, -1},
          {-1, 35, -1, -1, 13, -1},
          {-1, -1, -1, -1, -1, -1},
          {-1, 15, -1, -1, -1, -1}}, 4, trials);
    test({{-1, -1},
          {-1,  3}}, 1, trials);
    return 0;
}


