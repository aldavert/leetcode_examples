#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countTestedDevices(std::vector<int> batteryPercentages)
{
    int result = 0;
    for (int value : batteryPercentages)
        result += (value - result > 0);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> batteryPercentages,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countTestedDevices(batteryPercentages);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2, 1, 3}, 3, trials);
    test({0, 1, 2}, 2, trials);
    return 0;
}


