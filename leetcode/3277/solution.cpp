#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int>
maximumSubarrayXor(std::vector<int> nums, std::vector<std::vector<int> > queries)
{
    const int n = static_cast<int>(nums.size());
    std::vector<std::vector<int> > xors(n, std::vector<int>(n)),
                                   dp(n, std::vector<int>(n));
    std::vector<int> result;
    
    for (int i = 0; i < n; ++i)
    {
        xors[i][i] = nums[i];
        dp[i][i] = nums[i];
    }
    for (int d = 1; d < n; ++d)
    {
        for (int i = 0; i + d < n; ++i)
        {
            const int j = i + d;
            xors[i][j] = xors[i][j - 1] ^ xors[i + 1][j];
            dp[i][j] = std::max({xors[i][j], dp[i][j - 1], dp[i + 1][j]});
        }
    }
    for (const auto &query : queries)
        result.push_back(dp[query[0]][query[1]]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumSubarrayXor(nums, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 8, 4, 32, 16, 1}, {{0, 2}, {1, 4}, {0, 5}}, {12, 60, 60}, trials);
    test({0, 7, 3, 2, 8, 5, 1}, {{0, 3}, {1, 5}, {2, 4}, {2, 6}, {5, 6}},
         {7, 14, 11, 14, 5}, trials);
    return 0;
}


