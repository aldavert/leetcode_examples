#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int countOfSubstrings(std::string word, int k)
{
    const int n = static_cast<int>(word.size());
    auto isVowel = [](char c) -> bool
    {
        static constexpr std::string_view VOWELS = "aeiou";
        return VOWELS.find(c) != std::string_view::npos;
    };
    auto substringsWithAtMost = [&](int p) -> long
    {
        if (p == -1) return 0;
        long result = 0;
        int vowels = 0, unique_vowels = 0;
        std::unordered_map<char, int> vowel_last_seen;
        for (int l = 0, r = 0; r < n; ++r)
        {
            if (isVowel(word[r]))
            {
                ++vowels;
                auto it = vowel_last_seen.find(word[r]);
                unique_vowels += (it == vowel_last_seen.end() || (it->second < l));
                vowel_last_seen[word[r]] = r;
            }
            while (r - l + 1 - vowels > p)
            {
                if (isVowel(word[l]))
                {
                    --vowels;
                    if (vowel_last_seen[word[l]] == l)
                        --unique_vowels;
                }
                ++l;
            }
            if (unique_vowels == 5)
                result += std::min({vowel_last_seen['a'],
                                    vowel_last_seen['e'],
                                    vowel_last_seen['i'],
                                    vowel_last_seen['o'],
                                    vowel_last_seen['u']}) - l + 1;
        }
        return result;
    };
    return static_cast<int>(substringsWithAtMost(k) - substringsWithAtMost(k - 1));
}

// ############################################################################
// ############################################################################

void test(std::string word, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int t = 0; t < trials; ++t)
        result = countOfSubstrings(word, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aeioqq", 1, 0, trials);
    test("aeiou", 0, 1, trials);
    test("ieaouqqieaouqq", 1, 3, trials);
    return 0;
}


