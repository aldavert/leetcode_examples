#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> getGoodIndices(std::vector<std::vector<int> > variables, int target)
{
    auto modPow = [&](auto &&self, long x, long n, int mod) -> long
    {
        if (n == 0) return 1;
        if (n % 2 == 1) return x * self(self, x % mod, (n - 1), mod) % mod;
        return self(self, x * x % mod, (n / 2), mod) % mod;
    };
    std::vector<int> result;
    for (int i = 0, m = static_cast<int>(variables.size()); i < m; ++i)
        if (modPow(modPow, modPow(modPow, variables[i][0], variables[i][1], 10),
                   variables[i][2], variables[i][3]) == target)
            result.push_back(i);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > variables,
          int target,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getGoodIndices(variables, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 3, 3, 10}, {3, 3, 3, 1}, {6, 1, 1, 4}}, 2, {0, 2}, trials);
    test({{39, 3, 1000, 1000}}, 17, {}, trials);
    return 0;
}


