#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isPossible(int n, std::vector<std::vector<int> > edges)
{
    std::vector<std::unordered_set<int> > graph(n);
    for (auto &edge : edges)
    {
        graph[edge[0] - 1].insert(edge[1] - 1);
        graph[edge[1] - 1].insert(edge[0] - 1);
    }
    
    std::vector<int> odd_nodes;
    for (int i = 0, m = static_cast<int>(graph.size()); i < m; ++i)
        if (graph[i].size() % 2)
            odd_nodes.push_back(i);
    if (odd_nodes.empty()) return true;
    if (odd_nodes.size() == 2)
        for (int i = 0; i < n; ++i)
            if ((graph[i].find(odd_nodes[0]) == graph[i].end())
            &&  (graph[i].find(odd_nodes[1]) == graph[i].end()))
                return true;
    if (odd_nodes.size() == 4)
        return (graph[odd_nodes[0]].find(odd_nodes[1]) == graph[odd_nodes[0]].end()
             && graph[odd_nodes[2]].find(odd_nodes[3]) == graph[odd_nodes[2]].end())
            || (graph[odd_nodes[0]].find(odd_nodes[2]) == graph[odd_nodes[0]].end()
             && graph[odd_nodes[1]].find(odd_nodes[3]) == graph[odd_nodes[1]].end())
            || (graph[odd_nodes[0]].find(odd_nodes[3]) == graph[odd_nodes[0]].end()
             && graph[odd_nodes[1]].find(odd_nodes[2]) == graph[odd_nodes[1]].end());
    return false;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPossible(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{1, 2}, {2, 3}, {3, 4}, {4, 2}, {1, 4}, {2, 5}}, true, trials);
    test(4, {{1, 2}, {3, 4}}, true, trials);
    test(4, {{1, 2}, {1, 3}, {1, 4}}, false, trials);
    return 0;
}


