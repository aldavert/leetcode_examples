# Add Edges to Make Degrees on All Nodes Even

There is an **undirected** graph consisting of `n` nodes numbered from `1` to `n`. You are given the integer `n` and a **2D** array `edges` where `edges[i] = [a_i, b_i]` indicates that there is an edge between nodes `a_i` and `b_i`. The graph can be disconnected.

You can add **at most** two additional edges (possibly none) to this graph so that there are no repeated edges and no self-loops.

Return `true` *if it is possible to make the degree of each node in the graph even, otherwise return* `false`.

The degree of a node is the number of edges connected to it.

#### Example 1:
> ```mermaid
> flowchart LR;
> subgraph S1 [ ]
> A1((1))---B1((4))
> B1---C1((3))
> B1---D1((2))
> A1---D1
> C1---D1
> D1---E1((5))
> end
> subgraph S2 [ ]
> A2((1))---B2((4))
> B2---C2((3))
> B2---D2((2))
> A2---D2
> C2---D2
> D2---E2((5))
> B2---E2
> end
> S1-->S2
> linkStyle 12 stroke:red
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class S1,S2 empty;
> ```
> *Input:* `n = 5, edges = [[1, 2], [2, 3], [3, 4], [4, 2], [1, 4], [2, 5]]`  
> *Output:* `true`  
> *Explanation:* The above diagram shows a valid way of adding an edge. Every node in the resulting graph is connected to an even number of edges.

#### Example 2:
> ```mermaid 
> flowchart LR;
> subgraph S1 [ ]
> A1((1))---B1((2))
> C1((3))---D1((4))
> end
> subgraph S2 [ ]
> A2((1))---B2((2))
> C2((3))---D2((4))
> A2---C2
> B2---D2
> end
> S1-->S2
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class S1,S2 empty;
> linkStyle 4,5 stroke:red
> ```
> *Input:* `n = 4, edges = [[1, 2], [3, 4]]`  
> *Output:* `true`  
> *Explanation:* The above diagram shows a valid way of adding two edges.

#### Example 3:
> ```mermaid 
> graph TD;
> A((1))---B((2))
> A---C((3))
> A---D((4))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `n = 4, edges = [[1, 2], [1, 3], [1, 4]]`  
> *Output:* `false`  
> *Explanation:* It is not possible to obtain a valid graph with adding at most `2` edges.

#### Constraints:
- `3 <= n <= 10^5`
- `2 <= edges.length <= 10^5`
- `edges[i].length == 2`
- `1 <= a_i, b_i <= n`
- `a_i != b_i`
- There are no repeated edges.


