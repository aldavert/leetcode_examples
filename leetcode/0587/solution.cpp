#include "../common/common.hpp"
#include <stack>
#include <set>
#include <cmath>

// ############################################################################
// ############################################################################

#if 1
std::vector<std::vector<int> > outerTrees(std::vector<std::vector<int> > trees)
{
    struct Point
    {
        int x = 0;
        int y = 0;
        bool operator<(const Point &other) const
        {
            return (x < other.x)
               || ((x == other.x) && (y < other.y));
        }
    };
    auto ccw = [](const Point &a, const Point &b, const Point &c)
    {
        return (b.x - a.x) * (c.y - b.y) - (c.x - b.x) * (b.y - a.y);
    };
    const int n = static_cast<int>(trees.size());
    std::vector<Point> points(n), up, down;
    up.reserve(n);
    down.reserve(n);
    for (int i = 0; i < n; ++i)
        points[i] = { trees[i][0], trees[i][1] };
    std::sort(points.begin(), points.end());
    for (const auto &point : points)
    {
        while ((up.size() >= 2)
           &&  (ccw(up[up.size() - 2], up.back(), point) > 0))
            up.pop_back();
        while ((down.size() >= 2)
           &&  (ccw(down[down.size() - 2], down.back(), point) < 0))
            down.pop_back();
        up.push_back(point);
        down.push_back(point);
    }
    std::set<Point> unique(up.begin(), up.end());
    unique.insert(down.begin(), down.end());
    std::vector<std::vector<int> > result;
    for (const auto &p : unique)
        result.push_back({p.x, p.y});
    return result;
}
#else
std::vector<std::vector<int> > outerTrees(std::vector<std::vector<int> > trees)
{
    // Graham scan algorithm for convex-hull generation.
    struct Point
    {
        Point(int _x, int _y, Point P0) :
            x(_x),
            y(_y),
            angle(std::atan2(static_cast<float>(_y - P0.y),
                             static_cast<float>(_x - P0.x))) {}
        Point(int _x, int _y) : x(_x), y(_y) {}
        Point(void) = default;
        int x = 0;
        int y = 0;
        float angle = 0;
        bool operator<(const Point &other) { return angle < other.angle; }
    };
    const int n = static_cast<int>(trees.size());
    std::stack<Point> s;
    std::vector<Point> points(n);
    auto ccw = [](const Point &a, const Point &b, Point &c)
    {
        return (b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y);
    };
    
    // Find the lowest y-coordinate and leftmost point, called P0.
    Point P0 = { std::numeric_limits<int>::max(), std::numeric_limits<int>::max() };
    for (int i = 0; i < n; ++i)
        if ((trees[i][1] < P0.y) || ((trees[i][1] == P0.y) && (trees[i][0] < P0.x)))
            P0 = { trees[i][0], trees[i][1] };
    std::cout << "P0=" << P0.x << ' ' << P0.y << '\n';
    // Sort points by polar angle with P0, if several points have the same polar angle then only
    // keep the farthest.
    for (int i = 0; i < n; ++i)
        points[i] = Point(trees[i][0], trees[i][1], P0);
    std::sort(points.begin(), points.end());
    // Check which points are on the same angle
    float current_angle = std::numeric_limits<float>::max();
    for (int i = 0, begin = 0, end = 0; i < n; ++i)
    {
        std::cout << points[i].x << ' ' << points[i].y << ' ' << points[i].angle << ' ' << std::abs(current_angle - points[i].angle) << '\n';
        if (std::abs(current_angle - points[i].angle) < 1e-4) ++end;
        else
        {
            if (begin < end)
            {
                std::cout << "** " << begin << ' ' << end << '\n';
            }
            current_angle = points[i].angle;
            begin = end = i;
            std::cout << "==> " << current_angle << ' ' << begin << ' ' << end << '\n';
        }
    }
    
    for (auto &point : points)
    {
        while (s.size() > 1)
        {
            // Pop the last point from the stack if we turn clockwise to reach this point.
            // The problem states that we have to return all points that touch the rope, so
            // we have to also return co-linear points, i.e points where ccw is 0.
            Point top = s.top();
            s.pop();
            if (ccw(s.top(), top, point) >= 0)
            {
                s.push(top);
                break;
            }
        }
        std::cout << point.x << ' ' << point.y << ' ' << s.size() << '\n';
        s.push(point);
    }
    if (s.size() > 2)
    {
        Point top = s.top();
        s.pop();
        if (ccw(s.top(), top, P0) >= 0)
            s.push(top);
    }
    std::vector<std::vector<int> > result;
    result.reserve(s.size());
    while (!s.empty())
    {
        result.push_back({s.top().x, s.top().y});
        s.pop();
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    std::set<std::pair<int, int> > unique_left;
    for (const auto &point : left)
    {
        if (point.size() != 2) return false;
        unique_left.insert({point[0], point[1]});
    }
    if (left.size() != right.size()) return false;
    std::vector<std::pair<int, int> > wrong;
    for (const auto &point : right)
    {
        if (point.size() != 2) return false;
        if (auto it = unique_left.find({point[0], point[1]}); it != unique_left.end())
            unique_left.erase(it);
        else wrong.push_back({point[0], point[1]});
    }
    if (wrong.size() > 0)
    {
        std::cout << "[RIGHT] COORDINATES NOT FOUND";
        for (auto &v : wrong) std::cout << ' ' << v.first << ':' << v.second;
        std::cout << '\n';
        std::cout << "[LEFT] REMAINING";
        for (auto &v : unique_left) std::cout << ' ' << v.first << ':' << v.second;
        std::cout << '\n';
        return false;
    }
    return true;
}

void test(std::vector<std::vector<int> > trees,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = outerTrees(trees);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1}, {2, 2}, {2, 0}, {2, 4}, {3, 3}, {4, 2}},
         {{1, 1}, {2, 0}, {3, 3}, {2, 4}, {4, 2}}, trials);
    test({{1, 2}, {2, 2}, {4, 2}}, {{4, 2}, {2, 2}, {1, 2}}, trials);
    test({{3, 0}, {4, 0}, {5, 0}, {6, 1}, {7, 2},
          {7, 3}, {7, 4}, {6, 5}, {5, 5}, {4, 5},
          {3, 5}, {2, 5}, {1, 4}, {1, 3}, {1, 2},
          {2, 1}, {4, 2}, {0, 3}},
         {{7, 4}, {5, 0}, {7, 3}, {2, 1}, {5, 5},
          {4, 5}, {3, 5}, {7, 2}, {1, 2}, {1, 4},
          {4, 0}, {2, 5}, {6, 1}, {6, 5}, {0, 3},
          {3, 0}}, trials);
    test({{0, 0}, {0, 1}, {0, 2}, {1, 2}, {2, 2},
          {3, 2}, {3, 1}, {3, 0}, {2, 0}},
         {{2, 0}, {2, 2}, {0, 1}, {1, 2}, {3, 1},
          {0, 0}, {3, 0}, {3, 2}, {0, 2}}, trials);
    test({{0, 2}, {0, 4}, {0, 5}, {0, 9}, {2, 1},
          {2, 2}, {2, 3}, {2, 5}, {3, 1}, {3, 2},
          {3, 6}, {3, 9}, {4, 2}, {4, 5}, {5, 8},
          {5, 9}, {6, 3}, {7, 9}, {8, 1}, {8, 2},
          {8, 5}, {8, 7}, {9, 0}, {9, 1}, {9, 6}},
         {{2, 1}, {7, 9}, {0, 9}, {0, 2}, {9, 0},
          {3, 9}, {0, 4}, {0, 5}, {5, 9}, {9, 6},
          {9, 1}}, trials);
    return 0;
}


