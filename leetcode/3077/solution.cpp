#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
long long maximumStrength(std::vector<int> nums, int k)
{
    std::vector<int> work = nums;
    work.insert(work.begin(), 0);
    const int n = static_cast<int>(nums.size());
    std::vector<long> dp(k + 1, static_cast<long>(-1e15));
    dp[0] = 0;
    long result = -1e18;
    for (int i = n; i >= 1; --i)
    {
        for (int j = std::min(k, n - i + 1); j >= 1; --j)
        {
            long sign = 2 * (j & 1) - 1;
            dp[j] = std::max({dp[j], dp[j - 1], dp[j - 1]})
                  + sign * work[i] * j;
            if (j == k)
                result = std::max(result, dp[j]);
        }
    }
    return result;
}
#elif 0
long long maximumStrength(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    constexpr long MIN = -0x3f3f3f3f3f3f3f3f;
    std::vector<std::vector<long> > mem[2] = {
        std::vector<std::vector<long> >(n + 1, std::vector<long>(k + 1, MIN)),
        std::vector<std::vector<long> >(n + 1, std::vector<long>(k + 1, MIN)) };
    mem[0][0][0] = 0;
    for (int i = 1; i <= n; ++i)
    {
        int current = nums[i - 1];
        for (int j = 0; j <= k; ++j)
        {
            long sign = 2 * (j & 1) - 1;
            long val = sign * current * (k - j + 1);
            mem[0][i][j] = std::max(mem[0][i - 1][j], mem[1][i - 1][j]);
            mem[1][i][j] = std::max(mem[1][i][j], mem[1][i - 1][j] + val);
            if (j > 0)
            {
                long t = std::max(mem[0][i - 1][j - 1], mem[1][i - 1][j - 1]) + val;
                mem[1][i][j] = std::max(mem[1][i][j], t);
            }
        }
    }
    return std::max(mem[0][n][k], mem[1][n][k]);
}
#else
long long maximumStrength(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    constexpr long MIN = std::numeric_limits<long>::lowest() / 2;
    std::vector<std::vector<std::vector<long> > > mem(n,
            std::vector<std::vector<long> >(k + 1, std::vector<long>(2, -1)));
    auto process = [&] (auto &&self, int i, int ck, bool fresh) -> long
    {
        if (n - i < ck) return MIN;
        if (ck == 0) return 0;
        if (i == n) return (ck == 0)?0:MIN;
        if (mem[i][ck][fresh] != -1) return mem[i][ck][fresh];
        long skip = (fresh)?self(self, i + 1, ck, true):MIN,
             gain = (1 - 2 * (ck % 2 == 0)) * static_cast<long>(nums[i]) * ck;
        return mem[i][ck][fresh] = std::max(skip, gain +
                std::max(self(self, i + 1, ck, false), self(self, i + 1, ck - 1, true)));
    };
    return process(process, 0, k, true);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumStrength(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, -1, 2}, 3, 22, trials);
    test({12, -2, -2, -2, -2}, 5, 64, trials);
    test({-1, -2, -3}, 1, -1, trials);
    return 0;
}


