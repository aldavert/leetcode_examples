#include "../common/common.hpp"
#include <deque>

// ############################################################################
// ############################################################################

#if 1
long long maxWeight(std::vector<int> pizzas)
{
    std::sort(pizzas.begin(), pizzas.end());
    const int n = static_cast<int>(pizzas.size()), m = n / 4, odd = (m + 1) / 2;
    long long result = 0;
    for (int i = m * 2 + odd; i <= n - odd; i += 2) result += pizzas[i];
    for (int i = n - odd + 1; i < n; ++i) result += pizzas[i];
    return result;
}
#else
long long maxWeight(std::vector<int> pizzas)
{
    std::sort(pizzas.rbegin(), pizzas.rend());
    std::deque<int> q;
    for (size_t i = 0, n = pizzas.size() / 2; i < n; ++i)
        q.push_back(pizzas[i]);
    const size_t n = q.size() / 2;
    long long score = 0;
    for (size_t i = 0, m = (n + 1) / 2; i < m; ++i)
    {
        score += q.front();
        q.pop_front();
        q.pop_back();
    }
    for (size_t i = 0, m = n / 2; i < m; ++i)
    {
        score += q.back();
        q.pop_back();
        q.pop_back();
    }
    return score;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> pizzas, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxWeight(pizzas);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6, 7, 8}, 14, trials);
    test({2, 1, 1, 1, 1, 1, 1, 1}, 3, trials);
    test({2, 4, 4, 1, 1, 4, 5, 4, 1, 5, 3, 1, 5, 4, 5, 2}, 19, trials);
    return 0;
}


