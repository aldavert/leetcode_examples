#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int tribonacci(int n)
{
    if (n <= 0) return 0;
    if (n <= 2) return 1;
    int t0 = 0, t1 = 1, t2 = 1, result = 2;
    for (int i = 3; i < n; ++i)
    {
        t0 = t1;
        t1 = t2;
        t2 = result;
        result = t0 + t1 + t2;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = tribonacci(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 2, trials);
    test(4, 4, trials);
    test(25, 1389537, trials);
    return 0;
}


