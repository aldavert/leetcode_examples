#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int furthestBuilding(std::vector<int> heights, int bricks, int ladders)
{
    int n = static_cast<int>(heights.size());
    std::priority_queue<int, std::vector<int>, std::greater<int> > queue;
    for (int i = 1; i < n; ++i)
    {
        int diff = heights[i] - heights[i - 1];
        if (diff > 0)
        {
            queue.push(diff);
            if (static_cast<int>(queue.size()) > ladders)
            {
                bricks -= queue.top();
                queue.pop();
            }
            if (bricks < 0)
                return i - 1;
        }
    }
    return n - 1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> heights,
          int bricks,
          int ladders,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = furthestBuilding(heights, bricks, ladders);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 7, 6, 9, 14, 12}, 5, 1, 4, trials);
    test({4, 12, 2, 7, 3, 18, 20, 3, 19}, 10, 2, 7, trials);
    test({14, 3, 19, 3}, 17, 0, 3, trials);
    return 0;
}


