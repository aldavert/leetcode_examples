#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

double maxAverageRatio(std::vector<std::vector<int> > classes, int extraStudents)
{
    std::priority_queue<std::tuple<double, int, int>> heap;
    auto extraPassRatio = [](int pass, int total) -> double
    {
        return static_cast<double>(pass + 1) / static_cast<double>(total + 1)
             - static_cast<double>(pass) / static_cast<double>(total);
    };
    
    for (const auto &c : classes)
        heap.emplace(extraPassRatio(c[0], c[1]), c[0], c[1]);
    for (int i = 0; i < extraStudents; ++i)
    {
        const auto [_, pass, total] = heap.top();
        heap.pop();
        heap.emplace(extraPassRatio(pass + 1, total + 1), pass + 1, total + 1);
    }
    double ratio_sum = 0;
    while (!heap.empty())
    {
        const auto [_, pass, total] = heap.top();
        heap.pop();
        ratio_sum += pass / static_cast<double>(total);
    }
    return ratio_sum / static_cast<double>(classes.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > classes,
          int extraStudents,
          double solution,
          unsigned int trials = 1)
{
    double result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxAverageRatio(classes, extraStudents);
    showResult(std::abs(solution - result) < 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {3, 5}, {2, 2}}, 2, 0.78333, trials);
    test({{2, 4}, {3, 9}, {4, 5}, {2, 10}}, 4, 0.53485, trials);
    return 0;
}


