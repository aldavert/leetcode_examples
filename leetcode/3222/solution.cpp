#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string losingPlayer(int x, int y)
{
    return (std::min(x, y / 4) % 2 == 0)?"Bob":"Alice";
}

// ############################################################################
// ############################################################################

void test(int x, int y, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = losingPlayer(x, y);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 7, "Alice", trials);
    test(4, 11, "Bob", trials);
    return 0;
}


