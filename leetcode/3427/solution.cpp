#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int subarraySum(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> sum(nums.size() + 1);
    for (int i = 0; i < n; ++i)
        sum[i + 1] = sum[i] + nums[i];
    int result = 0;
    for (int i = 0; i < n; ++i)
        result += sum[i + 1] - sum[std::max(i - nums[i], 0)];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = subarraySum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 1}, 11, trials);
    test({3, 1, 1, 2}, 13, trials);
    return 0;
}


