#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int areaOfMaxDiagonal(std::vector<std::vector<int> > dimensions)
{
    int result = 0, diagonal = 0;
    for (const auto &rect : dimensions)
    {
        int d = rect[0] * rect[0] + rect[1] * rect[1];
        if (d > diagonal)
            result = rect[0] * rect[1],
            diagonal = d;
        else if (d == diagonal) result = std::max(result, rect[0] * rect[1]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > dimensions,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = areaOfMaxDiagonal(dimensions);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{9, 3}, {8, 6}}, 48, trials);
    test({{3, 4}, {4, 3}}, 12, trials);
    //     12:40  5:26    30:109   32:80
    test({{2, 6}, {5, 1}, {3, 10}, {8, 4}}, 30, trials);
    return 0;
}


