#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxBottlesDrunk(int numBottles, int numExchange)
{
    int result = numBottles;
    for (; numBottles >= numExchange; ++numExchange, ++result)
        numBottles = (numBottles - numExchange + 1);
    return result;
}

// ############################################################################
// ############################################################################

void test(int numBottles, int numExchanges, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxBottlesDrunk(numBottles, numExchanges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(13, 6, 15, trials);
    test(10, 3, 13, trials);
    return 0;
}


