#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int getMaximumGold(std::vector<std::vector<int> > grid)
{
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>(grid[0].size());
    int total_gold = 0, result = 0;
    for (int row = 0; row < n_rows; ++row)
        for (int col = 0; col < n_cols; ++col)
            total_gold += grid[row][col];
    auto countGold = [&](int row, int col) -> int
    {
        auto inner = [&](auto &&self, int r, int c) -> int
        {
            if (result == total_gold) return result;
            if ((r < 0) || (c < 0) || (r >= n_rows) || (c >= n_cols))
                return 0;
            if (grid[r][c] == 0) return 0;
            int gold = grid[r][c];
            grid[r][c] = 0;
            int max_gold = std::max({self(self, r + 1, c),
                                     self(self, r - 1, c),
                                     self(self, r, c + 1),
                                     self(self, r, c - 1)});
            grid[r][c] = gold;
            return max_gold + gold;
        };
        return inner(inner, row, col);
    };
    for (int row = 0; row < n_rows; ++row)
        for (int col = 0; col < n_cols; ++col)
            result = std::max(result, countGold(row, col));
    return result;
}
#else
int getMaximumGold(std::vector<std::vector<int> > grid)
{
    const int n_rows = static_cast<int>(grid.size()),
             n_cols = static_cast<int>(grid[0].size());
    auto countGold = [&](int row, int col) -> int
    {
        auto inner = [&](auto &&self, int r, int c) -> int
        {
            if ((r < 0) || (c < 0) || (r >= n_rows) || (c >= n_cols))
                return 0;
            if (grid[r][c] == 0) return 0;
            int gold = grid[r][c];
            grid[r][c] = 0;
            int max_gold = std::max({self(self, r + 1, c),
                                     self(self, r - 1, c),
                                     self(self, r, c + 1),
                                     self(self, r, c - 1)});
            grid[r][c] = gold;
            return max_gold + gold;
        };
        return inner(inner, row, col);
    };
    auto checkFullMap = [&]() -> int
    {
        int count = 0;
        for (int row = 0; row < n_rows; ++row)
        {
            for (int col = 0; col < n_cols; ++col)
            {
                if (grid[row][col] == 0) return 0;
                count += grid[row][col];
            }
        }
        return count;
    };
    int result = checkFullMap();
    if (result != 0) return result;
    for (int row = 0; row < n_rows; ++row)
        for (int col = 0; col < n_cols; ++col)
            result = std::max(result, countGold(row, col));
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getMaximumGold(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 6, 0}, {5, 8, 7}, {0, 9, 0}}, 24, trials);
    test({{1, 0, 7}, {2, 0, 6}, {3, 4, 5}, {0, 3, 0}, {9, 0, 20}}, 28, trials);
    return 0;
}


