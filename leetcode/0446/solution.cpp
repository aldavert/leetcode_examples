#include "../common/common.hpp"
#include <unordered_map>
#include <limits>

// ############################################################################
// ############################################################################

#if 1
int numberOfArithmeticSlices(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = 0;
    std::unordered_map<int, std::vector<int> > histogram;
    int dp[1001][1001] = {};
    for (int i = 0; i < n; ++i) histogram[nums[i]].push_back(i);
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < i; j++) 
        {
            long next = 2l * nums[j] - nums[i];
            if (next < std::numeric_limits<int>::lowest()
             || next > std::numeric_limits<int>::max())
                continue;
            if (auto it = histogram.find(static_cast<int>(next));
                it != end(histogram)) 
            {
                for (int k : it->second) 
                {
                    if (k >= j) break;
                    dp[i][j] += dp[j][k] + 1;
                }
            }
            result += dp[i][j];
        }
    }
    return result;
}
#else
int numberOfArithmeticSlices(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::unordered_map<long, int> dp[1001];
    int result = 0;
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < i; ++j)
        {
            long diff = static_cast<long>(nums[i]) - static_cast<long>(nums[j]);
            ++dp[i][diff];
            if (dp[j].find(diff) != dp[j].end())
            {
                dp[i][diff] += dp[j][diff];
                result += dp[j][diff];
            }
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfArithmeticSlices(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4, 6, 8, 10}, 7, trials);
    test({7, 7, 7, 7, 7}, 16, trials);
    return 0;
}


