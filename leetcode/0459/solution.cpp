#include "../common/common.hpp"
#include <set>
#include <cmath>

// ############################################################################
// ############################################################################

#if 1
bool repeatedSubstringPattern(std::string s)
{
    const int n = static_cast<int>(s.size());
    auto checkValid = [&](int length) -> bool
    {
        return s.substr(0, n - length) == s.substr(length);
    };
    if (int first_frequency = static_cast<int>(std::count(s.begin(), s.end(), s[0]));
        (first_frequency > 1) && (first_frequency == n)) return true;
    for (int i = 2, e = static_cast<int>(std::sqrt(n)); i <= e; ++i)
        if (n % i == 0)
            if (checkValid(i) || checkValid(n / i)) return true;
    return false;
}
#elif 0
bool repeatedSubstringPattern(std::string s)
{
    const int n = static_cast<int>(s.size());
    auto checkValid = [&](int length) -> bool
    {
        for (int i = length; i < n; i += length)
            if (s.substr(0, length) != s.substr(i, length))
                return false;
        return true;
    };
    if (int first_frequency = static_cast<int>(std::count(s.begin(), s.end(), s[0]));
        (first_frequency > 1) && (first_frequency == n)) return true;
    for (int i = 2, e = static_cast<int>(std::sqrt(n)); i <= e; ++i)
        if (n % i == 0)
            if (checkValid(i) || checkValid(n / i)) return true;
    return false;
}
#else
bool repeatedSubstringPattern(std::string s)
{
    const size_t n = s.size();
    std::set<size_t> divisors;
    for (size_t i = 1, e = static_cast<size_t>(std::sqrt(n)); i <= e; ++i)
    {
        if (n % i == 0)
        {
            divisors.insert(i);
            divisors.insert(n / i);
        }
    }
    divisors.erase(n);
    for (size_t d : divisors)
    {
        bool repeat = true;
        for (size_t i = d; i < n; i += d)
            repeat = repeat && (s.substr(0, d) == s.substr(i, d));
        if (repeat) return true;
    }
    return false;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = repeatedSubstringPattern(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abab", true, trials);
    test("aba", false, trials);
    test("abcabcabcabc", true, trials);
    test("a", false, trials);
    return 0;
}


