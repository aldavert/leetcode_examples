#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minCost(int maxTime,
            std::vector<std::vector<int> > edges,
            std::vector<int> passingFees)
{
    const int n = static_cast<int>(passingFees.size());
    std::vector<std::vector<std::pair<int, int> > > graph(n);
    
    for (const auto &edge : edges)
    {
        const int u = edge[0], v = edge[1], w = edge[2];
        graph[u].push_back({v, w});
        graph[v].push_back({u, w});
    }
    
    struct Position
    {
        int cost = 0;
        int distance = 0;
        int node = 0;
        bool operator<(const Position &other) const
        {
            return (cost > other.cost)
                || ((cost == other.cost) && (distance > other.distance))
                || ((cost == other.cost) && (distance == other.distance)
                                         && (node > other.node));
        }
    };
    auto dijkstra = [&](int src, int dst) -> int
    {
        std::vector<int> cost(graph.size(), std::numeric_limits<int>::max()),
                         distance(graph.size(), maxTime + 1);
        std::priority_queue<Position> min_heap;
        
        cost[src] = passingFees[src];
        distance[src] = 0;
        min_heap.push({cost[src], distance[src], src});
        while (!min_heap.empty())
        {
            auto current = min_heap.top(); min_heap.pop();
            if (current.node == dst) return cost[dst];
            for (const auto& [v, w] : graph[current.node])
            {
                if (current.distance + w > maxTime) continue;
                if (current.cost + passingFees[v] < cost[v])
                {
                    cost[v] = current.cost + passingFees[v];
                    distance[v] = current.distance + w;
                    min_heap.push({cost[v], distance[v], v});
                }
                else if (current.distance + w < distance[v])
                {
                    distance[v] = current.distance + w;
                    min_heap.push({current.cost + passingFees[v], distance[v], v});
                }
            }
        }
        return -1;
    };
    
    return dijkstra(0, n - 1);
}

// ############################################################################
// ############################################################################

void test(int maxTime,
          std::vector<std::vector<int> > edges,
          std::vector<int> passingFees,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCost(maxTime, edges, passingFees);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(30, {{0, 1, 10}, {1, 2, 10}, {2, 5, 10}, {0, 3, 1}, {3, 4, 10}, {4, 5, 15}},
         {5, 1, 2, 20, 20, 3}, 11, trials);
    test(29, {{0, 1, 10}, {1, 2, 10}, {2, 5, 10}, {0, 3, 1}, {3, 4, 10}, {4, 5, 15}},
         {5, 1, 2, 20, 20, 3}, 48, trials);
    test(25, {{0, 1, 10}, {1, 2, 10}, {2, 5, 10}, {0, 3, 1}, {3, 4, 10}, {4, 5, 15}},
         {5, 1, 2, 20, 20, 3}, -1, trials);
    return 0;
}


