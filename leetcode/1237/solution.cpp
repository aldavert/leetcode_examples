#include "../common/common.hpp"

// ############################################################################
// ############################################################################

template <typename CustomFunction>
std::vector<std::vector<int> > findSolution(CustomFunction& customfunction, int z)
{
    std::vector<std::vector<int> > result;
    
    for (int x = 1, y = 1000; x <= 1000 && y >= 1;)
    {
        int f = customfunction.f(x, y);
        if      (f < z) ++x;
        else if (f > z) --y;
        else result.push_back({x++, y--});
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(auto function,
          int z,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = findSolution(function, z);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    struct Test1 { int f(int x, int y) { return x + y; } };
    test(Test1(), 5, {{1, 4}, {2, 3}, {3, 2}, {4, 1}}, trials);
    struct Test2 { int f(int x, int y) { return x * y; } };
    test(Test2(), 5, {{1, 5}, {5, 1}}, trials);
    return 0;
}


