#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
std::vector<std::vector<std::string> >
displayTable(std::vector<std::vector<std::string> > orders)
{
    std::vector<std::vector<std::string>> result{{"Table"}};
    std::unordered_map<std::string, int> table_number_to_row_index;
    std::unordered_map<std::string, int> food_item_to_col_index;
    
    for (const auto &order : orders)
    {
        table_number_to_row_index[order[1]] = 0;
        food_item_to_col_index[order[2]] = 0;
    }
    for (const auto& [table_number, _] : table_number_to_row_index)
        result.push_back({table_number});
    for (const auto& [food_item, _] : food_item_to_col_index)
        result[0].push_back(food_item);
    
    std::sort(result[0].begin() + 1, result[0].end());
    std::sort(result.begin() + 1, result.end(),
            [](const std::vector<std::string> &a,
               const std::vector<std::string> &b)
             { return std::stoi(a[0]) < std::stoi(b[0]); });
    
    for (size_t i = 0; i < table_number_to_row_index.size(); ++i)
        table_number_to_row_index[result[i + 1][0]] = static_cast<int>(i);
    for (size_t i = 0; i < food_item_to_col_index.size(); ++i)
        food_item_to_col_index[result[0][i + 1]] = static_cast<int>(i);
    
    std::vector<std::vector<int> > count;
    for (size_t i = 0; i < table_number_to_row_index.size(); ++i)
        count.push_back(std::vector<int>(food_item_to_col_index.size()));
    for (const auto &order : orders)
    {
        const int row_index = table_number_to_row_index[order[1]],
                  col_index = food_item_to_col_index[order[2]];
        ++count[row_index][col_index];
    }
    for (size_t i = 0; i < table_number_to_row_index.size(); ++i)
        for (size_t j = 0; j < food_item_to_col_index.size(); ++j)
            result[i + 1].push_back(std::to_string(count[i][j]));
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<std::string> > orders,
          std::vector<std::vector<std::string> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<std::string> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = displayTable(orders);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{"David", "3", "Ceviche"},
          {"Corina", "10", "Beef Burrito"},
          {"David", "3", "Fried Chicken"},
          {"Carla", "5", "Water"},
          {"Carla", "5", "Ceviche"},
          {"Rous", "3", "Ceviche"}},
         {{"Table", "Beef Burrito", "Ceviche", "Fried Chicken", "Water"},
          {"3", "0", "2", "1", "0"},
          {"5", "0", "1", "0", "1"},
          {"10", "1", "0", "0", "0"}}, trials);
    test({{"James", "12", "Fried Chicken"},
          {"Ratesh", "12", "Fried Chicken"},
          {"Amadeus", "12", "Fried Chicken"},
          {"Adam", "1", "Canadian Waffles"},
          {"Brianna", "1", "Canadian Waffles"}},
         {{"Table", "Canadian Waffles", "Fried Chicken"},
          {"1", "2", "0"},
          {"12", "0", "3"}}, trials);
    test({{"Laura", "2", "Bean Burrito"},
          {"Jhon", "2", "Beef Burrito"},
          {"Melissa", "2", "Soda"}},
         {{"Table", "Bean Burrito", "Beef Burrito", "Soda"},
          {"2", "1", "1", "1"}}, trials);
    return 0;
}


