#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfSteps(int num)
{
    int result = 0;
    for (; num > 0; ++result)
    {
        if ((num & 1) == 0) num /= 2;
        else --num;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int num, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfSteps(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(14, 6, trials);
    test(8, 4, trials);
    test(123, 12, trials);
    return 0;
}


