#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> singleNumber(std::vector<int> nums)
{
    std::unordered_set<int> unique;
    for (int n : nums)
    {
        if (auto it = unique.find(n); it != unique.end())
            unique.erase(it);
        else unique.insert(n);
    }
    return std::vector<int>(unique.begin(), unique.end());
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_map<int, int> lut;
    for (int v : left)
        lut[v] += 1;
    for (int v : right)
    {
        if (auto it = lut.find(v); it != lut.end())
        {
            it->second -= 1;
            if (it->second <= 0)
                lut.erase(it);
        }
        else return false;
    }
    return true;
}

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = singleNumber(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1, 3, 2, 5}, {3, 5}, trials);
    test({-1, 0}, {-1, 0}, trials);
    test({0, 1}, {1, 0}, trials);
    return 0;
}


