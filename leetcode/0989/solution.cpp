#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> addToArrayForm(std::vector<int> num, int k)
{
    std::vector<int> result(num.begin(), num.end());
    const int n = static_cast<int>(num.size());
    bool carry = false;
    for (int i = n - 1; (i >= 0) && (k || carry); k /= 10, --i)
    {
        int r = result[i] + (k % 10) + carry;
        result[i] = r - (carry = r > 9) * 10;
    }
    if (carry || k)
    {
        k += carry;
        for (; k; k /= 10)
            result.insert(result.begin(), k % 10);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> num,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = addToArrayForm(num, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 0, 0}, 34, {1, 2, 3, 4}, trials);
    test({2, 7, 4}, 181, {4, 5, 5}, trials);
    test({2, 1, 5}, 806, {1, 0, 2, 1}, trials);
    test({9, 9, 9, 9, 9, 9, 9, 9, 9, 9}, 1, {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, trials);
    test({1, 2, 6, 3, 0, 7, 1, 7, 1, 9, 7, 5, 6, 6, 4, 4, 0, 0, 6, 3}, 516,
         {1, 2, 6, 3, 0, 7, 1, 7, 1, 9, 7, 5, 6, 6, 4, 4, 0, 5, 7, 9}, trials);
    test({0}, 23, {2, 3}, trials);
    test({9}, 23, {3, 2}, trials);
    return 0;
}


