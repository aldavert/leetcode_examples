#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string removeStars(std::string s)
{
    std::string result;
    for (char letter : s)
    {
        if (letter != '*') result.push_back(letter);
        else if (result.size() > 0) result.pop_back();
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = removeStars(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leet**cod*e", "lecoe", trials);
    test("erase*****", "", trials);
    return 0;
}


