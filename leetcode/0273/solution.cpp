#include "../common/common.hpp"
#include <stack>
#include <array>

// ############################################################################
// ############################################################################

std::string numberToWords(int num)
{
    if (num == 0) return "Zero";
    std::stack<std::string> tokens[4];
    typedef std::array<std::array<char, 16>, 10> Description;
    constexpr Description units = { "", "One", "Two", "Three",
                                    "Four", "Five", "Six", "Seven",
                                    "Eight", "Nine" };
    constexpr Description tens  = { "", "Ten", "Twenty", "Thirty",
                                    "Forty", "Fifty", "Sixty",
                                    "Seventy", "Eighty", "Ninety" };
    constexpr Description teens = { "Ten", "Eleven", "Twelve", "Thirteen",
                                    "Fourteen", "Fifteen", "Sixteen",
                                    "Seventeen", "Eighteen", "Nineteen" };
    constexpr Description separator = { "", "Thousand", "Million", "Billion" };
    std::string result;
    for (int position = 0, stack_id = -1; num; ++position)
    {
        if (position % 3 == 0) ++stack_id;
        
        int value = num % 10;
        num /= 10;
        if (position % 3 == 0)
        {
            if (num % 10 == 1)
            {
                tokens[stack_id].push(teens[value].data());
                num /= 10;
                ++position;
            }
            else if (value > 0) tokens[stack_id].push(units[value].data());
        }
        else if (position % 3 == 1)
        {
            if (value > 1)
                tokens[stack_id].push(tens[value].data());
        }
        else if (value > 0)
        {
            tokens[stack_id].push("Hundred");
            tokens[stack_id].push(units[value].data());
        }
    }
    bool next = false;
    for (int stack_id = 3; stack_id >= 0; --stack_id)
    {
        if (!tokens[stack_id].empty())
        {
            for (; !tokens[stack_id].empty(); tokens[stack_id].pop())
            {
                if (next) result += " ";
                next = true;
                result += tokens[stack_id].top();
            }
            if (stack_id)
                result += " " + std::string(separator[stack_id].data());
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int num, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberToWords(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(123, "One Hundred Twenty Three", trials);
    test(12345, "Twelve Thousand Three Hundred Forty Five", trials);
    test(1234567, "One Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven", trials);
    test(1000, "One Thousand", trials);
    test(1000000, "One Million", trials);
    test(110, "One Hundred Ten", trials);
    test(10, "Ten", trials);
    return 0;
}


