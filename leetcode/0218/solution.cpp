#include "../common/common.hpp"
#include <set>

// ############################################################################
// ############################################################################

#if 1
std::vector<std::vector<int> > getSkyline(std::vector<std::vector<int> > buildings)
{
    if (buildings.empty()) return {};
    
    struct Strip
    {
        int left = 0;
        int height = 0;
        bool operator<(const Strip &other) const
        {
            return (left < other.left)
               || ((left == other.left) && (height < other.height));
        }
    };
    
    std::vector<Strip> edges;
    for (const auto &b : buildings)
    {
        edges.push_back({b[0], -b[2]});
        edges.push_back({b[1],  b[2]});
    }
    std::sort(edges.begin(), edges.end());
    
    std::vector<std::vector<int> > result;
    std::multiset<int> m;
    m.insert(0);
    for (int pre = 0, cur = 0; auto e : edges)
    {
        if (e.height < 0)  m.insert(-e.height);
        else m.erase(m.find(e.height));
        cur = *(m.rbegin());
        if (cur != pre)
            result.push_back({e.left, pre = cur});
    }
    
    return result;
}
#else
std::vector<std::vector<int> > getSkyline(std::vector<std::vector<int> > buildings)
{
    if (buildings.empty()) return {};
    struct Strip
    {
        int left = 0;
        int height = 0;
    };
    struct SkyLine
    {
        std::vector<Strip> strips;
        SkyLine(int cap)
        {
            strips.reserve(cap);
        }
        void append(const Strip &st)
        {
            if ((!strips.empty()) && (strips.back().height == st.height))
                return;
            if ((!strips.empty()) && (strips.back().left == st.left))
                strips.back().height = std::max(strips.back().height, st.height);
            else strips.push_back(st);
        }
        SkyLine * merge(SkyLine * other)
        {
            const int nc = static_cast<int>(strips.size());
            const int no = static_cast<int>(other->strips.size());
            SkyLine * res = new SkyLine(nc + no);
            int i = 0, j = 0;
            for (int h1 = 0, h2 = 0; (i < nc) && (j < no); )
            {
                int idx;
                if (strips[i].left < other->strips[j].left)
                {
                    idx = strips[i].left;
                    h1 = strips[i++].height;
                }
                else if (strips[i].left > other->strips[j].left)
                {
                    idx = other->strips[j].left;
                    h2 = other->strips[j++].height;
                }
                else
                {
                    idx = strips[i].left;
                    h1 = strips[i++].height;
                    h2 = other->strips[j++].height;
                }
                res->append({idx, std::max(h1, h2)});
            }
            for (; i < nc; ++i)
                res->append(strips[i]);
            for (; j < no; ++j)
                res->append(other->strips[j]);
            return res;
        }
    };
    auto findSkyline = [&](auto &&self, int lo, int hi) -> SkyLine *
    {
        if (lo == hi)
        {
            SkyLine * res = new SkyLine(2);
            res->append({buildings[lo][0], buildings[lo][2]});
            res->append({buildings[lo][1], 0});
            return res;
        }
        else
        {
            int mid = (lo + hi) / 2;
            
            SkyLine * sl = self(self, lo, mid);
            SkyLine * sr = self(self, mid + 1, hi);
            SkyLine * res = sl->merge(sr);
            delete sl;
            delete sr;
            
            return res;
        }
    };
    SkyLine * res = findSkyline(findSkyline, 0, static_cast<int>(buildings.size()) - 1);
    const int n = static_cast<int>(res->strips.size());
    std::vector<std::vector<int> > result(n);
    for (int i = 0; i < n; ++i)
        result[i] = { res->strips[i].left, res->strips[i].height };
    delete res;

    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > buildings,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getSkyline(buildings);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 9, 10}, {3, 7, 15}, {5, 12, 12}, {15, 20, 10}, {19, 24, 8}},
         {{2, 10}, {3, 15}, {7, 12}, {12, 0}, {15, 10}, {20, 8}, {24, 0}}, trials);
    test({{0, 2, 3}, {2, 5, 3}}, {{0, 3}, {5, 0}}, trials);
    test({{1, 2, 1}, {1, 2, 2}, {1, 2, 3}}, {{1, 3}, {2, 0}}, trials);
    test({{1, 2, 3}}, {{1, 3}, {2, 0}}, trials);
    return 0;
}


