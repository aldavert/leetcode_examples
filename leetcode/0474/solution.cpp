#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findMaxForm(std::vector<std::string> strs, int m, int n)
{
    std::vector<std::vector<int> > dp(m + 1, std::vector<int>(n + 1));
    for (const std::string &s : strs)
    {
        const int count0 = static_cast<int>(std::count(s.begin(), s.end(), '0'));
        const int count1 = static_cast<int>(s.length() - count0);
        for (int i = m; i >= count0; --i)
            for (int j = n; j >= count1; --j)
                dp[i][j] = std::max(dp[i][j], dp[i - count0][j - count1] + 1);
    }
    return dp[m][n];
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> strs,
          int m,
          int n,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMaxForm(strs, m, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"10", "0001", "111001", "1", "0"}, 5, 3, 4, trials);
    test({"10", "0", "1"}, 1, 1, 2, trials);
    return 0;
}



