#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

std::string convertDateToBinary(std::string date)
{
    auto toBinary = [](int value) -> std::string
    {
        const std::string binary = std::bitset<16>(value).to_string();
        return binary.substr(binary.find('1'));
    };
    const int year = std::stoi(date.substr(0, 4));
    const int month = std::stoi(date.substr(5, 2));
    const int day = std::stoi(date.substr(8, 2));
    return toBinary(year) + '-' + toBinary(month) + '-' + toBinary(day);
}

// ############################################################################
// ############################################################################

void test(std::string date, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = convertDateToBinary(date);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("2080-02-29", "100000100000-10-11101", trials);
    test("1900-01-01", "11101101100-1-1", trials);
    return 0;
}


