#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

#if 0
TreeNode * bstFromPreorder(std::vector<int> preorder)
{
    const int n = static_cast<int>(preorder.size());
    if (n == 0) return nullptr;
    int idx = 0;
    
    auto build = [&](auto &&self, int min, int max) -> TreeNode *
    {
        if (idx >= n) return nullptr;
        int value = preorder[idx];
        if ((value > max) || (value < min)) return nullptr;
        TreeNode * root = new TreeNode(value);
        ++idx;
        root->left = self(self, min, value - 1);
        root->right = self(self, value + 1, max);
        return root;
    };
    return build(build, std::numeric_limits<int>::lowest(),
                        std::numeric_limits<int>::max());
}
#else
TreeNode * bstFromPreorder(std::vector<int> preorder)
{
    const int n = static_cast<int>(preorder.size());
    if (n == 0) return nullptr;
    
    auto build = [&](auto &&self, int begin, int end) -> TreeNode*
    {
        if (begin >= end) return nullptr;
        int right;
        for (right = begin + 1; right < end; ++right)
            if (preorder[right] > preorder[begin])
                break;
        return new TreeNode(preorder[begin], self(self, begin + 1, right),
                                             self(self, right, end));
    };
    return build(build, 0, n);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> preorder, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = bstFromPreorder(preorder);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 5, 1, 7, 10, 12}, {8, 5, 10, 1, 7, null, 12}, trials);
    test({1, 3}, {1, null, 3}, trials);
    test({8, 4, 1, 6, 5, 7, 10, 12},
         {8, 4, 10, 1, 6, null, 12, null, null, 5, 7}, trials);
    return 0;
}


