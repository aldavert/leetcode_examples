#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long minimumPerimeter(long long neededApples)
{
    auto numApples = [](long k)
    {
        return 2 * k * (k + 1) * (2 * k + 1);
    };
    long l = 1, r = 100'000;
    while (l < r)
    {
        if (long m = (l + r) / 2; numApples(m) >= neededApples) r = m;
        else l = m + 1;
    }
    return l * 8;
}

// ############################################################################
// ############################################################################

void test(long long neededApples, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumPerimeter(neededApples);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 8, trials);
    test(13, 16, trials);
    test(1000000000, 5040, trials);
    return 0;
}


