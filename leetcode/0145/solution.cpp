#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> postorderTraversal(TreeNode * root)
{
    std::vector<int> result;
    auto traverse = [&](auto &&self, TreeNode * ptr)
    {
        if (!ptr) return;
        self(self, ptr->left);
        self(self, ptr->right);
        result.push_back(ptr->val);
    };
    traverse(traverse, root);
    return result;
}
#else
std::vector<int> postorderTraversal(TreeNode * root)
{
    std::vector<int> result;
    if (!root) return result;
    std::stack<TreeNode *> stack;
    stack.push(root);
    for (TreeNode * prev = nullptr; !stack.empty();)
    {
        TreeNode * current = stack.top();
        if (!prev || (prev->left == current) || (prev->right == current))
        {
            if      (current->left ) stack.push(current->left );
            else if (current->right) stack.push(current->right);
            else
            {
                stack.pop();
                result.push_back(current->val);
            }
        }
        else if ((current->left == prev) && current->right)
            stack.push(current->right);
        else
        {
            stack.pop();
            result.push_back(current->val);
        }
        prev = current;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = postorderTraversal(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, null, 2, 3}, {3, 2, 1}, trials);
    test({}, {}, trials);
    test({1}, {1}, trials);
    //    1
    //  2   3
    // 4 5 6 7
    //8 9
    test({1, 2, 3, 4, 5, 6, 7, 8, 9}, {8, 9, 4, 5, 2, 6, 7, 3, 1}, trials);
    return 0;
}


