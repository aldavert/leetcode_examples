#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

#if 1
std::string toHex(int num)
{
    const char hex_digits[] = "0123456789abcdef";
    std::bitset<32> bits = num;
    std::string result;
    while (bits != 0)
    {
        result = hex_digits[(bits & std::bitset<32>{15}).to_ulong()] + result;
        bits >>= 4;
    }
    return result.empty()?"0":result;
}
#else
std::string toHex(int num)
{
    const char hex_digits[] = "0123456789abcdef";
    std::string result;
    for (unsigned int work = std::bit_cast<unsigned int>(num); work > 0; work >>= 4)
        result = hex_digits[work & 15] + result;
    return result.empty()?"0":result;
}
#endif

// ############################################################################
// ############################################################################

void test(int num, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = toHex(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(26, "1a", trials);
    test(-1, "ffffffff", trials);
    test(0, "0", trials);
    return 0;
}


