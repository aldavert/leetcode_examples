#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::string findLexSmallestString(std::string s, int a, int b)
{
    int n = static_cast<int>(s.size());
    std::string result = s;
    for (int i = 0, m = (b % 2 == 1) * 9; i <= 9; ++i)
    {
        for (int j = 0; j <= m; ++j)
        {
            std::string t = s;
            for (int k = 0; k < n; k += 2)
                t[k] = static_cast<char>(((t[k] - '0') + a * j) % 10 + '0');
            for (int k = 1; k < n; k += 2)
                t[k] = static_cast<char>(((t[k] - '0') + a * i) % 10 + '0');
            for (int k = 0; k < n; ++k)
            {
                result = std::min(result, t);
                t = t.substr(n - b) + t.substr(0, n - b);
            }
        }
    }
    return result;
}
#else
std::string findLexSmallestString(std::string s, int a, int b)
{
    auto add = [&](std::string t) -> std::string
    {
        for (size_t i = 1; i < t.size(); i += 2)
            t[i] = static_cast<char>('0' + (t[i] - '0' + a) % 10);
        return t;
    };
    auto rotate = [&](std::string t) -> std::string
    {
        return t.substr(t.size() - b, t.size()) + t.substr(0, t.size() - b);
    };
    std::string result = s;
    std::unordered_set<std::string> seen;
    
    std::function<void(std::string)> dfs = [&](std::string t) -> void
    {
        if (seen.find(t) != seen.end()) return;
        seen.insert(t);
        result = std::min(result, t);
        dfs(add(t));
        dfs(rotate(t));
    };
    dfs(s);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, int a, int b, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLexSmallestString(s, a, b);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("5525", 9, 2, "2050", trials);
    test("74", 5, 1, "24", trials);
    test("0011", 4, 2, "0011", trials);
    return 0;
}


