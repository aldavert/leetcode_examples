#include "../common/common.hpp"
#include <unordered_map>
#include <set>
#include <map>

#if 1
int findLUSlength(std::vector<std::string> strs)
{
    auto isSubsequence = [](const std::string &a, const std::string &b) -> bool
    {
        const int na = static_cast<int>(a.size()),
                  nb = static_cast<int>(b.size());
        int i = 0;
        for (int j = 0; (i < na) && (j < nb); ++j)
            if (a[i] == b[j])
                ++i;
        return i == na;
    };
    std::map<int, std::unordered_map<std::string, int> > table;
    for (auto &s: strs) ++table[static_cast<int>(s.size())][s];
    for (auto it = table.rbegin(); it != table.rend(); ++it)
    {
        auto check = [&](const std::string &str_first) -> bool
        {
            for (auto itn = table.rbegin(); itn != it; ++itn)
                for (const auto &str_second: itn->second)
                    if (isSubsequence(str_first, str_second.first))
                        return false;
            return true;
        };
        for (const auto &str_first: it->second)
            if ((str_first.second == 1) && (check(str_first.first)))
                return it->first;
    }
    return -1;
}
#else
int findLUSlength(std::vector<std::string> strs)
{
    const int n = static_cast<int>(strs.size());
    std::unordered_map<std::string, std::set<int> > mp;
    for(int ind = 0; ind < n; ++ind)
    {
        std::string curr = strs[ind];
        int n2 = static_cast<int>(curr.size());
        for (int i = 0; i < (1 << n2); ++i)
        {
            std::string now = "";
            for(int j = 0;j < n2;j++)
                if (i & (1 << j))
                    now += curr[j];
            mp[now].insert(ind);
        }
    }
    int res = -1;
    for(auto it : mp)
        if (it.second.size() == 1)
            res = std::max(res, static_cast<int>(it.first.size()));              
    return res;
}
#endif

void test(std::vector<std::string> strs, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLUSlength(strs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"aba", "cdc", "eae"}, 3, trials);
    test({"aaa", "aaa", "aa"}, -1, trials);
    return 0;
}


