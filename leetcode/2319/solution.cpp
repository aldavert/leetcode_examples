#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkXMatrix(std::vector<std::vector<int> > grid)
{
    for (size_t i = 0, n = grid.size(); i < n; ++i)
    {
        for (size_t j = 0; j < n; ++j)
        {
            if ((i == j) || (n - i - 1 == j))
            {
                if (grid[i][j] == 0) return false;
            }
            else if (grid[i][j] != 0) return false;
        }
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkXMatrix(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 0, 0, 1}, {0, 3, 1, 0}, {0, 5, 2, 0}, {4, 0, 0, 2}}, true, trials);
    test({{5, 7, 0}, {0, 3, 1}, {0, 5, 0}}, false, trials);
    return 0;
}


