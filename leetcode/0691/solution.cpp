#include "../common/common.hpp"
#include <cstring>
#include <limits>
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int minStickers(std::vector<std::string> stickers, std::string target)
{
    constexpr int invalid = -1;
    const int n_stickers = static_cast<int>(stickers.size());
    const int n_target = static_cast<int>(target.size());
    const int n_layouts = (1 << n_target);
    long letter_to_stickers[26] = {};
    int dp[32768];
    
    for (int i_sticker = 0; i_sticker < n_stickers; ++i_sticker)
        for (const char c : stickers[i_sticker])
            letter_to_stickers[c - 'a'] |= (1L << i_sticker);
    std::memset(dp, invalid, sizeof(dp));
    dp[0] = 0;
    for (int layout = 0; layout < n_layouts; ++layout)
    {
        if (dp[layout] == invalid)
            continue;
        int letter = -1;
        for (int i_target = 0; i_target < n_target; ++i_target)
        {
            if (!((layout >> i_target) & 1)
            &&  (letter_to_stickers[target[i_target] - 'a'] != 0))
            {
                letter = target[i_target] - 'a';
                break;
            }
        }
        if (letter == -1) break;
        for(int i_sticker = 0; i_sticker < n_stickers; ++i_sticker)
        {
            if (((letter_to_stickers[letter] >> i_sticker) & 1) == 0)
                continue;
            int next_layout = layout;
            for (const char c_sticker : stickers[i_sticker])
            {
                for (int i_target = 0; i_target < n_target; ++i_target)
                {
                    if ((target[i_target] == c_sticker)
                    &&  (((next_layout >> i_target) & 1) == 0))
                    {
                        next_layout |= (1 << i_target);
                        break;
                    }
                }
            }
            dp[next_layout] = (dp[next_layout] == invalid)
                                ?(dp[layout] + 1)
                                :std::min(dp[next_layout], dp[layout] + 1);
        }
    }
    return dp[n_layouts - 1];
}
#else
int minStickers(std::vector<std::string> stickers, std::string target)
{
    const size_t n = stickers.size();
    struct Hypothesis
    {
        Hypothesis(void) = default;
        Hypothesis(std::string word) : missing(word), number_of_stickers(0) {}
        Hypothesis(std::string word, int ns) : missing(word), number_of_stickers(ns) {}
        std::string missing = "";
        int number_of_stickers = 0;
        std::string id(void) const
        {
            return missing + std::to_string(number_of_stickers);
        }
        int score(void) const
        {
            return static_cast<int>(missing.size()) * (number_of_stickers + 1);
        };
        bool operator<(const Hypothesis &other) const
        {
            return score() > other.score();
        }
    };
    int result = std::numeric_limits<int>::max();
    struct Word
    {
        Word(void) = default;
        Word(std::string word)
        {
            for (char letter: word)
                ++histogram[letter - 'a'];
        }
        Hypothesis reduce(const Hypothesis &current) const
        {
            int missing_histogram[26] = {};
            std::string result;
            bool modified = false;
            for (char letter : current.missing)
            {
                const int idx = static_cast<int>(letter - 'a');
                if (missing_histogram[idx] >= histogram[idx])
                    result += letter;
                else modified = true;
                ++missing_histogram[idx];
            }
            return {result, current.number_of_stickers + modified};
        }
        int histogram[26] = {};
    };
    std::vector<Word> words_available;
    std::string merged;
    for (auto sticker: stickers)
    {
        words_available.emplace_back(sticker);
        merged += sticker;
    }
    Word global_sticker(merged), global_target(target);
    for (int i = 0; i < 26; ++i)
        if ((global_target.histogram[i] > 0) && !global_sticker.histogram[i])
            return -1;
    std::priority_queue<Hypothesis> hypothesis;
    //std::stack<Hypothesis> hypothesis;
    hypothesis.emplace(target);
    std::unordered_set<std::string> visited;
    while (!hypothesis.empty())
    {
        auto current = hypothesis.top();
        hypothesis.pop();
        if (std::string id = current.id(); visited.find(id) != visited.end()) continue;
        else visited.insert(id);
        if (current.number_of_stickers >= result) continue;
        //std::cout << current.missing << ' ' << current.number_of_stickers << '\n';
        for (size_t i = 0; i < n; ++i)
        {
            auto next = words_available[i].reduce(current);
            if (next.number_of_stickers > current.number_of_stickers)
            {
                if (next.missing.empty())
                    result = std::min(result, next.number_of_stickers);
                else hypothesis.push(next);
            }
        }
    }
    return (result < static_cast<int>(target.size()))?result:-1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> stickers,
          std::string target,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minStickers(stickers, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"with", "example", "science"}, "thehat", 3, trials);
    test({"notice", "possible"}, "basicbasic", -1, trials);
    return 0;
}


