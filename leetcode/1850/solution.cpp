#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int getMinSwaps(std::string num, int k)
{
    std::string perm = num;
    while (k--) next_permutation(perm.begin(), perm.end());
    
    int count = 0;
    for (int i = 0, j = 0, n = static_cast<int>(num.size()); i < n; ++i)
    {
        j = i;
        while (num[i] != perm[j]) ++j;
        while (i < j)
        {
            std::swap(perm[j], perm[j - 1]);
            --j;
            ++count;
        }
    }
    return count;
}

// ############################################################################
// ############################################################################

void test(std::string num, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getMinSwaps(num, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("5489355142", 4, 2, trials);
    test("11112", 4, 4, trials);
    test("00123", 1, 1, trials);
    return 0;
}


