#include "../common/common.hpp"

// ############################################################################
// ############################################################################

double trimMean(std::vector<int> arr)
{
    const int begin = static_cast<int>(arr.size()) * 5 / 100;
    const int end = static_cast<int>(arr.size()) - begin;
    std::sort(arr.begin(), arr.end());
    double mean = 0.0;
    for (int i = begin; i < end; ++i)
        mean += arr[i];
    return mean / static_cast<double>(end - begin);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, double solution, unsigned int trials = 1)
{
    double result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = trimMean(arr);
    showResult(std::abs(solution - result) <= 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3}, 2, trials);
    test({6, 2, 7, 5, 1, 2, 0, 3, 10, 2, 5, 0, 5, 5, 0, 8, 7, 6, 8, 0}, 4, trials);
    test({6, 0, 7, 0, 7, 5, 7, 8, 3, 4, 0, 7, 8, 1, 6, 8, 1, 1, 2, 4, 8, 1, 9, 5,
          4, 3, 8, 5, 10, 8, 6, 6, 1, 0, 6, 10, 8, 2, 3, 4}, 4.77778, trials);
    return 0;
}


