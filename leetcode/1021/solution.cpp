#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string removeOuterParentheses(std::string s)
{
    const size_t n = s.size();
    std::string result;
    for (size_t p = 0, count = 0; p < n; ++p)
    {
        if (s[p] == '(')
        {
            if (count > 0) result += s[p];
            ++count;
        }
        else
        {
            --count;
            if (count > 0) result += s[p];
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = removeOuterParentheses(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("(()())(())", "()()()", trials);
    test("(()())(())(()(()))", "()()()()(())", trials);
    return 0;
}

