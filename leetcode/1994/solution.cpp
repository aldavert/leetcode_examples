#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int numberOfGoodSubsets(std::vector<int> nums)
{
    constexpr int MOD = 1'000'000'007;
    constexpr int nprimes = 10;
    constexpr int primes[nprimes] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29};
    constexpr int n = 1 << nprimes;
    const int max_num = *std::max_element(nums.begin(), nums.end());
    std::vector<long> dp(n);
    std::vector<int> count(max_num + 1);
    auto primesMask = [&](int num) -> int
    {
        int mask = 0;
        for (int i = 0; i < nprimes; ++i)
            if (num % primes[i] == 0)
                mask |= 1 << i;
        return mask;
    };
    auto pow = [&](auto &&self, long x, int a) -> int
    {
        if (a == 0) return 1;
        if (a & 1) return static_cast<int>(x * self(self, x, a - 1) % MOD);
        return self(self, x * x % MOD, a / 2);
    };
    
    dp[0] = 1;
    for (int num : nums) ++count[num];
    for (int num = 2; num <= max_num; ++num)
    {
        if (count[num] == 0) continue;
        if ((num % 4 == 0) || (num % 9 == 0) || (num % 25 == 0)) continue;
        int mask = primesMask(num);
        for (int primes_mask = 0; primes_mask < n; ++primes_mask)
        {
            if ((primes_mask & mask) > 0) continue;
            int next_mask = primes_mask | mask;
            dp[next_mask] = (dp[next_mask] + dp[primes_mask] * count[num]) % MOD;
        }
    }
    
    return static_cast<int>((pow(pow, 2, count[1])
         * (std::accumulate(dp.begin() + 1, dp.end(), 0L) % MOD)) % MOD);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfGoodSubsets(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 6, trials);
    test({4, 2, 3, 15}, 5, trials);
    return 0;
}


