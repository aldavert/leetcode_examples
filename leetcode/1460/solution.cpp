#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

bool canBeEqual(std::vector<int> target, std::vector<int> arr)
{
    std::unordered_map<int, int> histogram;
    for (int number : target)
        ++histogram[number];
    for (int number : arr)
    {
        if (auto search = histogram.find(number); search == histogram.end())
            return false;
        else
        {
            if (search->second == 1) histogram.erase(search);
            else --search->second;
        }
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> target,
          std::vector<int> arr,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canBeEqual(target, arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, {2, 4, 1, 3}, true, trials);
    test({7}, {7}, true, trials);
    test({3, 7, 9}, {3, 7, 11}, false, trials);
    return 0;
}


