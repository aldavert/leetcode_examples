#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string oddString(std::vector<std::string> words)
{
    const int n = static_cast<int>(words[0].size());
    std::vector<int> model_first(n - 1), work(n - 1);
    unsigned int frequency_first = 1, frequency_second = 0;
    std::string first = words[0], second;
    for (int i = 1; i < n; ++i)
        model_first[i - 1] = words[0][i] - words[0][i - 1];
    for (size_t k = 1; k < words.size(); ++k)
    {
        for (int i = 1; i < n; ++i)
            work[i - 1] = words[k][i] - words[k][i - 1];
        if (model_first == work)
            ++frequency_first;
        else
        {
            ++frequency_second;
            if (second.empty()) second = words[k];
        }
    }
    return (frequency_first == 1)?first:second;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = oddString(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"adc", "wzy", "abc"}, "abc", trials);
    test({"aaa", "bob", "ccc", "ddd"}, "bob", trials);
    return 0;
}



