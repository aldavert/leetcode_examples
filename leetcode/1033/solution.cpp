#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> numMovesStones(int a, int b, int c)
{
    if (a > b) std::swap(a, b);
    if (a > c) std::swap(a, c);
    if (b > c) std::swap(b, c);
    if (c - a == 2) return {0, 0};
    return {2 - (std::min(b - a, c - b) <= 2), c - a - 2};
}

// ############################################################################
// ############################################################################

void test(int a, int b, int c, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = numMovesStones(a, b, c);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 2, 5, {1, 2}, trials);
    test(4, 3, 2, {0, 0}, trials);
    test(3, 5, 1, {1, 2}, trials);
    return 0;
}


