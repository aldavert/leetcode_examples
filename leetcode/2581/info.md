# Count Number of Possible Root Nodes

Alice has an undirected tree with `n` nodes labeled from `0` to `n - 1`. The tree is represented as a 2D integer array edges of length `n - 1` where `edges[i] = [a_i, b_i]` indicates that there is an edge between nodes `a_i` and `b_i` in the tree.

Alice wants Bob to find the root of the tree. She allows Bob to make several **guesses** about her tree. In one guess, he does the following:
- Chooses two **distinct** integers `u` and `v` such that there exists an edge `[u, v]` in the tree.
- He tells Alice that `u` is the parent of `v` in the tree.
Bob's guesses are represented by a 2D integer array `guesses` where `guesses[j] = [u_j, v_j]` indicates Bob guessed `u_j` to be the parent of `v_j`.

Alice being lazy, does not reply to each of Bob's guesses, but just says that at least `k` of his guesses are `true`.

Given the 2D integer arrays `edges`, `guesses` and the integer `k`, return *the* ***number of possible nodes*** *that can be the root of Alice's tree*. If there is no such tree, return `0`.

#### Example 1:
> ```mermaid 
> graph TD;
> A0((0))---B0((1))---C0((2))---D0((4))
> B0---E0((3))
> A1((1))---B1((2))---C1((4))
> A1---D1((0))
> A1---E1((3))
> A2((2))---B2((4))
> B2---EMPTY1(( ))
> B2---EMPTY2(( ))
> A2---C2((1))
> C2---D2((0))
> C2---E2((3))
> A3((3))---B3((1))---C3((2))---D3((4))
> B3---E3((0))
> A4((4))---B((2))---C((1))---D((0))
> C---E((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#AAF,stroke:#228,stroke-width:2px;
> class A0,A1,A2,A3,A4 selected;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class EMPTY1,EMPTY2 empty;
> linkStyle 9,10 stroke-width:0px
> ```
> *Input:* `edges = [[0, 1], [1, 2], [1, 3], [4, 2]], guesses = [[1, 3], [0, 1], [1, 0], [2, 4]], k = 3`  
> *Output:* `3`  
> *Explanation:*
> - `Root = 0`, correct `guesses = [1, 3], [0, 1], [2, 4]`
> - `Root = 1`, correct `guesses = [1, 3], [1, 0], [2, 4]`
> - `Root = 2`, correct `guesses = [1, 3], [1, 0], [2, 4]`
> - `Root = 3`, correct `guesses = [1, 0], [2, 4]`
> - `Root = 4`, correct `guesses = [1, 3], [1, 0]`
> 
> Considering `0`, `1`, or `2` as root node leads to `3` correct guesses.

#### Example 2:
> ```mermaid 
> graph TD;
> A0((0))---B0((1))---C0((2))---D0((3))---E0((4))
> A1((1))---B1((0))
> A1---C1((2))---D1((3))---E1((4))
> A2((2))---B2((1))
> A2---C2((3))
> B2---D2((0))
> C2---E2((4))
> A3((3))---B3((4))
> A3---C3((2))---D3((1))---E3((0))
> A4((4))---B4((3))---C4((2))---D4((1))---E4((0))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#AAF,stroke:#228,stroke-width:2px;
> class A0,A1,A2,A3,A4 selected;
> ```
> *Input:* `edges = [[0, 1], [1, 2], [2, 3], [3, 4]], guesses = [[1, 0], [3, 4], [2, 1], [3, 2]], k = 1`  
> *Output:* `5`  
> *Explanation:*
> - `Root = 0`, correct `guesses = [3, 4]`
> - `Root = 1`, correct `guesses = [1, 0], [3, 4]`
> - `Root = 2`, correct `guesses = [1, 0], [2, 1], [3, 4]`
> - `Root = 3`, correct `guesses = [1, 0], [2, 1], [3, 2], [3, 4]`
> - `Root = 4`, correct `guesses = [1, 0], [2, 1], [3, 2]`
> 
> Considering any node as root will give at least `1` correct guess.

#### Constraints:
- `edges.length == n - 1`
- `2 <= n <= 10^5`
- `1 <= guesses.length <= 10^5`
- `0 <= a_i, b_i, u_j, v_j <= n - 1`
- `a_i != b_i`
- `u_j != v_j`
- `edges` represents a valid tree.
- `guesses[j]` is an edge of the tree.
- `guesses` is unique.
- `0 <= k <= guesses.length`


