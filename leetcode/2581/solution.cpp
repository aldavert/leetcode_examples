#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int rootCount(std::vector<std::vector<int> > edges,
              std::vector<std::vector<int> > guesses,
              int k)
{
    int result = 0;
    const int n = static_cast<int>(edges.size()) + 1;
    std::vector<std::vector<int> > graph(n);
    std::vector<std::unordered_set<int> > guess_graph(n);
    std::vector<int> parent(n);
    auto dfs = [&](auto &&self, int u, int previous) -> void
    {
        parent[u] = previous;
        for (int v : graph[u])
            if (v != previous)
                self(self, v, u);
    };
    auto reroot = [&](auto &&self, int u, int prev, int correct_guess) -> void
    {
        if (u != 0)
        {
            if (guess_graph[u].count(prev)) ++correct_guess;
            if (guess_graph[prev].count(u)) --correct_guess;
        }
        if (correct_guess >= k) ++result;
        for (const int v : graph[u])
            if (v != prev)
                self(self, v, u, correct_guess);
    };
    
    for (const auto &edge : edges)
    {
        graph[edge[0]].push_back(edge[1]);
        graph[edge[1]].push_back(edge[0]);
    }
    for (const auto &guess : guesses)
        guess_graph[guess[0]].insert(guess[1]);
    dfs(dfs, 0, -1);
    int correct_guess = 0;
    for (int i = 1; i < n; ++i)
        if (guess_graph[parent[i]].count(i))
            ++correct_guess;
    reroot(reroot, 0, -1, correct_guess);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges,
          std::vector<std::vector<int> > guesses,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = rootCount(edges, guesses, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {1, 2}, {1, 3}, {4, 2}},
         {{1, 3}, {0, 1}, {1, 0}, {2, 4}}, 3, 3, trials);
    test({{0, 1}, {1, 2}, {2, 3}, {3, 4}},
         {{1, 0}, {3, 4}, {2, 1}, {3, 2}}, 1, 5, trials);
    return 0;
}


