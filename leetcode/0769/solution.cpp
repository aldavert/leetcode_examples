#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxChunksToSorted(std::vector<int> arr)
{
    int result = 0;
    for (int i = 0, max_v = -1, n = static_cast<int>(arr.size()); i < n; ++i)
    {
        max_v = std::max(max_v, arr[i]);
        result += (max_v == i);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxChunksToSorted(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 3, 2, 1, 0}, 1, trials);
    test({1, 0, 2, 3, 4}, 4, trials);
    return 0;
}


