#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int maxPalindromesAfterOperations(std::vector<std::string> words)
{
    std::unordered_map<char, int> count;
    std::vector<int> sorted_lengths;
    int pairs = 0, result = 0;
    for (const auto &word : words)
    {
        for (char c : word)
            ++count[c];
        sorted_lengths.push_back(static_cast<int>(word.size()));
    }
    std::sort(sorted_lengths.begin(), sorted_lengths.end());
    for (const auto [_, freq] : count)
        pairs += freq / 2;
    for (int length : sorted_lengths)
    {
        int need_pairs = length / 2;
        if (pairs < need_pairs)
            return result;
        ++result;
        pairs -= need_pairs;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxPalindromesAfterOperations(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"abbb", "ba", "aa"}, 3, trials);
    test({"abc", "ab"}, 2, trials);
    test({"cd", "ef", "a"}, 1, trials);
    return 0;
}


