#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxScore(int n,
             int k,
             std::vector<std::vector<int> > stayScore,
             std::vector<std::vector<int> > travelScore)
{
    std::vector<std::vector<int> > f(k + 1, std::vector<int>(n, 0));
    for (int i = 1; i <= k; ++i)
        for (int j = 0; j < n; ++j)
            for (int h = 0; h < n; ++h)
                f[i][j] = std::max(f[i][j], f[i - 1][h]
                        + ((j == h)?stayScore[i - 1][j]:travelScore[h][j]));
    return *std::max_element(f[k].begin(), f[k].end());
}

// ############################################################################
// ############################################################################

void test(int n,
          int k,
          std::vector<std::vector<int> > stayScore,
          std::vector<std::vector<int> > travelScore,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxScore(n, k, stayScore, travelScore);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 1, {{2, 3}}, {{0, 2}, {1, 0}}, 3, trials);
    test(3, 2, {{3, 4, 2}, {2, 1, 2}}, {{0, 2, 1}, {2, 0, 4}, {3, 2, 0}}, 8, trials);
    return 0;
}


