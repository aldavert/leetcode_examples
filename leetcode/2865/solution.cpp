#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

long long maximumSumOfHeights(std::vector<int> heights)
{
    const int n = static_cast<int>(heights.size());
    std::vector<long> max_sum(n);
    std::stack<int> stack{{-1}};
    long summ = 0;
    auto process = [&](int i) -> void
    {
        while ((stack.size() > 1) && (heights[stack.top()] > heights[i]))
        {
            int j = stack.top();
            stack.pop();
            summ -= std::abs(j - stack.top()) * static_cast<long>(heights[j]);
        }
        summ += std::abs(i - stack.top()) * static_cast<long>(heights[i]);
        stack.push(i);
    };
    
    for (int i = 0; i < n; ++i)
    {
        process(i);
        max_sum[i] = summ;
    }
    stack = std::stack<int>{{n}};
    summ = 0;
    for (int i = n - 1; i >= 0; --i)
    {
        process(i);
        max_sum[i] += summ - heights[i];
    }
    return *std::max_element(max_sum.begin(), max_sum.end());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> heights, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumSumOfHeights(heights);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 3, 4, 1, 1}, 13, trials);
    test({6, 5, 3, 9, 2, 7}, 22, trials);
    test({3, 2, 5, 5, 2, 3}, 18, trials);
    return 0;
}


