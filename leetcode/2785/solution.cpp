#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
std::string sortVowels(std::string s)
{
    auto isVowel = [](char letter) -> bool
    {
        return (letter == 'a') || (letter == 'e') || (letter == 'i')
            || (letter == 'o') || (letter == 'u') || (letter == 'A')
            || (letter == 'E') || (letter == 'I') || (letter == 'O')
            || (letter == 'U');
    };
    const size_t n = s.size();
    size_t histogram[128] = {};
    for (size_t i = 0; i < n; ++i)
        if (isVowel(s[i]))
            ++histogram[static_cast<size_t>(s[i])];
    for (size_t i = 0, idx = static_cast<size_t>('A'); i < n; ++i)
    {
        if (isVowel(s[i]))
        {
            while (histogram[idx] == 0) ++idx;
            s[i] = static_cast<char>(idx);
            --histogram[idx];
        }
    }
    return s;
}
#elif 0
std::string sortVowels(std::string s)
{
    const std::unordered_map<char, size_t> vowels = {{'a', 5}, {'e', 6}, {'i', 7},
        {'o', 8}, {'u', 9}, {'A', 0}, {'E', 1}, {'I', 2}, {'O', 3}, {'U', 4}};
    constexpr char letter[] = "AEIOUaeiou";
    const size_t n = s.size();
    size_t histogram[10] = {};
    for (size_t i = 0; i < n; ++i)
        if (auto search = vowels.find(s[i]); search != vowels.end())
            ++histogram[search->second];
    for (size_t i = 0, idx = 0; i < n; ++i)
    {
        if (vowels.find(s[i]) != vowels.end())
        {
            while (histogram[idx] == 0) ++idx;
            s[i] = letter[idx];
            --histogram[idx];
        }
    }
    return s;
}
#else
std::string sortVowels(std::string s)
{
    const std::unordered_set<char> vowels = {'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'};
    const size_t n = s.size();
    std::string vowels_sorted;
    for (size_t i = 0; i < n; ++i)
        if ((s[i] == 'a') || (s[i] == 'e') || (s[i] == 'i') || (s[i] == 'o') || (s[i] == 'u')
        ||  (s[i] == 'A') || (s[i] == 'E') || (s[i] == 'I') || (s[i] == 'O') || (s[i] == 'U'))
            vowels_sorted.push_back(s[i]);
    if (vowels_sorted.size() > 0)
    {
        std::sort(vowels_sorted.begin(), vowels_sorted.end());
        for (size_t idx = 0, i = 0; i < n; ++i)
            if ((s[i] == 'a') || (s[i] == 'e') || (s[i] == 'i') || (s[i] == 'o') || (s[i] == 'u')
            ||  (s[i] == 'A') || (s[i] == 'E') || (s[i] == 'I') || (s[i] == 'O') || (s[i] == 'U'))
                s[i] = vowels_sorted[idx++];
    }
    return s;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sortVowels(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("lEetcOde", "lEOtcede", trials);
    test("lYmpH", "lYmpH", trials);
    return 0;
}


