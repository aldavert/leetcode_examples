#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> occurrencesOfElement(std::vector<int> nums,
                                      std::vector<int> queries,
                                      int x)
{
    std::vector<int> indices, result;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        if (nums[i] == x)
            indices.push_back(i);
    for (int n = static_cast<int>(indices.size()); int query : queries)
        result.push_back((query <= n)?indices[query - 1]:-1);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> queries,
          int x,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = occurrencesOfElement(nums, queries, x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 1, 7}, {1, 3, 2, 4}, 1, {0, -1, 2, -1}, trials);
    test({1, 2, 3}, {10}, 5, {-1}, trials);
    return 0;
}


