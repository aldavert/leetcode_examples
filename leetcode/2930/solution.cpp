#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int stringCount(int n)
{
    constexpr int MOD = 1'000'000'007;
    auto modPow = [&](auto &&self, long x, long value) -> long
    {
        if (value == 0) return 1;
        if (value % 2 == 1)
            return x * self(self, x % MOD, (value - 1)) % MOD;
        return self(self, x * x % MOD, (value / 2)) % MOD;
    };
    const long count_26_all = modPow(modPow, 26, n),
               count_25_all = modPow(modPow, 25, n),
               count_24_all = modPow(modPow, 24, n),
               count_23_all = modPow(modPow, 23, n),
               count_25_minus_one = modPow(modPow, 25, n - 1),
               count_24_minus_one = modPow(modPow, 24, n - 1),
               count_23_minus_one = modPow(modPow, 23, n - 1);
    return static_cast<int>(((count_26_all + 3 * (count_24_all - count_25_all)
                            + n * (2 * count_24_minus_one - count_25_minus_one)
                            - count_23_all - n * count_23_minus_one) % MOD + MOD) % MOD);
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = stringCount(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 12, trials);
    test(10, 83943898, trials);
    return 0;
}


