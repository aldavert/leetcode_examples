import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':
    # make data
    number_of_points = 1000
    center_x = 0.0
    center_y = 0.0
    radius = 1.0
    r = radius * np.sqrt(np.random.random((1, number_of_points)))
    theta = np.random.random((1, number_of_points)) * 2.0 * np.pi
    x = center_x + r * np.cos(theta)
    y = center_y + r * np.sin(theta)
    # plot
    fig, ax = plt.subplots()
    ax.plot(x, y, 'b.', markersize = 1.0)
    ax.grid(True, linestyle = ':')
    x_min = np.floor(center_x - radius - 1)
    x_max = np.ceil(center_x + radius + 1)
    y_min = np.floor(center_y - radius - 1)
    y_max = np.ceil(center_y + radius + 1)
    ax.set(xlim = (x_min, x_max), xticks = np.arange(x_min, x_max, 0.25),
           ylim = (y_min, y_max), yticks = np.arange(y_min, y_max, 0.25))
    ax.set_aspect('equal')
    plt.show()
