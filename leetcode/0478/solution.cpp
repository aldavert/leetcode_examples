#include "../common/common.hpp"
#include <random>

// ############################################################################
// ############################################################################

class Solution
{
    double m_radius = 1.0;
    double m_x_center = 0.0;
    double m_y_center = 0.0;
    std::random_device m_rd;
    std::mt19937 m_generator;
    std::uniform_real_distribution<> m_distribution;
public:
    Solution(double radius, double x_center, double y_center) :
        m_radius(radius),
        m_x_center(x_center),
        m_y_center(y_center),
        m_generator(m_rd()),
        m_distribution(0.0, 1.0) {}
    std::vector<double> randPoint(void)
    {
        double theta = std::numbers::pi_v<double> * 2.0 * m_distribution(m_generator),
               radius = m_radius * std::sqrt(m_distribution(m_generator));
        return {m_x_center + radius * std::cos(theta),
                m_y_center + radius * std::sin(theta)};
    }
};

// ############################################################################
// ############################################################################

void test(double radius,
          double x_center,
          double y_center,
          unsigned int number_of_calls,
          unsigned int trials = 1)
{
    std::vector<std::vector<double> > result;
    auto valid = [&](void) -> bool
    {
        for (const auto &point : result)
        {
            double dx = point[0] - x_center,
                   dy = point[1] - y_center;
            double distance = std::sqrt(dx * dx + dy * dy);
            if (distance > radius) return false;
        }
        return true;
    };
    for (unsigned int t = 0; t < trials; ++t)
    {
        Solution object(radius, x_center, y_center);
        result.clear();
        for (unsigned int i = 0; i < number_of_calls; ++i)
            result.push_back(object.randPoint());
    }
    showResult(valid(), "", result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1.0, 0.0, 0.0, 3, trials);
    test(1.0, -1.0, 3.0, 10, trials);
    return 0;
}


