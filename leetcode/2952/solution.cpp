#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumAddedCoins(std::vector<int> coins, int target)
{
    int result = 0;
    size_t i = 0;
    std::sort(coins.begin(), coins.end());
    for (long miss = 1; miss <= target;)
    {
        if ((i < coins.size()) && (coins[i] <= miss))
        {
            miss += coins[i++];
        }
        else
        {
            miss += miss;
            ++result;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> coins, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumAddedCoins(coins, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 10}, 19, 2, trials);
    test({1, 4, 10, 5, 7, 19}, 19, 1, trials);
    test({1, 1, 1}, 20, 3, trials);
    return 0;
}


