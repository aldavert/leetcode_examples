#include "../common/common.hpp"

class Master
{
public:
    Master(std::string s, int a) : secret(s), attempts(a), found(false) {};
    std::string secret;
    int attempts = 10;
    bool found = false;
    int guess(std::string word)
    {
        const size_t n = std::min(word.size(), secret.size());
        int count = 0;
        for (size_t i = 0; i < n; ++i)
            count += (word[i] == secret[i]);
        if (attempts-- > 0)
            found = found || (count == static_cast<int>(secret.size()));
        return count;
    }
};

// ############################################################################
// ############################################################################

#if 1
void findSecretWord(std::vector<std::string> words, Master &master)
{
    auto cmp = [](std::string l, std::string r) -> int
    {
        return (l[0] == r[0]) + (l[1] == r[1]) + (l[2] == r[2])
             + (l[3] == r[3]) + (l[4] == r[4]) + (l[5] == r[5]);
    };
    auto candidate = [&](void) -> std::string
    {
        int histogram[6][26] = {};
        for (std::string word : words)
            for (size_t i = 0; i < 6; ++i)
                histogram[i][word[i] - 'a'] += 1;
        std::string selected;
        int best_score = 0;
        for (std::string word : words)
        {
            int score = 0;
            for (size_t i = 0; i < 6; ++i)
                score += histogram[i][word[i] - 'a'];
            if (score > best_score)
            {
                best_score = score;
                selected = word;
            }
        }
        return selected;
    };
    for (int i = 0; i < 100; ++i)
    {
        std::string selected = candidate();
        int matches = master.guess(selected);
        if (matches == 6) return;
        std::vector<std::string> next;
        for (std::string &word : words)
            if (cmp(word, selected) == matches)
                next.push_back(word);
        words = std::move(next);
    }
}
#else
void findSecretWord(std::vector<std::string> words, Master &master)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    auto cmp = [](std::string l, std::string r) -> int
    {
        return (l[0] == r[0]) + (l[1] == r[1]) + (l[2] == r[2])
             + (l[3] == r[3]) + (l[4] == r[4]) + (l[5] == r[5]);
    };
    for (int i = 0; i < 100; ++i)
    {
        std::string selected;
        if (words.size() == 0) return;
        if (words.size() == 1) selected = words[0];
        else
        {
            std::uniform_int_distribution<> dist(0, static_cast<int>(words.size()) - 1);
            selected = words[dist(gen)];
        }
        int matches = master.guess(selected);
        if (matches == 6) return;
        std::vector<std::string> next;
        for (std::string &word : words)
            if (cmp(word, selected) == matches)
                next.push_back(word);
        words = std::move(next);
    }
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::string secret,
          int number_of_guesses,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        Master m(secret, number_of_guesses);
        findSecretWord(words, m);
        result = m.found;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"acckzz", "ccbazz", "eiowzz", "abcczz"}, "acckzz", 10, true, trials);
    test({"hamada", "khaled"}, "hamada", 10, true, trials);
    test({"pzrooh", "aaakrw", "vgvkxb", "ilaumf", "snzsrz", "qymapx", "hhjgwz", 
          "mymxyu", "jglmrs", "yycsvl", "fuyzco", "ivfyfx", "hzlhqt", "ansstc", 
          "ujkfnr", "jrekmp", "himbkv", "tjztqw", "jmcapu", "gwwwmd", "ffpond", 
          "ytzssw", "afyjnp", "ttrfzi", "xkwmsz", "oavtsf", "krwjwb", "bkgjcs", 
          "vsigmc", "qhpxxt", "apzrtt", "posjnv", "zlatkz", "zynlqc", "czajmi", 
          "smmbhm", "rvlxav", "wkytta", "dzkfer", "urajfh", "lsroct", "gihvuu", 
          "qtnlhu", "ucjgio", "xyycvd", "fsssoo", "kdtmux", "bxnppe", "usucra", 
          "hvsmau", "gstvvg", "ypueay", "qdlvod", "djfbgs", "mcufib", "prohkc", 
          "dsqgms", "eoasya", "kzplbv", "rcuevr", "iwapqf", "ucqdac", "anqomr", 
          "msztnf", "tppefv", "mplbgz", "xnskvo", "euhxrh", "xrqxzw", "wraxvn", 
          "zjhlou", "xwdvvl", "dkbiys", "zbtnuv", "gxrhjh", "ctrczk", "iwylwn", 
          "wefuhr", "enlcrg", "eimtep", "xzvntq", "zvygyf", "tbzmzk", "xjptby", 
          "uxyacb", "mbalze", "bjosah", "ckojzr", "ihboso", "ogxylw", "cfnatk", 
          "zijwnl", "eczmmc", "uazfyo", "apywnl", "jiraqa", "yjksyd", "mrpczo", 
          "qqmhnb", "xxvsbx"}, "anqomr", 11, true, trials);
    return 0;
}



