#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxFreeTime(int eventTime,
                int k,
                std::vector<int> startTime,
                std::vector<int> endTime)
{
    std::vector<int> free_time;
    int previous = 0, result = 0;
    for (size_t i = 0; i < startTime.size(); ++i)
    {
        int time = startTime[i] - previous;
        result = std::max(result, time);
        free_time.push_back(time);
        previous = endTime[i];
    }
    if (eventTime != previous) free_time.push_back(eventTime - previous);
    
    int accu_time = 0;
    for (size_t i = 0; (i <= static_cast<size_t>(k)) && (i < free_time.size()); ++i)
        accu_time += free_time[i];
    result = std::max(result, accu_time);
    for (size_t i = k + 1; i < free_time.size(); ++i)
    {
        accu_time = free_time[i] + accu_time - free_time[i - k - 1];
        result = std::max(result, accu_time);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int eventTime,
          int k,
          std::vector<int> startTime,
          std::vector<int> endTime,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxFreeTime(eventTime, k, startTime, endTime);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 1, {1, 3}, {2, 5}, 2, trials);
    test(10, 1, {0, 2, 9}, {1, 4, 10}, 6, trials);
    test(5, 2, {0, 1, 2, 3, 4}, {1, 2, 3, 4, 5}, 0, trials);
    test(23, 3, {0, 3, 5, 7, 11, 16, 19}, {1, 4, 6, 8, 14, 17, 21}, 9, trials);
    //      2        1     1     3                 2        2           2
    //      -----    --    --    --------          -----    -----       -----
    //  ####     ####  ####  ####        ##########     ####     #######
    // -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+->
    //  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
    test(21, 1, {7, 10, 16}, {10, 14, 18}, 7, trials);
    //  7                                          2           3
    //  ---------------------                      ----        --------
    //                       #########|OOOOOOOOOOOO     #######
    // -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+->
    //  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
    return 0;
}


