#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int minimumCardPickup(std::vector<int> cards)
{
    int result = std::numeric_limits<int>::max();
    std::unordered_map<int, int> last_seen;
    
    for (int i = 0, n = static_cast<int>(cards.size()); i < n; ++i)
    {
        if (auto it = last_seen.find(cards[i]); it != last_seen.cend())
            result = std::min(result, i - it->second + 1);
        last_seen[cards[i]] = i;
    }
    return (result == std::numeric_limits<int>::max()) ? -1 : result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> cards, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumCardPickup(cards);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 4, 2, 3, 4, 7}, 4, trials);
    test({1, 0, 5, 3}, -1, trials);
    return 0;
}


