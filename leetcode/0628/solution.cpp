#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumProduct(std::vector<int> nums)
{
    const size_t n = nums.size();
    std::sort(nums.begin(), nums.end());
    if ((nums[0] < 0) && (nums[1] < 0) && (nums[n - 1] >= 0))
    {
        return nums[n - 1]
             * std::max(nums[0] * nums[1], nums[n - 2] * nums[n - 3]);
    }
    else
    {
        return nums[n - 1]
             * nums[n - 2]
             * nums[n - 3];
    }
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumProduct(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 6, trials);
    test({1, 2, 3, 4}, 24, trials);
    test({-1, -2, -3}, -6, trials);
    test({-1, -2, -3, 0}, 0, trials);
    test({-1, -2, -3, 0, 0, 3}, 18, trials);
    test({-1, -2, -3, -4}, -6, trials);
    test({1, -2, -3, -4}, 12, trials);
    return 0;
}


