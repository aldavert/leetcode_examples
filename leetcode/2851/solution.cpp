#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
int numberOfWays(std::string s, std::string t, long long k)
{
    constexpr int MOD = 1'000'000'007;
    auto modPow = [&](auto &&self, long long a, long long b) -> long long
    {
        if (b < 2)
        {
            if (b == 1) return a % MOD;
            return 1;
        }
        long long temp = self(self, a, b / 2);
        if (b % 2 == 0) return temp * temp % MOD;
        return temp * temp % MOD * a % MOD;
    };
    std::string ss = s + s;
    auto found1 = ss.find(t);
    if (found1 == std::string::npos) return 0;
    long long base = modPow(modPow, s.size() - 1, k);
    int diff = (k % 2 == 0)?1:-1;
    base = (base - diff) * modPow(modPow, s.size(), MOD - 2) % MOD;
    auto found2 = ss.find(t, found1 + 1);
    if (found2 == std::string::npos)
        return static_cast<int>(base);
    else if (found2 - found1 == s.size())
        return static_cast<int>((base + diff) % MOD);
    else
    {
        int d = static_cast<int>(found2 - found1);
        int ans_diff = (found1 == 0)?diff:0;
        return static_cast<int>((s.size() / d * base + ans_diff) % MOD);
    }
}
#else
int numberOfWays(std::string s, std::string t, long long k)
{
    auto zFunction = [](const std::string &w) -> std::vector<int>
    {
        const int n = static_cast<int>(w.size());
        std::vector<int> z(n);
        for (int i = 1, l = 0, r = 0; i < n; ++i)
        {
            if (i < r) z[i] = std::min(r - i, z[i - l]);
            while ((i + z[i] < n) && (w[z[i]] == w[i + z[i]])) ++z[i];
            if (i + z[i] > r)
            {
                l = i;
                r = i + z[i];
            }
        }
        return z;
    };
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(s.size());
    const int neg_one_pow_k = 1 - 2 * ((k & 1) == 1);
    std::vector<int> z = zFunction(s + t + t),
                     indices, dp(2);
    for (int i = n; i < n + n; ++i)
        if (z[i] >= n)
            indices.push_back(i - n);
    auto modPow = [](long px, long pn) -> long
    {
        auto compute = [&](auto &&self, long ix, long in) -> long
        {
            if (in == 0) return 1;
            if (in % 2 == 1) return ix * self(self, ix, in - 1) % MOD;
            return self(self, ix * ix % MOD, in / 2);
        };
        return compute(compute, px, pn);
    };
    dp[1] = static_cast<int>((modPow(n - 1, k) - neg_one_pow_k + MOD) % MOD
                            * modPow(n, MOD - 2) % MOD);
    dp[0] = static_cast<int>((dp[1] + neg_one_pow_k + MOD) % MOD);
    return (int)std::accumulate(indices.begin(), indices.end(), 0L,
            [&](long subtotal, int index) { return (subtotal + dp[index != 0]) % MOD; });
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s,
          std::string t,
          long long k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfWays(s, t, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcd", "cdab", 2, 2, trials);
    test("ababab", "ababab", 1, 2, trials);
    return 0;
}


