#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<std::string> findRepeatedDnaSequences(std::string s)
{
    if (s.size() < 10) return {};
    std::unordered_map<std::string, size_t> histogram;
    for (size_t i = 0, n = s.size() - 9; i < n; ++i)
        ++histogram[s.substr(i, 10)];
    std::vector<std::string> result;
    for (const auto &[sequence, frequency] : histogram)
        if (frequency > 1)
            result.push_back(sequence);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<std::string> sorted_left = left, sorted_right = right;
    std::sort(sorted_left.begin(), sorted_left.end());
    std::sort(sorted_right.begin(), sorted_right.end());
    for (size_t i = 0, n = left.size(); i < n; ++i)
        if (sorted_left[i] != sorted_right[i]) return false;
    return true;
}

void test(std::string s, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findRepeatedDnaSequences(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT", {"AAAAACCCCC", "CCCCCAAAAA"}, trials);
    test("AAAAAAAAAAAAA", {"AAAAAAAAAA"}, trials);
    return 0;
}


