#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

long long findKthSmallest(std::vector<int> coins, int k)
{
    const int n = static_cast<int>(coins.size());
    const unsigned int max_mask = 1 << n;
    std::vector<std::vector<long> > size_to_lcm(n + 1);
    for (unsigned int mask = 1; mask < max_mask; ++mask)
    {
        long lcm_coins = 1;
        for (int i = 0; i < n; ++i)
            if (mask >> i & 1)
                lcm_coins = std::lcm(lcm_coins, coins[i]);
        size_to_lcm[std::popcount(mask)].push_back(lcm_coins);
    }
    auto amountDenominations = [&](long m) -> long
    {
        long result = 0;
        for (int sz = 1; sz <= n; ++sz)
            for (const long lcm : size_to_lcm[sz])
                result += m / lcm * (2L * (sz & 1) - 1L);
        return result;
    };
    
    long l = 0, r = static_cast<long>(k) * *std::min_element(coins.begin(), coins.end());
    while (l < r)
    {
        long m = (l + r) / 2;
        if (amountDenominations(m) >= k)
            r = m;
        else l = m + 1;
    }
    return l;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> coins, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findKthSmallest(coins, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 6, 9}, 3, 9, trials);
    test({5, 2}, 7, 12, trials);
    return 0;
}


