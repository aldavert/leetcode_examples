#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int pivotInteger(int n)
{
    int sum_value = n * (n + 1) / 2,
        result = static_cast<int>(std::sqrt(sum_value));
    return (result * result == sum_value)?result:-1;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = pivotInteger(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(8, 6, trials);
    test(1, 1, trials);
    test(4, -1, trials);
    return 0;
}


