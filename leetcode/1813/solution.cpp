#include "../common/common.hpp"
#include <sstream>

// ############################################################################
// ############################################################################

bool areSentencesSimilar(std::string sentence1, std::string sentence2)
{
    auto split = [](const std::string& sentence) -> std::vector<std::string>
    {
        std::vector<std::string> words;
        std::istringstream iss(sentence);
        for (std::string s; iss >> s;)
            words.push_back(s);
        return words;
    };
    if (sentence1.length() == sentence2.length())
        return sentence1 == sentence2;
    std::vector<std::string> words1 = split(sentence1), words2 = split(sentence2);
    const int m = static_cast<int>(words1.size()),
              n = static_cast<int>(words2.size());
    if (m > n)
        return areSentencesSimilar(sentence2, sentence1);
    int i = 0;
    while ((i < m) && (words1[i] == words2[i]))
        ++i;
    while ((i < m) && (words1[i] == words2[i + n - m]))
        ++i;
    return i == m;
}

// ############################################################################
// ############################################################################

void test(std::string sentence1,
          std::string sentence2,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = areSentencesSimilar(sentence1, sentence2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("My name is Haley", "My Haley", true, trials);
    test("of", "A lot of words", false, trials);
    test("Eating right now", "Eating", true, trials);
    return 0;
}


