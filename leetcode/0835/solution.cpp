#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int largestOverlap(std::vector<std::vector<int> > img1,
                   std::vector<std::vector<int> > img2)
{
    const int n = static_cast<int>(img1.size());
    auto nbits = [](long value1, int offset, int value2) -> int
    {
        value1 = (value1 >> offset) & 0xFFFFFFFF;
        return __builtin_popcount(static_cast<int>(value1) & value2);
    };
    long buffer1[30];
    int  buffer2[30];
    for (int r = 0; r < n; ++r)
    {
        long row1 = 0;
        int  row2 = 0;
        for (int c = 0; c < n; ++c)
        {
            row1 = (row1 << 1) | (img1[r][c] == 1);
            row2 = (row2 << 1) | (img2[r][c] == 1);
        }
        buffer1[r] = row1 << (n - 1);
        buffer2[r] = row2;
    }
    int result = 0;
    for (int ir = n - 1; ir > -n; --ir)
    {
        int br1 = std::max(ir, 0), br2 = std::max(-ir, 0);
        int nrows = n - std::max(br1, br2);
        for (int ic = 0; ic < 2 * n - 1; ++ic)
        {
            int correlation = 0;
            for (int i = br1, j = br2, k = 0; k < nrows; ++i, ++j, ++k)
                correlation += nbits(buffer1[i], ic, buffer2[j]);
            result = std::max(result, correlation);
        }
    }
    return result;
}
#else
int largestOverlap(std::vector<std::vector<int> > img1,
                   std::vector<std::vector<int> > img2)
{
    const int n = static_cast<int>(img1.size());
    const int nd = 2 * n;
    bool buffer[90][90] = {};
    auto correlation = [&](int y, int x) -> int
    {
        int result = 0;
        for (int r = 0; r < n; ++r)
            for (int c = 0; c < n; ++c)
                result += img1[r][c] * buffer[r + y][c + x];
        return result;
    };
    for (int r = 0; r < n; ++r)
        for (int c = 0; c < n; ++c)
            buffer[r + n - 1][c + n - 1] = img2[r][c];
    int result = 0;
    for (int r = 0; r < nd; ++r)
        for (int c = 0; c < nd; ++c)
            result = std::max(result, correlation(r, c));
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > img1,
          std::vector<std::vector<int> > img2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestOverlap(img1, img2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 0}, {0, 1, 0}, {0, 1, 0}},
         {{0, 0, 0}, {0, 1, 1}, {0, 0, 1}}, 3, trials);
    test({{1}}, {{1}}, 1, trials);
    test({{0}}, {{0}}, 0, trials);
    test({{0, 0}, {1, 0}},
         {{0, 1}, {0, 0}}, 1, trials);
    return 0;
}


