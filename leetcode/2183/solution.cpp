#include "../common/common.hpp"
#include <unordered_map>
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
long long countPairs(std::vector<int> nums, int k)
{
    std::unordered_map<int, int> gcds;
    
    long dc = 0;
    for (size_t i = 0; i < nums.size(); ++i)
    {
        if (nums[i] % k == 0) ++dc;
        else if (int d = std::gcd(k, nums[i]); d != 1) ++gcds[d];
    }
    long result = dc * (nums.size() - dc);
    result += dc * (dc - 1) / 2;
    
    for (auto it = gcds.begin(); it != gcds.end(); ++it)
    {
        auto q = k / it->first;
        for (auto it2 = it; it2 != gcds.end(); ++it2)
        {
            if (it2->first % q == 0)
            {
                if (it->first == it2->first)
                    result += (it->second - 1) * it->second / 2;
                else result += it->second * it2->second;
            }
        }
    }
    return result;
}
#else
long long countPairs(std::vector<int> nums, int k)
{
    long result = 0;
    std::unordered_map<int, int> gcds;
    
    for (int num : nums)
    {
        long gcd_i = std::gcd(num, k);
        for (const auto& [gcd_j, count] : gcds)
            if (gcd_i * gcd_j % k == 0)
                result += count;
        ++gcds[static_cast<int>(gcd_i)];
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPairs(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 2, 7, trials);
    test({1, 2, 3, 4}, 5, 0, trials);
    return 0;
}


