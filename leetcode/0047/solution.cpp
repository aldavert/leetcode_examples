#include "../common/common.hpp"
#include <set>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > permuteUnique(std::vector<int> nums)
{
    const size_t n = nums.size();
    std::vector<std::vector<int> > result = {{nums[0]}};
    for (size_t i = 1; i < n; ++i)
    {
        std::set<std::vector<int> > unique;
        for (auto &current : result)
        {
            std::vector<int> next(i + 1);
            for (size_t p = 0; p <= i; ++p)
            {
                for (size_t j = 0, k = 0; j <= i; ++j)
                    next[j] = (p == j)?nums[i]:current[k++];
                unique.insert(next);
            }
        }
        result.clear();
        result.reserve(unique.size());
        for (auto it = unique.begin(); it != unique.end();)
            result.push_back(std::move(unique.extract(it++).value()));
    }
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    std::set<std::vector<int> > lut(left.begin(), left.end());
    for (const auto &r : right)
    {
        if (auto search = lut.find(r); search != lut.end())
            lut.erase(search);
        else return false;
    }
    return lut.empty();
}

void test(std::vector<int> nums,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = permuteUnique(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2}, {{1, 1, 2}, {1, 2, 1}, {2, 1, 1}}, trials);
    test({1, 2, 3},
         {{1, 2, 3}, {1, 3, 2}, {2, 1, 3}, {2, 3, 1}, {3, 1, 2}, {3, 2, 1}}, trials);
    return 0;
}


