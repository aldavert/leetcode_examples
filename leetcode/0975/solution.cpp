#include "../common/common.hpp"
#include <map>

// ############################################################################
// ############################################################################

int oddEvenJumps(std::vector<int> arr)
{
    const int n = static_cast<int>(arr.size());
    std::vector<bool> inc(n), dec(n);
    std::map<int, int> lut;
    lut[arr[n - 1]] = n - 1;
    inc[n - 1] = dec[n - 1] = true;
    for (int i = n - 2; i >= 0; --i)
    {
        if (auto lo = lut.lower_bound(arr[i]); lo != lut.end())
            inc[i] = dec[lo->second];
        if (auto hi = lut.upper_bound(arr[i]); hi != lut.begin())
            dec[i] = inc[prev(hi)->second];
        lut[arr[i]] = i;
    }
    return static_cast<int>(std::count(inc.begin(), inc.end(), true));
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = oddEvenJumps(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 13, 12, 14, 15}, 2, trials);
    test({2, 3, 1, 1, 4}, 3, trials);
    test({5, 1, 3, 4, 2}, 3, trials);
    return 0;
}


