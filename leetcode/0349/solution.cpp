#include "../common/common.hpp"
#include <set>
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> intersection(std::vector<int> nums1, std::vector<int> nums2)
{
    std::vector<int> result;
    std::set<int> nums1_set(nums1.begin(), nums1.end()),
                  nums2_set(nums2.begin(), nums2.end());
    std::set_intersection(nums1_set.begin(), nums1_set.end(),
                          nums2_set.begin(), nums2_set.end(),
                          std::back_inserter(result));
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left,
                const std::vector<int> &right)
{
    std::unordered_map<int, int> histogram;
    for (int l : left)
        ++histogram[l];
    for (int r : right)
    {
        if (auto search = histogram.find(r); search != histogram.end())
        {
            --search->second;
            if (!search->second)
                histogram.erase(search);
        }
        else return false;
    }
    return histogram.empty();
}

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = intersection(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 1}, {2, 2}, {2}, trials);
    test({4, 9, 5}, {9, 4, 9, 8, 4}, {9, 4}, trials);
    return 0;
}


