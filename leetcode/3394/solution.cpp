#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool checkValidCuts([[maybe_unused]] int n, std::vector<std::vector<int> > rectangles)
{
    auto countMerged = [](std::vector<std::pair<int, int> > &intervals) -> int
    {
        int count = 0;
        std::sort(intervals.begin(), intervals.end());
        for (int prev_end = 0; auto [int_begin, int_end] : intervals)
        {
            if (int_begin < prev_end) prev_end = std::max(prev_end, int_end);
            else
            {
                prev_end = int_end;
                ++count;
            }
        }
        return count;
    };
    std::vector<std::pair<int, int> > xs, ys;
    for (const auto &rectangle : rectangles)
    {
        xs.emplace_back(rectangle[0], rectangle[2]);
        ys.emplace_back(rectangle[1], rectangle[3]);
    }
    return std::max(countMerged(xs), countMerged(ys)) >= 3;
}
#else
bool checkValidCuts([[maybe_unused]] int n, std::vector<std::vector<int> > rectangles)
{
    struct Segment
    {
        int start = 0;
        int end = 0;
        size_t idx = 0;
        bool operator<(const Segment &other) const { return end < other.end; }
        bool operator<(int value) const { return end < value; }
    };
    std::vector<Segment> segments;
    auto canBePartitioned = [&]() -> bool
    {
        std::set<Segment> sorted;
        std::vector<size_t> id(segments.size());
        for (size_t i = 0; i < segments.size(); ++i)
            id[i] = i;
        auto find = [&](auto &&self, size_t u) -> size_t
        {
            return (id[u] != u)?id[u] = self(self, id[u]):u;
        };
        auto merge = [&](size_t i, size_t j) -> void
        {
            size_t u = find(find, i), v = find(find, j);
            if (u == v) return;
            if (u < v) id[v] = u;
            else id[u] = v;
        };
        auto cmp = [](int value, const Segment &s) { return value < s.end; };
        for (size_t i = 0; i < segments.size(); ++i)
        {
            auto it =
                std::upper_bound(sorted.begin(), sorted.end(), segments[i].start, cmp);
            for (; (it != sorted.end()) && (segments[i].end > it->start); ++it)
                merge(i, it->idx);
            sorted.insert(segments[i]);
        }
        std::set<size_t> groups;
        for (size_t i = 0; i < segments.size(); ++i)
            groups.insert(find(find, i));
        return groups.size() >= 3;
    };
    
    for (size_t i = 0; i < rectangles.size(); ++i)
        segments.push_back({rectangles[i][0], rectangles[i][2], segments.size()});
    if (canBePartitioned()) return true;
    segments.clear();
    for (size_t i = 0; i < rectangles.size(); ++i)
        segments.push_back({rectangles[i][1], rectangles[i][3], segments.size()});
    if (canBePartitioned()) return true;
    return false;
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > rectangles,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkValidCuts(n, rectangles);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{1, 0, 5, 2}, {0, 2, 2, 4}, {3, 2, 5, 3}, {0, 4, 4, 5}}, true, trials);
    test(4, {{0, 0, 1, 1}, {2, 0, 3, 4}, {0, 2, 2, 3}, {3, 0, 4, 3}}, true, trials);
    test(4, {{0, 2, 2, 4}, {1, 0, 3, 2}, {2, 2, 3, 4}, {3, 0, 4, 2},
             {3, 2, 4, 4}}, false, trials);
    test(4, {{0, 0, 1, 4}, {1, 0, 2, 4}, {2, 0, 3, 4}, {3, 0, 4, 4}}, true, trials);
    test(5, {{0, 0, 2, 3}, {2, 0, 3, 3}, {3, 0, 5, 3}, {0, 3, 1, 5},
             {1, 3, 3, 5}, {3, 3, 5, 5}}, false, trials);
    return 0;
}


