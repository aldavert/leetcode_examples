#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numFriendRequests(std::vector<int> ages)
{
    int histogram[122] = {};
    for (int age : ages)
        ++histogram[age];
    int result = 0;
    for (int i = 0; i < 121; ++i)
        for (int j = 0; j < 121; ++j)
            result += ((histogram[i] - (i == j)) * histogram[j])
                   * (!((static_cast<double>(j) <= 0.5 * i + 7)
                   || (j > i)
                   || ((j > 100) && (i < 100))));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> ages, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numFriendRequests(ages);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({16, 16}, 2, trials);
    test({16, 17, 18}, 2, trials);
    test({20, 30, 100, 110, 120}, 3, trials);
    return 0;
}


