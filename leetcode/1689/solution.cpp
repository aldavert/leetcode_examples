#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int minPartitions(std::string n)
{
    bool number[10] = {false};
    const int s = static_cast<int>(n.size());
    for (int i = 0; i < s; ++i)
        number[n[i] - '0'] = true;
    for (int i = 9; i > 0; --i)
        if (number[i]) return i;
    return 0;
}
#else
int minPartitions(std::string n)
{
    int max_number = 0;
    for (const auto &c : n)
    {
        if (c == '9') return 9;
        max_number = std::max(c - '0', max_number);
    }
    return max_number;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minPartitions(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 100000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("32", 3, trials);
    test("82734", 8, trials);
    test("27346209830709182346", 9, trials);
    return 0;
}


