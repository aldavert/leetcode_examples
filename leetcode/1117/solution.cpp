#include "../common/common.hpp"
#include <functional>
#include <semaphore.h>
#include <thread>

// ############################################################################
// ############################################################################

std::string output;
void releaseHydrogen(void) { output += "H"; }
void releaseOxygen(void) { output += "O"; }

class H2O
{
public:
    H2O(void)
    {
        sem_init(&hSemaphore, /*pshared=*/0, /*value=*/1);
        sem_init(&oSemaphore, /*pshared=*/0, /*value=*/0);
    }
    ~H2O(void)
    {
        sem_destroy(&hSemaphore);
        sem_destroy(&oSemaphore);
    }
    void hydrogen(std::function<void()> releaseHydrogen)
    {
        sem_wait(&hSemaphore);
        ++h;
        releaseHydrogen();
        if (h % 2 == 0) sem_post(&oSemaphore);
        else sem_post(&hSemaphore);
    }
    void oxygen(std::function<void()> releaseOxygen)
    {
        sem_wait(&oSemaphore);
        releaseOxygen();
        sem_post(&hSemaphore);
    }
private:
    sem_t hSemaphore;
    sem_t oSemaphore;
    int h = 0;
};

// ############################################################################
// ############################################################################

void test(std::string water, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        H2O obj;
        output.clear();
        std::thread t1([&]() { while (output.size() + 1 < water.size())
                                obj.hydrogen(releaseHydrogen); }),
                    t2([&]() { while (output.size() < water.size())
                                obj.oxygen(releaseOxygen); });
        t1.join();
        t2.join();
        result = output;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("HOH", "HHO", trials);
    test("OOHHHH", "HHOHHO", trials);
    return 0;
}


