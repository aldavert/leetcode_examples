#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumBeautifulSubstrings(std::string s)
{
    auto isPowerOfFive = [](int num) -> bool
    {
        while (num % 5 == 0) num /= 5;
        return num == 1;
    };
    const int n = static_cast<int>(s.size());
    std::vector<int> dp(n + 1, n + 1);
    dp[0] = 0;
    for (int i = 1; i <= n; ++i)
    {
        if (s[i - 1] == '0') continue;
        int num = 0;
        for (int j = i; j <= n; ++j)
        {
            num = (num << 1) + s[j - 1] - '0';
            if (isPowerOfFive(num))
                dp[j] = std::min(dp[j], dp[i - 1] + 1);
        }
    }
    return (dp[n] == n + 1)?-1:dp[n];
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumBeautifulSubstrings(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1011", 2, trials);
    test("111", 3, trials);
    test("0", -1, trials);
    return 0;
}


