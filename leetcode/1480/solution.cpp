#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> runningSum(std::vector<int> nums)
{
    std::vector<int> result;
    result.reserve(nums.size());
    for (int sum = 0; int value : nums)
    {
        sum += value;
        result.push_back(sum);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = runningSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, {1, 3, 6, 10}, trials);
    test({1, 1, 1, 1, 1}, {1, 2, 3, 4, 5}, trials);
    test({3, 1, 2, 10, 1}, {3, 4, 6, 16, 17}, trials);
    return 0;
}


