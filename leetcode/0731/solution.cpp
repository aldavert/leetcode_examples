#include "../common/common.hpp"
#include <map>

// ############################################################################
// ############################################################################

#if 1
class MyCalendarTwo
{
    struct Event
    {
        int start = -1;
        int end = -1;
        inline bool hasIntersection(const Event &other) const
        {
            return std::max(start, other.start) < std::min(end, other.end);
        }
        inline Event intersection(const Event &other) const
        {
            return {std::max(start, other.start), std::min(end, other.end)};
        }
    };
    std::vector<Event> calendar;    
    std::vector<Event> repeated; 
public:
    MyCalendarTwo(void) = default;
    bool book(int start, int end)
    {
        Event current = {start, end};
        for (auto present : repeated)
            if (current.hasIntersection(present))
                return false;
        for (auto event : calendar)
            if (current.hasIntersection(event))
                repeated.push_back(current.intersection(event));
        calendar.push_back(current);
        return true;
    }
};
#else
class MyCalendarTwo
{
    std::map<int, int> events;
public:
    MyCalendarTwo(void) = default;
    bool book(int start, int end)
    {
        ++events[start];
        --events[end];
        int active_events = 0;
        for (auto [_, count] : events)
        {
            active_events += count;
            if (active_events > 2)
            {
                if (--events[start] == 0)
                    events.erase(start);
                if (++events[end] == 0)
                    events.erase(end);
                return false;
            }
        }
        return true;
    }
};
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::tuple<int, int> > events,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        MyCalendarTwo obj;
        result.clear();
        for (auto [start, end] : events)
            result.push_back(obj.book(start, end));
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{10, 20}, {50, 60}, {10, 40}, {5, 15}, {5, 10}, {25, 55}},
         {true, true, true, false, true, true}, trials);
    return 0;
}


