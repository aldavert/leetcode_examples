#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minimumTime(int n, std::vector<std::vector<int> > relations, std::vector<int> time)
{
    std::vector<std::vector<int> > graph(n);
    std::vector<int> in_degree(n), dist(time);
    std::queue<int> q;
    
    for (const auto &r : relations)
    {
        graph[r[0] - 1].push_back(r[1] - 1);
        ++in_degree[r[1] - 1];
    }
    
    for (int i = 0; i < n; ++i)
        if (in_degree[i] == 0)
            q.push(i);
    while (!q.empty())
    {
        int node = q.front();
        q.pop();
        for (int next : graph[node])
        {
            dist[next] = std::max(dist[next], dist[node] + time[next]);
            if (--in_degree[next] == 0)
                q.push(next);
        }
    }
    
    return *std::max_element(dist.begin(), dist.end());
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > relations,
          std::vector<int> time,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumTime(n, relations, time);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{1, 3}, {2, 3}}, {3, 2, 5}, 8, trials);
    test(5, {{1, 5}, {2, 5}, {3, 5}, {3, 4}, {4, 5}}, {1, 2, 3, 4, 5}, 12, trials);
    return 0;
}


