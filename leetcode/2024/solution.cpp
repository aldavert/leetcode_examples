#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxConsecutiveAnswers(std::string answerKey, int k)
{
    const int n = static_cast<int>(answerKey.size());
    int result = 0;
    for (int l = 0, r = 0, max_count = 0, count[2] = {}; r < n; ++r)
    {
        max_count = std::max(max_count, ++count[answerKey[r] == 'T']);
        while (max_count + k < r - l + 1)
            --count[answerKey[l++] == 'T'];
        result = std::max(result, r - l + 1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string answerKey, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxConsecutiveAnswers(answerKey, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("TTFF", 2, 4, trials);
    test("TFFT", 1, 3, trials);
    test("TTFTTFTT", 1, 5, trials);
    return 0;
}


