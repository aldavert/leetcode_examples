#include "../common/common.hpp"
#include <map>

// ############################################################################
// ############################################################################

std::vector<int> sortJumbled(std::vector<int> mapping, std::vector<int> nums)
{
    std::vector<int> result;
    std::map<int, std::vector<int> > mapped_to_original_nums;
    
    for (int num : nums)
    {
        std::string mapped;
        for (char c : std::to_string(num))
            mapped += std::to_string(mapping[c - '0']);
        mapped_to_original_nums[std::stoi(mapped)].push_back(num);
    }
    for (const auto& [_, originalNums] : mapped_to_original_nums)
        result.insert(result.end(), originalNums.begin(), originalNums.end());
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> mapping,
          std::vector<int> nums,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sortJumbled(mapping, nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 9, 4, 0, 2, 1, 3, 5, 7, 6}, {991, 338, 38}, {338, 38, 991}, trials);
    test({0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, {789, 456, 123}, {123, 456, 789}, trials);
    test({}, {}, {}, trials);
    return 0;
}


