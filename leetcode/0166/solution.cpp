#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::string fractionToDecimal(int numerator, int denominator)
{
    std::string result;
    if (denominator == 0) return "";
    if (numerator == 0) return "0";
    if (((numerator <  0) && (denominator > 0))
    ||  ((numerator >= 0) && (denominator < 0)))
        result = "-";
    long num = std::abs(static_cast<long>(numerator));
    long den = std::abs(static_cast<long>(denominator));
    result += std::to_string(num / den);
    num = (num % den) * 10;
    if (num == 0) return result;
    result += '.';
    std::unordered_map<long, size_t> lut;
    while (num > 0)
    {
        if (auto search = lut.find(num); search != lut.end())
        {
            return result.substr(0, search->second) + "("
                 + result.substr(search->second, result.size()) + ")";
        }
        else lut[num] = result.size();
        if (num < den)
        {
            num *= 10;
            result += '0';
        }
        else
        {
            result += std::to_string(num / den);
            num = (num % den) * 10;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int numerator, int denominator, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = fractionToDecimal(numerator, denominator);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 2, "0.5", trials);
    test(2, 1, "2", trials);
    test(4, 333, "0.(012)", trials);
    test(1, 8, "0.125", trials);
    test(22, 7, "3.(142857)", trials);
    test(0, 3, "0", trials);
    test(7, -12, "-0.58(3)", trials);
    test(-1, -2147483648, "0.0000000004656612873077392578125", trials);
    test(420, 226, "1.(858407079646017699115044247787610619469"
                      "026548672566371681415929203539823008849"
                      "5575221238938053097345132743362831)", trials);
    return 0;
}


