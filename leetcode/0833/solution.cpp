#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string findReplaceString(std::string s,
                              std::vector<int> indices,
                              std::vector<std::string> sources,
                              std::vector<std::string> targets)
{
    const size_t n = s.size();
    std::vector<size_t> replace(n, n);
    for (size_t i = 0; i < indices.size(); ++i)
        if (s.substr(indices[i], sources[i].size()) == sources[i])
            replace[indices[i]] = i;
    std::string result;
    for (size_t i = 0; i < n;)
    {
        if (replace[i] != n)
        {
            result += targets[replace[i]];
            i += sources[replace[i]].size();
        }
        else result += s[i++];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<int> indices,
          std::vector<std::string> sources,
          std::vector<std::string> targets,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findReplaceString(s, indices, sources, targets);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcd", {0, 2}, {"a", "cd"}, {"eee", "ffff"}, "eeebffff", trials);
    test("abcd", {0, 2}, {"ab", "ec"}, {"eee", "ffff"}, "eeecd", trials);
    test("abcdef", {2, 2}, {"feg", "cdef"}, {"abc", "feg"}, "abfeg", trials);
    return 0;
}


