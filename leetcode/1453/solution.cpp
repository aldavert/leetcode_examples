#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int numPoints(std::vector<std::vector<int> > darts, int r)
{
    static constexpr double ERR = 1e-6;
    struct Point
    {
        double x;
        double y;
        Point(double px, double py) : x(px), y(py) {}
        double dist(const Point &other) const
        {
            double dx = x - other.x, dy = y - other.y;
            return std::sqrt(dx * dx + dy * dy);
        }
    };
    auto getCircles =
        [](const Point &p, const Point &q, int radius) -> std::vector<Point>
    {
        double dist = p.dist(q);
        if (dist - 2.0 * radius > ERR)
            return {};
        Point m{(p.x + q.x) / 2, (p.y + q.y) / 2};
        dist = std::sqrt(radius * radius - dist * dist / 4);
        double alpha = std::atan2(p.y - q.y, q.x - p.x);
        return { Point{m.x - dist * std::sin(alpha), m.y - dist * std::cos(alpha)},
                 Point{m.x + dist * std::sin(alpha), m.y + dist * std::cos(alpha)} };
    };
    int result = 1;
    std::vector<Point> points;
    for (const std::vector<int> &dart : darts)
        points.emplace_back(dart[0], dart[1]);
    for (int i = 0, n = static_cast<int>(points.size()); i < n; ++i)
    {
        for (int j = i + 1; j < n; ++j)
        {
            for (auto in_circle = getCircles(points[i], points[j], r);
                 Point &c : in_circle)
            {
                int count = 0;
                for (const Point &point : points)
                    count += (c.dist(point) - r <= ERR);
                result = std::max(result, count);
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > darts,
          int r,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numPoints(darts, r);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{-2, 0}, {2, 0}, {0, 2}, {0, -2}}, 2, 4, trials);
    test({{-3, 0}, {3, 0}, {2, 6}, {5, 4}, {0, 9}, {7, 8}}, 5, 5, trials);
    return 0;
}


