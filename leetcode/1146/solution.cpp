#include "../common/common.hpp"

constexpr int null = -1000;

// ############################################################################
// ############################################################################

#if 1
class SnapshotArray
{
public:
    SnapshotArray(int length) : m_snaps(length)
    {
        for (auto &snap : m_snaps) snap.push_back({0, 0});
    }
    void set(int index, int val)
    {
        auto &[last_snap_id, last_val] = m_snaps[index].back();
        if (last_snap_id == m_snap_id) last_val = val;
        else m_snaps[index].push_back({m_snap_id, val});
    }
    inline int snap(void) { return m_snap_id++; }
    int get(int index, int snap_id)
    {
        const std::vector<std::pair<int, int> > &snap = m_snaps[index];
        auto it = std::lower_bound(snap.begin(), snap.end(),
                std::make_pair(snap_id + 1, 0));
        return std::prev(it)->second;
    }
private:
    std::vector<std::vector<std::pair<int, int> > > m_snaps;
    int m_snap_id = 0;
};
#else
class SnapshotArray
{
    std::vector<std::unordered_map<int, int> > m_snaps;
    std::vector<int> m_active;
public:
    SnapshotArray(int length) :
        m_active(length) {}
    void set(int index, int val) { m_active[index] = val; }
    int snap()
    {
        m_snaps.push_back({});
        for (int i = 0, n = static_cast<int>(m_active.size()); i < n; ++i)
            if (m_active[i] != 0)
                m_snaps.back()[i] = std::exchange(m_active[i], 0);
        return static_cast<int>(m_snaps.size() - 1);
    }
    int get(int index, int snap_id)
    {
        if (snap_id < static_cast<int>(m_snaps.size()))
            return m_snaps[snap_id][index];
        else return m_active[index];
    }
};
#endif

// ############################################################################
// ############################################################################

enum class OP { SET, GET, SNAP };

void test(int length,
          std::vector<OP> op,
          std::vector<std::pair<int, int> > values,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        SnapshotArray obj(length);
        result.clear();
        for (size_t i = 0; i < op.size(); ++i)
        {
            switch (op[i])
            {
            case OP::SET:
                obj.set(values[i].first, values[i].second);
                result.push_back(null);
                break;
            case OP::GET:
                result.push_back(obj.get(values[i].first, values[i].second));
                break;
            case OP::SNAP:
                result.push_back(obj.snap());
                break;
            default:
                std::cerr << "[ERROR] Unknown operation.\n";
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {OP::SET, OP::SNAP, OP::SET, OP::GET}, {{0, 5}, {}, {0, 6}, {0, 0}},
         {null, 0, null, 5}, trials);
    return 0;
}


