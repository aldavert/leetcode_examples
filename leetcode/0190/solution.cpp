#include "../common/common.hpp"

// ############################################################################
// ############################################################################

uint32_t reverseBits(uint32_t n)
{
    uint32_t result = 0;
    for (int i = 0; i < 32; ++i)
    {
        result = (result << 1) | (n & 1);
        n >>= 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(uint32_t n, uint32_t solution, unsigned int trials = 1)
{
    uint32_t result = 0;
    for (unsigned int i = 0; i < trials; ++i)
        result = reverseBits(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(0b00000010100101000001111010011100,
         0b00111001011110000010100101000000,
         trials);
    test(0b11111111111111111111111111111101,
         0b10111111111111111111111111111111,
         trials);
    return 0;
}


