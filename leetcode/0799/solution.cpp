#include "../common/common.hpp"

// ############################################################################
// ############################################################################

double champagneTower(int poured, int query_row, int query_glass)
{
    double dp[2][100] = {};
    dp[0][0] = poured;
    bool active_row = false;
    for (int i = 0; i < query_row; ++i, active_row = !active_row)
    {
        dp[!active_row][0] = 0;
        for (int j = 0; j <= i; ++j)
        {
            if (dp[active_row][j] > 1)
            {
                dp[!active_row][j    ] += (dp[active_row][j] - 1) / 2.0;
                dp[!active_row][j + 1]  = (dp[active_row][j] - 1) / 2.0;
            }
            else dp[!active_row][j + 1] = 0;
        }
    }
    return std::min(1.0, dp[active_row][query_glass]);
}

// ############################################################################
// ############################################################################

void test(int poured,
          int query_row,
          int query_glass,
          double solution,
          unsigned int trials = 1)
{
    double result = 0.0;
    for (unsigned int i = 0; i < trials; ++i)
        result = champagneTower(poured, query_row, query_glass);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 1, 1, 0.0, trials);
    test(2, 1, 1, 0.5, trials);
    test(100000009, 33, 17, 1.0, trials);
    test(25, 6, 1, 0.18750, trials);
    return 0;
}


