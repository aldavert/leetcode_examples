#ifndef __COMMON_HEADER__
#define __COMMON_HEADER__

#include <iostream>
#include <vector>
#include <set>
#include <list>
#include <unordered_set>
#include <tuple>
#include <variant>

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::pair<T, T> &par);
template <typename... Ts>
std::ostream& operator<<(std::ostream &out, const std::tuple<Ts...> &tup);
template <typename... Ts>
std::ostream& operator<<(std::ostream &out, const std::variant<Ts...> &var);

template <template <class> typename CONTAINER, typename T>
concept Container = (std::same_as<CONTAINER<T>, std::set<T> >
                 ||  std::same_as<CONTAINER<T>, std::unordered_set<T> >
                 ||  std::same_as<CONTAINER<T>, std::vector<T> >
                 ||  std::same_as<CONTAINER<T>, std::list<T> >);

template <template <class> typename CONTAINER, typename T>
std::ostream& operator<<(std::ostream &out, const CONTAINER<T> &s)
requires(Container<CONTAINER, T>)
{
    constexpr size_t LIMIT = 10;
    out << '{';
    bool next = false, dots = true;
    size_t count = 0;
    for (const auto &v: s)
    {
        if ((count < LIMIT) || (count >= s.size() - LIMIT))
        {
            if (next) [[likely]] out << ", ";
            next = true;
            out << v;
        }
        else if (dots)
        {
            out << ", ...";
            dots = false;
        }
        ++count;
    }
    out << '}';
    return out;
}

template <typename... Ts>
std::ostream& operator<<(std::ostream &out, const std::variant<Ts...> &var)
{
    std::visit([&out](auto &&args) { out << args; }, var);
    return out;
}

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::pair<T, T> &par)
{
    out << '{' << par.first << ", " << par.second << '}';
    return out;
}

template <typename... Ts>
std::ostream& operator<<(std::ostream &out, const std::tuple<Ts...> &tup)
{
    out << '{';
    std::apply([&out](Ts const & ... args)
               {
                    size_t n = 0;
                    ((out << args << (++n != sizeof...(Ts)?", ":"")), ...);
               }, tup);
    out << '}';
    return out;
}

/*
#include "../common/common.hpp"
#include "../common/tree.hpp"
    showResult(solution == result, solution, result);
*/
std::string showResult(bool valid)
{
    return (valid)?"\033[30;42m[SUCCESS]\033[0m"
                  :"\033[30;41m[FAILURE]\033[0m";
}
void showResult(bool valid, const auto &solution, const auto &result)
{
    std::cout << showResult(valid) << " Expected '"
              << solution << "' and obtained '" << result << "'.\n";
}

#endif


