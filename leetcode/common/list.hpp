#ifndef __LIST_HEADER__
#define __LIST_HEADER__

#include <vector>

struct ListNode
{
    int val = 0;
    ListNode * next = nullptr;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode * n) : val(x), next(n) {}
    ~ListNode(void) { delete next; }
};

ListNode * vec2list(const std::vector<int> &vec)
{
    if (vec.size() == 0) return nullptr;
    else
    {
        const int n = static_cast<int>(vec.size());
        ListNode * l = new ListNode(vec[0]);
        ListNode * current = l;
        for (int i = 1; i < n; ++i)
            current = current->next = new ListNode(vec[i]);
        return l;
    }
}

std::vector<int> list2vec(const ListNode * l)
{
    int n = 0;
    for (const ListNode * current = l; current != nullptr; ++n, current = current->next);
    std::vector<int> vec(n);
    n = 0;
    for (const ListNode * current = l; current != nullptr; ++n, current = current->next)
        vec[n] = current->val;
    return vec;
}

#endif
