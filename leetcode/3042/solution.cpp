#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countPrefixSuffixPairs(std::vector<std::string> words)
{
    auto check = [](std::string particle, std::string word) -> bool
    {
        return (particle.size() <= word.size())
            && (word.substr(0, particle.size()) == particle)
            && (word.substr(word.size() - particle.size(), particle.size()) == particle);
    };
    int result = 0;
    for (size_t i = 0; i < words.size(); ++i)
        for (size_t j = i + 1; j < words.size(); ++j)
            result += check(words[i], words[j]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPrefixSuffixPairs(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"a", "aba", "ababa", "aa"}, 4, trials);
    test({"pa", "papa", "ma", "mama"}, 2, trials);
    test({"abab", "ab"}, 0, trials);
    test({"abab", "abab"}, 1, trials);
    return 0;
}


