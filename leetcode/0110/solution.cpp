#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <iostream>

// ############################################################################
// ############################################################################

bool isBalanced(TreeNode * root)
{
    bool result = true;
    auto height = [&](auto &&self, TreeNode * ptr) -> int
    {
        if (!result) return 0;
        if (!ptr) return 0;
        int left_height = self(self, ptr->left);
        int right_height = self(self, ptr->right);
        result = result && (std::abs(left_height - right_height) <= 1);
        return std::max(left_height, right_height) + 1;
    };
    height(height, root);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = isBalanced(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 9, 20, null, null, 15, 7}, true, trials);
    test({1, 2, 2, 3, 3, null, null, 4, 4}, false, trials);
    test({}, true, trials);
    return 0;
}


