# Balanced Binary Tree

Given a binary tree, determine if it is height-balanced.

For this problem, a height-balanced binary tree is defined as:
- a binary tree in which the left and right subtrees of *every* node differ in height by no more than `1`.

#### Example 1:
> ```mermaid 
> graph TD;
> A((3))---B((9))
> A---C((20))
> C---D((15))
> C---E((7))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [3, 9, 20, null, null, 15, 7]`  
> *Output:* `true`

#### Example 2:
> ```mermaid 
> graph TD;
> A((1))---B((2))
> A---C((2))
> B---D((3))
> B---E((3))
> D---F((4))
> D---G((4))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [1, 2, 2, 3, 3, null, null, 4, 4]`  
> *Output:* `false`

#### Example 3:
> *Input:* `root = []`  
> *Output:* `true`

#### Constraints:
- The number of nodes in the tree is in the range `[0, 5000]`.
- `-10^4 <= Node.val <= 10^4`


