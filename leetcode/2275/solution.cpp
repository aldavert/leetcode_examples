#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int largestCombination(std::vector<int> candidates)
{
    constexpr int MAXBIT = 24;
    int result = 0;
    
    for (int i = 0; i < MAXBIT; ++i)
    {
        int count = 0;
        for (int candidate : candidates)
            count += (candidate >> i & 1);
        result = std::max(result, count);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> candidates, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestCombination(candidates);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({16, 17, 71, 62, 12, 24, 14}, 4, trials);
    test({8, 8}, 2, trials);
    return 0;
}


