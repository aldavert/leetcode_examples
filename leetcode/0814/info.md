# Binary Tree Pruning

Given the `root` of a binary tree, return *the same tree where every subtree (of the given tree) not containing a 1 has been removed*.

A subtree of a `node` node is `node` plus every node that is a descendant of node.

#### Example 1:
> ```mermaid
> flowchart LR;
> subgraph S1 [ ]
> A1((1))---EMPTY1(( ))
> A1-->B1((0))
> B1-->C1((0))
> B1-->D1((1))
> end
> subgraph S2 [ ]
> A2((1))---EMPTY2(( ))
> A2-->B2((0))
> B2---EMPTY3(( ))
> B2-->D2((1))
> end
> S1-->S2
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef selected fill:#FA5,stroke:#000,stroke-width:2px;
> class EMPTY1,EMPTY2,EMPTY3,S1,S2 empty;
> class C1 selected;
> linkStyle 0,4,6 stroke-width:0px;
> ```
> *Input:* `root = [1, null, 0, 0, 1]`  
> *Output:* `[1, null, 0, null, 1]`  
> *Explanation:* Only the red nodes satisfy the property *"every subtree not containing a 1"*. The diagram on the right represents the answer.

#### Example 2:
> ```mermaid
> flowchart LR;
> subgraph S1 [ ]
> A1((1))-->B1((0))
> A1-->C1((1))
> B1-->D1((0))
> B1-->E1((0))
> C1-->F1((0))
> C1-->G1((1))
> end
> subgraph S2 [ ]
> A2((1))---EMPTY1(( ))
> A2-->B2((1))
> B2---EMPTY2(( ))
> B2-->D2((1))
> end
> S1-->S2
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef selected fill:#FA5,stroke:#000,stroke-width:2px;
> class EMPTY1,EMPTY2,EMPTY3,S1,S2 empty;
> class B1,D1,E1,F1 selected;
> linkStyle 6,8 stroke-width:0px;
> ```
> *Input:* `root = [1, 0, 1, 0, 0, 0, 1]`  
> *Output:* `[1, null, 1, null, 1]`

#### Example 3
> ```mermaid
> flowchart LR;
> subgraph S1 [ ]
> A1((1))-->B1((1))
> A1-->C1((0))
> B1-->D1((1))
> B1-->E1((1))
> C1-->F1((0))
> C1-->G1((1))
> D1-->H1((0))
> D1---EMPTY1(( ))
> end
> subgraph S2 [ ]
> A2((1))-->B2((1))
> A2-->C2((0))
> B2-->D2((1))
> B2-->E2((1))
> C2---EMPTY2(( ))
> C2-->G2((1))
> end
> S1-->S2
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef selected fill:#FA5,stroke:#000,stroke-width:2px;
> class EMPTY1,EMPTY2,S1,S2 empty;
> class H1,F1 selected;
> linkStyle 7,12 stroke-width:0px;
> ```
> *Input:* `root = [1, 1, 0, 1, 1, 0, 1, 0]`  
> *Output:* `[1, 1, 0, 1, 1, null, 1]`

#### Constrains:
- The number of nodes in the tree is in the range `[1, 200]`.
- `Node.val` is either `0` or `1`.


