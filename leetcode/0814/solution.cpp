#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * pruneTree(TreeNode * root)
{
    if (root != nullptr)
    {
        root->left = pruneTree(root->left);
        root->right = pruneTree(root->right);
        if ((root->val == 1) || (root->left != nullptr) || (root->right != nullptr))
            return root;
        else
        {
            delete root;
            return nullptr;
        }
    }
    else return nullptr;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        root = pruneTree(root);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, null, 0, 0, 1}, {1, null, 0, null, 1}, trials);
    test({1, 0, 1, 0, 0, 0, 1}, {1, null, 1, null, 1}, trials);
    test({1, 1, 0, 1, 1, 0, 1, 0}, {1, 1, 0, 1, 1, null, 1}, trials);
    test({0, null, 0, 0, 0}, {}, trials);
    return 0;
}


