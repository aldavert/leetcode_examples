#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int countPrefixes(std::vector<std::string> words, std::string s)
{
    auto cmp = [&](const auto &word) -> bool
    {
        return (word.size() <= s.size()) && (s.substr(0, word.size()) == word);
    };
    return static_cast<int>(std::count_if(words.begin(), words.end(), cmp));
}
#else
int countPrefixes(std::vector<std::string> words, std::string s)
{
    int result = 0;
    for (auto &word: words)
        result += (word.size() <= s.size()) && (s.substr(0, word.size()) == word);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::string s,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPrefixes(words, s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"a", "b", "c", "ab", "bc", "abc"}, "abc", 3, trials);
    test({"a", "a"}, "aa", 2, trials);
    return 0;
}


