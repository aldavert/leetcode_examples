#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findSmallestSetOfVertices(int n, std::vector<std::vector<int> > edges)
{
    std::vector<int> in_edge(n);
    for (const auto &edge : edges) ++in_edge[edge[1]];
    std::vector<int> result;
    for (int i = 0; i < n; ++i)
        if (in_edge[i] == 0)
            result.push_back(i);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<int> left_copy = left, right_copy = right;
    std::sort(left_copy.begin(), left_copy.end());
    std::sort(right_copy.begin(), right_copy.end());
    for (size_t i = 0, n = left.size(); i < n; ++i)
        if (left_copy[i] != right_copy[i])
            return false;
    return true;
}

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findSmallestSetOfVertices(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, {{0, 1}, {0, 2}, {2, 5}, {3, 4}, {4, 2}}, {0, 3}, trials);
    test(5, {{0, 1}, {2, 1}, {3, 1}, {1, 4}, {2, 4}}, {0, 2, 3}, trials);
    return 0;
}


