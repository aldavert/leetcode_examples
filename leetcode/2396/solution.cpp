#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isStrictlyPalindromic([[maybe_unused]] int n)
{
    return false;
}

// ############################################################################
// ############################################################################

void test(int n, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isStrictlyPalindromic(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(9, false, trials);
    test(4, false, trials);
    return 0;
}


