#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int makeStringGood(std::string s)
{
    int result = static_cast<int>(s.size());
    int count[26] = {};
    for (char c : s) ++count[c - 'a'];
    const int mx = *std::max_element(&count[0], &count[26]);
    for (int target = 1; target <= mx; ++target)
    {
        int dp[27] = {};
        for (int i = 25; i >= 0; --i)
        {
            dp[i] = std::min(count[i], std::abs(target - count[i])) + dp[i + 1];
            if ((i + 1 < 26) && (count[i + 1] < target))
            {
                int next_deficit = target - count[i + 1];
                int need_to_change = (count[i] > target)?count[i] - target:count[i];
                int change_to_target = (next_deficit > need_to_change)
                                     ? need_to_change + (next_deficit - need_to_change)
                                     : next_deficit + (need_to_change - next_deficit);
                dp[i] = std::min(dp[i], change_to_target + dp[i + 2]);
            }
        }
        result = std::min(result, dp[0]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = makeStringGood(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("acab", 1, trials);
    test("wddw", 0, trials);
    test("aaabc", 2, trials);
    return 0;
}


