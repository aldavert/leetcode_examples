#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int clumsy(int n)
{
    int accumulated = 0;
    int current = -n;
    --n;
    for (unsigned int idx = 0; n >= 1; --n)
    {
        if (idx == 0) // Multiplication
        {
            current *= n;
            ++idx;
        }
        else if (idx == 1) // Division
        {
            accumulated -= current / n;
            current = 0;
            ++idx;
        }
        else if (idx == 2) // Addition
        {
            accumulated += n;
            ++idx;
        }
        else if (idx == 3)
        {
            current = n;
            idx = 0;
        }
    }
    return accumulated - current;
}

// ############################################################################
// ############################################################################

void test(int input, int solution)
{
    int result = clumsy(input);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
    test(4, 7);
    test(10, 12);
    return 0;
}


