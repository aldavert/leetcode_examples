#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int eraseOverlapIntervals(std::vector<std::vector<int> > intervals)
{
    struct Interval
    {
        Interval(const std::vector<int> &interval) :
            start(interval[0]), end(interval[1]) {}
        int start = -1;
        int end = -1;
        bool operator<(const Interval &other) const { return end < other.end; }
    };
    std::vector<Interval> data(intervals.begin(), intervals.end());
    std::sort(data.begin(), data.end());
    int result = 0, current_end = data[0].end;
    for (int i = 1, n = static_cast<int>(data.size()); i < n; ++i)
    {
        if (data[i].start >= current_end)
            current_end = data[i].end;
        else ++result;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > intervals,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = eraseOverlapIntervals(intervals);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 3}, {3, 4}, {1, 3}}, 1, trials);
    test({{1, 2}, {1, 2}, {1, 2}}, 2, trials);
    test({{1, 2}, {2, 3}}, 0, trials);
    test({{-52, 31}, {-73, -26}, {82, 97}, {-65, -11}, {-62, -49}, {95, 99},
          {58, 95}, {-31, 49}, {66, 98}, {-63, 2}, {30, 47}, {-40, -26}}, 7, trials);
    return 0;
}


