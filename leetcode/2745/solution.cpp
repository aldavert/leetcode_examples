#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestString(int x, int y, int z)
{
    if (const int mn = std::min(x, y); x == y) return (mn * 2 + z) * 2;
    else return (mn * 2 + 1 + z) * 2;
}

// ############################################################################
// ############################################################################

void test(int x, int y, int z, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestString(x, y, z);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 5, 1, 12, trials);
    test(3, 2, 2, 14, trials);
    return 0;
}


