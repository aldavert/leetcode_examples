#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > criticalConnections(
        int n,
        std::vector<std::vector<int> > connections)
{
    constexpr static int NO_RANK = -2;
    std::vector<std::vector<int> > result, graph(n);
    
    auto compute =
        [&](auto &&self, int u, int curr_rank, std::vector<int> &&rank) -> int
    {
        if (rank[u] != NO_RANK)
            return rank[u];
        rank[u] = curr_rank;
        int min_rank = curr_rank;
        for (const int v : graph[u])
        {
            if ((rank[u] == static_cast<int>(rank.size()))
            ||  (rank[v] == curr_rank - 1))
                continue;
            int next_rank = self(self, v, curr_rank + 1, move(rank));
            if (next_rank == curr_rank + 1)
                result.push_back({u, v});
            min_rank = std::min(min_rank, next_rank);
        }
        rank[u] = static_cast<int>(rank.size());
        return min_rank;
    };
    
    for (const auto &c : connections)
    {
        graph[c[0]].push_back(c[1]);
        graph[c[1]].push_back(c[0]);
    }
    compute(compute, 0, 0, std::vector<int>(n, NO_RANK));
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    auto code = [](const std::vector<int> &v) -> int
    {
        int lo = v[0];
        int hi = v[1];
        if (hi < lo) std::swap(lo, hi);
        return ((lo & 0xFFFF) << 16) | (hi & 0xFFFF);
    };
    std::unordered_map<int, int> lut;
    for (const auto &l : left)
        lut[code(l)] += 1;
    for (const auto &r: right)
    {
        if (auto search = lut.find(code(r)); search != lut.end())
        {
            --search->second;
            if (!search->second)
                lut.erase(search);
        }
        else return false;
    }
    return lut.empty();
}

void test(int n,
          std::vector<std::vector<int> > connections,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = criticalConnections(n, connections);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{0, 1}, {1, 2}, {2, 0}, {1, 3}}, {{1, 3}}, trials);
    test(2, {{0, 1}}, {{0, 1}}, trials);
    return 0;
}


