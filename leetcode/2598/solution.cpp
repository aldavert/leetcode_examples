#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int findSmallestInteger(std::vector<int> nums, int value)
{
    std::unordered_map<int, int> count;
    for (int num : nums)
        ++count[(num % value + value) % value];
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        if (--count[i % value] < 0)
            return i;
    return static_cast<int>(nums.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int value, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findSmallestInteger(nums, value);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, -10, 7, 13, 6, 8}, 5, 4, trials);
    test({1, -10, 7, 13, 6, 8}, 7, 2, trials);
    return 0;
}


