#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long findMaximumNumber(long long k, int x)
{
    auto sumPrices = [](long num, int v) -> long
    {
        long sum_prices = 0;
        ++num;
        for (int i = 63 - std::countl_zero(static_cast<size_t>(num)) + 1; i > 0; --i)
        {
            if (i % v == 0)
            {
                const long group_size = 1L << i;
                const long half_group_size = 1L << (i - 1);
                sum_prices += num / group_size * half_group_size;
                sum_prices += std::max(0L, (num % group_size) - half_group_size);
            }
        }
        return sum_prices;
    };
    long l = 1, r = 1e15;
    while (l < r)
    {
        const long m = (l + r + 1) / 2;
        if (sumPrices(m, x) > k) r = m - 1;
        else l = m;
    }
    return l;
}

// ############################################################################
// ############################################################################

void test(long long k, int x, int solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMaximumNumber(k, x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(9, 1, 6, trials);
    test(7, 2, 9, trials);
    return 0;
}


