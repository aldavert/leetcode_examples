#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long minIncrementOperations(std::vector<int> nums, int k)
{
    long prev[3] = {};
    for (int num : nums)
    {
        const long dp = std::min({prev[0], prev[1], prev[2]}) + std::max(0, k - num);
        prev[2] = prev[1];
        prev[1] = prev[0];
        prev[0] = dp;
    }
    return std::min({prev[0], prev[1], prev[2]});
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minIncrementOperations(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 0, 0, 2}, 4, 3, trials);
    test({0, 1, 3, 3}, 5, 2, trials);
    test({1, 1, 2}, 1, 0, trials);
    return 0;
}


