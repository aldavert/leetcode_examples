#include "../common/common.hpp"
#include <fstream>
#include <sstream>
#include <map>
#include <set>

// ############################################################################
// ############################################################################

#if 1
int reversePairs([[maybe_unused]] std::vector<int> nums)
{
    struct Node
    {
        Node(void) = default;
        Node(const std::vector<int> &input)
        {
            const int n = static_cast<int>(input.size());
            std::map<int, int> histogram;
            for (int i = 0; i < n; ++i)
                ++histogram[input[i]];
            std::vector<int> values;
            std::vector<int> frequencies;
            for (auto &[val, idx] : histogram)
            {
                values.push_back(val);
                frequencies.push_back(idx);
            }
            histogram.clear();
            const int m = static_cast<int>(values.size());
            generate(values, frequencies, 0, m - 1);
        }
        void generate(std::vector<int> &values,
                      std::vector<int> &frequencies,
                      int l,
                      int r)
        {
            int mid = (l + r) / 2;
            value = values[mid];
            if (l != mid)
            {
                left = new Node();
                left->generate(values, frequencies, l, mid - 1);
            }
            if (r != mid)
            {
                right = new Node();
                right->generate(values, frequencies, mid + 1, r);
            }
            number_of_elements = frequencies[mid];
            accumulated = ((left != nullptr)?left->accumulated:0)
                               + ((right != nullptr)?right->accumulated:0)
                               + number_of_elements;
        }
        ~Node(void) { delete left; delete right; }
        void erase(int v)
        {
            --accumulated;
            if (v == value) --number_of_elements;
            else if (v < value) left->erase(v);
            else right->erase(v);
        }
        int lessEqual(int v)
        {
            if (v < value)
                return ((left)?left->lessEqual(v):0);
            else if (v == value)
                return number_of_elements
                     + ((left)?left->accumulated:0);
            else
                return number_of_elements
                     + ((left)?left->accumulated:0)
                     + ((right)?right->lessEqual(v):0);
        }
        int value = 0;
        int number_of_elements = 0;
        int accumulated = 0;
        Node * left = nullptr;
        Node * right = nullptr;
    };
    Node tree(nums);
    int result = 0;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        int threshold = nums[i] / 2 - ((nums[i] < 0) || (nums[i] % 2 == 0));
        tree.erase(nums[i]);
        result += tree.lessEqual(threshold);
    }
    return result;
}
#elif 0 // Multiset: 20980 ms
int reversePairs(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::multiset<int> ordered_values;
    int result = 0;
    for (int i = n - 1; i >= 0; --i)
    {
        auto ni = ordered_values.insert(nums[i]);
        int threshold = nums[i] / 2 - (nums[i] % 2 == 0);
        auto it = std::upper_bound(ordered_values.begin(), ni, threshold);
        result += static_cast<int>(std::distance(ordered_values.begin(), it));
    }
    return result;
}
#else // Brute force: 200ms
int reversePairs(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = 0;
    for (int i = 0; i < n; ++i)
        for (int j = i + 1; j < n; ++j)
            result += (nums[i] > 2 * nums[j]);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = reversePairs(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 3, 1}, 2, trials);
    test({-1, 1, 0, 1, -1}, 5, trials);
    test({-2, 0, -1, 0, -2}, 5, trials);
    test({-3, -1, -2, -1, -3}, 7, trials);
    test({2, 4, 3, 5, 1}, 3, trials);
    test({-1, 1, 0, 2, -2}, 5, trials);
    test({-2, 0, -1, 1, -3}, 5, trials);
    if (true)
    {
        std::ifstream file("data.txt");
        std::vector<int> testA;
        for (std::string line; std::getline(file, line);)
        {
            std::istringstream iss(line);
            int value;
            iss >> value;
            testA.push_back(value);
        }
        file.close();
        test(testA, 314'201'349, trials);
    }
    return 0;
}


