#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int semiOrderedPermutation(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int position_1 = 0, position_n = n - 1;
    for (int i = 0; i < n; ++i)
    {
        if (nums[i] == 1) position_1 = i;
        else if (nums[i] == n) position_n = i;
    }
    return position_1 + (n - 1) - position_n - (position_1 > position_n);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = semiOrderedPermutation(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 4, 3}, 2, trials);
    test({2, 4, 1, 3}, 3, trials);
    test({1, 3, 4, 2, 5}, 0, trials);
    return 0;
}


