#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool lemonadeChange(std::vector<int> bills)
{
    int fives = 0, tens = 0;
    for (int bill : bills)
    {
        if (bill == 5) ++fives;
        else if (bill == 10)
        {
            --fives;
            ++tens;
        }
        else if (tens > 0)
        {
            --fives;
            --tens;
        }
        else fives -= 3;
        if (fives < 0) return false;
    }
    return true;
}
#else
bool lemonadeChange(std::vector<int> bills)
{
    int fives = 0, tens = 0;
    for (int bill : bills)
    {
        if (bill == 5) ++fives;
        else if (bill == 10)
        {
            if (fives == 0) return false;
            --fives;
            ++tens;
        }
        else if (bill == 20)
        {
            if (tens > 0)
            {
                if (fives == 0) return false;
                --fives;
                --tens;
            }
            else
            {
                if (fives < 3) return false;
                fives -= 3;
            }
        }
    }
    return true;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> bills, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = lemonadeChange(bills);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({ 5,  5,  5, 10, 20},  true, trials);
    test({ 5,  5, 10, 10, 20}, false, trials);
    test({5, 5, 5, 5, 20, 20, 5, 5, 20, 5}, false, trials);
    test({ 5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 
          10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  
           5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 
          20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  
           5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 
          10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  
           5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 
          20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  
           5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 
          10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  
           5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 
          20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  
           5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 
          10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  
           5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 
          20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  
           5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 
          10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  
           5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 
          20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  
           5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 
          10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  
           5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 20,  5, 10,  5, 
          20,  5, 10,  5, 20,  5, 10,  5, 20}, true, trials);
    return 0;
}

