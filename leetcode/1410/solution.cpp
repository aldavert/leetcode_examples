#include "../common/common.hpp"
#include <map>

// ############################################################################
// ############################################################################

std::string entityParser(std::string text)
{
    const int n = static_cast<int>(text.size());
    std::map<std::string, char> entity_map = {
        {"&quot;" , '\"'},
        {"&apos;",  '\''}, 
        {"&amp;" , '&'}, 
        {"&gt;" , '>'}, 
        {"&lt;" , '<'}, 
        {"&frasl;" , '/'}
    };
    std::map<std::string, char>::iterator it;
    int slow = 0;
    
    for (int fast = 0, last_and = 0; fast < n; ++fast, ++slow)
    {
        text[slow] = text[fast];
        if (text[slow] == '&') last_and = slow;
        if (text[slow] == ';')
        {
            if ((it = entity_map.find(text.substr(last_and, slow - last_and + 1)))
            !=  entity_map.end())
            {
                slow = last_and;
                text[slow] = it->second;
            }
            last_and = slow + 1;
        }
    }
    text.resize(slow);
    return text;
}

// ############################################################################
// ############################################################################

void test(std::string text, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = entityParser(text);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("&amp; is an HTML entity but &ambassador; is not.",
         "& is an HTML entity but &ambassador; is not.", trials);
    test("and I quote: &quot;...&quot;", "and I quote: \"...\"", trials);
    return 0;
}


