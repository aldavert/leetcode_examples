#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

long long maximumCoins(std::vector<std::vector<int> > coins, int k)
{
    auto slide = [&](void) -> long long
    {
        long result = 0;
        for (long j = 0, window_sum = 0; const auto &coin : coins)
        {
            const int right_boundary = coin[0] + k;
            while ((j + 1 < static_cast<long>(coins.size()))
               &&  (coins[j + 1][0] < right_boundary))
            {
                window_sum += static_cast<long>(coins[j][1] - coins[j][0] + 1)
                            * coins[j][2];
                ++j;
            }
            long last = 0;
            if ((j < static_cast<long>(coins.size())) && (coins[j][0] < right_boundary))
                last = static_cast<long>(std::min(right_boundary - 1, coins[j][1])
                                         - coins[j][0] + 1) * coins[j][2];
            result = std::max(result, window_sum + last);
            window_sum -= static_cast<long>(coin[1] - coin[0] + 1) * coin[2];
        }
        return result;
    };
    std::sort(coins.begin(), coins.end());
    long long positive_result = slide();
    for (auto &coin : coins)
        coin[0] = -std::exchange(coin[1], -coin[0]);
    std::sort(coins.begin(), coins.end());
    return std::max(positive_result, slide());
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > coins,
          int k,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumCoins(coins, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{8, 10, 1}, {1, 3, 2}, {5, 6, 4}}, 4, 10, trials);
    test({{1, 10, 3}}, 2, 6, trials);
    return 0;
}


