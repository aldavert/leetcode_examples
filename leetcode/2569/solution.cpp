#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

std::vector<long long> handleQuery(std::vector<int> nums1,
                                   std::vector<int> nums2,
                                   std::vector<std::vector<int> > queries)
{
    struct LazySegmentTree
    {
    public:
        LazySegmentTree(const std::vector<int> &nums) :
            m_tree(4 * nums.size()), m_lazy(4 * nums.size())
        {
            build(nums, 0, 0, static_cast<int>(nums.size()) - 1);
        }
        void update(int i, int start, int end, int l, int r)
        {
            if (m_lazy[i]) propogate(i, start, end);
            if ((start > r) || (end < l)) return;
            if ((start >= l) && (end <= r))
            {
                flip(i, start, end);
                return;
            }
            const int mid = (start + end) / 2;
            update(i * 2 + 1, start, mid, l, r);
            update(i * 2 + 2, mid + 1, end, l, r);
            m_tree[i] = m_tree[2 * i + 1] + m_tree[2 * i + 2];
        }
        inline int sum() const { return m_tree[0]; }
    private:
        std::vector<int> m_tree;
        std::vector<bool> m_lazy;

        void build(const std::vector<int> &nums, int i, int start, int end)
        {
            if (start == end)
            {
                m_tree[i] = nums[start];
                return;
            }
            const int mid = (start + end) / 2;
            build(nums, 2 * i + 1, start, mid);
            build(nums, 2 * i + 2, mid + 1, end);
            m_tree[i] = m_tree[2 * i + 1] + m_tree[2 * i + 2];
        }
        void propogate(int i, int start, int end)
        {
            flip(i, start, end);
            m_lazy[i] = false;
        }
        void flip(int i, int start, int end)
        {
            m_tree[i] = (end - start + 1) - m_tree[i];
            if (start != end)
            {
                m_lazy[2 * i + 1] = !m_lazy[2 * i + 1];
                m_lazy[2 * i + 2] = !m_lazy[2 * i + 2];
            }
        }
    };
    
    std::vector<long long> result;
    LazySegmentTree tree(nums1);
    long long sum_nums2 = std::accumulate(nums2.begin(), nums2.end(), 0LL);
    for (const auto &query : queries)
    {
        if      (query[0] == 1)
            tree.update(0, 0, static_cast<int>(nums1.size()) - 1, query[1], query[2]);
        else if (query[0] == 2)
            sum_nums2 += static_cast<long long>(query[1]) * tree.sum();
        else result.push_back(sum_nums2);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          std::vector<std::vector<int> > queries,
          std::vector<long long> solution,
          unsigned int trials = 1)
{
    std::vector<long long> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = handleQuery(nums1, nums2, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 1}, {0, 0, 0}, {{1, 1, 1}, {2, 1, 0}, {3, 0, 0}}, {3}, trials);
    test({1}, {5}, {{2, 0, 0}, {3, 0, 0}}, {5}, trials);
    return 0;
}


