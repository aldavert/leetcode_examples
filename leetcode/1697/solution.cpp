#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<bool>
distanceLimitedPathsExist(int n,
                          std::vector<std::vector<int> > edgeList,
                          std::vector<std::vector<int> > queries)
{
    const int n_queries = static_cast<int>(queries.size());
    const int n_edge = static_cast<int>(edgeList.size());
    std::vector<bool> result(queries.size());
    std::vector<int> id(n), rank(n);
    for (int i = 0; i < n; ++i) id[i] = i;
    auto find = [&](auto &&self, int idx) -> int
    {
        return (id[idx] == idx)?idx:id[idx] = self(self, id[idx]);
    };
    auto unionByRank = [&](int u, int v) -> void
    {
        const int i = find(find, u), j = find(find, v);
        if (i == j) return;
        if      (rank[i] < rank[j]) id[i] = id[j];
        else if (rank[i] > rank[j]) id[j] = id[i];
        else                        id[i] = id[j], ++rank[j];
    };
    for (int i = 0; i < n_queries; ++i)
        queries[i].push_back(i);
    std::sort(queries.begin(), queries.end(),
              [](const auto& a, const auto& b) { return a[2] < b[2]; });
    std::sort(edgeList.begin(), edgeList.end(),
              [](const auto& a, const auto& b) { return a[2] < b[2]; });
    for (int i = 0; const auto &query : queries)
    {
        for (; (i < n_edge) && (edgeList[i][2] < query[2]); ++i)
            unionByRank(edgeList[i][0], edgeList[i][1]);
        if (find(find, query[0]) == find(find, query[1]))
            result[query[3]] = true;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edgeList,
          std::vector<std::vector<int> > queries,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = distanceLimitedPathsExist(n, edgeList, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{0, 1, 2}, {1, 2, 4}, {2, 0, 8}, {1, 0, 16}},
            {{0, 1, 2}, {0, 2, 5}}, {false, true}, trials);
    test(5, {{0, 1, 10}, {1, 2, 5}, {2, 3, 9}, {3, 4, 13}},
            {{0, 4, 14}, {1, 4, 13}}, {true, false}, trials);
    return 0;
}


