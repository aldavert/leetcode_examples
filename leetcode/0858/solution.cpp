#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int mirrorReflection(int p, int q)
{
    int m = 1, n = 1;
    while (m * p != n * q)
      m = ++n * q / p;
    if (n % 2 == 0) return 2;
    if (m % 2 == 0) return 0;
    if (m % 2 == 1) return 1;
    return -1;
}

// ############################################################################
// ############################################################################

void test(int p, int q, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = mirrorReflection(p, q);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 1, 2, trials);
    test(3, 1, 1, trials);
    return 0;
}


