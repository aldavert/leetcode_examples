#include "../common/common.hpp"
#include <cstring>
#include <queue>

// ############################################################################
// ############################################################################

int containVirus(std::vector<std::vector<int> > isInfected)
{
    struct Region
    {
        int y = -1;
        int x = -1;
        int id = -1;
        int growth = 0;
        int barriers = 0;
        bool active = false;
    };
    const int h = static_cast<int>(isInfected.size());
    const int w = static_cast<int>(isInfected.front().size());
    int labels[51][51], labels_grow[51][51];
    std::vector<Region> regions;
    auto detectRegions = [&](void) -> void
    {
        std::memset(labels, -1, sizeof(labels));
        for (int y = 0; y < h; ++y)
        {
            for (int x = 0; x < w; ++x)
            {
                if (isInfected[y][x] && (labels[y][x] == -1))
                {
                    const int status = isInfected[y][x];
                    std::queue<std::tuple<int, int> > q;
                    int id = static_cast<int>(regions.size());
                    regions.push_back({y, x, id, 0, 0, status == 1});
                    q.push({y, x});
                    while (!q.empty())
                    {
                        auto [j, i] = q.front();
                        q.pop();
                        if (labels[j][i] != -1) continue;
                        labels[j][i] = id;
                        if ((j     > 0) && (isInfected[j - 1][i]) == status)
                            q.push({j - 1, i});
                        if ((j + 1 < h) && (isInfected[j + 1][i]) == status)
                            q.push({j + 1, i});
                        if ((i     > 0) && (isInfected[j][i - 1]) == status)
                            q.push({j, i - 1});
                        if ((i + 1 < w) && (isInfected[j][i + 1]) == status)
                            q.push({j, i + 1});
                    }
                }
            }
        }
    };
    auto canGrow = [&](int y, int x, int id) -> bool
    {
        return (labels[y][x] == -1) || (labels[y][x] == id);
    };
    auto potentialGrowth = [&](void) -> void
    {
        for (size_t r = 0; r < regions.size(); ++r)
        {
            if (!regions[r].active) continue;
            const int id = regions[r].id;
            regions[r].growth = 0;
            regions[r].barriers = 0;
            std::memset(labels_grow, -1, sizeof(labels_grow));
            std::queue<std::tuple<int, int> > q;
            q.push({regions[r].y, regions[r].x});
            while (!q.empty())
            {
                auto [y, x] = q.front();
                q.pop();
                if (labels_grow[y][x] == id)
                {
                    regions[r].barriers += (labels[y][x] == -1);
                    continue;
                }
                if (labels[y][x] != id)
                {
                    regions[r].barriers += (labels[y][x] == -1);
                    regions[r].growth += (labels_grow[y][x] == -1);
                    labels_grow[y][x] = id;
                    continue;
                }
                labels_grow[y][x] = id;
                if ((y     > 0) && canGrow(y - 1, x, id)) q.push({y - 1, x});
                if ((y + 1 < h) && canGrow(y + 1, x, id)) q.push({y + 1, x});
                if ((x     > 0) && canGrow(y, x - 1, id)) q.push({y, x - 1});
                if ((x + 1 < w) && canGrow(y, x + 1, id)) q.push({y, x + 1});
            }
        }
    };
    auto grow = [&](void) -> void
    {
        for (size_t r = 0; r < regions.size(); ++r)
        {
            const int id = regions[r].id;
            std::memset(labels_grow, -1, sizeof(labels_grow));
            std::queue<std::tuple<int, int> > q;
            q.push({regions[r].y, regions[r].x});
            while (!q.empty())
            {
                auto [y, x] = q.front();
                q.pop();
                if (labels_grow[y][x] == id)
                {
                    continue;
                }
                if (labels[y][x] != id)
                {
                    labels_grow[y][x] = id;
                    if (regions[r].active) isInfected[y][x] = 1;
                    continue;
                }
                if (!regions[r].active) isInfected[y][x] = 2;
                labels_grow[y][x] = id;
                if ((y     > 0) && canGrow(y - 1, x, id)) q.push({y - 1, x});
                if ((y + 1 < h) && canGrow(y + 1, x, id)) q.push({y + 1, x});
                if ((x     > 0) && canGrow(y, x - 1, id)) q.push({y, x - 1});
                if ((x + 1 < w) && canGrow(y, x + 1, id)) q.push({y, x + 1});
            }
        }
    };
    int result = 0;
    while (true)
    {
        regions.clear();
        detectRegions();
        potentialGrowth();
        const size_t n = regions.size();
        size_t max_index = regions.size();
        int max_growth = 0;
        for (size_t i = 0; i < n; ++i)
        {
            if (regions[i].growth > max_growth)
            {
                max_growth = regions[i].growth;
                max_index = i;
            }
        }
        if (max_index == n) break;
        result += regions[max_index].barriers;
        regions[max_index].active = false;
        grow();
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > isInfected,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = containVirus(isInfected);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    // ---------------------------------------------
    test({{0, 1, 0, 0, 0, 0, 0, 1},
          {0, 1, 0, 0, 0, 0, 0, 1},
          {0, 0, 0, 0, 0, 0, 0, 1},
          {0, 0, 0, 0, 0, 0, 0, 0}}, 10, trials);
    // ---------------------------------------------
    test({{1, 1, 1},
          {1, 0, 1},
          {1, 1, 1}}, 4, trials);
    // ---------------------------------------------
    test({{1, 1, 1, 0, 0, 0, 0, 0, 0},
          {1, 0, 1, 0, 1, 1, 1, 1, 1},
          {1, 1, 1, 0, 0, 0, 0, 0, 0}}, 13, trials);
    // ---------------------------------------------
    test({{0, 1, 0, 0, 0, 0, 0, 1},
          {0, 1, 0, 0, 0, 0, 0, 1},
          {0, 0, 0, 0, 0, 0, 0, 1},
          {0, 0, 0, 0, 0, 1, 1, 0}}, 11, trials);
    // ---------------------------------------------
    test({{0, 1, 0, 0, 0, 0, 0, 1},
          {0, 1, 0, 0, 0, 0, 0, 1},
          {0, 0, 0, 0, 0, 0, 0, 1},
          {0, 0, 0, 1, 1, 1, 1, 0}}, 6 + 7 + 3, trials);
    // ---------------------------------------------
    test({{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
          {1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 0, 1, 0, 0, 0, 1, 0, 0, 0},
          {0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
          {0, 0, 0, 0, 1, 0, 1, 0, 0, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}, 56, trials);
    return 0;
}


