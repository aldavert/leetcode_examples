#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int missingInteger(std::vector<int> nums)
{
    int present[52] = {};
    int sum = 0;
    for (int previous = nums.front() - 1, valid = 1; int value : nums)
    {
        valid = valid && (value - previous == 1);
        sum += valid * value;
        previous = value;
        present[value] = 1;
    }
    while ((sum < 52) && present[sum]) ++sum;
    return sum;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = missingInteger(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 2, 5}, 6, trials);
    test({3, 4, 5, 1, 12, 14, 13}, 15, trials);
    return 0;
}


