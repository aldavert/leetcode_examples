#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int triangularSum(std::vector<int> nums)
{
    for (int sz = static_cast<int>(nums.size()); sz > 0; --sz)
        for (int i = 0; i + 1 < sz; ++i)
            nums[i] = (nums[i] + nums[i + 1]) % 10;
    return nums[0];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = triangularSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 8, trials);
    test({5}, 5, trials);
    return 0;
}


