# Find Triangular Sum of an Array

You are given a **0-indexed** integer array `nums`, where `nums[i]` is a digit between `0` and `9` **(inclusive)**.

The **triangular sum** of `nums` is the value of the only element present in `nums` after the following process terminates:

1. Let `nums` comprise of `n` elements. If `n == 1`, **end** the process. Otherwise, **create** a new **0-indexed** integer array `newNums` of length `n - 1`.
2. For each index `i`, where `0 <= i < n - 1`, **assign** the value of `newNums[i]` as `(nums[i] + nums[i + 1]) % 10`, where `%` denotes modulo operator.
3. **Replace** the array `nums` with `newNums`.
4. **Repeat** the entire process starting from step `1`.

Return *the triangular sum of* `nums`.

#### Example 1:
> ```mermaid 
> graph TD;
> R11[1]-->R21[3]
> R12[2]-->R21
> R12-->R22[5]
> R13[3]-->R22
> R13-->R23[7]
> R14[4]-->R23
> R14-->R24[9]
> R15[5]-->R24
> R21-->R31[8]
> R22-->R31
> R22-->R32[2]
> R23-->R32
> R23-->R33[6]
> R24-->R33
> R31-->R41[0]
> R32-->R41
> R32-->R42[8]
> R33-->R42
> R41-->R51[8]
> R42-->R51
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `nums = [1, 2, 3, 4, 5]`  
> *Output:* `8`  
> *Explanation:* The above diagram depicts the process from which we obtain the triangular sum of the array.

#### Example 2:
> *Input:* `nums = [5]`  
> *Output:* `5`  
> *Explanation:* Since there is only one element in `nums`, the triangular sum is the value of that element itself.

#### Constraints:
- `1 <= nums.length <= 1000`
- `0 <= nums[i] <= 9`


