#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long
numberOfPowerfulInt(long long start, long long finish, int limit, std::string s)
{
    const std::string a_original = std::to_string(start), b = std::to_string(finish);
    const std::string a = std::string(b.size() - a_original.size(), '0') + a_original;
    std::vector<long> mem[2][2] = {
        { std::vector<long>(b.size(), -1), std::vector<long>(b.size(), -1) }, 
        { std::vector<long>(b.size(), -1), std::vector<long>(b.size(), -1) }};
    auto count = [&](auto &&self, int i, bool is_tight1, bool is_tight2) -> long
    {
        if (i + s.size() == b.size())
        {
            const std::string a_min_suffix = is_tight1
                                           ? std::string(a.end() - s.size(), a.end())
                                           : std::string(s.size(), '0');
            const std::string b_max_suffix = is_tight2
                                           ? std::string(b.end() - s.size(), b.end())
                                           : std::string(s.size(), '9');
            const long suffix = std::stoll(s);
            return (std::stoll(a_min_suffix) <= suffix)
                && (suffix <= std::stoll(b_max_suffix));
        }
        if (mem[is_tight1][is_tight2][i] != -1) return mem[is_tight1][is_tight2][i];
        long result = 0;
        const int min_digit = (is_tight1)?a[i] - '0':0,
                  max_digit = (is_tight2)?b[i] - '0':9;
        for (int d = min_digit; d <= max_digit; ++d)
            if (d <= limit)
                result += self(self, i + 1, is_tight1 && (d == min_digit),
                                            is_tight2 && (d == max_digit));
        return mem[is_tight1][is_tight2][i] = result;
    };
    return count(count, 0, true, true);
}

// ############################################################################
// ############################################################################

void test(long long start,
          long long finish,
          int limit,
          std::string s,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfPowerfulInt(start, finish, limit, s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 6000, 4, "124", 5, trials);
    test(15, 215, 6, "10", 2, trials);
    test(1000, 2000, 4, "3000", 0, trials);
    return 0;
}


