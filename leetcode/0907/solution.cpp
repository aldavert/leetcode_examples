#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 1
int sumSubarrayMins(std::vector<int> arr)
{
    constexpr int LIMIT = 1'000'000'007;
    const size_t n = arr.size();
    std::vector<size_t> prev(n, -1), next(n, n);
    std::stack<size_t> s;
    for (size_t i = 0; i < n; ++i)
    {
        while (!s.empty() && (arr[s.top()] > arr[i]))
            next[s.top()] = i, s.pop();
        if (!s.empty()) prev[i] = s.top();
        s.push(i);
    }
    long result = 0;
    for (size_t i = 0; i < n; ++i)
    {
        result += static_cast<long>(arr[i]) * (i - prev[i]) * (next[i] - i);
        result %= LIMIT;
    }
    return static_cast<int>(result);
}
#else
int sumSubarrayMins(std::vector<int> arr)
{
    constexpr int LIMIT = 1'000'000'007;
    int result = std::accumulate(arr.begin(), arr.end(), 0);
    for (size_t n = arr.size(); n > 0; --n)
        for (size_t i = 0; i < n - 1; ++i)
            result = (result + (arr[i] = std::min(arr[i], arr[i + 1]))) % LIMIT;
    return result;
}
#endif


// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumSubarrayMins(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1, 2, 4}, 17, trials);
    test({11, 81, 94, 43, 3}, 444, trials);
    return 0;
}

