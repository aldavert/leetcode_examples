#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool sumGame(std::string num)
{
    auto getExpectation = [](char c) -> double
    {
        return (c == '?')?4.5:c - '0';
    };
    const int n = static_cast<int>(num.size());
    
    double result = 0.0;
    for (int i = 0; i < n / 2; ++i) result += getExpectation(num[i]);
    for (int i = n / 2; i < n; ++i) result -= getExpectation(num[i]);
    return result != 0.0;
}

// ############################################################################
// ############################################################################

void test(std::string num, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumGame(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("5023", false, trials);
    test("25??", true, trials);
    test("?3295???", false, trials);
    return 0;
}


