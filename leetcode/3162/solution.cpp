#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfPairs(std::vector<int> nums1,
                  std::vector<int> nums2,
                  int k)
{
    int result = 0;
    for (const int num1 : nums1)
        for (const int num2 : nums2)
            result += (num1 % (num2 * k) == 0);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfPairs(nums1, nums2, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 4}, {1, 3, 4}, 1, 5, trials);
    test({1, 2, 4, 12}, {2, 4}, 3, 2, trials);
    return 0;
}


