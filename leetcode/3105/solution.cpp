#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestMonotonicSubarray(std::vector<int> nums)
{
    int increasing = 1, decreasing = 1, result = 1;
    for (size_t i = 1; i < nums.size(); ++i)
    {
        increasing = increasing * (nums[i] > nums[i - 1]) + 1;
        decreasing = decreasing * (nums[i] < nums[i - 1]) + 1;
        result = std::max(result, std::max(increasing, decreasing));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestMonotonicSubarray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 3, 3, 2}, 2, trials);
    test({3, 3, 3, 3}, 1, trials);
    test({3, 2, 1}, 3, trials);
    test({1, 10, 10}, 2, trials);
    return 0;
}


