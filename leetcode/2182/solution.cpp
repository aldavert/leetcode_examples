#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string repeatLimitedString(std::string s, int repeatLimit)
{
    std::string result;
    int count[26] = {};
    auto shouldAddOne = [&](void) -> bool
    {
        for (int i = 25; i >= 0; --i)
            if (count[i])
                return result.back() == static_cast<char>('a' + i);
        return false;
    };
    auto getLargestChar = [&](void) -> int
    {
        for (int i = 25; i >= 0; --i)
            if (count[i]
            &&  (result.empty() || (result.back() != static_cast<char>('a' + i))))
                return i;
        return -1;
    };
    
    for (char c : s) ++count[c - 'a'];
    while (true)
    {
        const bool add_one = !result.empty() && shouldAddOne();
        const int i = getLargestChar();
        if (i == -1) break;
        const int repeats = (add_one)?1:std::min(count[i], repeatLimit);
        result += std::string(repeats, static_cast<char>('a' + i));
        count[i] -= repeats;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int repeatLimit, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = repeatLimitedString(s, repeatLimit);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("cczazcc", 3, "zzcccac", trials);
    test("aababab", 2, "bbabaa", trials);
    return 0;
}


