#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumTastiness(std::vector<int> price, int k)
{
    std::sort(price.begin(), price.end());
    int l = 0, r = *std::max_element(price.begin(), price.end())
                 - *std::min_element(price.begin(), price.end()) + 1;
    while (l < r)
    {
        const int m = (l + r) / 2;
        int baskets = 0, prev_price = -m;
        for (int p : price)
            if (p >= prev_price + m)
                prev_price = p,
                ++baskets;
        if (baskets >= k) l = m + 1;
        else r = m;
    }
    return l - 1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> price, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumTastiness(price, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({13, 5, 1, 8, 21, 2}, 3, 8, trials);
    test({1, 3, 1}, 2, 2, trials);
    test({7, 7, 7, 7}, 2, 0, trials);
    return 0;
}


