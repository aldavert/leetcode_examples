#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> nums)
{
    if (nums.size() <= 1) return 0;
    int result = 0;
    for (int previous = nums[0] - 1; int n : nums)
    {
        if (int increase = previous - n + 1; increase > 0)
        {
            result += increase;
            previous = n + increase;
        }
        else previous = n;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 1}, 3, trials);
    test({1, 5, 2, 4, 1}, 14, trials);
    test({8}, 0, trials);
    return 0;
}


