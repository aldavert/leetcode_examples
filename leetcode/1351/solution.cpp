#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countNegatives(std::vector<std::vector<int> > grid)
{
    const int ncols = static_cast<int>(grid[0].size());
    int result = 0;
    for (int idx = ncols - 1; const auto &row : grid)
    {
        while ((idx >= 0) && (row[idx] < 0)) --idx;
        result += ncols - idx - 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countNegatives(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{4, 3, 2, -1}, {3, 2, 1, -1}, {1, 1, -1, -2}, {-1, -1, -2, -3}}, 8, trials);
    test({{3, 2}, {1, 0}}, 0, trials);
    return 0;
}


