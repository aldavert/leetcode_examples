#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int dayOfYear(std::string date)
{
    //// MONTH_LENGTH: {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    constexpr int month_accum[] =
                        {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};
    int year = 1000 * (date[0] - '0') + 100 * (date[1] - '0')
             +   10 * (date[2] - '0') +   1 * (date[3] - '0');
    int month =  10 * (date[5] - '0') +   1 * (date[6] - '0');
    int day =    10 * (date[8] - '0') +   1 * (date[9] - '0');
    bool is_leap = (year % 4 == 0) && !((year % 100 == 0) && (year % 400 != 0));
    return month_accum[month - 1] + day + is_leap * (month > 2);
}

// ############################################################################
// ############################################################################

void test(std::string date, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = dayOfYear(date);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("2019-01-09", 9, trials);
    test("2019-02-10", 41, trials);
    return 0;
}


