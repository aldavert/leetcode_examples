#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int getMinDistance(std::vector<int> nums, int target, int start)
{
    const int n = static_cast<int>(nums.size());
    if (nums[start] == target) return 0;
    int l = start - 1, r = start + 1;
    for (; (l >= 0) && (r < n); --l, ++r)
    {
        if (nums[l] == target) return start - l;
        if (nums[r] == target) return r - start;
    }
    for (; l >= 0; --l)
        if (nums[l] == target) return start - l;
    for (; r < n; ++r)
        if (nums[r] == target) return r - start;
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int target,
          int start,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getMinDistance(nums, target, start);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 5, 3, 1, trials);
    test({1}, 1, 0, 0, trials);
    test({1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 1, 0, 0, trials);
    return 0;
}


