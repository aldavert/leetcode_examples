#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxDifference(std::string s)
{
    int histogram[26] = {},
        odd_frequency = 0,
        even_frequency = static_cast<int>(s.size());
    for (char letter : s)
        ++histogram[letter - 'a'];
    for (int i = 0; i < 26; ++i)
    {
        if (histogram[i] & 1)
            odd_frequency = std::max(odd_frequency, histogram[i]);
        else if (histogram[i] > 0)
            even_frequency = std::min(even_frequency, histogram[i]);
    }
    return odd_frequency - even_frequency;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxDifference(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aaaaabbc", 3, trials);
    test("abcabcab", 1, trials);
    return 0;
}


