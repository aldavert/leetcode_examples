#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int maxDistToClosest(std::vector<int> seats)
{
    const int n = static_cast<int>(seats.size());
    int result = 0, begin, end;
    for (begin = -1, end = 0; end < n; ++end)
    {
        if (seats[end])
        {
            if (begin == -1) [[unlikely]] result = end;
            else result = std::max(result, (end - begin) / 2);
            begin = end;
        }
    }
    return std::max(result, end - begin - 1);
}
#else
int maxDistToClosest(std::vector<int> seats)
{
    const int n = static_cast<int>(seats.size());
    int result = 0, begin, end;
    for (begin = -1, end = 0; end < n; ++end)
    {
        if (seats[end])
        {
            result = end;
            begin = end;
            break;
        }
    }
    for (; end < n; ++end)
    {
        if (seats[end])
        {
            result = std::max(result, (end - begin) / 2);
            begin = end;
        }
    }
    return std::max(result, end - begin - 1);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> seats, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxDistToClosest(seats);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 0, 0, 1, 0, 1}, 2, trials);
    test({1, 0, 0, 0}, 3, trials);
    test({0, 1, 1, 1, 0, 0, 1, 0, 0}, 2, trials);
    test({1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, 5, trials);
    test({0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1}, 3, trials);
    test({0, 1}, 1, trials);
    return 0;
}


