#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findComplement(int num)
{
    constexpr int ONE = 1 << 30;
    int result = 0, bits = 31;
    for (; num; --bits)
    {
        result >>= 1;
        if ((num & 1) == 0)
            result |= ONE;
        num >>= 1;
    }
    return result >> bits;
}

// ############################################################################
// ############################################################################

void test(int num, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findComplement(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 2, trials);      //   5: 101 -> 010 (2)
    test(175, 80, trials);   // 175: 10101111 -> 01010000 (80)
    test(1, 0, trials);      //   1: 1 -> 0 (0)
    return 0;
}


