#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

#if 1
long long kthLargestLevelSum(TreeNode* root, int k)
{
    std::vector<long long> levels_sum;
    std::queue<TreeNode *> q;
    q.push(root);
    while (!q.empty())
    {
        long long current_sum = 0;
        for (int sz = static_cast<int>(q.size()); sz; --sz)
        {
            TreeNode * node = q.front();
            q.pop();
            current_sum += node->val;
            if (node->left ) q.push(node->left );
            if (node->right) q.push(node->right);
        }
        levels_sum.push_back(current_sum);
    }
    int n = static_cast<int>(levels_sum.size());
    if (k > n) return -1;
    std::sort(levels_sum.begin(), levels_sum.end());
    return levels_sum[n - k];
}
#else
long long kthLargestLevelSum(TreeNode* root, int k)
{
    std::vector<long> level_sums;
    auto dfs = [&](auto &&self, TreeNode * node, int level) -> void
    {
        if (node == nullptr) return;
        if (static_cast<int>(level_sums.size()) == level)
        level_sums.push_back(0);
        level_sums[level] += node->val;
        self(self, node->left, level + 1);
        self(self, node->right, level + 1);
    };
    dfs(dfs, root, 0);
    if (static_cast<int>(level_sums.size()) < k) return -1;
    std::nth_element(level_sums.begin(), level_sums.begin() + k - 1,
                     level_sums.end(), std::greater<>());
    return level_sums[k - 1];
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = kthLargestLevelSum(root, k);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 8, 9, 2, 1, 3, 7, 4, 6}, 2, 13, trials);
    test({1, 2, null, 3}, 1, 3, trials);
    return 0;
}


