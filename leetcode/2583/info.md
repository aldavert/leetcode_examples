# Kth Largest Sum in a Binary Tree

You are given the `root` of a binary tree and a positive integer `k`.

The **level sum** in the tree is the sum of the values of the nodes that are on the **same** level.

Return *the* `k^{th}` ***largest*** *level sum in the tree (not necessarily distinct).* If there are fewer than `k` levels in the tree, return `-1`.

**Note** that two nodes are on the same level if they have the same distance from the root.

#### Example 1:
> ```mermaid 
> graph TD;
> A((5))---B((8))
> A---C((9))
> B---D((2))
> B---E((1))
> C---F((3))
> C---G((7))
> D---H((4))
> D---I((6))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [5, 8, 9, 2, 1, 3, 7, 4, 6], k = 2`  
> *Output:* `13`  
> *Explanation:* The level sums are the following:
> - Level `1`: `5`.
> - Level `2`: `8 + 9 = 17`.
> - Level `3`: `2 + 1 + 3 + 7 = 13`.
> - Level `4`: `4 + 6 = 10`.
> 
> The 2nd largest level sum is `13`.

#### Example 2:
> ```mermaid 
> graph TD;
> A((1))---B((2))
> A---EMPTY1(( ))
> B---C((3))
> B---EMPTY2(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class EMPTY1,EMPTY2 empty;
> linkStyle 1,3 stroke-width:0px;
> ```
> *Input:* `root = [1, 2, null, 3], k = 1`  
> *Output:* `3`  
> *Explanation:* The largest level sum is `3`.

#### Constraints:
- The number of nodes in the tree is `n`.
- `2 <= n <= 10^5`
- `1 <= Node.val <= 10^6`
- `1 <= k <= n`


