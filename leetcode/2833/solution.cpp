#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int furthestDistanceFromOrigin(std::string moves)
{
    int difference = 0, blank = 0;
    for (char letter : moves)
        difference += (letter == 'L') - (letter == 'R'),
        blank += (letter == '_');
    return std::abs(difference) + blank;
}

// ############################################################################
// ############################################################################

void test(std::string moves, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = furthestDistanceFromOrigin(moves);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("L_RL__R", 3, trials);
    test("_R__LL_", 5, trials);
    test("_______", 7, trials);
    return 0;
}


