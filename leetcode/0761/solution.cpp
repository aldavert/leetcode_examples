#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string makeLargestSpecial(std::string s)
{
    std::vector<std::string> specials;
    int count = 0;
    for (size_t i = 0, j = 0, n = s.size(); j < n; ++j)
    {
        count += 2 * (s[j] == '1') - 1;
        if (count == 0)
        {
            const std::string &inner = s.substr(i + 1, j - i - 1);
            specials.push_back('1' + makeLargestSpecial(inner) + '0');
            i = j + 1;
        }
    }
    std::sort(specials.begin(), specials.end(), std::greater<>());
    std::string result;
    for (const auto &value : specials)
        result += value;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = makeLargestSpecial(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("11011000", "11100100", trials);
    test("10", "10", trials);
    return 0;
}


