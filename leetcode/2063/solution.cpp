#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
long long countVowels(std::string word)
{
    auto isVowel = [](char c) -> bool
    {
        return (c == 'a') || (c == 'e') || (c == 'i') || (c == 'o') || (c == 'u');
    };
    long long result = 0;
    for (int i = 0, n = static_cast<int>(word.size()); i < n; ++i)
        if (isVowel(word[i]))
            result += (i + 1) * (n - i); 
    return result;
}
#else
long long countVowels(std::string word)
{
    constexpr std::string_view VOWELS = "aeiou";
    auto isVowel = [&](char c) -> bool
    {
        return VOWELS.find(c) != std::string_view::npos;
    };
    std::vector<long> dp(word.size() + 1);
    
    for (size_t i = 1; i <= word.length(); ++i)
    {
        dp[i] = dp[i - 1];
        if (isVowel(word[i - 1]))
            dp[i] += i;
    }
    return std::accumulate(dp.begin(), dp.end(), 0L);
}
#endif

// ############################################################################
// ############################################################################

void test(std::string word, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countVowels(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aba", 6, trials);
    test("abc", 3, trials);
    test("ltcd", 0, trials);
    return 0;
}


