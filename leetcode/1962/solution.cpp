#include "../common/common.hpp"
#include <queue>
#include <numeric>

// ############################################################################
// ############################################################################

int minStoneSum(std::vector<int> piles, int k)
{
    std::priority_queue<int> queue(piles.begin(), piles.end());
    int result = std::accumulate(piles.begin(), piles.end(), 0);
    for (int i = 0; i < k; ++i)
    {
        int value = queue.top();
        queue.pop();
        queue.push(value - value / 2);
        result -= value / 2;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> piles, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minStoneSum(piles, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 9}, 2, 12, trials);
    test({4, 3, 6, 7}, 3, 12, trials);
    return 0;
}


