#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numWays(std::string s)
{
    constexpr int MOD = 1'000'000'007;
    const long n = static_cast<long>(s.size());
    const int number_of_ones = static_cast<int>(std::count(s.begin(), s.end(), '1'));
    if (number_of_ones % 3 != 0)
        return 0;
    if (number_of_ones == 0)
        return static_cast<int>((n - 1) * (n - 2) / 2 % MOD);
    long s1_end = -1, s2_start = -1, s2_end = -1, s3_start = -1;
    
    for (long i = 0, ones_so_far = 0; i < n; ++i)
    {
        ones_so_far += (s[i] == '1');
        if ((s1_end == -1) && (ones_so_far == number_of_ones / 3))
            s1_end = i;
        else if ((s2_start == -1) && (ones_so_far == number_of_ones / 3 + 1))
            s2_start = i;
        if ((s2_end == -1) && (ones_so_far == number_of_ones / 3 * 2))
            s2_end = i;
        else if ((s3_start == -1) && (ones_so_far == number_of_ones / 3 * 2 + 1))
            s3_start = i;
    }
    return static_cast<int>((s2_start - s1_end) * (s3_start - s2_end) % MOD);
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numWays(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("10101", 4, trials);
    test("1001", 0, trials);
    test("0000", 3, trials);
    return 0;
}


