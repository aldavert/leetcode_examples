#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int kthSmallest(std::vector<std::vector<int> > mat, int k)
{
    struct Information
    {
        int i;
        int j;
        int sum;
        Information(int _i, int _j, int _s) : i(_i), j(_j), sum(_s) {} // Clang thing.
        bool operator<(const Information &other) const { return sum > other.sum; }
    };
    const int n = static_cast<int>(mat.size());
    std::vector<int> row = mat[0];
    
    for (int r = 1; r < n; ++r)
    {
        std::vector<int> current;
        std::priority_queue<Information> min_heap;
        
        for (int i = 0; (i < k) && (i < static_cast<int>(row.size())); ++i)
            min_heap.emplace(i, 0, row[i] + mat[r][0]);
        while (!min_heap.empty() && static_cast<int>(current.size()) < k)
        {
            const auto [i, j, _] = min_heap.top();
            min_heap.pop();
            current.push_back(row[i] + mat[r][j]);
            if (j + 1 < static_cast<int>(mat[r].size()))
                min_heap.emplace(i, j + 1, row[i] + mat[r][j + 1]);
        }
        row = current;
    }
    return row.back();
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > mat,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = kthSmallest(mat, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 3, 11}, {2, 4, 6}}, 5, 7, trials);
    test({{1, 3, 11}, {2, 4, 6}}, 9, 17, trials);
    test({{1, 10, 10}, {1, 4, 5}, {2, 3, 6}}, 7, 9, trials);
    return 0;
}


