#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int rotatedDigits(int n)
{
    int result = 0;
    for (int i = 1; i <= n; ++i)
    {
        bool is_rotated = false;
        for (int value = i; value > 0; value /= 10)
        {
            int digit = value % 10;
            if ((digit == 0) || (digit == 1) || (digit == 8)) continue;
            if ((digit == 2) || (digit == 5) || (digit == 6) || (digit == 9))
                is_rotated = true;
            else
            {
                is_rotated = false;
                break;
            }
        }
        result += is_rotated;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = rotatedDigits(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, 4, trials);
    test(1, 0, trials);
    test(2, 1, trials);
    test(180, 69, trials);
    return 0;
}


