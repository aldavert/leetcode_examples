#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

long long maxRunTime(int n, std::vector<int> batteries)
{
    auto work = batteries;
    long sum = std::accumulate(batteries.begin(), batteries.end(), 0L);
    std::sort(work.begin(), work.end());
    while (work.back() > sum / n)
    {
        sum -= work.back(), work.pop_back();
        --n;
    }
    return sum / n;
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<int> batteries, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxRunTime(n, batteries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {3, 3, 3}, 4, trials);
    test(2, {1, 1, 1, 1}, 2, trials);
    return 0;
}


