#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int reductionOperations(std::vector<int> nums)
{
    int histogram[50'001] = {};
    for (size_t i = 0, n = nums.size(); i < n; ++i)
        ++histogram[nums[i]];
    int result = 0;
    for (int i = 50'000, counter = 0; i >= 0; --i)
        if (histogram[i])
            result += counter,
            counter += histogram[i];
    return result;
}
#else
int reductionOperations(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::sort(nums.begin(), nums.end());
    int result = 0;
    for (int i = n - 2; i >= 0; --i)
        if (nums[i] != nums[i + 1])
            result += n - i - 1;
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = reductionOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 1, 3}, 3, trials);
    test({1, 1, 1}, 0, trials);
    test({1, 1, 2, 2, 3}, 4, trials);
    return 0;
}


