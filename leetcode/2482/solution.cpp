#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > onesMinusZeros(std::vector<std::vector<int> > grid)
{
    int n_rows = static_cast<int>(grid.size());
    int n_cols = static_cast<int>(grid[0].size());
    std::vector<int> row_ones(n_rows), col_ones(n_cols);
    for (int row = 0; row < n_rows; ++row)
    {
        for (int col = 0; col < n_cols; ++col)
        {
            row_ones[row] += (grid[row][col] == 1);
            col_ones[col] += (grid[row][col] == 1);
        }
    }
    std::vector<std::vector<int> > difference(n_rows, std::vector<int>(n_cols));
    for (int row = 0; row < n_rows; ++row)
        for (int col = 0; col < n_cols; ++col)
            difference[row][col] = row_ones[row] + col_ones[col]
                                 - (n_rows - row_ones[row])
                                 - (n_cols - col_ones[col]);
    return difference;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = onesMinusZeros(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 1}, {1, 0, 1}, {0, 0, 1}},
         {{0, 0, 4}, {0, 0, 4}, {-2, -2, 2}}, trials);
    test({{1, 1, 1}, {1, 1, 1}},
         {{5, 5, 5}, {5, 5, 5}}, trials);
    return 0;
}


