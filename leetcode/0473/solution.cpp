#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
bool makesquare(std::vector<int> matchsticks)
{
    const size_t n = matchsticks.size();
    std::vector<bool> st(n);
    std::sort(matchsticks.begin(), matchsticks.end());
    int len = std::accumulate(matchsticks.begin(), matchsticks.end(), 0);
    if (len % 4 != 0) return false;
    len /= 4;
    auto solve = [&](auto &&self, int k, int sum, size_t start) -> bool
    {
        if (k == 0) return true;
        if (sum == len) return self(self, k - 1, 0, 0);
        for (size_t i = start; i < n; ++i)
        {
            if (st[i]) continue;
            if ((sum == 0) && (matchsticks[i] + sum > len)) return false;
            if (matchsticks[i] + sum <= len)
            {
                st[i] = true;
                if (self(self, k , sum + matchsticks[i], i + 1)) 
                    return true;         
                st[i] = false;
            }
            while ((i < n - 1) && (matchsticks[i] == matchsticks[i + 1])) ++i;
        }
        return false;
    };
    return solve(solve, 4, 0, 0);
}
#else
bool makesquare(std::vector<int> matchsticks)
{
    long sum = 0, max = 0;
    if (matchsticks.size() < 4) return false;
    for (int m : matchsticks)
    {
        sum += m;
        if (m > max) max = m;
    }
    long target = sum / 4;
    if ((max > target) || (sum % 4 > 0)) return false;
    long sides[4] = {target, target, target, target};
    std::sort(matchsticks.rbegin(), matchsticks.rend());
    auto solve = [&](auto &&self, size_t match_index) -> bool
    {
        if (match_index >= matchsticks.size())
            return (sides[0] == 0)
                && (sides[1] == 0)
                && (sides[2] == 0)
                && (sides[3] == 0);
        for (int side_index = 0; side_index < 4; ++side_index)
        {
            if (sides[side_index] - matchsticks[match_index] >= 0)
            {
                sides[side_index] -= matchsticks[match_index];
                if (self(self, match_index + 1)) return true;
                sides[side_index] += matchsticks[match_index];
            }
        }
        return false;
    };
    return solve(solve, 0);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> matchsticks, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = makesquare(matchsticks);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10000000;
    constexpr unsigned int trials = 100;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2, 2, 2}, true, trials);
    test({3, 3, 3, 3, 4}, false, trials);
    test({8,16,24,32,40,48,56,64,72,80,88,96,104,112,60}, false, trials);
    return 0;
}


