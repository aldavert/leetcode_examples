#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxDistance(std::vector<int> colors)
{
    const int n = static_cast<int>(colors.size());
    int result = 0;
    for (int i = 0; i < n; ++i)
        for (int j = i + 1; j < n; ++j)
            if (colors[i] != colors[j])
                result = std::max(result, j - i);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> colors, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxDistance(colors);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 1, 6, 1, 1, 1}, 3, trials);
    test({1, 8, 3, 8, 3}, 4, trials);
    test({0, 1}, 1, trials);
    return 0;
}


