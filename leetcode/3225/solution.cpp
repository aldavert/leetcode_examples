#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
long long maximumScore(std::vector<std::vector<int> > grid)
{
    const int n = static_cast<int>(grid.size());
    std::vector<std::vector<long> > prefix(n, std::vector<long>(n + 1));
    std::vector<long> pick[2] = { std::vector<long>(n + 1), std::vector<long>(n + 1) };
    std::vector<long> skip[2] = { std::vector<long>(n + 1), std::vector<long>(n + 1) };
    bool selected = true;
    for (int j = 0; j < n; ++j)
        for (int i = 0; i < n; ++i)
            prefix[j][i + 1] = prefix[j][i] + grid[i][j];
    for (int j = 1; j < n; ++j)
    {
        for (int curr = 0; curr <= n; ++curr)
        {
            for (int prev = 0; prev <= n; ++prev)
            {
                if (curr > prev)
                {
                    const long score = prefix[j - 1][curr] - prefix[j - 1][prev];
                    pick[selected][curr] = std::max(pick[selected][curr],
                                                    skip[!selected][prev] + score);
                    skip[selected][curr] = std::max(skip[selected][curr],
                                                    skip[!selected][prev] + score);
                }
                else
                {
                    const long score = prefix[j][prev] - prefix[j][curr];
                    pick[selected][curr] = std::max(pick[selected][curr],
                                                    pick[!selected][prev] + score);
                    skip[selected][curr] = std::max(skip[selected][curr],
                                                    pick[!selected][prev]);
                }
            }
        }
        selected = !selected;
    }
    return *std::max_element(pick[!selected].begin(), pick[!selected].end());
}
#else
long long maximumScore(std::vector<std::vector<int> > grid)
{
    const int n = static_cast<int>(grid.size());
    std::vector<std::vector<long> > prefix(n, std::vector<long>(n + 1));
    std::vector<long> prev_pick(n + 1), prev_skip(n + 1);
    
    for (int j = 0; j < n; ++j)
        for (int i = 0; i < n; ++i)
            prefix[j][i + 1] = prefix[j][i] + grid[i][j];
    for (int j = 1; j < n; ++j)
    {
        std::vector<long> current_pick(n + 1), current_skip(n + 1);
        for (int curr = 0; curr <= n; ++curr)
        {
            for (int prev = 0; prev <= n; ++prev)
            {
                if (curr > prev)
                {
                    const long score = prefix[j - 1][curr] - prefix[j - 1][prev];
                    current_pick[curr] = std::max(current_pick[curr], prev_skip[prev] + score);
                    current_skip[curr] = std::max(current_skip[curr], prev_skip[prev] + score);
                }
                else
                {
                    const long score = prefix[j][prev] - prefix[j][curr];
                    current_pick[curr] = std::max(current_pick[curr], prev_pick[prev] + score);
                    current_skip[curr] = std::max(current_skip[curr], prev_pick[prev]);
                }
            }
        }
        prev_pick = std::move(current_pick);
        prev_skip = std::move(current_skip);
    }

    return *std::max_element(prev_pick.begin(), prev_pick.end());
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumScore(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0, 0, 0, 0},
          {0, 0, 3, 0, 0},
          {0, 1, 0, 0, 0},
          {5, 0, 0, 3, 0},
          {0, 0, 0, 0, 2}}, 11, trials);
    test({{10,  9,  0,  0, 15},
          { 7,  1,  0,  8,  0},
          { 5, 20,  0, 11,  0},
          { 0,  0,  0,  1,  2},
          { 8, 12,  1, 10,  3}}, 94, trials);
    return 0;
}


