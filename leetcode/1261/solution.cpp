#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

class FindElements
{
    std::unordered_set<int> m_values;
    void dfs(TreeNode * node, int value)
    {
        if (node == nullptr) return;
        node->val = value;
        m_values.insert(value);
        dfs(node->left , value * 2 + 1);
        dfs(node->right, value * 2 + 2);
    }
public:
    FindElements(TreeNode * root)
    {
        dfs(root, 0);
    }
    bool find(int target)
    {
        return m_values.find(target) != m_values.end();
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          std::vector<int> input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        TreeNode * root = vec2tree(tree);
        FindElements obj(root);
        result.clear();
        for (int value : input)
            result.push_back(obj.find(value));
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, null, -1}, {1, 2}, {false, true}, trials);
    test({-1, -1, -1, -1, -1}, {1, 3, 5}, {true, true, false}, trials);
    test({-1, null, -1, -1, null, -1}, {2, 3, 4, 5}, {true, false, false, true}, trials);
    return 0;
}


