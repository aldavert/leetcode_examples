#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findMaxConsecutiveOnes(std::vector<int> nums)
{
    int length = 0, max_length = 0;
    for (int n : nums)
    {
        if (n)
        {
            ++length;
            max_length = std::max(max_length, length);
        }
        else length = 0;
    }
    return max_length;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMaxConsecutiveOnes(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 0, 1, 1, 1}, 3, trials);
    test({1, 0, 1, 1, 0, 1}, 2, trials);
    return 0;
}


