#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumDeletions(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int min_pos = static_cast<int>(std::distance(nums.begin(),
                    std::min_element(nums.begin(), nums.end()))),
        max_pos = static_cast<int>(std::distance(nums.begin(),
                    std::max_element(nums.begin(), nums.end())));
    if (min_pos > max_pos) std::swap(min_pos, max_pos);
    return std::min({min_pos + 1 + n - max_pos, max_pos + 1, n - min_pos});
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumDeletions(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 10, 7, 5, 4, 1, 8, 6}, 5, trials);
    test({0, -4, 19, 1, 8, -2, -3, 5}, 3, trials);
    test({101}, 1, trials);
    return 0;
}


