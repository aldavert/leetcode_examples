#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> kWeakestRows(std::vector<std::vector<int> > mat, int k)
{
    const int n = static_cast<int>(mat.size());
    const size_t K = static_cast<size_t>(k);
    std::vector<int> result(k);
    std::priority_queue<std::pair<int, int> > queue;
    for (int row = 0; row < n; ++row)
    {
        int soldiers = 0;
        for (int p : mat[row])
        {
            if (!p) break;
            ++soldiers;
        }
        queue.push({soldiers, row});
        if (queue.size() > K)
            queue.pop();
    }
    for (int i = k - 1; i >= 0; --i)
    {
        result[i] = queue.top().second;
        queue.pop();
    }
    return result;
}
#else
std::vector<int> kWeakestRows(std::vector<std::vector<int> > mat, int k)
{
    std::vector<int> result(k);
    std::vector<std::pair<int, int> > sorted_rows(mat.size());
    for (int row = 0; const auto &col : mat)
    {
        int soldiers = 0;
        for (int p : col)
        {
            if (!p) break;
            ++soldiers;
        }
        sorted_rows[row] = {soldiers, row};
        ++row;
    }
    std::sort(sorted_rows.begin(), sorted_rows.end());
    for (int i = 0; i < k; ++i)
        result[i] = sorted_rows[i].second;
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int > > mat,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = kWeakestRows(mat, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 0, 0, 0},
          {1, 1, 1, 1, 0},
          {1, 0, 0, 0, 0},
          {1, 1, 0, 0, 0},
          {1, 1, 1, 1, 1}}, 3, {2, 0, 3}, trials);
    test({{1, 0, 0, 0},
          {1, 1, 1, 1},
          {1, 0, 0, 0},
          {1, 0, 0, 0}}, 2, {0, 2}, trials);
    return 0;
}


