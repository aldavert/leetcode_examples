#include "../common/common.hpp"
#include <random>

// ############################################################################
// ############################################################################

#if 1
class Solution
{
    std::unordered_set<int> flipped;
    std::random_device rd;
    std::mt19937 generator;
    std::uniform_int_distribution<> dist;
    int width;
    int size;
public:
    Solution(int m, int n) :
        generator(rd()),
        dist(0, m * n - 1),
        width(m),
        size(m * n)
    {
    }
    std::vector<int> flip(void)
    {
        if (static_cast<int>(flipped.size()) == size) return {};
        int idx = dist(generator);
        while (flipped.find(idx) != flipped.end())
            idx = (idx + 1) % size;
        flipped.insert(idx);
        return { idx % width, idx / width };
    }
    void reset(void)
    {
        flipped.clear();
    }
};
#else
class Solution
{
    std::vector<int> coordinates;
    int index = 0;
    int available = 0;
public:
    Solution(int m, int n) :
        coordinates(m * n),
        index(0),
        available(static_cast<int>(coordinates.size()))
    {
        std::random_device rd;
        std::mt19937 g(rd());
        for (int i = 0, k = 0; i < m; ++i)
            for (int j = 0; j < n; ++j, ++k)
                coordinates[k] = (i << 16) + j;
        std::shuffle(coordinates.begin(), coordinates.end(), g);
    }
    std::vector<int> flip(void)
    {
        if (available == 0) return {};
        std::vector<int> result = {
            0xFFFF & (coordinates[index] >> 16),
            0xFFFF & coordinates[index] };
        index = (index + 1) % static_cast<int>(coordinates.size());
        --available;
        return result;
    }
    void reset(void)
    {
        available = static_cast<int>(coordinates.size());
    }
};
#endif

// ############################################################################
// ############################################################################

void test(int m, int n, std::vector<bool> is_flip, unsigned int trials = 1)
{
    for (unsigned int i = 0; i < trials; ++i)
    {
        Solution object(m, n);
        std::vector<std::vector<int> > histogram(m, std::vector<int>(n, 0));
        for (bool flip : is_flip)
        {
            if (flip)
            {
                auto point = object.flip();
                if ((point[0] < 0) || (point[0] >= m)
                ||  (point[1] < 0) || (point[1] >= n))
                {
                    std::cerr << showResult(false)
                              << " Picked point is out of bounds.\n";
                    return;
                }
                if (histogram[point[0]][point[1]])
                {
                    std::cerr << showResult(false)
                              << " The point had been already picked.\n";
                    return;
                }
                ++histogram[point[0]][point[1]];
            }
            else
            {
                for (auto &row : histogram) row.assign(n, 0);
                object.reset();
            }
        }
    }
    std::cout << showResult(true) << " The algorithm works properly "
              << "(no check if flips are uniform)\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 1, {true, true, true, false, true}, trials);
    return 0;
}


