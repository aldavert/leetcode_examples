#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

long long subArrayRanges(std::vector<int> nums)
{
    auto subarraySum = [&](auto && op) -> long
    {
        const int n = static_cast<int>(nums.size());
        std::vector<int> prev(n, -1), next(n, n);
        long result = 0;
        std::stack<int> stack;
        
        for (int i = 0; i < n; ++i)
        {
            while (!stack.empty() && op(nums[stack.top()], nums[i]))
            {
                int index = stack.top();
                stack.pop();
                next[index] = i;
            }
            if (!stack.empty())
                prev[i] = stack.top();
            stack.push(i);
        }
        for (int i = 0; i < n; ++i)
            result += static_cast<long>(nums[i]) * (i - prev[i]) * (next[i] - i);
        return result;
    };
    return subarraySum(std::less<int>()) - subarraySum(std::greater<>());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = subArrayRanges(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 4, trials);
    test({1, 3, 3}, 4, trials);
    test({4, -2, -3, 4, 1}, 59, trials);
    return 0;
}


