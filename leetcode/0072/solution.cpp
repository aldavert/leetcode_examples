#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

////  DDD
//// |int|e|n|tion|
//// |  / / /|    |
//// | / / / |    |
//// |/ / /  |    |
//// |e|x|ecu|tion|
////    R AAA
//// 7 OPERATIONS: DEL i, DEL n, DEL t, REP n->x, ADD a, ADD c, ADD u
////
//// |int|e|n|tion|
//// |  / /  |    |
//// |ex|e|cu|tion|
//// DRR  DR
//// 
//// |h|o|r|s|e|
//// | | |/ /-/
//// |r|o|s|
////  R  D D
////
//// agglut|ination|   |
////       |\       \  |
////       | \       \ |
////       |  \       \|
//// retrov|acc|ination|
//// RRRRRR AAA

#if 1
int minDistance(std::string word1, std::string word2)
{
    const int n = static_cast<int>(word1.size()),
              m = static_cast<int>(word2.size());
    int dp[501];
    
    for (int j = 0; j <= m; ++j)
        dp[j] = j;
    for (int i = 1; i <= n; ++i)
    {
        int last_previous = dp[0];
        dp[0] = i;
        for (int j = 1; j <= m; ++j)
        {
            int last_current = dp[j];
            
            if (word1[i - 1] != word2[j - 1])
            {
                int cost = std::min(dp[j - 1], last_current);
                dp[j] = std::min(cost, last_previous) + 1;
            }
            else dp[j] = last_previous;
            
            last_previous = last_current;
        }
    }
    return dp[m];
}
#elif 0
int minDistance(std::string word1, std::string word2)
{
    const int n = static_cast<int>(word1.size()),
              m = static_cast<int>(word2.size());
    int dp[501][501] = {};
    for (int i = 0; i <= n; ++i) dp[i][0] = i;
    for (int j = 0; j <= m; ++j) dp[0][j] = j;
    
    for (int i = 1; i <= n; ++i)
    {
        for (int j = 1; j <= m; ++j)
        {
            if (word1[i - 1] != word2[j - 1])
            {
                int cost = std::min(dp[i][j - 1], dp[i - 1][j]);
                dp[i][j] = std::min(cost, dp[i - 1][j - 1]) + 1;
            }
            else dp[i][j] = dp[i - 1][j - 1];
        }
    }
    return dp[n][m];
}
#elif 0
int minDistance(std::string word1, std::string word2)
{
    const int n = static_cast<int>(word1.size()),
              m = static_cast<int>(word2.size());
    std::unordered_map<int, int> lut;
    std::vector<int> character_position[26];
    for (int i = 0; i < n; ++i)
        character_position[word1[i] - 'a'].push_back(i);
    
    auto distance = [&](auto &&self, int idx1, int idx2) -> int
    {
        const int call_id = idx1 << 16 | idx2;
        if (auto search = lut.find(call_id); search != lut.end())
            return search->second;
        
        while ((idx1 < n) && (idx2 < m) && (word1[idx1] == word2[idx2]))
        {
            ++idx1;
            ++idx2;
        }
        if ((idx1 == n) || (idx2 == m))
            return lut[call_id] = (n - idx1) + (m - idx2);
        else
        {
            // Replace.
            int cost = self(self, idx1 + 1, idx2 + 1) + 1;
            // Add.
            cost = std::min(cost, self(self, idx1, idx2 + 1) + 1);
            for (int pos : character_position[word2[idx2] - 'a'])
                if (pos > idx1) // Delete + replace
                    cost = std::min(cost, self(self, pos + 1, idx2 + 1) + (pos - idx1));
            return lut[call_id] = cost;
        };
    };
    
    return distance(distance, 0, 0);
}
#elif 0
int minDistance(std::string word1, std::string word2)
{
    const int n = static_cast<int>(word1.size()),
              m = static_cast<int>(word2.size());
    std::unordered_map<int, int> lut;
    
    auto distance = [&](auto &&self, int idx1, int idx2) -> int 
    {
        const int call_id = idx1 << 16 | idx2;
        if (auto search = lut.find(call_id); search != lut.end())
            return search->second;
        
        while ((idx1 < n) && (idx2 < m) && (word1[idx1] == word2[idx2]))
        {
            ++idx1;
            ++idx2;
        }
        if ((idx1 == n) || (idx2 == m))
            return lut[call_id] = (n - idx1) + (m - idx2);
        else
        {
            int cost = m + n;
            if (idx2 < m)                       // Add a character.
                cost = std::min(cost, self(self, idx1, idx2 + 1) + 1);
            if (idx1 < n)                       // Remove a character.
                cost = std::min(cost, self(self, idx1 + 1, idx2) + 1);
            if ((idx1 < n) && (idx2 < m))       // Replace a character.
                cost = std::min(cost, self(self, idx1 + 1, idx2 + 1) + 1);
            return lut[call_id] = cost;
        }
    };
    
    return distance(distance, 0, 0);
}
#else
int minDistance(std::string word1, std::string word2)
{
    const int n = static_cast<int>(word1.size()),
              m = static_cast<int>(word2.size());
    int min_distance = n + m;
    std::unordered_map<int, int> lut;
    
    auto distance = [&](auto &&self, int idx1, int idx2, int cost) -> void
    {
        if (cost > min_distance) return;
        
        const int call_id = idx1 << 16 | idx2;
        if (auto search = lut.find(call_id);
                (search != lut.end()) && (search->second < cost))
            return;
        lut[call_id] = cost;
        
        while ((idx1 < n) && (idx2 < m) && (word1[idx1] == word2[idx2]))
        {
            ++idx1;
            ++idx2;
        }
        if ((idx1 == n) || (idx2 == m))
        {
            // Reached the end of the word, add the cost of removing any remaining
            // character and finish the recursive call.
            min_distance = std::min(min_distance, cost + (n - idx1) + (m - idx2));
        }
        else
        {
            if (idx2 < m)
                self(self, idx1, idx2 + 1, cost + 1);         // Add a character.
            if (idx1 < n)
                self(self, idx1 + 1, idx2, cost + 1);         // Remove a character.
            if ((idx1 < n) && (idx2 < m))
                self(self, idx1 + 1, idx2 + 1, cost + 1);     // Replace a character.
        }
    };
    distance(distance, 0, 0, 0);
    
    return min_distance;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string word1, std::string word2, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDistance(word1, word2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("horse", "ros", 3, trials);
    test("intention", "execution", 5, trials);
    test("agglutination", "retrovaccination", 9, trials);
    test("pseudopseudohypoparathyroidism", "antidisestablishmentarianism", 22, trials);
    return 0;
}


