#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

bool btreeGameWinningMove(TreeNode * root, int n, int x)
{
    int left_count = 0, right_count = 0;
    auto count = [&](auto &&self, TreeNode * node) -> int
    {
        if (node == nullptr) return 0;
        int l = self(self, node->left), r = self(self, node->right);
        if (node->val == x)
            left_count = l,
            right_count = r;
        return 1 + l + r;
    };
    count(count, root);
    return std::max({left_count,
                     right_count,
                     n - left_count - right_count - 1}) > n / 2;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          int n,
          int x,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = btreeGameWinningMove(root, n, x);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, 11, 3, true, trials);
    test({1, 2, 3}, 3, 1, false, trials);
    return 0;
}


