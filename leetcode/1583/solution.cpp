#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int unhappyFriends(int n,
                   std::vector<std::vector<int> > preferences,
                   std::vector<std::vector<int> > pairs)
{
    std::vector<std::vector<int> > vec(n, std::vector<int>(n, n));
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n - 1; ++j)
            vec[i][preferences[i][j]] = j;
    int result = 0;
    for (int i = 0; i < n / 2; ++i)
    {
        int x = pairs[i][0], y = pairs[i][1];
        bool flag1 = false, flag2 = false;
        for (int j = 0; j < n / 2; ++j)
        {
            if (i == j) continue;
            int u = pairs[j][0], v = pairs[j][1];
            flag1 = flag1 || ((vec[x][u] < vec[x][y]) && (vec[u][x] < vec[u][v]));
            flag1 = flag1 || ((vec[x][v] < vec[x][y]) && (vec[v][x] < vec[v][u]));
            flag2 = flag2 || ((vec[y][u] < vec[y][x]) && (vec[u][y] < vec[u][v]));
            flag2 = flag2 || ((vec[y][v] < vec[y][x]) && (vec[v][y] < vec[v][u]));
        }
        result += flag1 + flag2;
    }
    return result;
}
#else
int unhappyFriends(int n,
                   std::vector<std::vector<int> > preferences,
                   std::vector<std::vector<int> > pairs)
{
    std::vector<std::unordered_map<int, int> > prefer(n);
    std::vector<int> matches(n);
    int result = 0;
    
    for (const auto &pair : pairs)
    {
        matches[pair[0]] = pair[1];
        matches[pair[1]] = pair[0];
    }
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n - 1; ++j)
            prefer[i][preferences[i][j]] = j;
    for (int x = 0; x < n; ++x)
    {
        for (auto [u, _] : prefer[x])
        {
            int y = matches[x];
            int v = matches[u];
            if ((prefer[x][u] < prefer[x][y]) && (prefer[u][x] < prefer[u][v]))
            {
                ++result;
                break;
            }
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > preferences,
          std::vector<std::vector<int> > pairs,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = unhappyFriends(n, preferences, pairs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{1, 2, 3}, {3, 2, 0}, {3, 1, 0}, {1, 2, 0}}, {{0, 1}, {2, 3}}, 2, trials);
    test(2, {{1}, {0}}, {{1, 0}}, 0, trials);
    test(4, {{1, 3, 2}, {2, 3, 0}, {1, 3, 0}, {0, 2, 1}}, {{1, 3}, {0, 2}}, 4, trials);
    return 0;
}


