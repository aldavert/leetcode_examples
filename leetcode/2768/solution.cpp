#include "../common/common.hpp"
#include <unordered_map>
#include <numeric>

// ############################################################################
// ############################################################################

std::vector<long long> countBlackBlocks(int m,
                                        int n,
                                        std::vector<std::vector<int> > coordinates)
{
    std::vector<long long> result(5);
    std::unordered_map<long, int> count;
    for (const auto &coordinate : coordinates)
        for (long i = coordinate[0]; i < coordinate[0] + 2; ++i)
            for (long j = coordinate[1]; j < coordinate[1] + 2; ++j)
                if (i - 1 >= 0 && i < m && j - 1 >= 0 && j < n)
                    ++count[i * n + j];
    for (const auto& [_, freq] : count)
        ++result[freq];
    result[0] = (m - 1L) * (n - 1) - std::accumulate(result.begin(), result.end(), 0L);
    return result;
}

// ############################################################################
// ############################################################################

void test(int m,
          int n,
          std::vector<std::vector<int> > coordinates,
          std::vector<long long> solution,
          unsigned int trials = 1)
{
    std::vector<long long> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countBlackBlocks(m, n, coordinates);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 3, {{0, 0}}, {3, 1, 0, 0, 0}, trials);
    test(3, 3, {{0, 0}, {1, 1}, {0, 2}}, {0, 2, 2, 0, 0}, trials);
    return 0;
}


