#include "../common/common.hpp"
#include <stack>

constexpr int null = -1000;

class Node
{
public:
    ~Node(void) { delete next; delete child; }
    int val = 0;
    Node * prev = nullptr;
    Node * next = nullptr;
    Node * child = nullptr;
};

// ############################################################################
// ############################################################################

Node * flatten(Node * head)
{
    if (head == nullptr) return nullptr;
    std::stack<Node *> s;
    Node * result = new Node{head->val, nullptr, nullptr, nullptr};
    Node * prev = result;
    if (head->next) s.push(head->next);
    if (head->child) s.push(head->child);
    while (!s.empty())
    {
        Node * current = s.top();
        s.pop();
        prev->next = new Node{current->val, prev, nullptr, nullptr};
        prev = prev->next;
        if (current->next) s.push(current->next);
        if (current->child) s.push(current->child);
    }
    return result;
}

// ############################################################################
// ############################################################################

Node * vec2list(const std::vector<int> &vec)
{
    const int n = static_cast<int>(vec.size());
    Node * root = nullptr;
    if (n > 0)
    {
        int idx;
        Node * header = root = new Node{vec[0], nullptr, nullptr, nullptr};
        Node * current = header;
        for (idx = 1; vec[idx] != null; ++idx)
        {
            current->next = new Node{vec[idx], current, nullptr, nullptr};
            current = current->next;
        }
        ++idx;
        while (idx < n)
        {
            for (; vec[idx] == null; ++idx)
                header = header->next;
            header->child = new Node{vec[idx], nullptr, nullptr, nullptr};
            current = header = header->child;
            for (++idx; (idx < n) && (vec[idx] != null); ++idx)
            {
                current->next = new Node{vec[idx], current, nullptr, nullptr};
                current = current->next;
            }
            ++idx;
        }
    }
    return root;
}
///===================================================================
//  1---2---3---4---5---6--NULL
//          |
//          7---8---9---10--NULL
//              |
//              11--12--NULL
///===================================================================
// [   1,    2,    3,    4,  5,    6, null]
// [null, null,    7,    8,  9,   10, null]
// [            null,   11, 12, null]
///===================================================================
// [1, 2, 3, 4, 5, 6, null, null, null, 7, 8, 9, 10, null, null, 11, 12]
///===================================================================

std::vector<int> list2vec(Node * root)
{
    std::vector<int> result;
    Node * current = root;
    int empty = 0;
    while (current)
    {
        Node * next_level = nullptr;
        for (int i = 0; i < empty; ++i)
            result.push_back(null);
        empty = 0;
        while ((current != nullptr) && (current->child == nullptr))
        {
            result.push_back(current->val);
            current = current->next;
            ++empty;
        }
        if (current == nullptr)
            break;
        next_level = current->child;
        result.push_back(current->val);
        current = current->next;
        for (; current != nullptr; current = current->next)
            result.push_back(current->val);
        result.push_back(null);
        current = next_level;
    }
    return result;
}

void test(std::vector<int> nodes, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        Node * root = vec2list(nodes);
        auto flat_list = flatten(root);
        result = list2vec(flat_list);
        delete root;
        delete flat_list;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6, null, null, null, 7, 8, 9, 10, null, null, 11, 12},
         {1, 2, 3, 7, 8, 11, 12, 9, 10, 4, 5, 6}, trials);
    test({1, 2, null, 3}, {1, 3, 2}, trials);
    test({}, {}, trials);
    return 0;
}


