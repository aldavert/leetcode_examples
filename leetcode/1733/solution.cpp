#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int minimumTeachings([[maybe_unused]] int n,
                     std::vector<std::vector<int> > languages,
                     std::vector<std::vector<int> > friendships)
{
    std::vector<std::unordered_set<int> > language_sets;
    std::unordered_set<int> need_teach;
    std::unordered_map<int, int> language_count;
    auto cantTalk = [&](int u, int v) -> bool
    {
        for (const int language : language_sets[u])
            if (language_sets[v].find(language) != language_sets[v].end())
                return false;
        return true;
    };
    
    for (const auto &language : languages)
        language_sets.push_back({language.begin(), language.end()});
    for (const auto &friendship : friendships)
    {
        if (cantTalk(friendship[0] - 1, friendship[1] - 1))
        {
            need_teach.insert(friendship[0] - 1);
            need_teach.insert(friendship[1] - 1);
        }
    }
    for (int u : need_teach)
        for (int language : language_sets[u])
            ++language_count[language];
    int max_count = 0;
    for (const auto& [_, freq] : language_count)
        max_count = std::max(max_count, freq);
    return static_cast<int>(need_teach.size()) - max_count;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > languages,
          std::vector<std::vector<int> > friendship,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumTeachings(n, languages, friendship);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {{1}, {2}, {1, 2}}, {{1, 2}, {1, 3}, {2, 3}}, 1, trials);
    test(3, {{2}, {1, 3}, {1, 2}, {3}}, {{1, 4}, {1, 2}, {3, 4}, {2, 3}}, 2, trials);
    return 0;
}


