#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> smallestSubarrays(std::vector<int> nums)
{
    constexpr int MAXBIT = 30;
    std::vector<int> result(nums.size(), 1);
    std::vector<int> closest(MAXBIT);
    
    for (int i = static_cast<int>(nums.size()) - 1; i >= 0; --i)
    {
        for (int j = 0; j < MAXBIT; ++j)
        {
            if (nums[i] >> j & 1) closest[j] = i;
            result[i] = std::max(result[i], closest[j] - i + 1);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestSubarrays(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 2, 1, 3}, {3, 3, 2, 2, 1}, trials);
    test({1, 2}, {2, 1}, trials);
    return 0;
}


