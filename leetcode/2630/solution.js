/**
 * @param {Function} fn
 */

function memoize(fn) {
  const memo = new Map();
  function explore(memo, args, index){
      if (index === args.length) {
          if (memo.has('result')) {
              return memo.get('result');
          }
          const result = fn(...args);
          memo.set('result', result)
          return result;
      }
      if (!memo.has(args[index])) {
          memo.set(args[index], new Map());
      }
      return explore(memo.get(args[index]), args, index + 1);
  }
  return function (...args) {
      return explore(memo, args, 0);  
  }
}


/** 
 * let callCount = 0;
 * const memoizedFn = memoize(function (a, b) {
 *	 callCount += 1;
 *   return a + b;
 * })
 * memoizedFn(2, 3) // 5
 * memoizedFn(2, 3) // 5
 * console.log(callCount) // 1 
 */
