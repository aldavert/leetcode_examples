#include "../common/common.hpp"
#include <unordered_map>
#include <numeric>

// ############################################################################
// ############################################################################

std::vector<long long> distance(std::vector<int> nums)
{
    std::vector<long long> result(nums.size());
    std::unordered_map<int, std::vector<int> > num_to_indices;
    
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        num_to_indices[nums[i]].push_back(i);
    for (const auto& [_, indices] : num_to_indices)
    {
        const int n = static_cast<int>(indices.size());
        if (n == 1) continue;
        long sum_so_far = std::accumulate(indices.begin(), indices.end(), 0L);
        for (int i = 0, prev_index = 0; i < n; ++i)
        {
            sum_so_far += (i - 1) * (indices[i] - prev_index);
            sum_so_far -= (n - 1 - i) * (indices[i] - prev_index);
            result[indices[i]] = sum_so_far;
            prev_index = indices[i];
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<long long> solution, unsigned int trials = 1)
{
    std::vector<long long> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = distance(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 1, 1, 2}, {5, 0, 3, 4, 0}, trials);
    test({0, 5, 3}, {0, 0, 0}, trials);
    return 0;
}


