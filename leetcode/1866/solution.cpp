#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int rearrangeSticks(int n, int k)
{
    constexpr int MOD = 1'000'000'007;
    long dp[1001][1001] = {};
    for (int j = 1; j <= k; ++j)
    {
        dp[j][j] = 1;
        for (int i = j + 1; i <= n; ++i)
            dp[i][j] = (dp[i - 1][j - 1] + dp[i - 1][j] * (i - 1)) % MOD;
    }
    return static_cast<int>(dp[n][k]);
}

// ############################################################################
// ############################################################################

void test(int n, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = rearrangeSticks(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 2, 3, trials);
    test(5, 5, 1, trials);
    test(20, 11, 647'427'950, trials);
    return 0;
}


