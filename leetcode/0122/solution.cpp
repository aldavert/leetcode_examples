#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int maxProfit(std::vector<int> prices)
{
    int profit = 0;
    for (int previous = prices[0]; int v : prices)
        profit += std::max(0, v - std::exchange(previous, v));
    return profit;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> prices, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxProfit(prices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({7, 1, 5, 3, 6, 4}, 7, trials);
    test({1, 2, 3, 4, 5}, 4, trials);
    test({7, 6, 4, 3, 1}, 0, trials);
    test({1, 3, 2, 4, 5, 3, 7, 1}, 9, trials);
    return 0;
}


