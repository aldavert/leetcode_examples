#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int minimumCost(std::vector<int> nums)
{
    int lowest[] = { std::numeric_limits<int>::max(),
                     std::numeric_limits<int>::max() };
    for (size_t i = 1; i < nums.size(); ++i)
    {
        if (nums[i] <= lowest[0])
            lowest[1] = std::exchange(lowest[0], nums[i]);
        else if (nums[i] < lowest[1])
            lowest[1] = nums[i];
    }
    return nums.front() + lowest[0] + lowest[1];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumCost(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 12}, 6, trials);
    test({5, 4, 3}, 12, trials);
    test({10, 3, 1, 1}, 12, trials);
    return 0;
}


