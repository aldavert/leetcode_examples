#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

long long minimumCost(std::string source,
                      std::string target,
                      std::vector<char> original,
                      std::vector<char> changed,
                      std::vector<int> cost)
{
    constexpr int MAX = 5'000'000;
    int edges[26][26];
    int seen_cost[26][26];
    std::memset(edges, MAX, sizeof(edges));
    std::memset(seen_cost, MAX, sizeof(seen_cost));
    for (size_t i = 0; i < original.size(); ++i)
        edges[original[i] - 'a'][changed[i] - 'a'] =
            std::min(cost[i], edges[original[i] - 'a'][changed[i] - 'a']);
    auto dijkstra = [&](int s, int d) -> int
    {
        s -= 'a';
        d -= 'a';
        if (seen_cost[s][d] < MAX)
            return seen_cost[s][d];
        int distance[26];
        bool visited[26] = {};
        std::memset(distance, MAX, sizeof(distance));
        distance[s] = 0;
        while (true)
        {
            int min_distance = MAX;
            int selected = 0;
            for (int i = 0; i < 26; ++i)
            {
                if (visited[i]) continue;
                if (distance[i] < min_distance)
                {
                    min_distance = distance[i];
                    selected = i;
                }
            }
            if (min_distance == MAX)
                return seen_cost[s][d] = -1;
            if (selected == d)
                return seen_cost[s][d] = min_distance;
            visited[selected] = true;
            for (size_t i = 0; i < 26; ++i)
            {
                int new_distance = distance[selected] + edges[selected][i];
                if (new_distance < distance[i])
                    distance[i] = new_distance;
            }
        }
        return -1;
    };
    long result = 0;
    for (size_t i = 0; i < source.size(); ++i)
    {
        if (source[i] == target[i]) continue;
        int distance = dijkstra(source[i], target[i]);
        if (distance == -1) return -1;
        result += distance;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string source,
          std::string target,
          std::vector<char> original,
          std::vector<char> changed,
          std::vector<int> cost,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumCost(source, target, original, changed, cost);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcd", "acbe", {'a', 'b', 'c', 'c', 'e', 'd'},
         {'b', 'c', 'b', 'e', 'b', 'e'},
         {2, 5, 5, 1, 2, 20}, 28, trials);
    test("aaaa", "bbbb", {'a', 'c'}, {'c', 'b'}, {1, 2}, 12, trials);
    test("abcd", "abce", {'a'}, {'e'}, {10000}, -1, trials);
    return 0;
}


