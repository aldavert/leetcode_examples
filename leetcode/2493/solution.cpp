#include "../common/common.hpp"
#include <unordered_map>
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int magnificentSets(int n, std::vector<std::vector<int> > edges)
{
    std::vector<std::vector<int> > graph(n, std::vector<int>());
    std::vector<int> group(n, -1);
    std::vector<bool> visited(n, false);
    
    for (auto &edge: edges)
    {
        graph[edge[0] - 1].push_back(edge[1] - 1);
        graph[edge[1] - 1].push_back(edge[0] - 1);
    }

    auto maxDepth = [&](int u) -> int
    {
        std::vector<bool> traverse_visit(n);
        traverse_visit[u] = true;
        
        int steps = 0; 
        for (std::queue<int> queue{{u}}; !queue.empty(); )
        {
            ++steps;
            for (int sz = static_cast<int>(queue.size()); sz--; )
            {
                int node = queue.front();
                queue.pop();
                for (int next : graph[node])
                {
                    if (!traverse_visit[next])
                    {
                        traverse_visit[next] = true;
                        queue.push(next);
                    }
                }
            }
        }
        return steps;
    };
    auto bfs = [&] (int u) -> int
    {
        group[u] = 1; visited[u] = true;
        std::vector<int> trace;

        for (std::queue<int> queue{{u}}; !queue.empty(); )
        {
            for (int sz = static_cast<int>(queue.size()); sz--; )
            {
                int node = queue.front();
                queue.pop();
                trace.push_back(node);

                for (int v: graph[node])
                {
                    if (!visited[v])
                    {
                        group[v] = group[node] + 1;
                        visited[v] = true;
                        queue.push(v);
                    }
                    else if (abs(group[v] - group[node]) != 1)
                        return -1;
                }
            }
        }
        int max_depth = 0;
        for (int x : trace) max_depth = std::max(max_depth, maxDepth(x));
        return max_depth;
    };
    
    int result = 0;
    for (int i = 0; i < n; ++i) 
    {
        if (!visited[i])
        {
            int ret = bfs(i);
            if (ret == -1) return -1;
            result += ret;
        }
    }
    return result;
}
#else
int magnificentSets(int n, std::vector<std::vector<int> > edges)
{
    std::vector<std::vector<int> > graph(n);
    auto bfs = [&](int s) -> int
    {
        int step = 0;
        std::queue<int> queue{{s}};
        std::unordered_map<int, int> node_to_step{{s, 1}};
        while (!queue.empty())
        {
            ++step;
            for (int sz = static_cast<int>(queue.size()); sz > 0; --sz)
            {
                int u = queue.front();
                queue.pop();
                for (int v : graph[u])
                {
                    if (!node_to_step.count(v))
                    {
                        queue.push(v);
                        node_to_step[v] = step + 1;
                    }
                    else if (node_to_step[v] == step) return -1;
                }
            }
        }
        return step;
    };
    std::vector<int> id(n), rank(n);
    for (int i = 1; i < n; ++i) id[i] = i;
    auto find = [&](auto &&self, int u) -> int
    {
        return (id[u] == u)?u:id[u] = self(self, id[u]);
    };
    auto merge = [&](int u, int v)
    {
        int i = find(find, u), j = find(find, v);
        if (i == j) return;
        if      (rank[i] < rank[j]) id[i] = id[j];
        else if (rank[i] > rank[j]) id[j] = id[i];
        else
        {
            id[i] = id[j];
            ++rank[j];
        }
    };
    
    for (const auto &edge : edges)
    {
        graph[edge[0] - 1].push_back(edge[1] - 1);
        graph[edge[1] - 1].push_back(edge[0] - 1);
        merge(edge[0] - 1, edge[1] - 1);
    }
    std::unordered_map<int, int> root_to_group_size;
    for (int i = 0; i < n; ++i)
    {
        int new_group_size = bfs(i);
        if (new_group_size == -1) return -1;
        int &group_size = root_to_group_size[find(find, i)];
        group_size = std::max(group_size, new_group_size);
    }
    
    int result = 0;
    for (const auto& [_, group_size] : root_to_group_size)
        result += group_size;
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = magnificentSets(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, {{1, 2}, {1, 4}, {1, 5}, {2, 6}, {2, 3}, {4, 6}}, 4, trials);
    test(3, {{1, 2}, {2, 3}, {3, 1}}, -1, trials);
    return 0;
}


