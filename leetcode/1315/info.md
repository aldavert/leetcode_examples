# Sum of Nodes with Even-Valued Grandparents

Given the `root` of a binary tree, return *the sum of values of nodes with an* ***even-valued grandparent***. If there are no nodes with an **even-valued grandparent**, return `0`.

A **grandparent** of a node is the parent of its parent if it exists.

#### Example 1:
> ```mermaid 
> graph TD;
> A((6))---B((7))
> A---C((8))
> B---D((2))
> B---E((7))
> C---F((1))
> C---G((3))
> D---H((9))
> D---EMPTY1(( ))
> E---I((1))
> E---J((4))
> G---EMPTY2(( ))
> G---K((5))
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class EMPTY1,EMPTY2 empty;
> linkStyle 7,10 stroke-width:0px;
> classDef red fill:#FAA,stroke:#822,stroke-width:2px;
> class D,E,F,G,K red;
> classDef blue fill:#AAF,stroke:#228,stroke-width:2px;
> class A,C blue;
> classDef default fill:#FFF,stroke:#333,stroke-width:2px;
> ```
> *Input:* `root = [6, 7, 8, 2, 7, 1, 3, 9, null, 1, 4, null, null, null, 5]`  
> *Output:* `18`  
> *Explanation:* The red nodes are the nodes with even-value grandparent while the blue nodes are the even-value grandparents.

#### Example 2:
> ```mermaid 
> graph TD;
> A((1))
> classDef default fill:#FFF,stroke:#333,stroke-width:2px;
> ```
> *Input:* `root = [1]`  
> *Output:* `0`

#### Constraints:
- The number of nodes in the tree is in the range `[1, 10^4]`.
- `1 <= Node.val <= 100`


