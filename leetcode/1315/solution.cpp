#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int sumEvenGrandparent(TreeNode * root)
{
    auto dfs = [&]() -> int
    {
        auto inner = [](auto &&self, TreeNode * node, int p, int gp) -> int
        {
            if (node == nullptr) return 0;
            return (gp % 2 == 0) * node->val
                 + self(self, node->left , node->val, p)
                 + self(self, node->right, node->val, p);
        };
        return inner(inner, root, 1, 1);
    };
    return dfs();
}

// ############################################################################
// ############################################################################

void test(const std::vector<int> &tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = sumEvenGrandparent(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({6, 7, 8, 2, 7, 1, 3, 9, null, 1, 4, null, null, null, 5}, 18, trials);
    test({1}, 0, trials);
    return 0;
}


