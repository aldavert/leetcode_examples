#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int integerBreak(int n)
{
    if (n == 2) return 1;
    if (n == 3) return 2;
    int result = 1;
    while (n > 4)
        n -= 3,
        result *= 3;
    return result * n;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = integerBreak(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 1, trials);
    test(10, 36, trials);
    return 0;
}


