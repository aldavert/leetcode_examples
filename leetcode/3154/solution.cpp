#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int waysToReachStair(int k)
{
    constexpr int MAXJUMP = 29;
    std::vector<std::vector<int> > comb(MAXJUMP + 2, std::vector<int>(MAXJUMP + 2));
    for (int i = 0; i <= MAXJUMP + 1; ++i)
        comb[i][0] = 1;
    for (int i = 1; i <= MAXJUMP + 1; ++i)
        for (int j = 1; j <= MAXJUMP + 1; ++j)
            comb[i][j] = comb[i - 1][j] + comb[i - 1][j - 1];
    
    int result = 0;
    for (int jump = 0; jump <= MAXJUMP; ++jump)
    {
        const int down = (1 << jump) - k;
        if ((down < 0) || (down > jump + 1))
            continue;
        result += comb[jump + 1][down];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = waysToReachStair(k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(0, 2, trials);
    test(1, 4, trials);
    return 0;
}


