#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool closeStrings(std::string word1, std::string word2)
{
    auto sort = [](int histogram[26]) -> void
    {
        int values[26] = {}, indexes[26] = {};
        int non_zeros = 0;
        for (int i = 0; i < 26; ++i)
            if (histogram[i])
                values[non_zeros] = histogram[i],
                indexes[non_zeros++] = i;
        std::sort(&values[0], &values[non_zeros]);
        std::sort(&indexes[0], &indexes[non_zeros]);
        for (int i = 0; i < non_zeros; ++i)
            histogram[indexes[i]] = values[i];
    };
    if (word1.size() != word2.size()) return false;
    int histogram1[26] = {}, histogram2[26] = {};
    for (char c : word1)
        ++histogram1[c - 'a'];
    for (char c : word2)
        ++histogram2[c - 'a'];
    sort(histogram1);
    sort(histogram2);
    for (size_t i = 0; i < 26; ++i)
        if (histogram1[i] != histogram2[i])
            return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string word1,
          std::string word2,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = closeStrings(word1, word2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", "bca", true, trials);
    test("a", "aa", false, trials);
    test("cabbba", "abbccc", true, trials);
    test("uau", "ssx", false, trials);
    test("abcde", "aecdb", true, trials);
    test("aacabb", "bbcbaa", true, trials);
    return 0;
}


