#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

int countCompleteSubstrings(std::string word, int k)
{
    const int n = static_cast<int>(word.size());
    auto count = [&](int window_size) -> int
    {
        int result = 0;
        for (int i = 0, count_letters = 0, freq[26] = {}; i < n; ++i)
        {
            ++freq[word[i] - 'a'];
            ++count_letters;
            if ((i > 0) && (std::abs(word[i] - word[i - 1]) > 2))
            {
                std::memset(freq, 0, sizeof(freq));
                ++freq[word[i] - 'a'];
                count_letters = 1;
            }
            if (count_letters == window_size + 1)
            {
                --freq[word[i - window_size] - 'a'];
                --count_letters;
            }
            if (count_letters == window_size)
            {
                bool all_of = true;
                for (int j = 0; j < 26; ++j)
                    all_of = all_of && ((freq[j] == 0) || (freq[j] == k));
                result += all_of;
            }
        }
        return result;
    };
    const int number_of_unique_letters =
        static_cast<int>(std::unordered_set<char>{word.begin(), word.end()}.size());
    int result = 0;
    for (int ws = k; (ws <= k * number_of_unique_letters) && (ws <= n); ws += k)
        result += count(ws);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countCompleteSubstrings(word, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("igigee", 2, 3, trials);
    test("aaabbbccc", 3, 6, trials);
    return 0;
}


