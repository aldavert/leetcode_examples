#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > construct2DArray(std::vector<int> original, int m, int n)
{
    if (static_cast<int>(original.size()) != m * n) return {};
    std::vector<std::vector<int> > result(m, std::vector<int>(n));
    for (int r = 0, k = 0; r < m; ++r)
        for (int c = 0; c < n; ++c, ++k)
            result[r][c] = original[k];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> original,
          int m,
          int n,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = construct2DArray(original, m, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 2, 2, {{1, 2}, {3, 4}}, trials);
    test({1, 2, 3}, 1, 3, {{1, 2, 3}}, trials);
    test({1, 2}, 1, 1, {}, trials);
    return 0;
}


