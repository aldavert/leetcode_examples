#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> >
sortTheStudents(std::vector<std::vector<int> > score, int k)
{
    std::sort(score.begin(), score.end(),
            [k](const std::vector<int>& a, const std::vector<int>& b) {
                return a[k] > b[k]; });
    return score;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > score,
          int k,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sortTheStudents(score, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{10, 6, 9, 1}, {7, 5, 11, 2}, {4, 8, 3, 15}}, 2,
         {{7, 5, 11, 2}, {10, 6, 9, 1}, {4, 8, 3, 15}}, trials);
    test({{3, 4}, {5, 6}}, 0, {{5, 6}, {3, 4}}, trials);
    return 0;
}


