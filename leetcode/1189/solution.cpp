#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxNumberOfBalloons(std::string text)
{
    int histogram[128] = {};
    for (char c : text)
        ++histogram[static_cast<int>(c)];
    int result = std::min(histogram['a'], histogram['b']);
    result = std::min(result, histogram['n']);
    result = std::min(result, std::min(histogram['l'], histogram['o']) / 2);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string text, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxNumberOfBalloons(text);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("nlaebolko", 1, trials);
    test("loonbalxballpoon", 2, trials);
    test("leetcode", 0, trials);
    return 0;
}


