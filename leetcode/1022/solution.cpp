#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int sumRootToLeaf(TreeNode * root)
{
    int result = 0;
    auto process = [&](auto &&self, TreeNode * current, int value) -> void
    {
        if (current)
        {
            value = (value << 1) + current->val;
            if (current->left || current->right)
            {
                self(self, current->left, value);
                self(self, current->right, value);
            }
            else result += value;
        }
    };
    process(process, root, 0);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = sumRootToLeaf(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 1, 0, 1, 0, 1}, 22, trials);
    test({0}, 0, trials);
    return 0;
}


