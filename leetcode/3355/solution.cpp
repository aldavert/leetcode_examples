#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isZeroArray(std::vector<int> nums, std::vector<std::vector<int> > queries)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> d(n + 1);
    for (const auto &query : queries)
    {
        ++d[query[0]];
        --d[query[1] + 1];
    }
    for (int i = 0, s = 0; i < n; ++i)
    {
        s += d[i];
        if (nums[i] > s) return false;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::vector<int> > queries,
          bool solution,
          unsigned int trials = 1)
{
    int result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isZeroArray(nums, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 1}, {{0, 2}}, true, trials);
    test({4, 3, 2, 1}, {{1, 3}, {0, 2}}, false, trials);
    return 0;
}


