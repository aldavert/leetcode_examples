#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string minimizeStringValue(std::string s)
{
    std::string result;
    int count_buffer[128] = {};
    auto count = [&count_buffer](char symbol) -> int&
        { return count_buffer[static_cast<int>(symbol)]; };
    std::vector<char> letters;
    for (char c : s)
        count(c) += (c != '?');
    for (char c : s)
    {
        if (c != '?') continue;
        char min_freq_letter = 'a';
        for (char l = 'b'; l <= 'z'; ++l)
            if (count(l) < count(min_freq_letter))
                min_freq_letter = l;
        letters.push_back(min_freq_letter);
        ++count(min_freq_letter);
    }
    std::sort(letters.begin(), letters.end());
    for (int i = 0; char c : s)
        result += (c == '?')?letters[i++] : c;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimizeStringValue(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("???", "abc", trials);
    test("a?a?", "abac", trials);
    return 0;
}


