#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimizedStringLength(std::string s)
{
    int histogram[26] = {};
    int result = 0;
    for (char letter : s)
        result += (++histogram[letter - 'a']) == 1;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimizedStringLength(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aaabc", 3, trials);
    test("cbbd", 3, trials);
    test("dddaaa", 2, trials);
    test("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", 1, trials);
    return 0;
}


