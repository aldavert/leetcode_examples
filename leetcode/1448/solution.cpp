#include "../common/common.hpp"
#include "../common/tree.hpp"


// ############################################################################
// ############################################################################

#if 1
int goodNodes(TreeNode * root)
{
    auto process = [&](auto &&self, TreeNode * ptr, int threshold) -> int
    {
        if (ptr == nullptr) return 0;
        threshold = std::max(threshold, ptr->val);
        return self(self, ptr->left , threshold)
             + self(self, ptr->right, threshold)
             + (ptr->val >= threshold);
    };
    return process(process, root, std::numeric_limits<int>::lowest());
}
#else
int goodNodes(TreeNode * root, int threshold)
{
    if (root == nullptr) return 0;
    threshold = std::max(threshold, root->val);
    return goodNodes(root->left, threshold)
         + goodNodes(root->right, threshold)
         + (root->val >= threshold);
}

int goodNodes(TreeNode * root)
{
    return goodNodes(root, std::numeric_limits<int>::lowest());
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = goodNodes(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1, 4, 3, null, 1, 5}, 4, trials);
    test({3, 3, null, 4, 2}, 3, trials);
    test({1}, 1, trials);
    return 0;
}


