# Count Good Nodes in Binary Tree

Given a binary tree `root`, a node *X* in the tree is named **good** if in the path from root to *X* there are no nodes with a value *greater than* X.

Return the number of **good** nodes in the binary tree.

#### Example 1:
> ```mermaid
> graph TD;
> A((3))---B((1))
> A---C((4))
> B---D((3))
> B---EMPTY(( ))
> C---E((1))
> C---F((5))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef good fill:#ACF,stroke:#000,stroke-width:2px;
> class A,C,D,F good;
> class EMPTY empty;
> linkStyle 3 stroke-width:0px;
> ```
> *Input:* `root = [3, 1, 4, 3, null, 1, 5]`  
> *Output:* `4`  
> *Explanation:* Nodes in blue are **good**.
> - Root Node (3) is always a good node.
> - Node `4 -> (3,4)` is the maximum value in the path starting from the root.
> - Node `5 -> (3,4,5)` is the maximum value in the path
> - Node `3 -> (3,1,3)` is the maximum value in the path.

#### Example 2:
> ```mermaid
> graph TD;
> A((3))---B((3))
> A---EMPTY(( ))
> B---C((4))
> B---D((2))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef good fill:#ACF,stroke:#000,stroke-width:2px;
> class A,B,C good;
> class EMPTY empty;
> linkStyle 1 stroke-width:0px;
> ```
> *Input:* `root = [3, 3, null, 4, 2]`  
> *Output:* `3`  
> *Explanation:* Node `2 -> (3, 3, 2)` is not good, because `3` is higher than it.

#### Example 3:
> *Input:* `root = [1]`  
> *Output:* `1`  
> *Explanation:* Root is considered as **good**.

#### Constrains:
- The number of nodes in the binary tree is in the range `[1, 10^5]`.
- Each node's value is between `[-10^4, 10^4]`.


