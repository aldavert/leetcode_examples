#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > transpose(std::vector<std::vector<int> > matrix)
{
    const size_t n = matrix.size();
    const size_t m = matrix[0].size();
    std::vector<std::vector<int> > result(m, std::vector<int>(n));
    for (size_t i = 0; i < n; ++i)
        for (size_t j = 0; j < m; ++j)
            result[j][i] = matrix[i][j];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = transpose(matrix);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}},
         {{1, 4, 7}, {2, 5, 8}, {3, 6, 9}}, trials);
    test({{1, 2, 3}, {4, 5, 6}},
         {{1, 4}, {2, 5}, {3, 6}}, trials);
    return 0;
}


