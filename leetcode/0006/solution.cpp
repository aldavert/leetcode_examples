#include "../common/common.hpp"

// ############################################################################
// ############################################################################

//// PAYPALISHIRING
//// P   A   H   N
//// A P L S I I G
//// Y   I   R
//// Result: PAHNAPLSIIGYIR
//// PAYPALISHIRING
//// 0---1   2   3
////  4-5·6 7 8 9 A
////   B···C   D
//// Skip = 1 + 2 * (numRows - 2) = 1 + 2 * (3 - 2) = 1 + 2 * 1 = 3
////
//// ======================================================================
////
//// P     I    N
//// A   L S  I G
//// Y A   H R
//// P     I
//// Result: PINALSIGYAHRPI
//// PAYPALISHIRING
//// 0-----1     2 
////  3---4·5   6 7
////   8-9···A B
////    C·····D
//// Skip = 1 + 2 * (numRows - 2) = 1 + 2 * (4 - 2) = 1 + 2 * 2 = 5
std::string convert(std::string s, int numRows)
{
    if (numRows <= 1) return s;
    const int n = static_cast<int>(s.size());
    std::string result(n, ' ');
    for (int i = 0, row = 0; i < n; ++row)
    {
        int skip_first = 2 * (numRows - row - 1), skip_second = 2 * row;
        int idx = row;
        bool use_skip_first = true;
        while ((i < n) && (idx < n))
        {
            result[i++] = s[idx];
            if (!skip_second || use_skip_first)
                idx += skip_first;
            if (!skip_first || !use_skip_first)
                idx += skip_second;
            use_skip_first = !use_skip_first;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int numRows, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = convert(s, numRows);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("PAYPALISHIRING", 3, "PAHNAPLSIIGYIR", trials);
    test("PAYPALISHIRING", 4, "PINALSIGYAHRPI", trials);
    test("A", 1, "A", trials);
    return 0;
}


