#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool parseBoolExpr(std::string expression)
{
    const int n = static_cast<int>(expression.size());
    int p = 0;
    auto closeParenthesis = [&](void) -> void
    {
        for (int close = 0; close >= 0; ++p)
            close += (expression[p] == '(') - (expression[p] == ')');
        --p;
    };
    auto parse = [&](auto &&self) -> bool
    {
        while (p < n)
        {
            if (expression[p] == '!')
            {
                p += 2;
                bool value = self(self);
                ++p;
                return !value;
            }
            else if (expression[p] == '&')
            {
                p += 2;
                bool value = true;
                do
                {
                    value = self(self) && value;
                    if (!value) closeParenthesis();
                } while (expression[p++] == ',');
                return value;
            }
            else if (expression[p] == '|')
            {
                p += 2;
                bool value = false;
                do
                {
                    value = self(self) || value;
                    if (value) closeParenthesis();
                } while (expression[p++] == ',');
                return value;
            }
            else if (expression[p] == 't')
            {
                ++p;
                return true;
            }
            else if (expression[p] == 'f')
            {
                ++p;
                return false;
            }
            else return false;
        }
        return false;
    };
    return parse(parse);
}

// ############################################################################
// ############################################################################

void test(std::string expression, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = parseBoolExpr(expression);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("&(|(f))", false, trials);
    test("|(f,f,f,t)", true, trials);
    test("!(&(f,t))", true, trials);
    test("|(f,!(t),&(t,!(f)))", true, trials);
    test("&(f,!(t),&(t,!(f)))", false, trials);
    test("&(!(f),f,&(t,!(f)))", false, trials);
    test("&(t,t,!(f),t,f,&(t,!(f)))", false, trials);
    test("&(|(f,f,!(f),f,!(t),!&(!(f)),f,t,t),&(t,!(f)))", true, trials);
    test("&(|(f,f,!(f),f,!(t),!(!(f)),f,t,t),&(f,!(f)))", false, trials);
    test("!(!(f))", false, trials);
    test("&(&(f,!(f)),|(f,f,!(f),f,!(t),!&(!(f)),f,t,t))", false, trials);
    test("|(&(f,!(f)),|(f,f,!(f),f,!(t),!&(!(f)),f,t,t))", true, trials);
    test("|(&(&(f,!(f)),|(f,f,!(f),f,!(t),!&(!(f)),f,t,t)))", false, trials);
    test("|(&(&(f,!(f)),|(f,f,!(f),f,!(t),!(!(f)),f,t,t)),t)", true, trials);
    test("|(&(&(f,!(f)),|(f,f,!(f),f,!(t),!&(!(f)),f,t,t)),f)", false, trials);
    return 0;
}


