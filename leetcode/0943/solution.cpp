#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

std::string shortestSuperstring(const std::vector<std::string> &words)
{
    auto wordContained =
        [&](auto &&self, const std::string &a, const std::string &b) -> bool
    {
        if (a.size() > b.size()) return self(self, b, a);
        else
        {
            const int na = static_cast<int>(a.size());
            const int n = static_cast<int>(b.size()) - na;
            for (int i = 0; i <= n; ++i)
                if (a == b.substr(i, na))
                    return true;
            return false;
        }
    };
    auto wordOverlap =
        [&](const std::string &a, const std::string &b, int &da, int &db) -> bool
    {
        if (wordContained(wordContained, a, b)) return true;
        const int na = static_cast<int>(a.size());
        const int nb = static_cast<int>(b.size());
        const int n = std::min(na, nb);
        da = nb;
        db = na;
        for (int k = 1; k <= n; ++k)
        {
            if (a.substr(na - k) == b.substr(0, k))
                da = nb - k;
            if (b.substr(nb - k) == a.substr(0, k))
                db = na - k;
        }
        return false;
    };
    // Special case: single element.
    if (words.size() == 1) return words[0];
    
    // Compute the overlapping between all pair of words.
    int n = static_cast<int>(words.size());
    int graph[20][20] = {};
    bool overlapped[20] = {};
    for(int i = 0; i < n; ++i) graph[i][i] = 0;
    for(int i = 0; i < n - 1; ++i)
        for(int j = i + 1; j < n; ++j)
            overlapped[(words[i].size() > words[j].size())?j:i] |=
                wordOverlap(words[i], words[j], graph[i][j], graph[j][i]);
    int number_to_remove = std::accumulate(overlapped, overlapped + n, 0);
    int word_idx[20];
    if (number_to_remove)
    {
        for (int i = 0, ri = 0; i < n; ++i)
        {
            if (overlapped[i]) continue;
            word_idx[ri] = i;
            for (int j = 0, rj = 0; j < n; ++j)
            {
                if (overlapped[j]) continue;
                graph[ri][rj] = graph[i][j];
                ++rj;
            }
            ++ri;
        }
        n -= number_to_remove;
    }
    else
    {
        for (int i = 0; i < n; ++i)
            word_idx[i] = i;
    }
    const int N = 1 << n;
    
    // Compute the dynamic programing values.
    std::vector<int> parent[20], dp[20];
    for (int i = 0; i < n; ++i)
        parent[i].resize(N),
        dp[i].resize(N);
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            dp[j][i] = std::numeric_limits<int>::max() / 2;
            parent[j][i] = -1;
        }
    }
    
    for (int i = 0; i < n; ++i)
        dp[i][1 << i] = static_cast<int>(words[word_idx[i]].size());
    for (int s = 1; s < N; ++s)
    {
        for (int j = 0; j < n; ++j)
        {
            const int J = 1 << j;
            if (!(s & J)) continue;
            int ps = s & ~J;
            for (int i = 0; i < n; ++i)
            {
                if (dp[j][s] > dp[i][ps] + graph[i][j])
                {
                    dp[j][s] = dp[i][ps] + graph[i][j];
                    parent[j][s] = i;
                }
            }
        }
    }
    
    // Build the string.
    std::string result;
    int idx = 0;
    int min_value = dp[0][N - 1];
    for (int i = 1; i < n; ++i)
    {
        if (dp[i][N - 1] < min_value)
        {
            min_value = dp[i][N - 1];
            idx = i;
        }
    }
    for (int s = N - 1; s > 0; )
    {
        int i = parent[idx][s];
        const std::string &current = words[word_idx[idx]];
        result = ((i < 0)?current
                         :current.substr(current.size() - graph[i][idx])) + result;
        s &= ~(1 << idx);
        idx = i;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestSuperstring(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"alex", "loves", "leetcode"}, "leetcodelovesalex", trials);
    test({"catg", "ctaagt", "gcta", "ttca", "atgcatc"}, "gctaagttcatgcatc", trials);
    test({"dbsh", "dsbbhs", "hdsb", "ssdb", "bshdbsd"}, "hdsbbhssdbshdbsd", trials);
    test({"001", "01101", "010"}, "0011010", trials);
    test({"geeks", "quiz", "for"}, "forquizgeeks", trials);
    test({"barcelona", "rcel"}, "barcelona", trials);
    test({"barcelona", "agbar"}, "agbarcelona", trials);
    test({"agbar", "barcelona"}, "agbarcelona", trials);
    test({"dbsh", "dsbbhs", "hdbs", "hdsb", "ssdb", "bshdbsd"},
         "hdsbbhssdbshdbsd", trials);
    return 0;
}


