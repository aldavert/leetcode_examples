#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int percentageLetter(std::string s, char letter)
{
    size_t frequency = 0;
    for (char chr : s)
        frequency += chr == letter;
    return static_cast<int>(100.0 * static_cast<double>(frequency)
                                  / static_cast<double>(s.size()));
}

// ############################################################################
// ############################################################################

void test(std::string s, char letter, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = percentageLetter(s, letter);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("foobar", 'o', 33, trials);
    test("jjjj", 'k', 0, trials);
    return 0;
}


