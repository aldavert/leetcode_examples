#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int distributeCandies(int n, int limit)
{
    int result = 0;
    for (int i = 0; i <= limit; ++i)
    {
        for (int j = 0; j <= limit; ++j)
        {
            int k = n - i - j;
            result += (k <= limit) && (k >= 0);
        }
    }
    return result;
}
#else
int distributeCandies(int n, int limit)
{
    auto ways = [](int value) -> int
    {
        return (value >= 0)?(value + 2) * (value + 1) / 2:0;
    };
    return ways(n)
         - 3 * ways(n -     limit - 1)
         + 3 * ways(n - 2 * limit - 2)
         -     ways(n - 3 * limit - 3);
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int limit, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = distributeCandies(n, limit);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 2, 3, trials);
    test(3, 3, 10, trials);
    return 0;
}


