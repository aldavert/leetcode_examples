#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxAscendingSum(std::vector<int> nums)
{
    int previous = std::numeric_limits<int>::max(), current = 0, result = 0;
    for (int number : nums)
    {
        if (previous < number)
            current += number;
        else
        {
            result = std::max(result, current);
            current = number;
        }
        previous = number;
    }
    return std::max(current, result);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxAscendingSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 20, 30, 5, 10, 50}, 65, trials);
    test({10, 20, 30, 40, 50}, 150, trials);
    test({12, 17, 15, 13, 10, 11, 12}, 33, trials);
    test({3, 6, 10, 1, 8, 9, 9, 8, 9}, 19, trials);
    return 0;
}


