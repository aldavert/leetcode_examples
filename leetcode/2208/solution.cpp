#include "../common/common.hpp"
#include <numeric>
#include <queue>

// ############################################################################
// ############################################################################

int halveArray(std::vector<int> nums)
{
    const double half_sum = std::accumulate(nums.begin(), nums.end(), 0.0) / 2.0;
    std::priority_queue<double> heap{nums.begin(), nums.end()};
    int result = 0;
    
    for (double running_sum = 0.0; running_sum < half_sum; ++result)
    {
        double max_value = heap.top() / 2;
        running_sum += max_value;
        heap.pop();
        heap.push(max_value);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = halveArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 19, 8, 1}, 3, trials);
    test({3, 8, 20}, 3, trials);
    return 0;
}


