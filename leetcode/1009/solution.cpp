#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int bitwiseComplement(int n)
{
    if (n == 0) return 1;
    constexpr int ONE = 1 << 30;
    int result = 0;
    int bits = 31;
    for (; n; --bits)
    {
        result >>= 1;
        if ((n & 1) == 0)
            result |= ONE;
        n >>= 1;
    }
    return result >> bits;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = bitwiseComplement(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 2, trials);       //   5:      101 -> 010      | 2
    test(175, 80, trials);    // 175: 10101111 -> 01010000 | 80
    test(1, 0, trials);       //   1:        1 -> 0        | 0
    test(10, 5, trials);      //  10:     1010 -> 0101     | 5
    test(7, 0, trials);       //   1:      111 -> 000      | 0
    return 0;
}


