#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long getDescentPeriods(std::vector<int> prices)
{
    long result = 1;
    
    for (int i = 1, dp = 1, n = static_cast<int>(prices.size()); i < n; ++i)
    {
        if (prices[i] == prices[i - 1] - 1) ++dp;
        else dp = 1;
        result += dp;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> prices, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getDescentPeriods(prices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 1, 4}, 7, trials);
    test({8, 6, 7, 7}, 4, trials);
    test({1}, 1, trials);
    return 0;
}


