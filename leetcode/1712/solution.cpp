#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int waysToSplit(std::vector<int> nums)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(nums.size());
    std::vector<int> prefix(n);
    int result = 0;
    
    std::partial_sum(nums.begin(), nums.end(), prefix.begin());
    for (int i = 0, j = 0, k = 0; i < n - 2; ++i)
    {
        j = std::max(j, i + 1);
        while ((j < n - 1) && (prefix[i] > prefix[j] - prefix[i]))
            ++j;
        k = std::max(k, j);
        while ((k < n - 1) && (prefix[k] - prefix[i] <= prefix.back() - prefix[k]))
            ++k;
        result = (result + (k - j)) % MOD;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = waysToSplit(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 1}, 1, trials);
    test({1, 2, 2, 2, 5, 0}, 3, trials);
    test({3, 2, 1}, 0, trials);
    return 0;
}


