#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool makeEqual(std::vector<std::string> words)
{
    const int n = static_cast<int>(words.size());
    int histogram[26] = {};
    for (const auto &word : words)
        for (char c : word)
            ++histogram[static_cast<int>(c - 'a')];
    for (int i = 0; i < 26; ++i)
        if (histogram[i] % n != 0)
            return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = makeEqual(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"abc", "aabc", "bc"}, true, trials);
    test({"ab", "a"}, false, trials);
    return 0;
}


