#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minNumberOfHours(int initialEnergy,
                     int initialExperience,
                     std::vector<int> energy,
                     std::vector<int> experience)
{
    int experience_deficit = 0;
    for (size_t i = 0; i < energy.size(); ++i)
    {
        initialEnergy -= energy[i];
        int delta_experience = std::max(0, experience[i] - initialExperience + 1);
        experience_deficit += delta_experience;
        initialExperience += experience[i] + delta_experience;
    }
    return std::max(0, -(initialEnergy - 1)) + experience_deficit;
}

// ############################################################################
// ############################################################################

void test(int initialEnergy,
          int initialExperience,
          std::vector<int> energy,
          std::vector<int> experience,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minNumberOfHours(initialEnergy, initialExperience, energy, experience);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 3, {1, 4, 3, 2}, {2, 6, 3, 1}, 8, trials);
    test(2, 4, {1}, {3}, 0, trials);
    test(1, 1, {1, 1, 1, 1}, {1, 1, 1, 50}, 51, trials);
    return 0;
}


