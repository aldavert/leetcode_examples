#include "../common/common.hpp"
#include "../common/list.hpp"
#include <stack>

// ############################################################################
// ############################################################################

ListNode * doubleIt(ListNode * head)
{
    std::stack<int> values;
    for (ListNode * node = head; node != nullptr; node = node->next)
        values.push(node->val);
    ListNode * result = nullptr;
    int carry = 0;
    for (; !values.empty(); values.pop())
    {
        result = new ListNode((values.top() * 2 + carry) % 10, result);
        carry = (values.top() * 2 + carry) / 10;
    }
    if (carry > 0)
        result = new ListNode(carry, result);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> list, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(list);
        ListNode * nw_head = doubleIt(head);
        result = list2vec(nw_head);
        delete head;
        delete nw_head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 8, 9}, {3, 7, 8}, trials);
    test({9, 9, 9}, {1, 9, 9, 8}, trials);
    return 0;
}


