#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int distanceSum(int m, int n, int k)
{
    const long MOD = 1'000'000'007;
    auto nCr = [&](int cn, int cr) -> long
    {
        if (cr > cn) return 0;
        long long numerator = 1, denominator = 1;        
        for (int i = 0; i < cr; ++i)
        {
            numerator = (numerator * (cn - i)) % MOD;
            denominator = (denominator * (i + 1)) % MOD;
        }
        long long inverse = 1, base = denominator, power = MOD - 2;
        while (power > 0)
        {
            if (power & 1) inverse = (inverse * base) % MOD;
            base = (base * base) % MOD;
            power >>= 1;
        }
        return (numerator * inverse) % MOD;
    };
    long vertical = 0;
    for (int i = 0; i < n - 1; ++i)
        vertical = (vertical + static_cast<long>(n - i)
                 * static_cast<long>(n - i - 1) / 2) % MOD;
    vertical = (vertical * m * m) % MOD;
    long horizontal = 0;
    for (int i = 0; i < m - 1; ++i)
        horizontal = (horizontal + static_cast<long>(m - i)
                   * static_cast<long>(m - i - 1) / 2) % MOD;
    horizontal = (horizontal * n * n) % MOD;
    return static_cast<int>((nCr(n * m - 2, k - 2) * (vertical + horizontal)) % MOD);
}

// ############################################################################
// ############################################################################

void test(int m, int n, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = distanceSum(m, n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 2, 2,  8, trials);
    test(1, 4, 3, 20, trials);
    return 0;
}


