#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> buildArray(std::vector<int> target, [[maybe_unused]] int n)
{
    std::vector<std::string> result;
    for (int previous = 0; int value : target)
    {
        for (int i = previous + 1; i < value; ++i)
        {
            result.push_back("Push");
            result.push_back("Pop");
        }
        result.push_back("Push");
        previous = value;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> target,
          int n,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = buildArray(target, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3}, 3, {"Push", "Push", "Pop", "Push"}, trials);
    test({1, 2, 3}, 3, {"Push", "Push", "Push"}, trials);
    test({1, 2}, 4, {"Push", "Push"}, trials);
    test({1, 5}, 5, {"Push", "Push", "Pop", "Push", "Pop", "Push", "Pop", "Push"},
         trials);
    test({2, 5}, 5, {"Push", "Pop", "Push", "Push", "Pop", "Push", "Pop", "Push"},
         trials);
    return 0;
}


