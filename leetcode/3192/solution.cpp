#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> nums)
{
    int result = 0;
    for (int target = 1; int num : nums)
    {
        if (num != target)
        {
            target ^= 1;
            ++result;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 1, 0, 1}, 4, trials);
    test({1, 0, 0, 0}, 1, trials);
    return 0;
}


