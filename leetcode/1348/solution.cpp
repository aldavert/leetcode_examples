#include "../common/common.hpp"
#include <unordered_map>
#include <map>

// ############################################################################
// ############################################################################

class TweetCounts
{
    std::unordered_map<std::string, std::map<int, int> > m_tweet_name_to_time_count;
public:
    TweetCounts(void) = default;
    void recordTweet(std::string tweetName, int time)
    {
        ++m_tweet_name_to_time_count[tweetName][time];
    }
    std::vector<int> getTweetCountsPerFrequency(std::string freq,
                                                std::string tweetName,
                                                int startTime,
                                                int endTime)
    {
        const int chunk_size = (freq == "minute")?60:((freq == "hour")?3600:86400);
        std::vector<int> result((endTime - startTime) / chunk_size + 1);
        const std::map<int, int> &tc = m_tweet_name_to_time_count[tweetName];
        for (auto it = tc.lower_bound(startTime), hi = tc.upper_bound(endTime); it != hi; ++it)
            result[(it->first - startTime) / chunk_size] += it->second;
        return result;
    }
};

// ############################################################################
// ############################################################################

struct Input
{
    Input(std::string f, std::string tn, int st, int et) :
        frequency(f), tweet_name(tn), start_time(st), end_time(et) {};
    Input(std::string tn, int t) :
        frequency(""), tweet_name(tn), time(t), end_time(-1) {};
    std::string frequency;
    std::string tweet_name;
    union
    {
        int start_time;
        int time;
    };
    int end_time;
};

void test(std::vector<bool> record,
          std::vector<Input> input,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        TweetCounts obj;
        result.clear();
        for (size_t i = 0; i < input.size(); ++i)
        {
            if (record[i])
            {
                obj.recordTweet(input[i].tweet_name, input[i].time);
                result.push_back({});
            }
            else result.push_back(obj.getTweetCountsPerFrequency(input[i].frequency,
                        input[i].tweet_name, input[i].start_time, input[i].end_time));
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({true, true, true, false, false, true, false},
         {{"tweet3", 0}, {"tweet3", 60}, {"tweet3", 10}, {"minute", "tweet3", 0, 59},
          {"minute", "tweet3", 0, 60}, {"tweet3", 120}, {"hour", "tweet3", 0, 210}},
         {{}, {}, {}, {2}, {2, 1}, {}, {4}},
         trials);
    return 0;
}


