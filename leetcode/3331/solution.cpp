#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

std::vector<int> findSubtreeSizes(std::vector<int> parent, std::string s)
{
    std::vector<std::vector<int> > children(parent.size());
    std::vector<int> result(parent.size());
    int closest_parent[26] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                              -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                              -1, -1, -1, -1, -1, -1};
    int root = -1;
    for (int node = 0, n = static_cast<int>(parent.size()); node < n; ++node)
    {
        if (parent[node] != -1)
            children[parent[node]].push_back(node);
        else root = node;
    }
    auto dfs = [&](auto &&self, int node) -> int
    {
        int &actual_parent = closest_parent[s[node] - 'a'];
        result[node] = 1;
        int previous = std::exchange(actual_parent, node);
        for (int child : children[node])
            result[node] += self(self, child);
        actual_parent = previous;
        if ((actual_parent != -1)
        &&  (s[actual_parent] == s[node]))
        {
            result[actual_parent] += result[node];
            return 0;
        }
        return result[node];
    };
    dfs(dfs, root);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> parents,
          std::string s,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findSubtreeSizes(parents, s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, 0, 0, 1, 1, 1}, "abaabc", {6, 3, 1, 1, 1, 1}, trials);
    test({-1, 0, 4, 0, 1}, "abbba", {5, 2, 1, 1, 1}, trials);
    test({-1, 12, 10, 2, 3, 3, 2, 5, 6, 10, 0, 6, 2}, "dccdbececbccb",
         {13, 1, 6, 4, 1, 2, 3, 1, 1, 1, 8, 1, 1}, trials);
    return 0;
}


