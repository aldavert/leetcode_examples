#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countSubIslands(std::vector<std::vector<int> > grid1,
                    std::vector<std::vector<int> > grid2)
{
    const int n = static_cast<int>(grid2.size()),
              m = static_cast<int>(grid2[0].size());
    auto dfs = [&](auto &&self, int i, int j)
    {
        if ((i < 0) || (i == n) || (j < 0) || (j == m)) return 1;
        if (grid2[i][j] != 1) return 1;
        grid2[i][j] = 2;
        return grid1[i][j] & self(self, i + 1, j) & self(self, i - 1, j)
             & self(self, i, j + 1) & self(self, i, j - 1);
    };
    int result = 0;
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < m; ++j)
            if (grid2[i][j] == 1)
                result += dfs(dfs, i, j);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid1,
          std::vector<std::vector<int> > grid2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSubIslands(grid1, grid2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 1, 0, 0},
          {0, 1, 1, 1, 1},
          {0, 0, 0, 0, 0},
          {1, 0, 0, 0, 0},
          {1, 1, 0, 1, 1}},
         {{1, 1, 1, 0, 0},
          {0, 0, 1, 1, 1},
          {0, 1, 0, 0, 0},
          {1, 0, 1, 1, 0},
          {0, 1, 0, 1, 0}}, 3, trials);
    test({{1, 0, 1, 0, 1},
          {1, 1, 1, 1, 1},
          {0, 0, 0, 0, 0},
          {1, 1, 1, 1, 1},
          {1, 0, 1, 0, 1}},
         {{0, 0, 0, 0, 0},
          {1, 1, 1, 1, 1},
          {0, 1, 0, 1, 0},
          {0, 1, 0, 1, 0},
          {1, 0, 0, 0, 1}}, 2, trials);
    return 0;
}


