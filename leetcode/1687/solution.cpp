#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int boxDelivering(std::vector<std::vector<int> > boxes,
                  int /*portsCount*/,
                  int maxBoxes,
                  int maxWeight)
{
    const int n = static_cast<int>(boxes.size());
    std::vector<int> dp(n + 1);
    
    for (int l = 0, r = 0, trips = 2, weight = 0; r < n; ++r)
    {
        weight += boxes[r][1];
        trips += ((r > 0) && (boxes[r][0] != boxes[r - 1][0]));

        while ((r - l + 1 > maxBoxes) || (weight > maxWeight)
           ||  ((l < r) && (dp[l + 1] == dp[l])))
        {
            weight -= boxes[l][1];
            trips -= (boxes[l][0] != boxes[l + 1][0]);
            ++l;
        }
        dp[r + 1] = dp[l] + trips;
    }
    return dp[n];
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > boxes,
          int portsCount,
          int maxBoxes,
          int maxWeight,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = boxDelivering(boxes, portsCount, maxBoxes, maxWeight);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1}, {2, 1}, {1, 1}}, 2, 3, 3, 4, trials);
    test({{1, 2}, {3, 3}, {3, 1}, {3, 1}, {2, 4}}, 3, 3, 6, 6, trials);
    test({{1, 4}, {1, 2}, {2, 1}, {2, 1}, {3, 2}, {3, 4}}, 3, 6, 7, 6, trials);
    return 0;
}


