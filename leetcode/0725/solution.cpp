#include "../common/common.hpp"
#include "../common/list.hpp"

// #############################################################################
// #############################################################################

std::vector<ListNode *> splitListToParts(ListNode* head, int k)
{
    std::vector<ListNode *> result(k, nullptr);
    int length = 0;
    for (ListNode * ptr = head; ptr != nullptr; ptr = ptr->next, ++length);
    const int loop_long = length % k;
    const int loop_length = length / k;
    for (int i = 0; i < loop_long; ++i)
    {
        result[i] = head;
        ListNode * previous = nullptr;
        for (int j = 0; j <= loop_length; ++j)
        {
            previous = head;
            head = head->next;
        }
        previous->next = nullptr;
    }
    for (int i = loop_long; i < k; ++i)
    {
        result[i] = head;
        ListNode * previous = nullptr;
        for (int j = 0; j < loop_length; ++j)
        {
            previous = head;
            head = head->next;
        }
        if (previous != nullptr)
            previous->next = nullptr;
    }
    return result;
}

// #############################################################################
// #############################################################################

void test(std::vector<int> input,
          int k,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * linput = vec2list(input);
        std::vector<ListNode * > ln_result = splitListToParts(linput, k);
        const int m = static_cast<int>(ln_result.size());
        result.resize(m);
        for (int j = 0; j < m; ++j)
        {
            result[j] = list2vec(ln_result[j]);
            delete ln_result[j];
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 5, {{1}, {2}, {3}, {}, {}}, trials);
    test({1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 3, {{1, 2, 3, 4}, {5, 6, 7}, {8, 9, 10}}, trials);
    return 0;
}


