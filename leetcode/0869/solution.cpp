#include "../common/common.hpp"

void generateTable(void)
{
    int number = 1;
    std::vector<size_t> length;
    std::cout << "static const char power_table[][10] = {\n";
    for (int i = 0; i < 31; ++i, number *= 2)
    {
        char numbers[10] = {};
        size_t number_of_digits = 0;
        for (int num = number; num; num /= 10, ++number_of_digits)
            ++numbers[num % 10];
        length.push_back(number_of_digits);
        if (i) std::cout << ",\n";
        std::cout << "                {" << static_cast<int>(numbers[0]);
        for (int j = 1; j < 10; ++j)
            std::cout << ", " << static_cast<int>(numbers[j]);
        std::cout << "}";
    }
    std::cout << "};\n";
    std::cout << "static const char power_ndigits[] = {" << length[0];
    for (size_t i = 1; i < length.size(); ++i)
        std::cout << ", " << length[i];
    std::cout << "};\n";
}

// ############################################################################
// ############################################################################

bool reorderedPowerOf2(int n)
{
    static const char power_table[][10] = {
                    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
                    {0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
                    {0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
                    {0, 1, 0, 0, 0, 0, 1, 0, 0, 0},
                    {0, 0, 1, 1, 0, 0, 0, 0, 0, 0},
                    {0, 0, 0, 0, 1, 0, 1, 0, 0, 0},
                    {0, 1, 1, 0, 0, 0, 0, 0, 1, 0},
                    {0, 0, 1, 0, 0, 1, 1, 0, 0, 0},
                    {0, 1, 1, 0, 0, 1, 0, 0, 0, 0},
                    {1, 1, 1, 0, 1, 0, 0, 0, 0, 0},
                    {1, 0, 1, 0, 1, 0, 0, 0, 1, 0},
                    {1, 0, 0, 0, 1, 0, 1, 0, 0, 1},
                    {0, 1, 1, 0, 0, 0, 0, 0, 1, 1},
                    {0, 1, 0, 1, 1, 0, 1, 0, 1, 0},
                    {0, 0, 1, 1, 0, 0, 1, 1, 1, 0},
                    {0, 0, 0, 1, 0, 2, 2, 0, 0, 0},
                    {1, 2, 1, 1, 0, 0, 0, 1, 0, 0},
                    {0, 1, 2, 0, 2, 0, 1, 0, 0, 0},
                    {0, 0, 2, 0, 1, 1, 0, 0, 2, 0},
                    {1, 1, 0, 0, 1, 1, 1, 1, 1, 0},
                    {1, 1, 2, 0, 0, 1, 0, 1, 0, 1},
                    {1, 1, 0, 1, 3, 0, 0, 0, 0, 1},
                    {1, 0, 0, 1, 0, 0, 1, 0, 4, 0},
                    {0, 2, 1, 0, 0, 0, 2, 3, 0, 0},
                    {0, 0, 1, 3, 2, 2, 0, 0, 0, 0},
                    {1, 1, 0, 0, 1, 0, 2, 1, 2, 0},
                    {0, 2, 2, 1, 1, 0, 0, 2, 1, 0},
                    {0, 0, 1, 1, 2, 2, 2, 0, 1, 0},
                    {1, 1, 1, 1, 0, 1, 1, 1, 1, 1},
                    {1, 2, 1, 1, 2, 0, 0, 2, 1, 0}};
    static const char power_ndigits[] = {1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4,
                        4, 5, 5, 5, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 9, 9, 9, 10};
    char numbers[10] = {};
    char number_of_digits = 0;
    for (int num = n; num; num /= 10, ++number_of_digits)
        ++numbers[num % 10];
    for (unsigned int i = 0; i < 31; ++i)
    {
        if (number_of_digits == power_ndigits[i])
        {
            bool same = true;
            for (unsigned int j = 0; j < 10; ++j)
                same = same && (power_table[i][j] == numbers[j]);
            if (same) return true;
        }
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(int n, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = reorderedPowerOf2(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test( 1,  true, trials);
    test(10, false, trials);
    return 0;
}


