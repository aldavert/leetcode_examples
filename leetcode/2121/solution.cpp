#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<long long> getDistances(std::vector<int> arr)
{
    const int n = static_cast<int>(arr.size());
    std::vector<long> prefix(n), suffix(n);
    std::vector<long long> result(n);
    std::unordered_map<int, std::vector<int> > num_to_indices;
    
    for (int i = 0; i < n; ++i)
        num_to_indices[arr[i]].push_back(i);
    for (const auto& [_, indices] : num_to_indices)
    {
        const int m = static_cast<int>(indices.size());
        for (int i = 1; i < m; ++i)
            prefix[indices[i]] += prefix[indices[i - 1]]
                               + i * (indices[i] - indices[i - 1]);
        for (int i = m - 2; i >= 0; --i)
            suffix[indices[i]] += suffix[indices[i + 1]]
                               + (m - i - 1) * (indices[i + 1] - indices[i]);
    }
    for (int i = 0; i < n; ++i)
        result[i] = prefix[i] + suffix[i];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, std::vector<long long> solution, unsigned int trials = 1)
{
    std::vector<long long> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getDistances(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3, 1, 2, 3, 3}, {4, 2, 7, 2, 4, 4, 5}, trials);
    test({10, 5, 10, 10}, {5, 0, 3, 4}, trials);
    return 0;
}


