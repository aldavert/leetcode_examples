#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> sortItems(int n,
                           int m,
                           std::vector<int> group,
                           std::vector<std::vector<int> > beforeItems)
{
    std::vector<std::vector<int> > graph(n + m);
    std::vector<int> in_degree(n + m);

    for (size_t i = 0; i < group.size(); ++i)
    {
        if (group[i] == -1) continue;
        graph[group[i] + n].push_back(static_cast<int>(i));
        ++in_degree[i];
    }
    for (size_t i = 0; i < beforeItems.size(); ++i)
    {
        for (const int b : beforeItems[i])
        {
            const int u = (group[b] == -1)?b:(group[b] + n);
            const int v = (group[i] == -1)?static_cast<int>(i):(group[i] + n);
            if (u == v)
            {
                graph[b].push_back(static_cast<int>(i));
                ++in_degree[i];
            }
            else
            {
                graph[u].push_back(v);
                ++in_degree[v];
            }
        }
    }
    std::vector<int> result;
    auto dp_process = [&](auto &&self, int u) -> void
    {
        if (u < n) result.push_back(u);
        in_degree[u] = -1;
        for (const int v : graph[u])
            if (--in_degree[v] == 0)
                self(self, v);
    };
    for (int i = 0; i < n + m; ++i)
        if (in_degree[i] == 0) dp_process(dp_process, i);
    return (static_cast<int>(result.size()) == n)?result:std::vector<int>();
}

// ############################################################################
// ############################################################################

void test(int n,
          int m,
          std::vector<int> group,
          std::vector<std::vector<int> > beforeItems,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sortItems(n, m, group, beforeItems);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(8, 2, {-1, -1, 1, 0, 0, 1, 0, -1}, {{}, {6}, {5}, {6}, {3, 6}, {}, {}, {}},
         {0, 7, 6, 3, 4, 1, 5, 2}, trials);
    test(8, 2, {-1, -1, 1, 0, 0, 1, 0, -1}, {{}, {6}, {5}, {6}, {3}, {}, {4}, {}},
         {}, trials);
    return 0;
}


