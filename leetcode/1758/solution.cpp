#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minOperations(std::string s)
{
    const int n = static_cast<int>(s.size());
    int result = 0;
    for (int i = 0; i < n; ++i)
        result += ('0' + (i & 1)) == s[i];
    return std::min(result, n - result);
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("0100", 1, trials);
    test("10", 0, trials);
    test("1111", 2, trials);
    test("11110", 2, trials);
    test("11111", 2, trials);
    test("10010100", 3, trials);
    return 0;
}


