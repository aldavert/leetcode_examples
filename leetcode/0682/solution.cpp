#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int calPoints(std::vector<std::string> ops)
{
    std::vector<int> record;
    record.reserve(ops.size());
    for (const std::string &value : ops)
    {
        switch (value[0])
        {
        case '+':
            if (record.size() >= 2)
                record.push_back(record.back() + record[record.size() - 2]);
            break;
        case 'D':
            if (!record.empty())
                record.push_back(record.back() * 2);
            break;
        case 'C':
            if (!record.empty())
                record.pop_back();
            break;
        default:
            record.push_back(std::stoi(value));
            break;
        }
    }
    return std::accumulate(record.begin(), record.end(), 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> ops, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = calPoints(ops);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"5", "2", "C", "D", "+"}, 30, trials);
    test({"5", "-2", "4", "C", "D", "9", "+", "+"}, 27, trials);
    test({"1"}, 1, trials);
    return 0;
}


