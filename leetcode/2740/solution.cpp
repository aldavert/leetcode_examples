#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findValueOfPartition(std::vector<int> nums)
{
    int result = std::numeric_limits<int>::max();
    std::sort(nums.begin(), nums.end());
    for (int i = 1, n = static_cast<int>(nums.size()); i < n; ++i)
        result = std::min(result, nums[i] - nums[i - 1]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findValueOfPartition(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 4}, 1, trials);
    test({100, 1, 10}, 9, trials);
    return 0;
}


