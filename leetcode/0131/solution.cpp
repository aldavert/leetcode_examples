#include "../common/common.hpp"
#include <set>

// ############################################################################
// ############################################################################

#if 1
std::vector<std::vector<std::string> > partition(std::string s)
{
    std::vector<std::vector<std::string> > result;
    std::vector<std::string> current;
    const int n = static_cast<int>(s.size());
    bool dp[16][16] = {};
    
    auto dfs = [&](auto &&self, int start) -> void
    {
        if (start >= n) result.emplace_back(current);
        for (int i = start; i < n; ++i)
        {
            if ((s[start] == s[i]) && ((i - start <= 2) || dp[start + 1][i - 1]))
            {
                dp[start][i] = true;
                current.emplace_back(s.substr(start, i - start + 1));
                self(self, i + 1);
                current.pop_back();
            }
        }
    };
    
    dfs(dfs, 0);
    return result;
}
#else
std::vector<std::vector<std::string> > partition(std::string s)
{
    int n = static_cast<int>(s.size());
    bool dp[16][16] = {};
    std::vector<std::vector<std::string> > mp[16];
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j <= i; ++j)
        {
            std::string str = s.substr(j, i - j + 1);
            if ((s[j] == s[i]) && ((i - j < 2) || dp[j + 1][i - 1]))
            {
                dp[j][i] = true;
                if (j == 0) mp[i].push_back({str});
                else
                {
                    for (const auto &ss : mp[j - 1])
                    {
                        mp[i].push_back(ss);
                        mp[i].back().push_back(str);
                    }
                }
            }
        }
    }
    return mp[n - 1];
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<std::string> > &left,
                const std::vector<std::vector<std::string> > &right)
{
    std::set<std::vector<std::string> > lut;
    for (const auto &l : left)
        lut.insert(l);
    for (const auto &r : right)
    {
        if (auto search = lut.find(r); search != lut.end())
            lut.erase(search);
        else return false;
    }
    return lut.empty();
}

void test(std::string s,
          std::vector<std::vector<std::string> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<std::string> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = partition(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aab", {{"a", "a", "b"}, {"aa", "b"}}, trials);
    test("a", {{"a"}}, trials);
    return 0;
}


