#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> sortArray(std::vector<int> nums)
{
    std::make_heap(nums.begin(), nums.end());
    for (size_t i = nums.size(); i > 0; --i)
        std::pop_heap(&nums[0], &nums[i]);
    return nums;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sortArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 2, 3 ,1}, {1, 2, 3, 5}, trials);
    test({5, 1, 1, 2, 0, 0}, {0, 0, 1, 1, 2, 5}, trials);
    return 0;
}


