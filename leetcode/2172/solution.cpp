#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

#if 1
int maximumANDSum(std::vector<int> nums, int numSlots)
{
    int m = static_cast<int>(std::pow(3, numSlots)) - 1;
    std::vector<int> dp(m + 1, -1);
    auto process = [&](auto &&self, int i, int mask) -> int
    {
        if (dp[mask] != -1) return dp[mask];
        if (i < 0) return 0;
        int result = 0;
        for (int slot = 1, bit = 1; slot <= numSlots; ++slot, bit *= 3)
            if (mask / bit % 3 > 0)
                result = std::max(result,
                                  (nums[i] & slot) + self(self, i - 1, mask - bit));
        return dp[mask] = result;
    };
    return process(process, static_cast<int>(nums.size()) - 1, m);
}
#else
int maximumANDSum(std::vector<int> nums, int numSlots)
{
    const int m = static_cast<int>(nums.size());
    const int n = 2 * numSlots;
    const int n_selected = 1 << n;
    auto get = [&](int i) -> int { return (i < m)?nums[i]:0; };
    std::vector<int> dp(n_selected);
    
    for (int mask = 1; mask < n_selected; ++mask)
    {
        int selected = __builtin_popcount(mask);
        int slot = (selected + 1) / 2;
        for (int i = 0; i < n; ++i)
            if (mask >> i & 1)
                dp[mask] = std::max(dp[mask], dp[mask ^ 1 << i] + (slot & get(i)));
    }
    return dp.back();
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int numSlots, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumANDSum(nums, numSlots);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6}, 3, 9, trials);
    test({1, 3, 10, 4, 7, 1}, 9, 24, trials);
    return 0;
}


