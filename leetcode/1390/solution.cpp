#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sumFourDivisors(std::vector<int> nums)
{
    int result = 0;
    for (int num : nums)
    {
        int divisor = 0;
        for (int i = 2; i * i <= num; ++i)
        {
            if (num % i == 0)
            {
                if (divisor == 0)
                    divisor = i;
                else
                {
                    divisor = 0;
                    break;
                }
            }
        }
        if ((divisor > 0) && (divisor * divisor < num))
            result += 1 + num + divisor + num / divisor;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumFourDivisors(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({21, 4, 7}, 32, trials);
    test({21, 21}, 64, trials);
    test({1, 2, 3, 4, 5}, 0, trials);
    return 0;
}


