#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string alphabetBoardPath(std::string target)
{
    std::string result;
    int x = 0, y = 0;
    for (char c : target)
    {
        int new_x = (c - 'a') % 5;
        int new_y = (c - 'a') / 5;
        result += std::string(std::max(0, y - new_y), 'U')
               +  std::string(std::max(0, new_x - x), 'R')
               +  std::string(std::max(0, x - new_x), 'L')
               +  std::string(std::max(0, new_y - y), 'D')
               + '!';
        x = new_x;
        y = new_y;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string target, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = alphabetBoardPath(target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leet", "RDD!UURRR!!DDD!", trials);
    test("code", "RR!RRDD!UUL!R!", trials);
    return 0;
}


