#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxValue(int n, int index, int maxSum)
{
    auto check = [&](long value) -> long
    {
        long left_strip = std::max(value - index - 1, 0L),
            right_strip = std::max(value - (n - index), 0L);
        return value * (value + 1) / 2 + value * (value - 1) / 2
             - left_strip * (left_strip + 1) / 2
             - right_strip * (right_strip + 1) / 2
             + std::max(index + 1 - value, 0L)
             + std::max(n - index - value, 0L);
    };
    long left = 1, right = maxSum;
    while (left < right)
    {
        long mid = (left + right) / 2;
        if (check(mid) <= maxSum) left = mid + 1;
        else right = mid - 1;
    }
    return static_cast<int>((check(left) <= maxSum)?left:(left - 1));
}

// ############################################################################
// ############################################################################

void test(int n, int index, int maxSum, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxValue(n, index, maxSum);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 2, 6, 2, trials);
    test(6, 1, 10, 3, trials);
    test(3, 1, 10, 4, trials);
    test(4, 0, 4, 1, trials);
    test(9, 0, 90'924'720, 10'102'750, trials);
    return 0;
}


