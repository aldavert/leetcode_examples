#include "../common/common.hpp"
#include <cmath>
#include <cstring>

// ############################################################################
// ############################################################################

#if 1
int getMaxGridHappiness(int m, int n, int introvertsCount, int extrovertsCount)
{
    constexpr int cross[3][3] = {{0, 0, 0}, {0, -60, -10}, {0, -10, 40}};
    if (m < n) std::swap(m, n);
    const int N = static_cast<int>(std::pow(3, n)), N_3 = N / 3;
    int f[26][8][8][243];
    std::memset(f, 0x80, sizeof(f));
    f[0][0][0][0] = 0;
    
    for (int pos = 0; pos < m * n; ++pos)
    {
        for (int mask = 0; mask < N; ++mask)
        {
            int left = mask % 3, up = mask / N_3;
            if (pos % n == 0) left = 0;
            for (int used1 = 0; used1 <= introvertsCount; ++used1)
            {
                for (int used2 = 0; used2 <= extrovertsCount; ++used2)
                {
                    if (used1 < introvertsCount)
                    {
                        int &to_update = f[pos+1][used1+1][used2][(mask * 3 + 1) % N];
                        to_update = std::max(to_update, f[pos][used1][used2][mask]
                                                      + 120 + cross[left][1]
                                                      + cross[up][1]);
                    }
                    if (used2 < extrovertsCount)
                    {
                        int &to_update = f[pos+1][used1][used2+1][(mask * 3 + 2) % N];
                        to_update = std::max(to_update, f[pos][used1][used2][mask]
                                                      + 40 + cross[left][2]
                                                      + cross[up][2]);
                    }
                    int &to_update = f[pos+1][used1][used2][(mask * 3) % N];
                    to_update = std::max(to_update, f[pos][used1][used2][mask]);
                }
            }
        }
    }
    
    int result = 0;
    for (int used1 = 0; used1 <= introvertsCount; ++used1)
        for (int used2 = 0; used2 <= extrovertsCount; ++used2)
            for (int mask = 0; mask < N; ++mask)
                result = std::max(result, f[m * n][used1][used2][mask]);
    return result;
}
#else
int getMaxGridHappiness(int m, int n, int introvertsCount, int extrovertsCount)
{
    constexpr int MAXSTATES = 243, init[] = {0, 120, 40}, gain[] = {0, -30, 20};
    int dp[6][6][7][7][MAXSTATES];
    std::memset(dp, -1, sizeof(dp));
    auto get = [](int val, int i) { return val / static_cast<int>(std::pow(3, i)) % 3; };
    auto update = [](int val, int s) { return (val * 3 + s) % MAXSTATES; };
    
    std::function<int(int, int, int, int, int)> dfs =
        [&](int x, int y, int in, int ex, int mask) -> int
    {
        if (in == 0 && ex == 0) return 0;
        if (x == n) { x = 0; ++y; }
        if (y == m) return 0;
        int &result = dp[x][y][in][ex][mask];
        if (result != -1) return result;
        
        result = dfs(x + 1, y, in, ex, update(mask, 0));
        std::vector<int> counts{0, in, ex};
        int up = get(mask, n - 1);
        int left = get(mask, 0);
        for (int i = 1; i <= 2; ++i)
        {
            if (counts[i] == 0) continue;
            int s = init[i];
            if (y - 1 >= 0 && up  ) s += gain[i] + gain[up];
            if (x - 1 >= 0 && left) s += gain[i] + gain[left];
            int delta = dfs(x + 1, y, in - (i == 1), ex - (i == 2), update(mask, i));
            result = std::max(result, s + delta);
        }
        return result;
    };
    return dfs(0, 0, introvertsCount, extrovertsCount, 0);
}
#endif

// ############################################################################
// ############################################################################

void test(int m,
          int n,
          int introvertCount,
          int extrovertCount,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getMaxGridHappiness(m, n, introvertCount, extrovertCount);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 3, 1, 2, 240, trials);
    test(3, 1, 2, 1, 260, trials);
    test(2, 2, 4, 0, 240, trials);
    return 0;
}


