#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> largestDivisibleSubset(std::vector<int> nums)
{
    std::sort(nums.begin(), nums.end());
    const int n = static_cast<int>(nums.size());

    std::vector<int> counter(n, 1), previous(n, -1);
    int max_ind = 0;
    for (int i = 1; i < n; ++i)
    {
        for (int j = 0; j < i; ++j)
        {
            if (nums[i] % nums[j] == 0)
            {
                if (counter[i] < counter[j] + 1)
                {
                    counter[i] = counter[j] + 1;
                    previous[i] = j;
                }
            }
        }
        if (counter[max_ind] < counter[i])
            max_ind = i;
    }
    
    int rsize = 0;
    for (int i = max_ind; i >= 0; i = previous[i], ++rsize);
    std::vector<int> result(rsize);
    for (int i = max_ind, j = rsize - 1; i >= 0; i = previous[i], --j)
        result[j] = nums[i];
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<int> &right)
{
    for (const auto &v : left)
        if (v == right) return true;
    return false;
}

void test(std::vector<int> nums,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestDivisibleSubset(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, {{1, 2}, {1, 3}}, trials);
    test({1, 2, 4, 8}, {{1, 2, 4, 8}}, trials);
    return 0;
}


