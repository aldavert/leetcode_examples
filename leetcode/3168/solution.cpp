#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumChairs(std::string s)
{
    int result = 0;
    for (int chairs = 0; char c : s)
    {
        chairs += c == 'E' ? 1 : -1;
        result = std::max(result, chairs);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumChairs(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("EEEEEEE", 7, trials);
    test("ELELEEL", 2, trials);
    test("ELEELEELLL", 3, trials);
    return 0;
}


