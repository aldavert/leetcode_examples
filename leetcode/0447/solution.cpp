#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int numberOfBoomerangs(std::vector<std::vector<int> > points)
{
    const size_t n = points.size();
    if (n <= 1) return 0;
    std::vector<std::vector<int> > distances(n);
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = i + 1; j < n; ++j)
        {
            int distance = (points[i][0] - points[j][0]) * (points[i][0] - points[j][0])
                         + (points[i][1] - points[j][1]) * (points[i][1] - points[j][1]);
            distances[i].push_back(distance);
            distances[j].push_back(distance);
        }
        std::sort(distances[i].begin(), distances[i].end());
    }
    int result = 0;
    for (size_t i = 0; i < n; ++i)
    {
        int last = distances[i][0], count = 1;
        for (size_t j = 1; j < n - 1; ++j)
        {
            if (distances[i][j] == last) ++count;
            else
            {
                result += count * (count - 1);
                count = 1;
                last = distances[i][j];
            }
        }
        result += count * (count - 1);
    }
    return result;
}
#elif 0
int numberOfBoomerangs(std::vector<std::vector<int> > points)
{
    const size_t n = points.size();
    std::vector<std::unordered_map<int, int> > distances(n);
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = 0; j < n; ++j)
        {
            if (i == j) continue;
            int distance = (points[i][0] - points[j][0]) * (points[i][0] - points[j][0])
                         + (points[i][1] - points[j][1]) * (points[i][1] - points[j][1]);
            ++distances[i][distance];
        }
    }
    int result = 0;
    for (size_t i = 0; i < n; ++i)
        for (auto [dist, frequency] : distances[i])
            result += frequency * (frequency - 1);
    return result;
}
#else
int numberOfBoomerangs(std::vector<std::vector<int> > points)
{
    int result = 0;
    for (size_t i = 0; i < points.size(); ++i)
    {
        for (size_t j = 0; j < points.size(); ++j)
        {
            if (i == j) continue;
            int distance_first = (points[i][0] - points[j][0]) * (points[i][0] - points[j][0])
                               + (points[i][1] - points[j][1]) * (points[i][1] - points[j][1]);
            for (size_t k = 0; k < points.size(); ++k)
            {
                if ((i == k) || (j == k)) continue;
                int distance_second = (points[i][0] - points[k][0]) * (points[i][0] - points[k][0])
                                    + (points[i][1] - points[k][1]) * (points[i][1] - points[k][1]);
                result += distance_first == distance_second;
            }
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfBoomerangs(points);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0}, {1, 0}, {2, 0}}, 2, trials);
    test({{1, 1}, {2, 2}, {3, 3}}, 2, trials);
    test({{1, 1}}, 0, trials);
    test({{0, 0}, {1, 0}, {-1, 0}, {0, 1}, {0, -1}}, 20, trials);
    test({{0, 0}, {1, 0}, {-1, 0}, {0, 1}}, 8, trials);
    return 0;
}


