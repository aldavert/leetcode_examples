#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int maxCoins(std::vector<int> piles)
{
    std::sort(piles.begin(), piles.end());
    int result = 0;
    for (size_t l = 0, r = piles.size() - 1; l < r; r -= 2, ++l)
        result += piles[r - 1];
    return result;
}
#else
int maxCoins(std::vector<int> piles)
{
    int histogram[10'001] = {};
    for (int pile : piles) ++histogram[pile];
    int last = 10'000, result = 0;
    for (size_t n = piles.size() / 3; n > 0; --n)
    {
        while ((last > 0) && !histogram[last]) --last;
        --histogram[last];
        while ((last > 0) && !histogram[last]) --last;
        --histogram[last];
        result += last;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> piles, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxCoins(piles);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4, 1, 2, 7, 8}, 9, trials);
    test({2, 4, 5}, 4, trials);
    test({9, 8, 7, 6, 5, 1, 2, 3, 4}, 18, trials);
    return 0;
}


