#include "../common/common.hpp"
#include <list>
#include <unordered_map>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class LRUCache
{
    std::unordered_map<int, std::tuple<int, std::list<int>::iterator> > m_data;
    std::list<int> m_list;
    size_t m_capacity;
public:
    LRUCache(int capacity) : m_capacity(capacity) {}
    int get(int key)
    {
        if (const auto search = m_data.find(key); search != m_data.end())
        {
            m_list.erase(std::get<1>(search->second));
            std::get<1>(search->second) = m_list.insert(m_list.end(), key);
            return std::get<0>(search->second);
        }
        else return -1;
    }
    void put(int key, int value)
    {
        auto search = m_data.find(key);
        if (search != m_data.end())
        {
            m_list.erase(std::get<1>(search->second));
            search->second = {value, m_list.insert(m_list.end(), key)};
            return;
        }
        if (m_data.size() >= m_capacity)
        {
            m_data.erase(*m_list.begin());
            m_list.pop_front();
        }
        m_data[key] = {value, m_list.insert(m_list.end(), key)};
    }
};

// ############################################################################
// ############################################################################

void test(int capacity,
          std::vector<bool> put,
          std::vector<std::vector<int> > parameters,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        LRUCache object(capacity);
        for (size_t i = 0; i < put.size(); ++i)
        {
            if (put[i])
            {
                result.push_back(null);
                object.put(parameters[i][0], parameters[i][1]);
            }
            else result.push_back(object.get(parameters[i][0]));
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {true, true, false, true, false, true, false, false, false},
         {{1, 1}, {2, 2}, {1}, {3, 3}, {2}, {4, 4}, {1}, {3}, {4}},
         {null, null, 1, null, -1, null, -1, 3, 4}, trials);
    test(2, {true, true, false, true, true, false, true, false, false, false},
         {{1, 1}, {2, 2}, {1}, {2, 5}, {3, 3}, {2}, {4, 4}, {1}, {3}, {4}},
         {null, null, 1, null, null, 5, null, -1, -1, 4}, trials);
    test(2, {true, true, false, true, true, false, false, true, false, false, false},
         {{1, 1}, {2, 2}, {1}, {2, 5}, {3, 3}, {2}, {3}, {4, 4}, {1}, {3}, {4}},
         {null, null, 1, null, null, 5, 3, null, -1, 3, 4}, trials);
    return 0;
}


