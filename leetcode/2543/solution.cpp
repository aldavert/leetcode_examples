#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

bool isReachable(int targetX, int targetY)
{
    return __builtin_popcount(std::gcd(targetX, targetY)) == 1;
}

// ############################################################################
// ############################################################################

void test(int targetX, int targetY, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isReachable(targetX, targetY);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, 9, false, trials);
    test(4, 7, true, trials);
    return 0;
}


