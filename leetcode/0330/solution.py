def minPatches(nums, n):
    reach, ans, idx = 0, 0, 0
    while reach < n:
        if idx < len(nums) and nums[idx] <= reach + 1:
            reach += nums[idx]
            idx += 1
        else:
            ans += 1
            reach = 2*reach + 1       
            
    return ans

def test(nums, n, solution):
    result = minPatches(nums, n);
    print(f'[{"SUCCESS" if result == solution else "FAILURE"}] minPaches({nums}, {n}) = {result}')
if __name__ == '__main__':
    test([1, 3], 6, 1);
    test([1, 5, 10], 20, 2);
    test([1, 2, 2], 5, 0);
    test([1, 5, 10], 20, 2);
    test([1, 2, 16, 19, 31, 35, 36, 64, 64, 67, 69, 71, 73, 74, 76, 79, 80, 91, 95, 96, 97], 8, 2);
