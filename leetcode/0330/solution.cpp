#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minPatches(std::vector<int> nums, int n)
{
    const int nv = static_cast<int>(nums.size());
    int result = 0;
    for (long reach = 1, idx = 0; reach <= n;)
    {
        if ((idx < nv) && (nums[idx] <= reach)) reach += nums[idx++];
        else
        {
            ++result;
            reach <<= 1;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minPatches(nums, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3}, 6, 1, trials);
    test({1, 5, 10}, 20, 2, trials);
    test({1, 2, 2}, 5, 0, trials);
    test({1, 5, 10}, 20, 2, trials);
    test({1, 2, 16, 19, 31, 35, 36, 64, 64, 67, 69, 71, 73, 74, 76, 79, 80, 91, 95, 96, 97}, 8, 2, trials);
    return 0;
}


