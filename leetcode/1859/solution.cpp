#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string sortSentence(std::string s)
{
    std::string values[10];
    const size_t n = s.size();
    size_t begin = 0, end = 0;
    for (; end < n; ++end)
    {
        if (s[end] == ' ')
        {
            if (end - begin >= 2)
                values[s[end - 1] - '0'] = s.substr(begin, end - begin - 1);
            begin = end + 1;
        }
    }
    if (end - begin >= 2)
        values[s[end - 1] - '0'] = s.substr(begin, end - begin - 1);
    std::string result;
    for (size_t i = 1; i <= 9; ++i)
    {
        if (!values[i].empty())
        {
            bool space = !result.empty();
            if (space) [[likely]] result += " ";
            result += values[i];
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sortSentence(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("is2 sentence4 This1 a3", "This is a sentence", trials);
    test("Myself2 Me1 I4 and3", "Me Myself and I", trials);
    return 0;
}


