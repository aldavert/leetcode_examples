#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countPaths(std::vector<std::vector<int> > grid)
{
    constexpr int MOD = 1'000'000'007;
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    int dirs[] = {0, 1, 0, -1, 0};
    std::vector<std::vector<int> > dp_table(m, std::vector<int>(n, -1));
    auto dfs = [&](auto &&self, int i, int j) -> int
    {
        if (dp_table[i][j] != -1) return dp_table[i][j];
        dp_table[i][j] = 1;
        for (int k = 0; k < 4; ++k)
        {
            int x = i + dirs[k], y = j + dirs[k + 1];
            if ((x < 0) || (x == m) || (y < 0) || (y == n)) continue;
            if (grid[x][y] <= grid[i][j]) continue;
            dp_table[i][j] = (dp_table[i][j] + self(self, x, y)) % MOD;
        }
        return dp_table[i][j];
    };
    int result = 0;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            result = (result + dfs(dfs, i, j)) % MOD;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPaths(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1}, {3, 4}}, 8, trials);
    test({{1}, {2}}, 3, trials);
    return 0;
}


