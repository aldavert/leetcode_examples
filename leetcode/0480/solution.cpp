#include "../common/common.hpp"
#include <set>

// ############################################################################
// ############################################################################

std::vector<double> medianSlidingWindow(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::vector<double> result;
    std::multiset<double> window(nums.begin(), nums.begin() + k);
    auto it = next(begin(window), (k - 1) / 2);
    for (int i = k; i < n; ++i)
    {
        result.push_back((k & 1)?*it:(*it + *next(it)) / 2.0);
        window.insert(nums[i]);
        if (nums[i] < *it) --it;
        if (nums[i - k] <= *it) ++it;
        window.erase(window.lower_bound(nums[i - k]));
    }
    result.push_back((k & 1)?*it:(*it + *next(it)) / 2.0);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          std::vector<double> solution,
          unsigned int trials = 1)
{
    std::vector<double> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = medianSlidingWindow(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, -1, -3, 5, 3, 6, 7}, 3, {1.0, -1.0, -1.0, 3.0, 5.0, 6.0}, trials);
    test({1, 2, 3, 4, 2, 3, 1, 4, 2}, 3, {2.0, 3.0, 3.0, 3.0, 2.0, 3.0, 2.0}, trials);
    return 0;
}


