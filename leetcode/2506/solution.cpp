#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int similarPairs(std::vector<std::string> words)
{
    const size_t n = words.size();
    std::vector<int> words_id(n);
    for (size_t i = 0; i < n; ++i)
    {
        int id = 0;
        for (char letter : words[i])
            id = id | (1 << (letter - 'a'));
        words_id[i] = id;
    }
    std::sort(words_id.begin(), words_id.end());
    int result = 0, current = 0;
    for (size_t i = 1; i < n; ++i)
    {
        if (words_id[i] == words_id[i - 1])
            ++current;
        else current = 0;
        result += current;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = similarPairs(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"aba", "aabb", "abcd", "bac", "aabc"}, 2, trials);
    test({"aabb", "ab", "ba"}, 3, trials);
    test({"nba", "cba", "dba"}, 0, trials);
    return 0;
}



