#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int distributeCoins(TreeNode * root)
{
    auto traverse = [&](void) -> int
    {
        int result = 0;
        auto inner = [&](auto &&self, TreeNode * node) -> int
        {
            if (node == nullptr) return 0;
            int l = self(self, node->left),
                r = self(self, node->right);
            result += std::abs(l) + std::abs(r);
            return (node->val - 1) + l + r;
        };
        inner(inner, root);
        return result;
    };
    return traverse();
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = distributeCoins(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 0, 0}, 2, trials);
    test({0, 3, 0}, 3, trials);
    return 0;
}


