# Insufficient Nodes in Root to Leaf Paths

Given the `root` of a binary tree and an integer `limit`, delete all **insufficient nodes** in the tree simultaneously, and return *the root of the resulting binary tree.*

A node is **insufficient** if every root to **leaf** path intersecting this node has a sum strictly less than `limit`.

A **leaf** is a node with no children.

#### Example 1:
> ```mermaid 
> graph TD;
> A((1))---B((2))
> A---C((3))
> B---D((4))
> B---E(( -99))
> C---F(( -99))
> C---G((7))
> D---H((8))
> D---I((9))
> E---J(( -99))
> E---K(( -99))
> F---L((12))
> F---M((13))
> G---N(( -99))
> G---O((14))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [1, 2, 3, 4, -99, -99, 7, 8, 9, -99, -99, 12, 13, -99, 14], limit = 1`  
> *Output:* `[1, 2, 3, 4, null, null, 7, 8, 9, null, 14]`

#### Example 2:
> ```mermaid 
> graph TD;
> A((5))---B((4))
> A---C((8))
> B---D((11))
> B---EMPTY1(( ))
> C---E((17))
> C---F((4))
> D---G((7))
> D---H((1))
> F---I((5))
> F---J((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class EMPTY1 empty;
> linkStyle 3 stroke-width:0px
> ```
> *Input:* `root = [5, 4, 8, 11, null, 17, 4, 7, 1, null, null, 5, 3], limit = 22`  
> *Output:* `[5, 4, 8, 11, null, 17, 4, 7, null, null, null, 5]`

#### Example 3:
> ```mermaid 
> graph TD;
> A((1))---B((2))
> A---C(( -3))
> B---D(( -5))
> B---EMPTY1(( ))
> C---E((4))
> C---EMPTY2(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class EMPTY1,EMPTY2 empty;
> linkStyle 3,5 stroke-width:0px
> ```
> *Input:* `root = [1, 2, -3, -5, null, 4, null], limit = -1`  
> *Output:* `[1, null, -3, 4]`

#### Constraints:
- The number of nodes in the tree is in the range `[1, 5000]`.
- `-10^5 <= Node.val <= 10^5`
- `-10^9 <= limit <= 10^9`


