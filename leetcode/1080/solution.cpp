#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * sufficientSubset(TreeNode * root, int limit)
{
    if (!root) return nullptr;
    if (!root->left && !root->right)
        return (root->val < limit)?nullptr:root;
    root->left = sufficientSubset(root->left, limit - root->val);
    root->right = sufficientSubset(root->right, limit - root->val);
    return ((!root->left) && (!root->right))?nullptr:root;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          int limit,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        TreeNode * new_root = sufficientSubset(root, limit);
        result = tree2vec(new_root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, -99, -99, 7, 8, 9, -99, -99, 12, 13, -99, 14}, 1,
         {1, 2, 3, 4, null, null, 7, 8, 9, null, 14}, trials);
    test({5, 4, 8, 11, null, 17, 4, 7, 1, null, null, 5, 3}, 22,
         {5, 4, 8, 11, null, 17, 4, 7, null, null, null, 5}, trials);
    test({1, 2, -3, -5, null, 4, null}, -1, {1, null, -3, 4}, trials);
    return 0;
}


