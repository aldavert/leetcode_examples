#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int countLargestGroup(int n)
{
    char decimal_number[5] = {1, 0, 0, 0, 0};
    int histogram[45] = {};
    for (int i = 1; i <= n; ++i)
    {
        const int number = static_cast<int>(decimal_number[0])
                         + static_cast<int>(decimal_number[1])
                         + static_cast<int>(decimal_number[2])
                         + static_cast<int>(decimal_number[3])
                         + static_cast<int>(decimal_number[4]);
        ++histogram[number];
        for (int j = 0; j < 5; decimal_number[j++] = 0)
            if (++decimal_number[j] < 10) [[likely]] break;
    }
    int max_frequency = 0, result = 0;
    for (int i = 0; i < 45; ++i)
        max_frequency = std::max(max_frequency, histogram[i]);
    for (int i = 0; i < 45; ++i)
        result += histogram[i] == max_frequency;
    return result;
}
#else
int countLargestGroup(int n)
{
    struct DecimalNumber
    {
        char values[5] = {1, 0, 0, 0, 0};
        void inc(void)
        {
            for (int i = 0; i < 5; values[i++] = 0)
                if (++values[i] < 10) [[likely]] break;
        }
        int sum(void) const { return static_cast<int>(values[0])
                                   + static_cast<int>(values[1])
                                   + static_cast<int>(values[2])
                                   + static_cast<int>(values[3])
                                   + static_cast<int>(values[4]); }
    };
    int histogram[45] = {};
    DecimalNumber decimal_number;
    for (int i = 1; i <= n; ++i, decimal_number.inc())
        ++histogram[decimal_number.sum()];
    int max_frequency = 0, result = 0;
    for (int i = 0; i < 45; ++i)
        max_frequency = std::max(max_frequency, histogram[i]);
    for (int i = 0; i < 45; ++i)
        result += histogram[i] == max_frequency;
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countLargestGroup(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(13, 4, trials);
    test(2, 2, trials);
    return 0;
}


