#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfEmployeesWhoMetTarget(std::vector<int> hours, int target)
{
    int result = 0;
    for (int employee_hours : hours)
        result += employee_hours >= target;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> hours, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfEmployeesWhoMetTarget(hours, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 2, 3, 4}, 2, 3, trials);
    test({5, 1, 4, 2, 2}, 6, 0, trials);
    return 0;
}


