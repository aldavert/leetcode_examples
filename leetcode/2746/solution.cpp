#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
int minimizeConcatenatedLength(std::vector<std::string> words)
{
    int dp[2][26];
    for (int i = 0; i < 26; ++i)
        dp[0][i] = dp[1][i] = std::numeric_limits<int>::lowest();
    dp[0][words[0].back() - 'a'] = dp[1][words[0][0] - 'a'] = 0;
    for (int i = 1; i < (int)words.size(); ++i)
    {
        int new_dp[2][26];
        for (int k = 0; k < 26; ++k)
            new_dp[0][k] = new_dp[1][k] = std::numeric_limits<int>::lowest();
        for (int right = 0; right < 2; ++right)
        {
            for (int c = 0; c < 26; ++c)
            {
                if (dp[right][c] == std::numeric_limits<int>::min())
                    continue;
                const int l = right ? c : words[i - 1][0] - 'a';
                const int r = !right ? c : words[i - 1].back() - 'a';
                new_dp[0][r] = std::max(new_dp[0][r], dp[right][c]
                             + static_cast<int>(words[i].back() - 'a' == l));
                new_dp[1][l] = std::max(new_dp[1][l], dp[right][c]
                             + static_cast<int>(r == words[i][0] - 'a'));
            }
        }
        for (int k = 0; k < 26; ++k)
            dp[0][k] = new_dp[0][k],
            dp[1][k] = new_dp[1][k];
    }
    int total = std::accumulate(words.begin(), words.end(), 0,
                [](const auto& accu, const auto& w) { return accu + w.size(); });
    int mx = 0;
    for (int right = 0; right < 2; ++right)
        for (int c = 0; c < 26; ++c)
            mx = std::max(mx, dp[right][c]);
    return total - mx;
}
#else
int minimizeConcatenatedLength(std::vector<std::string> words)
{
    const int n = static_cast<int>(words.size());
    int f[1001][26][26] = {};
    auto dfs = [&](auto &&self, int i, int a, int b)
    {
        if (i >= n) return 0;
        if (f[i][a][b]) return f[i][a][b];
        const auto &s = words[i];
        int m = static_cast<int>(s.size());
        int x = self(self, i + 1, a, s[m - 1] - 'a') - (s[0] - 'a' == b);
        int y = self(self, i + 1, s[0] - 'a', b) - (s[m - 1] - 'a' == a);
        return f[i][a][b] = m + std::min(x, y);
    };
    return static_cast<int>(words[0].size())
         + dfs(dfs, 1, words[0].front() - 'a', words[0].back() - 'a');
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimizeConcatenatedLength(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"aa", "ab", "bc"}, 4, trials);
    test({"ab", "b"}, 2, trials);
    test({"aaa", "c", "aba"}, 6, trials);
    test({"a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
          "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
          "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
          "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
          "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
          "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
          "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
          "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
          "a"}, 1, trials);
    return 0;
}


