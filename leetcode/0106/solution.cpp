#include "../common/common.hpp"
#include "../common/tree.hpp"

// ################################################################################################
// ################################################################################################

TreeNode * buildTree(std::vector<int> inorder, std::vector<int> postorder)
{
    const int n = static_cast<int>(inorder.size());
    int search[6002] = {};
    for (int i = 0; i < n; ++i)
        search[inorder[i] + 3000] = i;
    auto create =
        [&](auto &&self, int ibegin, int iend, int pbegin, int pend) -> TreeNode *
    {
        if (iend - ibegin == 0)
            return new TreeNode(inorder[ibegin]);
        else if (iend - ibegin < 0) return nullptr;
        int size = search[postorder[pend] + 3000] - ibegin - 1;
        return new TreeNode(postorder[pend],
                self(self, ibegin, ibegin + size, pbegin, pbegin + size),
                self(self, ibegin + size + 2, iend, pbegin + size + 1, pend - 1));
    };
    return create(create, 0, n - 1, 0, n - 1);
}

// ################################################################################################
// ################################################################################################

void test(std::vector<int> inorder, std::vector<int> postorder, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = buildTree(inorder, postorder);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({9, 3, 15, 20, 7}, {9, 15, 7, 20, 3}, {3, 9, 20, null, null, 15, 7}, trials);
    test({-1}, {-1}, {-1}, trials);
    test({2, 1}, {2, 1}, {1, 2}, trials);
    return 0;
}


