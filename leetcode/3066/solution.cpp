#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> nums, int k)
{
    int result = 0;
    std::priority_queue<long, std::vector<long>, std::greater<> > heap;
    for (const int num : nums) heap.push(num);
    while ((heap.size() > 1) && (heap.top() < k))
    {
        long x = heap.top();
        heap.pop();
        long y = heap.top();
        heap.pop();
        heap.push(std::min(x, y) * 2L + std::max(x, y));
        ++result;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 11, 10, 1, 3}, 10, 2, trials);
    test({1, 1, 2, 4, 9}, 20, 4, trials);
    return 0;
}


