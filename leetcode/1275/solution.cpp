#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string tictactoe(std::vector<std::vector<int> > moves)
{
    char board[3][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
    bool playerA = true;
    auto check = [&](int x, int y) -> bool
    {
        char current = (playerA)?'A':'B';
        // Check column.
        bool win = true;
        for (int i = 0; i < 3; ++i) win = win && board[i][y] == current;
        if (win) return true;
        // Check row.
        win = true;
        for (int i = 0; i < 3; ++i) win = win && board[x][i] == current;
        if (win) return true;
        // Check diagonal.
        if (x == y)
        {
            win = true;
            for (int i = 0; i < 3; ++i) win = win && board[i][i] == current;
            if (win) return true;
        }
        if ((x == y) || ((x - 1) * (y - 1) < 0))
        {
            win = true;
            for (int i = 0; i < 3; ++i) win = win && board[2 - i][i] == current;
            if (win) return true;
        }
        return false;
    };
    
    for (const auto &m : moves)
    {
        board[m[0]][m[1]] = (playerA)?'A':'B';
        if (check(m[0], m[1]))
            return (playerA)?"A":"B";
        playerA = !playerA;
    }
    return (moves.size() == 9)?"Draw":"Pending";
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > moves,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = tictactoe(moves);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0}, {2, 0}, {1, 1}, {2, 1}, {2, 2}}, "A", trials);
    test({{0, 0}, {1, 1}, {0, 1}, {0, 2}, {1, 0}, {2, 0}}, "B", trials);
    test({{0, 0}, {1, 1}, {2, 0}, {1, 0}, {1, 2}, {2, 1}, {0, 1}, {0, 2}, {2, 2}},
         "Draw", trials);
    test({{0, 0}, {1, 1}}, "Pending", trials);
    return 0;
}


