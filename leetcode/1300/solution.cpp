#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

#if 1
int findBestValue(std::vector<int> arr, int target)
{
    auto fun = [&](int val) -> int
    {
        int sum = 0;
        for(int num : arr)
            sum += std::min(num, val);
        return std::abs(sum - target);
    };
    int l = 0, r = *max_element(arr.begin(), arr.end());
    while (l < r)
    {
        int m = l + (r - l) / 2;
        if (fun(m) <= fun(m + 1)) r = m;
        else l = m + 1;
    }
    return l;
}
#else
int findBestValue(std::vector<int> arr, int target)
{
    const int n = static_cast<int>(arr.size());
    std::vector<int> prefix_sum(n + 1, 0);
    int max_value = 0, result = 0;
    
    std::sort(arr.begin(), arr.end());
    for (int i = 0; i < n; ++i)
    {
        prefix_sum[i + 1] = prefix_sum[i] + arr[i];
        max_value = std::max(max_value, arr[i]);
    }
    int smallest_diff = std::numeric_limits<int>::max();
    for (int value = 0; value <= max_value; ++value)
    {
        int idx = static_cast<int>(std::upper_bound(arr.begin(), arr.end(), value)
                                   - arr.begin());
        int diff = std::abs(prefix_sum[idx] + (n - idx) * value - target);
        if (smallest_diff > diff)
        {
            smallest_diff = diff;
            result = value;
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findBestValue(arr, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 9, 3}, 10, 3, trials);
    test({2, 3, 5}, 10, 5, trials);
    test({60864, 25176, 27249, 21296, 20204}, 56803, 11361, trials);
    test({3, 4, 5, 6}, 18, 6, trials);
    return 0;
}


