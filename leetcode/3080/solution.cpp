#include "../common/common.hpp"
#include <queue>
#include <numeric>

// ############################################################################
// ############################################################################

std::vector<long long> unmarkedSumArray(std::vector<int> nums,
                                        std::vector<std::vector<int> > queries)
{
    std::vector<long long> result;
    std::vector<bool> marked(nums.size());
    long sum = std::accumulate(nums.begin(), nums.end(), 0L);
    std::priority_queue<std::pair<int, int>,
        std::vector<std::pair<int, int> >, std::greater<>> heap;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        heap.emplace(nums[i], i);
    for (const auto &query : queries)
    {
        if (!marked[query[0]])
        {
            marked[query[0]] = true;
            sum -= nums[query[0]];
        }
        for (int popped = 0; (popped < query[1]) && !heap.empty();)
        {
            const auto [num, i] = heap.top();
            heap.pop();
            if (!marked[i])
            {
                marked[i] = true;
                sum -= num;
                ++popped;
            }
        }
        result.push_back(sum);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::vector<int> > queries,
          std::vector<long long> solution,
          unsigned int trials = 1)
{
    std::vector<long long> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = unmarkedSumArray(nums, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 1, 2, 3, 1}, {{1, 2}, {3, 3}, {4, 2}}, {8, 3, 0}, trials);
    test({1, 4, 2, 3}, {{0, 1}}, {7}, trials);
    return 0;
}


