#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sumBase(int n, int k)
{
    int result = 0;
    while (n)
    {
        result += n % k;
        n /= k;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumBase(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(34, 6, 9, trials);
    test(10, 10, 1, trials);
    return 0;
}


