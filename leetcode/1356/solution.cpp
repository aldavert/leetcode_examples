#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> sortByBits(std::vector<int> arr)
{
    auto nBits = [](int n) -> int
    {
        int bits = 0;
        for (; n; n >>= 1) bits += (n & 1);
        return bits;
    };
    std::vector<int> result, buckets[14];
    for (int value : arr)
        buckets[nBits(value)].push_back(value);
    for (int i = 0; i < 14; ++i)
    {
        if (buckets[i].size() > 0)
        {
            std::sort(buckets[i].begin(), buckets[i].end());
            for (int value : buckets[i])
                result.push_back(value);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sortByBits(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 2, 3, 4, 5, 6, 7, 8},
         {0, 1, 2, 4, 8, 3, 5, 6, 7}, trials);
    test({1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1},
         {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024}, trials);
    return 0;
}

