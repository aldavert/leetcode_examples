#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int squareFreeSubsets(std::vector<int> nums)
{
    constexpr int MOD = 1'000'000'007;
    constexpr int primes[10] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29};
    int count[31] = {};
    for (int x : nums) ++count[x];
    int n = 10;
    std::vector<long long> f(1 << n);
    f[0] = 1;
    for (int i = 0; i < count[1]; ++i)
        f[0] = f[0] * 2 % MOD;
    for (int x = 2; x < 31; ++x)
    {
        if ((count[x] == 0) || (x % 4 == 0) || (x % 9 == 0) || (x % 25 == 0))
            continue;
        int mask = 0;
        for (int i = 0; i < n; ++i)
            if (x % primes[i] == 0)
                mask |= 1 << i;
        for (int state = (1 << n) - 1; state; --state)
            if ((state & mask) == mask)
                f[state] = (f[state] + 1LL * count[x] * f[state ^ mask]) % MOD;
    }
    long long result = -1;
    for (int i = 0, m = 1 << n; i < m; ++i)
        result = (result + f[i]) % MOD;
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = squareFreeSubsets(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 4, 4, 5}, 3, trials);
    test({1}, 1, trials);
    return 0;
}


