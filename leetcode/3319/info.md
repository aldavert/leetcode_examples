# K-th Largest Perfect Subtree Size in Binary Tree

You are given the `root` of a **binary tree** and an integer `k`.

Return an integer denoting the size of the `k^{th}` **largest perfect binary** *subtree*, or `-1` if it doesn't exist.

A perfect binary tree is a tree where all leaves are on the same level, and every parent has two children.

**Note:** A subtree of `treeName` is a tree consisting of a node in `treeName` and all of its descendants.

#### Example 1:
> *Input:* `root = [5, 3, 6, 5, 2, 5, 7, 1, 8, null, null, 6, 8], k = 2`  
> *Output:* `3`  
> *Explanation:*
> ```mermaid
> graph TD;
> A((5))---B((3))
> A---C((6))
> B---D((5))
> B---E((2))
> C---F((5))
> C---G((7))
> D---H((1))
> D---I((8))
> F---J((6))
> F---K((8))
> classDef default fill:#111, stroke:#000, stroke-width:2px, color:#FFF;
> classDef white fill:#FFF, stroke:#000, stroke-width:2px;
> class A,B,C white;
> ```
> The roots of the perfect binary subtrees are highlighted in black. Their sizes, in non-increasing order are `[3, 3, 1, 1, 1, 1, 1, 1]`. The 2nd largest size is `3`.

#### Example 2:
> *Input:* `root = [1, 2, 3, 4, 5, 6, 7], k = 1`  
> *Output:* `7`  
> *Explanation:*
> ```mermaid
> graph TD;
> A((1))---B((2))
> A---C((3))
> B---D((4))
> B---E((5))
> C---F((6))
> C---G((7))
> classDef default fill:#111, stroke:#000, stroke-width:2px, color:#FFF;
> ```
> The sizes of the perfect binary subtrees in non-increasing order are `[7, 3, 3, 1, 1, 1, 1]`. The size of the largest perfect binary subtree is `7`.

#### Example 3:
> *Input:* `root = [1, 2, 3, null, 4], k = 3`  
> *Output:* `-1`  
> *Explanation:*
> ```mermaid
> graph TD;
> A((1))---B((2))
> A---C((3))
> B---EMPTY(( ))
> B---D((4))
> classDef default fill:#FFF, stroke:#000, stroke-width:2px;
> classDef black fill:#111, stroke:#000, stroke-width:2px, color:#FFF;
> classDef empty fill:#FFF0, stroke:#FFF0, stroke-width:0px;
> class C,D black;
> class EMPTY empty;
> linkStyle 2 stroke-width:0px;
> ```
> The sizes of the perfect binary subtrees in non-increasing order are `[1, 1]`. There are fewer than `3` perfect binary subtrees.

#### Constraints:
- The number of nodes in the tree is in the range `[1, 2000]`.
- `1 <= Node.val <= 2000`
- `1 <= k <= 1024`


