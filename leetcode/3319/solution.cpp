#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int kthLargestPerfectSubtree(TreeNode * root, int k)
{
    std::vector<int> perfect_sizes;
    auto traverse = [&](auto &&self, TreeNode * node) -> int
    {
        int size_left = (node->left)?self(self, node->left):0;
        int size_right = (node->right)?self(self, node->right):0;
        if ((size_left == -1) || (size_left != size_right))
            return -1;
        perfect_sizes.push_back(size_left + size_right + 1);
        return perfect_sizes.back();
    };
    traverse(traverse, root);
    if (k > static_cast<int>(perfect_sizes.size())) return -1;
    std::sort(perfect_sizes.rbegin(), perfect_sizes.rend());
    return perfect_sizes[k - 1];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = kthLargestPerfectSubtree(root, k);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 3, 6, 5, 2, 5, 7, 1, 8, null, null, 6, 8}, 2, 3, trials);
    test({1, 2, 3, 4, 5, 6, 7}, 1, 7, trials);
    test({1, 2, 3, null, 4}, 3, -1, trials);
    return 0;
}


