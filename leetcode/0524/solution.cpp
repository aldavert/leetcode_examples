#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string findLongestWord(std::string s, std::vector<std::string> dictionary)
{
    std::sort(dictionary.begin(), dictionary.end(),
              [](const auto &left, const auto &right) -> bool
              {
                return (left.size() > right.size())
                    || ((left.size() == right.size()) && (left < right));
              });
    auto isInside = [](std::string text, std::string codeword) -> bool
    {
        for (size_t i = 0; char letter : text)
        {
            if (letter == codeword[i])
            {
                ++i;
                if (i == codeword.size()) return true;
            }
        }
        return false;
    };
    for (auto codeword : dictionary)
        if (isInside(s, codeword))
            return codeword;
    return "";
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<std::string> dictionary,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLongestWord(s, dictionary);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abpcplea", {"ale", "apple", "monkey", "plea"}, "apple", trials);
    test("abpcplea", {"a", "b", "c"}, "a", trials);
    return 0;
}


