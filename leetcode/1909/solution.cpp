#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canBeIncreasing(std::vector<int> nums)
{
    bool removed = false;
    for (int i = 1, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        if (nums[i - 1] >= nums[i])
        {
            if (removed) return false;
            removed = true;
            if ((i > 1) && (nums[i - 2] >= nums[i]))
                nums[i] = nums[i - 1];
        }
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canBeIncreasing(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 10, 5, 7}, true, trials);
    test({2, 3, 1, 2}, false, trials);
    test({1, 1, 1}, false, trials);
    return 0;
}


