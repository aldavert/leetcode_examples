#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int mostFrequent(std::vector<int> nums, int key)
{
    int result = -1, max_frequency = 0, histogram[1001] = {};
    for (size_t i = 1, n = nums.size(); i < n; ++i)
        if ((nums[i - 1] == key) && (++histogram[nums[i]] > max_frequency))
            max_frequency = histogram[nums[i]],
            result = nums[i];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int key, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = mostFrequent(nums, key);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 100, 200, 1, 100}, 1, 100, trials);
    test({2, 2, 2, 2, 3}, 2, 2, trials);
    return 0;
}


