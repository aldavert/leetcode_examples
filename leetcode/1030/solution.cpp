#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> >
allCellsDistOrder(int rows, int cols, int rCenter, int cCenter)
{
    struct Cell
    {
        int distance = -1;
        int x = 0;
        int y = 0;
        bool operator<(const Cell &other) const
        {
            return distance < other.distance;
        }
    };
    std::vector<std::vector<int> > result;
    std::vector<Cell> cells;
    cells.reserve(rows * cols);
    result.reserve(rows * cols);
    for (int r = 0; r < rows; ++r)
        for (int c = 0; c < cols; ++c)
            cells.push_back({std::abs(r - rCenter) + std::abs(c - cCenter), r, c});
    std::sort(cells.begin(), cells.end());
    for (const Cell &c : cells)
        result.push_back({c.x, c.y});
    return result;
}

// ############################################################################
// ############################################################################

void test(int rows,
          int cols,
          int rCenter,
          int cCenter,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    auto toDistances = [&](const std::vector<std::vector<int> > &coordinates)
    {
        std::vector<int> distances;
        for (const auto &coord : coordinates)
        {
            if (coord.size() != 2) break;
            distances.push_back(std::abs(coord[0] - rCenter)
                              + std::abs(coord[1] - cCenter));
        }
        return distances;
    };
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = allCellsDistOrder(rows, cols, rCenter, cCenter);
    bool valid = (solution.size() == result.size())
              && (toDistances(solution) == toDistances(solution));
    showResult(valid, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 2, 0, 0, {{0, 0}, {0, 1}}, trials);
    test(2, 2, 0, 1, {{0, 1}, {0, 0}, {1, 1}, {1, 0}}, trials);
    test(2, 3, 1, 2, {{1, 2}, {0, 2}, {1, 1}, {0, 1}, {1, 0}, {0, 0}}, trials);
    return 0;
}


