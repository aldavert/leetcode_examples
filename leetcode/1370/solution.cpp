#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string sortString(std::string s)
{
    const int n = static_cast<int>(s.size());
    int histogram[28] = {};
    for (char c : s)
        ++histogram[c - 'a' + 1];
    for (int i = 0, direction = 1, idx = 1; i < n; ++i)
    {
        while (!histogram[idx])
        {
            direction *= 1 - 2 * ((idx < 1) || (idx > 26));
            idx += direction;
        }
        s[i] = static_cast<char>((idx - 1) + 'a');
        --histogram[idx];
        idx += direction;
    }
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sortString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aaaabbbbcccc", "abccbaabccba", trials);
    test("rat", "art", trials);
    test("zaaz", "azza", trials);
    test("leetcode", "cdelotee", trials);
    return 0;
}


