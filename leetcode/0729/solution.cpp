#include "../common/common.hpp"
#include <map>
#include "values.hpp"

// ############################################################################
// ############################################################################

#if 0
class MyCalendar {
    std::map<int, int, std::greater<int> > m_dates;
public:
    MyCalendar() = default;
    bool book(int start, int end)
    {
        if (start >= end) return true; // Special case: not specified what to do.
        bool available = true;
        for (auto it_begin = m_dates.lower_bound(end), it_end = m_dates.end();
                available && (it_begin != it_end);
                ++it_begin)
            if (start < it_begin->second)
                available = (std::min(it_begin->second, end) - std::max(it_begin->first, start)) <= 0;
        if (available)
            m_dates[start] = end;
        return available;
    }
};
#else
class MyCalendar {
    int m_start[1000] = {};
    int m_end[1000] = {};
    int m_elements = 0;
public:
    MyCalendar() = default;
    bool book(int start, int end)
    {
        if (start >= end) return true; // Special case: not specified what to do.
        bool available = true;
        for (int i = 0; available && (i < m_elements); ++i)
            if ((end > m_start[i]) && (start < m_end[i]))
                available = (std::min(m_end[i], end) - std::max(m_start[i], start)) <= 0;
        if (available)
        {
            m_start[m_elements] = start;
            m_end[m_elements] = end;
            ++m_elements;
        }
        return available;
    }
};
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::pair<int, int> > dates,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result(dates.size());
    for (unsigned int t = 0; t < trials; ++t)
    {
        MyCalendar * cal = new MyCalendar();
        for (size_t j = 0; j < dates.size(); ++j)
            result[j] = cal->book(dates[j].first, dates[j].second);
        delete cal;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10000000;
    constexpr unsigned int trials = 30000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{10, 20}, {15, 25}, {20, 30}}, {true, false, true}, trials);
    test(testA::dates, testA::solution, trials);
    return 0;
}


