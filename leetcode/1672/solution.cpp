#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int maximumWealth(std::vector<std::vector<int> > accounts)
{
    int result = 0;
    for (const auto &person : accounts)
        result = std::max(result, std::accumulate(person.begin(), person.end(), 0));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > accounts, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumWealth(accounts);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {3, 2, 1}}, 6, trials);
    test({{1, 5}, {7, 3}, {3, 5}}, 10, trials);
    test({{2, 8, 7}, {7, 1, 3}, {1, 9, 5}}, 17, trials);
    return 0;
}


