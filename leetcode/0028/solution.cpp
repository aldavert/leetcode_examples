#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int strStr(std::string haystack, std::string needle)
{
    if (needle == "") return 0;
    auto result = haystack.find(needle);
    return (result != std::string::npos)?static_cast<int>(result):-1;
}

// ############################################################################
// ############################################################################

void test(std::string hayback,
          std::string needle,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = strStr(hayback, needle);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("hello", "ll", 2, trials);
    test("heloll", "ll", 4, trials);
    test("aaaaa", "bba", -1, trials);
    test("aaaaa", "aaaaaaa", -1, trials);
    return 0;
}


