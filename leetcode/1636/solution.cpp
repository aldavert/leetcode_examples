#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> frequencySort(std::vector<int> nums)
{
    auto cmp = [](auto left, auto right)
    {
        return  (std::get<1>(left) <  std::get<1>(right))
            || ((std::get<1>(left) == std::get<1>(right))
            &&  (std::get<0>(left) >  std::get<0>(right)));
    };
    std::unordered_map<int, int> lut;
    for (int value : nums) ++lut[value];
    std::vector<std::tuple<int, int> > value_frequency(lut.begin(), lut.end());
    std::sort(value_frequency.begin(), value_frequency.end(), cmp);
    std::vector<int> result;
    for (auto [value, frequency] : value_frequency)
        for (int i = 0; i < frequency; ++i)
            result.push_back(value);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = frequencySort(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2, 2, 2, 3}, {3, 1, 1, 2, 2, 2}, trials);
    test({2, 3, 1, 3, 2}, {1, 3, 3, 2, 2}, trials);
    test({-1, 1, -6, 4, 5, -6, 1, 4, 1}, {5, -1, 4, 4, -6, -6, 1, 1, 1}, trials);
    return 0;
}


