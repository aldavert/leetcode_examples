#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumScore(int a, int b, int c)
{
    return std::min((a + b + c) / 2, a + b + c - std::max({a, b, c}));
}

// ############################################################################
// ############################################################################

void test(int a, int b, int c, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumScore(a, b, c);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 4, 6, 6, trials);
    test(4, 4, 6, 7, trials);
    test(1, 8, 8, 8, trials);
    return 0;
}


