#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkPalindromeFormation(std::string a, std::string b)
{
    auto isPalindrome = [](const std::string &s, int i, int j) -> bool
    {
        while (i < j)
            if (s[i++] != s[j--])
                return false;
        return true;
    };
    auto check = [&isPalindrome](const std::string &l, const std::string &r) -> bool
    {
        for (int i = 0, j = static_cast<int>(l.size()) - 1; i < j; ++i, --j)
            if (l[i] != r[j])
                return isPalindrome(l, i, j) || isPalindrome(r, i, j);
        return true;
    };
    return check(a, b) || check(b, a);
}

// ############################################################################
// ############################################################################

void test(std::string a, std::string b, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkPalindromeFormation(a, b);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("x", "y", true, trials);
    test("xbdef", "xecab", false, trials);
    test("ulacfd", "jizalu", true, trials);
    return 0;
}


