#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumXorProduct(long long a, long long b, int n)
{
    constexpr int MOD = 1'000'000'007;
    if (n > 0)
    {
        for (long bit = 1L << (n - 1); bit > 0; bit >>= 1)
        {
            if ((std::min(a, b) & bit) == 0)
            {
                a ^= bit;
                b ^= bit;
            }
        }
    }
    return static_cast<int>(a % MOD * (b % MOD) % MOD);
}

// ############################################################################
// ############################################################################

void test(long long a, long long b, int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumXorProduct(a, b, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(12, 5, 4, 98, trials);
    test(6, 7, 5, 930, trials);
    test(1, 6, 3, 12, trials);
    return 0;
}


