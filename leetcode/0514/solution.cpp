#include "../common/common.hpp"
#include <unordered_map>
#include <limits>

// ############################################################################
// ############################################################################

#if 1
int findRotateSteps(std::string ring, std::string key)
{
    int dp[2][100] = {};
    const int nr = static_cast<int>(ring.size());
    const int nk = static_cast<int>(key.size());
    int result = std::numeric_limits<int>::max();
    std::vector<int> pos[26];
    for (int j = 0; j < nr; ++j)
        pos[static_cast<int>(ring[j] - 'a')].push_back(j);
    for (int idx : pos[static_cast<int>(key[0] - 'a')]) 
        dp[0][idx] = std::min(idx, nr - idx);
    for (int i = 1; i < nk; ++i)
    {
        for (int idx1 : pos[static_cast<int>(key[i] - 'a')]) 
        {
            dp[i & 1][idx1] = std::numeric_limits<int>::max();
            for (int idx2 : pos[static_cast<int>(key[i - 1] - 'a')]) 
            {
                int tmp = abs(idx1 - idx2);
                dp[i & 1][idx1] = std::min(dp[i & 1][idx1],
                                           dp[(i - 1) & 1][idx2] + std::min(tmp, nr - tmp));
            }
        }
    }
    for (int idx : pos[key[nk - 1] - 'a'])
        result = std::min(result, dp[(nk - 1) & 1][idx]);
    return result + nk;
}
#else
int findRotateSteps(std::string ring, std::string key)
{
    const int n = static_cast<int>(ring.size());
    std::unordered_map<std::string, int> memo;
    std::function<int(std::string, int)> dfs =
        [&](std::string current_ring, int index) -> int
    {
        if (index == static_cast<int>(key.size())) return 0;
        const std::string hashKey = current_ring + std::to_string(index);
        if (memo.count(hashKey)) return memo[hashKey];
        int result = std::numeric_limits<int>::max();
        for (int i = 0; i < n; ++i)
        {
            if (current_ring[i] == key[index])
            {
                int min_rotates = std::min(i, n - i);
                std::string new_ring = current_ring.substr(i) + current_ring.substr(0, i);
                int remainingRotates = dfs(new_ring, index + 1);
                result = std::min(result, min_rotates + remainingRotates);
            }
        }
        return memo[hashKey] = result;
    };
    return dfs(ring, 0) + static_cast<int>(key.size());
}
#endif

// ############################################################################
// ############################################################################

void test(std::string ring, std::string key, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findRotateSteps(ring, key);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("godding", "gd", 4, trials);
    test("godding", "godding", 13, trials);
    return 0;
}


