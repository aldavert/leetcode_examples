#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int numSubmatrixSumTarget(std::vector<std::vector<int> > matrix, int target)
{
    auto work = matrix;
    const size_t m = matrix.size();
    const size_t n = matrix[0].size();
    int result = 0;
    for (auto &row : matrix)
        for (size_t i = 1; i < n; ++i)
            row[i] += row[i - 1];
    for (size_t col = 0; col < n; ++col)
    {
        for (size_t j = col; j < n; ++j)
        {
            std::unordered_map<int, int> prefix_count{{0, 1}};
            int sum = 0;
            for (size_t i = 0; i < m; ++i)
            {
                if (col > 0)
                    sum -= matrix[i][col - 1];
                sum += matrix[i][j];
                if (auto search = prefix_count.find(sum - target);
                    search != prefix_count.end())
                    result += search->second;
                ++prefix_count[sum];
            }
        }
    }
    return result;
}
#else
int numSubmatrixSumTarget(std::vector<std::vector<int> > matrix, int target)
{
    const size_t rows = matrix.size();
    const size_t cols = matrix[0].size();
    std::vector<std::vector<int> > integral(rows + 1, std::vector<int>(cols + 1, 0));
    for (size_t r = 0; r < rows; ++r)
        for (size_t c = 0; c < cols; ++c)
            integral[r + 1][c + 1] = matrix[r][c] + integral[r + 1][c]
                                   + integral[r][c + 1] - integral[r][c];
    int result = 0;
    for (size_t x1 = 0; x1 < rows; ++x1)
    {
        for (size_t y1 = 0; y1 < cols; ++y1)
        {
            for (size_t x2 = x1 + 1; x2 <= rows; ++x2)
            {
                for (size_t y2 = y1 + 1; y2 <= cols; ++y2)
                {
                    int sum = integral[x2][y2] - integral[x1][y2] - integral[x2][y1]
                            + integral[x1][y1];
                    result += sum == target;
                }
            }
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix,
          int target,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numSubmatrixSumTarget(matrix, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 0}, {1, 1, 1}, {0, 1, 0}}, 0, 4, trials);
    test({{1, -1}, {-1, 1}}, 0, 5, trials);
    test({{904}}, 0, 0, trials);
    return 0;
}


