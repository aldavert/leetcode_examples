#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minExtraChar(std::string s, std::vector<std::string> dictionary)
{
    const int n = static_cast<int>(s.size()),
              m = static_cast<int>(dictionary.size());
    std::vector<int> subsize(m), lut(n, n + 1);
    for (int i = 0; const auto &substring : dictionary)
        subsize[i++] = static_cast<int>(substring.size());
    auto process = [&](auto &&self, int position) -> int
    {
        if (position >= n) return 0;
        if (lut[position] != n + 1)
            return lut[position];
        int result = n + 1;
        for (int i = 0; i < m; ++i)
            if ((position + subsize[i] <= n)
            &&  (s.substr(position, subsize[i]) == dictionary[i]))
                result = std::min(result, self(self, position + subsize[i]));
        return lut[position] = std::min(result, self(self, position + 1) + 1);
    };
    return process(process, 0);
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<std::string> dictionary,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minExtraChar(s, dictionary);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leetscode", {"leet", "code", "leetcode"}, 1, trials);
    test("sayhelloworld", {"hello", "world"}, 3, trials);
    test("hellohellohello", {"leet", "code", "hello"}, 0, trials);
    test("this is a very long string where there will not be any kind of matching"
         "in the dictionary to check what happens when no substring can be removed.",
         {"leet", "code", "helo", "adfa", "fewb", "fids", "fids", "boff", "bdes",
          "afsl", "dfas", "mvew", "bbed", "dave", "vedd"}, 71 + 73, trials);
    test("this is a very long string where there will be multiple matching in the"
         "dictionary that will have to be explored in order to find the correct "
         "reduction.",
         {"adfa", "fewb", "fids", "fids", "boff", "bdes", "afsl", "dfas", "mvew",
          "is", "to", "find", "will", "the", "in order", "in order to"
          "in order to find", "this is", "is a", "is a very long", "to be"},
          99, trials);
    return 0;
}


