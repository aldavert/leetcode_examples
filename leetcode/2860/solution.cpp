#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countWays(std::vector<int> nums)
{
    nums.push_back(-1);
    nums.push_back(std::numeric_limits<int>::max());
    std::sort(nums.begin(), nums.end());
    int result = 0;
    for (int i = 0; i <= static_cast<int>(nums.size()); ++i)
        if ((nums[i] < i) && (i < nums[i + 1]))
            ++result;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countWays(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1}, 2, trials);
    test({6, 0, 3, 3, 6, 7, 2, 7}, 3, trials);
    return 0;
}


