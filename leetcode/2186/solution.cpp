#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minSteps(std::string s, std::string t)
{
    int count[26] = {};
    for (char c : s) ++count[c - 'a'];
    for (char c : t) --count[c - 'a'];
    int result = 0;
    for (int i = 0; i < 26; ++i)
        result += std::abs(count[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string t, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSteps(s, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leetcode", "coats", 7, trials);
    test("night", "thing", 0, trials);
    return 0;
}


