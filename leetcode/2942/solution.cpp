#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findWordsContaining(std::vector<std::string> words, char x)
{
    std::vector<int> result;
    for (int i = 0, n = static_cast<int>(words.size()); i < n; ++i)
        if (words[i].find(x) != std::string::npos)
            result.push_back(i);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_set<int> left_set;
    for (int number : left)
        left_set.insert(number);
    for (int number : right)
        if (auto search = left_set.find(number); search == left_set.end())
            return false;
        else left_set.erase(search);
    return left_set.empty();
}

void test(std::vector<std::string> words,
          char x,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findWordsContaining(words, x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"leet", "code"}, 'e', {0, 1}, trials);
    test({"abc", "bcd", "aaaa", "cbc"}, 'a', {0, 2}, trials);
    test({"abc", "bcd", "aaaa", "cbc"}, 'z', {}, trials);
    return 0;
}


