#include "../common/common.hpp"
#include <limits>
#include <queue>
#include <fstream>

// ############################################################################
// ############################################################################

#if 1
int findCheapestPrice(int n,
                      std::vector<std::vector<int> > flights,
                      int src,
                      int dst,
                      int k)
{
    struct Edge { int node = 0; int cost = 0; };
    struct Position
    {
        int jumps = 0;
        int node = 0;
        int cost = 0;
        bool operator<(const Position &other) const { return cost > other.cost; }
    };
    const int UNBOUNDED = std::numeric_limits<int>::max();
    std::vector<std::vector<Edge> > graph(n);
    for (const auto &flight : flights)
        graph[flight[0]].push_back({flight[1], flight[2]});
    std::vector<Edge> visited(n, { UNBOUNDED, UNBOUNDED });
    std::priority_queue<Position> q;
    q.push({0, src, 0});
    
    while (!q.empty())
    {
        auto [jumps, node, cost] = q.top();
        q.pop();
        if (jumps > k + 1) continue;
        if ((visited[node].node <= jumps) && (visited[node].cost <= cost))
            continue;
        if ((visited[node].node >= jumps) && (visited[node].cost >= cost))
            visited[node] = { jumps, cost };
        if (node == dst)
            return cost;
        for (auto [next, incr] : graph[node])
            q.push({jumps + 1, next, incr + cost});
    }
    return -1;
}
#else
int findCheapestPrice(int n,
                      std::vector<std::vector<int> > flights,
                      int src,
                      int dst,
                      int k)
{
    struct Edge
    {
        int node = 0;
        int cost = 0;
    };
    const int UNBOUNDED = std::numeric_limits<int>::max();
    std::vector<std::vector<Edge> > graph(n);
    for (const auto &flight : flights)
        graph[flight[0]].push_back({flight[1], flight[2]});
    std::vector<Edge> visited(n, { UNBOUNDED, UNBOUNDED });
    std::queue<Edge> q;
    q.push({src, 0});
    int result = UNBOUNDED;
    
    for (int jumps = 0; jumps <= k + 1; ++jumps)
    {
        for (int elements = static_cast<int>(q.size()); elements > 0; --elements)
        {
            auto [node, cost] = q.front();
            q.pop();
            if ((visited[node].node <= jumps) && (visited[node].cost <= cost))
                continue;
            if ((visited[node].node >= jumps) && (visited[node].cost >= cost))
                visited[node] = { jumps, cost };
            if (node == dst)
            {
                result = std::min(result, cost);
                continue;
            }
            for (auto [next, incr] : graph[node])
                q.push({next, incr + cost});
        }
    }
    return (result != UNBOUNDED)?result:-1;
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > flights,
          int src,
          int dst,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findCheapestPrice(n, flights, src, dst, k);
    showResult(solution == result, solution, result);
}

void test(const char * filename, unsigned int trials)
{
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cout << "Could not open test file '" << filename << '\n';
        return;
    }
    std::vector<std::vector<int> > edges;
    int n, number_of_edges, src, dst, k, solution;
    file >> n >> number_of_edges;
    for (int i = 0; i < number_of_edges; ++i)
    {
        int from, to, cost;
        file >> from >> to >> cost;
        edges.push_back({from, to, cost});
    }
    file >> src >> dst >> k >> solution;
    test(n, edges, src, dst, k, solution, trials);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{0, 1, 100}, {1, 2, 100}, {2, 0, 100}, {1, 3, 600}, {2, 3, 200}}, 0, 3, 1, 700, trials);
    test(3, {{0, 1, 100}, {1, 2, 100}, {0, 2, 500}}, 0, 2, 1, 200, trials);
    test(3, {{0, 1, 100}, {1, 2, 100}, {0, 2, 500}}, 0, 2, 0, 500, trials);
    //test("testLong.txt", trials);
    return 0;
}


