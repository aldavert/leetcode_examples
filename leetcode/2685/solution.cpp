#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countCompleteComponents(int n, std::vector<std::vector<int> > edges)
{
    std::vector<int> id(n), rank(n), node_count(n, 1), edge_count(n);
    for (int i = 0; i < n; ++i) id[i] = i;
    auto find = [&](auto &&self, int u) -> int
    {
        return (id[u] == u)?u:id[u] = self(self, id[u]);
    };
    auto rankUnion = [&](int u, int v)
    {
        int i = find(find, u), j = find(find, v);
        ++edge_count[i];
        if (i == j) return;
        if (rank[i] <= rank[j])
        {
            id[i] = j;
            edge_count[j] += edge_count[i];
            node_count[j] += node_count[i];
            rank[j] += (rank[i] == rank[j]);
        }
        else
        {
            id[j] = i;
            edge_count[i] += edge_count[j];
            node_count[i] += node_count[j];
        }
    };
    auto isComplete = [&](int u) -> bool
    {
        return node_count[u] * (node_count[u] - 1) / 2 == edge_count[u];
    };
    std::unordered_set<int> parents;
    
    for (const auto &edge : edges)
        rankUnion(edge[0], edge[1]);
    int result = 0;
    for (int i = 0; i < n; ++i)
    {
        const int parent = find(find, i);
        if (parents.insert(parent).second && isComplete(parent))
            ++result;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countCompleteComponents(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, {{0, 1}, {0, 2}, {1, 2}, {3, 4}}, 3, trials);
    test(6, {{0, 1}, {0, 2}, {1, 2}, {3, 4}, {3, 5}}, 1, trials);
    return 0;
}


