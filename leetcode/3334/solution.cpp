#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

long long maxScore(std::vector<int> nums)
{
    int n = static_cast<int>(nums.size());
    std::vector<long long> suf_gcd(n + 1, 0), suf_lcm(n + 1, 1);
    for (int i = n - 1; i >= 0; --i)
    {
        suf_gcd[i] = std::gcd(suf_gcd[i + 1], nums[i]);
        suf_lcm[i] = std::lcm(suf_lcm[i + 1], nums[i]);
    }
    long long result = suf_gcd[0] * suf_lcm[0], pre_gcd = 0, pre_lcm = 1;
    for (int i = 0; i < n; ++i)
    {
        result = std::max(result, std::gcd(pre_gcd, suf_gcd[i + 1])
                                * std::lcm(pre_lcm, suf_lcm[i + 1]));
        pre_gcd = std::gcd(pre_gcd, nums[i]);
        pre_lcm = std::lcm(pre_lcm, nums[i]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxScore(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4, 8, 16}, 64, trials);
    test({1, 2, 3, 4, 5}, 60, trials);
    test({3}, 9, trials);
    return 0;
}


