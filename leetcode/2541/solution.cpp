#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long minOperations(std::vector<int> nums1, std::vector<int> nums2, int k)
{
    if (k == 0) return (nums1 == nums2) - 1;
    long result = 0, ops_diff = 0;
    for (int i = 0, n = static_cast<int>(nums1.size()); i < n; ++i)
    {
        const int diff = nums1[i] - nums2[i];
        if (diff == 0) continue;
        if (diff % k != 0) return -1;
        int ops = diff / k;
        ops_diff += ops;
        result += std::abs(ops);
    }
    return (ops_diff == 0)?result / 2:-1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int k,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums1, nums2, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 3, 1, 4}, {1, 3, 7, 1}, 3, 2, trials);
    test({3, 8, 5, 2}, {2, 4, 1, 6}, 1, -1, trials);
    return 0;
}


