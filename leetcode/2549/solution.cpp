#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int distinctIntegers(int n)
{
    return std::max(1, n - 1);
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = distinctIntegers(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 4, trials);
    test(3, 2, trials);
    return 0;
}


