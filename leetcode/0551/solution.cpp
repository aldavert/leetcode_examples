#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkRecord(std::string s)
{
    int absent = 0;
    for (int late = 0; char c : s)
    {
        if (c == 'A')
        {
            ++absent;
            if (absent >= 2) return false;
        }
        if (c == 'L')
        {
            ++late;
            if (late >= 3) return false;
        }
        else late = 0;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkRecord(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("PPALLP", true, trials);
    test("PPALLL", false, trials);
    return 0;
}


