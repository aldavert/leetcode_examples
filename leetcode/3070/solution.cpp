#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countSubmatrices(std::vector<std::vector<int> > grid, int k)
{
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    int result = 0;
    std::vector<std::vector<int> > prefix(m + 1, std::vector<int>(n + 1));
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            prefix[i + 1][j + 1] = grid[i][j] + prefix[i][j + 1] + prefix[i + 1][j]
                                 - prefix[i][j];
            result += (prefix[i + 1][j + 1] <= k);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSubmatrices(grid, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{7, 6, 3}, {6, 6, 1}}, 18, 4, trials);
    test({{7, 2, 9}, {1, 5, 0}, {2, 6, 6}}, 20, 6, trials);
    return 0;
}


