#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 0
double nthPersonGetsNthSeat(int n)
{
    return 1.0 / (1 + (n != 1));
}
#else
double nthPersonGetsNthSeat(int n)
{
    return (n == 1)?1:0.5;
}
#endif

// ############################################################################
// ############################################################################

void test(int n, double solution, unsigned int trials = 1)
{
    double result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = nthPersonGetsNthSeat(n);
    showResult(std::abs(solution - result) < 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 1.0, trials);
    test(2, 0.5, trials);
    return 0;
}


