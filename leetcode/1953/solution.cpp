#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

long long numberOfWeeks(std::vector<int> milestones)
{
    const int max = *std::max_element(milestones.begin(), milestones.end());
    const long sum = std::accumulate(milestones.begin(), milestones.end(), 0L);
    return std::min(sum, 2 * (sum - max) + 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> milestones, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfWeeks(milestones);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 6, trials);
    test({5, 2, 1}, 7, trials);
    return 0;
}


