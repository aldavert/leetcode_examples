#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> nums, std::vector<int> numsDivide)
{
    int gcd = numsDivide[0];
        for (int num : numsDivide)
            gcd = std::__gcd(gcd, num);
    std::sort(nums.begin(), nums.end());
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        if (gcd % nums[i] == 0)
            return i;
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> numsDivide,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums, numsDivide);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 2, 4, 3}, {9, 6, 9, 3, 15}, 2, trials);
    test({4, 3, 6}, {8, 2, 6, 10}, -1, trials);
    return 0;
}


