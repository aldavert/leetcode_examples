#include "../common/common.hpp"
#include <fstream>
#include <sstream>

// ############################################################################
// ############################################################################

bool possibleBipartition(int n, std::vector<std::vector<int> > dislikes)
{
    std::vector<std::vector<int>> grundges(n + 1);
    for (const auto &grundge : dislikes)
    {
        grundges[grundge[0]].push_back(grundge[1]);
        grundges[grundge[1]].push_back(grundge[0]);
    }
    std::vector<bool> banned_groupA(n + 1, false), banned_groupB(n + 1, false);
    std::vector<bool> active(n + 1, true);
    active[0] = false;
    for (int start_number = 1; start_number <= n;)
    {
        while ((start_number < n) && (!active[start_number])) ++start_number;
        for (int grundge : grundges[start_number])
            banned_groupB[grundge] = true;
        active[start_number++] = false;
        for (bool changes = true; changes; ) 
        {
            changes = false;
            for (int number = start_number; number <= n; ++number)
            {
                if (!active[number]) continue;
                if ((!banned_groupA[number]) && (!banned_groupB[number]))
                    continue; // Do nothing.
                else if (!banned_groupA[number])
                {
                    // Number has to be added to on group A as it's disliked in B.
                    for (int grundge : grundges[number])
                        banned_groupA[grundge] = true;
                    active[number] = false;
                    changes = true;
                }
                else if (!banned_groupB[number])
                {
                    // Number has to be added to on group B as it's disliked in A.
                    for (int grundge : grundges[number])
                        banned_groupB[grundge] = true;
                    active[number] = false;
                    changes = true;
                }
                // The number is banned from two groups so return false.
                else return false;
            }
        }
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > dislikes,
          bool solution,
          unsigned int trials = 1)
{
    int result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = possibleBipartition(n, dislikes);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
    [[maybe_unused]] auto load =
        [](const char * filename) -> std::vector<std::vector<int> >
    {
        std::ifstream file(filename);
        std::vector<std::vector<int> > output;
        std::string line;
        while (std::getline(file, line))
        {
            std::istringstream input(line);
            int first, second;
            input >> first >> second;
            output.push_back({first, second});
        }
        file.close();
        return output;
    };
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{1, 2}, {3, 4}, {4, 5}, {3, 5}}, false, trials);
    test(4, {{1, 2}, {1, 3}, {2, 4}}, true, trials);
    test(3, {{1, 2}, {1, 3}, {2, 3}}, false, trials);
    test(5, {{1, 2}, {2, 3}, {3, 4}, {4, 5}, {1, 5}}, false, trials);
    test(100, load("dataA.txt"), false, trials);
    test(200, load("dataB.txt"), false, trials);
    return 0;
}


