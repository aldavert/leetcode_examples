#include "../common/common.hpp"
#include <unordered_map>

constexpr int null = -1;

// ############################################################################
// ############################################################################

class NumberContainers
{
    std::unordered_map<int, int> m_index_to_number;
    std::unordered_map<int, std::set<int> > m_number_to_indices;
public:
    NumberContainers(void) = default;
    void change(int index, int number)
    {
        if (auto it = m_index_to_number.find(index); it != m_index_to_number.end())
        {
            int original_number = it->second;
            auto &indices = m_number_to_indices[original_number];
            indices.erase(index);
            if (indices.empty())
                m_number_to_indices.erase(original_number);
            it->second = number;
        }
        else m_index_to_number[index] = number;
        m_number_to_indices[number].insert(index);
    }
    int find(int number)
    {
        auto it = m_number_to_indices.find(number);
        if ((it == m_number_to_indices.end()) || (it->second.empty()))
            return -1;
        return *it->second.begin();
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        NumberContainers obj;
        result.clear();
        for (size_t i = 0; i < input.size(); ++i)
        {
            if (input[i].size() == 2)
            {
                obj.change(input[i][0], input[i][1]);
                result.push_back(null);
            }
            else if (input[i].size() == 1)
                result.push_back(obj.find(input[i][0]));
            else
            {
                std::cerr << "[ERROR] Unexpected number of parameters.\n";
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{10}, {2, 10}, {1, 10}, {3, 10}, {5, 10}, {10}, {1, 20}, {10}},
         {-1, null, null, null, null, 1, null, 2}, trials);
    return 0;
}


