#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<std::string> spellchecker(std::vector<std::string> wordlist,
                                      std::vector<std::string> queries)
{
    std::vector<std::string> result;
    std::unordered_map<std::string, std::string> dictionary;
    auto lowerKey = [](const std::string &word) -> std::string
    {
        std::string s{"$"};
        for (char c : word) s += static_cast<char>(std::tolower(c));
        return s;
    };
    auto vowelKey = [](const std::string &word) -> std::string
    {
        static constexpr std::string_view vowels = "aeiouAEIOU";
        std::string s;
        for (char c : word)
            s += (vowels.find(c) != std::string_view::npos)?'*':static_cast<char>(std::tolower(c));
        return s;
    };
    
    for (const auto &word : wordlist)
    {
        dictionary.insert({word, word});
        dictionary.insert({lowerKey(word), word});
        dictionary.insert({vowelKey(word), word});
    }
    for (const auto &query : queries)
    {
        if (auto it = dictionary.find(query); it != dictionary.cend())
            result.push_back(it->second);
        else if (it = dictionary.find(lowerKey(query)); it != dictionary.cend())
            result.push_back(it->second);
        else if (it = dictionary.find(vowelKey(query)); it != dictionary.cend())
            result.push_back(it->second);
        else result.push_back("");
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> wordlist,
          std::vector<std::string> queries,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = spellchecker(wordlist, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"KiTe", "kite", "hare", "Hare"},
         {"kite", "Kite", "KiTe", "Hare", "HARE", "Hear", "hear", "keti", "keet", "keto"},
         {"kite", "KiTe", "KiTe", "Hare", "hare", "", "", "KiTe", "", "KiTe"}, trials);
    test({"yellow"}, {"YellOw"}, {"yellow"}, trials);
    return 0;
}


