#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minDeletions(std::string s)
{
    int frequency[26] = {};
    for (char c : s)
        ++frequency[c - 'a'];
    std::sort(&frequency[0], &frequency[26], std::greater<int>());
    int remove = 0;
    for (int i = 1; (i < 26) && (frequency[i] > 0); ++i)
    {
        if (frequency[i] >= frequency[i - 1])
        {
            int diff = frequency[i] - frequency[i - 1] + (frequency[i - 1] > 0);
            remove += diff;
            frequency[i] -= diff;
        }
    }
    return remove;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDeletions(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aab", 0, trials);
    test("aaabbbcc", 2, trials);
    test("ceabaacb", 2, trials);
    test("bbcebab", 2, trials);
    return 0;
}
