#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

long long calculateScore(std::string s)
{
    std::stack<size_t> last[26];
    std::vector<bool> marked(s.size(), false);
    long long result = 0;
    for (size_t i = 0; i < s.size(); ++i)
    {
        int mirror_idx = 26 - (s[i] - 'a') - 1;
        while (!last[mirror_idx].empty() && marked[last[mirror_idx].top()])
            last[mirror_idx].pop();
        if (!last[mirror_idx].empty())
        {
            result += i - last[mirror_idx].top();
            marked[last[mirror_idx].top()] = true;
            marked[i] = true;
            last[mirror_idx].pop();
        }
        last[s[i] - 'a'].push(i);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = calculateScore(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aczzx", 5, trials);
    test("abcdef", 0, trials);
    test("eockppxdqclkhjgvnw", 50, trials);
    test("azapfwonwwcdagew", 3, trials);
    return 0;
}


