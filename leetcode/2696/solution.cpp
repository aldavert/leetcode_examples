#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

int minLength(std::string s)
{
    std::stack<char> stack;
    int result = static_cast<int>(s.size());
    for (char letter : s)
    {
        if ((letter == 'A') || (letter == 'C'))
            stack.push(letter);
        else if (!stack.empty() && ((letter == 'B') || (letter == 'D')))
        {
            if (stack.top() != letter - 1)
                stack = {};
            else stack.pop(), result -= 2;
        }
        else stack = {};
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minLength(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ABFCACDB", 2, trials);
    test("ACBBD", 5, trials);
    test("VKBAJBOYY", 9, trials);
    return 0;
}


