import sqlite3

sql_problem = """
SELECT teacher_id, COUNT(DISTINCT subject_id) AS cnt
FROM Teacher
GROUP BY teacher_id
"""

solution_text = """
| 1          | 2   |
| 2          | 4   |
"""

def loadDB():
    conn = sqlite3.connect(":memory:")
    cursor = conn.cursor()
    f = open('schema.sql')
    for command in f.readlines():
        if 'TRUNCATE TABLE' in command:
            command = command.replace('TRUNCATE TABLE', 'DELETE FROM')
        cursor.execute(command)
    f.close()
    cursor.close()
    return conn

def parseSolution():
    result = []
    for row in solution_text.split('\n'):
        if not '|' in row:
            continue
        result.append(tuple(map(lambda x: int(x) if x != 'Null' else None, filter(lambda x: len(x) > 0, [x.strip() for x in row.split('|')]))))
    return result

if __name__ == '__main__':
    solution = parseSolution()
    conn = loadDB()
    cursor = conn.cursor()
    cursor.execute(sql_problem)
    rows = cursor.fetchall()
    row_idx = 0
    correct = True
    for row in rows:
        correct = correct and (row == solution[row_idx])
        row_idx += 1
    cursor.close()
    if correct:
        print("[CORRECT] QUERY OUTPUT IS EQUAL TO THE EXPECTED OUTPUT\n")
    else:
        print("[WRONG] QUERY OUTPUT IS DIFFERENT TO THE EXPECTED OUTPUT\n")
    print("Query output:")
    for row in rows:
        print(row)
    print("Expected output:")
    for row in solution:
        print(row)


