#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxOperations(std::string s)
{
    int result = 0, ones = 0;
    for (int i = 0, n = static_cast<int>(s.size()); i < n; ++i)
    {
        if (s[i] == '1') ++ones;
        else if ((i + 1 == n) || (s[i + 1] == '1')) result += ones;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxOperations(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1001101", 4, trials);
    test("00111", 0, trials);
    return 0;
}


