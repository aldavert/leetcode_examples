#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<double> calcEquation(
        std::vector<std::vector<std::string> > equations,
        std::vector<double> values,
        std::vector<std::vector<std::string> > queries)
{
    const int n = static_cast<int>(values.size());
    std::vector<double> result(queries.size(), -1.0);
    std::unordered_map<std::string, std::unordered_map<std::string, double> > graph;
    
    auto divide = [&](auto &&self,
                      std::string a,
                      std::string c,
                      std::unordered_set<std::string> seen) -> double
    {
        if (a == c) return 1.0;
        seen.insert(a);
        for (const auto &[b, value] : graph.at(a))
        {
            if (seen.count(b)) continue;
            const double res = self(self, b, c, seen);
            if (res > 0) return value * res;
        }
        return -1;
    };
    
    for (int i = 0; i < n; ++i)
    {
        graph[equations[i][0]][equations[i][1]] = values[i];
        graph[equations[i][1]][equations[i][0]] = 1 / values[i];
    }
    for (int i = 0; const auto &q : queries)
    {
        if (graph.count(q[0]) && graph.count(q[1]))
            result[i] = divide(divide, q[0], q[1], {});
        ++i;
    }
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<double> &left, const std::vector<double> &right)
{
    if (left.size() != right.size()) return false;
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (std::abs(left[i] - right[i]) > 0.00001) return false;
    return true;
}

void test(std::vector<std::vector<std::string> > equations,
          std::vector<double> values,
          std::vector<std::vector<std::string> > queries,
          std::vector<double> solution,
          unsigned int trials = 1)
{
    std::vector<double> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = calcEquation(equations, values, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{"a", "b"}, {"b", "c"}},
         {2.0, 3.0},
         {{"a", "c"}, {"b", "a"}, {"a", "e"}, {"a", "a"}, {"x", "x"}},
         {6.00000, 0.50000, -1.00000, 1.00000, -1.00000}, trials);
    test({{"a", "b"}, {"b", "c"}, {"bc", "cd"}},
         {1.5, 2.5, 5.0},
         {{"a", "c"}, {"c", "b"}, {"bc", "cd"}, {"cd", "bc"}},
         {3.75000, 0.40000, 5.00000, 0.20000}, trials);
    test({{"a", "b"}},
         {0.5},
         {{"a", "b"}, {"b", "a"}, {"a", "c"}, {"x", "y"}},
         {0.50000, 2.00000, -1.00000, -1.00000}, trials);
    return 0;
}


