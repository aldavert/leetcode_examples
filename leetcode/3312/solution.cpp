#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> gcdValues(std::vector<int> nums, std::vector<long long> queries)
{
    int max_num = *std::max_element(nums.begin(), nums.end());
    std::vector<int> result, count_divisor(max_num + 1);
    std::vector<long> count_gcd_pair(max_num + 1), prefix_count_gcd_pair{0};
    
    for (const int num : nums)
    {
        for (int i = 1; i * i <= num; ++i)
        {
            if (num % i == 0)
            {
                ++count_divisor[i];
                if (i != num / i)
                    ++count_divisor[num / i];
            }
        }
    }
    for (int gcd = max_num; gcd >= 1; --gcd)
    {
        count_gcd_pair[gcd] =
            count_divisor[gcd] * static_cast<long>(count_divisor[gcd] - 1) / 2;
        for (int largerGcd = 2 * gcd; largerGcd <= max_num; largerGcd += gcd)
            count_gcd_pair[gcd] -= count_gcd_pair[largerGcd];
    }
    for (int gcd = 1; gcd <= max_num; ++gcd)
        prefix_count_gcd_pair.push_back(prefix_count_gcd_pair.back()
                                      + count_gcd_pair[gcd]);
    for (const long query : queries)
    {
        int l = 1;
        int r = static_cast<int>(prefix_count_gcd_pair.size()) - 1;
        while (l < r)
        {
            if (int m = (l + r) / 2; prefix_count_gcd_pair[m] < query + 1)
                l = m + 1;
            else r = m;
        }
        result.push_back(l);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<long long> queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = gcdValues(nums, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 4}, {0, 2, 2}, {1, 2, 2}, trials);
    test({4, 4, 2, 1}, {5, 3, 1, 0}, {4, 2, 1, 1}, trials);
    test({2, 2}, {0, 0}, {2, 2}, trials);
    return 0;
}


