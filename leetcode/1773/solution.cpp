#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countMatches(std::vector<std::vector<std::string> > items,
                 std::string ruleKey,
                 std::string ruleValue)
{
    const int index = 2 - 2 * (ruleKey == "type") - (ruleKey == "color");
    int count = 0;
    for (size_t i = 0, n = items.size(); i < n; ++i)
        count += items[i][index] == ruleValue;
    return count;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<std::string> > items,
          std::string ruleKey,
          std::string ruleValue,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countMatches(items, ruleKey, ruleValue);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{"phone", "blue", "pixel"},
          {"computer", "silver", "lenovo"},
          {"phone", "gold", "iphone"}}, "color", "silver", 1, trials);
    test({{"phone", "blue", "pixel"},
          {"computer", "silver", "phone"},
          {"phone", "gold", "iphone"}}, "type", "phone", 2, trials);
    return 0;
}


