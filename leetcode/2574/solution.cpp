#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

std::vector<int> leftRightDifference(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> result, right_sum(n);
    for (int i = n - 1, accum = 0; i >= 0; --i)
        right_sum[i] = std::exchange(accum, accum + nums[i]);
    for (int i = 0, accum = 0; i < n; accum += nums[i++])
        result.push_back(std::abs(accum - right_sum[i]));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = leftRightDifference(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 4, 8, 3}, {15, 1, 11, 22}, trials);
    test({1}, {0}, trials);
    return 0;
}


