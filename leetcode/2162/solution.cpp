#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minCostSetTime(int startAt, int moveCost, int pushCost, int targetSeconds)
{
    int mins = (targetSeconds > 5999)?99:targetSeconds / 60;
    int secs = targetSeconds - mins * 60;
    auto calcCost = [&](int m, int s) -> int
    {
        int cost = 0;
        char curr = static_cast<char>('0' + startAt);
        for (char c : std::to_string(m * 100 + s))
        {
            if (c == curr) cost += pushCost;
            else
            {
                cost += moveCost + pushCost;
                curr = c;
            }
        }
        return cost;
    };
    
    int result = std::numeric_limits<int>::max();
    for (; secs < 100; --mins, secs += 60)
        result = std::min(result, calcCost(mins, secs));
    return result;
}

// ############################################################################
// ############################################################################

void test(int startAt,
          int moveCost,
          int pushCost,
          int targetSeconds,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCostSetTime(startAt, moveCost, pushCost, targetSeconds);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 2, 1, 600, 6, trials);
    test(0, 1, 2, 76, 6, trials);
    return 0;
}


