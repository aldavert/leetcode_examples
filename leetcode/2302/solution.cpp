#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long countSubarrays(std::vector<int> nums, long long k)
{
    const long n = static_cast<long>(nums.size());
    long result = 0;
    for (long l = 0, r = 0, sum = 0; r < n; ++r)
    {
        for (sum += nums[r]; sum * (r - l + 1) >= k; ++l) sum -= nums[l];
        result += r - l + 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          long long k,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSubarrays(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 4, 3, 5}, 10, 6, trials);
    test({1, 1, 1}, 5, 5, trials);
    return 0;
}


