#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int rob(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    if (n == 1) return nums[0];
    else if (n > 2)
    {
        nums[n - 3] += nums[n - 1];
        for (int i = n - 4; i >= 0; --i)
            nums[i] += std::max(nums[i + 2], nums[i + 3]);
    }
    return std::max(nums[0], nums[1]);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = rob(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 1}, 4, trials);
    test({2, 7, 9, 3, 1}, 12, trials);
    test({2, 7, 9, 1, 1, 3, 1}, 14, trials);
    return 0;
}


