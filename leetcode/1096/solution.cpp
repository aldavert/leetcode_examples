#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> braceExpansionII(std::string expression)
{
    auto merge = [](std::vector<std::vector<std::string> > &groups,
                    const std::vector<std::string> &group) -> void
    {
        if (groups.back().empty())
        {
            groups.back() = group;
            return;
        }
        std::vector<std::string> merged_group;
        for (auto &word1 : groups.back())
            for (auto &word2 : group)
                merged_group.push_back(word1 + word2);
        groups.back() = merged_group;
    };
    auto dp_process = [&](auto &&self, int s, int e) -> std::vector<std::string>
    {
        std::set<std::string> result;
        std::vector<std::vector<std::string> > groups{{}};
        int layer = 0, left = 0;
        for (int i = s; i <= e; ++i)
        {
            if ((expression[i] == '{') && (++layer == 1))
                left = i + 1;
            else if ((expression[i] == '}') && (--layer == 0))
                merge(groups, self(self, left, i - 1));
            else if ((expression[i] == ',') && (layer == 0))
                groups.push_back({});
            else if (layer == 0)
                merge(groups, {std::string(1, expression[i])});
        }
        for (const std::vector<std::string> &group : groups)
            for (const std::string &word : group)
                result.insert(word);
        return {result.begin(), result.end()};
    };
    return dp_process(dp_process, 0, static_cast<int>(expression.size()) - 1);
}

// ############################################################################
// ############################################################################

void test(std::string expression,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = braceExpansionII(expression);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("{a,b}{c,{d,e}}", {"ac", "ad", "ae", "bc", "bd", "be"}, trials);
    test("{{a,z},a{b,c},{ab,z}}", {"a", "ab", "ac", "z"}, trials);
    return 0;
}


