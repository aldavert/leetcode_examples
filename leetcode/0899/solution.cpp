#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string orderlyQueue(std::string s, int k)
{
    if (k >= 2) std::sort(s.begin(), s.end());
    else
    {
        const size_t n = s.size();
        std::string r = s;
        for (size_t i = 0; i < n; r = r.substr(1, n - 1) + r.substr(0, 1), ++i)
            s = std::min(s, r);
    }
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = orderlyQueue(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("cba", 1, "acb", trials);
    test("baaca", 3, "aaabc", trials);
    return 0;
}


