#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxScore(std::vector<int> nums)
{
    long prefix = 0;
    std::sort(nums.begin(), nums.end(), std::greater<>());
    const int n = static_cast<int>(nums.size());
    for (int i = 0; i < n; ++i)
    {
        prefix += nums[i];
        if (prefix <= 0)
            return i;
    }
    return n;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxScore(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, -1, 0, 1, -3, 3, -3}, 6, trials);
    test({-2, -3, 0}, 0, trials);
    return 0;
}


