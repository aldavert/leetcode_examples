#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int longestSubarray(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = 0;
    for (int i = 0, j = 0, count = 0; j < n; ++j)
    {
        if (nums[j] == 0) ++count;
        while (count > 1)
        {
            if (nums[i] == 0) --count;
            ++i;
        }
        result = std::max(result, j - i);
    }
    return result;
}
#elif 0
int longestSubarray(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = 0;
    bool no_zeros = true;
    for (int left = 0, right = 0, previous = 0, zeros = 0; right < n;)
    {
        zeros = 0;
        while ((left < n) && (nums[left] == 0)) ++left, ++zeros;
        right = left;
        while ((right < n) && (nums[right] == 1)) ++right;
        int size = right - left;
        no_zeros = zeros == 0;
        result = std::max(result, (zeros == 1)?(size + previous):size);
        previous = size;
        left = right;
    }
    return result - no_zeros;
}
#else
int longestSubarray(std::vector<int> nums)
{
    int result = 0, ones = 0, zeros = 0, previous = 0;
    bool no_zeros = true, can_merge = false;
    for (int bit : nums)
    {
        if (bit == 1)
        {
            if (ones == 0)
                can_merge = zeros == 1;
            ++ones;
            zeros = 0;
        }
        else
        {
            if (zeros == 0)
            {
                result = std::max(result, (can_merge)?ones + previous:ones);
                previous = ones;
                ones = 0;
            }
            ++zeros;
            no_zeros = false;
        }
    }
    result = std::max(result, (can_merge)?ones + previous:ones);
    return result - no_zeros;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestSubarray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 0, 1}, 3, trials);
    test({0, 1, 1, 1, 0, 1, 1, 0, 1}, 5, trials);
    test({0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1}, 4, trials);
    test({1, 1, 1}, 2, trials);
    return 0;
}


