#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int incremovableSubarrayCount(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int start_index = 0;
    for (int i = n - 2; i >= 0; --i)
    {
        if (nums[i] >= nums[i + 1])
        {
            start_index = i + 1;
            break;
        }
    }
    if (start_index == 0) return n * (n + 1) / 2;
    int result = n - start_index + 1;
    for (int i = 0, j = start_index; i < start_index; ++i)
    {
        if ((i > 0) && (nums[i] <= nums[i - 1])) break;
        while ((j < n) && (nums[i] >= nums[j])) ++j;
        result += n - j + 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = incremovableSubarrayCount(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 10, trials);
    test({6, 5, 7, 8}, 7, trials);
    test({8, 7, 6, 6}, 3, trials);
    return 0;
}


