#include "../common/common.hpp"
#include <functional>
#include <thread>
#include <mutex>

struct Action
{
    int id = -1;
    int fork = -1;  // 1: left, 2: right
    int operation = -1; // 1: pick, 2: put, 3: eat
    inline bool operator==(const Action &other) const
    {
        return (id == other.id)
            && (fork == other.fork)
            && (operation == other.operation);
    }
};
std::vector<Action> output;
template <int current_philosopher>
void pickLeftFork(void) { output.push_back({current_philosopher, 1, 1}); }
template <int current_philosopher>
void pickRightFork(void) { output.push_back({current_philosopher, 2, 1}); }
template <int current_philosopher>
void eat(void) { output.push_back({current_philosopher, 1, 3}); }
template <int current_philosopher>
void putLeftFork(void) { output.push_back({current_philosopher, 1, 2}); }
template <int current_philosopher>
void putRightFork(void) { output.push_back({current_philosopher, 2, 2}); }

std::ostream& operator<<(std::ostream &out, const Action &action)
{
    out << '{' << action.id << ", " << action.fork << ", " << action.operation <<  '}';
    return out;
}

// ############################################################################
// ############################################################################

class DiningPhilosophers
{
    std::mutex mutex;
public:
    DiningPhilosophers(void)
    {
    }
    void wantsToEat(int /*philosopher*/,
                    std::function<void(void)> pickLeftFork,
                    std::function<void(void)> pickRightFork,
                    std::function<void(void)> eat,
                    std::function<void(void)> putLeftFork,
                    std::function<void(void)> putRightFork)
    {
        mutex.lock();
        pickLeftFork();
        pickRightFork();
        eat();
        putLeftFork();
        putRightFork();
        mutex.unlock();
    }
};

// ############################################################################
// ############################################################################

template <int id>
void call(const int n, DiningPhilosophers &obj)
{
    for (int i = 0; i < n; ++i)
        obj.wantsToEat(id,
                       pickLeftFork<id>,
                       pickRightFork<id>,
                       eat<id>,
                       putLeftFork<id>,
                       putRightFork<id>);
}

void test(int n, std::vector<Action> solution, unsigned int trials = 1)
{
    std::vector<Action> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        DiningPhilosophers obj;
        output.clear();
        std::thread t1([&]() { call<1>(n, obj); }),
                    t2([&]() { call<2>(n, obj); }),
                    t3([&]() { call<3>(n, obj); }),
                    t4([&]() { call<4>(n, obj); }),
                    t5([&]() { call<5>(n, obj); });
        t1.join();
        t2.join();
        t3.join();
        t4.join();
        t5.join();
        result = output;
    }
    showResult(true, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, {{4, 2, 1}, {4, 1, 1}, {0, 1, 1}, {2, 2, 1}, {2, 1, 1}, {2, 0, 3},
             {2, 1, 2}, {2, 2, 2}, {4, 0, 3}, {4, 1, 2}, {0, 2, 1}, {4, 2, 2},
             {3, 2, 1}, {3, 1, 1}, {0, 0, 3}, {0, 1, 2}, {0, 2, 2}, {1, 2, 1},
             {1, 1, 1}, {3, 0, 3}, {3, 1, 2}, {3, 2, 2}, {1, 0, 3}, {1, 1, 2},
             {1, 2, 2}}, trials);
    return 0;
}


