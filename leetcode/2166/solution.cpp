#include "../common/common.hpp"

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class Bitset
{
    std::string m_s;
    std::string m_r;
    int m_count = 0;
public:
    Bitset(int size) : m_s(size, '0'), m_r(size, '1') {}
    void fix(int idx)
    {
        if (m_s[idx] == '0') ++m_count;
        m_s[idx] = '1';
        m_r[idx] = '0';
    }
    void unfix(int idx)
    {
        if (m_s[idx] == '1') --m_count;
        m_s[idx] = '0';
        m_r[idx] = '1';
    }
    void flip(void)
    {
        std::swap(m_s, m_r);
        m_count = static_cast<int>(m_s.size()) - m_count;
    }
    bool all(void)
    {
        return m_count == static_cast<int>(m_s.size());
    }
    bool one(void)
    {
        return m_count;
    }
    int count(void)
    {
        return m_count;
    }
    std::string toString(void)
    {
        return m_s;
    }
};

// ############################################################################
// ############################################################################

enum class OP { FIX, UNFIX, FLIP, ALL, ONE, COUNT, TOSTRING };

void test(int size,
          std::vector<OP> operations,
          std::vector<int> input,
          std::vector<std::variant<int, std::string> > solution,
          unsigned int trials = 1)
{
    std::vector<std::variant<int, std::string> > result;
    std::variant<int, std::string> value;
    for (unsigned int t = 0; t < trials; ++t)
    {
        Bitset obj(size);
        result.clear();
        for (size_t i = 0; i < operations.size(); ++i)
        {
            switch (operations[i])
            {
            case OP::FIX:
                obj.fix(input[i]);
                std::get<int>(value) = null;
                result.push_back(value);
                break;
            case OP::UNFIX:
                obj.unfix(input[i]);
                std::get<int>(value) = null;
                result.push_back(value);
                break;
            case OP::FLIP:
                obj.flip();
                std::get<int>(value) = null;
                result.push_back(value);
                break;
            case OP::ALL:
                std::get<int>(value) = obj.all();
                result.push_back(value);
                break;
            case OP::ONE:
                std::get<int>(value) = obj.one();
                result.push_back(value);
                break;
            case OP::COUNT:
                std::get<int>(value) = obj.count();
                result.push_back(value);
                break;
            case OP::TOSTRING:
                result.push_back(obj.toString());
                break;
            default:
                std::cerr << "[ERROR] UNKNOWN PARAMETER.\n";
                break;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5,
         {OP::FIX, OP::FIX, OP::FLIP, OP::ALL, OP::UNFIX, OP::FLIP, OP::ONE,
          OP::UNFIX, OP::COUNT, OP::TOSTRING},
         {3, 1, null, null, 0, null, null, 0, null, null},
         {null, null, null, false, null, null, true, null, 2, "01010"},
         trials);
    return 0;
}


