#include "../common/common.hpp"
#include <unordered_map>
#include <limits>
#include <cmath>

// ############################################################################
// ############################################################################

double minAreaFreeRect(std::vector<std::vector<int> > points)
{
    auto hash = [](const std::vector<int> &p, const std::vector<int> &q) -> int
    {
        return static_cast<int>((static_cast<long>(p[0] + q[0]) << 16) + (p[1] + q[1]));
    };
    auto distance = [](int px, int py, int qx, int qy) -> long
    {
        return static_cast<long>(px - qx) * static_cast<long>(px - qx)
             + static_cast<long>(py - qy) * static_cast<long>(py - qy);
    };
    long result = std::numeric_limits<long>::max();
    std::unordered_map<int, std::vector<std::tuple<int, int, int, int> > > center_to_points;
    
    for (const auto &a : points)
        for (const auto &b : points)
            center_to_points[hash(a, b)].emplace_back(a[0], a[1], b[0], b[1]);
    for (const auto &[_, selected_points] : center_to_points)
    {
        for (const auto &[ax, ay, bx, by] : selected_points)
        {
            for (const auto &[cx, cy, dx, dy] : selected_points)
            {
                if ((cx - ax) * (dx - ax) + (cy - ay) * (dy - ay) == 0)
                {
                    long squared_area =
                        distance(ax, ay, cx, cy) * distance(ax, ay, dx, dy);
                    if (squared_area > 0)
                        result = std::min(result, squared_area);
                }
            }
        }
    }
    return (result == std::numeric_limits<long>::max())?0:std::sqrt(result);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points,
          double solution,
          unsigned int trials = 1)
{
    double result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minAreaFreeRect(points);
    showResult(std::abs(solution - result) < 1e-4, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 1}, {1, 0}, {0, 1}}, 2.0, trials);
    test({{0, 1}, {2, 1}, {1, 1}, {1, 0}, {2, 0}}, 1.0, trials);
    test({{0, 3}, {1, 2}, {3, 1}, {1, 3}, {2, 1}}, 0.0, trials);
    return 0;
}


