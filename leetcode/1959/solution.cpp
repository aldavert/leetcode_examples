#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

#if 1
int minSpaceWastedKResizing(std::vector<int> nums, int k)
{
    int dp[201][201] = {};
    int waste[200][200] = {};
    const int n = static_cast<int>(nums.size());
    if (n == ++k) return 0;
    for (int l = 0; l < n; ++l)
        for (int r = l, total = 0, max_number = nums[l]; r < n; ++r)
            total += nums[r],
            max_number = std::max(max_number, nums[r]),
            waste[l][r] = (r - l + 1) * max_number - total;
    std::memset(dp, 100, sizeof(dp));
    const int INF = dp[0][0];
    dp[0][0] = 0;
    for(int i = 0; i < n; ++i)
        for(int j = 0; j < k; ++j)
            if (dp[i][j] < INF)
                for (int ni = i; ni < n; ++ni)
                    dp[ni + 1][j + 1] = std::min(dp[ni + 1][j + 1],
                                                 dp[i][j] + waste[i][ni]);
    return dp[n][k];
}
#else
int minSpaceWastedKResizing(std::vector<int> nums, int k)
{
    constexpr int MAX = 200'000'000;
    std::vector<std::vector<int> > mem(nums.size(), std::vector<int>(k + 1, -1));
    auto process = [&](auto &&self, size_t i, int l)
    {
        if (i == nums.size()) return 0;
        if (l == -1) return MAX;
        if (mem[i][l] != -1) return mem[i][l];
        int result = MAX, sum = 0, max_number = nums[i];
        for (size_t j = i; j < nums.size(); ++j)
        {
            sum += nums[j];
            max_number = std::max(max_number, nums[j]);
            result = std::min(result, self(self, j + 1, l - 1)
                                    + max_number * static_cast<int>(j - i + 1) - sum);
        }
        return mem[i][l] = result;
    };
    return process(process, 0, k);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSpaceWastedKResizing(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 20}, 0, 10, trials);
    test({10, 20, 30}, 1, 10, trials);
    test({10, 20, 15, 30, 20}, 2, 15, trials);
    return 0;
}


