#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minSteps(int n)
{
    if (n <= 1) return 0;
    int dp[1001];
    dp[0] = 0;
    dp[1] = 1;
    for (int i = 2; i <= n; ++i)
    {
        dp[i] = i;
        for (int j = i / 2; j > 2; --j)
        {
            if (i % j == 0)
            {
                dp[i] = dp[j] + i / j;
                break;
            }
        }
    }
    return dp[n];
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSteps(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 3, trials);
    test(1, 0, trials);
    test(30, 10, trials);
    test(300, 17, trials);
    test(500, 19, trials);
    test(73, 73, trials);
    test(1000, 21, trials);
    test(999, 46, trials);
    return 0;
}


