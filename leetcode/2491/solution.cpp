#include "../common/common.hpp"
#include <unordered_map>
#include <numeric>

// ############################################################################
// ############################################################################

long long dividePlayers(std::vector<int> skill)
{
    const int n = static_cast<int>(skill.size());
    const int team_skill = std::accumulate(skill.begin(), skill.end(), 0) / (n / 2);
    long result = 0;
    
    std::unordered_map<int, int> count;
    for (int s : skill)  ++count[s];
    for (const auto& [s, freq] : count)
    {
        const int required_skill = team_skill - s;
        if (const auto it = count.find(required_skill);
            (it == count.end()) || (it->second != freq)) return -1;
        result += static_cast<long>(s) * required_skill * freq;
    }
    return result / 2;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> skill, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = dividePlayers(skill);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 5, 1, 3, 4}, 22, trials);
    test({3, 4}, 12, trials);
    test({1, 1, 2, 3}, -1, trials);
    return 0;
}


