#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestNiceSubarray(std::vector<int> nums)
{
    int result = 0;
    for (int l = 0, r = 0, used = 0, n = static_cast<int>(nums.size()); r < n; ++r)
    {
        while (used & nums[r])
            used ^= nums[l++];
        used |= nums[r];
        result = std::max(result, r - l + 1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestNiceSubarray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 8, 48, 10}, 3, trials);
    test({3, 1, 5, 11, 13}, 1, trials);
    return 0;
}


