#include "../common/common.hpp"
#include <limits>
#include <cstring>

// ############################################################################
// ############################################################################

long long minCost(int n, std::vector<std::vector<int> > cost)
{
    constexpr long long MAX = std::numeric_limits<long long>::max();
    long long dp[3][3] = {};
    for (int i = 0, j = n - 1; i <= j; ++i, --j)
    {
        long long current[3][3] = {{MAX, MAX, MAX}, {MAX, MAX, MAX}, {MAX, MAX, MAX}};
        for (int x = 0; x < 3; ++x)
        {
            for (int y = 0; y < 3; ++y)
            {
                if ((x == y) and (i != j)) continue;
                for (int a = 0; a < 3; ++a)
                    for (int b = 0; b < 3; ++b)
                        if ((a != x) && (b != y))
                            current[x][y] = std::min(current[x][y], dp[a][b]);
                current[x][y] += cost[i][x] + (i != j) * cost[j][y];
            }
        }
        std::memcpy(dp, current, sizeof(dp));
    }
    long long result = MAX;
    for (int i = 0; i < 3; ++i)
        for (int j = 0; j < 3; ++j)
            result = std::min(result, dp[i][j]);
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > cost,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCost(n, cost);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{3, 5, 7}, {6, 2, 9}, {4, 8, 1}, {7, 3, 5}}, 9, trials);
    test(6, {{2, 4, 6}, {5, 3, 8}, {7, 1, 9}, {4, 6, 2}, {3, 5, 7}, {8, 2, 4}}, 18, trials);
    return 0;
}


