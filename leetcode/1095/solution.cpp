#include "../common/common.hpp"
#include <unordered_map>

class MountainArray
{
    std::vector<int> m_data;
    int m_number_calls;
public:
    MountainArray(std::vector<int> data) : m_data(data), m_number_calls(0) {}
    inline int get(int index) { ++m_number_calls; return m_data[index]; }
    inline int length() const { return static_cast<int>(m_data.size()); }
    inline int calls() const { return m_number_calls; }
};

// ############################################################################
// ############################################################################

int findInMountainArray(int target, MountainArray &mountainArr)
{
    std::vector<int> lut_value(mountainArr.length());
    std::vector<bool> lut_active(mountainArr.length(), false);
    auto get = [&](int index) -> int
    {
        if (lut_active[index]) return lut_value[index];
        lut_active[index] = true;
        return lut_value[index] = mountainArr.get(index);
    };
    auto searchMax = [&](auto &&self, int left, int right) -> int
    {
        if (left == right) return left;
        if (int mid = (left + right) / 2; get(mid) > get(mid + 1))
            return self(self, left, mid);
        else return self(self, mid + 1, right);
    };
    int max_index = searchMax(searchMax, 0, mountainArr.length() - 1);
    for (int left = 0, right = max_index; left <= right; )
    {
        int mid = (left + right) / 2;
        if (int value = get(mid); value == target) return mid;
        else if (value < target) left = mid + 1;
        else right = mid - 1;
    };
    for (int left = max_index, right = mountainArr.length() - 1; left <= right; )
    {
        int mid = (left + right) / 2;
        if (int value = get(mid); value == target) return mid;
        else if (value > target) left = mid + 1;
        else right = mid - 1;
    };
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> array,
          int target,
          int solution,
          bool verbose,
          unsigned int trials = 1)
{
    int result = -1;
    bool right = true;
    for (unsigned int i = 0; i < trials; ++i)
    {
        MountainArray obj(array);
        result = findInMountainArray(target, obj);
        right = right && (obj.calls() <= 100);
    }
    if (verbose)
        showResult((solution == result) && right, solution, result);
    else if (!((solution == result) && right))
        showResult(false, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 3, 1}, 3, 2, trials);
    test({0, 1, 2, 4, 2, 1}, 3, -1, trials);
    auto generate = [](int mount, int size) -> std::vector<int>
    {
        std::vector<int> vec(size);
        int value = size - mount + 100;
        for (int i = 0; i < mount; ++i) vec[i] = value++;
        for (int i = mount; i < size; ++i) vec[i] = value--;
        return vec;
    };
    auto bulkTest = [&](int mount, int size) -> void
    {
        std::cout << "Testing bulk values for mount = " << mount << " and size = " << size << "...";
        std::cout.flush();
        auto values = generate(mount, size);
        std::unordered_map<int, int> lut;
        for (int i = 0, n = static_cast<int>(values.size()); i < n; ++i)
        {
            if (auto search = lut.find(values[i]); search != lut.end())
                test(values, values[i], search->second, false, trials);
            else
            {
                lut[values[i]] = i;
                test(values, values[i], i, false, trials);
            }
        }
        std::cout << " DONE\n";
    };
    bulkTest(25, 100);
    bulkTest(26, 100);
    bulkTest(24, 100);
    bulkTest(50, 100);
    bulkTest(49, 100);
    bulkTest(51, 100);
    bulkTest(75, 100);
    bulkTest(74, 100);
    bulkTest(76, 100);
    bulkTest(25, 99);
    bulkTest(26, 99);
    bulkTest(24, 99);
    bulkTest(50, 99);
    bulkTest(49, 99);
    bulkTest(51, 99);
    bulkTest(75, 99);
    bulkTest(74, 99);
    bulkTest(76, 99);
    test(generate( 2'500,  10'000),  10'099, 2499 , true, trials);
    test(generate(25'000, 100'000), 100'099, 24999, true, trials);
    return 0;
}


