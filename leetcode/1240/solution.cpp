#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int tilingRectangle(int n, int m)
{
    if (n > m) std::swap(n, m);
    std::vector<std::vector<int> > memo(n + 1, std::vector<int>(m + 1, -1));
    auto dpProcess = [&](auto &&self, int a, int b)
    {
        if (a == b) return 1;
        if (a > b) std::swap(a, b);
        if (a == 0) return 0;
        if (memo[a][b] > 0) return memo[a][b];
        int result = self(self, a, b - a) + 1;
        for (int s = a - 1; s > 0; --s)
            for  (int k = a - s; k <= std::min(s, b - s); ++k)
                result = std::min(result, self(self, a - s, b - k)
                                        + self(self, k + s - a, b - s - k)
                                        + self(self, a - k, b - s) + 2);
        return memo[a][b] = result;
    };
    return dpProcess(dpProcess, n, m);
}
#else
int tilingRectangle(int n, int m)
{
    std::vector<int> heights(m);
    constexpr int BASE = 13;
    std::unordered_map<long, int> dp_table;
    auto dpProcess = [&](auto &&self, long hashed_heights) -> int
    {
        if (const auto it = dp_table.find(hashed_heights); it != dp_table.end())
            return it->second;
        
        const auto it = std::min_element(heights.begin(), heights.end());
        const int min_height = *it;
        if (min_height == n) return 0;
        
        int result = m * n;
        const int start = static_cast<int>(it - heights.begin());
        for (int sz = 1; sz <= std::min(m - start, n - min_height); ++sz)
        {
            if (heights[start + sz - 1] != min_height) break;
            for (int i = start; i < start + sz; ++i)
                heights[i] += sz;
            long new_hashed_heights = 0;
            for (int i = static_cast<int>(heights.size()) - 1; i >= 0; --i)
                    new_hashed_heights = new_hashed_heights * BASE + heights[i];
            result = std::min(result, self(self, new_hashed_heights));
            for (int i = start; i < start + sz; ++i)
                heights[i] -= sz;
        }
        return dp_table[hashed_heights] = 1 + result;
    };
    return dpProcess(dpProcess, 0);
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int m, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = tilingRectangle(n, m);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 3, 3, trials);
    test(5, 8, 5, trials);
    test(11, 13, 6, trials);
    return 0;
}


