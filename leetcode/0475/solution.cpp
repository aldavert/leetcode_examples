#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int findRadius(std::vector<int> houses, std::vector<int> heaters)
{
    std::sort(heaters.begin(), heaters.end());
    int result = 0;
    for (int house : houses)
    {
        auto search = std::upper_bound(heaters.begin(), heaters.end(), house);
        int distance = std::numeric_limits<int>::max();
        if (search != heaters.begin())
            distance = std::min(distance, house - *std::prev(search));
        if (search != heaters.end())
            distance = std::min(distance, *search - house);
        result = std::max(result, distance);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> houses,
          std::vector<int> heaters,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findRadius(houses, heaters);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, {2}, 1, trials);
    test({1, 2, 3, 4}, {1, 4}, 1, trials);
    test({1, 5}, {2}, 3, trials);
    test({1, 5, 9, 11, 15, 21}, {2, 10, 16}, 5, trials);
    return 0;
}


