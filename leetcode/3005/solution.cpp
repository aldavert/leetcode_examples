#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxFrequencyElements(std::vector<int> nums)
{
    int histogram[101] = {}, max_frequency = 0;
    for (int value : nums)
    {
        ++histogram[value];
        max_frequency = std::max(max_frequency, histogram[value]);
    }
    int result = 0;
    for (int i = 1; i <= 100; ++i)
        result += histogram[i] * (histogram[i] == max_frequency);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxFrequencyElements(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 3, 1, 4}, 4, trials);
    test({1, 2, 3, 4, 5}, 5, trials);
    return 0;
}


