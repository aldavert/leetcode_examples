#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int numOfSubarrays(std::vector<int> arr)
{
    constexpr int MOD = 1'000'000'007;
    long result = 0, dp[2] = {};
    
    for (int value : arr)
    {
        if (value & 1)
            dp[0] = std::exchange(dp[1], dp[0] + 1);
        else ++dp[0];
        result = (result + dp[1]) % MOD;
    }
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numOfSubarrays(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 5}, 4, trials);
    test({2, 4, 6}, 0, trials);
    test({1, 2, 3, 4, 5, 6, 7}, 16, trials);
    return 0;
}


