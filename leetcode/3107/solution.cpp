#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
long long minOperationsToMakeMedianK(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::sort(nums.begin(), nums.end());
    int ind_med = n / 2;
    if (nums[ind_med] == k) return 0;
    long long result = 0;
    if (nums[ind_med] < k)
        for (int i = ind_med; (i < n) && (nums[i] < k); ++i)
            result += k - nums[i];
    else
        for (int i = ind_med; (i >= 0) && (nums[i] > k); --i)
            result += nums[i] - k;
    return result;
}
#else
long long minOperationsToMakeMedianK(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    long result = 0;
    std::sort(nums.begin(), nums.end());
    for (int i = 0; i <= n / 2; ++i) result += std::max(0, nums[i] - k);
    for (int i = n / 2; i < n; ++i) result += std::max(0, k - nums[i]);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperationsToMakeMedianK(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 5, 6, 8, 5}, 4, 2, trials);
    test({2, 5, 6, 8, 5}, 7, 3, trials);
    test({1, 2, 3, 4, 5, 6}, 4, 0, trials);
    return 0;
}


