#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int earliestSecondToMarkIndices(std::vector<int> nums,
                                std::vector<int> changeIndices)
{
    const int n = static_cast<int>(nums.size());
    const int o = static_cast<int>(changeIndices.size());
    auto canMark = [&](int second) -> bool
    {
        int num_marked = 0, decrement = 0;
        std::vector<int> index_to_last_second(n, -1);
        
        for (int i = 0; i < second; ++i)
            index_to_last_second[changeIndices[i] - 1] = i;
        for (int i = 0; i < second; ++i)
        {
            const int index = changeIndices[i] - 1;
            if (i == index_to_last_second[index])
            {
                if (nums[index] > decrement)
                    return false;
                decrement -= nums[index];
                ++num_marked;
            }
            else ++decrement;
        }
        return num_marked == n;
    };
    int l = 0;
    int r = o + 1;
    while (l < r)
    {
        const int m = (l + r) / 2;
        if (canMark(m)) r = m;
        else l = m + 1;
    }
    return (l <= o)?l:-1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> changeIndices,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = earliestSecondToMarkIndices(nums, changeIndices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 2, 0}, {2, 2, 2, 2, 3, 2, 2, 1}, 8, trials);
    test({1, 3}, {1, 1, 1, 2, 1, 1, 1}, 6, trials);
    test({0, 1}, {2, 2, 2}, -1, trials);
    return 0;
}


