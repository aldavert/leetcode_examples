#include "../common/common.hpp"
#include <cstring>
#include <map>

// ############################################################################
// ############################################################################

#if 0
int findTargetSumWays(std::vector<int> nums, int target)
{
    auto process = [&](void) -> int
    {
        int accum[21] = {};
        for (size_t i = nums.size(); i > 0; --i)
            accum[i - 1] += accum[i] + nums[i - 1];
        std::map<int, int> dp[20];
        auto inner = [&](auto &&self, size_t index, int sum) -> int
        {
            if (index >= nums.size()) return sum == target;
            if (!((sum + accum[index] >= target) && (sum - accum[index] <= target)))
                return 0;
            if (auto search = dp[index].find(sum); search != dp[index].end())
                return search->second;
            int result = 0;
            result += self(self, index + 1, sum + nums[index]);
            result += self(self, index + 1, sum - nums[index]);
            return dp[index][sum] = result;
        };
        return inner(inner, 0, 0);
    };
    return process();
}
#else
int findTargetSumWays(std::vector<int> nums, int target)
{
    auto process = [&](void) -> int
    {
        int accum[21] = {};
        for (size_t i = nums.size(); i > 0; --i)
            accum[i - 1] += accum[i] + nums[i - 1];
        int dp[20][2'001];
        for (size_t i = 0; i < nums.size(); ++i)
            std::memset(dp[i], -1, sizeof(dp[i]));
        auto inner = [&](auto &&self, size_t index, int sum) -> int
        {
            if (index >= nums.size()) return sum == target;
            if (!((sum + accum[index] >= target) && (sum - accum[index] <= target)))
                return 0;
            if (int value = dp[index][1'000 + sum]; value != -1)
                return value;
            int result = 0;
            result += self(self, index + 1, sum + nums[index]);
            result += self(self, index + 1, sum - nums[index]);
            return dp[index][1'000 + sum] = result;
        };
        return inner(inner, 0, 0);
    };
    return process();
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int target,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findTargetSumWays(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 1, 1, 1}, 3, 5, trials);
    test({1}, 1, 1, trials);
    test({1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
         10, 15504, trials);
    test({1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
         -10, 15504, trials);
    test({50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50,
          50, 50, 50, 50, 50, 50}, 500, 15504, trials);
    return 0;
}


