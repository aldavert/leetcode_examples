#include "../common/common.hpp"
#include <queue>
#include <utility>

constexpr int null = -1000;

struct Node
{
    int val = 0;
    Node * left = nullptr;
    Node * right = nullptr;
    Node * next = nullptr;
    Node(void) = default;
    Node(int v) : val(v), left(nullptr), right(nullptr), next(nullptr) {}
    Node(int v, Node * l, Node * r, Node * n) :
        val(v),
        left(l),
        right(r),
        next(n) {}
    ~Node(void) { delete left; delete right; }
};

Node * vec2tree(const std::vector<int> &tree)
{
    std::queue<Node *> active_nodes;
    
    Node * result = nullptr;
    bool left = true;
    for (const auto &v : tree)
    {
        if (v != null)
        {
            if (result == nullptr)
            {
                result = new Node(v);
                active_nodes.push(result);
                active_nodes.push(result);
            }
            else
            {
                Node * current = new Node(v);
                if (left) active_nodes.front()->left = current;
                else active_nodes.front()->right = current;
                active_nodes.pop();
                active_nodes.push(current);
                active_nodes.push(current);
                left = !left;
            }
        }
        else
        {
            active_nodes.pop();
            left = !left;
        }
    }
    return result;
}

std::vector<int> tree2vec(Node * root, bool traverse = true)
{
    if (root == nullptr) return {};
    std::vector<int> result;
    if (traverse)
    {
        for (Node * current = root; current; current = current->left)
        {
            for (Node * ptr = current; ptr; ptr = ptr->next)
                result.push_back(ptr->val);
            result.push_back(null);
        }
    }
    else
    {
        std::queue<Node *> active_nodes;
        active_nodes.push(root);
        result.push_back(root->val);
        while (!active_nodes.empty())
        {
            Node * current = active_nodes.front();
            active_nodes.pop();
            if (current->left != nullptr)
            {
                active_nodes.push(current->left);
                result.push_back(current->left->val);
            }
            else result.push_back(null);
            if (current->right != nullptr)
            {
                active_nodes.push(current->right);
                result.push_back(current->right->val);
            }
            else result.push_back(null);
        }
        while (result.back() == null) result.pop_back();
    }
    return result;
}

// ############################################################################
// ############################################################################

Node * connect(Node * root)
{
    if (!root) return root;
    std::queue<Node *> q;
    q.push(nullptr);
    q.push(root->left);
    q.push(root->right);
    for (Node * previous = nullptr, * current = root; previous || current || q.front(); q.pop())
    {
        previous = std::exchange(current, q.front());
        if (previous)
            previous->next = current;
        if (current)
        {
            if (current->left)
                q.push(current->left);
            if (current->right)
                q.push(current->right);
        }
        else q.push(nullptr);
    }
    return root;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        Node * root = vec2tree(tree);
        root = connect(root);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, null, 7}, {1, null, 2, 3, null, 4, 5, 7, null}, trials);
    test({}, {}, trials);
    return 0;
}


