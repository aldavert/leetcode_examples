#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
long long numberOfPairs(std::vector<int> nums1, std::vector<int> nums2, int diff)
{
    const int n = static_cast<int>(nums1.size());
    int min_pair = std::numeric_limits<int>::max(),
        max_pair = std::numeric_limits<int>::lowest(),
        bit_tree[50'003] = {};
    long result = 0;
    
    for (int i = 0; i < n; i++)
    {
        int difference = nums1[i] - nums2[i];
        min_pair = std::min(min_pair, difference);
        max_pair = std::max(max_pair, difference);
    }
    if (diff <= 0) min_pair += diff;
    else           max_pair += diff;
    int max_size = max_pair - min_pair + 2;
    auto updateBIT = [&](int index, int value) -> void
    {
        for (++index; index <= max_size; index += index & (-index))
            bit_tree[index] += value;
    };
    auto getSum = [&](int index) -> int
    {
        int total = 0;
        for (++index; index > 0; index -= index & (-index))
            total += bit_tree[index];
        return total;
    };
    for (int i = 0; i < n; ++i)
    {
        int curr = nums1[i] - nums2[i] - min_pair;
        result += getSum(curr + diff);
        updateBIT(curr, 1);
    }
    return result;
}
#else
long long numberOfPairs(std::vector<int> nums1, std::vector<int> nums2, int diff)
{
    std::vector<int> work;
    long result = 0;
    auto merge = [&](int l, int m, int r) -> void
    {
        int hi = m + 1;

        for (int i = l; i <= m; ++i)
        {
            while ((hi <= r) && (work[i] > work[hi] + diff))
                ++hi;
            result += r - hi + 1;
        }

        std::vector<int> sorted(r - l + 1);
        int k = 0, i = l, j = m + 1;
        while ((i <= m) && (j <= r))
        {
            if (work[i] < work[j]) sorted[k++] = work[i++];
            else                   sorted[k++] = work[j++];
        }
        while (i <= m) sorted[k++] = work[i++];
        while (j <= r) sorted[k++] = work[j++];
        std::copy(sorted.begin(), sorted.end(), work.begin() + l);
    };
    auto mergeSort = [&](auto &&self, int l, int r) -> void
    {
        if (l >= r) return;
        int m = (l + r) / 2;
        self(self,     l, m);
        self(self, m + 1, r);
        merge(l, m, r);
    };
    
    for (size_t i = 0; i < nums1.size(); ++i)
        work.push_back(nums1[i] - nums2[i]);
    mergeSort(mergeSort, 0, static_cast<int>(work.size()) - 1);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int diff,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfPairs(nums1, nums2, diff);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 5}, {2, 2, 1}, 1, 3, trials);
    test({3, -1}, {-2, 2}, -1, 0, trials);
    return 0;
}


