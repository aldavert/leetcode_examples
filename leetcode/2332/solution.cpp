#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int latestTimeCatchTheBus(std::vector<int> buses,
                          std::vector<int> passengers,
                          int capacity)
{
    const int n = static_cast<int>(buses.size()),
              m = static_cast<int>(passengers.size());
    std::sort(buses.begin(), buses.end());
    std::sort(passengers.begin(), passengers.end());
    
    if (passengers.front() > buses.back()) return buses.back();
    int result = passengers[0] - 1;
    for (int i = 0, j = 0; i < n; ++i)
    {
        int arrived = 0;
        while ((arrived < capacity) && (j < m) && (passengers[j] <= buses[i]))
        {
            if ((j > 0) && (passengers[j] != passengers[j - 1] + 1))
                result = passengers[j] - 1;
            ++j;
            ++arrived;
        }
        if ((arrived < capacity) && (j > 0) && (passengers[j - 1] != buses[i]))
            result = buses[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> buses,
          std::vector<int> passengers,
          int capacity,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = latestTimeCatchTheBus(buses, passengers, capacity);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 20}, {2, 17, 18, 19}, 2, 16, trials);
    test({20, 30, 10}, {19, 13, 26, 4, 25, 11, 21}, 2, 20, trials);
    return 0;
}


