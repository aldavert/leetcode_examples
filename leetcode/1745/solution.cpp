#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkPartitioning(std::string s)
{
    const int n = static_cast<int>(s.size());
    std::vector<std::vector<bool> > dp(n + 1, std::vector<bool>(n + 1));
    
    for (int i = 0; i < n; ++i) dp[i][i] = true;
    for (int d = 1; d < n; ++d)
        for (int i = 0; i + d < n; ++i)
            if (int j = i + d; s[i] == s[j])
                dp[i][j] = (i + 1 > j - 1) || dp[i + 1][j - 1];
    
    for (int i = 0; i < n; ++i)
        for (int j = i + 1; j < n; ++j)
            if (dp[0][i] && dp[i + 1][j] && dp[j + 1][n - 1])
                return true;
    
    return false;
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkPartitioning(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcbdd", true, trials);
    test("bcbddxy", false, trials);
    test("bbab", true, trials);
    return 0;
}


