#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> countBits(int n)
{
    std::vector<int> result(n + 1);
    result[0] = 0;
    for (int i = 1, j = 0, k = 1; i <= n; ++i)
    {
        result[i] = result[j] + 1;
        ++j;
        if (j == k)
        {
            k <<= 1;
            j = 0;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countBits(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(0, {0}, trials);
    test(1, {0, 1}, trials);
    test(2, {0, 1, 1}, trials);
    test(5, {0, 1, 1, 2, 1, 2}, trials);
    test(15, {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4}, trials);
    return 0;
}


