#include "../common/common.hpp"
#include <limits>
#include <bitset>

// ############################################################################
// ############################################################################

int flipgame(std::vector<int> fronts, std::vector<int> backs)
{
    if (fronts.size() != backs.size()) return 0;
    const size_t n = fronts.size();
    std::bitset<2001> valid_numbers;
    valid_numbers.set();
    for (size_t i = 0; i < n; ++i)
        if (fronts[i] == backs[i])
            valid_numbers[fronts[i]] = false;
    int result = std::numeric_limits<int>::max();
    for (size_t i = 0; i < n; ++i)
    {
        if (valid_numbers[fronts[i]])
            result = std::min(result, fronts[i]);
        if (valid_numbers[backs[i]])
            result = std::min(result, backs[i]);
    }
    return (result != std::numeric_limits<int>::max())?result:0;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> fronts,
          std::vector<int> backs,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = flipgame(fronts, backs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 4, 4, 7}, {1, 3, 4, 1, 3}, 2, trials);
    test({1}, {1}, 0, trials);
    return 0;
}


