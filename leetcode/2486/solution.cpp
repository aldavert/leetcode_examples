#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int appendCharacters(std::string s, std::string t)
{
    const int n = static_cast<int>(s.size()),
              m = static_cast<int>(t.size());
    int j = 0;
    for (int i = 0; (i < n) && (j < m); ++i)
        if (t[j] == s[i])
            ++j;
    return m - j;
}
#else
int appendCharacters(std::string s, std::string t)
{
    const int n = static_cast<int>(t.size());
    int i = 0;
    for (const char c : s)
        if (c == t[i])
            if (++i == n)
                return 0;
    return n - i;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, std::string t, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = appendCharacters(s, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("coaching", "coding", 4, trials);
    test("abcde", "a", 0, trials);
    test("z", "abcde", 5, trials);
    return 0;
}


