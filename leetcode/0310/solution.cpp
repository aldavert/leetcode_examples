#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 0
std::vector<int> findMinHeightTrees(int n, std::vector<std::vector<int> > edges)
{
    if (n == 1) return {0};
    if (n == 2) return {0, 1};
    
    std::vector<std::unordered_set<int> > neighbors(n);
    for (auto &e : edges)
    {
        neighbors[e[0]].insert(e[1]);
        neighbors[e[1]].insert(e[0]);
    }
    
    std::vector<int> leafs[2];
    bool current = false;
    for (int i = 0; i < n; ++i)
        if (neighbors[i].size() == 1)
            leafs[current].push_back(i);
    
    while (n > 2)
    {
        n -= static_cast<int>(leafs[current].size());
        leafs[!current].clear();
        for (int leaf_idx : leafs[current])
        {
            int next_idx = *neighbors[leaf_idx].begin();
            neighbors[next_idx].erase(leaf_idx);
            if (neighbors[next_idx].size() == 1)
                leafs[!current].push_back(next_idx);
        }
        current = !current;
    }
    
    return leafs[current];
}
#else
std::vector<int> findMinHeightTrees(int n, std::vector<std::vector<int> > edges)
{
    if (n == 1) return {0};
    if (n == 2) return {0, 1};
    
    std::vector<std::unordered_set<int> > neighbors(n);
    for (auto &e : edges)
    {
        neighbors[e[0]].insert(e[1]);
        neighbors[e[1]].insert(e[0]);
    }
    
    std::vector<int> leafs;
    for (int i = 0; i < n; ++i)
        if (neighbors[i].size() == 1)
            leafs.push_back(i);
    
    while (n > 2)
    {
        n -= static_cast<int>(leafs.size());
        std::vector<int> new_leafs;
        for (int leaf_idx : leafs)
        {
            int next_idx = *neighbors[leaf_idx].begin();
            neighbors[next_idx].erase(leaf_idx);
            if (neighbors[next_idx].size() == 1)
                new_leafs.push_back(next_idx);
        }
        leafs = std::move(new_leafs);
    }
    
    return leafs;
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    std::unordered_set<int> lut;
    for (int l : left)
        lut.insert(l);
    for (int r : right)
    {
        if (auto search = lut.find(r); search == lut.end())
            return false;
        else lut.erase(search);
    }
    return lut.empty();
}

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMinHeightTrees(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{1, 0}, {1, 2}, {1, 3}}, {1}, trials);
    test(6, {{3, 0}, {3, 1}, {3, 2}, {3, 4}, {5, 4}}, {3, 4}, trials);
    test(1, {}, {0}, trials);
    test(2, {{0, 1}}, {1, 0}, trials);
    //|        o 0      |
    //|        |        |
    //|        o 1      |
    //|        |        |
    //|        o 2      |
    //|       / \       |
    //|    3 o   o 4    |
    //|     /     \     |
    //|  5 o       o 6  |
    test(7, {{0, 1}, {1, 2}, {2, 3}, {3, 5}, {2, 4}, {4, 6}}, {2}, trials);
    //|        o 0      |
    //|        |        |
    //|        o 1      |
    //|       / \       |
    //|    2 o   o 3    |
    //|      |   |      |
    //|    4 o   o 5    |
    //|      |          |
    //|    6 o          |
    test(7, {{0, 1}, {1, 2}, {1, 3}, {2, 4}, {3, 5}, {4, 6}}, {1, 2}, trials);
    return 0;
}


