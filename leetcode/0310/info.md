# Minimum Height Trees

A tree is an undirected graph in which any two vertices are connected by exactly one path. In other words, any connected graph without simple cycles is a tree.

Given a tree of `n` nodes labelled from `0` to `n - 1`, and an array of `n - 1` edges where `edges[i] = [ai, bi]` indicates that there is an undirected edge between the two nodes `a_i` and `b_i` in the tree, you can choose any node of the tree as the root. When you select a node `x` as the root, the result tree has height `h`. Among all possible rooted trees, those with minimum height (i.e. `min(h)`)  are called **minimum height trees** (MHTs).

Return *a list of all* ***MHTs'*** *root labels*. You can return the answer in **any order**.

The **height** of a rooted tree is the number of edges on the longest downward path between the root and a leaf.

#### Example 1:
> ```mermaid 
> graph TD;
> A1((0))---B1((1))
> B1---C1((2))
> B1---D1((3))
> A2((1))---B2((0))
> A2---C2((2))
> A2---D2((3))
> A3((2))---B3((1))
> B3---C3((0))
> B3---D3((3))
> A4((3))---B4((1))
> B4---C4((0))
> B4---D4((2))
> classDef top fill:#AAF,stroke:#005,stroke-width:2px;
> classDef middle fill:#CCC,stroke:#333,stroke-width:2px;
> classDef bottom fill:#FAA,stroke:#500,stroke-width:2px;
> class A1,A2,A3,A4 top;
> class B1,B2,C2,D2,B3,B4 middle;
> class C1,D1,C3,D3,C4,D4 bottom;
> ```
> *Input:* `n = 4, edges = [[1, 0], [1, 2], [1, 3]]`  
> *Output:* `[1]`
> *Explanation:* As shown, the height of the tree is `1` when the root is the node with label `1` which is the only MHT.

#### Example 2:
> ```mermaid 
> graph TD;
> A1((3))---B1((0))
> A1---C1((1))
> A1---D1((2))
> A1---E1((4))
> E1---F1((5))
> A2((4))---B2((5))
> A2---C2((3))
> C2---D2((0))
> C2---E2((1))
> C2---F2((2))
> classDef top fill:#AAF,stroke:#005,stroke-width:2px;
> classDef middle fill:#CCC,stroke:#333,stroke-width:2px;
> classDef bottom fill:#FAA,stroke:#500,stroke-width:2px;
> class A1,A2 top;
> class B1,C1,D1,E1,B2,C2 middle;
> class F1,D2,E2,F2 bottom;
> ```
> *Input:* `n = 6, edges = [[3, 0], [3, 1], [3, 2], [3, 4], [5, 4]]`  
> *Output:* `[3, 4]`

#### Example 3:
> *Input:* `n = 1, edges = []`  
> *Output:* `[0]`

#### Example 4:
> *Input:* `n = 2, edges = [[0, 1]]`  
> *Output:* `[0, 1]`
 
#### Constraints:
- `1 <= n <= 2 * 10^4`
- `edges.length == n - 1`
- `0 <= a_i, b_i < n`
- `a_i != b_i`
- All the pairs `(ai, bi)` are distinct.
- The given input is guaranteed to be a tree and there will be no repeated edges.


