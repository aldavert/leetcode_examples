#include "../common/common.hpp"
#include <bit>

// ############################################################################
// ############################################################################

#if 0 // This function and other <bit> library functions are not available on leetcode.
int hammingWeight(uint32_t n)
{
    return std::popcount(n);
}
#else
int hammingWeight(uint32_t n)
{
    int result = 0;
    while (n != 0)
    {
        result += (n & 1);
        n >>= 1;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(uint32_t n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = hammingWeight(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(0x1011, 3, trials);
    test(0x10000000, 1, trials);
    test(std::numeric_limits<uint32_t>::max() - 2, 31, trials);
    return 0;
}


