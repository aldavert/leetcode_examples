#include "../common/common.hpp"
#include <cmath>
#include <limits>

// ############################################################################
// ############################################################################

std::vector<int> constructRectangle(int area)
{
    int L = 0, W = 0;
    int min_diff = std::numeric_limits<int>::max();
    for (int i = 1, e = static_cast<int>(std::sqrt(area)); i <= e; ++i)
    {
        if (area % i == 0)
        {
            int other = area / i;
            if (other == i)
                return {i, i};
            else
            {
                int Lc = std::max(other, i);
                int Wc = std::min(other, i);
                if (Lc - Wc < min_diff)
                {
                    min_diff = Lc - Wc;
                    L = Lc;
                    W = Wc;
                }
            }
        }
    }
    return {L, W};
}

// ############################################################################
// ############################################################################

void test(int area, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = constructRectangle(area);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {2, 2}, trials);
    test(37, {37, 1}, trials);
    test(122122, {427, 286}, trials);
    return 0;
}


