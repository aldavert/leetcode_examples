#include "../common/common.hpp"
#include <unordered_map>
#include <queue>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class Twitter
{
    long m_timestamp = 0;
    std::unordered_map<int, std::vector<std::tuple<int, int> > > m_database;
    std::unordered_map<int, std::unordered_set<int> > m_relations;
public:
    Twitter(void) = default;
    void postTweet(int userId, int tweetId)
    {
        m_database[userId].push_back({m_timestamp++, tweetId});
    }
    std::vector<int> getNewsFeed(int userId)
    {
        std::vector<int> result;
        std::priority_queue<std::tuple<int, int> > q;
        for (auto value : m_database[userId])
            q.push(value);
        for (auto followeeId : m_relations[userId])
            if (followeeId != userId)
                for (auto value : m_database[followeeId])
                    q.push(value);
        for (size_t i = 0; (i < 10) && !q.empty(); ++i)
        {
            auto [timestamp, tweet_id] = q.top();
            q.pop();
            result.push_back(tweet_id);
        }
        return result;
    }
    void follow(int followerId, int followeeId)
    {
        m_relations[followerId].insert(followeeId);
    }
    void unfollow(int followerId, int followeeId)
    {
        m_relations[followerId].erase(followeeId);
    }
};

// ############################################################################
// ############################################################################

enum class ID { POST, GET, FOLLOW, UNFOLLOW };

void test(std::vector<ID> id,
          std::vector<std::tuple<int, int> > input,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    const size_t n = id.size();
    std::vector<std::vector<int> > result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        Twitter twitter;
        for (size_t i = 0; i < n; ++i)
        {
            auto [user_id, second_id] = input[i];
            if (id[i] == ID::POST)
            {
                twitter.postTweet(user_id, second_id);
                result.push_back({});
            }
            else if (id[i] == ID::GET)
            {
                result.push_back(twitter.getNewsFeed(user_id));
            }
            else if (id[i] == ID::FOLLOW)
            {
                twitter.follow(user_id, second_id);
                result.push_back({});
            }
            else if (id[i] == ID::UNFOLLOW)
            {
                twitter.unfollow(user_id, second_id);
                result.push_back({});
            }
            else
            {
                std::cerr << "[ERROR] Unknown test ID.\n";
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({ID::POST, ID::GET, ID::FOLLOW, ID::POST, ID::GET, ID::UNFOLLOW, ID::GET},
         {{1, 5}, {1, null}, {1, 2}, {2, 6}, {1, null}, {1, 2}, {1, null}},
         {{}, {5}, {}, {}, {6, 5}, {}, {5}}, trials);
    test({ID::POST, ID::POST, ID::POST, ID::POST, ID::POST, ID::POST, ID::POST,
          ID::POST, ID::POST, ID::POST, ID::POST, ID::GET},
         {{1, 5}, {1, 3}, {1, 101}, {1, 13}, {1, 10}, {1, 2}, {1, 94},
          {1, 505}, {1, 333}, {1, 22}, {1, 11}, {1, null}}, 
         {{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {},
         {11, 22, 333, 505, 94, 2, 10, 13, 101, 3}}, trials);
    test({ID::POST, ID::POST, ID::POST, ID::POST, ID::POST, ID::POST, ID::POST,
          ID::POST, ID::POST, ID::POST, ID::POST, ID::POST, ID::POST, ID::POST,
          ID::POST, ID::POST, ID::POST, ID::POST, ID::POST, ID::POST, ID::POST,
          ID::POST, ID::GET, ID::FOLLOW, ID::GET, ID::UNFOLLOW, ID::GET},
         {{1, 5}, {2, 3}, {1, 101}, {2, 13}, {2, 10}, {1, 2}, {1, 94}, {2, 505},
          {1, 333}, {2, 22}, {1, 11}, {1, 205}, {2, 203}, {1, 201}, {2, 213},
          {1, 200}, {2, 202}, {1, 204}, {2, 208}, {2, 233}, {1, 222}, {2, 211},
          {1, null}, {1, 2}, {1, null}, {1, 2}, {1, null}}, 
         {{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {},
          {}, {}, {}, {}, {}, {222, 204, 200, 201, 205, 11, 333, 94, 2, 101},
          {}, {211, 222, 233, 208, 204, 202, 200, 213, 201, 203},
          {}, {222, 204, 200, 201, 205, 11, 333, 94, 2, 101}},  trials);
    return 0;
}


