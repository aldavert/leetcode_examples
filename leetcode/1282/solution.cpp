#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > groupThePeople(std::vector<int> groupSizes)
{
    const int n = static_cast<int>(groupSizes.size());
    std::vector<std::vector<int> > histogram(n + 1);
    for (int i = 0; i < n; ++i)
        histogram[groupSizes[i]].push_back(i);
    std::vector<std::vector<int> > result;
    for (int i = 0; i <= n; ++i)
    {
        if (!histogram[i].empty())
        {
            std::vector<int> current;
            for (int person_id : histogram[i])
            {
                current.push_back(person_id);
                if (static_cast<int>(current.size()) == i)
                {
                    result.push_back(current);
                    current.clear();
                }
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> groupSizes,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = groupThePeople(groupSizes);
    bool valid = true;
    const int n = static_cast<int>(groupSizes.size());
    std::vector<bool> present(n);
    int number_of_people = 0;
    for (const auto &group : result)
    {
        for (int person_id : group)
        {
            if ((person_id >= n)
            ||  (present[person_id])
            ||  (groupSizes[person_id] != static_cast<int>(group.size())))
            {
                valid = false;
                break;
            }
            present[person_id] = true;
            ++number_of_people;
        }
        if (!valid) break;
    }
    valid = valid && (number_of_people == n);
    
    showResult(valid, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 3, 3, 3, 3, 1, 3}, {{5}, {0, 1, 2}, {3, 4, 6}}, trials);
    test({2, 1, 3, 3, 3, 2}, {{1}, {0, 5}, {2, 3, 4}}, trials);
    return 0;
}


