#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int minMoves(std::vector<int> nums, int k)
{
    const int n_nums = static_cast<int>(nums.size());
    int temp = 0, mid = k / 2, left = 0, right = k, n = 0;
    for (int i = 0; i < n_nums; ++i)
        if (nums[i] == 1) nums[n++] = i;
    for (int ind = 0; ind < k; ++ind)
        temp += std::abs(nums[mid] - nums[ind]) - abs(mid - ind);
    int result = temp;
    while (right < n)
    {
        temp += (nums[right] - nums[(k & 1)?(mid + 1):mid])
             -  (nums[mid] - nums[left]);
        ++left, ++mid, ++right;
        result = (result > temp)?temp:result;
    }
    return result;
}
#else
int minMoves(std::vector<int> nums, int k)
{
    const int n_nums = static_cast<int>(nums.size());
    std::vector<int> ones;
    
    for (int i = 0; i < n_nums; ++i)
        if (nums[i] == 1)
            ones.push_back(i);
    auto getMedianIndex = [&](int i) { return (i + (i + k - 1)) / 2; };
    auto nThSum = [&](int n) { return n * (n + 1) / 2; };
    const int median = ones[getMedianIndex(0)];
    const int n_ones = static_cast<int>(ones.size());
    int moves = 0;
    for (int i = 0; i < k; ++i) moves += std::abs(ones[i] - median);
    
    int result = moves;
    for (int i = 1; i <= n_ones - k; ++i)
    {
        const int new_median_index = ones[getMedianIndex(i)];
        if (k & 1) moves += new_median_index - ones[getMedianIndex(i - 1)];
        moves += ones[i + k - 1] - 2 * new_median_index + ones[i - 1];
        result = std::min(result, moves);
    }
    return result - nThSum((k - 1) / 2) - nThSum(k / 2);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minMoves(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 0, 1, 0, 1}, 2, 1, trials);
    test({1, 0, 0, 0, 0, 0, 1, 1}, 3, 5, trials);
    test({1, 1, 0, 1}, 2, 0, trials);
    return 0;
}



