#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

#if 1
int countSpecialNumbers(int n)
{
    std::vector<bool> visited(10);
    const std::string s = std::to_string(n);
    const int n_s = static_cast<int>(s.size());
    auto count = [](int m, int k) -> int
    {
        if (k == 0) return 1;
        int result = 1;
        for (int i = 0; i < k; ++i)
            result *= m - i;
        return result;
    };
    int result = 0;
    auto dfs = [&](auto &&self, int i) -> void
    {
        if (i >= n_s)
        {
            ++result;
            return;
        }
        for (int d = 0; d < 10; d++)
        {
            if ((d == 0) && (i == 0)) continue;
            if (visited[d]) continue;
            if (d < (s[i] - '0'))
                result += count(10 - i - 1, n_s - 1 - i);
            else if (d == s[i] - '0')
            {
                visited[d] = true;
                self(self, i + 1);
                visited[d] = false;
            }
        }
    };
    
    for (int len = 1; len <= n_s - 1; ++len)
        result += count(10, len) - count(9, len - 1);        
    dfs(dfs, 0);
    return result;
}
#else
int countSpecialNumbers(int n)
{
    const int digit_size = static_cast<int>(std::log10(n)) + 1;
    std::vector<std::vector<std::vector<int> > > dp(digit_size + 1,
            std::vector<std::vector<int> >(1 << 10, std::vector<int>(2, -1)));
    std::string s = std::to_string(n);
    const int n_s = static_cast<int>(s.size());
    auto count = [&](auto &&self, int i, int used_mask, bool is_tight) -> int
    {
        if (i == n_s) return 1;
        if (dp[i][used_mask][is_tight] != -1)
            return dp[i][used_mask][is_tight];
        int result = 0, max_digit = is_tight?(s[i] - '0'): 9;
        for (int d = 0; d <= max_digit; ++d)
        {
            if (used_mask >> d & 1)
                continue;
            bool next_is_tight = is_tight && (d == max_digit);
            int next_mask = used_mask | (!((used_mask == 0) && (d == 0))) * (1 << d);
            result += self(self, i + 1, next_mask, next_is_tight);
        }
        return dp[i][used_mask][is_tight] = result;
    };
    return count(count, 0, 0, true) - 1;
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSpecialNumbers(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(20, 19, trials);
    test(5, 5, trials);
    test(135, 110, trials);
    return 0;
}


