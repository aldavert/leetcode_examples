#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int brokenCalc(int startValue, int target)
{
    int result = 0;
    while (startValue < target)
    {
        if ((target & 1) == 0)
            target >>= 1;
        else ++target;
        ++result;
    }
    return result + startValue - target;
}

// ############################################################################
// ############################################################################

void test(int startValue, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = brokenCalc(startValue, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 3, 2, trials);
    test(5, 8, 2, trials);
    test(3, 10, 3, trials);
    return 0;
}


