#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

double frogPosition(int n, std::vector<std::vector<int> > edges, int t, int target)
{
    std::vector<std::vector<int> > tree(n + 1);
    std::queue<int> q{{1}};
    std::vector<bool> seen(n + 1);
    std::vector<double> probability(n + 1);
    
    seen[1] = true;
    probability[1] = 1.0;
    for (const auto &edge : edges)
        tree[edge[0]].push_back(edge[1]),
        tree[edge[1]].push_back(edge[0]);
    
    while (!q.empty() && (t-- > 0))
    {
        for (int sz = static_cast<int>(q.size()); sz > 0; --sz)
        {
            int a = q.front();
            q.pop();
            int n_children = 0;
            for (int neighbor : tree[a])
                n_children += !seen[neighbor];
            for (int b : tree[a])
            {
                if (seen[b]) continue;
                seen[b] = true;
                probability[b] = probability[a] / n_children;
                q.push(b);
            }
            if (n_children > 0)
                probability[a] = 0.0;
        }
    }
    
    return probability[target];
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          int t,
          int target,
          double solution,
          unsigned int trials = 1)
{
    double result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = frogPosition(n, edges, t, target);
    showResult(std::abs(solution - result) < 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, {{1, 2}, {1, 3}, {1, 7}, {2, 4}, {2, 6}, {3, 5}}, 2, 4,
         0.16666666666666666, trials);
    test(7, {{1, 2}, {1, 3}, {1, 7}, {2, 4}, {2, 6}, {3, 5}}, 1, 7,
         0.3333333333333333, trials);
    return 0;
}


