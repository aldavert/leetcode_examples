#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countVowelSubstrings(std::string word)
{
    const int n = static_cast<int>(word.size());
    auto v2i = [](char letter) -> int
    {
        if      (letter == 'a') return 0;
        else if (letter == 'e') return 1;
        else if (letter == 'i') return 2;
        else if (letter == 'o') return 3;
        else if (letter == 'u') return 4;
        return -1;
    };
    int result = 0;
    for (int i = 0; i < n; ++i)
    {
        int mask = 0;
        for (int j = i, k = v2i(word[i]); (j < n) && (k >= 0); k = v2i(word[++j]))
        {
            mask = mask | (1 << k);
            result += mask == 31;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countVowelSubstrings(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aeiouu", 2, trials);
    test("unicornarihan", 0, trials);
    test("cuaieuouac", 7, trials);
    return 0;
}


