#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numRescueBoats(std::vector<int> people, int limit)
{
    int result = 0;
    std::sort(people.begin(), people.end());
    int begin = 0, end = static_cast<int>(people.size()) - 1;
    while (begin < end)
    {
        if (people[end] + people[begin] > limit)
        {
            --end;
            ++result;
        }
        else
        {
            --end;
            ++begin;
            ++result;
        }
    }
    return result + (begin == end);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> people, int limit, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numRescueBoats(people, limit);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2}, 3, 1, trials);
    test({3, 2, 2, 1}, 3, 3, trials);
    test({3, 5, 3, 4}, 4, 4, trials);
    return 0;
}


