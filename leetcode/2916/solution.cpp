#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int sumCounts(std::vector<int> nums)
{
    class SegmentTree
    {
    public:
        SegmentTree(int n, int mod) : N(n), MOD(mod),
            m_lazy(4 * n), m_sums(4 * n), m_squared_sums(4 * n) {}
        void updateRange(int l, int r) { return updateRange(0, 0, N - 1, l, r); }
        void propagate(int i, int l, int r)
        {
            const int gap = r - l + 1;
            m_squared_sums[i] += 2 * m_lazy[i] * m_sums[i] + m_lazy[i] * m_lazy[i] * gap;
            m_squared_sums[i] %= MOD;
            m_sums[i] = (m_sums[i] + m_lazy[i] * gap) % MOD;
            if (l < r)
            {
                m_lazy[i * 2 + 1] += m_lazy[i];
                m_lazy[i * 2 + 2] += m_lazy[i];
            }
            m_lazy[i] = 0;
        }
        int getTreeSquaredSums() { return static_cast<int>(m_squared_sums[0]); }
    private:
        const int N;
        const int MOD;
        std::vector<long> m_lazy;
        std::vector<long> m_sums;
        std::vector<long> m_squared_sums;
        
        void updateRange(int i, int start, int end, int l, int r)
        {
            if (m_lazy[i] > 0) propagate(i, start, end);
            if ((end < l) || (start > r)) return;
            if (start >= l && end <= r)
            {
                m_lazy[i] = 1;
                propagate(i, start, end);
                return;
            }
            const int mid = (start + end) / 2;
            updateRange(i * 2 + 1, start, mid, l, r);
            updateRange(i * 2 + 2, mid + 1, end, l, r);
            m_sums[i] = (m_sums[i * 2 + 1] + m_sums[i * 2 + 2]) % MOD;
            m_squared_sums[i] =
                (m_squared_sums[i * 2 + 1] + m_squared_sums[i * 2 + 2]) % MOD;
        }
    };
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(nums.size());
    std::unordered_map<int, int> last_seen;
    SegmentTree tree(n, MOD);
    int result = 0;
    
    for (int r = 0; r < n; ++r)
    {
        auto search = last_seen.find(nums[r]);
        const int l = (search != last_seen.end())?search->second + 1:0;
        tree.updateRange(l, r);
        last_seen[nums[r]] = r;
        result = (result + tree.getTreeSquaredSums()) % MOD;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumCounts(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1}, 15, trials);
    test({2, 2}, 3, trials);
    return 0;
}


