#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumTimeToInitialState(std::string word, int k)
{
    const int n = static_cast<int>(word.size());
    const int max_ops = (n - 1) / k + 1;
    std::vector<int> z_function(n);
    for (int i = 1, l = 0, r = 0; i < n; ++i)
    {
        if (i < r) z_function[i] = std::min(r - i, z_function[i - l]);
        while ((i + z_function[i] < n)
           &&  (word[z_function[i]] == word[i + z_function[i]]))
            ++z_function[i];
        if (i + z_function[i] > r)
        {
            l = i;
            r = i + z_function[i];
        }
    }
    for (int result = 1; result < max_ops; ++result)
        if (z_function[result * k] >= n - result * k)
            return result;
    return max_ops;
}

// ############################################################################
// ############################################################################

void test(std::string word, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumTimeToInitialState(word, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abacaba", 3, 2, trials);
    test("abacaba", 4, 1, trials);
    test("abcbabcd", 2, 4, trials);
    return 0;
}


