#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minimumObstacles(std::vector<std::vector<int> > grid)
{
    const int m_number_of_rows = static_cast<int>(grid.size()),
              m_number_of_columns = static_cast<int>(grid[0].size());
    constexpr int directions[] = {0, 1, 0, -1, 0};
    using Info = std::tuple<int, int, int>;
    std::priority_queue<Info, std::vector<Info>, std::greater<> > heap;
    std::vector<std::vector<int> > distances(m_number_of_rows,
            std::vector<int>(m_number_of_columns, std::numeric_limits<int>::max()));
    
    heap.push({grid[0][0], 0, 0});
    distances[0][0] = grid[0][0];
    while (!heap.empty())
    {
        auto [distance, row, column] = heap.top();
        heap.pop();
        if ((row == m_number_of_rows - 1) && (column == m_number_of_columns - 1))
            return distance;
        for (int k = 0; k < 4; ++k)
        {
            const int x = row + directions[k], y = column + directions[k + 1];
            if ((x < 0) || (x == m_number_of_rows)
            ||  (y < 0) || (y == m_number_of_columns))
                continue;
            int new_distance = distance + grid[row][column];
            if (new_distance < distances[x][y])
            {
                distances[x][y] = new_distance;
                heap.push({new_distance, x, y});
            }
        }
    }
    return distances[m_number_of_rows - 1][m_number_of_columns - 1];
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumObstacles(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 1}, {1, 1, 0}, {1, 1, 0}}, 2, trials);
    test({{0, 1, 0, 0, 0}, {0, 1, 0, 1, 0}, {0, 0, 0, 1, 0}}, 0, trials);
    return 0;
}


