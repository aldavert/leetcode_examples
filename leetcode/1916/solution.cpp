#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int waysToBuildRooms(std::vector<int> prevRoom)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(prevRoom.size());
    std::unordered_map<int, std::vector<int> > graph;
    std::vector<int> mod_factorial(n), mod_inv(n), mod_inv_factorial(n);
    auto mulmod = [&MOD](int l, int r) -> int
    {
        long result = static_cast<long>(l) * static_cast<long>(r);
        return static_cast<int>(result % MOD);
    };
    
    mod_factorial[0] = mod_inv_factorial[0] = 1;
    mod_inv[0] = 0;
    mod_factorial[1] = mod_inv[1] = mod_inv_factorial[1] = 1;
    for (int i = 2; i < n; ++i)
    {
        mod_factorial[i] = mulmod(mod_factorial[i - 1], i);
        mod_inv[i] = mulmod(mod_inv[MOD % i], MOD - MOD / i);
        mod_inv_factorial[i] = mulmod(mod_inv_factorial[i - 1], mod_inv[i]);
    }
    for (int i = 0; i < n; ++i)
        graph[prevRoom[i]].push_back(i);
    auto dfs = [&](auto &&self, int node) -> std::tuple<int, int>
    {
        auto search = graph.find(node);
        if (search == graph.end()) return {1, 1};
        int result = 1, l = 0;
        for (auto child : search->second)
        {
            auto [temp, r] = self(self, child);
            int mod_combination_rep = mulmod(mod_factorial[l + r], mod_inv_factorial[l]);
            mod_combination_rep = mulmod(mod_combination_rep, mod_inv_factorial[r]);
            result = static_cast<int>((((static_cast<long>(result)
                                     *   static_cast<long>(temp)) % MOD)
                                     * mod_combination_rep) % MOD);
            l += r;
        }
        return {result, l + 1};
    };
    return std::get<0>(dfs(dfs, 0));
}

// ############################################################################
// ############################################################################

void test(std::vector<int> prevRoom, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = waysToBuildRooms(prevRoom);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, 0, 1}, 1, trials);
    test({-1, 0, 0, 1, 2}, 6, trials);
    return 0;
}


