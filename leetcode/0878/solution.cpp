#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int nthMagicalNumber(int n, int a, int b)
{
    // a = 2, b = 3
    //      Position: 1 2 3   4   5 6  7    8    9 10
    // Valid numbers: 2 3 4 . 6 . 8 9 10 . 12 . 14 15
    // n = 1 -> 2
    // n = 4 -> 6
    //
    // a = 2, b = 4
    //      Position: 1   2   3   4    5    6    7
    // Valid numbers: 2 . 4 . 6 . 8 . 10 . 12 . 14
    // n = 5 -> 10
    //
    // a = 6, b = 4
    //      Position:     1   2   3        4   
    // Valid numbers: . . 4 . 6 . 8 . . . 12 . .
    // n = 3 -> 8
    
    // Up to 20 with a = 2 and b = 3 you have
    // 2 . 4 . 6 . 8 . 10 .. 12 .. 14 .. 16 .. 18 .. 20
    // . 3 . . 6 . . 9 .. .. 12 .. .. 15 .. .. 18 .. ..
    //         ^              ^                 ^
    //         |              |                 |
    // 'a' gives you 20 // a = 10 numbers and 'b' gives you 20 // b = 6 numbers.
    // However there are 3 numbers which are repeated. These correspond to multiples
    // of the least common multiplier between 2 and 3, i.e. 6. So LCM(2, 3) = 6 gives
    // you the amount of repeated by doing 20 // LCM(2, 3) = 3. Therefore, at 20 there
    // are only 13 unique magical numbers.
    // 
    // For 10 -> 10 // 2 + 10 // 3 - 10 // LCM(2, 3) = 5 + 3 - 1 = 7
    // For 15 -> 15 // 2 + 15 // 3 - 15 // LCM(2, 3) = 7 + 5 - 2 = 10
    // 
    // So the number can be found using a binary search between min(2, 3) and 
    // min(2, 3) * n and using the equation to compute the number of magic numbers
    // at each point.
    
    constexpr long mod = 1'000'000'007;
    long left = std::min(a, b);
    long right = left * static_cast<long>(n);
    long lcm = std::lcm(a, b);
    while (left < right)
    {
        long mid = (left + right) / 2;
        long count = mid / a + mid / b - mid / lcm;
        if (count < n)
            left = mid + 1;
        else right = mid;
    }
    return static_cast<int>(left % mod);
}

// ############################################################################
// ############################################################################

void test(int n, int a, int b, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = nthMagicalNumber(n, a, b);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 2, 3, 2, trials);
    test(4, 2, 3, 6, trials);
    test(5, 2, 4, 10, trials);
    test(3, 6, 4, 8, trials);
    test(1000000000, 40000, 40000, 999720007, trials);
    return 0;
}


