#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minReorder(int n, std::vector<std::vector<int> > connections)
{
    std::vector<std::vector<int> > from_to(n), to_from(n);
    std::vector<bool> visit(n);
    for (const auto &connection : connections)
    {
        from_to[connection[0]].push_back(connection[1]);
        to_from[connection[1]].push_back(connection[0]);
    }
    std::queue<int> active;
    active.push(0);
    int changes = 0;
    while (!active.empty())
    {
        for (size_t i = 0, m = active.size(); i < m; ++i)
        {
            int node = active.front();
            active.pop();
            visit[node] = true;
            for (int next : from_to[node])
            {
                if (visit[next]) continue;
                ++changes;
                active.push(next);
            }
            for (int next : to_from[node])
            {
                if (visit[next]) continue;
                active.push(next);
            }
        }
    }
    return changes;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > connections,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minReorder(n, connections);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, {{0, 1}, {1, 3}, {2, 3}, {4, 0}, {4, 5}}, 3, trials);
    test(5, {{1, 0}, {1, 2}, {3, 2}, {3, 4}}, 2, trials);
    test(3, {{1, 0}, {2, 0}}, 0, trials);
    return 0;
}


