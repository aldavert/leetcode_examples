# Reorder Routes to Make All Paths Lead to the City Zero

There are `n` cities numbered from `0` to `n - 1` and `n - 1` roads such that there is only one way to travel between two different cities (this network form a tree). Last year, The ministry of transport decided to orient the roads in one direction because they are too narrow.

Roads are represented by connections where `connections[i] = [a_i, b_i]` represents a road from city `a_i` to city `b_i`.

This year, there will be a big event in the capital (city `0`), and many people want to travel to this city.

Your task consists of reorienting some roads such that each city can visit the city `0`. Return the **minimum** number of edges changed.

It's **guaranteed** that each city can reach city `0` after reorder.

#### Example 1:
> ```mermaid
> graph LR;
> A((0))--->B((1))
> B-->C((3))
> D((2))-->C
> E((4))-->A
> E-->F((5))
> B==>A
> C==>B
> F==>E
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> linkStyle 5 stroke:red
> linkStyle 6 stroke:red
> linkStyle 7 stroke:red
> ```
> *Input:* `n = 6, connections = [[0, 1], [1, 3], [2, 3], [4, 0], [4, 5]]`  
> *Output:* `3`  
> *Explanation:* Change the direction of edges show in red such that each node can reach the node `0` (capital).

#### Example 2:
> ```mermaid
> graph LR;
> B((1))-->A((0))
> B-->C((2))
> C==>B
> D((3))-->C
> D-->E((4))
> E==>D
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> linkStyle 2 stroke:red
> linkStyle 5 stroke:red
> ```
> *Input:* `n = 5, connections = [[1, 0], [1, 2], [3, 2], [3, 4]]`  
> *Output:* `2`  
> *Explanation:* Change the direction of edges show in red such that each node can reach the node `0` (capital).

#### Example 3:
> *Input:* `n = 3, connections = [[1, 0], [2, 0]]`  
> *Output:* `0`

#### Constraints:
- `2 <= n <= 5 * 10^4`
- `connections.length == n - 1`
- `connections[i].length == 2`
- `0 <= a_i, b_i <= n - 1`
- `a_i != b_i`


