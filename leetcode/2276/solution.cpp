#include "../common/common.hpp"
#include <map>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class CountIntervals
{
public:
    CountIntervals()
    {
    }
    void add(int left, int right)
    {
        while (isOverlapped(left, right))
        {
            auto it = std::prev(m_intervals.upper_bound(right));
            int l = it->first, r = it->second;
            left  = std::min(left,  l);
            right = std::max(right, r);
            m_intervals.erase(l);
            m_count -= r - l + 1;
        }
        m_intervals[left] = right;
        m_count += right - left + 1;
    }
    int count()
    {
        return m_count;
    }
private:
    std::map<int, int> m_intervals;
    int m_count = 0;
    
    bool isOverlapped(int left, int right)
    {
        auto it = m_intervals.upper_bound(right);
        return (it != m_intervals.begin()) && (prev(it)->second >= left);
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<bool> add,
          std::vector<std::pair<int, int> > parameters,
          std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        CountIntervals obj;
        result.clear();
        for (size_t i = 0; i < add.size(); ++i)
        {
            if (add[i])
            {
                obj.add(parameters[i].first, parameters[i].second);
                result.push_back(null);
            }
            else result.push_back(obj.count());
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({true, true, false, true, false},
         {{2, 3}, {7, 10}, {}, {5, 8}, {}},
         {null, null, 6, null, 8}, trials);
    return 0;
}


