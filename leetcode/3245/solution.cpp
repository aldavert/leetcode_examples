#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> numberOfAlternatingGroups(std::vector<int> colors,
                                           std::vector<std::vector<int> > queries)
{
    class FenwickTree
    {
        std::vector<std::array<int, 2> > tree;
    public:
        FenwickTree(int n) : tree(n + 1) {}
        void update(int size, int op)
        {
            const int n = static_cast<int>(tree.size());
            for (int i = n - size; i < n; i += i & -i)
            {
                tree[i][0] += op;
                tree[i][1] += op * size;
            }
        }
        int query(int size)
        {
            const int n = static_cast<int>(tree.size());
            int cnt = 0, sum = 0;
            for (int i = n - size; i > 0; i &= i - 1)
            {
                cnt += tree[i][0];
                sum += tree[i][1];
            }
            return sum - cnt * (size - 1);
        }
    };
    const int n = static_cast<int>(colors.size());
    std::set<int> s;
    FenwickTree t(n);
    auto update = [&](int i, int op)
    {
        auto it = s.lower_bound(i);
        int pre = (it == s.begin())?*s.rbegin():*prev(it);
        int nxt = (it == s.end())?*s.begin():*it;
        t.update((nxt - pre + n - 1) % n + 1, -op); 
        t.update((i - pre + n) % n, op);
        t.update((nxt - i + n) % n, op); 
    };
    auto add = [&](int i)
    {
        if (s.empty()) t.update(n, 1);
        else update(i, 1);
        s.insert(i);
    };
    auto del = [&](int i)
    {
        s.erase(i);
        if (s.empty()) t.update(n, -1);
        else update(i, -1);
    };
    
    for (int i = 0; i < n; ++i)
        if (colors[i] == colors[(i + 1) % n])
            add(i); 
    std::vector<int> result;
    for (auto& q : queries)
    {
        if (q[0] == 1)
        {
            if (s.empty()) result.push_back(n); 
            else result.push_back(t.query(q[1]));
        }
        else
        {
            int i = q[1];
            if (colors[i] == q[2]) continue;
            int pre = (i - 1 + n) % n, nxt = (i + 1) % n;
            if (colors[pre] == colors[i]) del(pre);
            if (colors[i] == colors[nxt]) del(i);
            colors[i] ^= 1;
            if (colors[pre] == colors[i]) add(pre);
            if (colors[i] == colors[nxt]) add(i);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> colors,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfAlternatingGroups(colors, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 1, 0, 1}, {{2, 1, 0}, {1, 4}}, {2}, trials);
    test({0, 0, 1, 0, 1, 1}, {{1, 3}, {2, 3, 0}, {1, 5}}, {2, 0}, trials);
    return 0;
}


