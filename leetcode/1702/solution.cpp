#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string maximumBinaryString(std::string binary)
{
    const size_t zeros = std::count(binary.begin(), binary.end(), '0');
    const size_t prefix_ones = binary.find('0');
    
    binary.assign(binary.length(), '1');
    if (prefix_ones != std::string::npos)
        binary[prefix_ones + zeros - 1] = '0';
    return binary;
}

// ############################################################################
// ############################################################################

void test(std::string binary, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumBinaryString(binary);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("000110", "111011", trials);
    test("01", "01", trials);
    return 0;
}


