#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxUncrossedLines(std::vector<int> nums1, std::vector<int> nums2)
{
    const int n1 = static_cast<int>(nums1.size()),
              n2 = static_cast<int>(nums2.size());
    int dp[501][501] = {};
    for (int i = 1; i <= n1; ++i)
        for (int j = 1; j <= n2; ++j)
        dp[i][j] = (nums1[i - 1] == nums2[j - 1])
                 ? dp[i - 1][j - 1] + 1
                 : std::max(dp[i - 1][j], dp[i][j - 1]);
    return dp[n1][n2];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxUncrossedLines(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 2}, {1, 2, 4}, 2, trials);
    test({2, 5, 1, 2, 5}, {10, 5, 2, 1, 5, 2}, 3, trials);
    test({1, 3, 7, 1, 7, 5}, {1, 9, 2, 5, 1}, 2, trials);
    return 0;
}


