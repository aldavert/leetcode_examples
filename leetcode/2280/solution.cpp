#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int minimumLines(std::vector<std::vector<int> > stockPrices)
{
    auto calcSlope = [](const std::vector<int> &p,
                        const std::vector<int> &q) -> std::pair<int, int>
    {
        int dx = p[0] - q[0], dy = p[1] - q[1];
        if (dx == 0) return {0, p[0]};
        if (dy == 0) return {p[1], 0};
        int d = std::gcd(dx, dy);
        return {dx / d, dy / d};
    };
    int result = 0;
    
    std::sort(stockPrices.begin(), stockPrices.end());
    for (int i = 2, n = static_cast<int>(stockPrices.size()); i < n; ++i)
    {
        auto a = calcSlope(stockPrices[i - 2], stockPrices[i - 1]),
             b = calcSlope(stockPrices[i - 1], stockPrices[i]);
        if (a != b)
            ++result;
    }
    return result + (stockPrices.size() > 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > stockPrices,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumLines(stockPrices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 7}, {2, 6}, {3, 5}, {4, 4}, {5, 4}, {6, 3}, {7, 2}, {8, 1}}, 3, trials);
    test({{3, 4}, {1, 2}, {7, 8}, {2, 3}}, 1, trials);
    return 0;
}


