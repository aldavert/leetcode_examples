#include "../common/common.hpp"

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class MapSum
{
private:
    static constexpr int SIZE = 26;
    struct Node
    {
        struct Node * children[SIZE] = {nullptr};
        int sum = 0;
        int word = 0;
        ~Node(void)
        {
            for (int i = 0; i < SIZE; ++i) delete children[i];
        }
    };
    Node m_root;
public:
    MapSum(void) {}
    void insert(std::string key, int val)
    {
        Node * current = &m_root;
        const int n = static_cast<int>(key.size());
        for (int i = 0; i < n; ++i)
        {
            const int chr = key[i] - 'a';
            if (!current->children[chr])
                current->children[chr] = new Node();
            current = current->children[chr];
            current->sum += val;
        }
        if (current->word != 0)
        {
            const int previous = current->word;
            current = &m_root;
            for (int i = 0; i < n; ++i)
            {
                const int chr = key[i] - 'a';
                current = current->children[chr];
                current->sum -= previous;
            }
        }
        current->word = val;
    }
    int sum(std::string prefix)
    {
        const int n = static_cast<int>(prefix.size());
        const Node * current = &m_root;
        for (int i = 0; (current != nullptr) && (i < n); ++i)
            current = current->children[prefix[i] - 'a'];
        return (current != nullptr)?current->sum:0;
    }
};

// ############################################################################
// ############################################################################

enum class OP { CREATE, INSERT, SUM };
void test(std::vector<OP> operations,
          std::vector<std::string> keys,
          std::vector<int> val,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    const int n = static_cast<int>(operations.size());
    std::vector<int> result(n, null);
    for (unsigned int t = 0; t < trials; ++t)
    {
        MapSum * sum = nullptr;
        for (int o = 0; o < n; ++o)
        {
            switch (operations[o])
            {
            case OP::CREATE:
                sum = new MapSum();
                break;
            case OP::INSERT:
                sum->insert(keys[o], val[o]);
                break;
            case OP::SUM:
                result[o] = sum->sum(keys[o]);
                break;
            default:
                throw std::out_of_range("Operation unknown");
            };
        }
        delete sum;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::CREATE, OP::INSERT, OP::SUM, OP::INSERT, OP::SUM},
         {""        , "apple"   , "ap"   , "app"     , "ap"   },
         {null      , 3         , null   , 2         , null   },
         {null      , null      , 3      , null      , 5      }, trials);
    test({OP::CREATE, OP::INSERT, OP::SUM, OP::INSERT, OP::SUM},
         {""        , "apple"   , "apple", "app"     , "ap"   },
         {null      , 3         , null   , 2         , null   },
         {null      , null      , 3      , null      , 5      }, trials);
    test({OP::CREATE, OP::INSERT, OP::SUM, OP::INSERT, OP::SUM},
         {""        , "a"       , "ap"   , "b"       , "a"    },
         {null      , 3         , null   , 2         , null   },
         {null      , null      , 0      , null      , 3      }, trials);
    test({OP::CREATE, OP::INSERT, OP::SUM, OP::INSERT, OP::INSERT, OP::SUM},
         {""        , "apple"   , "ap"   , "app"     , "apple"   , "ap"   },
         {null      , 3         , null   , 2         , 2         , null   },
         {null      , null      , 3      , null      , null      , 4      }, trials);
    return 0;
}


