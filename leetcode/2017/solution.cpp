#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

long long gridGame(std::vector<std::vector<int> > grid)
{
    const int n = static_cast<int>(grid[0].size());
    long result = std::numeric_limits<long>::max(),
         sum_row0 = std::accumulate(grid[0].begin(), grid[0].end(), 0L),
         sum_row1 = 0;
    for (int i = 0; i < n; ++i)
    {
        sum_row0 -= grid[0][i];
        result = std::min(result, std::max(sum_row0, sum_row1));
        sum_row1 += grid[1][i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = gridGame(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 5, 4}, {1, 5, 1}}, 4, trials);
    test({{3, 3, 1}, {8, 5, 2}}, 4, trials);
    test({{1, 3, 1, 15}, {1, 3, 3, 1}}, 7, trials);
    return 0;
}


