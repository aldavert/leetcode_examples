#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countKSubsequencesWithMaxBeauty(std::string s, int k)
{
    constexpr int MOD = 1'000'000'007;
    int f[26] = {};
    int count = 0;
    for (char& c : s)
        if (++f[c - 'a'] == 1)
            ++count;
    if (count < k) return 0;
    std::vector<int> vs(count);
    for (int i = 0, j = 0; i < 26; ++i)
        if (f[i])
            vs[j++] = f[i];
    std::sort(vs.rbegin(), vs.rend());
    long long result = 1;
    int val = vs[k - 1], x = 0;
    for (int v : vs)
        x += v == val;
    for (int v : vs)
    {
        if (v == val)
            break;
        --k;
        result = result * v % MOD;
    }
    std::vector<std::vector<int> > c(x + 1, std::vector<int>(x + 1));
    for (int i = 0; i <= x; ++i)
    {
        c[i][0] = 1;
        for (int j = 1; j <= i; ++j)
            c[i][j] = (c[i - 1][j] + c[i - 1][j - 1]) % MOD;
    }
    auto qpow = [&](long long a, int n) -> long long
    {
        long long r = 1;
        for (; n; n >>= 1)
        {
            if (n & 1)
                r = r * a % MOD;
            a = a * a % MOD;
        }
        return r;
    };
    result = (result * c[x][k] % MOD) * qpow(val, k) % MOD;
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countKSubsequencesWithMaxBeauty(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("bcca", 2, 4, trials);
    test("abbcd", 4, 2, trials);
    return 0;
}


