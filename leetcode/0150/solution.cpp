#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 1
int evalRPN(std::vector<std::string> tokens)
{
    std::stack<long> values;
    for (const auto &t : tokens)
    {
        if (t.size() == 1 && ((t[0] == '+') || (t[0] == '-')
                          ||  (t[0] == '*') || (t[0] == '/')))
        {
            long vr = values.top();
            values.pop();
            long vl = values.top();
            values.pop();
            switch (t[0])
            {
            case '+':
                vl += vr;
                break;
            case '-':
                vl -= vr;
                break;
            case '*':
                vl *= vr;
                break;
            case '/':
                vl /= vr;
                break;
            default:
                break;
            }
            values.push(vl);
        }
        else values.push(std::stol(t));
    }
    return static_cast<int>(values.top());
}
#else
int evalRPN(std::vector<std::string>& tokens)
{
    std::stack<long> values;
    for (const auto &t : tokens)
    {
        if      (t == "+")
        {
            long r = values.top();
            values.pop();
            r = values.top() + r;
            values.pop();
            values.push(r);
        }
        else if (t == "-")
        {
            long r = values.top();
            values.pop();
            r = values.top() - r;
            values.pop();
            values.push(r);
        }
        else if (t == "*")
        {
            long r = values.top();
            values.pop();
            r = values.top() * r;
            values.pop();
            values.push(r);
        }
        else if (t == "/")
        {
            long r = values.top();
            values.pop();
            r = values.top() / r;
            values.pop();
            values.push(r);
        }
        else values.push(std::stoi(t));
    }
    return static_cast<int>(values.top());
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> tokens, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = evalRPN(tokens);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"2", "1", "+", "3", "*"}, 9, trials);
    test({"4", "13", "5", "/", "+"}, 6, trials);
    test({"10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"},
         22, trials);
    test({"-128","-128","*","-128","*","-128","*","8","*","-1","*"}, -2147483648,
         trials);
    return 0;
}


