#include "../common/common.hpp"
#include "../common/list.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * sortedListToBST(ListNode * head)
{
    int number_of_nodes = 0;
    ListNode * current = head;
    while (current)
        current = current->next,
        ++number_of_nodes;
    current = head;
    auto build = [&](auto &&self, int n) -> TreeNode *
    {
        if (n <= 0) return nullptr;
        TreeNode * left = self(self, n / 2);
        TreeNode * node = new TreeNode(current->val, left, nullptr);
        current = current->next;
        node->right = self(self, n - n / 2 - 1);
        return node;
    };
    return build(build, number_of_nodes);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> list_values,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(list_values);
        TreeNode * root = sortedListToBST(head);
        result = tree2vec(root);
        delete root;
        delete head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-10, -3, 0, 5, 9}, {0, -3, 9, -10, null, 5}, trials);
    test({}, {}, trials);
    return 0;
}


