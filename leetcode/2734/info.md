# Lexicographically Smallest String After Substring Operation

Given a string `s` consisting of lowercase English letters. Perform the following operation:
- Select any non-empty substring then replace every letter of the substring with the preceding letter of the English alphabet. For example, `'b'` is converted to `'a'`, and `'a'` is converted to `'z'`.

Return the **lexicographically smallest** string **after performing the operation.**

**Note**
- A **substring** is a contiguous **non-empty** sequence of characters within a string.
- A string `a` is **lexicographically smaller** than a string `b` if in the first position where `a` and `b` differ, string `a` has a letter that appears earlier in the alphabet than the corresponding letter in `b`. If the first `min(a.length, b.length)` characters do not differ, then the shorter string is the lexicographically smaller one.

#### Example 1:
> *Input:* `s = "cbabc"`  
> *Output:* `"baabc"`  
> *Explanation:* Perform the operation on the substring starting at index `0`, and ending at index `1` inclusive.

#### Example 2:
> *Input:* `s = "aa"`  
> *Output:* `"az"`  
> *Explanation:* Perform the operation on the last letter.

#### Example 3:
> *Input:* `s = "acbbc"`  
> *Output:* `"abaab"`  
> *Explanation:* Perform the operation on the substring starting at index `1`, and ending at index `4` inclusive.

#### Example 4:
> *Input:* `s = "leetcode"`  
> *Output:* `"kddsbncd"`  
> *Explanation:* Perform the operation on the entire string.

#### Constraints:
- `1 <= s.length <= 3 * 10^5`
- `s` consists of lowercase English letters


