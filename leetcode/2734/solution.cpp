#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string smallestString(std::string s)
{
    const int n = static_cast<int>(s.size());
    int i = 0;
    while ((i < n) && (s[i] == 'a')) ++i;
    if (i == n)
    {
        s[n - 1] = 'z';
        return s;
    }
    while ((i < n) && (s[i] != 'a')) --s[i++];
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("cbabc", "baabc", trials);
    test("aa", "az", trials);
    test("acbbc", "abaab", trials);
    test("leetcode", "kddsbncd", trials);
    return 0;
}


