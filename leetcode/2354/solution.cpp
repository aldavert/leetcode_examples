#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long countExcellentPairs(std::vector<int> nums, int k)
{
    constexpr int MAXBIT = 30;
    long count[MAXBIT] = {}, result = 0;
    
    for (int number : std::unordered_set<int>(nums.begin(), nums.end()))
        ++count[__builtin_popcount(number)];
    for (int i = 0; i < MAXBIT; ++i)
        for (int j = 0; j < MAXBIT; ++j)
            if (i + j >= k)
                result += count[i] * count[j];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countExcellentPairs(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 1}, 3, 5, trials);
    test({5, 1, 1}, 10, 0, trials);
    return 0;
}


