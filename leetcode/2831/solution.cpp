#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int longestEqualSubarray(std::vector<int> nums, int k)
{
    std::unordered_map<int, int> count;
    int result = 0;
    for (int l = 0, r = 0, n = static_cast<int>(nums.size()); r < n; ++r)
    {
        result = std::max(result, ++count[nums[r]]);
        while (r - l + 1 - k > result)
            --count[nums[l++]];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestEqualSubarray(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 3, 1, 3}, 3, 3, trials);
    test({1, 1, 2, 2, 1, 1}, 2, 4, trials);
    return 0;
}


