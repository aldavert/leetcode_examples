# Number of Provinces

There are `n` cities. Some of them are connected, while some are not. If city `a` is connected directly with city `b`, and city `b` is connected directly with city `c`, then city `a` is connected indirectly with city `c`.

A **province** is a group of directly or indirectly connected cities and no other cities outside of the group.

You are given an `n * n` matrix `isConnected` where `isConnected[i][j] = 1` if the `i^{th}` city and the `j^{th}` city are directly connected, and `isConnected[i][j] = 0` otherwise.

Return *the total number of* ***provinces.***

#### Example 1:
> ```mermaid
> graph LR;
> A((1))---B((2))
> C((3))
> classDef blue fill:#AAF,stroke:55F,stroke-width:2px;
> classDef white fill:#FFF,stroke:000,stroke-width:2px;
> class A,B blue
> class C white
> ```
> *Input:* `isConnected = [[1, 1, 0], [1, 1, 0], [0, 0, 1]]`  
> *Output:* `2`

#### Example 2:
> ```mermaid
> graph LR;
> A((1))---B((2))
> C((3))
> classDef blue fill:#AAF,stroke:55F,stroke-width:2px;
> classDef red fill:#FAA,stroke:F55,stroke-width:2px;
> classDef magenta fill:#FAF,stroke:F5F,stroke-width:2px;
> class A red
> class B blue
> class C magenta
> linkStyle 0 stroke-width:0px
> ```
> *Input:* `isConnected = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]`  
> *Output:* `3`

#### Constraints:
- `1 <= n <= 200`
- `n == isConnected.length`
- `n == isConnected[i].length`
- `isConnected[i][j]` is `1` or `0`.
- `isConnected[i][i] == 1`
- `isConnected[i][j] == isConnected[j][i]`


