#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findCircleNum(std::vector<std::vector<int> > isConnected)
{
    const int n = static_cast<int>(isConnected.size());
    std::vector<int> id(n), rank(n);
    int count = n;
    for (int i = 0 ; i < n; ++i) id[i] = i;
    auto find = [&](auto &&self, int u) -> int
    {
        return (id[u] == u)?u:id[u] = self(self, id[u]);
    };
    auto merge = [&](int u, int v) -> void
    {
        int i = find(find, u), j = find(find, v);
        if (i == j) return;
        if      (rank[i] < rank[j]) id[i] = id[j];
        else if (rank[i] > rank[j]) id[j] = id[i];
        else                        id[i] = id[j], ++rank[j];
        --count;
    };
    for (int i = 0; i < n; ++i)
        for (int j = i; j < n; ++j)
            if (isConnected[i][j]) merge(i, j);
    return count;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > isConnected,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findCircleNum(isConnected);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 0}, {1, 1, 0}, {0, 0, 1}}, 2, trials);
    test({{1, 0, 0}, {0, 1, 0}, {0, 0, 1}}, 3, trials);
    return 0;
}


