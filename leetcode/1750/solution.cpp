#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumLength(std::string s)
{
    const int n = static_cast<int>(s.size());
    int left = 0, right = n - 1;
    while ((left < right) && (s[left] == s[right]))
    {
        char chr = s[left];
        for (; (left < n) && (s[left] == chr); ++left);
        for (; (right >= 0) && (s[right] == chr); --right);
    }
    return std::max(right - left + 1, 0);
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumLength(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ca", 2, trials);
    test("cabaabac", 0, trials);
    test("aabccabba", 3, trials);
    test("bbbbbbbbbbbbbbbbbbb", 0, trials);
    return 0;
}


