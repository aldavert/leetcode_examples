#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maxStrength(std::vector<int> nums)
{
    long product_positive = 1, product_negative = 1;
    int maximum_negative = std::numeric_limits<int>::lowest(), count_negative = 0;
    bool has_positive = false, has_zero = false;
    
    for (int num : nums)
    {
        if (num > 0)
        {
            product_positive *= num;
            has_positive = true;
        }
        else if (num < 0)
        {
            product_negative *= num;
            maximum_negative = std::max(maximum_negative, num);
            ++count_negative;
        }
        else has_zero = true;
    }
    if ((count_negative == 0) && !has_positive) return 0;
    if (count_negative % 2 == 0) return product_negative * product_positive;
    if (count_negative >= 3)
        return product_negative / maximum_negative * product_positive;
    if (has_positive) return product_positive;
    if (has_zero) return 0;
    return maximum_negative;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxStrength(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, -1, -5, 2, 5, -9}, 1350, trials);
    test({-4, -5, -4}, 20, trials);
    return 0;
}


