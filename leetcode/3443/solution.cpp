#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxDistance(std::string s, int k)
{
    int result = 0;
    for (int i = 0, x = 0, y = 0, n = static_cast<int>(s.size()); i < n; ++i)
    {
        if      (s[i] == 'E') ++x;
        else if (s[i] == 'W') --x;
        else if (s[i] == 'N') ++y;
        else if (s[i] == 'S') --y;
        result = std::max(result, std::min(std::abs(x) + std::abs(y) + 2 * k, i + 1));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxDistance(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("NWSE", 1, 3, trials);
    test("NSWWEW", 3, 6, trials);
    return 0;
}


