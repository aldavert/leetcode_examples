#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfUniqueGoodSubsequences(std::string binary)
{
    constexpr int MOD = 1'000'000'007;
    int ends_with[2] = {};
    bool has_zero = false;
    
    for (char c : binary)
    {
        ends_with[c - '0'] = (ends_with[0] + ends_with[1]) % MOD;
        ends_with[1] += (c == '1');
        has_zero = has_zero || (c == '0');
    }
    
    return (ends_with[0] + ends_with[1] + has_zero) % MOD;
}

// ############################################################################
// ############################################################################

void test(std::string binary, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfUniqueGoodSubsequences(binary);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("001", 2, trials);
    test("11", 2, trials);
    test("101", 5, trials);
    return 0;
}


