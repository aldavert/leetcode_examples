#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findRedundantDirectedConnection(std::vector<std::vector<int> > edges)
{
    const int n = static_cast<int>(edges.size());
    std::vector<int> histogram(n + 1), node_id(n + 1), node_rank(n + 1);
    int node_with_two_parents = 0;
    auto find = [&](auto &&self, int u) -> int
    {
        return (node_id[u] == u)?u:node_id[u] = self(self, node_id[u]);
    };
    auto search = [&](int skip) -> std::vector<int>
    {
        for (int i = 0; i <= n; ++i)
            node_id[i] = i,
            node_rank[i] = 0;
        for (int i = 0; i < n; ++i)
        {
            if (i == skip) continue;
            const int u = find(find, edges[i][0]);
            const int v = find(find, edges[i][1]);
            if (u == v) return edges[i];
            else
            {
                if      (node_rank[u] < node_rank[v]) node_id[u] = node_id[v];
                else if (node_rank[u] > node_rank[v]) node_id[v] = node_id[u];
                else node_id[u] = node_id[v], ++node_rank[v];
            }
        }
        return {};
    };
    for (const auto &edge : edges)
    {
        if (const int v = edge[1]; ++histogram[v] == 2)
        {
            node_with_two_parents = v;
            break;
        }
    }
    if (!node_with_two_parents) return search(-1);
    for (int i = n - 1; i >= 0; --i)
    {
        if (edges[i][1] == node_with_two_parents)
        {
            if (search(i).empty()) return edges[i];
        }
    }
    return {};
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findRedundantDirectedConnection(edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {1, 3}, {2, 3}}, {2, 3}, trials);
    test({{1, 2}, {2, 3}, {3, 4}, {4, 1}, {1, 5}}, {4, 1}, trials);
    test({{2, 1}, {3, 1}, {4, 2}, {1, 4}}, {2, 1}, trials);
    //test({}, {}, trials);
    return 0;
}


