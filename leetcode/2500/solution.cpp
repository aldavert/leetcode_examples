#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int deleteGreatestValue(std::vector<std::vector<int> > grid)
{
    const size_t n = grid.size(), m = grid[0].size();
    auto copy = grid;
    for (size_t i = 0; i < n; ++i)
        std::sort(copy[i].begin(), copy[i].end());
    int result = 0;
    for (size_t j = 0; j < m; ++j)
    {
        int current = 0;
        for (size_t i = 0; i < n; ++i)
            current = std::max(current, copy[i][j]);
        result += current;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = deleteGreatestValue(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 4}, {3, 3, 1}}, 8, trials);
    test({{10}}, 10, trials);
    return 0;
}


