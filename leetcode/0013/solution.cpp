#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int romanToInt(std::string s)
{
    int lut[26] = {0, 0, 100, 500, 0, 0, 0, 0, 1, // ABCDEFGHI
                   0, 0, 50, 1000, 0, 0, 0, 0, 0, // JKLMNOPQR
                   0, 0, 0, 5, 0, 10, 0, 0};      // STUVWXYZ
    int previous_value = std::numeric_limits<int>::max();
    int result = 0;
    for (char c : s)
    {
        int value = lut[c - 'A'];
        if (value > previous_value)
            result -= 2 * previous_value;
        result += value;
        previous_value = value;
    }
    return result;
}

// ############################################################################
// ############################################################################


void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = romanToInt(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("III", 3, trials);
    test("LVIII", 58, trials);
    test("MCMXCIV", 1994, trials);
    return 0;
}

