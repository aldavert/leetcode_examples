#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int numberOfBeams(std::vector<std::string> bank)
{
    const size_t m = bank.front().size();
    int result = 0, previous = 0;
    for (size_t i = 0, n = bank.size(); i < n; ++i)
    {
        int current = 0;
        for (size_t j = 0; j < m; ++j)
            current += (bank[i][j] == '1');
        result += previous * current;
        if (current != 0) previous = current;
    }
    return result;
}
#else
int numberOfBeams(std::vector<std::string> bank)
{
    int result = 0;
    for (int previous = 0; auto r : bank)
    {
        int current = static_cast<int>(std::count_if(r.begin(), r.end(), [](char c) { return c == '1'; }));
        if (current != 0)
        {
            result += previous * current;
            previous = current;
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> bank, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfBeams(bank);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"011001", "000000", "010100", "001000"}, 8, trials);
    test({"000", "111", "000"}, 0, trials);
    return 0;
}


