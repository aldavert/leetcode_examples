#include "../common/common.hpp"
#include <limits>
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> longestSpecialPath(std::vector<std::vector<int> > edges,
                                    std::vector<int> nums)
{
    const int n = static_cast<int>(edges.size() + 1);
    constexpr int N = 50'000;
    int hd[N], to[N * 2], next[N * 2], length[N * 2], dep[N + 1], sum[N] = {};
    std::fill_n(hd, n, -1);
    for (int tot = 0; const auto &edge: edges)
    {
        to[tot] = edge[1];
        next[tot] = hd[edge[0]];
        length[tot] = edge[2];
        hd[edge[0]] = tot++;
        to[tot] = edge[0];
        next[tot] = hd[edge[1]];
        length[tot] = edge[2];
        hd[edge[1]] = tot++;
    }
    int mx = *std::max_element(nums.begin(), nums.end());
    std::fill_n(dep, mx + 1, -1);
    int result = 0, count = 1;
    auto dfs = [&](auto &&self, int u, int p, int d, int d0) -> void
    {
        d0 = std::max(dep[nums[u]] + 1, d0);
        int tmp = dep[nums[u]];
        dep[nums[u]] = d;
        if (sum[d] - sum[d0] > result)
        {
            result = sum[d] - sum[d0];
            count = d - d0 + 1;
        }
        else if (sum[d] - sum[d0] == result)
            count = std::min(count, d - d0 + 1);
        for (int e = hd[u]; e >= 0; e = next[e])
        {
            if (to[e] == p) continue;
            sum[d + 1] = sum[d] + length[e];
            self(self, to[e], u, d + 1, d0);
        }
        dep[nums[u]] = tmp;
    };
    dfs(dfs, 0, -1, 0, 0);
    return {result, count};
}
#else
std::vector<int> longestSpecialPath(std::vector<std::vector<int> > edges,
                                    std::vector<int> nums)
{
    std::vector<std::unordered_map<int, int> > graph(nums.size());
    for (const auto &edge : edges)
        graph[edge[0]][edge[1]] = edge[2],
        graph[edge[1]][edge[0]] = edge[2];
    std::unordered_map<int, int> last_occurrence;
    std::vector<std::tuple<int, int> > path;
    int start = 0,
        result_length = std::numeric_limits<int>::lowest(),
        result_min_nodes = std::numeric_limits<int>::max();
    auto dfs = [&](auto &&self, int u, int parent, int distance) -> void
    {
        int old_nums_u_occ = -1, old_start = -1;
        if (auto search = last_occurrence.find(nums[u]);
            search != last_occurrence.end())
        {
            old_nums_u_occ = search->second;
            if (search->second >= start)
            {
                old_start = start;
                start = search->second + 1;
            }
        }
        path.push_back({u, distance});
        last_occurrence[nums[u]] = static_cast<int>(path.size()) - 1;
        int path_length = distance - std::get<1>(path[start]);
        int num_nodes = static_cast<int>(path.size()) - start;
        if (path_length > result_length)
            result_length = path_length,
            result_min_nodes = num_nodes;
        else if (path_length == result_length)
            result_min_nodes = std::min(result_min_nodes, num_nodes);
        for (auto [v, _] : graph[u])
        {
            if (v == parent) continue;
            self(self, v, u, distance + graph[u][v]);
        }
        if (old_start != -1)
            start = old_start,
            last_occurrence[nums[u]] = old_nums_u_occ;
        else if (old_nums_u_occ != -1)
            last_occurrence[nums[u]] = old_nums_u_occ;
        else last_occurrence.erase(nums[u]);
        path.pop_back();
    };
    dfs(dfs, 0, -1, 0);
    return {result_length, result_min_nodes};
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges,
          std::vector<int> nums,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestSpecialPath(edges, nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 2}, {1, 2, 3}, {1, 3, 5}, {1, 4, 4}, {2, 5, 6}},
         {2, 1, 2, 1, 3, 1}, {6, 2}, trials);
    test({{1, 0, 8}}, {2, 2}, {0, 1}, trials);
    return 0;
}


