#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

long long minimumRemoval(std::vector<int> beans)
{
    const long n = static_cast<long>(beans.size());
    const long sum = std::accumulate(beans.begin(), beans.end(), 0L);
    long result = std::numeric_limits<long>::max();
    std::sort(beans.begin(), beans.end());
    for (int i = 0; i < n; ++i)
        result = std::min(result, sum - (n - i) * beans[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> beans, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumRemoval(beans);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 1, 6, 5}, 4, trials);
    test({2, 10, 3, 2}, 7, trials);
    return 0;
}


