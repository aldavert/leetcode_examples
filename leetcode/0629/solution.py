def kInversePairs(n, k):
    dp = [[1] * (k + 1) for _ in range(n + 1)]
    sp = [[1] * (k + 1) for _ in range(n + 1)]
    N = 10 ** 9 + 7
    for i in range(1, n + 1):
        for j in range(1, k + 1):
            dp[i][j] = sp[i - 1][j] if j < i else (sp[i - 1][j] - sp[i - 1][j - i]) % N
            sp[i][j] = (sp[i][j - 1] + dp[i][j]) % N
    
    return dp[-1][-1]

def test(n, k, sol):
    res = kInversePairs(n, k)
    print(f'[{"SUCCESS" if res == sol else "FAILURE"}] kInversePairs({n}, {k})={kInversePairs(n, k)} --> {sol}')

if __name__ == '__main__':
    test(3, 0, 1)
    test(3, 1, 2)
    test(3, 3, 1)
    test(1000, 1000, 663677020)
