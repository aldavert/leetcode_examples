#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int kInversePairs(int n, int k)
{
    const int limit = 1'000'000'007;
    bool previous = false;
    int dp[2][1010] = {};
    dp[0][0] = dp[1][0] = 1;
    for (int i = 1; i <= n; ++i)
    {
        for (int j = 1; j <= k; ++j)
        {
            dp[previous][j] = (dp[!previous][j] + dp[previous][j - 1]) % limit;
            if (j - i >= 0)
                dp[previous][j] =
                    (dp[previous][j] - dp[!previous][j - i] + limit) % limit;
        }
        previous = !previous;
    }
    return dp[!previous][k];
}

// ############################################################################
// ############################################################################

void test(int n, int k, int solution, unsigned long trials = 1)
{
    int result = -1;
    long r = 0;
    for (unsigned long i = 0; i < trials; ++i)
        r += (result = kInversePairs(n, k));
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned long trials = 1'000'000'000'000'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 0, 1, trials);
    test(3, 1, 2, trials);
    test(3, 3, 1, trials);
    test(10, 11, 32683, trials);
    test(1000, 1000, 663677020, trials);
    return 0;
}


