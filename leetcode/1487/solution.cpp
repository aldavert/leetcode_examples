#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<std::string> getFolderNames(std::vector<std::string> &names)
{
    std::vector<std::string> result;
    std::unordered_map<std::string, int> name_to_suffix;
    
    for (const auto &name : names)
    {
        if (auto it = name_to_suffix.find(name); it != name_to_suffix.end())
        {
            int suffix = it->second;
            std::string new_name = name + "(" + std::to_string(++suffix) + ")";
            while (name_to_suffix.count(new_name))
                new_name = name + "(" + std::to_string(++suffix) + ")";
            name_to_suffix[name] = suffix;
            name_to_suffix[new_name] = 0;
            result.push_back(new_name);
        }
        else
        {
            name_to_suffix[name] = 0;
            result.push_back(name);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> names,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getFolderNames(names);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"pes", "fifa", "gta", "pes(2019)"},
         {"pes", "fifa", "gta", "pes(2019)"}, trials);
    test({"gta", "gta(1)", "gta", "avalon"},
         {"gta", "gta(1)", "gta(2)", "avalon"}, trials);
    test({"onepiece", "onepiece(1)", "onepiece(2)", "onepiece(3)", "onepiece"},
         {"onepiece", "onepiece(1)", "onepiece(2)", "onepiece(3)", "onepiece(4)"}, trials);
    return 0;
}


