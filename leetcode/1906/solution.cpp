#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> minDifference(std::vector<int> nums,
                               std::vector<std::vector<int> > queries)
{
    std::vector<int> num_to_indices[101];
    
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        num_to_indices[nums[i]].push_back(i);
    if (num_to_indices[nums[0]].size() == nums.size())
        return std::vector<int>(queries.size(), -1);
    
    std::vector<int> result;
    for (const auto &query : queries)
    {
        const int l = query[0], r = query[1];
        int prev_num = -1, min_diff = 101;
        for (int num = 1; num <= 100; ++num)
        {
            const auto &indices = num_to_indices[num];
            const auto it = std::lower_bound(indices.begin(), indices.end(), l);
            if ((it == indices.cend()) || (*it > r))
                continue;
            if (prev_num != -1)
            min_diff = std::min(min_diff, num - prev_num);
            prev_num = num;
        }
        result.push_back((min_diff == 101)?-1:min_diff);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDifference(nums, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 4, 8}, {{0, 1}, {1, 2}, {2, 3}, {0, 3}}, {2, 1, 4, 1}, trials);
    test({4, 5, 2, 2, 7, 10}, {{2, 3}, {0, 2}, {0, 5}, {3, 5}}, {-1, 1, 1, 3}, trials);
    return 0;
}


