#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countPairs(std::vector<int> nums, int k)
{
    std::vector<int> indices[101];
    int result = 0;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        for (int index : indices[nums[i]])
            result += (index * i) % k == 0;
        indices[nums[i]].push_back(i);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPairs(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1, 2, 2, 2, 1, 3}, 2, 4, trials);
    test({1, 2, 3, 4}, 1, 0, trials);
    test({10, 2, 3, 4, 9, 6, 3, 10, 3, 6, 3, 9, 1}, 4, 8, trials);
    return 0;
}


