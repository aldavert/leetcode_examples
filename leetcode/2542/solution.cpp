#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

long long maxScore(std::vector<int> nums1, std::vector<int> nums2, int k)
{
    long long result = 0, sum = 0;
    std::vector<std::pair<int, int> > input;
    std::priority_queue<int, std::vector<int>, std::greater<> > min_heap;

    for (int i = 0; i < static_cast<int>(nums1.size()); ++i)
        input.emplace_back(nums2[i], nums1[i]);
    std::sort(input.begin(), input.end(), std::greater<>());
    
    for (auto [num2, num1] : input)
    {
        min_heap.push(num1);
        sum += num1;
        if (static_cast<int>(min_heap.size()) > k)
            sum -= min_heap.top(), min_heap.pop();
        if (static_cast<int>(min_heap.size()) == k)
            result = std::max(result, sum * num2);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int k,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxScore(nums1, nums2, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 3, 2}, {2, 1, 3, 4}, 3, 12, trials);
    test({4, 2, 3, 1, 1}, {7, 5, 10, 9, 6}, 1, 30, trials);
    return 0;
}


