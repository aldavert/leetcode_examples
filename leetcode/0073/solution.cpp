#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 0
void setZeroes(std::vector<std::vector<int> > &matrix)
{
    const int m = static_cast<int>(matrix.size()),
              n = static_cast<int>(matrix[0].size());
    bool zero_row[201] = {}, zero_col[201] = {};
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (matrix[i][j] == 0)
                zero_row[i] = zero_col[j] = true;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (zero_row[i] || zero_col[j])
                matrix[i][j] = 0;
}
#elif 1
void setZeroes(std::vector<std::vector<int> > &matrix)
{
    const int m = static_cast<int>(matrix.size()),
              n = static_cast<int>(matrix[0].size());
    std::vector<bool> zero_row(m, false), zero_col(n, false);
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (matrix[i][j] == 0)
                zero_row[i] = zero_col[j] = true;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (zero_row[i] || zero_col[j])
                matrix[i][j] = 0;
}
#else
void setZeroes(std::vector<std::vector<int> > &matrix)
{
    const int m = static_cast<int>(matrix.size()),
              n = static_cast<int>(matrix[0].size());
    bool zero_row[201] = {}, zero_col[201] = {};
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (matrix[i][j] == 0)
                zero_row[i] = zero_col[j] = true;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (zero_row[i] || zero_col[j])
                matrix[i][j] = 0;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = matrix;
        setZeroes(result);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 1}, {1, 0, 1}, {1, 1, 1}},
         {{1, 0, 1}, {0, 0, 0}, {1, 0, 1}}, trials);
    test({{0, 1, 2, 0}, {3, 4, 5, 2}, {1, 3, 1, 5}},
         {{0, 0, 0, 0}, {0, 4, 5, 0}, {0, 3, 1, 0}}, trials);
    return 0;
}


