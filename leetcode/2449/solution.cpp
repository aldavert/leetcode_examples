#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long makeSimilar(std::vector<int> nums, std::vector<int> target)
{
    std::vector<int> nums_even_odd[2], target_even_odd[2];
    
    for (int num : nums) nums_even_odd[num % 2].push_back(num);
    for (int num : target) target_even_odd[num % 2].push_back(num);
    std::sort(nums_even_odd[0].begin(), nums_even_odd[0].end());
    std::sort(nums_even_odd[1].begin(), nums_even_odd[1].end());
    std::sort(target_even_odd[0].begin(), target_even_odd[0].end());
    std::sort(target_even_odd[1].begin(), target_even_odd[1].end());
    
    long result = 0;
    for (int i = 0; i < 2; ++i)
        for (int j = 0, n = static_cast<int>(nums_even_odd[i].size()); j < n; ++j)
            result += abs(nums_even_odd[i][j] - target_even_odd[i][j]) / 2;
    return result / 2;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> target,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = makeSimilar(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 12, 6}, {2, 14, 10}, 2, trials);
    test({1, 2, 5}, {4, 1, 3}, 1, trials);
    test({1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}, 0, trials);
    return 0;
}


