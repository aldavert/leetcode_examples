#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> minReverseOperations(int n, int p, std::vector<int> banned, int k)
{
    std::vector<int> result(n);
    
    for (int b : banned) result[b] = -1;
    --k;
    int a = std::abs(p - k), b = p + k;
    if (b >= n) b = 2 * (n - 1) - b;
    int l[2] = {p, a}, r[2] = {p, b},  na = n,  nb = -1,  n2k = 2 * (n - 1) - k,  d = 2;
    auto update = [&](int i, int value) -> void
    {
        if (!result[i])
        {
            result[i] = value;
            na = std::min(na, std::abs(i - k));
            nb = std::max(nb, (i + k < n)?i + k:n2k - i);
        }
    };
    
    for (int i = a; i <= b; i += 2)  update(i, 1);
    a = na;
    b = nb;
    while(a <= b)
    {
        na = n;
        nb = -1;
        for (int i = a, im = l[d & 1]; i < im; i += 2) update(i, d);
        for (int i = b, im = r[d & 1]; i > im; i -= 2) update(i, d);
        l[d & 1] = std::min(l[d & 1], a);
        r[d & 1] = std::max(r[d & 1], b);
        a = na;
        b = nb;
        ++d;
    }
    for(int i = 0; i < n; ++i)
        if(!result[i])
            result[i] = -1;
    result[p] = 0;
    return result;
}
#else
std::vector<int> minReverseOperations(int n, int p, std::vector<int> banned, int k)
{
    std::unordered_set<int> banned_set{banned.begin(), banned.end()};
    std::vector<int> result(n, -1);
    std::vector<std::set<int> > unseen(2);
    
    for (int num = 0; num < n; ++num)
        if ((num != p) && !banned_set.count(num))
            unseen[num & 1].insert(num);
    std::queue<int> q{{p}};
    result[p] = 0;
    while (!q.empty())
    {
        int u = q.front();
        q.pop();
        int lo = std::max(u - k + 1, k - 1 - u),
            hi = std::min(u + k - 1, n - 1 - (u - (n - k)));
        auto &nums = unseen[lo & 1];
        for (auto it = nums.lower_bound(lo); (it != nums.end()) && *it <= hi;)
        {
            result[*it] = result[u] + 1;
            q.push(*it);
            it = nums.erase(it);
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          int p,
          std::vector<int> banned,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minReverseOperations(n, p, banned, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 0, {1, 2}, 4, {0, -1, -1, 1}, trials);
    test(5, 0, {2, 4}, 3, {0, -1, -1, -1, -1}, trials);
    test(4, 2, {0, 1, 3}, 1, {-1, -1, 0, -1}, trials);
    test(5, 0, {}, 2, {0, 1, 2, 3, 4}, trials);
    return 0;
}


