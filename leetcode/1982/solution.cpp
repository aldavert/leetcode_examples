#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> recoverArray(int /*n*/, std::vector<int> sums)
{
    std::vector<int> sorted_sums(sums);
    std::sort(sorted_sums.begin(), sorted_sums.end());
    
    auto recover =
        [&](auto &&self, const std::vector<int> &values) -> std::vector<int>
    {
        if (values.size() == 1) return {};
        std::unordered_map<int, int> count;
        std::vector<int> result;
        
        for (int sum : values) ++count[sum];
        int num = values[1] - values[0];
        bool include_num = false; 
        for (int sum : values)
        {
            auto search = count.find(sum);
            if ((search == count.end()) || (search->second == 0)) continue;
            --search->second;
            --count[sum + num];
            result.push_back(sum);
            include_num = include_num || (sum + num == 0);
        }
        if (include_num)
            for (int &value : result)
                value += num;
        result = self(self, result);
        result.push_back(include_num?-num:num);
        return result;
    };
    return recover(recover, sorted_sums);
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<int> left_work(left), right_work(right);
    std::sort(left_work.begin(), left_work.end());
    std::sort(right_work.begin(), right_work.end());
    auto test = [&](void) -> bool
    {
        for (size_t i = 0; i < left.size(); ++i)
            if (left_work[i] != right_work[i])
                return false;
        return true;
    };
    if (test()) return true;
    for (int &value : right_work)
        value = -value;
    std::sort(right_work.begin(), right_work.end());
    return test();
}

void test(int n,
          std::vector<int> sums,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = recoverArray(n, sums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {-3, -2, -1, 0, 0, 1, 2, 3}, {1, 2, -3}, trials);
    test(2, {0, 0, 0, 0}, {0, 0}, trials);
    test(4, {0, 0, 5, 5, 4, -1, 4, 9, 9, -1, 4, 3, 4, 8, 3, 8}, {0, -1, 4, 5}, trials);
    return 0;
}


