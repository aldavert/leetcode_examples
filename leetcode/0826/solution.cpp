#include "../common/common.hpp"
#include <map>

// ############################################################################
// ############################################################################

#if 1
int maxProfitAssignment(std::vector<int> difficulty,
                        std::vector<int> profit,
                        std::vector<int> worker)
{
    constexpr int max_difficulty = 100001;
    int histogram[max_difficulty]{};
    const int n = static_cast<int>(difficulty.size());
    for (int i = 0; i < n; ++i)
        histogram[difficulty[i]] = std::max(histogram[difficulty[i]], profit[i]);
    for (int i = 1; i < max_difficulty; ++i)
        histogram[i] = std::max(histogram[i], histogram[i - 1]);
    int result = 0;
    for (auto &w : worker)
        result += histogram[w];
    return result;
}
#else
int maxProfitAssignment(std::vector<int> difficulty,
                        std::vector<int> profit,
                        std::vector<int> worker)
{
    std::multimap<int, int> dp;
    for (size_t i = 0; i < difficulty.size(); ++i)
        dp.insert({difficulty[i], profit[i]});
    std::vector<int> worker_copy(worker);
    std::sort(worker_copy.begin(), worker_copy.end());
    int result = 0;
    auto it = dp.begin();
    int best_profit = 0;
    for (auto w : worker_copy)
    {
        for (; (it != dp.end()) && (w >= it->first); ++it)
            best_profit = std::max(best_profit, it->second);
        result += best_profit;
    }
    return result;
}
#endif

void test(std::vector<int> difficulty,
          std::vector<int> profit,
          std::vector<int> worker,
          int solution)
{
    int result = maxProfitAssignment(difficulty, profit, worker);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
    test({2, 4, 6, 8, 10}, {10, 20, 30, 40, 50}, {4, 5, 6, 7}, 100);
    test({85, 47, 57}, {24, 66, 99}, {40, 25, 25}, 0);
    return 0;
}


