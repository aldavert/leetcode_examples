#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> numSmallerByFrequency(std::vector<std::string> queries,
                                       std::vector<std::string> words)
{
    std::vector<int> words_frequency, result;
    auto f = [](const std::string &word) -> int
    {
        int count = 0;
        char current_char = 'z' + 1;
        for (const char c : word)
        {
            if (c < current_char)
                current_char = c,
                count = 1;
            else count += (c == current_char);
        }
        return count;
    };
    for (const auto &word : words)
        words_frequency.push_back(f(word));
    std::sort(words_frequency.begin(), words_frequency.end());
    for (const auto &query : queries)
        result.push_back(static_cast<int>(words_frequency.end()
                    - std::upper_bound(words_frequency.begin(), words_frequency.end(), f(query))));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> queries,
          std::vector<std::string> words,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = numSmallerByFrequency(queries, words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"cbd"}, {"zaaaz"}, {1}, trials);
    test({"bbb", "cc"}, {"a", "aa", "aaa", "aaaa"}, {1, 2}, trials);
    return 0;
}


