#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int minimumRounds(std::vector<int> tasks)
{
    std::unordered_map<int, int> histogram;
    for (int difficulty : tasks)
        ++histogram[difficulty];
    int result = 0;
    for (auto [difficulty, frequency] : histogram)
    {
        if (frequency == 1) return -1;
        int quotient = frequency / 3;
        result += quotient + (quotient * 3 != frequency);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tasks, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumRounds(tasks);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 2, 3, 3, 2, 4, 4, 4, 4, 4}, 4, trials);
    test({2, 3, 3}, -1, trials);
    return 0;
}


