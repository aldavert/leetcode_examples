#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

ListNode * addTwoNumbers(ListNode * l1, ListNode * l2)
{
    const ListNode * p1 = l1, * p2 = l2;
    int remainder = 0;
    ListNode * result = new ListNode();
    ListNode * current = result;
    for (; (p1 != nullptr) && (p2 != nullptr); p1 = p1->next, p2 = p2->next)
    {
        int sum = p1->val + p2->val + remainder;
        remainder = sum / 10;
        current = current->next = new ListNode(sum - remainder * 10);
    }
    for (; p1 != nullptr; p1 = p1->next)
    {
        int sum = p1->val + remainder;
        remainder = sum / 10;
        current = current->next = new ListNode(sum - remainder * 10);
    }
    for (; p2 != nullptr; p2 = p2->next)
    {
        int sum = p2->val + remainder;
        remainder = sum / 10;
        current = current->next = new ListNode(sum - remainder * 10);
    }
    if (remainder)
        current->next = new ListNode(remainder);
    current = result;
    result = result->next;
    current->next = nullptr;
    delete current;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> v1,
          std::vector<int> v2,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * l1 = vec2list(v1);
        ListNode * l2 = vec2list(v2);
        ListNode * lr = addTwoNumbers(l1, l2);
        result = list2vec(lr);
        delete l1;
        delete l2;
        delete lr;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4, 3}, {5, 6, 4}, {7, 0, 8}, trials);
    test({0}, {0}, {0}, trials);
    test({9, 9, 9, 9, 9, 9, 9}, {9, 9, 9, 9}, {8, 9, 9, 9, 0, 0, 0, 1}, trials);
    return 0;
}


