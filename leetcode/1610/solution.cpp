#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int visiblePoints(std::vector<std::vector<int> > points,
                  int angle,
                  std::vector<int> location)
{
    std::vector<double> point_angles;
    int same = 0, max_visible = 0;
    
    for (const auto &p : points)
        if ((p[0] == location[0]) && (p[1] == location[1])) ++same;
        else point_angles.push_back(std::atan2(p[1] - location[1],
                                               p[0] - location[0]) * 180 / M_PI);
    std::sort(point_angles.begin(), point_angles.end());
    
    for (int i = 0, n = static_cast<int>(point_angles.size()); i < n; ++i)
        point_angles.push_back(point_angles[i] + 360);
    for (int l = 0, r = 0, n = static_cast<int>(point_angles.size()); r < n; ++r)
    {
        while (point_angles[r] - point_angles[l] > angle) ++l;
        max_visible = std::max(max_visible, r - l + 1);
    }
    return max_visible + same;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points,
          int angle,
          std::vector<int> location,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = visiblePoints(points, angle, location);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 1}, {2, 2}, {3, 3}}, 90, {1, 1}, 3, trials);
    test({{2, 1}, {2, 2}, {3, 4}, {1, 1}}, 90, {1, 1}, 4, trials);
    test({{1, 0}, {2, 1}}, 13, {1, 1}, 1, trials);
    return 0;
}


