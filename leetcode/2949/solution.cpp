#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int beautifulSubstrings(std::string s, int k)
{
    constexpr std::string_view VOWELS = "aeiouAEIOU";
    auto calcRoot = [](int v) -> int
    {
        for (int i = 1; i <= v; ++i)
            if (i * i % v == 0)
                return i;
        return -1;
    };
    auto isVowel = [&](char c) -> bool
    {
        return VOWELS.find(c) != std::string_view::npos;
    };
    struct PairHash
    {
        size_t operator()(const std::pair<int, int> &p) const
        {
            return p.first ^ p.second;
        }
    };
    const int root = calcRoot(k);
    int result = 0, vowels = 0, vowels_minus_consonants = 0;
    std::unordered_map<std::pair<int, int>, int, PairHash> prefix_count{{{0, 0}, 1}};
    for (const char c : s)
    {
        if (isVowel(c))
        {
            vowels = (vowels + 1) % root;
            ++vowels_minus_consonants;
        }
        else --vowels_minus_consonants;
        const std::pair<int, int> prefix{vowels, vowels_minus_consonants};
        result += prefix_count[prefix]++;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = beautifulSubstrings(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("baeyh", 2, 2, trials);
    test("abba", 1, 3, trials);
    test("bcdf", 1, 0, trials);
    return 0;
}


