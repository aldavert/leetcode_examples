#include "../common/common.hpp"

// ############################################################################
// ############################################################################

double soupServings(int n)
{
    double memo[4800 / 25][4800 / 25] = {};
    auto dfs = [&](auto &&self, int a, int b) -> double
    {
        if ((a <= 0) && (b <= 0)) return 0.5;
        if (a <= 0) return 1.0;
        if (b <= 0) return 0.0;
        if (memo[a][b] > 0) return memo[a][b];
        return memo[a][b] = 0.25 * (self(self, a - 4, b    )
                                 +  self(self, a - 3, b - 1)
                                 +  self(self, a - 2, b - 2)
                                 +  self(self, a - 1, b - 3));
    };
    return (n >= 4800)?1.0:dfs(dfs, (n + 24) / 25, (n + 24) / 25);
}

// ############################################################################
// ############################################################################

void test(int n, double solution, unsigned int trials = 1)
{
    double result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = soupServings(n);
    showResult(std::abs(solution - result) < 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(50, 0.62500, trials);
    test(100, 0.71875, trials);
    return 0;
}


