#include "../common/common.hpp"
#include <limits>
#include <deque>

// ############################################################################
// ############################################################################

#if 1
int maxDifference(std::string s, int k)
{
    constexpr int MAX = std::numeric_limits<int>::max();
    const int n = static_cast<int>(s.size());
    int cnt[2][30'001] = {};
    int result = -1'000'000'000;
    for (int i = 0; i < 5; ++i)
    {
        for (int j = 0; j < 5; ++j)
        {
            if (i == j) continue;
            int cur_i = 0, cur_j = 0, mn[2][2] = {MAX, MAX, MAX, MAX}, idx = 0;
            cnt[0][0] = cnt[1][0] = 0;
            for (int l = 1; l <= n; ++l)
            {
                if      (s[l - 1] == '0' + i) ++cur_i;
                else if (s[l - 1] == '0' + j) ++cur_j;
                cnt[0][l] = cur_i;
                cnt[1][l] = cur_j;
                while ((l - idx >= k) && (cur_i != cnt[0][idx]) && (cur_j != cnt[1][idx]))
                {
                    int id1 = cnt[0][idx] & 1,
                        id2 = cnt[1][idx] & 1;
                    mn[id1][id2] = std::min(mn[id1][id2], cnt[0][idx] - cnt[1][idx]);
                    ++idx;
                }
                int id1 = cur_i & 1, id2 = cur_j & 1;
                if (mn[!id1][id2] != MAX)
                    result = std::max(result, cur_i - cur_j - mn[!id1][id2]);
            }
        }
    }
    return result;
}
#else
int maxDifference(std::string s, int k)
{
    constexpr long INF = std::numeric_limits<int>::max();
    const int n = static_cast<int>(s.size() + 1);
    std::set<char> v(s.begin(), s.end());
    s += "5";
    long overall = -INF;
    for (auto odd : v)
    {
        for (auto even : v)
        {
            if (odd == even) continue;
            long counto = 0, counte = 0;
            std::vector<long> best(4, INF);
            std::deque<std::tuple<long, long, long> > current;
            current.push_back({k - 1, 0, 0});
            for (int i = 0; i < n; ++i)
            {
                current.push_back({i + k, counto, counte});
                if      (s[i] ==  odd) counto += 1;
                else if (s[i] == even) counte += 1;
                while ((current.size() > 0) && (std::get<0>(current[0]) <= i))
                {
                    auto [_, bo, be] = current[0];
                    if (be == counte) break;
                    int bindex = static_cast<int>((bo % 2) * 2 + (be % 2));
                    best[bindex] = std::min(best[bindex], bo - be);
                    current.pop_front();
                }
                if (i + 1 >= k)
                {
                    int bindex = static_cast<int>(((counto + 1) % 2) * 2 + (counte % 2));
                    overall = std::max(overall, counto - counte - best[bindex]);
                }
            }
        }
    }
    return static_cast<int>(overall);
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxDifference(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("12233", 4, -1, trials);
    test("1122211", 3, 1, trials);
    test("110", 3, -1, trials);
    test("2421", 1, -1, trials);
    test("44114402", 7, 1, trials);
    return 0;
}


