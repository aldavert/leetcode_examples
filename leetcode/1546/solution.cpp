#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxNonOverlapping(std::vector<int> nums, int target)
{
    int result = 0, prefix = 0;
    std::unordered_set<int> prefixes{0};
    
    for (int num : nums)
    {
        prefix += num;
        if (prefixes.count(prefix - target))
        {
            ++result;
            prefix = 0;
            prefixes = {0};
        }
        else prefixes.insert(prefix);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxNonOverlapping(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 1, 1, 1}, 2, 2, trials);
    test({-1, 3, 5, 1, 4, 2, -9}, 6, 2, trials);
    return 0;
}


