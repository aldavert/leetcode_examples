#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isBalanced(std::string num)
{
    int sum[2] = {};
    for (size_t i = 0, n = num.size(); i < n; ++i)
        sum[i & 1] += num[i] - '0';
    return sum[0] == sum[1];
}

// ############################################################################
// ############################################################################

void test(std::string num, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int t = 0; t < trials; ++t)
        result = isBalanced(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1234", false, trials);
    test("24123", true, trials);
    return 0;
}


