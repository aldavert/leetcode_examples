# Minimize the Maximum of Two Arrays

We have two arrays `arr1` and `arr2` which are initially empty. You need to add positive integers to them such that they satisfy all the following conditions:
- `arr1` contains `uniqueCnt_1` **distinct** positive integers, each of which is **not divisible** by `divisor_1`.
- `arr2` contains `uniqueCnt_2` **distinct** positive integers, each of which is **not divisible** by `divisor_2`.
- **No** integer is present in both `arr1` and `arr2`.

Given `divisor_1`, `divisor_2`, `uniqueCnt_1`, and `uniqueCnt_2`, return *the* ***minimum possible maximum*** *integer that can be present in either array.*

#### Example 1:
> *Input:* `divisor_1 = 2, divisor_2 = 7, uniqueCnt_1 = 1, uniqueCnt_2 = 3`  
> *Output:* `4`  
> *Explanation:* We can distribute the first 4 natural numbers into `arr1` and `arr2`. `arr1 = [1]` and `arr2 = [2, 3, 4]`. We can see that both arrays satisfy all the conditions. Since the maximum value is `4`, we return it.

#### Example 2:
> *Input:* `divisor_1 = 3, divisor_2 = 5, uniqueCnt_1 = 2, uniqueCnt_2 = 1`  
> *Output:* `3`
> *Explanation:* Here `arr1 = [1, 2]`, and `arr2 = [3]` satisfy all conditions. Since the maximum value is `3`, we return it.

#### Example 3:
> *Input:* `divisor_1 = 2, divisor_2 = 4, uniqueCnt_11 = 8, uniqueCnt_2 = 2`  
> *Output:* `15`  
> *Explanation:* Here, the final possible arrays can be `arr1 = [1, 3, 5, 7, 9, 11, 13, 15]`, and `arr2 = [2, 6]`. It can be shown that it is not possible to obtain a lower maximum satisfying all conditions. 
 

Constraints:
- `2 <= divisor_1, divisor_2 <= 10^5`
- `1 <= uniqueCnt1, uniqueCnt2 < 10^9`
- `2 <= uniqueCnt1 + uniqueCnt_2 <= 10^9`


