#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int minimizeSet(int divisor1, int divisor2, int uniqueCnt1, int uniqueCnt2)
{
    const long divisor_lcm = std::lcm(static_cast<long>(divisor1), divisor2);
    auto isPossible = [&](long m) -> bool
    {
        return ((m - m / divisor1) >= uniqueCnt1)
            && ((m - m / divisor2) >= uniqueCnt2)
            && ((m - m / divisor_lcm) >= uniqueCnt1 + uniqueCnt2);
    };
    
    long l = 0, r = std::numeric_limits<long>::max();
    while (l < r)
    {
        const long m = (l + r) / 2;
        if (isPossible(m))
            r = m;
        else l = m + 1;
    }
    return static_cast<int>(l);
}

// ############################################################################
// ############################################################################

void test(int divisor1,
          int divisor2,
          int uniqueCnt1,
          int uniqueCnt2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimizeSet(divisor1, divisor2, uniqueCnt1, uniqueCnt2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 7, 1, 3, 4, trials);
    test(3, 5, 2, 1, 3, trials);
    return 0;
}


