#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
std::vector<long long> maximumEvenSplit(long long finalSum)
{
    if (finalSum & 1) return {};
    std::vector<long long> result;
    for (int i = 2; i <= finalSum; finalSum -= i, i += 2)
        result.push_back(i);
    result.back() += finalSum;
    return result;
}
#else
std::vector<long long> maximumEvenSplit(long long finalSum)
{
    if (finalSum & 1) return {};
    std::vector<long long> result;
    long need_sum = finalSum;
    
    for (long even = 2; need_sum - even >= even + 2; need_sum -= even, even += 2)
        result.push_back(even);
    result.push_back(need_sum);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(long long finalSum, std::vector<long long> solution, unsigned int trials = 1)
{
    std::vector<long long> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumEvenSplit(finalSum);
    bool valid = (solution.size() == result.size())
              && (solution.empty()
              || (std::accumulate(result.begin(), result.end(), 0) == finalSum));
    showResult(valid, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(12, {2, 4, 6}, trials);
    test(7, {}, trials);
    test(28, {6, 8, 2, 12}, trials);
    return 0;
}


