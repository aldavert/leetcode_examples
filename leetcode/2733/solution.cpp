#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int findNonMinOrMax(std::vector<int> nums)
{
    if (nums.size() < 3) return -1;
    int a = nums[0], b = nums[1], c = nums[2];
    if (a > b) std::swap(a, b);
    if (b > c) std::swap(b, c);
    if (a > b) std::swap(a, b);
    return b;
}
#elif 0
int findNonMinOrMax(std::vector<int> nums)
{
    if (nums.size() < 3) return -1;
    return nums[0] + nums[1] + nums[2]
         - std::min({nums[0], nums[1], nums[2]})
         - std::max({nums[0], nums[1], nums[2]});
}
#else
int findNonMinOrMax(std::vector<int> nums)
{
    int minimum = nums[0], maximum = nums[0];
    for (int number : nums)
    {
        if      (number <= minimum) minimum = number;
        else if (number >= maximum) maximum = number;
    }
    for (int number : nums)
        if ((number > minimum) && (number < maximum)) return number;
    return -1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::unordered_set<int> solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findNonMinOrMax(nums);
    showResult(solution.find(result) != solution.end(), solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 1, 4}, {2, 3}, trials);
    test({1, 2}, {-1}, trials);
    test({2, 1, 3}, {2}, trials);
    return 0;
}


