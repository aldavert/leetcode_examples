#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string reverseWords(std::string s)
{
    const int n = static_cast<int>(s.size());
    int start = -1;
    for (int i = 0; i < n; ++i)
    {
        if (s[i] == ' ')
        {
            if (i - start > 2)
                std::reverse(&s[start + 1], &s[i]);
            start = i;
        }
    }
    if (n - start > 2)
        std::reverse(&s[start + 1], &s[n]);
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = reverseWords(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("Let's take LeetCode contest", "s'teL ekat edoCteeL tsetnoc", trials);
    test("God Ding", "doG gniD", trials);
    test("God Ding u", "doG gniD u", trials);
    test("  God Ding  ", "  doG gniD  ", trials);
    test("u God Ding", "u doG gniD", trials);
    return 0;
}


