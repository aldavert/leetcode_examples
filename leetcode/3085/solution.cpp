#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumDeletions(std::string word, int k)
{
    int result = std::numeric_limits<int>::max();
    int count[26] = {};
    for (char c : word) ++count[c - 'a'];
    for (int i = 0; i < 26; ++i)
    {
        int deletions = 0;
        for (int j = 0; j < 26; ++j)
        {
            if (count[j] < count[i]) deletions += count[j];
            else deletions += std::max(0, count[j] - (count[i] + k));
        }
        result = std::min(result, deletions);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumDeletions(word, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aabcaba", 0, 3, trials);
    test("dabdcbdcdcd", 2, 2, trials);
    test("aaabaaa", 2, 1, trials);
    return 0;
}


