#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numRookCaptures(std::vector<std::vector<char> > board)
{
    int result = 0;
    auto searchRook = [&](int &r, int &c) -> void
    {
        for (r = 0; r < 8; ++r)
            for (c = 0; c < 8; ++c)
                if (board[r][c] == 'R')
                    return;
    };
    auto check = [&](int r, int c) -> bool
    {
        if (board[r][c] == 'p')
        {
            ++result;
            return true;
        }
        else if (board[r][c] == 'B') return true;
        return false;
    };
    int r, c;
    searchRook(r, c);
    for (int i = r; i >= 0; --i)
        if (check(i, c)) break;
    for (int i = r; i < 8; ++i)
        if (check(i, c)) break;
    for (int i = c; i >= 0; --i)
        if (check(r, i)) break;
    for (int i = c; i < 8; ++i)
        if (check(r, i)) break;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > board, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numRookCaptures(board);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'.', '.', '.', '.', '.', '.', '.', '.'},
          {'.', '.', '.', 'p', '.', '.', '.', '.'},
          {'.', '.', '.', 'R', '.', '.', '.', 'p'},
          {'.', '.', '.', '.', '.', '.', '.', '.'},
          {'.', '.', '.', '.', '.', '.', '.', '.'},
          {'.', '.', '.', 'p', '.', '.', '.', '.'},
          {'.', '.', '.', '.', '.', '.', '.', '.'},
          {'.', '.', '.', '.', '.', '.', '.', '.'}}, 3, trials);
    test({{'.', '.', '.', '.', '.', '.', '.', '.'},
          {'.', 'p', 'p', 'p', 'p', 'p', '.', '.'},
          {'.', 'p', 'p', 'B', 'p', 'p', '.', '.'},
          {'.', 'p', 'B', 'R', 'B', 'p', '.', '.'},
          {'.', 'p', 'p', 'B', 'p', 'p', '.', '.'},
          {'.', 'p', 'p', 'p', 'p', 'p', '.', '.'},
          {'.', '.', '.', '.', '.', '.', '.', '.'},
          {'.', '.', '.', '.', '.', '.', '.', '.'}}, 0, trials);
    test({{'.', '.', '.', '.', '.', '.', '.', '.'},
          {'.', '.', '.', 'p', '.', '.', '.', '.'},
          {'.', '.', '.', 'p', '.', '.', '.', '.'},
          {'p', 'p', '.', 'R', '.', 'p', 'B', '.'},
          {'.', '.', '.', '.', '.', '.', '.', '.'},
          {'.', '.', '.', 'B', '.', '.', '.', '.'},
          {'.', '.', '.', 'p', '.', '.', '.', '.'},
          {'.', '.', '.', '.', '.', '.', '.', '.'}}, 3, trials);
    return 0;
}


