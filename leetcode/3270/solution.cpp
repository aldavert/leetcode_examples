#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int generateKey(int num1, int num2, int num3)
{
    int result = 0;
    for (int i = 0, p = 1; i < 4; ++i, p *= 10, num1 /= 10, num2 /= 10, num3 /= 10)
        result = result + p * std::min({num1 % 10, num2 % 10, num3 % 10});
    return result;
}

// ############################################################################
// ############################################################################

void test(int num1, int num2, int num3, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = generateKey(num1, num2, num3);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 10, 1000, 0, trials);
    test(987, 879, 798, 777, trials);
    return 0;
}


