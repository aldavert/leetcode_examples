#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int minimumDeviation(std::vector<int> nums)
{
    int result = std::numeric_limits<int>::max();
    int min_n = std::numeric_limits<int>::max();
    std::priority_queue<int> pq;
    for (int n : nums)
    {
        n = (n & 1)?(n * 2):n;
        pq.push(n);
        min_n = std::min(n, min_n);
    }
    while ((pq.top() & 1) == 0)
    {
        int n = pq.top();
        pq.pop();
        pq.push(n / 2);
        min_n = std::min(min_n, n / 2);
        result = std::min(result, pq.top() - min_n);
    }
    return result;
}
#else
int minimumDeviation(std::vector<int> nums)
{
    std::set<int> s;
    for (int x : nums) s.insert((x & 1)?(x * 2):x);
    int result = *s.rbegin() - *s.begin();
    while ((*rbegin(s) & 1) == 0)
    {
        s.insert(*s.rbegin() / 2);
        s.erase(--(s.rbegin().base()));
        result = std::min(result, *s.rbegin() - *s.begin());
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumDeviation(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 1, trials);
    test({4, 1, 5, 20, 3}, 3, trials);
    test({2, 10, 8}, 3, trials);
    test({1, 3}, 1, trials);
    test({3, 5}, 1, trials);
    return 0;
}


