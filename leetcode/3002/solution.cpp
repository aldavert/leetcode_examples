#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumSetSize(std::vector<int> nums1, std::vector<int> nums2)
{
    const std::unordered_set<int> set1{nums1.begin(), nums1.end()};
    const std::unordered_set<int> set2{nums2.begin(), nums2.end()};
    std::unordered_set<int> common;
    for (int num1 : set1)
        if (set2.contains(num1))
            common.insert(num1);
    const int n = static_cast<int>(nums1.size()),
              n1 = static_cast<int>(set1.size()),
              n2 = static_cast<int>(set2.size()),
              nc = static_cast<int>(common.size());
    return std::min(n, std::min(n1 - nc, n / 2) + std::min(n2 - nc, n / 2) + nc);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumSetSize(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1, 2}, {1, 1, 1, 1}, 2, trials);
    test({1, 2, 3, 4, 5, 6}, {2, 3, 2, 3, 2, 3}, 5, trials);
    test({1, 1, 2, 2, 3, 3}, {4, 4, 5, 5, 6, 6}, 6, trials);
    return 0;
}


