#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> cycleLengthQueries(int /*n*/, std::vector<std::vector<int> > queries)
{
    std::vector<int> result;
    for (const auto &query : queries)
    {
        int current = 1;
        for (int a = query[0], b = query[1]; a != b; )
        {
            if (a > b) a /= 2;
            else b /= 2;
            ++current;
        }
        result.push_back(current);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = cycleLengthQueries(n, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{5, 3}, {4, 7}, {2, 3}}, {4, 5, 3}, trials);
    test(2, {{1, 2}}, {2}, trials);
    return 0;
}


