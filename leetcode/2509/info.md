# Cycle Length Queries in a Tree

You are given an integer `n`. There is a **complete binary tree** with `2^n - 1` nodes. The root of that tree is the node with the value `1`, and every node with a value `val` in the range `[1, 2^{n - 1} - 1]` has two children where:
- The left node has the value `2 * val`, and
- The right node has the value `2 * val + 1`.

You are also given a 2D integer array `queries` of length `m`, where `queries[i] = [a_i, b_i]`. For each query, solve the following problem:
- Add an edge between the nodes with values `a_i` and `b_i`.
- Find the length of the cycle in the graph.
- Remove the added edge between nodes with values `a_i` and `b_i`.

**Note** that:
- A **cycle** is a path that starts and ends at the same node, and each edge in the path is visited only once.
- The length of a cycle is the number of edges visited in the cycle.
- There could be multiple edges between two nodes in the tree after adding the edge of the query.

Return *an array* `answer` *of length* `m` *where* `answer[i]` *is the answer to the* `i^{th}` *query*.

#### Example 1:
> ```mermaid 
> graph TD;
> A1((1))---B1((2))
> A1---C1((3))
> B1---D1((4))
> B1---E1((5))
> C1---F1((6))
> C1---G1((7))
> A2((1))---B2((2))
> A2---C2((3))
> B2---D2((4))
> B2---E2((5))
> C2---F2((6))
> C2---G2((7))
> A3((1))---B3((2))
> A3---C3((3))
> B3---D3((4))
> B3---E3((5))
> C3---F3((6))
> C3---G3((7))
> E1---C1
> D2---G2
> B3---C3
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> linkStyle 18,19,20 stroke:red,stroke-width:3px;
> 
> classDef selected fill:#F99,stroke:#933,stroke-width:3px;
> class A1,B1,C1,E1 selected;
> class A2,B2,D2,C2,G2 selected;
> class A3,B3,C3 selected;
> ```
> *Input:* `n = 3, queries = [[5, 3], [4, 7], [2, 3]]`  
> *Output:* `[4, 5, 3]`  
> *Explanation:* The diagrams above show the tree of `2^3 - 1` nodes. Nodes colored in red describe the nodes in the cycle after adding the edge.
> - After adding the edge between nodes `3` and `5`, the graph contains a cycle of nodes `[5, 2, 1, 3]`. Thus answer to the first query is `4`. We delete the added edge and process the next query.
> - After adding the edge between nodes `4` and `7`, the graph contains a cycle of nodes `[4, 2, 1, 3, 7]`. Thus answer to the second query is `5`. We delete the added edge and process the next query.
> - After adding the edge between nodes `2` and `3`, the graph contains a cycle of nodes `[2, 1, 3]`. Thus answer to the third query is `3`. We delete the added edge.

#### Example 2:
> ```mermaid 
> graph TD;
> A((1))---B((2))
> A---C((3))
> A---B
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#F99,stroke:#933,stroke-width:3px;
> class A,B selected;
> linkStyle 2 stroke:red
> ```
> *Input:* `n = 2, queries = [[1, 2]]`  
> *Output:* `[2]`  
> *Explanation:* The diagram above shows the tree of `2^2 - 1` nodes. Nodes colored in red describe the nodes in the cycle after adding the edge.
> - After adding the edge between nodes `1` and `2`, the graph contains a cycle of nodes `[2, 1]`. Thus answer for the first query is `2`. We delete the added edge.

#### Constraints:
- `2 <= n <= 30`
- `m == queries.length`
- `1 <= m <= 10^5`
- `queries[i].length == 2`
- `1 <= a_i, b_i <= 2^n - 1`
- `a_i != b_i`


