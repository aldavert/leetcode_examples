#include "../common/common.hpp"

// ############################################################################
// ############################################################################

class Allocator
{
    std::vector<int> m_memory;
    std::vector<std::vector<int> > m_ID_to_indices;
public:
    Allocator(int n) : m_memory(n), m_ID_to_indices(1001) {}
    int allocate(int size, int mID)
    {
        int consecutive_free = 0;
        for (int i = 0, n = static_cast<int>(m_memory.size()); i < n; ++i)
        {
            consecutive_free = (m_memory[i] == 0)?consecutive_free + 1:0;
            if (consecutive_free == size)
            {
                for (int j = i - consecutive_free + 1; j <= i; ++j)
                {
                    m_memory[j] = mID;
                    m_ID_to_indices[mID].push_back(j);
                }
                return i - consecutive_free + 1;
            }
        }
        return -1;
    }
    int free(int mID)
    {
        std::vector<int> &indices = m_ID_to_indices[mID];
        int freed_units = static_cast<int>(indices.size());
        for (int index : indices)
            m_memory[index] = 0;
        indices.clear();
        return freed_units;
    }
};

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        Allocator obj(n);
        result.clear();
        for (const auto &parameters : input)
        {
            if (parameters.size() == 1)
                result.push_back(obj.free(parameters[0]));
            else result.push_back(obj.allocate(parameters[0], parameters[1]));
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, {{1, 1}, {1, 2}, {1, 3}, {2}, {3, 4}, {1, 1}, {1, 1}, {1}, {10, 2}, {7}},
         {0, 1, 2, 1, 3, 1, 6, 3, -1, 0}, trials);
    return 0;
}


