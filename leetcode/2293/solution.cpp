#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minMaxGame(std::vector<int> nums)
{
    std::vector<int> work(nums);
    for (int n = static_cast<int>(nums.size()); n > 1; n /= 2)
        for (int i = 0, j = 0; i < n; i += 2, ++j)
            work[j] = (!(j & 1))?std::min(work[i], work[i + 1])
                                :std::max(work[i], work[i + 1]);
    return work[0];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minMaxGame(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 5, 2, 4, 8, 2, 2}, 1, trials);
    test({3}, 3, trials);
    return 0;
}


