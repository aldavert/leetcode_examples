# Min-max Game

You are given a **0-indexed** integer array `nums` whose length is a power of `2`.

Apply the following algorithm on `nums`:
1. Let `n` be the length of `nums`. If `n == 1`, end the process. Otherwise, create a new **0-indexed** integer array `newNums` of length `n / 2`.
2. For every even index `i` where `0 <= i < n / 2`, assign the value of `newNums[i]` as `min(nums[2 * i], nums[2 * i + 1])`.
3. For every odd index `i` where `0 <= i < n / 2`, assign the value of `newNums[i]` as `max(nums[2 * i], nums[2 * i + 1])`.
4. Replace the array `nums` with `newNums`
5. Repeat the entire process starting from step `1`.

Return *the last number that remains in* `nums` *after applying the algorithm.*

#### Example 1:
> ```mermaid 
> graph TD;
> A[1]
> B[3]
> C[5]
> D[2]
> E[4]
> F[8]
> G[2]
> H[2]
> A---I[<b>MIN</b><br/>1]
> B---I
> C---J[<b>MAX</b><br/>5]
> D---J
> E---K[<b>MIN</b><br/>4]
> F---K
> G---L[<b>MAX</b><br/>2]
> H---L
> I---M[<b>MIN</b><br/>1]
> J---M
> K---N[<b>MAX</b><br/>4]
> L---N
> M---O[<b>MIN</b><br/>1]
> N---O
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `nums = [1, 3, 5, 2, 4, 8, 2, 2]`  
> *Output:* `1`  
> *Explanation:* The following arrays are the results of applying the algorithm repeatedly.
> - First: `nums = [1, 5, 4, 2]`
> - Second: `nums = [1, 4]`
> - Third: `nums = [1]`
> 
> `1` is the last remaining number, so we return `1`.

#### Example 2:
> *Input:* `nums = [3]`  
> *Output:* `3`  
> *Explanation:* `3` is already the last remaining number, so we return `3`.

#### Constraints:
- `1 <= nums.length <= 1024`
- `1 <= nums[i] <= 10^9`
- `nums.length` is a power of `2`.


