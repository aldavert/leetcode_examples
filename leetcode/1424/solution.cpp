#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> findDiagonalOrder(std::vector<std::vector<int> > nums)
{
    const int n = static_cast<int>(nums.size());
    std::queue<std::tuple<int, int> > q({{0, 0}});
    std::vector<int> result;
    while (!q.empty())
    {
        auto [row, col] = q.front();
        q.pop();
        result.push_back(nums[row][col]);
        if ((col == 0) && (row + 1 < n))
            q.push({row + 1, col});
        if (col + 1 < static_cast<int>(nums[row].size()))
            q.push({row, col + 1});
    }
    return result;
}
#else
std::vector<int> findDiagonalOrder(std::vector<std::vector<int> > nums)
{
    struct Info
    {
        int diagonal;
        int order;
        int value;
        bool operator<(const Info &other) const
        {
            return  (diagonal <  other.diagonal)
                || ((diagonal == other.diagonal) && (order < other.order));
        }
    };
    std::vector<Info> values;
    for (int row = 0, n = static_cast<int>(nums.size()); row < n; ++row)
        for (int col = 0, m = static_cast<int>(nums[row].size()); col < m; ++col)
            values.push_back({row + col, col, nums[row][col]});
    std::sort(values.begin(), values.end());
    std::vector<int> result;
    for (auto [_d, _o, value] : values)
        result.push_back(value);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > nums,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findDiagonalOrder(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, {1, 4, 2, 7, 5, 3, 8, 6, 9}, trials);
    test({{1, 2, 3, 4, 5}, {6, 7}, {8}, {9, 10, 11}, {12, 13, 14, 15, 16}},
          {1, 6, 2, 8, 7, 3, 9, 4, 12, 10, 5, 13, 11, 14, 15, 16}, trials);
    return 0;
}


