#include "../common/common.hpp"

// ############################################################################
// ############################################################################

double average(std::vector<int> salary)
{
    int minimum = salary[0], maximum = salary[0];
    double result = 0.0;
    for (int s : salary)
    {
        minimum = std::min(minimum, s);
        maximum = std::max(maximum, s);
        result += static_cast<double>(s);
    }
    return (result - maximum - minimum) / static_cast<double>(salary.size() - 2);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> salary, double solution, unsigned int trials = 1)
{
    double result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = average(salary);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4000, 3000, 1000, 2000}, 2500.00000, trials);
    test({1000, 2000, 3000}, 2000.00000, trials);
    return 0;
}


