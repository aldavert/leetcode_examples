#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

#if 1
int minimumIncompatibility(std::vector<int> nums, int k)
{
    constexpr int inf = 0x3f3f3f3f;
    int n = static_cast<int>(nums.size()), sz = n / k;
    std::sort(nums.begin(), nums.end());
    int dp[1 << 16][16];
    std::memset(dp, inf, sizeof(dp));
    for (int i = 0; i < n; ++i) dp[0][i] = 0;
    for (int m = 0; m < (1 << n) - 1; ++m)
    {
        int cnt = __builtin_popcount(m);
        for (int i = 0; i < n; ++i)
        {
            if (dp[m][i] == inf) continue;
            if (cnt % sz == 0)
            {
                int j = __builtin_ffs(~m) - 1;
                dp[m | (1 << j)][j] = std::min(dp[m | (1 << j)][j], dp[m][i]);
            }
            else
            {
                for (int j = i + 1; j < n; ++j)
                {
                    if ((m & (1 << j)) || (nums[j] == nums[i])) continue;
                    dp[m | (1 << j)][j] = std::min(dp[m|(1 << j)][j],
                                                   dp[m][i] + nums[j] - nums[i]);
                }
            }
        }
    }
    int result = inf;
    for (int i = 0; i < n; ++i)
        result = std::min(result, dp[(1 << n) - 1][i]);
    return (result == inf)?-1:result;
}
#else
int minimumIncompatibility(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size()), c = n / k;
    int dp[1 << 16][16];
    std::memset(dp, 0x7f, sizeof(dp));
    for (int i = 0; i < n; ++i) dp[1 << i][i] = 0;
    for (int s = 0; s < 1 << n; ++s)
    {
        for (int i = 0; i < n; ++i)
        {
            if ((s & (1 << i)) == 0) continue;
            for (int j = 0; j < n; ++j)
            {
                if (s & (1 << j)) continue;
                const int t = s | (1 << j);
                if (__builtin_popcount(s) % c == 0)
                    dp[t][j] = std::min(dp[t][j], dp[s][i]);
                else if (nums[j] > nums[i])
                    dp[t][j] = std::min(dp[t][j], dp[s][i] + nums[j] - nums[i]);
            }
        }
    }
    int result = *std::min_element(std::begin(dp[(1 << n) - 1]),
                                     std::end(dp[(1 << n) - 1]));
    return (result > 1'000'000'009)?-1:result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumIncompatibility(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1, 4}, 2, 4, trials);
    test({6, 3, 8, 1, 3, 1, 2, 2}, 4, 6, trials);
    test({5, 3, 3, 6, 3, 3}, 3, -1, trials);
    return 0;
}


