#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * trimBST(TreeNode * root, int low, int high)
{
    if (!root) return root;
    if (root->val < low)
    {
        TreeNode * next = root->right;
        root->right = nullptr;
        delete root;
        return trimBST(next, low, high);
    }
    if (root->val > high)
    {
        TreeNode * next = root->left;
        root->left = nullptr;
        delete root;
        return trimBST(next, low, high);
    }
    TreeNode * search, * previous;
    // -----------------------------------------------
    search = root;
    previous = nullptr;
    while (search && (search->val >= low))
    {
        previous = search;
        search = search->left;
    }
    if (search)
    {
        TreeNode * previous_clean = nullptr;
        while (search && (search->val < low))
        {
            previous_clean = search;
            search = search->right;
        }
        if (previous_clean) previous_clean->right = nullptr;
        delete previous->left;
        previous->left = trimBST(search, low, high);
    }
    // -----------------------------------------------
    search = root;
    previous = nullptr;
    while (search && (search->val <= high))
    {
        previous = search;
        search = search->right;
    }
    if (search)
    {
        TreeNode * previous_clean = nullptr;
        while (search && (search->val > high))
        {
            previous_clean = search;
            search = search->left;
        }
        if (previous_clean) previous_clean->left = nullptr;
        delete previous->right;
        previous->right = trimBST(search, low, high);
    }
    return root;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          int low,
          int high,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        root = trimBST(root, low, high);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 2}, 1, 2, {1, null, 2}, trials);
    test({3, 0, 4, null, 2, null, null, 1}, 1, 3, {3, 2, null, 1}, trials);
    test({1, null, 2}, 2, 4, {2}, trials);
    test({3}, 2, 2, {}, trials);
    test({3, 1, 4, null, 2}, 2, 12, {3, 2, 4}, trials);
    test({28, 12, 45, 4, 24, 35, 47, 2, 9, 14, 25, 31, 42, 46, 48, 0, 3, 8, 11,
          13, 20, null, 26, 30, 33, 41, 43, null, null, null, 49, null, 1, null,
          null, 7, null, 10, null, null, null, 17, 22, null, 27, 29, null, 32,
          34, 36, null, null, 44, null, null, null, null, 6, null, null, null,
          16, 18, 21, 23, null, null, null, null, null, null, null, null, null,
          37, null, null, 5, null, 15, null, null, 19, null, null, null, null,
          null, 40, null, null, null, null, null, null, 39, null, 38},
          12,  22,
          {12, null, 14, 13, 20, null, null, 17, 22, 16, 18, 21, null, 15, null,
          null, 19},
          trials);
    return 0;
}


