#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool doesAliceWin(std::string s)
{
    auto isVowel = [](char c) -> bool
    {
        static constexpr std::string_view VOWELS = "aeiou";
        return VOWELS.find(c) != std::string_view::npos;
    };
    return std::any_of(s.begin(), s.end(), [=](char c) { return isVowel(c); });
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = doesAliceWin(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leetcoder", true, trials);
    test("bbcd", false, trials);
    return 0;
}


