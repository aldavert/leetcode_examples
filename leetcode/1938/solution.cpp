#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> maxGeneticDifference(std::vector<int> parents,
                                      std::vector<std::vector<int> > queries)
{
    struct TrieNode
    {
        TrieNode * next[2] = {};
        int count = 0;
    };
    const int n = static_cast<int>(parents.size()),
              m = static_cast<int>(queries.size());
    int root_index = 0;
    std::vector<std::vector<int> > graph(n);
    std::unordered_map<int, std::vector<int> > node_to_index;
    TrieNode root;
    std::vector<int> result(m);
    
    for (int i = 0; i < n; ++i)
    {
        if (parents[i] != -1) graph[parents[i]].push_back(i);
        else root_index = i;
    }
    for (int i = 0; i < m; ++i)
        node_to_index[queries[i][0]].push_back(i);
    auto getAnswer = [&](TrieNode *node, int q)
    {
        int answer = 0;
        for (int i = 31; i >= 0; --i)
        {
            if (int b = q >> i & 1; node->next[1 - b] && node->next[1 - b]->count)
                node = node->next[1 - b],
                answer |= 1 << i;
            else node = node->next[b];
        }
        return answer;
    };
    auto dfs = [&](auto &&self, int u) -> void
    {
        auto node = &root;
        for (int i = 31; i >= 0; --i)
        {
            int b = u >> i & 1;
            if (!node->next[b]) node->next[b] = new TrieNode();
            node = node->next[b];
            ++node->count;
        }
        if (auto search = node_to_index.find(u); search != node_to_index.end())
            for (int index : search->second)
                result[index] = getAnswer(&root, queries[index][1]);
        for (int v : graph[u]) self(self, v);
        node = &root;
        for (int i = 31; i >= 0; --i)
        {
            int b = u >> i & 1;
            node = node->next[b];
            --node->count;
        }
    };
    dfs(dfs, root_index);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> parents,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxGeneticDifference(parents, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, 0, 1, 1}, {{0, 2}, {3, 2}, {2, 5}}, {2, 3, 7}, trials);
    test({3, 7, -1, 2, 0, 7, 0, 2}, {{4, 6}, {1, 15}, {0, 5}}, {6, 14, 7}, trials);
    return 0;
}


