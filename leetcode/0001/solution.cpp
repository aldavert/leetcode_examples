#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> twoSum(std::vector<int> nums, int target)
{
    std::unordered_map<int, int> possible_values;
    const int n = static_cast<int>(nums.size());
    for (int i = 0; i < n; ++i)
    {
        auto it = possible_values.find(target - nums[i]);
        if (it != possible_values.end())
            return {it->second, i};
        possible_values[nums[i]] = i;
    }
    return {};
}
#else
std::vector<int> twoSum(std::vector<int> nums, int target)
{
    const int n = static_cast<int>(nums.size());
    for (int i = 0; i != n; ++i)
        for (int j = 0; j != n; ++j)
            if ((i != j) && (nums[i] + nums[j] == target))
                return {i, j};
    return {};
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int target,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
        result = twoSum(nums, target);
    const bool correct = (result.size() == 2)
                      && (((result[0] == solution[0]) && (result[1] == solution[1]))
                      || ((result[0] == solution[1]) && (result[1] == solution[0])));
    showResult(correct, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 7, 11, 15}, 9, {0, 1}, trials);
    test({3, 2, 4}, 6, {1, 2}, trials);
    test({3, 3}, 6, {1, 0}, trials);
    return 0;
}


