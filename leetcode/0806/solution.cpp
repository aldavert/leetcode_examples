#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> numberOfLines(std::vector<int> widths, std::string s)
{
    int pixels = 0, lines = 1;
    for (char c : s)
    {
        const int current = widths[c - 'a'];
        if (pixels + current > 100)
        {
            ++lines;
            pixels = 0;
        }
        pixels += current;
    }
    return {lines, pixels};
}

// ############################################################################
// ############################################################################

void test(std::vector<int> width,
          std::string s,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfLines(width, s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
          10, 10, 10, 10, 10, 10, 10, 10, 10}, "abcdefghijklmnopqrstuvwxyz",
          {3, 60}, trials);
    test({4, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
          10, 10, 10, 10, 10, 10, 10, 10}, "bbbcccdddaaa", {2, 4}, trials);
    return 0;
}


