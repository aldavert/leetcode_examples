#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countServers(std::vector<std::vector<int> > grid)
{
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    int result = 0;
    std::vector<int> rows(m), cols(n);
    
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            rows[i] += grid[i][j] == 1,
            cols[j] += grid[i][j] == 1;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            result += ((grid[i][j] == 1) && ((rows[i] > 1) || (cols[j] > 1)));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countServers(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0}, {0, 1}}, 0, trials);
    test({{1, 0}, {1, 1}}, 3, trials);
    test({{1, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}}, 4, trials);
    return 0;
}


