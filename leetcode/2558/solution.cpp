#include "../common/common.hpp"
#include <queue>
#include <cmath>

// ############################################################################
// ############################################################################

long long pickGifts(std::vector<int> gifts, int k)
{
    std::priority_queue<int> heap;
    long long result = 0;
    for (int gift : gifts)
    {
        heap.push(gift);
        result += gift;
    }
    for (int i = 0; (i < k) && (heap.top() > 1); ++i)
    {
        int current = heap.top();
        heap.pop();
        int remain = static_cast<int>(std::sqrt(current));
        result -= current - remain;
        heap.push(remain);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> gifts, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = pickGifts(gifts, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({25, 64, 9, 4, 100}, 4, 29, trials);
    test({1, 1, 1, 1}, 4, 4, trials);
    return 0;
}


