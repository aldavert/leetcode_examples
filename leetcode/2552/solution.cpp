#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
long long countQuadruplets(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    long result = 0;
    int dp[4001] = {};
    
    for (int k = 2; k < n; ++k)
    {
        for (int j = 0, numbers_less_than_k = 0; j < k; ++j)
        {
            if (nums[j] < nums[k])
            {
                ++numbers_less_than_k;
                result += dp[j];
            }
            else if (nums[j] > nums[k])
                dp[j] += numbers_less_than_k;
        }
    }
    return result;
}
#else
long long countQuadruplets(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    long result = 0;
    std::vector<int> dp(n);
    
    for (int k = 2; k < n; ++k)
    {
        for (int j = 0, numbers_less_than_k = 0; j < k; ++j)
        {
            if (nums[j] < nums[k])
            {
                ++numbers_less_than_k;
                result += dp[j];
            }
            else if (nums[j] > nums[k])
                dp[j] += numbers_less_than_k;
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countQuadruplets(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 4, 5}, 2, trials);
    test({1, 2, 3, 4}, 0, trials);
    return 0;
}


