#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool digitCount(std::string num)
{
    int histogram[10] = {};
    for (char letter : num) ++histogram[letter - '0'];
    for (size_t i = 0, n = num.size(); i < n; ++i)
        if (histogram[i] != num[i] - '0') return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string num, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = digitCount(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1210", true, trials);
    test("030", false, trials);
    return 0;
}


