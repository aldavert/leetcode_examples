#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

int numWays(int steps, int arrLen)
{
    constexpr int MOD = 1'000'000'007;
    const int n = std::min(arrLen, steps / 2 + 1);
    int dp_table[252] = {};
    dp_table[0] = 1;
    while (steps--)
    {
        int new_table[252] = {};
        for (int i = 0; i < n; ++i)
        {
            long current = dp_table[i];
            if (i - 1 >= 0) current += dp_table[i - 1];
            if (i + 1 <  n) current += dp_table[i + 1];
            new_table[i] = static_cast<int>(current % MOD);
        }
        std::memcpy(dp_table, new_table, sizeof(new_table));
    }
    return dp_table[0];
}

// ############################################################################
// ############################################################################

void test(int steps, int arrLen, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numWays(steps, arrLen);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 2, 4, trials);
    test(2, 4, 2, trials);
    test(4, 2, 8, trials);
    return 0;
}


