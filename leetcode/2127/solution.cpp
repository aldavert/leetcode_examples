#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 0
int maximumInvitations(std::vector<int> favorite)
{
    const int n = static_cast<int>(favorite.size());
    int in_degree[100'001] = {}, chain[100'001] = {};
    int count = 0, result = 0;
    std::queue<int> q;
    
    for (int it : favorite)
        ++in_degree[it];
    for (int i = 0; i < n; ++i)
        if (in_degree[i] == 0)
            q.push(i);
    while (!q.empty())
    {
        int v = q.front();
        q.pop();
        ++chain[v];
        int w = favorite[v];
        chain[w] = std::max(chain[w], chain[v]);
        if (--in_degree[w] == 0) q.push(w);
    }
    for (int i = 0; i < n; ++i)
    {
        int w = 0;
        if (in_degree[i] == 0) continue;
        for (; in_degree[i] != 0; i = favorite[i], ++w)
            in_degree[i] = 0;
        if (w == 2)
            count += chain[i] + chain[favorite[i]] + 2;
        else result = std::max(result, w);
    }
    return std::max(result, count);
}
#else
int maximumInvitations(std::vector<int> favorite)
{
    enum class State { Init, Visiting, Visited };
    const int n = static_cast<int>(favorite.size());
    std::vector<int> in_degree(n), max_chain_length(n, 1), parent(n, -1);
    int sum_components_length = 0, max_cycle_length = 0;
    std::vector<std::vector<int> > graph(n);
    std::vector<State> states(n);
    std::vector<bool> seen(n);
    std::queue<int> q;
    
    auto findCycle = [&](auto &&self, int u) -> void
    {
        seen[u] = true;
        states[u] = State::Visiting;
        
        for (const int v : graph[u])
        {
            if (!seen[v])
            {
                parent[v] = u;
                self(self, v);
            }
            else if (states[v] == State::Visiting)
            {
                int curr = u, cycle_length = 1;
                while (curr != v)
                {
                    curr = parent[curr];
                    ++cycle_length;
                }
                max_cycle_length = std::max(max_cycle_length, cycle_length);
            }
        }
        states[u] = State::Visited;
    };
    
    for (int i = 0; i < n; ++i)
    {
        graph[i].push_back(favorite[i]);
        ++in_degree[favorite[i]];
    }
    for (int i = 0; i < n; ++i)
        if (in_degree[i] == 0)
            q.push(i);
    while (!q.empty())
    {
        int u = q.front();
        q.pop();
        for (int v : graph[u])
        {
            if (--in_degree[v] == 0)
            q.push(v);
            max_chain_length[v] = std::max(max_chain_length[v], 1 + max_chain_length[u]);
        }
    }
    for (int i = 0; i < n; ++i)
        if (favorite[favorite[i]] == i)
            sum_components_length += max_chain_length[i] + max_chain_length[favorite[i]];
    
    for (int i = 0; i < n; ++i)
        if (!seen[i]) findCycle(findCycle, i);
    return std::max(sum_components_length / 2, max_cycle_length);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> favorite, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumInvitations(favorite);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 2, 1, 2}, 3, trials);
    test({1, 2, 0}, 3, trials);
    test({3, 0, 1, 4, 1}, 4, trials);
    return 0;
}


