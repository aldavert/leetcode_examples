#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numTeams(std::vector<int> rating)
{
    int result = 0;
    
    for (int i = 1, n = static_cast<int>(rating.size()); i < n - 1; ++i)
    {
        int left_less = 0, left_greater = 0;
        for (int j = 0; j < i; ++j)
        {
            if (rating[j] < rating[i])
                ++left_less;
            else if (rating[j] > rating[i])
                ++left_greater;
        }
        int right_less = 0, right_greater = 0;
        for (int j = i + 1; j < n; ++j)
        {
            if (rating[j] < rating[i])
                ++right_less;
            else if (rating[j] > rating[i])
                ++right_greater;
        }
        result += left_less * right_greater + left_greater * right_less;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> rating, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numTeams(rating);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 5, 3, 4, 1}, 3, trials);
    test({2, 1, 3}, 0, trials);
    test({1, 2, 3, 4}, 4, trials);
    return 0;
}


