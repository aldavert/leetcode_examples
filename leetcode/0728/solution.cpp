#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<int> selfDividingNumbers(int left, int right)
{
    std::vector<int> result;
    for (int i = left; i <= right; ++i)
    {
        int current = i;
        for (; current; current /= 10)
            if (int digit = current % 10; (digit == 0) || (i % digit != 0))
                break;
        if (!current)
        result.push_back(i);
    }
    return result;
}
#else
std::vector<int> selfDividingNumbers(int left, int right)
{
    std::vector<int> result;
    auto valid = [](int value)
    {
        for (int current = value; current; current /= 10)
            if (int digit = current % 10; (digit == 0) || (value % digit != 0))
                return false;
        return true;
    };
    for (int i = left; i <= right; ++i)
        if (valid(i))
            result.push_back(i);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int left, int right, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = selfDividingNumbers(left, right);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 22, {1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15, 22}, trials);
    test(47, 85, {48, 55, 66, 77}, trials);
    return 0;
}


