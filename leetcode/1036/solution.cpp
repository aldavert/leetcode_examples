#include "../common/common.hpp"
#include <bitset>
#include <queue>

// ############################################################################
// ############################################################################

#if 1
bool isEscapePossible(std::vector<std::vector<int> > blocked,
                      std::vector<int> source,
                      std::vector<int> target)
{
    std::bitset<512> map[512];
    auto buildMap = [&](int x, int y) -> void
    {
        int cy = 256 - y, cx = 256 - x;
        for (size_t r = 0; r < 512; ++r) map[r].reset();
        if ((cy - 1 >= 0) && (cy - 1 < 512))
            map[cy - 1].set();
        if ((cy + 999'999 >= 0) && (cy + 999'999 < 512))
            map[cy + 999'999].set();
        if ((cx - 1 >= 0) && (cx - 1 < 512))
            for (int iy = 0, rx = cx - 1; iy < 512; ++iy)
                map[iy][rx] = true;
        if ((cx + 999'999 >= 0) && (cx + 999'999 < 512))
            for (int iy = 0, rx = cx + 999'999; iy < 512; ++iy)
                map[iy][rx] = true;
        for (const auto &block : blocked)
            if ((std::abs(block[0] - x) <= 200) && (std::abs(block[1] - y) <= 200))
                map[cy + block[1]][cx + block[0]] = true;
    };
    auto noExit = [&](int sx, int sy, int ex, int ey) -> bool
    {
        const int d = std::abs(ex - sx) + std::abs(ey - sy);
        buildMap(sx, sy);
        std::queue<std::tuple<int, int> > q({{256, 256}});
        ey += 256;
        ex += 256;
        for (int i = 0; (!q.empty()) && (i <= 200); ++i)
        {
            for (size_t j = 0, n = q.size(); j < n; ++j)
            {
                auto [x, y] = q.front();
                q.pop();
                if ((y == ey) && (x == ex)) return false;
                if (map[y][x]) continue;
                map[y][x] = true;
                if ((x < 50) || (y < 50) || (x > 460) || (y > 460)) return false;
                if (!map[y][x - 1]) q.push({x - 1, y});
                if (!map[y][x + 1]) q.push({x + 1, y});
                if (!map[y - 1][x]) q.push({x, y - 1});
                if (!map[y + 1][x]) q.push({x, y + 1});
            }
        }
        return q.empty() || (d < 200);
    };
    if (noExit(source[0], source[1], target[0], target[1])) return false;
    return !noExit(target[0], target[1], source[0], source[1]);
}
#else
bool isEscapePossible(std::vector<std::vector<int> > blocked,
                      std::vector<int> source,
                      std::vector<int> target)
{
    auto hash = [](int i, int j) { return (static_cast<unsigned long>(i) << 32) + j; };
    std::unordered_set<long> blocked_set;
    std::unordered_set<long> visited;
    for (const auto &b : blocked)
        blocked_set.insert(hash(b[0], b[1]));
    auto dfs = [&](auto &&self, int i, int j, long t) -> bool
    {
        long id = hash(i, j);
        if ((i < 0) || (i >= 999'999) || (j < 0) || (j >= 999'999)
        || blocked_set.count(id) || visited.count(id))
            return false;
        visited.insert(id);
        if ((static_cast<int>(visited.size()) > (1 + 199) * 199 / 2)
        ||  (id == t))
            return true;
        return self(self, i + 1, j, t) ||
               self(self, i - 1, j, t) ||
               self(self, i, j + 1, t) ||
               self(self, i, j - 1, t);
    };
    bool result = dfs(dfs, source[0], source[1], hash(target[0], target[1]));
    visited.clear();
    return result && dfs(dfs, target[0], target[1], hash(source[0], source[1]));
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > blocked,
          std::vector<int> source,
          std::vector<int> target,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isEscapePossible(blocked, source, target);
    showResult(solution == result, solution, result);
}

void testMap(std::vector<std::string> text_map, bool solution, unsigned int trials)
{
    const int n_rows = static_cast<int>(text_map.size());
    const int n_cols = static_cast<int>(text_map[0].size());
    std::vector<std::vector<int> > blocked;
    std::vector<int> source(2), target(2);
    for (int r = 0; r < n_rows; ++r)
    {
        for (int c = 0; c < n_cols; ++c)
        {
            if (text_map[r][c] == 'X') blocked.push_back({r, c});
            if (text_map[r][c] == 's') source = {r, c};
            if (text_map[r][c] == 'e') target = {r, c};
        }
    }
    test(blocked, source, target, solution, trials);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 10'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {1, 0}}, {0, 0}, {0, 2}, false, trials);
    test({}, {0, 0}, {999999, 999999}, true, trials);
    testMap({"sX...X...XXX...X...X......",
             ".XXX...X...X.X.X.X..X..X..",
             "....X...X..X.X.X.XX..X.X..",
             ".XX..X...X.X.X.X.X.X..XX..",
             ".X.X..XX.X.X.X.X.X..X..X..",
             ".X..X..X.X.X.X.X.X...X....",
             ".XX..X...X.X.X.X.X.X..X...",
             "......XXXX...X...X.XX....e"}, true, trials);
    testMap({"s...........X.............",
             "............X.............",
             "............X.............",
             "XXXXXXXXXXXXXXXXXXXXXXXXXX",
             "............X.............",
             "............X.............",
             "............X.............",
             "............X............e"}, false, trials);
    testMap({"sX...X...XXX...X...X......",
             ".XXX...X...X.X...X..X..X..",
             "....X...X..X.X.X.XX..X.X..",
             ".XX..X...X.X.X...X.X..XX..",
             ".X.X..XX.X.X.X.X.X..X..X..",
             ".X..X..X.X.X.X...X...X.X..",
             ".XX..X...X...X.X.X.X..X...",
             "X.....XXXXXXXXX.XX.XX....e"}, false, trials);
    test({{0, 3}, {1, 0}, {1, 1}, {1, 2}, {1, 3}}, {0, 0}, {0, 2}, true, trials);
    testMap({"sX..",
             ".X..",
             "eX..",
             "XX.."}, true, trials);
    testMap({"s.eX",
             "XXXX",
             "....",
             "...."}, true, trials);
    test({{10, 9}, {9, 10}, {10, 11}, {11, 10}}, {0, 0}, {10, 10}, false, trials);
    testMap({"s...",
             "..X.",
             ".XeX",
             "..X."}, false, trials);
    test({{0, 999991}, {0, 999993}, {0, 999996}, {1, 999996}, {1, 999997},
          {1, 999998}, {1, 999999}}, {0, 999997}, {0, 2}, false, trials);
    testMap({"s...X.eX",
             ".....XX."}, false, trials);
    return 0;
}


