#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> vowelStrings(std::vector<std::string> words,
                              std::vector<std::vector<int> > queries)
{
    static constexpr std::string_view VOWELS = "aeiou";
    std::vector<int> result, prefix(words.size() + 1);
    for (int i = 0, n = static_cast<int>(words.size()); i < n; ++i)
        prefix[i + 1] += prefix[i] 
                      + ((VOWELS.find(words[i].front()) != std::string_view::npos)
                      && (VOWELS.find(words[i].back()) != std::string_view::npos));
    for (const auto &query : queries)
        result.push_back(prefix[query[1] + 1] - prefix[query[0]]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = vowelStrings(words, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"aba", "bcb", "ece", "aa", "e"}, {{0, 2}, {1, 4}, {1, 1}}, {2, 3, 0}, trials);
    test({"a", "e", "i"}, {{0, 2}, {0, 1}, {2, 2}}, {3, 2, 1}, trials);
    return 0;
}


