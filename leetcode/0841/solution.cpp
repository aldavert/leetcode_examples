#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

bool canVisitAllRooms(std::vector<std::vector<int> > rooms)
{
    std::vector<bool> visited(rooms.size(), false);
    std::queue<int> q;
    visited[0] = true;
    q.push(0);
    size_t number_rooms_visited = 1;
    while (!q.empty())
    {
        int room = q.front();
        q.pop();
        for (int key : rooms[room])
        {
            if (!visited[key])
            {
                q.push(key);
                visited[key] = true;
                ++number_rooms_visited;
            }
        }
    }
    return number_rooms_visited == rooms.size();
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > rooms, int solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canVisitAllRooms(rooms);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1}, {2}, {3}, {}}, true, trials);
    test({{1, 3}, {3, 0, 1}, {2}, {0}}, false, trials);
    return 0;
}


