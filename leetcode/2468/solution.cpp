#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<std::string> splitMessage(std::string message, int limit)
{
    const int n = static_cast<int>(message.size());
    if ((n > 0) && (limit <= 5)) return {};
    int parts = 0, count = 0;
    for (int len = 1, max_len = static_cast<int>(std::to_string(n).size());
         len <= max_len; ++len)
    {
        parts = count = 0;
        for (int tag = len + 4, m = 9, j = 0; (j < len) && (limit > tag); ++j)
        {
            if (count + m * (limit - tag) >= n)
            {
                parts += (n - count) / (limit - tag)
                      +  ((n - count) % (limit - tag) != 0);
                count = n;
                break;
            }
            else
            {
                parts += m;
                count += m * (limit - tag);
            }
            m *= 10;
            tag += 1;
        }
        if (count >= n) break;
    }
    if (count < n) return {};
    
    std::vector<std::string> result(parts);
    std::string suffix = std::string("/") + std::to_string(parts) + std::string(">");
    for (int i = 0, tag = 2 + static_cast<int>(suffix.size()), j = 0; i < parts; ++i)
    {
        if ((i == 9) || (i == 99) || (i == 999) || (i == 9999)) tag += 1;
        result[i] = message.substr(j, limit - tag)
                  + "<" + std::to_string(i + 1) + suffix;
        j += limit - tag;
    }
    return result;
}
#else
std::vector<std::string> splitMessage(std::string message, int limit)
{
    const int n = static_cast<int>(message.size());
    auto numberDigits = [](int number) -> int
    {
        return static_cast<int>(std::to_string(number).size());
    };
    
    int b = 1;
    for (int a_length = 1; b * limit < b * (numberDigits(b) + 3) + a_length + n; )
    {
        if (numberDigits(b) * 2 + 3 >= limit)
            return {};
        a_length += numberDigits(++b);
    }
    
    std::vector<std::string> result;
    for (int i = 0, a = 1; a <= b; ++a)
    {
        int j = limit - (numberDigits(a) + numberDigits(b) + 3);
        result.push_back(message.substr(i, j)
                       + "<" + std::to_string(a) + "/" + std::to_string(b) + ">");
        i += j;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string message,
          int limit,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = splitMessage(message, limit);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("this is really a very awesome message", 9,
         {"thi<1/14>", "s i<2/14>", "s r<3/14>", "eal<4/14>", "ly <5/14>",
          "a v<6/14>", "ery<7/14>", " aw<8/14>", "eso<9/14>", "me<10/14>",
          " m<11/14>", "es<12/14>", "sa<13/14>", "ge<14/14>"}, trials);
    test("short message", 15, {"short mess<1/2>", "age<2/2>"}, trials);
    return 0;
}


