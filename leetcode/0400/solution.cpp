#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findNthDigit(int n)
{
    long n_digits = 1, begin = 0, end = 9, d_begin = 0, d_end = 9;
    for (long value = 10; (d_end < n) && (n_digits < 10); ++n_digits, value *= 10)
    {
        begin = end + 1;
        end = begin + 9 * value - 1;
        d_begin = d_end + 1;
        d_end = d_begin + (9 * value) * (n_digits + 1) - 1;
    }
    const long index = (n - d_begin) % n_digits;
    return std::to_string(begin + (n - d_begin) / n_digits)[index] - '0';
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findNthDigit(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 3, trials);
    test(11, 0, trials);
    test(345, 1, trials);
    test(34563, 9, trials);
    test(3456324, 4, trials);
    test(993456324, 4, trials);
    test(9956324, 1, trials);
    test(7858324, 4, trials);
    test(7858325, 7, trials);
    test(7858326, 1, trials);
    test(7858327, 2, trials);
    return 0;
}


