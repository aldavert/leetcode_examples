#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findKthNumber(int m, int n, int k)
{
    auto lowerNum = [&](int target) -> int
    {
        
        int res = 0;
        for (int row = m - 1, col = 0; (row >= 0) && (col < n); )
        {
            if ((row + 1) * (col + 1) > target) --row;
            else
            {
                res += row + 1;
                ++col;
            }
        }
        return res;
    };
    int low = 1;
    for (int high = m * n; low <= high; )
    {
        if (int mid = (high + low) / 2; lowerNum(mid) < k) low = mid + 1;
        else high = mid - 1;
    }
    return low;
}

// ############################################################################
// ############################################################################

void test(int m, int n, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findKthNumber(m, n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 3, 5, 3, trials);
    test(2, 3, 6, 6, trials);
    return 0;
}


