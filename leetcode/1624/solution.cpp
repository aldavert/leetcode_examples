#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

int maxLengthBetweenEqualCharacters(std::string s)
{
    const int n = static_cast<int>(s.size());
    int lut[26][2] = {};
    std::memset(lut, -1, sizeof(lut));
    for (int i = 0; i < n; ++i)
    {
        int idx = static_cast<int>(s[i] - 'a');
        if (lut[idx][0] == -1) lut[idx][0] = i;
        else lut[idx][1] = i;
    }
    int result = -1;
    for (int i = 0; i < 26; ++i)
        result = std::max(result, lut[i][1] - lut[i][0] - 1);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxLengthBetweenEqualCharacters(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aa", 0, trials);
    test("abca", 2, trials);
    test("cbzxy", -1, trials);
    return 0;
}


