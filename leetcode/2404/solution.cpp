#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int mostFrequentEven(std::vector<int> nums)
{
    int histogram[100'001] = {};
    int min = 100'001, max = 0;
    for (int number : nums)
    {
        if ((number & 1) == 0)
        {
            ++histogram[number];
            min = std::min(min, number);
            max = std::max(max, number);
        }
    }
    if (max < min) return -1;
    int max_frequency = min, max_value = histogram[min];
    for (int i = min + 2; i <= max; i += 2)
        if (histogram[i] > max_value)
            max_value = histogram[i],
            max_frequency = i;
    return max_frequency;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = mostFrequentEven(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 2, 2, 4, 4, 1}, 2, trials);
    test({4, 4, 4, 9, 2, 4}, 4, trials);
    test({29, 47, 21, 41, 13, 37, 25, 7}, -1, trials);
    return 0;
}


