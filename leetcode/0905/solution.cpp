#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> sortArrayByParity(std::vector<int> nums)
{
    for (size_t begin = 0, last = nums.size(); begin < last;)
    {
        if (nums[begin] & 1)
            std::swap(nums[begin], nums[--last]);
        else ++begin;
    }
    return nums;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    int histogram[5002] = {};
    int left_even = 0;
    for (int l : left)
    {
        if ((l >= 0) && (l <= 5000)) ++histogram[5001];
        else ++histogram[l];
        if ((l & 1) == 0) ++left_even;
    }
    for (int r : right)
    {
        if (left_even)
        {
            if (r & 1) return false;
            --left_even;
        }
        else
        {
            if ((r & 1) == 0) return false;
        }
        if ((r >= 0) && (r <= 5000))
        {
            if (histogram[5001]) --histogram[5001];
            else return false;
        }
        else
        {
            if (histogram[r]) --histogram[r];
            else return false;
        }
    }
    return true;
}

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sortArrayByParity(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1, 2, 4}, {2, 4, 3, 1}, trials);
    test({0}, {0}, trials);
    return 0;
}


