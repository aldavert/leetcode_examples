#include "../common/common.hpp"
#include "../common/list.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

ListNode * sortList(ListNode * head)
{
    if (!head) return head;
    auto radixSort = [](ListNode * first, int maximum) -> ListNode *
    {
        int total_blocks = static_cast<int>(std::floor(std::log2(maximum) / 3.0) + 1);
        ListNode * bucket_head[8], * bucket_tail[8];
        for (int i = 0; i < 8; ++i)
            bucket_head[i] = bucket_tail[i] = nullptr;
        for (int block = 0; block < total_blocks; ++block)
        {
            ListNode * current = first;
            while (current)
            {
                ListNode * next = current->next;
                current->next = nullptr;
                int idx = (current->val >> (block * 3)) & 7;
                if (bucket_tail[idx])
                {
                    bucket_tail[idx]->next = current;
                    bucket_tail[idx] = current;
                }
                else bucket_tail[idx] = bucket_head[idx] = current;
                current = next;
            }
            current = first = nullptr;
            for (int idx = 0; idx < 8; ++idx)
            {
                while (bucket_head[idx])
                {
                    if (!first)
                        first = current = bucket_head[idx];
                    else
                    {
                        current->next = bucket_head[idx];
                        current = current->next;
                    }
                    bucket_head[idx] = bucket_head[idx]->next;
                }
                bucket_tail[idx] = nullptr;
            }
        }
        return first;
    };
    ListNode * positive = nullptr, * negative = nullptr;
    int max_positive = 0, max_negative = 0;
    for (ListNode * current = head, * positive_tail = positive, * negative_tail = negative; current;)
    {
        ListNode * next = current->next;
        current->next = nullptr;
        if (current->val < 0)
        {
            current->val = -current->val;
            max_negative = std::max(max_negative, current->val);
            if (negative_tail)
            {
                negative_tail->next = current;
                negative_tail = current;
            }
            else negative = negative_tail = current;
        }
        else
        {
            max_positive = std::max(max_positive, current->val);
            if (positive_tail)
            {
                positive_tail->next = current;
                positive_tail = current;
            }
            else positive = positive_tail = current;
        }
        current = next;
    }
    if (negative)
        negative = radixSort(negative, max_negative);
    if (positive)
        positive = radixSort(positive, max_positive);
    head = positive;
    while (negative)
    {
        ListNode * next = negative->next;
        negative->val = -negative->val;
        negative->next = head;
        head = negative;
        negative = next;
    }
    return head;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> input, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * root = vec2list(input);
        root = sortList(root);
        result = list2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 1, 3}, {1, 2, 3, 4}, trials);
    test({-1, 5, 3, 4, 0}, {-1, 0, 3, 4, 5}, trials);
    test({5, 3, -1, -4, 0}, {-4, -1, 0, 3, 5}, trials);
    test({5, 3, -1, -4, 0, -8, 8}, {-8, -4, -1, 0, 3, 5, 8}, trials);
    test({}, {}, trials);
    return 0;
}


