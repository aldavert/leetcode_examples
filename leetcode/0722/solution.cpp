#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> removeComments(std::vector<std::string> source)
{
    std::vector<std::string> result;
    bool block_comment = false;
    std::string current;
    for (auto line : source)
    {
        for (char previous = '\0'; char symbol : line)
        {
            if (block_comment)
            {
                if ((previous == '*') && (symbol == '/'))
                {
                    block_comment = false;
                    symbol = '\0';
                }
            }
            else
            {
                if ((previous == '/') && (symbol == '/'))
                {
                    current.pop_back();
                    break;
                }
                else if ((previous == '/') && (symbol == '*'))
                {
                    block_comment = true;
                    symbol = '\0';
                    current.pop_back();
                }
                else current.push_back(symbol);
            }
            previous = symbol;
        }
        if (block_comment) continue;
        if (!current.empty()) result.push_back(current);
        current.clear();
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> source,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = removeComments(source);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"/*Test program */", "int main()", "{ ", "  // variable declaration ",
          "int a, b, c;", "/* This is a test", "   multiline  ",
          "   comment for ", "   testing */", "a = b + c;", "}"},
          {"int main()", "{ ", "  ", "int a, b, c;", "a = b + c;", "}"}, trials);
    test({"a/*comment", "line", "more_comment*/b"}, {"ab"}, trials);
    test({"a//*b//*c", "blank", "d/*/e*//f"}, {"a", "blank", "d/f"}, trials);
    return 0;
}


