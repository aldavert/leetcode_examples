#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<std::string> findWords(std::vector<std::vector<char> > board,
                                   std::vector<std::string> words)
{
    static constexpr int DEGREE = 26;
    class Trie
    {
    public:
        Trie(void) : m_index(0)
        {
            for (int i = 0; i < DEGREE; ++i) m_children[i] = nullptr;
        }
        ~Trie(void)
        {
            for (int i = 0; i < DEGREE; ++i) delete m_children[i];
        }
        Trie(const Trie &) = delete;
        Trie(const Trie &&) = delete;
        Trie& operator=(const Trie &) = delete;
        Trie& operator=(const Trie &&) = delete;
        void insert(const std::string &word, short id)
        {
            Trie * ptr = this;
            for (char c : word)
            {
                const int idx = static_cast<int>(c - 'a');
                if (ptr->m_children[idx] == nullptr)
                    ptr->m_children[idx] = new Trie();
                ptr = ptr->m_children[idx];
            }
            ptr->m_index = id;
        }
        const Trie * search(char c) const { return m_children[c - 'a']; }
        short index(void) const { return m_index; }
    private:
        Trie * m_children[DEGREE];
        short m_index = 0;
    };
    
    std::vector<std::string> result;
    const int m = static_cast<int>(board.size());
    const int n = static_cast<int>(board[0].size());
    Trie trie;
    short nw = 0;
    for (const std::string &w : words)
        trie.insert(w, ++nw);
    char current_word[16];
    std::vector<bool> available(nw, true);
    auto search = [&](auto &&self, int x, int y, int p, const Trie * t) -> void
    {
        if (t == nullptr) return;
        if ((x < 0) || (x >= m) || (y < 0) || (y >= n)) return;
        if (board[x][y] == '%') return;
        if (nw == 0) return;
        
        current_word[p] = board[x][y];
        if ((t = t->search(current_word[p])))
        {
            if (short idx = t->index(); idx)
            {
                if (available[idx])
                {
                    current_word[p + 1] = '\0';
                    result.push_back(current_word);
                    available[idx] = false;
                    --nw;
                }
            }
            board[x][y] = '%';
            self(self, x - 1, y    , p + 1, t);
            self(self, x    , y - 1, p + 1, t);
            self(self, x + 1, y    , p + 1, t);
            self(self, x    , y + 1, p + 1, t);
            board[x][y] = current_word[p];
        }
    };
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            search(search, i, j, 0, &trie);
    return result;
}
#else
std::vector<std::string> findWords(std::vector<std::vector<char> > board,
                                   std::vector<std::string> words)
{
    const int m = static_cast<int>(board.size());
    const int n = static_cast<int>(board[0].size());
    auto search = [&](auto &&self, std::string word, int x, int y, int p) -> bool
    {
        const int ws = static_cast<int>(word.size());
        if (p == ws) return true;
        if ((x < 0) || (x >= m) || (y < 0) || (y >= n)) return false;
        
        bool result = false;
        if (word[p] == board[x][y])
        {
            board[x][y] = '%';
            result = self(self, word, x - 1, y    , p + 1)
                  || self(self, word, x    , y - 1, p + 1)
                  || self(self, word, x + 1, y    , p + 1)
                  || self(self, word, x    , y + 1, p + 1);
            board[x][y] = word[p];
        }
        return result;
    };
    auto exist = [&](std::string word) -> bool
    {
        for (int i = 0; i < m; ++i)
            for (int j = 0; j < n; ++j)
                if (search(search, word, i, j, 0))
                    return true;
        return false;
    };
    
    std::vector<std::string> result;
    for (const std::string &w : words)
        if (exist(w))
            result.push_back(w);
    return result;
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    std::unordered_set lut(left.begin(), left.end());
    if (lut.size() != right.size()) return false;
    for (const std::string &w : right)
    {
        if (auto it = lut.find(w); it != lut.end())
            lut.erase(it);
        else return false;
    }
    return lut.empty();
}

void test(std::vector<std::vector<char> > board,
          std::vector<std::string> words,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findWords(board, words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'o', 'a', 'a', 'n'},
          {'e', 't', 'a', 'e'},
          {'i', 'h', 'k', 'r'},
          {'i', 'f', 'l', 'v'}},
          {"oath", "pea", "eat", "rain"}, {"eat", "oath"}, trials);
    test({{'a', 'b'}, {'c', 'd'}}, {"abcb"}, {}, trials);
    test({{'o', 'a', 'b', 'n'},
          {'o', 't', 'a', 'e'},
          {'a', 'h', 'k', 'r'},
          {'a', 'f', 'l', 'v'}},
          {"oa", "oaa"}, {"oa", "oaa"}, trials);
    return 0;
}


