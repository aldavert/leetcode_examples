#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long minimumReplacement(std::vector<int> nums)
{
    long result = 0;
    for (int i = static_cast<int>(nums.size()) - 2, max = nums.back(); i >= 0; --i)
    {
        int ops = (nums[i] - 1) / max;
        result += ops;
        max = nums[i] / (ops + 1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumReplacement(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 9, 3}, 2, trials);
    test({1, 2, 3, 4, 5}, 0, trials);
    return 0;
}


