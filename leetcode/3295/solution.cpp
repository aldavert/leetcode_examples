#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool reportSpam(std::vector<std::string> message,
                std::vector<std::string> bannedWords)
{
    std::unordered_set<std::string> banned(bannedWords.begin(), bannedWords.end());
    int count = 0;
    for (std::string word : message)
    {
        count += (banned.find(word) != banned.end());
        if (count == 2) return true;
    }
    return false;
}
#else
bool reportSpam(std::vector<std::string> message,
                std::vector<std::string> bannedWords)
{
    auto toInt = [](const std::string &word) -> unsigned long
    {
        unsigned long result = 0;
        for (char letter : word)
            result = result * 27 + static_cast<long>(letter - 'a' + 1);
        return result;
    };
    std::unordered_set<unsigned long> banned;
    for (std::string word : bannedWords)
        banned.insert(toInt(word));
    int count = 0;
    for (std::string word : message)
    {
        count += (banned.find(toInt(word)) != banned.end());
        if (count == 2) return true;
    }
    return false;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> message,
          std::vector<std::string> bannedWords,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = reportSpam(message, bannedWords);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"hello", "world", "leetcode"}, {"world", "hello"}, true, trials);
    test({"hello", "programming", "fun"},
         {"world", "programming", "leetcode"}, false, trials);
    test({"c"}, {"z"}, false, trials);
    test({"s", "a", "aj", "ps", "h", "ou", "e", "i", "x"},
         {"j", "a", "b", "fa", "z", "a", "no", "ih", "nq"}, false, trials);
    return 0;
}


