#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int maxProduct(std::string s)
{
    const int n = static_cast<int>(s.size()), mask = (1 << n) - 1;
    int dp[4096] = {}, result = 0;
    auto findpalindrome = [](std::string &str, int m) -> int
    {
        int r = 0;
        for (int i = 0, j = static_cast<int>(str.size()) - 1; i <= j; )
        {
            if      ((m & (1 << i)) == 0) ++i;
            else if ((m & (1 << j)) == 0) --j;
            else if (str[i] != str[j]) return 0;
            else r += 1 + (i++ != j--);
        }
        return r;
    };
    for (int i = 1; i <= mask; ++i)
        dp[i] = findpalindrome(s, i);
    for (int i = mask; i > 0; --i)
        if (dp[i] * (n - dp[i]) > result)
            for (int j = i ^ mask ; j > 0; j = ((j - 1) & (i ^ mask)))
                result = std::max(result, dp[i] * dp[j]);
    return result;
}
#else
int maxProduct(std::string s)
{
    auto isPalindrome = [](const std::string &str) -> bool
    {
        for (int i = 0, j = static_cast<int>(str.size()) - 1; i < j; ++i, --j)
            if (str[i] != str[j])
                return false;
        return true;
    };
    std::string s1, s2;
    size_t result = 0;
    
    auto dfs = [&](auto &&self, size_t i)
    {
        if (i == s.size())
        {
            if (isPalindrome(s1) && isPalindrome(s2))
                result = std::max(result, s1.size() * s2.size());
            return;
        }
        s1.push_back(s[i]);
        self(self, i + 1);
        s1.pop_back();
        s2.push_back(s[i]);
        self(self, i + 1);
        s2.pop_back();
        self(self, i + 1);
    };
    dfs(dfs, 0);
    return static_cast<int>(result);
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxProduct(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leetcodecom", 9, trials);
    test("bb", 1, trials);
    test("accbcaxxcxx", 25, trials);
    return 0;
}


