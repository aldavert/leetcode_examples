#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string firstPalindrome(std::vector<std::string> words)
{
    auto isPalindrome = [](std::string word)
    {
        for (int l = 0, r = static_cast<int>(word.size() - 1); l < r; ++l, --r)
            if (word[l] != word[r])
                return false;
        return true;
    };
    for (std::string word : words)
        if (isPalindrome(word))
            return word;
    return "";
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = firstPalindrome(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"abc", "car", "ada", "racecar", "cool"}, "ada", trials);
    test({"notapalindrome", "racecar"}, "racecar", trials);
    test({"def", "ghi"}, "", trials);
    return 0;
}


