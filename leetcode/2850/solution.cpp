#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
int minimumMoves(std::vector<std::vector<int> > grid)
{
    int result = std::numeric_limits<int>::max();
    auto backtrack = [&](auto &&self, int i, int j, int curr) -> void
    {
        if      (i == 3) result = std::min(result, curr);
        else if (j == 3) self(self, i + 1, 0, curr);
        else if (grid[i][j] != 0) self(self, i, j + 1, curr);
        else
        {
            for (int m = 0; m < 3; m++)
            {
                for (int n = 0; n < 3; n++)
                {
                    if (grid[m][n] > 1)
                    {
                        --grid[m][n];
                        ++grid[i][j];
                        self(self, i, j, curr + std::abs(m - i) + std::abs(n - j));
                        ++grid[m][n];
                        --grid[i][j];
                    }
                }
            }
        }
    };
    backtrack(backtrack, 0, 0, 0);
    return result;
}
#else
int minimumMoves(std::vector<std::vector<int> > grid)
{
    const int zero_count = std::accumulate(grid.begin(), grid.end(), 0,
    [](int subtotal, const std::vector<int> &row) {
    return subtotal + std::count(row.begin(), row.end(), 0); });
    if (zero_count == 0) return 0;
    int result = std::numeric_limits<int>::max();
    auto update = [&](int i, int j, int x, int y) -> void
    {
        --grid[x][y];
        ++grid[i][j];
        result = std::min(result,
                          std::abs(x - i) + std::abs(y - j) + minimumMoves(grid));
        ++grid[x][y];
        --grid[i][j];
    };
    for (int i = 0; i < 3; ++i)
        for (int j = 0; j < 3; ++j)
            if (grid[i][j] == 0)
            for (int x = 0; x < 3; ++x)
                for (int y = 0; y < 3; ++y)
                    if (grid[x][y] > 1)
                        update(i, j, x, y);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumMoves(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 0}, {1, 1, 1}, {1, 2, 1}}, 3, trials);
    test({{1, 3, 0}, {1, 0, 0}, {1, 0, 3}}, 4, trials);
    return 0;
}


