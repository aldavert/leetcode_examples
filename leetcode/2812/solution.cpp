#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int maximumSafenessFactor(std::vector<std::vector<int> > grid)
{
    const int direction[] = { -1, 0, 1, 0, -1 };
    const int n = static_cast<int>(grid.size());
    if (grid[0][0] || grid[n - 1][n - 1]) return 0;
    const int inf = n * n * n;
    std::vector<std::vector<int> > dist(n, std::vector<int>(n));
    for (int r = 0; r < n; ++r)
        for (int c = 0; c < n; ++c)
            dist[r][c] = inf * (grid[r][c] == 0);
    for (int c = 1; c < n; ++c)
        dist[0][c] = std::min(dist[0][c], dist[0][c - 1] + 1);
    for (int r = 1; r < n; ++r)
    {
        dist[r][0] = std::min(dist[r][0], dist[r - 1][0] + 1);
        for (int c = 1; c < n; ++c)
            dist[r][c] = std::min(dist[r][c],
                                  std::min(dist[r - 1][c], dist[r][c - 1]) + 1);
    }
    for (int c = n - 2; c >= 0; --c)
        dist[n - 1][c] = std::min(dist[n - 1][c], dist[n - 1][c + 1] + 1);
    for (int r = n - 2; r >= 0; --r)
    {
        dist[r][n - 1] = std::min(dist[r][n - 1], dist[r + 1][n - 1] + 1);
        for (int c = n - 2; c >= 0; --c)
            dist[r][c] = std::min(dist[r][c],
                                  std::min(dist[r + 1][c], dist[r][c + 1]) + 1);
    }
    struct Position
    {
        int dist = -1;
        int r = -1;
        int c = -1;
        bool operator<(const Position &other) const
        {
            return (dist < other.dist)
                || ((dist == other.dist) && (r < other.r))
                || ((dist == other.dist) && (r == other.r) && (c < other.c));
        }
    };
    std::priority_queue<Position> queue;
    std::vector<std::vector<bool> > visited(n, std::vector<bool>(n, 0));
    queue.emplace(dist[0][0], 0, 0);
    visited[0][0] = true;
    for (int r = 0; r < n; ++r)
        for (int c = 0; c < n; ++c)
            if (grid[r][c])
                visited[r][c] = true;
    while (!queue.empty())
    {
        auto [ds, r, c] = queue.top();
        queue.pop();
        if((r == n - 1) && (c == n - 1))
            return queue.top().dist;
        for(int l = 0; l < 4; ++l)
        {
            int nr = r + direction[l];
            int nc = c + direction[l + 1];
            if ((nr < 0) || (nc < 0) || (nr >= n) || (nc >= n))
                continue;
            if (!visited[nr][nc])
            {
                visited[nr][nc] = true;
                queue.emplace(std::min(ds, dist[nr][nc]), nr, nc);
            }
        }
    }
    return 0;
}
#elif 0
int maximumSafenessFactor(std::vector<std::vector<int> > grid)
{
    const int direction[] = { -1, 0, 1, 0, -1 };
    const int n = static_cast<int>(grid.size());
    const int inf = n * n * n;
    std::vector<std::vector<int> > dist(n, std::vector<int>(n));
    for (int r = 0; r < n; ++r)
        for (int c = 0; c < n; ++c)
            dist[r][c] = inf * (grid[r][c] == 0);
    for (int c = 1; c < n; ++c)
        dist[0][c] = std::min(dist[0][c], dist[0][c - 1] + 1);
    for (int r = 1; r < n; ++r)
    {
        dist[r][0] = std::min(dist[r][0], dist[r - 1][0] + 1);
        for (int c = 1; c < n; ++c)
            dist[r][c] = std::min(dist[r][c],
                                  std::min(dist[r - 1][c], dist[r][c - 1]) + 1);
    }
    for (int c = n - 2; c >= 0; --c)
        dist[n - 1][c] = std::min(dist[n - 1][c], dist[n - 1][c + 1] + 1);
    for (int r = n - 2; r >= 0; --r)
    {
        dist[r][n - 1] = std::min(dist[r][n - 1], dist[r + 1][n - 1] + 1);
        for (int c = n - 2; c >= 0; --c)
            dist[r][c] = std::min(dist[r][c],
                                  std::min(dist[r + 1][c], dist[r][c + 1]) + 1);
    }
    
    auto validPath = [&](int safeness) -> bool
    {
        if (dist[0][0] < safeness) return false;
        std::queue<std::pair<int, int> > q{{{0, 0}}};
        std::vector<std::vector<bool> > seen(n, std::vector<bool>(n));
        seen[0][0] = true;
        while (!q.empty())
        {
            const auto [i, j] = q.front();
            q.pop();
            if (dist[i][j] < safeness) continue;
            if ((i == n - 1) && (j == n - 1)) return true;
            for (int dir = 0; dir < 4; ++dir)
            {
                int x = i + direction[dir],
                    y = j + direction[dir + 1];
                if ((x < 0) || (x == n) || (y < 0) || (y == n))
                    continue;
                if (seen[x][y])
                    continue;
                q.emplace(x, y);
                seen[x][y] = true;
            }
        }
        return false;
    };
    int l = 0, r = n * 2;
    while (l < r)
    {
        int m = (l + r) / 2;
        if (validPath(m)) l = m + 1;
        else r = m;
    }
    return l - 1;
}
#else
int maximumSafenessFactor(std::vector<std::vector<int> > grid)
{
    const int direction[] = { -1, 0, 1, 0, -1 };
    const int n = static_cast<int>(grid.size());
    const int inf = n * n * n;
    std::vector<std::vector<int> > dist(n, std::vector<int>(n, inf)),
                                   cost(n, std::vector<int>(n)),
                                   prev(n, std::vector<int>(n, 4));
    std::vector<std::vector<bool> > active(n, std::vector<bool>(n, true));
    for (int r = 0; r < n; ++r)
        for (int c = 0; c < n; ++c)
            cost[r][c] = inf * (grid[r][c] == 0);
    for (int c = 1; c < n; ++c)
        cost[0][c] = std::min(cost[0][c], cost[0][c - 1] + 1);
    for (int r = 1; r < n; ++r)
    {
        cost[r][0] = std::min(cost[r][0], cost[r - 1][0] + 1);
        for (int c = 1; c < n; ++c)
            cost[r][c] = std::min(cost[r][c],
                                  std::min(cost[r - 1][c], cost[r][c - 1]) + 1);
    }
    for (int c = n - 2; c >= 0; --c)
        cost[n - 1][c] = std::min(cost[n - 1][c], cost[n - 1][c + 1] + 1);
    for (int r = n - 2; r >= 0; --r)
    {
        cost[r][n - 1] = std::min(cost[r][n - 1], cost[r + 1][n - 1] + 1);
        for (int c = n - 2; c >= 0; --c)
            cost[r][c] = std::min(cost[r][c],
                                  std::min(cost[r + 1][c], cost[r][c + 1]) + 1);
    }
    int max_cost = 0;
    for (int r = 0; r < n; ++r)
        for (int c = 0; c < n; ++c)
            max_cost = std::max(max_cost, cost[r][c] + 1);
    dist[0][0] = 0;
    for (int n_active = n * n; n_active > 0; --n_active)
    {
        int selected_r = 0, selected_c = 0, selected_d = inf;
        for (int r = 0; r < n; ++r)
            for (int c = 0; c < n; ++c)
                if (active[r][c] && (dist[r][c] < selected_d))
                    selected_d = dist[r][c],
                    selected_r = r,
                    selected_c = c;
        active[selected_r][selected_c] = false;
        for (int i = 0; i < 4; ++i)
        {
            int nr = selected_r + direction[i],
                nc = selected_c + direction[i + 1];
            if ((nc >= n) || (nr >= n) || (nc < 0) || (nr < 0)) continue;
            if (!active[nr][nc]) continue;
            int new_dist = dist[selected_r][selected_c] + (max_cost - cost[nr][nc]);
            if (new_dist < dist[nr][nc])
                dist[nr][nc] = new_dist,
                prev[nr][nc] = i;
        }
    }
    int result = cost[n - 1][n - 1];
    for (int r = n - 1, c = n - 1; (r != 0) || (c != 0);)
    {
        int p = prev[r][c];
        r -= direction[p];
        c -= direction[p + 1];
        result = std::min(result, cost[r][c]);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumSafenessFactor(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0, 0}, {0, 0, 0}, {0, 0, 1}}, 0, trials);
    test({{0, 0, 1}, {0, 0, 0}, {0, 0, 0}}, 2, trials);
    test({{0, 0, 0, 1}, {0, 0, 0, 0}, {0, 0, 0, 0}, {1, 0, 0, 0}}, 2, trials);
    test({{1}}, 0, trials);
    test({{0, 1, 1}, {0, 1, 1}, {0, 0, 1}}, 0, trials);
    test({{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
          {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}, 2, trials);
    return 0;
}


