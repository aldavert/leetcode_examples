#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minStartValue(std::vector<int> nums)
{
    int minimum = 0, sum = 0;
    for (int n : nums)
        minimum = std::min(minimum, sum += n);
    return -minimum + 1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minStartValue(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-3, 2, -3, 4, 2}, 5, trials);
    test({1, 2}, 1, trials);
    test({1, -2, -3}, 5, trials);
    return 0;
}
