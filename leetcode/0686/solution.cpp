#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int repeatedStringMatch(std::string a, std::string b)
{
    size_t pos = 0;
    while ((pos = a.find(b[0], pos)) != a.npos)
    {
        if (std::string head = a.substr(pos, b.size());
            head == b.substr(0, a.size() - pos))
        {
            size_t size = b.size() - head.size();
            int parts = static_cast<int>(size / a.size());
            int result = 1 + parts + (size != 0) * (size % a.size() != 0);
            size_t idx = head.size();
            bool good = true;
            for (; size > a.size(); size -= a.size(), idx += a.size())
            {
                if (a != b.substr(idx, a.size()))
                {
                    good = false;
                    break;
                }
            }
            if (good && (a.substr(0, size) == b.substr(idx, a.size())))
                return result;
        }
        ++pos;
    }
    return -1;
}
#else
int repeatedStringMatch(std::string a, std::string b)
{
    size_t pos = 0;
    while ((pos = a.find(b[0], pos)) != a.npos)
    {
        if (std::string head = a.substr(pos, b.size());
            head == b.substr(0, a.size() - pos))
        {
            size_t size = b.size() - head.size();
            int parts = static_cast<int>(size / a.size());
            int result = 1 + parts + (size != 0) * (size % a.size() != 0);
            std::string r = head;
            for (; size > a.size(); size -= a.size())
                r += a;
            r += a.substr(0, size);
            if (r == b) return result;
        }
        ++pos;
    }
    return -1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string a, std::string b, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = repeatedStringMatch(a, b);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcd", "cdabcdab", 3, trials);
    test("a", "aa", 2, trials);
    test("abab", "aba", 1, trials);
    test("aaaaaaaaaaaaaaaaaaaaaab", "ba", 2, trials);
    return 0;
}


