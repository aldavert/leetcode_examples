#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findPeaks(std::vector<int> mountain)
{
    std::vector<int> result;
    for (int i = 1, n = static_cast<int>(mountain.size()) - 1; i < n; ++i)
        if ((mountain[i] > mountain[i - 1]) && (mountain[i] > mountain[i + 1]))
            result.push_back(i);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_set<int> left_unique;
    for (int value : left)
        left_unique.insert(value);
    for (int value : right)
        if (auto search = left_unique.find(value); search == left_unique.end())
            return false;
        else left_unique.erase(search);
    return left_unique.empty();
}

void test(std::vector<int> mountain, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findPeaks(mountain);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4, 4}, {}, trials);
    test({1, 4, 3, 8, 5}, {1, 3}, trials);
    return 0;
}


