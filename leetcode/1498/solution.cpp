#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numSubseq(std::vector<int> nums, int target)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(nums.size());
    std::vector<int> powers_mod(n, 1);
    int result = 0;
    
    for (int i = 1; i < n; ++i)
        powers_mod[i] = (powers_mod[i - 1] * 2) % MOD;
    std::sort(nums.begin(), nums.end());
    for (int l = 0, r = n - 1; l <= r;)
        if (nums[l] + nums[r] <= target)
            result = (result + powers_mod[r - l++]) % MOD;
        else --r;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numSubseq(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 5, 6, 7}, 9, 4, trials);
    test({3, 3, 6, 8}, 10, 6, trials);
    test({2, 3, 3, 4, 6, 7}, 12, 61, trials);
    return 0;
}


