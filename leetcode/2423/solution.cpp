#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

bool equalFrequency(std::string word)
{
    int frequency[26] = {};
    for (char letter : word)
        ++frequency[letter - 'a'];
    std::unordered_map<int, int> histogram;
    for (int i = 0; i < 26; ++i)
        if (frequency[i])
            ++histogram[frequency[i]];
    if (histogram.size() > 2) return false;
    if (histogram.size() == 1) return (histogram.begin()->first == 1)
                                   || (histogram.begin()->second == 1);
    std::pair<int, int> first = *histogram.begin(),
                        second = *std::next(histogram.begin());
    if (first.first < second.first) std::swap(first, second);
    if ((second.first == 1) && (second.second == 1)) return true;
    if ((first.first - second.first == 1) && (first.second == 1)) return true;
    return false;
}

// ############################################################################
// ############################################################################

void test(std::string word, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = equalFrequency(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcc", true, trials);
    test("aazz", false, trials);
    test("bac", true, trials);
    test("abbcc", true, trials);
    test("zz", true, trials);
    test("cccd", true, trials);
    test("cccaa", true, trials);
    return 0;
}


