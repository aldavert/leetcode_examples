#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int coinChange(std::vector<int> coins, int amount)
{
    std::vector<int> dp(amount + 1, amount + 1);
    dp[0] = 0;
    for (const int coin : coins)
        for (int i = coin; i <= amount; ++i)
            dp[i] = std::min(dp[i], dp[i - coin] + 1);
    return (dp[amount] == amount + 1)?-1:dp[amount];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> coins, int amount, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = coinChange(coins, amount);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 5}, 11, 3, trials);
    test({2, 5}, 11, 4, trials);
    test({2}, 3, -1, trials);
    test({1}, 0, 0, trials);
    return 0;
}


