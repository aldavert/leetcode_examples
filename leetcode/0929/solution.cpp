#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numUniqueEmails(std::vector<std::string> emails)
{
    std::unordered_set<std::string> unique;
    for (const std::string &current : emails)
    {
        const int n = static_cast<int>(current.size());
        std::string address;
        int i;
        bool copy = true;
        for (i = 0; (current[i] != '@') && (i < n); ++i)
        {
            if (current[i] == '+') copy = false;
            if (copy && (current[i] != '.'))
                    address += current[i];
        }
        address += current.substr(i, n - i);
        unique.insert(address);
    }
    return static_cast<int>(unique.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> emails, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numUniqueEmails(emails);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"test.email+alex@leetcode.com", "test.e.mail+bob.cathy@leetcode.com",
          "testemail+david@lee.tcode.com"}, 2, trials);
    test({"a@leetcode.com","b@leetcode.com","c@leetcode.com"}, 3, trials);
    return 0;
}


