#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string clearStars(std::string s)
{
    std::string result = s;
    std::vector<int> buckets[26];
    for (int i = 0, n = static_cast<int>(s.size()); i < n; ++i)
    {
        if (s[i] == '*')
        {
            result[i] = ' ';
            int j = 0;
            while (buckets[j].empty()) ++j;
            result[buckets[j].back()] = ' ';
            buckets[j].pop_back();
        }
        else buckets[s[i] - 'a'].push_back(i);
    }
    std::erase(result, ' ');
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = clearStars(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aaba*", "aab", trials);
    test("abc", "abc", trials);
    return 0;
}


