#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int getMaximumGenerated(int n)
{
    int num[102];
    num[0] = 0;
    num[1] = 1;
    int result = (n > 0);
    for (int i = 2; i < n; i += 2)
    {
        int p = i >> 1;
        num[i    ] = num[p];
        num[i + 1] = num[p] + num[p + 1];
        result = std::max(result, std::max(num[i], num[i + 1]));
    }
    return std::max(result, num[n / 2]);
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getMaximumGenerated(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, 3, trials);
    test(2, 1, trials);
    test(3, 2, trials);
    test(4, 2, trials);
    test(15, 5, trials);
    test(0, 0, trials);
    test(1, 1, trials);
    return 0;
}


