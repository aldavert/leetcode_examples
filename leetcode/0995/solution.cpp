#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minKBitFlips(std::vector<int> nums, int k)
{
    int result = 0, flipped = 0;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        if ((i >= k) && (nums[i - k] == 2)) --flipped;
        if ((flipped & 1) == nums[i])
        {
            if (i + k > n) return -1;
            ++result;
            ++flipped;
            nums[i] = 2;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minKBitFlips(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 0}, 1, 2, trials);
    test({1, 1, 0}, 2, -1, trials);
    test({0, 0, 0, 1, 0, 1, 1, 0}, 3, 3, trials);
    return 0;
}


