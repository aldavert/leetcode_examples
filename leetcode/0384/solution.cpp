#include "../common/common.hpp"
#include <random>

// ############################################################################
// ############################################################################

class Solution
{
    std::vector<int> m_elements;
    std::random_device m_rd;
    std::mt19937 m_g;
public:
    Solution(const std::vector<int> &nums) : m_elements(nums), m_g(m_rd()) {}
    Solution(std::vector<int> &&nums) : m_elements(std::move(nums)), m_g(m_rd()) {}
    
    /// Resets the array to its original configuration and return it.
    const std::vector<int>& reset() const & { return m_elements; }
    std::vector<int> reset() const && { return std::move(m_elements); }
    
    /// Returns a random shuffling of the array.
    std::vector<int> shuffle()
    {
        std::vector<int> result(m_elements);
        std::shuffle(result.begin(), result.end(), m_g);
        return result;
    }
};

// ############################################################################
// ############################################################################

bool operator==(const std::unordered_set<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_set<int> lut;
    const int n = static_cast<int>(right.size());
    for (int i = 0; i < n; ++i)
    {
        if (left.find(right[i]) == left.end()) return false; // Lost element.
        if (lut.find(right[i]) != left.end()) return false; // Duplicated element.
        lut.insert(right[i]);
    }
    return true;
}

enum class OP { SOLUTION, SHUFFLE, RESET };

void test(std::vector<OP> operations,
          std::vector<std::vector<int> > input,
          std::vector<std::vector<int> > output,
          unsigned int trials = 1)
{
    bool result = true;
    const int n = static_cast<int>(operations.size());
    if (static_cast<int>(input.size()) != n)
    {
        std::cout << "[ERROR] Incorrect input length\n";
        result = false;
    }
    if (static_cast<int>(output.size()) != n)
    {
        std::cout << "[ERROR] Incorrect output length\n";
        result = false;
    }
    for (unsigned int t = 0; result && (t < trials); ++t)
    {
        Solution * solution = nullptr;
        for (int i = 0; i < n; ++i)
        {
            switch (operations[i])
            {
            case OP::SOLUTION:
                delete solution;
                solution = new Solution(input[i]);
                break;
            case OP::SHUFFLE:
                if (solution == nullptr) { result = false; break; }
                result = std::unordered_set<int>(output[i].begin(), output[i].end())
                       == solution->shuffle();
                break;
            case OP::RESET:
                if (solution == nullptr) { result = false; break; }
                result = solution->reset() == output[i];
                break;
            default:
                result = false;
                break;
            }
        }
        delete solution;
    }
    showResult(result, output, output);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::SOLUTION, OP::SHUFFLE, OP::RESET, OP::SHUFFLE},
         {   {1, 2, 3},          {},        {},           {}},
         {          {},   {3, 1, 2}, {1, 2, 3},    {1, 3, 2}}, trials);
    return 0;
}


