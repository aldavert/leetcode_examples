# Path Sum III

Given the `root` of a binary tree and an integer `targetSum`, return *the number of paths where the sum of the values along the path equals* `targetSum`.

The path does not need to start or end at the root or a leaf, but it must go downwards (i.e., traveling only from parent nodes to child nodes).

#### Example 1:
> ```mermaid
> graph TD;
> A((10))-->B((5))
> B-->C((3))
> B-->D((2))
> D---EMPTY1(( ))
> D-->G((1))
> C-->E((3))
> C-->F(( -2))
> A-->H(( -3))
> H---EMPTY2(( ))
> H-->I((11))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef red fill:#FAA,stroke:#000,stroke-width:2px;
> classDef orange fill:#FCA,stroke:#000,stroke-width:2px;
> classDef mixed fill:#FAA,stroke:#FCA,stroke-width:8px;
> classDef blue fill:#CCF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class B mixed;
> class C red;
> class D,G orange;
> class H,I blue;
> class EMPTY1,EMPTY2 empty;
> linkStyle 3,8 stroke-width:0px;
> ```
> *Input:* `root = [10, 5, -3, 3, 2, null, 11, 3, -2, null, 1], targetSum = 8`  
> *Output:* `3`  
> *Explanation:* The paths that sum to 8 are shown.

#### Example 2:
> *Input:* `root = [5, 4, 8, 11, null, 13, 4, 7, 2, null, null, 5, 1], targetSum = 22`  
> *Output:* `3`
 
#### Constraints:
- The number of nodes in the tree is in the range `[0, 1000]`.
- `-10^9 <= Node.val <= 10^9`
- `-1000 <= targetSum <= 1000`


