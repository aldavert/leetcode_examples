#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <unordered_map>

// ################################################################################################
// ################################################################################################

#if 1
int pathSum(TreeNode* root, int targetSum)
{
    std::unordered_map<long, int> lut;
    int result = 0;
    auto traverse = [&](auto &&self, TreeNode * node, long sum) -> void
    {
        if (node != nullptr)
        {
            sum += node->val;
            if (auto it = lut.find(sum - targetSum); it != lut.end())
                result += it->second;
            lut[sum]++;
            self(self, node->left, sum);
            self(self, node->right, sum);
            lut[sum]--;
        }
    };
    lut[0] = 1;
    traverse(traverse, root, 0);
    return result;
}
#else
int pathSum(TreeNode* root, int targetSum)
{
    long histogram[1000] = {};
    int result = 0;
    auto traverse = [&](auto &&self, TreeNode * node, int depth) -> void
    {
        if (node != nullptr)
        {
            for (int i = 0; i < depth; ++i)
                if ((histogram[i] += node->val) == targetSum)
                    ++result;
            if ((histogram[depth] = node->val) == targetSum)
                ++result;
            self(self, node->left, depth + 1);
            self(self, node->right, depth + 1);
            
            for (int i = 0; i < depth; ++i)
                histogram[i] -= node->val;
        }
    };
    traverse(traverse, root, 0);
    return result;
}
#endif

// ################################################################################################
// ################################################################################################

void test(const std::vector<int> &tree, int targetSum, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = pathSum(root, targetSum);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 5, -3, 3, 2, null, 11, 3, -2, null, 1}, 8, 3, trials);
    test({5, 4, 8, 11, null, 13, 4, 7, 2, null, null, 5, 1}, 22, 3, trials);
    return 0;
}
