#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumSumSubsequence(std::vector<int> nums,
                          std::vector<std::vector<int> > queries)
{
    struct NodeType
    {
        long v[2][2] = {};
        long max(void) const { return std::max({v[0][0], v[0][1], v[1][0], v[1][1]}); }
    };
    static constexpr long MOD = 1'000'000'007;
    static constexpr int INF = 1'000'000'000;
    static constexpr NodeType DEFAULTNODE = {{{-INF, -INF}, {-INF, -INF}}};
    class SegmentTree
    {
        const int n;
        std::vector<NodeType> tree;
    public:
        explicit SegmentTree(const std::vector<int> &nums) :
            n(static_cast<int>(nums.size())),
            tree(4 * n)
        {
            build(nums, 0, 0, n - 1);
        }
        void update(int i, int val)
        {
            update(0, 0, n - 1, i, val);
        }
        NodeType query(int i, int j) const
        {
            return query(0, 0, n - 1, i, j);
        }
    private:
        void build(const std::vector<int> &nums, int index, int lo, int hi)
        {
            if (lo == hi)
            {
                tree[index] = {{{0, -INF}, {-INF, nums[lo]}}};
                return;
            }
            const int mid = (lo + hi) / 2;
            build(nums, 2 * index + 1, lo, mid);
            build(nums, 2 * index + 2, mid + 1, hi);
            tree[index] = merge(tree[2 * index + 1], tree[2 * index + 2]);
        }
        void update(int index, int lo, int hi, int i, int val)
        {
            if (lo == hi)
            {
                tree[index] = {{{0, -INF}, {-INF, val}}};
                return;
            }
            const int mid = (lo + hi) / 2;
            if (i <= mid)
                update(2 * index + 1, lo, mid, i, val);
            else
                update(2 * index + 2, mid + 1, hi, i, val);
            tree[index] = merge(tree[2 * index + 1], tree[2 * index + 2]);
        }
        NodeType query(int index, int lo, int hi, int i, int j) const
        {
            if ((i <= lo) && (hi <= j)) return tree[index];
            if ((j < lo) || (hi < i)) return DEFAULTNODE;
            const int mid = (lo + hi) / 2;
            return merge(query(2 * index + 1, lo, mid, i, j),
                         query(2 * index + 2, mid + 1, hi, i, j));
        }
        NodeType merge(const NodeType &a, const NodeType &b) const
        {
            NodeType node;
            for (int l = 0; l < 2; ++l)
                for (int r = 0; r < 2; ++r)
                    node.v[l][r] = static_cast<int>(std::max({a.v[l][0] + b.v[0][r],
                                             a.v[l][0] + b.v[1][r],
                                             a.v[l][1] + b.v[0][r]}) % MOD);
            return node;
        }
    };
    const int n = static_cast<int>(nums.size());
    long result = 0;
    SegmentTree tree(nums);
    
    for (const auto &query : queries)
    {
        tree.update(query[0], query[1]);
        NodeType q = tree.query(0, n - 1);
        result = (result + static_cast<long>(q.max())) % MOD;
    }
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::vector<int> > queries,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumSumSubsequence(nums, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 5, 9}, {{1, -2}, {0, -3}}, 21, trials);
    test({0, -1}, {{0, -5}}, 0, trials);
    return 0;
}


