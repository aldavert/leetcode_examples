#include "../common/common.hpp"
#include <queue>
#include <cstring>

// ############################################################################
// ############################################################################

#if 1
int minDays(std::vector<std::vector<int> > grid)
{
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>(grid[0].size()),
              dir[4][2] = {{-1, 0}, {0, -1}, {1, 0}, {0, 1}};
    int tin[32][32], tup[32][32], timer = 0;
    auto dfs = [&](auto &&self, int i, int j, int pi, int pj) -> bool
    {
        tin[i][j] = timer;
        tup[i][j] = timer++;
        
        bool result = false;
        int children = 0;
        for (int k = 0; k < 4; ++k)
        {
            int ni = i + dir[k][0];
            int nj = j + dir[k][1];
            
            if ((ni >= 0) && (ni < n_rows) && (nj >= 0) && (nj < n_cols)
            &&  grid[ni][nj] && ((ni != pi) || (nj != pj)))
            {
                if (tin[ni][nj] != -1)
                    tup[i][j] = std::min(tup[i][j], tin[ni][nj]);
                else
                {
                    if (self(self, ni, nj, i, j)) result = true;
                    tup[i][j] = std::min(tup[i][j], tup[ni][nj]);
                    if ((tup[ni][nj] >= tin[i][j]) && (pi != -1)) result = true;
                    ++children;
                }
            }
        }
        return result || ((pi == -1) && (children > 1));
    };
    
    for (int r = 0; r < n_rows; ++r)
        for (int c = 0; c < n_cols; ++c)
            tin[r][c] = tup[r][c] = -1;
    int result = 2, count = 0;
    for (int i = 0; i < n_rows; ++i) 
    {
        for (int j = 0; j < n_cols; ++j)
        {
            if (grid[i][j] == 1) ++count;
            if ((grid[i][j] == 1) && (tin[i][j] == -1))
            {
                if (timer > 0) return 0;
                if (dfs(dfs, i, j, -1, -1)) result = 1;
            }
        }
    }
    if (count < 2) result = count;
    return result;
}
#else
int minDays(std::vector<std::vector<int> > grid)
{
    const int n_rows = static_cast<int>(grid.size());
    const int n_cols = static_cast<int>(grid[0].size());
    int seen[31][31] = {};
    auto fill = [&](int row, int col) -> void
    {
        std::queue<std::pair<int, int> > q({{row, col}});
        while (!q.empty())
        {
            auto [r, c] = q.front();
            q.pop();
            if ((c < 0) || (r < 0) || (c >= n_cols) || (r >= n_rows)) continue;
            if (grid[r][c] == 0) continue;
            if (seen[r][c]++) continue;
            q.push({r +  1, c +  0});
            q.push({r +  0, c +  1});
            q.push({r + -1, c +  0});
            q.push({r +  0, c + -1});
        }
    };
    auto disconnected = [&](void) -> int
    {
        int count = 0;
        std::memset(seen, 0, sizeof(seen));
        for (int row = 0; row < n_rows; ++row)
        {
            for (int col = 0; col < n_cols; ++col)
            {
                if (grid[row][col] && !seen[row][col])
                {
                    fill(row, col);
                    if (++count > 1) return 2;
                }
            }
        }
        return count;
    };
    
    if (disconnected() != 1) return 0;
    for (int row = 0; row < n_rows; ++row)
    {
        for (int col = 0; col < n_cols; ++col)
        {
            if (!grid[row][col]) continue;
            grid[row][col] = 0;
            if (disconnected() != 1) return 1;
            grid[row][col] = 1;
        }
    }
    return 2;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDays(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 1, 0},
          {0, 1, 1, 0},
          {0, 0, 0, 0}}, 2, trials);
    test({{1, 1}}, 2, trials);
    test({{0, 0, 0},
          {0, 1, 0},
          {0, 0, 0}}, 1, trials);
    test({{1, 0, 1, 0}}, 0, trials);
    test({{1, 1, 0, 1, 1},
          {1, 1, 1, 1, 1},
          {1, 1, 0, 1, 1},
          {1, 1, 0, 1, 1}}, 1, trials);
    test({{1, 1, 0, 1, 1},
          {1, 1, 1, 1, 1},
          {1, 1, 0, 1, 1},
          {1, 1, 1, 1, 1}}, 2, trials);
    test({{1, 0, 0},
          {1, 1, 0},
          {1, 0, 0}}, 1, trials);
    return 0;
}


