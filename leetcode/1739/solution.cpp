#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumBoxes(int n)
{
    int n_boxes = 0, next_touchings = 0, current_level_boxes = 0;
    
    while (n_boxes < n)
        ++next_touchings,
        current_level_boxes += next_touchings,
        n_boxes += current_level_boxes;
    if (n_boxes == n) return current_level_boxes;
    
    n_boxes -= current_level_boxes;
    current_level_boxes -= next_touchings;
    next_touchings = 0;
    while (n_boxes < n)
        ++next_touchings,
        n_boxes += next_touchings;
    
    return current_level_boxes + next_touchings;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumBoxes(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 3, trials);
    test(4, 3, trials);
    test(10, 6, trials);
    return 0;
}


