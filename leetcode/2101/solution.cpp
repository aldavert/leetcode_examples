#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumDetonation(std::vector<std::vector<int> > bombs)
{
    const int n = static_cast<int>(bombs.size());
    std::vector<std::vector<int> > graph(n);
    int result = 0;
    auto squaredDist = [&](int i, int j) -> long
    {
        return static_cast<long>(bombs[i][0] - bombs[j][0]) *
                                (bombs[i][0] - bombs[j][0]) +
               static_cast<long>(bombs[i][1] - bombs[j][1]) *
                                (bombs[i][1] - bombs[j][1]);
    };
    auto dfs = [&](auto &&self, int u, std::unordered_set<int> &seen) -> void
    {
        for (int v : graph[u])
        {
            if (auto search = seen.find(v); search != seen.end()) continue;
            seen.insert(v);
            self(self, v, seen);
        }
    };
    
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            if (i == j) continue;
            if (long ri = bombs[i][2]; ri * ri >= squaredDist(i, j))
                graph[i].push_back(j);
        }
    }
    
    for (int i = 0; i < n; ++i)
    {
        std::unordered_set<int> seen{i};
        dfs(dfs, i, seen);
        result = std::max(result, static_cast<int>(seen.size()));
    }
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > bombs, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumDetonation(bombs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 1, 3}, {6, 1, 4}}, 2, trials);
    test({{1, 1, 5}, {10, 10, 5}}, 1, trials);
    test({{1, 2, 3}, {2, 3, 1}, {3, 4, 2}, {4, 5, 3}, {5, 6, 4}}, 5, trials);
    return 0;
}


