#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string smallestEquivalentString(std::string s1,
                                     std::string s2,
                                     std::string baseStr)
{
    const size_t n = s1.size();
    char lut[128];
    for (int i = 0; i < 128; ++i)
        lut[i] = static_cast<char>(i);
    auto find = [&](auto &&self, char chr) -> char
    {
        if (lut[static_cast<int>(chr)] == chr) return chr;
        return self(self, lut[static_cast<int>(chr)]);
    };
    auto replace = [&](auto &&self, char chr, char label) -> void
    {
        if (lut[static_cast<int>(chr)] != chr)
            self(self, lut[static_cast<int>(chr)], label);
        lut[static_cast<int>(chr)] = label;
    };
    for (size_t i = 0; i < n; ++i)
    {
        char first  = find(find, s1[i]),
             second = find(find, s2[i]);
        char label = std::min(first, second);
        replace(replace, s1[i], label);
        replace(replace, s2[i], label);
    }
    for (char c = 'a'; c <= 'z'; ++c)
        replace(replace, c, find(find, lut[static_cast<int>(c)]));
    for (char &chr : baseStr)
        chr = lut[static_cast<int>(chr)];
    return baseStr;
}

// ############################################################################
// ############################################################################

void test(std::string s1,
          std::string s2,
          std::string baseStr,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestEquivalentString(s1, s2, baseStr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("parker", "morris", "parser", "makkek", trials);
    test("hello", "world", "hold", "hdld", trials);
    test("leetcode", "programs", "sourcecode", "aauaaaaada", trials);
    return 0;
}


