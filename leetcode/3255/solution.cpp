#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> resultsArray(std::vector<int> nums, int k)
{
    std::vector<int> result;
    for (int i = 0, start = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        if ((i > 0) && (nums[i] != nums[i - 1] + 1))
            start = i;
        if (i >= k - 1)
            result.push_back((i - start + 1 >= k)?nums[i]:-1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = resultsArray(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 3, 2, 5}, 3, {3, 4, -1, -1, -1}, trials);
    test({2, 2, 2, 2, 2}, 4, {-1, -1}, trials);
    test({3, 2, 3, 2, 3, 2}, 2, {-1, 3, -1, 3, -1}, trials);
    return 0;
}


