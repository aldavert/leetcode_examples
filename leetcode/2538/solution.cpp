#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

long long maxOutput(int n,
                    std::vector<std::vector<int> > edges,
                    std::vector<int> price)
{
    int result = 0;
    std::vector<std::vector<int> > tree(n);
    std::vector<int> max_sums(n);
    auto maxSum = [&](auto &&self, int u, int prev) -> int
    {
        int max_child_sum = 0;
        for (const int v : tree[u])
            if (prev != v)
                max_child_sum = std::max(max_child_sum, self(self, v, u));
        return max_sums[u] = price[u] + max_child_sum;
    };
    auto reroot = [&](auto &&self, int u, int prev, int parent_sum) -> void
    {
        int max_subtree_sum1 = 0, max_subtree_sum2 = 0, max_node = -1;
        for (int v : tree[u])
        {
            if (v == prev) continue;
            if (max_sums[v] > max_subtree_sum1)
            {
                max_subtree_sum2 = std::exchange(max_subtree_sum1, max_sums[v]);
                max_node = v;
            }
            else if (max_sums[v] > max_subtree_sum2)
                max_subtree_sum2 = max_sums[v];
        }
        if (tree[u].size() == 1)
            result = std::max({result, parent_sum, max_subtree_sum1});
        for (int v : tree[u])
        {
            if (v == prev) continue;
            int next_parent_sum =
                ((v == max_node)?price[u] + std::max(parent_sum, max_subtree_sum2)
                                :price[u] + std::max(parent_sum, max_subtree_sum1));
            self(self, v, u, next_parent_sum);
        }
    };
    
    for (const auto &edge : edges)
    {
        tree[edge[0]].push_back(edge[1]);
        tree[edge[1]].push_back(edge[0]);
    }
    maxSum(maxSum, 0, -1);
    reroot(reroot, 0, -1, 0);
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<int> price,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxOutput(n, edges, price);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, {{0, 1}, {1, 2}, {1, 3}, {3, 4}, {3, 5}}, {9, 8, 7, 6, 10, 5}, 24, trials);
    test(3, {{0, 1}, {1, 2}}, {1, 1, 1}, 2, trials);
    return 0;
}


