# Difference Between Maximum and Minimum Price Sum

There exists an undirected and initially unrooted tree with `n` nodes indexed from `0` to `n - 1`. You are given the integer `n` and a 2D integer array edges of length `n - 1`, where `edges[i] = [a_i, b_i]` indicates that there is an edge between nodes `a_i` and `b_i` in the tree.

Each node has an associated price. You are given an integer array `price`, where `price[i]` is the price of the `i^{th}` node.

The **price sum** of a given path is the sum of the prices of all nodes lying on that path.

The tree can be rooted at any node `root` of your choice. The incurred **cost** after choosing `root` is the difference between the maximum and minimum **price sum** amongst all paths starting at `root`.

Return *the* ***maximum*** *possible* ***cost*** *amongst all possible root choices*.

#### Example 1:
> ```mermaid
> graph TD;
> A1(("2 [7]"))---B1(("1 [8]"))
> A1---EMPTY1((" "))
> B1---C1(("0 [9]"))
> B1---D1(("3 [6]"))
> D1---E1(("4 [10]"))
> D1---F1(("5 [5]"))
> 
> A2(("2 [7]"))---B2(("1 [8]"))
> A2---EMPTY2((" "))
> B2---C2(("0 [9]"))
> B2---D2(("3 [6]"))
> D2---E2(("4 [10]"))
> D2---F2(("5 [5]"))
> 
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef red fill:#F99,stroke:#933,stroke-width:3px;
> classDef blue fill:#99F,stroke:#339,stroke-width:3px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px
> class A1,B1,D1,E1 red
> class A2 blue
> class EMPTY1,EMPTY2 empty
> linkStyle 1,7 stroke-width:0px;
> ```
> *Input:* `n = 6, edges = [[0, 1], [1, 2], [1, 3], [3, 4], [3, 5]], price = [9, 8, 7, 6, 10, 5]`  
> *Output:* `24`  
> *Explanation:* The diagram above denotes the tree after rooting it at node `2`. The first part (colored in red) shows the path with the maximum price sum. The second part (colored in blue) shows the path with the minimum price sum.
> - The first path contains nodes `[2, 1, 3, 4]`: the prices are `[7, 8, 6, 10]`, and the sum of the prices is `31`.
> - The second path contains the node `[2]` with the price `[7]`.
> 
> The difference between the maximum and minimum price sum is `24`. It can be proved that `24` is the maximum cost.

#### Example 2:
> ```mermaid
> graph TD
> A1(("1 [1]"))---B1(("1 [1]"))
> A1---EMPTY1a((" "))
> B1---C1(("2 [1]"))
> B1---EMPTY1b((" "))
> 
> A2(("1 [1]"))---B2(("1 [1]"))
> A2---EMPTY2a((" "))
> B2---C2(("2 [1]"))
> B2---EMPTY2b((" "))
> 
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef red fill:#F99,stroke:#933,stroke-width:3px;
> classDef blue fill:#99F,stroke:#339,stroke-width:3px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px
> class A1,B1,C1 red
> class A2 blue
> class EMPTY1a,EMPTY1b,EMPTY2a,EMPTY2b empty
> linkStyle 1,3,5,7 stroke-width:0px;
> ```
> *Input:* `n = 3, edges = [[0, 1], [1, 2]], price = [1, 1, 1]`  
> *Output:* `2`  
> *Explanation:* The diagram above denotes the tree after rooting it at node `0`. The first part (colored in red) shows the path with the maximum price sum. The second part (colored in blue) shows the path with the minimum price sum.
> - The first path contains nodes `[0, 1, 2]`: the prices are `[1, 1, 1]`, and the sum of the prices is `3`.
> - The second path contains node `[0]` with a price `[1]`.
> 
> The difference between the maximum and minimum price sum is `2`. It can be proved that `2` is the maximum cost.

#### Constraints:
- `1 <= n <= 10^5`
- `edges.length == n - 1`
- `0 <= a_i, b_i <= n - 1`
- `edges` represents a valid tree.
- `price.length == n`
- `1 <= price[i] <= 10^5`


