#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int divide(int dividend, int divisor)
{
    long D = dividend;
    long d = divisor;
    bool negative = false;
    if (D < 0)
    {
        D = -D;
        negative = !negative;
    }
    if (d < 0)
    {
        d = -d;
        negative = !negative;
    }
    if      (d >  D) return 0;
    else if (d == D) return (negative)?-1:1;
    long result = 0;
    int nbits = 0;
    for (long aux = d; (aux > 0) && (nbits < 32); ++nbits, aux >>= 1);
    d <<= 32 - nbits;
    for (int i = 0; i <= 32 - nbits; ++i)
    {
        result <<= 1;
        if (D >= d)
        {
            result |= 1;
            D -= d;
        }
        d >>= 1;
    }
    return static_cast<int>((negative)?-result:std::min<long>(result, 0x7FFFFFFF));
}

// ############################################################################
// ############################################################################

void test(int dividend, int divisor, long solution, unsigned int trials = 1)
{
    long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = divide(dividend, divisor);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, 3, 2, trials);
    test(7, 3, 2, trials);
    test(8, 3, 2, trials);
    test(9, 3, 3, trials);
    test(10, 3, 3, trials);
    test(7, -3, -2, trials);
    test(1543, 3, 514, trials);
    test(1542, 3, 514, trials);
    test(1544, 3, 514, trials);
    test(-1, 1, -1, trials);
    //test(-2147483648, -1, 2147483648, trials);
    test(-2147483648, -1, 2147483647, trials);
    test(-2147483648,  1, -2147483648, trials);
    return 0;
}


