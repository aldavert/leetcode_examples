#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 0
int maxDivScore(std::vector<int> nums, std::vector<int> divisors)
{
    const int n = static_cast<int>(divisors.size());
    std::unordered_set<int> seen;
    std::unordered_map<int, int> histogram;
    for (int number : nums)
        ++histogram[number];
    int result = 0, maximum_divisor_count = -1;
    for (int i = 0; i < n; ++i)
    {
        if (seen.find(divisors[i]) != seen.end()) continue;
        seen.insert(divisors[i]);
        int number_divisible = 0;
        for (auto b = histogram.begin(), e = histogram.end(); b != e; ++b)
            number_divisible += (b->first % divisors[i] == 0) * b->second;
        if (number_divisible > maximum_divisor_count)
        {
            maximum_divisor_count = number_divisible;
            result = divisors[i];
        }
        else if (number_divisible == maximum_divisor_count)
            result = std::min(result, divisors[i]);
    }
    return result;
}
#elif 1
int maxDivScore(std::vector<int> nums, std::vector<int> divisors)
{
    int result = 0, maximum_divisor_count = -1;
    for (int divisor : divisors)
    {
        int number_divisible = 0;
        for (int number : nums)
            number_divisible += number % divisor == 0;
        if (number_divisible > maximum_divisor_count)
        {
            maximum_divisor_count = number_divisible;
            result = divisor;
        }
        else if (number_divisible == maximum_divisor_count)
            result = std::min(result, divisor);
    }
    return result;
}
#else
int maxDivScore(std::vector<int> nums, std::vector<int> divisors)
{
    std::unordered_set<int> seen;
    std::unordered_map<int, int> histogram;
    for (int number : nums)
        ++histogram[number];
    int result = 0, maximum_divisor_count = -1;
    for (int divisor : divisors)
    {
        if (seen.find(divisor) != seen.end()) continue;
        seen.insert(divisor);
        int number_divisible = 0;
        for (auto [number, frequency] : histogram)
            number_divisible += (number % divisor == 0) * frequency;
        if (number_divisible > maximum_divisor_count)
        {
            maximum_divisor_count = number_divisible;
            result = divisor;
        }
        else if (number_divisible == maximum_divisor_count)
            result = std::min(result, divisor);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> divisors,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxDivScore(nums, divisors);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 7, 9, 3, 9}, {5, 2, 3}, 3, trials);
    test({20, 14, 21, 10}, {5, 7, 5}, 5, trials);
    test({12}, {10, 16}, 10, trials);
    return 0;
}


