#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> nums, int k)
{
    int result = 0;
    for (int value : nums) result += value < k;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({ 2, 11, 10,  1,  3}, 10, 3, trials);
    test({ 1,  1,  2,  4,  9},  1, 0, trials);
    test({ 1,  1,  2,  4,  9},  9, 4, trials);
    return 0;
}


