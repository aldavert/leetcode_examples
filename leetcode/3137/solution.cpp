#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int minimumOperationsToMakeKPeriodic(std::string word, int k)
{
    std::unordered_map<std::string, int> count;
    int max_freq = 0;
    for (int i = 0, n = static_cast<int>(word.size()); i < n; i += k)
        ++count[word.substr(i, k)];
    for (const auto& [_, freq] : count)
        max_freq = std::max(max_freq, freq);
    return static_cast<int>(word.size()) / k - max_freq;
}

// ############################################################################
// ############################################################################

void test(std::string word, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumOperationsToMakeKPeriodic(word, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leetcodeleet", 4, 1, trials);
    test("leetcoleet", 2, 3, trials);
    return 0;
}


