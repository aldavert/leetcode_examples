#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

long long maxKelements(std::vector<int> nums, int k)
{
    std::priority_queue<int> heap;
    for (int num : nums)
        heap.push(num);
    long result = 0;
    for (int i = 0; i < k; ++i)
    {
        int num = heap.top();
        heap.pop();
        result += num;
        heap.push((num + 2) / 3);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxKelements(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 10, 10, 10, 10}, 5, 50, trials);
    test({1, 10, 3, 3, 3}, 3, 17, trials);
    return 0;
}


