#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

#if 0
int minSubArrayLen(int target, std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> accumulated(n + 1);
    for (int i = 1; i <= n; ++i)
        accumulated[i] = accumulated[i - 1] + nums[i - 1];
    auto lowerBound = [&](int low, int s) -> int
    {
        int result = std::numeric_limits<int>::max();
        for (int high = n - 1; low <= high;)
        {
            int mid = (low + high) / 2;
            if (accumulated[mid + 1] - s >= target)
                result = mid,
                high = mid - 1;
            else low = mid + 1;
        }
        return result;
    };
    int result = std::numeric_limits<int>::max();
    for (int l = 0; l < n; ++l)
        result = std::min(result, lowerBound(l, accumulated[l]) - l);
    return (result < n)?result + 1:0;
}
#else
int minSubArrayLen(int target, std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = std::numeric_limits<int>::max();
    int l = 0, r = 0, sum = 0;
    while (r < n)
    {
        if (sum >= target)
            result = std::min(result, r - l),
            sum -= nums[l++];
        else sum += nums[r++];
    }
    while (l < n)
    {
        if (sum >= target)
            result = std::min(result, r - l),
            sum -= nums[l++];
        else break;
    }
    return (result < std::numeric_limits<int>::max())?result:0;
}
#endif

// ############################################################################
// ############################################################################

void test(int target, std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSubArrayLen(target, nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, {2, 3, 1, 2, 4, 3}, 2, trials);
    test(4, {1, 4, 4}, 1, trials);
    test(11, {1, 1, 1, 1, 1, 1, 1, 1}, 0, trials);
    return 0;
}


