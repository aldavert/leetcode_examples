#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

#if 1
int shortestSequence(std::vector<int> rolls, int k)
{
    std::bitset<100'001> seen;
    int result = 1, count = 0;
    for (int roll : rolls)
    {
        count += seen[roll] == false;
        seen[roll] = true;
        if (count == k)
        {
            ++result;
            seen.reset();
            count = 0;
        }
    }
    return result;
}
#else
int shortestSequence(std::vector<int> rolls, int k)
{
    std::unordered_set<int> seen;
    int result = 1;
    for (int roll : rolls)
    {
        seen.insert(roll);
        if (static_cast<int>(seen.size()) == k)
        {
            ++result;
            seen.clear();
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> rolls, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestSequence(rolls, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 1, 2, 3, 3, 2, 4, 1}, 4, 3, trials);
    test({1, 1, 2, 2}, 2, 2, trials);
    test({1, 1, 3, 2, 2, 2, 3, 3}, 4, 1, trials);
    return 0;
}


