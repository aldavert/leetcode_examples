#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numSubarraysWithSum(std::vector<int> nums, int goal)
{
    int result = 0, count[30'002] = {};
    count[0] = 1;
    for (int prefix = 0; int num : nums)
    {
        prefix += num;
        if (int search = prefix - goal; search >= 0)
            result += count[search];
        ++count[prefix];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int goal, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numSubarraysWithSum(nums, goal);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 1, 0, 1}, 2, 4, trials);
    test({0, 0, 0, 0, 0}, 0, 15, trials);
    return 0;
}


