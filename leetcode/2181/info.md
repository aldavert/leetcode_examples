# Merge Nodes in Between Zeros

You are given the `head` of a linked list, which contains a series of integers **separated** by `0`'s. The **beginning** and end of the linked list will have `Node.val == 0`.

For **every** two consecutive `0`'s, **merge** all the nodes lying in between them into a single node whose value is the **sum** of all the merged nodes. The modified list should not contain any `0`'s.

Return *the* `head` *of the modified linked list.*

#### Example 1:
> ```mermaid 
> graph LR;
> A((0))-->B((3))-->C((1))-->D((0))-->E((4))-->F((5))-->G((2))-->I((0))
> classDef green fill:#AFA,stroke:#282,stroke-width:2px;
> classDef red fill:#FAA,stroke:#822,stroke-width:2px;
> classDef default fill:#FFF,stroke:#222,stroke-width:2px;
> class B,C green;
> class E,F,G red;
> ```
> *Input:* `head = [0, 3, 1, 0, 4, 5, 2, 0]`  
> *Output:* `[4, 11]`  
> *Explanation:* The above figure represents the given linked list. The modified list contains
> - The sum of the nodes marked in green: `3 + 1 = 4`.
> - The sum of the nodes marked in red: `4 + 5 + 2 = 11`.

#### Example 2:
> ```mermaid 
> graph LR;
> A((0))-->B((1))-->C((0))-->D((3))-->E((0))-->F((2))-->G((2))-->I((0))
> classDef green fill:#AFA,stroke:#282,stroke-width:2px;
> classDef red fill:#FAA,stroke:#822,stroke-width:2px;
> classDef yellow fill:#FFA,stroke:#882,stroke-width:2px;
> classDef default fill:#FFF,stroke:#222,stroke-width:2px;
> class B green;
> class D red;
> class F,G yellow;
> ```
> *Input:* `head = [0, 1, 0, 3, 0, 2, 2, 0]`  
> *Output:* `[1, 3, 4]`  
> *Explanation:* The above figure represents the given linked list. The modified list contains:
> - The sum of the nodes marked in green: `1 = 1`.
> - The sum of the nodes marked in red: `3 = 3`.
> - The sum of the nodes marked in yellow: `2 + 2 = 4`.

#### Constraints:
- The number of nodes in the list is in the range `[3, 2 * 10^5]`.
- `0 <= Node.val <= 1000`
- There are **no** two consecutive nodes with `Node.val == 0`.
- The **beginning** and **end** of the linked list have `Node.val == 0`.


