#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

ListNode * mergeNodes(ListNode * head)
{
    int sum = 0;
    ListNode * result = nullptr, * last = nullptr;
    for (ListNode * current = head->next; current; current = current->next)
    {
        if (current->val == 0)
        {
            if (sum != 0)
            {
                if (result)
                {
                    last->next = new ListNode(sum);
                    last = last->next;
                }
                else
                {
                    result = last = new ListNode(sum);
                }
            }
            sum = 0;
        }
        else sum += current->val;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> list, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(list);
        ListNode * nw_head = mergeNodes(head);
        result = list2vec(nw_head);
        delete head;
        delete nw_head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 3, 1, 0, 4, 5, 2, 0}, {4, 11}, trials);
    test({0, 1, 0, 3, 0, 2, 2, 0}, {1, 3, 4}, trials);
    return 0;
}


