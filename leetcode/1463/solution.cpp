#include "../common/common.hpp"
#include <fmt/core.h>

// ############################################################################
// ############################################################################

int cherryPickup(std::vector<std::vector<int> > grid)
{
    constexpr int max_size = 71;
    const int rows = static_cast<int>(grid.size());
    const int cols = static_cast<int>(grid[0].size());
    
    int F[max_size][max_size] = {}, lp[max_size] = {}, rp[max_size] = {},
        Ml[max_size][max_size] = {}, Mr[max_size][max_size] = {}, lj, rj;
    auto Max = [&](int from, int to, auto table, int r) -> int
    {
        int result = std::numeric_limits<int>::lowest();
        for (int k = from; k <= to; ++k)
            result = std::max(result, table[r + 1][k]);
        return result;
    };
    auto max = [](int a, int b, int c) -> int { return std::max(std::max(a, b), c); };
    auto min = [](int a, int b, int c) -> int { return std::min(std::min(a, b), c); };
    
    for (int c = 0; c < cols; ++c)
        F[rows - 1][c + 1] = grid[rows - 1][c];
    for (int r = rows - 1; r >= 0; --r)
    {
        F[r][0] = grid[r][0]
                + std::max(F[r + 1][0], F[r + 1][1]);
        F[r][cols - 1] = grid[r][cols - 1]
                       + std::max(F[r + 1][cols - 2], F[r + 1][cols - 1]);
        for (int c = 1; c < cols - 1; ++c)
            F[r][c] = grid[r][c]
                    + max(F[r + 1][c - 1], F[r + 1][c], F[r + 1][c + 1]);
    }
    
    lp[0] = 0;
    rp[0] = cols - 1;
    for (int r = 1; r < rows; ++r)
    {
        lj = lp[r] = lp[r - 1];
        if ((lj > 0) && (F[r][lj - 1] >= F[r][lj]))
            lp[r] = lj - 1;
        if ((lj < cols - 1) && (F[r][lp[r]] <  F[r][lj + 1]))
            lp[r] = lj + 1;
        
        rj = rp[r] = rp[r - 1];
        if ((rj < cols - 1) && (F[r][rj + 1] >= F[r][rj]))
            rp[r] = rj + 1;
        if ((rj > 0) && (F[r][rp[r]] <  F[r][rj - 1]))
            rp[r] = rj - 1;
    }
    
    lj = std::max(rp[rows - 1], lp[rows - 1] + 1);
    for (int c = lj; c < cols; ++c)
        Ml[rows - 1][c] = grid[rows - 1][lp[rows - 1]] + grid[rows - 1][c];
    rj = std::min(lp[rows - 1], rp[rows - 1] + 1);
    for (int c = 0; c <= rj; ++c)
        Mr[rows - 1][c] = grid[rows - 1][rp[rows - 1]] + grid[rows - 1][c];
    for (int r = rows - 1; r >= 0; --r)
    {
        int Mri = Max(std::max(0, lp[r] - 1), lp[r + 1] - 1                , Mr, r);
        int Mli = Max(rp[r + 1] + 1         , std::min(cols - 1, rp[r] + 1), Ml, r);
        for (int c = std::max(lp[r] + 1, rp[r]); c < cols; ++c)
        {
            int inc = 0;
            if (std::max(lp[r], 1) <= lp[r + 1])
                inc = Max(std::max(rp[r + 1], c - 1), std::min(c + 1, cols - 1), F, r)
                    + Mri - F[r + 1][rp[r + 1]];
            if (lp[r + 1] + 2 <= cols)
                inc = std::max(inc, Max(max(c - 1, rp[r + 1], lp[r + 1] + 1),
                                    std::min(c + 1, cols - 1), Ml, r));
            Ml[r][c] = grid[r][lp[r]] + grid[r][c] + inc;
        }
        for (int c = 0; c <= std::min(lp[r], rp[r] - 1); ++c)
        {
            int inc = 0;
            if (rp[r + 1] <= std::min(cols - 2, rp[r]))
                inc = Max(std::max(0, c - 1), std::min(c + 1, lp[r + 1]), F, r)
                    + Mli - F[r + 1][lp[r + 1]];
            if (1 <= rp[r + 1])
                inc = std::max(inc, Max(std::max(0, c - 1),
                                    min(c + 1, lp[r + 1], rp[r + 1] - 1), Mr, r));
            Mr[r][c] = grid[r][rp[r]] + grid[r][c] + inc;
        }
    }
    
#if 0
    //// ########################### DEBUG DEBUG ###########################
    //// ########################### DEBUG DEBUG ###########################
    //// ########################### DEBUG DEBUG ###########################
    fmt::print("{0:─^{1}}\n", "[ GRID ]", std::max(rows, cols) * 4);
    for (int r = 0; r < rows; ++r)
    {
        for (int c = 0; c < cols; ++c)
            fmt::print(" {:3d}", grid[r][c]);
        fmt::print("\n");
    }
    fmt::print("{0:─^{1}}\n", "[ F ]", std::max(rows, cols) * 4);
    for (int r = 0; r < rows; ++r)
    {
        for (int c = 0; c < cols; ++c)
            fmt::print(" {:3d}", F[r][c]);
        fmt::print("\n");
    }
    fmt::print("{0:─^{1}}\n", "[ lp & rp ]", std::max(rows, cols) * 4);
    fmt::print("{0: ^{1}}", "", std::max(0, cols - rows) * 2);
    for (int r = 0; r < rows; ++r)
        fmt::print(" {:3d}", lp[r]);
    fmt::print("\n");
    fmt::print("{0: ^{1}}", "", std::max(0, cols - rows) * 2);
    for (int r = 0; r < rows; ++r)
        fmt::print(" {:3d}", rp[r]);
    fmt::print("\n");
    fmt::print("{0:─^{1}}\n", "[ Ml ]", std::max(rows, cols) * 4);
    for (int r = 0; r < rows; ++r)
    {
        for (int c = 0; c < cols; ++c)
            fmt::print(" {:3d}", Ml[r][c]);
        fmt::print("\n");
    }
    fmt::print("{0:─^{1}}\n", "[ Mr ]", std::max(rows, cols) * 4);
    for (int r = 0; r < rows; ++r)
    {
        for (int c = 0; c < cols; ++c)
            fmt::print(" {:3d}", Mr[r][c]);
        fmt::print("\n");
    }
    fmt::print("{0:─^{1}}\n", "", std::max(rows, cols) * 4);
    //// ########################### DEBUG DEBUG ###########################
    //// ########################### DEBUG DEBUG ###########################
    //// ########################### DEBUG DEBUG ###########################
#endif
    return Ml[0][cols - 1];
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = cherryPickup(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 1, 1},
          {2, 5, 1},
          {1, 5, 5},
          {2, 1, 1}}, 24, trials);
    test({{1, 0, 0, 0, 0, 0, 1},
          {2, 0, 0, 0, 0, 3, 0},
          {2, 0, 9, 0, 0, 0, 0},
          {0, 3, 0, 5, 4, 0, 0},
          {1, 0, 2, 3, 0, 0, 6}}, 28, trials);
    return 0;
}


