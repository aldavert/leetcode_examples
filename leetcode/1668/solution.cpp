#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxRepeating(std::string sequence, std::string word)
{
    if (word.size() >= sequence.size()) return static_cast<int>(sequence == word);
    const int n = static_cast<int>(sequence.size());
    const int m = static_cast<int>(word.size());
    int result = 0;
    for (int i = 0; i < n; ++i)
    {
        int k = 0;
        for (int j = i; (j + m <= n) && (sequence.substr(j, m) == word); j += m, ++k);
        result = std::max(result, k);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string sequence, std::string word, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxRepeating(sequence, word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ababc", "ab", 2, trials);
    test("ababc", "ba", 1, trials);
    test("ababc", "ac", 0, trials);
    test("abab", "ab", 2, trials);
    return 0;
}


