#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> successfulPairs(std::vector<int> spells,
                                 std::vector<int> potions,
                                 long long success)
{
    std::vector<int> result;
    std::sort(potions.begin(), potions.end());
    for (int spell : spells)
    {
        long long threshold = success / static_cast<long long>(spell)
                            + (success % spell != 0);
        int size = 0;
        if (threshold <= potions.back())
        {
            auto position = std::lower_bound(potions.begin(), potions.end(), threshold);
            size = static_cast<int>(std::distance(position, potions.end()));
        }
        result.push_back(size);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> spells,
          std::vector<int> potions,
          long long success,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = successfulPairs(spells, potions, success);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 1, 3}, {1, 2, 3, 4, 5}, 7, {4, 0, 3}, trials);
    test({3, 1, 2}, {8, 5, 8}, 16, {2, 0, 2}, trials);
    return 0;
}


