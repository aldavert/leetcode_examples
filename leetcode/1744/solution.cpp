#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<bool> canEat(std::vector<int> candiesCount, std::vector<std::vector<int> > queries)
{
    const int n = static_cast<int>(candiesCount.size());
    std::vector<bool> result;
    std::vector<long> prefix{0};
    for (int i = 0; i < n; ++i)
        prefix.push_back(prefix.back() + candiesCount[i]);
    for (const auto &query : queries)
        result.push_back((prefix[query[0]] / query[2] <= query[1])
                      && (query[1] <= prefix[query[0] + 1] - 1));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> candiesCount,
          std::vector<std::vector<int> > queries,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = canEat(candiesCount, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({7, 4, 5, 3, 8}, {{0, 2, 2}, {4, 2, 4}, {2, 13, 1000000000}},
         {true, false, true}, trials);
    test({5, 2, 6, 4, 1}, {{3, 1, 2}, {4, 10, 3}, {3, 10, 100}, {4, 100, 30}, {1, 3, 1}},
         {false, true, true, false, false}, trials);
    return 0;
}


