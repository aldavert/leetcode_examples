#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int uniqueLetterString(std::string s)
{
    int prev_count[26] = {}, prev_position[26] = {};
    for (size_t i = 0; i < 26; ++i) prev_position[i] = -1;
    int result = 0;
    for (int i = 0, count = 0, n = static_cast<int>(s.size()); i < n; ++i)
    {
        int chr_idx = s[i] - 'A';
        int current = i - std::exchange(prev_position[chr_idx], i);
        count += current - std::exchange(prev_count[chr_idx], current);
        result += count;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = uniqueLetterString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ABC", 10, trials);
    test("ABA", 8, trials);
    test("LEETCODE", 92, trials);
    return 0;
}


