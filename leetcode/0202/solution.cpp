#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isHappy(int n)
{
    std::unordered_set<int> visited;
    while ((n != 1) && (visited.find(n) == visited.end()))
    {
        visited.insert(n);
        int new_n = 0;
        while (n > 0)
        {
            int r = n % 10;
            new_n += r * r;
            n /= 10;
        }
        n = new_n;
    }
    return n == 1;
}

// ############################################################################
// ############################################################################

void test(int n, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isHappy(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(19, true, trials);
    test(2, false, trials);
    return 0;
}


