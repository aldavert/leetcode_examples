#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maxEnergyBoost(std::vector<int> energyDrinkA, std::vector<int> energyDrinkB)
{
    long dp[2] = {};
    for (size_t i = 0; i < energyDrinkA.size(); ++i)
    {
        long new_dp[2] = {
            std::max(dp[1], dp[0] + energyDrinkA[i]),
            std::max(dp[0], dp[1] + energyDrinkB[i]) };
        dp[0] = new_dp[0];
        dp[1] = new_dp[1];
    }
    return std::max(dp[0], dp[1]);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> energyDrinkA,
          std::vector<int> energyDrinkB,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxEnergyBoost(energyDrinkA, energyDrinkB);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 1}, {3, 1, 1}, 5, trials);
    test({4, 1, 1}, {1, 1, 3}, 7, trials);
    return 0;
}


