#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int punishmentNumber(int n)
{
    auto isPossible =
        [&](auto &&self, int accumulate, int running, const std::string &num_chars,
            int s, int target) -> bool
    {
        if (s == static_cast<int>(num_chars.length()))
            return target == accumulate + running;
        const int d = num_chars[s] - '0';
        return self(self, accumulate, running * 10 + d, num_chars, s + 1, target)
            || self(self, accumulate + running, d, num_chars, s + 1, target);
    };
    int result = 0;
    for (int i = 1; i <= n; ++i)
        if (isPossible(isPossible, 0, 0, std::to_string(i * i), 0, i))
            result += i * i;
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = punishmentNumber(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, 182, trials);
    test(37, 1478, trials);
    return 0;
}


