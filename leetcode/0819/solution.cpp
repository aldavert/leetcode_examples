#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::string mostCommonWord(std::string paragraph, std::vector<std::string> banned)
{
    std::unordered_set<std::string> lut_banned;
    std::unordered_map<std::string, size_t> histogram;
    auto lower = [](std::string str) -> std::string
    {
        std::transform(str.begin(), str.end(), str.begin(),
                       [](char c){return std::tolower(c);});
        return str;
    };
    for (const std::string &b : banned)
        lut_banned.insert(lower(b));
    const size_t n = paragraph.size();
    size_t start = 0, end = 0;
    while (end < n)
    {
        while ((start < n) && !std::isalpha(paragraph[start]))
            ++start;
        end = start + 1;
        while ((end < n) && std::isalpha(paragraph[end]))
            ++end;
        if (start < n)
        {
            std::string current = lower(paragraph.substr(start, end - start));
            if (lut_banned.find(current) == lut_banned.end())
                ++histogram[current];
        }
        start = end + 1;
    }
    size_t max_frequency = 0;
    std::string max_string = "";
    for (auto [word, frequency] : histogram)
    {
        if (frequency > max_frequency)
        {
            max_frequency = frequency;
            max_string = word;
        }
    }
    return max_string;
}

// ############################################################################
// ############################################################################

void test(std::string paragraph,
          std::vector<std::string> banned,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = mostCommonWord(paragraph, banned);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("Bob hit a ball, the hit BALL flew far after it was hit.",
         {"hit"}, "ball", trials);
    test("Bob hit a ball, the hit BALL flew far after it was hit",
         {"hIt"}, "ball", trials);
    test("a.", {}, "a", trials);
    return 0;
}


