#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

std::vector<TreeNode *> delNodes(TreeNode * root, std::vector<int> to_delete)
{
    auto process = [&](void) -> std::vector<TreeNode *>
    {
        std::vector<TreeNode *> result;
        std::unordered_set<int> to_delete_set{to_delete.begin(), to_delete.end()};
        auto inner = [&](auto &&self, TreeNode * node, bool is_root) -> TreeNode *
        {
            if (!node) return nullptr;
            bool deleted = to_delete_set.count(node->val);
            if (is_root && !deleted)
                result.push_back(node);
            node->left  = self(self, node->left , deleted);
            node->right = self(self, node->right, deleted);
            return (deleted)?nullptr:node;
        };
        inner(inner, root, true);
        return result;
    };
    return process();
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          std::vector<int> to_delete,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        std::vector<TreeNode *> output = delNodes(root, to_delete);
        for (auto node : output)
            result.push_back(tree2vec(node));
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6, 7}, {3, 5}, {{1, 2, null, 4}, {6}, {7}}, trials);
    test({1, 2, 4, null, 3}, {3}, {{1, 2, 4}}, trials);
    return 0;
}


