#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumPartition(std::string s, int k)
{
    int result = 1;
    for (long current = 0; char c : s)
    {
        current = current * 10 + c - '0';
        if (current > k)
        {
            current = c - '0';
            ++result;
        }
        if (current > k) return -1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumPartition(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("165462", 60, 4, trials);
    test("238182", 5, -1, trials);
    return 0;
}


