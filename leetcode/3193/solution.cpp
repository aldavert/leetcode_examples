#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int numberOfPermutations(int n, std::vector<std::vector<int> > requirements)
{
    constexpr int MOD = 1'000'000'007;
    std::sort(requirements.begin(), requirements.end());
    int dp[2][401] = { 1 }, prev = 0, j = 0;
    for (int i = 1; i <= n; ++i)
    {
        for (int k = prev; k <= requirements[j][1]; ++k)
        {
            dp[i % 2][k] = (dp[(i - 1) % 2][k] + ((k > 0)?dp[i % 2][k - 1]:0)) % MOD;
            if (k >= i)
                dp[i % 2][k] = (MOD + dp[i % 2][k] - dp[(i - 1) % 2][k - i]) % MOD;
        }
        if (i - 1 == requirements[j][0])
        {
            std::fill_n(&dp[0][0] + prev, requirements[j][1] - prev, 0);
            std::fill_n(&dp[1][0] + prev, requirements[j][1] - prev, 0);
            prev = requirements[j++][1];
        }
    }  
    return dp[n % 2][requirements.back()[1]];
}
#else
int numberOfPermutations(int n, std::vector<std::vector<int> > requirements)
{
    constexpr int MOD = 1'000'000'007;
    std::vector<int> end_to_count(n, -1);
    int max_inversions = 0;
    for (const auto &requirement : requirements)
    {
        end_to_count[requirement[0]] = requirement[1];
        max_inversions = std::max(max_inversions, requirement[1]);
    }
    if (end_to_count[0] > 0) return 0;
    end_to_count[0] = 0;
    std::vector<std::vector<int> > dp(n, std::vector<int>(max_inversions + 1, 0));
    dp[0][0] = 1;
    for (int i = 1; i < n; ++i)
    {
        int l = 0, r = max_inversions;
        if (end_to_count[i] >= 0)
            l = r = end_to_count[i];
        for (int j = l; j <= r; ++j)
            for (int k = 0; k <= std::min(i, j); ++k)
                dp[i][j] = (dp[i][j] + dp[i - 1][j - k]) % MOD;
    }
    return dp[n - 1][end_to_count[n - 1]];
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > requirements,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfPermutations(n, requirements);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{2, 2}, {0, 0}}, 2, trials);
    test(3, {{2, 2}, {1, 1}, {0, 0}}, 1, trials);
    test(2, {{0, 0}, {1, 0}}, 1, trials);
    test(3, {{2, 2}, {0, 1}}, 0, trials);
    return 0;
}


