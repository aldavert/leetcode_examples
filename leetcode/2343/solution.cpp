#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<int> smallestTrimmedNumbers(std::vector<std::string> nums,
                                        std::vector<std::vector<int> > queries)
{
    std::vector<int> cache[10], buf;
    const int n = static_cast<int>(nums.size()),
              m = static_cast<int>(nums.front().size());
    
    std::vector<int> result;
    for (int count = 0, pre_count = 0; auto &itr : queries)
    {
        buf.clear();
        pre_count = 0;
        for (int i = 0; i < n; ++i) buf.push_back(i);
        for (int i = m - itr[1]; i < m; ++i)
        {
            for (int k = 0; k < 10; ++k) cache[k].clear();
            for (int j : buf) cache[nums[j][i] - '0'].push_back(j);
            count = pre_count;
            for (int j = 0; j < 10; ++j)
            {
                if (cache[j].empty()) continue;
                pre_count = count;
                count += static_cast<int>(cache[j].size());
                if (count >= itr[0])
                {
                    buf = cache[j];
                    break;
                }
            }
            if (buf.size() == 1) break;
        }
        result.push_back(buf[itr[0] - pre_count - 1]);
    }
    return result;
}
#else
std::vector<int> smallestTrimmedNumbers(std::vector<std::string> nums,
                                        std::vector<std::vector<int> > queries)
{
    const int n = static_cast<int>(nums.size());
    auto kSmallestIndex = [&](int k, int trim) ->  int
    {
        const int start_index = static_cast<int>(nums[0].size()) - trim;
        std::vector<std::pair<std::string, int> > trimmed;
        for (int i = 0; i < n; ++i)
            trimmed.push_back({nums[i].substr(start_index), i});
        std::sort(trimmed.begin(), trimmed.end());
        return trimmed[k - 1].second;
    };
    std::vector<int> result;
    for (const auto &query : queries)
        result.push_back(kSmallestIndex(query[0], query[1]));
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> nums,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestTrimmedNumbers(nums, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"102", "473", "251", "814"}, {{1, 1}, {2, 3}, {4, 2}, {1, 2}},
         {2, 2, 1, 0}, trials);
    test({"24", "37", "96", "04"}, {{2, 1}, {2, 2}}, {3, 0}, trials);
    return 0;
}


