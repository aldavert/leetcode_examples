#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string reversePrefix(std::string word, char ch)
{
    const size_t n = word.size();
    size_t pos = 0;
    while ((pos < n) && (word[pos] != ch)) ++pos;
    if (pos != n)
        for (size_t i = 0; i < pos; ++i, --pos) std::swap(word[i], word[pos]);
    return word;
}

// ############################################################################
// ############################################################################

void test(std::string word, char ch, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = reversePrefix(word, ch);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcdefd", 'd', "dcbaefd", trials);
    test("xyxzxe", 'z', "zxyxxe", trials);
    test("abcd", 'z', "abcd", trials);
    return 0;
}


