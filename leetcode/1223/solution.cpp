#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int dieSimulator(int n, std::vector<int> rollMax)
{
    constexpr int MOD = 1'000'000'007;
    int dp[5001][6] = {};
    for (int i = 0; i < 6; ++i) dp[1][i] = 1;
    for (int i = 2; i <= n; ++i)
    {
        for (int j = 0; j < 6; ++j)
        {
            int result = 0;
            for (int k = 0; k < 6; ++k)
                result = (result + dp[i - 1][k]) % MOD;
            int pre = i - rollMax[j] - 1;
            if (pre >= 1)
            {
                for (int k = 0; k < 6; ++k)
                {
                    if (k == j) continue;
                    result = (result + (MOD - dp[pre][k])) % MOD;
                }
            }
            else result -= (pre == 0);
            dp[i][j] = result % MOD;
        }
    }
    int result = 0;
    for (int i = 0; i < 6; ++i) result = (result + dp[n][i]) % MOD;
    return result;
}
#else
int dieSimulator(int n, std::vector<int> rollMax)
{
    constexpr int MAX_ROLLS = 15;
    constexpr int MOD = 1'000'000'007;
    
    std::vector<std::vector<std::vector<int> > > dp(n + 1,
            std::vector<std::vector<int> >(6, std::vector<int>(MAX_ROLLS + 1, 0)));
    for (int i = 0; i < 6; ++i) dp[1][i][1] = 1;
    for (int i = 2; i < n + 1; ++i)
    {
        for (int curr_num = 0; curr_num < 6; ++curr_num)
        {
            for (int prev_num = 0; prev_num < 6; ++prev_num)
            {
                for (int k = 1; k < MAX_ROLLS + 1; ++k)
                {
                    if (prev_num != curr_num) dp[i][curr_num][1] =
                        (dp[i][curr_num][1] + dp[i - 1][prev_num][k]) % MOD;
                    else if (k < rollMax[curr_num])
                        dp[i][curr_num][k + 1] = dp[i - 1][curr_num][k];
                }
            }
        }
    }
    int result = 0;
    for (int i = 0; i < 6; ++i)
        for (int k = 1; k < MAX_ROLLS + 1; ++k)
            result = (result + dp[n][i][k]) % MOD;
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int n, std::vector<int> rollMax, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = dieSimulator(n, rollMax);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {1, 1, 2, 2, 2, 3},  34, trials);
    test(2, {1, 1, 1, 1, 1, 1},  30, trials);
    test(3, {1, 1, 1, 2, 2, 3}, 181, trials);
    test(20, {8, 5, 10, 8, 7, 2}, 822005673, trials);
    return 0;
}


