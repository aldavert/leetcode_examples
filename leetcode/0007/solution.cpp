#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int reverse(int x)
{
    constexpr int THR = std::numeric_limits<int>::max() / 10;
    int result = 0;
    while (x)
    {
        if ((result > THR) || (result < -THR)) return 0;
        result = result * 10 + x % 10;
        x /= 10;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int x, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = reverse(x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(123, 321, trials);
    test(-123, -321, trials);
    test(120, 21, trials);
    test(1'534'236'469, 0, trials);
    // Maximum: 2'147'483'647
    test(2'147'483'641, 1'463'847'412, trials);
    test(1'463'847'412, 2'147'483'641, trials);
    test(-2'147'483'648, 0, trials);
    return 0;
}


