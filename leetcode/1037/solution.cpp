#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isBoomerang(std::vector<std::vector<int> > points)
{
    return std::abs(points[0][0] * (points[1][1] - points[2][1])
                  + points[1][0] * (points[2][1] - points[0][1])
                  + points[2][0] * (points[0][1] - points[1][1])) > 0;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isBoomerang(points);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1}, {2, 3}, {3, 2}},  true, trials);
    test({{1, 1}, {2, 2}, {3, 3}}, false, trials);
    return 0;
}


