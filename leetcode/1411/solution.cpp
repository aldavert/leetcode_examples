#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numOfWays(int n)
{
    constexpr int MOD = 1'000'000'007;
    long color2 = 6, color3 = 6;
    for (int i = 1; i < n; ++i)
    {
        long next_color2 = color2 * 3 + color3 * 2;
        long next_color3 = color2 * 2 + color3 * 2;
        color2 = next_color2 % MOD;
        color3 = next_color3 % MOD;
    }
    return static_cast<int>((color2 + color3) % MOD);
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numOfWays(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 12, trials);
    test(5000, 30228214, trials);
    return 0;
}


