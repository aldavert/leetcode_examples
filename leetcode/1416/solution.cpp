#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfArrays(std::string s, int k)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(s.size());
    std::vector<int> dp(n + 1);
    dp[n] = 1;
    for (int i = n - 1; i >= 0; --i)
    {
        if (s[i] == '0') continue;
        long num = 0;
        for (int j = i; j < n; ++j)
        {
            num = num * 10 + (s[j] - '0');
            if (num > k) break;
            dp[i] = (dp[i] + dp[j + 1]) % MOD;
        }
    }
    return dp[0];
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfArrays(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1000", 10'000, 1, trials);
    test("1000", 10, 0, trials);
    test("1317", 2'000, 8, trials);
    return 0;
}


