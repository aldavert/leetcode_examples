#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int largestPerimeter(std::vector<int> nums)
{
    std::sort(nums.begin(), nums.end());
    for (int i = static_cast<int>(nums.size()) - 1; i > 1; --i)
        if (nums[i - 2] + nums[i - 1] > nums[i])
            return nums[i - 2] + nums[i - 1] + nums[i];
    return 0;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestPerimeter(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 2}, 5, trials);
    test({1, 2, 1}, 0, trials);
    return 0;
}


