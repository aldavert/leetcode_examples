#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string convertToBase7(int num)
{
    std::string result;
    bool negative = num < 0;
    num = num * (-1 * (2 * negative - 1));
    while (num >= 7)
    {
        result += std::to_string(num % 7);
        num /= 7;
    }
    result += std::to_string(num);
    if (negative) result += "-";
    std::reverse(result.begin(), result.end());
    return result;
}

// ############################################################################
// ############################################################################

void test(int num, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = convertToBase7(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(100, "202", trials);
    test(-7, "-10", trials);
    return 0;
}


