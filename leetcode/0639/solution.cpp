#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numDecodings(std::string s)
{
    const int n = static_cast<int>(s.size());
    auto updateW = [](long current, long weight, long previous)
    {
        constexpr long limit = 1'000'000'007;
        return (current + (weight * previous) % limit) % limit;
    };
    auto update1 = [](long current, long previous)
    {
        constexpr long limit = 1'000'000'007;
        return (current + previous) % limit;
    };
    
    if (s[0] == '0') return 0;
    long previous[2] = {1, (s[0] == '*')?9:1};
    for (int i = 2; i <= n; ++i)
    {
        char curr = s[i - 1], prev = s[i - 2];
        long value = 0;
        
        if      (curr == '*') value = updateW(value, 9, previous[1]);
        else if (curr != '0') value = update1(value   , previous[1]);
        if(prev=='*')
        {
            if      (curr == '*') value = updateW(value, 15, previous[0]);
            else if (curr <= '6') value = updateW(value,  2, previous[0]);
            else                  value = update1(value    , previous[0]);
        }
        else if (prev == '1')
        {
            if (curr == '*') value = updateW(value, 9, previous[0]);
            else             value = update1(value   , previous[0]);
        }
        else if (prev == '2')
        {
            if      (curr == '*') value = updateW(value, 6, previous[0]);
            else if (curr <= '6') value = update1(value   , previous[0]);
        }
        previous[0] = previous[1];
        previous[1] = value;
    }
    return static_cast<int>(previous[1]);
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numDecodings(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    // 11106 => |11| 10 6 =>  1 1 10 6 AND 11 10 6
    test("11106", 2, trials);
    test("*", 9, trials);
    test("1*", 18, trials);
    test("2*", 15, trials);
    test("**", 81 + 9 + 6, trials);
    return 0;
}


