#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> findFrequentTreeSum(TreeNode * root)
{
    std::unordered_map<int, int> histogram;
    auto process = [&histogram,&root](void) -> void
    {
        auto inner = [&histogram](auto &&self, TreeNode * node) -> int
        {
            if (node == nullptr) return 0;
            int sum = node->val
                    + self(self, node->left)
                    + self(self, node->right);
            ++histogram[sum];
            return sum;
        };
        inner(inner, root);
    };
    process();
    std::vector<int> result;
    int max_frequency = 0;
    for (auto [sum, frequency] : histogram)
        max_frequency = std::max(max_frequency, frequency);
    for (auto [sum, frequency] : histogram)
        if (frequency == max_frequency)
            result.push_back(sum);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<int> left_work = left, right_work = right;
    std::sort(left_work.begin(), left_work.end());
    std::sort(right_work.begin(), right_work.end());
    for (size_t i = 0, n = left.size(); i < n; ++i)
        if (left_work[i] != right_work[i])
            return false;
    return true;
}

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = findFrequentTreeSum(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 2, -3}, {2, -3, 4}, trials);
    test({5, 2, -5}, {2}, trials);
    return 0;
}


