#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minNonZeroProduct(int p)
{
    constexpr int MOD = 1'000'000'007;
    auto modPow = [&](auto &&self, long x, long n) -> long
    {
        if (n == 0) return 1L;
        x %= MOD;
        if (n % 2 == 1) return x * self(self, x, n - 1) % MOD;
        return self(self, x * x, n / 2) % MOD;
    };
    const long n = 1L << p;
    return static_cast<int>(modPow(modPow, n - 2, n / 2 - 1) * ((n - 1) % MOD) % MOD);
}

// ############################################################################
// ############################################################################

void test(int p, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minNonZeroProduct(p);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 1, trials);
    test(2, 6, trials);
    test(3, 1512, trials);
    return 0;
}


