#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string removeOccurrences(std::string s, std::string part)
{
    const int n = static_cast<int>(s.size()),
              k = static_cast<int>(part.size());
    std::string result(n, ' ');
    
    int j = 0;
    for (int i = 0; i < n; ++i)
    {
        result[j++] = s[i];
        if ((j >= k) && (result.substr(j - k, k) == part))
            j -= k;
    }
    return result.substr(0, j);
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string part, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = removeOccurrences(s, part);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("daabcbaabcbc", "abc", "dab", trials);
    test("axxxxyyyyb", "xy", "ab", trials);
    return 0;
}


