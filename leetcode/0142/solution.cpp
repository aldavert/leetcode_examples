#include "../common/common.hpp"
#include "../common/list.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 0
ListNode * detectCycle(ListNode * head)
{
    if (!head) return nullptr;
    constexpr int ADD = 200'000;
    constexpr int THR = 100'000;
    ListNode * tail = head;
    for (; tail && tail->next && (tail->next->val < THR); tail = tail->next)
        tail->val += ADD;
    for (ListNode * current = head; current != tail; current = current->next)
        current->val -= ADD;
    return tail->next;
}
#elif 1
ListNode * detectCycle(ListNode * head)
{
    for (ListNode * slow = head, * fast = head; fast && fast->next;)
    {
        slow = slow->next;
        fast = fast->next->next;
        if (slow == fast)
        {
            slow = head;
            for (; slow != fast; slow = slow->next, fast = fast->next);
            return slow;
        }
    }
    return nullptr;
}
#else
ListNode * detectCycle(ListNode * head)
{
    std::unordered_set<ListNode *> visited;
    for (ListNode * current = head; current; current = current->next)
    {
        if (visited.find(current->next) != visited.end())
            return current->next;
        visited.insert(current);
    }
    return nullptr;
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    std::unordered_set<int> lut;
    for (int l : left)
        lut.insert(l);
    for (int r : right)
        if (lut.find(r) == lut.end())
            return false;
    return true;
}

void test(std::vector<int> list, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        std::unordered_map<ListNode *, int> lut;
        ListNode * head = vec2list(list);
        std::vector<ListNode *> head_access;
        for (ListNode * current = head; current; current = current->next)
        {
            head_access.push_back(current);
            lut[current] = static_cast<int>(head_access.size()) - 1;
        }
        lut[nullptr] = -1;
        if ((head != nullptr) && (solution != -1))
            head_access.back()->next = head_access[solution];
        
        result = lut[detectCycle(head)];
        
        head_access.back()->next = nullptr;
        delete head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 0, -4}, 1, trials);
    test({1, 2}, 0, trials);
    test({1}, -1, trials);
    return 0;
}


