#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

int totalSteps(std::vector<int> nums)
{
    std::vector<int> dp(nums.size());
    std::stack<int> stack;
    
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        int step = 1;
        while (!stack.empty() && (nums[stack.top()] <= nums[i]))
            step = std::max(step, dp[stack.top()] + 1),
            stack.pop();
        if (!stack.empty())
            dp[i] = step;
        stack.push(i);
    }
    return *std::max_element(dp.begin(), dp.end());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = totalSteps(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 3, 4, 4, 7, 3, 6, 11, 8, 5, 11}, 3, trials);
    test({4, 5, 7, 7, 13}, 0, trials);
    return 0;
}


