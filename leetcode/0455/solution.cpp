#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findContentChildren(std::vector<int> g, std::vector<int> s)
{
    int result = 0;
    std::sort(g.begin(), g.end());
    std::sort(s.begin(), s.end());
    for (size_t pos = 0; int greed : g)
    {
        while ((pos < s.size()) && (s[pos] < greed)) ++pos;
        if (pos >= s.size()) break;
        ++result;
        ++pos;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> g,
          std::vector<int> s,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findContentChildren(g, s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, {1, 1}, 1, trials);
    test({1, 2}, {1, 2, 3}, 2, trials);
    test({1, 2, 3}, {3}, 1, trials);
    return 0;
}


