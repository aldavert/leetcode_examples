#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

#if 1
std::vector<std::vector<int> >
differenceOfDistinctValues(std::vector<std::vector<int> > grid)
{
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>(grid[0].size());
    std::vector<std::vector<int> > result(n_rows, std::vector<int>(n_cols, 0));
    for (int i = 0; i < n_rows; ++i)
    {
        for (int j = 0; j < n_cols; ++j)
        {
            std::bitset<51> diagonal_left, diagonal_right;
            for (int tr = i - 1, tc = j - 1; (tr >= 0) && (tc >= 0); --tr, --tc)
                diagonal_left.set(grid[tr][tc]);
            for (int tr = i + 1, tc = j + 1; (tr < n_rows) && (tc < n_cols); ++tr, ++tc)
                diagonal_right.set(grid[tr][tc]);
            result[i][j] = std::abs(static_cast<int>(diagonal_left.count())
                                  - static_cast<int>(diagonal_right.count()));
        }
    }
    return result;
}
#else
std::vector<std::vector<int> >
differenceOfDistinctValues(std::vector<std::vector<int> > grid)
{
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    std::vector<std::vector<int> > result(m, std::vector<int>(n));
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            int x = i, y = j;
            std::unordered_set<int> s;
            while ((x > 0) && (y > 0))
                s.insert(grid[--x][--y]);
            int tl = static_cast<int>(s.size());
            x = i;
            y = j;
            s.clear();
            while ((x < m - 1) && (y < n - 1))
                s.insert(grid[++x][++y]);
            int br = static_cast<int>(s.size());
            result[i][j] = std::abs(tl - br);
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = differenceOfDistinctValues(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {3, 1, 5}, {3, 2, 1}}, {{1, 1, 0}, {1, 0, 1}, {0, 1, 1}}, trials);
    test({{1}}, {{0}}, trials);
    return 0;
}


