#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<int> sumPrefixScores(std::vector<std::string> words)
{
    const int n = static_cast<int>(words.size());
    std::vector<int> result(n);
    std::vector<std::pair<std::string, int> > v;
    for (int i = 0; i < n; ++i)
        v.push_back({words[i], i});
    
    auto cmp = [&](const std::pair<std::string, int> &l,
                   const std::pair<std::string, int> &r)
    {
        return l.first < r.first;
    };
    std::sort(v.begin(), v.end(), cmp);
    auto calPrefixScore = [&](void) -> void
    {
        std::vector<int> dp(v[0].first.size());
        
        for (int i = 1; i < n; ++i)
        {
            auto [s, idx] = v[i];
            std::vector<int> ndp = std::vector<int>(s.size(), 0);
            
            for (size_t j = 0; j < std::min(s.size(), dp.size()); ++j)
            {
                if (s[j] != v[i - 1].first[j]) break;
                ndp[j] = dp[j] + 1;
                result[idx] += ndp[j];
            }
            dp = ndp;
        }
    };
    
    calPrefixScore();
    std::reverse(v.begin(), v.end());
    calPrefixScore();
    for (auto [s, idx] : v) result[idx] += static_cast<int>(s.size());
    return result;
}
#else
std::vector<int> sumPrefixScores(std::vector<std::string> words)
{
    class Trie
    {
        struct Node
        {
            Node(void) = default;
            ~Node(void) { for (int i = 0; i < 26; ++i) delete links[i]; }
            Node * links[26] = {};
            int score = 0;
            inline bool containkey(char ch) const { return links[ch - 'a'] != nullptr; }
            inline void put(char ch, Node * node) { links[ch - 'a'] = node; }
            inline Node * get(char ch) { return links[ch - 'a']; }
            inline void increase(void) { ++score; }
        };
    public:
        Node root;
        Trie(void) = default;
        void add(std::string s)
        {
            Node * node = &root;
            for (char c : s)
            {
                if (!node->containkey(c))
                    node->put(c, new Node());
                node = node->get(c);
                node->increase();
            }
        }
        int score(std::string s)
        {
            Node * node = &root;
            int result = 0;
            for (char c : s)
            {
                node = node->get(c);
                result += node->score;
            }
            return result;
        }
    };
    Trie trie;
    for (auto &word : words) trie.add(word);
    std::vector<int> result;
    for (auto &word : words)
        result.push_back(trie.score(word));
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumPrefixScores(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"abc", "ab", "bc", "b"}, {5, 4, 3, 2}, trials);
    test({"abcd"}, {4}, trials);
    return 0;
}


