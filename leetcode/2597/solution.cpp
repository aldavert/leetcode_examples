#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int beautifulSubsets(std::vector<int> nums, int k)
{
    std::unordered_map<int, std::set<int> > mod_to_subset;
    int count[1001] = {};
    
    for (int num : nums)
    {
        ++count[num];
        mod_to_subset[num % k].insert(num);
    }
    int prev_num = -k, skip = 0, pick = 0;
    for (const auto& [_, subset] : mod_to_subset)
    {
        for (int num : subset)
        {
            int non_empty_count = (1 << count[num]) - 1,
                cache_skip = skip;
            skip += pick;
            pick = non_empty_count * (1 + cache_skip + ((num - prev_num == k)?0:pick));
            prev_num = num;
        }
    }
    return skip + pick;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = beautifulSubsets(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4, 6}, 2, 4, trials);
    test({1}, 1, 1, trials);
    return 0;
}


