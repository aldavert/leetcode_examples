#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> beautifulArray(int n)
{
    std::vector<int> result[2];
    result[0].resize(n);
    result[1].resize(n);
    result[0][0] = 1;
    bool selected = false;
    for (int s = 1; s < n; selected = !selected)
    {
        int o = 0;
        for (int i = 0; i < s; ++i)
            if (int val = result[selected][i] * 2 - 1; val <= n)
                result[!selected][o++] = val;
        for (int i = 0; i < s; ++i)
            if (int val = result[selected][i] * 2; val <= n)
                result[!selected][o++] = val;
        s = o;
    }
    return result[selected];
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<int> solution, unsigned int trials = 1)
{
    auto valid = [](const std::vector<int> &vec) -> bool
    {
        for (size_t i = 0; i < vec.size(); ++i)
        {
            if ((vec[i] <= 0) || (vec[i] > static_cast<int>(vec.size()))) return false;
            for (size_t j = i + 2; j < vec.size(); ++j)
                for (size_t k = i + 1; k < j; ++k)
                    if (2 * vec[k] == vec[i] + vec[j])
                        return false;
        }
        return true;
    };
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = beautifulArray(n);
    if (solution == result)
        std::cout << showResult(true) << " expected " << solution
                  << " and obtained " << result << ".\n";
    else if (valid(result))
        std::cout << showResult(true) << " " << result << " is a beautiful array.\n";
    else std::cout << showResult(false) << " expected " << solution
                   << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {2, 1, 4, 3}, trials);
    test(5, {3, 1, 2, 5, 4}, trials);
    return 0;
}


