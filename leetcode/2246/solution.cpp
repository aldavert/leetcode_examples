#include "../common/common.hpp"
#include <unordered_map>
#include <queue>

// ############################################################################
// ############################################################################

int longestPath(std::vector<int> parent, std::string s)
{
    const int n = static_cast<int>(parent.size());
    std::unordered_map<int, std::vector<int> > lut_children;
    for (int i = 1; i < n; ++i)
        lut_children[parent[i]].push_back(i);
    std::queue<std::tuple<int, int> > q;
    for (int children : lut_children[0])
        q.push({0, children});
    std::string labels_original(s);
    for (int index = 1; !q.empty(); ++index)
    {
        auto [parent_index, node] = q.front();
        q.pop();
        parent[index] = parent_index;
        s[index] = labels_original[node];
        if (auto search = lut_children.find(node); search != lut_children.end())
            for (int children : search->second)
                q.push({index, children});
    }
    std::vector<int> length(n, 1);
    int result = 1;
    for (int i = n - 1; i > 0; --i)
    {
        if (s[parent[i]] != s[i])
        {
            result = std::max(result, length[parent[i]] + length[i]);
            length[parent[i]] = std::max(length[parent[i]], length[i] + 1);
            result = std::max(result, length[parent[i]]);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> parent,
          std::string s,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestPath(parent, s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, 0, 0, 1, 1, 2}, "abacbe", 3, trials);
    test({-1, 0, 0, 0}, "aabc", 3, trials);
    test({-1}, "z", 1, trials);
    test({  -1, 137,  65,  60,  73, 138,  81,  17,  45, 163, // __0 .. __9
           145,  99,  29, 162,  19,  20, 132, 132,  13,  60, // _10 .. _19
            21,  18, 155,  65,  13, 163, 125, 102,  96,  60, // _20 .. _29
            50, 101, 100,  86, 162,  42, 162,  94,  21,  56, // _30 .. _39
            45,  56,  13,  23, 101,  76,  57,  89,   4, 161, // _40 .. _49
            16, 139,  29,  60,  44, 127,  19,  68,  71,  55, // _50 .. _59
            13,  36, 148, 129,  75,  41, 107,  91,  52,  42, // _60 .. _69
            93,  85, 125,  89, 132,  13, 141,  21, 152,  21, // _70 .. _79
            79, 160, 130, 103,  46,  65,  71,  33, 129,   0, // _80 .. _89
            19, 148,  65, 125,  41,  38, 104, 115, 130, 164, // _90 .. _99
           138, 108,  65,  31,  13,  60,  29, 116,  26,  58, // 100 .. 109
           118,  10, 138,  14,  28,  91,  60,  47,   2, 149, // 110 .. 119
            99,  28, 154,  71,  96,  60, 106,  79, 129,  83, // 120 .. 129
            42, 102,  34,  41,  55,  31, 154,  26,  34, 127, // 130 .. 139
            42, 133, 113, 125, 113,  13,  54, 132,  13,  56, // 140 .. 149
            13,  42, 102, 135, 130,  75,  25,  80, 159,  39, // 150 .. 159
            29,  41,  89,  85,  19},                         // 160 .. 164
         "ajunvefrdrpgxltugqqrwisyfwwtldxjgaxsbbkh"
         "vuqeoigqssefoyngykgtthpzvsxgxrqedntvsjcp"
         "dnupvqtroxmbpsdwoswxfarnixkvcimzgvrevxnx"
         "tkkovwxcjmtgqrrsqyshxbfxptuvqrytctujnzzy"
         "dhpal", 17, trials);
    return 0;
}


