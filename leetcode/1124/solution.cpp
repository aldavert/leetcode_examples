#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int longestWPI(std::vector<int> hours)
{
    std::unordered_map<int, int> map;
    int result = 0;
    
    for (int i = 0, prefix = 0, n = static_cast<int>(hours.size()); i < n; ++i)
    {
        prefix += 2 * (hours[i] > 8) - 1;
        if (prefix > 0) result = i + 1;
        else
        {
            if (!map.count(prefix)) map[prefix] = i;
            if (const auto it = map.find(prefix - 1); it != map.cend())
                result = std::max(result, i - it->second);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> hours, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestWPI(hours);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({9, 9, 6, 0, 6, 6, 9}, 3, trials);
    test({6, 6, 6}, 0, trials);
    return 0;
}


