#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int makeTheIntegerZero(int num1, int num2)
{
    for (long ops = 0; ops <= 60; ++ops)
    {
        const long target = num1 - ops * num2;
        if ((std::popcount(static_cast<unsigned long>(target)) <= ops)
        &&  (ops <= target))
            return static_cast<int>(ops);
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(int num1, int num2, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = makeTheIntegerZero(num1, num2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, -2, 3, trials);
    test(5, 7, -1, trials);
    return 0;
}


