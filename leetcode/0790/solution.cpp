#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int numTilings(int n)
{
    constexpr int MOD = 1'000'000'007;
    if (n < 3) return n;
    
    // p(k) = p(k - 1) + f(k - 2)
    // f(k) = f(k - 1) + f(k - 2) + 2 * p(k - 1)
    // f(1) = 1; f(2) = 2; p(2) = 1
    long p_curr, p_prev, f_curr, f_prev, f_avan;
    f_avan = 1;
    f_prev = 2;
    p_prev = 1;
    for (int i = 2; i < n; ++i)
    {
        f_curr = (f_prev + f_avan + 2 * p_prev) % MOD;
        p_curr = (p_prev + f_avan) % MOD;
        f_avan = f_prev;
        f_prev = f_curr;
        p_prev = p_curr;
    }
    return static_cast<int>(f_curr);
}
#else
int numTilings(int n)
{
    constexpr int MOD = 1'000'000'007;
    if (n < 3) return n;
    
    std::vector<std::vector<long> > dp(n + 1, std::vector<long>(3));
    dp[0][0] = 1;
    dp[0][1] = dp[0][2] = 0;
    dp[1][0] = dp[1][1] = dp[1][2] = 1;
    for (int i = 2; i <= n; ++i)
    {
        dp[i][0] = (dp[i - 1][0] + dp[i - 2][0] + dp[i - 2][1] + dp[i - 2][2]) % MOD;
        dp[i][1] = (dp[i - 1][0] + dp[i - 1][2]) % MOD;
        dp[i][2] = (dp[i - 1][0] + dp[i - 1][1]) % MOD;
    }
    return static_cast<int>(dp[n][0]);
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numTilings(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    constexpr int solutions[] = {1, 2, 5, 11, 24, 53, 117, 258, 569, 1255, 2768,
                                 6105, 13465, 29698, 65501, 144467, 318632, 702765,
                                 1549997, 3418626 };
    test(3, 5, trials);
    test(1, 1, trials);
    test(30, 312'342'182, trials);
    std::cout << '\n';
    std::cout << "Testing all from 1 to 20...\n";
    std::cout << "======================================================\n";
    for (int i = 1; i <= 20; ++i)
        test(i, solutions[i - 1], trials);
    std::cout << "======================================================\n";
    return 0;
}


