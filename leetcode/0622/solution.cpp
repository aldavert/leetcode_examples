#include "../common/common.hpp"
#include <memory>

constexpr int null = -5'000'000;

// ############################################################################
// ############################################################################

class MyCircularQueue
{
    std::vector<int> m_elements;
    size_t m_begin = 0;
    size_t m_end = 0;
    size_t m_size = 0;
public:
    MyCircularQueue(int k) :
        m_elements(k),
        m_begin(0),
        m_end(k - 1),
        m_size(0)
    {
    }
    bool enQueue(int value)
    {
        if (m_size < m_elements.size())
        {
            if (++m_end == m_elements.size()) m_end = 0;
            ++m_size;
            m_elements[m_end] = value;
            return true;
        }
        else return false;
    }
    bool deQueue(void)
    {
        if (m_size != 0)
        {
            if (++m_begin == m_elements.size()) m_begin = 0;
            --m_size;
            return true;
        }
        else return false;
    }
    int Front(void) { return (m_size !=0)?m_elements[m_begin]:-1; }
    int Rear(void) { return (m_size != 0)?m_elements[m_end]:-1; }
    bool isEmpty() { return m_size == 0; }
    bool isFull() { return m_size == m_elements.size(); }
};

// ############################################################################
// ############################################################################

enum class CMD { MYCIRCULARQUEUE, ENQUEUE, DEQUEUE, ISEMPTY, ISFULL, FRONT, REAR };

void test(std::vector<CMD> commands,
          std::vector<int> input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        std::vector<int> output;
        std::unique_ptr<MyCircularQueue> queue;
        const size_t n = commands.size();
        for (size_t j = 0; j < n; ++j)
        {
            switch (commands[j])
            {
            case CMD::MYCIRCULARQUEUE:
                queue = std::make_unique<MyCircularQueue>(input[j]);
                output.push_back(null);
                break;
            case CMD::ENQUEUE:
                output.push_back(queue->enQueue(input[j]));
                break;
            case CMD::DEQUEUE:
                output.push_back(queue->deQueue());
                break;
            case CMD::ISEMPTY:
                output.push_back(queue->isEmpty());
                break;
            case CMD::ISFULL:
                output.push_back(queue->isFull());
                break;
            case CMD::FRONT:
                output.push_back(queue->Front());
                break;
            case CMD::REAR:
                output.push_back(queue->Rear());
                break;
            default:
                std::cerr << "[ERROR] Unknown command.\n";
                return;
            }
        }
        result = output;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({CMD::MYCIRCULARQUEUE, CMD::ENQUEUE, CMD::ENQUEUE, CMD::ENQUEUE,
          CMD::ENQUEUE, CMD::REAR, CMD::ISFULL, CMD::DEQUEUE, CMD::ENQUEUE,
          CMD::REAR}, {3, 1, 2, 3, 4, null, null, null, 4, null},
          {null, true, true, true, false, 3, true, true, true, 4}, trials);
    test({CMD::MYCIRCULARQUEUE, CMD::ENQUEUE, CMD::REAR, CMD::REAR, CMD::DEQUEUE,
          CMD::ENQUEUE, CMD::REAR, CMD::DEQUEUE, CMD::FRONT, CMD::DEQUEUE,
          CMD::DEQUEUE, CMD::DEQUEUE},
         {6, 6, null, null, null, 5, null, null, null, null, null, null},
         {null, true, 6, 6, true, true, 5, true, -1, false, false, false}, trials);
    return 0;
}


