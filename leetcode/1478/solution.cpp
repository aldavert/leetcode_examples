#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int minDistance(std::vector<int> houses, int k)
{
    constexpr int MAX = 1'000'000;
    const int n = static_cast<int>(houses.size());
    int dp[101][101], cost[101][101] = {};
    
    auto minDistance = [&](auto &&self, int c, int i) -> int
    {
        if ((c == 0) && (i == n)) return 0;
        if ((c == 0) || (i == n)) return MAX;
        if (dp[c][i] != std::numeric_limits<int>::max()) return dp[c][i];
        
        for (int j = i; j < n; ++j)
            dp[c][i] = std::min(dp[c][i], cost[i][j] + self(self, c - 1, j + 1));
        return dp[c][i];
    };
    std::sort(houses.begin(), houses.end());
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            dp[i][j] = std::numeric_limits<int>::max();
        for (int j = i + 1; j < n; ++j)
        {
            dp[i][j] = std::numeric_limits<int>::max();
            int median = houses[(i + j) / 2];
            for (int x = i; x <= j; ++x)
                cost[i][j] += abs(houses[x] - median);
        }
    }
    return minDistance(minDistance, k, 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> houses, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDistance(houses, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 8, 10, 20}, 3, 5, trials);
    test({2, 3, 5, 12, 18}, 2, 9, trials);
    return 0;
}


