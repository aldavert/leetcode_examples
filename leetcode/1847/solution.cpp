#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> closestRoom(std::vector<std::vector<int> > rooms,
                             std::vector<std::vector<int> > queries)
{
    std::vector<int> result(queries.size());
    std::set<int> room_ids;
    
    for (int i = 0, n = static_cast<int>(queries.size()); i < n; ++i)
        queries[i].push_back(i);
    auto descSize = [](const auto& a, const auto& b) { return a[1] > b[1]; };
    std::sort(rooms.begin(), rooms.end(), descSize);
    std::sort(queries.begin(), queries.end(), descSize);
    
    for (int i = 0, n = static_cast<int>(rooms.size()); const auto &query : queries)
    {
        while ((i < n) && (rooms[i][1] >= query[1]))
            room_ids.insert(rooms[i++][0]);
        auto it = room_ids.lower_bound(query[0]);
        int id1 = (it == room_ids.begin())?-1:*(prev(it));
        int id2 = (it == room_ids.end())?-1:*it;
        if      (id1 == -1) result[query[2]] = id2;
        else if (id2 == -1) result[query[2]] = id1;
        else if (std::abs(query[0] - id1) <= std::abs(query[0] - id2))
            result[query[2]] = id1;
        else result[query[2]] = id2;
    }
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > rooms,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = closestRoom(rooms, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 2}, {1, 2}, {3, 2}}, {{3, 1}, {3, 3}, {5, 2}}, {3, -1, 3}, trials);
    test({{1, 4}, {2, 3}, {3, 5}, {4, 1}, {5, 2}}, {{2, 3}, {2, 4}, {2, 5}},
         {2, 1, 3}, trials);
    test({}, {}, {}, trials);
    return 0;
}



