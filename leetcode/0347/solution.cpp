#include "../common/common.hpp"
#include <queue>
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> topKFrequent(std::vector<int> nums, int k)
{
    std::vector<int> result(k);
    std::priority_queue<std::tuple<int, int>,
                        std::vector<std::tuple<int, int> >,
                        std::greater<std::tuple<int, int> > > queue;
    std::unordered_map<int, int> histogram;
    auto update = [&](int value, int frequency)
    {
        if (static_cast<int>(queue.size()) >= k)
        {
            if (std::get<0>(queue.top()) < frequency)
            {
                queue.pop();
                queue.push({frequency, value});
            }
        }
        else queue.push({frequency, value});
    };
    for (int value : nums)
        ++histogram[value];
    for (auto [value, frequency] : histogram)
        update(value, frequency);
    for (int idx = 0; !queue.empty(); ++idx)
    {
        result[idx] = std::get<1>(queue.top());
        queue.pop();
    }
    return result;
}
#else
std::vector<int> topKFrequent(std::vector<int> nums, int k)
{
    std::vector<int> result(k);
    std::sort(nums.begin(), nums.end());
    std::priority_queue<std::tuple<int, int>,
                        std::vector<std::tuple<int, int> >,
                        std::greater<std::tuple<int, int> > > histogram;
    auto update = [&](int value, int frequency)
    {
        if (static_cast<int>(histogram.size()) >= k)
        {
            if (std::get<0>(histogram.top()) < frequency)
            {
                histogram.pop();
                histogram.push({frequency, value});
            }
        }
        else histogram.push({frequency, value});
    };
    int frequency = 0, current = nums[0];
    for (int value : nums)
    {
        if (value == current)
            ++frequency;
        else
        {
            update(current, frequency);
            current = value;
            frequency = 1;
        }
    }
    update(current, frequency);
    for (int idx = 0; !histogram.empty(); ++idx)
    {
        result[idx] = std::get<1>(histogram.top());
        histogram.pop();
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

template <typename T>
bool operator==(const std::vector<T> &left, const std::vector<T> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_map<T, unsigned int> histogram;
    for (const T &lv : left)
        ++histogram[lv];
    for (const T &rv :right)
    {
        if (auto search = histogram.find(rv); search != histogram.end())
        {
            if (search->second == 1)
                histogram.erase(search);
            else --search->second;
        }
        else return false;
    }
    return histogram.empty();
}

void test(std::vector<int> nums,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = topKFrequent(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 1, 2, 2, 3}, 2, {1, 2}, trials);
    test({1}, 1, {1}, trials);
    return 0;
}


