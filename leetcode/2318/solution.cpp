#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
int distinctSequences(int n)
{
    constexpr int MOD = 1'000'000'007;
    if (n == 1) return 6;
    int dp[10001][6][6] = {}, total = 0;
    for (int i = 2; i <= n; ++i)
    {
        for (int prev = 0; prev < 6; ++prev)
        {
            for (int dice = 0; dice < 6; ++dice)
            {
                if ((dice != prev) && ((dice % 3 != prev % 3) || (dice % 3 != 2))
                &&  ((dice % 2 != prev % 2) || (dice % 2 != 1)))
                {
                    for (int prev_prev = 0; prev_prev < 6; ++prev_prev)
                        if (prev_prev != prev)
                            dp[i][dice][prev] = (i == 2)?1:(dp[i][dice][prev]
                                              + dp[i - 1][prev_prev][dice]) % MOD;
                    if (i == n)
                        total = (total + dp[i][dice][prev]) % MOD;
                }
            }
        }
    }
    return total;
}
#else
int distinctSequences(int n)
{
    constexpr int MOD = 1'000'000'007;
    std::vector<int> dp[7][7];
    auto distinctSequences = [&](auto &&self, int m, int prev, int prev_prev) -> int
    {
        if (m == 0) return 1;
        if (dp[prev][prev_prev][m])
            return dp[prev][prev_prev][m];
        for (int dice = 1; dice <= 6; ++dice)
            if ((dice != prev) && (dice != prev_prev)
            &&  ((prev == 0) || (std::gcd(dice, prev) == 1)))
                dp[prev][prev_prev][m] = (dp[prev][prev_prev][m]
                                       + self(self, m - 1, dice, prev)) % MOD;
        return dp[prev][prev_prev][m];
    };
    
    for (int i = 0; i < 7; ++i)
        for (int j = 0; j < 7; ++j)
            dp[i][j].resize(n + 1);
    return distinctSequences(distinctSequences, n, 0, 0);
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = distinctSequences(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 184, trials);
    test(2, 22, trials);
    return 0;
}


