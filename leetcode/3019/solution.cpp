#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countKeyChanges(std::string s)
{
    int result = 0;
    for (char previous = static_cast<char>(std::tolower(s.front())); char current : s)
    {
        current = static_cast<char>(std::tolower(current));
        result += (current != previous);
        previous = current;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countKeyChanges(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aAbBcC", 2, trials);
    test("AaAaAaaA", 0, trials);
    return 0;
}


