#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countKConstraintSubstrings(std::string s, int k)
{
    int result = 0, count[2] = {};
    for (int l = 0, r = 0, n = static_cast<int>(s.size()); r < n; ++r)
    {
        ++count[s[r] - '0'];
        while ((count[0] > k) && (count[1] > k))
            --count[s[l++] - '0'];
        result += r - l + 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countKConstraintSubstrings(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("10101", 1, 12, trials);
    test("1010101", 2, 25, trials);
    test("11111", 1, 15, trials);
    return 0;
}


