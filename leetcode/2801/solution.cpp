#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

int countSteppingNumbers(std::string low, std::string high)
{
    const int MOD = 1'000'000'007;
    int f[101][10];
    std::memset(f, -1, sizeof(f));
    std::string num = high;
    auto dfs = [&](auto &&self, int pos, int pre, bool lead, bool limit) -> int
    {
        if (pos >= static_cast<int>(num.size())) return !lead;
        if (!lead && !limit && (f[pos][pre] != -1)) return f[pos][pre];
        int up = (limit)?num[pos] - '0':9, result = 0;
        for (int i = 0; i <= up; ++i)
        {
            if ((i == 0) && lead)
                result += self(self, pos + 1, pre, true, limit && (i == up));
            else if ((pre == -1) || (std::abs(pre - i) == 1))
                result += self(self, pos + 1, i, false, limit && (i == up));
            result %= MOD;
        }
        if (!lead && !limit) f[pos][pre] = result;
        return result;
    };
    int a = dfs(dfs, 0, -1, true, true);
    std::memset(f, -1, sizeof(f));
    for (int i = static_cast<int>(low.size()) - 1; i >= 0; --i)
    {
        if (low[i] == '0') low[i] = '9';
        else
        {
            low[i] -= 1;
            break;
        }
    }
    num = low;
    int b = dfs(dfs, 0, -1, true, true);
    return (a - b + MOD) % MOD;
}

// ############################################################################
// ############################################################################

void test(std::string low, std::string high, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSteppingNumbers(low, high);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1", "11", 10, trials);
    test("90", "101", 2, trials);
    return 0;
}


