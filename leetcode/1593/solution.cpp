#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxUniqueSplit(std::string s)
{
    auto process = [&](void) -> int
    {
        std::unordered_set<std::string> seen;
        const int n = static_cast<int>(s.size());
        size_t result = 0;
        
        auto inner = [&](auto &&self, int position) -> void
        {
            if (position == n)
            {
                result = std::max(result, seen.size());
                return;
            }
            for (int i = 1; position + i <= n; ++i)
            {
                const std::string substr = s.substr(position, i);
                if (seen.count(substr))
                    continue;
                seen.insert(substr);
                self(self, position + i);
                seen.erase(substr);
            }
        };
        inner(inner, 0);
        return static_cast<int>(result);
    };
    return process();
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxUniqueSplit(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ababccc", 5, trials);
    test("aba", 2, trials);
    test("aa", 1, trials);
    return 0;
}


