#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int kIncreasing(std::vector<int> arr, int k)
{
    const int n = static_cast<int>(arr.size());
    int dp[100'001] = {};
    int result = 0;
    
    for (int j = 0, s = 0; j < k; ++j)
    {
        s = 0;
        for (int i = j; i < n; i += k)
        {
            auto v = std::upper_bound(dp, dp + s, arr[i]) - dp;
            if (v == s) dp[s++] = arr[i];
            else dp[v] = arr[i];
        }
        result += s;
    }
    return n - result;
}
#else
int kIncreasing(std::vector<int> arr, int k)
{
    const int n = static_cast<int>(arr.size());
    int result = 0;
    auto numReplaced = [](const std::vector<int> &work) -> int
    {
        std::vector<int> tail;
        for (int v : work)
        {
            if (tail.empty() || (tail.back() <= v))
                tail.push_back(v);
            else tail[std::upper_bound(tail.begin(), tail.end(), v) - tail.begin()] = v;
        }
        return static_cast<int>(work.size() - tail.size());
    };
    
    for (int i = 0; i < k; ++i)
    {
        std::vector<int> work;
        for (int j = i; j < n; j += k)
            work.push_back(arr[j]);
        result += numReplaced(work);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = kIncreasing(arr, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 3, 2, 1}, 1, 4, trials);
    test({4, 1, 5, 2, 6, 2}, 2, 0, trials);
    test({4, 1, 5, 2, 6, 2}, 3, 2, trials);
    return 0;
}


