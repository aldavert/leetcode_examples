#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> evenOddBit(int n)
{
    std::vector<int> result(2);
    for (bool even = false; n; n >>= 1, even = !even)
        result[even] += n & 1;
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = evenOddBit(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(17, {2, 0}, trials);
    test( 2, {0, 1}, trials);
    return 0;
}


