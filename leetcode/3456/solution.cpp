#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool hasSpecialSubstring(std::string s, int k)
{
    int counter = 0;
    for (char previous = '\0'; char symbol : s)
    {
        if (previous == symbol) ++counter;
        else
        {
            if (counter == k) return true;
            counter = 1;
        }
        previous = symbol;
    }
    return counter == k;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = hasSpecialSubstring(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aaabaaa", 3, true, trials);
    test("abc", 2, false, trials);
    return 0;
}


