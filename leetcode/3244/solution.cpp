#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> shortestDistanceAfterQueries(int n,
                                              std::vector<std::vector<int> > queries)
{
    std::vector<int> result(queries.size());
    std::unordered_map<int, int> node_to_farthest_node;
    for (int i = 0; i < n - 1; ++i)
        node_to_farthest_node[i] = i + 1;
    for (size_t i = 0; i < queries.size(); ++i)
    {
        const int u = queries[i][0], v = queries[i][1];
        result[i] = static_cast<int>(node_to_farthest_node.size());
        if (!node_to_farthest_node.contains(u)) continue;
        int node = node_to_farthest_node[u];
        if (node >= v) continue;
        while (node < v)
        {
            const int cache = node;
            node = node_to_farthest_node[node];
            node_to_farthest_node.erase(cache);
        }
        node_to_farthest_node[u] = v;
        result[i] = static_cast<int>(node_to_farthest_node.size());
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestDistanceAfterQueries(n, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{2, 4}, {0, 2}, {0, 4}}, {3, 2, 1}, trials);
    test(4, {{0, 3}, {0, 2}}, {1, 1}, trials);
    return 0;
}


