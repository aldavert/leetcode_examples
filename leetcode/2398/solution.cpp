#include "../common/common.hpp"
#include <deque>

// ############################################################################
// ############################################################################

int maximumRobots(std::vector<int> chargeTimes,
                  std::vector<int> runningCosts,
                  long long budget)
{
    const int n = static_cast<int>(chargeTimes.size());
    std::deque<int> deque;
    long cost = 0;
    
    int j = 0;
    for (int i = 0; i < n; ++i)
    {
        cost += runningCosts[i];
        while (!deque.empty() && (deque.back() < chargeTimes[i]))
            deque.pop_back();
        deque.push_back(chargeTimes[i]);
        if (deque.front() + (i - j + 1) * cost > budget)
        {
            if (deque.front() == chargeTimes[j])
                deque.pop_front();
            cost -= runningCosts[j++];
        }
    }
    return n - j;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> chargeTimes,
          std::vector<int> runningCosts,
          long long budget,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumRobots(chargeTimes, runningCosts, budget);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 6, 1, 3, 4}, {2, 1, 3, 4, 5}, 25, 3, trials);
    test({11, 12, 19}, {10, 8, 7}, 19, 0, trials);
    return 0;
}


