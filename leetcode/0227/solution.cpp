#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 1
int calculate(std::string s)
{
    const int n = static_cast<int>(s.size());
    if (n == 0) return 0;
    int number = 0, previous = 0, result = 0;
    char operand = '+';
    
    auto compute = [&](char c) -> void
    {
        switch(operand)
        {
        case '+': 
            result += previous;
            previous = number;
            break;
        case '-': 
            result += previous;
            previous = -number;
            break;
        case '*': 
            previous *= number;
            break;
        case '/':
            previous /= number;
            break;
        default:
            break;
        }
        number = 0;
        operand = c;
    };
    for (char c : s)
    {
        if (std::isdigit(c))
            number = number * 10 + (c - '0');
        else if (c != ' ')
            compute(c);
        
    }
    compute('\0');
    return result + previous;
}
#else
int calculate(std::string s)
{
    auto evalRPN = [](const std::vector<std::string> &tokens) -> int
    {
        std::stack<int> values;
        for (const auto &t : tokens)
        {
            if (t.size() == 1 && ((t[0] == '+') || (t[0] == '-') || (t[0] == '*') || (t[0] == '/')))
            {
                int vr = values.top();
                values.pop();
                int vl = values.top();
                values.pop();
                switch (t[0])
                {
                case '+':
                    vl += vr;
                    break;
                case '-':
                    vl -= vr;
                    break;
                case '*':
                    vl *= vr;
                    break;
                case '/':
                    vl /= vr;
                    break;
                default:
                    break;
                }
                values.push(vl);
            }
            else values.push(std::stoi(t));
        }
        return values.top();
    };
    auto extractTokens = [](const std::string &tok) -> std::vector<std::string>
    {
        const int n = static_cast<int>(tok.size());
        std::vector<std::string> tokens;
        int begin = 0;
        bool number = false;
        for (int i = 0; i < n; ++i)
        {
            if (tok[i] == ' ')
            {
                if (number)
                {
                    tokens.push_back(tok.substr(begin, i - begin));
                    number = false;
                }
            }
            else if ((tok[i] >= '0') && (tok[i] <= '9'))
            {
                if (!number)
                {
                    begin = i;
                    number = true;
                }
            }
            else
            {
                if (number)
                {
                    tokens.push_back(tok.substr(begin, i - begin));
                    number = false;
                }
                tokens.push_back({tok[i]});
            }
        }
        if (number)
            tokens.push_back(tok.substr(begin, n - begin));
        return tokens;
    };
    auto toRPN = [](const std::vector<std::string> &infix) -> std::vector<std::string>
    {
        std::vector<std::string> result;
        std::stack<std::string> operators;
        result.reserve(infix.size());
        for (const std::string &t : infix)
        {
            if ((t[0] == '+') || (t[0] == '-'))
            {
                while (!operators.empty())
                {
                    result.push_back(operators.top());
                    operators.pop();
                }
                operators.push(t);
            }
            else if ((t[0] == '/') || (t[0] == '*'))
            {
                while (!operators.empty()
                        && ((operators.top()[0] == '/') || (operators.top()[0] == '*')))
                {
                    result.push_back(operators.top());
                    operators.pop();
                }
                operators.push(t);
            }
            else
            {
                result.push_back(t);
            }
        }
        while (!operators.empty())
        {
            result.push_back(operators.top());
            operators.pop();
        }
        return result;
    };
    return evalRPN(toRPN(extractTokens(s)));
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = calculate(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("3+2*2", 7, trials);
    test(" 3/2 ", 1, trials);
    test(" 3+5 / 2 ", 5, trials);
    test(" 3+50 / 20 ", 5, trials);
    return 0;
}


