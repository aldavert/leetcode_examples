#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int maxIncreasingCells(std::vector<std::vector<int> > mat)
{
    const int m = static_cast<int>(mat.size()),
              n = static_cast<int>(mat[0].size());
    std::pair<int, int> v[100'001];
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            v[i * n + j] = {mat[i][j], i * n + j};
    std::sort(&v[0], &v[m * n], std::greater<std::pair<int, int>>());
    std::vector<int> row(m), col(n);
    std::vector<std::pair<int, std::pair<int, int> > > t;
    int result = 0;
    for (int i = 0; i < m * n; i++)
    {
        int r = v[i].second / n, c = v[i].second % n,
            cur = std::max(row[r], col[c]) + 1;
        if (i + 1 < m * n && v[i + 1].first == v[i].first)
            t.push_back({cur, {r, c}});
        else
        {
            row[r] = std::max(row[r], cur);
            col[c] = std::max(col[c], cur);
            while (!t.empty())
            {
                const auto &b = t.back();
                row[b.second.first] = std::max(row[b.second.first], b.first);
                col[b.second.second] = std::max(col[b.second.second], b.first);
                t.pop_back();
            }
        }
        result = std::max(result, cur);
    }
    return result;
}
#else
int maxIncreasingCells(std::vector<std::vector<int> > mat)
{
    const int m = static_cast<int>(mat.size()),
              n = static_cast<int>(mat[0].size());
    std::vector<int> rows(m), cols(n);
    std::unordered_map<int, std::vector<std::pair<int, int> > > val_to_indices;
    std::vector<std::vector<int> > max_path_length(m, std::vector<int>(n));
    std::set<int, std::greater<> > decreasing_set;
    
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            val_to_indices[mat[i][j]].push_back({i, j});
            decreasing_set.insert(mat[i][j]);
        }
    }
    for (const int val : decreasing_set)
    {
        for (auto [i, j] : val_to_indices[val])
            max_path_length[i][j] = std::max(rows[i], cols[j]) + 1;
        for (auto [i, j] : val_to_indices[val])
        {
            rows[i] = std::max(rows[i], max_path_length[i][j]);
            cols[j] = std::max(cols[j], max_path_length[i][j]);
        }
    }
    return std::max(*std::max_element(rows.begin(), rows.end()),
                    *std::max_element(cols.begin(), cols.end()));
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > mat, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxIncreasingCells(mat);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 1}, {3, 4}}, 2, trials);
    test({{1, 1}, {1, 1}}, 1, trials);
    test({{3, 1, 6}, {-9, 5, 7}}, 4, trials);
    return 0;
}


