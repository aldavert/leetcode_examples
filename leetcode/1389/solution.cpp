#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> createTargetArray(std::vector<int> nums, std::vector<int> index)
{
    std::vector<int> result;
    for (size_t i = 0, n = nums.size(); i < n; ++i)
        result.insert(result.begin() + index[i], nums[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> index,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = createTargetArray(nums, index);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 2, 3, 4}, {0, 1, 2, 2, 1}, {0, 4, 1, 3, 2}, trials);
    test({1, 2, 3, 4, 0}, {0, 1, 2, 3, 0}, {0, 1, 2, 3, 4}, trials);
    test({1}, {0}, {1}, trials);
    return 0;
}


