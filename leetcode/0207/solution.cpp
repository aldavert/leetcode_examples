#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

bool canFinish(int numCourses, std::vector<std::vector<int> > prerequisites)
{
    std::vector<std::vector<int> > graph(numCourses);
    for (auto &prerequisite : prerequisites)
        graph[prerequisite[0]].push_back(prerequisite[1]);
    std::bitset<2001> visited, current_trace;
    auto isCycle = [&](auto &&self, int node) -> bool
    {
        if (current_trace[node]) return true;
        if (visited[node]) return false;
        current_trace[node] = visited[node] = true;
        for (int next : graph[node])
            if (self(self, next)) return true;
        current_trace[node] = false;
        return false;
    };
    for (int i = 0; i < numCourses; ++i)
    {
        if (visited[i]) continue;
        if (isCycle(isCycle, i)) return false;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(int numCourses,
          std::vector<std::vector<int> > prerequisites,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canFinish(numCourses, prerequisites);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {{1, 0}}, true, trials);
    test(2, {{1, 0}, {0, 1}}, false, trials);
    test(4, {{1, 0}, {2, 0}, {3, 1}, {3, 2}}, true, trials);
    test(3, {{0, 1}, {0, 2}, {1, 2}}, true, trials);
    return 0;
}


