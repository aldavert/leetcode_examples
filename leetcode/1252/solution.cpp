#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int oddCells(int m, int n, std::vector<std::vector<int> > indices)
{
    int rows[51] = {}, columns[51] = {};
    int result = 0;
    for (const auto &index : indices)
    {
        ++rows[index[0]];
        ++columns[index[1]];
    }
    int odd_rows = 0;
    for (int r = 0; r < m; ++r)
        odd_rows += rows[r] & 1;
    for (int c = 0; c < n; ++c)
    {
        bool odd_column = columns[c] & 1;
        result += odd_column * (m - odd_rows) + (!odd_column) * odd_rows;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int m,
          int n,
          std::vector<std::vector<int> > indices,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = oddCells(m, n, indices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 3, {{0, 1}, {1, 1}}, 6, trials);
    test(2, 2, {{1, 1}, {0, 0}}, 0, trials);
    return 0;
}


