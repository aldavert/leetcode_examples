#include "../common/common.hpp"

// ############################################################################
// ############################################################################

double myPow(double x, int n)
{
    if (n == 0) return 1;
    double tmp = myPow(x, n / 2);
    if (n % 2 == 0) return tmp * tmp;
    return (n > 0)?(x * tmp * tmp):((tmp * tmp) / x);
}

// ############################################################################
// ############################################################################

void test(double x, int n, double solution, unsigned int trials = 1)
{
    double result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = myPow(x, n);
    showResult(std::abs(solution - result) < 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 10, 1024, trials);
    test(2.1, 3, 9.261, trials);
    test(2, -2, 0.25, trials);
    return 0;
}


