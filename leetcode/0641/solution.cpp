#include "../common/common.hpp"

constexpr int null = -1'000'000;

// ############################################################################
// ############################################################################

class MyCircularDeque
{
    std::vector<int> buffer;
    int front;
    int rear;
    int n_elements;
    int size;
    inline void decrease(int &value) const
    {
        --value;
        if (value < 0) value = size - 1;
    }
    inline void increase(int &value) const
    {
        ++value;
        if (value == size) value = 0;
    }
public:
    MyCircularDeque(int k) :
        buffer(k),
        front(1),
        rear(0),
        n_elements(0),
        size(k)
    {
    }
    bool insertFront(int value)
    {
        if (n_elements >= size)
            return false;
        decrease(front);
        buffer[front] = value;
        ++n_elements;
        return true;
    }
    bool insertLast(int value)
    {
        if (n_elements >= size)
            return false;
        increase(rear);
        buffer[rear] = value;
        ++n_elements;
        return true;
    }
    bool deleteFront(void)
    {
        if (n_elements == 0) return false;
        increase(front);
        --n_elements;
        return true;
    }
    bool deleteLast(void)
    {
        if (n_elements == 0) return false;
        decrease(rear);
        --n_elements;
        return true;
    }
    int getFront(void)
    {
        if (n_elements == 0) return -1;
        return buffer[front];
    }
    int getRear(void)
    {
        if (n_elements == 0) return -1;
        return buffer[rear];
    }
    bool isEmpty(void)
    {
        return n_elements == 0;
    }
    bool isFull(void)
    {
        return n_elements == size;
    }
};

// ############################################################################
// ############################################################################

enum class OP { INSERT_FRONT, INSERT_LAST, DELETE_FRONT, DELETE_LAST,
                GET_FRONT, GET_REAR, IS_EMPTY, IS_FULL };

void test(int k,
          std::vector<OP> operations,
          std::vector<int> input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        MyCircularDeque object(k);
        for (size_t i = 0; i < operations.size(); ++i)
        {
            switch (operations[i])
            {
            case OP::INSERT_FRONT:
                result.push_back(object.insertFront(input[i]));
                break;
            case OP::INSERT_LAST:
                result.push_back(object.insertLast(input[i]));
                break;
            case OP::DELETE_FRONT:
                result.push_back(object.deleteFront());
                break;
            case OP::DELETE_LAST:
                result.push_back(object.deleteLast());
                break;
            case OP::GET_FRONT:
                result.push_back(object.getFront());
                break;
            case OP::GET_REAR:
                result.push_back(object.getRear());
                break;
            case OP::IS_EMPTY:
                result.push_back(object.isEmpty());
                break;
            case OP::IS_FULL:
                result.push_back(object.isFull());
                break;
            default:
                std::cerr << "[ERROR] Unknown operation at test code.\n";
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3,
         {OP::INSERT_LAST, OP::INSERT_LAST, OP::INSERT_FRONT, OP::INSERT_FRONT,
         OP::GET_REAR, OP::IS_FULL, OP::DELETE_LAST, OP::INSERT_FRONT,
         OP::GET_FRONT},
         {1, 2, 3, 4, null, null, null, 4, null},
         {true, true, true, false, 2, true, true, true, 4}, trials);
    return 0;
}


