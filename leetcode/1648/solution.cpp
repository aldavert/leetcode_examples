#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxProfit(std::vector<int> inventory, int orders)
{
    auto trapezoid = [](long a, long b) -> long
    {
        return (a + b) * (a - b + 1) / 2;
    };
    constexpr int MOD = 1'000'000'007;
    long result = 0, largest_count = 1;
    
    std::sort(inventory.begin(), inventory.end(), std::greater<>());
    for (int i = 0, n = static_cast<int>(inventory.size()); i < n; ++i, ++largest_count)
    {
        if ((i == n - 1) || (inventory[i] > inventory[i + 1]))
        {
            const int pick = inventory[i] - ((i < n - 1)?inventory[i + 1]:0);
            if (largest_count * pick >= orders)
            {
                const long actual_pick = orders / largest_count;
                const long remaining = orders % largest_count;
                return static_cast<int>((result
                     + largest_count
                     * trapezoid(inventory[i], inventory[i] - actual_pick + 1)
                     + remaining * static_cast<long>(inventory[i] - actual_pick)) % MOD);
            }
            result += largest_count * trapezoid(inventory[i], inventory[i] - pick + 1);
            result %= MOD;
            orders -= static_cast<int>(largest_count * pick);
        }
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> inventory,
          int orders,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxProfit(inventory, orders);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 5}, 4, 14, trials);
    test({3, 5}, 6, 19, trials);
    return 0;
}


