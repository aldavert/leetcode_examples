#include "../common/common.hpp"
#include <numeric>
#include <unordered_map>

// ############################################################################
// ############################################################################

int numFactoredBinaryTrees(std::vector<int> arr)
{
    constexpr int MOD = 1e9 + 7;
    const int n = static_cast<int>(arr.size());
    std::vector<long> dp(n, 1);
    std::unordered_map<int, int> number_index;
    
    std::sort(arr.begin(), arr.end());
    for (int i = 0; i < n; ++i)
        number_index[arr[i]] = i;
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < i; ++j)
            if (arr[i] % arr[j] == 0)
                if (auto search = number_index.find(arr[i] / arr[j]);
                    search != number_index.end())
                    dp[i] = (dp[i] + dp[j] * dp[search->second]) % MOD;
    return static_cast<int>(std::accumulate(dp.begin(), dp.end(), 0L) % MOD);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numFactoredBinaryTrees(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4}, 3, trials);
    test({2, 4, 5, 10}, 7, trials);
    return 0;
}


