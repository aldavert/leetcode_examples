#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxNumberOfAlloys(int n,
                      [[maybe_unused]] int k,
                      int budget,
                      std::vector<std::vector<int> > composition,
                      std::vector<int> stock,
                      std::vector<int> cost)
{
    auto isPossible = [&](int m) -> bool
    {
        for (const auto &machine : composition)
        {
            long required_money = 0;
            for (int j = 0; j < n; ++j)
            {
                const long required_units =
                    std::max(0L, static_cast<long>(machine[j]) * m - stock[j]);
                required_money += static_cast<long>(required_units) * cost[j];
            }
            if (required_money <= budget)
                return true;
        }
        return false;
    };
    int l = 1, r = 1'000'000'000;
    while (l < r)
    {
        const int m = (l + r) / 2;
        if (isPossible(m)) l = m + 1;
        else r = m;
    }
    return l - 1;
}

// ############################################################################
// ############################################################################

void test(int n,
          int k,
          int budget,
          std::vector<std::vector<int> > composition,
          std::vector<int> stock,
          std::vector<int> cost,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxNumberOfAlloys(n, k, budget, composition, stock, cost);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 2, 15, {{1, 1, 1}, {1, 1, 10}}, {0, 0, 0}, {1, 2, 3}, 2, trials);
    test(3, 2, 15, {{1, 1, 1}, {1, 1, 10}}, {0, 0, 100}, {1, 2, 3}, 5, trials);
    test(2, 3, 10, {{2, 1}, {1, 2}, {1, 1}}, {1, 1}, {5, 5}, 2, trials);
    return 0;
}


