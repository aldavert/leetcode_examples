#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int findChampion(std::vector<std::vector<int> > grid)
{
    const int n_rows = static_cast<int>(grid.size());
    int result = -1, max_count = -1;
    for (int r = 0; r < n_rows; ++r)
    {
        int sum = std::accumulate(grid[r].begin(), grid[r].end(), 0);
        if (sum > max_count)
        {
            max_count = sum;
            result = r;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findChampion(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {0, 0}}, 0, trials);
    test({{0, 0, 1}, {1, 0, 1}, {0, 0, 0}}, 1, trials);
    return 0;
}


