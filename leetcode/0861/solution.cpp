#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int matrixScore(std::vector<std::vector<int> > grid)
{
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>(grid.front().size());
    bool flip_row[21] = {};
    for (int r = 0; r < n_rows; ++r)
        flip_row[r] = grid[r][0] == 0;
    int result = 0;
    for (int c = 0; c < n_cols; ++c)
    {
        int n_ones = 0;
        for (int r = 0; r < n_rows; ++r)
            n_ones += (grid[r][c] != flip_row[r]);
        bool flip_col = n_ones <= n_rows / 2;
        for (int r = 0; r < n_rows; ++r)
            result += (flip_col != (grid[r][c] != flip_row[r])) << (n_cols - c - 1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = matrixScore(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0, 1, 1}, {1, 0, 1, 0}, {1, 1, 0, 0}}, 39, trials);
    test({{0}}, 1, trials);
    return 0;
}


