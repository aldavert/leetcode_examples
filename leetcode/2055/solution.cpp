#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> platesBetweenCandles(std::string s,
                                      std::vector<std::vector<int> > queries)
{
    const int n = static_cast<int>(s.size());
    std::vector<int> result, closest_left_candle(n), closest_right_candle(n),
                     candle_count(n);
    int candle = -1, count = 0;
    
    for (int i = 0; i < n; ++i)
    {
        if (s[i] == '|')
        {
            candle = i;
            ++count;
        }
        closest_left_candle[i] = candle;
        candle_count[i] = count;
    }
    candle = -1;
    for (int i = n - 1; i >= 0; --i)
    {
        if (s[i] == '|')
            candle = i;
        closest_right_candle[i] = candle;
    }
    for (const auto &query : queries)
    {
        const int l = closest_right_candle[query[0]],
                  r = closest_left_candle[query[1]];
        if ((l == -1) || (r == -1) || (l > r))
            result.push_back(0);
        else result.push_back((r - l + 1) - (candle_count[r] - candle_count[l] + 1));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = platesBetweenCandles(s, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("**|**|***|", {{2, 5}, {5, 9}}, {2, 3}, trials);
    test("***|**|*****|**||**|*", {{1, 17}, {4, 5}, {14, 17}, {5, 11}, {15, 16}},
         {9, 0, 0, 0, 0}, trials);
    return 0;
}


