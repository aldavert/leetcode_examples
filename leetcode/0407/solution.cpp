#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int trapRainWater(std::vector<std::vector<int> > heightMap)
{
    if (heightMap.empty()) return 0;
    constexpr int dir[4][2] = {{0, -1}, {0, 1}, {-1, 0}, {1, 0}};
    const int m = static_cast<int>(heightMap.size()),
              n = static_cast<int>(heightMap[0].size());
    int result = 0;
    struct Position
    {
        int height = 0;
        int x = 0;
        int y = 0;
        bool operator<(const Position &other) const
        {
            return (height >  other.height)
               || ((height == other.height) && ((x > other.x)
                           || ((x == other.x) && (y > other.y))));
        }
    };
    std::priority_queue<Position> q;
    int visited[201][201] = {};
    for (int i = 0; i < m; ++i)
    {
        visited[i][0] = visited[i][n - 1] = 1;
        q.push({heightMap[i][0    ], i, 0    });
        q.push({heightMap[i][n - 1], i, n - 1});
    }
    for (int j = 0; j < n; ++j)
    {
        visited[0][j] = visited[m - 1][j] = 1;
        q.push({heightMap[0    ][j], 0    , j});
        q.push({heightMap[m - 1][j], m - 1, j});
    }
    for (int shorter = 0; !q.empty(); )
    {
        auto [h, x, y] = q.top();
        q.pop();
        shorter = std::max(shorter, h);
        for (int i = 0; i < 4; ++i)
        {
            int xx = x + dir[i][0], yy = y + dir[i][1];
            if ((xx < 0) || (xx >= m) || (yy < 0) || (yy >= n)
            || (visited[xx][yy] == 1))
                continue;
            visited[xx][yy] = 1;
            if (heightMap[xx][yy] < shorter)
                result += shorter - heightMap[xx][yy];
            q.push({heightMap[xx][yy], xx, yy});
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > heightMap,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = trapRainWater(heightMap);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 4, 3, 1, 3, 2},
          {3, 2, 1, 3, 2, 4},
          {2, 3, 3, 2, 3, 1}}, 4, trials);
    test({{3, 3, 3, 3, 3},
          {3, 2, 2, 2, 3},
          {3, 2, 1, 2, 3},
          {3, 2, 2, 2, 3},
          {3, 3, 3, 3, 3}}, 10, trials);
    return 0;
}


