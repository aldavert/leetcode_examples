#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumBags(std::vector<int> capacity,
                std::vector<int> rocks,
                int additionalRocks)
{
    for (size_t i = 0, n = capacity.size(); i < n; ++i)
        capacity[i] -= rocks[i];
    std::sort(capacity.begin(), capacity.end());
    int result = 0;
    for (size_t i = 0, n = capacity.size(); additionalRocks && (i < n); ++i)
    {
        int aux = std::min(additionalRocks, capacity[i]);
        additionalRocks -= aux;
        result += (capacity[i] -= aux) == 0;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> capacity,
          std::vector<int> rocks,
          int additionalRocks,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumBags(capacity, rocks, additionalRocks);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 4, 5}, {1, 2, 4, 4}, 2, 3, trials);
    test({10, 2, 2}, {2, 2, 0}, 100, 3, trials);
    return 0;
}


