#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long largestSquareArea(std::vector<std::vector<int> > bottomLeft,
                            std::vector<std::vector<int> > topRight)
{
    int min_side = 0;
    for (size_t i = 0; i < bottomLeft.size(); ++i)
    {
        for (size_t j = i + 1; j < bottomLeft.size(); ++j)
        {
            const int ax1 = bottomLeft[i][0];
            const int ay1 = bottomLeft[i][1];
            const int ax2 = topRight[i][0];
            const int ay2 = topRight[i][1];
            const int bx1 = bottomLeft[j][0];
            const int by1 = bottomLeft[j][1];
            const int bx2 = topRight[j][0];
            const int by2 = topRight[j][1];
            const int overlap_x = std::min(ax2, bx2) - std::max(ax1, bx1);
            const int overlap_y = std::min(ay2, by2) - std::max(ay1, by1);
            min_side = std::max(min_side, std::min(overlap_x, overlap_y));
        }
    }
    return static_cast<long>(min_side) * min_side;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > bottomLeft,
          std::vector<std::vector<int> > topRight,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestSquareArea(bottomLeft, topRight);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1}, {2, 2}, {3, 1}}, {{3, 3}, {4, 4}, {6, 6}}, 1, trials);
    test({{1, 1}, {1, 3}, {1, 5}}, {{5, 5}, {5, 7}, {5, 9}}, 4, trials);
    test({{1, 1}, {2, 2}, {1, 2}}, {{3, 3}, {4, 4}, {3, 4}}, 1, trials);
    return 0;
}


