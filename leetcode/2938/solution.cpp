#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long minimumSteps(std::string s)
{
    long result = 0;
    for (int ones = 0; const char c : s)
    {
        if (c == '1') ++ones;
        else result += ones;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumSteps(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("101", 1, trials);
    test("100", 2, trials);
    test("0111", 0, trials);
    return 0;
}


