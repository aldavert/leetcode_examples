#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxCount(std::vector<int> banned, int n, int maxSum)
{
    int result = 0;
    const std::unordered_set<int> banned_set{banned.begin(), banned.end()};
    for (int i = 1, sum = 0; i <= n; ++i)
    {
        if (!banned_set.contains(i) && (sum + i <= maxSum))
        {
            ++result;
            sum += i;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> banned,
          int n,
          int maxSum,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxCount(banned, n, maxSum);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 6, 5}, 5, 6, 2, trials);
    test({1, 2, 3, 4, 5, 6, 7}, 8, 1, 0, trials);
    test({11}, 7, 50, 7, trials);
    return 0;
}


