#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int findTheLongestSubstring(std::string s)
{
    constexpr std::string_view VOWELS = "aeiou";
    int result = 0;
    std::unordered_map<int, int> prefix_to_index{{0, -1}};
    
    for (int i = 0, prefix = 0, n = static_cast<int>(s.size()); i < n; ++i)
    {
        if (int index = static_cast<int>(VOWELS.find(s[i])); index != -1)
            prefix ^= 1 << index;
        if (!prefix_to_index.count(prefix))
        prefix_to_index[prefix] = i;
        result = std::max(result, i - prefix_to_index[prefix]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findTheLongestSubstring(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("eleetminicoworoep", 13, trials);
    test("leetcodeisgreat", 5, trials);
    test("bcbcbc", 6, trials);
    return 0;
}


