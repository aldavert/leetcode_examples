#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> maxTargetNodes(std::vector<std::vector<int> > edges1,
                                std::vector<std::vector<int> > edges2)
{
    using Vector2D = std::vector<std::vector<int> >;
    auto buildGraph = [](const Vector2D edges) -> Vector2D
    {
        Vector2D graph(edges.size() + 1);
        for (const auto &edge : edges)
        {
            graph[edge[0]].push_back(edge[1]);
            graph[edge[1]].push_back(edge[0]);
        }
        return graph;
    };
    auto dfs = [](auto &&self,
                  const Vector2D &graph,
                  int u,
                  int prev,
                  std::vector<bool> &parity,
                  bool is_even) -> int
    {
        int result = is_even;
        parity[u] = is_even;
        for (int v : graph[u])
            if (v != prev)
                result += self(self, graph, v, u, parity, !is_even);
        return result;
    };
    const int n = static_cast<int>(edges1.size()) + 1,
              m = static_cast<int>(edges2.size()) + 1;
    const Vector2D graph1 = buildGraph(edges1), graph2 = buildGraph(edges2);
    std::vector<bool> parity1(n), parity2(m);
    const int even1 = dfs(dfs, graph1, 0, -1, parity1, true),
              even2 = dfs(dfs, graph2, 0, -1, parity2, true),
              odd1 = n - even1,
              odd2 = m - even2;
    std::vector<int> result;
    for (int i = 0; i < n; ++i)
        result.push_back(((parity1[i])?even1:odd1) + std::max(even2, odd2));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges1,
          std::vector<std::vector<int> > edges2,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxTargetNodes(edges1, edges2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {0, 2}, {2, 3}, {2, 4}},
         {{0, 1}, {0, 2}, {0, 3}, {2, 7}, {1, 4}, {4, 5}, {4, 6}},
         {8, 7, 7, 8, 8}, trials);
    test({{0, 1}, {0, 2}, {0, 3}, {0, 4}},
         {{0, 1}, {1, 2}, {2, 3}},
         {3, 6, 6, 6, 6}, trials);
    return 0;
}


