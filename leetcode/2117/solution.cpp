#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

std::string abbreviateProduct(int left, int right)
{
    constexpr long long MAXSUF = 1'000'000'000'000;
    int count_digits = 0, count_zeros = 0;
    long long suf = 1;
    double prod = 1.0;
    
    for (int num = left; num <= right; ++num)
    {
        prod *= num;
        while (prod >= 1.0)
        {
            prod /= 10;
            ++count_digits;
        }
        suf *= num;
        while (suf % 10 == 0)
        {
            suf /= 10;
            ++count_zeros;
        }
        if (suf > MAXSUF) suf %= MAXSUF;
    }
    
    if (count_digits - count_zeros <= 10)
    {
        long tens = static_cast<long>(std::pow(10, count_digits - count_zeros));
        return std::to_string(static_cast<long>(prod * static_cast<double>(tens) + 0.5))
             + 'e' + std::to_string(count_zeros);
    }
    
    std::string suf_str = std::to_string(suf);
    return std::to_string((long)(prod * std::pow(10, 5))) + "..."
         + suf_str.substr(suf_str.size() - 5) + 'e' + std::to_string(count_zeros);
}

// ############################################################################
// ############################################################################

void test(int left, int right, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = abbreviateProduct(left, right);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 4, "24e0", trials);
    test(2, 11, "399168e2", trials);
    test(371, 375, "7219856259e3", trials);
    return 0;
}


