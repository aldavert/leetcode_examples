#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int bestRotation(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> rotation(n);
    for (int i = 0; i < n; ++i)
        --rotation[(i - nums[i] + 1 + n) % n];
    for (int i = 1; i < n; ++i)
        rotation[i] += rotation[i - 1] + 1;
    int maximum_value = rotation[0], maximum_position = 0;
    for (int i = 1; i < n; ++i)
        if (rotation[i] > maximum_value)
            maximum_value = rotation[i],
            maximum_position = i;
    return maximum_position;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = bestRotation(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 1, 4, 0}, 3, trials);
    test({1, 3, 0, 2, 4}, 0, trials);
    return 0;
}


