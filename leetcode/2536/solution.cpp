#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > rangeAddQueries(int n,
                                               std::vector<std::vector<int> > queries)
{
    std::vector<std::vector<int> > result(n, std::vector<int>(n)),
                                   prefix(n, std::vector<int>(n + 1));
    for (const auto &query : queries)
    {
        for (int i = query[0]; i <= query[2]; ++i)
        {
            ++prefix[i][query[1]];
            --prefix[i][query[3] + 1];
        }
    }
    for (int i = 0; i < n; ++i)
    {
        int sum = 0;
        for (int j = 0; j < n; ++j)
        {
            sum += prefix[i][j];
            result[i][j] = sum;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > queries,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = rangeAddQueries(n, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{1, 1, 2, 2}, {0, 0, 1, 1}}, {{1, 1, 0}, {1, 2, 1}, {0, 1, 1}}, trials);
    test(2, {{0, 0, 1, 1}}, {{1, 1}, {1, 1}}, trials);
    return 0;
}


