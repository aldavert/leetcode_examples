#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool isPalindrome(int x)
{
    if (x < 0) return false;
    int values[10] = {}, size = 0;
    for (; x > 0; x /= 10)
        values[size++] = x % 10;
    for (int left = 0, right = size - 1; left < right; ++left, --right)
        if (values[left] != values[right])
            return false;
    return true;
}
#else
bool isPalindrome(int x)
{
    if (x < 0) return false;
    std::vector<int> values;
    values.reserve(10);
    for (; x > 0; x /= 10)
        values.push_back(x % 10);
    for (int l = 0, r = static_cast<int>(values.size()) - 1; l < r; ++l, --r)
        if (values[l] != values[r])
            return false;
    return true;
}
#endif

// ############################################################################
// ############################################################################

void test(int x, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPalindrome(x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(121, true, trials);
    test(123, false, trials);
    test(-121, false, trials);
    test(10, false, trials);
    return 0;
}


