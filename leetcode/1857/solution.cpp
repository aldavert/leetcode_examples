#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int largestPathValue(std::string colors, std::vector<std::vector<int> > edges)
{
    const int n = static_cast<int>(colors.size());
    int result = 0, processed = 0;
    std::vector<std::vector<int> > graph(n), count(n, std::vector<int>(26));
    std::vector<int> in_degree(n);
    std::queue<int> q;
    for (const auto &edge : edges)
        ++in_degree[edge[1]],
        graph[edge[0]].push_back(edge[1]);
    for (int i = 0; i < n; ++i)
        if (in_degree[i] == 0)
            q.push(i);
    while (!q.empty())
    {
        int out = q.front();
        q.pop();
        ++processed;
        result = std::max(result, ++count[out][colors[out] - 'a']);
        for (const int in : graph[out])
        {
            for (int i = 0; i < 26; ++i)
                count[in][i] = std::max(count[in][i], count[out][i]);
            if (--in_degree[in] == 0)
                q.push(in);
        }
    }
    return (processed == n)?result:-1;
}

// ############################################################################
// ############################################################################

void test(std::string colors,
          std::vector<std::vector<int> > edges,
          int solution,
          unsigned int trials = 1)
{
    int result = -2;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestPathValue(colors, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abaca", {{0, 1}, {0, 2}, {2, 3}, {3, 4}}, 3, trials);
    test("a", {{0, 0}}, -1, trials);
    return 0;
}


