#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int getNumberOfBacklogOrders(std::vector<std::vector<int> > orders)
{
    constexpr int MOD = 1'000'000'007;
    int result = 0;
    std::priority_queue<std::vector<int> > buys_heap;
    std::priority_queue<std::vector<int>,
                        std::vector<std::vector<int> >,
                        std::greater<> > sells_heap;
    
    for (const auto &order : orders)
    {
        if (order[2] == 0) buys_heap.push(order);
        else sells_heap.push(order);
        while (!buys_heap.empty() && !sells_heap.empty()
           && (buys_heap.top()[0] >= sells_heap.top()[0]))
        {
            const int min_amount = std::min(buys_heap.top()[1], sells_heap.top()[1]);
            std::vector<int> buys_heap_top = buys_heap.top();
            buys_heap.pop();
            buys_heap_top[1] -= min_amount;
            if (buys_heap_top[1] > 0)
                buys_heap.push(buys_heap_top);
            
            std::vector<int> sells_heap_top = sells_heap.top();
            sells_heap.pop();
            sells_heap_top[1] -= min_amount;
            if (sells_heap_top[1] > 0)
                sells_heap.push(sells_heap_top);
        }
    }
    while (!buys_heap.empty())
        result = (result + buys_heap.top()[1]) % MOD,
        buys_heap.pop();
    while (!sells_heap.empty())
        result = (result + sells_heap.top()[1]) % MOD,
        sells_heap.pop();
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > orders, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getNumberOfBacklogOrders(orders);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{10, 5, 0}, {15, 2, 1}, {25, 1, 1}, {30, 4, 0}}, 6, trials);
    test({{7, 1000000000, 1}, {15, 3, 0}, {5, 999999995, 0}, {5, 1, 1}},
          999999984, trials);
    return 0;
}


