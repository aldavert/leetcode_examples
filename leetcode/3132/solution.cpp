#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumAddedInteger(std::vector<int> nums1, std::vector<int> nums2)
{
    auto isValidDiff = [&](int inc) -> bool
    {
        int removed = 0;
        for (int n = static_cast<int>(nums2.size()), i = 0; int num : nums1)
        {
            if (num + inc == nums2[i])
            {
                if (++i == n) break;
            }
            else ++removed;
        }
        return removed <= 2;
    };
    int result = std::numeric_limits<int>::max();
    std::sort(nums1.begin(), nums1.end());
    std::sort(nums2.begin(), nums2.end());
    for (int i = 0; i < 3; ++i)
    {
        const int inc = nums2[0] - nums1[i];
        if (isValidDiff(inc))
            result = std::min(result, inc);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumAddedInteger(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 20, 16, 12, 8}, {14, 18, 10}, -2, trials);
    test({3, 5, 5, 3}, {7, 7}, 2, trials);
    return 0;
}


