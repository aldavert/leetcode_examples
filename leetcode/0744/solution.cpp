#include "../common/common.hpp"

// ############################################################################
// ############################################################################

char nextGreatestLetter(std::vector<char> letters, char target)
{
    auto search = std::upper_bound(letters.begin(), letters.end(), target);
    if (search == letters.end()) return letters[0];
    else return *search;
}

// ############################################################################
// ############################################################################

void test(std::vector<char> letters, char target, char solution, unsigned int trials = 1)
{
    char result = '\0';
    for (unsigned int i = 0; i < trials; ++i)
        result = nextGreatestLetter(letters, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({'c', 'f', 'j'}, 'a', 'c', trials);
    test({'c', 'f', 'j'}, 'c', 'f', trials);
    test({'c', 'f', 'j'}, 'd', 'f', trials);
    test({'a', 'b'}, 'z', 'a', trials);
    return 0;
}


