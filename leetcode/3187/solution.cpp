#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> countOfPeaks(std::vector<int> nums,
                              std::vector<std::vector<int> > queries)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> result, peak(n);
    for (int i = 1; i + 1 < n; ++i)
        peak[i] = (nums[i] > nums[i - 1]) && (nums[i] > nums[i + 1]);
    class FenwickTree
    {
        std::vector<int> sums;
        static inline int lowbit(int i) { return i & -i; }
    public:
        FenwickTree(int n) : sums(n + 1) {}
        void add(int i, int delta)
        {
            for (const int m = static_cast<int>(sums.size()); i < m; i += lowbit(i))
                sums[i] += delta;
        }
        int get(int i) const
        {
            int sum = 0;
            for (; i > 0; i -= lowbit(i))
                sum += sums[i];
            return sum;
        }
    };
    
    FenwickTree tree(static_cast<int>(peak.size()));
    for (int i = 0, m = static_cast<int>(peak.size()); i < m; ++i)
        tree.add(i + 1, peak[i]);
    auto update = [&](int i)
    {
        const int new_peak = (i > 0) && (i + 1 < n)
                          && (nums[i] > nums[i - 1])
                          && (nums[i] > nums[i + 1]);
        if (new_peak != peak[i])
        {
            tree.add(i + 1, new_peak - peak[i]);
            peak[i] = new_peak;
        }
    };
    
    for (const auto &query : queries)
    {
        if (query[0] == 1)
            result.push_back((query[2] - query[1] < 2)?0:
                              tree.get(query[2]) - tree.get(query[1] + 1));
        else if (query[0] == 2)
        {
            nums[query[1]] = query[2];
            update(query[1]);
            if (query[1] > 0) update(query[1] - 1);
            if (query[1] + 1 < n) update(query[1] + 1);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countOfPeaks(nums, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1, 4, 2, 5}, {{2, 3, 4}, {1, 0, 4}}, {0}, trials);
    test({4, 1, 4, 2, 1, 5}, {{2, 2, 4}, {1, 0, 2}, {1, 0, 4}}, {0, 1}, trials);
    return 0;
}


