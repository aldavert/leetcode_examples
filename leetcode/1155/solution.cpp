#include "../common/common.hpp"
#include <cstring>
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int numRollsToTarget(int n, int k, int target)
{
    constexpr int MOD = 1'000'000'007;
    int dp[2][1001] = {};
    dp[0][0] = 1;
    bool second = false;
    for (int dice = 0; dice < n; ++dice, second = !second)
    {
        std::memset(dp[!second], 0, sizeof(dp[!second]));
        for (int i = 1; i <= k; ++i)
            for (int t = i; t <= target; ++t)
                dp[!second][t] = (dp[!second][t] + dp[second][t - i]) % MOD;
    }
    return dp[second][target];
}
#else
int numRollsToTarget(int n, int k, int target)
{
    constexpr int MOD = 1'000'000'007;
    std::unordered_map<int, int> lut;
    auto signature = [](int idx, int value) -> int { return idx << 16 | value; };
    auto dp = [&](auto &&self, int idx, int value) -> int
    {
        if (idx == n)
            return static_cast<int>(value == target);
        else
        {
            if (auto search = lut.find(signature(idx, value)); search != lut.end())
                return search->second;
            else
            {
                int accum = 0;
                for (int i = 1; i <= k; ++i)
                {
                    if (int current = value + i; current <= target)
                    {
                        int output = self(self, idx + 1, current);
                        lut[signature(idx + 1, current)] = output;
                        accum = (accum + output) % MOD;
                    }
                }
                return accum;
            }
        }
    };
    return dp(dp, 0, 0);
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int k, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numRollsToTarget(n, k, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 6, 3, 1, trials);
    test(2, 6, 7, 6, trials);
    test(30, 30, 500, 222'616'187, trials);
    return 0;
}


