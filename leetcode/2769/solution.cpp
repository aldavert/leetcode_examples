#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int theMaximumAchievableX(int num, int t)
{
    return num + 2 * t;
}

// ############################################################################
// ############################################################################

void test(int num, int t, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = theMaximumAchievableX(num, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 1, 6, trials);
    test(3, 2, 7, trials);
    return 0;
}


