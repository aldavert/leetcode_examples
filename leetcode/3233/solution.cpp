#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int nonSpecialCount(int l, int r)
{
    const int max_root = static_cast<int>(std::sqrt(r));
    std::vector<bool> is_prime(max_root + 1, true);
    is_prime[0] = is_prime[1] = false;
    for (int i = 2; i * i <= max_root; ++i)
        if (is_prime[i])
            for (int j = i * i; j <= max_root; j += i)
                is_prime[j] = false;
    int special_count = 0;
    for (int num = 2; num <= max_root; ++num)
        special_count += (is_prime[num] && (l <= num * num) && (num * num <= r));
    return r - l + 1 - special_count;
}

// ############################################################################
// ############################################################################

void test(int l, int r, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = nonSpecialCount(l, r);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 7, 3, trials);
    test(4, 16, 11, trials);
    return 0;
}


