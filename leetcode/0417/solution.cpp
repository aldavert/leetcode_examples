#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > pacificAtlantic(std::vector<std::vector<int> > heights)
{
    const int m = static_cast<int>(heights.size()),
              n = static_cast<int>(heights[0].size());
    std::vector<std::vector<char> > seen(m, std::vector<char>(n, ' '));
    auto process = [&](auto &&self, int i, int j, int h, char ocean)
    {
        if ((i < 0) || (i == m) || (j < 0) || (j == n))
            return;
        if ((seen[i][j] == ocean) || (seen[i][j] == 'B') || (heights[i][j] < h))
            return;
        seen[i][j] = (seen[i][j] != ' ')?'B':ocean;
        self(self, i + 1, j, heights[i][j], ocean);
        self(self, i - 1, j, heights[i][j], ocean);
        self(self, i, j + 1, heights[i][j], ocean);
        self(self, i, j - 1, heights[i][j], ocean);
    };
    for (int i = 0; i < m; ++i)
    {
        process(process, i,     0, 0, 'P');
        process(process, i, n - 1, 0, 'A');
    }
    for (int j = 0; j < n; ++j)
    {
        process(process,     0, j, 0, 'P');
        process(process, m - 1, j, 0, 'A');
    }
    std::vector<std::vector<int> > result;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (seen[i][j] == 'B')
                result.push_back({i, j});
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > heights,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = pacificAtlantic(heights);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 2, 3, 5}, {3, 2, 3, 4, 4}, {2, 4, 5, 3, 1}, {6, 7, 1, 4, 5}, {5, 1, 1, 2, 4}}, {{0, 4}, {1, 3}, {1, 4}, {2, 2}, {3, 0}, {3, 1}, {4, 0}}, trials);
    test({{1}}, {{0, 0}}, trials);
    return 0;
}


