#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minOperationsMaxProfit(std::vector<int> customers,
                           int boardingCost,
                           int runningCost)
{
    int waiting = 0, profit = 0, max_profit = 0, rotate = 0, max_rotate = -1;
    for (int i = 0, n = static_cast<int>(customers.size()); (waiting > 0) || (i < n);)
    {
        if (i < n)
            waiting += customers[i++];
        const int new_onboard = std::min(waiting, 4);
        waiting -= new_onboard;
        profit += new_onboard * boardingCost - runningCost;
        ++rotate;
        if (profit > max_profit)
        {
            max_profit = profit;
            max_rotate = rotate;
        }
    }
    return max_rotate;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> customers,
          int boardingCost,
          int runningCost,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperationsMaxProfit(customers, boardingCost, runningCost);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 3}, 5, 6, 3, trials);
    test({10, 9, 6}, 6, 4, 7, trials);
    test({3, 4, 0, 5, 1}, 1, 92, -1, trials);
    return 0;
}


