#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int secondMinimum(int n, std::vector<std::vector<int> > edges, int time, int change)
{
    std::vector<std::vector<int> > graph(n + 1);
    std::queue<std::pair<int, int> > q{{{1, 0}}};
    std::vector<std::vector<int> > min_time(n + 1,
            std::vector<int>(2, std::numeric_limits<int>::max()));
    
    min_time[1][0] = 0;
    for (const auto &edge : edges)
    {
        graph[edge[0]].push_back(edge[1]);
        graph[edge[1]].push_back(edge[0]);
    }
    while (!q.empty())
    {
        auto [i, prev_time] = q.front();
        q.pop();
        int num_change_signal = prev_time / change;
        int wait_time = (num_change_signal & 1)?(change - (prev_time % change)):0;
        int new_time = prev_time + wait_time + time;
        for (int j : graph[i])
        {
            if (new_time < min_time[j][0])
            {
                min_time[j][0] = new_time;
                q.emplace(j, new_time);
            }
            else if ((min_time[j][0] < new_time) && (new_time < min_time[j][1]))
            {
                if (j == n) return new_time;
                min_time[j][1] = new_time;
                q.emplace(j, new_time);
            }
        }
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          int time,
          int change,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = secondMinimum(n, edges, time, change);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{1, 2}, {1, 3}, {1, 4}, {3, 4}, {4, 5}}, 3, 5, 13, trials);
    test(2, {{1, 2}}, 3, 2, 11, trials);
    return 0;
}


