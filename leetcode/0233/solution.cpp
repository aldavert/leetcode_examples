#include "../common/common.hpp"
#include <algorithm>

// ############################################################################
// ############################################################################

int countDigitOne(int n)
{
    constexpr int MAX = 1'000'000'000;
    int sol = 0;
    for (int p = MAX; p > 1; p /= 10)
        sol += n / p * (p / 10) + std::clamp(n % p - p / 10 + 1, 0, p / 10);
    return sol + (n == MAX);
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countDigitOne(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(13, 6, trials);
    test(0, 0, trials);
    return 0;
}


