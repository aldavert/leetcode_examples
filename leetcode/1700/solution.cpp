#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countStudents(std::vector<int> students, std::vector<int> sandwiches)
{
    auto condition = [](int v) { return v == 0; };
    size_t number_of[2];
    number_of[0] = std::count_if(students.begin(), students.end(), condition);
    number_of[1] = static_cast<int>(students.size()) - number_of[0];
    for (size_t i = 0; i < sandwiches.size(); )
    {
        if (number_of[sandwiches[i]] > 0)
            --number_of[sandwiches[i++]];
        else break;
    }
    return static_cast<int>(number_of[0] + number_of[1]);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> students,
          std::vector<int> sandwiches,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countStudents(students, sandwiches);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 0, 0}, {0, 1, 0, 1}, 0, trials);
    test({1, 1, 1, 0, 0, 1}, {1, 0, 0, 0, 1, 1}, 3, trials);
    return 0;
}


