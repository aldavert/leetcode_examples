#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int countBalls(int lowLimit, int highLimit)
{
    std::unordered_map<int, int> lut;
    int result = 0;
    for (int number = lowLimit;  number <= highLimit; ++number)
    {
        int current = number, box = 0;
        while (current) { box += current % 10; current /= 10; }
        result = std::max(result, ++lut[box]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int lowLimit, int highLimit, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countBalls(lowLimit, highLimit);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 10, 2, trials);
    test(5, 15, 2, trials);
    test(19, 28, 2, trials);
    return 0;
}


