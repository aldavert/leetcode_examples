#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<bool> findAnswer(std::vector<int> parent, std::string s)
{
    struct Manacher
    {
        std::vector<int> p;
        Manacher(int n, std::string str) :
            p(std::max(0, 2 * n - 1))
        {
            for (int i = 0, j = 0, k = 0; i <= 2 * (n - 1); ++i)
            {
                int d = (i < k)?std::min(p[j + j - i], (k - i) / 2):0;
                int a = i / 2 - d, b = (i + 1) / 2 + d;
                while ((0 <= a) && (b < n) && (str[a] == str[b]))
                {
                    --a;
                    ++b;
                    ++d;
                }
                p[i] = d;
                if (k < 2 * b - 1)
                {
                    j = i;
                    k = 2 * b - 1;
                }
            }
        }
        bool isPalindrome(int l, int r)
        {
            int m = l + r - 1;
            return m / 2 - p[m] < l;
        }
    };
    const int n = static_cast<int>(parent.size());
    std::vector<std::vector<int> > children = std::vector<std::vector<int> >(n);
    for (int i = 1; i < n; ++i)
        children[parent[i]].push_back(i);
    std::vector<std::pair<int, int> > ranges(n);
    std::string str;
    auto dfs = [&](auto &&self, int v) -> void
    {
        ranges[v].first = static_cast<int>(str.size());
        for (int w : children[v]) self(self, w);
        str += s[v];
        ranges[v].second = static_cast<int>(str.size());
    };
    dfs(dfs, 0);
    
    Manacher m(n, str);
    std::vector<bool> result;
    result.reserve(n);
    for (auto [l, r] : ranges)
        result.push_back(m.isPalindrome(l, r));
    return result;
}
#else
std::vector<bool> findAnswer(std::vector<int> parent, std::string s)
{
    class Hashing
    {
        std::vector<long long> p;
        std::vector<long long> h;
        long long mod;
    public:
        Hashing(std::string word, long long base, int m) :
            p(word.size() + 1),
            h(word.size() + 1),
            mod(m)
        {
            p[0] = 1;
            for (size_t i = 1, n = word.size(); i <= n; ++i)
            {
                p[i] = (p[i - 1] * base) % m;
                h[i] = (h[i - 1] * base + word[i - 1] - 'a') % m;
            }
        }
        inline long long query(int l, int r) const
        {
            return (h[r] - h[l - 1] * p[r - l + 1] % mod + mod) % mod;
        }
    };
    
    const int n = static_cast<int>(s.size());
    std::vector<std::vector<int> > g(n);
    for (int i = 1; i < n; ++i)
        g[parent[i]].push_back(i);
    std::string dfs_str;
    std::vector<std::pair<int, int> > pos(n);
    auto dfs = [&](auto &&self, int i) -> void
    {
        int l = static_cast<int>(dfs_str.size()) + 1;
        for (int j : g[i]) self(self, j);
        dfs_str.push_back(s[i]);
        pos[i] = {l, static_cast<int>(dfs_str.size())};
    };
    dfs(dfs, 0);
    constexpr int base = 13'331, mod = 998'244'353;
    Hashing h1(dfs_str, base, mod);
    std::reverse(dfs_str.begin(), dfs_str.end());
    Hashing h2(dfs_str, base, mod);
    std::vector<bool> result(n);
    for (int i = 0; i < n; ++i)
    {
        auto [l, r] = pos[i];
        int k = r - l + 1;
        result[i] = h1.query(l, l + k / 2 - 1)
                 == h2.query(n - r + 1, n - r + 1 + k / 2 - 1);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> parents,
          std::string s,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findAnswer(parents, s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, 0, 0, 1, 1, 2}, "aababa", {true, true, false, true, true, true}, trials);
    test({-1, 0, 0, 0, 0}, "aabcb", {true, true, true, true, true}, trials);
    return 0;
}


