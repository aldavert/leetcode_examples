#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxProduct(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = nums[0];
    for (int i = 1, cmax = nums[0], cmin = nums[0]; i < n; ++i)
    {
        int temp = std::max(nums[i], std::max(nums[i] * cmax, nums[i] * cmin));
        cmin = std::min(nums[i], std::min(nums[i] * cmax, nums[i] * cmin));
        cmax = temp;
        result = std::max(result, cmax);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxProduct(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, -2, 4}, 6, trials);
    test({-2, 0, -1}, 0, trials);
    return 0;
}


