#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maxPoints(std::vector<std::vector<int> > points)
{
    const int n = static_cast<int>(points[0].size());
    std::vector<long> left_to_right(n), right_to_left(n), dp(n);
    
    for (const auto &row : points)
    {
        long running_max = 0;
        for (int j = 0; j < n; ++j)
        {
            running_max = std::max(running_max - 1, dp[j]);
            left_to_right[j] = running_max;
        }
        running_max = 0;
        for (int j = n - 1; j >= 0; --j)
        {
            running_max = std::max(running_max - 1, dp[j]);
            right_to_left[j] = running_max;
        }
        for (int j = 0; j < n; ++j)
            dp[j] = std::max(left_to_right[j], right_to_left[j]) + row[j];
    }
    return *std::max_element(dp.begin(), dp.end());
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxPoints(points);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {1, 5, 1}, {3, 1, 1}}, 9, trials);
    test({{1, 5}, {2, 3}, {4, 2}}, 11, trials);
    return 0;
}


