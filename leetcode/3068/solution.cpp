#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maximumValueSum(std::vector<int> nums,
                          int k,
                          [[maybe_unused]] std::vector<std::vector<int> > edges)
{
    long long max_sum = 0;
    int changed_count = 0;
    int min_change_diff = std::numeric_limits<int>::max();
    for (int num : nums)
    {
        max_sum += std::max(num, num ^ k);
        changed_count += ((num ^ k) > num) ? 1 : 0;
        min_change_diff = std::min(min_change_diff, std::abs(num - (num ^ k)));
    }
    if (changed_count % 2 == 0) return max_sum;
    return max_sum - min_change_diff;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          std::vector<std::vector<int> > edges,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumValueSum(nums, k, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1}, 3, {{0, 1}, {0, 2}}, 6, trials);
    test({2, 3}, 7, {{0, 1}}, 9, trials);
    test({7, 7, 7, 7, 7, 7}, 3, {{0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}}, 42, trials);
    return 0;
}


