#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestMountain(std::vector<int> arr)
{
    if (arr.size() == 0) return 0;
    const size_t n = arr.size() - 1;
    int result = 0;
    for (size_t i = 0; i < n; )
    {
        while ((i < n) && (arr[i] == arr[i + 1]))
            ++i;
        int up = 0, down = 0;
        for (; (i < n) && (arr[i] < arr[i + 1]); ++up, ++i);
        for (; (i < n) && (arr[i] > arr[i + 1]); ++down, ++i);
        if ((up > 0) && (down > 0))
            result = std::max(result, up + down + 1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestMountain(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 4, 7, 3, 2, 5}, 5, trials);
    test({2, 2, 2}, 0, trials);
    return 0;
}


