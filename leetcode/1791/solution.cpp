#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findCenter(std::vector<std::vector<int> > edges)
{
    return edges[0][(edges[0][0] != edges[1][0]) && (edges[0][0] != edges[1][1])];
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findCenter(edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 3}, {4, 2}}, 2, trials);
    test({{1, 2}, {5, 1}, {1, 3}, {1, 4}}, 1, trials);
    return 0;
}


