#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countPalindromes(std::string s)
{
    constexpr int MOD = 1'000'000'007;
    constexpr int PATTERNSIZE = 5;
    long result = 0;
    
    for (char a = '0'; a <= '9'; ++a)
    {
        for (char b = '0'; b <= '9'; ++b)
        {
            const std::vector<char> pattern{a, b, '.', b, a};
            long dp[PATTERNSIZE + 1] = {};
            dp[PATTERNSIZE] = 1;
            for (char c : s)
                for (int i = 0; i < PATTERNSIZE; ++i)
                    if ((pattern[i] == '.') || (pattern[i] == c))
                        dp[i] += dp[i + 1];
            result = (result + dp[0]) % MOD;
        }
    }
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPalindromes(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("103301", 2, trials);
    test("0000000", 21, trials);
    test("9999900000", 2, trials);
    return 0;
}


