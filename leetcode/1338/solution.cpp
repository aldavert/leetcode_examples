#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int minSetSize(std::vector<int> arr)
{
    std::unordered_map<int, int> histogram;
    for (int v : arr)
    {
        if (auto it = histogram.find(v); it != histogram.end())
            ++(it->second);
        else histogram[v] = 1;
    }
    std::vector<std::pair<int, int> > elements(histogram.begin(), histogram.end());
    std::sort(elements.begin(), elements.end(),
            [](const std::pair<int, int> &left, const std::pair<int, int> &right)
            { return left.second > right.second; });
    const int mid = static_cast<int>(arr.size() / 2);
    int result = 0, accum = 0;
    for (auto &v : elements)
    {
        ++result;
        accum += v.second;
        if (accum >= mid) break;
    }
    return result;
}

// ############################################################################
// ############################################################################


void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSetSize(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 3, 3, 3, 5, 5, 5, 2, 2, 7}, 2, trials);
    test({7, 7, 7, 7, 7, 7}, 1, trials);
    test({1, 9}, 1, trials);
    test({1000, 1000, 3, 7}, 1, trials);
    test({1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 5, trials);
    return 0;
}


