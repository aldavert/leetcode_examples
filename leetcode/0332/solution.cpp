#include "../common/common.hpp"
#include <unordered_map>
#include <set>

// ############################################################################
// ############################################################################

std::vector<std::string> findItinerary(std::vector<std::vector<std::string> > tickets)
{
    std::vector<std::string> result(tickets.size() + 1);
    std::unordered_map<std::string, std::multiset<std::string> > graph;
    for (const auto &t : tickets)
        graph[t[0]].insert(t[1]);
    int idx = static_cast<int>(tickets.size());
    auto process = [&](auto &&self, std::string airport) -> void
    {
        while (!graph[airport].empty())
        {
            auto first = graph[airport].begin();
            std::string next = *(first);
            graph[airport].erase(first);
            self(self, next);
        }
        result[idx--] = airport;
    };
    process(process, "JFK");
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<std::string> > tickets,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findItinerary(tickets);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{"MUC", "LHR"}, {"JFK", "MUC"}, {"SFO", "SJC"}, {"LHR", "SFO"}},
         {"JFK", "MUC", "LHR", "SFO", "SJC"}, trials);
    test({{"JFK", "SFO"}, {"JFK", "ATL"}, {"SFO", "ATL"}, {"ATL", "JFK"},
          {"ATL", "SFO"}}, {"JFK", "ATL", "JFK", "SFO", "ATL", "SFO"}, trials);
    return 0;
}


