#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> >
insert(std::vector<std::vector<int> > intervals,
       std::vector<int> newInterval)
{
    std::vector<std::vector<int> > result;
    const size_t n = intervals.size();
    size_t i = 0;
    while ((i < n) && (intervals[i][1] < newInterval[0]))
    {
        result.push_back(intervals[i]);
        ++i;
    }
    size_t begin = i;
    while ((i < n) && (intervals[i][0] <= newInterval[1])) ++i;
    if ((i == 0) || (begin == n)) result.push_back(newInterval);
    else result.push_back({std::min(intervals[begin][0], newInterval[0]),
                           std::max(intervals[i - 1][1], newInterval[1])});
    while (i < n)
    {
        result.push_back(intervals[i]);
        ++i;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > intervals,
          std::vector<int> newInterval,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = insert(intervals, newInterval);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 3}, {6, 9}}, {2, 5},
         {{1, 5}, {6, 9}}, trials);
    test({{1, 3}, {6, 9}}, {0, 5},
         {{0, 5}, {6, 9}}, trials);
    test({{3, 5}, {6, 9}}, {0, 2},
         {{0, 2}, {3, 5}, {6, 9}}, trials);
    test({{1, 5}}, {5, 7},
         {{1, 7}}, trials);
    test({{1, 5}}, {6, 8},
         {{1, 5}, {6, 8}}, trials);
    test({{1, 2}, {3, 5}, {6, 7}, {8, 10}, {12, 16}}, {4, 8},
         {{1, 2}, {3, 10}, {12, 16}}, trials);
    return 0;
}


