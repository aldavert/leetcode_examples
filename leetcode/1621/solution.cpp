#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int numberOfSets(int n, int k)
{
    constexpr long MOD = 1'000'000'007;
    if (k == 1) return n * (n - 1) / 2;
    std::vector<std::vector<long>> dp(n, std::vector<long>(k));
    for (int i = 1; i < n; ++i)
        dp[i][0] = dp[i - 1][0] + (i * (i + 1)) / 2;
    for (int i = 2; i < n; ++i)
        for (int j = 1; (j < i) && (j < k); ++j)
            dp[i][j] = (dp[i - 1][j] - dp[i - 2][j] + dp[i - 1][j - 1] + MOD) % MOD
                     + dp[i - 1][j];
    return static_cast<int>((dp[n - 1][k - 1] - dp[n - 2][k - 1]) % MOD);
}
#else
int numberOfSets(int n, int k)
{
    auto execute = [&](void) -> int
    {
        std::vector<std::vector<std::vector<int> > > mem(n,
                std::vector<std::vector<int> >(k + 1, std::vector<int>(2, -1)));
        const int MOD = 1'000'000'007;
        auto inner = [&](auto &&self, int i, int j, bool drawing) -> int
        {
            if (j == 0) return 1;
            if (i == n) return 0;
            if (mem[i][j][drawing] != -1)
                return mem[i][j][drawing];
            if (drawing)
                return mem[i][j][drawing] = (self(self, i + 1, j, true)
                                          +  self(self, i, j - 1, false)) % MOD;
            return mem[i][j][drawing] = (self(self, i + 1, j, false)
                                      +  self(self, i + 1, j, true)) % MOD;
        };
        return inner(inner, 0, k, false);
    };
    return execute();
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfSets(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 2, 5, trials);
    test(3, 1, 3, trials);
    test(30, 7, 796'297'179, trials);
    return 0;
}


