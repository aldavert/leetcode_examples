# Binary Tree Preorder Traversal

Given the `root` of a binary tree, return *the preorder traversal of its nodes' values*.

#### Example 1:
> *Input:* `root = [1, null, 2, 3]`  
> *Output:* `[1, 2, 3]`  
> *Explanation:*
> ```mermaid
> graph TD;
> A((1))---EMPTY1(( ))
> A---B((2))
> B---C((3))
> B---EMPTY2(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY1,EMPTY2 empty;
> linkStyle 0,3 stroke-width:0px;
> ```

#### Example 2:
> *Input:* `root = [1, 2, 3, 4, 5, null, 8, null, null, 6, 7, 9]`  
> *Output:* `[1, 2, 4, 5, 6, 7, 3, 8, 9]`  
> *Explanation:*
> ```mermaid
> graph TD;
> A((1))---B((2))
> A---C((3))
> B---D((4))
> B---E((5))
> E---F((6))
> E---G((7))
> C---EMPTY1(( ))
> C---H((8))
> H---I((9))
> H---EMPTY2(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY1,EMPTY2 empty;
> linkStyle 6,9 stroke-width:0px;
> ```

#### Example 3:
> *Input:* `root = []`  
> *Output:* `[]`

#### Example 4:
> *Input:* `root = [1]`  
> *Output:* `[1]`
 
#### Constraints:
- The number of nodes in the tree is in the range `[0, 100]`.
- `-100 <= Node.val <= 100`

**Follow up:** Recursive solution is trivial, could you do it iteratively?


