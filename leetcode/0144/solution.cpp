#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 0
std::vector<int> preorderTraversal(TreeNode * root)
{
    std::vector<int> result;
    auto traverse = [&](auto &&self, TreeNode * ptr)
    {
        if (!ptr) return;
        result.push_back(ptr->val);
        self(self, ptr->left);
        self(self, ptr->right);
    };
    traverse(traverse, root);
    return result;
}
#else
std::vector<int> preorderTraversal(TreeNode * root)
{
    std::stack<TreeNode *> stack;
    std::vector<int> result;
    for (TreeNode * ptr = root;;)
    {
        for (; ptr != nullptr; ptr = ptr->left)
        {
            result.push_back(ptr->val);
            stack.push(ptr);
        }
        if (!stack.empty())
        {
            ptr = stack.top();
            stack.pop();
            ptr = ptr->right;
        }
        else break;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = preorderTraversal(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, null, 2, 3}, {1, 2, 3}, trials);
    test({}, {}, trials);
    test({1}, {1}, trials);
    //    1
    //  2   3
    // 4 5 6 7
    //8 9
    test({1, 2, 3, 4, 5, 6, 7, 8, 9}, {1, 2, 4, 8, 9, 5, 3, 6, 7}, trials);
    return 0;
}


