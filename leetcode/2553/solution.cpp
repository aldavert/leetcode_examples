#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<int> separateDigits(std::vector<int> nums)
{
    std::vector<int> result;
    for (int number : nums)
    {
        int digits[16] = {}, number_of_digits = 0;
        for (; number; number /= 10) digits[number_of_digits++] = number % 10;
        while (number_of_digits) result.push_back(digits[--number_of_digits]);
    }
    return result;
}
#else
std::vector<int> separateDigits(std::vector<int> nums)
{
    std::vector<int> result;
    for (int number : nums)
    {
        std::stack<int> digits;
        for (; number; number /= 10) digits.push(number % 10);
        for (; !digits.empty(); digits.pop()) result.push_back(digits.top());
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = separateDigits(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({13, 25, 83, 77}, {1, 3, 2, 5, 8, 3, 7, 7}, trials);
    test({7, 1, 3, 9}, {7, 1, 3, 9}, trials);
    return 0;
}



