#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int valueAfterKSeconds(int n, int k)
{
    constexpr int MOD = 1'000'000'007;
    int m = n + k - 1;
    std::vector<long> fact(m + 1), inv_fact(m + 1), inv(m + 1);
    inv[0] = inv[1] = fact[0] = inv_fact[0] = 1;
    for (int i = 1; i <= m; ++i)
    {
        if (i >= 2) inv[i] = MOD - MOD / i * inv[MOD % i] % MOD;
        fact[i] = fact[i - 1] * i % MOD;
        inv_fact[i] = inv_fact[i - 1] * inv[i] % MOD;
    }
    return static_cast<int>(fact[n + k - 1] * inv_fact[n - 1] % MOD * inv_fact[k] % MOD);
}

// ############################################################################
// ############################################################################

void test(int n, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = valueAfterKSeconds(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 5, 56, trials);
    test(5, 3, 35, trials);
    return 0;
}


