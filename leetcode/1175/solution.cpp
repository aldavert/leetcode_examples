#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numPrimeArrangements(int n)
{
    constexpr int MOD = 1'000'000'007;
    constexpr int PRIMES[] = { 2,  3,  5,  7, 11, 13, 17, 19, 23, 29, 31, 37, 41,
                              43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
    auto factorial = [&MOD](int v) -> long 
    {
        long result = 1;
        for (; v > 0; --v) result = (result * v) % MOD;
        return result;
    };
    int nprimes = static_cast<int>(
                std::distance(PRIMES, std::upper_bound(PRIMES, PRIMES + 25, n)));
    return static_cast<int>((factorial(nprimes) * factorial(n - nprimes)) % MOD);
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numPrimeArrangements(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 12, trials);
    test(100, 682289015, trials);
    return 0;
}


