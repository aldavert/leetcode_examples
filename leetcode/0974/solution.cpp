#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int subarraysDivByK(std::vector<int> nums, int k)
{
    int result = 0;
    std::vector<int> count(k, 0);
    count[0] = 1;
    for (int prefix = 0; int value : nums)
    {
        prefix = (prefix + value % k + k) % k;
        result += count[prefix];
        ++count[prefix];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = subarraysDivByK(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 5, 0, -2, -3, 1}, 5, 7, trials);
    test({5}, 9, 0, trials);
    return 0;
}


