#include "../common/common.hpp"

// ############################################################################
// ############################################################################

class MagicDictionary
{
    struct Trie
    {
        Trie(void) : is_word(false)
        {
            for (size_t i = 0; i < 26; ++i) next[i] = nullptr;
        }
        ~Trie(void)
        {
            for (size_t i = 0; i < 26; ++i) delete next[i];
        }
        void clear(void)
        {
            for (size_t i = 0; i < 26; ++i)
            {
                delete next[i];
                next[i] = nullptr;
            }
            is_word = false;
        }
        void add(std::string word, size_t i)
        {
            if (i == word.size())
            {
                is_word = true;
                return;
            }
            size_t idx = static_cast<size_t>(word[i] - 'a');
            if (next[idx] == nullptr)
                next[idx] = new Trie();
            next[idx]->add(word, i + 1);
        }
        bool search(const std::string &word, size_t i, int number_of_changes) const
        {
            if (i == word.size())
                return (number_of_changes == 0) && is_word;
            if (number_of_changes < 0) return false;
            size_t idx = static_cast<size_t>(word[i] - 'a');
            for (size_t j = 0; j < 26; ++j)
                if (next[j]
                &&  next[j]->search(word, i + 1, number_of_changes - (j != idx)))
                        return true;
            return false;
        }
        Trie * next[26] = {};
        bool is_word = false;
    };
    Trie root;
public:
    MagicDictionary(void)
    {
    }
    void buildDict(std::vector<std::string> dictionary)
    {
        for (std::string codeword : dictionary)
            root.add(codeword, 0);
    }
    bool search(std::string searchWord)
    {
        return root.search(searchWord, 0, 1);
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<std::string> dictionary,
          std::vector<std::string> search,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        MagicDictionary object;
        object.buildDict(dictionary);
        result.clear();
        for (std::string word : search)
            result.push_back(object.search(word));
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"hello", "leetcode"}, {"hello", "hhllo", "hell", "leetcoded"},
         {false, true, false, false}, trials);
    test({"hello", "hallo", "leetcode"}, {"hello", "hhllo", "hell", "leetcoded"},
         {true, true, false, false}, trials);
    return 0;
}


