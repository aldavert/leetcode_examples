#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> resultArray(std::vector<int> nums)
{
    if (nums.size() <= 2) return nums;
    std::vector<int> arr1, arr2;
    arr1.push_back(nums[0]);
    arr2.push_back(nums[1]);
    for (size_t i = 2; i < nums.size(); ++i)
    {
        if (arr1.back() > arr2.back()) arr1.push_back(nums[i]);
        else arr2.push_back(nums[i]);
    }
    for (int value : arr2)
        arr1.push_back(value);
    return arr1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = resultArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3}, {2, 3, 1}, trials);
    test({5, 4, 3, 8}, {5, 3, 4, 8}, trials);
    return 0;
}


