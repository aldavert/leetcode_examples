#include "../common/common.hpp"
#include "values_testA.hpp"

// ############################################################################
// ############################################################################

int numMatchingSubseq(std::string s, std::vector<std::string> words)
{
    const size_t n = words.size();
    size_t position[26] = {};
    int result = 0;
    if (n < 26)
    {
        for (size_t i = 0; i < n; ++i)
            position[i] = 0;
        
        for (char c : s)
        {
            for (size_t i = 0; i < n; ++i)
                if ((position[i] < words[i].size()) && (c == words[i][position[i]]))
                    ++position[i];
        }
        for (size_t i = 0; i < n; ++i)
            if (position[i] == words[i].size())
                ++result;
    }
    else
    {
        std::pair<size_t, size_t> lut[26][5'000], aux[5'000];
        for (size_t i = 0; i < 26; ++i)
            position[i] = 0;
        for (size_t i = 0; i < words.size(); ++i)
            lut[words[i][0] - 'a'][position[words[i][0] - 'a']++] = {0, i};
        for (char c : s)
        {
            size_t idx = c - 'a', position_aux = 0;
            for (size_t i = 0; i < position[idx]; ++i)
            {
                auto [str_pos, word_pos] = lut[idx][i];
                ++str_pos;
                if (str_pos < words[word_pos].size()) [[likely]]
                {
                    size_t new_idx = words[word_pos][str_pos] - 'a';
                    if (new_idx == idx)
                        aux[position_aux++] = {str_pos, word_pos};
                    else lut[new_idx][position[new_idx]++] = {str_pos, word_pos};
                }
                else ++result;
            }
            position[idx] = 0;
            for (size_t i = 0; i < position_aux; ++i)
                lut[idx][position[idx]++] = aux[i];
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<std::string> words,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numMatchingSubseq(s, words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    //constexpr unsigned int trials = 1;
    constexpr unsigned int trials = 10'000;
#endif
    test("abcde", {"a", "bb", "acd", "ace"}, 3, trials);
    test("dsahjpjauf", {"ahjpjau", "ja", "ahbwzgqnuk", "tnmlanowax"}, 2, trials);
    test(testA::s, testA::words, 1000, trials);
    return 0;
}


