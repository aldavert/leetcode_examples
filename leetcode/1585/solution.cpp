#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isTransformable(std::string s, std::string t)
{
    std::vector<int> position[10];
    int count[10] = {}, frequency[10] = {};
    for (int i = 0, n = static_cast<int>(s.size()); i < n; ++i)
        position[s[i] - '0'].push_back(i),
        ++frequency[s[i] - '0'];
    for (int i = 0, n = static_cast<int>(t.size()); i < n; ++i)
    {
        int d = t[i] - '0';
        if (count[d] + 1 > frequency[d]) return false;
        for (int sd = 0; sd < d; ++sd)
            if ((count[sd] < frequency[sd])
            &&  (position[sd][count[sd]] < position[d][count[d]]))
                return false;
        ++count[d];
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string t, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isTransformable(s, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("84532", "34852", true, trials);
    test("34521", "23415", true, trials);
    test("12345", "12435", false, trials);
    return 0;
}


