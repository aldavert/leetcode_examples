#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int numberOfWays(int n, int x)
{
    constexpr int MOD = 1'000'000'007;
    int dp[301] = {};
    
    dp[0] = 1;
    for (int a = 1, ax = 0; (ax = static_cast<int>(std::pow(a, x))) <= n; ++a)
        for (int i = n; i >= ax; --i)
            dp[i] = (dp[i] + dp[i - ax]) % MOD;
    return dp[n];
}

// ############################################################################
// ############################################################################

void test(int n, int x, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfWays(n, x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, 2, 1, trials);
    test(4, 1, 2, trials);
    return 0;
}


