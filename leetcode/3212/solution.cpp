#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfSubmatrices(std::vector<std::vector<char> > grid)
{
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    int result = 0;
    std::vector<std::vector<int> > x(m + 1, std::vector<int>(n + 1)),
                                   y(m + 1, std::vector<int>(n + 1));
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            x[i + 1][j + 1] = (grid[i][j] == 'X') + x[i][j + 1] + x[i + 1][j] - x[i][j];
            y[i + 1][j + 1] = (grid[i][j] == 'Y') + y[i][j + 1] + y[i + 1][j] - y[i][j];
            result += ((x[i + 1][j + 1] > 0) && (x[i + 1][j + 1] == y[i + 1][j + 1]));
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfSubmatrices(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'X', 'Y', '.'}, {'Y', '.', '.'}}, 3, trials);
    test({{'X', 'X'}, {'X', 'Y'}}, 0, trials);
    test({{'.', '.'}, {'.', '.'}}, 0, trials);
    return 0;
}


