#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxTurbulenceSize(std::vector<int> arr)
{
    const int n = static_cast<int>(arr.size());
    int sign = 0, previous = 0, result = 0, i, j;
    for (i = 0, j = 0; j < n - 1; ++j)
    {
        sign = arr[j] - arr[j + 1];
        sign = (0 < sign) - (sign < 0);
        
        if (sign == 0)
        {
            result = std::max(result, j - i + 1);
            i = j + 1;
        }
        if (sign != -previous)
        {
            result = std::max(result, j - i + 1);
            i = j;
        } 
        previous = sign;
    }
    return std::max(result, j - i + 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxTurbulenceSize(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({9, 4, 2, 10, 7, 8, 8, 1, 9}, 5, trials);
    test({4, 8, 12, 16}, 2, trials);
    test({100}, 1, trials);
    return 0;
}


