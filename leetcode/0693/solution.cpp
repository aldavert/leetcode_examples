#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool hasAlternatingBits(int n)
{
    for (bool positive = n & 1; n > 0; positive = !positive, n >>= 1)
        if ((n & 1) != positive)
            return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(int n, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = hasAlternatingBits(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test( 5,  true, trials);
    test( 7, false, trials);
    test(11, false, trials);
    return 0;
}
