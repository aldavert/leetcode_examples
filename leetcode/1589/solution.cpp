#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxSumRangeQuery(std::vector<int> nums, std::vector<std::vector<int> > requests)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(nums.size());
    long result = 0;
    std::vector<int> count(n);
    
    for (const auto &request : requests)
    {
        ++count[request[0]];
        if (request[1] + 1 < n)
            --count[request[1] + 1];
    }
    for (int i = 1; i < n; ++i)
        count[i] += count[i - 1];
    std::sort(count.begin(), count.end());
    std::sort(nums.begin(), nums.end());
    for (int i = 0; i < n; ++i)
        result = (result + static_cast<long>(nums[i]) * count[i]) % MOD;
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::vector<int> >  requests,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSumRangeQuery(nums, requests);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, {{1, 3}, {0, 1}}, 19, trials);
    test({1, 2, 3, 4, 5, 6}, {{0, 1}}, 11, trials);
    test({1, 2, 3, 4, 5, 10}, {{0, 2}, {1, 3}, {1, 1}}, 47, trials);
    return 0;
}


