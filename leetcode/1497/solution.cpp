#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canArrange(std::vector<int> arr, int k)
{
    std::vector<int> bucket(k);
    
    for (int a : arr)
    {
        int i = a % k;
        if (i < 0)
            i += k;
        ++bucket[i];
    }
    if (bucket[0] % 2 != 0)
        return false;
    for (int i = 1; i <= k / 2; ++i)
        if (bucket[i] != bucket[k - i])
            return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int k, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canArrange(arr, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 10, 6, 7, 8, 9}, 5, true, trials);
    test({1, 2, 3, 4, 5, 6}, 7, true, trials);
    test({1, 2, 3, 4, 5, 6}, 10, false, trials);
    test({-1, -1, -1, -1, 2, 2, -2, -2}, 3, false, trials);
    return 0;
}


