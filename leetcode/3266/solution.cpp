#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<int> getFinalState(std::vector<int> nums, int k, int multiplier)
{
    constexpr int MOD = 1'000'000'007;
    auto modPow = [&](auto &&self, long x, long n) -> long
    {
        if (n == 0) return 1;
        if (n % 2 == 1) return x * self(self, x, n - 1) % MOD;
        return self(self, x * x % MOD, n / 2);
    };
    if (multiplier == 1) return nums;
    
    const int n = static_cast<int>(nums.size());
    const int max_num = *std::max_element(nums.begin(), nums.end());
    std::vector<int> result(n);
    std::priority_queue<std::pair<int, int>,
                        std::vector<std::pair<int, int> >,
                        std::greater<> > heap;
    for (int i = 0; i < n; ++i)
        heap.emplace(nums[i], i);
    while ((k > 0) && (static_cast<long>(heap.top().first) * multiplier <= max_num))
    {
        const auto [num, i] = heap.top();
        heap.pop();
        heap.emplace(num * multiplier, i);
        --k;
    }
    std::vector<std::pair<int, int> > sorted_indexed_nums;
    while (!heap.empty())
        sorted_indexed_nums.push_back(heap.top()),
        heap.pop();
    const int multiplies_per_num = k / n;
    const int remaining_k = k % n;
    for (auto& [num, _] : sorted_indexed_nums)
    {
        long pow = modPow(modPow, multiplier, multiplies_per_num);
        num = static_cast<int>((num * pow) % MOD);
    }
    for (int i = 0; i < remaining_k; ++i)
    {
        long value = static_cast<long>(sorted_indexed_nums[i].first) * multiplier % MOD;
        sorted_indexed_nums[i].first = static_cast<int>(value);
    }
    for (const auto& [num, i] : sorted_indexed_nums)
        result[i] = num;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          int multiplier,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getFinalState(nums, k, multiplier);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3, 5, 6}, 5, 2, {8, 4, 6, 5, 6}, trials);
    test({100000, 2000}, 2, 1000000, {999999307, 999999993}, trials);
    return 0;
}


