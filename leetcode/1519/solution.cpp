#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> countSubTrees(int n,
                               std::vector<std::vector<int> > edges,
                               std::string labels)
{
    std::unordered_map<int, std::vector<int> > neighbors;
    std::vector<bool> available(n, true);
    std::vector<int> count(n, 0);
    for (const auto &edge : edges)
    {
        neighbors[edge[0]].push_back(edge[1]);
        neighbors[edge[1]].push_back(edge[0]);
    }
    auto traverse = [&](auto &&self, int node) -> std::vector<int>
    {
        available[node] = false;
        std::vector<int> histogram(26, 0);
        ++histogram[labels[node] - 'a'];
        for (int neighbor : neighbors[node])
        {
            if (available[neighbor])
            {
                auto current = self(self, neighbor);
                for (int i = 0; i < 26; ++i)
                    histogram[i] += current[i];
            }
        }
        count[node] = histogram[labels[node] - 'a'];
        return histogram;
    };
    traverse(traverse, 0);
    return count;
}
#else
std::vector<int> countSubTrees(int n,
                               std::vector<std::vector<int> > edges,
                               std::string labels)
{
    std::unordered_map<int, std::vector<int> > neighbors;
    std::vector<bool> available(n, true);
    std::vector<int> count(n, 0);
    for (const auto &edge : edges)
    {
        neighbors[edge[0]].push_back(edge[1]);
        neighbors[edge[1]].push_back(edge[0]);
    }
    auto traverse = [&](auto &&self, int node) -> std::unordered_map<char, int>
    {
        available[node] = false;
        std::unordered_map<char, int> histogram;
        ++histogram[labels[node]];
        for (int neighbor : neighbors[node])
        {
            if (available[neighbor])
            {
                auto current = self(self, neighbor);
                for (auto [letter, frequency] : current)
                    histogram[letter] += frequency;
            }
        }
        count[node] = histogram[labels[node]];
        return histogram;
    };
    traverse(traverse, 0);
    return count;
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::string labels,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSubTrees(n, edges, labels);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, {{0, 1}, {0, 2}, {1, 4}, {1, 5}, {2, 3}, {2, 6}},
         "abaedcd", {2, 1, 1, 1, 1, 1, 1}, trials);
    test(4, {{0, 1}, {1, 2}, {0, 3}},
         "bbbb", {4, 2, 1, 1}, trials);
    test(5, {{0, 1}, {0, 2}, {1, 3}, {0, 4}},
         "aabab", {3, 2, 1, 1, 1}, trials);
    return 0;
}


