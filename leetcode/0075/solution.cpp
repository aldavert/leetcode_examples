#include "../common/common.hpp"
#include <algorithm>

// ############################################################################
// ############################################################################

#if 1
void sortColors(std::vector<int> &nums)
{
    int begin = 0, end = static_cast<int>(nums.size()) - 1;
    for (int i = 0; i <= end;)
    {
        if (nums[i] == 0)
        {
            std::swap(nums[begin], nums[i]);
            ++begin;
            ++i;
        }
        else if (nums[i] == 1) ++i;
        else if (nums[i] == 2)
        {
            std::swap(nums[end], nums[i]);
            --end;
        }
    }
}
#else
void sortColors(std::vector<int> &nums)
{
    int histogram[3] = {0, 0, 0};
    for (int n : nums)
        ++histogram[n];
    for (int i = 0, j = 0; i < 3; ++i)
        for (int k = 0; k < histogram[i]; ++k, ++j)
            nums[j] = i;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = nums;
        sortColors(result);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 0, 2, 1, 1, 0}, {0, 0, 1, 1, 2, 2}, trials);
    test({2, 0, 1}, {0, 1, 2}, trials);
    test({0}, {0}, trials);
    test({1}, {1}, trials);
    test({}, {}, trials);
    return 0;
}


