#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int minimumOperations(std::vector<int> nums)
{
    struct Data
    {
        std::unordered_map<int, int> count;
        int mx = 0;
        int second_max = 0;
        int max_freq = 0;
        int second_max_freq = 0;
    };
    Data ts[2];
    
    for (size_t i = 0; i < nums.size(); ++i)
    {
        Data &t = ts[i % 2];
        const int freq = ++t.count[nums[i]];
        if (freq > t.max_freq)
        {
            t.max_freq = freq;
            t.mx = nums[i];
        }
        else if (freq > t.second_max_freq)
        {
            t.second_max_freq = freq;
            t.second_max = nums[i];
        }
    }
    if (ts[0].mx == ts[1].mx)
        return static_cast<int>(nums.size())
             - std::max(ts[0].max_freq + ts[1].second_max_freq,
                        ts[1].max_freq + ts[0].second_max_freq);
    return static_cast<int>(nums.size()) - (ts[0].max_freq + ts[1].max_freq);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1, 3, 2, 4, 3}, 3, trials);
    test({1, 2, 2, 2, 2}, 2, trials);
    return 0;
}


