#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minimumMoves(std::vector<std::vector<int> > grid)
{
    enum class POS { HOR, VER };
    const int n = static_cast<int>(grid.size());
    int result = 0;
    std::queue<std::tuple<int, int, POS> > q{{{0, 0, POS::HOR}}};
    std::vector<std::vector<std::vector<bool> > > seen(n,
            std::vector<std::vector<bool> >(n, std::vector<bool>(2)));
    seen[0][0][static_cast<int>(POS::HOR)] = true;
    
    while (!q.empty())
    {
        for (int sz = static_cast<int>(q.size()); sz > 0; --sz)
        {
            const auto [x, y, pos] = q.front();
            q.pop();
            if ((x == n - 1) && (y == n - 2) && (pos == POS::HOR))
                return result;
            if (((pos == POS::HOR)?(y + 2 < n) && !grid[x][y + 2] // Can move right
                                  :(y + 1 < n) && !grid[x][y + 1] && !grid[x + 1][y + 1])
            &&  !seen[x][y + 1][static_cast<int>(pos)])
            {
                q.emplace(x, y + 1, pos);
                seen[x][y + 1][static_cast<int>(pos)] = true;
            }
            if (((pos == POS::VER)?(x + 2 < n) && !grid[x + 2][y] // Can move down
                                  :(x + 1 < n) && !grid[x + 1][y] && !grid[x + 1][y + 1])
            &&  !seen[x + 1][y][static_cast<int>(pos)])
            {
                q.emplace(x + 1, y, pos);
                seen[x + 1][y][static_cast<int>(pos)] = true;
            }
            const POS newPos = (pos == POS::HOR)?POS::VER:POS::HOR;
            if ((((pos == POS::HOR) && (x + 1 < n) && !grid[x + 1][y]) // Can rotate
            ||   ((pos == POS::VER) && (y + 1 < n) && !grid[x][y + 1]))
            && !grid[x + 1][y + 1] && !seen[x][y][static_cast<int>(newPos)])
            {
                q.emplace(x, y, newPos);
                seen[x][y][static_cast<int>(newPos)] = true;
            }
        }
        ++result;
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumMoves(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0, 0, 0, 0, 1},
          {1, 1, 0, 0, 1, 0},
          {0, 0, 0, 0, 1, 1},
          {0, 0, 1, 0, 1, 0},
          {0, 1, 1, 0, 0, 0},
          {0, 1, 1, 0, 0, 0}}, 11, trials);
    test({{0, 0, 1, 1, 1, 1},
          {0, 0, 0, 0, 1, 1},
          {1, 1, 0, 0, 0, 1},
          {1, 1, 1, 0, 0, 1},
          {1, 1, 1, 0, 0, 1},
          {1, 1, 1, 0, 0, 0}}, 9, trials);
    return 0;
}


