#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minSwaps(std::string s)
{
    int unmatched = 0;
    
    for (char c : s)
    {
        if (c == '[') ++unmatched;
        else if (unmatched > 0) --unmatched;
    }
    return (unmatched + 1) / 2;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSwaps(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("][][", 1, trials);
    test("]]][[[", 2, trials);
    test("[]", 0, trials);
    return 0;
}


