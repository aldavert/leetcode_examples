#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int buttonWithLongestTime(std::vector<std::vector<int> > events)
{
    int previous_time = 0, largest_time = 0;
    size_t selected = 0;
    for (size_t i = 0; i < events.size(); ++i)
    {
        int time_increase = events[i][1] - previous_time;
        if ((time_increase > largest_time)
        ||  ((time_increase == largest_time) && (events[i][0] < events[selected][0])))
        {
            largest_time = time_increase;
            selected = i;
        }
        previous_time = events[i][1];
    }
    return events[selected][0];
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > events, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = buttonWithLongestTime(events);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 5}, {3, 9}, {1, 15}}, 1, trials);
    test({{10, 5}, {1, 7}}, 10, trials);
    test({{8, 4}, {7, 6}, {19, 9}, {8, 14}, {13, 15}, {2, 16}, {2, 18}}, 8, trials);
    test({{9, 4}, {19, 5}, {2, 8}, {3, 11}, {2, 15}}, 2, trials);
    return 0;
}


