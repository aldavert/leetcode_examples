#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<double> convertTemperature(double celsius)
{
    return { celsius + 273.15, celsius * 1.8 + 32 };
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<double> &left, const std::vector<double> &right)
{
    if (left.size() != right.size()) return false;
    for (size_t i = 0, n = left.size(); i < n; ++i)
        if (std::abs(left[i] - right[i]) >= 1e-5)
            return false;
    return true;
}

void test(double celsius, std::vector<double> solution, unsigned int trials = 1)
{
    std::vector<double> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = convertTemperature(celsius);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(36.50, {309.65000, 97.70000}, trials);
    test(122.11, {395.26000, 251.79800}, trials);
    return 0;
}


