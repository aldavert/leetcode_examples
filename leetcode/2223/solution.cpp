#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
long long sumScores(std::string s)
{
    const int n = static_cast<int>(s.size());
    int location_prefix[100'000] = {}, location_count[100'000] = {};
    for (int i = 1; i < n; ++i)
    {
        int index = location_prefix[i - 1];
        while (index && (s[index] != s[i]))
            index = location_prefix[index - 1];
        if (s[index] == s[i])
            location_prefix[i] = index + 1;
    }
    for (int i = 1; i < n; ++i)
        if (location_prefix[i])
            location_count[i] = 1 + location_count[location_prefix[i]-1];
    long long result = n;
    for (int i = 0; i < n; ++i) result += location_count[i];
    return result;
}
#else
long long sumScores(std::string s)
{
    const int n = static_cast<int>(s.size());
    std::vector<int> score(n);
    int left = 0, right = 0;
    
    for (int i = 1; i < n; ++i)
    {
        if (i <= right) score[i] = std::min(right - i + 1, score[i - left]);
        while ((i + score[i] < n) && (s[score[i]] == s[i + score[i]])) ++score[i];
        if (i + score[i] - 1 > right)
        {
            left = i;
            right = i + score[i] - 1;
        }
    }
    return std::accumulate(score.begin(), score.end(), 0LL) + n;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumScores(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("babab", 9, trials);
    test("azbazbzaz", 14, trials);
    return 0;
}


