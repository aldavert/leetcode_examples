#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string maximumOddBinaryNumber(std::string s)
{
    const size_t n = s.size();
    size_t number_of_ones = 0;
    for (size_t i = 0; i < n; ++i)
        number_of_ones += s[i] == '1';
    if (number_of_ones == 0) return s;
    for (size_t i = 0; i < n; ++i)
        s[i] = static_cast<char>('0' + ((i < number_of_ones - 1) || (i == n - 1)));
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumOddBinaryNumber(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("010", "001", trials);
    test("0101", "1001", trials);
    return 0;
}


