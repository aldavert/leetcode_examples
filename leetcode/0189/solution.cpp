#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
void rotate(std::vector<int> &nums, int k)
{
    const int n = static_cast<int>(nums.size());
    k = k % n;
    if (k == 0) return;
    for (int i = 0, j = n - k - 1; i <= j; ++i, --j)
        std::swap(nums[i], nums[j]);
    for (int i = n - k, j = n - 1; i <= j; ++i, --j)
        std::swap(nums[i], nums[j]);
    for (int i = 0, j = n - 1; i <= j; ++i, --j)
        std::swap(nums[i], nums[j]);
}
#else
void rotate(std::vector<int> &nums, int k)
{
    const int n = static_cast<int>(nums.size());
    k = k % n;
    if (k == 0) return;
    std::vector<int> copy(nums);
    int i = 0;
    for (int j = k; j < n; ++i, ++j)
        nums[j] = copy[i];
    for (int j = 0; i < n; ++i, ++j)
        nums[j] = copy[i];
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = nums;
        rotate(result, k);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6, 7}, 3, {5, 6, 7, 1, 2, 3, 4}, trials);
    test({ 1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15}, 3,
         {13, 14, 15,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12}, trials);
    test({-1, -100, 3, 99}, 2, {3, 99, -1, -100}, trials);
    return 0;
}


