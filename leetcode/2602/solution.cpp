#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<long long> minOperations(std::vector<int>& nums, std::vector<int>& queries)
{
    const int n = static_cast<int>(nums.size());
    std::vector<long long> result, prefix{0};
    std::sort(nums.begin(), nums.end());
    for (int i = 0; i < n; ++i)
        prefix.push_back(prefix.back() + nums[i]);
    for (const long query : queries)
    {
        int i = static_cast<int>(std::upper_bound(nums.begin(), nums.end(), query)
                               - nums.begin());
        long inc = query * i - prefix[i];
        long dec = prefix[n] - prefix[i] - query * (n - i);
        result.push_back(inc + dec);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> queries,
          std::vector<long long> solution,
          unsigned int trials = 1)
{
    std::vector<long long> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1, 6, 8}, {1, 5}, {14, 10}, trials);
    test({2, 9, 6, 3}, {10}, {20}, trials);
    return 0;
}


