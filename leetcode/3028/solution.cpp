#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int returnToBoundaryCount(std::vector<int> nums)
{
    int result = 0;
    for (int total = 0; int number : nums)
        result += (total += number) == 0;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = returnToBoundaryCount(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, -5}, 1, trials);
    test({3, 2, -3, -4}, 0, trials);
    return 0;
}


