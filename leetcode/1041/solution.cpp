#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isRobotBounded(std::string instructions)
{
    constexpr int dir[] = {0, 1, 0, -1, 0 };
    int x = 0, y = 0;
    int o = 0;
    for (char i : instructions)
    {
        if (i == 'G')
        {
            x += dir[o];
            y += dir[o + 1];
        }
        else if (i == 'L')
        {
            --o;
            if (o == -1) o = 3;
        }
        else if (i == 'R')
        {
            ++o;
            if (o == 4) o = 0;
        }
    }
    return (o != 0) || ((x == 0) && (y == 0));
}

// ############################################################################
// ############################################################################

void test(std::string instructions, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isRobotBounded(instructions);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("GGLLGG", true, trials);
    test("GG", false, trials);
    test("GL", true, trials);
    return 0;
}


