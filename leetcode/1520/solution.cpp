#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> maxNumOfSubstrings(std::string s)
{
    const int n = static_cast<int>(s.size());
    int leftmost[26] = {}, rightmost[26] = {};
    std::vector<std::string> result;
    auto getNewRight = [&](int i)
    {
        int right = rightmost[s[i] - 'a'];
        for (int j = i; j <= right; ++j)
        {
            if (leftmost[s[j] - 'a'] < i)
                return -1;
            right = std::max(right, rightmost[s[j] - 'a']);
        }
        return right;
    };
    
    for (int i = 0; i < 26; ++i) leftmost[i] = n, rightmost[i] = -1;
    for (int i = 0; i < n; ++i)
    {
        leftmost[s[i] - 'a'] = std::min(leftmost[s[i] - 'a'], i);
        rightmost[s[i] - 'a'] = i;
    }
    int right = -1;
    for (int i = 0; i < n; ++i)
    {
        if (i == leftmost[s[i] - 'a'])
        {
            const int new_right = getNewRight(i);
            if (new_right == -1)
                continue;
            if ((i <= right) && !result.empty())
                result.back() = s.substr(i, new_right - i + 1);
            else
                result.push_back(s.substr(i, new_right - i + 1));
            right = new_right;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<std::string> copy_left = left, copy_right = right;
    std::sort(copy_left.begin(), copy_left.end());
    std::sort(copy_right.begin(), copy_right.end());
    for (size_t i = 0, n = copy_left.size(); i < n; ++i)
        if (copy_left[i] != copy_right[i])
            return false;
    return true;
}

void test(std::string s, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxNumOfSubstrings(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("adefaddaccc", {"e", "f", "ccc"}, trials);
    test("abbaccd", {"d", "bb", "cc"}, trials);
    return 0;
}


