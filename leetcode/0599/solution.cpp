#include "../common/common.hpp"
#include <unordered_map>
#include <limits>

// ############################################################################
// ############################################################################

std::vector<std::string> findRestaurant(std::vector<std::string> list1,
                                        std::vector<std::string> list2)
{
    std::vector<std::string> result;
    size_t distance = std::numeric_limits<size_t>::max();
    std::unordered_map<std::string, size_t> positions;
    for (size_t i = 0; i < list1.size(); ++i)
        positions[list1[i]] = i;
    for (size_t i = 0; i < list2.size(); ++i)
    {
        if (auto search = positions.find(list2[i]); search != positions.end())
        {
            size_t d = i + search->second;
            if (d < distance)
            {
                result = {list2[i]};
                distance = d;
            }
            else if (d == distance)
                result.push_back(list2[i]);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

template <typename T>
bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_map<std::string, size_t> histogram;
    for (const auto &s : left)
        ++histogram[s];
    for (const auto &s : right)
    {
        if (auto search = histogram.find(s); search != histogram.end())
        {
            --search->second;
            if (search->second == 0)
                histogram.erase(search);
        }
        else return false;
    }
    return histogram.empty();
}

void test(std::vector<std::string> list1,
          std::vector<std::string> list2,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findRestaurant(list1, list2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"Shogun", "Tapioca Express", "Burger King", "KFC"},
         {"Piatti", "The Grill at Torrey Pines", "Hungry Hunter Steakhouse", "Shogun"},
         {"Shogun"}, trials);
    test({"Shogun", "Tapioca Express", "Burger King", "KFC"},
         {"KFC", "Shogun", "Burger King"},
         {"Shogun"}, trials);
    return 0;
}


