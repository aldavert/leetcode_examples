#include "../common/common.hpp"
#include <unordered_map>
#include <set>

// ############################################################################
// ############################################################################

#if 1
std::vector<std::vector<std::string> >
accountsMerge(std::vector<std::vector<std::string> > accounts)
{
    // Convert everything to integers.
    std::vector<std::string_view> id2name, id2mail;
    std::unordered_map<std::string_view, int> name2id, mail2id;
    id2name.reserve(1'001);
    id2mail.reserve(10'001);
    std::array<int, 10'001> names, emails_chains;
    for (int i = 0; i < 10'001; ++i)
    {
        names[i] = -1;
        emails_chains[i] = i;
    }
    
    auto find = [&](int email) -> int
    {
        while (email != emails_chains[email])
            email = emails_chains[email];
        return email;
    };
    auto join = [&](int email1, int email2) -> void
    {
        int e1 = find(email1);
        int e2 = find(email2);
        if (e1 != e2)  emails_chains[e1] = email2;
    };
    auto mailId = [&](std::string_view mail) -> int
    {
        int mail_id;
        if (auto it = mail2id.find(mail); it == mail2id.end())
        {
            mail_id = static_cast<int>(id2mail.size());
            mail2id[mail] = mail_id;
            id2mail.push_back(mail);
        }
        else mail_id = it->second;
        return mail_id;
    };
    
    std::vector<std::vector<int> > accounts_mails(accounts.size());
    int idx = 0;
    for (const auto &a : accounts)
    {
        auto element = a.begin();
        int name_id;
        accounts_mails[idx].reserve(a.size() - 1);
        
        // Process name .............................................
        std::string_view current(*element);
        if (auto it = name2id.find(current); it == name2id.end())
        {
            name_id = static_cast<int>(id2name.size());
            name2id[current] = name_id;
            id2name.push_back(current);
        }
        else name_id = it->second;
        ++element;
        
        // Process e-mails ..........................................
        int first_mail = mailId(*element);
        accounts_mails[idx].push_back(first_mail);
        if (names[first_mail] == -1)
            names[first_mail] = name_id;
        join(first_mail, first_mail);
        ++element;
        for (auto end = a.end(); element != end; ++element)
        {
            int mail_id = mailId(*element);
            accounts_mails[idx].push_back(mail_id);
            if (names[mail_id] == -1)
                names[mail_id] = name_id;
            join(first_mail, mail_id);
        }
        ++idx;
    }
    
    std::unordered_map<int, std::unordered_set<int> > group;
    for (const auto &mails : accounts_mails)
        group[find(mails[0])].insert(mails.begin(), mails.end());

    std::vector<std::vector<std::string> > result;
    for (const auto &pair : group)
    {
        std::vector<std::string> emails;
        emails.reserve(pair.second.size() + 1);
        emails.push_back(std::string(id2name[names[pair.first]]));
        for (const auto &e : pair.second)
            emails.push_back(std::string(id2mail[e]));
        std::sort(emails.begin() + 1, emails.end());
        result.push_back(std::move(emails));
    }
    return result;
}    
#else
std::vector<std::vector<std::string> >
accountsMerge(std::vector<std::vector<std::string> > accounts)
{
    const int n = static_cast<int>(accounts.size());
    std::unordered_map<std::string, std::string> emails_chains;
    std::unordered_map<std::string, std::string> names;

    auto find = [&](std::string email) -> std::string
    {
        while (email != emails_chains[email])
            email = emails_chains[email];
        return email;
    };
    auto join = [&](const std::string &email1, const std::string &email2) -> bool
    {
        std::string e1 = find(email1);
        std::string e2 = find(email2);
        if (e1 != e2)  emails_chains[e1] = email2;
        return e1 == e2;
    };

    for (int i = 0; i < n; ++i)
    {
        auto &account = accounts[i];
        const int m = static_cast<int>(account.size());
        auto &name = account[0];
        for (int j = 1; j < m; ++j)
        {
            auto &email = account[j];
            if (names.find(email) == names.end())
            {
                emails_chains[email] = email;
                names[email] = name;
            }
            join(account[1], email);
        }
    }

    std::unordered_map<std::string, std::set<std::string> > group;
    for (const auto &acc : accounts)
        group[find(acc[1])].insert(acc.begin() + 1, acc.end());

    std::vector<std::vector<std::string> > result;
    for (const auto &pair : group)
    {
        std::vector<std::string> emails;
        emails.reserve(pair.second.size() + 1);
        emails.push_back(names[pair.first]);
        for (const auto &e : pair.second)
            emails.push_back(e);
        result.push_back(std::move(emails));
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<std::string> > &left,
                const std::vector<std::vector<std::string> > &right)
{
    if (left.size() != right.size()) return false;
    std::set<std::vector<std::string> > unique;
    for (const auto &l : left)
        unique.insert(l);
    for (const auto &r : right)
    {
        if (auto it = unique.find(r); it != unique.end())
            unique.erase(it);
        else return false;
    }
    return unique.empty();
}

void test(std::vector<std::vector<std::string> > accounts,
          std::vector<std::vector<std::string> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<std::string> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = accountsMerge(accounts);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{"John", "johnsmith@mail.com", "john_newyork@mail.com"},
          {"John", "johnsmith@mail.com", "john00@mail.com"},
          {"Mary", "mary@mail.com"}, {"John", "johnnybravo@mail.com"}},
         {{"John", "john00@mail.com", "john_newyork@mail.com", "johnsmith@mail.com"},
          {"Mary", "mary@mail.com"}, {"John", "johnnybravo@mail.com"}}, trials);
    test({{"Gabe", "Gabe0@m.co", "Gabe3@m.co", "Gabe1@m.co"},
          {"Kevin", "Kevin3@m.co", "Kevin5@m.co", "Kevin0@m.co"},
          {"Ethan", "Ethan5@m.co", "Ethan4@m.co", "Ethan0@m.co"},
          {"Hanzo", "Hanzo3@m.co", "Hanzo1@m.co", "Hanzo0@m.co"},
          {"Fern", "Fern5@m.co", "Fern1@m.co", "Fern0@m.co"}},
         {{"Ethan", "Ethan0@m.co", "Ethan4@m.co", "Ethan5@m.co"},
          {"Gabe", "Gabe0@m.co", "Gabe1@m.co", "Gabe3@m.co"},
          {"Hanzo", "Hanzo0@m.co", "Hanzo1@m.co", "Hanzo3@m.co"},
          {"Kevin", "Kevin0@m.co", "Kevin3@m.co", "Kevin5@m.co"},
          {"Fern", "Fern0@m.co", "Fern1@m.co", "Fern5@m.co"}}, trials);
    return 0;
}


