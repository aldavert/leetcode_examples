#include "../common/common.hpp"
#include "values.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int maxPerformance(int n, std::vector<int> speed, std::vector<int> efficiency, int k)
{
    struct Info
    {
        int speed;
        int efficiency;
        bool operator<(const Info &other) { return efficiency < other.efficiency; }
    };
    std::vector<Info> values(n);
    for (int i = 0; i < n; ++i)
        values[i] = {speed[i], efficiency[i]};
    std::sort(values.begin(), values.end());
    
    std::priority_queue<int, std::vector<int>, std::greater<int> > heap;
    long result = 0, sum = 0;
    for (int i = n - 1; i >= 0; --i)
    {
        sum += values[i].speed;
        heap.push(values[i].speed);
        if (static_cast<int>(heap.size()) > k)
        {
            sum -= heap.top();
            heap.pop();
        }
        result = std::max(result, sum * values[i].efficiency);
    }
    return static_cast<int>(result % 1'000'000'007);
}
#else
int maxPerformance(int n, std::vector<int> speed, std::vector<int> efficiency, int k)
{
    struct Info
    {
        int speed;
        int efficiency;
        bool operator<(const Info &other) { return efficiency < other.efficiency; }
    };
    auto sortEfficiency = [](const Info &first, const Info &second)
                          { return first.efficiency < second.efficiency; };
    auto sortSpeed = [](const Info &first, const Info &second)
                     { return first.speed < second.speed; };
    std::vector<Info> sort_efficiency(n), sort_speed(n);
    for (int i = 0; i < n; ++i)
        sort_speed[i] = sort_efficiency[i] = {speed[i], efficiency[i]};
    std::sort(sort_efficiency.begin(), sort_efficiency.end(), sortEfficiency );
    std::sort(sort_speed.begin(), sort_speed.end(), sortSpeed);
    
    long result = 0;
    for (int i = n - 1; i >= 0; --i)
    {
        const int sk = std::min(n - i, k);
        long current = 0;
        for (int j = n - 1, o = 0; (o < sk) && (j >= 0); --j)
        {
            if (sort_speed[j].efficiency >= sort_efficiency[i].efficiency)
            {
                current += sort_speed[j].speed;
                ++o;
            }
        }
        result = std::max(result, current * sort_efficiency[i].efficiency);
    }
    return static_cast<int>(result % 1'000'000'007);
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<int> speed,
          std::vector<int> efficiency,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxPerformance(n, speed, efficiency, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10000000;
    constexpr unsigned int trials = 100;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, {2, 10, 3, 1, 5, 8}, {5, 4, 3, 9, 7, 2}, 2, 60, trials);
    test(6, {2, 10, 3, 1, 5, 8}, {5, 4, 3, 9, 7, 2}, 3, 68, trials);
    test(6, {2, 10, 3, 1, 5, 8}, {5, 4, 3, 9, 7, 2}, 4, 72, trials);
    test(3, {2, 8, 2}, {2, 7, 1}, 2, 56, trials);
    test(825, testA::values_speed, testA::values_efficiency, 529, 904549190, trials);
    test(100000, testB::values_speed, testB::values_efficiency, 86484, 301574164, trials);
    
    return 0;
}


