#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string dayOfTheWeek(int day, int month, int year)
{
    const std::string day_name[] = {"Monday", "Tuesday", "Wednesday", "Thursday",
                                    "Friday", "Saturday", "Sunday"};
    constexpr int month_accum[] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273,
                                   304, 334};
    bool is_leap = (year % 4 == 0) && !((year % 100 == 0) && (year % 400 != 0));
    int day_of_the_year = month_accum[month - 1] + day + is_leap * (month > 2);
    --year;
    int day_of_first_day = (year / 4) * 366 + (year - year / 4) * 365;
    ++year;
    return day_name[(day_of_first_day + day_of_the_year + 5) % 7];
}

// ############################################################################
// ############################################################################

void test(int day,
          int month,
          int year,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = dayOfTheWeek(day, month, year);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(31,  8, 2019, "Saturday", trials);
    test(18,  7, 1999, "Sunday", trials);
    test(15,  8, 1993, "Sunday", trials);
    test( 7, 10, 2022, "Friday", trials);
    test( 1,  1, 2022, "Saturday", trials);
    test(29,  2, 2016, "Monday", trials);
    test( 1,  1, 2016, "Friday", trials);
    return 0;
}


