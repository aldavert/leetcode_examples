#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

std::vector<double> averageOfLevels(TreeNode * root)
{
    std::vector<double> result;
    std::queue<TreeNode *> q;
    if (root) q.push(root);
    q.push(nullptr);
    double average = 0.0;
    size_t n_nodes = 0;
    while (q.size() > 1)
    {
        TreeNode * current = q.front();
        q.pop();
        if (current == nullptr)
        {
            result.push_back(average / static_cast<double>(n_nodes));
            n_nodes = 0;
            average = 0.0;
            q.push(nullptr);
        }
        else
        {
            average += static_cast<double>(current->val);
            ++n_nodes;
            if (current->left) q.push(current->left);
            if (current->right) q.push(current->right);
        }
    }
    result.push_back(average / static_cast<double>(n_nodes));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<double> solution, unsigned int trials = 1)
{
    std::vector<double> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = averageOfLevels(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 9, 20, null, null, 15, 7}, {3.00000, 14.50000, 11.00000}, trials);
    test({3, 9, 20, 15, 7}, {3.00000, 14.50000, 11.00000}, trials);
    return 0;
}


