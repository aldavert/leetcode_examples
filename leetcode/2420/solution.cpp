#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> goodIndices(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> result, dec(n, 1), inc(n, 1);
    
    for (int i = 1; i < n; ++i)
        if (nums[i - 1] >= nums[i])
            dec[i] = dec[i - 1] + 1;
    for (int i = n - 2; i >= 0; --i)
        if (nums[i] <= nums[i + 1])
            inc[i] = inc[i + 1] + 1;
    for (int i = k; i < n - k; ++i)
        if ((dec[i - 1] >= k) && (inc[i + 1] >= k))
            result.push_back(i);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = goodIndices(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 1, 1, 3, 4, 1}, 2, {2, 3}, trials);
    test({2, 1, 1, 2}, 2, {}, trials);
    return 0;
}


