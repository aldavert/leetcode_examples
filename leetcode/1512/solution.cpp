#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numIdenticalPairs(std::vector<int> nums)
{
    int histogram[101] = {}, result = 0;
    for (int value : nums)
    {
        result += histogram[value];
        ++histogram[value];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numIdenticalPairs(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 1, 1, 3}, 4, trials);
    test({1, 1, 1, 1}, 6, trials);
    test({1, 2, 3}, 0, trials);
    return 0;
}


