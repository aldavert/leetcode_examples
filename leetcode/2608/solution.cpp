#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int findShortestCycle(int n, std::vector<std::vector<int> > edges)
{
    constexpr int INF = 1001;
    int distance[INF];
    std::vector<std::vector<int> > graph(n);
    int result = INF;
    auto dfs = [&](auto &&self, int current, int previous, int depth) -> void
    {
        distance[current] = depth;
        for(auto sub : graph[current])
        {
            if (sub != previous)
            {
                if (distance[sub] > depth + 1) self(self, sub, current, depth+1);
                else if (distance[sub] < depth)
                    result = std::min(result, depth - distance[sub] + 1);
            }
        }
    };
    
    for (int i = 0; i < n; ++i) distance[i] = INF;
    for (const auto &edge : edges)
    {
        graph[edge[0]].push_back(edge[1]);
        graph[edge[1]].push_back(edge[0]);
    }
    for (int i = 0; i < n; ++i)
        if (distance[i] == INF)
            dfs(dfs, i, -1 ,0);
    
    return (result == INF)?-1:result;
}
#else
int findShortestCycle(int n, std::vector<std::vector<int> > edges)
{
    constexpr int INF = 1 << 30;
    std::vector<std::vector<int> > graph(n);
    auto bfs = [&](int i) -> int
    {
        std::vector<int> distance(graph.size(), INF);
        std::queue<int> q{{i}};
        distance[i] = 0;
        int result = INF;
        while (!q.empty())
        {
            int u = q.front();
            q.pop();
            for (int v : graph[u])
            {
                if (distance[v] == INF)
                {
                    distance[v] = distance[u] + 1;
                    q.push(v);
                }
                else if (distance[v] + 1 != distance[u])
                    result = std::min(result, distance[v] + distance[u] + 1);
            }
        }
        return result;
    };
    
    int result = INF;
    for (const auto &edge : edges)
    {
        graph[edge[0]].push_back(edge[1]);
        graph[edge[1]].push_back(edge[0]);
    }
    for (int i = 0; i < n; ++i)
        result = std::min(result, bfs(i));
    return (result == INF)?-1:result;
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findShortestCycle(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, {{0, 1}, {1, 2}, {2, 0}, {3, 4}, {4, 5}, {5, 6}, {6, 3}}, 3, trials);
    test(4, {{0, 1}, {0, 2}}, -1, trials);
    test(6, {{4, 2}, {5, 1}, {5, 0}, {0, 3}, {5, 2}, {1, 4}, {1, 3}, {3, 4}}, 3, trials);
    return 0;
}


