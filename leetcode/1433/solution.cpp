#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkIfCanBreak(std::string s1, std::string s2)
{
    int count[26] = {};
    for (char c : s1) ++count[c - 'a'];
    for (char c : s2) --count[c - 'a'];
    for (int i = 1; i < 26; ++i) count[i] += count[i - 1];
    return std::all_of(&count[0], &count[26], [](int c) { return c <= 0; })
        || std::all_of(&count[0], &count[26], [](int c) { return c >= 0; });
}

// ############################################################################
// ############################################################################

void test(std::string s1, std::string s2, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkIfCanBreak(s1, s2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", "xya", true, trials);
    test("abe", "acd", false, trials);
    test("leetcodee", "interview", true, trials);
    return 0;
}


