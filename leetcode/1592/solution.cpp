#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string reorderSpaces(std::string text)
{
    size_t spaces = 0;
    std::vector<std::string> words;
    for (char previous = ' '; char c : text)
    {
        if (c == ' ') ++spaces;
        else
        {
            if (previous == ' ') words.push_back("");
            words.back() += c;
        }
        previous = c;
    }
    if (words.size() == 1)
    {
        text = words.front();
        for (size_t i = 0; i < spaces; ++i) text += " ";
    }
    else
    {
        size_t separation = spaces / (words.size() - 1);
        size_t remaining = spaces % (words.size() - 1);
        text = words[0];
        for (size_t i = 1, n = words.size(); i < n; ++i)
        {
            for (size_t j = 0; j < separation; ++j)
                text += " ";
            text += words[i];
        }
        for (size_t i = 0; i < remaining; ++i) text += " ";
    }
    return text;
}

// ############################################################################
// ############################################################################

void test(std::string text, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = reorderSpaces(text);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("  this   is  a sentence ", "this   is   a   sentence", trials);
    test(" practice   makes   perfect", "practice   makes   perfect ", trials);
    return 0;
}


