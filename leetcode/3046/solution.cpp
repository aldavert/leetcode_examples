#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isPossibleToSplit(std::vector<int> nums)
{
    int histogram[101] = {};
    for (int value : nums)
        if (++histogram[value] > 2) return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPossibleToSplit(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2, 2, 3, 4}, true, trials);
    test({1, 1, 1, 1}, false, trials);
    return 0;
}


