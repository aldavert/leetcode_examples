#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minChanges(int n, int k)
{
    return ((n & k) == k)?std::popcount(static_cast<unsigned int>(n ^ k)):-1;
}

// ############################################################################
// ############################################################################

void test(int n, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minChanges(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(13,  4,  2, trials);
    test(21, 21,  0, trials);
    test(14, 13, -1, trials);
    return 0;
}


