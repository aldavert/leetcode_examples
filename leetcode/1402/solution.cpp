#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxSatisfaction(std::vector<int> satisfaction)
{
    const int n = static_cast<int>(satisfaction.size());
    std::sort(satisfaction.begin(), satisfaction.end());
    std::vector<int> accum(n);
    for (int i = n - 1, previous = 0; i >= 0; --i)
        previous = accum[i] = previous + satisfaction[i];
    int result = 0;
    for (int i = 0; i < n; ++i)
        result += (i + 1) * satisfaction[i];
    for (int i = 0; (i < n) && (result < result - accum[i]); ++i)
        result -= accum[i];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> satisfaction, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSatisfaction(satisfaction);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, -8, 0, 5, -9}, 14, trials);
    test({4, 3, 2}, 20, trials);
    test({-1, -4, -5}, 0, trials);
    return 0;
}


