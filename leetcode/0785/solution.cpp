#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

bool isBipartite(std::vector<std::vector<int> > graph)
{
    const int n = static_cast<int>(graph.size());
    char colors[101] = {};
    for (int i = 0; i < n; ++i)
    {
        if (colors[i] != 0) continue;
        colors[i] = 1;
        std::queue<int> q{{i}};
        while (!q.empty())
        {
            const int u = q.front();
            q.pop();
            for (const int v : graph[u])
            {
                if (colors[v] == colors[u]) return false;
                if (colors[v] == 0)
                {
                    colors[v] = (colors[u] == 1)?-1:1;
                    q.push(v);
                }
            }
        }
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > graph, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isBipartite(graph);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {0, 2}, {0, 1, 3}, {0, 2}}, false, trials);
    test({{1, 3}, {0, 2}, {1, 3}, {0, 2}}, true, trials);
    return 0;
}


