#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<long long> findPrefixScore(std::vector<int> nums)
{
    std::vector<long long> result;
    long prefix = 0;
    for (int max_num = 0; int num : nums)
    {
        max_num = std::max(max_num, num);
        prefix += num + max_num;
        result.push_back(prefix);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<long long> solution,
          unsigned int trials = 1)
{
    std::vector<long long> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findPrefixScore(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 7, 5, 10}, {4, 10, 24, 36, 56}, trials);
    test({1, 1, 2, 4, 8, 16}, {2, 4, 8, 16, 32, 64}, trials);
    return 0;
}


