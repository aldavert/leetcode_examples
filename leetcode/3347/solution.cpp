#include "../common/common.hpp"
#include <unordered_map>
#include <map>

// ############################################################################
// ############################################################################

#if 1
int maxFrequency(std::vector<int> nums, int k, int numOperations)
{
    const int n = static_cast<int>(nums.size());
    int a = 0, b = 0, c = 0, d = 0;
    long kl = k;
    std::sort(nums.begin(), nums.end());
    int result = 0;
    while (b < n)
    {
        long thr = 2 * kl + static_cast<long>(nums[a]);
        while ((b < n) && (static_cast<long>(nums[b]) <= thr))
            ++b;
        result = std::max(result, std::min(b - a, numOperations));
        ++a;
    }
    a = b = 0;
    while (b < n)
    {
        c = b;
        while ((c < n) && (nums[c] == nums[b]))
            ++c;
        while (nums[a] < std::max(1, nums[b] - k))
            ++a;
        long thr = static_cast<long>(nums[b]) + kl;
        while ((d < n) && (static_cast<long>(nums[d]) <= thr))
            ++d;
        result = std::max(result, c - b + std::min(numOperations, d - c + b - a));
        b = c;
    }
    return result;
}
#else
int maxFrequency(std::vector<int> nums, int k, int numOperations)
{
    std::unordered_map<int, int> count;
    std::map<int, int> line;
    std::set<int> candidates;
    int result = 1;
    
    for (const int num : nums)
    {
        ++count[num];
        ++line[num - k];
        --line[num + k + 1];
        candidates.insert(num);
        candidates.insert(num - k);
        candidates.insert(num + k + 1);
    }
    for (int adjustable = 0; const int num : candidates)
    {
        adjustable += line.contains(num) ? line[num] : 0;
        const int count_num = count.contains(num) ? count[num] : 0;
        const int adjusted = adjustable - count_num;
        result = std::max(result, count_num + std::min(numOperations, adjusted));
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          int numOperations,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxFrequency(nums, k, numOperations);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 5}, 1, 2, 2, trials);
    test({5, 11, 20, 20}, 5, 1, 2, trials);
    return 0;
}


