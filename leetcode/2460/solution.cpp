#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> applyOperations(std::vector<int> nums)
{
    std::vector<int> result(nums.size());
    for (size_t i = 0, n = nums.size(), j = 0; i < n; ++i)
    {
        if (nums[i] == 0) continue;
        else if ((i + 1 < n) && (nums[i] == nums[i + 1]))
            result[j++] = nums[i++] * 2;
        else result[j++] = nums[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = applyOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 1, 1, 0}, {1, 4, 2, 0, 0, 0}, trials);
    test({1, 2, 2, 1, 1, 3}, {1, 4, 2, 3, 0, 0}, trials);
    test({0, 1}, {1, 0}, trials);
    test({1, 1, 2}, {2, 2, 0}, trials);
    return 0;
}


