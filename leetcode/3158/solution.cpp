#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int duplicateNumbersXOR(std::vector<int> nums)
{
    constexpr int MAX = 50;
    int count[MAX + 1] = {};
    int result = 0;
    for (int num : nums) ++count[num];
    for (int num = 1; num <= MAX; ++num)
        if (count[num] == 2)
            result ^= num;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = duplicateNumbersXOR(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1, 3}, 1, trials);
    test({1, 2, 3}, 0, trials);
    test({1, 2, 2, 1}, 3, trials);
    return 0;
}


