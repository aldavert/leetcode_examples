# Delete Node in a BST

Given a root node reference of a BST and a key, delete the node with the given key in the BST. Return the root node reference (possibly updated) of the BST.

Basically, the deletion can be divided into two stages:
1. Search for a node to remove.
2. If the node is found, delete the node.
 
#### Example 1:
> ```mermaid
> flowchart LR;
> subgraph SA [ ]
> direction TB;
> A1((5))-->B1((3))
> A1-->C1((6))
> B1-->D1((2))
> B1-->E1((4))
> C1---EMPTY1(( ))
> C1-->F1((7))
> end
> subgraph SB [ ]
> direction TB;
> A2((5))-->B2((4))
> A2-->C2((6))
> B2-->D2((2))
> B2---EMPTY2(( ))
> C2---EMPTY3(( ))
> C2-->F2((7))
> end
> SA-->SB
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef selected fill:#AAF,stroke:#337,stroke-width:2px;
> class EMPTY1,EMPTY2,EMPTY3,SA,SB empty;
> class B1 selected;
> linkStyle 4,9,10 stroke-width:0px;
> ```
> *Input:* `root = [5, 3, 6, 2, 4, null, 7], key = 3`  
> *Output:* `[5, 4, 6, 2, null, null, 7]`  
> *Explanation:* Given key to delete is `3`. So we find the node with value `3` and delete it. One valid answer is `[5, 4, 6, 2, null, null, 7]`, shown in the above BST. Please notice that another valid answer is `[5, 2, 6, null, 4, null, 7]` and it's also accepted.

#### Example 2:
> ```mermaid
> graph TD;
> A((5))-->B((2))
> A-->C((6))
> B---EMPTY1(( ))
> B-->D((4))
> C---EMPTY2(( ))
> C-->E((7))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY1,EMPTY2 empty;
> linkStyle 2,4 stroke-width:0px;
> ```
> *Input:* `root = [5, 3, 6, 2, 4, null, 7], key = 0`  
> *Output:* `[5, 3, 6, 2, 4, null, 7]`  
> *Explanation:* The tree does not contain a node with `value = 0`.

#### Example 3:
> *Input:* `root = [], key = 0`  
> *Output:* `[]`
 
#### Constraints:
- The number of nodes in the tree is in the range `[0, 10^4]`.
- `-10^5 <= Node.val <= 10^5`
- Each node has a unique value.
- `root` is a valid binary search tree.
- `-10^5 <= key <= 10^5`
 
**Follow up:** Could you solve it with time complexity `O(height of tree)`?

