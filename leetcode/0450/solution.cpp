#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * deleteNode(TreeNode* root, int key)
{
    auto search = [&](auto &&self, TreeNode * node, TreeNode * previous, bool to_left)
        -> std::tuple<TreeNode *, TreeNode *, bool>
    {
        if (node == nullptr) return {nullptr, previous, to_left};
        if (node->val == key) return {node, previous, to_left};
        if (node->val > key) return self(self, node->left, node, true);
        else return self(self, node->right, node, false);
    };
    auto minimum = [&](auto &&self, TreeNode * node) -> TreeNode *
    {
        if (node->left == nullptr) return node;
        return self(self, node->left);
    };
    
    auto [current, previous, to_left] = search(search, root, nullptr, false);
    if (current != nullptr)
    {
        if (current->right == nullptr)
        {
            if (previous == nullptr)
            {
                root = current->left;
                current->left = nullptr;
                delete current;
            }
            else
            {
                if (to_left) previous->left = current->left;
                else previous->right = current->left;
                current->left = nullptr;
                delete current;
            }
        }
        else
        {
            auto node_min = minimum(minimum, current->right);
            node_min->left = current->left;
            current->left = nullptr;
            if (previous == nullptr)
                root = current->right;
            else
            {
                if (to_left) previous->left = current->right;
                else previous->right = current->right;
            }
            current->right = nullptr;
            delete current;
        }
    }
    return root;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          int key,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        root = deleteNode(root, key);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 3, 6, 2, 4, null, 7}, 3, {5, 4, 6, 2, null, null, 7}, trials);
    test({5, 3, 6, 2, 4, null, 7}, 0, {5, 3, 6, 2, 4, null, 7}, trials);
    test({}, 0, {}, trials);
    return 0;
}


