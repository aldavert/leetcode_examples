#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int minimumIncrements(std::vector<int> nums, std::vector<int> target)
{
    std::sort(nums.begin(), nums.end());
    std::sort(target.rbegin(), target.rend());
    const int m = static_cast<int>(target.size());
    std::vector<int> status(m);
    int result = std::numeric_limits<int>::max();
    auto dfs = [&](auto &&self, int idx) -> int
    {
        if (idx >= m) return 0;
        else if (status[idx] == 1) return self(self, idx + 1);
        int need = std::numeric_limits<int>::max();
        for (int tgt = 0; tgt < nums.back(); )
        {
            tgt += target[idx];
            std::vector<int> old_status = status;
            for (int i = 0; i < m; ++i)
            {
                if (status[i] || (tgt % target[i] == 0))
                    status[i] = 1;
            }
            auto i = std::upper_bound(nums.begin(), nums.end(), tgt);
            if (i != nums.begin())
            {
                int num = *(i - 1);
                if (abs(num - tgt) < result)
                {
                    nums.erase(i - 1);
                    need = std::min(need, std::abs(num - tgt)
                                  + self(self, idx + 1));
                    nums.insert(i - 1, num);
                }
            }
            status = old_status;
        }
        if (need >= result) return std::numeric_limits<int>::max();
        if (idx == 0) result = need;
        return need;
    };
    dfs(dfs, 0);
    return result;      
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> target,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumIncrements(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, {4}, 1, trials);
    test({8, 4}, {10, 5}, 2, trials);
    test({7, 9, 10}, {7}, 0, trials);
    return 0;
}


