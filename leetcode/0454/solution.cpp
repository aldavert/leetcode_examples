#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int fourSumCount(std::vector<int> nums1,
                 std::vector<int> nums2,
                 std::vector<int> nums3,
                 std::vector<int> nums4)
{
    const int n = static_cast<int>(nums1.size());
    int result = 0;
    std::unordered_map<int, int> sums;
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            ++sums[nums1[i] + nums2[j]];
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            int s = nums3[i] + nums4[j];
            if (auto it = sums.find(-s); it != sums.end())
                result += it->second;
        }
    }
    return result;
}
#else
int fourSumCount(std::vector<int> nums1,
                 std::vector<int> nums2,
                 std::vector<int> nums3,
                 std::vector<int> nums4)
{
    const int n = static_cast<int>(nums1.size());
    int result = 0;
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            for (int k = 0; k < n; ++k)
                for (int m = 0; m < n; ++m)
                    if (nums1[i] + nums2[j] + nums3[k] + nums4[m] == 0)
                        ++result;
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          std::vector<int> nums3,
          std::vector<int> nums4,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = fourSumCount(nums1, nums2, nums3, nums4);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2}, {-2, -1}, {-1, 2}, {0, 2}, 2, trials);
    test({-1, -1}, {-1, 1}, {-1, 1}, {1, -1}, 6, trials);
    test({0}, {0}, {0}, {0}, 1, trials);
    return 0;
}


