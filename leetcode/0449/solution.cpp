#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

class Codec {
public:
    std::string serialize(TreeNode * root)
    {
        if (root == nullptr) return "";
        std::string result;
        std::queue<TreeNode *> active_nodes;
        active_nodes.push(root);
        result = std::to_string(root->val);
        while (!active_nodes.empty())
        {
            TreeNode * current = active_nodes.front();
            active_nodes.pop();
            if (current->left != nullptr)
            {
                active_nodes.push(current->left);
                result += "," + std::to_string(current->left->val);
            }
            else result += ",#";
            if (current->right != nullptr)
            {
                active_nodes.push(current->right);
                result += "," + std::to_string(current->right->val);
            }
            else result += ",#";
        }
        size_t count_null = 0;
        while ((2 * (count_null + 1) < result.size())
           && (result.substr(result.size() - 2 * (count_null + 1), 2) == ",#"))
            ++count_null;
        return result.substr(0, result.size() - 2 * count_null);
    }
    TreeNode * deserialize(std::string data)
    {
        std::queue<TreeNode *> active_nodes;
        
        TreeNode * result = nullptr;
        bool left = true;
        for (size_t i = 0, n = data.size(); i < n;)
        {
            size_t next = i;
            for (; (next < n) && (data[next] != ','); ++next);
            std::string v = data.substr(i, next - i);
            i = next + 1;
            if (v != "#")
            {
                if (result == nullptr)
                {
                    result = new TreeNode(std::atoi(v.c_str()));
                    active_nodes.push(result);
                    active_nodes.push(result);
                }
                else
                {
                    TreeNode * current = new TreeNode(std::atoi(v.c_str()));
                    if (left) active_nodes.front()->left = current;
                    else active_nodes.front()->right = current;
                    active_nodes.pop();
                    active_nodes.push(current);
                    active_nodes.push(current);
                    left = !left;
                }
            }
            else
            {
                active_nodes.pop();
                left = !left;
            }
        }
        return result;
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        Codec serialize, de_serialize;
        TreeNode * root = vec2tree(tree);
        std::string text = serialize.serialize(root);
        delete root;
        root = de_serialize.deserialize(text);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3}, {2, 1, 3}, trials);
    test({}, {}, trials);
    test({0}, {0}, trials);
    return 0;
}


