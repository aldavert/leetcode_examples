#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

ListNode * deleteDuplicates(ListNode * head)
{
    if (head)
    {
        ListNode * current = head->next;
        ListNode * previous = head;
        head = nullptr;
        ListNode * last = nullptr;
        while (current)
        {
            if (current->val != previous->val)
            {
                if (previous->next == current)
                {
                    if (head) [[likely]]
                        last->next = previous;
                    else
                        head = previous;
                    last = previous;
                    last->next = nullptr;
                    previous = current;
                }
                else
                {
                    while (previous != current)
                    {
                        ListNode * aux = previous->next;
                        previous->next = nullptr;
                        delete previous;
                        previous = aux;
                    }
                }
            }
            current = current->next;
        }
        if (previous->next == nullptr)
        {
            if (head)
                last->next = previous;
            else head = previous;
        }
        else delete previous;
    }
    return head;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(input);
        ListNode * new_head = deleteDuplicates(head);
        result = list2vec(new_head);
        delete new_head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 3, 4, 4, 5}, {1, 2, 5}, trials);
    test({1, 2, 3, 3, 4, 5, 5, 7}, {1, 2, 4, 7}, trials);
    test({1, 2, 3, 3, 4, 5, 6, 6, 6, 6, 6, 7}, {1, 2, 4, 5, 7}, trials);
    test({1, 1, 1, 2, 3}, {2, 3}, trials);
    test({2, 3, 4, 4, 4, 4}, {2, 3}, trials);
    test({}, {}, trials);
    test({1, 1, 1}, {}, trials);
    test({1}, {1}, trials);
    test({1, 1, 2}, {2}, trials);
    return 0;
}


