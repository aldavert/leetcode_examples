#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > highestPeak(std::vector<std::vector<int> > isWater)
{
    constexpr int dirs[4][2] = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
    const int m = static_cast<int>(isWater.size()),
              n = static_cast<int>(isWater[0].size());
    std::vector<std::vector<int> > result(m, std::vector<int>(n, -1));
    std::queue<std::pair<int, int> > q;
    
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            if (isWater[i][j] == 1)
            {
                q.emplace(i, j);
                result[i][j] = 0;
            }
        }
    }
    while (!q.empty())
    {
        const auto [i, j] = q.front();
        q.pop();
        for (const auto& [dx, dy] : dirs)
        {
            const int x = i + dx;
            const int y = j + dy;
            if ((x < 0) || (x == m) || (y < 0) || (y == n))
                continue;
            if (result[x][y] != -1)
                continue;
            result[x][y] = result[i][j] + 1;
            q.emplace(x, y);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > isWater,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = highestPeak(isWater);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {0, 0}}, {{1, 0}, {2, 1}}, trials);
    test({{0, 0, 1}, {1, 0, 0}, {0, 0, 0}}, {{1, 1, 0}, {0, 1, 1}, {1, 2, 2}}, trials);
    return 0;
}


