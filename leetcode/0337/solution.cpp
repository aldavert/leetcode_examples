#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int rob(TreeNode * root)
{
    auto inner_rob = [&](auto &&self, TreeNode * node) -> std::pair<int, int>
    {
        if (node == nullptr) return {0, 0};
        auto [left_rob, left_pass] = self(self, node->left);
        auto [right_rob, right_pass] = self(self, node->right);
        int rob = left_pass + right_pass + node->val;
        int pass = std::max(left_pass, left_rob) + std::max(right_pass, right_rob);
        return {rob, pass};
    };
    auto [rob, pass] = inner_rob(inner_rob, root);
    return std::max(rob, pass);
}

// ############################################################################
// ############################################################################


void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = rob(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 3, null, 3, null, 1}, 7, trials);
    test({3, 4, 5, 1, 3, null, 1}, 9, trials);
    //       15·
    //    4      5
    //  2·  3·     1
    //               15·
    test({15, 4, 5, 2, 3, null, 1, null, null, null, null, null, 15}, 35, trials);
    return 0;
}


