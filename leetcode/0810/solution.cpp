#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

bool xorGame(std::vector<int> nums)
{
    return (std::accumulate(nums.begin(), nums.end(), 0, std::bit_xor<int>()) == 0)
        || (nums.size() % 2 == 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = xorGame(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2}, false, trials);
    test({0, 1}, true, trials);
    test({1, 2, 3}, true, trials);
    return 0;
}


