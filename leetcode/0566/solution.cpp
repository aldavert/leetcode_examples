#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > matrixReshape(std::vector<std::vector<int> > mat,
                                             int r,
                                             int c)
{
    const int n = static_cast<int>(mat.size()),
              m = static_cast<int>(mat[0].size());
    if (n * m != r * c) return mat;
    std::vector<std::vector<int> > result(r);
    result[0].resize(c);
    for (int i = 0, ir = 0, ic = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j, ++ic)
        {
            if (ic == c)
            {
                ic = 0;
                ++ir;
                result[ir].resize(c);
            }
            result[ir][ic] = mat[i][j];
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > mat,
          int r,
          int c,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = matrixReshape(mat, r, c);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {3, 4}}, 1, 4, {{1, 2, 3, 4}}, trials);
    test({{1, 2}, {3, 4}}, 2, 4, {{1, 2}, {3, 4}}, trials);
    return 0;
}


