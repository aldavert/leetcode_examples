#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxEqualFreq(std::vector<int> nums)
{
    int result = 0, max_frequency = 0, count[100'001] = {}, frequency[100'001] = {};
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        int &current_count = count[nums[i]];
        --frequency[current_count++];
        ++frequency[current_count];
        max_frequency = std::max(max_frequency, current_count);
        if ((max_frequency == 1)
        ||  (max_frequency * frequency[max_frequency] == i)
        ||  ((max_frequency - 1) * (frequency[max_frequency - 1] + 1) == i))
            result = i + 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxEqualFreq(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 2, 1, 1, 5, 3, 3, 5}, 7, trials);
    test({1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5}, 13, trials);
    return 0;
}


