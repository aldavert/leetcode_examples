#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int garbageCollection(std::vector<std::string> garbage, std::vector<int> travel)
{
    auto process = [](std::string house) -> std::tuple<int, int, int>
    {
        int metal = 0, paper = 0, glass = 0;
        for (char type : house)
        {
            metal += (type == 'M');
            paper += (type == 'P');
            glass += (type == 'G');
        }
        return {metal, paper, glass};
    };
    bool enable_metal = false, enable_paper = false, enable_glass = false;
    int travel_metal = 0, travel_paper = 0, travel_glass = 0, result = 0;
    for (int i = static_cast<int>(garbage.size()) - 1; i >= 0; --i)
    {
        auto [metal, paper, glass] = process(garbage[i]);
        result += metal +  paper +  glass;
        enable_metal |= metal > 0;
        enable_paper |= paper > 0;
        enable_glass |= glass > 0;
        if (enable_metal && (i > 0)) travel_metal += travel[i - 1];
        if (enable_paper && (i > 0)) travel_paper += travel[i - 1];
        if (enable_glass && (i > 0)) travel_glass += travel[i - 1];
    }
    return result + travel_metal + travel_paper + travel_glass;
}
#else
int garbageCollection(std::vector<std::string> garbage, std::vector<int> travel)
{
    auto process = [](std::string house) -> std::tuple<int, int, int>
    {
        int metal = 0, paper = 0, glass = 0;
        for (char type : house)
        {
            metal += (type == 'M');
            paper += (type == 'P');
            glass += (type == 'G');
        }
        return {metal, paper, glass};
    };
    auto [metal, paper, glass] = process(garbage[0]);
    size_t last_metal = 0, last_paper = 0, last_glass = 0;
    std::vector<int> accumulated_travel(garbage.size());
    accumulated_travel[0] = 0;
    for (size_t i = 1, n = garbage.size(); i < n; ++i)
    {
        auto [current_metal, current_paper, current_glass] = process(garbage[i]);
        accumulated_travel[i] = accumulated_travel[i - 1] + travel[i - 1];
        if (current_metal > 0) last_metal = i;
        if (current_paper > 0) last_paper = i;
        if (current_glass > 0) last_glass = i;
        metal += current_metal;
        paper += current_paper;
        glass += current_glass;
    }
    return metal + accumulated_travel[last_metal]
         + paper + accumulated_travel[last_paper]
         + glass + accumulated_travel[last_glass];
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> garbage,
          std::vector<int> travel,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = garbageCollection(garbage, travel);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"G", "P", "GP", "GG"}, {2, 4, 3}, 21, trials);
    test({"MMM", "PGM", "GP"}, {3, 10}, 37, trials);
    return 0;
}


