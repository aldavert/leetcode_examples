#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<int> findDisappearedNumbers(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    for (int i = 0; i < n; ++i)
    {
        int idx = std::abs(nums[i]) - 1;
        if (nums[idx] > 0)
            nums[idx] = -nums[idx];
    }
    int size = 0;
    for (int i = 0; i < n; ++i)
        if (nums[i] >= 0)
            nums[size++] = i + 1;
    nums.resize(size);
    return nums;
}
#else
std::vector<int> findDisappearedNumbers(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    for (int i = 0; i < n; ++i)
    {
        int idx = std::abs(nums[i]) - 1;
        if (nums[idx] > 0)
            nums[idx] = -nums[idx];
    }
    std::vector<int> result;
    for (int i = 0; i < n; ++i)
        if (nums[i] >= 0)
            result.push_back(i + 1);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findDisappearedNumbers(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 3, 2, 7, 8, 2, 3, 1}, {5, 6}, trials);
    test({1, 1}, {2}, trials);
    return 0;
}


