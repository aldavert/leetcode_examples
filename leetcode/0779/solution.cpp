#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int kthGrammar(int n, int k)
{
    auto process = [&](auto &&self, int level, int position) -> bool
    {
        if (level == 0) return false;
        else
        {
            bool previous = self(self, level - 1, position / 2);
            return (position & 1)?!previous:previous;
        }
    };
    return process(process, n - 1, k - 1);
}

// ############################################################################
// ############################################################################

void test(int n, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = kthGrammar(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 1, 0, trials);
    test(2, 1, 0, trials);
    test(2, 2, 1, trials);
    test(5,  1, 0, trials);
    test(5,  2, 1, trials);
    test(5,  3, 1, trials);
    test(5,  4, 0, trials);
    test(5,  5, 1, trials);
    test(5,  6, 0, trials);
    test(5,  7, 0, trials);
    test(5,  8, 1, trials);
    test(5,  9, 1, trials);
    test(5, 10, 0, trials);
    test(5, 11, 0, trials);
    test(5, 12, 1, trials);
    test(5, 13, 0, trials);
    test(5, 14, 1, trials);
    test(5, 15, 1, trials);
    test(5, 16, 0, trials);
    return 0;
}


