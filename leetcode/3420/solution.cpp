#include "../common/common.hpp"
#include <deque>

// ############################################################################
// ############################################################################

long long countNonDecreasingSubarrays(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    long result = 0, cost = 0;
    std::deque<std::pair<int, int> > dq;
    
    for (int i = n - 1, j = n - 1; i >= 0; --i)
    {
        int count = 1;
        while (!dq.empty() && dq.back().first < nums[i])
        {
            const auto [next_num, next_count] = dq.back();
            dq.pop_back();
            count += next_count;
            cost += static_cast<long>(nums[i] - next_num) * next_count;
        }
        dq.emplace_back(nums[i], count);
        while (cost > k)
        {
            const auto [rightmost_num, rightmost_count] = dq.front();
            dq.pop_front();
            cost -= static_cast<long>(rightmost_num - nums[j--]);
            if (rightmost_count > 1)
                dq.emplace_front(rightmost_num, rightmost_count - 1);
        }
        result += j - i + 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countNonDecreasingSubarrays(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({6, 3, 1, 2, 4, 4}, 7, 17, trials);
    test({6, 3, 1, 3, 6}, 4, 12, trials);
    return 0;
}


