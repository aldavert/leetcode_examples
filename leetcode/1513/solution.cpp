#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int numSub(std::string s)
{
    long result = 0, counter = 0;
    for (int i = 0, n = static_cast<int>(s.size()); i < n; ++i)
    {
        counter += s[i] == '1';
        if (s[i] == '0')
        {
            result += counter * (counter + 1) / 2;
            counter = 0;
        }
    }
    result += counter * (counter + 1) / 2;
    return static_cast<int>(result % 1'000'000'007);
}
#else
int numSub(std::string s)
{
    constexpr int MOD = 1'000'000'007;
    int result = 0;
    
    for (int i = 0, l = -1, n = static_cast<int>(s.size()); i < n; ++i)
    {
        if (s[i] == '0') l = i;
        result = (result + i - l) % MOD;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numSub(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("0110111", 9, trials);
    test("101", 2, trials);
    test("111111", 21, trials);
    return 0;
}


