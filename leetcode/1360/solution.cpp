#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int daysBetweenDates(std::string date1, std::string date2)
{
    struct Date
    {
        int year;
        int month;
        int day;
        bool operator<(const Date &other) const
        {
            return (year <  other.year)
                || ((year == other.year) && (month <  other.month))
                || ((year == other.year) && (month == other.month) && (day < other.day));
        }
    };
    auto parse = [](std::string date) -> Date
    {
        return { (date[0] - '0') * 1000 + (date[1] - '0') * 100
               + (date[2] - '0') * 10 + (date[3] - '0'),
                 (date[5] - '0') * 10 + (date[6] - '0'),
                 (date[8] - '0') * 10 + (date[9] - '0') };
    };
    auto isLeap = [](int year) -> bool
    {
        return (year % 4 == 0) && !((year % 100 == 0) && (year % 400 != 0));
    };
    auto dayOfYear = [&](Date date) -> int
    {
        constexpr int month_accum[] = {0, 31, 59, 90, 120, 151, 181, 212, 243,
                                       273, 304, 334};
        return month_accum[date.month - 1] + date.day
             + isLeap(date.year) * (date.month > 2);
    };
    auto yearLength = [&](int year) -> int { return 365 + isLeap(year); };
    auto value1 = parse(date1), value2 = parse(date2);
    if (value2 < value1) std::swap(value1, value2);
    if (value1.year == value2.year)
        return dayOfYear(value2) - dayOfYear(value1);
    else
    {
        int result = yearLength(value1.year) - dayOfYear(value1);
        for (int year = value1.year + 1; year < value2.year; ++year)
            result += yearLength(year);
        return result + dayOfYear(value2);
    }
}

// ############################################################################
// ############################################################################

void test(std::string date1,
          std::string date2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = daysBetweenDates(date1, date2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("2019-06-29", "2019-06-30",  1, trials);
    test("2020-01-15", "2019-12-31", 15, trials);
    test("2020-01-15", "2018-12-31", 380, trials);
    return 0;
}

