#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int removeCoveredIntervals(std::vector<std::vector<int> > intervals)
{
    const int n = static_cast<int>(intervals.size());
    std::vector<bool> enabled(n, true);
    auto cmp = [](const auto &l, const auto &r) -> bool
    {
        return l[0] < r[0] || ((l[0] == r[0]) && (l[1] > r[1]));
    };
    std::sort(intervals.begin(), intervals.end(), cmp);
    int result = n;
    for (int i = 0; i < n; ++i)
    {
        if (enabled[i])
        {
            for (int j = i + 1; (j < n) && (intervals[j][0] < intervals[i][1]); ++j)
            {
                if (enabled[j]
                &&  (intervals[i][0] <= intervals[j][0])
                &&  (intervals[i][1] >= intervals[j][1]))
                {
                    enabled[j] = false;
                    --result;
                }
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > intervals,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = removeCoveredIntervals(intervals);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 4}, {3, 6}, {2, 8}}, 2, trials);
    test({{1, 4}, {2, 3}}, 1, trials);
    test({{1, 2}, {1, 4}, {3, 4}}, 1, trials);
    return 0;
}


