#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestAlternatingSubarray(std::vector<int> nums, int threshold)
{
    const int n = static_cast<int>(nums.size());
    int result = 0;
    for (int l = 0; l < n; ++l)
    {
        while ((l < n) && ((nums[l] % 2 != 0) || (nums[l] > threshold))) ++l;
        int r = l;
        bool even = false;
        while ((r < n) && (nums[r] % 2 == even) && (nums[r] <= threshold))
            even = !even,
            ++r;
        result = std::max(result, r - l);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int threshold, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestAlternatingSubarray(nums, threshold);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 5, 4}, 5, 3, trials);
    test({1, 2}, 2, 1, trials);
    test({2, 3, 4, 5}, 4, 3, trials);
    return 0;
}


