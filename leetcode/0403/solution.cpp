#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

bool canCross(std::vector<int> stones)
{
    const int goal = stones.back();
    std::unordered_map<int, std::unordered_set<int> > positions;
    for (int s : stones) positions[s] = {};
    auto dp = [&](auto &&self, int pos, int step)
    {
        if (pos == goal) return true;
        if (step == 0) return false;
        if (auto it_pos = positions.find(pos); it_pos != positions.end())
        {
            auto &steps_done = it_pos->second;
            if (auto it_step = steps_done.find(step);
                     it_step == steps_done.end())
            {
                steps_done.insert(step);
                if (self(self, pos + step + 1, step + 1)) return true;
                if (self(self, pos + step    , step    )) return true;
                if (self(self, pos + step - 1, step - 1)) return true;
            }
        }
        return false;
    };
    return dp(dp, 1, 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> stones, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canCross(stones);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 3, 5, 6, 8, 12, 17}, true, trials);
    test({0, 1, 2, 3, 4, 8, 9, 11}, false, trials);
    test({0, 2}, false, trials);
    return 0;
}


