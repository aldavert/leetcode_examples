#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string longestDiverseString(int a, int b, int c,
                                 char A = 'a', char B = 'b', char C = 'c')
{
    if (a < b)
        return longestDiverseString(b, a, c, B, A, C);
    if (b < c)
        return longestDiverseString(a, c, b, A, C, B);
    if (b == 0)
        return std::string(std::min(a, 2), A);
    int use_a = std::min(a, 2),
        use_b = (a - use_a >= b);
    return std::string(use_a, A) + std::string(use_b, B) +
        longestDiverseString(a - use_a, b - use_b, c, A, B, C);
}

// ############################################################################
// ############################################################################

void test(int a, int b, int c, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestDiverseString(a, b, c);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 1, 7, "ccaccbcc", trials);
    test(7, 1, 0, "aabaa", trials);
    return 0;
}


