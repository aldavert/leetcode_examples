#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

bool validateStackSequences(std::vector<int> pushed, std::vector<int> popped)
{
    const int n = static_cast<int>(pushed.size());
    std::stack<int> s;
    int pop_idx = 0;
    for (int push_idx = 0; push_idx < n; ++push_idx)
    {
        s.push(pushed[push_idx]);
        while (!s.empty() && (s.top() == popped[pop_idx]))
        {
            s.pop();
            ++pop_idx;
        }
    }
    return s.empty();
}

// ############################################################################
// ############################################################################

void test(std::vector<int> pushed,
          std::vector<int> popped,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = validateStackSequences(pushed, popped);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, {4, 5, 3, 2, 1},  true, trials);
    test({1, 2, 3, 4, 5}, {4, 3, 5, 1, 2}, false, trials);
    test({1, 2, 3, 4, 5}, {1, 2, 3, 4, 5},  true, trials);
    test({1, 2, 3, 4, 5}, {3, 2, 1, 4, 5},  true, trials);
    return 0;
}


