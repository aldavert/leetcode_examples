#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

std::vector<int> getSumAbsoluteDifferences(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int total_sum = std::accumulate(nums.begin(), nums.end(), 0);
    std::vector<int> result;
    for (int previous = 0, count = 0; int value : nums)
    {
        result.push_back(total_sum - previous + value * (2 * count - n));
        //result.push_back(value * count - previous
        //               + total_sum - value * (nums.size() - count));
        total_sum -= value;
        previous += value;
        ++count;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getSumAbsoluteDifferences(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 5}, {4, 3, 5}, trials);
    test({1, 4, 6, 8, 10}, {24, 15, 13, 15, 21}, trials);
    return 0;
}


