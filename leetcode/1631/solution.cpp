#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minimumEffortPath(std::vector<std::vector<int> > heights)
{
    const int rows = static_cast<int>(heights.size());
    const int cols = static_cast<int>(heights[0].size());
    constexpr static int dx[] = {  1,  0, -1,  0};
    constexpr static int dy[] = {  0, -1,  0,  1};
    struct Position
    {
        int x = 0;
        int y = 0;
        int effort = 0;
        inline bool operator<(const Position &other) const
        {
            return effort > other.effort;
        }
        inline bool outside(int r, int c) const
        {
            return (x < 0) || (y < 0) || (x >= r) || (y >= c);
        }
        inline bool& operator[](bool matrix[101][101])
        {
            return matrix[x][y];
        }
        inline int& operator[](int matrix[101][101])
        {
            return matrix[x][y];
        }
        inline int& operator[](std::vector<std::vector<int> > &matrix)
        {
            return matrix[x][y];
        }
    };
    std::priority_queue<Position> heap;
    int efforts[101][101];
    bool seen[101][101];
    for (int r = 0; r < rows; ++r)
    {
        for (int c = 0; c < cols; ++c)
        {
            efforts[r][c] = std::numeric_limits<int>::max();
            seen[r][c] = false;
        }
    }
    heap.push({0, 0, 0});
    efforts[0][0] = 0;
    while (!heap.empty())
    {
        auto current = heap.top();
        heap.pop();
        if ((current.x == rows - 1) && (current.y == cols - 1))
            return current.effort;
        current[seen] = true;
        for (int k = 0; k < 4; ++k)
        {
            Position next = {current.x + dx[k], current.y + dy[k], 0};
            if (next.outside(rows, cols) || next[seen])
                continue;
            next.effort = std::max(current[efforts],
                                   std::abs(current[heights] - next[heights]));
            if (next.effort < next[efforts])
            {
                next[efforts] = next.effort;
                heap.push(next);
            }
        }
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > heights,
          int solution,
          unsigned int trials = 1)
{
    int result = 0;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumEffortPath(heights);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 2},
          {3, 8, 2},
          {5, 3, 5}}, 2, trials);
    test({{1, 2, 3},
          {3, 8, 4},
          {5, 3, 5}}, 1, trials);
    test({{1, 2, 1, 1, 1},
          {1, 2, 1, 2, 1},
          {1, 2, 1, 2, 1},
          {1, 2, 1, 2, 1},
          {1, 1, 1, 2, 1}}, 0, trials);
    return 0;
}


