#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> constructTransformedArray(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> result;
    for (int i = 0; i < n; ++i)
        result.push_back(nums[(n + ((nums[i] + i) % n)) % n]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
        result = constructTransformedArray(nums);
    showResult(result == solution, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, -2, 1, 1}, {1, 1, 1, 3}, trials);
    test({-1, 4, -1}, {-1, -1, 4}, trials);
    return 0;
}


