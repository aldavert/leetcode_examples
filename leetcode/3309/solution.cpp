#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxGoodNumber(std::vector<int> nums)
{
    auto concat = [](int a, int b) -> int
    {
        return a << (32 - std::countl_zero(static_cast<unsigned>(b))) | b;
    };
    if (concat(nums[0], nums[2]) < concat(nums[2], nums[0])) std::swap(nums[0], nums[2]);
    if (concat(nums[0], nums[1]) < concat(nums[1], nums[0])) std::swap(nums[0], nums[1]);
    if (concat(nums[1], nums[2]) < concat(nums[2], nums[1])) std::swap(nums[1], nums[2]);
    return concat(concat(nums[0], nums[1]), nums[2]);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxGoodNumber(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 30, trials);
    test({2, 8, 16}, 1296, trials);
    return 0;
}


