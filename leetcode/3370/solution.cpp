#include "../common/common.hpp"
#include <bit>

// ############################################################################
// ############################################################################

int smallestNumber(int n)
{
    int n_ones = 32 - std::countl_zero(static_cast<unsigned int>(n));
    return (1 << n_ones) - 1;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestNumber(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 7, trials);
    test(10, 15, trials);
    test(3, 3, trials);
    return 0;
}


