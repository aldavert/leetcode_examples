#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int longestSubsequence(std::vector<int> arr, int difference)
{
    const int n = static_cast<int>(arr.size());
    constexpr int OFFSET = 10'000;
    constexpr int SIZE = 3 * OFFSET + 4;
    
    int result = 0, prefix_longest[SIZE] = {};
    for (int i = 0; i < n; ++i)
    {
        int current_length = 1, pre_idx = arr[i] - difference;
        if (pre_idx >= -OFFSET)
        {
            current_length = std::max(current_length,
                                      1 + prefix_longest[OFFSET + pre_idx]);
            result = std::max(result, current_length);
        }
        prefix_longest[OFFSET + arr[i]] = current_length;
    }
    return result;
}
#else
int longestSubsequence(std::vector<int> arr, int difference)
{
    int result = 0;
    std::unordered_map<int, int> length_at;
    
    for (int a : arr)
    {
        if (auto it = length_at.find(a - difference); it != length_at.end())
            length_at[a] = it->second + 1;
        else
            length_at[a] = 1;
        result = std::max(result, length_at[a]);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int difference, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestSubsequence(arr, difference);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 1, 4, trials);
    test({1, 3, 5, 7}, 1, 1, trials);
    test({1, 5, 7, 8, 5, 3, 4, 2, 1}, -2, 4, trials);
    return 0;
}


