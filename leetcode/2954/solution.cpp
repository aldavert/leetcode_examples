#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfSequence(int n, std::vector<int> sick)
{
    constexpr int MOD = 1'000'000'007;
    auto modPow = [&](auto &&self, long x, long v) -> long
    {
        if (v == 0) return 1;
        if (v % 2 == 1) return x * self(self, x % MOD, (v - 1)) % MOD;
        return self(self, x * x % MOD, (v / 2)) % MOD;
    };
    const int m = n - static_cast<int>(sick.size());
    std::vector<long> fact(m + 1), inv_fact(m + 1), inv(m + 1);
    fact[0] = inv_fact[0] = 1;
    inv[0] = inv[1] = 1;
    for (int i = 1; i <= m; ++i)
    {
        if (i >= 2)
            inv[i] = MOD - MOD / i * inv[MOD % i] % MOD;
        fact[i] = fact[i - 1] * i % MOD;
        inv_fact[i] = inv_fact[i - 1] * inv[i] % MOD;
    }
    long result = fact[m];
    int prev_sick = -1;
    for (size_t i = 0; i < sick.size(); ++i)
    {
        const int non_infected = sick[i] - prev_sick - 1;
        prev_sick = sick[i];
        if (non_infected == 0) continue;
        result = (result * inv_fact[non_infected]) % MOD;
        if (i > 0)
            result = (result * modPow(modPow, 2, non_infected - 1)) % MOD;
    }
    return static_cast<int>((result * inv_fact[n - sick.back() - 1]) % MOD);
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<int> sick, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfSequence(n, sick);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {0, 4}, 4, trials);
    test(4, {1}, 3, trials);
    return 0;
}


