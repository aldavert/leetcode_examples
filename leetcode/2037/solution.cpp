#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minMovesToSeat(std::vector<int> seats, std::vector<int> students)
{
    int result = 0;
    std::sort(seats.begin(), seats.end());
    std::sort(students.begin(), students.end());
    for (size_t i = 0, n = seats.size(); i < n; ++i)
        result += std::abs(seats[i] - students[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> seats,
          std::vector<int> students,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minMovesToSeat(seats, students);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1, 5}, {2, 7, 4}, 4, trials);
    test({4, 1, 5, 9}, {1, 3, 2, 6}, 7, trials);
    test({2, 2, 6, 6}, {1, 3, 2, 6}, 4, trials);
    return 0;
}


