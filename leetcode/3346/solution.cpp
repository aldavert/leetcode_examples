#include "../common/common.hpp"
#include <unordered_map>
#include <map>

// ############################################################################
// ############################################################################

#if 1
int maxFrequency(std::vector<int> nums, int k, int numOperations)
{
    int max_v = *std::max_element(nums.begin(), nums.end());
    std::vector<int> count(max_v), s(max_v + 1);
    for (int x : nums) ++count[x - 1];
    for (int i = 1; i <= max_v; ++i) s[i] = s[i - 1] + count[i - 1];
    int result = 1;
    for (int i = 0; i < max_v; ++i)
    {
        int l = std::max(0, i - k), r = std::min(max_v - 1, i + k);
        result = std::max(result, std::min(s[r + 1] - s[l] - count[i], numOperations)
               + count[i]);
    }
    return result;
}
#else
int maxFrequency(std::vector<int> nums, int k, int numOperations)
{
    std::unordered_map<int, int> count;
    std::map<int, int> line;
    std::set<int> candidates;
    int result = 1;
    
    for (const int num : nums)
    {
        ++count[num];
        ++line[num - k];
        --line[num + k + 1];
        candidates.insert(num);
        candidates.insert(num - k);
        candidates.insert(num + k + 1);
    }
    for (int adjustable = 0; const int num : candidates)
    {
        adjustable += line.contains(num) ? line[num] : 0;
        const int count_num = count.contains(num) ? count[num] : 0;
        const int adjusted = adjustable - count_num;
        result = std::max(result, count_num + std::min(numOperations, adjusted));
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          int numOperations,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxFrequency(nums, k, numOperations);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 5}, 1, 2, 2, trials);
    test({5, 11, 20, 20}, 5, 1, 2, trials);
    return 0;
}


