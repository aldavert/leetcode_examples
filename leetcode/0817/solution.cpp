#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

int numComponents(ListNode * head, std::vector<int> nums)
{
    std::unordered_set<int> valid(nums.begin(), nums.end());
    int result = 0;
    for (ListNode * current = head; current != nullptr; current = current->next)
        result += (valid.find(current->val) != valid.end())
               && ((!current->next) || (valid.find(current->next->val) == valid.end()));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> list,
          std::vector<int> nums,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(list);
        result = numComponents(head, nums);
        delete head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 2, 3}, {0, 1, 3}, 2, trials);
    test({0, 1, 2, 3, 4}, {0, 3, 1, 4}, 2, trials);
    return 0;
}


