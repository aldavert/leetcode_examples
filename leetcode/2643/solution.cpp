#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> rowAndMaximumOnes(std::vector<std::vector<int> > mat)
{
    std::vector<int> result(2);
    for (size_t i = 0, n = mat.size(); i < n; ++i)
    {
        int count = 0;
        for (int value : mat[i])
            count += value == 1;
        if (count > result[1])
            result[0] = static_cast<int>(i), result[1] = count;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > mat,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = rowAndMaximumOnes(mat);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {1, 0}}, {0, 1}, trials);
    test({{0, 0, 0}, {0, 1, 1}}, {1, 2}, trials);
    test({{0, 0}, {1, 1}, {0, 0}}, {1, 2}, trials);
    return 0;
}


