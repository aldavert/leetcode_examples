#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canMakeArithmeticProgression(std::vector<int> arr)
{
    if (arr.size() == 2) return true;
    std::sort(arr.begin(), arr.end());
    const int step = arr[1] - arr[0];
    for (int previous = arr[0] - step; int value : arr)
    {
        if (value - previous != step) return false;
        previous = value;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canMakeArithmeticProgression(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 5, 1},  true, trials);
    test({1, 2, 4}, false, trials);
    return 0;
}


