#include "../common/common.hpp"
#include <memory>

// ############################################################################
// ############################################################################

#if 1
class BookMyShow
{
    int m_number_of_rows = 0;
    int m_number_of_seats = 0;
    int m_tz = 1;
    std::vector<int> m_t;
    std::vector<unsigned long> m_b;

public:
    BookMyShow(int n, int m) : m_number_of_rows(n), m_number_of_seats(m), m_b(n)
    {
        unsigned long sum = 0;
        m_tz = 1;
        while (m_tz < m_number_of_rows) m_tz *= 2;
        m_t.resize(m_tz * 2);
        for (int i = 0; i < m_number_of_rows; ++i)
            m_b[i] = sum += m_t[i + m_tz] = m_number_of_seats;
        for (int i = m_tz - 1; i >= 1; --i)
            m_t[i] = std::max(m_t[i * 2], m_t[i * 2 + 1]);
        for (int j = m_number_of_rows - 1; j >= 0; --j)
            if (int i = j & (j + 1); i > 0)
                m_b[j] -= m_b[i - 1];
    }
    std::vector<int> gather(int k, int maxRow)
    {
        int r = getR(k);
        if (r > maxRow) return {};
        int c = m_number_of_seats - m_t[r + m_tz];
        update(r, k);
        return {r, c};
    }
    
    bool scatter(int k, int maxRow)
    {
        if (prefSum(maxRow) < static_cast<unsigned long>(k)) return false;
        int i = getR(1);
        while (k > 0)
        {
            if (int x = m_t[i + m_tz]; x > 0)
            {
                int d = std::min(x, k);
                k -= d;
                update(i, d);
            }
            ++i;
        }
        return true;
    }
private:
    int getR(int k)
    {
        int i = 1;
        if (m_t[i] < k) return m_number_of_rows;
        while ((i *= 2) < m_tz * 2)
            if (m_t[i] < k) ++i;
        return i / 2 - m_tz;
    }
    void update(int i, int d)
    {
        int j = i + m_tz;
        m_t[j] -= d;
        while ((j /= 2) > 0)
            m_t[j] = std::max(m_t[j * 2], m_t[j * 2 + 1]);
        m_b[j = i] -= d;
        while ((j |= j + 1) < m_number_of_rows)
            m_b[j] -= d;
    }
    unsigned long prefSum(int i)
    {
        unsigned long r = m_b[i];
        while ((i &= i + 1) > 0)
            r += m_b[--i];
        return r;
    }
};
#else
class SegmentTree
{
public:
    SegmentTree(int n, int m) : m_m(m), m_root(build(0, n - 1)) {}
    std::vector<int> maxRange(int k, int max_row)
    {
        return maxRange(m_root, k, max_row);
    }
    inline long sumRange(int max_row) { return sumRange(m_root, 0, max_row); }
    inline void minus(int row, int k) { minus(m_root, row, k); }

private:
    struct SegmentTreeNode
    {
        int lo = 0;
        int hi = 0;
        std::unique_ptr<SegmentTreeNode> left;
        std::unique_ptr<SegmentTreeNode> right;
        int max = 0;
        long sum = 0;
        SegmentTreeNode(int plo, int phi, std::unique_ptr<SegmentTreeNode> &&pleft,
        std::unique_ptr<SegmentTreeNode> &&pright, int pmax, long psum) : lo(plo),
        hi(phi), left(std::move(pleft)), right(std::move(pright)),
        max(pmax), sum(psum) {} // Clang needs this...
    };
    const int m_m;
    std::unique_ptr<SegmentTreeNode> m_root;

    std::unique_ptr<SegmentTreeNode> build(int l, int r)
    {
        if (l == r)
            return std::make_unique<SegmentTreeNode>(l, r, nullptr, nullptr, m_m, m_m);
        int mid = (l + r) / 2;
        std::unique_ptr<SegmentTreeNode> left = build(l, mid),
                                         right = build(mid + 1, r);
        int new_max = std::max(left->max, right->max);
        long new_sum = left->sum + right->sum;
        return std::make_unique<SegmentTreeNode>(l, r,
                                                 std::move(left), std::move(right),
                                                 new_max, new_sum);
    }
    std::vector<int> maxRange(std::unique_ptr<SegmentTreeNode> &node, int k, int max_row)
    {
        if (node->lo == node->hi)
        {
            if ((node->sum < k) || (node->lo > max_row))
                return {};
            return {node->lo, m_m - static_cast<int>(node->sum)};
        }
        if (node->left->max >= k)
            return maxRange(node->left, k, max_row);
        return maxRange(node->right, k, max_row);
    }
    long sumRange(std::unique_ptr<SegmentTreeNode> &node, int i, int j)
    {
        if ((node->lo == i) && (node->hi == j))
            return node->sum;
        int mid = (node->lo + node->hi) / 2;
        if (j <= mid)
            return sumRange(node->left, i, j);
        if (i > mid)
            return sumRange(node->right, i, j);
        return sumRange(node->left, i, mid) + sumRange(node->right, mid + 1, j);
    }
    void minus(std::unique_ptr<SegmentTreeNode> &node, int row, int k)
    {
        if (node == nullptr)
            return;
        if ((node->lo == node->hi) && (node->hi == row))
        {
            node->max -= k;
            node->sum -= k;
            return;
        }
        int mid = (node->lo + node->hi) / 2;
        if (row <= mid)
            minus(node->left, row, k);
        else
            minus(node->right, row, k);
        node->max = std::max(node->left->max, node->right->max);
        node->sum = node->left->sum + node->right->sum;
    }
};

class BookMyShow
{
  SegmentTree m_tree;
  std::vector<int> m_seats;
  int m_min_vacant_row = 0;
public:
    BookMyShow(int n, int m) : m_tree(n, m), m_seats(n, m) {}
    std::vector<int> gather(int k, int maxRow)
    {
        std::vector<int> result = m_tree.maxRange(k, maxRow);
        if (result.size() == 2)
        {
            m_tree.minus(result[0], k);
            m_seats[result[0]] -= k;
        }
        return result;
    }
    bool scatter(int k, int maxRow)
    {
        if (m_tree.sumRange(maxRow) < k)
            return false;
        while (k > 0)
        {
            if (m_seats[m_min_vacant_row] >= k)
            {
                m_tree.minus(m_min_vacant_row, k);
                m_seats[m_min_vacant_row] -= k;
                k = 0;
            }
            else
            {
                m_tree.minus(m_min_vacant_row, m_seats[m_min_vacant_row]);
                k -= m_seats[m_min_vacant_row];
                m_seats[m_min_vacant_row] = 0;
                ++m_min_vacant_row;
            }
        }
        return true;
    }
};
#endif

// ############################################################################
// ############################################################################

void test(int n,
          int m,
          std::vector<bool> gather,
          std::vector<std::pair<int, int> > input,
          std::vector<std::variant<std::vector<int>, bool> > solution,
          unsigned int trials = 1)
{
    std::vector<std::variant<std::vector<int>, bool> > result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        BookMyShow object(n, m);
        result.clear();
        for (size_t i = 0; i < gather.size(); ++i)
        {
            if (gather[i])
                result.push_back(object.gather(input[i].first, input[i].second));
            else result.push_back(object.scatter(input[i].first, input[i].second));
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 5, {true, true, false, false},
         {{4, 0}, {2, 0}, {5, 1}, {5, 1}},
         {std::vector<int>{0, 0}, std::vector<int>(), true, false}, trials);
    return 0;
}


