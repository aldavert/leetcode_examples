#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int countBattleships(std::vector<std::vector<char> > board)
{
    const size_t m = board.size(),
                 n = board[0].size();
    int result = 0;
    for (size_t y = 0; y < m; ++y)
        for (size_t x = 0; x < n; ++x)
            if (board[y][x] == 'X')
                result += !(((y + 1 < m) && (board[y + 1][x] == 'X'))
                         || ((x + 1 < n) && (board[y][x + 1] == 'X')));
    return result;
}
#else
int countBattleships(std::vector<std::vector<char> > board)
{
    const size_t m = board.size(),
                 n = board[0].size();
    int result = 0;
    for (size_t y = 0; y < m; ++y)
    {
        for (size_t x = 0; x < n; ++x)
        {
            if (board[y][x] == 'X')
            {
                ++result;
                if ((y + 1 < m) && (board[y + 1][x] == 'X'))
                    for (size_t i = y; (i < m) && (board[i][x] == 'X'); ++i)
                        board[i][x] = '.';
                else if ((x + 1 < n) && (board[y][x + 1] == 'X'))
                    for (size_t i = x; (i < n) && (board[y][i] == 'X'); ++i)
                        board[y][i] = '.';
                else board[y][x] = '.';
            }
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > board, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countBattleships(board);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'X', '.', '.', 'X'}, {'.', '.', '.', 'X'}, {'.', '.', '.', 'X'}}, 2, trials);
    test({{'.'}}, 0, trials);
    test({{'X', '.', 'X'}, {'X', '.', 'X'}}, 2, trials);
    return 0;
}


