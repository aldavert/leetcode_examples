#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkZeroOnes(std::string s)
{
    if (s.empty()) return false;
    int length_digit[2] = {};
    char previous = s[0];
    int length = 0;
    auto update = [&]()
    {
        const int index = static_cast<int>(previous - '0');
        length_digit[index] = std::max(length_digit[index], length);
    };
    for (char digit : s)
    {
        if (digit != previous)
        {
            update();
            previous = digit;
            length = 0;
        }
        ++length;
    }
    update();
    return length_digit[1] > length_digit[0];
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkZeroOnes(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1101", true, trials);
    test("111000", false, trials);
    test("110100010", false, trials);
    return 0;
}


