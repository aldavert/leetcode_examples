#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool hasAllCodes(std::string s, int k)
{
    int nvalues = 1 << k;
    std::vector<bool> present(nvalues, false);
    const int n = static_cast<int>(s.size());
    if (n < k + nvalues - 1) return false;
    int value = 0, mask = 0;
    for (int i = 0; i < k - 1; ++i)
    {
        value = (value << 1) | (s[i] == '1');
        mask = (mask << 1) | 1;
    }
    mask = (mask << 1) | 1;
    for (int i = k - 1; i < n; ++i)
    {
        value = ((value << 1) | (s[i] == '1')) & mask;
        if (!present[value])
        {
            present[value] = true;
            --nvalues;
            if (!nvalues)
                return true;
        }
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = hasAllCodes(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("00110110", 2, true, trials);
    test("0110", 1, true, trials);
    test("0110", 2, false, trials);
    test("11111", 2, false, trials);
    return 0;
}


