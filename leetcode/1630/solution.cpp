#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<bool> checkArithmeticSubarrays(
        std::vector<int> nums,
        std::vector<int> l,
        std::vector<int> r)
{
    auto putativeArithmeticSubarray = [&](int left, int right)
    {
        int sum = 0,
            min = std::numeric_limits<int>::max(),
            max = std::numeric_limits<int>::lowest();
        for (int i = left; i <= right; ++i)
            max = std::max(max, nums[i]),
            min = std::min(min, nums[i]),
            sum += nums[i];
        return sum == (((min + max) * (right - left + 1)) / 2);
    };
    auto sortCheck = [&](int left, int right)
    {
        std::vector<int> data;
        for (int i = left; i <= right; ++i)
            data.push_back(nums[i]);
        std::sort(data.begin(), data.end());
        for (int i = 2, n = right - left + 1, inc = data[1] - data[0]; i < n; ++i)
            if (data[i] - data[i - 1] != inc) return false;
        return true;
    };
    const size_t m = l.size();
    std::vector<bool> result(m);
    for (size_t i = 0; i < m; ++i)
    {
        if (putativeArithmeticSubarray(l[i], r[i]))
            result[i] = sortCheck(l[i], r[i]);
        else result[i] = false;
    }
    return result;
}
#elif 0
std::vector<bool> checkArithmeticSubarrays(
        std::vector<int> nums,
        std::vector<int> l,
        std::vector<int> r)
{
    auto putativeArithmeticSubarray = [&](int left, int right)
    {
        int sum = 0,
            min = std::numeric_limits<int>::max(),
            max = std::numeric_limits<int>::lowest();
        for (int i = left; i <= right; ++i)
            max = std::max(max, nums[i]),
            min = std::min(min, nums[i]),
            sum += nums[i];
        return sum == (((min + max) * (right - left + 1)) / 2);
    };
    const int n = static_cast<int>(nums.size());
    const size_t m = l.size();
    std::vector<std::tuple<int, int> > value_position;
    for (int i = 0; i < n; ++i)
        value_position.push_back({nums[i], i});
    std::sort(value_position.begin(), value_position.end());
    std::vector<bool> result(m);
    for (size_t i = 0; i < m; ++i)
        result[i] = putativeArithmeticSubarray(l[i], r[i]);
    std::vector<int> increase(m, -1), previous(m, -1'000'000);
    for (auto [value, position] : value_position)
    {
        for (size_t j = 0; j < m; ++j)
        {
            if (result[j] && ((position >= l[j]) && (position <= r[j])))
            {
                if (previous[j] == -1'000'000)
                    previous[j] = value;
                else if (increase[j] == -1)
                    increase[j] = value - previous[j],
                    previous[j] = value;
                else if (value - previous[j] == increase[j])
                    previous[j] = value;
                else result[j] = false;
            }
        }
    }
    return result;
}
#else
std::vector<bool> checkArithmeticSubarrays(
        std::vector<int> nums,
        std::vector<int> l,
        std::vector<int> r)
{
    const int n = static_cast<int>(nums.size());
    const size_t m = l.size();
    std::vector<std::tuple<int, int> > value_position;
    for (int i = 0; i < n; ++i)
        value_position.push_back({nums[i], i});
    std::sort(value_position.begin(), value_position.end());
    std::vector<bool> result(m, true);
    std::vector<int> increase(m, -1), previous(m, -1'000'000);
    for (auto [value, position] : value_position)
    {
        for (size_t j = 0; j < m; ++j)
        {
            if (result[j] && ((position >= l[j]) && (position <= r[j])))
            {
                if (previous[j] == -1'000'000)
                    previous[j] = value;
                else if (increase[j] == -1)
                    increase[j] = value - previous[j],
                    previous[j] = value;
                else if (value - previous[j] == increase[j])
                    previous[j] = value;
                else result[j] = false;
            }
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> l,
          std::vector<int> r,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkArithmeticSubarrays(nums, l, r);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 6, 5, 9, 3, 7}, {0, 0, 2}, {2, 3, 5}, {true, false, true}, trials);
    test({-12, -9, -3, -12, -6, 15, 20, -25, -20, -15, -10},
         {0, 1, 6, 4, 8, 7},
         {4, 4, 9, 7, 9, 10},
         {false, true, false, false, true, true}, trials);
    return 0;
}


