#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int arrayPairSum(std::vector<int> nums)
{
    const size_t n = nums.size();
    int result = 0;
    std::sort(nums.begin(), nums.end());
    for (size_t i = 0; i < n; i += 2)
        result += nums[i];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = arrayPairSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 3, 2}, 4, trials);
    test({6, 2, 6, 5, 1, 2}, 9, trials);
    return 0;
}


