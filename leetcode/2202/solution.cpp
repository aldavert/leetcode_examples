#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumTop(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    if ((k == 0) || (k == 1))
        return (n == k)?-1:nums[k];
    if (n == 1)
        return (k & 1)?-1:nums[0];
    int mx = *std::max_element(nums.begin(), nums.begin() + std::min(n, k - 1));
    return (k >= n)?mx:std::max(mx, nums[k]);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumTop(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 2, 2, 4, 0, 6}, 4, 5, trials);
    test({2}, 1, -1, trials);
    return 0;
}


