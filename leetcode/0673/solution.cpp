#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findNumberOfLIS(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = 0, max_length = 0;
    std::vector<int> length(n, 1), count(n, 1);
    
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < i; ++j)
        {
            if (nums[j] < nums[i])
            {
                if (length[i] < length[j] + 1)
                    length[i] = length[j] + 1, count[i] = count[j];
                else if (length[i] == length[j] + 1) count[i] += count[j];
            }
        }
    }
    for (int i = 0; i < n; ++i)
    {
        if (length[i] > max_length)
            max_length = length[i], result = count[i];
        else if (length[i] == max_length) result += count[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findNumberOfLIS(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 5, 4, 7}, 2, trials);
    test({2, 2, 2, 2, 2}, 5, trials);
    return 0;
}


