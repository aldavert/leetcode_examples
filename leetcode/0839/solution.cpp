#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

int numSimilarGroups(std::vector<std::string> strs)
{
    const int n = static_cast<int>(strs.size());
    const int m = static_cast<int>(strs[0].size());
    std::vector<int> index(strs.size());
    auto find = [&](auto &&self, int idx) -> int
    {
        return (index[idx] == idx)?idx:index[idx] = self(self, index[idx]);
    };
    auto similar = [&](int u, int v) -> bool
    {
        for (int i = 0, number_of_differences = 0, index_different = 0; i < m; ++i)
        {
            if (strs[u][i] != strs[v][i])
            {
                if      (number_of_differences == 0)
                    index_different = i;
                else if (number_of_differences == 1)
                {
                    if ((strs[u][i] != strs[v][index_different])
                    ||  (strs[u][index_different] != strs[v][i]))
                        return false;
                }
                else return false;
                ++number_of_differences;
            }
        }
        return true;
    };
    for (int i = 0; i < n; ++i)
        index[i] = i;
    for (int i = 0; i < n; ++i)
    {
        for (int j = i + 1; j < n; ++j)
        {
            if (similar(i, j))
            {
                int u = find(find, i), v = find(find, j);
                if (v < u) index[u] = v;
                else index[v] = u;
            }
        }
    }
    std::bitset<301> unique;
    for (int i = 0; i < n; ++i)
        unique[index[i] = find(find, i)] = true;
    return static_cast<int>(unique.count());
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> strs, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numSimilarGroups(strs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"tars", "rats", "arts", "star"}, 2, trials);
    test({"omv", "ovm"}, 1, trials);
    return 0;
}


