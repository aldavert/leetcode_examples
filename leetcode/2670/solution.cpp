#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> distinctDifferenceArray(std::vector<int> nums)
{
    int left[51] = {}, right[51] = {}, count_left = 0, count_right = 0;
    std::vector<int> result;
    for (int value : nums)
        count_right += (++right[value]) == 1;
    for (int value : nums)
    {
        count_left += (++left[value]) == 1;
        count_right -= (--right[value]) == 0;
        result.push_back(count_left - count_right);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = distinctDifferenceArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, {-3, -1, 1, 3, 5}, trials);
    test({3, 2, 3, 4, 2}, {-2, -1, 0, 2, 3}, trials);
    return 0;
}

