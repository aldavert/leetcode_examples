#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int threeSumMulti(std::vector<int> arr, int target)
{
    auto mod = [](long v) -> int { return static_cast<int>(v % 1'000'000'007); };
    std::unordered_map<int, long> histogram;
    int result = 0;
    for (int a : arr) ++histogram[a];
    for (const auto& [i, x] : histogram)
    {
        for (const auto& [j, y] : histogram)
        {
            int k = target - i - j;
            if (auto search = histogram.find(k); search != histogram.end())
            {
                if ((i == j) && (j == k))
                    result = mod(result + x * (x - 1) * (x - 2) / 6);
                else if ((i == j) && (j != k))
                    result = mod(result + x * (x - 1) / 2 * search->second);
                else if ((i < j) && (j < k))
                    result = mod(result + x * y * search->second);
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = threeSumMulti(arr, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2, 2, 3, 3, 4, 4, 5, 5}, 8, 20, trials);
    test({1, 1, 2, 2, 2, 2}, 5, 12, trials);
    return 0;
}


