#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int winningPlayerCount(int n, std::vector<std::vector<int> > pick)
{
    constexpr int MAXCOLOR = 10;
    int result = 0;
    std::vector<std::vector<int> > counts(n, std::vector<int>(MAXCOLOR + 1));
    for (const auto &p : pick)
        ++counts[p[0]][p[1]];
    for (int i = 0; i < n; ++i)
    {
        int max_count = 0;
        for (const int freq : counts[i])
            max_count = std::max(max_count, freq);
        result += (max_count > i);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > pick,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = winningPlayerCount(n, pick);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{0, 0}, {1, 0}, {1, 0}, {2, 1}, {2, 1}, {2, 0}}, 2, trials);
    test(5, {{1, 1}, {1, 2}, {1, 3}, {1, 4}}, 0, trials);
    test(5, {{1, 1}, {2, 4}, {2, 4}, {2, 4}}, 1, trials);
    return 0;
}


