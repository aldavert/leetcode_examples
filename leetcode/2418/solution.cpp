#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> sortPeople(std::vector<std::string> names,
                                    std::vector<int> heights)
{
    const size_t n = names.size();
    std::vector<std::tuple<int, std::string> > merged(n);
    for (size_t i = 0; i < n; ++i)
        merged[i] = { heights[i], names[i] };
    std::sort(merged.begin(), merged.end(), std::greater<>());
    std::vector<std::string> result(n);
    for (size_t i = 0; i < n; ++i)
        result[i] = std::get<1>(merged[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> names,
          std::vector<int> heights,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sortPeople(names, heights);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"Mary", "John", "Emma"}, {180, 165, 170}, {"Mary", "Emma", "John"}, trials);
    test({"Alice", "Bob", "Bob"}, {155, 185, 150}, {"Bob", "Alice", "Bob"}, trials);
    return 0;
}


