#include "../common/common.hpp"
#include <stack>
#include <queue>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class MyStack
{
    std::queue<int> m_queue;
public:
    MyStack() {}
    void push(int x)
    {
        size_t n = m_queue.size();
        m_queue.push(x);
        for (size_t i = 0; i < n; ++i)
        {
            m_queue.push(m_queue.front());
            m_queue.pop();
        }
    }
    int pop()
    {
        if (m_queue.empty()) return -1;
        int value = m_queue.front();
        m_queue.pop();
        return value;
    }
    int top()
    {
        if (m_queue.empty()) return -1;
        return m_queue.front();
    }
    bool empty()
    {
        return m_queue.empty();
    }
};

// ############################################################################
// ############################################################################

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    for (bool test = false; int v : vec)
    {
        if (test) [[likely]] out << ", ";
        test = true;
        if (v == null) out << "out";
        else out << v;
    }
    out << '}';
    return out;
}

enum class OP { PUSH = 1, POP = 2, EMPTY = 4, TOP = 8 };

void test(std::vector<OP> operations,
          std::vector<int> input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    const size_t n = operations.size();
    std::vector<int> result(n, null);
    for (unsigned int t = 0; t < trials; ++t)
    {
        MyStack stack;
        for (size_t i = 0; i < n; ++i)
        {
            switch (operations[i])
            {
            case OP::PUSH:
                stack.push(input[i]);
                break;
            case OP::POP:
                result[i] = stack.pop();
                break;
            case OP::EMPTY:
                result[i] = stack.empty();
                break;
            case OP::TOP:
                result[i] = stack.top();
                break;
            default:
                std::cerr << "[ERROR] Unknown parameter.\n";
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::PUSH, OP::PUSH, OP::TOP, OP::POP, OP::EMPTY},
         {       1,        2,    null,    null,      null},
         {    null,     null,       2,       2,     false}, trials);
    return 0;
}


