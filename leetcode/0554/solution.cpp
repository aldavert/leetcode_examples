#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int leastBricks(std::vector<std::vector<int> > wall)
{
    std::unordered_map<long, size_t> histogram;
    for (const auto &row : wall)
        for (long p = 0, i = 0, n = static_cast<long>(row.size()) - 1; i < n; ++i)
            ++histogram[p += row[i]];
    size_t result = 0;
    for (auto [position, frequency] : histogram)
        result = std::max(result, frequency);
    return static_cast<int>(wall.size() - result);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > wall, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = leastBricks(wall);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 2, 1},
          {3, 1, 2},
          {1, 3, 2},
          {2, 4},
          {3, 1, 2},
          {1, 3, 1, 1}}, 2, trials);
    test({{1}, {1}, {1}}, 3, trials);
    return 0;
}


