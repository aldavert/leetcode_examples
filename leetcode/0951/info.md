# Flip Equivalent Binary Trees

For a binary tree **T**, we can define a **flip operation** as follows: choose any node, and swap the left and right child subtrees.

A binary tree **X** is flip equivalent to a binary tree **Y** if and only if we can make **X** equal to **Y** after some number of flip operations.

Given the roots of two binary trees `root1` and `root2`, return `true` if the two trees are flip equivalent or `false` otherwise.

#### Example 1:
> ```mermaid 
> graph TD;
> A1((1))-->B1((2))
> A1-->C1((3))
> B1-->D1((4))
> B1-->E1((5))
> C1-->F1((6))
> C1-->EMPTY1(( ))
> E1-->G1((7))
> E1-->H1((8))
> A2((1))-->B2((3))
> A2-->C2((2))
> B2-->EMPTY2(( ))
> B2-->D2((6))
> C2-->E2((4))
> C2-->F2((5))
> F2-->G2((8))
> F2-->H2((7))
> F1---EMPTY3(( ))
> F1---EMPTY4(( ))
> E2---EMPTY5(( ))
> E2---EMPTY6(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class EMPTY1,EMPTY2,EMPTY3,EMPTY4,EMPTY5,EMPTY6 empty;
> linkStyle 5,10,16,17,18,19 stroke-width:0px;
> ```
> *Input:* `root1 = [1, 2, 3, 4, 5, 6, null, null, null, 7, 8], root2 = [1, 3, 2, null, 6, 4, 5, null, null, null, null, 8, 7]`  
> *Output:* `true`  
> *Explanation:* We flipped at nodes with values `1`, `3`, and `5`.

#### Example 2:
> *Input:* `root1 = [], root2 = []`  
> *Output:* `true`

#### Example 3:
> *Input:* `root1 = [], root2 = [1]`  
> *Output:* `false`

#### Constraints:
- The number of nodes in each tree is in the range [0, 100].
- Each tree will have unique node values in the range [0, 99].


