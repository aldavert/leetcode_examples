#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

bool flipEquiv(TreeNode* root1, TreeNode* root2)
{
    if (root1 == nullptr) return root2 == nullptr;
    if (root2 == nullptr) return root1 == nullptr;
    if (root1->val != root2->val) return false;
    return (flipEquiv(root1->left, root2->left)
        &&  flipEquiv(root1->right, root2->right))
        || (flipEquiv(root1->left, root2->right)
        &&  flipEquiv(root1->right, root2->left));
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree1,
          std::vector<int> tree2,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root1 = vec2tree(tree1),
                 * root2 = vec2tree(tree2);
        result = flipEquiv(root1, root2);
        delete root1;
        delete root2;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6, null, null, null, 7, 8},
         {1, 3, 2, null, 6, 4, 5, null, null, null, null, 8, 7}, true, trials);
    test({}, {}, true, trials);
    test({}, {1}, false, trials);
    return 0;
}


