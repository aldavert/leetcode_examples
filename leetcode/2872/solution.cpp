#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxKDivisibleComponents(int n,
                            std::vector<std::vector<int> > edges,
                            std::vector<int> values,
                            int k)
{
    std::vector<std::vector<int> > graph(n);
    int result = 0;
    auto dfs = [&](auto &&self, int u, int prev) -> long
    {
        long tree_sum = values[u];
        for (const int v : graph[u])
            if (v != prev)
                tree_sum += self(self, v, u);
        result += (tree_sum % k == 0);
        return tree_sum;
    };
    
    for (const auto &edge : edges)
    {
        graph[edge[0]].push_back(edge[1]);
        graph[edge[1]].push_back(edge[0]);
    }
    dfs(dfs, 0, -1);
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<int> values,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxKDivisibleComponents(n, edges, values, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{0, 2}, {1, 2}, {1, 3}, {2, 4}}, {1, 8, 1, 4, 4}, 6, 2, trials);
    test(7, {{0, 1}, {0, 2}, {1, 3}, {1, 4}, {2, 5}, {2, 6}},
         {3, 0, 6, 1, 5, 2, 1}, 3, 3, trials);
    return 0;
}


