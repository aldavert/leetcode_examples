#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string largestPalindromic(std::string num)
{
    int count[10] = {};
    std::string mid = "";
    
    for (char c : num) ++count[c - '0'];
    for (int i = 9; ~i; --i)
    {
        if (count[i] % 2)
        {
            mid += static_cast<char>(i + '0');
            --count[i];
            break;
        }
    }
    std::string t = "";
    for (int i = 0; i < 10; ++i)
    {
        if (count[i])
        {
            count[i] >>= 1;
            while (count[i]--)
                t += static_cast<char>(i + '0');
        }
    }
    while (t.size() && (t.back() == '0'))
        t.pop_back();
    std::string result = t;
    std::reverse(result.begin(), result.end());
    result += mid + t;
    return (result == "")?"0":result;
}

// ############################################################################
// ############################################################################

void test(std::string num, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestPalindromic(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("444947137", "7449447", trials);
    test("00009", "9", trials);
    return 0;
}


