#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> countPairsOfConnectableServers(std::vector<std::vector<int> > edges,
                                                int signalSpeed)
{
    const int n = static_cast<int>(edges.size()) + 1;
    std::vector<int> result;
    std::vector<std::vector<std::pair<int, int> > > tree(n);
    auto connectablePairsRootedAt = [&](int idx) -> int
    {
        auto dfs = [&](auto &&self, int u, int prev, int dist) -> int
        {
            int count = 0;
            for (auto [v, w] : tree[u])
                if (v != prev)
                    count += self(self, v, u, dist + w);
            return count + (dist % signalSpeed == 0);
        };
        int pairs = 0;
        for (int count = 0; auto [v, w] : tree[idx])
        {
            int child_count = dfs(dfs, v, idx, w);
            pairs += count * child_count;
            count += child_count;
        }
        return pairs;
    };
    
    for (const auto &edge : edges)
    {
        tree[edge[0]].emplace_back(edge[1], edge[2]);
        tree[edge[1]].emplace_back(edge[0], edge[2]);
    }
    for (int i = 0; i < n; ++i)
        result.push_back(connectablePairsRootedAt(i));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges,
          int signalSpeed,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPairsOfConnectableServers(edges, signalSpeed);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 1}, {1, 2, 5}, {2, 3, 13}, {3, 4, 9}, {4, 5, 2}},
         1, {0, 4, 6, 6, 4, 0}, trials);
    test({{0, 6, 3}, {6, 5, 3}, {0, 3, 1}, {3, 2, 7}, {3, 1, 6}, {3, 4, 2}},
         3, {2, 0, 0, 0, 0, 0, 2}, trials);
    return 0;
}


