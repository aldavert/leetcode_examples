#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int minimumTime(std::vector<int> nums1, std::vector<int> nums2, int x)
{
    const int n = static_cast<int>(nums1.size());
    int sum1 = std::accumulate(nums1.begin(), nums1.end(), 0),
        sum2 = std::accumulate(nums2.begin(), nums2.end(), 0);
    std::vector<std::pair<int, int> > sorted_nums;
    std::vector<int> dp(n + 1);
    
    for (int i = 0; i < n; ++i)
        sorted_nums.emplace_back(nums2[i], nums1[i]);
    std::sort(sorted_nums.begin(), sorted_nums.end());
    for (int i = 1; i <= n; ++i)
    {
        auto [num2, num1] = sorted_nums[i - 1];
        for (int j = i; j > 0; --j)
            dp[j] = std::max( dp[j], dp[j - 1] + num2 * j + num1);
    }
    for (int op = 0; op <= n; ++op)
        if (sum1 + sum2 * op - dp[op] <= x)
            return op;
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int x,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumTime(nums1, nums2, x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, {1, 2, 3}, 4, 3, trials);
    test({1, 2, 3}, {3, 3, 3}, 4, -1, trials);
    return 0;
}


