# Add One Row to Tree

Given the `root` of a binary tree and two integers `val` and `depth`, add a row of nodes with value `val` at the given depth `depth`. Note that the root `node` is at depth `1`.

The adding rule is:
- Given the integer `depth`, for each not null tree node `cur` at the depth `depth - 1`, create two tree nodes with value val as cur's left subtree root and right subtree root.
- `cur`'s original left subtree should be the left subtree of the new left subtree root.
- `cur`'s original right subtree should be the right subtree of the new right subtree root.
- If `depth == 1` that means there is no depth `depth - 1` at all, then create a tree node with value `val` as the new root of the whole original tree, and the original tree is the new root's left subtree.

#### Example 1:
> ```mermaid
> flowchart LR;
> subgraph S1 [ ]
> direction TB;
> A1((4))---B1((2))
> B1---C1((3))
> B1---D1((1))
> A1---E1((6))
> E1---F1((5))
> E1---EMPTY1(( ))
> end
> subgraph S2 [ ]
> direction TB;
> A2((4))---G2((1))
> A2---H2((1))
> G2---B2((2))
> G2---EMPTY2(( ))
> EMPTY2---EMPTY21(( ))
> EMPTY2---EMPTY22(( ))
> H2---EMPTY3(( ))
> EMPTY3---EMPTY31(( ))
> EMPTY3---EMPTY32(( ))
> H2---E2((6))
> B2---C2((3))
> B2---D2((1))
> E2---F2((5))
> E2---EMPTY4(( ))
> end
> S1-->S2
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef new fill:#FC0,stroke:#555,stroke-width:2px;
> class S1,S2,EMPTY1,EMPTY2,EMPTY3,EMPTY4,EMPTY21,EMPTY22,EMPTY31,EMPTY32 empty;
> class G2,H2 new;
> linkStyle 5,9,10,11,12,13,14,19 stroke-width:0px;
> ```
> *Input:* `root = [4, 2, 6, 3, 1, 5], val = 1, depth = 2`  
> *Output:* `[4, 1, 1, 2, null, null, 6, 3, 1, 5]`

#### Example 2:
> ```mermaid
> flowchart LR;
> subgraph S1 [ ]
> direction TB;
> A1((4))---B1((2))
> A1---EMPTY1(( ))
> B1---C1((3))
> B1---D1((1))
> end
> subgraph S2 [ ]
> direction TB;
> A2((4))---B2((2))
> A2---EMPTY2(( ))
> B2---C2((1))
> B2---D2((1))
> C2---E2((3))
> C2---EMPTY3(( ))
> D2---EMPTY4(( ))
> D2---F2((1))
> EMPTY2---EMPTY21(( ))
> EMPTY2---EMPTY22(( ))
> EMPTY22---EMPTY221(( ))
> EMPTY22---EMPTY222(( ))
> end
> S1-->S2
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef new fill:#FC0,stroke:#555,stroke-width:2px;
> class S1,S2,EMPTY1,EMPTY2,EMPTY3,EMPTY4,EMPTY21,EMPTY22,EMPTY221,EMPTY222 empty;
> class C2,D2 new;
> linkStyle 1,5,9,10,12,13,14,15 stroke-width:0px;
> ```
> *Input:* `root = [4, 2, null, 3, 1], val = 1, depth = 3`  
> *Output:* `[4, 2, null, 1, 1, 3, null, null, 1]`  

#### Constraints:
- The number of nodes in the tree is in the range `[1, 10^4]`.
- The depth of the tree is in the range `[1, 10^4]`.
- `-100 <= Node.val <= 100`
- `-10^5 <= val <= 10^5`
- `1 <= depth <= the depth of tree + 1`


