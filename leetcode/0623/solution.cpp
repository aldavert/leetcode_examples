#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * addOneRow(TreeNode * root, int val, int depth)
{
    if (root == nullptr) return nullptr;
    TreeNode * current;
    if (depth == 1)
        current = new TreeNode(val, addOneRow(root, val, -1), nullptr);
    else if (depth == 2)
        current = new TreeNode(root->val,
                           new TreeNode(val, addOneRow(root->left, val, -1), nullptr),
                           new TreeNode(val, nullptr, addOneRow(root->right, val, -1)));
    else current = new TreeNode(root->val,
                                addOneRow(root->left , val, depth - 1),
                                addOneRow(root->right, val, depth - 1));
    return current;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          int val,
          int depth,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root_input = vec2tree(tree);
        TreeNode * root_result = addOneRow(root_input, val, depth);
        result = tree2vec(root_result);
        delete root_input;
        delete root_result;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 6, 3, 1, 5}, 1, 2, {4, 1, 1, 2, null, null, 6, 3, 1, 5}, trials);
    test({4, 2, 6, 3, 1, 5}, 1, 1, {1, 4, null, 2, 6, 3, 1, 5}, trials);
    test({4, 2, null, 3, 1}, 1, 3, {4, 2, null, 1, 1, 3, null, null, 1}, trials);
    return 0;
}


