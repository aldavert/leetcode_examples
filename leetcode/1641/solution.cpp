#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countVowelStrings(int n)
{
    int dp[5] = {1, 1, 1, 1, 1};
    for (int i = 2; i <= n; ++i)
    {
        int new_dp[5] = {0, 0, 0, 0, 0};
        for (int j = 0; j < 5; ++j)
            for (int k = 0; k <= j; ++k)
                new_dp[j] += dp[k];
        dp[0] = new_dp[0];
        dp[1] = new_dp[1];
        dp[2] = new_dp[2];
        dp[3] = new_dp[3];
        dp[4] = new_dp[4];
    }
    return dp[0] + dp[1] + dp[2] + dp[3] + dp[4];
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countVowelStrings(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 5, trials);
    test(2, 15, trials);
    test(33, 66045, trials);
    return 0;
}


