#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<bool> canMakePaliQueries(std::string s,
                                     std::vector<std::vector<int> > queries)
{
    std::vector<unsigned int> dp(s.length() + 1);
    std::vector<bool> result;
    
    for (size_t i = 1; i <= s.size(); ++i)
        dp[i] = dp[i - 1] ^ 1 << (s[i - 1] - 'a');
    for (const auto &query : queries)
    {
        const int odds = std::popcount(dp[query[1] + 1] ^ dp[query[0]]);
        result.push_back(odds / 2 <= query[2]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<std::vector<int> > queries,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = canMakePaliQueries(s, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcda", {{3, 3, 0}, {1, 2, 0}, {0, 3, 1}, {0, 3, 2}, {0, 4, 1}},
         {true, false, false, true, true}, trials);
    test("lyb", {{0, 1, 0}, {2, 2, 1}}, {false, true}, trials);
    return 0;
}


