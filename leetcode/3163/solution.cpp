#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string compressedString(std::string word)
{
    const int n = static_cast<int>(word.size());
    std::string result;
    for (int i = 0, j = 0; i < n; i = j)
    {
        int count = 0;
        for (; (j < n) && (word[j] == word[i]) && (count < 9); ++j, ++count);
        result += std::to_string(count) + word[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = compressedString(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcde", "1a1b1c1d1e", trials);
    test("aaaaaaaaaaaaaabb", "9a5a2b", trials);
    return 0;
}


