#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minInsertions(std::string s)
{
    int needed_right = 0, missing_left = 0, missing_right = 0;
    
    for (char c : s)
    {
        if (c == '(')
        {
            if (needed_right % 2 == 1)
            {
                ++missing_right;
                --needed_right;
            }
            needed_right += 2;
        }
        else if (--needed_right < 0)
        {
        ++missing_left;
        needed_right += 2;
        }
    }
    return needed_right + missing_left + missing_right;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minInsertions(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("(()))", 1, trials);
    test("())", 0, trials);
    test("))())(", 3, trials);
    return 0;
}


