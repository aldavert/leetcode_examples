#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 0
std::vector<int> productExceptSelf(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> left(n), right(n);
    left[0] = 1;
    for (int i = 1; i < n; ++i)
        left[i] = left[i - 1] * nums[i - 1];
    right[n - 1] = 1;
    for (int i = n - 2; i >= 0; --i)
        right[i] = right[i + 1] * nums[i + 1];
    for (int i = 0; i < n; ++i)
        nums[i] = left[i] * right[i];
    return nums;
}
#elif 1
std::vector<int> productExceptSelf(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> result(n);
    for (int i = 0, aux = 1; i < n; ++i)
    {
        result[i] = aux;
        aux *= nums[i];
    }
    for (int i = n - 1, aux = 1; i >= 0; --i)
    {
        result[i] *= aux;
        aux *= nums[i];
    }
    return result;
}
#else
std::vector<int> productExceptSelf(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> result(n, 1);
    int left = 1, right = 1;
    for (int i = 0; i < n; ++i)
    {
        result[i] *= left;
        left *= nums[i];
        result[n - 1 - i] *= right;
        right *= nums[n - 1 - i];
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = productExceptSelf(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, {24, 12, 8, 6}, trials);
    test({-1, 1, 0, -3, 3}, {0, 0, 9, 0, 0}, trials);
    return 0;
}


