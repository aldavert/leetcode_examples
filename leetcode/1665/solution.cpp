#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumEffort(std::vector<std::vector<int> > tasks)
{
    std::sort(tasks.begin(), tasks.end(), [](const auto& a, const auto& b) {
                                             return a[1] - a[0] < b[1] - b[0];});
    int result = 0;
    for (const auto& task : tasks)
        result = std::max(result + task[0], task[1]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > tasks, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumEffort(tasks);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 4}, {4, 8}}, 8, trials);
    test({{1, 3}, {2, 4}, {10, 11}, {10, 12}, {8, 9}}, 32, trials);
    test({{1, 7}, {2, 8}, {3, 9}, {4, 10}, {5, 11}, {6, 12}}, 27, trials);
    return 0;
}


