#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int longestOnes(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    int maximum_sum = 0;
    for (int begin = 0, end = 0, zeroes = 0; end < n; ++end)
    {
        if (nums[end] == 0) ++zeroes;
        while (zeroes > k)
            if (nums[begin++] == 0)
                --zeroes;
        maximum_sum = std::max(maximum_sum, end - begin + 1);
    }
    return maximum_sum;
}
#elif 0
int longestOnes(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    int begin = 0, end = 0, count = nums[0] == 0;
    int maximum_sum = 0;
    while (end + 1 < n)
    {
        while ((end + 1 < n) && (count < k))
        {
            if (nums[++end] == 0)
                ++count;
        }
        while ((end + 1 < n) && (nums[end + 1] == 1)) ++end;
        maximum_sum = std::max(maximum_sum, end - begin + 1);
        if (nums[begin++] == 0) --count;
    }
    return maximum_sum;
}
#else
int longestOnes(std::vector<int> nums, int k)
{
    // Count the number of ones groups.
    int n_ones = nums[0] == 1;
    int prev = nums[0];
    for (int v : nums)
    {
        if ((v != prev) && (v == 1))
            ++n_ones;
        prev = v;
    }
    // Generate a histogram with the number of 0's and 1's groups. They are interlaced.
    std::vector<int> histogram(n_ones * 2 + 1);
    int idx;
    if (nums[0] == 1)
        histogram[0] = histogram[prev = idx = 1] = 0;
    else histogram[prev = idx = 0] = 0;
    for (int v : nums)
    {
        if (v != prev) histogram[++idx] = 0;
        ++histogram[idx];
        prev = v;
    }
    if (prev == 1) histogram[idx + 1] = 0;
    
    // Search for the largest amount of consecutive ones.
    int maximum_sum = 0;
    for (int i = 0, m = 1; i < n_ones; ++i, m += 2)
    {
        int current_sum = histogram[m];
        int flips = k;
        int n = m + 2;
        for (int j = i + 1; j < n_ones; ++j, n += 2)
        {
            if (histogram[n - 1] > flips) break;
            current_sum += histogram[n - 1] + histogram[n];
            flips -= histogram[n - 1];
        }
        current_sum += std::min(flips, histogram[n - 1] + histogram[m - 1]);
        if (current_sum > maximum_sum)
            maximum_sum = current_sum;
    }
    
    return maximum_sum;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestOnes(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0}, 2, 6, trials);
    test({0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1}, 3, 10, trials);
    return 0;
}


