#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string removeKdigits(std::string num, int k)
{
    std::string res;
    for (char c : num)
    {
        for (; (k > 0) && (!res.empty()) && (res.back() > c); --k)
            res.pop_back();
        res.push_back(c);
    }
    int end = static_cast<int>(res.size()) - k;
    int begin = 0;
    for (; res[begin] == '0'; ++begin);
    return (begin >= end)?"0":res.substr(begin, end - begin);
}

// ############################################################################
// ############################################################################

void test(std::string num, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = removeKdigits(num, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1432219", 3, "1219", trials);
    test("10200", 1, "200", trials);
    test("10", 2, "0", trials);
    test("100", 1, "0", trials);
    return 0;
}


