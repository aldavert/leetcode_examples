#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

double maxAmount(std::string initialCurrency,
                 std::vector<std::vector<std::string> > pairs1,
                 std::vector<double> rates1,
                 std::vector<std::vector<std::string> > pairs2,
                 std::vector<double> rates2)
{
    std::unordered_map<std::string, double> dp;
    dp[initialCurrency] = 1.0;
    auto bellman = [&](const std::vector<std::vector<std::string> > &pairs,
                       const std::vector<double> &rates) -> void
    {
        for (size_t relax = 0; relax < pairs.size(); ++relax)
        {
            for (size_t i = 0; i < pairs.size(); ++i)
            {
                const std::string start = pairs[i][0];
                const std::string target = pairs[i][1];
                dp[target] = std::max(dp[target], dp[start] * rates[i]);
                dp[start] = std::max(dp[start], dp[target] / rates[i]);
            }
        }
    };
    bellman(pairs1, rates1);
    bellman(pairs2, rates2);
    return dp[initialCurrency];
}

// ############################################################################
// ############################################################################

void test(std::string initialCurrency,
          std::vector<std::vector<std::string> > pairs1,
          std::vector<double> rates1,
          std::vector<std::vector<std::string> > pairs2,
          std::vector<double> rates2,
          double solution,
          unsigned int trials = 1)
{
    double result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxAmount(initialCurrency, pairs1, rates1, pairs2, rates2);
    showResult(std::abs(solution - result) < 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("EUR", {{"EUR", "USD"}, {"USD", "JPY"}}, {2.0, 3.0},
         {{"JPY", "USD"}, {"USD", "CHF"}, {"CHF", "EUR"}}, {4.0, 5.0, 6.0}, 720, trials);
    test("NGN", {{"NGN", "EUR"}}, {9.0}, {{"NGN", "EUR"}}, {6.0}, 1.5, trials);
    test("USD", {{"USD", "EUR"}}, {1.0}, {{"EUR", "JPY"}}, {10.0}, 1, trials);
    return 0;
}


