#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long countFairPairs(std::vector<int> nums, int lower, int upper)
{
    auto count = [&](int sum) -> long
    {
        long result = 0;
        for (int i = 0, j = static_cast<int>(nums.size()) - 1; i < j; ++i)
        {
            while ((i < j) && (nums[i] + nums[j] > sum)) --j;
            result += j - i;
        }
        return result;
    };
    std::sort(nums.begin(), nums.end());
    return count(upper) - count(lower - 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int lower,
          int upper,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countFairPairs(nums, lower, upper);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 7, 4, 4, 5}, 3, 6, 6, trials);
    test({1, 7, 9, 2, 5}, 11, 11, 1, trials);
    return 0;
}


