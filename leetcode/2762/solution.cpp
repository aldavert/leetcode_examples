#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long continuousSubarrays(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    long result = 1;
    for (int r = 1, l = 0, left = nums[0] - 2, right = nums[0] + 2; r < n; ++r)
    {
        if ((left <= nums[r]) && (nums[r] <= right))
        {
            left = std::max(left, nums[r] - 2);
            right = std::min(right, nums[r] + 2);
        }
        else
        {
            left = nums[r] - 2;
            right = nums[r] + 2;
            for (l = r; (nums[r] - 2 <= nums[l]) && (nums[l] <= nums[r] + 2); --l)
            {
                left = std::max(left, nums[l] - 2);
                right = std::min(right, nums[l] + 2);
            }
            ++l;
        }
        result += r - l + 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = continuousSubarrays(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 2, 4}, 8, trials);
    test({1, 2, 3}, 6, trials);
    return 0;
}


