#include "../common/common.hpp"
#include <set>
#include <unordered_map>

struct CustomHash
{
    size_t operator()(std::tuple<int, int, int, int> const &t) const
    {                                              
        size_t seed = 0;
        seed ^= std::hash<int>()(std::get<0>(t)) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        seed ^= std::hash<int>()(std::get<1>(t)) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        seed ^= std::hash<int>()(std::get<2>(t)) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        seed ^= std::hash<int>()(std::get<3>(t)) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        return seed;
    }                                              
};

#if 0
std::vector<std::vector<int> > fourSum(std::vector<int> nums, int target)
{
    const int n = static_cast<int>(nums.size());
    std::unordered_map<int, std::vector<std::pair<int, int> > > sum_of_two;
    for (int i = 0; i < n; ++i)
        for (int j = i + 1; j < n; j++)
            sum_of_two[nums[i] + nums[j]].push_back({i, j});
    
    std::unordered_set<std::tuple<int, int, int, int>, CustomHash> sum_of_four;
    auto insert = [&](int A, int B, int C, int D)
    {
        if (A > B) std::swap(A, B);
        if (C > D) std::swap(C, D);
        
        if (A > C) std::swap(A, C);
        if (B > D) std::swap(B, D);
        if (B > C) sum_of_four.insert({A, C, B, D});
        else sum_of_four.insert({A, B, C, D});
    };
    for (auto [key, value] : sum_of_two)
        if (auto it = sum_of_two.find(target - key); it != sum_of_two.end())
            for (auto [a, b] : sum_of_two[key])
                for (auto [c, d] : it->second)
                    if ((a != c) && (b != c) && (a != d) && (b != d))
                        insert(nums[a], nums[b], nums[c], nums[d]);
    
    std::vector<std::vector<int> > result;
    for (const auto &element : sum_of_four)
        result.push_back({std::get<0>(element), std::get<1>(element), std::get<2>(element), std::get<3>(element)});
    return result;
}
#else
std::vector<std::vector<int> > fourSum(std::vector<int> nums, int target)
{
    const int n = static_cast<int>(nums.size());
    std::unordered_map<int, std::vector<std::pair<int, int> > > sum_of_two;
    for (int i = 0; i < n; ++i)
        for (int j = i + 1; j < n; j++)
            sum_of_two[nums[i] + nums[j]].push_back({i, j});
    
    std::set<std::vector<int> > sum_of_four;
    for (auto [key, value] : sum_of_two)
    {
        if (auto it = sum_of_two.find(target - key); it != sum_of_two.end())
        {
            for (auto [a, b] : sum_of_two[key])
            {
                for (auto [c, d] : it->second)
                {
                    if ((a != c) && (b != c) && (a != d) && (b != d))
                    {
                        std::vector<int> tmp = {nums[a], nums[b], nums[c], nums[d]};
                        std::sort(tmp.begin(), tmp.end());
                        sum_of_four.insert(tmp);
                    } 
                }
            }
        }
    }
    
    return {sum_of_four.begin(), sum_of_four.end()};
}
#endif

bool operator==(const std::vector<std::vector<int> > &left, const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_set<std::tuple<int, int, int, int>, CustomHash> lut;
    for (const auto &vec : left)
        lut.insert({vec[0], vec[1], vec[2], vec[3]});
    if (lut.size() != right.size()) return false;
    for (const auto &vec : right)
        if (lut.find({vec[0], vec[1], vec[2], vec[3]}) == lut.end()) return false;
    return true;
}

void test(std::vector<int> nums, int target, std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = fourSum(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, -1, 0, -2, 2}, 0, {{-2, -1, 1, 2}, {-2, 0, 0, 2}, {-1, 0, 0, 1}}, trials);
    test({2, 2, 2, 2, 2}, 8, {{2, 2, 2, 2}}, trials);
    return 0;
}


