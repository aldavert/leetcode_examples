#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <map>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > verticalTraversal(TreeNode * root)
{
    std::vector<std::vector<int> > result;
    std::map<int, std::multiset<std::pair<int, int> > > sorted_pairs;
    auto process = [&](auto &&self, TreeNode * node, int x, int y)
    {
        if (!node) return;
        sorted_pairs[x].emplace(y, node->val);
        self(self, node->left , x - 1, y + 1);
        self(self, node->right, x + 1, y + 1);
    };
    process(process, root, 0, 0);
    for (const auto& [_, pairs] : sorted_pairs)
    {
        std::vector<int> vals;
        for (const auto& pair : pairs)
            vals.push_back(pair.second);
        result.push_back(vals);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = verticalTraversal(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 9, 20, null, null, 15, 7}, {{9}, {3, 15}, {20}, {7}}, trials);
    test({1, 2, 3, 4, 5, 6, 7}, {{4}, {2}, {1, 5, 6}, {3}, {7}}, trials);
    test({1, 2, 3, 4, 6, 5, 7}, {{4}, {2}, {1, 5, 6}, {3}, {7}}, trials);
    return 0;
}


