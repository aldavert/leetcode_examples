#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxDepth(std::string s)
{
    int levels = 0, result = 0;
    for (char c : s)
    {
        if (c == '(')
        {
            ++levels;
            result = std::max(result, levels);
        }
        else if (c == ')') --levels;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxDepth(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("(1+(2*3)+((8)/4))+1", 3, trials);
    test("(1)+((2))+(((3)))", 3, trials);
    return 0;
}


