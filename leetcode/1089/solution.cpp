#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

void duplicateZeros(std::vector<int> &arr)
{
    std::queue<int> buffer;
    for (int &a : arr)
    {
        buffer.push(a);
        if (a == 0) buffer.push(0);
        a = buffer.front();
        buffer.pop();
    }
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = arr;
        duplicateZeros(result);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 2, 3, 0, 4, 5, 0}, {1, 0, 0, 2, 3, 0, 0, 4}, trials);
    test({1, 2, 3}, {1, 2, 3}, trials);
    return 0;
}


