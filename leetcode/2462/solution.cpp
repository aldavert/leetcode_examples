#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

long long totalCost(std::vector<int> costs, int k, int candidates)
{
    std::priority_queue<int, std::vector<int>, std::greater<> > heap_left, heap_right;
    int i = 0, j = static_cast<int>(costs.size()) - 1;
    long result = 0;
    
    for (int hired = 0; hired < k; ++hired)
    {
        while ((static_cast<int>(heap_left.size()) < candidates) && (i <= j))
            heap_left.push(costs[i++]);
        while ((static_cast<int>(heap_right.size()) < candidates) && (i <= j))
            heap_right.push(costs[j--]);
        if (heap_left.empty())
            result += heap_right.top(), heap_right.pop();
        else if (heap_right.empty())
            result += heap_left.top(), heap_left.pop();
        else if (heap_left.top() <= heap_right.top())
            result += heap_left.top(), heap_left.pop();
        else
            result += heap_right.top(), heap_right.pop();
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> costs,
          int k,
          int candidates,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = totalCost(costs, k, candidates);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({17, 12, 10, 2, 7, 2, 11, 20, 8}, 3, 4, 11, trials);
    test({1, 2, 4, 1}, 3, 3, 4, trials);
    return 0;
}


