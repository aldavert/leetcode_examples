#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int smallestEqual(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    const int N = n / 10;
    int i = 0;
    for (int k = 0; k < N; ++k)
        for (int m = 0; m < 10; ++m, ++i)
            if (m == nums[i]) return i;
    for (int m = 0; i < n; ++m, ++i)
        if (m == nums[i]) return i;
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestEqual(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 2}, 0, trials);
    test({4, 3, 2, 1}, 2, trials);
    test({1, 2, 3, 4, 5, 6, 7, 8, 9, 0}, -1, trials);
    test({7, 8, 3, 5, 2, 6, 3, 1, 1, 4, 5, 4, 8, 7, 2, 0, 9, 9, 0, 5, 7, 1, 6},
         21, trials);
    return 0;
}


