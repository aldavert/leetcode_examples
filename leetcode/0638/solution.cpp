#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int shoppingOffers(std::vector<int> price,
                   std::vector<std::vector<int> > special,
                   std::vector<int> needs)
{
    auto dfs = [&](void) -> int
    {
        std::unordered_map<int, int> lut;
        auto isValid = [&](size_t i) -> bool
        {
            for (size_t k = 0; k < needs.size(); ++k)
                if (needs[k] < special[i][k])
                    return false;
            return true;
        };
        auto signature = [&](void) -> int
        {
            int id = 0;
            for (size_t i = 0; i < needs.size(); ++i)
                id = (id << 4) + needs[i];
            return id;
        };
        auto inner = [&](auto &&self) -> int
        {
            int needs_id = signature();
            if (const auto search = lut.find(needs_id);
                (search != lut.end()) && (search->second != 0))
                return search->second;
            int result = 0;
            for (size_t i = 0; i < price.size(); ++i)
                result += price[i] * needs[i];
            for (size_t i = 0; i < special.size(); ++i)
            {
                if (isValid(i))
                {
                    for (size_t j = 0; j < needs.size(); ++j)
                        needs[j] -= special[i][j];
                    result = std::min(result, special[i].back() + self(self));
                    for (size_t j = 0; j < needs.size(); ++j)
                        needs[j] += special[i][j];
                }
            }
            return lut[needs_id] = result;
        };
        return inner(inner);
    };
    return dfs();
}

// ############################################################################
// ############################################################################

void test(std::vector<int> price,
          std::vector<std::vector<int> > special,
          std::vector<int> needs,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = shoppingOffers(price, special, needs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 5}, {{3, 0, 5}, {1, 2, 10}}, {3, 2}, 14, trials);
    test({2, 3, 4}, {{1, 1, 0, 4}, {2, 2, 1, 9}}, {1, 2, 1}, 11, trials);
    test({4, 9, 6},
         {{0, 1, 1, 5}, {2, 3, 2, 1}, {1, 2, 1, 7}, {1, 0, 0, 2}}, {4, 5, 3}, 10, trials);
    return 0;
}


