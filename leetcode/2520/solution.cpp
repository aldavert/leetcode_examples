#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countDigits(int num)
{
    int result = 0;
    for (int work = num; work; work /= 10)
        result += num % (work % 10) == 0;
    return result;
}

// ############################################################################
// ############################################################################

void test(int num, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countDigits(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, 1, trials);
    test(121, 2, trials);
    test(1248, 4, trials);
    return 0;
}


