#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findJudge(int n, std::vector<std::vector<int> > trust)
{
    std::vector<std::pair<bool, int> > people(n, {true, 0});
    for (const auto &t : trust)
    {
        people[t[0] - 1].first = false;
        ++people[t[1] - 1].second;
    }
    int result = -1;
    for (int i = 0; i < n; ++i)
    {
        auto [can_be, frequency] = people[i];
        if (can_be)
        {
            if ((result == -1) && (frequency == n - 1))
                result = i + 1;
            else return -1;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > trust,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findJudge(n, trust);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {{1, 2}}, 2, trials);
    test(3, {{1, 3}, {2, 3}}, 3, trials);
    test(3, {{1, 3}, {2, 3}, {3, 1}}, -1, trials);
    return 0;
}


