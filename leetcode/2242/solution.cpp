#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumScore(std::vector<int> scores,
                 std::vector<std::vector<int> > edges)
{
    std::vector<std::set<std::pair<int, int> > > graph(scores.size());
    int result = -1;
    
    for (const auto &edge : edges)
    {
        int u = edge[0], v = edge[1];
        graph[u].emplace(scores[v], v);
        graph[v].emplace(scores[u], u);
        if (graph[u].size() > 3)
            graph[u].erase(graph[u].begin());
        if (graph[v].size() > 3)
            graph[v].erase(graph[v].begin());
    }
    for (const auto &edge : edges)
    {
        int u = edge[0], v = edge[1];
        for (const auto& [score_a, a] : graph[u])
            for (const auto& [score_b, b] : graph[v])
                if ((a != b) && (a != v) && (b != u))
                    result = std::max(result,
                            score_a + scores[u] + scores[v] + score_b);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> scores,
          std::vector<std::vector<int> > edges,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumScore(scores, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 2, 9, 8, 4},
         {{0, 1}, {1, 2}, {2, 3}, {0, 2}, {1, 3}, {2, 4}}, 24, trials);
    test({9, 20, 6, 4, 11, 12}, {{0, 3}, {5, 3}, {2, 4}, {1, 3}}, -1, trials);
    return 0;
}


