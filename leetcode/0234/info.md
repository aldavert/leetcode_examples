# Palindrome Linked List

Given the `head` of a singly linked list, return `true` *if it is a palindrome or* `false` *otherwise*.

#### Example 1:
> ```mermaid 
> graph LR;
> A((1))-->B((2))-->C((2))-->D((1))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `head = [1, 2, 2, 1]`  
> *Output:* `true`

#### Example 2:
> ```mermaid 
> graph LR;
> A((1))-->B((2))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `head = [1, 2]`  
> *Output:* `false`

#### Constraints:
- The number of nodes in the list is in the range `[1, 10^5]`.
- `0 <= Node.val <= 9`

**Follow up:** Could you do it in `O(n)` time and `O(1)` space?


