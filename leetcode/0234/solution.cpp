#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

bool isPalindrome(ListNode * head)
{
    auto reverse = [](ListNode * node) -> ListNode *
    {
        ListNode * previous = nullptr;
        while (node)
        {
            ListNode * next = node->next;
            node->next = previous;
            previous = node;
            node = next;
        }
        return previous;
    };
    ListNode * slow = head, * fast = head;
    while (fast && fast->next)
    {
        slow = slow->next;
        fast = fast->next->next;
    }
    if (fast) slow = slow->next;
    slow = reverse(slow);
    for (ListNode * begin = head, * end = slow;
            end;
            begin = begin->next, end = end->next)
    {
        if (begin->val != end->val)
        {
            reverse(slow);
            return false;
        }
    }
    reverse(slow);
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> input,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(input);
        result = isPalindrome(head);
        delete head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 1}, true, trials);
    test({1, 2}, false, trials);
    return 0;
}


