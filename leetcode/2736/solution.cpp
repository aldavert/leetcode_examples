#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> maximumSumQueries(std::vector<int> nums1,
                                   std::vector<int> nums2,
                                   std::vector<std::vector<int> > queries)
{
    struct Pair
    {
        int x = -1;
        int y = -1;
        bool operator<(const Pair &other) const { return x > other.x; }
    };
    struct IndexAndQuery
    {
        int index;
        int min_x;
        int min_y;
        bool operator<(const IndexAndQuery &other) const { return min_x > other.min_x; }
    };
    std::vector<Pair> pairs;
    for (int i = 0, n = static_cast<int>(nums1.size()); i < n; ++i)
        pairs.push_back({nums1[i], nums2[i]});
    std::sort(pairs.begin(), pairs.end());
    std::vector<IndexAndQuery> index_and_queries;
    for (int i = 0, n = static_cast<int>(queries.size()); i < n; ++i)
        index_and_queries.push_back({i, queries[i][0], queries[i][1]});
    std::sort(index_and_queries.begin(), index_and_queries.end());
    std::vector<int> result(queries.size());
    std::vector<std::pair<int, int> > stack;
    
    for (int i = 0; auto [index, min_x, min_y] : index_and_queries)
    {
        for (int n = static_cast<int>(pairs.size()); (i < n) && (pairs[i].x >= min_x);)
        {
            auto [x, y] = pairs[i++];
            while (!stack.empty() && (x + y >= stack.back().second))
                stack.pop_back();
            if (stack.empty() || (y > stack.back().first))
                stack.push_back({y, x + y});
        }
        auto it = std::lower_bound(stack.begin(), stack.end(),
                std::pair<int, int>{min_y, std::numeric_limits<int>::lowest()});
        result[index] = (it == stack.end())?-1:it->second;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumSumQueries(nums1, nums2, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 3, 1, 2}, {2, 4, 9, 5}, {{4, 1}, {1, 3}, {2, 5}}, {6, 10, 7}, trials);
    test({3, 2, 5}, {2, 3, 4}, {{4, 4}, {3, 2}, {1, 1}}, {9, 9, 9}, trials);
    test({2, 1}, {2, 3}, {{3, 3}}, {-1}, trials);
    return 0;
}


