#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int regionsBySlashes(std::vector<std::string> grid)
{
    const int n = static_cast<int>(grid.size());
    const int m = 4 * n * n;
    std::vector<int> size(m, 1), id(m);
    auto coord2id = [&](int i, int j, int k) -> int
    {
        return (i * n + j) * 4 + k;
    };
    auto find = [&](auto &&self, int index) -> int
    {
        return (id[index] == index)?index:id[index] = self(self, id[index]);
    };
    auto unionBySize = [&](int x, int y) -> void
    {
        x = find(find, x);
        y = find(find, y);
        if (x == y) return;
        if (size[x] < size[y])
        {
            id[x] = y;
            size[y] += size[x];
        }
        else
        {
            id[y] = x;
            size[x] += size[y];
        }
    };
    
    for (int i = 0; i < m; ++i) id[i] = i;
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            if (i > 0)
                unionBySize(coord2id(i - 1, j, 2), coord2id(i, j, 0));
            if (j > 0)
                unionBySize(coord2id(i, j - 1, 1), coord2id(i, j, 3));
            if (grid[i][j] == ' ')
            {
                unionBySize(coord2id(i, j, 0), coord2id(i, j, 1));
                unionBySize(coord2id(i, j, 2), coord2id(i, j, 3));
                unionBySize(coord2id(i, j, 2), coord2id(i, j, 0));
            }
            else if (grid[i][j] == '/')
            {
                unionBySize(coord2id(i, j, 1), coord2id(i, j, 2));
                unionBySize(coord2id(i, j, 0), coord2id(i, j, 3));
            }
            else
            {
                unionBySize(coord2id(i, j, 0), coord2id(i, j, 1));
                unionBySize(coord2id(i, j, 2), coord2id(i, j, 3));
            }
        }
    }
    int result = 0;
    for (int i = 0; i < m; ++i) result += (id[i] == i);
    return result;
}
#else
int regionsBySlashes(std::vector<std::string> grid)
{
    const int n = static_cast<int>(grid.size());
    std::vector<std::vector<int> > unscaled_grid(3 * n + 2, std::vector<int>(3 * n + 2));
    
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            if (grid[i][j] == '/')
            {
                unscaled_grid[3 * i + 1][3 * j + 3] = 1;
                unscaled_grid[3 * i + 2][3 * j + 2] = 1;
                unscaled_grid[3 * i + 3][3 * j + 1] = 1;
            }
            else if (grid[i][j] == '\\')
            {
                unscaled_grid[3 * i + 1][3 * j + 1] = 1;
                unscaled_grid[3 * i + 2][3 * j + 2] = 1;
                unscaled_grid[3 * i + 3][3 * j + 3] = 1;
            }
        }
    }
    for (int j = 0; j < 3 * n + 2; ++j)
        unscaled_grid[0][j] = unscaled_grid[3 * n + 1][j] = 1,
        unscaled_grid[j][0] = unscaled_grid[j][3 * n + 1] = 1;
    int result = 0;
    for (int i = 1; i <= 3 * n; ++i)
    {
        for (int j = 1; j <= 3 * n; ++j)
        {
            if (unscaled_grid[i][j] == 0)
            {
                std::queue<std::tuple<int, int> > active;
                active.push({i, j});
                while (!active.empty())
                {
                    auto [x, y] = active.front();
                    active.pop();
                    if (unscaled_grid[x][y] != 0) continue;
                    unscaled_grid[x][y] = 2;
                    active.push({x + 1, y});
                    active.push({x - 1, y});
                    active.push({x, y + 1});
                    active.push({x, y - 1});
                }
                ++result;
            }
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = regionsBySlashes(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({" /", "/ "}, 2, trials);
    test({" /", "  "}, 1, trials);
    test({"/\\", "\\/"}, 5, trials);
    return 0;
}


