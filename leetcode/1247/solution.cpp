#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumSwap(std::string s1, std::string s2)
{
    int xy = 0, yx = 0;
    for (size_t i = 0, n = s1.size(); i < n; ++i)
    {
        if (s1[i] == s2[i]) continue;
        if (s1[i] == 'x') ++xy;
        else ++yx;
    }
    if ((xy + yx) % 2 == 1) return -1;
    return xy / 2 + yx / 2 + (xy % 2 == 1 ? 2 : 0);
}

// ############################################################################
// ############################################################################

void test(std::string s1, std::string s2, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumSwap(s1, s2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("xx", "yy", 1, trials);
    test("xy", "yx", 2, trials);
    return 0;
}


