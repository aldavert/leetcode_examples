#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int stoneGameII(std::vector<int> piles)
{
    const int n = static_cast<int>(piles.size());
    std::vector<std::vector<int> > dp(n, std::vector<int>(n));
    std::vector<int> suffix_sum = piles;
    for (int i = n - 2; i >= 0; --i) suffix_sum[i] += suffix_sum[i + 1];
    auto process = [&](auto &&self, int i, int m) -> int
    {
        if (i + 2 * m >= n) return suffix_sum[i];
        if (dp[i][m]) return dp[i][m];
        int opponent = suffix_sum[i];
        for (int x = 1; x <= 2 * m; ++x)
            opponent = std::min(opponent, self(self, i + x, std::max(m, x)));
        return dp[i][m] = suffix_sum[i] - opponent;
    };
    return process(process, 0, 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> piles, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = stoneGameII(piles);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 7, 9, 4, 4}, 10, trials);
    test({1, 2, 3, 4, 5, 100}, 104, trials);
    return 0;
}


