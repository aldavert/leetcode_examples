#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

bool isSameTree(TreeNode * p, TreeNode * q)
{
    if (!p && !q)
        return true;
    if ((!p && q) || (p && !q))
        return false;
    if (p->val != q->val)
        return false;
    return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree_p,
          std::vector<int> tree_q,
          bool solution,
          unsigned int trials = 1)
{
    bool result = false;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root_p = vec2tree(tree_p);
        TreeNode * root_q = vec2tree(tree_q);
        result = isSameTree(root_p, root_q);
        delete root_p;
        delete root_q;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, {1, 2, 3},  true, trials);
    test({1, 2}, {1, null, 2}, false, trials);
    test({1, 2, 1}, {1, 1, 2}, false, trials);
    return 0;
}


