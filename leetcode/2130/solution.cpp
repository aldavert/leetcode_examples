#include "../common/common.hpp"
#include "../common/list.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int pairSum(ListNode * head)
{
    auto reverse = [](ListNode * ptr)
    {
        ListNode * prev = nullptr;
        while (ptr)
        {
            ListNode * next = ptr->next;
            ptr->next = std::exchange(prev, ptr);
            ptr = next;
        }
        return prev;
    };
    ListNode * middle = head;
    for (ListNode * fast = head; fast; middle = middle->next, fast = fast->next->next);
    middle = reverse(middle);
    int result = 0;
    for (ListNode * r = middle, * l = head; r; r = r->next, l = l->next)
        result = std::max(result, l->val + r->val);
    middle = reverse(middle);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> vec, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(vec);
        result = pairSum(head);
        delete head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 2, 1}, 6, trials);
    test({4, 2, 2, 3}, 7, trials);
    test({1, 100000}, 100001, trials);
    return 0;
}


