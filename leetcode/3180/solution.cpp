#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

int maxTotalReward(std::vector<int> rewardValues)
{
    constexpr int POSSIBLEREWARDS = 100'000;
    std::bitset<POSSIBLEREWARDS> dp;
    dp[0] = true;
    std::sort(rewardValues.begin(), rewardValues.end());
    for (const int num : rewardValues)
    {
        std::bitset<POSSIBLEREWARDS> new_bits = dp;
        new_bits <<= POSSIBLEREWARDS - num;
        new_bits >>= POSSIBLEREWARDS - num;
        dp |= new_bits << num;
    }
    for (int result = POSSIBLEREWARDS - 1; result >= 0; --result)
        if (dp[result])
        return result;
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> rewardValues, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxTotalReward(rewardValues);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 3, 3}, 4, trials);
    test({1, 6, 4, 3, 2}, 11, trials);
    return 0;
}


