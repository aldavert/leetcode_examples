#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canBeEqual(std::string s1, std::string s2)
{
    return (((s1[0] == s2[0]) && (s1[2] == s2[2]))
        ||  ((s1[0] == s2[2]) && (s1[2] == s2[0])))
        && (((s1[1] == s2[1]) && (s1[3] == s2[3]))
        ||  ((s1[1] == s2[3]) && (s1[3] == s2[1])));
}

// ############################################################################
// ############################################################################

void test(std::string s1, std::string s2, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canBeEqual(s1, s2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcd", "cdab",  true, trials);
    test("abcd", "dacb", false, trials);
    return 0;
}


