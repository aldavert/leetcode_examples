#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::string minInteger(std::string num, int k)
{
    struct FenwickTree
    {
    public:
        FenwickTree(int n) : m_sums(n + 1), m_n(n) {}
        void update(int i, int delta)
        {
            while (i <= m_n)
                m_sums[i] += delta,
                i += i & -i;
        }
        int get(int i)
        {
            int sum = 0;
            while (i > 0)
                sum += m_sums[i],
                i -= i & -i;
            return sum;
        }
    private:
        std::vector<int> m_sums;
        int m_n;
    };
    
    const int n = static_cast<int>(num.size());
    std::string result;
    FenwickTree tree(n);
    std::vector<bool> used(n);
    std::queue<int> num_to_indices[10];

    for (int i = 0; i < n; ++i)
        num_to_indices[num[i] - '0'].push(i);
    while ((k > 0) && (static_cast<int>(result.size()) < n))
    {
        for (int d = 0; d < 10; ++d)
        {
            if (num_to_indices[d].empty())
                continue;
            int i = num_to_indices[d].front();
            int cost = i - tree.get(i);
            if (cost > k)
                continue;
            k -= cost;
            result += static_cast<char>('0' + d);
            used[i] = true;
            tree.update(i + 1, 1);
            num_to_indices[d].pop();
            break;
        }
    }
    for (int i = 0; i < n; ++i)
        if (!used[i])
            result += num[i];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string num, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minInteger(num, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("4321", 4, "1342", trials);
    test("100", 1, "010", trials);
    test("36789", 1'000, "36789", trials);
    return 0;
}


