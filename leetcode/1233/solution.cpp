#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> removeSubfolders(std::vector<std::string> folder)
{
    std::vector<std::string> result;
    std::string prev;
    
    std::sort(folder.begin(), folder.end());
    for (const auto &f : folder)
    {
        if (!prev.empty() && (f.find(prev) == 0) && (f[prev.length()] == '/'))
            continue;
        result.push_back(f);
        prev = f;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> folder,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = removeSubfolders(folder);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"/a", "/a/b", "/c/d", "/c/d/e", "/c/f"}, {"/a", "/c/d", "/c/f"}, trials);
    test({"/a", "/a/b/c", "/a/b/d"}, {"/a"}, trials);
    test({"/a/b/c", "/a/b/ca", "/a/b/d"}, {"/a/b/c", "/a/b/ca", "/a/b/d"}, trials);
    return 0;
}


