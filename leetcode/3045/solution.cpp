#include "../common/common.hpp"
#include <map>
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
long long countPrefixSuffixPairs(std::vector<std::string> words)
{
    std::map<std::pair<int, int>, int> mem;
    long long cnt = 0;
    int MOD = 1'000'000'007;
    for(const std::string &s: words)
    {
        int n = static_cast<int>(s.size());
        long long fh = 0, bh = 0;
        for (int i = 0 ; i < n ; i++)
        {
            ((fh *= 31) += s[i] - 96) %= MOD;
            ((bh *= 31) += s[n - i - 1] - 96) %= MOD;
            if (mem.contains({fh, bh}))
                cnt += mem[{fh, bh}];
        }
        mem[{fh, bh}] += 1;
    }
    return cnt;
}
#else
long long countPrefixSuffixPairs(std::vector<std::string> words)
{
    class Node
    {
    public:
        std::unordered_map<int, Node *> children;
        int cnt;
        Node(void) : cnt(0) {}
        ~Node(void)
        {
            for (auto &[key, next] : children)
                delete next;
        }
    };
    long long result = 0;
    Node * trie = new Node();
    for (const std::string &s : words)
    {
        Node * node = trie;
        int m = static_cast<int>(s.size());
        for (int i = 0; i < m; ++i)
        {
            int p = s[i] * 32 + s[m - i - 1];
            if (node->children.find(p) == node->children.end())
            {
                node->children[p] = new Node();
            }
            node = node->children[p];
            result += node->cnt;
        }
        ++node->cnt;
    }
    delete trie;
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPrefixSuffixPairs(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"a", "aba", "ababa", "aa"}, 4, trials);
    test({"pa", "papa", "ma", "mama"}, 2, trials);
    test({"abab", "ab"}, 0, trials);
    return 0;
}


