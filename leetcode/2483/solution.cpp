#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int bestClosingTime(std::string customers)
{
    const int n = static_cast<int>(customers.size());
    std::vector<int> customer_in(n + 1);
    for (int i = 0; i < n; ++i)
        customer_in[i + 1] = customer_in[i] + (customers[i] == 'Y');
    int min_penalty = n - customer_in.back(), result = n;
    for (int i = n - 1; i >= 0; --i)
    {
        int lost_customers = customer_in.back() - customer_in[i],
            empty_shop = i - customer_in[i];
        int penalty = lost_customers + empty_shop;
        if (penalty <= min_penalty)
        {
            min_penalty = penalty;
            result = i;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string customers, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = bestClosingTime(customers);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("YYNY", 2, trials);
    test("NNNNN", 0, trials);
    test("YYYY", 4, trials);
    return 0;
}


