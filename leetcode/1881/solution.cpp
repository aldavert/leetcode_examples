#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string maxValue(std::string n, int x)
{
    bool is_negative = (n[0] == '-');
    for (int i = 0, m = static_cast<int>(n.size()); i < m; ++i)
        if ((!is_negative && (n[i] - '0' < x))
        ||  ( is_negative && (n[i] - '0' > x)))
            return n.substr(0, i) + static_cast<char>('0' + x) + n.substr(i);
    return n + static_cast<char>('0' + x);
}

// ############################################################################
// ############################################################################

void test(std::string n, int x, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxValue(n, x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("99", 9, "999", trials);
    test("-13", 2, "-123", trials);
    return 0;
}


