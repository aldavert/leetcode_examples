#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numDifferentIntegers(std::string word)
{
    const int n = static_cast<int>(word.size());
    std::unordered_set<std::string> unique;
    int begin = -1;
    for (int i = 0; i < n; ++i)
    {
        if (std::isdigit(word[i]) && ((begin == -1) || (word[begin] == '0')))
            begin = i;
        if (std::isalpha(word[i]) && (begin != -1))
        {
            unique.insert(word.substr(begin, i - begin));
            begin = -1;
        }
    }
    if (begin != -1)
        unique.insert(word.substr(begin, n - begin));
    return static_cast<int>(unique.size());
}

// ############################################################################
// ############################################################################

void test(std::string word, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numDifferentIntegers(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("a123bc34d8ef34", 3, trials);
    test("leet1234code234", 2, trials);
    test("a1b01c001", 1, trials);
    return 0;
}


