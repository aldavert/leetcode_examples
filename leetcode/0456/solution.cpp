#include "../common/common.hpp"
#include <stack>
#include <limits>

// ############################################################################
// ############################################################################

bool find132pattern(std::vector<int> nums)
{
    std::stack<int> stack;
    stack.push(std::numeric_limits<int>::max());
    int last_value = std::numeric_limits<int>::lowest();
    for (int i = static_cast<int>(nums.size()) - 1; i >= 0; --i)
    {
        if (nums[i] < last_value)
            return true;
        for (; nums[i] > stack.top(); stack.pop())
            last_value = stack.top();
        stack.push(nums[i]);
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = find132pattern(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({ 1, 2, 3, 4}, false, trials);
    test({ 3, 1, 4, 2},  true, trials);
    test({ 3, 2, 4, 2}, false, trials);
    test({ 3, 3, 4, 2}, false, trials);
    test({-1, 3, 2, 0},  true, trials);
    test({ 1, 0, 1, -4, -3}, false, trials);
    test({ 3, 5, 0, 3, 4}, true, trials);
    return 0;
}


