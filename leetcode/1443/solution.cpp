#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int minTime(int n,
            std::vector<std::vector<int> > edges,
            std::vector<bool> hasApple)
{
    std::unordered_map<int, std::vector<int> > neighbors;
    for (const auto &edge : edges)
    {
        neighbors[edge[0]].push_back(edge[1]);
        neighbors[edge[1]].push_back(edge[0]);
    }
    std::vector<bool> available(n, true);
    auto traverse = [&](auto &&self, int node) -> int
    {
        available[node] = false;
        int result = 0;
        for (int neighbor : neighbors[node])
            if (available[neighbor])
                result += self(self, neighbor);
        return result
             + 2 * (node != 0) * (((result == 0) && hasApple[node]) || (result != 0));
    };
    return traverse(traverse, 0);
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<bool> hasApple,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minTime(n, edges, hasApple);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, {{0, 1}, {0, 2}, {1, 4}, {1, 5}, {2, 3}, {2, 6}},
            {false, false, true, false, true, true, false}, 8, trials);
    test(7, {{0, 1}, {0, 2}, {1, 4}, {1, 5}, {2, 3}, {2, 6}},
            {false, false, true, false, false, true, false}, 6, trials);
    test(7, {{0, 1}, {0, 2}, {1, 4}, {1, 5}, {2, 3}, {2, 6}},
            {false, false, false, false, false, false, false}, 0, trials);
    test(7, {{0, 1}, {0, 2}, {1, 4}, {1, 5}, {2, 3}, {2, 6}},
            {true, true, true, true, true, true, true}, 12, trials);
    test(7, {{0, 1}, {0, 2}, {1, 4}, {1, 5}, {2, 3}, {2, 6}},
            {false, false, false, true, true, true, true}, 12, trials);
    return 0;
}


