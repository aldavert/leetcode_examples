#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minSideJumps(std::vector<int> obstacles)
{
    constexpr int INF = 1'000'000;
    std::vector<int> dp{INF, 1, 0, 1};
    
    for (int obstacle : obstacles)
    {
        if (obstacle > 0)
        dp[obstacle] = INF;
        for (int i = 1; i <= 3; ++i)
        {
            if (i == obstacle) continue;
            for (int j = 1; j <= 3; ++j)
                dp[i] = std::min(dp[i], dp[j] + (i != j));
        }
    }
    return *std::min_element(dp.begin(), dp.end());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> obstacles, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSideJumps(obstacles);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 2, 3, 0}, 2, trials);
    test({0, 1, 1, 3, 3, 0}, 0, trials);
    test({0, 2, 1, 0, 3, 0}, 2, trials);
    return 0;
}


