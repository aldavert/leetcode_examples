#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

int minOperationsToFlip(std::string expression)
{
    struct Information
    {
        int flip_left = 0;
        int flip_right = 0;
        int operation = 0;
        Information opAnd(const Information &other) const
        {
            return {std::min(flip_left, other.flip_left),
                    std::min(flip_right + other.flip_right,
                             std::min(flip_right, other.flip_right) + 1),
                    0};
        }
        Information opOr(const Information &other) const
        {
            return {std::min(flip_left + other.flip_left,
                             std::min(flip_left, other.flip_left) + 1),
                    std::min(flip_right, other.flip_right),
                    0};
        }
        Information opClear(void) const { return {flip_left, flip_right, 0}; }
    };
    std::stack<Information> s;
    s.push({});
    for (char e : expression)
    {
        if (e == '(')
            s.push({});
        else if ((e == '&') || (e == '|'))
            s.top().operation = e;
        else
        {
            if (isdigit(e)) s.push({e != '0', e != '1', 0});
            auto right = s.top(); s.pop();
            auto left  = s.top(); s.pop();
            if      (left.operation == '&') s.push(right.opAnd(left));
            else if (left.operation == '|') s.push(right.opOr(left));
            else s.push(right.opClear());
        }
    }
    return std::max(s.top().flip_left, s.top().flip_right);
}

// ############################################################################
// ############################################################################

void test(std::string expression, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperationsToFlip(expression);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1&(0|1)", 1, trials);
    test("(0&0)&(0&0&0)", 3, trials);
    test("(0|(1|0&1))", 1, trials);
    return 0;
}


