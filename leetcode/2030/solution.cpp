#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string smallestSubsequence(std::string s, int k, char letter, int repetition)
{
    const int n = static_cast<int>(s.size());
    std::string result;
    std::vector<char> stack;
    int required = repetition;
    int n_letters = static_cast<int>(std::count(s.begin(), s.end(), letter));
    
    for (int i = 0; i < n; ++i)
    {
        const char c = s[i];
        while (!stack.empty()
           &&  (stack.back() > c)
           &&  (static_cast<int>(stack.size()) + n - i - 1 >= k)
           &&  ((stack.back() != letter) || (n_letters > required)))
        {
            const char popped = stack.back();
            stack.pop_back();
            if (popped == letter) ++required;
        }
        if (int sz = static_cast<int>(stack.size()); sz < k)
        {
            if (c == letter)
            {
                stack.push_back(c);
                --required;
            }
            else if (k > sz + required) stack.push_back(c);
        }
        if (c == letter) --n_letters;
    }
    for (char c : stack) result += c;
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          int k,
          char letter,
          int repetition,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestSubsequence(s, k, letter, repetition);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leet", 3, 'e', 1, "eet", trials);
    test("leetcode", 4, 'e', 2, "ecde", trials);
    test("bb", 2, 'b', 2, "bb", trials);
    return 0;
}


