#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countGoodTriplets(std::vector<int> arr, int a, int b, int c)
{
    const size_t n = arr.size();
    int result = 0;
    for (size_t i = 0; i < n; ++i)
        for (size_t j = i + 1; j < n; ++j)
            for (size_t k = j + 1; k < n; ++k)
                if ((std::abs(arr[i] - arr[j]) <= a)
                &&  (std::abs(arr[j] - arr[k]) <= b)
                &&  (std::abs(arr[i] - arr[k]) <= c))
                    ++result;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr,
          int a,
          int b,
          int c,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countGoodTriplets(arr, a, b, c);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 0, 1, 1, 9, 7}, 7, 2, 3, 4, trials);
    test({1, 1, 2, 2, 3}, 0, 0, 1, 0, trials);
    return 0;
}


