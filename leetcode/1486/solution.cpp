#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int xorOperation(int n, int start)
{
    int result = start;
    for (int i = 1; i < n; ++i)
        result ^= (start + 2 * i);
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int start, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = xorOperation(n, start);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 0, 8, trials);
    test(4, 3, 8, trials);
    return 0;
}


