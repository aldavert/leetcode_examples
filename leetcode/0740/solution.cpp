#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int deleteAndEarn(std::vector<int> nums)
{
    int points[10'001] = {};
    for (int n : nums) points[n] += n;
    int result[2] = { 0, 0 };
    for (int num : points)
        result[1] = std::exchange(result[0], std::max(result[0], result[1] + num));
    return result[0];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = deleteAndEarn(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 4, 2}, 6, trials);
    test({2, 2, 3, 3, 3, 4}, 9, trials);
    return 0;
}


