#include "../common/common.hpp"
#include <unordered_map>
#include <numeric>

// ############################################################################
// ############################################################################

long long minCost(std::vector<int> basket1, std::vector<int> basket2)
{
    std::vector<int> swapped;
    std::unordered_map<int, int> count;
    
    for (int b : basket1) ++count[b];
    for (int b : basket2) --count[b];
    for (auto [num, freq] : count)
    {
        if (freq % 2 != 0) return -1;
        for (int i = 0, m = std::abs(freq) / 2; i < m; ++i)
            swapped.push_back(num);
    }
    
    const int min_num = std::min(*min_element(basket1.begin(), basket1.end()),
                                 *min_element(basket2.begin(), basket2.end()));
    const auto middle_iterator = swapped.begin() + swapped.size() / 2;
    std::nth_element(swapped.begin(), middle_iterator, swapped.end());
    auto func = [min_num](long subtotal, int num)
    {
        return subtotal + std::min(2 * min_num, num);
    };
    return std::accumulate(swapped.begin(), middle_iterator, 0LL, func);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> basked1,
          std::vector<int> basked2,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCost(basked1, basked2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 2, 2}, {1, 4, 1, 2}, 1, trials);
    test({2, 3, 4, 1}, {3, 2, 5, 1}, -1, trials);
    return 0;
}


