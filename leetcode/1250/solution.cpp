#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

bool isGoodArray(std::vector<int> nums)
{
    int array_gcd = nums[0];
    for (size_t i = 1; i < nums.size(); ++i)
        array_gcd = std::gcd(array_gcd, nums[i]);
    return array_gcd == 1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isGoodArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({12, 5, 7, 23}, true, trials);
    test({29, 6, 10}, true, trials);
    test({3, 6}, false, trials);
    return 0;
}


