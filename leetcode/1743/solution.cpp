#include "../common/common.hpp"
#include <unordered_map>
#include <utility>
#include <limits>

// ############################################################################
// ############################################################################

std::vector<int> restoreArray(std::vector<std::vector<int> > adjacentPairs)
{
    if (adjacentPairs.size() == 0) return {};
    std::unordered_map<int, std::vector<int> > lut;
    for (const auto &pair : adjacentPairs)
    {
        lut[pair[0]].push_back(pair[1]);
        lut[pair[1]].push_back(pair[0]);
    }
    int current = 0;
    for (const auto &entry : lut)
    {
        if (std::get<1>(entry).size() == 1)
        {
            current = std::get<0>(entry);
            break;
        }
    }
    std::vector<int> result;
    int previous = std::numeric_limits<int>::max();
    while (true)
    {
        result.push_back(current);
        const auto &entry = lut[current];
        if (entry.size() == 1)
        {
            if (entry[0] == previous)
                break;
            previous = std::exchange(current, entry[0]);
        }
        else previous = std::exchange(current, entry[entry[0] == previous]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > adjacentPairs,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = restoreArray(adjacentPairs);
    std::vector<int> solution_reverse = solution;
    std::reverse(solution_reverse.begin(), solution_reverse.end());
    showResult((solution == result) || (solution_reverse == result),
               ((solution_reverse == result)?solution_reverse:solution), result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 1}, {3, 4}, {3, 2}}, {1, 2, 3, 4}, trials);
    test({{4, -2}, {1, 4}, {-3, 1}}, {-2, 4, 1, -3}, trials);
    test({{100000, -100000}}, {100000, -100000}, trials);
    return 0;
}


