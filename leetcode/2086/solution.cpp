#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumBuckets(std::string hamsters)
{
    for (size_t i = 0; i < hamsters.size(); ++i)
    {
        if (hamsters[i] == 'H')
        {
            if ((i > 0) && (hamsters[i - 1] == 'B'))
                continue;
            if ((i + 1 < hamsters.size()) && (hamsters[i + 1] == '.'))
                hamsters[i + 1] = 'B';
            else if ((i > 0) && (hamsters[i - 1] == '.'))
                hamsters[i - 1] = 'B';
            else return -1;
        }
    }
    return static_cast<int>(std::count(hamsters.begin(), hamsters.end(), 'B'));
}

// ############################################################################
// ############################################################################

void test(std::string hamsters, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumBuckets(hamsters);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("H..H", 2, trials);
    test(".H.H.", 1, trials);
    test(".HHH.", -1, trials);
    return 0;
}


