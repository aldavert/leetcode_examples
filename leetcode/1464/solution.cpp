#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int maxProduct(std::vector<int> nums)
{
    int maximum_value_first = 0, maximum_value_second = 0;
    for (int number : nums)
    {
        if (number > maximum_value_first)
            maximum_value_second = std::exchange(maximum_value_first, number);
        else if (number > maximum_value_second) maximum_value_second = number;
    }
    return (maximum_value_first - 1) * (maximum_value_second - 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxProduct(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 4, 5, 2}, 12, trials);
    test({1, 5, 4, 5}, 16, trials);
    test({3, 7}, 12, trials);
    return 0;
}


