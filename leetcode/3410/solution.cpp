#include "../common/common.hpp"
#include <limits>
#include <unordered_map>

// ############################################################################
// ############################################################################

long long maxSubarraySum(std::vector<int> nums)
{
    long result = *std::max_element(nums.begin(), nums.end()), prefix = 0, min_prefix = 0;
    long modified_min_prefix = 0;
    std::unordered_map<int, int> count;
    std::unordered_map<int, long> min_prefix_plus_removal;
    
    for (const int num : nums)
    {
        prefix += num;
        result = std::max(result, prefix - modified_min_prefix);
        if (num < 0)
        {
            ++count[num];
            min_prefix_plus_removal[num] =
                std::min(min_prefix_plus_removal[num], min_prefix) + num;
            modified_min_prefix = std::min({modified_min_prefix,
                                            count[num] * static_cast<long>(num),
                                            min_prefix_plus_removal[num]});
        }
        min_prefix = std::min(min_prefix, prefix);
        modified_min_prefix = std::min(modified_min_prefix, min_prefix);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSubarraySum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-3, 2, -2, -1, 3, -2, 3}, 7, trials);
    test({1, 2, 3, 4}, 10, trials);
    return 0;
}


