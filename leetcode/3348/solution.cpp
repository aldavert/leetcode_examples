#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

std::string smallestNumber(std::string num, long long t)
{
    constexpr int ps[] = {2, 3, 5, 7};
    long tmp = t;
    for (int i = 0; i < 4; ++i)
    {
        while (tmp % ps[i] == 0)
            tmp /= ps[i];
    }
    if (tmp != 1) return "-1";
    const int n = static_cast<int>(num.size());
    std::vector<long> left_t(n + 1);
    left_t[0] = t;
    int i0 = n - 1;
    for (int i = 0; i < n; ++i)
    {
        if (num[i] == '0')
        {
            i0 = i;
            break;
        }
        left_t[i + 1] = left_t[i] / std::gcd(left_t[i], num[i] - '0');
    }
    if (left_t[n] == 1) return num;
    for (int i = i0; i >= 0; --i)
    {
        while (++num[i] <= '9')
        {
            long tt = left_t[i] / std::gcd(left_t[i], num[i] - '0');
            int d = 9;
            for (int j = n - 1; j > i; --j)
            {
                while (tt % d) --d;
                tt /= d;
                num[j] = static_cast<char>('0' + d);
            }
            if (tt == 1) return num;
        }
    }
    std::string result;
    for (int d = 9; d >= 2; --d)
        for (; t % d == 0; t /= d)
            result += static_cast<char>('0' + d);
    result += std::string(std::max(n + 1 - (int)result.size(), 0), '1');
    std::reverse(result.begin(), result.end());
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string num, long long t, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestNumber(num, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1234", 256, "1488", trials);
    test("12355", 50, "12355", trials);
    test("11111", 26, "-1", trials);
    return 0;
}


