#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> findMode(TreeNode * root)
{
    std::vector<int> result;
    TreeNode * prev = nullptr;
    int count = 0, max_count = 0;
    auto inorder = [&](auto &&self, TreeNode * node) -> void
    {
        if (node)
        {
            self(self, node->left);
            if (prev && (prev->val == node->val))
                ++count;
            else count = 1;
            if (count > max_count)
            {
                max_count = count;
                result = {node->val};
            } else if (count == max_count)
            result.push_back(node->val);
            prev = node;
            self(self, node->right);
        }
    };
    inorder(inorder, root);
    return result;
}

// ############################################################################
// ############################################################################

template <typename T>
bool operator==(const std::vector<T> &left, const std::vector<T> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_map<T, unsigned int> histogram;
    for (const auto &l : left)
        ++histogram[l];
    for (const auto &r : right)
    {
        if (auto search = histogram.find(r); search != histogram.end())
        {
            if (search->second == 0) return false;
            --search->second;
            if (search->second == 0)
                histogram.erase(search);
        }
        else return false;
    }
    return histogram.empty();
}

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = findMode(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, null, 2, 2}, {2}, trials);
    test({0}, {0}, trials);
    return 0;
}

