#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long minimalKSum(std::vector<int> nums, int k)
{
    long result = 0;
    nums.push_back(0);
    std::sort(nums.begin(), nums.end());
    
    for (size_t i = 0; i + 1 < nums.size(); ++i)
    {
        if (nums[i] == nums[i + 1])
            continue;
        int l = nums[i] + 1, r = std::min(nums[i] + k, nums[i + 1] - 1);
        result += static_cast<long>(l + r) * (r - l + 1) / 2;
        k -= r - l + 1;
        if (k == 0)
            return result;
    }
    if (k > 0)
    {
        int l = nums.back() + 1, r = nums.back() + k;
        result += static_cast<long>(l + r) * (r - l + 1) / 2;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimalKSum(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 25, 10, 25}, 2, 5, trials);
    test({5, 6}, 6, 25, trials);
    return 0;
}


