#include "../common/common.hpp"
#include <limits>
#include <unordered_map>

// ############################################################################
// ############################################################################

bool isRectangleCover(std::vector<std::vector<int> > rectangles)
{
    std::unordered_map<long, int> corners;
    long area = 0;
    int minx = std::numeric_limits<int>::max();
    int miny = std::numeric_limits<int>::max();
    int maxx = std::numeric_limits<int>::lowest();
    int maxy = std::numeric_limits<int>::lowest();
    auto Code = [&](int x, int y) -> long
    {
        x += 100'005;
        y += 100'005;
        return (static_cast<long>(x) << 32) + static_cast<long>(y);
    };
    auto AddCorner = [&](int x, int y)
    {
        ++corners[Code(x, y)];
    };
    auto RemoveCorner = [&](int x, int y) -> bool
    {
        if (auto it = corners.find(Code(x, y));
                (it != corners.end()) && (it->second == 1))
        {
            corners.erase(it);
            return false;
        }
        else return true;
    };
    for (const auto &r : rectangles)
    {
        minx = std::min(minx, r[0]);
        miny = std::min(miny, r[1]);
        maxx = std::max(maxx, r[2]);
        maxy = std::max(maxy, r[3]);
        area += static_cast<long>(r[2] - r[0]) * static_cast<long>(r[3] - r[1]);
        AddCorner(r[0], r[1]);
        AddCorner(r[0], r[3]);
        AddCorner(r[2], r[3]);
        AddCorner(r[2], r[1]);
    }
    if (area == static_cast<long>(maxx - minx) * static_cast<long>(maxy - miny))
    {
        if (RemoveCorner(maxx, maxy)
         || RemoveCorner(maxx, miny)
         || RemoveCorner(minx, maxy)
         || RemoveCorner(minx, miny)) return false;
        for (auto [corner, frequency] : corners)
            if (frequency & 1) return false;
        
        return true;
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > rectangles,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isRectangleCover(rectangles);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 3, 3}, {3, 1, 4, 2}, {3, 2, 4, 4}, {1, 3, 2, 4}, {2, 3, 3, 4}}, true, trials);
    test({{1, 1, 2, 3}, {1, 3, 2, 4}, {3, 1, 4, 2}, {3, 2, 4, 4}}, false, trials);
    test({{1, 1, 3, 3}, {3, 1, 4, 2}, {1, 3, 2, 4}, {2, 2, 4, 4}}, false, trials);
    //// BB  CC
    //// BB  CC
    ////   EE
    ////   EE
    //// AA  DD
    //// AA  DD
    test({{0, 0, 2, 2}, {0, 4, 2, 6}, {4, 0, 6, 2}, {4, 4, 6, 6}, {2, 2, 4, 4},
          {2, 2, 4, 4}, {2, 2, 4, 4}, {2, 2, 4, 4}, {2, 2, 4, 4}}, false, trials);
    test({{0, 0, 2, 2}, {0, 4, 2, 6}, {4, 0, 6, 2}, {4, 4, 6, 6}, {2, 2, 4, 4},
          {1, 2, 3, 4}, {1, 2, 3, 4}, {3, 2, 5, 4}, {3, 2, 5, 4}}, false, trials);
    test({{0, 0, 2, 2}, {0, 4, 2, 6}, {4, 0, 6, 2}, {4, 4, 6, 6}, {2, 2, 4, 4},
          {1, 1, 3, 3}, {1, 3, 3, 5}, {3, 1, 5, 3}, {3, 3, 5, 5}}, false, trials);
    test({{0, 0, 2, 2}, {0, 4, 2, 6}, {4, 0, 6, 2}, {4, 4, 6, 6}, {2, 2, 4, 4},
          {0, 2, 2, 4}, {4, 2, 6, 4}, {2, 0, 4, 2}, {2, 4, 4, 6}}, true, trials);
    test({{0, 0, 4, 1}, {7, 0, 8, 2}, {5, 1, 6, 3}, {6, 0, 7, 2},
          {4, 0, 5, 1}, {4, 2, 5, 3}, {2, 1, 4, 3}, {-1, 2, 2, 3},
          {0, 1, 2, 2}, {6, 2, 8, 3}, {5, 0, 6, 1}, {4, 1, 5, 2}}, false, trials);
    return 0;
}


