from itertools import accumulate
from functools import lru_cache

def stoneGameVII(A):
    CSum = [0] + list(accumulate(A))
    @lru_cache(2000)
    def dp(i, j):
        if i > j: return 0
        sm = CSum[j + 1] - CSum[i]
        return sm - min(A[i] + dp(i + 1, j), A[j] + dp(i, j - 1))
    return dp(0, len(A) - 1)

if __name__ == '__main__':
    print(stoneGameVII([5, 3, 1, 4, 2]))
    print(stoneGameVII([7, 90, 5, 1, 100, 10, 10, 2]))
