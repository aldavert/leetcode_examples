#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int stoneGameVII(std::vector<int> stones)
{
    const int n = static_cast<int>(stones.size());
    int dp_current[1'001] = {}, dp_last[1'001] = {};
    for (int i = n - 2; i >= 0; --i)
    {
        int t = stones[i];
        for (int j = 0; j < n; ++j)
            std::swap(dp_last[j], dp_current[j]);
        for (int j = i + 1; j < n; ++j)
        {
            t += stones[j];
            dp_current[j] = t - std::min(stones[i] + dp_last[j],
                                         stones[j] + dp_current[j - 1]);
        }
    }
    return dp_current[n - 1];
}
#elif 0
int stoneGameVII(std::vector<int> stones)
{
    const int n = static_cast<int>(stones.size());
    int dp[1'001] = {}, acum[1'001];
    for (int i = 0; i < n; ++i) acum[i] = stones[i];
    for (int i = 1; i < n; ++i)
        for (int j = 0; j < n - i; ++j)
            dp[j] = (acum[j] += stones[j + i])
                  - std::min(stones[j] + dp[j + 1], stones[j + i] + dp[j]);
    return dp[0];
}
#elif 0
int stoneGameVII(std::vector<int> stones)
{
    const int n = static_cast<int>(stones.size());
    int dp[1'001] = {}, acum[1'001] = {};
    for (int i = 0; i < n; ++i) acum[i] = stones[i];
    for (int i = 1; i < n; ++i)
    {
        int left = 0, right = i;
        for (int j = 0; j < n - i; ++j, ++left, ++right)
        {
            acum[j] += stones[right];
            dp[j] = acum[j] - std::min(stones[left] + dp[j + 1], stones[right] + dp[j]);
        }
    }
    return dp[0];
}
#else
int stoneGameVII(std::vector<int> stones)
{
    const int n = static_cast<int>(stones.size());
    struct Problem
    {
        short left;
        short right;
        Problem inc_left(void) const { return { static_cast<short>(left + 1), right }; }
        Problem dec_right(void) const { return { left, static_cast<short>(right - 1) }; }
    };
    struct Info
    {
        int accum;
        int difference;
        int scoreAlice;
        int scoreBob;
        bool operator<(const Info &other) const { return difference < other.difference; }
    };
    auto update = [](const Info &previous, bool increase, int cost)
    {
        int value = previous.accum - cost;
        if (increase)
            return Info{ value, previous.difference + value,
                          previous.scoreAlice + cost, previous.scoreBob };
        else return Info{ value, previous.difference - value,
                            previous.scoreAlice, previous.scoreBob + cost };
    };
    auto display = [](Info * dp, int nelements)
    {
        bool next = false;
        std::cout << '{';
        for (int i = 0; i < nelements; ++i)
        {
            if (next) [[likely]] std::cout << ", ";
            next = true;
            std::cout << '[' << dp[i].accum << ':' << dp[i].difference;
            //std::cout << ':' << dp[i].scoreAlice << ':' << dp[i].scoreBob;
            //std::cout << ':' << dp[i].scoreAlice + dp[i].scoreBob;
            std::cout << ']';
        }
        std::cout << "}\n";
    };
    Info dp[1'002];
    std::queue<Problem> q;
    int accum = 0;
    for (int s : stones)
        accum += s;
    q.push({0, static_cast<short>(n - 1)});
    dp[0] = { accum, 0, 0, 0 };
    int nelements = 1;
    bool increase = true;
    
    display(dp, nelements);
    for (; nelements <= n; ++nelements)
    {
        Info previous = dp[0];
        dp[0] = update(dp[0], increase, stones[q.front().left]);
        q.push(q.front().inc_left());
        for (int i = 1; i < nelements; ++i)
        {
            std::cout << '.';
            Info aux = update(previous, increase, stones[q.front().right]);
            // NOT NEEDED: // q.push(q.front().dec_right());
            q.pop();
            previous = dp[i];
            if (!increase) dp[i] = std::max(aux, update(dp[i], increase, stones[q.front().left]));
            else dp[i] = std::min(aux, update(dp[i], increase, stones[q.front().left]));
            q.push(q.front().inc_left());
        }
        std::cout << '\n';
        dp[nelements] = update(previous, increase, stones[q.front().right]);
        q.push(q.front().dec_right());
        q.pop();
        
        display(dp, nelements + 1);
        
        increase = !increase;
    }
    int result = dp[0].difference;
    for (int i = 1; i < nelements; ++i)
        result = std::min(result, dp[i].difference);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> stones, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = stoneGameVII(stones);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 3, 1, 4, 2}, 6, trials);
    test({7, 90, 5, 1, 100, 10, 10, 2}, 122, trials);
    return 0;
}


