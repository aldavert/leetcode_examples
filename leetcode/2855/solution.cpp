#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumRightShifts(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int position_negative = -1;
    for (int previous = nums.back(), i = 0; i < n; ++i)
    {
        if (nums[i] - previous < 0)
        {
            if (position_negative != -1) return -1;
            position_negative = i;
        }
        previous = nums[i];
    }
    return (n - position_negative) % n;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumRightShifts(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 4, 5, 1, 2}, 2, trials);
    test({1, 3, 5}, 0, trials);
    test({2, 1, 4}, -1, trials);
    return 0;
}


