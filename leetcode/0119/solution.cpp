#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

std::vector<int> getRow(int rowIndex)
{
    std::vector<int> result(1, 1);
    for (int row = 0; row < rowIndex; ++row)
    {
        int previous = result[0];
        for (size_t j = 1; j < result.size(); ++j)
            previous = std::exchange(result[j], result[j] + previous);
        result.push_back(1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int rowIndex, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getRow(rowIndex);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(0, {1}, trials);
    test(1, {1, 1}, trials);
    test(2, {1, 2, 1}, trials);
    test(3, {1, 3, 3, 1}, trials);
    test(4, {1, 4, 6, 4, 1}, trials);
    test(5, {1, 5, 10, 10, 5, 1}, trials);
    test(6, {1, 6, 15, 20, 15, 6, 1}, trials);
    test(7, {1, 7, 21, 35, 35, 21, 7, 1}, trials);
    return 0;
}


