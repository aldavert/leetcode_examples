#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minCost(std::vector<std::vector<int> > grid)
{
    const std::pair<int, int> directions[] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
    const int n_rows = static_cast<int>(grid.size());
    const int n_cols = static_cast<int>(grid[0].size());
    std::vector<std::vector<int> > dp_table(n_rows, std::vector<int>(n_cols, -1));
    std::queue<std::pair<int, int> > q;
    
    auto search = [&](auto &&self, int i, int j, int cost) -> void
    {
        if ((i < 0) || (i == n_rows) || (j < 0) || (j == n_cols))
            return;
        if (dp_table[i][j] != -1)
            return;
        dp_table[i][j] = cost;
        q.emplace(i, j);
        const auto &orientation = directions[grid[i][j] - 1];
        self(self, i + orientation.first, j + orientation.second, cost);
    };
    search(search, 0, 0, 0);
    for (int cost = 1; !q.empty(); ++cost)
    {
        for (int sz = static_cast<int>(q.size()); sz > 0; --sz)
        {
            const auto [i, j] = q.front();
            q.pop();
            for (int d = 0; d < 4; ++d)
                search(search, i + directions[d].first, j + directions[d].second, cost);
        }
    }
    return dp_table[n_rows - 1][n_cols - 1];
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCost(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 1, 1}, {2, 2, 2, 2}, {1, 1, 1, 1}, {2, 2, 2, 2}}, 3, trials);
    test({{1, 1, 3}, {3, 2, 2}, {1, 1, 4}}, 0, trials);
    test({{1, 2}, {4, 3}}, 1, trials);
    return 0;
}


