#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> numMovesStonesII(std::vector<int> stones)
{
    const int n = static_cast<int>(stones.size());
    int min_moves = n;
    
    std::sort(stones.begin(), stones.end());
    for (int l = 0, r = 0; r < n; ++r)
    {
        while (stones[r] - stones[l] + 1 > n) ++l;
        int already_stored = r - l + 1;
        if ((already_stored == n - 1) && (stones[r] - stones[l] + 1 == n - 1))
            min_moves = std::min(min_moves, 2);
        else min_moves = std::min(min_moves, n - already_stored);
    }
    return { min_moves, std::max(stones[n - 1] - stones[1] - n + 2,
                                 stones[n - 2] - stones[0] - n + 2)};
}

// ############################################################################
// ############################################################################

void test(std::vector<int> stones, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = numMovesStonesII(stones);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({7, 4, 9}, {1, 2}, trials);
    test({6, 5, 4, 3, 10}, {2, 3}, trials);
    return 0;
}


