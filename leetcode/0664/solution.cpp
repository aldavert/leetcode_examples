#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int strangePrinter(std::string s)
{
    const int n = static_cast<int>(s.size());
    std::vector<std::vector<int> > dp(n, std::vector<int>(n));
    auto compute = [&](auto &&self, int i, int j) -> int
    {
        if (i > j) return 0;
        if (dp[i][j] > 0) return dp[i][j];
        dp[i][j] = self(self, i + 1, j) + 1;
        for (int k = i + 1; k <= j; ++k)
            if (s[k] == s[i])
                dp[i][j] = std::min(dp[i][j],
                                    self(self, i, k - 1) + self(self, k + 1, j));
        return dp[i][j];
    };
    return compute(compute, 0, n - 1);
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = strangePrinter(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aaabbb", 2, trials);
    test("aba", 2, trials);
    return 0;
}


