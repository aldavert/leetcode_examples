# Unique Binary Search Trees II

Given an integer `n`, return *all the structurally unique* ***BST's*** *(binary search trees), which has exactly* `n` *nodes of unique values from* `1` *to* `n`. Return the answer in **any order**.

#### Example 1:
> ```mermaid
> graph TD;
> A((1))---E1(( ))
> A---B((3))
> B---C((2))
> B---E2(( ))
> D((1))---E3(( ))
> D---E((2))
> E---E4(( ))
> E---F((3))
> H((2))---I((1))
> H---J((3))
> K((3))---L((2))
> K---E5(( ))
> L---M((1))
> L---E6(( ))
> N((3))---O((1))
> N---E7(( ))
> O---E8(( ))
> O---P((2))
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class E1,E2,E3,E4,E5,E6,E7,E8 empty;
> linkStyle 0,3,4,6,11,13,15,16 stroke-width:0px
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#FCA,stroke:#000,stroke-width:2px;
> class H,I,J selected;
> ```
> *Input:* `n = 3`  
> *Output:* `[[1, null, 2, null, 3], [1, null, 3, 2], [2, 1, 3], [3, 1, null, null, 2], [3, 2, null, 1]]`

#### Example 2:
> *Input:* `n = 1`  
> *Output:* `[[1]]`
 
#### Constraints:
- `1 <= n <= 8`

