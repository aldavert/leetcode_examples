#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<TreeNode *> generateTrees(int n)
{
    auto generate = [&](auto &&self, int begin, int end) -> std::vector<TreeNode *>
    {
        if (begin > end) return { nullptr };
        if (begin == end) return { new TreeNode(begin) };
        std::vector<TreeNode *> result;
        for (int i = begin; i <= end; ++i)
        {
            std::vector<TreeNode *> left = self(self, begin, i - 1);
            std::vector<TreeNode *> right = self(self, i + 1, end);
            for (auto tl : left)
                for (auto tr : right)
                    result.push_back(new TreeNode(i, tl, tr));
        }
        return result;
    };
    return generate(generate, 1, n);
}
#else
std::vector<TreeNode *> generateTrees(int n)
{
    auto generate = [&](auto &&self, int begin, int end) -> std::vector<TreeNode *>
    {
        if (begin > end) return { nullptr };
        if (begin == end) return { new TreeNode(begin) };
        std::vector<TreeNode *> result;
        for (int i = begin; i <= end; ++i)
        {
            std::vector<TreeNode *> left = self(self, begin, i - 1);
            std::vector<TreeNode *> right = self(self, i + 1, end);
            for (auto tl : left)
                for (auto tr : right)
                    result.push_back(new TreeNode(i, tl, tr));
            for (auto tl : left)
                delete tl;
            for (auto tr : right)
                delete tr;
        }
        return result;
    };
    return generate(generate, 1, n);
}
#endif


// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result.clear();
        std::vector<TreeNode *> tree_results = generateTrees(n);
        for (auto tn : tree_results)
        {
            result.push_back(tree2vec(tn));
            delete tn;
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{1, null, 2, null, 3},
             {1, null, 3, 2},
             {2, 1, 3},
             {3, 1, null, null, 2},
             {3, 2, null, 1}}, trials);
    test(1, {{1}}, trials);
    return 0;
}


