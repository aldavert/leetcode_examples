#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countOdds(int low, int high)
{
    return (high - low) / 2 + ((high & 1) || (low & 1));
}

// ############################################################################
// ############################################################################

void test(int low, int high, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countOdds(low, high);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 7, 3, trials);
    test(2, 7, 3, trials);
    test(3, 8, 3, trials);
    test(4, 8, 2, trials);
    test(3, 6, 2, trials);
    test(8, 10, 1, trials);
    test(9, 10, 1, trials);
    test(8, 9, 1, trials);
    return 0;
}


