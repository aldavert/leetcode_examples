#include "../common/common.hpp"
#include <unordered_map>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class UndergroundSystem
{
    struct Info
    {
        int accum = 0;
        int time = 0;
        void add(int a, int t) { accum += a; time += t; }
    };
    std::unordered_map<int, std::tuple<std::string, int> > m_transit;
    std::unordered_map<std::string, std::unordered_map<std::string, Info > > m_time;
public:
    UndergroundSystem()
    {
    }
    void checkIn(int id, std::string stationName, int t)
    {
        m_transit[id] = {stationName, t};
    }
    void checkOut(int id, std::string stationName, int t)
    {
        auto search = m_transit.find(id);
        auto [origin, t0] = search->second;
        m_time[origin][stationName].add(t - t0, 1);
        m_transit.erase(search);
    }
    double getAverageTime(std::string startStation, std::string endStation)
    {
        auto [ accum, frequency ] = m_time[startStation][endStation];
        return static_cast<double>(accum) / static_cast<double>(frequency);
    }
};

// ############################################################################
// ############################################################################

bool operator==(const std::vector<double> &left, const std::vector<double> &right)
{
    if (left.size() != right.size()) return false;
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (std::abs(left[i] - right[i]) > 0.00001)
            return false;
    return true;
}

enum class OP { IN  = 1, OUT = 2, GET = 4 };

struct DataCheck
{
    int id;
    std::string name;
    int time;
};
struct DataGet
{
    std::string start;
    std::string end;
};

void test(const std::vector<OP> operations,
          std::vector<DataCheck> input_check,
          std::vector<DataGet> input_get,
          std::vector<double> solution,
          unsigned int trials = 1)
{
    const size_t n = solution.size();
    std::vector<double> result(n);
    for (unsigned int i = 0; i < trials; ++i)
    {
        UndergroundSystem obj;
        for (size_t k = 0; k < n; ++k)
        {
            result[k] = null;
            if (operations[k] == OP::IN)
            {
                auto [id, stationName, time] = input_check[k];
                obj.checkIn(id, stationName, time);
            }
            else if (operations[k] == OP::OUT)
            {
                auto [id, stationName, time] = input_check[k];
                obj.checkOut(id, stationName, time);
            }
            else if (operations[k] == OP::GET)
            {
                auto [startStation, endStation] = input_get[k];
                result[k] = obj.getAverageTime(startStation, endStation);
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::IN, OP::IN, OP::IN, OP::OUT, OP::OUT, OP::OUT, OP::GET, OP::GET,
          OP::IN, OP::GET, OP::OUT, OP::GET},
         {{45, "Leyton", 3}, {32, "Paradise", 8}, {27, "Leyton", 10},
          {45, "Waterloo", 15}, {27, "Waterloo", 20}, {32, "Cambridge", 22}, {},
          {}, {10, "Leyton", 24}, {}, {10, "Waterloo", 38}, {}},
         {{}, {}, {}, {}, {}, {}, {"Paradise", "Cambridge"}, {"Leyton", "Waterloo"},
          {}, {"Leyton", "Waterloo"}, {}, {"Leyton", "Waterloo"}},
         {null, null, null, null, null, null, 14.00000, 11.00000, null, 11.00000,
          null, 12.00000}, trials);
    test({OP::IN, OP::OUT, OP::GET, OP::IN, OP::OUT, OP::GET, OP::IN, OP::OUT,
          OP::GET}, {{10, "Leyton", 3}, {10, "Paradise", 8}, {}, {5, "Leyton", 10},
         {5, "Paradise", 16}, {}, {2, "Leyton", 21}, {2, "Paradise", 30}, {}},
         {{}, {}, {"Leyton", "Paradise"}, {}, {}, {"Leyton", "Paradise"}, {}, {},
          {"Leyton", "Paradise"}}, {null, null, 5.00000, null, null, 5.50000,
          null, null, 6.66667}, trials);
    return 0;
}


