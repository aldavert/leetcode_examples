#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<int> relocateMarbles(std::vector<int> nums,
                                 std::vector<int> moveFrom,
                                 std::vector<int> moveTo)
{
    std::unordered_set<int> hash;
    for (size_t i = 0; i < nums.size(); ++i)
        hash.insert(nums[i]);
    for (size_t i = 0; i < moveFrom.size(); i++)
    {
        hash.erase(moveFrom[i]);
        hash.insert(moveTo[i]);
    }
    std::vector<int> result;
    for (auto it = hash.begin(); it != hash.end(); ++it)
        result.push_back(*it);
    std::sort(result.begin(), result.end());
    return result;
}
#else
std::vector<int> relocateMarbles(std::vector<int> nums,
                                 std::vector<int> moveFrom,
                                 std::vector<int> moveTo)
{
    std::set<int> nums_set{nums.begin(), nums.end()};
    for (size_t i = 0; i < moveFrom.size(); ++i)
    {
      nums_set.erase(nums_set.find(moveFrom[i]));
      nums_set.insert(moveTo[i]);
    }
    return {nums_set.begin(), nums_set.end()};
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> moveFrom,
          std::vector<int> moveTo,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = relocateMarbles(nums, moveFrom, moveTo);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 6, 7, 8}, {1, 7, 2}, {2, 9, 5}, {5, 6, 8, 9}, trials);
    test({1, 1, 3, 3}, {1, 3}, {2, 2}, {2}, trials);
    return 0;
}


