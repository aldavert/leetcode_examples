#include "../common/common.hpp"

// ############################################################################
// ############################################################################

void nextPermutation(std::vector<int> &nums)
{
    const int n = static_cast<int>(nums.size());
    int i, j;
    for (i = n - 2; i >= 0; --i)
        if (nums[i] < nums[i + 1])
            break;
    if (i >= 0)
    {
        for (j = n - 1; (j > i) && (nums[j] <= nums[i]); --j);
        if (j > i)
            std::swap(nums[i], nums[j]);
    }
    for (j = n - 1, ++i; i < j; ++i, --j)
        std::swap(nums[i], nums[j]);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = nums;
        nextPermutation(result);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, {1, 3, 2}, trials);
    test({3, 2, 1}, {1, 2, 3}, trials);
    test({1, 1, 5}, {1, 5, 1}, trials);
    test({2, 3, 1}, {3, 1, 2}, trials);
    return 0;
}

