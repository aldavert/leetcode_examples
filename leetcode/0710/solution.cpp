#include "../common/common.hpp"
#include <random>
#include <map>

// ############################################################################
// ############################################################################

class Solution
{
    std::random_device rd;
    std::mt19937 gen;
    std::uniform_int_distribution<> distribution;
    int number_of_elements;
    std::map<int, int> valid_to_full_range;
public:
    Solution(int n, std::vector<int> blacklist) :
        gen(rd())
    {
        std::sort(blacklist.begin(), blacklist.end());
        int valid_integer = 0;
        number_of_elements = 0;
        for (int blacklisted : blacklist)
        {
            if (valid_integer != blacklisted)
            {
                valid_to_full_range[number_of_elements] = valid_integer;
                number_of_elements += blacklisted - valid_integer;
                valid_integer = blacklisted;
            }
            ++valid_integer;
        }
        if (valid_integer < n)
        {
            valid_to_full_range[number_of_elements] = valid_integer;
            number_of_elements += n - valid_integer;
        }
        distribution = std::uniform_int_distribution<>(0, number_of_elements - 1);
    }
    int pick(void)
    {
        const int value = distribution(gen);
        auto search = std::prev(valid_to_full_range.upper_bound(value));
        return value - search->first + search->second;
    }
};

// ############################################################################
// ############################################################################

void test(int n, std::vector<int> blacklist, unsigned int trials = 1)
{
    std::vector<int> histogram;
    for (unsigned int i = 0; i < trials; ++i)
    {
        std::unordered_set<int> blacklist_set(blacklist.begin(), blacklist.end());
        std::vector<int> trial_histogram(n, 0);
        Solution s(n, blacklist);
        for (size_t e = 0, m = (n - blacklist.size()) * 1000; e < m; ++e)
        {
            int value = s.pick();
            if (auto search = blacklist_set.find(value); search != blacklist_set.end())
            {
                std::cout << showResult(false) << " The object returns '" << value
                          << "' which is present in the blacklist " << blacklist
                          << '\n';
                return;
            }
            if ((value < 0) || (value >= n))
            {
                std::cout << showResult(false) << " The object returns '" << value
                          << "' which is out of the range [0, " << n - 1 << "]\n";
                return;
            }
            ++trial_histogram[value];
        }
        histogram = trial_histogram;
    }
    std::sort(blacklist.begin(), blacklist.end());
    const int m = static_cast<int>(blacklist.size());
    int average = 0;
    for (int i = 0, j = 0; i < n; ++i)
    {
        if ((j < m) && (i == blacklist[j])) { ++j; continue; }
        average += histogram[i];
    }
    average /= n - m;
    double chi2 = 0.0;
    for (int i = 0, j = 0; i < n; ++i)
    {
        if ((j < m) && (i == blacklist[j])) { ++j; continue; }
        chi2 += static_cast<double>(histogram[i] - average)
              * static_cast<double>(histogram[i] - average)
              / static_cast<double>(average);
    }
    auto igf = [](double S, double Z) -> double
    {
        if (Z < 0.0) return 0.0;
        double Sc = 1.0 / S;
        Sc *= std::pow(Z, S) * exp(-Z);
        double sum = 1.0, nom = 1.0, denom = 1.0;
        for (int i = 0; i < 200; ++i)
        {
            nom *= Z;
            ++S;
            denom *= S;
            sum += nom / denom;
        }
        return sum * Sc;
    };
    double pvalue = igf((n - m - 1) / 2.0, chi2 / 2.0);
    if (std::isnan(pvalue) || std::isinf(pvalue) || (pvalue <= 1e-8))
        pvalue = 1e-14;
    else pvalue /= std::tgamma((n - m - 1) / 2.0);
    
    std::cout << "N=" << n << "; Blacklist=" << blacklist << '\n';
    if (pvalue > 0.05)
        std::cout << showResult(true) << " Distribution is uniform  with p-value "
                  << "= " << pvalue << "\n(hypothesis that the distribution is not "
                  << "uniform cannot be refuted)\n";
    else
    {
        std::cout << showResult(false) << " The values retrieved by the function "
                  << "are not uniformly distributed in the range [0, " << n - 1
                  << "]: " << histogram << '\n' << "The p-value is "
                  << pvalue << '\n';
    }
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, {2, 3, 5}, trials);
    test(10, {0, 5, 6, 9}, trials);
    test(10, {1, 2, 3, 7, 8}, trials);
    return 0;
}


