#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int diameterOfBinaryTree(TreeNode * root)
{
    int result = 0;
    auto search = [&](auto &&self, TreeNode * node) -> int
    {
        if (!node) return 0;
        int left = self(self, node->left);
        int right = self(self, node->right);
        result = std::max(result, left + right);
        return std::max(left, right) + 1;
    };
    search(search, root);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = diameterOfBinaryTree(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 3, trials);
    test({1, 2}, 1, trials);
    return 0;
}


