#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int smallestRepunitDivByK(int k)
{
    if (!(k & 1) || (k % 5 == 0)) return -1;
    for (int number = 0, len = 1; len <= k; ++len)
    {
        number = (number * 10 + 1) % k;
        if (number == 0) return len;
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestRepunitDivByK(k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 1, trials);
    test(2, -1, trials);
    test(3, 3, trials);
    test(4, -1, trials);
    test(5, -1, trials);
    return 0;
}


