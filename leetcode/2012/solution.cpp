#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sumOfBeauties(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> min_of_right(n);
    int result = 0;
    
    min_of_right.back() = nums.back();
    for (int i = n - 2; i >= 2; --i)
        min_of_right[i] = std::min(nums[i], min_of_right[i + 1]);
    for (int i = 1, max_of_left = nums[0]; i <= n - 2; ++i)
    {
        if ((max_of_left < nums[i]) && (nums[i] < min_of_right[i + 1]))
            result += 2;
        else if ((nums[i - 1] < nums[i]) && (nums[i] < nums[i + 1]))
            result += 1;
        max_of_left = std::max(max_of_left, nums[i]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOfBeauties(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 2, trials);
    test({2, 4, 6, 4}, 1, trials);
    test({3, 2, 1}, 0, trials);
    return 0;
}


