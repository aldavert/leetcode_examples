#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int largestValsFromLabels(std::vector<int> values,
                          std::vector<int> labels,
                          int numWanted,
                          int useLimit)
{
    const int n = static_cast<int>(values.size());
    struct Pair
    {
        int value = 0;
        int label = 0;
        bool operator<(const Pair &other) const { return value > other.value; }
    };
    std::vector<Pair> items;
    
    for (int i = 0; i < n; ++i) items.emplace_back(values[i], labels[i]);
    std::sort(items.begin(), items.end());
    int result = 0;
    std::unordered_map<int, int> labels_used;
    for (const auto& [value, label] : items)
    {
        if (labels_used[label] < useLimit)
        {
            result += value;
            ++labels_used[label];
            if (--numWanted == 0) break;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> values,
          std::vector<int> labels,
          int numWanted,
          int useLimit,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestValsFromLabels(values, labels, numWanted, useLimit);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 3, 2, 1}, {1, 1, 2, 2, 3}, 3, 1, 9, trials);
    test({5, 4, 3, 2, 1}, {1, 3, 3, 3, 2}, 3, 2, 12, trials);
    test({9, 8, 8, 7, 6}, {0, 0, 0, 1, 1}, 3, 1, 16, trials);
    return 0;
}


