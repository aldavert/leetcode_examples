#include "../common/common.hpp"
#include <deque>

// ############################################################################
// ############################################################################

#if 1
long long minMaxSubarraySum(std::vector<int> nums, int k)
{
    auto count = [&](int l, int r) -> long long
    {
        l = std::min(l, k);
        r = std::min(r, k);
        long long ex = std::max(l + r - 1 - k, 0);
        return (static_cast<long long>(l) * r) - (ex * (ex + 1) / 2);
    };
    constexpr int MAX = 80'001;
    const int n = static_cast<int>(nums.size());
    int left_big[MAX] = {}, left_small[MAX] = {}, right_big[MAX] = {},
        right_small[MAX] = {};
    
    left_big[0] = left_small[0] = -1;
    for (int i = 1; i < n; ++i)
    {
        int index = i - 1;
        while ((index != -1) && (nums[index] <= nums[i]))
            index = left_big[index];
        left_big[i] = index;
        index = i - 1;
        while ((index != -1) && (nums[index] >= nums[i]))
            index = left_small[index];
        left_small[i] = index;
    }
    right_big[n-1] = right_small[n-1] = n;
    for (int i = n - 2; i >= 0; --i)
    {
        int index = i + 1;
        while ((index != n) && (nums[index] < nums[i]))
            index = right_big[index];
        right_big[i] = index;
        index = i + 1;
        while ((index != n) && (nums[index] > nums[i]))
            index = right_small[index];
        right_small[i] = index;
    }
    
    long long result = 0;
    for (int i = 0; i < n; ++i)
    {
        result += nums[i] * count(i - left_big[i]  , right_big[i] - i  );
        result += nums[i] * count(i - left_small[i], right_small[i] - i);
    }
    return result;
}
#else
long long minMaxSubarraySum(std::vector<int> nums, int k)
{
    long long result = 0;
    std::deque<std::array<long long, 3> > min_value, max_value;
    long long mininum_sum = 0, maximum_sum = 0;
    
    for (long long i = 0, n = static_cast<long>(nums.size()); i < n; ++i)
    {
        long long minimum_cover = i, maximum_cover = i;
        while (!min_value.empty() && (min_value[0][0] <= i - k))
            mininum_sum -= min_value[0][2],
            min_value.pop_front();
        while (!max_value.empty() && (max_value[0][0] <= i - k))
            maximum_sum -= max_value[0][2],
            max_value.pop_front();
        if (!min_value.empty() && (min_value[0][1] == i - k))
        {
            mininum_sum -= min_value[0][2];
            ++min_value[0][1];
        }
        if (!max_value.empty() && (max_value[0][1] == i - k))
        {
            maximum_sum -= max_value[0][2];
            ++max_value[0][1];
        }
        while (!min_value.empty() && (min_value.back()[2] >= nums[i]))
        {
            minimum_cover = min_value.back()[1];
            mininum_sum -= (min_value.back()[0] - min_value.back()[1] + 1)
                         * min_value.back()[2];
            min_value.pop_back();
        }
        while (!max_value.empty() && (max_value.back()[2] <= nums[i]))
        {
            maximum_cover = max_value.back()[1];
            maximum_sum -= (max_value.back()[0] - max_value.back()[1] + 1)
                         * max_value.back()[2];
            max_value.pop_back();
        }
        max_value.push_back({i, maximum_cover, nums[i]});
        min_value.push_back({i, minimum_cover, nums[i]});
        minimum_cover = std::max(minimum_cover, i - k + 1);
        maximum_cover = std::max(maximum_cover, i - k + 1);
        mininum_sum += (i - minimum_cover + 1) * nums[i];
        maximum_sum += (i - maximum_cover + 1) * nums[i];
        result += mininum_sum + maximum_sum;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minMaxSubarraySum(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 2, 20, trials);
    test({1, -3, 1}, 2, -6, trials);
    return 0;
}


