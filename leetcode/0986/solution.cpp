#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> >
intervalIntersection(std::vector<std::vector<int> > firstList,
                     std::vector<std::vector<int> > secondList)
{
    std::vector<std::vector<int> > result;
    const int n = static_cast<int>(firstList.size());
    const int m = static_cast<int>(secondList.size());
    for (int i = 0, j = 0; (i < n) && (j < m); )
    {
        const auto &first = firstList[i];
        const auto &second = secondList[j];
        int begin = std::max(first[0], second[0]);
        int end = std::min(first[1], second[1]);
        int intersection = end - begin + 1;
        if (intersection > 0)
            result.push_back({begin, end});
        if (first[1] > second[1])
            ++j;
        else ++i;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > firstList,
          std::vector<std::vector<int> > secondList,
          std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = intervalIntersection(firstList, secondList);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 2}, {5, 10}, {13, 23}, {24, 25}}, {{1, 5}, {8, 12}, {15, 24},
         {25, 26}}, {{1, 2}, {5, 5}, {8, 10}, {15, 23}, {24, 24}, {25, 25}}, trials);
    test({{1, 3}, {5, 9}}, {}, {}, trials);
    test({}, {{4, 8}, {10, 12}}, {}, trials);
    test({{1, 7}}, {{3, 10}}, {{3, 7}}, trials);
    return 0;
}


