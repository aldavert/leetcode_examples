#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool hasMatch(std::string s, std::string p)
{
    auto asterisc = p.find('*');
    if (asterisc == 0)
        return s.find(p.substr(1)) != std::string::npos;
    auto begin = s.find(p.substr(0, asterisc));
    if (begin == std::string::npos) return false;
    return s.find(p.substr(asterisc + 1), begin + asterisc) != std::string::npos;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string p, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = hasMatch(s, p);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leetcode", "ee*e", true, trials);
    test("car", "c*v", false, trials);
    test("luck", "u*", true, trials);
    test("car", "*r", true, trials);
    test("car", "*v", false, trials);
    test("car", "c*", true, trials);
    test("car", "v*", false, trials);
    return 0;
}


