#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minElement(std::vector<int> nums)
{
    int result = std::numeric_limits<int>::max();
    for (int value : nums)
    {
        int current = 0;
        for (; value > 0; value /= 10) current += value % 10;
        result = std::min(result, current);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minElement(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 12, 13, 14}, 1, trials);
    test({1, 2, 3, 4}, 1, trials);
    test({999, 19, 199}, 10, trials);
    return 0;
}


