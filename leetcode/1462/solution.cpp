#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<bool> checkIfPrerequisite(int numCourses,
                                      std::vector<std::vector<int> > prerequisites,
                                      std::vector<std::vector<int> > queries)
{
    std::vector<bool> result;
    std::vector<std::vector<int> > graph(numCourses);
    std::vector<std::vector<bool> > isPrerequisite(numCourses,
                                                   std::vector<bool>(numCourses));
    auto dfs = [&](auto &&self, int u, std::vector<bool> &used) -> void
    {
        for (const int v : graph[u])
        {
            if (used[v])
                continue;
            used[v] = true;
            self(self, v, used);
        }
    };
    
    for (const auto &prerequisite : prerequisites)
        graph[prerequisite[0]].push_back(prerequisite[1]);
    for (int i = 0; i < numCourses; ++i)
        dfs(dfs, i, isPrerequisite[i]);
    for (const auto &query : queries)
        result.push_back(isPrerequisite[query[0]][query[1]]);
    return result;
}

// ############################################################################
// ############################################################################

void test(int numCourses,
          std::vector<std::vector<int> > prerequisites,
          std::vector<std::vector<int> > queries,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkIfPrerequisite(numCourses, prerequisites, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {{1, 0}}, {{0, 1}, {1, 0}}, {false, true}, trials);
    test(2, {}, {{1, 0}, {0, 1}}, {false, false}, trials);
    test(3, {{1, 2}, {1, 0}, {2, 0}}, {{1, 0}, {1, 2}}, {true, true}, trials);
    return 0;
}


