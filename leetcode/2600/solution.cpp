#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int kItemsWithMaximumSum(int numOnes, int numZeros, int numNegOnes, int k)
{
    return std::min(numOnes, k)
         - std::min(numNegOnes, std::max(0, k - numOnes - numZeros));
}

// ############################################################################
// ############################################################################

void test(int numOnes,
          int numZeros,
          int numNegOnes,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = kItemsWithMaximumSum(numOnes, numZeros, numNegOnes, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 2, 0, 2, 2, trials);
    test(3, 2, 0, 4, 3, trials);
    return 0;
}


