#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canConvertString(std::string s, std::string t, int k)
{
    if (s.size() != t.size()) return false;
    int shift_count[26] = {};
    
    for (size_t i = 0; i < s.size(); ++i)
    {
        int shift = (t[i] - s[i] + 26) % 26;
        if (shift == 0) continue;
        if (shift + 26 * shift_count[shift] > k)
            return false;
        ++shift_count[shift];
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string t, int k, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canConvertString(s, t, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("input", "ouput", 9, true, trials);
    test("abc", "bcd", 10, false, trials);
    test("aab", "bbb", 27, true, trials);
    return 0;
}


