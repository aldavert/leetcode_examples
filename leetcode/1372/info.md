# Longest ZigZag Path in a Binary Tree

You are given the `root` of a binary tree.

A ZigZag path for a binary tree is defined as follow:

- Choose **any** node in the binary tree and a direction (right or left).
- If the current direction is right, move to the right child of the current node; otherwise, move to the left child.
- Change the direction from right to left or from left to right.
- Repeat the second and third steps until you can't move in the tree.

Zigzag length is defined as the number of nodes visited - 1. (A single node has a length of 0).

Return *the longest* **ZigZag** *path contained in that tree.*

#### Example 1:
> ```mermaid
> graph TD;
> A((1))-->empty0(( ))
> A((1))-->B((1))
> B-->C((1))
> B-- Right -->D((1))
> D-- Left -->E((1))
> D-->F((1))
> E-->empty1(( ))
> E-- Right -->G((1))
> G-->empty2(( ))
> G-->H((1))
> H-->empty3(( ))
> H-->I((1))
> classDef default fill:#ffffff,stroke:#000000,stroke-width:2px;
> classDef Blank fill:#ffffff, stroke:#ffffff;
> classDef Selected fill:#ffaa00,stroke:#000000,stroke-width:2px;
> class empty0,empty1,empty2,empty3 Blank;
> class B,D,E,G Selected;
> linkStyle 0,6,8,10 display:none
> linkStyle 3,4,7 stroke:red
> ```
> *Input:* `root = [1, null, 1, 1, 1, null, null, 1, 1, null, 1, null, null, null, 1, null, 1]`  
> *Output:* `3`  
> *Explanation:* Longest ZigZag path in blue nodes `(right -> left -> right)`.

#### Example 2:
> ```mermaid
> graph TD;
> A((1))-- Left -->B((1))
> A-->C((1))
> B-->empty0(( ))
> B-- Right -->D((1))
> D-- Left -->E((1))
> D-->F((1))
> E-->empty1(( ))
> E-- Right -->G((1))
> classDef default fill:#ffffff,stroke:#000000,stroke-width:2px;
> classDef Blank fill:#ffffff, stroke:#ffffff;
> classDef Selected fill:#ffaa00,stroke:#000000,stroke-width:2px;
> class empty0,empty1 Blank;
> class A,B,D,E,G Selected;
> linkStyle 2,6 display:none
> linkStyle 0,3,4,7 stroke:red
> ```
> *Input:* `root = [1, 1, 1, null, 1, null, null, 1, 1, null, 1]`  
> *Output:* `4`  
> *Explanation:* Longest ZigZag path in blue nodes `(left -> right -> left -> right)`.

#### Example 3:
> *Input:* `root = [1]`  
> *Output:* `0`

#### Constraints:
- The number of nodes in the tree is in the range `[1, 5 * 10^4]`.
- `1 <= Node.val <= 100`


