#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int longestZigZag(TreeNode* root)
{
    int result = 0;
    auto traverse = [&](auto &&self, TreeNode * node, char direction, int score) -> void
    {
        result = std::max(result, score);
        if (!node) return;
        self(self, node->left, -1, (score + 1) * (direction ==  1));
        self(self, node->right, 1, (score + 1) * (direction == -1));
    };
    traverse(traverse, root, 0, 0);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = longestZigZag(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, null, 1, 1, 1, null, null, 1, 1, null, 1, null, null, null, 1, null, 1},
         3, trials);
    test({1, 1, 1, null, 1, null, null, 1, 1, null, 1}, 4, trials);
    test({1}, 0, trials);
    return 0;
}


