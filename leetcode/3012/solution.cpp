#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumArrayLength(std::vector<int> nums)
{
    const int min_num = *std::min_element(nums.begin(), nums.end());
    auto unary_pred = [&](int num) { return num % min_num > 0; };
    return (std::any_of(nums.begin(), nums.end(), unary_pred))
         ? 1
         : static_cast<int>((std::count(nums.begin(), nums.end(), min_num) + 1) / 2);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumArrayLength(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 3, 1}, 1, trials);
    test({5, 5, 5, 10, 5}, 2, trials);
    test({2, 3, 4}, 1, trials);
    return 0;
}


