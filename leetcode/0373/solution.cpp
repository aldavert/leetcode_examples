#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > kSmallestPairs(std::vector<int> nums1,
                                              std::vector<int> nums2,
                                              int k)
{
    struct Info
    {
        int i = -1;
        int j = -1;
        int sum = -1;
        bool operator<(const Info &other) const { return sum > other.sum; }
    };
    std::vector<std::vector<int> > result;
    std::priority_queue<Info> heap;
    for (int i = 0, n = static_cast<int>(nums1.size()); (i < k) && (i < n); ++i)
        heap.push({i, 0, nums1[i] + nums2[0]});
    while (!heap.empty() && (static_cast<int>(result.size()) < k))
    {
        auto [i, j, sum] = heap.top(); heap.pop();
        result.push_back({nums1[i], nums2[j]});
        if (j + 1 < static_cast<int>(nums2.size()))
            heap.push({i, j + 1, nums1[i] + nums2[j + 1]});
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int k,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = kSmallestPairs(nums1, nums2, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 7, 11}, {2, 4, 6}, 3, {{1, 2}, {1, 4}, {1, 6}}, trials);
    test({1, 1, 2}, {1, 2, 3}, 2, {{1, 1}, {1, 1}}, trials);
    test({1, 2}, {3}, 3, {{1, 3}, {2, 3}}, trials);
    return 0;
}


