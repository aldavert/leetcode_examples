#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numOfStrings(std::vector<std::string> patterns, std::string word)
{
    auto isSubstring = [](std::string p, std::string w) -> bool
    {
        if (p.size() > w.size()) return false;
        for (size_t i = 0, n = w.size() - p.size(); i <= n; ++i)
            if (w.substr(i, p.size()) == p)
                return true;
        return false;
    };
    int result = 0;
    for (auto pattern : patterns)
        result += isSubstring(pattern, word);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> patterns,
          std::string word,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numOfStrings(patterns, word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"a", "abc", "bc", "d"}, "abc", 3, trials);
    test({"a", "b", "c"}, "aaaaabbbbb", 2, trials);
    test({"a", "a", "a"}, "ab", 3, trials);
    return 0;
}


