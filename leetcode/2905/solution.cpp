#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findIndices(std::vector<int> nums,
                             int indexDifference,
                             int valueDifference)
{
    int min_index = 0, max_index = 0;
    for (int i = indexDifference, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        if (nums[i - indexDifference] < nums[min_index])
            min_index = i - indexDifference;
        if (nums[i - indexDifference] > nums[max_index])
            max_index = i - indexDifference;
        if (nums[i] - nums[min_index] >= valueDifference)
            return {i, min_index};
        if (nums[max_index] - nums[i] >= valueDifference)
            return {i, max_index};
    }
    return {-1, -1};
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int indexDifference,
          int valueDifference,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findIndices(nums, indexDifference, valueDifference);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 1, 4, 1}, 2, 4, {3, 0}, trials);
    test({2, 1}, 0, 0, {0, 0}, trials);
    test({1, 2, 3}, 2, 4, {-1, -1}, trials);
    return 0;
}


