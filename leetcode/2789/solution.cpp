#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maxArrayValue(std::vector<int> nums)
{
    long result = nums.back();
    for (int i = static_cast<int>(nums.size()) - 2; i >= 0; --i)
    {
        if (nums[i] > result) result = nums[i];
        else result += nums[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxArrayValue(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 7, 9, 3}, 21, trials);
    test({5, 3, 3}, 11, trials);
    return 0;
}


