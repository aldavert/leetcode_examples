#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int findSubstringInWraproundString(std::string s)
{
    if (s.empty()) return 0;
    int count[26] = {}, max_length = 1;
    count[static_cast<int>(s[0] - 'a')] = 1;
    for (size_t i = 1; i < s.size(); ++i)
    {
        if ((s[i] - s[i - 1] == 1) || (s[i - 1] - s[i] == 25))
            ++max_length;
        else max_length = 1;
        const int index = s[i] - 'a';
        count[index] = std::max(count[index], max_length);
    }
    return std::accumulate(&count[0], &count[26], 0);
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findSubstringInWraproundString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("a", 1, trials);
    test("cac", 2, trials);
    test("zab", 6, trials);
    test("zabfdef", 12, trials);
    test("zabfdefghij", 34, trials);  // zab(6) + defghij(28)
    test("zabcfzabcdef", 28, trials); // zabcdef(28)
    test("aaaaaaa", 1, trials);
    test("abcdzyxw", 14, trials);
    test("abcdexcdefg", 25, trials); // abcde(15) - cde(6) + x(1) + cdefg(15)
    return 0;
}


