# Binary Tree Zigzag Level Order Traversal

Given the `root` of a binary tree, return *the zigzag level order traversal of its nodes' values.* (i.e., from left to right, then right to left for the next level and alternate between).

#### Example 1:
> ```mermaid 
> graph TD;
> A((3))-->B((9))
> A-->C((20))
> C-->D((15))
> C-->E((7))
> classDef first fill:#FDD,stroke:#000,stroke-width:2px;
> classDef second fill:#DFD,stroke:#000,stroke-width:2px;
> classDef third fill:#DDF,stroke:#000,stroke-width:2px;
> class A first;
> class B,C second;
> class D,E third;
> ```
> *Input:* `root = [3, 9, 20, null, null, 15, 7]`  
> *Output:* `[[3], [20, 9], [15, 7]]`

#### Example 2:
> *Input:* `root = [1]`  
> *Output:* `[[1]]`

#### Example 3:
> *Input:* `root = []`  
> *Output:* `[]`

#### Constraints:
- The number of nodes in the tree is in the range `[0, 2000]`.
- `-100 <= Node.val <= 100`


