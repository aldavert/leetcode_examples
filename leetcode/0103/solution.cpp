#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > zigzagLevelOrder(TreeNode * root)
{
    std::vector<std::vector<int> > result;
    std::queue<TreeNode *> queue;
    if (root)
        queue.push(root);
    for (bool reverse = false; !queue.empty(); reverse = !reverse)
    {
        std::vector<int> current_level;
        for (size_t i = 0, n = queue.size(); i < n; ++i)
        {
            TreeNode * node = queue.front();
            queue.pop();
            current_level.push_back(node->val);
            if (node->left ) queue.push(node->left );
            if (node->right) queue.push(node->right);
        }
        if (reverse) std::reverse(current_level.begin(), current_level.end());
        result.push_back(std::move(current_level));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = zigzagLevelOrder(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 9, 20, null, null, 15, 7}, {{3}, {20, 9}, {15, 7}}, trials);
    test({1}, {{1}}, trials);
    test({}, {}, trials);
    return 0;
}


