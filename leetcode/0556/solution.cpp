#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int nextGreaterElement(int n)
{
    std::string number = std::to_string(n);
    int m = static_cast<int>(number.size());
    int index = m - 1;
    for (; (index >= 0) && (number[index] >= number[index + 1]); --index);
    if (index >= 0)
    {
        for (int i = m - 1; i > index; --i)
        {
            if (number[i] > number[index])
            {
                std::swap(number[index], number[i]);
                break;
            }
        }
    }
    for (++index, --m; index < m; ++index, --m)
        std::swap(number[index], number[m]);
    long result = std::stol(number);
    return ((result > std::numeric_limits<int>::max())
        ||  (result <= n))?-1:static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = nextGreaterElement(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(12, 21, trials);
    test(21, -1, trials);
    test(1234567890, 1234567908, trials);
    test(2147483647, -1, trials);
    test(2147483641, -1, trials);
    test(123, 132, trials);
    test(1223, 1232, trials);
    test(1221, 2112, trials);
    test(123454321, 123512344, trials);
    test(101, 110, trials);
    test(212, 221, trials);
    test(101427, 101472, trials);
    test(3101427, 3101472, trials);
    test(311427, 311472, trials);
    return 0;
}


