#include "../common/common.hpp"
#include <set>

// ############################################################################
// ############################################################################

bool checkValidString(std::string s)
{
    auto process = [&]() -> bool
    {
        const size_t n = s.size();
        std::set<std::tuple<size_t, size_t> > visited;
        auto inner =
            [&](auto &&self, size_t position, size_t number_of_open_parenthesis) -> int
        {
            if (visited.find({position, number_of_open_parenthesis}) != visited.end())
                return false;
            visited.insert({position, number_of_open_parenthesis});
            for (; (position < n) && (s[position] != '*'); ++position)
            {
                if (s[position] == '(')
                    ++number_of_open_parenthesis;
                else if (s[position] == ')')
                {
                    if (number_of_open_parenthesis == 0) return false;
                    --number_of_open_parenthesis;
                }
            }
            if (position == n) return number_of_open_parenthesis == 0;
            if (self(self, position + 1, number_of_open_parenthesis + 1))
                return true;
            if ((number_of_open_parenthesis > 0)
            &&   self(self, position + 1, number_of_open_parenthesis - 1))
                return true;
            return self(self, position + 1, number_of_open_parenthesis);
        };
        return inner(inner, 0, 0);
    };
    return process();
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkValidString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("()",   true, trials);
    test("(*)",  true, trials);
    test("(*))", true, trials);
    test("(((((()*)(*)*))())())(()())())))((**)))))(()())()", false, trials);
    test("(((((()()(()())())())(()())())))(((()))))(()())()", false, trials);
    test("************************************************************", true, trials);
    return 0;
}


