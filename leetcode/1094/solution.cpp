#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

bool carPooling(std::vector<std::vector<int> > trips, int capacity)
{
    struct Info
    {
        Info(int f, int t, int p) :
            from(static_cast<short>(f)),
            to(static_cast<short>(t)),
            passangers(static_cast<short>(p)) {}
        short from;
        short to;
        short passangers;
        bool operator<(const Info &other) const { return from < other.from; }
    };
    struct Drop
    {
        short position;
        short passangers;
        bool operator<(const Drop &other) const { return position > other.position; }
    };
    std::vector<Info> information;
    information.reserve(trips.size());
    for (const auto &t : trips)
        information.push_back({t[1], t[2], t[0]});
    std::sort(information.begin(), information.end());
    std::priority_queue<Drop> drop_positions;
    for (const auto &current : information)
    {
        capacity -= current.passangers;
        drop_positions.push({current.to, current.passangers});
        while (!drop_positions.empty()
           &&  (drop_positions.top().position <= current.from))
        {
            capacity += drop_positions.top().passangers;
            drop_positions.pop();
        }
        if (capacity < 0) return false;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > trips,
          int capacity,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = carPooling(trips, capacity);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 1, 5}, {3, 3, 7}}, 4, false, trials);
    test({{2, 1, 5}, {3, 3, 7}}, 5,  true, trials);
    test({{2, 1, 5}, {1, 2, 3}, {3, 3, 7}}, 5,  true, trials);
    // 1 2 3 4 5 6 7
    // ---------
    //   ---
    //     ---------
    return 0;
}


