#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findKthNumber(int n, int k)
{
    auto jump = [&n](long a) -> long
    {
        long r = 0;
        for (long b = a + 1; a <= n; a *= 10, b *= 10)
            r += std::min<long>(n + 1, b) - a;
        return r;
    };
    long result = 1;
    for (long i = 1; i < k;)
    {
        if (long inc = jump(result); i + inc <= k)
        {
            i += inc;
            ++result;
        }
        else
        {
            ++i;
            result *= 10;
        }
    }
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(int n, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findKthNumber(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(13, 2, 10, trials);
    test(1, 1, 1, trials);
    return 0;
}


