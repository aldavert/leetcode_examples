#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int minAreaRect(std::vector<std::vector<int> > points)
{
    int result = std::numeric_limits<int>::max();
    std::unordered_map<int, std::unordered_set<int> > x_to_y;
    
    for (const auto &point : points)
        x_to_y[point[0]].insert(point[1]);
    for (size_t i = 1; i < points.size(); ++i)
    {
        for (size_t j = 0; j < i; ++j)
        {
            const auto &p = points[i], &q = points[j];
            if ((p[0] == q[0]) || (p[1] == q[1]))
                continue;
            if (x_to_y[p[0]].count(q[1]) && x_to_y[q[0]].count(p[1]))
                result = std::min(result, std::abs(p[0] - q[0]) * std::abs(p[1] - q[1]));
        }
    }
    return (result == std::numeric_limits<int>::max())?0: result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minAreaRect(points);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1}, {1, 3}, {3, 1}, {3, 3}, {2, 2}}, 4, trials);
    test({{1, 1}, {1, 3}, {3, 1}, {3, 3}, {4, 1}, {4, 3}}, 2, trials);
    return 0;
}


