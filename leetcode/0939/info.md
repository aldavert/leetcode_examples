# Minimum Area Rectangle

You are given an array of points in the **X-Y** plane points where `points[i] = [x_i, y_i]`.

Return *the minimum area of a rectangle formed from these points, with sides parallel to the X and Y axes.* If there is not any such rectangle, return `0`.

#### Example 1:
> ```
>    |
>  4 +
>    |
>  3 +  *-----*
>    |  |     |
>  2 +  |  *  |
>    |  |     |
>  1 +  *-----*
>    |
> +--+--+--+--+--+--+--+--+
>    |  1  2  3  4  5  6  7
> -1 +
>    |
> ```
> *Input:* `points = [[1, 1], [1, 3], [3, 1], [3, 3], [2, 2]]`  
> *Output:* `4`

#### Example 2:
> ```
>    |
>  4 +
>    |
>  3 +  *     *--*
>    |        |  |
>  2 +        |  |
>    |        |  |
>  1 +  *     *--*
>    |
> +--+--+--+--+--+--+--+--+
>    |  1  2  3  4  5  6  7
> -1 +
>    |
> ```
> *Input:* `points = [[1, 1], [1, 3], [3, 1], [3, 3], [4, 1], [4, 3]]`  
> *Output:* `2`

#### Constraints:
- `1 <= points.length <= 500`
- `points[i].length == 2`
- `0 <= x_i, y_i <= 4 * 10^4`
- All the given points are **unique**.


