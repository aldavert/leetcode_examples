#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

std::vector<int> sequentialDigits(int low, int high)
{
    const int dlow  = static_cast<int>(std::min(9.0, std::floor(std::log10(low))  + 1));
    const int dhigh = static_cast<int>(std::min(9.0, std::floor(std::log10(high)) + 1));
    std::vector<int> result;
    for (int digits = dlow; digits <= dhigh; ++digits)
    {
        int value = 0, pow_high = 1;
        for (int d = 1; d <= digits; ++d, pow_high *= 10)
            value = value * 10 + d;
        if (value > high) break;
        if (value >= low) result.push_back(value);
        pow_high /= 10;
        for (int d = digits + 1; d <= 9; ++d)
        {
            value = (value - (d - digits) * pow_high) * 10 + d;
            if (value > high) break;
            if (value >= low) result.push_back(value);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int low, int high, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sequentialDigits(low, high);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(100, 300, {123, 234}, trials);
    test(1000, 13000, {1234, 2345, 3456, 4567, 5678, 6789, 12345}, trials);
    test(10, 1'000'000'000, {12, 23, 34, 45, 56, 67, 78, 89, 123, 234, 345, 456,
                             567, 678, 789, 1234, 2345, 3456, 4567, 5678, 6789,
                             12345, 23456, 34567, 45678, 56789, 123456, 234567,
                             345678, 456789, 1234567, 2345678, 3456789, 12345678,
                             23456789, 123456789}, trials);
    return 0;
}


