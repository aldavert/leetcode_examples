#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long numberOfWays(std::string s)
{
    int before[2] = {}, after[2] = {};
    long result = 0;
    
    after[0] = static_cast<int>(std::count(s.begin(), s.end(), '0'));
    after[1] = static_cast<int>(s.size()) - after[0];
    for (char c : s)
    {
        int num = c - '0';
        --after[num];
        if (num == 0) result += before[1] * after[1];
        else result += before[0] * after[0];
        ++before[num];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfWays(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("001101", 6, trials);
    test("11100", 0, trials);
    return 0;
}


