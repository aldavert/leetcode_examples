#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numMusicPlaylists(int n, int goal, int k)
{
    constexpr int MOD = 1'000'000'007;
    long dp[101][101] = {};
    dp[0][0] = 1;
    for (int i = 1; i <= goal; ++i)
        for (int j = 1; j <= n; ++j)
            dp[i][j] = dp[i - 1][j - 1] * (n - (j - 1))
                     + dp[i - 1][j] * std::max(0, j - k),
            dp[i][j] %= MOD;
    return static_cast<int>(dp[goal][n]);
}

// ############################################################################
// ############################################################################

void test(int n, int goal, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numMusicPlaylists(n, goal, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 3, 1, 6, trials);
    test(2, 3, 0, 6, trials);
    test(2, 3, 1, 2, trials);
    test(1, 3, 0, 1, trials);
    return 0;
}


