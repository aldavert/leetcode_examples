#include "../common/common.hpp"
#include "../common/list.hpp"
#include <random>

// ############################################################################
// ############################################################################

#if 0
class Solution
{
    std::vector<int> m_values;
    std::random_device m_rd;
    std::mt19937 m_gen;
    std::uniform_int_distribution<> m_dis;
public:
    Solution(ListNode * head) :
        m_gen(m_rd())
    {
        for (ListNode * current = head; current; current = current->next)
            m_values.push_back(current->val);
        m_dis = std::uniform_int_distribution<>(0, m_values.size() - 1);
    }
    
    int getRandom()
    {
        return m_values[m_dis(m_gen)];
    }
};
#else
class Solution
{
    ListNode * m_head;
    std::random_device m_rd;
    std::mt19937 m_gen;
    std::uniform_int_distribution<> m_dis;
    int m_n;
public:
    Solution(ListNode * head) :
        m_head(head),
        m_gen(m_rd())
    {
        m_n = 0;
        for (ListNode * current = head; current; current = current->next)
            ++m_n;
        m_dis = std::uniform_int_distribution<>(0, m_n - 1);
    }
    
    int getRandom()
    {
        if (m_head == nullptr) return 0;
        
        ListNode * current = m_head;
        for (int i = m_dis(m_gen); i; --i)
            current = current->next;
        return current->val;
    }
};
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    std::unordered_set<int> lut;
    for (int l : left) lut.insert(l);
    for (int r : right)
        if (lut.find(r) == lut.end())
            return false;
    return true;
}

void test(std::vector<int> input, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        auto * head = vec2list(input);
        Solution sol(head);
        result.clear();
        for (int j = 0, n = 10 * static_cast<int>(input.size()); j < n; ++j)
            result.push_back(sol.getRandom());
        delete head;
    }
    showResult(input == result, input, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, trials);
    test({1, 2, 3, 4, 5}, trials);
    return 0;
}


