#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int specialArray(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::sort(nums.begin(), nums.end());
    for (int i = 1, j = 0; j < n; ++i)
    {
        while ((j < n) && (nums[j] < i)) ++j;
        if (n - j == i) return i;
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = specialArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 5}, 2, trials);
    test({0, 0}, -1, trials);
    test({0, 4, 3, 0, 4}, 3, trials);
    return 0;
}


