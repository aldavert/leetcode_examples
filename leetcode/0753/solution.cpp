#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string crackSafe(int n, int k)
{
    std::vector<int> buffer(k * n + 1, 0), sequence;
    auto deBruijn = [&](auto &&self, int t, int p) -> void
    {
        if (t > n)
        {
            if (n % p == 0)
                for (int m = 1; m <= p; ++m)
                    sequence.push_back(buffer[m]);
        }
        else
        {
            buffer[t] = buffer[t - p];
            self(self, t + 1, p);
            for (int j = buffer[t - p] + 1; j < k; ++j)
            {
                buffer[t] = j;
                self(self, t + 1, t);
            }
        }
    };
    deBruijn(deBruijn, 1, 1);
    std::string result;
    for (int i : sequence)
        result += static_cast<char>('0' + i);
    for (int i = 1; i < n; ++i)
        result += result[i - 1];
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int k, std::vector<std::string> solutions, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = crackSafe(n, k);
    std::unordered_set<std::string> solutions_set(solutions.begin(), solutions.end());
    showResult(solutions_set.find(result) != solutions_set.end(), solutions, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 2, {"10", "01"}, trials);
    test(2, 2, {"01100", "00110", "10011", "11001"}, trials);
    test(1, 1, {"0"}, trials);
    return 0;
}


