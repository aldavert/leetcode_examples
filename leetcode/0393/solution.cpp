#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool validUtf8(std::vector<int> data)
{
    int count = 0;
    for (int val : data)
    {
        if (count == 0)
        {
            if      ((val & 0b10'00'00'00) == 0b00'00'00'00) continue;
            else if ((val & 0b11'10'00'00) == 0b11'00'00'00) count = 1;
            else if ((val & 0b11'11'00'00) == 0b11'10'00'00) count = 2;
            else if ((val & 0b11'11'10'00) == 0b11'11'00'00) count = 3;
            else return false;
        }
        else
        {
            if ((val & 0b11'00'00'00) != 0b10'00'00'00) return false;
            --count;
        }
    }
    return count == 0;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> data, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = validUtf8(data);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({197, 130, 1}, true, trials);
    test({235, 140, 4}, false, trials);
    return 0;
}


