#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string baseNeg2(int n)
{
    std::string result;
    while (n != 0)
    {
        result.push_back('0' + (n & 1));
        n = -(n >> 1);
    }
    std::reverse(result.begin(), result.end());
    return (!result.empty())?result:"0";
}

// ############################################################################
// ############################################################################

void test(int n, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = baseNeg2(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, "110", trials);
    test(3, "111", trials);
    test(4, "100", trials);
    return 0;
}


