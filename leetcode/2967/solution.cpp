#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

long long minimumCost(std::vector<int> nums)
{
    auto isPalindrome = [&](int num) -> bool
    {
        const std::string original = std::to_string(num),
                          reversed = {original.rbegin(), original.rend()};
        return original == reversed;
    };
    auto cost = [&](int palindrome, int delta) -> long
    {
        while (!isPalindrome(palindrome))
            palindrome += delta;
        return std::accumulate(nums.begin(), nums.end(), 0L,
            [&](long subtotal, int num) { return subtotal + abs(palindrome - num);});
    };
    std::sort(nums.begin(), nums.end());
    const int median = nums[nums.size() / 2];
    return std::min(cost(median,  1), cost(median, -1));
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumCost(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 6, trials);
    test({10, 12, 13, 14, 15}, 11, trials);
    test({22, 33, 22, 33, 22}, 22, trials);
    return 0;
}


