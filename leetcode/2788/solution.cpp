#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> splitWordsBySeparator(std::vector<std::string> words,
                                               char separator)
{
    std::vector<std::string> result;
    for (const std::string &word : words)
    {
        const int n = static_cast<int>(word.size());
        int previous = 0;
        for (int i = 0; i < n; ++i)
        {
            if (word[i] == separator)
            {
                if (i - previous > 0)
                    result.push_back(word.substr(previous, i - previous));
                previous = i + 1;
            }
        }
        if (n - previous > 0)
            result.push_back(word.substr(previous, n - previous));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          char separator,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = splitWordsBySeparator(words, separator);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"one.two.three", "four.five", "six"}, '.',
         {"one", "two", "three", "four", "five", "six"}, trials);
    test({"$easy$", "$problem$"}, '$', {"easy", "problem"}, trials);
    test({"|||"}, '|', {}, trials);
    return 0;
}


