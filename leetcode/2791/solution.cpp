#include "../common/common.hpp"
#include <unordered_map>
#include <stack>

// ############################################################################
// ############################################################################

#if 1
long long countPalindromePaths(std::vector<int> parent, std::string s)
{
    std::vector<std::vector<int> > adjacency(parent.size());
    for (int i = 0, n = static_cast<int>(parent.size()); i < n; ++i)
        if (parent[i] != -1)
            adjacency[parent[i]].push_back(i);
    std::unordered_map<int, long> count;
    std::stack<std::pair<int, int> > stack;
    stack.push({0, 0});
    while (!stack.empty())
    {
        auto [u, mask] = stack.top(); stack.pop();
        ++count[mask];
        for (int v : adjacency[u])
            stack.push({v, mask ^ (1 << (s[v] - 'a'))});
    }
    long result = 0;
    for (auto [mask, value] : count)
    {
        result += value * (value - 1);
        for (int i = 0; i < 26; ++i)
            if (auto search = count.find(mask ^ (1 << i)); search != count.end())
                result += value * search->second;
    }
    return result / 2;
}
#else
long long countPalindromePaths(std::vector<int> parent, std::string s)
{
    std::unordered_map<int, int> count;
    count[0] = 1;
    std::vector<std::vector<int> > adjacency(parent.size());
    for (int i = 0, n = static_cast<int>(parent.size()); i < n; ++i)
        if (parent[i] != -1)
            adjacency[parent[i]].push_back(i);
    auto iterDFS = [&](void) -> long
    {
        std::stack<std::pair<int, int> > stack;
        for (int v : adjacency[0]) stack.push({v, 0});
        
        long result = 0;
        while (!stack.empty())
        {
            auto [u, mask] = stack.top(); stack.pop();
            mask ^= 1 << (s[u] - 'a');
            for (int i = 0; i < 26; ++i)
                if (auto search = count.find(mask ^ (1 << i)); search != count.end())
                    result += search->second;
            result += count[mask]++;
            for (int v : adjacency[u])
                stack.push({v, mask});
        }
        return result;
    };
    return iterDFS();
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> parent,
          std::string s,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPalindromePaths(parent, s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, 0, 0, 1, 1, 2}, "acaabc", 8, trials);
    test({-1, 0, 0, 0, 0}, "aaaaa", 10, trials);
    test({-1, 0}, "pi", 1, trials);
    return 0;
}


