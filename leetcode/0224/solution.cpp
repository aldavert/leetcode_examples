#include "../common/common.hpp"
#include <stack>
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int calculate(std::string s)
{
    const int n = static_cast<int>(s.size());
    std::vector<std::pair<bool, int> > rpn(s.size());
    int rpn_begin = 0, rpn_end = 0;
    std::stack<int> operators;
    bool expected_number = true;
    auto flush = [&]()
    {
        while (!operators.empty() && (operators.top() != 3))
        {
            rpn[rpn_end++] = {true, operators.top()};
            operators.pop(); 
        }
    };
    
    expected_number = true;
    for (int i = 0; i < n;)
    {
        if ((s[i] >= '0') && (s[i] <= '9'))
        {
            int number = 0;
            while ((s[i] >= '0') && (s[i] <= '9'))
                number = number * 10 + (s[i++] - '0');
            rpn[rpn_end++] = {false, number};
            expected_number = false;
        }
        else if (s[i] == '+')
        {
            expected_number = true;
            flush();
            operators.push(0);
            ++i;
        }
        else if (s[i] == '-')
        {
            if (expected_number)
            {
                operators.push(2);
            }
            else
            {
                flush();
                operators.push(1);
            }
            ++i;
        }
        else if (s[i] == '(')
        {
            expected_number = true;
            operators.push(3);
            ++i;
        }
        else if (s[i] == ')')
        {
            expected_number = false;
            flush();
            operators.pop();
            ++i;
        }
        else ++i;
    }
    while (!operators.empty()) { rpn[rpn_end++] = {true, operators.top()}; operators.pop(); }
    while (rpn_begin != rpn_end)
    {
        if (!rpn[rpn_begin].first) operators.push(rpn[rpn_begin].second);
        else
        {
            if (rpn[rpn_begin].second == 2)
            {
                int first = operators.top();
                operators.pop();
                operators.push(-first);
            }
            else
            {
                int first = operators.top();
                operators.pop();
                int second = operators.top();
                operators.pop();
                if (rpn[rpn_begin].second) operators.push(second - first);
                else operators.push(first + second);
            }
        }
        ++rpn_begin;
    }
    return operators.top();
}
#else
int calculate(std::string s)
{
    const int n = static_cast<int>(s.size());
    std::queue<std::pair<bool, int> > rpn;
    std::stack<int> operators;
    bool expected_number = true;
    auto flush = [&]()
    {
        while (!operators.empty() && (operators.top() != 3))
        {
            rpn.push({true, operators.top()});
            operators.pop(); 
        }
    };
    
    expected_number = true;
    for (int i = 0; i < n;)
    {
        if ((s[i] >= '0') && (s[i] <= '9'))
        {
            int number = 0;
            while ((s[i] >= '0') && (s[i] <= '9'))
                number = number * 10 + (s[i++] - '0');
            rpn.push({false, number});
            expected_number = false;
        }
        else if (s[i] == '+')
        {
            expected_number = true;
            flush();
            operators.push(0);
            ++i;
        }
        else if (s[i] == '-')
        {
            if (expected_number)
            {
                operators.push(2);
            }
            else
            {
                flush();
                operators.push(1);
            }
            ++i;
        }
        else if (s[i] == '(')
        {
            expected_number = true;
            operators.push(3);
            ++i;
        }
        else if (s[i] == ')')
        {
            expected_number = false;
            flush();
            operators.pop();
            ++i;
        }
        else ++i;
    }
    while (!operators.empty()) { rpn.push({true, operators.top()}); operators.pop(); }
    while (!rpn.empty())
    {
        if (!rpn.front().first) operators.push(rpn.front().second);
        else
        {
            if (rpn.front().second == 2)
            {
                int first = operators.top();
                operators.pop();
                operators.push(-first);
            }
            else
            {
                int first = operators.top();
                operators.pop();
                int second = operators.top();
                operators.pop();
                if (rpn.front().second) operators.push(second - first);
                else operators.push(first + second);
            }
        }
        rpn.pop();
    }
    return operators.top();
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = calculate(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1 + 1", 2, trials);
    test(" 2-1 + 2 ", 3, trials);
    test("(1+(4+5+2)-3)+(6+8)", 23, trials);
    test("(-50) + 80", 30, trials);
    test("1-(4+5+2)", -10, trials);
    test("-2+1", -1, trials);
    test("2-1", 1, trials);
    test("-(3 + (4 + 5))", -12, trials);
    return 0;
}


