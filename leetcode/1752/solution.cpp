#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool check(std::vector<int> nums)
{
    bool rotated = false;
    for (int i = 1, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        if (nums[i] < nums[i - 1])
        {
            if (rotated) return false;
            rotated = true;
        }
    }
    if (rotated)
        return nums.back() <= nums.front();
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = check(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 4, 5, 1, 2}, true, trials);
    test({2, 1, 3, 4}, false, trials);
    test({1, 2, 3}, true, trials);
    return 0;
}


