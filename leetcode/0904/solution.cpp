#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int totalFruit(std::vector<int> fruits)
{
    struct Entry
    {
        int type = -1;
        int frequency = 0;
        bool operator==(const Entry &other) const { return type == other.type; }
        bool operator!=(const Entry &other) const { return type != other.type; }
    };
    std::vector<Entry> histogram;
    for (int previous = -1; int type : fruits)
    {
        if (type != previous)
            histogram.push_back({type, 1});
        else ++histogram.back().frequency;
        previous = type;
    }
    const int n = static_cast<int>(histogram.size());
    if (n == 1) return histogram[0].frequency;
    int maximum = histogram[0].frequency + histogram[1].frequency;
    if (n == 2) return maximum;
    Entry active[2] = {histogram[0], histogram[1]};
    
    int current = maximum;
    for (int i = 2; i < n; ++i)
    {
        if (active[0] != histogram[i])
        {
            maximum = std::max(maximum, current);
            current = active[1].frequency;
        }
        current += histogram[i].frequency;
        active[0] = std::exchange(active[1], histogram[i]);
    }
    return std::max(maximum, current);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> fruits, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = totalFruit(fruits);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1}, 3, trials);
    test({0, 1, 2, 2}, 3, trials);
    test({1, 2, 3, 2, 2}, 4, trials);
    return 0;
}


