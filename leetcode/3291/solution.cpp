#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minValidStrings(std::vector<std::string> words, std::string target)
{
    auto calcLPS = [](const std::string &pattern) -> std::vector<int>
    {
        std::vector<int> lps(pattern.length());
        for (int i = 1, j = 0, n = static_cast<int>(pattern.size()); i < n; ++i)
        {
            while ((j > 0) && (pattern[j] != pattern[i])) j = lps[j - 1];
            if (pattern[i] == pattern[j]) lps[i] = ++j;
        }
        return lps;
    };
    int unmatched_prefix = static_cast<int>(target.size()), result = 0;
    std::vector<std::vector<int> > lps_list;
    
    for (const auto &word : words)
        lps_list.push_back(calcLPS(word + '#' + target));
    while (unmatched_prefix > 0)
    {
        int max_match_suffix = 0;
        for (size_t i = 0; i < words.size(); ++i)
        max_match_suffix = std::max<int>(max_match_suffix,
                lps_list[i][words[i].size() + unmatched_prefix]);
        if (max_match_suffix == 0) return -1;
        ++result;
        unmatched_prefix -= max_match_suffix;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::string target,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minValidStrings(words, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"abc", "aaaaa", "bcdef"}, "aabcdabc", 3, trials);
    test({"abababab", "ab"}, "ababaababa", 2, trials);
    test({"abcdef"}, "xyz", -1, trials);
    return 0;
}


