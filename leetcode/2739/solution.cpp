#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int distanceTraveled(int mainTank, int additionalTank)
{
    int distance = 0, fuel = mainTank;
    while (fuel >= 5)
    {
        fuel += (additionalTank-- > 0) - 5;
        distance += 50;
    }
    return distance + fuel * 10;
}

// ############################################################################
// ############################################################################

void test(int mainTank, int additionalTank, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = distanceTraveled(mainTank, additionalTank);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 10, 60, trials);
    test(1, 2, 10, trials);
    test(1, 20, 10, trials);
    test(9, 2, 110, trials);
    return 0;
}


