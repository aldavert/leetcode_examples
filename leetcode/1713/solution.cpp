#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> target, std::vector<int> arr)
{
    const int n_target = static_cast<int>(target.size());
    std::unordered_map<int, int> target_to_index;
    for (int i = 0; i < n_target; ++i)
        target_to_index[target[i]] = i;
    std::vector<int> dp;
    for (int x : arr)
    {
        auto it_index = target_to_index.find(x);
        if (it_index == target_to_index.end()) continue;
        int index = it_index->second;
        if (dp.empty() || (index > dp.back()))
            dp.push_back(index);
        else *std::lower_bound(dp.begin(), dp.end(), index) = index;
    }
    return n_target - static_cast<int>(dp.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> target,
          std::vector<int> arr,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(target, arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 1, 3}, {9, 4, 2, 3, 4}, 2, trials);
    test({6, 4, 8, 1, 3, 2}, {4, 7, 6, 2, 3, 8, 6, 1}, 3, trials);
    return 0;
}


