#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minDeletionSize(std::vector<std::string> strs)
{
    const int n = static_cast<int>(strs.size()),
              m = static_cast<int>(strs.front().size());
    std::vector<bool> sorted(n - 1);
    int result = 0;
    
    for (int j = 0; j < m; ++j)
    {
        int i;
        for (i = 0; i + 1 < n; ++i)
        {
            if (!sorted[i] && (strs[i][j] > strs[i + 1][j]))
            {
                ++result;
                break;
            }
        }
        if (i + 1 == n)
            for (i = 0; i + 1 < n; ++i)
                sorted[i] = sorted[i] || (strs[i][j] < strs[i + 1][j]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> strs, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDeletionSize(strs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"ca", "bb", "ac"}, 1, trials);
    test({"xc", "yb", "za"}, 0, trials);
    test({"zyx", "wvu", "tsr"}, 3, trials);
    return 0;
}


