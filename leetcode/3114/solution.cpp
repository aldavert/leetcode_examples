#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string findLatestTime(std::string s)
{
    if ((s[0] == '?') && (s[1] == '?')) s[0] = s[1] = '1';
    else if (s[0] == '?')
        s[0] = '0' + ((s[1] == '1') || (s[1] == '0'));
    else if (s[1] == '?') s[1] = (s[0] == '0')?'9':'1';
    
    if (s[3] == '?') s[3] = '5';
    if (s[4] == '?') s[4] = '9';
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLatestTime(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1?:?4", "11:54", trials);
    test("0?:5?", "09:59", trials);
    return 0;
}


