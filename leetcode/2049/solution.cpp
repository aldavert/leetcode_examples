#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countHighestScoreNodes(std::vector<int> parents)
{
    std::vector<std::vector<int> > tree(parents.size());
    long max_score = 0;
    int result = 0;
    auto dfs = [&](auto &&self, int u) -> int
    {
        int count = 1;
        long score = 1;
        for (int v : tree[u])
        {
            const int child_count = self(self, v);
            count += child_count;
            score *= child_count;
        }
        int above_count = static_cast<int>(tree.size()) - count;
        score *= std::max(above_count, 1);
        if (score > max_score)
        {
            max_score = score;
            result = 1;
        }
        else result += (score == max_score);
        return count;
    };
    
    for (size_t i = 0; i < parents.size(); ++i)
        if (parents[i] != -1)
            tree[parents[i]].push_back(static_cast<int>(i));
    dfs(dfs, 0);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> parents, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countHighestScoreNodes(parents);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, 2, 0, 2, 0}, 3, trials);
    test({-1, 2, 0}, 2, trials);
    return 0;
}


