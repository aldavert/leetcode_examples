#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int medianOfUniquenessArray(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    auto subarray = [&](int k) -> long
    {
        long result = 0;
        int count[100'001] = {};
        for (int l = 0, r = 0; r < n; ++r)
        {
            if (++count[nums[r]] == 1) --k;
            while (k == -1)
                if (--count[nums[l++]] == 0)
                    ++k;
            result += r - l + 1;
        }
        return result;
    };
    const long median_count = ((n * (n + 1L) / 2L) + 1L) / 2L;
    int l = 1, r = n;
    while (l < r)
    {
        const int m = (l + r) / 2;
        if (subarray(m) >= median_count) r = m;
        else l = m + 1;
    }
    return l;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = medianOfUniquenessArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 1, trials);
    test({3, 4, 3, 4, 5}, 2, trials);
    test({4, 3, 5, 4}, 2, trials);
    return 0;
}


