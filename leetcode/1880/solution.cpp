#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isSumEqual(std::string firstWord,
                std::string secondWord,
                std::string targetWord)
{
    auto convert = [](std::string text) -> long
    {
        long result = 0;
        for (char chr : text)
            result = result * 10 + static_cast<long>(chr - 'a');
        return result;
    };
    long first = convert(firstWord),
         second = convert(secondWord),
         target = convert(targetWord);
    return first + second == target;
}

// ############################################################################
// ############################################################################

void test(std::string firstWord,
          std::string secondWord,
          std::string targetWord,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isSumEqual(firstWord, secondWord, targetWord);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("acb", "cba", "cdb", true, trials);
    test("aaa", "a", "aab", false, trials);
    test("aaa", "a", "aaaa", true, trials);
    return 0;
}


