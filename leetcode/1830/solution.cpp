#include "../common/common.hpp"
#include <map>

// ############################################################################
// ############################################################################

int makeStringSorted(std::string s)
{
    constexpr int MOD = 1'000'000'007;
    int n = static_cast<int>(s.size());
    auto pow = [&](long a, long p) -> long
    {
        long result = 1;
        for (; p > 0; a = (a * a) % MOD, p = p >> 1)
            if (p & 1) result = (result * a) % MOD;
        return result;
    };
    std::map<int, int> frequency; 
    for (char ch : s)
        ++frequency[ch - 'a'];

    std::vector<long> fact(s.size() + 1, 1);
    for (int i = 1; i <= n; ++i)
        fact[i] = (fact[i - 1] * i) % MOD;

    long result = 0;
    for (char ch : s)
    {
        long frequency_sum = 0;
        long duplicates = 1;
        for (int i = 0; i < 26; ++i)
        {
            if (i < (ch - 'a')) frequency_sum += frequency[i];
            duplicates = (duplicates * fact[frequency[i]]) % MOD;
        }
        result += (frequency_sum * fact[n - 1] % MOD) * pow(duplicates, MOD - 2);
        result %= MOD;
        --n;
        --frequency[ch - 'a'];
    }
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = makeStringSorted(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("cba", 5, trials);
    test("aabaa", 2, trials);
    return 0;
}


