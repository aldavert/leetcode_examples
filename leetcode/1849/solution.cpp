#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool splitString(std::string s)
{
    const int n = static_cast<int>(s.size());
    auto isValid = [&](auto && self, int start, long previous, int segment) -> bool
    {
        if ((start == n) && (segment > 1))
            return true;
        long current = 0;
        for (int i = start; i < n; ++i)
        {
            current = current * 10 + s[i] - '0';
            if (current > 9999999999L)
                return false;
            if (((previous == -1) || (current == previous - 1))
            &&  self(self, i + 1, current, segment + 1))
                return true;
        }
        return false;
    };
    return isValid(isValid, 0, -1, 0);
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = splitString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1234", false, trials);
    test("050043", true, trials);
    test("9080701", false, trials);
    return 0;
}


