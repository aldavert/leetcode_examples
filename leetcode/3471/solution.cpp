#include "../common/common.hpp"
#include <unordered_map>
#include <unordered_set>

// ############################################################################
// ############################################################################

int largestInteger(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::unordered_map<int, int> histogram;
    for (int i = 0; i <= n - k; ++i)
    {
        std::unordered_set<int> present;
        for (int j = 0; j < k; ++j)
            present.insert(nums[i + j]);
        for (int value : present)
            ++histogram[value];
    }
    int result = -1;
    for (auto [value, frequency] : histogram)
        if (frequency == 1)
            result = std::max(result, value);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestInteger(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 9, 2, 1, 7}, 3, 7, trials);
    test({3, 9, 7, 2, 1, 7}, 4, 3, trials);
    test({0, 0}, 1, -1, trials);
    test({0, 0}, 2, 0, trials);
    return 0;
}


