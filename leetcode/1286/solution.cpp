#include "../common/common.hpp"
#include <variant>

// ############################################################################
// ############################################################################

class CombinationIterator
{
    std::string m_characters = "";
    std::vector<int> m_index = {};
    int m_n = 0;
public:
    CombinationIterator(std::string characters, int combinationLength) :
        m_characters(characters),
        m_index(combinationLength, 0),
        m_n(combinationLength)
    {
        for (int i = 1; i < combinationLength; ++i) m_index[i] = i;
    }
    std::string next()
    {
        std::string result(m_n, '\0');
        for (int i = 0; i < m_n; ++i)
            result[i] = m_characters[m_index[i]];
        for (int i = m_n - 1; i >= 0; --i)
        {
            ++m_index[i];
            if (m_index[i] < static_cast<int>(m_characters.size()) - (m_n - i - 1))
            {
                for (int j = i + 1; j < m_n; ++j)
                    m_index[j] = m_index[j - 1] + 1;
                break;
            }
        }
        return result;
    }
    bool hasNext()
    {
        return m_index[0] < static_cast<int>(m_characters.size()) - (m_n - 1);
    }
};

// ############################################################################
// ############################################################################

enum class OP { CREATE, NEXT, HASNEXT };

void test(std::vector<OP> operations,
          std::vector<std::pair<std::string, int> > input,
          std::vector<std::variant<std::string, bool> > solution,
          unsigned int trials = 1)
{
    std::vector<std::variant<std::string, bool> > result(operations.size());
    for (unsigned int i = 0; i < trials; ++i)
    {
        const int n = static_cast<int>(operations.size());
        CombinationIterator * obj = nullptr;
        for (int idx = 0; idx < n; ++idx)
        {
            switch (operations[idx])
            {
            case OP::CREATE:
                result[idx] = false;
                delete obj;
                obj = new CombinationIterator(input[idx].first, input[idx].second);
                break;
            case OP::NEXT:
                result[idx] = obj->next();
                break;
            case OP::HASNEXT:
                result[idx] = obj->hasNext();
                break;
            default:
                std::cerr << "[ERROR] Unknown operand.\n";
                return;
            }
        }
        delete obj;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::CREATE, OP::NEXT, OP::HASNEXT, OP::NEXT, OP::HASNEXT, OP::NEXT,
          OP::HASNEXT}, {{"abc", 2}, {}, {}, {}, {}, {}, {}},
         {false, "ab", true, "ac", true, "bc", false}, trials);
    test({OP::CREATE, OP::NEXT, OP::HASNEXT, OP::NEXT, OP::HASNEXT, OP::NEXT,
          OP::HASNEXT, OP::NEXT, OP::HASNEXT, OP::NEXT, OP::HASNEXT, OP::NEXT,
          OP::HASNEXT, OP::NEXT, OP::HASNEXT, OP::NEXT, OP::HASNEXT, OP::NEXT,
          OP::HASNEXT, OP::NEXT, OP::HASNEXT},
         {{"abcde", 3}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}},
         {false, "abc", true, "abd", true, "abe", true, "acd", true, "ace", true,
          "ade", true, "bcd", true, "bce", true, "bde", true, "cde", false}, trials);
    test({OP::CREATE, OP::NEXT, OP::HASNEXT, OP::NEXT, OP::HASNEXT, OP::NEXT,
          OP::HASNEXT}, {{"abc", 1}, {}, {}, {}, {}, {}, {}},
         {false, "a", true, "b", true, "c", false}, trials);
    return 0;
}


