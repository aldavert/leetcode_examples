#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isSubsequence(std::string s, std::string t)
{
    const int n = static_cast<int>(s.size());
    int position = 0;
    for (char c : t)
    {
        if (c == s[position])
        {
            ++position;
            if (position == n) break;
        }
    }
    return position == n;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string t, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isSubsequence(s, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", "ahbgdc", true , trials);
    test("axc", "ahbgdc", false, trials);
    return 0;
}


