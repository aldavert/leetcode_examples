#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findMinMoves(std::vector<int> machines)
{
    const int n = static_cast<int>(machines.size());
    int number_of_dresses = 0;
    for (int dresses : machines)
        number_of_dresses += dresses;
    if (number_of_dresses % n != 0) return -1;
    const int expected = number_of_dresses / n;
    int result = 0;
    for (int excess = 0; int dresses : machines)
        excess += dresses - expected,
        result = std::max({result, std::abs(excess), dresses - expected});
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> machines, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMinMoves(machines);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 5}, 3, trials);
    test({0, 3, 0}, 2, trials);
    test({0, 2, 0}, -1, trials);
    test({0, 10, 0, 10, 0, 10, 0, 0, 0, 0}, 12, trials);
    test({10, 0, 0, 0, 10, 0, 0, 0, 0, 10}, 7, trials);
    return 0;
}


