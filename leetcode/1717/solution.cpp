#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumGain(std::string s, int x, int y)
{
    struct Pair
    {
        std::string substring;
        int points;
    };
    auto gain = [&](const Pair &first, const Pair &second) -> int
    {
        std::vector<char> stack1, stack2;
        int points = 0;
        
        for (char c : s)
        {
            if (!stack1.empty() && (stack1.back() == first.substring[0])
                                && (c == first.substring[1]))
            {
                stack1.pop_back();
                points += first.points;
            }
            else stack1.push_back(c);
        }
        for (char c : stack1)
        {
            if (!stack2.empty() && (stack2.back() == second.substring[0])
                                && (c == second.substring[1]))
            {
                stack2.pop_back();
                points += second.points;
            }
            else stack2.push_back(c);
        }
        return points;
    };
    return (x > y)?gain({"ab", x}, {"ba", y}):gain({"ba", y}, {"ab", x});
}

// ############################################################################
// ############################################################################

void test(std::string s, int x, int y, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumGain(s, x, y);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("cdbcbbaaabab", 4, 5, 19, trials);
    test("aabbaaxybbaabb", 5, 4, 20, trials);
    return 0;
}


