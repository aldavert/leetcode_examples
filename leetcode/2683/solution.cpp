#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

bool doesValidArrayExist(std::vector<int> derived)
{
    return std::accumulate(derived.begin(), derived.end(), 0, std::bit_xor<>()) == 0;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> derived, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = doesValidArrayExist(derived);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 0}, true, trials);
    test({1, 1}, true, trials);
    test({1, 0}, false, trials);
    return 0;
}


