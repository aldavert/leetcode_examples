#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
long long distinctNames(std::vector<std::string> ideas)
{
    std::unordered_map<std::string, int> masks;
    for (auto idea : ideas)
        masks[idea.substr(1)] |= 1 << (idea[0] - 'a');
    int count[26][26][2] = {};
    for (auto [suffix, mask] : masks)
        for (int i = 0; i < 26; ++i)
            for (int j = i + 1; j < 26; ++j)
                if (((mask >> i) & 1) ^ ((mask >> j) & 1))
                    ++count[i][j][(mask >> i) & 1];
    long long result = 0;
    for (int i = 0; i < 26; ++i)
        for (int j = i + 1; j < 26; ++j)
            result += 2ll * count[i][j][0] * count[i][j][1];
    return result;
}
#else
long long distinctNames(std::vector<std::string> ideas)
{
    long long result = 0;
    std::unordered_set<std::string> suffixes[26];
    for (auto idea : ideas)
        suffixes[idea[0] - 'a'].insert(idea.substr(1));
    for (int i = 0; i < 25; ++i)
    {
        for (int j = i + 1; j < 26; ++j)
        {
            long long count = 0;
            for (auto suffix : suffixes[i])
                if (suffixes[j].count(suffix))
                    ++count;
            result += 2 * (suffixes[i].size() - count)
                        * (suffixes[j].size() - count);
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> ideas,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = distinctNames(ideas);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"coffee", "donuts", "time", "toffee"}, 6, trials);
    test({"lack", "back"}, 0, trials);
    return 0;
}


