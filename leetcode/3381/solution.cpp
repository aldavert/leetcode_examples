#include "../common/common.hpp"
#include <limits>
#include <fstream>

// ############################################################################
// ############################################################################

#if 1
long long maxSubarraySum(std::vector<int> nums, int k)
{
    long long result = std::numeric_limits<long long>::lowest(), prefix = 0;
    std::vector<long long> min_prefix(k, std::numeric_limits<long long>::max() / 2);
    min_prefix[k - 1] = 0;
    for (size_t i = 0; i < nums.size(); ++i)
    {
        prefix += nums[i];
        result = std::max(result, prefix - min_prefix[i % k]);
        min_prefix[i % k] = std::min(min_prefix[i % k], prefix);
    }
    return result;
}
#else
long long maxSubarraySum(std::vector<int> nums, int k)
{
    std::vector<long long> accum(nums.size() + 1);
    accum[0] = 0;
    for (size_t i = 1; i <= nums.size(); ++i)
        accum[i] = accum[i - 1] + nums[i - 1];
    const int n = static_cast<int>(nums.size());
    long long result = std::numeric_limits<long long>::lowest();
    for (int ws = k; ws <= n; ws += k)
        for (int i = ws; i <= n; ++i)
            result = std::max(result, accum[i] - accum[i - ws]);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSubarraySum(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2}, 1, 3, trials);
    test({-1, -2, -3, -4, -5}, 4, -10, trials);
    test({-5, 1, 2, -3, 4}, 2, 4, trials);
    std::ifstream file("data.txt");
    if (file.is_open())
    {
        std::vector<int> values;
        for (std::string line; std::getline(file, line); )
            values.push_back(std::stoi(line));
        file.close();
        test(values, 7, 49977, trials);
    }
    return 0;
}


