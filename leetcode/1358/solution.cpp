#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfSubstrings(std::string s)
{
    int result = 0, count[3] = {};
    
    for (int l = 0; char c : s)
    {
        ++count[static_cast<int>(c - 'a')];
        while ((count[0] > 0) && (count[1] > 0) && (count[2] > 0))
            --count[static_cast<int>(s[l++] - 'a')];
        result += l;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfSubstrings(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcabc", 10, trials);
    test("aaacb", 3, trials);
    test("abc", 1, trials);
    return 0;
}


