#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

int validSubarraySize(std::vector<int> nums, int threshold)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> prev(n, -1), next(n, n);
    std::stack<int> stack;
    
    for (int i = 0; i < n; ++i)
    {
        while (!stack.empty() && nums[stack.top()] > nums[i])
        {
            int index = stack.top();
            stack.pop();
            next[index] = i;
        }
        if (!stack.empty()) prev[i] = stack.top();
        stack.push(i);
    }
    for (int i = 0; i < n; ++i)
        if (int k = (i - prev[i]) + (next[i] - i) - 1;
            nums[i] > threshold / static_cast<double>(k))
            return k;
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int threshold,
          std::unordered_set<int> solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = validSubarraySize(nums, threshold);
    showResult(solution.find(result) != solution.end(), solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 4, 3, 1}, 6, {3}, trials);
    test({6, 5, 6, 5, 8}, 7, {1, 2, 3, 4, 5}, trials);
    return 0;
}


