#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumLength(std::vector<int> nums)
{
    int dp[2][2] = {};
    for (const int x : nums)
        for (int y = 0; y < 2; ++y)
            dp[x % 2][y] = dp[y][x % 2] + 1;
    return std::max({dp[0][0], dp[0][1], dp[1][0], dp[1][1]});
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumLength(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 4, trials);
    test({1, 2, 1, 1, 2, 1, 2}, 6, trials);
    test({1, 3}, 2, trials);
    return 0;
}


