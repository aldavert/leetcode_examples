#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int chalkReplacer(std::vector<int> chalk, int k)
{
    k = static_cast<int>(k % std::accumulate(chalk.begin(), chalk.end(), 0L));
    if (k == 0) return 0;
    for (int i = 0, n = static_cast<int>(chalk.size()); i < n; ++i)
    {
        k -= chalk[i];
        if (k < 0)
            return i;
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> chalk, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = chalkReplacer(chalk, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 1, 5}, 22, 0, trials);
    test({3, 4, 1, 2}, 25, 1, trials);
    return 0;
}


