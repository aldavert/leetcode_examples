#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minBitFlips(int start, int goal)
{
    int result = 0;
    while (start || goal)
        result += (start & 1) != (goal & 1),
        start >>= 1,
        goal >>= 1;
    return result;
}

// ############################################################################
// ############################################################################

void test(int start, int goal, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minBitFlips(start, goal);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, 7, 3, trials);
    test(3, 4, 3, trials);
    return 0;
}


