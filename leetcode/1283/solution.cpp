#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int smallestDivisor(std::vector<int> nums, int threshold)
{
    int l = 1, r = *std::max_element(nums.begin(), nums.end());
    while (l < r)
    {
        int m = (l + r) / 2, sum = 0;
        for (int num : nums)
            sum += (num - 1) / m + 1;
        if (sum <= threshold) r = m;
        else l = m + 1;
    }
    return l;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int threshold, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestDivisor(nums, threshold);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 5, 9}, 6, 5, trials);
    test({44, 33, 33, 11, 1}, 5, 44, trials);
    return 0;
}


