#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

long long minimumDifference(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size()) / 3;
    long result = std::numeric_limits<long>::max(), left_sum = 0, right_sum = 0;
    std::vector<long> min_left_sum(nums.size());
    
    std::priority_queue<int> max_heap;
    for (int i = 0; i < 2 * n; ++i)
    {
        max_heap.push(nums[i]);
        left_sum += nums[i];
        if (static_cast<int>(max_heap.size()) == n + 1)
            left_sum -= max_heap.top(), max_heap.pop();
        if (static_cast<int>(max_heap.size()) == n)
            min_left_sum[i] = left_sum;
    }
    std::priority_queue<int, std::vector<int>, std::greater<> > min_heap;
    for (int i = static_cast<int>(nums.size()) - 1; i >= n; --i)
    {
        min_heap.push(nums[i]);
        right_sum += nums[i];
        if (static_cast<int>(min_heap.size()) == n + 1)
        right_sum -= min_heap.top(), min_heap.pop();
        if (static_cast<int>(min_heap.size()) == n)
            result = std::min(result, min_left_sum[i - 1] - right_sum);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumDifference(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1, 2}, -1, trials);
    test({7, 9, 5, 8, 1, 3}, 1, trials);
    return 0;
}


