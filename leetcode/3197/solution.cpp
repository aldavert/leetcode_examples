#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumSum(std::vector<std::vector<int> > grid)
{
    constexpr int INF = std::numeric_limits<int>::max() / 4;
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    int result = m * n;
    auto minArea = [&](int i1, int j1, int i2, int j2)
    {
        int x1 = INF, y1 = INF;
        int x2 = -INF, y2 = -INF;
        for (int i = i1; i <= i2; i++)
        {
            for (int j = j1; j <= j2; j++)
            {
                if (grid[i][j] == 1)
                {
                    x1 = std::min(x1, i);
                    y1 = std::min(y1, j);
                    x2 = std::max(x2, i);
                    y2 = std::max(y2, j);
                }
            }
        }
        return ((x1 > x2) || (y1 > y2))?INF:(x2 - x1 + 1) * (y2 - y1 + 1);
    };

    for (int i1 = 0; i1 < m - 1; ++i1)
        for (int i2 = i1 + 1; i2 < m - 1; ++i2)
            result = std::min(result, minArea(0, 0, i1, n - 1)
                                    + minArea(i1 + 1, 0, i2, n - 1)
                                    + minArea(i2 + 1, 0, m - 1, n - 1));
    for (int j1 = 0; j1 < n - 1; ++j1)
        for (int j2 = j1 + 1; j2 < n - 1; ++j2)
            result = std::min(result, minArea(0, 0, m - 1, j1)
                                    + minArea(0, j1 + 1, m - 1, j2)
                                    + minArea(0, j2 + 1, m - 1, n - 1));
    for (int i = 0; i < m - 1; i++)
        for (int j = 0; j < n - 1; j++)
            result = std::min({result,
                    minArea(0, 0, i, j)
                  + minArea(0, j + 1, i, n - 1)
                  + minArea(i + 1, 0, m - 1, n - 1),
                    minArea(0, 0, i, n - 1)
                  + minArea(i + 1, 0, m - 1, j)
                  + minArea(i + 1, j + 1, m - 1, n - 1),
                    minArea(0, 0, i, j)
                  + minArea(i + 1, 0, m - 1, j)
                  + minArea(0, j + 1, m - 1, n - 1),
                    minArea(0, 0, m - 1, j)
                  + minArea(0, j + 1, i, n - 1)
                  + minArea(i + 1, j + 1, m - 1, n - 1)});
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumSum(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0, 1}, {1, 1, 1}}, 5, trials);
    test({{1, 0, 1, 0}, {0, 1, 0, 1}}, 5, trials);
    return 0;
}


