#include "../common/common.hpp"
#include <stack>

class NestedInteger
{
    std::vector<NestedInteger> m_vector;
    int m_value = 0;
    bool m_is_integer = true;
public:
    NestedInteger(int value) :
        m_value(value),
        m_is_integer(true) {}
    NestedInteger(std::initializer_list<NestedInteger> l) :
        m_vector(l),
        m_value(0),
        m_is_integer(false) {}
    bool isInteger(void) const { return m_is_integer; }
    int getInteger(void) const { return m_value; }
    const std::vector<NestedInteger>& getList(void) const { return m_vector; }
};

// ############################################################################
// ############################################################################

class NestedIterator
{
    struct Position
    {
        const std::vector<NestedInteger> * m_nested_list;
        size_t m_position;
        const NestedInteger& current(void) const
        {
            return (*m_nested_list)[m_position];
        }
        bool outOfRange(void) const
        {
            return m_position >= m_nested_list->size();
        }
        void next(void)
        {
            ++m_position;
        }
        bool valid(void) const
        {
            return (m_position < m_nested_list->size())
                && (*m_nested_list)[m_position].isInteger();
        }
    };
    std::stack<Position> m_stack;
public:
    NestedIterator(std::vector<NestedInteger> &nestedList)
    {
        if (nestedList.size() > 0)
        {
            m_stack.push({&nestedList, 0});
            searchInteger();
        }
    }
    int next(void)
    {
        int value = m_stack.top().current().getInteger();
        m_stack.top().next();
        searchInteger();
        return value;
    }
    bool hasNext(void)
    {
        return !m_stack.empty();
    }
private:
    void searchInteger(void)
    {
        while (!m_stack.empty() && !m_stack.top().valid())
        {
            if (m_stack.top().outOfRange())
            {
                m_stack.pop();
                if (!m_stack.empty())
                    m_stack.top().next();
            }
            else if (!m_stack.top().valid())
            {
                auto * ptr = &m_stack.top().current().getList();
                if (ptr->empty()) m_stack.top().next();
                else m_stack.push({ptr, 0});
            }
        };
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<NestedInteger> nested_list,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result.clear();
        NestedIterator iter(nested_list);
        while (iter.hasNext())
            result.push_back(iter.next());
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1}, 2, {1, 1}}, {1, 1, 2, 1, 1}, trials);
    test({1, {4, {6}}}, {1, 4, 6}, trials);
    test({{}, {}, {{}, {}}}, {}, trials);
    test({{}, {2, {}}, {{1}, {}}}, {2, 1}, trials);
    return 0;
}


