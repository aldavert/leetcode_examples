#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

int cherryPickup(std::vector<std::vector<int> > grid)
{
    const int n = static_cast<int>(grid.size());
    int dp[51][51][51];
    std::memset(dp, -1, sizeof(dp));
    dp[1][1][1] = grid[0][0];
    for (int x1 = 1; x1 <= n; ++x1)
    {
        for (int y1 = 1; y1 <= n; ++y1)
        {
            for (int x2 = 1; x2 <= n; ++x2)
            {
                const int y2 = x1 + y1 - x2;
                if (y2 < 1 || y2 > n) continue;
                if ((grid[x1 - 1][y1 - 1] == -1) || (grid[x2 - 1][y2 - 1] == -1))
                    continue;
                const int result = std::max({dp[x1 - 1][y1][x2],
                                             dp[x1 - 1][y1][x2 - 1],
                                             dp[x1][y1 - 1][x2],
                                             dp[x1][y1 - 1][x2 - 1]});
                if (result < 0) continue;
                dp[x1][y1][x2] = result + grid[x1 - 1][y1 - 1];
                if (x1 != x2) dp[x1][y1][x2] += grid[x2 - 1][y2 - 1];
            }
        }
    }
    return std::max(0, dp[n][n][n]);    
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = cherryPickup(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, -1}, {1,  0, -1}, { 1, 1, 1}}, 5, trials);
    test({{1, 1, -1}, {1, -1,  1}, {-1, 1, 1}}, 0, trials);
    return 0;
}


