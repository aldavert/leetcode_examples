#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<std::string> alertNames(std::vector<std::string> keyName,
                                    std::vector<std::string> keyTime)
{
    std::unordered_map<std::string, std::vector<int> > name_to_minutes;
    std::vector<std::string> result;
    auto calcMinutes = [](const std::string &time) -> int
    {
        return 60 * std::stoi(time.substr(0, 2)) + std::stoi(time.substr(3));
    };
    auto hasAlert = [](std::vector<int> &minutes) -> bool
    {
        if (minutes.size() > 70)
            return true;
        std::sort(minutes.begin(), minutes.end());
        for (int i = 2, n = static_cast<int>(minutes.size()); i < n; ++i)
            if (minutes[i - 2] + 60 >= minutes[i])
                return true;
        return false;
    };
    
    for (int i = 0, n = static_cast<int>(keyName.size()); i < n; ++i)
        name_to_minutes[keyName[i]].push_back(calcMinutes(keyTime[i]));
    for (auto& [name, minutes] : name_to_minutes)
        if (hasAlert(minutes))
            result.push_back(name);
    std::sort(result.begin(), result.end());
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> keyName,
          std::vector<std::string> keyTime,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = alertNames(keyName, keyTime);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"daniel", "daniel", "daniel", "luis", "luis", "luis", "luis"},
         {"10:00", "10:40", "11:00", "09:00", "11:00", "13:00", "15:00"},
         {"daniel"}, trials);
    test({"alice", "alice", "alice", "bob", "bob", "bob", "bob"},
         {"12:01", "12:00", "18:00", "21:00", "21:20", "21:30", "23:00"},
         {"bob"}, trials);
    return 0;
}


