#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> lexicographicallySmallestArray(std::vector<int> nums, int limit)
{
    std::vector<int> result(nums.size());
    std::vector<std::vector<std::pair<int, int> > > groups;
    std::vector<std::pair<int, int> > enumerate_nums;
    
    for (size_t i = 0; i < nums.size(); ++i)
        enumerate_nums.emplace_back(nums[i], i);
    std::sort(enumerate_nums.begin(), enumerate_nums.end());
    for (const auto &num_and_index : enumerate_nums)
    {
        if (groups.empty()
        ||  (num_and_index.first - groups.back().back().first > limit))
            groups.push_back({num_and_index});
        else groups.back().push_back(num_and_index);
    }
    for (const auto &group : groups)
    {
        std::vector<int> sorted_nums, sorted_indices;
        for (auto [num, index] : group)
        {
            sorted_nums.push_back(num);
            sorted_indices.push_back(index);
        }
        std::sort(sorted_indices.begin(), sorted_indices.end());
        for (size_t i = 0; i < sorted_nums.size(); ++i)
            result[sorted_indices[i]] = sorted_nums[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int limit,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = lexicographicallySmallestArray(nums, limit);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 5, 3, 9, 8}, 2, {1, 3, 5, 8, 9}, trials);
    test({1, 7, 6, 18, 2, 1}, 3, {1, 6, 7, 18, 1, 2}, trials);
    test({1, 7, 28, 19, 10}, 3, {1, 7, 28, 19, 10}, trials);
    return 0;
}


