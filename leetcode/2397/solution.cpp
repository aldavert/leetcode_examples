#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumRows(std::vector<std::vector<int> > matrix, int numSelect)
{
    int m = static_cast<int>(matrix.size()),
        n = static_cast<int>(matrix[0].size());
    std::vector<int> rows(m);
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (matrix[i][j])
                rows[i] |= 1 << j;
    int result = 0;
    for (int mask = 1; mask < 1 << n; ++mask)
    {
        if (__builtin_popcount(mask) != numSelect)
            continue;
        int t = 0;
        for (int x : rows)
            t += (x & mask) == x;
        result = std::max(result, t);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix,
          int numSelect,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumRows(matrix, numSelect);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0, 0}, {1, 0, 1}, {0, 1, 1}, {0, 0, 1}}, 2, 3, trials);
    test({{1}, {0}}, 1, 2, trials);
    return 0;
}


