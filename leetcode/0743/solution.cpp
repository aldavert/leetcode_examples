#include "../common/common.hpp"
#include <queue>
#include <limits>

// ############################################################################
// ############################################################################

int networkDelayTime(const std::vector<std::vector<int> > &times, int n, int k)
{
    struct Edge 
    {
        int destination = -1;
        int cost = -1;
        bool operator<(const Edge &other) const { return cost > other.cost; }
    };
    struct Info
    {
        int cost = std::numeric_limits<int>::max();
        std::vector<Edge> neighbors;
    };
    std::vector<Info> nodes(n + 1);
    nodes[0].cost = 0;
    for (const auto &t : times)
        nodes[t[0]].neighbors.push_back({t[1], t[2]});
    std::priority_queue<Edge> q;
    q.push({k, 0});
    while (!q.empty())
    {
        auto [node, cost] = q.top();
        q.pop();
        if (nodes[node].cost > cost)
        {
            nodes[node].cost = cost;
            for (auto [d, c] : nodes[node].neighbors)
                q.push({d, c + cost});
        }
    }
    int max_cost = 0;
    for (const auto &node : nodes)
        max_cost = std::max(max_cost, node.cost);
    return (max_cost < std::numeric_limits<int>::max())?max_cost:-1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > times,
          int n,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = networkDelayTime(times, n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 1, 1}, {2, 3, 1}, {3, 4, 1}}, 4, 2, 2, trials);
    test({{1, 2, 1}}, 2, 1, 1, trials);
    test({{1, 2, 1}}, 2, 2, -1, trials);
    return 0;
}


