#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

#if 1
int maximumScore(std::vector<int> nums, std::vector<int> multipliers)
{
    const int n = static_cast<int>(nums.size());
    const int m = static_cast<int>(multipliers.size());
    std::vector<int> dp(m + 1);
    dp[0] = 0;
    for (int i = 0; i < m; ++i)
    {
        const int mult = multipliers[i];
        int l = i;
        int r = n - 1;
        int previous = dp[0];
        dp[0] += mult * nums[l--];
        for (int k = 1; k <= i; ++k, --l, --r)
        {
            int new_value = std::max(previous + mult * nums[r], dp[k] + mult * nums[l]);
            previous = std::exchange(dp[k], new_value);
        }
        dp[i + 1] = previous + mult * nums[r];
    }
    int result = dp[0];
    for (int i = 1; i <= m; ++i)
        result = std::max(result, dp[i]);
    return result;
}
#else
int maximumScore(std::vector<int> nums, std::vector<int> multipliers)
{
    constexpr int EMPTY = std::numeric_limits<int>::lowest();
    const int n = static_cast<int>(nums.size());
    const int m = static_cast<int>(multipliers.size());
    std::vector<int> lut(m * m, EMPTY);
    auto dp = [&](auto &&self, int b, int p) -> int
    {
        if (p == m) return 0;
        const int id = m * p + b;
        if (lut[id] != EMPTY) return lut[id];
        int e = b + n - p - 1;
        return lut[id] = std::max(nums[b] * multipliers[p] + self(self, b + 1, p + 1),
                                  nums[e] * multipliers[p] + self(self, b    , p + 1));
    };
    return dp(dp, 0, 0);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> multipliers,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumScore(nums, multipliers);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, {3, 2, 1}, 14, trials);
    test({-5, -3, -3, -2, 7, 1}, {-10, -5, 3, 4, 6}, 102, trials);
    test({0, 0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0,  0, 0, 0},  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, -1, trials);
    return 0;
}


