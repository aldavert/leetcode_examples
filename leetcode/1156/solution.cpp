#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxRepOpt1(std::string text)
{
    int histogram[26] = {};
    
    for (char c : text) ++histogram[c - 'a'];
    std::vector<std::pair<char, int> > groups{{text[0], 1}};
    for (int i = 1, n = static_cast<int>(text.size()); i < n; ++i)
    {
        if (text[i] == text[i - 1]) ++groups[groups.size() - 1].second;
        else groups.emplace_back(text[i], 1);
    }
    int result = 0;
    for (auto [c, length] : groups)
        result = std::max(result, std::min(length + 1, histogram[c - 'a']));
    for (int i = 1, n = static_cast<int>(groups.size()); i + 1 < n; ++i)
        if ((groups[i - 1].first == groups[i + 1].first)
        &&  (groups[i].second == 1))
            result = std::max(result,
                              std::min(groups[i - 1].second + groups[i + 1].second + 1,
                                       histogram[groups[i - 1].first - 'a']));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string text, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxRepOpt1(text);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ababa", 3, trials);
    test("aaabaaa", 6, trials);
    test("aaaaa", 5, trials);
    return 0;
}


