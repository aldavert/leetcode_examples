#include "../common/common.hpp"
#include <unordered_map>
#include <queue>

// ############################################################################
// ############################################################################

std::vector<std::string> topKFrequent(std::vector<std::string> words, int k)
{
    std::unordered_map<std::string, int> histogram;
    std::vector<std::string> ordered_strings[501];
    for (auto word: words)
        ++histogram[word];
    for (auto [word, frequency] : histogram)
        ordered_strings[frequency].push_back(word);
    std::vector<std::string> result(k);
    for (int i = 500, index = 0; (i >= 0) && (k > 0); --i)
    {
        if (ordered_strings[i].size() > 0)
        {
            if (static_cast<int>(ordered_strings[i].size()) <= k)
            {
                std::sort(ordered_strings[i].begin(), ordered_strings[i].end());
                for (std::string word : ordered_strings[i])
                    result[index++] = word;
                k -= static_cast<int>(ordered_strings[i].size());
            }
            else
            {
                std::priority_queue<std::string> queue;
                for (std::string word : ordered_strings[i])
                {
                    queue.push(word);
                    if (static_cast<int>(queue.size()) > k)
                        queue.pop();
                }
                for (int j = static_cast<int>(result.size() - 1);
                     !queue.empty(); --j, queue.pop())
                    result[j] = queue.top();
                k = 0;
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          int k,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = topKFrequent(words, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"i", "love", "leetcode", "i", "love", "coding"}, 2,
         {"i", "love"}, trials);
    test({"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"}, 4,
         {"the", "is", "sunny", "day"}, trials);
    return 0;
}


