#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> >
constructProductMatrix(std::vector<std::vector<int> > grid)
{
    constexpr long MOD = 12345;
    const int n = static_cast<int>(grid.size()),
              m = static_cast<int>(grid[0].size());
    std::vector<std::vector<int> > result(n, std::vector<int>(m));
    std::vector<long> prefix{1};
    long suffix = 1;
    for (const auto &row : grid)
        for (const int cell : row)
            prefix.push_back(static_cast<long>(prefix.back()) * cell % MOD);
    for (int i = n - 1; i >= 0; --i)
    {
        for (int j = m - 1; j >= 0; --j)
        {
            result[i][j] = static_cast<int>(prefix[i * m + j] * suffix % MOD);
            suffix = static_cast<int>(static_cast<long>(suffix) * grid[i][j] % MOD);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = constructProductMatrix(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {3, 4}}, {{24, 12}, {8, 6}}, trials);
    test({{12345}, {2}, {1}}, {{2}, {0}, {0}}, trials);
    return 0;
}


