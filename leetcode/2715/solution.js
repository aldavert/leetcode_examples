var cancellable = function(fn, args, t) {
    let timeout_id;
    const cancelFn = () =>  clearTimeout(timeout_id);
    timeout_id = setTimeout(() => fn(...args), t);
    return cancelFn;  
};
