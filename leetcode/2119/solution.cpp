#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isSameAfterReversals(int num)
{
    return (num == 0) || (num % 10 != 0);
}

// ############################################################################
// ############################################################################

void test(int num, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isSameAfterReversals(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(526, true, trials);
    test(1800, false, trials);
    test(0, true, trials);
    return 0;
}


