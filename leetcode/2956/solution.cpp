#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findIntersectionValues(std::vector<int> nums1,
                                        std::vector<int> nums2)
{
    std::vector<int> result(2, 0);
    int histogram1[101] = {}, histogram2[101] = {};
    for (int value : nums1)
        ++histogram1[value];
    for (int value : nums2)
        ++histogram2[value];
    for (size_t i = 0; i <= 100; ++i)
    {
        result[0] += ((histogram1[i] > 0) && (histogram2[i] > 0)) * histogram1[i];
        result[1] += ((histogram1[i] > 0) && (histogram2[i] > 0)) * histogram2[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findIntersectionValues(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 3, 2, 3, 1}, {2, 2, 5, 2, 3, 6}, {3, 4}, trials);
    test({3, 4, 2, 3}, {1, 5}, {0, 0}, trials);
    return 0;
}


