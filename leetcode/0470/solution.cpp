#include "../common/common.hpp"
#include <limits>
#include <random>

int rand7()
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distribution(1, 7);
    return distribution(gen);
}

// ############################################################################
// ############################################################################

#if 1
int rand10()
{
    long result = 0;
    for (int i = 0, factor = 1; i < 10; ++i, factor *= 7)
        result += (rand7() - 1) * factor;
    return static_cast<int>(result % 10) + 1;
}
#else
int rand10()
{
    int value = 40;
    while (value >= 40) value = (rand7() - 1) * 7 + rand7() - 1;
    return value % 10 + 1;
}
#endif

// ############################################################################
// ############################################################################

void test(unsigned int trials)
{
    int histogram[11] = {};
    bool correct = true;
    for (unsigned int i = 0; i < trials; ++i)
    {
        int result = rand10();
        correct = correct && (result >= 1) && (result <= 10);
        if (correct) ++histogram[result];
    }
    bool valid = false;
    std::string message;
    if (correct)
    {
        const double expected = static_cast<double>(trials) / 10.0;
        double chi_test = 0.0;
        for (int i = 1; i <= 10; ++i)
            chi_test += (static_cast<double>(histogram[i]) - expected)
                      * (static_cast<double>(histogram[i]) - expected)
                      / expected;
        // From. D.E. Knuth, "The Art of Computer Programming. Volume 2", Addison-Wesley 1969
        std::tuple<double, double> probability[] = {
            {1, 2.088}, {5, 3.325}, {25, 5.899}, {50, 8.343},
            {75, 11.39}, {95, 16.92}, {99, 21.67},
            {100, std::numeric_limits<double>::max()} };
        int index = 0;
        for (int i = 0; i < 8; ++i)
        {
            if (chi_test < std::get<1>(probability[i]))
            {
                index = i;
                break;
            }
        }
        double likelihood = 100.0 - std::get<0>(probability[index]);
        valid = (likelihood >= 50);
        message = "Random distribution within range with a likelihood of "
                + std::to_string(likelihood) + "% of being uniform "
                + "(chi-test = " + std::to_string(chi_test) + ").\n";
    }
    else message = "The probability distribution falls out of range.\n";
    showResult(valid, "", message);
}

int main(int, char **)
{
    test(10'000);
    return 0;
}


