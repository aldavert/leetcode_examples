#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int longestUnivaluePath(TreeNode * root)
{
    auto compute = [](TreeNode * root_node) -> int
    {
        int result = 0;
        auto inner = [&result](auto &&self, TreeNode * node) -> std::tuple<int, int>
        {
            if (node == nullptr) return {-2000, 0};
            auto [left_value, left_counter] = self(self, node->left);
            auto [right_value, right_counter] = self(self, node->right);
            int length = 0;
            if (left_value == node->val)
                length = left_counter;
            if (right_value == node->val)
            {
                result = std::max(result, length + right_counter);
                length = std::max(length, right_counter);
            }
            result = std::max(result, length);
            return {node->val, length + 1};
        };
        inner(inner, root_node);
        return result;
    };
    return compute(root);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = longestUnivaluePath(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 5, 1, 1, null, 5}, 2, trials);
    test({1, 4, 5, 4, 4, null, 5}, 2, trials);
    test({1, 4, 5, 4, 4, null, 5, 1, 4, 3, 2, null, null, 4, 4, 4, 4, 3, 3, 3, 3},
          4, trials);
    return 0;
}


