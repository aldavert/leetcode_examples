# Longest Univalue Path

Given the `root` of a binary tree, return *the length of the longest path, where each node in the path has the same value*. This path may or may not pass through the root.

**The length of the path** between two nodes is represented by the number of edges between them.

#### Example 1:
> ```mermaid 
> graph TD;
> A((5))---B((4))
> A-->C((5))
> B---D((1))
> B---E((1))
> C---EMPTY(( ))
> C-->F((5))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#FAA,stroke:#822,stroke-width:2px;
> class A,C,F selected;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class EMPTY empty;
> linkStyle 1,5 stroke-width:4px,stroke:red;
> linkStyle 4 stroke-width:0px;
> ```
> *Input:* `root = [5, 4, 5, 1, 1, null, 5]`  
> *Output:* `2`  
> *Explanation:* The longest path is marked on the diagram with red arrows.

#### Example 2:
> ```mermaid 
> graph TD;
> A((1))---B((4))
> A-->C((5))
> B-->D((4))
> B---E((4))
> E-->B
> E---B
> C---EMPTY(( ))
> C-->F((5))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#FAA,stroke:#822,stroke-width:2px;
> class B,D,E selected;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class EMPTY empty;
> linkStyle 2,4 stroke-width:4px,stroke:red;
> linkStyle 3,5,6 stroke-width:0px;
> ```
> *Input:* `root = [1, 4, 5, 4, 4, null, 5]`  
> *Output:* `2`  
> *Explanation:* The longest path is marked on the diagram with red arrows.

#### Constraints:
- The number of nodes in the tree is in the range `[0, 10^4]`.
- `-1000 <= Node.val <= 1000`
- The depth of the tree will not exceed `1000`.


