#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> secondGreaterElement(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> result(n, -1), stack_one, stack_two, tmp;
    
    for(auto i = 0; i != n; ++i)
    {
        while (!stack_two.empty() && (nums[stack_two.back()] < nums[i]))
            result[stack_two.back()] = nums[i],
            stack_two.pop_back();
        while (!stack_one.empty() && (nums[stack_one.back()] < nums[i]))
            tmp.push_back(stack_one.back()), stack_one.pop_back();
        while (!tmp.empty())
            stack_two.push_back(tmp.back()), tmp.pop_back();
        stack_one.push_back(i);
    }
    return result;
}
#else
std::vector<int> secondGreaterElement(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::stack<int> stack_previous, stack_current;
    std::vector<int> result(n, -1);
    
    for (int i = 0; i < n; ++i)
    {
        while (!stack_previous.empty() && nums[stack_previous.top()] < nums[i])
            result[stack_previous.top()] = nums[i], stack_previous.pop();
        std::stack<int> decreasing_indices;
        while (!stack_current.empty() && (nums[stack_current.top()] < nums[i]))
            decreasing_indices.push(stack_current.top()),
            stack_current.pop();
        while (!decreasing_indices.empty())
            stack_previous.push(decreasing_indices.top()),
            decreasing_indices.pop();
        stack_current.push(i);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = secondGreaterElement(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4, 0, 9, 6}, {9, 6, 6, -1, -1}, trials);
    test({3, 3}, {-1, -1}, trials);
    return 0;
}


