#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> getSneakyNumbers(std::vector<int> nums)
{
    constexpr int MAX = 100;
    int count[MAX + 1] = {};
    std::vector<int> result;
    for (int num : nums)
        if (++count[num] == 2)
            result.push_back(num);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left,
                const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<int> work_left(left), work_right(right);
    std::sort(work_left.begin(), work_left.end());
    std::sort(work_right.begin(), work_right.end());
    for (size_t i = 0; i < work_left.size(); ++i)
        if (work_left[i] != work_right[i])
            return false;
    return true;
}

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getSneakyNumbers(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 1, 0}, {0, 1}, trials);
    test({0, 3, 2, 1, 3, 2}, {2, 3}, trials);
    test({7, 1, 5, 4, 3, 4, 6, 0, 9, 5, 8, 2}, {4, 5}, trials);
    return 0;
}


