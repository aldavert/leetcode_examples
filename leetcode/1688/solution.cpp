#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfMatches(int n)
{
    int result = 0;
    while (n > 1)
    {
        result += n / 2;
        n = n / 2 + (n & 1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfMatches(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, 6, trials);
    test(14, 13, trials);
    return 0;
}


