#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int waysToMakeFair(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = 0, left[2] = {}, right[2] = {};
    
    for (int i = 0; i < n; ++i)
        right[i % 2] += nums[i];
    for (int i = 0; i < n; ++i)
    {
        right[i % 2] -= nums[i];
        result += (left[0] + right[1] == left[1] + right[0]);
        left[i % 2] += nums[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = waysToMakeFair(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 6, 4}, 1, trials);
    test({1, 1, 1}, 3, trials);
    test({1, 2, 3}, 0, trials);
    return 0;
}


