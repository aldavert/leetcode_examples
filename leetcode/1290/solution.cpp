#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

int getDecimalValue(ListNode * head)
{
    int result = 0;
    for (ListNode * ptr = head; ptr != nullptr; ptr = ptr->next)
        result = (result << 1) + ptr->val;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> input, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * linput = vec2list(input);
        result = getDecimalValue(linput);
        delete linput;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 1}, 5, trials);
    test({0}, 0, trials);
    test({1}, 1, trials);
    test({1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0}, 18880, trials);
    test({0, 0}, 0, trials);
    return 0;
}


