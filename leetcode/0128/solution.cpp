#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int longestConsecutive(std::vector<int> nums)
{
    std::unordered_set<int> seen(nums.begin(), nums.end());
    int result = 0;
    for (int v : seen)
    {
        if (seen.find(v - 1) != seen.end()) continue;
        int begin = v;
        while (seen.find(++v) != seen.end());
        result = std::max(result, v - begin);
    }
    return result;
}
#else
// The complexity is only O(n) when the hash table can perfectly accommodate
// the sequence inside so the query complexity is O(1). In the worse case
// scenario the complexity of the algorithm will be O(n^2) otherwise.
int longestConsecutive(std::vector<int> nums)
{
    std::unordered_set<int> seen(nums.begin(), nums.end());
    int result = 0;
    for (int v : seen)
    {
        // Check if we are at the beginning of the sequence.
        if (seen.find(v - 1) != seen.end()) continue;
        // If we are at the beginning, we save the starting point.
        int begin = v;
        // Increase the value until it can not be found anymore.
        while (seen.find(++v) != seen.end());
        // Check if the run-length is the longest in the sequence.
        result = std::max(result, v - begin);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestConsecutive(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({100, 4, 200, 1, 3, 2}, 4, trials);
    test({0, 3, 7, 2, 5, 8, 4, 6, 0, 1}, 9, trials);
    return 0;
}


