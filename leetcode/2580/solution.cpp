#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countWays(std::vector<std::vector<int> > ranges)
{
    constexpr int MOD = 1'000'000'007;
    int result = 1;
    std::sort(ranges.begin(), ranges.end());
    for (int prev_end = -1; const auto &range : ranges)
    {
        if (range[0] > prev_end)
            result = result * 2 % MOD;
        prev_end = std::max(prev_end, range[1]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > ranges, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countWays(ranges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{6, 10}, {5, 15}}, 2, trials);
    test({{1, 3}, {10, 20}, {2, 5}, {4, 8}}, 4, trials);
    return 0;
}


