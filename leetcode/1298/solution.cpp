#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int maxCandies(std::vector<int> status,
               std::vector<int> candies,
               std::vector<std::vector<int> > keys,
               std::vector<std::vector<int> > containedBoxes,
               std::vector<int> initialBoxes)
{
    auto work = status;
    int result = 0;
    std::queue<int> q;
    bool reached_closed_boxes[1001] = {};
    auto pushBoxesIfPossible = [&](const std::vector<int> &boxes)
    {
        for (int box : boxes)
            if (work[box]) q.push(box);
            else reached_closed_boxes[box] = true;
    };
    pushBoxesIfPossible(initialBoxes);
    while (!q.empty())
    {
        int current_box = q.front();
        q.pop();
        result += candies[current_box];
        for (int key : keys[current_box])
        {
            if (!work[key] && reached_closed_boxes[key])
                q.push(key);
            work[key] = 1;
        }
        pushBoxesIfPossible(containedBoxes[current_box]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> status,
          std::vector<int> candies,
          std::vector<std::vector<int> > keys,
          std::vector<std::vector<int> > containedBoxes,
          std::vector<int> initialBoxes,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxCandies(status, candies, keys, containedBoxes, initialBoxes);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 1, 0}, {7, 5, 4, 100}, {{}, {}, {1}, {}}, {{1, 2}, {3}, {}, {}},
         {0}, 16,  trials);
    test({1, 0, 0, 0, 0, 0}, {1, 1, 1, 1, 1, 1},
         {{1, 2, 3, 4, 5}, {}, {}, {}, {}, {}},
         {{1, 2, 3, 4, 5}, {}, {}, {}, {}, {}}, {0}, 6, trials);
    return 0;
}


