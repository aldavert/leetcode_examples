#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findTheLongestBalancedSubstring(std::string s)
{
    int number_of_digits[2] = {};
    int result = 0;
    for (char previous = '\0'; char digit : s)
    {
        if (previous != digit)
        {
            if (previous == '1')
                result = std::max(result,
                                  std::min(number_of_digits[0], number_of_digits[1]));
            number_of_digits[digit - '0'] = 1;
        }
        else ++number_of_digits[digit - '0'];
        previous = digit;
    }
    if (s.back() == '1')
        return 2 * std::max(result, std::min(number_of_digits[0], number_of_digits[1]));
    else return 2 * result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findTheLongestBalancedSubstring(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("01000111", 6, trials);
    test("00111", 4, trials);
    test("111", 0, trials);
    test("10", 0, trials);
    test("010", 2, trials);
    return 0;
}


