#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> commonChars(std::vector<std::string> words)
{
    int histogram_common[26];
    for (int i = 0; i < 26; ++i)
        histogram_common[i] = std::numeric_limits<int>::max();
    for (std::string word : words)
    {
        int histogram_current[26] = {};
        for (char c : word)
            ++histogram_current[c - 'a'];
        for (int i = 0; i < 26; ++i)
            histogram_common[i] = std::min(histogram_common[i], histogram_current[i]);
    }
    std::vector<std::string> result;
    char char_string[] = ".";
    for (int i = 0; i < 26; ++i)
    {
        for (int j = 0; j < histogram_common[i]; ++j)
        {
            char_string[0] = static_cast<char>(i + 'a');
            result.push_back(char_string);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

template <typename T>
bool operator==(const std::vector<T> &left, const std::vector<T> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<T> sort_left(left), sort_right(right);
    std::sort(sort_left.begin(), sort_left.end());
    std::sort(sort_right.begin(), sort_right.end());
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (!(sort_left[i] == sort_right[i])) return false;
    return true;
}

void test(std::vector<std::string> words,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = commonChars(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"bella", "label", "roller"}, {"e", "l", "l"}, trials);
    test({"cool", "lock", "cook"}, {"c", "o"}, trials);
    return 0;
}


