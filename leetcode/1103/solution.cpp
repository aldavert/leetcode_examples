#include "../common/common.hpp"

// ###########################################################################
// ###########################################################################

std::vector<int> distributeCandies(int candies, int num_people)
{
    std::vector<int> result(num_people, 0);
    for (int i = 0; candies; ++i)
    {
        result[i % num_people] += std::min(candies, i + 1);
        candies = std::max(0, candies - (i + 1));
    }
    return result;
}

// ###########################################################################
// ###########################################################################

void test(int candies,
          int num_people,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = distributeCandies(candies, num_people);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test( 7, 4, {1, 2, 3, 1}, trials);
    test(10, 3, {5, 2, 3}, trials);
    return 0;
}


