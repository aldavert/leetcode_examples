#include "../common/common.hpp"
#include <stack>

class NestedInteger
{
    std::vector<NestedInteger> m_list;
    int m_value = 0;
    bool m_integer = false;
public:
     NestedInteger(void) = default;
     NestedInteger(int value) : m_value(value), m_integer(true) {}
     inline bool isInteger() const { return m_integer; }
     int getInteger() const { return m_value; }
     void setInteger(int value)
     {
         m_integer = true;
         m_value = value;
         m_list.clear();
     }
     void add(const NestedInteger &ni)
     {
         m_integer = false;
         m_list.push_back(ni);
     }
     const std::vector<NestedInteger>& getList() const { return m_list; }
     bool operator==(const NestedInteger &other) const
     {
         if (m_integer != other.m_integer) return false;
         if (m_integer)
             return m_value == other.m_value;
         else
         {
             if (m_list.size() != other.m_list.size()) return false;
             for (size_t i = 0; i < m_list.size(); ++i)
                 if (!(m_list[i] == other.m_list[i]))
                     return false;
             return true;
         }
     }
 };

std::ostream& operator<<(std::ostream &out, const NestedInteger &ni)
{
    if (ni.isInteger()) out << ni.getInteger();
    else
    {
        out << '{';
        for (bool next = false; const auto &value : ni.getList())
        {
            if (next) [[likely]] out << ", ";
            next = true;
            out << value;
        }
        out << '}';
    }
    return out;
}

// ############################################################################
// ############################################################################

NestedInteger deserialize(std::string s)
{
    std::stack<NestedInteger> stack;
    bool negative = false, value_set = false;
    int value = 0;
    for (char letter : s)
    {
        if (letter == '[')
        {
            stack.push({});
            negative = value_set = false;
            value = 0;
        }
        else if (letter == ']')
        {
            if (value_set)
                stack.top().add(NestedInteger((1 - 2 * negative) * value));
            auto current = stack.top();
            if (stack.size() == 1)
                return stack.top();
            stack.pop();
            stack.top().add(current);
            negative = value_set = false;
            value = 0;
        }
        else if (letter == ',')
        {
            if (value_set)
                stack.top().add(NestedInteger((1 - 2 * negative) * value));
            negative = value_set = false;
            value = 0;
        }
        else if (letter == '-') value_set = negative = true;
        else
        {
            value = value * 10 + (letter - '0');
            value_set = true;
        }
    }
    if (value_set) return NestedInteger((1 - 2 * negative) * value);
    return {};
}

// ############################################################################
// ############################################################################

void test(std::string s, NestedInteger solution, unsigned int trials = 1)
{
    NestedInteger result;
    for (unsigned int i = 0; i < trials; ++i)
        result = deserialize(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("324", {324}, trials);
    {
        NestedInteger solution, second, third;
        solution.add(NestedInteger(123));
        second.add(NestedInteger(456));
        third.add(NestedInteger(789));
        second.add(third);
        solution.add(second);
        test("[123,[456,[789]]]", solution, trials);
    }
    {
        NestedInteger solution;
        solution.add(NestedInteger(-123));
        solution.add(NestedInteger(-789));
        test("[-123,-789]", solution, trials);
    }
    test("-324", {-324}, trials);
    return 0;
}


