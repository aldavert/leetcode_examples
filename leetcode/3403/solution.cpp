#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string answerString(std::string word, int numFriends)
{
    if (numFriends == 1) return word;
    int i = 0, j = 1, k = 0;
    for (const int n = static_cast<int>(word.size()); j + k < n; )
    {
        if (word[i + k] == word[j + k]) ++k;
        else if (word[i + k] > word[j + k])
        {
            j = j + k + 1;
            k = 0;
        }
        else
        {
            i = std::max(i + k + 1, j);
            j = i + 1;
            k = 0;
        }
    }
    std::string s = word.substr(i);
    return s.substr(0, std::min(s.size(), word.size() - numFriends + 1));
}

// ############################################################################
// ############################################################################

void test(std::string word,
          int numFriends,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = answerString(word, numFriends);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("dbca", 2, "dbc", trials);
    test("gggg", 4, "g", trials);
    return 0;
}


