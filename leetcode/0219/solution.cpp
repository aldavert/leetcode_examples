#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

bool containsNearbyDuplicate(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::unordered_map<int, int> histogram;
    
    for (int i = 0; i < std::min(k, n); ++i)
        ++histogram[nums[i]];
    for (int i = 0; i < n; ++i)
    {
        --histogram[nums[i]];
        if (i + k < n)
            ++histogram[nums[i + k]];
        if (histogram[nums[i]])
            return true;
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = containsNearbyDuplicate(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 1}, 3, true, trials);
    test({1, 0, 1, 1}, 1, true, trials);
    test({1, 2, 3, 1, 2, 3}, 2, false, trials);
    test({1}, 1, false, trials);
    return 0;
}


