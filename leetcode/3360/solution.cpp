#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canAliceWin(int n)
{
    bool alice = true;
    for (int i = 10; (n >= 0) && (i > 0); n -= i, --i, alice = !alice);
    return alice;
}

// ############################################################################
// ############################################################################

void test(int n, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canAliceWin(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(12, true, trials);
    test(1, false, trials);
    test(10, true, trials);
    return 0;
}


