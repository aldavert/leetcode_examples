#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

std::vector<int> findMissingAndRepeatedValues(std::vector<std::vector<int> > grid)
{
    std::bitset<50 * 50 + 1> present;
    std::vector<int> result(2, 0);
    for (const auto &row : grid)
    {
        for (const int value : row)
        {
            if (present[value]) result[0] = value;
            present[value] = true;
        }
    }
    for (size_t i = 1, n = grid.size() * grid.size(); i <= n; ++i)
    {
        if (!present[i])
        {
            result[1] = static_cast<int>(i);
            break;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMissingAndRepeatedValues(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 3}, {2, 2}}, {2, 4}, trials);
    test({{9, 1, 7}, {8, 9, 2}, {3, 4, 6}}, {9, 5}, trials);
    return 0;
}


