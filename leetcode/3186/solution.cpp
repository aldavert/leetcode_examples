#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
long long maximumTotalDamage(std::vector<int> power)
{
    const int n = static_cast<int>(power.size());
    std::sort(power.begin(), power.end());
    std::vector<int> unique{power[0]};
    std::vector<long long> count{power[0]};
    for (int i = 1; i < n; ++i)
    {
        if (power[i] == power[i - 1])
            count.back() += power[i];
        else
        {
            unique.push_back(power[i]);
            count.push_back(power[i]);
        }
    }
    const int m = static_cast<int>(unique.size());
    std::vector<long long> dp(m, 0);
    for (int i = 0; i < m; ++i)
    {
        dp[i] = count[i];
        for (int j = std::max(i - 3, 0); j < i; ++j)
            dp[i] = std::max(dp[i], dp[j] + ((unique[i] > unique[j] + 2)?count[i]:0));
    }
    return dp.back();
}
#else
long long maximumTotalDamage(std::vector<int> power)
{
    std::unordered_map<int, int> count;
    
    for (int damage : power) ++count[damage];
    std::vector<int> unique_damages;
    for (const auto& [damage, _] : count)
        unique_damages.push_back(damage);
    std::sort(unique_damages.begin(), unique_damages.end());
    const int n = static_cast<int>(unique_damages.size());
    std::vector<long> dp[2] = { std::vector<long>(n), std::vector<long>(n) };
    
    for (int i = 0; i < n; ++i)
    {
        int damage = unique_damages[i];
        if (i == 0)
        {
            dp[0][0] = 0;
            dp[1][0] = static_cast<long>(damage) * count[damage];
            continue;
        }
        dp[0][i] = std::max(dp[0][i - 1], dp[1][i - 1]);
        dp[1][i] = damage * count[damage];
        if ((i >= 1)
        &&  (unique_damages[i - 1] != damage - 1)
        &&  (unique_damages[i - 1] != damage - 2))
            dp[1][i] += std::max(dp[0][i - 1], dp[1][i - 1]);
        else if ((i >= 2) && (unique_damages[i - 2] != damage - 2))
            dp[1][i] += std::max(dp[0][i - 2], dp[1][i - 2]);
        else if (i >= 3)
            dp[1][i] += std::max(dp[0][i - 3], dp[1][i - 3]);
    }
    return std::max(dp[0].back(), dp[1].back());
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> power, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumTotalDamage(power);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 3, 4}, 6, trials);
    test({7, 1, 6, 6}, 13, trials);
    return 0;
}


