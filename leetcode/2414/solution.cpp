#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestContinuousSubstring(std::string s)
{
    int result = 1;
    for (int i = 1, running_len = 1, n = static_cast<int>(s.size()); i < n; ++i)
    {
        if (s[i] == s[i - 1] + 1) result = std::max(result, ++running_len);
        else running_len = 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestContinuousSubstring(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abacaba", 2, trials);
    test("abcde", 5, trials);
    return 0;
}


