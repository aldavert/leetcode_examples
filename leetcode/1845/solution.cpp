#include "../common/common.hpp"
#include <queue>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class SeatManager
{
    std::priority_queue<int, std::vector<int>, std::greater<int> > m_free_seats;
    std::vector<bool> m_available_seats;
public:
    SeatManager(int n) : m_available_seats(n + 1, true)
    {
        for (int i = 1; i <= n; ++i)
            m_free_seats.push(i);
    }
    int reserve()
    {
        while (!m_free_seats.empty())
        {
            int seat = m_free_seats.top();
            m_free_seats.pop();
            if (m_available_seats[seat])
                return seat;
        }
        return -1;
    }
    void unreserve(int seatNumber)
    {
        m_available_seats[seatNumber] = true;
        m_free_seats.push(seatNumber);
    }
};

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<bool> reserve,
          std::vector<int> seat_number,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        SeatManager object(n);
        for (size_t i = 0; i < reserve.size(); ++i)
        {
            if (reserve[i]) result.push_back(object.reserve());
            else
            {
                object.unreserve(seat_number[i]);
                result.push_back(null);
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5,
         {true, true, false, true, true, true, true, false},
         {null, null,     2, null, null, null, null,     5},
         {   1,    2,  null,    2,    3,    4,    5,  null}, trials);
    return 0;
}


