#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool searchMatrix(std::vector<std::vector<int> > matrix, int target)
{
    const int m = static_cast<int>(matrix.size()),
              n = static_cast<int>(matrix[0].size());
    std::vector<int> first_row(m);
    for (int i = 0; i < m; ++i)
        first_row[i] = matrix[i][0];
    auto selected_row =
        std::distance(first_row.begin(),
                      std::upper_bound(first_row.begin(), first_row.end(), target)) - 1;
    if ((selected_row >= 0) && (selected_row < m))
    {
        auto selected_column =
             std::distance(matrix[selected_row].begin(),
                           std::lower_bound(matrix[selected_row].begin(),
                                            matrix[selected_row].end(), target));
        return (selected_column < n)
            && (matrix[selected_row][selected_column] == target);
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix,
          int target,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = searchMatrix(matrix, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 3, 5, 7}, {10, 11, 16, 20}, {23, 30, 34, 60}}, -1, false, trials);
    test({{1, 3, 5, 7}, {10, 11, 16, 20}, {23, 30, 34, 60}},  9, false, trials);
    test({{1, 3, 5, 7}, {10, 11, 16, 20}, {23, 30, 34, 60}},  3,  true, trials);
    test({{1, 3, 5, 7}, {10, 11, 16, 20}, {23, 30, 34, 60}}, 13, false, trials);
    return 0;
}


