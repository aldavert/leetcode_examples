#include "../common/common.hpp"
#include <bitset>
#include <queue>
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int slidingPuzzle(std::vector<std::vector<int> > board)
{
    constexpr int mask_status = (1 << 18) - 1;
    std::bitset<1 << 18> visited;
    const int expected = (1 << (3 * 0)) | (2 << (3 * 1)) | (3 << (3 * 2))
                       | (4 << (3 * 3)) | (5 << (3 * 4)) | (0 << (3 * 5))
                       | (((2 << 1) | 1) << 18);
    auto swap = [](int status, int y0, int x0, int y1, int x1) -> int
    {
        int bit0 = (y0 * 3 + x0) * 3, bit1 = (y1 * 3 + x1) * 3;
        const int mask = (mask_status ^ (7 << bit0)) ^ (7 << bit1);
        int value0 = (status & (7 << bit0)) >> bit0,
            value1 = (status & (7 << bit1)) >> bit1;
        return (status & mask) | (value0 << bit1) | (value1 << bit0)
             | (((x1 << 1) | y1) << 18);
    };
    int status = (board[0][0] << (3 * 0))
               | (board[0][1] << (3 * 1))
               | (board[0][2] << (3 * 2))
               | (board[1][0] << (3 * 3))
               | (board[1][1] << (3 * 4))
               | (board[1][2] << (3 * 5));
    if      (board[0][0] == 0) status = status | (((0 << 1) | 0) << 18);
    else if (board[0][1] == 0) status = status | (((1 << 1) | 0) << 18);
    else if (board[0][2] == 0) status = status | (((2 << 1) | 0) << 18);
    else if (board[1][0] == 0) status = status | (((0 << 1) | 1) << 18);
    else if (board[1][1] == 0) status = status | (((1 << 1) | 1) << 18);
    else if (board[1][2] == 0) status = status | (((2 << 1) | 1) << 18);
    else return -1;
    std::queue<int> q;
    q.push(status);
    int steps = 0;
    for (; !q.empty(); ++steps)
    {
        for (int i = 0, n = static_cast<int>(q.size()); i < n; ++i)
        {
            status = q.front();
            q.pop();
            if (status == expected) return steps;
            if (visited[status & mask_status])
                continue;
            visited[status & mask_status] = true;
            int y = (status >> 18) & 1, x = status >> 19;
            q.push(swap(status, y, x, !y, x));
            if (x > 0) q.push(swap(status, y, x, y, x - 1));
            if (x < 2) q.push(swap(status, y, x, y, x + 1));
        }
    }
    return -1;
}
#elif 0
int slidingPuzzle(std::vector<std::vector<int> > board)
{
    constexpr int mask_status = (1 << 18) - 1;
    std::unordered_map<int, int> visited;
    const int expected = (1 << (3 * 0)) | (2 << (3 * 1)) | (3 << (3 * 2))
                       | (4 << (3 * 3)) | (5 << (3 * 4)) | (0 << (3 * 5))
                       | (((2 << 1) | 1) << 18);
    int result = (1 << 18);
    auto swap = [](int status, int y0, int x0, int y1, int x1) -> int
    {
        int bit0 = (y0 * 3 + x0) * 3, bit1 = (y1 * 3 + x1) * 3;
        const int mask = (mask_status ^ (7 << bit0)) ^ (7 << bit1);
        int value0 = (status & (7 << bit0)) >> bit0,
            value1 = (status & (7 << bit1)) >> bit1;
        return (status & mask) | (value0 << bit1) | (value1 << bit0)
             | (((x1 << 1) | y1) << 18);
    };
    auto traverse = [&](auto &&self, int status, int steps) -> void
    {
        int y = (status >> 18) & 1, x = status >> 19;
        if (status == expected)
        {
            result = std::min(result, steps);
            return;
        }
        if (steps >= result) return;
        if (auto search = visited.find(status); search != visited.end())
        {
            if (search->second <= steps) return;
            search->second = steps;
        }
        else visited[status] = steps;
        self(self, swap(status, y, x, !y, x), steps + 1);
        if (x > 0) self(self, swap(status, y, x, y, x - 1), steps + 1);
        if (x < 2) self(self, swap(status, y, x, y, x + 1), steps + 1);
    };
    int status = (board[0][0] << (3 * 0))
               | (board[0][1] << (3 * 1))
               | (board[0][2] << (3 * 2))
               | (board[1][0] << (3 * 3))
               | (board[1][1] << (3 * 4))
               | (board[1][2] << (3 * 5));
    if      (board[0][0] == 0) status = status | (((0 << 1) | 0) << 18);
    else if (board[0][1] == 0) status = status | (((1 << 1) | 0) << 18);
    else if (board[0][2] == 0) status = status | (((2 << 1) | 0) << 18);
    else if (board[1][0] == 0) status = status | (((0 << 1) | 1) << 18);
    else if (board[1][1] == 0) status = status | (((1 << 1) | 1) << 18);
    else if (board[1][2] == 0) status = status | (((2 << 1) | 1) << 18);
    else return -1;
    traverse(traverse, status, 0);
    return (result < (1 << 18))?result:-1;
}
#elif 0
int slidingPuzzle(std::vector<std::vector<int> > board)
{
    int visited[(1 << 18)] = {};
    for (int i = 0; i < (1 << 18); ++i)
        visited[i] = 1 << 18;
    const int expected = (1 << (3 * 5)) | (2 << (3 * 4)) | (3 << (3 * 3))
                       | (4 << (3 * 2)) | (5 << (3 * 1)) | (0 << (3 * 0));
    int result = (1 << 18);
    auto swap = [](int status, int y0, int x0, int y1, int x1) -> int
    {
        int bit0 = ((!y0) * 3 + (2 - x0)) * 3, bit1 = ((!y1) * 3 + (2 - x1)) * 3;
        const int mask = (((1 << 18) - 1) ^ (7 << bit0)) ^ (7 << bit1);
        int value0 = (status & (7 << bit0)) >> bit0,
            value1 = (status & (7 << bit1)) >> bit1;
        return (status & mask) | (value0 << bit1) | (value1 << bit0);
    };
    auto traverse = [&](auto &&self, int status, int y, int x, int steps) -> void
    {
        if (status == expected)
        {
            result = std::min(result, steps);
            return;
        }
        if (steps >= result) return;
        if (visited[status] <= steps) return;
        visited[status] = steps;
        self(self, swap(status, y, x, !y, x), !y, x, steps + 1);
        if (x > 0) self(self, swap(status, y, x, y, x - 1), y, x - 1, steps + 1);
        if (x < 2) self(self, swap(status, y, x, y, x + 1), y, x + 1, steps + 1);
    };
    int status = (board[0][0] << (3 * 5))
               | (board[0][1] << (3 * 4))
               | (board[0][2] << (3 * 3))
               | (board[1][0] << (3 * 2))
               | (board[1][1] << (3 * 1))
               | (board[1][2] << (3 * 0));
    if      (board[0][0] == 0) traverse(traverse, status, 0, 0, 0);
    else if (board[0][1] == 0) traverse(traverse, status, 0, 1, 0);
    else if (board[0][2] == 0) traverse(traverse, status, 0, 2, 0);
    else if (board[1][0] == 0) traverse(traverse, status, 1, 0, 0);
    else if (board[1][1] == 0) traverse(traverse, status, 1, 1, 0);
    else if (board[1][2] == 0) traverse(traverse, status, 1, 2, 0);
    else return -1;
    return (result < (1 << 18))?result:-1;
}
#else
int slidingPuzzle(std::vector<std::vector<int> > board)
{
    std::unordered_map<int, int> visited;
    const int expected = (1 << (3 * 5)) | (2 << (3 * 4)) | (3 << (3 * 3))
                       | (4 << (3 * 2)) | (5 << (3 * 1)) | (0 << (3 * 0));
    int result = (1 << 18);
    auto swap = [](int status, int y0, int x0, int y1, int x1) -> int
    {
        int bit0 = ((!y0) * 3 + (2 - x0)) * 3, bit1 = ((!y1) * 3 + (2 - x1)) * 3;
        const int mask = (((1 << 18) - 1) ^ (7 << bit0)) ^ (7 << bit1);
        int value0 = (status & (7 << bit0)) >> bit0,
            value1 = (status & (7 << bit1)) >> bit1;
        return (status & mask) | (value0 << bit1) | (value1 << bit0);
    };
    auto traverse = [&](auto &&self, int status, int y, int x, int steps) -> void
    {
        if (status == expected)
        {
            result = std::min(result, steps);
            return;
        }
        if (steps >= result) return;
        if (auto search = visited.find(status); search != visited.end())
        {
            if (search->second <= steps) return;
            search->second = steps;
        }
        else visited[status] = steps;
        self(self, swap(status, y, x, !y, x), !y, x, steps + 1);
        if (x > 0) self(self, swap(status, y, x, y, x - 1), y, x - 1, steps + 1);
        if (x < 2) self(self, swap(status, y, x, y, x + 1), y, x + 1, steps + 1);
    };
    int status = (board[0][0] << (3 * 5))
               | (board[0][1] << (3 * 4))
               | (board[0][2] << (3 * 3))
               | (board[1][0] << (3 * 2))
               | (board[1][1] << (3 * 1))
               | (board[1][2] << (3 * 0));
    if      (board[0][0] == 0) traverse(traverse, status, 0, 0, 0);
    else if (board[0][1] == 0) traverse(traverse, status, 0, 1, 0);
    else if (board[0][2] == 0) traverse(traverse, status, 0, 2, 0);
    else if (board[1][0] == 0) traverse(traverse, status, 1, 0, 0);
    else if (board[1][1] == 0) traverse(traverse, status, 1, 1, 0);
    else if (board[1][2] == 0) traverse(traverse, status, 1, 2, 0);
    else return -1;
    return (result < (1 << 18))?result:-1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > board, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = slidingPuzzle(board);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {4, 0, 5}},  1, trials);
    test({{1, 2, 3}, {5, 4, 0}}, -1, trials);
    test({{4, 1, 2}, {5, 0, 3}},  5, trials);
    return 0;
}


