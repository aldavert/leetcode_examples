#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

bool isRationalEqual(std::string s, std::string t)
{
    auto decimalValue = [](const std::string& text) -> double
    {
        constexpr double ratios[] = { 1.0, 1.0 / 9, 1.0 / 99, 1.0 / 999, 1.0 / 9999};
        if (text.find('(') == std::string::npos) return std::stod(text);
        size_t index_left = text.find_first_of('(');
        size_t index_right = text.find_first_of(')');
        size_t index_dot = text.find_first_of('.');
    return std::stod(text.substr(0, index_left))
         + std::stoi(text.substr(index_left + 1, index_right))
         * std::pow(0.1, index_left - index_dot - 1)
         * ratios[index_right - index_left - 1];
    };
    return std::abs(decimalValue(s) - decimalValue(t)) < 1e-9;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string t, bool solution, unsigned int trials = 1)
{
    int result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isRationalEqual(s, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("0.(52)", "0.5(25)", true, trials);
    test("0.1666(6)", "0.166(66)", true, trials);
    test("0.9(9)", "1.", true, trials);
    return 0;
}


