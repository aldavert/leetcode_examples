#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maximumTotalSum(std::vector<int> maximumHeight)
{
    long long result = 0;
    std::sort(maximumHeight.begin(), maximumHeight.end(), std::greater<>());
    for (int max = std::numeric_limits<int>::max(); int num : maximumHeight)
    {
        if (max == 0) return -1;
        max = std::min(num, max);
        result += max;
        --max;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> maximumHeight, long long solution, unsigned int trials = 1)
{
    long long result = -2;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumTotalSum(maximumHeight);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 4, 3}, 10, trials);
    test({15, 10}, 25, trials);
    test({2, 2, 1}, -1, trials);
    return 0;
}


