#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isPalindrome(std::string s)
{
    if (s.empty()) return true;
    for (size_t i = 0, j = s.size() - 1; i < j;)
    {
        if      (!std::isalnum(s[i])) ++i;
        else if (!std::isalnum(s[j])) --j;
        else if (std::tolower(s[i]) != std::tolower(s[j]))
            return false;
        else { ++i; --j; }
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPalindrome(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("A man, a plan, a canal: Panama", true, trials);
    test("race a car", false, trials);
    test(" ", true, trials);
    return 0;
}


