#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumTotalPrice(int n,
                      std::vector<std::vector<int> > edges,
                      std::vector<int> price,
                      std::vector<std::vector<int> > trips)
{
    std::vector<std::vector<int> > graph(n);
    std::vector<int> count(n), path, dp[2] =
                            { std::vector<int>(n, -1), std::vector<int>(n, -1) };
    auto countPaths = [&](auto &&self, int node, int previous, int end) -> void
    {
        path.push_back(node);
        if (node == end)
            for (int i : path)
                ++count[i];
        else
        {
            for (int next : graph[node])
                if (next != previous)
                    self(self, next, node, end);
            path.pop_back();
        }
    };
    auto detphSearch = [&](auto &&self, int node, int previous, bool parent_halved) -> int
    {
        if (dp[parent_halved][node] != -1) return dp[parent_halved][node];
        int sum_with_full_node = price[node] * count[node];
        for (int next : graph[node])
            if (next != previous)
                sum_with_full_node += self(self, next, node, false);
        if (parent_halved)
            return dp[parent_halved][node] = sum_with_full_node;
        int sum_with_halved_node = (price[node] / 2) * count[node];
        for (int next : graph[node])
            if (next != previous)
                sum_with_halved_node += self(self, next, node, true);
        return dp[parent_halved][node] =
            std::min(sum_with_full_node, sum_with_halved_node);
    };
    
    for (const auto &edge : edges)
    {
        graph[edge[0]].push_back(edge[1]);
        graph[edge[1]].push_back(edge[0]);
    }
    for (const auto &trip : trips)
    {
        path.clear();
        countPaths(countPaths, trip[0], -1, trip[1]);
    }
    return detphSearch(detphSearch, 0, -1, false);
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<int> price,
          std::vector<std::vector<int> > trips,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumTotalPrice(n, edges, price, trips);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{0, 1}, {1, 2}, {1, 3}}, {2, 2, 10, 6},
            {{0, 3}, {2, 1}, {2, 3}}, 23, trials);
    test(2, {{0, 1}}, {2, 2}, {{0, 0}}, 1, trials);
    return 0;
}


