#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int addedInteger(std::vector<int> nums1, std::vector<int> nums2)
{
    return *std::min_element(nums2.begin(), nums2.end())
         - *std::min_element(nums1.begin(), nums1.end());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = addedInteger(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 6, 4}, {9, 7, 5}, 3, trials);
    test({10}, {5}, -5, trials);
    test({1, 1, 1, 1}, {1, 1, 1, 1}, 0, trials);
    return 0;
}


