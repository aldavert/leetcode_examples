#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> powerfulIntegers(int x, int y, int bound)
{
    std::unordered_set<int> result;
    for (int i = 1; i < bound; i *= x)
    {
        for (int j = 1; i + j <= bound; j *= y)
        {
            result.insert(i + j);
            if (y == 1) break;
        }
        if (x == 1) break;
    }
    return {result.begin(), result.end()};
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<int> work_left(left), work_right(right);
    std::sort(work_left.begin(), work_left.end());
    std::sort(work_right.begin(), work_right.end());
    for (size_t i = 0; i < left.size(); ++i)
        if (work_left[i] != work_right[i])
            return false;
    return true;
}

void test(int x, int y, int bound, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = powerfulIntegers(x, y, bound);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 3, 10, {2, 3, 4, 5, 7, 9, 10}, trials);
    test(3, 5, 15, {2, 4, 6, 8, 10, 14}, trials);
    return 0;
}


