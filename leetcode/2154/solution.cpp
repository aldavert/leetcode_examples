#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

int findFinalValue(std::vector<int> nums, int original)
{
    std::bitset<1001> available;
    for (int number : nums)
        available[number] = true;
    while ((original <= 1000) && available[original]) original *= 2;
    return original;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int original, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findFinalValue(nums, original);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 3, 6, 1, 12}, 3, 24, trials);
    test({2, 7, 9}, 4, 4, trials);
    return 0;
}


