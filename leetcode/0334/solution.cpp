#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

bool increasingTriplet(std::vector<int> nums)
{
    for (int minimum_1st = std::numeric_limits<int>::max(),
             minimum_2on = std::numeric_limits<int>::max(); int n : nums)
    {
        if      (n <= minimum_1st) minimum_1st = n;
        else if (n <= minimum_2on) minimum_2on = n;
        else return true;
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = increasingTriplet(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, true, trials);
    test({5, 4, 3, 2, 1}, false, trials);
    test({2, 1, 5, 0, 4, 6}, true, trials);
    return 0;
}


