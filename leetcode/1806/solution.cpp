#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int reinitializePermutation(int n)
{
    int result = 0, i = 1;
    
    do
    {
        if (i < n / 2) i = i * 2;
        else i = (i - n / 2) * 2 + 1;
        ++result;
    }
    while (i != 1);
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = reinitializePermutation(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 1, trials);
    test(4, 2, trials);
    return 0;
}


