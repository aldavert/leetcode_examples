#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> mostCompetitive(std::vector<int> nums, int k)
{
    std::vector<int> result;
    
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        while (!result.empty() && (result.back() > nums[i])
           &&  (static_cast<int>(result.size()) - 1 + n - i >= k))
            result.pop_back();
        if (static_cast<int>(result.size()) < k)
            result.push_back(nums[i]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = mostCompetitive(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 5, 2, 6}, 2, {2, 6}, trials);
    test({2, 4, 3, 3, 5, 4, 9, 6}, 4, {2, 3, 3, 4}, trials);
    return 0;
}


