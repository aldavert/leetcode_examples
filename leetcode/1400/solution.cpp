#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

bool canConstruct(std::string s, int k)
{
    if (static_cast<int>(s.size()) < k) return false;
    std::bitset<26> odd;
    for (char c : s) odd.flip(c - 'a');
    return static_cast<int>(odd.count()) <= k;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canConstruct(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("annabelle", 2, true, trials);
    test("leetcode", 3, false, trials);
    test("true", 4, true, trials);
    return 0;
}


