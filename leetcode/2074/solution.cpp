#include "../common/common.hpp"
#include "../common/list.hpp"
#include <utility>

// ############################################################################
// ############################################################################

ListNode * reverseEvenLengthGroups(ListNode * head)
{
    ListNode dummy(0, head);
    ListNode * prev = &dummy, * tail = head, * next = head->next;
    int group_length = 1;
    
    while (true)
    {
        if (group_length % 2 == 1)
        {
            prev->next = head;
            prev = tail;
        }
        else
        {
            tail->next = nullptr;
            for (ListNode * h = head; h; )
                prev->next = std::exchange(h, std::exchange(h->next, prev->next));
            head->next = next;
            prev = head;
        }
        if (next == nullptr) break;
        head = next;
        int length = 1;
        for (tail = head; (length <= group_length) && (tail->next); tail = tail->next)
            ++length;
        next = tail->next;
        group_length = length;
    }
    return std::exchange(dummy.next, nullptr);
  }

// ############################################################################
// ############################################################################

void test(std::vector<int> list, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(list);
        ListNode * nw_head = reverseEvenLengthGroups(head);
        result = list2vec(nw_head);
        delete nw_head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 2, 6, 3, 9, 1, 7, 3, 8, 4}, {5, 6, 2, 3, 9, 1, 4, 8, 3, 7}, trials);
    test({1, 1, 0, 6}, {1, 0, 1, 6}, trials);
    test({1, 1, 0, 6, 5}, {1, 0, 1, 5, 6}, trials);
    return 0;
}


