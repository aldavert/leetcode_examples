# Reverse Nodes in Even Length Groups

You are given the `head` of a linked list.

The nodes in the linked list are **sequentially** assigned to **non-empty** groups whose lengths form the sequence of the natural numbers (`1, 2, 3, 4, ...`). The **length** of a group is the number of nodes assigned to it. In other words,
- The `1^{st}` node is assigned to the first group.
- The `2^{nd}` and the `3^{rd}` nodes are assigned to the second group.
- The `4^{th}`, `5^{th}`, and `6^{th}` nodes are assigned to the third group, and so on.

Note that the length of the last group may be less than or equal to `1 + the length of the second to last group`.

**Reverse** the nodes in each group with an **even** length, and return *the* `head` *of the modified linked list.*

#### Example 1:
> ```mermaid 
> graph LR;
> subgraph SA [first group]
> A1((5))
> A2((5))
> end
> subgraph SB [second group]
> B1((2))-->C1((6))
> B2((6))-->C2((2))
> end
> subgraph SC [third group]
> D1((3))-->E1((9))-->F1((1))
> D2((3))-->E2((9))-->F2((1))
> end
> subgraph SD [last group]
> G1((7))-->H1((3))-->I1((8))-->J1((4))
> G2((4))-->H2((8))-->I2((3))-->J2((7))
> end
> A1-->B1
> C1-->D1
> F1-->G1
> A2-->B2
> C2-->D2
> F2-->G2
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef group fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#FAA,stroke:#822,stroke-width:2px;
> class SA,SB,SC,SD group;
> class B2,C2,G2,H2,I2,J2 selected;
> ```
> *Input:* `head = [5, 2, 6, 3, 9, 1, 7, 3, 8, 4]`  
> *Output:* `[5, 6, 2, 3, 9, 1, 4, 8, 3, 7]`  
> *Explanation:*
> - The length of the first group is 1, which is odd, hence no reversal occurs.
> - The length of the second group is 2, which is even, hence the nodes are reversed.
> - The length of the third group is 3, which is odd, hence no reversal occurs.
> - The length of the last group is 4, which is even, hence the nodes are reversed.

#### Example 2:
> ```mermaid 
> graph LR;
> subgraph SA [first group]
> A1((1))
> A2((1))
> end
> subgraph SB [second group]
> B1((1))-->C1((0))
> B2((0))-->C2((1))
> end
> subgraph SC [last group]
> D1((6))
> D2((6))
> end
> A1-->B1
> C1-->D1
> A2-->B2
> C2-->D2
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef group fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#FAA,stroke:#822,stroke-width:2px;
> class SA,SB,SC group;
> class B2,C2 selected;
> ```
> *Input:* `head = [1, 1, 0, 6]`  
> *Output:* `[1, 0, 1, 6]`  
> *Explanation:*
> - The length of the first group is `1`. No reversal occurs.
> - The length of the second group is `2`. The nodes are reversed.
> - The length of the last group is `1`. No reversal occurs.

#### Example 3:
> ```mermaid 
> graph LR;
> subgraph SA [first group]
> A1((1))
> A2((1))
> end
> subgraph SB [second group]
> B1((1))-->C1((0))
> B2((0))-->C2((1))
> end
> subgraph SC [last group]
> D1((6))-->E1((5))
> D2((5))-->E2((6))
> end
> A1-->B1
> C1-->D1
> A2-->B2
> C2-->D2
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef group fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#FAA,stroke:#822,stroke-width:2px;
> class SA,SB,SC group;
> class B2,C2,D2,E2 selected;
> ```
> *Input:* `head = [1, 1, 0, 6, 5]`  
> *Output:* `[1, 0, 1, 5, 6]`  
> *Explanation:*
> - The length of the first group is `1`. No reversal occurs.
> - The length of the second group is `2`. The nodes are reversed.
> - The length of the last group is `2`. The nodes are reversed.

#### Constraints:
- The number of nodes in the list is in the range `[1, 10^5]`.
- `0 <= Node.val <= 10^5`


