#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
void wiggleSort(std::vector<int> &nums)
{
    const int n = static_cast<int>(nums.size());
    int histogram[5001] = {}, left = 0, right = 5000;
    for (int i = 0; i < n; ++i)
        ++histogram[nums[i]];
    for (int i = 0, m = n + (n & 1) - 2; i < n; ++i)
    {
        if (i & 1)
        {
            while ((right >=    0) && (histogram[right] == 0)) --right;
            nums[i] = right;
            --histogram[right];
        }
        else
        {
            while ((left  <  5001) && (histogram[left]  == 0)) ++left;
            nums[m - i] = left;
            --histogram[left];
        }
    }
}
#else
void wiggleSort(std::vector<int> &nums)
{
    std::vector<int> sorted_nums = nums;
    std::sort(sorted_nums.begin(), sorted_nums.end());
    int n = static_cast<int>(nums.size()), mid_index = (n - 1) >> 1,
            last_index = n - 1;
    for (int k = 0; k < n; ++k)
    {
        if (k % 2 == 0) nums[k] = sorted_nums[mid_index--];
        else nums[k] = sorted_nums[last_index--];
    }
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = nums;
        wiggleSort(result);
    }
    bool valid = solution == result;
    if (!valid)
    {
        std::vector<int> copy_a = result, copy_b = solution;
        std::sort(copy_a.begin(), copy_a.end());
        std::sort(copy_b.begin(), copy_b.end());
        bool smaller = true;
        valid = (copy_a == copy_b);
        const size_t n = nums.size();
        for (size_t i = 0; valid && (i + 1 < n); ++i, smaller = !smaller)
            if (( smaller && (result[i] >= result[i + 1]))
            ||  (!smaller && (result[i] <= result[i + 1])))
                valid = false;
    }
    showResult(valid, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 5, 1, 1, 6, 4}, {1, 6, 1, 5, 1, 4}, trials);
    test({1, 3, 2, 2, 3, 1}, {2, 3, 1, 3, 1, 2}, trials);
    test({1, 3, 2, 2, 3, 1, 1, 2}, {2, 3, 1, 3, 1, 2, 1, 2}, trials);
    test({1, 4, 3, 4, 1, 2, 1, 3, 1, 3, 2, 3, 3},
         {3, 4, 2, 4, 2, 3, 1, 3, 1, 3, 1, 3, 1}, trials);
    test({4, 5, 5, 6}, {5, 6, 4, 5}, trials);
    test({4, 5, 5, 5, 5, 6, 6, 6}, {5, 6, 5, 6, 5, 6, 4, 5}, trials);
    return 0;
}


