#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int numEquivDominoPairs(std::vector<std::vector<int> > dominoes)
{
    int histogram[100] = {};
    int result = 0;
    for (const auto &piece : dominoes)
    {
        int signature = (std::min(piece[0], piece[1]) * 10)
                      +  std::max(piece[0], piece[1]);
        result += histogram[signature];
        ++histogram[signature];
    }
    return result;
}
#else
int numEquivDominoPairs(std::vector<std::vector<int> > dominoes)
{
    std::unordered_map<int, int> histogram;
    int result = 0;
    for (const auto &piece : dominoes)
    {
        int signature = (std::min(piece[0], piece[1]) << 16)
                      |  std::max(piece[0], piece[1]);
        result += histogram[signature];
        ++histogram[signature];
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > dominoes,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numEquivDominoPairs(dominoes);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 1}, {3, 4}, {5, 6}}, 1, trials);
    test({{1, 2}, {1, 2}, {1, 1}, {1, 2}, {2, 2}}, 3, trials);
    return 0;
}


