#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumFinishTime(std::vector<std::vector<int> > tires,
                      int changeTime,
                      int numLaps)
{
    std::vector<int> dp(numLaps + 1, std::numeric_limits<int>::max());
    
    for (const auto &tire : tires)
        for (long seconds = tire[0], seconds_sum = tire[0], lap_count = 1;
             (seconds < tire[0] + changeTime) && (lap_count <= numLaps);
             seconds *= tire[1], ++lap_count, seconds_sum += seconds)
                dp[lap_count] = std::min(static_cast<int>(seconds_sum), dp[lap_count]);
    
    for (int i = 2; i <= numLaps; ++i)
        for (int j = 1; j * 2 <= i; ++j)
            dp[i] = std::min(dp[i], dp[j] + dp[i - j] + changeTime);
    return dp[numLaps];
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > tires,
          int changeTime,
          int numLaps,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumFinishTime(tires, changeTime, numLaps);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 3}, {3, 4}}, 5, 4, 21, trials);
    test({{1, 10}, {2, 2}, {3, 4}}, 6, 5, 25, trials);
    return 0;
}


