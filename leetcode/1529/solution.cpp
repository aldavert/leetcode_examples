#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minFlips(std::string target)
{
    int result = 0, state = 0;
    
    for (char c : target)
    {
        if (c - '0' != state)
        {
            state = c - '0';
            ++result;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minFlips(target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("10111", 3, trials);
    test(  "101", 3, trials);
    test("00000", 0, trials);
    return 0;
}


