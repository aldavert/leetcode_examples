#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

int numberOfSpecialChars(std::string word)
{
    std::bitset<26> lower, upper;
    int result = 0;
    for (char c : word)
    {
        if (std::islower(c)) lower[c - 'a'] = !upper[c - 'a'];
        else upper[c - 'A'] = true;
    }
    for (int i = 0; i < 26; ++i)
        result += (lower[i] && upper[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfSpecialChars(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aaAbcBC", 3, trials);
    test("abc", 0, trials);
    test("AbBCab", 0, trials);
    return 0;
}


