#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canMakeSquare(std::vector<std::vector<char> > grid)
{
    auto check = [&](int i, int j) -> bool
    {
        return ((grid[i + 0][j + 0] == 'B')
              + (grid[i + 0][j + 1] == 'B')
              + (grid[i + 1][j + 0] == 'B')
              + (grid[i + 1][j + 1] == 'B')) != 2;
    };
    return check(0, 0) || check(0, 1) || check(1, 0) || check(1, 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > grid, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canMakeSquare(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'B', 'W', 'B'}, {'B', 'W', 'W'}, {'B', 'W', 'B'}}, true, trials);
    test({{'B', 'W', 'B'}, {'W', 'B', 'W'}, {'B', 'W', 'B'}}, false, trials);
    test({{'B', 'W', 'B'}, {'B', 'W', 'W'}, {'B', 'W', 'W'}}, true, trials);
    return 0;
}


