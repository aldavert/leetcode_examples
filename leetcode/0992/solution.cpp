#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int subarraysWithKDistinct(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    auto atMost =  [&](int u)
    {
        int result = 0;
        std::vector<int> count(nums.size() + 1);
        for (int l = 0, r = 0; r < n; ++r)
        {
            u -= (++count[nums[r]] == 1);
            while (u == -1)
                u += (--count[nums[l++]] == 0);
            result += r - l + 1;
        }
        return result;
    };
    return atMost(k) - atMost(k - 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = subarraysWithKDistinct(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1, 2, 3}, 2, 7, trials);
    test({1, 2, 1, 3, 4}, 3, 3, trials);
    return 0;
}


