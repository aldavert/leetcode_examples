#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

std::string largestNumber(std::vector<int> cost, int target)
{
    std::vector<int> dp(target + 1, std::numeric_limits<int>::lowest());
    dp[0] = 0;
    for (int i = 1; i <= target; ++i)
        for (int d = 0; d < 9; ++d)
            if (i >= cost[d])
                dp[i] = std::max(dp[i], dp[i - cost[d]] + 1);
    
    if (dp[target] < 0) return "0";
    std::string result;
    for (int d = 8; d >= 0; --d)
        while ((target >= cost[d]) && (dp[target] == dp[target - cost[d]] + 1))
            target -= cost[d],
            result += static_cast<char>('1' + d);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> cost,
          int target,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestNumber(cost, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({7, 6, 5, 5, 5, 6, 8, 7, 8}, 12, "85", trials);
    test({2, 4, 6, 2, 4, 6, 4, 4, 4}, 5, "0", trials);
    return 0;
}


