#include "../common/common.hpp"

// ############################################################################
// ############################################################################

class ParkingSystem
{
    int parking[4] = {0, 0, 0, 0};
public:
    ParkingSystem(int big, int medium, int small)
    {
        parking[1] = big;
        parking[2] = medium;
        parking[3] = small;
    }
    bool addCar(int carType)
    {
        if (parking[carType])
        {
            --parking[carType];
            return true;
        }
        else return false;
    }
};

// ############################################################################
// ############################################################################

void test(std::tuple<int, int, int> init,
          std::vector<int> car_types,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ParkingSystem parking = std::make_from_tuple<ParkingSystem>(init);
        result.clear();
        for (int car_type : car_types)
            result.push_back(parking.addCar(car_type));
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 0}, {1, 2, 3, 1}, {true, true, false, false}, trials);
    return 0;
}


