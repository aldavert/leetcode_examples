#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int maxOperations(std::vector<int> nums, int k)
{
    const size_t n = nums.size();
    std::unordered_map<int, int> lut;
    for (size_t i = 0; i < n; ++i)
        if (nums[i] < k)
            ++lut[nums[i]];
    int result = 0;
    for (auto [integer, frequency] : lut)
    {
        int difference = k - integer;
        if (auto search = lut.find(difference); search != lut.end())
        {
            if (integer == difference)
                result += frequency / 2;
            else result += std::min(frequency, search->second);
            search->second = 0;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxOperations(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 5, 2, trials);
    test({3, 1, 3, 4, 3}, 6, 1, trials);
    return 0;
}


