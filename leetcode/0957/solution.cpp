#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> prisonAfterNDays(std::vector<int> cells, int n)
{
    const int m = static_cast<int>(cells.size());
    std::vector<int> first_day_cells, next_day_cells(m);
    
    for (int day = 0; n-- > 0; cells = next_day_cells, ++day)
    {
        for (int i = 1; i + 1 < m; ++i)
            next_day_cells[i] = cells[i - 1] == cells[i + 1];
        if (day == 0) first_day_cells = next_day_cells;
        else if (next_day_cells == first_day_cells) n %= day;
    }
    return cells;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> cells,
          int n,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = prisonAfterNDays(cells, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 0, 1, 1, 0, 0, 1}, 7, {0, 0, 1, 1, 0, 0, 0, 0}, trials);
    test({1, 0, 0, 1, 0, 0, 1, 0}, 1'000'000'000, {0, 0, 1, 1, 1, 1, 1, 0}, trials);
    return 0;
}


