#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int smallestRangeII(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::sort(nums.begin(), nums.end());
    int result = nums.back() - nums.front();
    for (int i = 0, l = nums.front() + k, r = nums.back() - k; i + 1 < n; ++i)
        result = std::min(result,
                          std::max(r, nums[i] + k) - std::min(l, nums[i + 1] - k));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestRangeII(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1}, 0, 0, trials);
    test({0, 10}, 2, 6, trials);
    test({1, 3, 6}, 3, 3, trials);
    return 0;
}


