#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
bool containsCycle(std::vector<std::vector<char> > grid)
{
    constexpr int dir[5] = {-1, 0, 1, 0, -1};
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    std::vector<std::vector<bool> > seen(m, std::vector<bool>(n));
    auto searchLoop = [&](int i, int j) -> bool
    {
        std::queue<std::tuple<int, int, int, int> > q{{{i, j, -1, -1}}};
        char c = grid[i][j];
        seen[i][j] = true;
        while (!q.empty())
        {
            auto [ci, cj, pi, pj] = q.front();
            q.pop();
            for (int d = 0; d < 4; ++d)
            {
                int x = ci + dir[d], y = cj + dir[d + 1];
                if ((x < 0) || (x == m) || (y < 0) || (y == n))
                    continue;
                if ((x == pi) && (y == pj))
                    continue;
                if (grid[x][y] != c)
                    continue;
                if (seen[x][y])
                    return true;
                seen[x][y] = true;
                q.push({x, y, ci, cj});
            }
        }
        return false;
    };
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            if (seen[i][j]) continue;
            if (searchLoop(i, j)) return true;
        }
    }
    return false;
}
#else
bool containsCycle(std::vector<std::vector<char> > grid)
{
    constexpr int dir[5] = {-1, 0, 1, 0, -1};
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    std::vector<std::vector<bool> > seen(m, std::vector<bool>(n));
    auto dfs = [&](auto &&self, int i, int j, int prev_i, int prev_j, char c) -> bool
    {
        seen[i][j] = true;
        for (int d = 0; d < 4; ++d)
        {
            int x = i + dir[d], y = j + dir[d + 1];
            if ((x < 0) || (x == m) || (y < 0) || (y == n))
                continue;
            if ((x == prev_i) && (y == prev_j))
                continue;
            if (grid[x][y] != c)
                continue;
            if (seen[x][y])
                return true;
            if (self(self, x, y, i, j, c))
                return true;
        }
        return false;
    };
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            if (seen[i][j]) continue;
            if (dfs(dfs, i, j, -1, -1, grid[i][j]))
                return true;
        }
    }
    return false;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > grid, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = containsCycle(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'a', 'a', 'a', 'a'},
          {'a', 'b', 'b', 'a'},
          {'a', 'b', 'b', 'a'},
          {'a', 'a', 'a', 'a'}}, true, trials);
    test({{'c', 'c', 'c', 'a'},
          {'c', 'd', 'c', 'c'},
          {'c', 'c', 'e', 'c'},
          {'f', 'c', 'c', 'c'}}, true, trials);
    test({{'a', 'b', 'b'},
          {'b', 'z', 'b'},
          {'b', 'b', 'a'}}, false, trials);
    return 0;
}


