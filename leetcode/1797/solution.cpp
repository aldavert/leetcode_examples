#include "../common/common.hpp"
#include <unordered_map>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class AuthenticationManager
{
    int m_time_to_live;
    std::unordered_map<std::string, int> m_time_to_expire;
    std::set<int> m_times;
public:
    AuthenticationManager(int timeToLive) : m_time_to_live(timeToLive) {}
    void generate(std::string tokenId, int currentTime)
    {
        m_time_to_expire[tokenId] = currentTime;
        m_times.emplace(currentTime);
    }
    void renew(std::string tokenId, int currentTime)
    {
        auto it = m_time_to_expire.find(tokenId);
        if ((it == m_time_to_expire.cend())
        ||  (currentTime >= it->second + m_time_to_live)) return;
        m_times.erase(m_time_to_expire[tokenId]);
        m_time_to_expire[tokenId] = currentTime;
        m_times.emplace(currentTime);
    }
    int countUnexpiredTokens(int currentTime)
    {
        auto it = m_times.lower_bound(currentTime - m_time_to_live + 1);
        m_times.erase(m_times.begin(), it);
        return static_cast<int>(m_times.size());
    }
};

// ############################################################################
// ############################################################################

enum class OP { GENERATE, RENEW, COUNT };

void test(int timeToLive,
          std::vector<OP> operations,
          std::vector<std::variant<std::tuple<std::string, int>, int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        AuthenticationManager object(timeToLive);
        for (size_t i = 0; i < operations.size(); ++i)
        {
            switch (operations[i])
            {
            case OP::GENERATE:
            {
                auto [token, time] = std::get<std::tuple<std::string, int> >(input[i]);
                result.push_back(null);
                object.generate(token, time);
                break;
            }
            case OP::RENEW:
            {
                auto [token, time] = std::get<std::tuple<std::string, int> >(input[i]);
                result.push_back(null);
                object.renew(token, time);
                break;
            }
            case OP::COUNT:
            {
                result.push_back(object.countUnexpiredTokens(std::get<int>(input[i])));
                break;
            }
            default:
                std::cerr << "[ERROR] Unknown operation at index " << i << ".\n";
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    auto I = [](std::string s, int v) -> std::tuple<std::string, int> { return {s, v}; };
    test(5, {OP::RENEW, OP::GENERATE, OP::COUNT, OP::GENERATE, OP::RENEW,
         OP::RENEW, OP::COUNT}, {I("aaa", 1), I("aaa", 2), 6, I("bbb", 7),
         I("aaa", 8), I("bbb", 10), 15}, {null, null, 1, null, null, null, 0}, trials);
    return 0;
}


