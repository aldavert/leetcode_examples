#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int networkBecomesIdle(std::vector<std::vector<int> > edges,
                       std::vector<int> patience)
{
    const int n = static_cast<int>(patience.size());
    std::vector<std::vector<int> > graph(n);
    std::queue<int> q{{0}};
    std::vector<int> dist(n, std::numeric_limits<int>::max());
    dist[0] = 0;
    for (const auto &edge : edges)
    {
        graph[edge[0]].push_back(edge[1]);
        graph[edge[1]].push_back(edge[0]);
    }
    while (!q.empty())
    {
        for (int sz = static_cast<int>(q.size()); sz > 0; --sz)
        {
            int u = q.front();
            q.pop();
            for (int v : graph[u])
            {
                if (dist[v] == std::numeric_limits<int>::max())
                {
                    dist[v] = dist[u] + 1;
                    q.push(v);
                }
            }
        }
    }
    
    int result = 0;
    for (int i = 1; i < n; ++i)
        result = std::max(result, patience[i] * ((dist[i] * 2 - 1) / patience[i])
                                + dist[i] * 2);
    return result + 1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges,
          std::vector<int> patience,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = networkBecomesIdle(edges, patience);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {1, 2}}, {0, 2, 1}, 8, trials);
    test({{0, 1}, {0, 2}, {1, 2}}, {0, 10, 10}, 3, trials);
    return 0;
}


