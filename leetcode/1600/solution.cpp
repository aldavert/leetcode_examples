#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

class ThroneInheritance
{
public:
    ThroneInheritance(std::string king_name) : m_king_name(king_name) {}
    void birth(std::string parent_name, std::string child_name)
    {
        m_family[parent_name].push_back(child_name);
    }
    void death(std::string name)
    {
        m_dead.insert(name);
    }
    std::vector<std::string> getInheritanceOrder(void)
    {
        std::vector<std::string> ans;
        dfs(m_king_name, ans);
        return ans;
    }
private:
    std::unordered_set<std::string> m_dead;
    std::unordered_map<std::string, std::vector<std::string> > m_family;
    std::string m_king_name;
    
    void dfs(const std::string &name, std::vector<std::string> &result)
    {
        if (m_dead.find(name) == m_dead.end())
            result.push_back(name);
        auto search = m_family.find(name);
        if (search == m_family.end()) return;
        for (const std::string &child : search->second)
            dfs(child, result);
    }
};

// ############################################################################
// ############################################################################

enum class OP { BIRTH, DEATH, ORDER };

void test(std::string king_name,
          std::vector<OP> op,
          std::vector<std::vector<std::string> > input,
          std::vector<std::vector<std::string> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<std::string> > result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        ThroneInheritance obj(king_name);
        for (size_t i = 0; i < op.size(); ++i)
        {
            result.push_back({});
            switch (op[i])
            {
            case OP::BIRTH:
                obj.birth(input[i][0], input[i][1]);
                break;
            case OP::DEATH:
                obj.death(input[i][0]);
                break;
            case OP::ORDER:
                result.back() = obj.getInheritanceOrder();
                break;
            default:
                std::cerr << "[ERROR] Unknown operation at position: " << i << '\n';
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("king", {OP::BIRTH, OP::BIRTH, OP::BIRTH, OP::BIRTH, OP::BIRTH, OP::BIRTH,
         OP::ORDER, OP::DEATH, OP::ORDER}, {{"king", "andy"}, {"king", "bob"},
         {"king", "catherine"}, {"andy", "matthew"}, {"bob", "alex"},
         {"bob", "asha"}, {}, {"bob"}, {}},
         {{}, {}, {}, {}, {}, {},
         {"king", "andy", "matthew", "bob", "alex", "asha", "catherine"}, {},
         {"king", "andy", "matthew", "alex", "asha", "catherine"}}, trials);
    return 0;
}


