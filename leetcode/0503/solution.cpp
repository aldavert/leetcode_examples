#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

std::vector<int> nextGreaterElements(std::vector<int> nums)
{
    constexpr int null = -1'500'000'000;
    struct Info
    {
        int value = -1;
        int index = -1;
    };
    std::vector<int> result(nums.size(), null);
    const int n = static_cast<int>(nums.size());
    std::stack<Info> s;
    for (int i = 0; i < n; ++i)
    {
        while (!s.empty() && (nums[i] > s.top().value))
        {
            result[s.top().index] = nums[i];
            s.pop();
        }
        s.push({nums[i], i});
    }
    for (int i = 0; i < n; ++i)
    {
        while (!s.empty() && (nums[i] > s.top().value))
        {
            result[s.top().index] = nums[i];
            s.pop();
        }
        if (result[i] == null) result[i] = -1;
        s.push({nums[i], i});
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = nextGreaterElements(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1}, {2, -1, 2}, trials);
    test({1, 2, 3, 4, 3}, {2, 3, 4, -1, 4}, trials);
    test({1,   8,  -1, -100,  -1,     222, 1111111, -111111},
         {8, 222, 222,   -1, 222, 1111111,      -1,       1}, trials);
    return 0;
}


