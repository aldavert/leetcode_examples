#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

TreeNode * createBinaryTree(std::vector<std::vector<int> > descriptions)
{
    std::unordered_map<int, int[2]> nodes;
    std::unordered_set<int> is_child;
    for (const auto &description : descriptions)
    {
        nodes[description[0]][!description[2]] = description[1];
        is_child.insert(description[1]);
    }
    TreeNode * result = nullptr;
    std::queue<TreeNode *> q;
    for (const auto &info : nodes)
    {
        if (is_child.find(info.first) == is_child.end())
        {
            result = new TreeNode(info.first);
            q.push(result);
            break;
        }
    }
    while (!q.empty())
    {
        TreeNode * node = q.front();
        q.pop();
        auto [left, right] = nodes[node->val];
        if (left)
        {
            node->left = new TreeNode(left);
            q.push(node->left);
        }
        if (right)
        {
            node->right = new TreeNode(right);
            q.push(node->right);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > descriptions,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = createBinaryTree(descriptions);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{20, 15, 1}, {20, 17, 0}, {50, 20, 1}, {50, 80, 0}, {80, 19, 1}},
         {50, 20, 80, 15, 17, 19}, trials);
    test({{1, 2, 1}, {2, 3, 0}, {3, 4, 1}}, {1, 2, null, null, 3, 4}, trials);
    return 0;
}


