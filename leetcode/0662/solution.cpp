#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int widthOfBinaryTree(TreeNode* root)
{
    std::queue<std::pair<int, TreeNode *> > queue;
    if (!root) return 0;
    queue.push({0, root});
    int result = 0;
    for (int size = 1, new_size = 0; !queue.empty(); )
    {
        int left = std::numeric_limits<int>::max();
        int right = 0;
        auto add = [&](int new_index, TreeNode * new_ptr)
        {
            if (new_ptr)
            {
                queue.push({new_index, new_ptr});
                if (left == std::numeric_limits<int>::max())
                    left = new_index;
                right = std::max(right, new_index);
                ++new_size;
            }
        };
        for (int base_index = std::numeric_limits<int>::max(); size; --size) 
        {
            auto [index, ptr] = queue.front();
            queue.pop();
            if (ptr->left || ptr->right)
            {
                if (base_index == std::numeric_limits<int>::max())
                    base_index = index;
                add(2 * (index - base_index)    , ptr->left );
                add(2 * (index - base_index) + 1, ptr->right);
            }
        }
        
        if (left != std::numeric_limits<int>::max())
            result = std::max(result, right - left);
        left = std::numeric_limits<int>::max();
        right = 0;
        size = new_size;
        new_size = 0;
    }
    return result + 1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = widthOfBinaryTree(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 5, 3, null, 9}, 4, trials);
    test({1, 3, null, 5, 3}, 2, trials);
    test({1, 3, 2, 5, null, null, 9, 6, null, 7}, 7, trials);
    test({1, 3, 2, 5}, 2, trials);
    //                                1
    //                            1       1
    //                          1   1   1   1
    //                         · · · 1 · · · ·
    //                           2       2
    //                         2   2   2   2
    //                        2 · 2 · · 2 · 2
    test({1, 1, 1, 1, 1, 1, 1, null, null, null, 1, null, null, null, null,
          2, 2, 2, 2, 2, 2, 2, null, 2, null, null, 2, null, 2}, 8, trials);
    test({1, 1, 1, 1, 1, 1, 1, null, null, null, 1, 1, 1, 1, 1}, 5, trials);
    test({1}, 1, trials);
    test({0, 0, 0, null, 0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null, null, 0, 0, null, null, 0, 0, null, null,
                         0, 0, null}, 2, trials);
    return 0;
}


