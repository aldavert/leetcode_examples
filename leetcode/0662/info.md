# Maximum With of Binary Tree


Given the `root` of a binary tree, return *the* ***maximum width*** *of the given tree*.

The **maximum width** of a tree is the maximum **width** among all levels.

The **width** of one level is defined as the length between the end-nodes (the leftmost and rightmost non-null nodes), where the null nodes between the end-nodes are also counted into the length calculation.

It is **guaranteed** that the answer will in the range of **32-bit** signed integer.

#### Example 1:
> ```mermaid
> graph TD;
> A((1))---B((3))
> A---C((2))
> B---D((5))
> B---E((3))
> C---EMPTY(( ))
> C---F((9))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY empty;
> linkStyle 4 stroke-width:0px;
> ```
> *Input:* `root = [1, 3, 2, 5, 3, null, 9]`  
> *Output:* `4`  
> *Explanation:* The maximum width existing in the third level with the length 4 `(5, 3, null, 9)`.

#### Example 2:
> ```mermaid
> graph TD;
> A((1))---B((3))
> A---EMPTY(( ))
> B---C((5))
> B---D((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY empty;
> linkStyle 1 stroke-width:0px;
> ```
> *Input:* `root = [1, 3, null, 5, 3]`  
> *Output:* `2`  
> *Explanation:* The maximum width existing in the third level with the length 2 `(5, 3)`.

#### Example 3:
> ```mermaid
> graph TD;
> A((1))---B((3))
> A---C((2))
> B---D((5))
> B---EMPTY(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY empty;
> linkStyle 3 stroke-width:0px;
> ```
> *Input:* `root = [1, 3, 2, 5]`  
> *Output:* `2`  
> *Explanation:* The maximum width existing in the second level with the length 2 `(3, 2)`.

#### Example 4:
> ```mermaid
> graph TD;
> A((1))---B((3))
> A---C((2))
> B---D((5))
> B---EMPTY1(( ))
> C---EMPTY2(( ))
> C---E((9))
> D---F((6))
> D---EMPTY3(( ))
> E---G((7))
> E---EMPTY4(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY1,EMPTY2,EMPTY3,EMPTY4 empty;
> linkStyle 3,4,7,9 stroke-width:0px;
> ```
> *Input:* `root = [1, 3, 2, 5, null, null, 9, 6, null, 7]`  
> *Output:* `7`  
> *Explanation:* The maximum width exists in the fourth level with length `7` `(6, null, null, null, null, null, 7)`.
 
#### Constraints:
- The number of nodes in the tree is in the range `[1, 3000]`.
- `-100 <= Node.val <= 100`


