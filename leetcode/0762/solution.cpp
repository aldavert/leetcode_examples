#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countPrimeSetBits(int left, int right)
{
    static constexpr bool isPrime[] = {
    //         0,     1,     2,     3,     4,     5,     6,     7,     8,     9,    10,    11,
           false, false,  true,  true, false,  true, false,  true, false, false, false,  true,
    //        12,    13,    14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
           false,  true, false, false, false,  true, false,  true, false, false, false,  true,
    //        24,    25,    26,    27,    28,    29,    30,    31,    32, 
           false, false, false, false, false,  true, false,  true, false};
    int result = 0;
    for (int i = left; i <= right; ++i)
    {
        int ones = 0, current = i;
        while (current)
        {
            ones += (current & 1);
            current >>= 1;
        }
        result += isPrime[ones];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int left, int right, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPrimeSetBits(left, right);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test( 6, 10, 4, trials);
    test(10, 15, 5, trials);
    return 0;
}


