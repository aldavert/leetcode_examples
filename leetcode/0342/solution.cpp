#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isPowerOfThree(int n)
{
    return n > 0
        && __builtin_popcountll(n) == 1
        && (n - 1) % 3 == 0;
}

// ############################################################################
// ############################################################################

void test(int n, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPowerOfThree(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(16, true, trials);
    test(5, false, trials);
    test(1, true, trials);
    test(2, false, trials);
    return 0;
}


