#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

int lengthLongestPath(std::string input)
{
    std::stack<size_t> path_length;
    bool is_file = false;
    size_t indent = 0, length = 0;
    int result = 0;
    auto update = [&](void) -> void
    {
        while (!path_length.empty() && (path_length.size() > indent))
            path_length.pop();
        path_length.push((path_length.empty()?0:path_length.top()) + length + 1);
        if (is_file)
            result = std::max(result, static_cast<int>(path_length.top()) - 1);
    };
    for (char letter : input)
    {
        if (letter == '\t')
        {
            ++indent;
        }
        else if (letter == '\n')
        {
            update();
            indent = 0;
            length = 0;
            is_file = false;
        }
        else if (letter == '.')
        {
            is_file = true;
            ++length;
        }
        else ++length;
    }
    update();
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string input, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = lengthLongestPath(input);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext", 20, trials);
    test("dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\t"
         "subdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext", 32, trials);
    test("a", 0, trials);
    return 0;
}


