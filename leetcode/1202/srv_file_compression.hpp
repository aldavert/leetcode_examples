// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_FILE_COMPRESSION_HPP_HEADER_FILE__
#define __SRV_FILE_COMPRESSION_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <iostream>

namespace srv
{
    // Reimplements the zfstream library in the SRV library.
    enum CompressionPolicy {
        NoCompression = 0,
        ZLibCompression = 1,
        GZipCompression = ZLibCompression,
        BZipCompression = 2
    };
    
    /// Sets the compression policy preference for the library.
    void compressionPolicy(CompressionPolicy c);
    /// Returns the compression policy used by the library.
    CompressionPolicy& compressionPolicy(void);
    
    /** Returns the stream object needed to read a file.
     *  \param[in] source input filename or string.
     *  \param[in] as_file treats #source as a file otherwise returns it treats it as a string stream.
     *  \returns the object needed to read the file or stream. NULL when the file cannot be loaded.
     */
    std::istream * iZipStream(const std::string &source, bool as_file = true);
    
    /** Returns the stream object needed to write to a file.
     *  \param[in] filename input filename.
     */
    std::ostream * oZipStream(const std::string &filename);
}

#endif

