#include "../common/common.hpp"
#include <unordered_map>
#include <fstream>
#include <memory>
#include <utility>
#include <queue>
//#include "srv_file_compression.hpp"
#include "srv_file_compression.cpp"

// ############################################################################
// ############################################################################

std::string smallestStringWithSwaps(std::string s,
                                    std::vector<std::vector<int> > &pairs)
{
    // Find the pairs that are all connected.
    const int n = static_cast<int>(s.size());
    std::vector<int> connected(n);
    std::unordered_map<int, int> lut;
    for (int i = 0; i < n; ++i) connected[i] = i;
    auto update = [&](int &first, int &second)
    {
        while (connected[first] != first) first = connected[first];
        while (connected[second] != second)
            second = std::exchange(connected[second], first);
    };
    for (const auto &p : pairs)
    {
        int first = p[0], second = p[1];
        if (first > second) std::swap(first, second);
        update(first, second);
        connected[second] = first;
    }
    for (int i = 0; i < n; ++i)
    {
        int first = i, second = i;
        update(first, second);
        if (lut.count(first) == 0)
            lut[first] = static_cast<int>(lut.size());
    }
    for (int i = 0; i < n; ++i)
        connected[i] = lut[connected[i]];
    const int number_of_blocks = static_cast<int>(lut.size());
    
    std::vector<std::priority_queue<char, std::vector<char>,
                            std::greater<char> > > characters(number_of_blocks);
    for (int i = 0; i < n; ++i)
        characters[connected[i]].push(s[i]);
    for (int i = 0; i < n; ++i)
    {
        s[i] = characters[connected[i]].top();
        characters[connected[i]].pop();
    }
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<std::vector<int> > pairs,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestStringWithSwaps(s, pairs);
    if (solution == result) std::cout << showResult(true);
    else std::cout << showResult(false);
    if (solution.size() > 10)
    {
        std::cout << " expected '" << solution.substr(0, 10)
                  << "...' and obtained '" << result.substr(0, 10) << "...'.\n";
    }
    else std::cout << " expected '" << solution << "' and obtained '" << result << "'.\n";
}

void test(const char * filename, unsigned int trials)
{
    srv::compressionPolicy(srv::GZipCompression);
    auto compressed_file = srv::iZipStream(filename);
    std::istream &file = *compressed_file;
    std::string s, solution;
    int number_of_pairs;
    std::getline(file, s);
    file >> solution;
    file >> number_of_pairs;
    std::vector<std::vector<int> > pairs(number_of_pairs);
    for (int i = 0; i < number_of_pairs; ++i)
    {
        pairs[i].resize(2);
        file >> pairs[i][0];
        file >> pairs[i][1];
    }
    delete compressed_file;
    test(s, pairs, solution, trials);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("dcab", {{0, 3}, {1, 2}}, "bacd", trials);
    test("dcab", {{0, 3}, {1, 2}, {0, 2}}, "abcd", trials);
    test("cba", {{0, 1}, {1, 2}}, "abc", trials);
    test("testD.bz2", trials);
    return 0;
}


