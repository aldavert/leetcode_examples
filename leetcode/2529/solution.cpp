#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumCount(std::vector<int> nums)
{
    auto it_zero = std::lower_bound(nums.begin(), nums.end(), 0);
    auto it_ones = std::lower_bound(it_zero, nums.end(), 1);
    return std::max(static_cast<int>(it_zero - nums.begin()),
                    static_cast<int>(nums.size() - (it_ones - nums.begin())));
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumCount(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-2, -1, -1, 1, 2, 3}, 3, trials);
    test({-3, -2, -1, 0, 0, 1, 2}, 3, trials);
    test({5, 20, 66, 1314}, 4, trials);
    return 0;
}


