#include "../common/common.hpp"

// ############################################################################
// ############################################################################

double findMaxAverage(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    const double dk = static_cast<double>(k);
    double result = 0.0;
    for (int i = 0; i < k; ++i)
        result += static_cast<double>(nums[i]);
    double current = result;
    result /= dk;
    for (int i = k; i < n; ++i)
    {
        current += static_cast<double>(nums[i] - nums[i - k]);
        result = std::max(result, current / dk);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, double solution, unsigned int trials = 1)
{
    double result = -10e7;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMaxAverage(nums, k);
    showResult(std::abs(solution - result) < 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 12, -5, -6, 50, 3}, 4, 12.75, trials);
    test({5}, 1, 5.0, trials);
    return 0;
}


