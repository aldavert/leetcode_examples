#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxPower(std::string s)
{
    int result = 0, streak = 0;
    char previous = '\0';
    for (char c : s)
    {
        if (c == previous)
            ++streak;
        else
        {
            result = std::max(result, streak);
            streak = 1;
        }
        previous = c;
    }
    return std::max(result, streak);
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxPower(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leetcode", 2, trials);
    test("abbcccddddeeeeedcba", 5, trials);
    test("triplepillooooow", 5, trials);
    test("hooraaaaaaaaaaay", 11, trials);
    test("tourist", 1, trials);
    return 0;
}


