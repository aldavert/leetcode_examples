#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool checkValidGrid(std::vector<std::vector<int> > grid)
{
    if (grid[0][0] != 0) return false;
    const int n = static_cast<int>(grid.size());
    std::vector<std::pair<int, int> > pos(n * n);
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            pos[grid[i][j]] = {i, j};
    for (int i = 1; i < n * n; ++i)
    {
        auto [x1, y1] = pos[i - 1];
        auto [x2, y2] = pos[i];
        int dx = std::abs(x1 - x2);
        int dy = std::abs(y1 - y2);
        bool ok = ((dx == 1) && (dy == 2)) || ((dx == 2) && (dy == 1));
        if (!ok) return false;
    }
    return true;
}
#else
bool checkValidGrid(std::vector<std::vector<int> > grid)
{
    constexpr std::pair<int, int> dirs[] = {
        {-2, 1}, {-1, 2}, {1, 2}, {2, 1}, {2, -1}, {1, -2}, {-1, -2}, {-2, -1}};
    const int n = static_cast<int>(grid.size());
    auto nextGrid = [&](int i, int j, int target) -> std::pair<int, int>
    {
        for (const auto& [dx, dy] : dirs)
        {
            const int x = i + dx, y = j + dy;
            if ((x < 0) || (x >= n) || (y < 0) || (y >= n)) continue;
            if (grid[x][y] == target) return {x, y};
        }
        return {-1, -1};
    };
    
    for (int target = 1, i = 0, j = 0; target < n * n; ++target)
    {
        const auto [x, y] = nextGrid(i, j, target);
        if ((x == -1) && (y == -1))
            return false;
        i = x;
        j = y;
    }
    return true;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkValidGrid(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{ 0, 11, 16,  5, 20},
          {17,  4, 19, 10, 15},
          {12,  1,  8, 21,  6},
          { 3, 18, 23, 14,  9},
          {24, 13,  2,  7, 22}}, true, trials);
    test({{0, 3, 6},
          {5, 8, 1},
          {2, 7, 4}}, false, trials);
    test({{8, 3, 6},
          {5, 0, 1},
          {2, 7, 4}}, false, trials);
    return 0;
}


