#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maxTaxiEarnings(int n, std::vector<std::vector<int> > rides)
{
    std::vector<std::vector<std::pair<int, int> > > end_to_start_and_earns(n + 1);
    std::vector<long> dp(n + 1);
    for (const auto &ride : rides)
        end_to_start_and_earns[ride[1]].emplace_back(ride[0],
                                                     ride[1] - ride[0] + ride[2]);
    for (int i = 1; i <= n; ++i)
    {
        dp[i] = dp[i - 1];
        for (auto [start, earn] : end_to_start_and_earns[i])
            dp[i] = std::max(dp[i], dp[start] + earn);
    }
    return dp[n];
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > rides,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxTaxiEarnings(n, rides);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{2, 5, 4}, {1, 5, 1}}, 7, trials);
    test(20, {{1, 6, 1}, {3, 10, 2}, {10, 12, 3}, {11, 12, 2}, {12, 15, 2},
              {13, 18, 1}}, 20, trials);
    return 0;
}


