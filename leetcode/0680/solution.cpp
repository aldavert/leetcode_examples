#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool validPalindrome(std::string s)
{
    auto check = [&](auto &&self, int left, int right, int remove) -> bool
    {
        for (; left < right; ++left, --right)
        {
            if (s[left] != s[right])
            {
                if (remove)
                    return self(self, left + 1, right, remove - 1)
                        || self(self, left, right - 1, remove - 1);
                else return false;
            }
        }
        return true;
    };
    return check(check, 0, static_cast<int>(s.size() - 1), 1);
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = validPalindrome(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aba" ,  true, trials);
    test("abca",  true, trials);
    test("abc" , false, trials);
    test("aguokepatgbnvfqmgmlcupuufxoohdfpgjdmysgv"
         "hmvffcnqxjjxqncffvmhvgsymdjgpfdhooxfuupu"
         "culmgmqfvnbgtapekouga", true, trials);
    return 0;
// c != u
// removing U we have: c == c ok
// removing C we have: u == u ok
////aguokepatgbnvfqmg
////ml[_]cupuufxoohdfpg
////jdmysgvhmvffcnqxj
////jxqncffvmhvgsymdj
////gpfdhooxfuupuc[U]lm
////gmqfvnbgtapekouga
}


