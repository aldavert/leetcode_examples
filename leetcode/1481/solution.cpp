#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int findLeastNumOfUniqueInts(std::vector<int> arr, int k)
{
    std::unordered_map<int, int> histogram;
    for (int number : arr)
        ++histogram[number];
    std::vector<int> sorted_frequencies;
    for (auto [number, frequency] : histogram)
        sorted_frequencies.push_back(frequency);
    std::sort(sorted_frequencies.begin(), sorted_frequencies.end());
    int index = 0, result = 0;
    for (; k > 0; ++index)
    {
        int diff = std::max(0, k - sorted_frequencies[index]);
        result += (k - diff == sorted_frequencies[index]);
        k = diff;
    }
    return static_cast<int>(sorted_frequencies.size()) - result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLeastNumOfUniqueInts(arr, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 5, 4}, 1, 1, trials);
    test({4, 3, 1, 1, 3, 3, 2}, 3, 2, trials);
    test({2, 1, 1, 3, 3, 3}, 3, 1, trials);
    return 0;
}


