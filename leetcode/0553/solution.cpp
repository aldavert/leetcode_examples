#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string optimalDivision(std::vector<int> nums)
{
    std::string result = std::to_string(nums[0]);
    if (nums.size() == 1) return result;
    else if (nums.size() == 2) return result + "/" + std::to_string(nums[1]);
    result += "/(" + std::to_string(nums[1]);
    for (size_t i = 2; i < nums.size(); ++i)
        result += "/" + std::to_string(nums[i]);
    result += ")";
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = optimalDivision(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1000, 100, 10, 2}, "1000/(100/10/2)", trials);
    test({2, 3, 4}, "2/(3/4)", trials);
    return 0;
}


