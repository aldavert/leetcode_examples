#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minMaxDifference(int num)
{
    int largest = 0, smallest = 9;
    for (int work = num; work > 0; work /= 10)
    {
        int digit = work % 10;
        if (digit < 9) largest = digit;
        if (digit > 0) smallest = digit;
    }
    int result = 0;
    for (int work = num, shift = 1; work > 0; work /= 10, shift *= 10)
    {
        int digit = work % 10;
        result += (((digit == largest)?9:digit) - ((digit == smallest)?0:digit)) * shift;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int num, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minMaxDifference(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(11891, 99009, trials);
    test(90, 99, trials);
    return 0;
}


