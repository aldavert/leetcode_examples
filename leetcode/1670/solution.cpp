#include "../common/common.hpp"
#include <deque>

constexpr int null = -1;

// ############################################################################
// ############################################################################

class FrontMiddleBackQueue
{
public:
    FrontMiddleBackQueue(void) {}
    void pushFront(int val)
    {
        m_front_queue.push_front(val);
        moveFrontToBackIfNeeded();
    }
    void pushMiddle(int val)
    {
        if (m_front_queue.size() == m_back_queue.size())
            m_back_queue.push_front(val);
        else m_front_queue.push_back(val);
    }
    void pushBack(int val)
    {
        m_back_queue.push_back(val);
        moveBackToFrontIfNeeded();
    }
    int popFront(void)
    {
        if (!m_front_queue.empty())
        {
            const int x = m_front_queue.front();
            m_front_queue.pop_front();
            moveBackToFrontIfNeeded();
            return x;
        }
        if (!m_back_queue.empty())
        {
            const int x = m_back_queue.front();
            m_back_queue.pop_front();
            moveFrontToBackIfNeeded();
            return x;
        }
        return -1;
    }
    int popMiddle(void)
    {
        if (m_front_queue.empty() && m_back_queue.empty())
            return -1;
        if (m_front_queue.size() + 1 == m_back_queue.size())
        {
            const int x = m_back_queue.front();
            m_back_queue.pop_front();
            return x;
        }
        else
        {
            const int x = m_front_queue.back();
            m_front_queue.pop_back();
            return x;
        }
    }
    int popBack(void)
    {
        if (m_back_queue.empty()) return -1;
        const int x = m_back_queue.back();
        m_back_queue.pop_back();
        moveFrontToBackIfNeeded();
        return x;
    }
private:
    std::deque<int> m_front_queue;
    std::deque<int> m_back_queue;
    
    void moveFrontToBackIfNeeded(void)
    {
        if (m_front_queue.size() - 1 == m_back_queue.size())
        {
            int x = m_front_queue.back();
            m_front_queue.pop_back();
            m_back_queue.push_front(x);
        }
    }
    void moveBackToFrontIfNeeded(void)
    {
        if (m_front_queue.size() + 2 == m_back_queue.size())
        {
            const int x = m_back_queue.front();
            m_back_queue.pop_front();
            m_front_queue.push_back(x);
        }
    }
};

// ############################################################################
// ############################################################################

enum class OP { PUSH_FRONT, PUSH_MIDDLE, PUSH_BACK, POP_FRONT, POP_MIDDLE, POP_BACK };

void test(std::vector<OP> op,
          std::vector<int> input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        FrontMiddleBackQueue obj;
        result.clear();
        for (size_t i = 0; i < op.size(); ++i)
        {
            switch (op[i])
            {
            case OP::PUSH_FRONT:
                result.push_back(null);
                obj.pushFront(input[i]);
                break;
            case OP::PUSH_MIDDLE:
                result.push_back(null);
                obj.pushMiddle(input[i]);
                break;
            case OP::PUSH_BACK:
                result.push_back(null);
                obj.pushBack(input[i]);
                break;
            case OP::POP_FRONT:
                result.push_back(obj.popFront());
                break;
            case OP::POP_MIDDLE:
                result.push_back(obj.popMiddle());
                break;
            case OP::POP_BACK:
                result.push_back(obj.popBack());
                break;
            default:
                std::cerr << "[ERROR] Unknown operation.\n";
                return;
            };
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::PUSH_FRONT, OP::PUSH_BACK, OP::PUSH_MIDDLE, OP::PUSH_MIDDLE,
          OP::POP_FRONT, OP::POP_MIDDLE, OP::POP_MIDDLE, OP::POP_BACK, OP::POP_FRONT},
         {1, 2, 3, 4, null, null, null, null, null},
         {null, null, null, null, 1, 3, 4, 2, -1}, trials);
    return 0;
}


