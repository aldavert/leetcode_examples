#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> arrayChange(std::vector<int> nums,
                             std::vector<std::vector<int> > operations)
{
    std::unordered_map<int, int> num_to_index;
    
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        num_to_index[nums[i]] = i;
    for (const auto &o : operations)
    {
        int index = num_to_index[o[0]];
        nums[index] = o[1];
        num_to_index.erase(o[0]);
        num_to_index[o[1]] = index;
    }
    return nums;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::vector<int> > operations,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = arrayChange(nums, operations);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 4, 6}, {{1, 3}, {4, 7}, {6, 1}}, {3, 2, 7, 1}, trials);
    test({1, 2}, {{1, 3}, {2, 1}, {3, 2}}, {2, 1}, trials);
    return 0;
}


