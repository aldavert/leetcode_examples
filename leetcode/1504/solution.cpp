#include "../common/common.hpp"
#include <stack>
#include <numeric>

// ############################################################################
// ############################################################################

int numSubmat(std::vector<std::vector<int> > mat)
{
    const int n = static_cast<int>(mat[0].size());
    std::vector<int> hist(n);
    int result = 0;
    auto count = [&hist,&n](void) -> int
    {
        std::vector<int> submatrices(hist.size());
        std::stack<int> stack;
        for (int i = 0; i < n; ++i)
        {
            while (!stack.empty() && (hist[stack.top()] >= hist[i]))
                stack.pop();
            if (!stack.empty())
                submatrices[i] = submatrices[stack.top()] + hist[i] * (i - stack.top());
            else submatrices[i] = hist[i] * (i + 1);
            stack.push(i);
        };
        return std::accumulate(submatrices.begin(), submatrices.end(), 0);
    };
    
    for (const auto &row : mat)
    {
        for (int i = 0; i < n; ++i)
            hist[i] = (row[i] == 0)?0:hist[i] + 1;
        result += count();
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > mat, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numSubmat(mat);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0, 1}, {1, 1, 0}, {1, 1, 0}}, 13, trials);
    test({{0, 1, 1, 0}, {0, 1, 1, 1}, {1, 1, 1, 0}}, 24, trials);
    return 0;
}


