#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int numberOfPaths(std::vector<std::vector<int> > grid, int k)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(grid.size()),
              m = static_cast<int>(grid[0].size());
    std::vector<std::vector<std::vector<long> > > dp(n,
            std::vector<std::vector<long> >(m, std::vector<long>(k)));
    
    for (int i = 0, sum = 0; i < m; ++i)
    {
        sum += grid[0][i];
        ++dp[0][i][sum % k];
    }
    for (int i = 0, sum = 0; i < n; ++i)
    {
        sum += grid[i][0];
        ++dp[i][0][sum % k];
    }
    for (int i = 1; i < n; ++i)
    {
        for (int j = 1; j < m; ++j)
        {
            int p = grid[i][j] % k;
            for (int c = 0; c < k; ++c)
            {
                long r = dp[i - 1][j][c] + dp[i][j - 1][c];
                dp[i][j][(p + c) % k] = (dp[i][j][(p + c) % k] + r) % MOD;
            }
        }
    }
    int result = static_cast<int>(dp[n - 1][m - 1][0]);
    if ((n == 1) && (m == 1))
        return (result - 1 >= 0)?result - 1:0;
    return result;
}
#else
int numberOfPaths(std::vector<std::vector<int> > grid, int k)
{
    constexpr int MOD = 1'000'000'007;
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    std::vector<std::vector<std::vector<int> > > dp(m,
    std::vector<std::vector<int> >(n, std::vector<int>(k)));
    
    dp[0][0][grid[0][0] % k] = 1;
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            for (int sum = 0; sum < k; ++sum)
            {
                int new_sum = (grid[i][j] + sum) % k;
                long value = dp[i][j][new_sum];
                if (i > 0) value += dp[i - 1][j][sum];
                if (j > 0) value += dp[i][j - 1][sum];
                dp[i][j][new_sum] = static_cast<int>(value % MOD);
            }
        }
    }
    return dp[m - 1][n - 1][0];
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfPaths(grid, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{5, 2, 4}, {3, 0, 5}, {0, 7, 2}}, 3, 2, trials);
    test({{0, 0}}, 5, 1, trials);
    test({{7, 3, 4, 9}, {2, 3, 6, 2}, {2, 3, 7, 0}}, 1, 10, trials);
    return 0;
}


