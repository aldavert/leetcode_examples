#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<int> getFinalState(std::vector<int> nums, int k, int multiplier)
{
    std::vector<int> result(nums.size());
    std::priority_queue<std::pair<int, int>,
                        std::vector<std::pair<int, int> >, std::greater<> > heap;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        heap.emplace(nums[i], i);
    while (k-- > 0)
    {
        const auto [num, i] = heap.top();
        heap.pop();
        heap.emplace(num * multiplier, i);
    }
    while (!heap.empty())
    {
        const auto [num, i] = heap.top();
        heap.pop();
        result[i] = num;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          int multiplier,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getFinalState(nums, k, multiplier);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3, 5, 6}, 5, 2, {8, 4, 6, 5, 6}, trials);
    test({1, 2}, 3, 4, {16, 8}, trials);
    return 0;
}


