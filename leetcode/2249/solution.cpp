#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countLatticePoints(std::vector<std::vector<int> > circles)
{
    int result = 0;
    for (int x = 0; x < 201; ++x)
        for (int y = 0; y < 201; ++y)
        result += std::any_of(circles.begin(), circles.end(),
                [&](const auto& c) -> bool { return (c[0] - x) * (c[0] - x)
                                    + (c[1] - y) * (c[1] - y) <= c[2] * c[2]; });
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > circles, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countLatticePoints(circles);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 2, 1}}, 5, trials);
    test({{2, 2, 2}, {3, 4, 1}}, 16, trials);
    return 0;
}


