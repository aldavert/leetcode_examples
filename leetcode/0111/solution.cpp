#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int minDepth(TreeNode * root)
{
    if (!root) return 0;
    int depth = std::numeric_limits<int>::max();
    if (root->left) depth = std::min(depth, minDepth(root->left));
    if (root->right) depth = std::min(depth, minDepth(root->right));
    if (depth < std::numeric_limits<int>::max())
        return depth + 1;
    else return 1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = minDepth(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 9, 20, null, null, 15, 7}, 2, trials);
    test({2, null, 3, null, 4, null, 5, null, 6}, 5, trials);
    return 0;
}


