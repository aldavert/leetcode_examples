#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string truncateSentence(std::string s, int k)
{
    const int n = static_cast<int>(s.size());
    for (int i = 0, counter = 0; i < n; ++i)
    {
        if (s[i] == ' ')
        {
            ++counter;
            if (counter == k)
                return s.substr(0, i);
        }
    }
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = truncateSentence(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("Hello how are you Contestant", 4, "Hello how are you", trials);
    test("What is the solution to this problem", 4, "What is the solution", trials);
    test("chopper is not a tanuki", 5, "chopper is not a tanuki", trials);
    return 0;
}


