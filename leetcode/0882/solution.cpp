#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int reachableNodes(std::vector<std::vector<int>> &edges, int maxMoves, int n)
{
    std::vector<std::pair<int, int> > g[3001];
    for (const auto &e: edges)
    {
        g[e[0]].push_back({e[1], e[2] + 1});
        g[e[1]].push_back({e[0], e[2] + 1});
    }
    
    std::priority_queue<std::pair<int, int>,
                        std::vector<std::pair<int, int> >,
                        std::greater<std::pair<int, int> > > queue;
    queue.push({0, 0});
    std::vector<int> distances(n, std::numeric_limits<int>::max());
    distances[0] = 0;

    while (!queue.empty())
    {
        auto [dist, node] = queue.top();
        queue.pop();
        for (auto [dst, cnt] : g[node])
        {
            if (distances[dst] > dist + cnt)
            {
                distances[dst] = dist + cnt;
                queue.push({distances[dst], dst});
            }
        }
    }
    
    int result = 0;
    for (int i = 0; i < n; ++i)
        if (distances[i] <= maxMoves) ++result; 
    for (const auto &e : edges)
    {
        int src = e[0], dest = e[1], between = e[2];
        int x = std::max(0, (maxMoves - distances[src]));
        int y = std::max(0, (maxMoves - distances[dest]));
        result += std::min(between, x + y);
    }
    return result;
}
#else
int reachableNodes(std::vector<std::vector<int> > &edges,
                   int maxMoves,
                   [[maybe_unused]] int n)
{
    struct Node
    {
        std::unordered_map<int, int> neighbors;
        bool visited = false;
    };
    struct Position
    {
        int moves = 0;
        int node = 0;
        int count = 0;
        bool operator<(const Position &other) const { return moves < other.moves; }
        bool operator>(const Position &other) const { return moves > other.moves; }
        bool operator==(const Position &other) const { return moves == other.moves; }
    };
    Node graph[3001];
    for (const auto &e : edges)
    {
        graph[e[0]].neighbors[static_cast<short>(e[1])] = static_cast<short>(e[2] + 1);
        graph[e[1]].neighbors[static_cast<short>(e[0])] = static_cast<short>(e[2] + 1);
    }
    int result = 0;
    std::priority_queue<Position, std::vector<Position>, std::greater<Position> > queue;
    queue.push({maxMoves, 0, 1});
    while (!queue.empty())
    {
        auto [moves, current, count] = queue.top();
        queue.pop();
        if (!graph[current].visited)
        {
            result += count;
            graph[current].visited = true;
            for (auto [next, c] : graph[current].neighbors)
            {
                graph[next].neighbors[current] -= moves;
                if (c > moves) result += moves;
                else queue.push({moves - c, next, c});
            }
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges,
          int maxMoves,
          int n,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = reachableNodes(edges, maxMoves, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 10}, {0, 2, 1}, {1, 2, 2}}, 6, 3, 13, trials);
    test({{0, 1, 4}, {1, 2, 6}, {0, 2, 8}, {1, 3, 1}}, 10, 4, 23, trials);
    test({{1, 2, 4}, {1, 4, 5}, {1, 3, 1}, {2, 3, 4}, {3, 4, 5}}, 17, 5, 1, trials);
    test({{2, 4, 2}, {3, 4, 5}, {2, 3, 1}, {0, 2, 1}, {0, 3, 5}}, 14, 5, 18, trials);
    return 0;
}


