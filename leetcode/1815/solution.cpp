#include "../common/common.hpp"
#include <map>
#include <cmath>

// ############################################################################
// ############################################################################

int maxHappyGroups(int batchSize, std::vector<int> groups)
{
    int happy = 0;
    std::vector<int> freq(batchSize);
    std::map<std::vector<int>, int> memo;
    auto dp = [&](auto &&self, int r) -> int
    {
        if (auto it = memo.find(freq); it != memo.end())
            return it->second;
        int result = 0;
        if (std::any_of(freq.begin(), freq.end(), std::isnormal<int>))
        {
            for (int i = 0; i < batchSize; ++i)
            {
                if (freq[i])
                {
                    --freq[i];
                    result = std::max(result, self(self, (r + i) % batchSize));
                    ++freq[i];
                }
            }
            if (r == 0) ++result;
        }
        return memo[freq] = result;
    };
    
    for (int g : groups)
    {
        g %= batchSize;
        if (g == 0) ++happy;
        else if (freq[batchSize - g]) --freq[batchSize - g], ++happy;
        else ++freq[g];
    }
    
    return happy + dp(dp, 0);
}

// ############################################################################
// ############################################################################

void test(int batchSize, std::vector<int> groups, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxHappyGroups(batchSize, groups);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {1, 2, 3, 4, 5, 6}, 4, trials);
    test(4, {1, 3, 2, 5, 2, 2, 1, 6}, 4, trials);
    return 0;
}


