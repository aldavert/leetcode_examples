#include "../common/common.hpp"
#include <list>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class StreamChecker
{
    class Node
    {
    public:
        Node * child[26];
        bool end;
        Node() : end(false)
        {
            for (int i = 0; i < 26; ++i)
                child[i] = nullptr;
        }
    };
    Node * head;
    std::list<Node *> active;
    void insert(Node * h, std::string &s)
    {
        const int n = static_cast<int>(s.size());
        Node * current = h;
        for (int i = 0; i < n; ++i)
        {
            int c = s[i] - 'a';
            if (!current->child[c])
                current->child[c] = new Node();
            current = current->child[c];
        }
        current->end = true;
    }
public:
    StreamChecker(std::vector<std::string> &words)
    {
        head = new Node();
        for (auto &w : words)
            insert(head, w);
    }
    
    bool query(char letter)
    {
        letter -= 'a';
        if (head->child[static_cast<int>(letter)])
            active.push_back(head);
        bool result = false;
        for (auto begin = active.begin(), end = active.end(); begin != end;)
        {
            Node * current = *begin;
            if (current->child[static_cast<int>(letter)])
            {
                *begin = current = current->child[static_cast<int>(letter)];
                result |= current->end;
                ++begin;
            }
            else begin = active.erase(begin);
        }
        return result;
    }
};

// ############################################################################
// ############################################################################

enum class OP { STREAMCHECKER, QUERY };

void test(std::vector<OP> op,
          std::vector<std::vector<std::string> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    const int nelements = static_cast<int>(op.size());
    std::vector<int> result(op.size(), -1);
    for (unsigned int i = 0; i < trials; ++i)
    {
        StreamChecker * checker = nullptr;
        for (int j = 0; j < nelements; ++j)
        {
            switch (op[j])
            {
                case OP::STREAMCHECKER:
                    checker = new StreamChecker(input[j]);
                    result[j] = null;
                    break;
                case OP::QUERY:
                    result[j] = checker->query(input[j][0][0]);
                    break;
                default:
                    std::cout << "[WARNING] " << j + 1
                              << " operation not implemented, yet.\n";
            }
        }
        delete checker;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::STREAMCHECKER,  OP::QUERY, OP::QUERY, OP::QUERY, OP::QUERY,
          OP::QUERY, OP::QUERY, OP::QUERY, OP::QUERY, OP::QUERY, OP::QUERY,
          OP::QUERY, OP::QUERY}, {{"cd", "f", "kl"}, {"a"}, {"b"}, {"c"}, {"d"},
          {"e"}, {"f"}, {"g"}, {"h"}, {"i"}, {"j"}, {"k"}, {"l"}},
         {null, false, false, false, true, false, true, false, false, false,
         false, false, true}, trials);
    return 0;
}


