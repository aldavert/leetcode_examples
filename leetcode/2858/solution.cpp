#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<int> minEdgeReversals(int n, std::vector<std::vector<int> > edges)
{
    constexpr int MAX = 100'010;
    std::vector<std::pair<int, int> > adj[MAX];
    std::vector<int> result(n);
    int count = 0;
    auto DFS1 = [&](auto &&self, int x, int p) -> void
    {
        for (auto [y, d] : adj[x])
        {
            if (y != p)
            {
                count += (d ^ 1);
                self(self, y, x);
            }
        }
    };
    auto DFS2 = [&](auto &&self, int x, int p, int cur) -> void
    {
        result[x] = cur;
        for (auto [y, d] : adj[x])
            if (y != p)
                self(self, y, x, cur + (2 * d - 1));
    };
    for (const auto& e : edges)
    {
        adj[e[0]].emplace_back(e[1], 1);
        adj[e[1]].emplace_back(e[0], 0);
    }
    DFS1(DFS1, 0, -1);
    DFS2(DFS2, 0, -1, count);
    return result;
}
#else
std::vector<int> minEdgeReversals(int n, std::vector<std::vector<int> > edges)
{
    std::vector<int> result(n), mem(n, -1);
    std::vector<std::vector<std::pair<int, bool> > > graph(n);
    std::vector<bool> seen(n);
    auto dfs = [&](auto &&self, int u) -> void
    {
        seen[u] = true;
        for (const auto& [v, is_forward] : graph[u])
        {
            if (seen[v]) continue;
            seen[v] = true;
            result[v] = result[u] + (2 * is_forward - 1);
            self(self, v);
        }
    };
    auto edgeReversals = [&](auto &&self, int u) -> int
    {
        if (mem[u] != -1) return mem[u];
        int r = 0;
        seen[u] = true;
        for (const auto& [v, is_forward] : graph[u])
        {
            if (seen[v]) continue;
            seen[v] = true;
            r += self(self, v) + !is_forward;
        }
        return mem[u] = r;
    };
    
    for (const auto &edge : edges)
    {
        graph[edge[0]].emplace_back(edge[1], true);
        graph[edge[1]].emplace_back(edge[0], false);
    }
    result[0] = edgeReversals(edgeReversals, 0);
    seen = std::vector<bool>(n);
    dfs(dfs, 0);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minEdgeReversals(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{2, 0}, {2, 1}, {1, 3}}, {1, 1, 0, 2}, trials);
    test(3, {{1, 2}, {2, 0}}, {2, 0, 1}, trials);
    return 0;
}


