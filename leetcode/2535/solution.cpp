#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int differenceOfSum(std::vector<int> nums)
{
    int sum_element = 0, sum_digit = 0;
    for (int num : nums)
    {
        sum_element += num;
        while (num)
            sum_digit += num % 10,
            num /= 10;
    }
    return std::abs(sum_element - sum_digit);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = differenceOfSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 15, 6, 3}, 9, trials);
    test({1, 2, 3, 4}, 0, trials);
    return 0;
}



