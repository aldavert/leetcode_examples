#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestSubsequence(std::vector<int> nums)
{
    int m = *std::max_element(nums.begin(), nums.end());
    int diff = m - *std::min_element(nums.begin(), nums.end());
    std::vector<std::vector<int> > dp(m + 1, std::vector<int>(diff + 1));
    for (int value : nums)
    {
        int next = 1;
        for (int d = diff; d >= 0; --d)
        {
            if ((value - d >= 0) && (dp[value - d][d] + 1 > next))
                next = dp[value - d][d] + 1;
            if ((value + d <= m) && (dp[value + d][d] + 1 > next))
                next = dp[value + d][d] + 1;
            dp[value][d] = next;
        }
    }
    int result = dp[0][0];
    for (const auto &row : dp)
        for (int value : row)
            result = std::max(result, value);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestSubsequence(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({16, 6, 3}, 3, trials);
    test({6, 5, 3, 4, 2, 1}, 4, trials);
    test({10, 20, 10, 19, 10, 20}, 5, trials);
    return 0;
}


