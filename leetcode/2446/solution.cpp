#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool haveConflict(std::vector<std::string> event1, std::vector<std::string>& event2)
{
    auto toInteger = [](std::string time) -> int
    {
        return static_cast<int>(10 * (time[0] - '0') + (time[1] - '0')) * 60
             + static_cast<int>(10 * (time[3] - '0') + (time[4] - '0'));
    };
    return std::max(toInteger(event1[0]), toInteger(event2[0]))
        <= std::min(toInteger(event1[1]), toInteger(event2[1]));
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> event1,
          std::vector<std::string> event2,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = haveConflict(event1, event2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"01:15", "02:00"}, {"02:00", "03:00"},  true, trials);
    test({"01:00", "02:00"}, {"01:20", "03:00"},  true, trials);
    test({"10:00", "11:00"}, {"14:00", "15:00"}, false, trials);
    return 0;
}


