#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int lengthOfLongestSubstring(std::string s)
{
    int position[128] = {}, initial_position = 0, length = 0;
    for (int idx = 0; char c : s)
    {
        initial_position = std::max(initial_position, position[static_cast<int>(c)]);
        position[static_cast<int>(c)] = ++idx;
        length = std::max(length, idx - initial_position);
    }
    return length;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = lengthOfLongestSubstring(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcabcbb", 3, trials);
    test("bbbbb", 1, trials);
    test("pwwkew", 3, trials);
    return 0;
}


