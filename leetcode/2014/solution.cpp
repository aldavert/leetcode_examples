#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::string longestSubsequenceRepeatedK(std::string s, int k)
{
    int count[26] = {};
    std::string result;
    std::vector<char> possible_chars;
    std::queue<std::string> q{{""}};
    auto isSubsequence = [&](const std::string &subsequence)
    {
        const int n = static_cast<int>(subsequence.size());
        for (int i = 0, m = k; char c : s)
        {
            if (c == subsequence[i])
            {
                if (++i == n)
                {
                    if (--m == 0)
                        return true;
                    i = 0;
                }
            }
        }
        return false;
    };
    
    for (const char c : s) ++count[c - 'a'];
    for (char c = 'a'; c <= 'z'; ++c)
        if (count[c - 'a'] >= k)
            possible_chars.push_back(c);
    while (!q.empty())
    {
        std::string current_subsequence = q.front();
        q.pop();
        if (current_subsequence.size() * k > s.size())
            return result;
        for (char c : possible_chars)
        {
            std::string new_subsequence = current_subsequence + c;
            if (isSubsequence(new_subsequence))
            {
                q.push(new_subsequence);
                result = new_subsequence;
            }
        }
    }
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestSubsequenceRepeatedK(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("letsleetcode", 2, "let", trials);
    test("bb", 2, "b", trials);
    test("ab", 2, "", trials);
    return 0;
}


