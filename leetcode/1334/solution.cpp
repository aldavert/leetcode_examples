#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findTheCity(int n, std::vector<std::vector<int> > edges, int distanceThreshold)
{
    int min_cities_count = n;
    std::vector<std::vector<int> > dist(n, std::vector<int>(n, distanceThreshold + 1));
    
    for (int i = 0; i < n; ++i) dist[i][i] = 0;
    for (const auto &edge : edges)
        dist[edge[0]][edge[1]] = edge[2],
        dist[edge[1]][edge[0]] = edge[2];
    for (int k = 0; k < n; ++k)
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                dist[i][j] = std::min(dist[i][j], dist[i][k] + dist[k][j]);
    
    int result = -1;
    for (int i = 0; i < n; ++i)
    {
        int cities_count = 0;
        for (int j = 0; j < n; ++j)
            if (dist[i][j] <= distanceThreshold)
                ++cities_count;
        if (cities_count <= min_cities_count)
        {
            result = i;
            min_cities_count = cities_count;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          int distanceThreshold,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findTheCity(n, edges, distanceThreshold);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{0, 1, 3}, {1, 2, 1}, {1, 3, 4}, {2, 3, 1}}, 4, 3, trials);
    test(5, {{0, 1, 2}, {0, 4, 8}, {1, 2, 3}, {1, 4, 2}, {2, 3, 1}, {3, 4, 1}},
         2, 0, trials);
    return 0;
}


