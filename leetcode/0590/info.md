# N-ary Tree Postorder Traversal

Given the `root` of an n-ary tree, return the *postorder traversal of its nodes' values*. Nary-Tree input serialization is represented in their level order traversal. Each group of children is separated by the null value.

#### Example 1:
> ```mermaid 
> graph TD;
> A((1))-->B((3))
> A-->C((2))
> A-->D((4))
> B-->E((5))
> B-->F((6))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [1, null, 3, 2, 4, null, 5, 6]`  
> *Output:* `[5, 6, 3, 2, 4, 1]`

#### Example 2:
> ```mermaid 
> graph TD;
> A((1))-->B((2))
> A-->C((3))
> A-->D((4))
> A-->E((5))
> C-->F((6))
> C-->G((7))
> D-->H((8))
> E-->I((9))
> E-->J((10))
> G-->K((11))
> H-->L((12))
> I-->M((13))
> K-->N((14))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [1, null, 2, 3, 4, 5, null, null, 6, 7, null, 8, null, 9, 10, null, null, 11, null, 12, null, 13, null, null, 14]`  
> *Output:* `[2, 6, 14, 11, 7, 3, 12, 8, 4, 13, 9, 10, 5, 1]`

#### Constraints:
- The number of nodes in the tree is in the range `[0, 10^4]`.
- `0 <= Node.val <= 10^4`
- The height of the n-ary tree is less than or equal to `1000`.

**Follow up:** Recursive solution is trivial, could you do it iteratively?


