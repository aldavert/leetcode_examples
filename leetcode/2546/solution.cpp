#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool makeStringsEqual(std::string s, std::string target)
{
    return (s.find('1') != std::string::npos) == (target.find('1') != std::string::npos);
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string target, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = makeStringsEqual(s, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1010", "0110", true, trials);
    test("11", "00", false, trials);
    return 0;
}


