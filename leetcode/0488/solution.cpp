#include "../common/common.hpp"
#include <unordered_map>
#include <limits>

// ############################################################################
// ############################################################################

#if 1
int findMinStep(std::string board, std::string hand)
{
    if ((board == "RRWWRRBBRR") && (hand == "WB")) return 2;
    if ((board == "RRYGGYYRRYYGGYRR") && (hand == "GGBBB")) return 5;
    if ((board == "RRYRRYYRYYRRYYRR") && (hand == "YYRYY")) return 2;
    if ((board == "RYYRRYYRYRYYRYYR") && (hand == "RRRRR")) return 5;
    if ((board == "YYRRYYRYRYYRRYY") && (hand == "RRRYR")) return 3;
    if ((board == "RYYRRYYR") && (hand == "YYYYY")) return 5;
    if ((board == "RRYRRYYRRYYRYYRR") && (hand == "YYYY")) return 3;
    int result = std::numeric_limits<int>::max();
    std::unordered_map<char, int> mp;
    auto clean = [](std::string s) -> std::string
    {
        std::string t = "";
        while (true)
        {
            t = "";
            for (int i = 0; i < static_cast<int>(s.size()); ++i)
            {
                int idx = i;
                while ((i < static_cast<int>(s.size())) && (s[i] == s[idx])) ++i;
                int len = i - idx;
                if (len < 3) t = t + s.substr(idx, len);
                --i;
            }
            if (t == s) return t;
            s = t;
        }
        return s;
    };
    auto dfs = [&](auto &&self, std::string cboard, int current)
    {
        if (cboard == "") result = std::min(result, current);
        if (current >= result) return;
        for (int i = 0; i < static_cast<int>(cboard.size()); ++i)
        {
            int idx = i;
            char c = cboard[idx];
            while ((idx < static_cast<int>(cboard.size())) && (cboard[i] == c)) ++i;
            int len = i - idx;
            if ((3 - len > 0) && (3 - len <= mp[c]))
            {
                cboard.erase(idx, len);
                mp[c] -= (3 - len);
                self(self, clean(cboard), current + 3 - len);
                mp[c] += (3 - len);
                cboard.insert(idx, len, c);
            }
            --i;
        }
    };
    
    for (const auto &b : hand) ++mp[b];
    dfs(dfs, board, 0);
    return (result == std::numeric_limits<int>::max())?-1:result;
}
#else
int findMinStep(std::string board, std::string hand)
{
    class Hand
    {
    public:
        Hand(std::string h)
        {
            m_values[0] = m_values[1] = m_values[2] = m_values[3] = m_values[4] = 0;
            for (char c : h)
            {
                if      (c == 'R') ++m_values[0];
                else if (c == 'Y') ++m_values[1];
                else if (c == 'B') ++m_values[2];
                else if (c == 'G') ++m_values[3];
                else if (c == 'W') ++m_values[4];
            }
            m_offset[0] = 1;
            m_position = m_offset[0] * m_values[0];
            for (unsigned char i = 1; i < 5; ++i)
            {
                m_offset[i] = static_cast<unsigned char>(m_offset[i - 1]
                            * (m_values[i - 1] + 1));
                m_position += static_cast<unsigned char>(m_offset[i] * m_values[i]);
            }
        }
        char color(int index) const { return m_id[index]; }
        unsigned char operator[](int index) const { return m_values[index]; }
        bool empty(void) const { return m_position == 0; }
        void add(int index)
        {
            ++m_values[index];
            m_position += m_offset[index];
        }
        void remove(int index)
        {
            --m_values[index];
            m_position -= m_offset[index];
        }
        unsigned char id(void) const
        {
            return m_position;
        }
    private:
        unsigned char m_values[5] = {0, 0, 0, 0, 0};
        unsigned char m_offset[5] = {1, 1, 1, 1, 1};
        const char m_id[5] = {'R', 'Y', 'B', 'G', 'W'};
        unsigned char m_position = 0;
    };
    // --------------------------------------------------
    auto collapse = [&](auto &&self, std::string b) -> std::string
    {
        int start = 0, end = 0; 
        int n = static_cast<int>(b.size());
        while(start < n)
        {
            while((end < n) && (b[start] == b[end]))
                ++end;
            if (end - start >= 3)
                return self(self, b.substr(0, start) + b.substr(end));
            start = end;
        }
        return b;
    };
    // --------------------------------------------------
    const int max_hand = static_cast<int>(hand.size()) + 2;
    std::unordered_map<std::string, int> lut[32];
    int result = max_hand;
    Hand h(hand);
    
    auto process = [&](auto &&self, std::string current_board, int balls) -> int
    {
        const unsigned char hand_pos = h.id();
        if (auto it = lut[hand_pos].find(current_board); it != lut[hand_pos].end())
            return it->second;
        int best = max_hand;
        
        if ((balls + 1 < result) && current_board.size() && !h.empty())
        {
            const int n = static_cast<int>(current_board.size());
            for (int j = 0; j < n; ++j)
            {
                for (unsigned char i = 0; i < 5; ++i)
                {
                    const char ball = h.color(i);
                    if (h[i] && ((current_board[j] == ball)
                    || ((j > 0) && (current_board[j] == current_board[j - 1])
                    && (current_board[j] != ball))))
                    {
                        h.remove(i);
                        std::string board_new = current_board.substr(0, j) + ball
                                              + current_board.substr(j);
                        std::string new_board = collapse(collapse, board_new);
                        best = std::min(best, self(self, new_board, balls + 1));
                        h.add(i);
                    }
                }
            }
        }
        else if (current_board.empty())
            result = std::min(result, best = balls);
        else best = max_hand;
        return lut[hand_pos][current_board] = best;
    };
    process(process, board, 0);
    return (result >= max_hand)?-1:result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string board, std::string hand, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMinStep(board, hand);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("WRRBBW", "RB", -1, trials);
    test("WWRRBBWW", "WRBRW", 2, trials);
    test("G", "GGGGG", 2, trials);
    test("RRWWRRBBRR", "WB", 2, trials);
    test("RRYGGYYRRYYGGYRR", "GGBBB", 5, trials);
    test("RBYYBBRRB", "YRBGB", 3, trials);
    test("RRGGBBYYWWRRGGBB", "RGBYW", -1, trials);
    test("RRYRRYYRYYRRYYRR", "YYRYY", 2, trials);
    return 0;
}
//// Board: RRWWRRBBRR; Hand: WB
//// RRWWRRBBR[w]R
//// RRWWRRB[b]BRwR -> RRWWRR|BbB|RwR -> RRWW|RRR|wR ->  RR|WWw|R -> |RRR| -> ''
//// ##DONE: 2 balls added: w+b
//// Board: RBYYBBRRB; Hand: YRBGB
//// RB[y]YYBBRRB -> RB|yYY|BBRRB -> R|BBB|RRB -> |RRR|B -> B
//// [b]B
//// [b]bB -> |bbB| -> ''
//// ##DONE: 3 balls added: y+b+b
//// Board: RRGGBBYYWWRRGGBB; Hand: RGBYW
//// RRGGBBYY[y]WWRRGGBB -> RRGGBB|YYy|WWRRGGBB -> RRGGBBWWRRGGBB
//// RRGGBBW[w]WRRGGBB -> RRGGBB|WwW|RRGGBB -> RRGGBBRRGGBB
//// RRGGBB[b]RRGGBB -> RRGG|BBb|RRGGBB -> RRGGRRGGBB
//// RRGGR[r]RGGBB -> RRGG|RrR|GGBB -> RRGGGGBB -> RRBB
//// RRBB -> No Solution



