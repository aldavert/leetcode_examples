#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkStraightLine(std::vector<std::vector<int> > coordinates)
{
    struct LineParameters
    {
        LineParameters(const std::vector<int> &p1, const std::vector<int> &p2)
        {
            int dx = p2[0] - p1[0];
            int dy = p2[1] - p1[1];
            a =  dy;
            b = -dx;
            c = p1[1] * dx - p1[0] * dy;
        }
        bool isLinePoint(int x, int y) const
        {
            return x * a + y * b + c == 0;
        }
        int a = 0;
        int b = 0;
        int c = 0;
    };
    LineParameters parameters(coordinates[0], coordinates[1]);
    for (size_t i = 2, n = coordinates.size(); i < n; ++i)
        if (!parameters.isLinePoint(coordinates[i][0], coordinates[i][1]))
            return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > coordinates,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkStraightLine(coordinates);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 3}, {3, 4}, {4, 5}, {5, 6}, {6, 7}}, true, trials);
    test({{1, 1}, {2, 2}, {3, 4}, {4, 5}, {5, 6}, {7, 7}}, false, trials);
    test({{0, 0}, {0, 1}, {0, -1}}, true, trials);
    return 0;
}


