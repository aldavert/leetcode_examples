#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minMaxWeight(int n,
                 std::vector<std::vector<int> > edges,
                 [[maybe_unused]] int threshold)
{
    constexpr int MAX = 1'000'000;
    std::vector<std::vector<std::pair<int, int> > > reversed_graph(n);
    std::vector<bool> seen(n);
    auto dfs = [&](auto &&self, int u, int max_weight) -> int
    {
        int result = 1;
        seen[u] = true;
        for (const auto &[v, w] : reversed_graph[u])
        {
            if ((w > max_weight) || seen[v]) continue;
            result += self(self, v, max_weight);
        }
        return result;
    };
    
    for (const auto &edge : edges)
        reversed_graph[edge[1]].push_back({edge[0], edge[2]});
    
    int l = 1, r = MAX + 1;
    while (l < r)
    {
        int m = (l + r) / 2;
        seen.assign(n, false);
        if (dfs(dfs, 0, m) == n) r = m;
        else l = m + 1;
    }
    return l == (MAX + 1) ? -1 : l;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          int threshold,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minMaxWeight(n, edges, threshold);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{1, 0, 1}, {2, 0, 2}, {3, 0, 1}, {4, 3, 1}, {2, 1, 1}}, 2, 1, trials);
    test(5, {{0, 1, 1}, {0, 2, 2}, {0, 3, 1}, {0, 4, 1}, {1, 2, 1}, {1, 4, 1}},
         1, -1, trials);
    test(5, {{1, 2, 1}, {1, 3, 3}, {1, 4, 5}, {2, 3, 2}, {3, 4, 2}, {4, 0, 1}},
         1, 2, trials);
    test(5, {{1, 2, 1}, {1, 3, 3}, {1, 4, 5}, {2, 3, 2}, {4, 0, 1}}, 1, -1, trials);
    return 0;
}


