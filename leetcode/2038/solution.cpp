#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool winnerOfGame(std::string colors)
{
    const int n = static_cast<int>(colors.size());
    int previous = 0, can_remove[] = {0, 0};
    for (int i = 1; i < n; ++i)
    {
        if (colors[i] != colors[i - 1])
        {
            can_remove[colors[previous] == 'B'] += std::max(0, i - previous - 2);
            previous = i;
        }
    }
    can_remove[colors.back() == 'B'] += std::max(0, n - previous - 2);
    return can_remove[0] > can_remove[1];
}
#else
bool winnerOfGame(std::string colors)
{
    const int n = static_cast<int>(colors.size());
    int previous = 0, can_remove_alice = 0, can_remove_bob = 0;
    for (int i = 1; i < n; ++i)
    {
        if (colors[i] != colors[i - 1])
        {
            if (colors[previous] == 'A')
                can_remove_alice += std::max(0, i - previous - 2);
            else can_remove_bob += std::max(0, i - previous - 2);
            previous = i;
        }
    }
    if (colors.back() == 'A')
        can_remove_alice += std::max(0, n - previous - 2);
    else can_remove_bob += std::max(0, n - previous - 2);
    return can_remove_alice > can_remove_bob;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string colors, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = winnerOfGame(colors);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("AAABABB", true, trials);
    test("AA", false, trials);
    test("ABBBBBBBAAA", false, trials);
    test("AAAABBBB", false, trials);
    return 0;
}


