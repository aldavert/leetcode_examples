#include "../common/common.hpp"
#include "../common/list.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

ListNode * insertGreatestCommonDivisors(ListNode * head)
{
    for (ListNode * curr = head; curr->next != nullptr;)
    {
        ListNode* inserted =
            new ListNode(std::gcd(curr->val, curr->next->val), curr->next);
        curr->next = inserted;
        curr = inserted->next;
    }
    return head;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> list, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(list);
        ListNode * nw_head = insertGreatestCommonDivisors(head);
        result = list2vec(nw_head);
        delete nw_head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({18, 6, 10, 3}, {18, 6, 6, 2, 10, 1, 3}, trials);
    test({7}, {7}, trials);
    return 0;
}




