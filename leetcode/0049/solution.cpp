#include "../common/common.hpp"
#include <fstream>
#include <sstream>
#include <map>
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 0
std::vector<std::vector<std::string> > groupAnagrams(std::vector<std::string> strs)
{
    // 3.2 seconds on PC | 50 ms on LEETCODE
    std::unordered_map<std::string, std::vector<std::string> > groups;
    std::vector<std::vector<std::string> > result;
    for (std::string str : strs)
    {
        std::string key = str;
        std::sort(key.begin(), key.end());
        groups[key].push_back(str);
    }
    for (auto& [_, group] : groups)
        result.emplace_back(std::move(group));
    return result;
}
#elif 1 // 2.6 seconds on PC | 23 ms on LEETCODE
std::vector<std::vector<std::string> > groupAnagrams(std::vector<std::string> strs)
{
    auto signature = [](const std::string &str) -> int
    {
        int result = 0;
        for (char c : str)
            result |= 1 << (c - 'a');
        return result;
    };
    std::vector<std::vector<std::string> > result;
    std::unordered_map<int, std::vector<int> > groups;
    const int n = static_cast<int>(strs.size());
    for (int i = 0; i < n; ++i)
        groups[signature(strs[i])].push_back(i);
    for (const auto& [_1, group] : groups)
    {
        if (group.size() == 1)
            result.push_back({strs[group[0]]});
        else
        {
            std::map<std::string, std::vector<int>> anagrams;
            for (int i : group)
            {
                std::string key = strs[i];
                std::sort(key.begin(), key.end());
                anagrams[key].push_back(i);
            }
            for (const auto& [_2, ana] : anagrams)
            {
                std::vector<std::string> current;
                for (int i : ana)
                    current.push_back(strs[i]);
                result.emplace_back(std::move(current));
            }
        }
    }
    return result;
}
#else // 2.3 seconds on PC | 38 ms on LEETCODE
std::vector<std::vector<std::string> > groupAnagrams(std::vector<std::string> strs)
{
    auto signature = [](const std::string &str) -> int
    {
        int result = 0;
        for (char c : str)
            result |= 1 << (c - 'a');
        return result;
    };
    std::vector<std::vector<std::string> > result;
    std::unordered_map<int, std::vector<int> > groups;
    const int n = static_cast<int>(strs.size());
    for (int i = 0; i < n; ++i)
        groups[signature(strs[i])].push_back(i);
    auto cmp = [](const std::string &a, const std::string &b) -> bool
    {
        if (a.size() < b.size()) return true;
        else if (a.size() > b.size()) return false;
        char hist[26] = {};
        for (char c : a)
            ++hist[c - 'a'];
        for (char c : b)
            --hist[c - 'a'];
        for (int i = 0; i < 26; ++i)
        {
            if (hist[i] < 0) return true;
            if (hist[i] > 0) return false;
        }
        return false;
    };
    for (const auto& [_1, group] : groups)
    {
        if (group.size() == 1)
            result.push_back({strs[group[0]]});
        else
        {
            std::map<std::string, std::vector<int>, decltype(cmp)> anagrams(cmp);
            for (int i : group)
                anagrams[strs[i]].push_back(i);
            for (const auto& [_2, ana] : anagrams)
            {
                std::vector<std::string> current;
                for (int i : ana)
                    current.push_back(strs[i]);
                result.emplace_back(std::move(current));
            }
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<std::string> > &left,
                const std::vector<std::vector<std::string> > &right)
{
    auto signature = [](const std::string &str) -> int
    {
        int result = 0;
        for (char c : str)
            result |= 1 << (c - 'a');
        return result;
    };
    if (left.size() != right.size()) return false;
    std::map<int, std::vector<std::unordered_set<std::string> > > groups;
    for (const auto &l : left)
        groups[signature(l[0])].push_back({l.begin(), l.end()});
    for (const auto &r : right)
    {
        auto it = groups.find(signature(r[0]));
        if (it == groups.end()) return false;
        bool found = false;
        for (const auto &lut : it->second)
        {
            if (r.size() == lut.size())
            {
                bool not_found = false;
                for (std::string s : r)
                {
                    if (lut.find(s) == lut.end())
                    {
                        not_found = true;
                        break;
                    }
                }
                if (!not_found)
                {
                    found = true;
                    break;
                }
            }
        }
        if (!found) return false;
    }
    return true;
}

void test(std::vector<std::string> strs,
          std::vector<std::vector<std::string> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<std::string> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = groupAnagrams(strs);
    showResult(solution == result, solution, result);
}

std::vector<std::string> load_test(const char * filename)
{
    std::ifstream file(filename);
    std::vector<std::string> result;
    std::string current;
    while (file >> current)
        result.emplace_back(std::move(current));
    file.close();
    return result;
}

std::vector<std::vector<std::string> > load_solution(const char * filename)
{
    std::ifstream file(filename);
    std::vector<std::vector<std::string> > result;
    std::string line;
    while (std::getline(file, line))
    {
        std::istringstream iss(line);
        std::vector<std::string> current;
        std::string word;
        while (iss >> word)
            current.emplace_back(std::move(word));
        if (current.size() > 0)
            result.emplace_back(std::move(current));
    }
    file.close();
    return result;
}

int main(int, char **)
{
// https://leetcode.com/problems/group-anagrams/submissions/
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"eat", "tea", "tan", "ate", "nat", "bat"}, {{"bat"}, {"nat", "tan"}, {"ate", "eat", "tea"}}, trials);
    test({"abc", "bca", "cba"}, {{"abc", "bca", "cba"}}, trials);
    test({""}, {{""}}, trials);
    test({"a"}, {{"a"}}, trials);
    test({"abba", "baab", "ab"}, {{"ab"}, {"abba", "baab"}}, trials);
    test({"ddddddddddg", "dgggggggggg"}, {{"dgggggggggg"}, {"ddddddddddg"}}, trials);
    test(load_test("test_a.txt"), load_solution("solution_a.txt"), trials);
    return 0;
}


