#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

long long kMirror(int k, int n)
{
    int base10[64] = {}, base10_len = 1, basek[64] = {}, basek_len = 0;
    long long base10_value = 0;
    auto next = [&](void) -> void
    {
        for (int i0 = basek_len / 2, i1 = basek_len - i0 - 1;
             i0 < basek_len; ++i0, --i1)
        {
            int next_number = basek[i0] + 1;
            if (next_number < k)
            {
                basek[i0] = basek[i1] = next_number;
                for (int j0 = basek_len / 2, j1 = basek_len - j0 - 1;
                     j0 < i0; ++j0, --j1)
                    basek[j0] = basek[j1] = 0;
                return;
            }
        }
        std::memset(basek, 0, sizeof(basek));
        basek[0] = 1;
        basek[++basek_len - 1] = 1;
    };
    auto toBase10 = [&](void) -> void
    {
        base10_value = 0;
        for (int i = 0; i < basek_len; ++i)
            base10_value = base10_value * k + basek[i];
        base10_len = 0;
        for (long long value = base10_value; value; value /= 10)
            base10[base10_len++] = static_cast<int>(value % 10);
    };
    auto compareReverse = [&](void) -> bool
    {
        for (int i = 0, j = base10_len - 1; i < j; ++i, --j)
            if (base10[i] != base10[j])
                return false;
        return true;
    };
    long long result = 0;
    for (int i = 0; i < n; ++i)
    {
        do
        {
            next();
            toBase10();
        }
        while ((basek_len < 60) && !compareReverse());
        result += base10_value;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int k, int n, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = kMirror(k, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 5, 25, trials);
    test(3, 7, 499, trials);
    test(7, 17, 20'379'000, trials);
    return 0;
}


