from typing import List

class Solution:
    def kMirror(self, k: int, n: int) -> int:
        ans = 0
        A = ['0']
        def nextKMirror(A: List[chr]) -> List[chr]:
            for i in range(len(A) // 2, len(A)):
                nextNum = int(A[i]) + 1
                if nextNum < k:
                    A[i] = str(nextNum)
                    A[~i] = str(nextNum)
                    for j in range(len(A) // 2, i):
                        A[j] = '0'
                        A[~j] = '0'
                    return A
            return ['1'] + ['0'] * (len(A) - 1) + ['1']
        for _ in range(n):
            while True:
                A = nextKMirror(A)
                num = int(''.join(A), k)
                if str(num)[::-1] == str(num):
                    break
            ans += num
        return ans

if __name__ == '__main__':
    sol = Solution()
    assert sol.kMirror(2, 5) == 25
    assert sol.kMirror(3, 7) == 499
    assert sol.kMirror(7, 17) == 20379000

nextKMirror(['0'])
def nextKMirror(A):
    for i in range(len(A) // 2, len(A)):
        nextNum = int(A[i]) + 1
        if nextNum < k:
            A[i] = str(nextNum)
            A[~i] = str(nextNum)
            print((i, ~i, list(range(len(A) // 2, i))))
            for j in range(len(A) // 2, i):
                print((j, ~j))
                A[j] = '0'
                A[~j] = '0'
            return A
    return ['1'] + ['0'] * (len(A) - 1) + ['1']
