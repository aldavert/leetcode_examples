#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int getKth(int lo, int hi, int k)
{
    std::vector<std::pair<int, int> > pow_and_vals; 
    auto getPow = [](int n) -> int
    {
        int result = 0;
        for (; n != 1; ++result)
            n = (n % 2 == 0)?n / 2:n * 3 + 1;
        return result;
    };
    
    for (int i = lo; i <= hi; ++i)
        pow_and_vals.emplace_back(getPow(i), i);
    std::nth_element(pow_and_vals.begin(),
                     pow_and_vals.begin() + k - 1,
                     pow_and_vals.end());
    return pow_and_vals[k - 1].second;
}

// ############################################################################
// ############################################################################

void test(int lo, int high, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getKth(lo, high, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(12, 15, 2, 13, trials);
    test( 7, 11, 4,  7, trials);
    return 0;
}


