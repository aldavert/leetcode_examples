#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long coloredCells(int n)
{
    return static_cast<long>(n) * n + static_cast<long>(n - 1) * (n - 1);
}

// ############################################################################
// ############################################################################

void test(int n, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = coloredCells(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 1, trials);
    test(2, 5, trials);
    return 0;
}


