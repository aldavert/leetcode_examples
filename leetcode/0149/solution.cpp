#include "../common/common.hpp"
#include <numeric>
#include <unordered_map>
#include <cmath>

// ############################################################################
// ############################################################################

// Line equation: y = m * x + b, where m = dy / dx;
// Then b at point p1 is:
//  y1 = dy / dx * x1 + b -> b = y1 - dy / dx * x1;
// Combining both equations we have:
// y - y1 + dy / dx * x1 = dy / dx * x
// dx * y - dx * y1 + dy * x1 = dy * x
// -dy * x + dx * y - dx * y1 + dy * x1 = 0
// So the parameters of the line become:
// A = -dy
// B = dx
// C = -dx * y1 + dy * x1
int maxPoints(std::vector<std::vector<int> > points)
{
    const int n = static_cast<int>(points.size());
    std::unordered_map<long, int> histogram;
    for (int i = 0; i < n; ++i)
    {
        int x1 = points[i][0], y1 = points[i][1];
        for (int j = i + 1; j < n; ++j)
        {
            int x2 = points[j][0], y2 = points[j][1];
            int dx = x1 - x2;
            int dy = y1 - y2;
            if (((dy == 0) && (dx < 0))
             || ((dx == 0) && (dy < 0))
             || ((dx <  0) && (dy > 0))
             || ((dx <  0) && (dy < 0)))
            {
                dx = -dx;
                dy = -dy;
            }
            int g = std::gcd(dx, dy);
            dx /= g;
            dy /= g;
            int A = -dy;
            int B = dx;
            int C = -dx * y1 + dy * x1;
            long id = std::abs(A) | (A < 0) << 15;
            id |= static_cast<long>(std::abs(B) | (B < 0) << 15) << 16;
            id |= static_cast<long>(std::abs(C) | (C < 0) << 30) << 31;
            ++histogram[id];
        }
    }
    int max_frequency = 0;
    for (const auto &h : histogram)
        max_frequency = std::max(max_frequency, h.second);
    return static_cast<int>(std::sqrt(1 + 8 * max_frequency) - 1) / 2 + 1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxPoints(points);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1}, {2, 2}, {3, 3}}, 3, trials);
    test({{1, 1}, {3, 2}, {5, 3}, {4, 1}, {2, 3}, {1, 4}}, 4, trials);
    test({{2, 3}, {3, 3}, {-5, 3}}, 3, trials);
    test({{4, 5}, {4, -1}, {4, 0}}, 3, trials);
    return 0;
}


