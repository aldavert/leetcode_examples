#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfRounds(std::string loginTime, std::string logoutTime)
{
    auto calculateMinutes = [](const std::string &time) -> int
    {
        return 60 * std::stoi(time.substr(0, 2)) + std::stoi(time.substr(3));
    };
    int start = calculateMinutes(loginTime), finish = calculateMinutes(logoutTime);
    if (start > finish)
        finish += 60 * 24;
    return std::max(0, finish / 15 - (start + 14) / 15);
}

// ############################################################################
// ############################################################################

void test(std::string loginTime,
          std::string logoutTime,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfRounds(loginTime, logoutTime);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("09:31", "10:14", 1, trials);
    test("21:30", "03:00", 22, trials);
    return 0;
}


