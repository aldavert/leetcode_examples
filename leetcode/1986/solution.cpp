#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minSessions(std::vector<int> tasks, int sessionTime)
{
    auto dfs = [&](auto &&self, size_t s, std::vector<int> &sessions) -> bool
    {
        if (s == tasks.size())
            return true;
        for (int &session : sessions)
        {
            if (session + tasks[s] > sessionTime)
                continue;
            session += tasks[s];
            if (self(self, s + 1, sessions))
                return true;
            session -= tasks[s];
            if (session == 0)
                return false;
        }
        return false;
    };
    for (size_t num_sessions = 1; num_sessions <= tasks.size(); ++num_sessions)
    {
        std::vector<int> sessions(num_sessions);
        if (dfs(dfs, 0, sessions))
            return static_cast<int>(num_sessions);
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tasks, int sessionTime, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSessions(tasks, sessionTime);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 3, 2, trials);
    test({3, 1, 3, 1, 1}, 8, 2, trials);
    test({1, 2, 3, 4, 5}, 15, 1, trials);
    return 0;
}


