#include "../common/common.hpp"

// ############################################################################
// ############################################################################


int maxSideLength(std::vector<std::vector<int> > mat, int threshold)
{
    const int m = static_cast<int>(mat.size());
    const int n = static_cast<int>(mat[0].size());
    int result = 0;
    std::vector<std::vector<int> > prefix(m + 1, std::vector<int>(n + 1));
    auto squareSum = [&](int r1, int c1, int r2, int c2) -> int
    {
        return prefix[r2 + 1][c2 + 1] + prefix[r1    ][c1    ]
             - prefix[r1    ][c2 + 1] - prefix[r2 + 1][c1    ];
    };
    
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            prefix[i + 1][j + 1] = mat[i][j] + prefix[i][j + 1]
                                 + prefix[i + 1][j] - prefix[i][j];
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            for (int length = result; length < std::min(m - i, n - j); ++length)
            {
                if (squareSum(i, j, i + length, j + length) > threshold)
                    break;
                result = std::max(result, length + 1);
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > mat,
          int threshold,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSideLength(mat, threshold);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 3, 2, 4, 3, 2},
          {1, 1, 3, 2, 4, 3, 2},
          {1, 1, 3, 2, 4, 3, 2}}, 4, 2, trials);
    test({{2, 2, 2, 2, 2},
          {2, 2, 2, 2, 2},
          {2, 2, 2, 2, 2},
          {2, 2, 2, 2, 2},
          {2, 2, 2, 2, 2}}, 1, 0, trials);
    return 0;
}


