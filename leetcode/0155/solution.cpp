#include "../common/common.hpp"

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class MinStack
{
public:
    MinStack(void) :
        m_minimum(std::numeric_limits<int>::max())
    {
        m_data.reserve(30'000);
    }
    void push(int val)
    {
        m_minimum = std::min(m_minimum, val);
        m_data.push_back({val, m_minimum});
    }
    void pop()
    {
        m_data.pop_back();
        m_minimum = (m_data.size() > 0)?m_data.back().second:std::numeric_limits<int>::max();
    }
    int top()
    {
        return m_data.back().first;
    }
    int getMin()
    {
        return m_data.back().second;
    }
protected:
    std::vector<std::pair<int, int> > m_data;
    int m_minimum;
};

// ############################################################################
// ############################################################################

enum class OP { MINSTACK, PUSH, POP, TOP, GETMIN };

void test(std::vector<OP> operations,
          std::vector<int> input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        const int n = static_cast<int>(operations.size());
        std::vector<int> current_result(n);
        MinStack * stack = nullptr;
        for (int k = 0; k < n; ++k)
        {
            switch (operations[k])
            {
            case OP::MINSTACK:
                stack = new MinStack();
                current_result[k] = null;
                break;
            case OP::PUSH:
                stack->push(input[k]);
                current_result[k] = null;
                break;
            case OP::POP:
                stack->pop();
                current_result[k] = null;
                break;
            case OP::TOP:
                current_result[k] = stack->top();
                break;
            case OP::GETMIN:
                current_result[k] = stack->getMin();
                break;
            default:
                std::cerr << "[ERROR] Unknown operation.\n";
                return;
            }
        }
        delete stack;
        result = current_result;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::MINSTACK, OP::PUSH, OP::PUSH, OP::PUSH, OP::GETMIN, OP::POP,
          OP::TOP, OP::GETMIN},
         {null, -2, 0, -3, null, null, null, null},
         {null, null, null, null, -3, null, 0, -2}, trials);
    test({OP::MINSTACK, OP::PUSH, OP::PUSH, OP::PUSH, OP::TOP, OP::POP,
          OP::GETMIN, OP::POP, OP::GETMIN, OP::POP, OP::PUSH, OP::TOP,
          OP::GETMIN, OP::PUSH, OP::TOP, OP::GETMIN, OP::POP, OP::GETMIN},
         {null, 2147483646, 2147483646, 2147483647, null, null, null, null, null,
          null, 2147483647, null, null, -2147483648, null, null, null, null},
         {null, null, null, null, 2147483647, null, 2147483646, null, 2147483646,
          null, null, 2147483647, 2147483647, null, -2147483648, -2147483648,
          null, 2147483647}, trials);
    return 0;
}


