#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

double getMinDistSum(std::vector<std::vector<int> > positions)
{
    auto accumulatedSum = [&](double a, double b) -> double
    {
        double sum = 0;
        for (const auto &p : positions)
        {
            double dx = a - p[0], dy = b - p[1];
            sum += std::sqrt(dx * dx + dy * dy);
        }
        return sum;
    };
    constexpr double ERR = 1e-6;
    double current_x = 50, current_y = 50, step = 1;
    double result = accumulatedSum(current_x, current_y);
    
    while (step > ERR)
    {
        double dx[] = {    0,     0,  step, -step};
        double dy[] = { step, -step,     0,     0};
        bool should_decrease_step = true;
        for (int i = 0; i < 4; ++i)
        {
            double x = current_x + dx[i], y = current_y + dy[i],
                   new_distance_sum = accumulatedSum(x, y);
            if (new_distance_sum < result)
            {
                result = new_distance_sum;
                current_x = x;
                current_y = y;
                should_decrease_step = false;
            }
        }
        if (should_decrease_step) step /= 10;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > positions,
          double solution,
          unsigned int trials = 1)
{
    double result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getMinDistSum(positions);
    showResult(std::abs(solution - result) < 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {1, 0}, {1, 2}, {2, 1}}, 4.0, trials);
    test({{1, 1}, {3, 3}}, 2.82843, trials);
    return 0;
}



