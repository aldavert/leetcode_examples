#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool stoneGame([[maybe_unused]] std::vector<int> piles)
{
    return true; // Alice always wins.
}
#else
bool stoneGame(std::vector<int> piles)
{
    const int n = static_cast<int>(piles.size());
    int dp_alice[501][501] = {};
    int dp_lee[501][501] = {};
    for (int left = n - 1; left >= 0; --left)
    {
        dp_alice[left][left] = piles[left];
        dp_lee[left][left] = 0;
        for (int right = left + 1; right < n; ++right)
        {
            if (piles[left] + dp_lee[left + 1][right]
              > piles[right] + dp_lee[left][right - 1])
            {
                dp_alice[left][right] = piles[left] + dp_lee[left + 1][right];
                dp_lee[left][right] = dp_alice[left + 1][right];
            }
            else
            {
                dp_alice[left][right] = piles[right] + dp_lee[left][right - 1];
                dp_lee[left][right] = dp_alice[left][right - 1];
            }
        }
    }
    return dp_alice[0][n - 1] > dp_lee[0][n - 1];
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> piles, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = stoneGame(piles);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 3, 4, 5}, true, trials);
    return 0;
}


