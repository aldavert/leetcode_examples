#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestSubsequence(std::string s, int k)
{
    int one_count = 0;
    for (int i = static_cast<int>(s.size()) - 1, num = 0, pow = 1;
         (i >= 0) && (num + pow <= k); --i, pow *= 2)
        if (s[i] == '1')
            ++one_count,
            num += pow;
    return static_cast<int>(std::count(s.begin(), s.end(), '0')) + one_count;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestSubsequence(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1001010", 5, 5, trials);
    test("00101001", 1, 6, trials);
    return 0;
}


