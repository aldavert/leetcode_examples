#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumOperations(std::string num)
{
    const int n = static_cast<int>(num.size());
    bool seen_five = false, seen_zero = false;
    for (int i = n - 1; i >= 0; --i)
    {
        if ((seen_zero && ((num[i] == '0') || (num[i] == '5')))
        ||  (seen_five && ((num[i] == '2') || (num[i] == '7'))))
            return n - i - 2;
        seen_zero = seen_zero || (num[i] == '0');
        seen_five = seen_five || (num[i] == '5');
    }
    return n - seen_zero;
}

// ############################################################################
// ############################################################################

void test(std::string num, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumOperations(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("2245047", 2, trials);
    test("2908305", 3, trials);
    test("10", 1, trials);
    return 0;
}


