#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countTriplets(std::vector<int> nums)
{
    int result = 0, max_value = 0;
    int count[1 << 16] = {};
    for (int a : nums)
    {
        for (int b : nums)
            ++count[a & b];
        max_value = std::max(max_value, a);
    }
    for (int num : nums)
        for (int i = 0; i <= max_value; ++i)
            if ((num & i) == 0)
                result += count[i];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countTriplets(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3}, 12, trials);
    test({0, 0, 0}, 27, trials);
    return 0;
}


