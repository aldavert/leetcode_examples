#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::string makeFancyString(std::string s)
{
    size_t count = 1;
    for (size_t i = 1, repeated = 1; i < s.size(); ++i)
    {
        if (s[i] == s[i - 1]) ++repeated;
        else repeated = 1;
        if (repeated <= 2)
            s[count++] = s[i];
    }
    s.resize(count);
    return s;
}
#elif 0
std::string makeFancyString(std::string s)
{
    const int n = static_cast<int>(s.size());
    std::string result;
    for (int count = 0, i = 0; i < n; ++i)
    {
        count = count * (!result.empty()) * (result.back() == s[i]) + 1;
        if (count <= 2)
            result += s[i];
    }
    return result;
}
#elif 0
std::string makeFancyString(std::string s)
{
    std::string result;
    for (int count = 0; char letter : s)
    {
        count = count * (!result.empty()) * (result.back() == letter) + 1;
        if (count <= 2)
            result += letter;
    }
    return result;
}
#else
std::string makeFancyString(std::string s)
{
    std::string result, buffer;
    for (char letter : s)
    {
        if (buffer.empty() || (letter == buffer.back()))
        {
            if (buffer.size() < 2) buffer += letter;
        }
        else result += std::exchange(buffer, letter);
    }
    return result + buffer;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = makeFancyString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leeetcode", "leetcode", trials);
    test("aaabaaaa", "aabaa", trials);
    test("aab", "aab", trials);
    return 0;
}


