#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int minimumSum(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = std::numeric_limits<int>::max();
    std::vector<int> min_prefix(n), min_suffix(n);
    std::partial_sum(nums.begin(), nums.end(), min_prefix.begin(),
                                    [](int x, int y) { return std::min(x, y); });
    std::partial_sum(nums.rbegin(), nums.rend(), min_suffix.begin(),
                                    [](int x, int y) { return std::min(x, y); });
    std::reverse(min_suffix.begin(), min_suffix.end());
    for (int i = 0; i < n; ++i)
        if ((nums[i] > min_prefix[i]) && (nums[i] > min_suffix[i]))
            result = std::min(result, nums[i] + min_prefix[i] + min_suffix[i]);
    return (result == std::numeric_limits<int>::max())?-1:result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 6, 1, 5, 3}, 9, trials);
    test({5, 4, 8, 7, 10, 2}, 13, trials);
    test({6, 5, 4, 3, 4, 5}, -1, trials);
    return 0;
}


