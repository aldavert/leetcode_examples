#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string defangIPaddr(std::string address)
{
    std::string result;
    for (char c : address)
        result += (c == '.')?"[.]":std::string(1, c);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string address, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = defangIPaddr(address);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1.1.1.1", "1[.]1[.]1[.]1", trials);
    test("255.100.50.0", "255[.]100[.]50[.]0", trials);
    return 0;
}


