#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minimumCost(std::vector<int> start,
                std::vector<int> target,
                std::vector<std::vector<int> > specialRoads)
{
    auto dist = [](int x1, int y1, int x2, int y2)
    {
        return std::abs(x1 - x2) + std::abs(y1 - y2);
    };
    struct Node
    {
        int distance = 0;
        int x = 0;
        int y = 0;
        bool operator<(const Node &other) const
        {
            return (distance > other.distance)
                || ((distance == other.distance) && (x > other.x))
                || ((distance == other.distance) && (x == other.x) && (y > other.y));
        }
    };
    int result = 1 << 30;
    std::priority_queue<Node> pq;
    pq.push({0, start[0], start[1]});
    std::unordered_set<long long> visited;
    while (!pq.empty())
    {
        auto [d, x, y] = pq.top();
        pq.pop();
        long long signature = 1LL * x * 1'000'000 + y;
        if (visited.count(signature)) continue;
        visited.insert(signature);
        result = std::min(result, d + dist(x, y, target[0], target[1]));
        for (auto &r : specialRoads)
        {
            int x1 = r[0], y1 = r[1], x2 = r[2], y2 = r[3], cost = r[4];
            pq.push({d + dist(x, y, x1, y1) + cost, x2, y2});
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> start,
          std::vector<int> target,
          std::vector<std::vector<int> > specialRoads,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumCost(start, target, specialRoads);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1}, {4, 5}, {{1, 2, 3, 3, 2}, {3, 4, 4, 5, 1}}, 5, trials);
    test({3, 2}, {5, 7}, {{5, 7, 3, 2, 1}, {3, 2, 3, 4, 4}, {3, 3, 5, 5, 5},
         {3, 4, 5, 6, 6}}, 7, trials);
    test({1, 1}, {10, 4}, {{4, 2, 1, 1, 3}, {1, 2, 7, 4, 4}, {10, 3, 6, 1, 2},
         {6, 1, 1, 2, 3}}, 8, trials);
    return 0;
}


