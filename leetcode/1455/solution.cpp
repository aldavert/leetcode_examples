#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int isPrefixOfWord(std::string sentence, std::string searchWord)
{
    for (size_t number_of_words = 0, index = 0; char c : sentence)
    {
        if (c == ' ') index = 0;
        else
        {
            number_of_words += (index == 0);
            if ((index < searchWord.size()) && (c == searchWord[index]))
            {
                ++index;
                if (index == searchWord.size())
                    return static_cast<int>(number_of_words);
            }
            else index = searchWord.size();
        }
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::string sentence,
          std::string searchWord,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPrefixOfWord(sentence, searchWord);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("i love eating burger", "burg", 4, trials);
    test("this problem is an easy problem", "pro", 2, trials);
    test("i am tired", "you", -1, trials);
    return 0;
}


