#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> countOfPairs(int n, int x, int y)
{
    --x, --y;
    std::vector<int> result(n);
    for (int i = 0; i < n; ++i)
    {
        for (int j = i + 1; j < n; ++j)
        {
            int a = j - i;
            int b = std::abs(x - i) + std::abs(y - j) + 1;
            int c = std::abs(y - i) + std::abs(x - j) + 1;
            result[std::min({a, b, c}) - 1] += 2;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int x, int y, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countOfPairs(n, x, y);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 1, 3, {6, 0, 0}, trials);
    test(5, 2, 4, {10, 8, 2, 0, 0}, trials);
    test(4, 1, 1, {6, 4, 2, 0}, trials);
    return 0;
}


