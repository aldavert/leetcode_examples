#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int maxRemovals(std::string source, std::string pattern, std::vector<int> targetIndices)
{
    const int n = static_cast<int>(source.size()),
              m = static_cast<int>(pattern.size());
    std::sort(targetIndices.rbegin(), targetIndices.rend());
    std::vector<int> memo(m + 1, std::numeric_limits<int>::lowest() + 10);
    memo[0] = 0;
    for (int i = 0; i < n; ++i)
    {
        if (targetIndices.size() and targetIndices.back() == i)
        {
            ++memo[m];
            for (int j = m - 1; j >= 0; --j)
            {
                if (pattern[j] == source[i])
                    memo[j + 1] = std::max(memo[j + 1], memo[j]);
                ++memo[j];
            }
            targetIndices.pop_back();
        }
        else
        {
            for (int j = m - 1; j >= 0; --j)
                if (pattern[j] == source[i])
                    memo[j + 1] = std::max(memo[j + 1], memo[j]);
        }
    }
    return memo[m];
}
#else
int maxRemovals(std::string source, std::string pattern, std::vector<int> targetIndices)
{
    const int n = static_cast<int>(source.size()),
              m = static_cast<int>(pattern.size());
    std::vector<std::vector<int> > memory(n + 1, std::vector<int>(m + 1, -1));
    std::vector<bool> targets(n, false);
    for (int target : targetIndices)
        targets[target] = true;
    auto process = [&](auto &&self, int source_idx, int pattern_idx) -> int
    {
        if (memory[source_idx][pattern_idx] != -1)
            return memory[source_idx][pattern_idx];
        if (pattern_idx >= m)
        {
            int result = 0;
            for (int i = source_idx; i < n; ++i)
                result += targets[i];
            return memory[source_idx][pattern_idx] = result;
        }
        if (source_idx >= n)
            return memory[source_idx][pattern_idx] = std::numeric_limits<int>::lowest();
        if (targets[source_idx])
        {
            if (source[source_idx] == pattern[pattern_idx])
                return memory[source_idx][pattern_idx] =
                    std::max(self(self, source_idx + 1, pattern_idx + 1),
                             1 + self(self, source_idx + 1, pattern_idx));
            else return memory[source_idx][pattern_idx] =
                1 + self(self, source_idx + 1, pattern_idx);
        }
        else
        {
            if (source[source_idx] == pattern[pattern_idx])
                return memory[source_idx][pattern_idx] =
                    self(self, source_idx + 1, pattern_idx + 1);
            else return memory[source_idx][pattern_idx] =
                self(self, source_idx + 1, pattern_idx);
        }
    };
    return std::max(-1, process(process, 0, 0));
}
#endif

// ############################################################################
// ############################################################################

void test(std::string source,
          std::string pattern,
          std::vector<int> targetIndices,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxRemovals(source, pattern, targetIndices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abbaa", "aba", {0, 1, 2}, 1, trials);
    test("bcda", "d", {0, 3}, 2, trials);
    test("dda", "dda", {0, 1, 2}, 0, trials);
    test("yeyeykyded", "yeyyd", {0, 2, 3, 4}, 2, trials);
    return 0;
}


