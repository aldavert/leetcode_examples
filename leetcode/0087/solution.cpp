#include "../common/common.hpp"
#include <cstring>
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
bool isScramble(std::string s1, std::string s2)
{
    const int n = static_cast<int>(s1.size());
    if (n == 1) return s1 == s2;
    if (n == 2) return (s1 == s2) || ((s1[0] == s2[1]) && (s1[1] == s2[0]));
    char lut[30][30][30][30];
    std::memset(lut, -1, sizeof(lut));
    
    auto search = [&](auto &&self, int s1_l, int s1_r, int s2_l, int s2_r) -> bool
    {
        char &clut = lut[s1_l][s1_r][s2_l][s2_r];
        if (s1_l == s1_r) return (clut = (s1[s2_l] == s2[s1_l]));
        if (clut != -1)
            return clut;
        for (int i = s1_l, j = s2_l; i < s1_r; ++i, ++j)
        {
            if (self(self, s1_l, i, s2_l, j)
            &&  self(self, i + 1, s1_r, j + 1, s2_r))
                return (clut = true);
            if (self(self, s1_l, i, s2_r - j + s2_l, s2_r)
            &&  self(self, i + 1, s1_r, s2_l, s2_l + s2_r - j - 1))
                return (clut = true);
        }
        return (clut = false);
    };
    return search(search, 0, n - 1, 0, n - 1);
}
#else
bool isScramble(std::string s1, std::string s2)
{
    const int n = static_cast<int>(s1.size());
    if (n == 1) return s1 == s2;
    if (n == 2) return (s1 == s2) || ((s1[0] == s2[1]) && (s1[1] == s2[0]));
    std::unordered_map<int, bool> lut;
    
    auto solve = [&](auto &&self, int s1_l, int s1_r, int s2_l, int s2_r) -> bool
    {
        if (s1_l == s1_r) return s1[s2_l] == s2[s1_l];
        const int call_id = s1_l << 24 | s1_r << 16 | s2_l << 8 | s2_r;
        if (auto search = lut.find(call_id); search != lut.end())
            return search->second;
        for (int i = s1_l, j = s2_l; i < s1_r; ++i, ++j)
        {
            if (self(self, s1_l, i, s2_l, j)
             && self(self, i + 1, s1_r, j + 1, s2_r))
                return (lut[call_id] = true);
            if (self(self, s1_l, i, s2_r - j + s2_l, s2_r)
             && self(self, i + 1, s1_r, s2_l, s2_l + s2_r - j - 1))
                return (lut[call_id] = true);
        }
        return (lut[call_id] = false);
    };
    return solve(solve, 0, n - 1, 0, n - 1);
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s1, std::string s2, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isScramble(s1, s2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("great", "rgeat", true, trials);
    test("abcde", "caebd", false, trials);
    test("a", "a", true, trials);
    test("a", "b", false, trials);
    test("aa", "ab", false, trials);
    return 0;
}


