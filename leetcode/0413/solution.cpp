#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfArithmeticSlices(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    if (n < 3) return 0;
    int result = 0;
    for (int i = 2, dp = 0; i < n; ++i)
    {
        if ((nums[i] - nums[i - 1]) == (nums[i - 1] - nums[i - 2]))
            result += ++dp;
        else dp = 0;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfArithmeticSlices(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 3, trials);
    test({1}, 0, trials);
    test({1, 2, 3, 8, 9, 10}, 2, trials);
    return 0;
}


