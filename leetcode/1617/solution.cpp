#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int>
countSubgraphsForEachDiameter(int n, std::vector<std::vector<int> > edges)
{
    int dp[16][16][16] = {};
    std::vector<int> g[16];
    for (const auto &e: edges)
        g[e[0] - 1].push_back(e[1] - 1),
        g[e[1] - 1].push_back(e[0] - 1);
    auto dfs = [&](auto &&self, int v, int parent) -> void
    {
        dp[v][0][0] = 1;
        for (int w: g[v])
        {
            if(w != parent)
            {
                int t[16][16] = {}, index;
                self(self, w, v);
                for (int wdepth = 0; wdepth + 1 < n; ++wdepth)
                {
                    for (int wd = wdepth; wd < n; ++wd)
                    {
                        if (dp[w][wdepth][wd])
                        {
                            for (int vdepth = 0; vdepth + wdepth + 1 < n; ++vdepth)
                            {
                                index = std::max(vdepth, wdepth + 1);
                                int aux = vdepth + wdepth + 1;
                                for (int vd = vdepth; vd < n; ++vd)
                                    if (dp[v][vdepth][vd])
                                        t[index][std::max(std::max(vd, wd), aux)] +=
                                            dp[v][vdepth][vd] * dp[w][wdepth][wd];
                            }
                        }
                    }
                }
                for(int depth = 0; depth < n; ++depth)
                    for(int d = 0; d < n; ++d)
                        dp[v][depth][d] += t[depth][d];
            }
        }
    };
    
    std::vector<int> result(n - 1);
    dfs(dfs, 0, -1);
    for(int i = 0; i < n; ++i)
        for(int depth = 0; depth < n; ++depth)
            for(int d = 1; d < n; ++d)
                result[d - 1] += dp[i][depth][d];
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSubgraphsForEachDiameter(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{1, 2}, {2, 3}, {2, 4}}, {3, 4, 0}, trials);
    test(2, {{1, 2}}, {1}, trials);
    test(3, {{1, 2}, {2, 3}}, {2, 1}, trials);
    return 0;
}


