#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> relativeSortArray(std::vector<int> arr1, std::vector<int> arr2)
{
    int histogram[1001] = {};
    std::vector<int> result;
    result.reserve(arr1.size());
    for (int i : arr1)
        ++histogram[i];
    for (int i : arr2)
        for (; histogram[i]; --histogram[i])
            result.push_back(i);
    for (int i = 0; i <= 1000; ++i)
        for (; histogram[i]; --histogram[i])
            result.push_back(i);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr1,
          std::vector<int> arr2,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = relativeSortArray(arr1, arr2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 1, 3, 2, 4, 6, 7, 9, 2, 19},
         {2, 1, 4, 3, 9, 6},
         {2, 2, 2, 1, 4, 3, 3, 9, 6, 7, 19},
         trials);
    test({28, 6, 22, 8, 44, 17},
         {22, 28, 8, 6},
         {22, 28, 8, 6, 17, 44},
         trials);
    return 0;
}


