#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string longestPrefix(std::string s)
{
    int n = static_cast<int>(s.size());
    std::vector<int> lps(n, 0);
    for (int i = 1, len = 0; i < n; )
        if (s[i] == s[len]) lps[i++] = ++len;
        else if (len > 0) len = lps[len - 1];
        else ++i;
    return s.substr(0, lps[lps.size() - 1]);
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestPrefix(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("level", "l", trials);
    test("ababab", "abab", trials);
    test("vwantmbocxcwrqtvgzuvgrmdltfiglltaxkjfajxthcppcatddcunpkqsgpnjjgqanrwa"
         "bgrtwuqbrfl", "", trials);
    return 0;
}


