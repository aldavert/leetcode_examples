#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximizeWin(std::vector<int> prizePositions, int k)
{
    int result = 0;
    std::vector<int> dp(prizePositions.size() + 1);
    for (int i = 0, j = 0, n = static_cast<int>(prizePositions.size()); i < n; ++i)
    {
        while (prizePositions[i] - prizePositions[j] > k)
        ++j;
        const int covered = i - j + 1;
        dp[i + 1] = std::max(dp[i], covered);
        result = std::max(result, dp[j] + covered);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> prizePositions, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximizeWin(prizePositions, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2, 2, 3, 3, 5}, 2, 7, trials);
    test({1, 2, 3, 4}, 0, 2, trials);
    return 0;
}


