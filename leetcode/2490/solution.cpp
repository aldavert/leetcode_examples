#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isCircularSentence(std::string sentence)
{
    const int n = static_cast<int>(sentence.size());
    if (sentence[0] != sentence[n - 1]) return false;
    for (int i = 0; i < n; ++i)
        if ((sentence[i] == ' ') && (sentence[i - 1] != sentence[i + 1]))
            return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string sentence, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isCircularSentence(sentence);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leetcode exercises sound delightful", true, trials);
    test("eetcode", true, trials);
    test("Leetcode is cool", false, trials);
    return 0;
}


