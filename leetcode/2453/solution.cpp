#include "../common/common.hpp"
#include <limits>
#include <unordered_map>

// ############################################################################
// ############################################################################

int destroyTargets(std::vector<int> nums, int space)
{
    int result = std::numeric_limits<int>::max(), max_count = 0;
    std::unordered_map<int, int> count;
    
    for (const int num : nums)
        max_count = std::max(max_count, ++count[num % space]);
    for (const int num : nums)
        if (count[num % space] == max_count)
            result = std::min(result, num);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int space, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = destroyTargets(nums, space);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 7, 8, 1, 1, 5}, 2, 1, trials);
    test({1, 3, 5, 2, 4, 6}, 2, 1, trials);
    test({6, 2, 5}, 100, 2, trials);
    return 0;
}


