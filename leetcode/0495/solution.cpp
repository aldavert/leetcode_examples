#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findPoisonedDuration(std::vector<int> timeSeries, int duration)
{
    int previous = -duration;
    int time = 0;
    for (int start : timeSeries)
    {
        time += duration - (duration - std::min(duration, start - previous));
        previous = start;
    }
    return time;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> timeSeries,
          int duration,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findPoisonedDuration(timeSeries, duration);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4}, 2, 4, trials);
    test({1, 2}, 2, 3, trials);
    return 0;
}


