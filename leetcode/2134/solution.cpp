#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minSwaps(std::vector<int> nums)
{
    size_t number_of_ones = 0;
    for (size_t i = 0; i < nums.size(); ++i)
        number_of_ones += nums[i];
    
    int ones = 0, max_ones = 0;
    for (size_t i = 0; i < number_of_ones; ++i)
        ones += nums[i];
    for (size_t i = number_of_ones; i < nums.size(); ++i)
        max_ones = std::max(max_ones, ones += nums[i] - nums[i - number_of_ones]);
    for (size_t i = 0; i < number_of_ones; ++i)
        max_ones = std::max(max_ones,
                            ones += nums[i] - nums[nums.size() - number_of_ones + i]);
    return static_cast<int>(number_of_ones) - max_ones;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSwaps(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 0, 1, 1, 0, 0}, 1, trials);
    test({0, 1, 1, 1, 0, 0, 1, 1, 0}, 2, trials);
    test({1, 1, 0, 0, 1}, 0, trials);
    return 0;
}


