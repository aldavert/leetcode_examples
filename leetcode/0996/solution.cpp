#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int numSquarefulPerms(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<bool> used(n);
    std::vector<int> path;
    int result = 0;
    auto isSquare = [](int num) -> bool
    {
        const int root = static_cast<int>(std::sqrt(num));
        return root * root == num;
    };
    auto dpProcess = [&](auto &&self) -> void
    {
        if ((path.size() > 1) && !isSquare(path.back() + path[path.size() - 2]))
            return;
        if (path.size() == nums.size())
        {
            ++result;
            return;
        }
        for (int i = 0; i < n; ++i)
        {
            if (used[i] || ((i > 0) && (nums[i] == nums[i - 1]) && !used[i - 1]))
                continue;
            used[i] = true;
            path.push_back(nums[i]);
            self(self);
            path.pop_back();
            used[i] = false;
        }
    };
    std::sort(nums.begin(), nums.end());
    dpProcess(dpProcess);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numSquarefulPerms(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 17, 8}, 2, trials);
    test({2, 2, 2}, 1, trials);
    return 0;
}


