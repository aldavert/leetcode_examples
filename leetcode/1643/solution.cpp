#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string kthSmallestPath(std::vector<int> destination, int k)
{
    int h = destination[1], v = destination[0], n = h + v;
    std::vector<std::vector<int> > combinations(n + 1, std::vector<int>(n + 1));
    for (int i = 0; i <= n; ++i)
        combinations[i][0] = 1;
    combinations[1][1] = 1;
    for (int i = 2; i <= n; ++i)
        for (int j = 1; j <= i; ++j)
            combinations[i][j] = combinations[i - 1][j] + combinations[i - 1][j - 1];
    std::string result;
    for (int i = 0; i < n; ++i)
    {
        if (h)
        {
            int c = combinations[h + v - 1][h - 1];
            if (k > c) result += 'V', --v, k -= c;
            else result += 'H', --h;
        }
        else result += 'V';
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> destination,
          int k,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = kthSmallestPath(destination, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3}, 1, "HHHVV", trials);
    test({2, 3}, 2, "HHVHV", trials);
    test({2, 3}, 3, "HHVVH", trials);
    return 0;
}


