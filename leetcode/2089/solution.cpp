#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> targetIndices(std::vector<int> nums, int target)
{
    std::vector<int> result;
    size_t numbers_equal_to_target = 0, numbers_lower_than_target = 0;
    for (int value : nums)
    {
        numbers_lower_than_target += value < target;
        numbers_equal_to_target += value == target;
    }
    while (numbers_equal_to_target)
    {
        result.push_back(static_cast<int>(numbers_lower_than_target++));
        --numbers_equal_to_target;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int target,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = targetIndices(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 5, 2, 3}, 2, {1, 2}, trials);
    test({1, 2, 5, 2, 3}, 3, {3}, trials);
    test({1, 2, 5, 2, 3}, 5, {4}, trials);
    return 0;
}


