#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findLengthOfShortestSubarray(std::vector<int> arr)
{
    const int n = static_cast<int>(arr.size());
    int l = 0, r = n - 1;
    
    while ((l < n - 1) && (arr[l + 1] >= arr[l])) ++l;
    while ((r > 0)     && (arr[r - 1] <= arr[r])) --r;
    int result = std::min(n - 1 - l, r);
    for (int i = l, j = n - 1; (i >= 0) && (j >= r) && (j > i); )
    {
        if (arr[i] <= arr[j]) --j;
        else --i;
        result = std::min(result, j - i);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLengthOfShortestSubarray(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 10, 4, 2, 3, 5}, 3, trials);
    test({5, 4, 3, 2, 1}, 4, trials);
    test({1, 2, 3}, 0, trials);
    return 0;
}


