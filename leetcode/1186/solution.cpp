#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int maximumSum(std::vector<int> arr)
{
    int result = std::numeric_limits<int>::lowest() / 2,
        zero = std::numeric_limits<int>::lowest() / 2,
        one = std::numeric_limits<int>::lowest() / 2;
    for (int value : arr)
    {
        one = std::max({value, one + value, zero});
        zero = std::max(value, zero + value);
        result = std::max(result, one);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumSum(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, -2, 0, 3}, 4, trials);
    test({1, -2, -2, 3}, 3, trials);
    test({-1, -1, -1, -1}, -1, trials);
    return 0;
}


