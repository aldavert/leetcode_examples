#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1 // 4ms (LC) | 620ms (Local)
std::vector<std::vector<int> > subsetsWithDup(std::vector<int> nums)
{
    int n = static_cast<int>(nums.size());
    std::string nums_str(n, 0);
    for (int i = 0; i < n; ++i)
        nums_str[i] = static_cast<char>('a' + nums[i] + 10);
    std::sort(nums_str.begin(), nums_str.end());
    std::unordered_set<std::string> seen;
    char buffer[12] = {};
    for (int mask = 0, size = 1 << n; mask < size; ++mask)
    {
        char * ptr = buffer;
        for (int i = 0; i < n; ++i)
            if ((mask >> i) & 1)
                *(ptr++) = nums_str[i];
        *ptr = '\0';
        if (std::string code(buffer); seen.find(code) == seen.end())
            seen.insert(code);
    }
    std::vector<std::vector<int> > result(seen.size());
    for (int idx = 0; const auto &s : seen)
    {
        const int l = static_cast<int>(s.size());
        result[idx].resize(l);
        for (int k = 0; k < l; ++k)
            result[idx][k] = static_cast<int>(s[k] - 'a') - 10;
        ++idx;
    }
    return result;
}
#elif 0 // 4ms (LC) | 194ms (Local)
std::vector<std::vector<int> > subsetsWithDup(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> aux;
    aux.reserve(nums.size());
    std::sort(nums.begin(), nums.end());
    auto solution = [&](void) -> std::vector<std::vector<int> >
    {
        std::vector<std::vector<int> > result;
        auto inner = [&](auto &&self, int idx) -> void
        {
            result.push_back(aux);
            if (idx < n)
            {
                for (int i = idx; i < n; ++i)
                {
                    if ((i > idx) && (nums[i] == nums[i - 1])) continue;
                    aux.push_back(nums[i]);
                    self(self, i + 1);
                    aux.pop_back();
                }
            }
        };
        inner(inner, 0);
        return result;
    };
    return solution();
}
#elif 0 // 4ms (LC) | 484ms (Local)
std::vector<std::vector<int> > subsetsWithDup(std::vector<int> nums)
{
    std::sort(nums.begin(), nums.end());
    std::vector<std::vector<int> > result{{}};
    for (auto num : nums)
    {
        for (size_t i = 0, size = result.size(); i < size; ++i)
        {
            std::vector<int> aux = result[i];
            aux.push_back(num);
            if (std::find(result.begin(), result.end(), aux) == result.end())
                result.push_back(aux);
        }
    }
    return result;
}
#else // 4ms (LC) | 500 ms
std::vector<std::vector<int> > subsetsWithDup(std::vector<int> nums)
{
    const size_t n = nums.size();
    std::string nums_str(n, 0);
    for (size_t i = 0; i < n; ++i)
        nums_str[i] = static_cast<char>('a' + nums[i] + 10);
    sort(nums_str.begin(), nums_str.end());
    std::unordered_set<std::string> seen;
    std::vector<std::string> result_str{""};
    for (char num : nums_str)
    {
        for (size_t i = 0, size = result_str.size(); i < size; ++i)
        {
            if (std::string aux = result_str[i] + num; seen.find(aux) == seen.end())
            {
                seen.insert(aux);
                result_str.push_back(aux);
            }
        }
    }
    const size_t N = result_str.size();
    std::vector<std::vector<int> > result(N);
    for (size_t i = 0; i < N; ++i)
    {
        const size_t l = result_str[i].size();
        result[i].resize(l);
        for (size_t j = 0; j < l; ++j)
            result[i][j] = static_cast<int>(result_str[i][j] - 'a') - 10;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    const size_t n = left.size();
    std::unordered_set<std::string> left_set, right_set;
    auto vec2str = [](const std::vector<int> &vec)
    {
        const int l = static_cast<int>(vec.size());
        std::string text(l, '0');
        for (int j = 0; j < l; ++j)
            text[j] = static_cast<char>('a' + vec[j] + 10);
        return text;
    };
    for (size_t i = 0; i < n; ++i)
    {
        left_set.emplace(vec2str(left[i]));
        right_set.emplace(vec2str(right[i]));
    }
    if ((n != left_set.size()) || (n != right_set.size())) return false;
    for (const std::string &key : left_set)
        if (right_set.find(key) == right_set.end()) return false;
    return true;
}

void test(std::vector<int> nums,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = subsetsWithDup(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2}, {{}, {1}, {1, 2}, {1, 2, 2}, {2}, {2, 2}}, trials);
    test({0}, {{}, {0}}, trials);
    test({1, 2, 3}, {{}, {1}, {1, 2}, {1, 2, 3}, {1, 3}, {2}, {2, 3}, {3}}, trials);
    test({4, 4, 4, 1, 4},
         {{}, {1}, {1, 4}, {1, 4, 4}, {1, 4, 4, 4}, {1, 4, 4, 4, 4},
          {4}, {4, 4}, {4, 4, 4}, {4, 4, 4, 4}}, trials);
    test({2, 1, 2, 1, 3},
         {{}, {1}, {1, 1}, {1, 1, 2}, {1, 1, 2, 2}, {1, 1, 2, 2, 3},
          {1, 1, 2, 3}, {1, 1, 3}, {1, 2}, {1, 2, 2}, {1, 2, 2, 3},
          {1, 2, 3}, {1, 3}, {2}, {2, 2}, {2, 2, 3}, {2, 3}, {3}}, trials);
    return 0;
}


