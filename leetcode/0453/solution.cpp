#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minMoves(std::vector<int> nums)
{
    int minimum = nums[0];
    for (int number : nums)
        minimum = std::min(minimum, number);
    int result = 0;
    for (int number : nums)
        result += number - minimum;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minMoves(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 3, trials);
    test({1, 1, 1}, 0, trials);
    test({7, 1, 2}, 7, trials);
    test({7, 1, 1, 2, 2}, 8, trials);
    test({1, 1, 1, 1, 4}, 3, trials);
    test({1, 1, 1, 4}, 3, trials);
    test({1, 1, 4}, 3, trials);
    test({1, 4}, 3, trials);
    test({4}, 0, trials);
    return 0;
}


