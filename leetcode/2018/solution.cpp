#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool placeWordInCrossword(std::vector<std::vector<char> > board, std::string word)
{
    auto tokens = [&](const std::vector<char> &chars) -> std::vector<std::string>
    {
        std::string row;
        for (char c : chars) row += c;
        std::vector<std::string> result;
        size_t start = 0, end;
        do
        {
            end = row.find('#', start);
            std::string token = row.substr(start, end - start);
            if (!token.empty())
                result.push_back(token);
            start = end + 1;
        }
        while (end != std::string::npos);
        return result;
    };
    auto canFit = [](const std::string &letters, const std::string &token) -> bool
    {
        for (size_t i = 0; i < letters.size(); ++i)
            if ((token[i] != ' ') && (token[i] != letters[i]))
                return false;
        return true;
    };
    std::vector<std::vector<char> > rotated(board[0].size(),
                                            std::vector<char>(board.size()));
    for (size_t i = 0; i < board.size(); ++i)
        for (size_t j = 0; j < board[0].size(); ++j)
            rotated[j][i] = board[i][j];
    std::string reverse_word = std::string(word.rbegin(), word.rend());
    for (const std::vector<std::vector<char> > &state : {board, rotated})
    {
        for (const auto &chars : state)
        {
            for (const auto &token : tokens(chars))
            {
                if (word.size() != token.size()) continue;
                if (canFit(word, token) || canFit(reverse_word, token))
                    return true;
            }
        }
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > board,
          std::string word,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = placeWordInCrossword(board, word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'#', ' ', '#'}, {' ', ' ', '#'}, {'#', 'c', ' '}}, "abc", true, trials);
    test({{' ', '#', 'a'}, {' ', '#', 'c'}, {' ', '#', 'a'}}, "ac", false, trials);
    test({{'#', ' ', '#'}, {' ', ' ', '#'}, {'#', ' ', 'c'}}, "ca", true, trials);
    return 0;
}


