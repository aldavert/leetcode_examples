#include "../common/common.hpp"
#include <cstring>
#include <numeric>

// ############################################################################
// ############################################################################

int subsequencePairCount(std::vector<int> nums)
{
    constexpr int MOD = 1'000'000'007;
    int max_num = *std::max_element(nums.begin(), nums.end());
    int dp[2][201][201] = {};
    dp[0][0][0] = 1;
    bool s = false;
    for (int num : nums)
    {
        std::memset(dp[!s], 0, sizeof(dp[0]));
        for (int x = 0; x <= max_num; ++x)
        {
            for (int y = 0; y <= max_num; ++y)
            {
                dp[!s][x][y] = (dp[!s][x][y] + dp[s][x][y]) % MOD;
                const int new_x = std::gcd(x, num);
                dp[!s][new_x][y] = (dp[!s][new_x][y] + dp[s][x][y]) % MOD;
                const int new_y = std::gcd(y, num);
                dp[!s][x][new_y] = (dp[!s][x][new_y] + dp[s][x][y]) % MOD;
            }
        }
        s = !s;
    }
    int result = 0;
    for (int i = 1; i <= max_num; ++i)
        result = (result + dp[s][i][i]) % MOD;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = subsequencePairCount(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 10, trials);
    test({10, 20, 30}, 2, trials);
    return 0;
}


