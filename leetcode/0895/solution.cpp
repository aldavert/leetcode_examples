#include "../common/common.hpp"
#include <unordered_map>
#include <stack>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class FreqStack
{
public:
    void push(int val)
    {
        m_frequency_values[++m_histogram[val]].push(val);
        m_maximum_frequency = std::max(m_maximum_frequency, m_histogram[val]);
    }
    int pop()
    {
        const int val = m_frequency_values[m_maximum_frequency].top();
        --m_histogram[val];
        m_frequency_values[m_maximum_frequency].pop();
        if (m_frequency_values[m_maximum_frequency].empty())
          --m_maximum_frequency;
        return val;
    }
private:
    int m_maximum_frequency = 0;
    std::unordered_map<int, int> m_histogram;
    std::unordered_map<int, std::stack<int> > m_frequency_values;
};

// ############################################################################
// ############################################################################

void test(std::vector<bool> input_push,
          std::vector<int> input_value,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    const int n = static_cast<int>(input_push.size());
    std::vector<int> result(n, null);
    for (unsigned int t = 0; t < trials; ++t)
    {
        FreqStack fs;
        for (int i = 0; i < n; ++i)
        {
            if (input_push[i])
                fs.push(input_value[i]);
            else result[i] = fs.pop();
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({ true,  true,  true,  true,  true,  true, false, false, false, false},
         {    5,     7,     5,     7,     4,     5,  null,  null,  null,  null},
         { null,  null,  null,  null,  null,  null,     5,     7,     5,     4},
         trials);
    return 0;
}


