#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumDistance(std::string word)
{
    const int n = static_cast<int>(word.size());
    int dp[27][27][301] = {};
    for (int i = 0; i < 27; ++i)
        for (int j = 0; j < 27; ++j)
            for (int k = 0; k < n; ++k)
                dp[i][j][k] = -1;
    auto distance = [](int a, int b) -> int
    {
        if (a == 26) return 0;
        return std::abs(a / 6 - b / 6) + std::abs(a % 6 - b % 6);
    };
    auto dpProcess = [&](auto &&self, int i, int j, int k) -> int
    {
        if (k == n) return 0;
        if (dp[i][j][k] != -1) return dp[i][j][k];
        int next = word[k] - 'A';
        return dp[i][j][k] =
            std::min(distance(i, next) + self(self, next, j, k + 1),
                     distance(j, next) + self(self, i, next, k + 1));
    };
    return dpProcess(dpProcess, 26, 26, 0);
}

// ############################################################################
// ############################################################################

void test(std::string word, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumDistance(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("CAKE", 3, trials);
    test("HAPPY", 6, trials);
    return 0;
}


