#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int minimumCost(int m,
                int n,
                std::vector<int> horizontalCut,
                std::vector<int> verticalCut)
{
    int cost = 0,
        sum_h = std::accumulate(horizontalCut.begin(), horizontalCut.end(), 0),
        sum_v = std::accumulate(verticalCut.begin(), verticalCut.end(), 0);
    std::sort(horizontalCut.begin(), horizontalCut.end(), std::greater<>());
    std::sort(verticalCut.begin(), verticalCut.end(), std::greater<>());
    for (int i = 0, j = 0; i < m - 1 && j < n - 1;)
    {
        if (horizontalCut[i] > verticalCut[j])
        {
            cost += horizontalCut[i] + sum_v;
            sum_h -= horizontalCut[i];
            ++i;
        }
        else
        {
            cost += verticalCut[j] + sum_h;
            sum_v -= verticalCut[j];
            ++j;
        }
    }
    return cost + sum_h + sum_v;
}

// ############################################################################
// ############################################################################

void test(int m,
          int n,
          std::vector<int> horizontalCut,
          std::vector<int> verticalCut,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumCost(m, n, horizontalCut, verticalCut);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 2, {1, 3}, {5}, 13, trials);
    test(2, 2, {7}, {4}, 15, trials);
    return 0;
}


