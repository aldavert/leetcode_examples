#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * reverseOddLevels(TreeNode * root)
{
    auto dfs =
        [&](auto &&self, TreeNode * left, TreeNode * right, bool odd_level) -> void
    {
        if (!left) return;
        if (odd_level) std::swap(left->val, right->val);
        self(self, left->left, right->right, !odd_level);
        self(self, left->right, right->left, !odd_level);
    };
    dfs(dfs, root->left, root->right, true);
    return root;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        TreeNode * new_root = reverseOddLevels(root);
        result = tree2vec(new_root);
        delete new_root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 5, 8, 13, 21, 34}, {2, 5, 3, 8, 13, 21, 34}, trials);
    test({7, 13, 11}, {7, 11, 13}, trials);
    test({0, 1, 2, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2},
         {0, 2, 1, 0, 0, 0, 0, 2, 2, 2, 2, 1, 1, 1, 1}, trials);
    return 0;
}


