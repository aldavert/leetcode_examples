# Reverse Odd Levels of Binary Tree

Given the `root` of a **perfect** binary tree, reverse the node values at each **odd** level of the tree.
- For example, suppose the node values at level 3 are `[2, 1, 3, 4, 7, 11, 29, 18]`, then it should become `[18, 29, 11, 7, 4, 3, 1, 2]`.

Return *the root of the reversed tree.*

A binary tree is **perfect** if all parent nodes have two children and all leaves are on the same level.

The **level** of a node is the number of edges along the path between it and the root node.

#### Example 1:
> ```mermaid 
> flowchart LR;
> subgraph ga [ ]
> A((2))---B((3))
> A---C((5))
> B---D((8))
> B---E((13))
> C---F((21))
> C---G((34))
> end
> subgraph gb [ ]
> M((2))---N((5))
> M---O((3))
> N---P((8))
> N---Q((13))
> O---R((21))
> O---S((34))
> end
> ga-->gb
> classDef box fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class ga,gb box;
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef blue fill:#DDF,stroke:#000,stroke-width:2px;
> classDef red fill:#FDD,stroke:#000,stroke-width:2px;
> class B,O red;
> class N,C blue;
> ```
> *Input:* `root = [2, 3, 5, 8, 13, 21, 34]`  
> *Output:* `[2, 5, 3, 8, 13, 21, 34]`  
> *Explanation:* The tree has only one odd level. The nodes at level `1` are `3, 5` respectively, which are reversed and become `5, 3`.

#### Example 2:
> ```mermaid 
> flowchart LR;
> subgraph ga [ ]
> A((7))---B((13))
> A---C((11))
> end
> subgraph gb [ ]
> D((7))---E((11))
> D---F((13))
> end
> ga-->gb
> classDef box fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class ga,gb box;
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef blue fill:#DDF,stroke:#000,stroke-width:2px;
> classDef red fill:#FDD,stroke:#000,stroke-width:2px;
> class B,F red;
> class E,C blue;
> ```
> *Input:* `root = [7, 13, 11]`  
> *Output:* `[7, 11, 13]`  
> *Explanation:* The nodes at level `1` are `13, 11`, which are reversed and become `11, 13`.

#### Example 3:
> *Input:* `root = [0, 1, 2, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2]`  
> *Output:* `[0, 2, 1, 0, 0, 0, 0, 2, 2, 2, 2, 1, 1, 1, 1]`  
> *Explanation:* The odd levels have non-zero values. The nodes at level `1` were `1, 2`, and are 2, 1 after the reversal. The nodes at level `3` were `1, 1, 1, 1, 2, 2, 2, 2`, and are `2, 2, 2, 2, 1, 1, 1, 1` after the reversal.

#### Constraints:
- The number of nodes in the tree is in the range `[1, 2^{14}]`.
- `0 <= Node.val <= 10^5`
- `root` is a **perfect** binary tree.


