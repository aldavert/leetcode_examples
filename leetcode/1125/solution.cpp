#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> smallestSufficientTeam(std::vector<std::string> req_skills,
                                        std::vector<std::vector<std::string> > people)
{
    const int n = static_cast<int>(req_skills.size());
    const int n_skills = 1 << n;
    std::unordered_map<std::string, int> skill_to_id;
    std::unordered_map<int, std::vector<int> > dp_table;
    dp_table.reserve(n_skills);
    dp_table[0] = {};
    for (int i = 0; i < n; ++i) skill_to_id[req_skills[i]] = i;
    auto getSkill = [&](const std::vector<std::string> &person)
    {
        int mask = 0;
        for (const auto &skill : person)
            if (const auto it = skill_to_id.find(skill); it != skill_to_id.cend())
                mask |= 1 << it->second;
        return mask;
    };
    for (int i = 0, m = static_cast<int>(people.size()); i < m; ++i)
    {
        const int current_skill = getSkill(people[i]);
        for (const auto& [mask, indices] : dp_table)
        {
            const int new_skill_set = mask | current_skill;
            if (new_skill_set == mask)
                continue;
            if (auto search = dp_table.find(new_skill_set); search == dp_table.end())
            {
                auto &entry = dp_table[new_skill_set] = indices;
                entry.push_back(i);
            }
            else if (search->second.size() > indices.size() + 1)
                search->second = indices,
                search->second.push_back(i);
        }
    }
    return dp_table[n_skills - 1];
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<int> left_copy(left), right_copy(right);
    std::sort(left_copy.begin(), left_copy.end());
    std::sort(right_copy.begin(), right_copy.end());
    for (size_t i = 0, n = left_copy.size(); i < n; ++i)
        if (left_copy[i] != right_copy[i])
            return false;
    return true;
}

void test(std::vector<std::string> req_skills,
          std::vector<std::vector<std::string> > people,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestSufficientTeam(req_skills, people);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"java", "nodejs", "reactjs"},
         {{"java"}, {"nodejs"}, {"nodejs", "reactjs"}}, {0, 2}, trials);
    test({"algorithms", "math", "java", "reactjs", "csharp", "aws"},
         {{"algorithms", "math", "java"}, {"algorithms", "math", "reactjs"},
          {"java", "csharp", "aws"}, {"reactjs", "csharp"}, {"csharp", "math"},
          {"aws", "java"}}, {1, 2}, trials);
    return 0;
}


