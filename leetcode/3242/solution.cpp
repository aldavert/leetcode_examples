#include "../common/common.hpp"

// ############################################################################
// ############################################################################

class NeighborSum
{
    int m_size;
    std::vector<std::vector<int> > m_grid;
    std::vector<std::pair<int, int> > m_num_to_pos;
public:
    NeighborSum(const std::vector<std::vector<int> > &grid) :
        m_size(static_cast<int>(grid.size())),
        m_grid(grid),
        m_num_to_pos(m_size * m_size)
    {
        for (int i = 0; i < m_size; ++i)
            for (int j = 0; j < m_size; ++j)
                m_num_to_pos[grid[i][j]] = {i, j};
    }
    int adjacentSum(int value)
    {
        constexpr int dir[] = {-1, 0, 1, 0, -1};
        auto [i, j] = m_num_to_pos[value];
        int sum = 0;
        for (int k = 0; k < 4; ++k)
        {
            int x = i + dir[k],
                y = j + dir[k + 1];
            if ((x >= 0) && (x < m_size) && (y >= 0) && (y < m_size))
                sum += m_grid[x][y];
        }
        return sum;
    }
    int diagonalSum(int value)
    {
        constexpr int dir[] = {-1, -1, 1, 1, -1};
        auto [i, j] = m_num_to_pos[value];
        int sum = 0;
        for (int k = 0; k < 4; ++k)
        {
            int x = i + dir[k],
                y = j + dir[k + 1];
            if ((x >= 0) && (x < m_size) && (y >= 0) && (y < m_size))
                sum += m_grid[x][y];
        }
        return sum;
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<bool> adjacent,
          std::vector<int> value,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        NeighborSum obj(grid);
        result.clear();
        for (size_t i = 0; i < adjacent.size(); ++i)
        {
            if (adjacent[i])
                result.push_back(obj.adjacentSum(value[i]));
            else
                result.push_back(obj.diagonalSum(value[i]));
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 2}, {3, 4, 5}, {6, 7, 8}},
         {true, true, false, false},
         {1, 4, 4, 8},
         {6, 16, 16, 4}, trials);
    test({{1, 2, 0, 3}, {4, 7, 15, 6}, {8, 9, 10, 11}, {12, 13, 14, 5}},
         {true, false},
         {15, 9},
         {23, 45}, trials);
    return 0;
}


