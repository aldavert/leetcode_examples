#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int superPow(int a, std::vector<int> b)
{
    const int MOD = 1337;
    if (a == 0) return 0;
    if (a == 1) return 1;
    int result = 1;
    a = a % MOD;
    for (int i = static_cast<int>(b.size() - 1); i >= 0; --i)
    {
        for (int k = 0; k < b[i]; ++k)
            result = (result * a) % MOD;
        for (int k = 1, aux = a; k < 10; ++k)
            a = (a * aux) % MOD;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int a, std::vector<int> b, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = superPow(a, b);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {3}, 8, trials);
    test(2, {1, 0}, 1024, trials);
    test(25, {3, 5, 8}, 1306, trials);
    test(1, {4, 3,3, 8, 5, 2}, 1, trials);
    return 0;
}


