#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfCuts(int n)
{
    if (n == 1) return 0;
    else if ((n & 1) == 0) return n / 2;
    else return n;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfCuts(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 2, trials);
    test(3, 3, trials);
    return 0;
}


