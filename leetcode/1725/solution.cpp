#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countGoodRectangles(std::vector<std::vector<int> > rectangles)
{
    int maxlen = 0;
    for (const auto &rect : rectangles)
        maxlen = std::max(maxlen, std::min(rect[0], rect[1]));
    int result = 0;
    for (const auto &rect : rectangles)
        result += maxlen == std::min(rect[0], rect[1]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > rectangles, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countGoodRectangles(rectangles);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{5, 8}, {3, 9}, {5, 12}, {16, 5}}, 3, trials);
    test({{2, 3}, {3, 7}, {4, 3}, {3, 7}}, 3, trials);
    return 0;
}


