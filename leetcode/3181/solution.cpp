#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

#if 1
int maxTotalReward(std::vector<int> rewardValues)
{
    auto &values = rewardValues;
    auto help = [&](auto &&self, int i, int limit) -> int
    {
        int result = 0;
        for (int j = i - 1; j >= 0; --j)
        {
            if (values[j] > limit) continue;
            if (result >= values[j] + std::min(limit - values[j], values[j] - 1)) break;
            int next = self(self, j, std::min(limit - values[j], values[j] - 1));
            result = std::max(result, values[j] + next);
        }
        return result;
    };
    std::sort(values.begin(), values.end());
    values.erase(std::unique(values.begin(), values.end()), values.end());
    return values.back()
         + help(help, static_cast<int>(values.size()) - 1, values.back() - 1);
}
#else
int maxTotalReward(std::vector<int> rewardValues)
{
    constexpr int POSSIBLEREWARDS = 100'000;
    std::bitset<POSSIBLEREWARDS> dp;
    dp[0] = true;
    std::sort(rewardValues.begin(), rewardValues.end());
    for (const int num : rewardValues)
    {
        std::bitset<POSSIBLEREWARDS> new_bits = dp;
        new_bits <<= POSSIBLEREWARDS - num;
        new_bits >>= POSSIBLEREWARDS - num;
        dp |= new_bits << num;
    }
    for (int result = POSSIBLEREWARDS - 1; result >= 0; --result)
        if (dp[result])
        return result;
    return -1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> rewardValues, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxTotalReward(rewardValues);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 3, 3}, 4, trials);
    test({1, 6, 4, 3, 2}, 11, trials);
    return 0;
}


