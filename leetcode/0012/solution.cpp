#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::string intToRoman(int num)
{
    std::unordered_map<unsigned int, std::string> roman = {
        {1, "I"}, {5, "V"}, {10, "X"}, {50, "L"}, {100, "C"}, {500, "D"}, {1000, "M"}
    };
    std::string result;
    if ((num >= 1) && (num < 4000))
    {
        for (int divisor = 1'000; divisor >= 1; divisor /= 10)
        {
            unsigned int value = num / divisor;
            num -= value * divisor;
            if ((value >= 1) && (value <= 9))
            {
                if (value == 9) result += roman[divisor] + roman[divisor * 10];
                else
                {
                    if (value == 4)
                        result += roman[divisor] + roman[divisor * 5];
                    else
                    {
                        if (value >= 5)
                        {
                            result += roman[divisor * 5];
                            value -= 5;
                        }
                        for (unsigned int r = 0; r < value; ++r)
                            result += roman[divisor];
                    }
                }
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int num, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = intToRoman(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, "III", trials);
    test(58, "LVIII", trials);
    test(1994, "MCMXCIV", trials);
    return 0;
}



