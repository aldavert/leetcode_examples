#include "../common/common.hpp"
#include <unordered_map>
#include <queue>

// ############################################################################
// ############################################################################

std::vector<std::string> findAllRecipes(std::vector<std::string> recipes,
                                        std::vector<std::vector<std::string> > ingredients,
                                        std::vector<std::string> supplies)
{
    std::vector<std::string> result;
    std::unordered_set<std::string> supplies_set(supplies.begin(), supplies.end());
    std::unordered_map<std::string, std::vector<std::string> > graph;
    std::unordered_map<std::string, int> in_degrees;
    std::queue<std::string> q;
    
    for (size_t i = 0; i < recipes.size(); ++i)
    {
        for (const auto &ingredient : ingredients[i])
        {
            if (!supplies_set.contains(ingredient))
            {
                graph[ingredient].push_back(recipes[i]);
                ++in_degrees[recipes[i]];
            }
        }
    }
    for (const auto &recipe : recipes)
        if (!in_degrees.contains(recipe))
            q.push(recipe);
    while (!q.empty())
    {
        std::string u = q.front();
        q.pop();
        result.push_back(u);
        if (!graph.contains(u)) continue;
        for (const auto &v : graph[u])
            if (--in_degrees[v] == 0)
                q.push(v);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> recipes,
          std::vector<std::vector<std::string> > ingredients,
          std::vector<std::string> supplies,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findAllRecipes(recipes, ingredients, supplies);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"bread"},
         {{"yeast", "flour"}},
         {"yeast", "flour", "corn"},
         {"bread"}, trials);
    test({"bread", "sandwich"},
         {{"yeast", "flour"}, {"bread", "meat"}},
         {"yeast", "flour", "meat"},
         {"bread", "sandwich"}, trials);
    test({"bread", "sandwich", "burger"},
         {{"yeast", "flour"}, {"bread", "meat"}, {"sandwich", "meat", "bread"}},
         {"yeast", "flour", "meat"},
         {"bread", "sandwich", "burger"}, trials);
    return 0;
}


