#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int minimumBoxes(std::vector<int> apple, std::vector<int> capacity)
{
    std::vector<int> sorted_capacity(capacity);
    std::sort(sorted_capacity.begin(), sorted_capacity.end());
    int total_number_of_apples = std::accumulate(apple.begin(), apple.end(), 0);
    int result = 0;
    for (int i = static_cast<int>(sorted_capacity.size()) - 1;
         (i >= 0) && (total_number_of_apples > 0);
         ++result, total_number_of_apples -= sorted_capacity[i--]);
    return result;
        
}

// ############################################################################
// ############################################################################

void test(std::vector<int> apple,
          std::vector<int> capacity,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumBoxes(apple, capacity);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2}, {4, 3, 1, 5, 2}, 2, trials);
    test({5, 5, 5}, {2, 4, 2, 7}, 4, trials);
    test({1, 8, 3, 3, 5}, {3, 9, 5, 1, 9}, 3, trials);
    return 0;
}


