#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > queensAttacktheKing(std::vector<std::vector<int> > queens,
                                                   std::vector<int> king)
{
    std::vector<std::vector<int> > result;
    std::unordered_set<int> queens_set;
    auto hash = [](int i, int j) -> int { return i * 8 + j; };
    for (auto &queen : queens)
        queens_set.insert(hash(queen[0], queen[1]));
    std::vector<std::vector<int> > directions = {
        {-1, -1}, {-1,  0}, {-1,  1}, { 0, -1},
        { 0,  1}, { 1, -1}, { 1,  0}, { 1,  1}};
    for (auto d : directions)
    {
        for (int i = king[0] + d[0], j = king[1] + d[1];
             0 <= i && i < 8 && 0 <= j && j < 8; i += d[0], j += d[1])
        {
            if (queens_set.count(hash(i, j)))
            {
                result.push_back({i, j});
                break;
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > queens,
          std::vector<int> king,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = queensAttacktheKing(queens, king);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {1, 0}, {4, 0}, {0, 4}, {3, 3}, {2, 4}}, {0, 0},
         {{0, 1}, {1, 0}, {3, 3}}, trials);
    test({{0, 0}, {1, 1}, {2, 2}, {3, 4}, {3, 5}, {4, 4}, {4, 5}}, {3, 3},
         {{2, 2}, {3, 4}, {4, 4}}, trials);
    return 0;
}


