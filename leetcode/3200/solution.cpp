#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int maxHeightOfTriangle(int red, int blue)
{
    auto maxHeight = [](int n1, int n2) -> int
    {
        int odd_height = static_cast<int>(std::sqrt(4 * n1)) - 1,
            even_height = static_cast<int>(std::sqrt(4 * n2 + 1)) - 1;
        return std::min(odd_height, even_height)
             + (std::abs(odd_height - even_height) >= 1);
    };
    return std::max(maxHeight(red, blue), maxHeight(blue, red));
}

// ############################################################################
// ############################################################################

void test(int red, int blue, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxHeightOfTriangle(red, blue);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 4, 3, trials);
    test(2, 1, 2, trials);
    test(1, 1, 1, trials);
    test(10, 1, 2, trials);
    return 0;
}


