# Maximum Height of a Triangle

You are given two integers `red` and `blue` representing the count of red and blue colored balls. You have to arrange these balls to form a triangle such that the 1st row will have 1 ball, the 2nd row will have 2 balls, the 3rd row will have 3 balls, and so on.

All the balls in a particular row should be the **same** color, and adjacent rows should have **different** colors.

Return the **maximum** *height of the triangle* that can be achieved.

#### Example 1:
> *Input:* `red = 2, blue = 4`  
> *Output:* `3`  
> *Explanation:*
> ```mermaid 
> graph TD;
> A(( ))---B(( ))
> A---C(( ))
> B---D(( ))
> B---E(( ))
> C---E
> C---F(( ))
> classDef red fill:#FAA,stroke:#A00,stroke-width:2px;
> classDef blue fill:#AAF,stroke:#00A,stroke-width:2px;
> class A,D,E,F blue;
> class B,C red;
> linkStyle 0,1,2,3,4,5 stroke-width:0px;
> ```

The only possible arrangement is shown above.

#### Example 2:
> *Input:* `red = 2, blue = 1`  
> *Output:* `2`  
> *Explanation:*
> ```mermaid 
> graph TD;
> A(( ))---B(( ))
> A---C(( ))
> classDef red fill:#FAA,stroke:#A00,stroke-width:2px;
> classDef blue fill:#AAF,stroke:#00A,stroke-width:2px;
> class A blue;
> class B,C red;
> linkStyle 0,1 stroke-width:0px;
> ```
> The only possible arrangement is shown above.

#### Example 3:
> *Input:* `red = 1, blue = 1`  
> *Output:* `1`

#### Example 4:
> *Input:* `red = 10, blue = 1`  
> *Output:* `2`  
> *Explanation:*
> ```mermaid 
> graph TD;
> A(( ))---B(( ))
> A---C(( ))
> classDef red fill:#FAA,stroke:#A00,stroke-width:2px;
> classDef blue fill:#AAF,stroke:#00A,stroke-width:2px;
> class A blue;
> class B,C red;
> linkStyle 0,1 stroke-width:0px;
> ```
> The only possible arrangement is shown above.

#### Constraints:
- `1 <= red, blue <= 100`


