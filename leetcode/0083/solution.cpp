#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

ListNode * deleteDuplicates(ListNode * head)
{
    if (!head) return nullptr;
    for (ListNode * ptr = head; ptr->next;)
    {
        if (ptr->next->val == ptr->val)
        {
            ListNode * aux = ptr->next;
            ptr->next = ptr->next->next;
            aux->next = nullptr;
            delete aux;
        }
        else ptr = ptr->next;
    }
    return head;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(input);
        ListNode * new_head = deleteDuplicates(head);
        result = list2vec(new_head);
        delete new_head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2}, {1, 2}, trials);
    test({1, 1, 2, 3, 3}, {1, 2, 3}, trials);
    return 0;
}

