#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findLucky(std::vector<int> arr)
{
    std::set<int> lucky;
    int histogram[501] = {};
    for (int value : arr)
    {
        if (histogram[value] == value)
            lucky.erase(lucky.find(value));
        ++histogram[value];
        if (histogram[value] == value)
            lucky.insert(value);
    }
    return (lucky.empty())?-1:*lucky.rbegin();
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLucky(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 2, 3, 4}, 2, trials);
    test({1, 2, 2, 3, 3, 3}, 3, trials);
    test({2, 2, 2, 3, 3}, -1, trials);
    return 0;
}


