# Minimum Cost Tree From Leaf Values

Given an array `arr` of positive integers, consider all binary trees such that:
- Each node has either `0` or `2` children;
- The values of `arr` correspond to the values of each leaf in an in-order traversal of the tree.
- The value of each non-leaf node is equal to the product of the largest leaf value in its left and right subtree, respectively.

Among all possible binary trees considered, return *the smallest possible sum of the values of each non-leaf node.* It is guaranteed this sum fits into a **32-bit** integer.

A node is a **leaf** if and only if it has zero children.

#### Example 1:
> ```mermaid 
> graph TD;
> A((24))---B((12))
> A---C((4))
> B---D((6))
> B---E((2))
> C---EMPTY1(( ))
> C---EMPTY2(( ))
> F((24))---G((6))
> G---EMPTY3(( ))
> G---EMPTY4(( ))
> F---H((8))
> H---I((2))
> H---J((4))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY1,EMPTY2,EMPTY3,EMPTY4 empty;
> linkStyle 4,5,7,8 stroke-width:0px;
> ```
> *Input:* `arr = [6, 2, 4]`
> *Output:* `32`  
> *Explanation:* There are two possible trees shown. The first has a non-leaf node sum `36`, and the second has non-leaf node sum `32`.

#### Example 2:
> ```mermaid 
> graph TD;
> A((44))---B((4))
> A---C((11))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `arr = [4, 11]`  
> *Output:* `44`

#### Constraints:
- `2 <= arr.length <= 40`
- `1 <= arr[i] <= 15`
- It is guaranteed that the answer fits into a **32-bit** signed integer (i.e., it is less than `2^{31}`).


