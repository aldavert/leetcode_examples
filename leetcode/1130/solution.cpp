#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int mctFromLeafValues(std::vector<int> arr)
{
    std::vector<int> stack{std::numeric_limits<int>::max()};
    int result = 0;
    for (int a : arr)
    {
        while (stack.back() <= a)
        {
            int mid = stack.back();
            stack.pop_back();
            result += std::min(stack.back(), a) * mid;
        }
        stack.push_back(a);
    }
    for (size_t i = 2; i < stack.size(); ++i)
        result += stack[i] * stack[i - 1];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = mctFromLeafValues(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({6, 2, 4}, 32, trials);
    test({4, 11}, 44, trials);
    return 0;
}


