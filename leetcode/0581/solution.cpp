#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

#if 1
int findUnsortedSubarray(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int value_min = std::numeric_limits<int>::max();
    int value_max = std::numeric_limits<int>::lowest();
    int start = 0, end = -1;
    for (int l = 0, r = n - 1; l < n; ++l, --r)
    {
        if (nums[l]  >= value_max) value_max = nums[l];
        else end = l;
        if (nums[r] <= value_min) value_min = nums[r];
        else start = r;
    }
    return end - start + 1;
}
#else
int findUnsortedSubarray(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int value_min = std::numeric_limits<int>::max();
    int value_max = std::numeric_limits<int>::lowest();
    int idx_left, idx_right;
    for (int i = 1, error = 0; i < n; ++i)
        if ((error = error || (nums[i] < nums[i - 1])))
            value_min = std::min(value_min, nums[i]);
    for (int i = n - 2, error = 0; i >= 0; --i)
        if ((error = error || (nums[i] > nums[i + 1])))
            value_max = std::max(value_max, nums[i]);
    for (idx_left = 0; idx_left < n; ++idx_left)
        if (nums[idx_left] > value_min)
            break;
    for (idx_right = n - 1; idx_right >= 0; --idx_right)
        if (nums[idx_right] < value_max)
            break;
    return (idx_left < idx_right)?(idx_right - idx_left + 1):0;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findUnsortedSubarray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 6, 4, 8, 10, 9, 15}, 5, trials);
    test({1, 2, 3, 4}, 0, trials);
    test({1}, 0, trials);
    test({2, 1}, 2, trials);
    return 0;
}


