# Delete the Middle Node of a Linked List

You are given the `head` of a linked list. **Delete** the **middle node**, and return *the* `head` *of the modified linked list*.

The **middle node** of a linked list of size `n` is the `⌊n / 2⌋^th` node from the **start** using **0-based indexing**, where `⌊x⌋` denotes the largest integer less than or equal to `x`.

- For `n = 1, 2, 3, 4,` and `5,` the middle nodes are `0, 1, 1, 2,` and `2,` respectively.

#### Example 1
> ```mermaid 
> graph LR
> A((1))-->B((3))
> B-->C((4))
> C-->D((7))
> D-->E((1))
> E-->F((2))
> F-->G((6))
> classDef delete fill:#FC0,stroke:#555,stroke-width:5px;
> classDef default fill:#FFF,stroke:#555,stroke-width:3px;
> class D delete;
> ```
> *Input:* `head = [1, 3, 4, 7, 1, 2, 6]`  
> *Output:* `[1, 3, 4, 1, 2, 6]`  
> *Explanation:* The above figure represents the given linked list. The indices of the nodes are written below. Since `n = 7`, node `3` with value `7` is the middle node, which is marked in red. We return the new list after removing this node. 

#### Example 2:
> ```mermaid 
> graph LR
> A((1))-->B((2))
> B-->C((3))
> C-->D((4))
> classDef delete fill:#FC0,stroke:#555,stroke-width:5px;
> classDef default fill:#FFF,stroke:#555,stroke-width:3px;
> class C delete;
> ```
> *Input:* `head = [1, 2, 3, 4]`  
> *Output:* `[1, 2, 4]`  
> *Explanation:* The above figure represents the given linked list. For `n = 4`, node `2` with value `3` is the middle node, which is marked in red.

#### Example 3:
> ```mermaid 
> graph LR
> A((2))-->B((1))
> classDef delete fill:#FC0,stroke:#555,stroke-width:5px;
> classDef default fill:#FFF,stroke:#555,stroke-width:3px;
> class B delete;
> ```
> *Input:* `head = [2, 1]`  
> *Output:* `[2]`  
> *Explanation:* The above figure represents the given linked list. For `n = 2`, node `1` with value `1` is the middle node, which is marked in red. Node `0` with value `2` is the only node remaining after removing node `1`.

#### Constraints:
- The number of nodes in the list is in the range `[1, 10^5]`.
- `1 <= Node.val <= 10^5`


