#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

ListNode * deleteMiddle(ListNode * head)
{
    if (head && head->next)
    {
        auto next = [](ListNode * ptr) { return (ptr)?ptr->next:nullptr; };
        ListNode * result = new ListNode(head->val);
        ListNode * pin = next(head);
        ListNode * pdbl = next(next(pin));
        ListNode * pout = result;
        for (; pdbl; pout = next(pout), pin = next(pin), pdbl = next(next(pdbl)))
            pout->next = new ListNode(pin->val);
        pin = next(pin);
        for (; pin; pin = next(pin), pout = next(pout))
            pout->next = new ListNode(pin->val);
        return result;
    }
    else return nullptr;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> vec,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(vec);
        ListNode * output = deleteMiddle(head);
        result = list2vec(output);
        delete head;
        delete output;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 4, 7, 1, 2, 6}, {1, 3, 4, 1, 2, 6}, trials);
    test({1, 2, 3, 4}, {1, 2, 4}, trials);
    test({2, 1}, {2}, trials);
    return 0;
}

