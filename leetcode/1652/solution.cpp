#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> decrypt(std::vector<int> code, int k)
{
    const int n = static_cast<int>(code.size());
    auto mod = [&](int value) { return (n + (value % n)) % n; };
    std::vector<int> result(n, 0);
    if (k > 0)
        for (int i = 0; i < n; ++i)
            for (int c = 0; c < k; ++c)
                result[i] += code[mod(i + c + 1)];
    else if (k < 0)
        for (int i = 0; i < n; ++i)
            for (int c = 0; c > k; --c)
                result[i] += code[mod(i + c - 1)];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> code,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = decrypt(code, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 7, 1, 4}, 3, {12, 10, 16, 13}, trials);
    test({1, 2, 3, 4}, 0, {0, 0, 0, 0}, trials);
    test({2, 4, 9, 3}, -2, {12, 5, 6, 13}, trials);
    return 0;
}


