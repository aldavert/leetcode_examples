#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumCostSubstring(std::string s, std::string chars, std::vector<int> vals)
{
    int result = 0, cost = 0;
    int costs[26] = {};
    
    for (int i = 0; i < 26; ++i) costs[i] = i + 1;
    for (int i = 0, n = static_cast<int>(chars.size()); i < n; ++i)
        costs[chars[i] - 'a'] = vals[i];
    for (char c : s)
    {
        cost = std::max(0, cost + costs[c - 'a']);
        result = std::max(result, cost);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::string chars,
          std::vector<int> vals,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumCostSubstring(s, chars, vals);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("adaa", "d", {-1000}, 2, trials);
    test("abc", "abc", {-1, -1, -1}, 0, trials);
    return 0;
}


