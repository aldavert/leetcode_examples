#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int orderOfLargestPlusSign(int n, std::vector<std::vector<int> > mines)
{
    const short one = 1;
    short grid[500][500] = {};
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            grid[i][j] = 1;
    for (const auto &coord : mines)
        grid[coord[0]][coord[1]] = 0;
    // Left
    for (int i = 0; i < n; ++i)
        for (int j = 1; j < n; ++j)
            grid[i][j] = (grid[i][j])?(grid[i][j - 1] + 1):0;
    // Up
    for (int i = 0; i < n; ++i)
    {
        short accum = grid[0][i] = std::min(grid[0][i], one);
        for (int j = 1; j < n; ++j)
            grid[j][i] = std::min(grid[j][i], accum = (grid[j][i])?(accum + 1):0);
    }
    // Right
    for (int i = 0; i < n; ++i)
    {
        short accum = grid[i][n - 1] = std::min(grid[i][n - 1], one);
        for (int j = n - 2; j >= 0; --j)
            grid[i][j] = std::min(grid[i][j], accum = (grid[i][j])?(accum + 1):0);
    }
    // Down
    for (int i = 0; i < n; ++i)
    {
        short accum = grid[n - 1][i] = std::min(grid[n - 1][i], one);
        for (int j = n - 2; j >= 0; --j)
            grid[j][i] = std::min(grid[j][i], accum = (grid[j][i])?(accum + 1):0);
    }
    short result = 0;
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            result = std::max(result, grid[i][j]);
    return result;
}
#else
int orderOfLargestPlusSign(int n, std::vector<std::vector<int> > mines)
{
    char grid[500][500] = {};
    short accum_left[500][500] = {}, accum_up[500][500] = {},
          accum_down[500][500] = {}, accum_right[500][500] = {};
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            grid[i][j] = 1;
            accum_left[i][j] = accum_up[i][j] = accum_down[i][j] = accum_right[i][j] = 1;
        }
    }
    for (const auto &coord : mines)
    {
        int x = coord[0];
        int y = coord[1];
        grid[x][y] = 0;
        accum_left[x][y] = accum_up[x][y] = accum_down[x][y] = accum_right[x][y] = 0;
    }
    for (int i = 0; i < n; ++i)
    {
        for (int j = 1; j < n; ++j)
        {
            accum_left[i][j] = (grid[i][j])?(accum_left[i][j - 1] + 1):0;
            accum_down[j][i] = (grid[j][i])?(accum_down[j - 1][i] + 1):0;
        }
        for (int j = n - 2; j >= 0; --j)
        {
            accum_right[i][j] = (grid[i][j])?(accum_right[i][j + 1] + 1):0;
            accum_up[j][i] = (grid[j][i])?(accum_up[j + 1][i] + 1):0;
        }
    }
    short result = 0;
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            if (grid[i][j])
            {
                result = std::max(result, std::min(std::min(accum_left[i][j], accum_right[i][j]), std::min(accum_up[i][j], accum_down[i][j])));
            }
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int n, std::vector<std::vector<int> > mines, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = orderOfLargestPlusSign(n, mines);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{4, 2}}, 2, trials);
    test(1, {{0, 0}}, 0, trials);
    return 0;
}


