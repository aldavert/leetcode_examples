#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool hasTrailingZeros(std::vector<int> nums)
{
    int number_even = 0;
    for (int value : nums)
        number_even += (value & 1) == 0;
    return number_even > 1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = hasTrailingZeros(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, true, trials);
    test({2, 4, 8, 16}, true, trials);
    test({1, 3, 5, 7, 9}, false, trials);
    return 0;
}


