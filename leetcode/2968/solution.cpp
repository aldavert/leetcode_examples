#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxFrequencyScore(std::vector<int> nums, long long k)
{
    int result = 0;
    std::sort(nums.begin(), nums.end());
    long cost = 0;
    for (int l = 0, r = 0, n = static_cast<int>(nums.size()); r < n; ++r)
    {
        cost += nums[r] - nums[(l + r) / 2];
        for (; cost > k; ++l)
            cost -= nums[(l + r + 1) / 2] - nums[l];
        result = std::max(result, r - l + 1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxFrequencyScore(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 6, 4}, 3, 3, trials);
    test({1, 4, 4, 2, 4}, 0, 3, trials);
    return 0;
}


