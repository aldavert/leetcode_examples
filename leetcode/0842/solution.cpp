#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> splitIntoFibonacci(std::string num)
{
    auto dfs = [&](void) -> std::vector<int>
    {
        const int n = static_cast<int>(num.size());
        std::vector<int> result;
        auto inner = [&](auto &&self, int s) -> bool
        {
            if ((s == n) &&  (result.size() >= 3))
                return true;
            for (int i = s; i < n; ++i)
            {
                if ((num[s] == '0') && (i > s))
                    break;
                const long v = std::stol(num.substr(s, i + 1 - s));
                if (v > static_cast<long>(std::numeric_limits<int>::max()))
                    break;
                if ((result.size() >= 2)
                &&  (v > result[result.size() - 2]
                         + static_cast<long>(result.back())))
                    break;
                if ((result.size() <= 1)
                ||  (v == result[result.size() - 2]
                          + static_cast<long>(result.back())))
                {
                    result.push_back(static_cast<int>(v));
                    if (self(self, i + 1))
                        return true;
                    result.pop_back();
                }
            }
            return false;
        };
        inner(inner, 0);
        return result;
    };
    return dfs();
}

// ############################################################################
// ############################################################################

void test(std::string num, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = splitIntoFibonacci(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1101111", {11, 0, 11, 11}, trials);
    test("112358130", {}, trials);
    test("0123", {}, trials);
    return 0;
}


