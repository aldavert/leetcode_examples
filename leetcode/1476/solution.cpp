#include "../common/common.hpp"

constexpr int null = -1'000;

// ############################################################################
// ############################################################################

class SubrectangleQueries
{
    const std::vector<std::vector<int> > &m_rectangle;
    std::vector<std::array<int, 5> > m_updates;
public:
    SubrectangleQueries(std::vector<std::vector<int> > &rectangle) :
        m_rectangle(rectangle) {}
    void updateSubrectangle(int row1, int col1, int row2, int col2, int newValue)
    {
        m_updates.push_back({row1, col1, row2, col2, newValue});
    }
    int getValue(int row, int col)
    {
        for (int i = static_cast<int>(m_updates.size()) - 1; i >= 0; --i)
        {
            auto [r1, c1, r2, c2, v] = m_updates[i];
            if ((r1 <= row) && (row <= r2) && (c1 <= col) && (col <= c2))
                return v;
        }
        return m_rectangle[row][col];
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > rectangles,
          std::vector<std::vector<int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        SubrectangleQueries obj(rectangles);
        for (size_t i = 0; i < input.size(); ++i)
        {
            if (input[i].size() == 5)
            {
                obj.updateSubrectangle(input[i][0],
                                       input[i][1],
                                       input[i][2],
                                       input[i][3],
                                       input[i][4]);
                result.push_back(null);
            }
            else result.push_back(obj.getValue(input[i][0], input[i][1]));
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 1}, {4, 3, 4}, {3, 2, 1}, {1, 1, 1}},
         {{0, 2}, {0, 0, 3, 2, 5}, {0, 2}, {3, 1}, {3, 0, 3, 2, 10}, {3, 1}, {0, 2}},
         {1, null, 5, 5, null, 10, 5}, trials);
    test({{1, 1, 1}, {2, 2, 2}, {3, 3, 3}},
         {{0, 0}, {0, 0, 2, 2, 100}, {0, 0}, {2, 2}, {1, 1, 2, 2, 20}, {2, 2}},
         {1, null, 100, 100, null, 20}, trials);
    return 0;
}


