#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

#if 1                                   // Time (1'000 repetitions): 0.054 seconds
int numDupDigitsAtMostN(int n)
{
    int dp_table[10][2][2];
    std::vector<int> digits;
    for (int x = n; x > 0; x /= 10)
        digits.push_back(x % 10);
    int number_of_digits = static_cast<int>(digits.size());
    std::reverse(digits.begin(),digits.end());
    for (int digit = 0; digit < 10; ++digit)
        for (int tight = 0; tight < 2; ++tight)
            for (int repetition = 0; repetition < 2; ++repetition)
                dp_table[digit][tight][repetition] = -1;
    int digit_frequency[10] = {};
    auto process = [&](auto &&self, int p, bool tight, bool repetition) -> int
    {
        if (p == number_of_digits) return repetition;
        if (dp_table[p][tight][repetition] != -1)
            return dp_table[p][tight][repetition];
        int result = 0;
        for (int d = (p == 0), e = (tight)?digits[p]:9; d <= e; ++d)
        {
            ++digit_frequency[d];
            result += self(self, p + 1, tight && (d >= digits[p]),
                                        (digit_frequency[d] > 1) || repetition);
            --digit_frequency[d];
        }
        return dp_table[p][tight][repetition] = result;
    };
    
    int result = 0;
    for(int i = 1; i < number_of_digits; ++i)
    {
        int l = number_of_digits - i, p = 9, q = 9, pow = 1;
        for (int j = 2; j <= l; ++j, p *= q, --q, pow *= 10);
        result += 9 * pow - p;
    }
    return result + process(process, 0, true, false);
}
#else                                   // Time (1'000 repetitions): 26.596 seconds
int numDupDigitsAtMostN(int n)
{
    const int c_digit_size = static_cast<int>(std::log10(n)) + 1;
    std::vector<std::vector<std::vector<int> > > dp_table(c_digit_size + 1,
            std::vector<std::vector<int> >(1 << 10, std::vector<int>(2, -1)));
    auto dp_process = [&](auto &&self,
                          std::string s,
                          int digit_size,
                          int used_mask,
                          bool is_tight) -> int
    {
        if (digit_size == 0) return 1;
        if (dp_table[digit_size][used_mask][is_tight] != -1)
            return dp_table[digit_size][used_mask][is_tight];
        int result = 0, max_digit = is_tight?(s[s.length() - digit_size] - '0'):9;
        for (int digit = 0; digit <= max_digit; ++digit)
        {
            if (used_mask >> digit & 1) continue;
            result += self(self, s, digit_size - 1, used_mask
                    | ((used_mask != 0) || (digit != 0)) * (1 << digit),
                    is_tight && (digit == max_digit));
        }

        return dp_table[digit_size][used_mask][is_tight] = result;
    };
    return n - (dp_process(dp_process, std::to_string(n), c_digit_size, 0, true) - 1);
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numDupDigitsAtMostN(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(20, 1, trials);
    test(100, 10, trials);
    test(1000, 262, trials);
    std::vector<std::tuple<int, int> > values = {
        {725198401, 720573043}, {223441265, 220651895}, {710051127, 705487677},
        {268059877, 265118827}, {956788065, 951308055}, {733678004, 729018794},
        {948733874, 943283672}, {674193736, 669770020}, {385607400, 382230426},
        {132012582, 129581052}, {774234536, 769429166}, {518476565, 514605905},
        {770287073, 765481703}, {321206311, 318048301}, {427086133, 423545251},
        {896469853, 891227563}, {205119544, 202395694}, {252240025, 249359935},
        {316496790, 313362588}, {601643038, 597479908}, {352779617, 349533287},
        {996242321, 990630551}, {612846719, 608637197}, {682615010, 678158840},
        { 61750508,  60096182}, {913579586, 908277536}, {171359449, 168766639},
        {519574370, 515698520}, {617467430, 613239020}, {734922992, 730259174},
        {996004951, 990393181}, {978680216, 973108766}, {132120482, 129688232},
        {420480947, 416964377}, {158582998, 156042748}, {846678563, 841602473},
        {360166320, 356892750}, {231166578, 228371448}, {830368113, 825359703},
        {118673186, 116287016}, {271822150, 268862740}, {408749418, 405280728},
        {349871239, 346638437}, {295262695, 292209805}, { 50943375,  49485861},
        {515360565, 511502715}, {327492706, 324313168}, {110619068, 108232898},
        {657971157, 653579187}, {927895648, 922530838}};
    for (auto [num, solution] : values)
        test(num, solution, trials);
    return 0;
}


