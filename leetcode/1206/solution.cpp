#include "../common/common.hpp"
#include <memory>
#include <stack>

// ############################################################################
// ############################################################################

class Skiplist
{
    struct Node
    {
        int val;
        std::shared_ptr<Node> next;
        std::shared_ptr<Node> down;
        Node(int v = -1,
             std::shared_ptr<Node> n = nullptr,
             std::shared_ptr<Node> d = nullptr) : val(v), next(n), down(d) {}
    };
    std::shared_ptr<Node> dummy = std::make_shared<Node>(-1);
    void advance(std::shared_ptr<Node>& node, int target)
    {
        while (node->next && (node->next->val < target))
            node = node->next;
    }
public:
    bool search(int target) 
    {
        for (std::shared_ptr<Node> node = dummy; node; node = node->down)
        {
            advance(node, target);
            if (node->next && (node->next->val == target))
                return true;
        }
        return false;
    }
    void add(int num)
    {
        std::stack<std::shared_ptr<Node> > nodes;
        for (std::shared_ptr<Node> node = dummy; node; node = node->down)
        {
            advance(node, num);
            nodes.push(node);
        }
        std::shared_ptr<Node> down;
        bool should_insert = true;
        while (should_insert && !nodes.empty())
        {
            std::shared_ptr<Node> prev = nodes.top();
            nodes.pop();
            prev->next = std::make_shared<Node>(num, prev->next, down);
            down = prev->next;
            should_insert = rand() & 1;
        }
        if (should_insert)
            dummy = std::make_shared<Node>(-1, nullptr, dummy);
    }
    bool erase(int num)
    {
        bool found = false;
        for (std::shared_ptr<Node> node = dummy; node; node = node->down)
        {
            advance(node, num);
            if (node->next && (node->next->val == num))
                node->next = node->next->next,
                found = true;
        }
        return found;
    }
};

// ############################################################################
// ############################################################################

enum class OP { ADD, SEARCH, ERASE };

void test(std::vector<OP> operations,
          std::vector<int> input,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        Skiplist obj;
        for (size_t i = 0, n = operations.size(); i < n; ++i)
        {
            if (operations[i] == OP::ADD)
            {
                obj.add(input[i]);
                result.push_back(false);
            }
            else if (operations[i] == OP::SEARCH)
            {
                result.push_back(obj.search(input[i]));
            }
            else if (operations[i] == OP::ERASE)
            {
                result.push_back(obj.erase(input[i]));
            }
            else
            {
                std::cerr << "[ERROR] Operation not implemented yet!\n";
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::ADD, OP::ADD, OP::ADD, OP::SEARCH, OP::ADD, OP::SEARCH, OP::ERASE,
          OP::ERASE, OP::SEARCH},
         {1, 2, 3, 0, 4, 1, 0, 1, 1},
         {false, false, false, false, false, true, false, true, false}, trials);
    return 0;
}


