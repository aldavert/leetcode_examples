#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

std::vector<int> timeTaken(std::vector<std::vector<int> > edges)
{
    struct Info
    {
      int node = -1;
      int time = 0;
    };
    const int n = static_cast<int>(edges.size()) + 1;
    std::vector<int> result(n);
    std::vector<std::vector<int> > tree(n);
    std::vector<std::pair<Info, Info> > dp(n);
    
    auto calcTime = [](int u) -> int { return u % 2 == 0 ? 2 : 1; };
    auto dfs = [&](auto &&self, int u, int prev) -> int
    {
        Info max1, max2;
        for (const int v : tree[u])
        {
            if (v == prev) continue;
            const int time = self(self, v, u) + calcTime(v);
            if (time >= max1.time)
                max2 = std::exchange(max1, {v, time});
            else if (time > max2.time) max2 = {v, time};
        }
        dp[u] = {max1, max2};
        return max1.time;
    };
    auto reroot = [&](auto &&self, int u, int prev, int max_time) -> void
    {
        result[u] = std::max(max_time, dp[u].first.time);
        for (const int v : tree[u])
        {
            if (v == prev) continue;
            int new_max_time = (dp[u].first.node == v)?dp[u].second.time
                                                      :dp[u].first.time;
            self(self, v, u, calcTime(u) + std::max(max_time, new_max_time));
        }
    };
    for (const auto &edge : edges)
    {
        tree[edge[0]].push_back(edge[1]);
        tree[edge[1]].push_back(edge[0]);
    }
    dfs(dfs, 0, -1);
    reroot(reroot, 0, -1, 0);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = timeTaken(edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {0, 2}}, {2, 4, 3}, trials);
    test({{0, 1}}, {1, 2}, trials);
    test({{2, 4}, {0, 1}, {2, 3}, {0, 2}}, {4, 6, 3, 5, 5}, trials);
    return 0;
}


