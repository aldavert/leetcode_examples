#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string largestPalindrome(int n, int k)
{
    switch (k)
    {
    case 1:
        return std::string(n, '9');
    case 2:
        return (n <= 2)?std::string(n, '8'):"8" + std::string(n - 2, '9') + "8";
    case 3:
    case 9:
        return std::string(n, '9');
    case 4:
        return (n <= 4)?std::string(n, '8'):"88" + std::string(n - 4, '9') + "88";
    case 5:
        return (n <= 2)?std::string(n, '5'):"5" + std::string(n - 2, '9') + "5";
    case 6:
        if (n <= 2) return std::string(n, '6');
        else if (n % 2 == 1)
        {
            const int l = n / 2 - 1;
            return "8" + std::string(l, '9') + "8" + std::string(l, '9') + "8";
        }
        else
        {
            const int l = n / 2 - 2;
            return "8" + std::string(l, '9') + "77" + std::string(l, '9') + "8";
        }
    case 8:
        return (n <= 6)?std::string(n, '8'):"888" + std::string(n - 6, '9') + "888";
    default:
        const std::string middle[] = {"",          "7",          "77",
                                      "959",       "9779",       "99799",
                                      "999999",    "9994999",    "99944999",
                                      "999969999", "9999449999", "99999499999"};
        const int q = n / 12;
        const int r = n % 12;
        return std::string(q * 6, '9') + middle[r] + std::string(q * 6, '9');
    }
    return "";
}

// ############################################################################
// ############################################################################

void test(int n, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestPalindrome(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 5, "595", trials);
    test(1, 4, "8", trials);
    test(5, 6, "89898", trials);
    return 0;
}


