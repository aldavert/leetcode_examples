#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimizeSum(std::vector<int> nums)
{
    std::sort(nums.begin(), nums.end());
    const int n = static_cast<int>(nums.size());
    return std::min({nums.back() - nums[2],
                     nums[n - 3] - nums[0],
                     nums[n - 2] - nums[1]});
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimizeSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 7, 8, 5}, 3, trials);
    test({1, 4, 3}, 0, trials);
    return 0;
}


