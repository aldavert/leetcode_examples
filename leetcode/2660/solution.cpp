#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int isWinner(std::vector<int> player1, std::vector<int> player2)
{
    auto score = [](const std::vector<int> &player) -> int
    {
        int result = 0;
        for (int bonus = 0; int pins : player)
        {
            result += ((bonus & 3) > 0) * pins + pins;
            bonus = ((bonus << 1) | (pins >= 10)) & 3;
        }
        return result;
    };
    int score1 = score(player1), score2 = score(player2);
    if      (score1 > score2) return 1;
    else if (score1 < score2) return 2;
    else return 0;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> player1,
          std::vector<int> player2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = isWinner(player1, player2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 10, 7, 9}, {6, 5, 2, 3}, 1, trials);
    test({3, 5, 7, 6}, {8, 10, 10, 2}, 2, trials);
    test({2, 3}, {4, 1}, 0, trials);
    test({7, 8, 8, 5, 2}, {10, 1, 4, 2, 6}, 1, trials);
    return 0;
}


