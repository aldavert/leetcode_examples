#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> zigzagTraversal(std::vector<std::vector<int> > grid)
{
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>((n_rows > 0)?grid[0].size():0);
    const int dir[] = {-1, 1},
              begin[] = { n_cols - 1 - (n_cols % 2 == 1), 0          },
              end[]   = {                              0, n_cols - 1 };
    bool move_right = true;
    std::vector<int> result;
    for (int r = 0; r < n_rows; ++r, move_right = !move_right)
        for (int c = begin[move_right], n = end[move_right];
             dir[move_right] * c <= dir[move_right] * n; c += 2 * dir[move_right])
            result.push_back(grid[r][c]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = zigzagTraversal(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {3, 4}}, {1, 4}, trials);
    test({{2, 1}, {2, 1}, {2, 1}}, {2, 1, 2}, trials);
    test({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, {1, 3, 5, 7, 9}, trials);
    return 0;
}


