#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxScoreWords(std::vector<std::string> words,
                  std::vector<char> letters,
                  std::vector<int> score)
{
    const int n = static_cast<int>(words.size());
    int count[26] = {};
    for (char c : letters) ++count[c - 'a'];
    auto dpProcess = [&](auto &&self, int s) -> int
    {
        int result = 0;
        for (int i = s; i < n; ++i)
        {
            bool is_valid = true;
            int earned = 0;
            for (char c : words[i])
            {
                if (--count[c - 'a'] < 0) is_valid = false;
                earned += score[c - 'a'];
            }
            if (is_valid && (earned > 0))
                result = std::max(result, earned + self(self, i + 1));
            for (char c : words[i]) ++count[c - 'a'];
        }
        return result;
    };
    return dpProcess(dpProcess, 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::vector<char> letters,
          std::vector<int> score,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxScoreWords(words, letters, score);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"dog", "cat", "dad", "good"},
         {'a', 'a', 'c', 'd', 'd', 'd', 'g', 'o', 'o'},
         {1, 0, 9, 5, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0}, 23, trials);
    test({"xxxz", "ax", "bx", "cx"},
         {'z', 'a', 'b', 'c', 'x', 'x', 'x'},
         {4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          5, 0, 10}, 27, trials);
    test({"leetcode"},
         {'l', 'e', 't', 'c', 'o', 'd'},
         {0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0,
          0, 0, 0}, 0, trials);
    test({"aac", "ab", "cc", "aab"},
         {'a', 'a', 'a', 'b', 'c', 'c'},
         {1, 5, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0}, 23, trials);
    return 0;
}


