#include "../common/common.hpp"
#include <queue>
#include <limits>

// ############################################################################
// ############################################################################

std::vector<int> smallestRange(std::vector<std::vector<int> > nums)
{
    struct Element
    {
        int value;
        size_t list_index;
        size_t element_index;
        bool operator<(const Element &other) const { return value > other.value; }
    };
    std::priority_queue<Element> queue;
    int maximum = std::numeric_limits<int>::lowest();
    for (size_t i = 0; i < nums.size(); ++i)
    {
        maximum = std::max(maximum, nums[i][0]);
        queue.push({nums[i][0], i, 0});
    }
    int width = maximum - queue.top().value;
    int selected_maximum = maximum, selected_minimum = queue.top().value;
    while (queue.size() == nums.size())
    {
        Element current = queue.top();
        queue.pop();
        ++current.element_index;
        if (current.element_index < nums[current.list_index].size())
        {
            current.value = nums[current.list_index][current.element_index];
            queue.push(current);
            maximum = std::max(maximum, current.value);
            if (maximum - queue.top().value < width)
            {
                width = maximum - queue.top().value;
                selected_maximum = maximum;
                selected_minimum = queue.top().value;
            }
        }
    }
    return {selected_minimum, selected_maximum};
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > nums,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestRange(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{4, 10, 15, 24, 26}, {0, 9, 12, 20}, {5, 18, 22, 30}}, {20, 24}, trials);
    test({{1, 2, 3}, {1, 2, 3}, {1, 2, 3}}, {1, 1}, trials);
    return 0;
}


