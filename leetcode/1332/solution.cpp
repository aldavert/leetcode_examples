#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int removePalindromeSub(std::string s)
{
    auto palindrom = [](std::string t) -> bool
    {
        if (t.empty()) return true;
        for (size_t i = 0, j = t.size() - 1; i < j; ++i, --j)
            if (t[i] != t[j])
                return false;
        return true;
    };
    return palindrom(s)?1:2;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = removePalindromeSub(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ababa", 1, trials);
    test("abb", 2, trials);
    test("baabb", 2, trials);
    return 0;
}


