#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int lenOfVDiagonal(std::vector<std::vector<int> > grid)
{
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    int a[502][502] = {}, b[502][502] = {}, c[502][502] = {}, d[502][502] = {};
    int result = 0;
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            int v = grid[i][j];
            if (v == 1)
            {
                result = 1;
                continue;
            }
            a[i][j] = b[i][j] = 1;
            if ((i - 1 >= 0) && (j - 1 >= 0) && (grid[i - 1][j - 1] == 2 - v))
                a[i][j] += a[i - 1][j - 1];
            if ((i - 1 >= 0) && (j + 1 < n) && (grid[i - 1][j + 1] == 2 - v))
                b[i][j] += b[i - 1][j + 1];
        }
    }
    for (int i = m - 1; i >= 0; --i)
    {
        for (int j = n - 1; j >= 0; --j)
        {
            int v = grid[i][j];
            if (v == 1) continue;
            c[i][j] = d[i][j] = 1;
            if ((i + 1 < m) && (j - 1 >= 0) && (grid[i + 1][j - 1] == 2 - v))
                c[i][j] += c[i + 1][j - 1];
            if ((i + 1 < m) && (j + 1 < n) && (grid[i + 1][j + 1] == 2 - v))
                d[i][j] += d[i + 1][j + 1];
        }
    }
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            int v = grid[i][j];
            if (v == 1) continue;
            if (((a[i][j] % 2 == 0) && (v == 0)) || ((a[i][j] % 2 == 1) && (v == 2)))
                if (int ki = i - a[i][j], kj = j - a[i][j];
                    (ki >= 0) && (kj >= 0) && (ki < m) && (kj < n)
                &&  (grid[ki][kj] == 1))
                    result = std::max(result, c[i][j] + a[i][j]);
            if (((b[i][j] % 2 == 0) && (v == 0)) || ((b[i][j] % 2 == 1) && (v == 2)))
                if (int ki = i - b[i][j], kj = j + b[i][j];
                    (ki >= 0) && (kj >= 0) && (ki < m) && (kj < n)
                &&  (grid[ki][kj] == 1))
                    result = std::max(result, a[i][j] + b[i][j]);
            if (((c[i][j] % 2 == 0) && (v == 0)) || ((c[i][j] % 2 == 1) && (v == 2)))
                if (int ki = i + c[i][j], kj = j - c[i][j];
                    (ki >= 0) && (kj >= 0) && (ki < m) && (kj < n)
                &&  (grid[ki][kj] == 1))
                    result = std::max(result, d[i][j] + c[i][j]);
            if (((d[i][j] % 2 == 0) && (v == 0)) || ((d[i][j] % 2 == 1) && (v == 2)))
                if (int ki = i + d[i][j], kj = j + d[i][j];
                    (ki >= 0) && (kj >= 0) && (ki < m) && (kj < n)
                &&  (grid[ki][kj] == 1))
                    result = std::max(result, b[i][j] + d[i][j]);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = lenOfVDiagonal(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 2, 1, 2, 2},
          {2, 0, 2, 2, 0},
          {2, 0, 1, 1, 0},
          {1, 0, 2, 2, 2},
          {2, 0, 0, 2, 2}}, 5, trials);
    test({{2, 2, 2, 2, 2},
          {2, 0, 2, 2, 0},
          {2, 0, 1, 1, 0},
          {1, 0, 2, 2, 2},
          {2, 0, 0, 2, 2}}, 4, trials);
    test({{1, 2, 2, 2, 2},
          {2, 2, 2, 2, 0},
          {2, 0, 0, 0, 0},
          {0, 0, 2, 2, 2},
          {2, 0, 0, 2, 0}}, 5, trials);
    test({{1}}, 1, trials);
    return 0;
}


