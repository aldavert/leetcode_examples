#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool primeSubOperation(std::vector<int> nums)
{
    auto sieveEratosthenes = [](int n) -> std::vector<int>
    {
        std::vector<int> primes;
        std::vector<bool> is_prime(n, true);
        is_prime[0] = is_prime[1] = false;
        for (int i = 2; i * i < n; ++i)
            if (is_prime[i])
                for (int j = i * i; j < n; j += i)
                    is_prime[j] = false;
        for (int i = 2; i < n; ++i)
            if (is_prime[i])
                primes.push_back(i);
        return primes;
    };
    const std::vector<int> primes = sieveEratosthenes(1'000);
    int prev_num = 0;
    for (int num : nums)
    {
        auto it = std::lower_bound(primes.begin(), primes.end(), num - prev_num);
        if (it != primes.begin()) num -= *prev(it);
        if (num <= prev_num) return false;
        prev_num = num;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = primeSubOperation(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 9, 6, 10}, true, trials);
    test({6, 8, 11, 12}, true, trials);
    test({5, 8, 3}, false, trials);
    return 0;
}


