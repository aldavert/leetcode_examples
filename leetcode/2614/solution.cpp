#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int diagonalPrime(std::vector<std::vector<int> > nums)
{
    auto isPrime = [](int number) -> bool
    {
        if ((number & 1) == 0) return number == 2;
        for (int i = 3; i * i <= number; i += 2)
            if (number % i == 0) return false;
        return true;
    };
    const size_t n = nums.size();
    std::priority_queue<int> elements;
    for (size_t i = 0; i < n; ++i)
    {
        elements.push(nums[i][i]);
        elements.push(nums[i][n - i - 1]);
    }
    while (!elements.empty())
    {
        int current = elements.top();
        if (current == 1) break;
        if (isPrime(current)) return current;
        elements.pop();
    }
    return 0;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = diagonalPrime(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {5, 6, 7}, {9, 10, 11}}, 11, trials);
    test({{1, 2, 3}, {5, 17, 7}, {9, 11, 10}}, 17, trials);
    return 0;
}


