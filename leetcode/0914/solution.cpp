#include "../common/common.hpp"
#include <unordered_map>
#include <numeric>

// ############################################################################
// ############################################################################

bool hasGroupsSizeX(std::vector<int> deck)
{
    std::unordered_map<int, int> histogram;
    for (int d : deck)
        ++histogram[d];
    int gcd = 0;
    for (const auto &h : histogram)
        gcd = std::gcd(gcd, h.second);
    return gcd >= 2;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> deck, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = hasGroupsSizeX(deck);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 4, 3, 2, 1},  true, trials);
    test({1, 1, 1, 2, 2, 2, 3, 3}, false, trials);
    test({1, 1, 2, 2, 2, 2}, true, trials);
    test({1}, false, trials);
    return 0;
}


