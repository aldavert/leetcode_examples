#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int search(std::vector<int> nums, int target)
{
    auto search = std::lower_bound(nums.begin(), nums.end(), target);
    if ((search == nums.end()) || (*search != target)) return -1;
    else return static_cast<int>(std::distance(nums.begin(), search));
}
#else
int search(std::vector<int> nums, int target)
{
    for (int begin = 0, end = static_cast<int>(nums.size() - 1); begin <= end;)
    {
        int mid = (begin + end) / 2;
        if (target == nums[mid]) return mid;
        else if (target < nums[mid])
            end = mid - 1;
        else begin = mid + 1;
    }
    return -1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = search(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, 0, 3, 5, 9, 12}, 9,  4, trials);
    test({-1, 0, 3, 5, 9, 12}, 2, -1, trials);
    test({-1}, 2, -1, trials);
    return 0;
}


