#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<std::vector<std::string> > mostPopularCreator(
        std::vector<std::string> creators,
        std::vector<std::string> ids,
        std::vector<int> views)
{
    struct Creator
    {
      long popularity;
      std::string video_id;
      int max_view;
    };
    std::vector<std::vector<std::string> > result;
    std::unordered_map<std::string, Creator> name_to_creator;
    long max_popularity = 0;
    
    for (size_t i = 0; i < creators.size(); ++i)
    {
        if (!name_to_creator.contains(creators[i]))
        {
            name_to_creator[creators[i]] = Creator{
                .popularity = views[i],
                .video_id = ids[i],
                .max_view = views[i],
            };
            max_popularity = std::max(max_popularity, static_cast<long>(views[i]));
            continue;
        }
        Creator& creator = name_to_creator[creators[i]];
        creator.popularity += views[i];
        max_popularity = std::max(max_popularity, static_cast<long>(creator.popularity));
        if ((creator.max_view < views[i])
        ||  ((creator.max_view == views[i]) && (creator.video_id > ids[i])))
        {
            creator.video_id = ids[i];
            creator.max_view = views[i];
        }
    }
    for (const auto &[name, creator] : name_to_creator)
        if (creator.popularity == max_popularity)
            result.push_back({name, creator.video_id});
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<std::string> > &left,
                const std::vector<std::vector<std::string> > &right)
{
    if (left.size() != right.size()) return false;
    auto work_left = left, work_right = right;
    std::sort(work_left.begin(), work_left.end());
    std::sort(work_right.begin(), work_right.end());
    for (size_t i = 0; i < left.size(); ++i)
        if (work_left[i] != work_right[i])
            return false;
    return true;
}

void test(std::vector<std::string> creators,
          std::vector<std::string> ids,
          std::vector<int> views,
          std::vector<std::vector<std::string> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<std::string> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = mostPopularCreator(creators, ids, views);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"alice", "bob", "alice", "chris"},
         {"one", "two", "three", "four"},
         {5, 10, 5, 4},
         {{"alice", "one"}, {"bob", "two"}},
         trials);
    test({"alice", "alice", "alice"},
         {"a", "b", "c"},
         {1, 2, 2},
         {{"alice", "b"}},
         trials);
    return 0;
}


