#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minCost(int n, std::vector<int> cuts)
{
    const int m = static_cast<int>(cuts.size() + 2);
    std::vector<int> work = cuts;
    work.push_back(0);
    work.push_back(n);
    std::sort(work.begin(), work.end());
    int dp[102][102] = {};
    
    for (int d = 2; d < m; ++d)
    {
        for (int i = 0; i + d < m; ++i)
        {
            const int j = i + d;
            dp[i][j] = std::numeric_limits<int>::max();
            for (int k = i + 1; k < j; ++k)
                dp[i][j] = std::min(dp[i][j], work[j] - work[i] + dp[i][k] + dp[k][j]);
        }
    }
    return dp[0][m - 1];
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<int> cuts, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCost(n, cuts);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, {1, 3, 4, 5}, 16, trials);
    test(9, {5, 6, 1, 4, 2}, 22, trials);
    return 0;
}


