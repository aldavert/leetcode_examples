#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxSum(std::vector<int> nums, int k)
{
    constexpr int MOD = 1'000'000'007, MAXBIT = 30;
    long result = 0;
    std::vector<int> min_indices(MAXBIT), optimal_nums(nums.size());
    
    for (const int num : nums)
        for (int i = 0; i < MAXBIT; ++i)
            if (num >> i & 1)
                optimal_nums[min_indices[i]++] |= 1 << i;
    for (int i = 0; i < k; ++i)
        result = (result + static_cast<long>(optimal_nums[i]) * optimal_nums[i]) % MOD;
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSum(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 6, 5, 8}, 2, 261, trials);
    test({4, 5, 4, 7}, 3, 90, trials);
    return 0;
}


