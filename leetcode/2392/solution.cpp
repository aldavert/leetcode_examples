#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> >
buildMatrix(int k,
            std::vector<std::vector<int> > rowConditions,
            std::vector<std::vector<int> > colConditions)
{
    using CONDITION = std::vector<std::vector<int> >;
    auto topologicalSort = [&](const CONDITION &conditions, int n) -> std::vector<int>
    {
        std::vector<int> order, in_degree(n + 1);
        std::vector<std::vector<int> > graph(n + 1);
        std::queue<int> q;
        
        for (const auto &condition : conditions)
        {
            graph[condition[0]].push_back(condition[1]);
            ++in_degree[condition[1]];
        }
        for (int i = 1; i <= n; ++i)
            if (in_degree[i] == 0)
                q.push(i);
        while (!q.empty())
        {
            int u = q.front();
            q.pop();
            order.push_back(u);
            for (int v : graph[u])
                if (--in_degree[v] == 0)
                    q.push(v);
        }
        return (static_cast<int>(order.size()) == n)?order:std::vector<int>();
    };
    std::vector<int> row_order = topologicalSort(rowConditions, k);
    if (row_order.empty()) return {};
    std::vector<int> col_order = topologicalSort(colConditions, k);
    if (col_order.empty()) return {};
    
    std::vector<std::vector<int> > result(k, std::vector<int>(k));
    std::vector<int> node_to_row_index(k + 1);
    for (int i = 0; i < k; ++i)
        node_to_row_index[row_order[i]] = i;
    for (int j = 0; j < k; ++j)
        result[node_to_row_index[col_order[j]]][j] = col_order[j];
    return result;
}

// ############################################################################
// ############################################################################

void test(int k,
          std::vector<std::vector<int> > rowConditions,
          std::vector<std::vector<int> > colConditions,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = buildMatrix(k, rowConditions, colConditions);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{1, 2}, {3, 2}}, {{2, 1}, {3, 2}},
            {{0, 0, 1}, {3, 0, 0}, {0, 2, 0}}, trials);
//            {{3, 0, 0}, {0, 0, 1}, {0, 2, 0}}, trials);
    test(3, {{1, 2}, {2, 3}, {3, 1}, {2, 3}}, {{2, 1}}, {}, trials);
    return 0;
}


