#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxNonDecreasingLength(std::vector<int> nums1, std::vector<int> nums2)
{
    int result = 1;
    for (int i = 1, dp[2] = {1, 1}, n = static_cast<int>(nums1.size()); i < n; ++i)
    {
        int m[2][2] = {{1 + (nums1[i - 1] <= nums1[i]) * dp[0],
                        1 + (nums1[i - 1] <= nums2[i]) * dp[0]},
                       {1 + (nums2[i - 1] <= nums1[i]) * dp[1],
                        1 + (nums2[i - 1] <= nums2[i]) * dp[1]}};
        dp[0] = std::max(m[0][0], m[1][0]);
        dp[1] = std::max(m[0][1], m[1][1]);
        result = std::max({result, dp[0], dp[1]});
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxNonDecreasingLength(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 1}, {1, 2, 1}, 2, trials);
    test({1, 3, 2, 1}, {2, 2, 3, 4}, 4, trials);
    test({1, 1}, {2, 2}, 2, trials);
    test({5, 16, 15}, {12, 1, 14}, 2, trials);
    return 0;
}


