#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxDistance(int side, std::vector<std::vector<int> > points, int k)
{
    const long n = static_cast<long>(points.size());
    std::vector<long> p;
    
    auto check = [&](long d)
    {
        for (long i = 0; i < n; ++i)
        {
            long j = i, count = 0;
            for (; count < k - 1; ++count)
            {
                auto end = std::lower_bound(p.begin() + j + 1,
                                            p.begin() + (i + n), p[j] + d);
                j = std::distance(p.begin(), end);
                if (j == i + n)
                    break;
            }
            if ((count == k - 1) && ((p[i] + 4ll * side) - p[j] >= d))
                return true;
        }
        return false;
    };
    auto binary_search_right = [&check](long l, long r)
    {
        while (l <= r)
        {
            long mid = (l + r) / 2;
            if (!check(mid)) r = mid - 1;
            else l = mid + 1;
        }
        return r;
    };
    
    for (const auto &x : points)
    {
        if      (x[0] == 0   ) p.push_back(0ll * side + x[1]);
        else if (x[1] == side) p.push_back(1ll * side + x[0]);
        else if (x[0] == side) p.push_back(2ll * side + (side - x[1]));
        else                   p.push_back(3ll * side + (side - x[0]));
    }
    std::sort(p.begin(), p.end());
    for (long i = 0; i < n; ++i) p.push_back(p[i] + 4ll * side);
    
    return static_cast<int>(binary_search_right(1, 4ll * side / k));
}

// ############################################################################
// ############################################################################

void test(int side,
          std::vector<std::vector<int> > points,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxDistance(side, points, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {{0, 2}, {2, 0}, {2, 2}, {0, 0}}, 4, 2, trials);
    test(2, {{0, 0}, {1, 2}, {2, 0}, {2, 2}, {2, 1}}, 4, 1, trials);
    test(2, {{0, 0}, {0, 1}, {0, 2}, {1, 2}, {2, 0}, {2, 2}, {2, 1}}, 5, 1, trials);
    return 0;
}


