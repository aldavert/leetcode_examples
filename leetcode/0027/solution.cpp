#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int removeElement(std::vector<int> &nums, int val)
{
    int n = static_cast<int>(nums.size());
    int k = 0;
    for (int i = 0; i < n; ++i)
        if (nums[i] != val)
            nums[k++] = nums[i];
    for (int i = n - k; i > 0; --i)
        nums.pop_back();
    return k;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int val,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    int ndifferent = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = nums;
        ndifferent = removeElement(result, val);
    }
    showResult((k == ndifferent) && (solution == result), solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 2, 3}, 3, 2, {2, 2}, trials);
    test({0, 1, 2, 2, 3, 0, 4, 2}, 2, 5, {0, 1, 3, 0, 4}, trials);
    return 0;
}


