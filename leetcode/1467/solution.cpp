#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
double getProbability(std::vector<int> balls)
{
    constexpr int BL = 8; // ball length;
    constexpr int BV = 6; // ball values;
    constexpr int MB = BL * BV;
    long C[MB + 1][MB + 1] = {};
    double f[BL + 1][MB + 1][BL * 2 + 1] = {};
    int n = static_cast<int>(balls.size());
    int sum = 0;
    for (int i = 0; i < n; ++i) sum += balls[i];
    int m = sum / 2;
    C[0][0] = 1;
    for (int i = 1; i <= sum; i++)
    {
        C[i][0] = 1;
        for (int j = 1; j <= i; ++j)
            C[i][j] = C[i - 1][j - 1] + C[i - 1][j];
    }
    f[0][0][n] = 1;
    int total = 0;
    for (int i = 0; i < n; i++)
    {
        int x = balls[i];
        for (int j = std::max(0, total - m); j <= std::min(total, m); j++)
        {
            for (int k = -n; k <= n; k++)
            {
                double p = 1.0 / static_cast<double>(C[sum - total][x]);
                if ((j + x <= m) && (k < n / 2))
                    f[i + 1][j + x][k + 1 + n] += f[i][j][k + n] * p
                                               * static_cast<double>(C[m - j][x]);
                for (int l = 1; l < x; l++)
                    if (j + l <= m)
                        f[i + 1][j + l][k + n] += f[i][j][k + n] * p
                                               *  static_cast<double>(C[m - j][l]
                                               *  C[m - (total - j)][x - l]);
                if (k > -n / 2)
                    f[i + 1][j][k - 1 + n] += f[i][j][k + n] * p
                                           *  static_cast<double>(C[m - (total - j)][x]);
            }
        }
        total += balls[i];
    }
    return f[n][m][0 + n];
}
#else
double getProbability(std::vector<int> balls)
{
    const int nb = static_cast<int>(balls.size());
    const int fact[] = {1, 1, 2, 6, 24, 120, 720};
    int n = 0;
    for (int i = 0; i < nb; ++i)
        n += balls[i];
    n /= 2;
    std::function<double(int, int, int, int, int, bool)> cases =
        [&](int i, int count_a, int count_b, int colors_a, int colors_b, bool equal_balls) -> double
    {
        if ((count_a > n) || (count_b > n)) return 0;
        if (i == nb) return equal_balls?1:(colors_a == colors_b);
        double result = 0;
        for (int taken_a = 0; taken_a <= balls[i]; ++taken_a)
        {
            int taken_b = balls[i] - taken_a,
                newcolors_a = colors_a + (taken_a > 0),
                newcolors_b = colors_b + (taken_b > 0);
            result += cases(i + 1, count_a + taken_a, count_b + taken_b,
                    newcolors_a, newcolors_b, equal_balls) /
                    (fact[taken_a] * fact[taken_b]);
        }
        return result;
    };
    return cases(0, 0, 0, 0, 0, false) / cases(0, 0, 0, 0, 0, true);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> balls, double solution, unsigned int trials = 1)
{
    double result = -1.0;
    for (unsigned int i = 0; i < trials; ++i)
        result = getProbability(balls);
    showResult(std::abs(solution - result) < 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1}, 1.00, trials);
    test({2, 1, 1}, 0.66667, trials);
    test({1, 2, 1, 2}, 0.60000, trials);
    return 0;
}


