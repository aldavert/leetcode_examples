#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minMoves(std::vector<int> nums, int limit)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> delta(limit * 2 + 2);
    int result = n;
    
    for (int i = 0; i < n / 2; ++i)
    {
        const int a = nums[i];
        const int b = nums[n - 1 - i];
        --delta[std::min(a, b) + 1];
        --delta[a + b];
        ++delta[a + b + 1];
        ++delta[std::max(a, b) + limit + 1];
    }
    for (int i = 2, moves = n; i <= limit * 2; ++i)
        result = std::min(result, moves += delta[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int limit, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minMoves(nums, limit);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 4, 3}, 4, 1, trials);
    test({1, 2, 2, 1}, 2, 2, trials);
    test({1, 2, 1, 2}, 2, 0, trials);
    return 0;
}


