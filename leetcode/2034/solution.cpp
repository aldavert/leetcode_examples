#include "../common/common.hpp"
#include <map>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class StockPrice
{
    std::map<int, int> m_timestamp_to_price;
    std::map<int, int> m_prices_count;
public:
    StockPrice(void) = default;
    void update(int timestamp, int price)
    {
        auto search = m_timestamp_to_price.find(timestamp);
        if (search != m_timestamp_to_price.end())
        {
            const int prev_price = search->second;
            if (--m_prices_count[prev_price] == 0)
                m_prices_count.erase(prev_price);
            search->second = price;
        }
        else m_timestamp_to_price[timestamp] = price;
        ++m_prices_count[price];
    }
    int current(void)
    {
        return m_timestamp_to_price.rbegin()->second;
    }
    int maximum(void)
    {
        return m_prices_count.rbegin()->first;
    }
    int minimum(void)
    {
        return m_prices_count.begin()->first;
    }
};

// ############################################################################
// ############################################################################

enum class OP { UPDATE, CURRENT, MAXIMUM, MINIMUM };

void test(std::vector<OP> operations,
          std::vector<std::vector<int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        StockPrice obj;
        for (size_t i = 0; i < operations.size(); ++i)
        {
            switch (operations[i])
            {
            case OP::UPDATE:
                obj.update(input[i][0], input[i][1]);
                result.push_back(null);
                break;
            case OP::CURRENT:
                result.push_back(obj.current());
                break;
            case OP::MAXIMUM:
                result.push_back(obj.maximum());
                break;
            case OP::MINIMUM:
                result.push_back(obj.minimum());
                break;
            default:
                std::cerr << "[ERROR] Unknown operation.\n";
                return;
            };
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::UPDATE, OP::UPDATE, OP::CURRENT, OP::MAXIMUM, OP::UPDATE,
          OP::MAXIMUM, OP::UPDATE, OP::MINIMUM},
         {{1, 10}, {2, 5}, {}, {}, {1, 3}, {}, {4, 2}, {}},
         {null, null, 5, 10, null, 5, null, 2}, trials);
    return 0;
}


