#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int smallestValue(int n)
{
    auto getPrimeSum = [](int value) -> int
    {
        int prime_sum = 0;
        for (int i = 2; i <= value; ++i)
        {
            while (value % i == 0)
            {
                value /= i;
                prime_sum += i;
            }
        }
        return prime_sum;
    };
    int prime_sum = getPrimeSum(n);
    while (n != prime_sum)
    {
        n = prime_sum;
        prime_sum = getPrimeSum(n);
    }
    return n;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestValue(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(15, 5, trials);
    test(3, 3, trials);
    return 0;
}


