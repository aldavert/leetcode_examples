#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> movesToStamp(std::string stamp, std::string target)
{
    if (stamp.size() == target.size())
    {
        if (stamp == target) return {0};
        else return {};
    }
    
    const size_t n = target.size();
    const size_t s = stamp.size();
    const size_t m = n - s + 1;
    std::vector<int> result;
    std::vector<bool> used(m, false), original(n, true);
    size_t number_of_changes = 0;
    auto check = [&](size_t position) -> size_t
    {
        const size_t e = position + s;
        size_t count = 0;
        for (size_t j = position; j < e; ++j)
        {
            if (original[j] && (target[j] != stamp[j - position]))
                return 0;
            count += original[j];
        }
        if (count)
            for (size_t i = position; i < e; ++i)
                original[i] = false;
        return count;
    };
    
    while (number_of_changes < n)
    {
        bool modification_applied = false;
        for (size_t i = 0; i < m; ++i)
        {
            if (used[i]) continue;
            size_t count = check(i);
            if (!count) continue;
            number_of_changes += count;
            modification_applied = used[i] = true;
            result.push_back(static_cast<int>(i));
        }
        if (!modification_applied) return {};
    }
    std::reverse(result.begin(), result.end());
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string stamp,
          std::string target,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    auto generate = [&](const std::vector<int> indexes) -> std::string
    {
        const size_t n = target.size();
        const size_t s = stamp.size();
        const int m = static_cast<int>(n - s + 1);
        std::string result(n, '*');
        for (int idx : indexes)
        {
            if (idx > m) return "";
            for (size_t i = idx, j = 0; j < s; ++i, ++j)
                result[i] = stamp[j];
        }
        return result;
    };
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = movesToStamp(stamp, target);
    showResult(generate(result) == target, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", "ababc", {0, 2}, trials);
    test("abca", "aabcaca", {3, 0, 1}, trials);
    return 0;
}


