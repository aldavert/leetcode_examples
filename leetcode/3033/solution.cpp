#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > modifiedMatrix(std::vector<std::vector<int> > matrix)
{
    const size_t m = matrix.size(), n = matrix.front().size();
    std::vector<std::vector<int> > result = matrix;
    std::vector<int> max_value = matrix.front();
    for (size_t i = 1; i < m; ++i)
        for (size_t j = 0; j < n; ++j)
            max_value[j] = std::max(max_value[j], matrix[i][j]);
    for (size_t i = 0; i < m; ++i)
        for (size_t j = 0; j < n; ++j)
            if (result[i][j] == -1)
                result[i][j] = max_value[j];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = modifiedMatrix(matrix);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, -1}, {4, -1, 6}, {7, 8, 9}}, {{1, 2, 9}, {4, 8, 6}, {7, 8, 9}}, trials);
    test({{3, -1}, {5, 2}}, {{3, 2}, {5, 2}}, trials);
    return 0;
}


