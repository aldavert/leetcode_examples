#include "../common/common.hpp"
#include <sstream>

// ############################################################################
// ############################################################################

int countAnagrams(std::string s)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(s.size());
    std::istringstream iss(s);
    
    std::vector<long> fact(n + 1), inv_fact(n + 1), inv(n + 1);
    fact[0] = inv_fact[0] = 1;
    inv[0] = inv[1] = 1;
    for (int i = 1; i <= n; ++i)
    {
        if (i >= 2) inv[i] = MOD - MOD / i * inv[MOD % i] % MOD;
        fact[i] = fact[i - 1] * i % MOD;
        inv_fact[i] = inv_fact[i - 1] * inv[i] % MOD;
    }
    
    long result = 1;
    for (std::string word; iss >> word;)
    {
        result = result * fact[word.length()] % MOD;
        int count[26] = {};
        for (char c : word) ++count[c - 'a'];
        for (int freq : count)
            result = result * inv_fact[freq] % MOD;
    }
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countAnagrams(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("too hot", 18, trials);
    test("aa", 1, trials);
    return 0;
}


