#include "../common/common.hpp"
#include "../common/tree.hpp"

std::vector<int> tovector(TreeNode * root)
{
    if (root == nullptr) return {};
    std::vector<int> result;
    std::queue<TreeNode *> active_nodes;
    active_nodes.push(root);
    result.push_back(root->val);
    while (!active_nodes.empty())
    {
        TreeNode * current = active_nodes.front();
        active_nodes.pop();
        if (current->right != nullptr)
        {
            active_nodes.push(current->right);
            result.push_back(current->right->val);
        }
        else result.push_back(null);
        if (current->left != nullptr)
        {
            active_nodes.push(current->left);
            result.push_back(current->left->val);
        }
        else result.push_back(null);
    }
    while (result.back() == null) result.pop_back();
    return result;
}

// ############################################################################
// ############################################################################

TreeNode * recoverFromPreorder(std::string traversal)
{
    const int n = static_cast<int>(traversal.size());
    int i = 0;
    auto recover = [&](auto &&self, int depth) -> TreeNode *
    {
        int n_dashes = 0;
        while ((i + n_dashes < n) && (traversal[i + n_dashes] == '-')) ++n_dashes;
        if (n_dashes != depth) return nullptr;
        
        i += depth;
        const int start = i;
        while ((i < n) && (std::isdigit(traversal[i]))) ++i;
        return new TreeNode(std::stoi(traversal.substr(start, i - start)),
                            self(self, depth + 1),
                            self(self, depth + 1));
    };
    return recover(recover, 0);
}

// ############################################################################
// ############################################################################

void test(std::string traversal, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = recoverFromPreorder(traversal);
        result = tovector(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1-2--3--4-5--6--7", {1, 2, 5, 3, 4, 6, 7}, trials);
    test("1-2--3---4-5--6---7", {1, 2, 5, 3, null, 6, null, 4, null, 7}, trials);
    test("1-401--349---90--88", {1, 401, null, 349, 88, 90}, trials);
    return 0;
}


