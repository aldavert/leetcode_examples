# Recover a Tree from Preorder Traversal

We run a preorder depth-first search (DFS) on the `root` of a binary tree.

At each node in this traversal, we output `D` dashes (where `D` is the depth of this node), then we output the value of this node.  If the depth of a node is `D`, the depth of its immediate child is `D + 1`.  The depth of the `root` node is `0`.

If a node has only one child, that child is guaranteed to be **the left child**.

Given the output `traversal` of this traversal, recover the tree and return *its* `root`.

#### Example 1:
> ```mermaid
> graph TD;
> A((1))---B((2))
> A---C((5))
> B---D((3))
> B---E((4))
> C---F((6))
> C---G((7))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `traversal = "1-2--3--4-5--6--7"`  
> *Output:* `[1, 2, 5, 3, 4, 6, 7]`

#### Example 2:
> ```mermaid
> graph TD;
> A((1))---B((2))
> A---C((5))
> B---D((3))
> B---EMPTY1(( ))
> C---E((6))
> C---EMPTY2(( ))
> D---F((4))
> D---EMPTY3(( ))
> E---G((7))
> E---EMPTY4(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY1,EMPTY2,EMPTY3,EMPTY4 empty;
> linkStyle 3,5,7,9 stroke-width:0px;
> ```
> *Input:* `traversal = "1-2--3---4-5--6---7"`  
> *Output:* `[1, 2, 5, 3, null, 6, null, 4, null, 7]`

#### Example 3:
> ```mermaid
> graph TD;
> A((1))---B((401))
> A---EMPTY1(( ))
> B---C((349))
> B---D((88))
> C---E((90))
> C---EMPTY2(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY1,EMPTY2 empty;
> linkStyle 1,5 stroke-width:0px;
> ```
> *Input:* `traversal = "1-401--349---90--88"`  
> *Output:* `[1, 401, null, 349, 88, 90]`

#### Constraints:
- The number of nodes in the original tree is in the range `[1, 1000]`.
- `1 <= Node.val <= 10^9`


