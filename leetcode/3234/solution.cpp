#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int numberOfSubstrings(std::string s)
{
    const int n = static_cast<int>(s.size());
    int total = 0, count = 0;
    std::vector<int> zeros, pre(n + 1);
    zeros.push_back(-1);
    for (int i = 0; i < n; ++i)
    {
        if (s[i] == '0')
        {
            total += (count * (count + 1)) / 2;
            count = 0;
            zeros.push_back(i);
        }
        else
        {
            pre[i] = 1;
            ++count;
        }
        if (i > 0) pre[i] += pre[i - 1];
    }
    pre[n] = pre[n - 1];
    zeros.push_back(n);
    total += (count * (count + 1)) / 2;
    for (int i = 1, m = static_cast<int>(zeros.size()); i + 1 < m; ++i)
    {
        int r = zeros[i + 1], tlen = r + 1;
        for (int j = i - 1; j >= 0; --j)
        {
            int l = zeros[j], len = r - l - 1, z = i - j, o = len - z, to = tlen - z;
            if (z * z > to) break;
            if (z * z > o) continue;
            int left = pre[zeros[j + 1]] - ((l == -1)?0:pre[l]);
            int right = pre[r] - pre[zeros[i]];
            int mid = pre[zeros[i]] - pre[zeros[j + 1]];
            if (int need = z * z - mid; need)
            {
                int sm = std::min(left, right), bi = std::max(left, right);
                count = 0;
                for (int k = std::max(0, need - bi); k <= sm; ++k)
                    count += std::min(bi + 1, (bi + k + 1 - need));
                total+= count;
            }
            else total += (left + 1) * (right + 1);
        }
    }
    return total;
}
#else
int numberOfSubstrings(std::string s)
{
    int result = 0;
    for (int zero = 0, n = static_cast<int>(s.size()); zero + zero * zero <= n; ++zero)
    {
        int last_invalid_pos = -1, count[2] = {};
        for (int l = 0, r = 0; r < n; ++r)
        {
            ++count[s[r] - '0'];
            for (; l < r; ++l)
            {
                if ((s[l] == '0') && (count[0] > zero))
                {
                    --count[0];
                    last_invalid_pos = l;
                }
                else if ((s[l] == '1') && (count[1] - 1 >= zero * zero))
                    --count[1];
                else break;
            }
            if ((count[0] == zero) && (count[1] >= zero * zero))
                result += l - last_invalid_pos;
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfSubstrings(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("00011", 5, trials);
    test("101101", 16, trials);
    return 0;
}


