#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

std::string smallestFromLeaf(TreeNode * root)
{
    auto traverse = [](TreeNode * root_node) -> std::string
    {
        std::string result, current;
        auto inner = [&](auto &&self, TreeNode * node) -> void
        {
            current += static_cast<char>('a' + node->val);
            if (node->left || node->right)
            {
                if (node->left ) self(self, node->left );
                if (node->right) self(self, node->right);
            }
            else
            {
                std::string candidate = current;
                std::reverse(candidate.begin(), candidate.end());
                if (result.empty()) result = candidate;
                else if (candidate < result) result = candidate;
            }
            current.pop_back();
        };
        inner(inner, root_node);
        return result;
    };
    return traverse(root);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = smallestFromLeaf(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 2, 3, 4, 3, 4}, "dba", trials);
    test({25, 1, 3, 1, 3, 0, 2}, "adz", trials);
    test({2, 2, 1, null, 1, 0, null, 0}, "abc", trials);
    return 0;
}


