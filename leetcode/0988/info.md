# Smallest String Starting from Leaf

You are given the root of a binary tree where each node has a value in the range `[0, 25]` representing the letters `'a'` to `'z'`.

Return *the* ***lexicographically smallest*** *string that starts at a leaf of this tree and ends at the root*.

As a reminder, any shorter prefix of a string is **lexicographically smaller**.
- For example, `"ab"` is lexicographically smaller than `"aba"`.

A leaf of a node is a node that has no children.

#### Example 1:
> ```mermaid 
> graph TD;
> A((a))---B((b))
> A---C((c))
> B---D((d))
> B---E((e))
> C---F((d))
> C---G((e))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#FA0,stroke:#500,stroke-width:3px;
> class A,B,D selected;
> ```
> *Input:* `root = [0, 1, 2, 3, 4, 3, 4]`  
> *Output:* `"dba"`

#### Example 2:
> ```mermaid 
> graph TD;
> A((z))---B((b))
> A---C((d))
> B---D((b))
> B---E((d))
> C---F((a))
> C---G((c))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#FA0,stroke:#500,stroke-width:3px;
> class A,C,F selected;
> ```
> *Input:* `root = [25, 1, 3, 1, 3, 0, 2]`  
> *Output:* `"adz"`

#### Example 3:
> ```mermaid 
> graph TD;
> A((c))---B((c))
> A---C((b))
> B---EMP1(( ))
> B---E((b))
> E---F((a))
> E---EMP2(( ))
> C---G((a))
> C---EMP3(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#FA0,stroke:#500,stroke-width:3px;
> classDef empty fill:none,stroke:none;
> class A,C,G selected;
> class EMP1,EMP2,EMP3 empty;
> linkStyle 2,5,7 stroke:none;
> ```
> *Input:* `root = [2, 2, 1, null, 1, 0, null, 0]`  
> *Output:* `"abc"`

#### Constraints:
- The number of nodes in the tree is in the range `[1, 8500]`.
- `0 <= Node.val <= 25`


