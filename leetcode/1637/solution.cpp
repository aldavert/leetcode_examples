#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxWidthOfVerticalArea(std::vector<std::vector<int> > points)
{
    const size_t n = points.size();
    std::vector<int> x_coordinates(n);
    for (size_t i = 0; i < n; ++i)
        x_coordinates[i] = points[i][0];
    std::sort(x_coordinates.begin(), x_coordinates.end());
    int result = 0;
    for (size_t i = 1; i < n; ++i)
        result = std::max(result, x_coordinates[i] - x_coordinates[i - 1]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxWidthOfVerticalArea(points);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{8, 7}, {9, 9}, {7, 4}, {9, 7}}, 1, trials);
    test({{3, 1}, {9, 0}, {1, 0}, {1, 4}, {5, 3}, {8, 8}}, 3, trials);
    return 0;
}


