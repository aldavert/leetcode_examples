# Widest Vertical Area Between Two Points Containing No Points

Given `n` `points` on a 2D plane where `points[i] = [x_i, y_i]`, Return *the* ***widest vertical area*** *between two points such that no points are inside the area*.

A **vertical area** is an area of fixed-width extending infinitely along the y-axis (i.e., infinite height). The **widest vertical area** is the one with the maximum width.

Note that points **on the edge** of a vertical area **are not** considered included in the area.

#### Example 1:
> ```
>     |         |XXX|///|
>  10 +         |XXX|///|
>     |         |XXX|///|
>   9 +         |XXX|///o
>     |         |XXX|///|
>   8 +         |XXX|///|
>     |         |XXX|///|
>   7 +         |XXXo///o
>     |         |XXX|///|
>   6 +         |XXX|///|
>     |         |XXX|///|
>   5 +         |XXX|///|
>     |         |XXX|///|
>   4 +         oXXX|///|
>     :         |XXX|///|
>     +..---+---+---+---+---+----
>           6   7   8   9   10
> ```
> *Input:* `points = [[8, 7], [9, 9], [7, 4], [9, 7]]`  
> *Output:* `1`  
> *Explanation:* Both the `XX` shaded area and the `//` shaded area are optimal.

#### Example 2:
> *Input:* `points = [[3, 1], [9, 0], [1, 0], [1, 4], [5, 3], [8, 8]]`  
> *Output:* `3`

#### Constraints:
- `n == points.length`
- `2 <= n <= 10^5`
- `points[i].length == 2`
- `0 <= x_i, y_i <= 10^9`


