#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<int> maxTargetNodes(std::vector<std::vector<int> > edges1,
                                std::vector<std::vector<int> > edges2,
                                int k)
{
    std::vector<std::vector<int> > tree1(edges1.size() + 1), tree2(edges2.size() + 1);
    for (const auto &edge : edges1)
    {
        tree1[edge[0]].push_back(edge[1]);
        tree1[edge[1]].push_back(edge[0]);
    }
    for (const auto &edge : edges2)
    {
        tree2[edge[0]].push_back(edge[1]);
        tree2[edge[1]].push_back(edge[0]);
    }
    auto reach =
        [](const std::vector<std::vector<int> > &tree, int node, int depth) -> int
    {
        std::vector<bool> visited(tree.size(), false);
        std::queue<int> q;
        q.push(node);
        int result = 0;
        for (; depth >= 0; --depth)
        {
            for (size_t i = 0, n = q.size(); i < n; ++i)
            {
                int current = q.front();
                q.pop();
                if (visited[current]) continue;
                visited[current] = true;
                ++result;
                for (int next : tree[current])
                {
                    if (visited[next]) continue;
                    q.push(next);
                }
            }
        }
        return result;
    };
    int max_second_tree = 0;
    for (int i = 0, n = static_cast<int>(edges2.size() + 1); i < n; ++i)
        max_second_tree = std::max(max_second_tree, reach(tree2, i, k - 1));
    std::vector<int> result(edges1.size() + 1);
    for (int i = 0, n = static_cast<int>(edges1.size() + 1); i < n; ++i)
        result[i] = reach(tree1, i, k) + max_second_tree;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges1,
          std::vector<std::vector<int> > edges2,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxTargetNodes(edges1, edges2, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {0, 2}, {2, 3}, {2, 4}},
         {{0, 1}, {0, 2}, {0, 3}, {2, 7}, {1, 4}, {4, 5}, {4, 6}},
         2, {9, 7, 9, 8, 8}, trials);
    test({{0, 1}, {0, 2}, {0, 3}, {0, 4}},
         {{0, 1}, {1, 2}, {2, 3}},
         1, {6, 3, 3, 3, 3}, trials);
    return 0;
}


