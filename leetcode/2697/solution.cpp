#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string makeSmallestPalindrome(std::string s)
{
    for (int l = 0, r = static_cast<int>(s.size()) - 1; l < r; ++l, --r)
    {
        if (s[l] != s[r])
        {
            if (s[l] < s[r]) s[r] = s[l];
            else s[l] = s[r];
        }
    }
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = makeSmallestPalindrome(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("egcfe", "efcfe", trials);
    test("abcd", "abba", trials);
    test("seven", "neven", trials);
    return 0;
}


