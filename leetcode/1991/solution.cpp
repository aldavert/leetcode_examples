#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findMiddleIndex(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int total = 0;
    for (int num : nums)
        total += num;
    for (int i = 0, left = 0; i < n; left += nums[i++])
        if (total - left - nums[i] == left)
            return i;
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMiddleIndex(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, -1, 8, 4}, 3, trials);
    test({1, -1, 4}, 2, trials);
    test({2, 5}, -1, trials);
    return 0;
}

