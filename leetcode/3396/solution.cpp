#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumOperations(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int histogram[101] = {}, repeated = 0;
    for (int i = 0; i < n; ++i)
        repeated += (++histogram[nums[i]] > 1);
    int result = 0;
    for (int i = 0; repeated > 0; ++result)
        for (int j = 0; (i < n) && (j < 3); ++i, ++j)
            repeated -= (histogram[nums[i]]-- > 1);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 2, 3, 3, 5, 7}, 2, trials);
    test({4, 5, 6, 4, 4}, 2, trials);
    test({6, 7, 8, 9}, 0, trials);
    test({2, 7, 15, 1, 15}, 1, trials);
    return 0;
}


