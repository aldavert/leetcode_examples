#include "../common/common.hpp"

// ############################################################################
// ############################################################################

double new21Game(int n, int k, int maxPts)
{
    if (k == 0 || n >= k - 1 + maxPts)
        return 1.0;
    
    double result = 0.0;
    std::vector<double> dp(n + 1);
    dp[0] = 1.0;
    double window_sum = dp[0];
    
    for (int i = 1; i <= n; ++i)
    {
        dp[i] = window_sum / maxPts;
        if (i < k) window_sum += dp[i];
        else result += dp[i];
        if (i - maxPts >= 0) window_sum -= dp[i - maxPts];
    }
    
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int k, int maxPts, double solution, unsigned int trials = 1)
{
    double result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = new21Game(n, k, maxPts);
    showResult(std::abs(solution - result) <= 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10,  1, 10, 1.00000, trials);
    test( 6,  1, 10, 0.60000, trials);
    test(21, 17, 10, 0.73278, trials);
    return 0;
}



