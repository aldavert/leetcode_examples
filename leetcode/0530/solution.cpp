#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 1
int getMinimumDifference(TreeNode * root)
{
    int result = std::numeric_limits<int>::max();
    int previous = -100'000;
    auto inorder = [&](auto &&self, TreeNode * ptr)
    {
        if (!ptr) return;
        self(self, ptr->left);
        result = std::min(result, ptr->val - previous);
        previous = ptr->val;
        self(self, ptr->right);
    };
    inorder(inorder, root);
    return result;
}
#else
int getMinimumDifference(TreeNode * root)
{
    int result = std::numeric_limits<int>::max();
    int prev = -1;
    std::stack<TreeNode *> stack;
    while (root || !stack.empty())
    {
        while (root)
        {
            stack.push(root);
            root = root->left;
        }
        root = stack.top(), stack.pop();
        if (prev >= 0)
            result = std::min(result, root->val - prev);
        prev = root->val;
        root = root->right;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = getMinimumDifference(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 6, 1, 3}, 1, trials);
    test({1, 0, 48, null, null, 12, 49}, 1, trials);
    test({236, 104, 701, null, 227, null, 911}, 9, trials);
    return 0;
}


