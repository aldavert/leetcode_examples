#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int carFleet(int target, std::vector<int> position, std::vector<int> speed)
{
    const int n = static_cast<int>(position.size());
    struct Car
    {
        int pos = 0;
        double time = 0;
        bool operator<(const Car &other) const { return pos > other.pos; }
    };
    int result = 0;
    std::vector<Car> cars(n);
    for (int i = 0; i < n; ++i)
        cars[i] = {position[i], static_cast<double>(target - position[i]) / speed[i]};
    std::sort(cars.begin(), cars.end());
    for (double max_time = 0; auto [_, time] : cars)
        if (time > max_time)
            max_time = time,
            ++result;
    return result;
}

// ############################################################################
// ############################################################################

void test(int target,
          std::vector<int> position,
          std::vector<int> speed,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = carFleet(target, position, speed);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(12, {10, 8, 0, 5, 3}, {2, 4, 1, 1, 3}, 3, trials);
    test(10, {3}, {3}, 1, trials);
    test(100, {0, 2, 4}, {4, 2, 1}, 1, trials);
    return 0;
}


