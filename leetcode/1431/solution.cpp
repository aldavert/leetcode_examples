#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<bool> kidsWithCandies(std::vector<int> candies, int extraCandies)
{
    int threshold = 0;
    for (int c : candies) threshold = std::max(threshold, c);
    threshold -= extraCandies;
    std::vector<bool> result;
    for (int c : candies) result.push_back(c >= threshold);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> candies,
          int extraCandies,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = kidsWithCandies(candies, extraCandies);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 5, 1, 3}, 3, {true, true, true, false, true}, trials);
    test({4, 2, 1, 1, 2}, 1, {true, false, false, false, false}, trials);
    test({12, 1, 12}, 1, {true, false, true}, trials);
    return 0;
}


