#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<std::string> > printTree(TreeNode * root)
{
    auto treeHeight = [&](auto &&self, TreeNode * node) -> int
    {
        if (!node) return 0;
        else return std::max(self(self, node->left), self(self, node->right)) + 1;
    };
    int height = treeHeight(treeHeight, root);
    std::vector<std::vector<std::string> > result;
    std::queue<TreeNode *> q;
    q.push(root);
    int max_width = (1 << height) - 1;
    for (int h = 0; h < height; ++h)
    {
        int space = max_width / (1 << h);
        int half_space = space / 2;
        result.push_back({});
        for (int i = 0; i < half_space; ++i) result.back().push_back("");
        for (size_t i = 0, n = q.size(); i < n; ++i)
        {
            if (i != 0)
                for (int s = 0; s < space; ++s) result.back().push_back("");
            TreeNode * node = q.front();
            q.pop();
            if (node)
            {
                result.back().push_back(std::to_string(node->val));
                q.push(node->left);
                q.push(node->right);
            }
            else
            {
                result.back().push_back("");
                q.push(nullptr);
                q.push(nullptr);
            }
        }
        for (int i = 0; i < half_space; ++i) result.back().push_back("");
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          std::vector<std::vector<std::string> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<std::string> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = printTree(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2},
         {{ "", "1",  ""},
          {"2",  "",  ""}}, trials);
    test({1, 2, 3, null, 4},
         {{ "",  "",  "", "1",  "",  "",  ""},
          { "", "2",  "",  "",  "", "3",  ""},
          { "",  "", "4",  "",  "",  "",  ""}}, trials);
    test({1, 2, 3, null, 4, null, null, 3, 9},
         {{ "", "", "", "", "", "", "","1", "", "", "", "", "", "", ""},
          { "", "", "","2", "", "", "", "", "", "", "","3", "", "", ""},
          { "", "", "", "", "","4", "", "", "", "", "", "", "", "", ""},
          { "", "", "", "","3", "","9", "", "", "", "", "", "", "", ""}}, trials);
    test({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
         {{  "",  "",  "",  "",  "",  "",  "", "1",  "",  "",  "",  "",  "",  "",  ""},
          {  "",  "",  "", "2",  "",  "",  "",  "",  "",  "",  "", "3",  "",  "",  ""},
          {  "", "4",  "",  "",  "", "5",  "",  "",  "", "6",  "",  "",  "", "7",  ""},
          { "8",  "", "9",  "","10",  "","11",  "","12",  "","13",  "","14",  "","15"}},
          trials);
    return 0;
}


