#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkMove(std::vector<std::vector<char> > board, int rMove, int cMove, char color)
{
    const std::vector<std::pair<int, int> > dirs{{-1, -1}, {-1, 0}, {-1, 1},
        {0, -1}, {0, 1},  {1, -1}, {1, 0},  {1, 1}};
    
    for (const auto &[dx, dy] : dirs)
    {
        int cells_count = 2;
        int i = rMove + dx;
        int j = cMove + dy;
        while ((0 <= i) && (i < 8) && (0 <= j) && (j < 8))
        {
            if (board[i][j] == '.') break;
            if ((cells_count == 2) && (board[i][j] == color))
                break;
            if (board[i][j] == color)
                return true;
            i += dx;
            j += dy;
            ++cells_count;
        }
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > board,
          int rMove,
          int cMove,
          char color,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkMove(board, rMove, cMove, color);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'.', '.', '.', 'B', '.', '.', '.', '.'},
          {'.', '.', '.', 'W', '.', '.', '.', '.'},
          {'.', '.', '.', 'W', '.', '.', '.', '.'},
          {'.', '.', '.', 'W', '.', '.', '.', '.'},
          {'W', 'B', 'B', '.', 'W', 'W', 'W', 'B'},
          {'.', '.', '.', 'B', '.', '.', '.', '.'},
          {'.', '.', '.', 'B', '.', '.', '.', '.'},
          {'.', '.', '.', 'W', '.', '.', '.', '.'}}, 4, 3, 'B', true, trials);
    test({{'.', '.', '.', '.', '.', '.', '.', '.'},
          {'.', 'B', '.', '.', 'W', '.', '.', '.'},
          {'.', '.', 'W', '.', '.', '.', '.', '.'},
          {'.', '.', '.', 'W', 'B', '.', '.', '.'},
          {'.', '.', '.', '.', '.', '.', '.', '.'},
          {'.', '.', '.', '.', 'B', 'W', '.', '.'},
          {'.', '.', '.', '.', '.', '.', 'W', '.'},
          {'.', '.', '.', '.', '.', '.', '.', 'B'}}, 4, 4, 'W', false, trials);
    return 0;
}


