# Check if Move is Legal

You are given a **0-indexed** `8 * 8` grid `board`, where `board[r][c]` represents the cell `(r, c)` on a game board. On the board, free cells are represented by `'.'`, white cells are represented by `'W'`, and black cells are represented by `'B'`.

Each move in this game consists of choosing a free cell and changing it to the color you are playing as (either white or black). However, a move is only **legal** if, after changing it, the cell becomes the **endpoint of a good line** (horizontal, vertical, or diagonal).

A **good line** is a line of **three or more cells (including the endpoints)** where the endpoints of the line are **one color**, and the remaining cells in the middle are the **opposite color** (no cells in the line are free). You can find examples for good lines in the figure below:
```
Good line with balck end points:
BWWB

God line with white end points:
W
B
W

No good lines:
WWBBB

Good diagonal line with black end points:
  B
 W
B

Good diagonal ine with white end points:
W
 B
  B
   W
```
Given two integers `rMove` and `cMove` and a character `color` representing the color you are playing as (white or black), return `true` *if changing cell* `(rMove, cMove)` *to color* `color` *is a* ***legal*** *move, or* `false` *if it is not legal*.

#### Example 1:
> ```
>    +- From B to X.
>    |
>    V
> ...B....
> ...W....
> ...W....
> ...W....
> WBBXWWWB <-- From B to X.
> ...B....
> ...B....
> ...W....
> ```
> *Input:* `board = [[".", ".", ".", "B", ".", ".", ".", "."], [".", ".", ".", "W", ".", ".", ".", "."], [".", ".", ".", "W", ".", ".", ".", "."], [".", ".", ".", "W", ".", ".", ".", "."], ["W", "B", "B", ".", "W", "W", "W", "B"], [".", ".", ".", "B", ".", ".", ".", "."], [".", ".", ".", "B", ".", ".", ".", "."], [".", ".", ".", "W", ".", ".", ".", "."]], rMove = 4, cMove = 3, color = "B"`  
> *Output:* `true`  
> *Explanation:* Cell (rMove, cMove) is represented with a `'X'` in the diagram. The two good lines with the chosen cell as an endpoint are highlighted with the arrows.

#### Example 2:
> ```
> ........
> .B..W...
> ..W.....
> ...WB...
> ....X...
> ....BW..
> ......W.
> .......B
> ```
> *Input:* `board = [[".", ".", ".", ".", ".", ".", ".", "."], [".", "B", ".", ".", "W", ".", ".", "."], [".", ".", "W", ".", ".", ".", ".", "."], [".", ".", ".", "W", "B", ".", ".", "."], [".", ".", ".", ".", ".", ".", ".", "."], [".", ".", ".", ".", "B", "W", ".", "."], [".", ".", ".", ".", ".", ".", "W", "."], [".", ".", ".", ".", ".", ".", ".", "B"]], rMove = 4, cMove = 4, color = "W"`  
> *Output:* `false`  
> *Explanation:* While there are good lines with the chosen cell as a middle cell, there are no good lines with the chosen cell as an endpoint.

#### Constraints:
- `board.length == board[r].length == 8`
- `0 <= rMove, cMove < 8`
- `board[rMove][cMove] == '.'`
- `color` is either `'B'` or `'W'`.


