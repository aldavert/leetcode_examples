#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 1
std::string makeGood(std::string s)
{
    auto keep = [](char left, char right) -> bool
    {
        if (std::islower(left) && std::isupper(right))
            return left != std::tolower(right);
        if (std::isupper(left) && std::islower(right))
            return std::tolower(left) != right;
        return true;
    };
    if (s.empty()) return s;
    std::stack<char> st;
    for (char c : s)
    {
        if (st.empty() || keep(st.top(), c))
            st.push(c);
        else st.pop();
    }
    std::string result(st.size(), ' ');
    for (; !st.empty(); st.pop())
        result[st.size() - 1] = st.top();
    return result;
}
#else
std::string makeGood(std::string s)
{
    auto skip = [](char left, char right) -> bool
    {
        if (std::islower(left) && std::isupper(right))
            return left == std::tolower(right);
        if (std::isupper(left) && std::islower(right))
            return std::tolower(left) == right;
        return false;
    };
    std::string result = s;
    do
    {
        s = result;
        const int n = static_cast<int>(s.size());
        result = "";
        int i = 0;
        while (i < n - 1)
        {
            if (skip(s[i], s[i + 1]))
                i += 2;
            else result += s[i++];
        }
        if (i < n) result += s[i];
    } while (result != s);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = makeGood(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leEeetcode", "leetcode", trials);
    test("abBAcC", "", trials);
    test("s", "s", trials);
    return 0;
}


