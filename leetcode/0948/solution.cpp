#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int bagOfTokensScore(std::vector<int> tokens, int power)
{
    std::sort(tokens.begin(), tokens.end());
    int left = 0, right = static_cast<int>(tokens.size()) - 1;
    int score = 0, max_score = 0;
    while (left <= right)
    {
        if (power >= tokens[left])
        {
            power -= tokens[left];
            ++score;
            max_score = std::max(max_score, score);
            ++left;
        }
        else if (score > 0)
        {
            power += tokens[right];
            --score;
            --right;
        }
        else break;
    }
    return max_score;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tokens, int power, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = bagOfTokensScore(tokens, power);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({100}, 50, 0, trials);
    test({100, 200}, 150, 1, trials);
    test({100, 200, 300, 400}, 200, 2, trials);
    return 0;
}


