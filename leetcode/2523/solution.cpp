#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> closestPrimes(int left, int right)
{
    std::vector<bool> is_prime(right + 1, true);
    is_prime[0] = is_prime[1] = false;
    for (int i = 2; i * i < right + 1; ++i)
        if (is_prime[i])
            for (int j = i * i; j < right + 1; j += i)
                is_prime[j] = false;
    std::vector<int> primes;
    
    for (int i = left; i <= right; ++i)
        if (is_prime[i])
            primes.push_back(i);
    if (primes.size() < 2)
        return {-1, -1};
    int min_diff = std::numeric_limits<int>::max(), num1 = -1, num2 = -1;
    for (int i = 1, n = static_cast<int>(primes.size()); i < n; ++i)
    {
        if (int diff = primes[i] - primes[i - 1]; diff < min_diff)
        {
            min_diff = diff;
            num1 = primes[i - 1];
            num2 = primes[i];
        }
    }
    return {num1, num2};
}

// ############################################################################
// ############################################################################

void test(int left, int right, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = closestPrimes(left, right);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, 19, {11, 13}, trials);
    test(4, 6, {-1, -1}, trials);
    return 0;
}


