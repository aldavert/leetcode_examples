#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int robotSim(std::vector<int> commands, std::vector<std::vector<int> > obstacles)
{
    struct PairHash
    {
        size_t operator()(const std::pair<int, int>& p) const
        {
            return p.first ^ p.second;
        }
    };
    constexpr int directions[] = {0, 1, 0, -1, 0};
    int result = 0, dir = 0, x = 0, y = 0;
    std::unordered_set<std::pair<int, int>, PairHash> obstacles_set;
    for (const auto &obstacle : obstacles)
        obstacles_set.insert({obstacle[0], obstacle[1]});
    for (int c : commands)
    {
        if      (c == -1) dir = (dir + 1) % 4;
        else if (c == -2) dir = (dir + 3) % 4;
        else
        {
            for (int step = 0; step < c; ++step)
            {
                x += directions[dir];
                y += directions[dir + 1];
                if (obstacles_set.find({x, y}) != obstacles_set.end())
                {
                    x -= directions[dir];
                    y -= directions[dir + 1];
                    break;
                }
            }
        }
        result = std::max(result, x * x + y * y);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> commands,
          std::vector<std::vector<int> > obstacles,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = robotSim(commands, obstacles);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, -1, 3}, {}, 25, trials);
    test({4, -1, 4, -2, 4}, {{2, 4}}, 65, trials);
    test({6, -1, -1, 6}, {}, 36, trials);
    return 0;
}


