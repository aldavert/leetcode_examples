#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findMaxFish(std::vector<std::vector<int> > grid)
{
    const int n = static_cast<int>(grid.size()),
              m = static_cast<int>(grid[0].size());
    auto dfs = [&](auto &&self, int i, int j) -> int
    {
        if ((i < 0) || (i == n) || (j < 0) || (j == m)) return 0;
        if (grid[i][j] == 0) return 0;
        int caught_fish = grid[i][j];
        grid[i][j] = 0;
        return caught_fish + self(self, i + 1, j) + self(self, i - 1, j)
                           + self(self, i, j + 1) + self(self, i, j - 1);
    };
    int result = 0;
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < m; ++j)
            if (grid[i][j] > 0)
                result = std::max(result, dfs(dfs, i, j));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMaxFish(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 2, 1, 0}, {4, 0, 0, 3}, {1, 0, 0, 4}, {0, 3, 2, 0}}, 7, trials);
    test({{1, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 1}}, 1, trials);
    return 0;
}


