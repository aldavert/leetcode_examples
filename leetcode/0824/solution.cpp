#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string toGoatLatin(std::string sentence)
{
    const size_t n = sentence.size();
    std::string result;
    size_t begin = 0, end = 0, number_of_words = 0;
    while (begin < n)
    {
        while ((begin < n) && (sentence[begin] == ' '))
        {
            result += " ";
            ++begin;
        }
        end = begin + 1;
        while ((end < n) && (sentence[end] != ' '))
            ++end;
        if (begin < n)
        {
            std::string word = sentence.substr(begin, end - begin);
            ++number_of_words;
            if ((word[0] != 'a') && (word[0] != 'e') && (word[0] != 'i')
            &&  (word[0] != 'o') && (word[0] != 'u') && (word[0] != 'A')
            &&  (word[0] != 'E') && (word[0] != 'I') && (word[0] != 'O')
            &&  (word[0] != 'U'))
                word = word.substr(1, word.size() - 1) + word[0];
            word += "ma";
            for (size_t i = 0; i < number_of_words; ++i)
                word += "a";
            result += word;
        }
        begin = end;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string sentence, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = toGoatLatin(sentence);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("I speak Goat Latin", "Imaa peaksmaaa oatGmaaaa atinLmaaaaa", trials);
    test("The quick brown fox jumped over the lazy dog", "heTmaa uickqmaaa rownbmaaaa oxfmaaaaa umpedjmaaaaaa overmaaaaaaa hetmaaaaaaaa azylmaaaaaaaaa ogdmaaaaaaaaaa", trials);
    return 0;
}

