#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> nums1, std::vector<int> nums2)
{
    const int n = static_cast<int>(nums1.size()),
              last_min = std::min(nums1.back(), nums2.back()),
              last_max = std::max(nums1.back(), nums2.back());
    int dp1 = 0, dp2 = 0;
    for (int i = 0; i < n; ++i)
    {
        const int a = nums1[i], b = nums2[i];
        if ((std::min(a, b) > last_min)
        ||  (std::max(a, b) > last_max)) return -1;
        dp1 += ((a > nums1.back()) || (b > nums2.back()));
        dp2 += ((a > nums2.back()) || (b > nums1.back()));
    }
    return std::min(dp1, dp2);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 7}, {4, 5, 3}, 1, trials);
    test({2, 3, 4, 5, 9}, {8, 8, 4, 4, 4}, 2, trials);
    test({1, 5, 4}, {2, 5, 3}, -1, trials);
    return 0;
}


