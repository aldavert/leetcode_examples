#include "../common/common.hpp"

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class Fancy
{
public:
    Fancy(void) : m_op{1, 0}
    {
    }
    void append(int val)
    {
        int a = submod(val, m_op.second, MOD),
            b = powmod(m_op.first, MOD - 2, MOD);
        m_array.push_back(mulmod(a, b, MOD));
    }
    void addAll(int inc)
    {
        m_op.second = addmod(m_op.second, inc, MOD);
    }
    void multAll(int m)
    {
        m_op.first = mulmod(m_op.first, m, MOD);
        m_op.second = mulmod(m_op.second, m, MOD);
    }
    int getIndex(int idx)
    {
        if (idx >= static_cast<int>(m_array.size()))
            return -1;
        return addmod(mulmod(m_array[idx], m_op.first, MOD), m_op.second, MOD);
    }
protected:
    unsigned int addmod(unsigned int a, unsigned int b, unsigned int mod)
    {
        return a + b - (mod - a <= b) * mod;
    }
    unsigned int submod(unsigned int a, unsigned int b, unsigned int mod)
    {
        return addmod(a, mod - b, mod);
    }
    unsigned int mulmod(unsigned int a, unsigned int b, unsigned int mod)
    {
        return static_cast<unsigned int>(static_cast<unsigned long>(a) * b % mod);
    }
    unsigned int powmod(unsigned int a, unsigned int b, unsigned int mod)
    {
        a %= mod;
        unsigned int result = 1;
        while (b)
        {
            if (b & 1)
                result = mulmod(result, a, mod);
            a = mulmod(a, a, mod);
            b >>= 1;
        }
        return result;
    }
    static constexpr int MOD = 1'000'000'007;
    std::vector<int> m_array;
    std::pair<int, int> m_op;
};

// ############################################################################
// ############################################################################

enum class OP { APPEND, ADD, MULT, INDEX };

void test(std::vector<OP> operations,
          std::vector<int> input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        Fancy object;
        result.clear();
        
        for (size_t i = 0, n = operations.size(); i < n; ++i)
        {
            if      (operations[i] == OP::APPEND)
            {
                object.append(input[i]);
                result.push_back(null);
            }
            else if (operations[i] == OP::ADD)
            {
                object.addAll(input[i]);
                result.push_back(null);
            }
            else if (operations[i] == OP::MULT)
            {
                object.multAll(input[i]);
                result.push_back(null);
            }
            else if (operations[i] == OP::INDEX)
            {
                result.push_back(object.getIndex(input[i]));
            }
            else
            {
                std::cerr << "[ERROR] UNKNOWN INPUT OPERATION\n";
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::APPEND, OP::ADD, OP::APPEND, OP::MULT, OP::INDEX, OP::ADD,
          OP::APPEND, OP::MULT, OP::INDEX, OP::INDEX, OP::INDEX},
         {2, 3, 7, 2, 0, 3, 10, 2, 0, 1, 2},
         {null, null, null, null, 10, null, null, null, 26, 34, 20}, trials);
    return 0;
}



