#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int shortestMatchingSubstring(std::string s, std::string p)
{
    constexpr int INF = std::numeric_limits<int>::max();
    const int n = static_cast<int>(s.size());
    std::string part[3];
    for (int idx = 0; char symbol : p)
    {
        if (symbol == '*') ++idx;
        else part[idx] += symbol;
    }
    auto calc = [&](std::string current)
    {
        std::string z_txt = current + '#' + s;
        std::vector<int> z(z_txt.size());
        int l = 0, r = 0, m = static_cast<int>(z_txt.size());
        for (int i = 1; i < m; ++i)
        {
            if (i <= r)
                z[i] = std::min(r - i + 1, z[i - l]);
            while ((i + z[i] < m)
               &&  (z_txt[z[i]] == z_txt[i + z[i]]))
                ++z[i];
            if (i + z[i] - 1 > r)
                l = i,
                r = i + z[i] - 1;
        }
        std::vector<int> result;
        for (int i = 0; i < n; ++i)
            if (z[i + current.size() + 1] == static_cast<int>(current.size()))
                result.push_back(i);
        if (current.size() == 0)
            result.push_back(n);
        return result;
    };
    auto l = calc(part[0]), c = calc(part[1]), r = calc(part[2]);
    int best = INF;
    for (int i : l)
    {
        int current = i + static_cast<int>(part[0].size());
        int idx = (int)(std::lower_bound(c.begin(), c.end(), current) - c.begin());
        if (idx < static_cast<int>(c.size()))
            current = c[idx];
        else continue;
        current += static_cast<int>(part[1].size());
        idx = (int)(std::lower_bound(r.begin(), r.end(), current) - r.begin());
        if (idx < static_cast<int>(r.size()))
            current = r[idx];
        else continue;
        best = std::min(best, current + static_cast<int>(part[2].size()) - i);
    }
    return (best < INF)?best:-1;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string p, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestMatchingSubstring(s, p);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abaacbaecebce", "ba*c*ce", 8, trials);
    test("ba*c*ce", "cc*baa*adb", -1, trials);
    test("a", "**", 0, trials);
    test("madlogic", "*adlogi*", 6, trials);
    return 0;
}


