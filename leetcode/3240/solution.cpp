#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minFlips(std::vector<std::vector<int> > grid)
{
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    int result = 0, middle_ones = 0, mismatched_pairs = 0;
    
    for (int i = 0; i < m / 2; ++i)
    {
        for (int j = 0; j < n / 2; ++j)
        {
            const int ones = grid[i][j] + grid[i][n - 1 - j] + grid[m - 1 - i][j] +
            grid[m - 1 - i][n - 1 - j];
            result += std::min(ones, 4 - ones);
        }
    }
    if (m % 2 == 1)
    {
        for (int j = 0; j < n / 2; ++j)
        {
            const int left_cell = grid[m / 2][j];
            const int right_cell = grid[m / 2][n - 1 - j];
            mismatched_pairs += left_cell ^ right_cell;
            middle_ones += left_cell + right_cell;
        }
    }
    if (n % 2 == 1)
    {
        for (int i = 0; i < m / 2; ++i)
        {
            const int top_cell = grid[i][n / 2];
            const int bottom_cell = grid[m - 1 - i][n / 2];
            mismatched_pairs += top_cell ^ bottom_cell;
            middle_ones += top_cell + bottom_cell;
        }
    }
    if (mismatched_pairs == 0)
        result += 2 * (middle_ones % 4 == 2);
    else result += mismatched_pairs;
    if ((m % 2 == 1) && (n % 2 == 1))
        result += grid[m / 2][n / 2];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minFlips(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0, 0}, {0, 1, 0}, {0, 0, 1}}, 3, trials);
    test({{0, 1}, {0, 1}, {0, 0}}, 2, trials);
    test({{1}, {1}}, 2, trials);
    return 0;
}


