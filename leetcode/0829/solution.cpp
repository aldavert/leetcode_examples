#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int consecutiveNumbersSum(int n)
{
    int result = 0;
    for (int i = 1; n > 0; n -= i, ++i)
        result += (n % i) == 0;
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = consecutiveNumbersSum(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 2, trials);
    test(9, 3, trials);
    test(15, 4, trials);
    return 0;
}


