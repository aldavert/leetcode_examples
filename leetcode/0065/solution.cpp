#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isNumber(std::string s)
{
    // VALID:=(NUMBER|INTEGER)[<e|E>INTEGER]
    // NUMBER:=[<+|->]((DIGIT)+.(DIGIT)*|.DIGIT+)
    // INTEGER:=[<+|->]DIGIT
    // DIGIT:=(<0|1|2|..|9>)+
    const int n = static_cast<int>(s.size());
    bool valid = false, decimal = false, exponent = false;
    for (int p = (s[0] == '+' || s[0] == '-'); p < n; ++p)
    {
        if (s[p] == '.')
        {
            if (decimal) return false;
            decimal = true;
        }
        else
        {
            if ((s[p] >= '0') && (s[p] <= '9'))
                valid = true;
            else if ((s[p] == 'e') || (s[p] == 'E'))
            {
                if (!valid) return false;
                if (exponent) return false;
                decimal = true;
                exponent = true;
                valid = false;
                if ((p + 1 < n) && ((s[p + 1] == '+') || (s[p + 1] == '-')))
                    ++p;
            }
            else return false;
        }
    }
    return valid;
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isNumber(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("2", true, trials);
    test("0089", true, trials);
    test("-0.1", true, trials);
    test("+3.14", true, trials);
    test("4.", true, trials);
    test("-.9", true, trials);
    test("2e10", true, trials);
    test("-90E3", true, trials);
    test("3e+7", true, trials);
    test("+6e-1", true, trials);
    test("53.5e93", true, trials);
    test("-123.456e789", true, trials);
    test("abc", false, trials);
    test("1a", false, trials);
    test("1e", false, trials);
    test("e3", false, trials);
    test("99e2.5", false, trials);
    test("--6", false, trials);
    test("-+3", false, trials);
    test("95a54e53", false, trials);
    test("0", true, trials);
    test("e", false, trials);
    test(".", false, trials);
    test(".1", true, trials);
    return 0;
}


