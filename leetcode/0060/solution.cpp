#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string getPermutation(int n, int k)
{
    int permutations = 1;
    for (int i = 2; i <= n; ++i)
        permutations *= i;
    std::string result(n, ' ');
    bool available[10];
    for (int i = 0; i < n; ++i) available[i] = true;
    for (int i = n; i > 0; --i)
    {
        int partition_size = permutations / i;
        int offset = (k - 1) / partition_size;
        int idx, count;
        for (idx = 0, count = -1; idx < n; ++idx)
        {
            if (available[idx])
            {
                ++count;
                if (count == offset)
                    break;
            }
        }
        available[idx] = false;
        result[n - i] = static_cast<char>('1' + idx);
        
        permutations /= i;
        k -= offset * partition_size;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getPermutation(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 3, "213", trials);
    test(4, 9, "2314", trials);
    test(3, 1, "123", trials);
    
    return 0;
}


