#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool divideArray(std::vector<int> nums)
{
    int histogram[501] = {};
    for (int number : nums) ++histogram[number];
    for (int i = 0; i <= 500; ++i) if (histogram[i] & 1) return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = divideArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 3, 2, 2, 2}, true, trials);
    test({1, 2, 3, 4}, false, trials);
    test({9, 9, 19, 10, 9, 12, 2, 12, 3, 3, 11, 5, 8, 4, 13, 6, 2, 11, 9, 19,
          11, 15, 9, 17, 15, 12, 5, 14, 12, 16, 18, 16, 10, 3, 8, 9, 16, 20, 2,
          4, 16, 12, 11, 14, 20, 16, 2, 18, 17, 20, 3, 13, 16, 17, 1, 1, 11,
          20, 20, 4}, false, trials);
    return 0;
}


