#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int paintWalls(std::vector<int> cost, std::vector<int> time)
{
    constexpr int MAX = 500'000'000;
    const int n = static_cast<int>(cost.size());
    int dp[502];
    for (int i = 1; i <= n; ++i) dp[i] = MAX;
    dp[0] = 0;
    for (int i = 0; i < n; ++i)
        for (int walls = n; walls > 0; --walls)
            dp[walls] = std::min(dp[walls], dp[std::max(walls - time[i] - 1, 0)]
                      + cost[i]);
    return dp[n];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> cost,
          std::vector<int> time,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = paintWalls(cost, time);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 2}, {1, 2, 3, 2}, 3, trials);
    test({2, 3, 4, 2}, {1, 1, 1, 1}, 4, trials);
    return 0;
}


