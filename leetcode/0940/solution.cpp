#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int distinctSubseqII(std::string s)
{
    constexpr int MOD = 1'000'000'007;
    long ends[26] = {};
    for (const char c : s)
        ends[c - 'a'] = std::accumulate(&ends[0], &ends[26], 1L) % MOD;
    return static_cast<int>(std::accumulate(&ends[0], &ends[26], 0L) % MOD);
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = distinctSubseqII(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", 7, trials);
    test("aba", 6, trials);
    test("aaa", 3, trials);
    return 0;
}


