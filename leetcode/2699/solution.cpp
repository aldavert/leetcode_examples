#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> >
modifiedGraphEdges(int n,
                   std::vector<std::vector<int> > edges,
                   int source,
                   int destination,
                   int target)
{
    struct Info
    {
        int node = 0;
        int distance = 0;
        bool operator<(const Info &other) const
        {
            return (distance > other.distance)
                || ((distance == other.distance) && (node == other.node));
        }
    };
    constexpr int MAX = 2'000'000'000;
    std::vector<Info> graph[101];
    int distance[101] = {};
    auto dijkstra = [&](int src, int dst) -> int
    {
        std::priority_queue<Info> heap;
        
        for (int i = 0; i < n; ++i)
            distance[i] = std::numeric_limits<int>::max();
        distance[src] = 0;
        heap.push({src, distance[src]});
        while (!heap.empty())
        {
            auto [node, dist] = heap.top();
            heap.pop();
            if (dist > target) continue;
            if (node == destination) break;
            for (auto [next, weight] : graph[node])
            {
                if (dist + weight < distance[next])
                {
                    distance[next] = dist + weight;
                    heap.push({next, distance[next]});
                }
            }
        }
        return distance[dst];
    };
    
    std::vector<int> edges_of_interest;
    const int m = static_cast<int>(edges.size());
    for (int i = 0; i < m; ++i)
    {
        if (edges[i][2] == -1) edges_of_interest.push_back(i);
        else
        {
            graph[edges[i][0]].push_back({edges[i][1], edges[i][2]});
            graph[edges[i][1]].push_back({edges[i][0], edges[i][2]});
        }
    }
    int distance_to_destination = dijkstra(source, destination);
    if (distance_to_destination < target) return {};
    if (distance_to_destination == target)
    {
        for (auto &edge : edges)
            if (edge[2] == -1)
                edge[2] = MAX;
        return edges;
    }
    for (int i : edges_of_interest)
    {
        const int u = edges[i][0];
        const int v = edges[i][1];
        int &w = edges[i][2];
        if (w != -1) continue;
        w = 1;
        graph[u].push_back({v, 1});
        graph[v].push_back({u, 1});
        distance_to_destination = dijkstra(source, destination);
        if (distance_to_destination <= target)
        {
            w += target - distance_to_destination;
            for (int j = i + 1; j < m; ++j)
                if (edges[j][2] == -1)
                    edges[j][2] = MAX;
            return edges;
        }
    }
    return {};
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    std::set<std::tuple<int, int> > edges;
    int target = 0;
    for (const auto &edge : left)
    {
        edges.insert({edge[0], edge[1]});
        target += edge[2];
    }
    for (const auto &edge : right)
    {
        if (auto search = edges.find({edge[0], edge[1]}); search != edges.end())
            edges.erase(search);
        else return false;
        target -= edge[2];
    }
    return edges.empty() && (target == 0);
}

void test(int n,
          std::vector<std::vector<int> > edges,
          int source,
          int destination,
          int target,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = modifiedGraphEdges(n, edges, source, destination, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{4, 1, -1}, {2, 0, -1}, {0, 3, -1}, {4, 3, -1}}, 0, 1, 5,
         {{4, 1, 1}, {2, 0, 1}, {0, 3, 3}, {4, 3, 1}}, trials);
    test(3, {{0, 1, -1}, {0, 2, 5}}, 0, 2, 6, {}, trials);
    test(4, {{1, 0, 4}, {1, 2, 3}, {2, 3, 5}, {0, 3, -1}}, 0, 2, 6,
         {{1, 0, 4}, {1, 2, 3}, {2, 3, 5}, {0, 3, 1}}, trials);
    return 0;
}


