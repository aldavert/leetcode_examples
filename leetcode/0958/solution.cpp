#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

bool isCompleteTree(TreeNode * root)
{
    std::queue<TreeNode *> q;
    q.push(root);
    for (bool null_node = false; !q.empty(); )
    {
        TreeNode * current = q.front();
        q.pop();
        if (current)
        {
            if (null_node) return false;
            q.push(current->left);
            q.push(current->right);
        }
        else null_node = true;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = isCompleteTree(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6}, true, trials);
    test({1, 2, 3, 4, 5, null, 7}, false, trials);
    return 0;
}


