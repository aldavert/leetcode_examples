#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
long long goodTriplets(std::vector<int> nums1, std::vector<int> nums2)
{
    const int m = static_cast<int>(nums1.size());
    constexpr int static n = 100'000;
    int bt[n + 1] = {};
    std::vector<int> ids(m);
    long result = 0;
    auto prefixSum = [&](int i) -> int
    {
        int sum = 0;
        for (i = i + 1; i > 0; i -= i & (-i)) sum += bt[i];
        return sum;
    };
    auto add = [&](int i, int val) -> void
    {
        for (i = i + 1; i <= n; i += i & (-i)) bt[i] += val;
    };
    
    for (int i = 0; i < m; ++i) ids[nums2[i]] = i;
    for (int i = 0; i < m - 1; ++i)
    {
        int mid = ids[nums1[i]], sm = prefixSum(mid), gr = m - 1 - mid - (i - sm);
        result += static_cast<long>(sm) * gr;
        add(mid, 1);
    }
    return result;
}
#else
long long goodTriplets(std::vector<int> nums1, std::vector<int> nums2)
{
    struct FenwickTree
    {
    public:
        FenwickTree(int n) : m_sums(n + 1) {}
        void update(int i, int delta)
        {
            while (i < static_cast<int>(m_sums.size()))
                m_sums[i] += delta,
                i += lowbit(i);
        }
        int get(int i) const
        {
            int sum = 0;
            while (i > 0)
                sum += m_sums[i],
                i -= lowbit(i);
            return sum;
        }
    private:
        std::vector<int> m_sums;
        inline int lowbit(int i) const { return i & -i; }
    };
    const int n = static_cast<int>(nums1.size());
    std::vector<int> index_of_num2_in_num1, left_smaller(n), right_larger(n);
    std::unordered_map<int, int> num_to_index;
    FenwickTree tree1(n), tree2(n);
    long result = 0;
    
    for (int i = 0; i < n; ++i) num_to_index[nums1[i]] = i;
    for (int num : nums2) index_of_num2_in_num1.push_back(num_to_index[num]);
    for (int i = 0; i < n; ++i)
    {
        left_smaller[i] = tree1.get(index_of_num2_in_num1[i]);
        tree1.update(index_of_num2_in_num1[i] + 1, 1);
    }
    for (int i = n - 1; i >= 0; --i)
    {
        right_larger[i] = tree2.get(n) - tree2.get(index_of_num2_in_num1[i]);
        tree2.update(index_of_num2_in_num1[i] + 1, 1);
    }
    for (int i = 0; i < n; ++i)
        result += static_cast<long>(left_smaller[i]) * right_larger[i];
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = goodTriplets(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 0, 1, 3}, {0, 1, 2, 3}, 1, trials);
    test({4, 0, 1, 3, 2}, {4, 1, 0, 2, 3}, 4, trials);
    return 0;
}


