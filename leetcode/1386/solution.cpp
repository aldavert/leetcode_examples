#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int maxNumberOfFamilies(int n, std::vector<std::vector<int> > reservedSeats)
{
    std::unordered_map<int, int> row_to_seats;
    int result = 0;
    
    for (const auto &reserved_seat : reservedSeats)
        row_to_seats[reserved_seat[0]] |= 1 << (reserved_seat[1] - 1);
    for (const auto& [_, seats] : row_to_seats)
    {
        if ((seats & 0b0111111110) == 0)
            result += 2;
        else if (((seats & 0b0111100000) == 0)
             ||  ((seats & 0b0001111000) == 0)
             ||  ((seats & 0b0000011110) == 0))
            result += 1;
    }
    return result + (n - static_cast<int>(row_to_seats.size())) * 2;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > reservedSeats,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxNumberOfFamilies(n, reservedSeats);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{1, 2}, {1, 3}, {1, 8}, {2, 6}, {3, 1}, {3, 10}}, 4, trials);
    test(2, {{2, 1}, {1, 8}, {2, 6}}, 2, trials);
    test(4, {{4, 3}, {1, 4}, {4, 6}, {1, 7}}, 4, trials);
    return 0;
}


