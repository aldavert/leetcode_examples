#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int shortestPathLength(std::vector<std::vector<int> > graph)
{
    const int n = static_cast<int>(graph.size());
    const int target = (1 << n) - 1;
    std::queue<std::pair<int, int> > q;
    std::vector<std::vector<bool> > visited(n, std::vector<bool>(target + 1));
    for (int i = 0; i < n; ++i)
        q.emplace(i, 1 << i);
    for (int result = 0; !q.empty(); ++result)
    {
        for (int size = static_cast<int>(q.size()); size > 0; --size)
        {
            const auto [u, state] = q.front();
            q.pop();
            if (state == target) return result;
            if (visited[u][state]) continue;
            visited[u][state] = true;
            for (const int v : graph[u])
                q.emplace(v, state | (1 << v));
        }
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > graph,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestPathLength(graph);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {0}, {0}, {0}}, 4, trials);
    test({{1}, {0, 2, 4}, {1, 3, 4}, {2}, {1, 2}}, 4, trials);
    return 0;
}


