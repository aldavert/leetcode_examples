# Shortest Path Visiting All Nodes

You have an undirected, connected graph of `n` nodes labeled from `0` to `n - 1`. You are given an array `graph` where `graph[i]` is a list of all the nodes connected with node `i` by an edge.

Return *the length of the shortest path that visits every node*. You may start and stop at any node, you may revisit nodes multiple times, and you may reuse edges.

#### Example 1:
> ```mermaid 
> graph TD;
> A((0))
> B((1))
> C((2))
> D((3))
> A-.-B
> A-->C
> A-.-C
> A-.-D
> B-->A
> C-->A
> A-->D
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> linkStyle 0,2,3 stroke-width:2px,stroke:black;
> linkStyle default stroke-width:1px,stroke:#44F,fill:none;
> ```
> *Input:* `graph = [[1, 2, 3], [0], [0], [0]]`  
> *Output:* `4`  
> *Explanation:* One possible path is `[1, 0, 2, 0, 3]`

#### Example 2:
> ```mermaid 
> graph LR;
> A((0))
> B((1))
> C((2))
> D((3))
> E((4))
> C-.-B
> B-.-A
> B-.-E
> C-.-E
> C-.-D
> A-->B
> B-->E
> E-->C
> C-->D
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> linkStyle 0,1,2,3,4 stroke-width:2px,stroke:black;
> linkStyle default stroke-width:1px,stroke:#44F,fill:none;
> ```
> *Input:* `graph = [[1], [0, 2, 4], [1, 3, 4], [2], [1, 2]]`  
> *Output:* `4`  
> *Explanation:* One possible path is `[0, 1, 4, 2, 3]`
 
#### Constraints:
- `n == graph.length`
- `1 <= n <= 12`
- `0 <= graph[i].length < n`
- `graph[i]` does not contain `i`.
- If `graph[a]` contains `b`, then `graph[b]` contains `a`.
- The input graph is always connected.


