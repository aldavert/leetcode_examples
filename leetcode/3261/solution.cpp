#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<long long>
countKConstraintSubstrings(std::string s,
                           int k,
                           std::vector<std::vector<int> > queries)
{
    const int n = static_cast<int>(s.size());
    std::vector<long long> result;
    std::vector<int> left_to_right(n), right_to_left(n);
    std::vector<long> prefix{0};
    int count[2] = {};
    
    for (int l = 0, r = 0; r < n; ++r)
    {
        ++count[s[r] - '0'];
        while ((count[0] > k) && (count[1] > k)) --count[s[l++] - '0'];
        right_to_left[r] = l;
    }
    count[0] = count[1] = 0;
    for (int l = n - 1, r = n - 1; l >= 0; --l)
    {
        ++count[s[l] - '0'];
        while ((count[0] > k) && (count[1] > k)) --count[s[r--] - '0'];
        left_to_right[l] = r;
    }
    for (int r = 0; r < n; ++r)
        prefix.push_back(prefix.back() + r - right_to_left[r] + 1);
    for (const auto &query : queries)
    {
        const int l = query[0], r = query[1];
        long num_valid_substrings = 0;
        if (r > left_to_right[l])
        {
            const int sz = left_to_right[l] - l + 1;
            num_valid_substrings =
                (sz * (sz + 1)) / 2 + (prefix[r + 1] - prefix[left_to_right[l] + 1]);
        }
        else
        {
            const int sz = r - l + 1;
            num_valid_substrings = (sz * static_cast<long>(sz + 1)) / 2;
        }
        result.push_back(num_valid_substrings);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          int k,
          std::vector<std::vector<int> > queries,
          std::vector<long long> solution, unsigned int trials = 1)
{
    std::vector<long long> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countKConstraintSubstrings(s, k, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("0001111", 2, {{0, 6}}, {26}, trials);
    test("010101", 1, {{0, 5}, {1, 4}, {2, 3}}, {15, 9, 3}, trials);
    return 0;
}


