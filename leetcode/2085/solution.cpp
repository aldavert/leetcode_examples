#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int countWords(std::vector<std::string> words1, std::vector<std::string> words2)
{
    std::unordered_map<std::string, size_t> histogram1, histogram2;
    for (std::string word : words1) ++histogram1[word];
    for (std::string word : words2) ++histogram2[word];
    int result = 0;
    for (auto [word, frequency] : histogram1)
    {
        if (frequency == 1)
        {
            auto search = histogram2.find(word);
            result += ((search != histogram2.end()) && (search->second == 1));
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words1,
          std::vector<std::string> words2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countWords(words1, words2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"leetcode", "is", "amazing", "as", "is"},
         {"amazing", "leetcode", "is"}, 2, trials);
    test({"b", "bb", "bbb"}, {"a", "aa", "aaa"}, 0, trials);
    test({"a", "ab"}, {"a", "a", "a", "ab"}, 1, trials);
    return 0;
}


