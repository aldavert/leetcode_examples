#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int countKReducibleNumbers(std::string s, int k)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(s.size());
    std::vector<std::vector<long> > C(n + 1, std::vector<long>(n + 1, 0));
    std::vector<int> f(n + 1, 0);
    long result = 0;
    
    C[0][0] = 1;
    for (int i = 1; i <= n; ++i)
    {
        C[i][0] = 1;
        for (int j = 1; j <= i; ++j)
            C[i][j] = (C[i - 1][j - 1] + C[i - 1][j]) % MOD;
    }
    for (int i = 1; i <n; ++i)
    {
        f[i] = f[__builtin_popcount(i)] + 1;
        if (f[i] <= k)
        {
            long count = 0;
            for (int j = 0, left1s = i; (j < n) && (left1s >= 0); ++j)
                if (s[j] == '1')
                    count = (count + C[n - 1 - j][left1s--]) % MOD;
            result = (count + result) % MOD;
        }
    }
    return static_cast<int>(result);
}
#else
int countKReducibleNumbers(std::string s, int k)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(s.size());
    std::vector<std::vector<std::vector<int> > > mem(n,
        std::vector<std::vector<int> >(n + 1, std::vector<int>(2, -1)));
    std::vector<int> ops(n + 1);
    for (int num = 2; num <= n; ++num)
        ops[num] = 1 + ops[__builtin_popcount(num)];
    auto count = [&](auto &&self, int i, int set_bits, bool is_tight) -> int
    {
        if (i == n) return (ops[set_bits] < k) && !is_tight;
        if (mem[i][set_bits][is_tight] != -1)
            return mem[i][set_bits][is_tight];
        int result = 0;
        const int max_digit = (is_tight)?s[i] - '0':1;
        for (int d = 0; d <= max_digit; ++d)
        {
            const bool next_is_tight = is_tight && (d == max_digit);
            result += self(self, i + 1, set_bits + d, next_is_tight);
            result %= MOD;
        }
        return mem[i][set_bits][is_tight] = result;
    };
    return count(count, 0, 0, true) - 1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countKReducibleNumbers(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("111", 1, 3, trials);
    test("1000", 2, 6, trials);
    test("1", 3, 0, trials);
    return 0;
}


