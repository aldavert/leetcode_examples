#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minSwapsCouples(std::vector<int> row)
{
    const int n = static_cast<int>(row.size()) / 2;
    int count = n, id[100], rank[100] = {};
    for (int i = 0; i < n; ++i) id[i] = i;
    auto find = [&](auto &&self, int index) -> int
    {
        return (id[index] == index)?index:id[index] = self(self, id[index]);
    };
    auto unionByRank = [&](int u, int v) -> void
    {
        const int i = find(find, u);
        const int j = find(find, v);
        if (i == j) return;
        if      (rank[i] < rank[j]) id[i] = id[j];
        else if (rank[i] > rank[j]) id[j] = id[i];
        else                        id[i] = id[j], ++rank[j];
        --count;
    };
    for (int i = 0; i < n; ++i)
        unionByRank(row[2 * i] / 2, row[2 * i + 1] / 2);
    return n - count;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> row, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSwapsCouples(row);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 2, 1, 3}, 1, trials);
    test({3, 2, 0, 1}, 0, trials);
    return 0;
}


