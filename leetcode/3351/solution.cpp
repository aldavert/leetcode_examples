#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int sumOfGoodSubsequences(std::vector<int> nums)
{
    constexpr int MOD = 1'000'000'007;
    const int max_num = *std::max_element(nums.begin(), nums.end());
    std::vector<long> ends_in(max_num + 3), dp(max_num + 3);
    
    for (int num : nums)
    {
        const long seqs_to_append = 1 + ends_in[num] + ends_in[num + 2];
        dp[num + 1] = (seqs_to_append * num
                    + (dp[num + 1] + dp[num] + dp[num + 2])) % MOD;
        ends_in[num + 1] = (ends_in[num + 1] + seqs_to_append) % MOD;
    }
    auto func = [&](int subtotal, int d) { return (subtotal + d) % MOD; };
    return std::accumulate(dp.begin(), dp.end(), 0, func);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOfGoodSubsequences(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1}, 14, trials);
    test({3, 4, 5}, 40, trials);
    return 0;
}


