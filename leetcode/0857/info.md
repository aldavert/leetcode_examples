# Minimum Cost to Hire K Workers

There are `n` workers. You are given two integer arrays quality and wage where `quality[i]` is the quality of the `i^th` worker and `wage[i]` is the minimum wage expectation for the `i^th` worker.

We want to hire exactly `k` workers to form a paid group. To hire a group of `k` workers, we must pay them according to the following rules:
1. Every worker in the paid group must be paid **at least their minimum wage expectation**.
2. Every worker in the paid group should be paid in the ratio of their quality compared to other workers in the paid group. This means, that the `wage / quality` ratio between all selected workers must be the same. Since the salary cannot be lowered as per the first condition, all salaries must be raised to meet the largest `wage / quality` ratio in the group. For example, given `quality = [10, 5]` and `wage = [70, 30]` results in `ratio = [7.0, 6.0]`. To equalize both ratios, we have to rise the wage of worker `1` from `30` to `35` so its `wage / quality` ratio also becomes `7`.

Given the integer `k`, return *the least amount of money needed to form a paid group satisfying the above conditions*. Answers within `10^-5` of the actual answer will be accepted.

#### Example 1:
> *Input:* `quality = [10, 20, 5], wage = [70, 50, 30], k = 2`  
> *Output:* `105.00000`  
> *Explanation:* We pay `70` to 0th worker and `35` to 2on worker.

#### Example 2:
> *Input:* `quality = [3, 1, 10, 10, 1], wage = [4, 8, 2, 2, 7], k = 3`  
> *Output:* `30.66667`  
> *Explanation:* The selected workers are 0th, 2on and 3rd. 0th worker has a `wage / quality` ratio of `1.3333` while 2on and 3rd workers both have a ratio of `0.2`. So we have to raise 2on and 3rd salaries to match 0th ratio. New salaries are obtained by multiplying the worker's quality (`10` for 2on and 3rd workers) by the `1.3333` (the highest `wage / quality` of the group). Therefore, we pay `4` to 0th worker, `13.33333` to 2nd and 3rd workers separately.

#### Constraints:
- `n == quality.length == wage.length`
- `1 <= k <= n <= 10^4`
- `1 <= quality[i], wage[i] <= 10^4`


