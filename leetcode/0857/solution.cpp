#include "../common/common.hpp"
#include <limits>
#include <queue>

// ############################################################################
// ############################################################################

double mincostToHireWorkers(std::vector<int> quality,
                            std::vector<int> wage,
                            int k)
{
    struct Worker
    {
        Worker(int _quality, int _wage) :
            ratio(static_cast<double>(_wage) / static_cast<double>(_quality)),
            quality(_quality) {}
        double ratio;
        int quality;
        bool operator<(const Worker &other) const
        {
            return  (ratio < other.ratio)
                || ((ratio == other.ratio) && (quality < other.quality));
        }
    };
    std::vector<Worker> workers;
    for (size_t i = 0, n = quality.size(); i < n; ++i)
        workers.push_back({quality[i], wage[i]});
    std::sort(workers.begin(), workers.end());
    std::priority_queue<int> k_smallest_quality;
    k_smallest_quality.push(100'000);
    double result = std::numeric_limits<double>::max();
    for (int quality_sum = 100'000; auto [current_ratio, current_quality] : workers)
    {
        k_smallest_quality.push(current_quality);
        quality_sum += current_quality;
        if (static_cast<int>(k_smallest_quality.size()) > k)
            quality_sum -= k_smallest_quality.top(),
            result = std::min(result, current_ratio * quality_sum),
            k_smallest_quality.pop();
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> quality,
          std::vector<int> wage,
          int k,
          double solution,
          unsigned int trials = 1)
{
    double result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = mincostToHireWorkers(quality, wage, k);
    showResult(std::abs(solution - result) < 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 20, 5}, {70, 50, 30}, 2, 105.00000, trials);
    test({3, 1, 10, 10, 1}, {4, 8, 2, 2, 7}, 3, 30.66667, trials);
    return 0;
}


