#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int finalPositionOfSnake(int n, std::vector<std::string> commands)
{
    int x = 0, y = 0;
    for (const auto &command : commands)
    {
        if      (command == "UP") --y;
        else if (command == "DOWN") ++y;
        else if (command == "LEFT") --x;
        else if (command == "RIGHT") ++x;
    }
    return y * n + x;
}
#else
int finalPositionOfSnake(int n, std::vector<std::string> commands)
{
    const std::unordered_map<std::string, std::pair<int, int> > directions = {
        {"UP", {-1, 0}}, {"RIGHT", {0, 1}}, {"DOWN", {1, 0}}, {"LEFT", {0, -1}}};
    int i = 0, j = 0;
    for (const auto &command : commands)
    {
        const auto& [dx, dy] = directions.at(command);
        i += dx;
        j += dy;
    }
    return i * n + j;
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::string> commands,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = finalPositionOfSnake(n, commands);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {"RIGHT", "DOWN"}, 3, trials);
    test(3, {"DOWN", "RIGHT", "UP"}, 1, trials);
    return 0;
}


