#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

#if 1
int largestPalindrome(int n)
{
    if (n == 1) return 9;
    const int mod = 1337;
    const int upper = static_cast<int>(std::pow(10, n)) - 1;
    for(long x = 1; x <= upper; ++x)
    {
        long a = static_cast<long>(std::pow(10, n)) - x;
        std::string reversed = std::to_string(a);
        std::reverse(reversed.begin(), reversed.end());
        long b = std::stol(reversed);
        long temp = x * x - 4 * b;
        if (temp < 0) continue;
        if (long temp1 = static_cast<long>(std::sqrt(temp));
                (temp1 * temp1 == temp) && ((x + temp1) % 2 == 0))
            return static_cast<int>((static_cast<long>(std::pow(10, n)) * a + b) % mod);
    }
    return -1;
}
#else
int largestPalindrome(int n)
{
    if (n == 1) return 9;
    const int mod = 1337;
    const int upper = static_cast<int>(std::pow(10, n)) - 1;
    const int lower = static_cast<int>(std::pow(10, n - 1)) - 1;
    for (int i = upper; i > lower; --i)
    {
        std::string reversed = std::to_string(i);
        std::reverse(reversed.begin(), reversed.end());
        const long putative = std::stol(std::to_string(i) + reversed);
        for (long j = upper; j * j >= putative; --j)
            if (putative % j == 0)
                return static_cast<int>(putative % mod);
    }
    return -1;
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestPalindrome(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 987, trials);
    test(1, 9, trials);
    return 0;
}


