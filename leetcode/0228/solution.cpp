#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> summaryRanges(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<std::string> result;
    if (n > 0)
    {
        int begin, end;
        auto update = [&](void)
        {
            if (begin != end - 1)
                result.push_back(std::to_string(nums[begin])
                                 + "->"
                                 + std::to_string(nums[end - 1]));
            else result.push_back(std::to_string(nums[begin]));
        };
        for (begin = 0, end = 1; end < n; ++end)
        {
            if (nums[end - 1] + 1 != nums[end])
            {
                update();
                begin = end;
            }
        }
        update();
    }
    return result;
}
// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = summaryRanges(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 2, 4, 5, 7}, {"0->2", "4->5", "7"}, trials);
    test({0, 2, 3, 4, 6, 8, 9}, {"0", "2->4", "6", "8->9"}, trials);
    return 0;
}


