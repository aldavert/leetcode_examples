#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> replaceElements(std::vector<int> arr)
{
    std::vector<int> result(arr.size());
    for (int i = static_cast<int>(arr.size()) - 1, largest = -1; i >= 0; --i)
    {
        result[i] = largest;
        largest = std::max(largest, arr[i]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = replaceElements(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({17, 18, 5, 4, 6, 1}, {18, 6, 6, 6, 1, -1}, trials);
    test({400}, {-1}, trials);
    return 0;
}


