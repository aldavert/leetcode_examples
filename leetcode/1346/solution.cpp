#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkIfExist(std::vector<int> arr)
{
    std::vector<bool> exist(4'002, false);
    int zeros = 0;
    for (int a : arr)
    {
        exist[a + 2000] = true;
        zeros += a == 0;
    }
    for (int a : arr)
        if (((a != 0) && exist[2 * a + 2000])
        ||  ((a == 0) && (zeros > 1)))
            return true;
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkIfExist(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 2, 5, 3}, true, trials);
    test({3, 1, 7, 11}, false, trials);
    test({-2, 0, 10, -19, 4, 6, -8}, false, trials);
    return 0;
}


