#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countSpecialSubsequences(std::vector<int> nums)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(nums.size());
    long dp[3] = {};
    
    if (nums[0] == 0) dp[0] = 1;
    for (int i = 1; i < n; ++i)
    {
        if      (nums[i] == 0) dp[0] = dp[0] * 2 + 1;
        else if (nums[i] == 1) dp[1] = dp[1] * 2 + dp[0];
        else                   dp[2] = dp[2] * 2 + dp[1];
        dp[0] %= MOD, dp[1] %= MOD, dp[2] %= MOD;
    }
    
    return static_cast<int>(dp[2]);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSpecialSubsequences(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 2, 2}, 3, trials);
    test({2, 2, 0, 0}, 0, trials);
    test({0, 1, 2, 0, 1, 2}, 7, trials);
    return 0;
}



