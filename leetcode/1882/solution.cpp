#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################



std::vector<int> assignTasks(std::vector<int> servers, std::vector<int> tasks)
{
    struct Data
    {
        Data(int w, int i, int f) : weight(w), index(i), free_time(f) {}
        int weight = -1;
        int index = -1;
        int free_time = -1;
    };
    const int n = static_cast<int>(servers.size()),
              m = static_cast<int>(tasks.size());
    std::vector<int> result(m);
    auto cmpFree = [](const Data &a, const Data &b) -> bool
    {
        return (a.weight == b.weight)?(a.index > b.index):(a.weight > b.weight);
    };
    auto cmpUsed = [](const Data &a, const Data &b) -> bool
    {
        if (a.free_time != b.free_time)
            return a.free_time > b.free_time;
        if (a.weight != b.weight)
            return a.weight > b.weight;
        return a.index > b.index;
    };
    std::priority_queue<Data, std::vector<Data>, decltype(cmpFree)> free(cmpFree);
    std::priority_queue<Data, std::vector<Data>, decltype(cmpUsed)> used(cmpUsed);
    
    for (int i = 0; i < n; ++i)
        free.emplace(servers[i], i, 0);
    for (int i = 0; i < m; ++i)
    {
        const int execution_time = tasks[i];
        while (!used.empty() && (used.top().free_time <= i))
        {
            free.push(used.top());
            used.pop();
        }
        if (free.empty())
        {
            Data server = used.top();
            used.pop();
            result[i] = server.index;
            server.free_time += execution_time;
            used.push(server);
        }
        else
        {
            Data server = free.top();
            free.pop();
            result[i] = server.index;
            server.free_time = i + execution_time;
            used.push(server);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> servers,
          std::vector<int> tasks,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = assignTasks(servers, tasks);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 3, 2}, {1, 2, 3, 2, 1, 2}, {2, 2, 0, 2, 1, 2}, trials);
    test({5, 1, 4, 3, 2}, {2, 1, 2, 4, 5, 2, 1}, {1, 4, 1, 4, 1, 3, 2}, trials);
    return 0;
}


