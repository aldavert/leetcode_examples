#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxAbsValExpr(std::vector<int> arr1, std::vector<int> arr2)
{
    const int n = static_cast<int>(arr1.size());
    auto diff = [&](const std::vector<int> &values) -> int
    {
        int min = values[0], max = values[0];
        for (int i = 1; i < n; ++i)
        {
            min = std::min(min, values[i]);
            max = std::max(max, values[i]);
        }
        return max - min;
    };
    std::vector<int> a(n), b(n), c(n), d(n);
    for (int i = 0; i < n; ++i)
    {
        a[i] = arr1[i] + arr2[i] + i;
        b[i] = arr1[i] + arr2[i] - i;
        c[i] = arr1[i] - arr2[i] + i;
        d[i] = arr1[i] - arr2[i] - i;
    }
    return std::max(std::max(diff(a), diff(b)), std::max(diff(c), diff(d)));
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr1,
          std::vector<int> arr2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxAbsValExpr(arr1, arr2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, {-1, 4, 5, 6}, 13, trials);
    test({1, -2, -5, 0, 10}, {0, -2, -1, -7, -4}, 20, trials);
    return 0;
}


