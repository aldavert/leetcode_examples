#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findRedundantConnection(std::vector<std::vector<int> > &edges)
{
    auto search = [](const int * lut, int idx)
    {
        while (lut[idx] != idx) idx = lut[idx];
        return idx;
    };
    const int n = static_cast<int>(edges.size());
    int lut[1'001] = {};
    for (int i = 0; i <= n; ++i) lut[i] = i;
    for (auto edge : edges)
    {
        int idx_a = search(lut, edge[0]);
        int idx_b = search(lut, edge[1]);
        if (idx_a == idx_b) return edge; // Loop detected.
        lut[idx_a] = idx_b;
    }
    return {};
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findRedundantConnection(edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {1, 3}, {2, 3}}, {2, 3}, trials);
    test({{1, 2}, {2, 3}, {3, 4}, {1, 4}, {1, 5}}, {1, 4}, trials);
    return 0;
}


