#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minOrAfterOperations(std::vector<int> nums, int k)
{
    constexpr int MAXBIT = 30;
    int result = 0, prefix_mask = 0;
    auto mergeOps = [&](void) -> int
    {
        int merge_ops = 0;
        for (int ands = prefix_mask; const int num : nums)
        {
            ands &= num;
            if ((ands | result) == result)
                ands = prefix_mask;
            else ++merge_ops;
        }
        return merge_ops;
    };
    for (int i = MAXBIT; i >= 0; --i)
    {
        prefix_mask |= 1 << i;
        if (mergeOps() > k)
            result |= 1 << i;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOrAfterOperations(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 5, 3, 2, 7}, 2, 3, trials);
    test({7, 3, 15, 14, 2, 8}, 4, 2, trials);
    test({10, 7, 10, 3, 9, 14, 9, 4}, 1, 15, trials);
    return 0;
}


