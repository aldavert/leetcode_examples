#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<long long> sumOfThree(long long num)
{
    if (num % 3) return {};
    long x = num / 3;
    return {x - 1, x, x + 1};
}

// ############################################################################
// ############################################################################

void test(long long num, std::vector<long long> solution, unsigned int trials = 1)
{
    std::vector<long long> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOfThree(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(33, {10, 11, 12}, trials);
    test(4, {}, trials);
    return 0;
}


