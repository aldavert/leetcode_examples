#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

class KthLargest
{
    int m_k;
    std::priority_queue<int, std::vector<int>, std::greater<int> > m_queue;
public:
    KthLargest(int k, const std::vector<int> &nums) :
        m_k(k)
    {
        const int n = static_cast<int>(nums.size());
        if (n <= k)
        {
            for (int i : nums)
                m_queue.push(i);
        }
        else
        {
            for (int i = 0; i < k; ++i)
                m_queue.push(nums[i]);
            for (int i = k; i < n; ++i)
            {
                if (nums[i] > m_queue.top())
                {
                    m_queue.pop();
                    m_queue.push(nums[i]);
                }
            }
        }
    }
    int add(int val)
    {
        if (static_cast<int>(m_queue.size()) < m_k)
            m_queue.push(val);
        else if (val > m_queue.top())
        {
            m_queue.pop();
            m_queue.push(val);
        }
        return m_queue.top();
    }
};

// ############################################################################
// ############################################################################

void test(int k,
          std::vector<int> nums_init,
          std::vector<int> nums_add,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    const int n = static_cast<int>(solution.size());
    std::vector<int> result(n);
    for (unsigned int i = 0; i < trials; ++i)
    {
        KthLargest obj(k, nums_init);
        for (int j = 0; j < n; ++j)
            result[j] = obj.add(nums_add[j]);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {4, 5, 8, 2}, {3, 5, 10, 9, 4}, {4, 5, 5, 8, 8}, trials);
    return 0;
}


