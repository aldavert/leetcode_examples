#include "../common/common.hpp"

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class NumArray
{
    std::vector<int> histogram;
public:
    NumArray(const std::vector<int> &nums) : histogram(nums.size() + 1)
    {
        const int n = static_cast<int>(nums.size());
        histogram[0] = 0;
        for (int i = 0; i < n; ++i)
            histogram[i + 1] = histogram[i] + nums[i];
    }
    int sumRange(int left, int right)
    {
        return histogram[right + 1] - histogram[left];
    }
};

// ############################################################################
// ############################################################################

enum class OP { NUMARRAY, SUMRANGE };

void test(std::vector<OP> operations,
          std::vector<std::vector<int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    const int n = static_cast<int>(operations.size());
    if (static_cast<int>(input.size()) != n)
    {
        std::cout << "[FAILURE] Input and operations must have the same length.\n";
        return;
    }
    if (static_cast<int>(solution.size()) != n)
    {
        std::cout << "[FAILURE] Solution and operations must have the same length.\n";
        return;
    }
    for (unsigned int t = 0; t < trials; ++t)
    {
        NumArray * array = nullptr;
        for (int i = 0; i < n; ++i)
        {
            switch (operations[i])
            {
            case OP::NUMARRAY:
                array = new NumArray(input[i]);
                break;
            case OP::SUMRANGE:
            {
                int result = array->sumRange(input[i][0], input[i][1]);
                showResult(solution[i] == result, solution[i], result);
                break;
            }
            default:
                std::cout << "[FAILURE] Unknown operation.\n";
                return;
            }
        }
        delete array;
    }
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::NUMARRAY         , OP::SUMRANGE, OP::SUMRANGE, OP::SUMRANGE},
         {{-2, 0, 3, -5, 2, -1}, {0, 2}      , {2, 5}      , {0, 5}      },
         {null                 , 1           , -1          , -3          }, trials);
    return 0;
}


