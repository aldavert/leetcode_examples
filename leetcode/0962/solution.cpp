#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

int maxWidthRamp(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::stack<int> stack;
    int result = 0;
    
    for (int i = 0; i < n; ++i)
        if (stack.empty() || (nums[i] < nums[stack.top()]))
            stack.push(i);
    for (int i = n - 1; i > result; --i)
        while (!stack.empty() && (nums[i] >= nums[stack.top()]))
            result = std::max(result, i - stack.top()),
            stack.pop();
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxWidthRamp(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({6, 0, 8, 2, 1, 5}, 4, trials);
    test({9, 8, 1, 0, 1, 9, 4, 0, 4, 1}, 7, trials);
    return 0;
}


