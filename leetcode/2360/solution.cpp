#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestCycle(std::vector<int> edges)
{
    const int n = static_cast<int>(edges.size());
    int result = -1;
    std::vector<int> time_visited(n);
    for (int i = 0, time = 1; i < n; ++i)
    {
        if (time_visited[i]) continue;
        const int start_time = time;
        int u = i;
        while ((u != -1) && !time_visited[u])
            time_visited[u] = time++,
            u = edges[u];
        if ((u != -1) && (time_visited[u] >= start_time))
            result = std::max(result, time - time_visited[u]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> edges, int solution, unsigned int trials = 1)
{
    int result = -2;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestCycle(edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 3, 4, 2, 3}, 3, trials);
    test({1, 3, 4, 0, 3}, 3, trials);
    test({2, -1, 3, 1}, -1, trials);
    return 0;
}


