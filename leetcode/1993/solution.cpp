#include "../common/common.hpp"

// ############################################################################
// ############################################################################

class LockingTree
{
    struct Node
    {
        std::vector<int> children;
        int locked_by = -1;
    };
    const std::vector<int> m_parent;
    std::vector<Node> m_nodes;
    bool anyLockedDescendant(int i)
    {
        if (m_nodes[i].locked_by != -1)
            return true;
        for (int child : m_nodes[i].children)
            if (anyLockedDescendant(child))
                return true;
        return false;
    }
    void unlockDescendants(int i)
    {
        m_nodes[i].locked_by = -1;
        for (int child : m_nodes[i].children)
            unlockDescendants(child);
    }
public:
    LockingTree(std::vector<int> &parent) : m_parent(parent), m_nodes(parent.size())
    {
        for (int i = 1, n = static_cast<int>(parent.size()); i < n; ++i)
            m_nodes[parent[i]].children.push_back(i);
    }
    bool lock(int num, int user)
    {
        if (m_nodes[num].locked_by != -1)
            return false;
        return (m_nodes[num].locked_by = user);
    }
    bool unlock(int num, int user)
    {
        if (m_nodes[num].locked_by != user)
            return false;
        return (m_nodes[num].locked_by = -1);
    }
    bool upgrade(int num, int user)
    {
        if (m_nodes[num].locked_by != -1)
            return false;
        if (!anyLockedDescendant(num))
            return false;
        for (int i = num; i != -1; i = m_parent[i])
            if (m_nodes[i].locked_by != -1)
                return false;
        unlockDescendants(num);
        return (m_nodes[num].locked_by = user);
    }
};

// ############################################################################
// ############################################################################

enum class OP { LOCK, UNLOCK, UPGRADE };

void test(std::vector<int> parent,
          std::vector<OP> operation,
          std::vector<std::pair<int, int> > parameters,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        LockingTree object(parent);
        result.clear();
        for (size_t i = 0; i < operation.size(); ++i)
        {
            auto [num, user] = parameters[i];
            switch (operation[i])
            {
            case OP::LOCK:
                result.push_back(object.lock(num, user));
                break;
            case OP::UNLOCK:
                result.push_back(object.unlock(num, user));
                break;
            case OP::UPGRADE:
                result.push_back(object.upgrade(num, user));
                break;
            default:
                std::cerr << "[ERROR] Unknown operation.\n";
                return;
            };
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, 0, 0, 1, 1, 2, 2},
         {OP::LOCK, OP::UNLOCK, OP::UNLOCK, OP::LOCK, OP::UPGRADE, OP::LOCK},
         {{2, 2}, {2, 3}, {2, 2}, {4, 5}, {0, 1}, {0, 1}},
         {true, false, true, true, true, false}, trials);
    return 0;
}


