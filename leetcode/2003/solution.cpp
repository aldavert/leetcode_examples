#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> smallestMissingValueSubtree(std::vector<int> parents,
                                             std::vector<int> nums)
{
    const int n = static_cast<int>(parents.size()),
              m = static_cast<int>(nums.size());
    std::vector<int> result(n, 1);
    std::vector<std::vector<int> > tree(n);
    std::vector<bool> seen(100'000);
    int min_miss = 1;
    auto getNode = [&](void)
    {
        for (int i = 0; i < m; ++i)
            if (nums[i] == 1)
                return i;
        return -1;
    };
    auto dfs = [&](auto &&self, int u) -> void
    {
        seen[nums[u]] = true;
        for (const int v : tree[u])
            self(self, v);
    };
    
    for (int i = 1; i < n; ++i)
        tree[parents[i]].push_back(i);
    int node_thats_one = getNode();
    if (node_thats_one == -1) return result;
    
    for (int u = node_thats_one, prev = -1; u != -1; )
    {
        for (const int v : tree[u])
        {
            if (v == prev) continue;
            dfs(dfs, v);
        }
        seen[nums[u]] = true;
        while (seen[min_miss])
            ++min_miss;
        result[u] = min_miss;
        prev = u;
        u = parents[u];
    }
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> parents,
          std::vector<int> nums,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestMissingValueSubtree(parents, nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, 0, 0, 2}, {1, 2, 3, 4}, {5, 1, 1, 1}, trials);
    test({-1, 0, 1, 0, 3, 3}, {5, 4, 6, 2, 1, 3}, {7, 1, 1, 4, 2, 1}, trials);
    test({-1, 2, 3, 0, 2, 4, 1}, {2, 3, 4, 5, 6, 7, 8}, {1, 1, 1, 1, 1, 1, 1}, trials);
    return 0;
}


