#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool isPowerOfTwo(int n)
{
    return (n > 0) && !(n & (n - 1));
}
#else
bool isPowerOfTwo(int n)
{
    return std::bitset<32>(n).count() == 1;
}
#endif

// ############################################################################
// ############################################################################

void test(int n, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPowerOfTwo(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, true, trials);
    test(16, true, trials);
    test(3, false, trials);
    return 0;
}


