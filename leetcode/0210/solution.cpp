#include "../common/common.hpp"
#include <set>
#include <queue>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> findOrder(int numCourses, std::vector<std::vector<int> > prerequisites)
{
    std::vector<std::vector<int> > adjacents(numCourses);
    std::vector<int> degree(numCourses, 0);
    std::vector<int> result;
    std::queue<int> nodes;
    
    for (const auto &p : prerequisites)
    {
        adjacents[p[1]].push_back(p[0]);   
        ++degree[p[0]]; 
    }
    for (int i = 0; i < numCourses; ++i)
        if (degree[i] == 0)
            nodes.push(i);
    int checked = 0;
    while (!nodes.empty())
    {
        int current = nodes.front();
        nodes.pop();
        result.push_back(current);
        ++checked;
        for (int neighbor : adjacents[current])
            if (--degree[neighbor] == 0)
                nodes.push(neighbor);
    }
    if (checked != numCourses) result.clear();
    return result;
}
#else
std::vector<int> findOrder(int numCourses, std::vector<std::vector<int> > prerequisites)
{
    struct Course
    {
        int id;
        std::set<int> prerequisites;
    };
    auto cmp = [](const Course * left, const Course * right) -> bool
    {
        return (left->prerequisites.size() > right->prerequisites.size())
            || ((left->prerequisites.size() == right->prerequisites.size()) && (left->id > right->id));
        return left->prerequisites.size() > right->prerequisites.size();
    };
    std::vector<Course> course_info(numCourses);
    std::vector<Course *> course_ptr(numCourses);
    std::vector<std::vector<int> > prerequisites_affect(numCourses);
    std::vector<int> result;
    
    for (int i = 0; i < numCourses; ++i)
    {
        course_info[i].id = i;
        course_ptr[i] = &course_info[i];
    }
    for (const auto &p : prerequisites)
    {
        course_info[p[0]].prerequisites.insert(p[1]);
        prerequisites_affect[p[1]].push_back(p[0]);
    }
    
    while (!course_ptr.empty())
    {
        std::sort(course_ptr.begin(), course_ptr.end(), cmp);
        
        int valid = 0;
        for (auto b = course_ptr.rbegin(), e = course_ptr.rend();
                (b != e) && ((*b)->prerequisites.size() == 0); ++b, ++valid);
        if (valid == 0) return {};
        for (int i = 0; i < valid; ++i)
        {
            int current = course_ptr.back()->id;
            result.push_back(current);
            for (int n : prerequisites_affect[current])
                course_info[n].prerequisites.erase(current);
            course_ptr.pop_back();
        }
    }
    
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int numCourses,
          std::vector<std::vector<int> > prerequisites,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findOrder(numCourses, prerequisites);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {{1, 0}}, {0, 1}, trials);
    test(4, {{1, 0}, {2, 0}, {3, 1}, {3, 2}}, {0, 1, 2, 3}, trials);
    test(1, {}, {0}, trials);
    return 0;
}


