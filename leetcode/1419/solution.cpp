#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minNumberOfFrogs(std::string croakOfFrogs)
{
    const std::string CROAK = "croak";
    int result = 0, frogs = 0, count[5] = {};
    
    for (char c : croakOfFrogs)
    {
        ++count[CROAK.find(c)];
        for (int i = 1; i < 5; ++i)
            if (count[i] > count[i - 1])
                return -1;
        if      (c == 'c') ++frogs;
        else if (c == 'k') --frogs;
        result = std::max(result, frogs);
    }
    return (frogs == 0)?result:-1;
}

// ############################################################################
// ############################################################################

void test(std::string croakFrogs, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minNumberOfFrogs(croakFrogs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("croakcroak", 1, trials);
    test("crcoakroak", 2, trials);
    test("croakcrook", -1, trials);
    return 0;
}


