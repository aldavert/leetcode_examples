#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countDaysTogether(std::string arriveAlice,
                      std::string leaveAlice,
                      std::string arriveBob,
                      std::string leaveBob)
{
    auto toTuple = [](std::string date) -> std::tuple<int, int>
    {
        int month = (date[0] - '0') * 10 + (date[1] - '0'),
              day = (date[3] - '0') * 10 + (date[4] - '0');
        return {month - 1, day};
    };
    auto arrive = toTuple(std::max(arriveAlice, arriveBob)),
         leave  = toTuple(std::min(leaveAlice, leaveBob));
    if (arrive > leave) return 0;
    else
    {
        int month_length[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int day = std::get<1>(arrive);
        int result = 0;
        for (int month = std::get<0>(arrive); month < std::get<0>(leave); ++month)
        {
            result += month_length[month] - day + 1;
            day = 1;
        }
        return result + std::get<1>(leave) - day + 1;
    }
}

// ############################################################################
// ############################################################################

void test(std::string arriveAlice,
          std::string leaveAlice,
          std::string arriveBob,
          std::string leaveBob,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countDaysTogether(arriveAlice, leaveAlice, arriveBob, leaveBob);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("08-15", "08-18", "08-16", "08-19", 3, trials);
    test("08-15", "10-18", "10-16", "12-19", 3, trials);
    test("08-15", "10-18", "08-16", "12-19", 64, trials);
    test("10-01", "10-31", "11-01", "12-31", 0, trials);
    test("09-01", "10-19", "06-19", "10-20", 49, trials);
    return 0;
}


