#include "../common/common.hpp"
#include <utility>
#include <limits>

// ############################################################################
// ############################################################################

int dominantIndex(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int largest = std::numeric_limits<int>::lowest();
    int second_largest = std::numeric_limits<int>::lowest();
    int index = -1;
    for (int i = 0; i < n; ++i)
    {
        if (nums[i] > largest)
        {
            second_largest = std::exchange(largest, nums[i]);
            index = i;
        }
        else if (nums[i] > second_largest)
            second_largest = nums[i];
    }
    return (second_largest * 2 <= largest)?index:-1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = dominantIndex(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 6, 1, 0},  1, trials);
    test({1, 2, 3, 4}, -1, trials);
    return 0;
}


