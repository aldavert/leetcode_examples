#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countOfPairs(std::vector<int> nums)
{
    constexpr int MOD = 1'000'000'007;
    constexpr int MAX = 1000;
    const int n = static_cast<int>(nums.size());
    int result = 0;
    std::vector<std::vector<int> > dp(n, std::vector<int>(MAX + 1));
    
    for (int num = 0; num <= nums[0]; ++num)
        dp[0][num] = 1;
    for (int i = 1; i < n; ++i)
    {
        for (int num = 0, ways = 0, prev_num = 0; num <= nums[i]; ++num)
        {
            if (prev_num <= std::min(num, num - (nums[i] - nums[i - 1])))
            {
                ways = (ways + dp[i - 1][prev_num]) % MOD;
                ++prev_num;
            }
            dp[i][num] = ways;
        }
    }
    for (int i = 0; i <= MAX; ++i)
        result = (result + dp[n - 1][i]) % MOD;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countOfPairs(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 2}, 4, trials);
    test({5, 5, 5, 5}, 126, trials);
    return 0;
}


