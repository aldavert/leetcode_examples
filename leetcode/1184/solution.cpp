#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int distanceBetweenBusStops(std::vector<int> distance, int start, int destination)
{
    const int n = static_cast<int>(distance.size());
    if (start > destination) std::swap(start, destination);
    int clockwise = 0, counterclockwise = 0;
    for (int i = 0; i < n; ++i)
    {
        if ((i < start) || (i >= destination)) counterclockwise += distance[i];
        else clockwise += distance[i];
    }
    return std::min(clockwise, counterclockwise);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> distance,
          int start,
          int destination,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = distanceBetweenBusStops(distance, start, destination);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 0, 1, 1, trials);
    test({1, 2, 3, 4}, 0, 2, 3, trials);
    test({1, 2, 3, 4}, 0, 3, 4, trials);
    return 0;
}


