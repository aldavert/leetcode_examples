#include "../common/common.hpp"
#include <unordered_map>
#include <stack>

// ############################################################################
// ############################################################################

std::string countOfAtoms(std::string formula)
{
    typedef std::unordered_map<std::string, int> HISTOGRAM;
    std::stack<HISTOGRAM> histogram;
    histogram.push({});
    
    int number = 0;
    std::string name;
    bool close_parenthesis = false;
    auto update = [&](void) -> void
    {
        if (close_parenthesis)
        {
            auto current = histogram.top();
            histogram.pop();
            for (auto &[n, f] : current)
                histogram.top()[n] += f * (number + (number == 0));
        }
        else if (!name.empty()) histogram.top()[name] += number + (number == 0);
        name = "";
        number = 0;
        close_parenthesis = false;
    };
    for (char letter : formula)
    {
        if (letter == '(')
        {
            update();
            histogram.push({});
        }
        else if (letter == ')')
        {
            update();
            close_parenthesis = true;
        }
        else if ((letter >= '0') && (letter <= '9'))
        {
            number = number * 10 + static_cast<int>(letter - '0');
        }
        else if ((letter >= 'A') && (letter <= 'Z'))
        {
            update();
            name = letter;
            number = 0;
        }
        else
        {
            name += letter;
        }
    }
    update();
    std::vector<std::string> sorted_names;
    for (auto &[n, f] : histogram.top())
        sorted_names.push_back(n);
    std::sort(sorted_names.begin(), sorted_names.end());
    std::string result;
    for (const auto &n : sorted_names)
    {
        result += n;
        int frequency = histogram.top()[n];
        if (frequency > 1)
            result += std::to_string(frequency);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string formula, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countOfAtoms(formula);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("H2O", "H2O", trials);
    test("Mg(OH)2", "H2MgO2", trials);
    test("K4(ON(SO3)2)2", "K4N2O14S4", trials);
    test("(N)1(N)1", "N2", trials);
    test("((N42)24(OB40Li30CHe3O48LiNN26)33(C12Li48N30H13HBe31)21"
         "(BHN30Li26BCBe47N40)15(H5)16)1",
         "B1350Be1356C300H389He99Li2421N3579O1617", trials);
    return 0;
}


