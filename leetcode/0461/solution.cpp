#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int hammingDistance(int x, int y)
{
    int result = 0;
    for (int diff = x ^ y; diff > 0; diff >>= 1)
        result += (diff & 1);
    return result;
}

// ############################################################################
// ############################################################################

void test(int x, int y, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = hammingDistance(x, y);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 4, 2, trials);
    test(3, 1, 1, trials);
    return 0;
}


