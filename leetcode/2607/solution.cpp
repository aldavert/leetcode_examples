#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long makeSubKSumEqual(std::vector<int> arr, int k)
{
    const int n = static_cast<int>(arr.size());
    long result = 0;
    std::vector<bool> seen(n);
    
    for (int i = 0; i < n; ++i)
    {
        std::vector<int> groups;
        int j = i;
        while (!seen[j])
        {
            groups.push_back(arr[j]);
            seen[j] = true;
            j = (j + k) % n;
        }
        std::nth_element(groups.begin(), groups.begin() + groups.size() / 2,
                         groups.end());
        for (const int num : groups)
            result += std::abs(num - groups[groups.size() / 2]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = makeSubKSumEqual(arr, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 1, 3}, 2, 1, trials);
    test({2, 5, 5, 7}, 3, 5, trials);
    return 0;
}


