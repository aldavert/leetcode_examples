#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int minSwaps(std::string s)
{
    const int ones = static_cast<int>(std::count(s.begin(), s.end(), '1')),
              zeros = static_cast<int>(s.size()) - ones;
    auto countSwaps = [&](char curr) -> int
    {
        int swaps = 0;
        for (const char c : s)
            swaps += (c != std::exchange(curr, curr ^1));
        return swaps / 2;
    };
    if (std::abs(ones - zeros) > 1) return -1;
    if (ones > zeros) return countSwaps('1');
    if (zeros > ones) return countSwaps('0');
    return std::min(countSwaps('1'), countSwaps('0'));
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSwaps(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("111000", 1, trials);
    test("010", 0, trials);
    test("1110", -1, trials);
    return 0;
}


