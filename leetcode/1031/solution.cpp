#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxSumTwoNoOverlap(std::vector<int> nums, int firstLen, int secondLen)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> left(n), right(n);
    int result = 0;
    auto process = [&](int l, int r) -> void
    {
        int sum = 0;
        for (int i = 0; i < n; ++i)
        {
            sum += nums[i];
            if (i >= l) sum -= nums[i - l];
            if (i >= l - 1) left[i] = (i > 0)?std::max(left[i - 1], sum):sum;
        }
        sum = 0;
        for (int i = n - 1; i >= 0; --i)
        {
            sum += nums[i];
            if (i <= n - r - 1) sum -= nums[i + r];
            if (i <= n - r) right[i] = (i < n - 1)?std::max(right[i + 1], sum):sum;
        }
        for (int i = 0; i < n - 1; ++i)
            result = std::max(result, left[i] + right[i + 1]);
    };
    process(firstLen, secondLen);
    process(secondLen, firstLen);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int firstLen,
          int secondLen,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSumTwoNoOverlap(nums, firstLen, secondLen);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 6, 5, 2, 2, 5, 1, 9, 4}, 1, 2, 20, trials);
    test({3, 8, 1, 3, 2, 1, 8, 9, 0}, 3, 2, 29, trials);
    test({2, 1, 5, 6, 0, 9, 5, 0, 3, 8}, 4, 3, 31, trials);
    return 0;
}


