#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findNumbers(std::vector<int> nums)
{
    int result = 0;
    for (int n : nums)
        result += ((1 + (n >=      10)
                      + (n >=     100)
                      + (n >=   1'000)
                      + (n >=  10'000)
                      + (n >= 100'000)) & 1) == 0;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findNumbers(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({12, 345, 2, 6, 7896}, 2, trials);
    test({555, 901, 482, 1771}, 1, trials);
    test({100000}, 1, trials);
    return 0;
}


