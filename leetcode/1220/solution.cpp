#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countVowelPermutation(int n)
{
    constexpr unsigned long MOD = 1'000'000'007;
    constexpr int a = 0, e = 1, i = 2, o = 3, u = 4;
    unsigned long buffer_current[5] = {1, 1, 1, 1, 1}, buffer_next[5] = {};
    unsigned long * cnt = buffer_current, * nxt = buffer_next;
    for (int k = 1; k < n; ++k)
    {
        nxt[a] = (cnt[e] + cnt[i] + cnt[u]) % MOD;
        nxt[e] = (cnt[a] + cnt[i]) % MOD;
        nxt[i] = (cnt[e] + cnt[o]) % MOD;
        nxt[o] = (cnt[i]) % MOD;
        nxt[u] = (cnt[i] + cnt[o]) % MOD;
        std::swap(cnt, nxt);
    }
    return static_cast<int>((cnt[a] + cnt[e] + cnt[i] + cnt[o] + cnt[u]) % MOD);
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countVowelPermutation(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 5, trials);
    test(2, 10, trials);
    test(5, 68, trials);
    test(144, 18'208'803, trials);
    return 0;
}


