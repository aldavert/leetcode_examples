#include "../common/common.hpp"
#include <numeric>
#include <bitset>

// ############################################################################
// ############################################################################

int componentValue(std::vector<int> nums, std::vector<std::vector<int> > edges)
{
    constexpr int MAX = 1'000'000'000;
    const int n = static_cast<int>(nums.size()),
              sum = std::accumulate(nums.begin(), nums.end(), 0);
    
    std::vector<std::vector<int> > tree(n);
    for (const auto &edge : edges)
    {
        tree[edge[0]].push_back(edge[1]);
        tree[edge[1]].push_back(edge[0]);
    }
    std::bitset<20'001> seen;
    auto dfs = [&](auto &&self, int u, int target) -> int
    {
        int result = nums[u];
        seen[u] = true;
        for (int v : tree[u])
        {
            if (seen[v]) continue;
            result += self(self, v, target);
            if (result > target) return MAX;
        }
        return (result == target)?0:result;
    };
    
    for (int i = n; i > 1; --i)
    {
        seen.reset();
        if ((sum % i == 0) && dfs(dfs, 0, sum / i) == 0)
            return i - 1;
    }
    return 0;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::vector<int> > edges,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = componentValue(nums, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({6, 2, 2, 2, 6}, {{0, 1}, {1, 2}, {1, 3}, {3, 4}}, 2, trials);
    test({2}, {}, 0, trials);
    test({1, 2, 1, 1, 1}, {{0, 1}, {1, 3}, {3, 4}, {4, 2}}, 1, trials);
    return 0;
}


