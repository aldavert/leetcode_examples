#include "../common/common.hpp"

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class MyLinkedList
{
    struct Node
    {
        Node(void) = default;
        ~Node(void) { delete next; }
        Node(int v, Node * n) : value(v), next(n) {}
        int value = 0;
        Node * next = nullptr;
    };
    Node * head = nullptr;
    Node * tail = nullptr;
    int size = 0;
public:
    MyLinkedList(void) : head(nullptr), tail(nullptr), size(0) {}
    ~MyLinkedList(void) { delete head; }
    int get(int index)
    {
        if ((index < 0) || (index >= size)) return -1;
        Node * current = head;
        for (; index > 0; --index, current = current->next);
        return current->value;
    }
    void addAtHead(int val)
    {
        ++size;
        head = new Node(val, head);
        if (tail == nullptr) tail = head;
    }
    void addAtTail(int val)
    {
        ++size;
        if (size == 1) head = tail = new Node(val, nullptr);
        else
        {
            tail->next = new Node(val, nullptr);
            tail = tail->next;
        }
    }
    void addAtIndex(int index, int val)
    {
        if ((index < 0) || (index > size)) return;
        if (index == size)
        {
            addAtTail(val);
            return;
        }
        ++size;
        if (index == 0) head = new Node(val, head);
        else
        {
            Node * current = head;
            for (; index > 1; --index, current = current->next);
            current->next = new Node(val, current->next);
        }
    }
    void deleteAtIndex(int index)
    {
        if ((index < 0) || (index >= size)) return;
        if (size == 1)
        {
            delete head;
            head = tail = nullptr;
            size = 0;
            return;
        }
        if (index == 0)
        {
            Node * tmp = head;
            head = head->next;
            tmp->next = nullptr;
            delete tmp;
        }
        else
        {
            Node * current = head;
            for (; index > 1; --index, current = current->next);
            if (current->next == tail)
            {
                delete current->next;
                current->next = nullptr;
                tail = current;
            }
            else
            {
                Node * tmp = current->next;
                current->next = current->next->next;
                tmp->next = nullptr;
                delete tmp;
            }
        }
        --size;
    }
};

// ############################################################################
// ############################################################################

enum class OP { GET, ADDHEAD, ADDTAIL, ADDINDEX, DELETE };

void test(std::vector<OP> ops,
          std::vector<std::vector<int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        MyLinkedList * obj = new MyLinkedList();
        for (size_t i = 0; i < ops.size(); ++i)
        {
            switch (ops[i])
            {
            case OP::GET:
                result.push_back(obj->get(input[i].front()));
                break;
            case OP::ADDHEAD:
                result.push_back(null);
                obj->addAtHead(input[i].front());
                break;
            case OP::ADDTAIL:
                result.push_back(null);
                obj->addAtTail(input[i].front());
                break;
            case OP::ADDINDEX:
                result.push_back(null);
                obj->addAtIndex(input[i].front(), input[i].back());
                break;
            case OP::DELETE:
                result.push_back(null);
                obj->deleteAtIndex(input[i].front());
                break;
            default:
                std::cerr << "[ERROR] UNKNOWN PARAMETER\n";
                return;
            }
        }
        delete obj;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::ADDHEAD, OP::ADDTAIL, OP::ADDINDEX, OP::GET, OP::DELETE, OP::GET},
         {{1}, {3}, {1, 2}, {1}, {1}, {1}},
         {null, null, null, 2, null, 3}, trials);
    test({OP::ADDHEAD, OP::DELETE},
         {{1}, {0}},
         {null, null}, trials);
    test({OP::ADDINDEX, OP::ADDINDEX, OP::ADDINDEX, OP::GET},
         {{0, 10}, {0, 20}, {1, 30}, {0}},
         {null, null, null, 20}, trials);
    test({OP::ADDHEAD, OP::GET, OP::ADDINDEX, OP::ADDINDEX, OP::DELETE,
          OP::ADDHEAD, OP::ADDHEAD, OP::DELETE, OP::ADDINDEX, OP::ADDHEAD, OP::DELETE},
         {{9}, {1}, {1, 1}, {1, 7}, {1}, {7}, {4}, {1}, {1, 4}, {2}, {5}},
         {null, -1, null, null, null, null, null, null, null, null, null}, trials);
    return 0;
}


