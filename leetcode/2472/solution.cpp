#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxPalindromes(std::string s, int k)
{
    const int n = static_cast<int>(s.size());
    std::vector<int> dp(n + 1);
    auto isPalindrome = [&](int l, int r) -> bool
    {
        if (l < 0) return false;
        while (l < r)
            if (s[l++] != s[r--])
                return false;
        return true;
    };
    
    for (int i = k; i <= n; ++i)
    {
        dp[i] = dp[i - 1];
        if (isPalindrome(i - k    , i - 1)) dp[i] = std::max(dp[i], 1 + dp[i - k]);
        if (isPalindrome(i - k - 1, i - 1)) dp[i] = std::max(dp[i], 1 + dp[i - k - 1]);
    }
    return dp[n];
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxPalindromes(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abaccdbbd", 3, 2, trials);
    test("adbcda", 2, 0, trials);
    return 0;
}


