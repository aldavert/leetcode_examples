#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string getHint(std::string secret, std::string guess)
{
    int histogram_secret[10] = {}, histogram_guess[10] = {};
    int bulls = 0;
    for (size_t i = 0, n = secret.size(); i < n; ++i)
    {
        if (secret[i] == guess[i]) ++bulls;
        else
        {
            ++histogram_secret[secret[i] - '0'];
            ++histogram_guess[guess[i] - '0'];
        }
    }
    int cows = 0;
    for (int i = 0; i < 10; ++i)
        cows += std::min(histogram_secret[i], histogram_guess[i]);
    return std::to_string(bulls) + "A" + std::to_string(cows) + "B";
}

// ############################################################################
// ############################################################################

void test(std::string secret,
          std::string guess,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getHint(secret, guess);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1807", "7810", "1A3B", trials);
    test("1123", "0111", "1A1B", trials);
    return 0;
}


