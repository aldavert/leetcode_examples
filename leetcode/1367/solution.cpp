#include "../common/common.hpp"
#include "../common/list.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

bool isSubPath(ListNode * head, TreeNode * root)
{
    auto isContinuousSubPath = [&]() -> bool
    {
        auto inner = [](auto &&self, ListNode * h, TreeNode * r) -> bool
        {
            if (h == nullptr) return true;
            if (r == nullptr) return false;
            return h->val == r->val
                && (self(self, h->next, r->left)
                ||  self(self, h->next, r->right));
        };
        return inner(inner, head, root);
    };
    if (root == nullptr) return false;
    return isContinuousSubPath()
        || isSubPath(head, root->left)
        || isSubPath(head, root->right);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> head,
          std::vector<int> tree,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * list = vec2list(head);
        TreeNode * root = vec2tree(tree);
        result = isSubPath(list, root);
        delete root;
        delete list;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 8},
         {1, 4, 4, null, 2, 2, null, 1, null, 6, 8, null, null, null, null, 1, 3},
         true, trials);
    test({1, 4, 2, 6},
         {1, 4, 4, null, 2, 2, null, 1, null, 6, 8, null, null, null, null, 1, 3},
         true, trials);
    test({1, 4, 2, 6, 8},
         {1, 4, 4, null, 2, 2, null, 1, null, 6, 8, null, null, null, null, 1, 3},
         false, trials);
    return 0;
}


