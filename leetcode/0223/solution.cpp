#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int computeArea(int ax1, int ay1, int ax2, int ay2,
                int bx1, int by1, int bx2, int by2)
{
    int ix1 = std::max(ax1, bx1), ix2 = std::min(ax2, bx2);
    int iy1 = std::max(ay1, by1), iy2 = std::min(ay2, by2);
    return (ax2 - ax1) * (ay2 - ay1)
         + (bx2 - bx1) * (by2 - by1)
         - std::max(0, ix2 - ix1) * std::max(0, iy2 - iy1);
}

// ############################################################################
// ############################################################################

void test(int ax1, int ay1, int ax2, int ay2,
          int bx1, int by1, int bx2, int by2,
          int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = computeArea(ax1, ay1, ax2, ay2, bx1, by1, bx2, by2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(-3,  0,  3,  4,  0, -1,  9,  2, 45, trials);
    test(-2, -2,  2,  2, -2, -2,  2,  2, 16, trials);
    return 0;
}


