#include "../common/common.hpp"
#include <unordered_map>
#include <queue>

// ############################################################################
// ############################################################################

#if 0
std::vector<std::vector<std::string> >
findLadders(std::string beginWord,
            std::string endWord,
            std::vector<std::string> wordList)
{
    std::unordered_map<std::string, int> dictionary;
    const int n = static_cast<int>(wordList.size());
    if (n > 10)
    {
        std::cerr << "TOO MANY ELEMENTS WILL END UP SING ALL MEMORY. THIS SOLUTION "
                     "IS WRONG BECAUSE YOU HAVE TO RETURN THE SHORTEST PATH SO YOU "
                     "DON'T HAVE TO KEEP AL POSSIBLE WORDS FOR EACH INDIVIDUAL "
                     "PATH\n\n\n";
        return {};
    }
    for (int idx = 0; const auto &w : wordList)
        dictionary[w] = idx++;
    if (dictionary.find(endWord) == dictionary.end()) return {};
    std::queue<std::pair<std::vector<std::string>, std::vector<bool> > > q;
    std::vector<std::vector<std::string> > result;
    
    q.push({{beginWord}, std::vector<bool>(n, true)});
    for (bool found = false; !(found || q.empty());)
    {
        for (int k = 0, nq = static_cast<int>(q.size()); k < nq; ++k)
        {
            auto [path, available] = std::move(q.front());
            q.pop();
            
            std::vector<std::string> words;
            std::string current = path.back();
            for (int i = 0, nc = static_cast<int>(current.size()); i < nc; ++i)
            {
                char previous_character = current[i];
                for (char c = 'a'; c <= 'z'; ++c)
                {
                    current[i] = c;
                    if (auto it = dictionary.find(current);
                        (it != dictionary.end()) && (available[it->second]))
                        words.push_back(current);
                }
                current[i] = previous_character;
            }
            
            for (const auto &w: words)
            {
                auto new_path = path;
                auto new_available = available; 
                new_path.push_back(w);
                new_available[dictionary[w]] = false;
                if (w == endWord)
                {
                    found = true;
                    result.push_back(new_path);
                }
                q.push({std::move(new_path), std::move(new_available)});
            }
        }
    }
    return result;
}
#elif 0
std::vector<std::vector<std::string> >
findLadders(std::string beginWord,
            std::string endWord,
            std::vector<std::string> wordList)
{
    if (wordList.size() > 10)
    {
        std::cerr << "TOO MANY ELEMENTS WILL END UP SING ALL MEMORY. "
                     "THIS SOLUTION IS WRONG BECAUSE YOU HAVE TO RETURN "
                     "THE SHORTEST PATH SO YOU DON'T HAVE TO KEEP AL "
                     "POSSIBLE WORDS FOR EACH INDIVIDUAL PATH\n\n\n";
        return {};
    }
    std::queue<std::pair<std::vector<std::string>,
                         std::unordered_set<std::string> > > q;
    std::vector<std::vector<std::string> > result;
    
    q.push({{beginWord},
            std::unordered_set<std::string>(wordList.begin(), wordList.end())});
    for (bool found = false; !(found || q.empty());)
    {
        for (int k = 0, nq = static_cast<int>(q.size()); k < nq; ++k)
        {
            auto [path, dictionary] = std::move(q.front());
            q.pop();
            
            std::vector<std::string> words;
            std::string current = path.back();
            for (int i = 0, nc = static_cast<int>(current.size()); i < nc; ++i)
            {
                char previous_character = current[i];
                for (char c = 'a'; c <= 'z'; ++c)
                {
                    current[i] = c;
                    if (dictionary.find(current) != dictionary.end())
                        words.push_back(current);
                }
                current[i] = previous_character;
            }
            
            for (const auto &w: words)
            {
                auto new_path = path;
                auto new_dictionary = dictionary; 
                new_path.push_back(w);
                new_dictionary.erase(w);
                if (w == endWord)
                {
                    found = true;
                    result.push_back(new_path);
                }
                q.push({std::move(new_path), std::move(new_dictionary)});
            }
        }
    }
    return result;
}
#elif 0
std::vector<std::vector<std::string> >
findLadders(std::string beginWord,
            std::string endWord,
            std::vector<std::string> wordList)
{
    std::queue<std::vector<std::string> > q;
    std::vector<std::vector<std::string> > result;
    std::unordered_set<std::string> dictionary(wordList.begin(), wordList.end());
    if (dictionary.find(endWord) == dictionary.end()) return {};
    
    q.push({beginWord});
    for (bool found = false; !q.empty();)
    {
        std::unordered_set<std::string> visited;
        
        for (int k = 0, nq = static_cast<int>(q.size()); k < nq; ++k)
        {
            std::vector<std::string> curr = std::move(q.front());
            q.pop();
            
            std::vector<std::string> words;
            std::string current = curr.back();
            for (int i = 0, nc = static_cast<int>(current.size()); i < nc; ++i)
            {
                char previous_character = current[i];
                for (char c = 'a'; c <= 'z'; ++c)
                {
                    current[i] = c;
                    if (dictionary.find(current) != dictionary.end())
                        words.push_back(current);
                }
                current[i] = previous_character;
            }
            
            for (auto s: words)
            {
                std::vector<std::string> new_path(curr.begin(), curr.end());
                new_path.push_back(s);
                if (s == endWord)
                {
                    found = true;
                    result.push_back(new_path);
                }
                visited.insert(s);
                q.push(new_path);
            }
        }
        if (found) break;
        for (auto s: visited) dictionary.erase(s);
    }
    return result;
}
#else
std::vector<std::vector<std::string> >
findLadders(std::string beginWord,
            std::string endWord,
            std::vector<std::string> wordList)
{
    const size_t size = wordList.size() + 1;
    std::vector<std::vector<std::string> > result;
    std::vector<std::vector<int> > precursor(size);
	auto isNeighbor = [](const std::string &s1, const std::string &s2) -> bool
    {
        bool hasChanged = false;
	    for (size_t i = 0; i < s1.size(); ++i)
        {
            if (s1[i] != s2[i])
            {
                if (hasChanged) return false;
                else hasChanged = true;
            }
        }
        return true;
    };
    auto generate = [&](auto &&self, std::vector<std::string> right,
                        std::vector<int> &current_precursor) -> void
    {
        if (current_precursor.empty())
            result.push_back(right);
        else
        {
            for (size_t i = 0; i < current_precursor.size(); ++i)
            {
                std::vector<std::string> copy(right.size() + 1);
                std::copy(right.begin(), right.end(), copy.begin() + 1);
                copy[0] = wordList[current_precursor[i]];
                self(self, std::move(copy), precursor[current_precursor[i]]);
            }
        }
	};
    wordList.push_back(beginWord);
    std::vector<std::vector<int> > neighbors(size);
    size_t end_word_idx = size;
    for (size_t i = 0; i < size; ++i)
    {
        if (wordList[i] == endWord)
            end_word_idx = i;
        for (size_t j = i + 1; j < size; ++j)
        {
            if (isNeighbor(wordList[i], wordList[j]))
            {
                neighbors[i].push_back(static_cast<int>(j));
                neighbors[j].push_back(static_cast<int>(i));
            }
        }
    }
    std::vector<size_t> steps(size);
    std::queue<size_t> line({size - 1});
    steps[size - 1] = 1;
    
    while (!line.empty())
    {
        int pos = static_cast<int>(line.front());
        line.pop();
        if (wordList[pos] == endWord)
            break;
        for (size_t i = 0; i < neighbors[pos].size(); ++i)
        {
            if (steps[neighbors[pos][i]] == 0)
            {
                steps[neighbors[pos][i]] = steps[pos] + 1;
                precursor[neighbors[pos][i]].push_back(pos);
                line.push(neighbors[pos][i]);
            }
            else if (steps[neighbors[pos][i]] == steps[pos] + 1)
                precursor[neighbors[pos][i]].push_back(pos);
        }
    }
    if ((end_word_idx == size) || (steps[end_word_idx] == 0))
        return result;
    generate(generate, {endWord}, precursor[end_word_idx]);
    return result;
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<std::string> > &left,
                const std::vector<std::vector<std::string> > &right)
{
    if (left.size() != right.size()) return false;
    const int n = static_cast<int>(left.size());
    std::vector<bool> not_used(n, true);
    for (int i = 0; i < n; ++i)
    {
        bool found = false;
        for (int j = 0; (!found) && (j < n); ++j)
        {
            if ((not_used[i]) && (left[i] == right[j]))
            {
                found = true;
                not_used[i] = false;
            }
        }
        if (!found) return false;
    }
    return true;
}

void test(std::string beginWord,
          std::string endWord,
          std::vector<std::string> wordList,
          std::vector<std::vector<std::string> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<std::string> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLadders(beginWord, endWord, wordList);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("hit", "cog", {"hot", "dot", "dog", "lot", "log", "cog"},
         {{"hit", "hot", "dot", "dog", "cog"}, {"hit", "hot", "lot", "log", "cog"}},
         trials);
    test("hit", "cog", {"hot", "dot", "dog", "lot", "log"}, {}, trials);
    test("cet", "ism",
         {"kid", "tag", "pup", "ail", "tun", "woo", "erg", "luz", "brr", "gay",
          "sip", "kay", "per", "val", "mes", "ohs", "now", "boa", "cet", "pal",
          "bar", "die", "war", "hay", "eco", "pub", "lob", "rue", "fry", "lit",
          "rex", "jan", "cot", "bid", "ali", "pay", "col", "gum", "ger", "row",
          "won", "dan", "rum", "fad", "tut", "sag", "yip", "sui", "ark", "has",
          "zip", "fez", "own", "ump", "dis", "ads", "max", "jaw", "out", "btu",
          "ana", "gap", "cry", "led", "abe", "box", "ore", "pig", "fie", "toy",
          "fat", "cal", "lie", "noh", "sew", "ono", "tam", "flu", "mgm", "ply",
          "awe", "pry", "tit", "tie", "yet", "too", "tax", "jim", "san", "pan",
          "map", "ski", "ova", "wed", "non", "wac", "nut", "why", "bye", "lye",
          "oct", "old", "fin", "feb", "chi", "sap", "owl", "log", "tod", "dot",
          "bow", "fob", "for", "joe", "ivy", "fan", "age", "fax", "hip", "jib",
          "mel", "hus", "sob", "ifs", "tab", "ara", "dab", "jag", "jar", "arm",
          "lot", "tom", "sax", "tex", "yum", "pei", "wen", "wry", "ire", "irk",
          "far", "mew", "wit", "doe", "gas", "rte", "ian", "pot", "ask", "wag",
          "hag", "amy", "nag", "ron", "soy", "gin", "don", "tug", "fay", "vic",
          "boo", "nam", "ave", "buy", "sop", "but", "orb", "fen", "paw", "his",
          "sub", "bob", "yea", "oft", "inn", "rod", "yam", "pew", "web", "hod",
          "hun", "gyp", "wei", "wis", "rob", "gad", "pie", "mon", "dog", "bib",
          "rub", "ere", "dig", "era", "cat", "fox", "bee", "mod", "day", "apr",
          "vie", "nev", "jam", "pam", "new", "aye", "ani", "and", "ibm", "yap",
          "can", "pyx", "tar", "kin", "fog", "hum", "pip", "cup", "dye", "lyx",
          "jog", "nun", "par", "wan", "fey", "bus", "oak", "bad", "ats", "set",
          "qom", "vat", "eat", "pus", "rev", "axe", "ion", "six", "ila", "lao",
          "mom", "mas", "pro", "few", "opt", "poe", "art", "ash", "oar", "cap",
          "lop", "may", "shy", "rid", "bat", "sum", "rim", "fee", "bmw", "sky",
          "maj", "hue", "thy", "ava", "rap", "den", "fla", "auk", "cox", "ibo",
          "hey", "saw", "vim", "sec", "ltd", "you", "its", "tat", "dew", "eva",
          "tog", "ram", "let", "see", "zit", "maw", "nix", "ate", "gig", "rep",
          "owe", "ind", "hog", "eve", "sam", "zoo", "any", "dow", "cod", "bed",
          "vet", "ham", "sis", "hex", "via", "fir", "nod", "mao", "aug", "mum",
          "hoe", "bah", "hal", "keg", "hew", "zed", "tow", "gog", "ass", "dem",
          "who", "bet", "gos", "son", "ear", "spy", "kit", "boy", "due", "sen",
          "oaf", "mix", "hep", "fur", "ada", "bin", "nil", "mia", "ewe", "hit",
          "fix", "sad", "rib", "eye", "hop", "haw", "wax", "mid", "tad", "ken",
          "wad", "rye", "pap", "bog", "gut", "ito", "woe", "our", "ado", "sin",
          "mad", "ray", "hon", "roy", "dip", "hen", "iva", "lug", "asp", "hui",
          "yak", "bay", "poi", "yep", "bun", "try", "lad", "elm", "nat", "wyo",
          "gym", "dug", "toe", "dee", "wig", "sly", "rip", "geo", "cog", "pas",
          "zen", "odd", "nan", "lay", "pod", "fit", "hem", "joy", "bum", "rio",
          "yon", "dec", "leg", "put", "sue", "dim", "pet", "yaw", "nub", "bit",
          "bur", "sid", "sun", "oil", "red", "doc", "moe", "caw", "eel", "dix",
          "cub", "end", "gem", "off", "yew", "hug", "pop", "tub", "sgt", "lid",
          "pun", "ton", "sol", "din", "yup", "jab", "pea", "bug", "gag", "mil",
          "jig", "hub", "low", "did", "tin", "get", "gte", "sox", "lei", "mig",
          "fig", "lon", "use", "ban", "flo", "nov", "jut", "bag", "mir", "sty",
          "lap", "two", "ins", "con", "ant", "net", "tux", "ode", "stu", "mug",
          "cad", "nap", "gun", "fop", "tot", "sow", "sal", "sic", "ted", "wot",
          "del", "imp", "cob", "way", "ann", "tan", "mci", "job", "wet", "ism",
          "err", "him", "all", "pad", "hah", "hie", "aim", "ike", "jed", "ego",
          "mac", "baa", "min", "com", "ill", "was", "cab", "ago", "ina", "big",
          "ilk", "gal", "tap", "duh", "ola", "ran", "lab", "top", "gob", "hot",
          "ora", "tia", "kip", "han", "met", "hut", "she", "sac", "fed", "goo",
          "tee", "ell", "not", "act", "gil", "rut", "ala", "ape", "rig", "cid",
          "god", "duo", "lin", "aid", "gel", "awl", "lag", "elf", "liz", "ref",
          "aha", "fib", "oho", "tho", "her", "nor", "ace", "adz", "fun", "ned",
          "coo", "win", "tao", "coy", "van", "man", "pit", "guy", "foe", "hid",
          "mai", "sup", "jay", "hob", "mow", "jot", "are", "pol", "arc", "lax",
          "aft", "alb", "len", "air", "pug", "pox", "vow", "got", "meg", "zoe",
          "amp", "ale", "bud", "gee", "pin", "dun", "pat", "ten", "mob"},
         {{"cet", "get", "gee", "gte", "ate", "ats", "its", "ito", "ibo", "ibm",
           "ism"},
          {"cet", "cat", "can", "ian", "inn", "ins", "its", "ito", "ibo", "ibm",
           "ism"},
          {"cet", "cot", "con", "ion", "inn", "ins", "its", "ito", "ibo", "ibm",
           "ism"}}, trials);
    test("aaaaa", "ggggg",
        {"aaaaa", "caaaa", "cbaaa", "daaaa", "dbaaa", "eaaaa", "ebaaa", "faaaa",
         "fbaaa", "gaaaa", "gbaaa", "haaaa", "hbaaa", "iaaaa", "ibaaa", "jaaaa",
         "jbaaa", "kaaaa", "kbaaa", "laaaa", "lbaaa", "maaaa", "mbaaa", "naaaa",
         "nbaaa", "oaaaa", "obaaa", "paaaa", "pbaaa", "bbaaa", "bbcaa", "bbcba",
         "bbdaa", "bbdba", "bbeaa", "bbeba", "bbfaa", "bbfba", "bbgaa", "bbgba",
         "bbhaa", "bbhba", "bbiaa", "bbiba", "bbjaa", "bbjba", "bbkaa", "bbkba",
         "bblaa", "bblba", "bbmaa", "bbmba", "bbnaa", "bbnba", "bboaa", "bboba",
         "bbpaa", "bbpba", "bbbba", "abbba", "acbba", "dbbba", "dcbba", "ebbba",
         "ecbba", "fbbba", "fcbba", "gbbba", "gcbba", "hbbba", "hcbba", "ibbba",
         "icbba", "jbbba", "jcbba", "kbbba", "kcbba", "lbbba", "lcbba", "mbbba",
         "mcbba", "nbbba", "ncbba", "obbba", "ocbba", "pbbba", "pcbba", "ccbba",
         "ccaba", "ccaca", "ccdba", "ccdca", "cceba", "cceca", "ccfba", "ccfca",
         "ccgba", "ccgca", "cchba", "cchca", "cciba", "ccica", "ccjba", "ccjca",
         "cckba", "cckca", "cclba", "cclca", "ccmba", "ccmca", "ccnba", "ccnca",
         "ccoba", "ccoca", "ccpba", "ccpca", "cccca", "accca", "adcca", "bccca",
         "bdcca", "eccca", "edcca", "fccca", "fdcca", "gccca", "gdcca", "hccca",
         "hdcca", "iccca", "idcca", "jccca", "jdcca", "kccca", "kdcca", "lccca",
         "ldcca", "mccca", "mdcca", "nccca", "ndcca", "occca", "odcca", "pccca",
         "pdcca", "ddcca", "ddaca", "ddada", "ddbca", "ddbda", "ddeca", "ddeda",
         "ddfca", "ddfda", "ddgca", "ddgda", "ddhca", "ddhda", "ddica", "ddida",
         "ddjca", "ddjda", "ddkca", "ddkda", "ddlca", "ddlda", "ddmca", "ddmda",
         "ddnca", "ddnda", "ddoca", "ddoda", "ddpca", "ddpda", "dddda", "addda",
         "aedda", "bddda", "bedda", "cddda", "cedda", "fddda", "fedda", "gddda",
         "gedda", "hddda", "hedda", "iddda", "iedda", "jddda", "jedda", "kddda",
         "kedda", "lddda", "ledda", "mddda", "medda", "nddda", "nedda", "oddda",
         "oedda", "pddda", "pedda", "eedda", "eeada", "eeaea", "eebda", "eebea",
         "eecda", "eecea", "eefda", "eefea", "eegda", "eegea", "eehda", "eehea",
         "eeida", "eeiea", "eejda", "eejea", "eekda", "eekea", "eelda", "eelea",
         "eemda", "eemea", "eenda", "eenea", "eeoda", "eeoea", "eepda", "eepea",
         "eeeea", "ggggg", "agggg", "ahggg", "bgggg", "bhggg", "cgggg", "chggg",
         "dgggg", "dhggg", "egggg", "ehggg", "fgggg", "fhggg", "igggg", "ihggg",
         "jgggg", "jhggg", "kgggg", "khggg", "lgggg", "lhggg", "mgggg", "mhggg",
         "ngggg", "nhggg", "ogggg", "ohggg", "pgggg", "phggg", "hhggg", "hhagg",
         "hhahg", "hhbgg", "hhbhg", "hhcgg", "hhchg", "hhdgg", "hhdhg", "hhegg",
         "hhehg", "hhfgg", "hhfhg", "hhigg", "hhihg", "hhjgg", "hhjhg", "hhkgg",
         "hhkhg", "hhlgg", "hhlhg", "hhmgg", "hhmhg", "hhngg", "hhnhg", "hhogg",
         "hhohg", "hhpgg", "hhphg", "hhhhg", "ahhhg", "aihhg", "bhhhg", "bihhg",
         "chhhg", "cihhg", "dhhhg", "dihhg", "ehhhg", "eihhg", "fhhhg", "fihhg",
         "ghhhg", "gihhg", "jhhhg", "jihhg", "khhhg", "kihhg", "lhhhg", "lihhg",
         "mhhhg", "mihhg", "nhhhg", "nihhg", "ohhhg", "oihhg", "phhhg", "pihhg",
         "iihhg", "iiahg", "iiaig", "iibhg", "iibig", "iichg", "iicig", "iidhg",
         "iidig", "iiehg", "iieig", "iifhg", "iifig", "iighg", "iigig", "iijhg",
         "iijig", "iikhg", "iikig", "iilhg", "iilig", "iimhg", "iimig", "iinhg",
         "iinig", "iiohg", "iioig", "iiphg", "iipig", "iiiig", "aiiig", "ajiig",
         "biiig", "bjiig", "ciiig", "cjiig", "diiig", "djiig", "eiiig", "ejiig",
         "fiiig", "fjiig", "giiig", "gjiig", "hiiig", "hjiig", "kiiig", "kjiig",
         "liiig", "ljiig", "miiig", "mjiig", "niiig", "njiig", "oiiig", "ojiig",
         "piiig", "pjiig", "jjiig", "jjaig", "jjajg", "jjbig", "jjbjg", "jjcig",
         "jjcjg", "jjdig", "jjdjg", "jjeig", "jjejg", "jjfig", "jjfjg", "jjgig",
         "jjgjg", "jjhig", "jjhjg", "jjkig", "jjkjg", "jjlig", "jjljg", "jjmig",
         "jjmjg", "jjnig", "jjnjg", "jjoig", "jjojg", "jjpig", "jjpjg", "jjjjg",
         "ajjjg", "akjjg", "bjjjg", "bkjjg", "cjjjg", "ckjjg", "djjjg", "dkjjg",
         "ejjjg", "ekjjg", "fjjjg", "fkjjg", "gjjjg", "gkjjg", "hjjjg", "hkjjg",
         "ijjjg", "ikjjg", "ljjjg", "lkjjg", "mjjjg", "mkjjg", "njjjg", "nkjjg",
         "ojjjg", "okjjg", "pjjjg", "pkjjg", "kkjjg", "kkajg", "kkakg", "kkbjg",
         "kkbkg", "kkcjg", "kkckg", "kkdjg", "kkdkg", "kkejg", "kkekg", "kkfjg",
         "kkfkg", "kkgjg", "kkgkg", "kkhjg", "kkhkg", "kkijg", "kkikg", "kkljg",
         "kklkg", "kkmjg", "kkmkg", "kknjg", "kknkg", "kkojg", "kkokg", "kkpjg",
         "kkpkg", "kkkkg", "ggggx", "gggxx", "ggxxx", "gxxxx", "xxxxx", "xxxxy",
         "xxxyy", "xxyyy", "xyyyy", "yyyyy", "yyyyw", "yyyww", "yywww", "ywwww",
         "wwwww", "wwvww", "wvvww", "vvvww", "vvvwz", "avvwz", "aavwz", "aaawz",
         "aaaaz"},
       {{"aaaaa", "aaaaz", "aaawz", "aavwz", "avvwz", "vvvwz", "vvvww", "wvvww",
         "wwvww", "wwwww", "ywwww", "yywww", "yyyww", "yyyyw", "yyyyy", "xyyyy",
         "xxyyy", "xxxyy", "xxxxy", "xxxxx", "gxxxx", "ggxxx", "gggxx", "ggggx",
         "ggggg"}}, trials);

    return 0;
}


