#include "../common/common.hpp"
#include <sstream>
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > subsets(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<std::vector<int> > result;
    std::vector<int> aux;
    auto dfs = [&](auto &&self, int cur) -> void
    {
        result.emplace_back(aux);
        for (int i = cur; i < n; ++i)
        {
            aux.emplace_back(nums[i]);
            self(self, i + 1);
            aux.pop_back();
        }
 	};
    if (n != 0) dfs(dfs, 0);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_map<std::string, int> histogram;
    for (const std::vector<int> &v : left)
    {
        std::ostringstream oss;
        oss << v;
        ++histogram[oss.str()];
    }
    for (const std::vector<int> &v : right)
    {
        std::ostringstream oss;
        oss << v;
        if (auto iter = histogram.find(oss.str()); iter != histogram.end())
        {
            --(iter->second);
            if (!iter->second)
                histogram.erase(iter);
        }
        else return false;
    }
    return true;
}

void test(std::vector<int> nums,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = subsets(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, {{}, {1}, {2}, {1, 2}, {3}, {1, 3}, {2, 3}, {1, 2, 3}}, trials);
    test({0}, {{}, {0}}, trials);
    return 0;
}


