#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

bool isSubstringPresent(std::string s)
{
    constexpr size_t number_of_characters = 'z' - 'a' + 1;
    std::bitset<number_of_characters * number_of_characters> histogram;
    for (size_t i = 0; i < s.size() - 1; ++i)
        histogram[(s[i + 1] - 'a') * number_of_characters + (s[i] - 'a')] = true;
    for (size_t i = 0; i < s.size() - 1; ++i)
        if (histogram[(s[i + 1] - 'a') + (s[i] - 'a') * number_of_characters])
            return true;
    return false;
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isSubstringPresent(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leetcode",  true, trials);
    test("abcba",  true, trials);
    test("abcd", false, trials);
    return 0;
}


