#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int movesToMakeZigzag(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int decreasing[2] = {};
    for (int i = 0; i < n; ++i)
    {
        int l = (i > 0)?nums[i - 1]:1001;
        int r = (i + 1 < n)?nums[i + 1]:1001;
        decreasing[i % 2] += std::max(0, nums[i] - std::min(l, r) + 1);
    }
    return std::min(decreasing[0], decreasing[1]);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = movesToMakeZigzag(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 2, trials);
    test({9, 6, 1, 6, 2}, 4, trials);
    return 0;
}


