#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxIncreasingGroups(std::vector<int> usageLimits)
{
    std::sort(usageLimits.begin(), usageLimits.end());
    long result = 0, total = 0;
    for (const auto& item : usageLimits)
    {
        total += item;
        result += (total * 2 >= (result + 1) * (result + 2));
    }
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> usageLimits, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxIncreasingGroups(usageLimits);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 5}, 3, trials);
    test({2, 1, 2}, 2, trials);
    test({1, 1}, 1, trials);
    return 0;
}


