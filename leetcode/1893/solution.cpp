#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isCovered(std::vector<std::vector<int> > ranges, int left, int right)
{
    char covered[51] = {};
    for (const auto &range : ranges)
        for (int i = range[0]; i <= range[1]; ++i)
            covered[i] = 1;
    for (int i = left; i <= right; ++i)
        if (!covered[i])
            return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > ranges,
          int left,
          int right,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isCovered(ranges, left, right);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {3, 4}, {5, 6}}, 2, 5, true, trials);
    test({{1, 10}, {10, 20}}, 21, 21, false, trials);
    return 0;
}


