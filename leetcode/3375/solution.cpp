#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> nums, int k)
{
    std::sort(nums.begin(), nums.end());
    if (nums[0] < k) return -1;
    int result = 0;
    for (int j = static_cast<int>(nums.size()) - 1; j >= 0;)
    {
        int i = j;
        while ((i >= 0) && (nums[i] == nums[j])) --i;
        j = i;
        ++result;
    }
    return result - (nums[0] == k);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 2, 5, 4, 5}, 2, 2, trials);
    test({2, 1, 2}, 2, -1, trials);
    test({9, 7, 5, 3}, 1, 4, trials);
    return 0;
}


