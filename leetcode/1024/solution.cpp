#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int videoStitching(std::vector<std::vector<int> > clips, int time)
{
    const int n = static_cast<int>(clips.size());
    std::sort(clips.begin(), clips.end());
    int result = 0;
    for (int i = 0, farthest = 0, end = 0; farthest < time; ++result, end = farthest)
    {
        while ((i < n) && (clips[i][0] <= end))
            farthest = std::max(farthest, clips[i++][1]);
        if (end == farthest) return -1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > clips,
          int time,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = videoStitching(clips, time);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 2}, {4, 6}, {8, 10}, {1, 9}, {1, 5}, {5, 9}}, 10, 3, trials);
    test({{0, 1}, {1, 2}}, 5, -1, trials);
    test({{0, 1}, {6, 8}, {0, 2}, {5, 6}, {0, 4}, {0, 3}, {6, 7}, {1, 3}, {4, 7},
          {1, 4}, {2, 5}, {2, 6}, {3, 4}, {4, 5}, {5, 7}, {6, 9}}, 9, 3, trials);
    return 0;
}


