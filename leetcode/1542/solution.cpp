#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestAwesome(std::string s)
{
    const int n = static_cast<int>(s.size());
    int result = 0, prefix = 0;
    std::vector<int> prefix_to_index(1024, n);
    prefix_to_index[0] = -1;
    
    for (int i = 0; i < n; ++i)
    {
        prefix ^= 1 << (s[i] - '0');
        result = std::max(result, i - prefix_to_index[prefix]);
        for (int j = 0; j < 10; ++j)
            result = std::max(result, i - prefix_to_index[prefix ^ 1 << j]);
        prefix_to_index[prefix] = std::min(prefix_to_index[prefix], i);
    }
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestAwesome(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("3242415", 5, trials);
    test("12345678", 1, trials);
    test("213123", 6, trials);
    return 0;
}


