#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

std::vector<std::string> twoEditWords(std::vector<std::string> queries,
                                      std::vector<std::string> dictionary)
{
    std::vector<std::string> result;
    for (const auto &query : queries)
    {
        for (const auto &word : dictionary)
        {
            if (std::inner_product(query.begin(), query.end(), word.begin(), 0,
                                   std::plus<>(), std::not_equal_to<char>()) < 3)
            {
                result.push_back(query);
                break;
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> queries,
          std::vector<std::string> dictionary,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = twoEditWords(queries, dictionary);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"word", "note", "ants", "wood"}, {"wood", "joke", "moat"},
         {"word", "note", "wood"}, trials);
    test({"yes"}, {"not"}, {}, trials);
    return 0;
}


