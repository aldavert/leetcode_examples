#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> resultArray(std::vector<int> nums)
{
    class BinaryIndexedTree
    {
        int m_size;
        std::vector<int> m_content;
    public:
        BinaryIndexedTree(int n) :
            m_size(n), m_content(n + 1) {}
        void update(int value, int delta)
        {
            for (; value <= m_size; value += value & -value)
                m_content[value] += delta;
        }
        int query(int value) const
        {
            int s = 0;
            for (; value > 0; value -= value & -value) s += m_content[value];
            return s;
        }
    };
    const int n = static_cast<int>(nums.size());
    std::vector<int> sorted_nums = nums;
    BinaryIndexedTree tree1(n + 1), tree2(n + 1);
    auto dist = [&](int value) -> int
    {
        return static_cast<int>(std::distance(sorted_nums.begin(),
            std::lower_bound(sorted_nums.begin(), sorted_nums.end(), value))) + 1;
    };
    
    std::sort(sorted_nums.begin(), sorted_nums.end());
    tree1.update(dist(nums[0]), 1);
    tree2.update(dist(nums[1]), 1);
    std::vector<int> arr1 = {nums[0]}, arr2 = {nums[1]};
    for (int k = 2; k < n; ++k)
    {
        int x = dist(nums[k]),
            a = static_cast<int>(arr1.size()) - tree1.query(x),
            b = static_cast<int>(arr2.size()) - tree2.query(x);
        if (a > b)
        {
            arr1.push_back(nums[k]);
            tree1.update(x, 1);
        }
        else if (a < b)
        {
            arr2.push_back(nums[k]);
            tree2.update(x, 1);
        }
        else if (arr1.size() <= arr2.size())
        {
            arr1.push_back(nums[k]);
            tree1.update(x, 1);
        }
        else
        {
            arr2.push_back(nums[k]);
            tree2.update(x, 1);
        }
    }
    arr1.insert(arr1.end(), arr2.begin(), arr2.end());
    return arr1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = resultArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3, 3}, {2, 3, 1, 3}, trials);
    test({5, 14, 3, 1, 2}, {5, 3, 1, 2, 14}, trials);
    test({3, 3, 3, 3}, {3, 3, 3, 3}, trials);
    return 0;
}


