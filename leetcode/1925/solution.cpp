#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countTriples(int n)
{
    std::vector<bool> valid(n * n, false);
    for (int k = 1; k < n; ++k)
        valid[k * k] = true;
    int result = 0;
    for (int k = 1; k <= n; ++k)
    {
        int k2 = k * k;
        for (int j = 1; j < k; ++j)
            result += valid[k2 - j * j];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countTriples(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 2, trials);
    test(10, 4, trials);
    test(250, 330, trials);
    return 0;
}


