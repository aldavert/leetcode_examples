#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> getStrongest(std::vector<int> arr, int k)
{
    const int mid_index = static_cast<int>(arr.size() - 1) / 2;
    std::nth_element(arr.begin(), arr.begin() + mid_index, arr.end());
    const int median = arr[mid_index];
    
    std::nth_element(arr.begin(), arr.begin() + k, arr.end(), [&](int a, int b) {
        int da = std::abs(a - median);
        int db = std::abs(b - median);
        return da == db ? a > b : da > db;
    });
    arr.resize(k);
    return arr;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left,
                const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<int> left_work(left), right_work(right);
    std::sort(left_work.begin(), left_work.end());
    std::sort(right_work.begin(), right_work.end());
    for (size_t i = 0; i < left_work.size(); ++i)
        if (left_work[i] != right_work[i])
            return false;
    return true;
}

void test(std::vector<int> arr,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getStrongest(arr, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 2, {5, 1}, trials);
    test({1, 1, 3, 5, 5}, 2, {5, 5}, trials);
    test({6, 7, 11, 7, 6, 8}, 5, {11, 8, 6, 6, 7}, trials);
    return 0;
}


