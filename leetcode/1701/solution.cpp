#include "../common/common.hpp"

// ############################################################################
// ############################################################################

double averageWaitingTime(std::vector<std::vector<int> > customers)
{
    double wait = 0, current = 0;
    
    for (const auto &customer : customers)
    {
        current = std::max(current, 1.0 * customer[0]) + customer[1];
        wait += current - customer[0];
    }
    return 1.0 * wait / static_cast<double>(customers.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > customers,
          double solution,
          unsigned int trials = 1)
{
    double result = -1.0;
    for (unsigned int i = 0; i < trials; ++i)
        result = averageWaitingTime(customers);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 5}, {4, 3}}, 5.0, trials);
    test({{5, 2}, {5, 4}, {10, 3}, {20, 1}}, 3.25, trials);
    return 0;
}


