#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool isPerfectSquare(int num)
{
    if (num < 1) return false;
    int left = 1, right = num;
    while (left < right)
    {
        int mid = left + (right - left) / 2;
        if (mid >= num / mid) right = mid;
        else left = mid + 1;
    }
    return left * left == num;
}
#else
bool isPerfectSquare(int num)
{
    if (num < 1) return false;
    int i = 1, j = 1;
    while (j < num)
    {
        ++i;
        j = i * i;
    }
    return j == num;
}
#endif

// ############################################################################
// ############################################################################

void test(int num, bool solution, unsigned int trials = 1)
{
    bool result = false;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPerfectSquare(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(16,  true, trials);
    test(14, false, trials);
    return 0;
}


