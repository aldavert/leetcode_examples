#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

bool areNumbersAscending(std::string s)
{
    int previous = -1, number = 0;
    bool is_digit = false;
    for (char letter : s)
    {
        if ((letter >= '0') && (letter <= '9'))
        {
            number = number * 10 + static_cast<int>(letter - '0');
            is_digit = true;
        }
        else if (is_digit)
        {
            if (number <= previous) return false;
            previous = std::exchange(number, 0);
            is_digit = false;
        }
    }
    return !is_digit || (is_digit && (number > previous));
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = areNumbersAscending(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1 box has 3 blue 4 red 6 green and 12 yellow marbles", true, trials);
    test("hello world 5 x 5", false, trials);
    test("sunset is at 7 51 pm overnight lows will be in the low 50 and 60 s", false, trials);
    return 0;
}



