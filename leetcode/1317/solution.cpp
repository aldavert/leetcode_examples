#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> getNoZeroIntegers(int n)
{
    auto NoZero = [](int m) -> bool
    {
        if (m == 0) return 0;
        for (; m > 0; m /= 10)
            if (m % 10 == 0) return false;
        return true;
    };
    for (int i = 1; i <= n / 2; ++i)
    {
        int a = i, b = n - i;
        if (NoZero(a) && NoZero(b))
            return {a, b};
    }
    return {-1, -1};
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getNoZeroIntegers(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test( 2, {1, 1}, trials);
    test(11, {2, 9}, trials);
    return 0;
}


