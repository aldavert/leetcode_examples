#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 0
int maxScore(std::vector<int> &cardPoints, int k)
{
    const int window_size = static_cast<int>(cardPoints.size()) - k;
    if (window_size == 0)
    {
        int total = 0;
        for (const auto &v : cardPoints)
            total += v;
        return total;
    }
    int window_value = 0, total = 0, idx;
    for (idx = 0; idx < window_size; ++idx)
        window_value += cardPoints[idx];
    int min_value = total = window_value;
    const int size = static_cast<int>(cardPoints.size());
    for (int previous = 0; idx < size; ++previous, ++idx)
    {
        window_value += (total += cardPoints[idx]) - cardPoints[previous];
        if (window_value < min_value)
            min_value = window_value;
    }
    return total - min_value;
}
#else
int maxScore(std::vector<int> &cardPoints, int k)
{
    const int window_size = static_cast<int>(cardPoints.size()) - k;
    int total = 0;
    for (const auto &v : cardPoints)
        total += v;
    if (window_size == 0) return total;
    int window_value = 0, idx;
    for (idx = 0; idx < window_size; ++idx)
        window_value += cardPoints[idx];
    int min_value = window_value;
    const int size = static_cast<int>(cardPoints.size());
    for (int previous = 0; idx < size; ++previous, ++idx)
    {
        window_value += cardPoints[idx] - cardPoints[previous];
        if (window_value < min_value)
            min_value = window_value;
    }
    return total - min_value;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> cardPoints, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxScore(cardPoints, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 2'000'000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6, 1}, 3, 12, trials);
    test({2, 2, 2}, 2, 4, trials);
    test({9, 7, 7, 9, 7, 7, 9}, 7, 55, trials);
    test({1, 1000, 1}, 1, 1, trials);
    test({1, 79, 80, 1, 1, 1, 200, 1}, 3, 202, trials);
    test({100, 40, 17, 9, 73, 75}, 3, 248, trials);
    return 0;
}


