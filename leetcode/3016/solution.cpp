#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumPushes(std::string word)
{
    int count[26] = {}, result = 0;
    for (char c : word) ++count[c - 'a'];
    std::sort(&count[0], &count[26], std::greater<>());
    for (int i = 0; i < 26; ++i)
        result += count[i] * (i / 8 + 1);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumPushes(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcde", 5, trials);
    test("xyzxyzxyzxyz", 12, trials);
    test("aabbccddeeffgghhiiiiii", 24, trials);
    return 0;
}


