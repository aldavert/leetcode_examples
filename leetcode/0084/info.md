# Largest Rectangle in Histogram

Given an array of integers `heights` representing the histogram's bar height where the width of each bar is `1`, return *the area of the largest rectangle in the histogram*.

#### Example 1:
> *Input:* `heights = [2, 1, 5, 6, 2, 3]`  
> *Output:* `10`  
> *Explanation:* The above is a histogram where width of each bar is `1`. The largest rectangle is formed by the `3^{rd}` and `4^{th}` columns, which has an `area = 2 * min(5, 6) = 10` units.

#### Example 2:
> *Input:* `heights = [2, 4]`  
> *Output:* `4`
 
#### Constraints:
- `1 <= heights.length <= 10^5`
- `0 <= heights[i] <= 10^4`


