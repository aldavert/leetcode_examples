#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int largestRectangleArea(std::vector<int> heights)
{
    const int n = static_cast<int>(heights.size());
    int result = 0;
    std::vector<int> stack;
    stack.reserve(n + 1);
    heights.push_back(0);
    for (int i = 0; i <= n; ++i)
    {
        while (stack.size() && (heights[stack.back()] > heights[i]))
        {
            int h = heights[stack.back()];
            stack.pop_back();
            int length = (stack.size())?(i - stack.back() - 1):i;
            result = std::max(result, h * length);
        }
        stack.push_back(i);
    }
    return result;
}
#else
int largestRectangleArea(std::vector<int> heights)
{
    if (heights.empty()) return 0;
    const int cols = static_cast<int>(heights.size());
    std::vector<int> left(cols), right(cols), height(cols);
    int result = 0, maximum_height = 0;
    
    for (int i = 0; i < cols; ++i)
    {
        left[i] = height[i] = 0;
        right[i] = cols;
        maximum_height = std::max(maximum_height, heights[i]);
    }
    for (int current_height = maximum_height; current_height > 0; --current_height)
    {
        int right_smallest_zero = cols;
        
        for (int j = cols - 1; j >= 0; --j)
        {
            if (current_height <= heights[j])
                right[j] = std::min(right[j], right_smallest_zero);
            else
            {
                right[j] = cols;
                right_smallest_zero = j;
            }
        }
        int left_largest_one = 0;
        for (int j = 0; j < cols; ++j)
        {
            if (current_height <= heights[j])
            {
                ++height[j];
                left[j] = std::max(left[j], left_largest_one);
            }
            else
            {
                height[j] = 0;
                left[j] = 0;
                left_largest_one = j + 1;
            }
            result = std::max(result, height[j] * (right[j] - left[j]));
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> heights, int solution, unsigned int trials = 1)
{
    int result = 0;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestRectangleArea(heights);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 5, 6, 2, 3}, 10, trials);
    test({2, 2, 5, 6, 2, 3}, 12, trials);
    test({2, 4}, 4, trials);
    return 0;
}


