#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxIceCream(std::vector<int> costs, int coins)
{
    std::sort(costs.begin(), costs.end());
    for (int i = 0, n = static_cast<int>(costs.size()); i < n; ++i)
    {
        if (costs[i] > coins) return i;
        coins -= costs[i];
    }
    return static_cast<int>(costs.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> costs, int coins, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxIceCream(costs, coins);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 4, 1}, 7, 4, trials);
    test({10, 6, 8, 7, 7, 8}, 5, 0, trials);
    test({1, 6, 3, 1, 2, 5}, 20, 6, trials);
    return 0;
}


