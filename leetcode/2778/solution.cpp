#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sumOfSquares(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = 0;
    for (int i = 0; i < n; ++i)
        if (n % (i + 1) == 0)
            result += nums[i] * nums[i];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOfSquares(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 21, trials);
    test({2, 7, 1, 19, 18, 3}, 63, trials);
    return 0;
}


