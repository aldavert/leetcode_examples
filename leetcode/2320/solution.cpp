#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int countHousePlacements(int n)
{
    constexpr int MOD = 1'000'000'007;
    int total = 2;
    for (int i = 2, house = 1, space = 1; i <= n; ++i)
    {
        house = std::exchange(space, total);
        total = (house + space) % MOD;
    }
    return static_cast<int>(static_cast<long>(total) * total % MOD);
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countHousePlacements(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 4, trials);
    test(2, 9, trials);
    return 0;
}


