#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> decode(std::vector<int> encoded)
{
    const int n = static_cast<int>(encoded.size()) + 1;
    int n_xors = 0, running_xors = 0, xors = 0;
    
    for (int i = 1; i <= n; i++) n_xors ^= i;
    for (const int encode : encoded)
    {
        running_xors ^= encode;
        xors ^= running_xors;
    }
    std::vector<int> result{xors ^ n_xors};
    for (const int encode : encoded)
        result.push_back(result.back() ^ encode);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> encoded, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = decode(encoded);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1}, {1, 2, 3}, trials);
    test({6, 5, 4, 6}, {2, 4, 1, 5, 3}, trials);
    return 0;
}


