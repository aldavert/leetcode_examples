#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

double largestSumOfAverages(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    double dp[102][102] = {}, prefix[102] = {};
    
    std::partial_sum(nums.begin(), nums.end(), &prefix[1]);
    for (int i = 1; i <= n; ++i)
        dp[i][1] = prefix[i] / static_cast<double>(i);
    for (int m = 2; m <= k; ++m)
        for (int i = m; i <= n; ++i)
            for (int j = m - 1; j < i; ++j)
                dp[i][m] = std::max(dp[i][m], dp[j][m - 1]
                         + (prefix[i] - prefix[j]) / static_cast<double>(i - j));
    return dp[n][k];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, double solution, unsigned int trials = 1)
{
    double result = -1.0;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestSumOfAverages(nums, k);
    showResult(std::abs(solution - result) < 1e-6, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({9, 1, 2, 3, 9}, 3, 20.0, trials);
    test({1,2, 3, 4, 5, 6, 7}, 4, 20.5, trials);
    return 0;
}


