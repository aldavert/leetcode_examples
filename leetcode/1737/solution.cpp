#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minCharacters(std::string a, std::string b)
{
    const int n_a = static_cast<int>(a.length()),
              n_b = static_cast<int>(b.length());
    int count_a[26] = {}, count_b[26] = {};
    
    for (char c : a) ++count_a[c - 'a'];
    for (char c : b) ++count_b[c - 'a'];
    int result = std::numeric_limits<int>::max(), prev_a = 0, prev_b = 0;
    for (char c = 'a'; c <= 'z'; ++c)
    {
        result = std::min(result, n_a + n_b - count_a[c - 'a'] - count_b[c - 'a']);
        if (c > 'a')
            result = std::min({result, n_a - prev_a + prev_b, n_b - prev_b + prev_a});
        prev_a += count_a[c - 'a'];
        prev_b += count_b[c - 'a'];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string a, std::string b, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCharacters(a, b);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aba", "caa", 2, trials);
    test("dabadd", "cda", 3, trials);
    return 0;
}


