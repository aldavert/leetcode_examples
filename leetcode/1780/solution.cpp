#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkPowersOfThree(int n)
{
    for (; n > 1; n /= 3)
        if (n % 3 == 2)
            return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(int n, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkPowersOfThree(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(12, true, trials);
    test(91, true, trials);
    test(21, false, trials);
    return 0;
}


