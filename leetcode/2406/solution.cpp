#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minGroups(std::vector<std::vector<int> > intervals)
{
    std::priority_queue<int, std::vector<int>, std::greater<>> heap;
    std::sort(intervals.begin(), intervals.end());
    for (const auto &interval : intervals)
    {
        if (!heap.empty() && (interval[0] > heap.top()))
            heap.pop();
        heap.push(interval[1]);
    }
    return static_cast<int>(heap.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > intervals,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minGroups(intervals);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{5, 10}, {6, 8}, {1, 5}, {2, 3}, {1, 10}}, 3, trials);
    test({{1, 3}, {5, 6}, {8, 10}, {11, 13}}, 1, trials);
    return 0;
}


