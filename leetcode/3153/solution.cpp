#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long sumDigitDifferences(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    const int digit_size = static_cast<int>(std::to_string(nums[0]).size());
    long result = 0;
    
    for (int i = 0, denominator = 1; i < digit_size; ++i, denominator *= 10)
    {
        int count[10] = {};
        for (int num : nums)
            ++count[num / denominator % 10];
        for (int j = 0; j < 10; ++j)
            result += static_cast<long>(count[j]) * static_cast<long>(n - count[j]);
    }
    return result / 2;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumDigitDifferences(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({13, 23, 12}, 4, trials);
    test({10, 10, 10, 10}, 0, trials);
    return 0;
}


