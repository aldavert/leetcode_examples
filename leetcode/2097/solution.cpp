#include "../common/common.hpp"
#include <unordered_map>
#include <stack>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > validArrangement(std::vector<std::vector<int> > pairs)
{
    std::vector<std::vector<int> > result(pairs.size());
    std::unordered_map<int, std::stack<int> > graph;
    std::unordered_map<int, int> out_degree;
    std::unordered_map<int, int> in_degree;
    int index = static_cast<int>(pairs.size());
    auto startNode = [&](void) -> int
    {
        for (const auto &[u, _] : graph)
            if (out_degree[u] - in_degree[u] == 1)
                return u;
        return pairs[0][0];
    };
    auto euler = [&](auto &&self, int u) -> void
    {
        auto &stack = graph[u];
        while (!stack.empty())
        {
            int v = stack.top();
            stack.pop();
            self(self, v);
            result[--index] = {u, v};
        }
    };

    for (const auto &pair : pairs)
    {
        graph[pair[0]].push(pair[1]);
        ++out_degree[pair[0]];
        ++in_degree[pair[1]];
    }
    euler(euler, startNode());
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > pairs,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = validArrangement(pairs);
    bool valid = true;
    for (size_t i = 1; i < result.size(); ++i)
        valid = valid && (result[i][0] == result[i - 1][1]);
    showResult((solution == result) || valid, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{5, 1}, {4, 5}, {11, 9}, {9, 4}}, {{11, 9}, {9, 4}, {4, 5}, {5, 1}}, trials);
    test({{1, 3}, {3, 2}, {2, 1}}, {{1, 3}, {3, 2}, {2, 1}}, trials);
    test({{1, 2}, {1, 3}, {2, 1}}, {{1, 2}, {2, 1}, {1, 3}}, trials);
    return 0;
}


