#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minChanges(std::string s)
{
    int result = 0;
    for (size_t i = 0; i + 1 < s.size(); i += 2)
        result += (s[i] != s[i + 1]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minChanges(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1001", 2, trials);
    test("10", 1, trials);
    test("0000", 0, trials);
    return 0;
}


