#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxProductPath(std::vector<std::vector<int> > grid)
{
    constexpr int MOD = 1'000'000'007;
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    std::vector<std::vector<long> > dp_min(m, std::vector<long>(n)),
                                    dp_max(m, std::vector<long>(n));
    
    dp_min[0][0] = dp_max[0][0] = grid[0][0];
    for (int i = 1; i < m; ++i)
        dp_min[i][0] = dp_max[i][0] = dp_min[i - 1][0] * grid[i][0];
    for (int j = 1; j < n; ++j)
        dp_min[0][j] = dp_max[0][j] = dp_min[0][j - 1] * grid[0][j];
    for (int i = 1; i < m; ++i)
    {
        for (int j = 1; j < n; ++j)
        {
            if (grid[i][j] < 0)
            {
                dp_min[i][j] = std::max(dp_max[i - 1][j], dp_max[i][j - 1]) * grid[i][j];
                dp_max[i][j] = std::min(dp_min[i - 1][j], dp_min[i][j - 1]) * grid[i][j];
            }
            else
            {
                dp_min[i][j] = std::min(dp_min[i - 1][j], dp_min[i][j - 1]) * grid[i][j];
                dp_max[i][j] = std::max(dp_max[i - 1][j], dp_max[i][j - 1]) * grid[i][j];
            }
        }
    }
    long max_i = std::max(dp_min.back().back(), dp_max.back().back());
    return (max_i < 0)?-1:static_cast<int>(max_i % MOD);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxProductPath(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{-1, -2, -3}, {-2, -3, -3}, {-3, -3, -2}}, -1, trials);
    test({{1, -2, 1}, {1, -2, 1}, {3, -4, 1}}, 8, trials);
    test({{1, 3}, {0, -4}}, 0, trials);
    return 0;
}


