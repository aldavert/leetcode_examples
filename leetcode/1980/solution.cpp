#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::string findDifferentBinaryString(std::vector<std::string> nums)
{
    const size_t n = nums.size();
    std::string result;
    for (size_t i = 0; i < n; ++i)
        result += '0' + (nums[i][i] == '0');
    return result;
}
#else
std::string findDifferentBinaryString(std::vector<std::string> nums)
{
    const size_t n = nums.size();
    std::vector<bool> available(1 << n, true);
    for (size_t i = 0; i < n; ++i)
    {
        int value = 0;
        for (size_t j = 0; j < n; ++j, value <<= 1)
            value += nums[i][j] == '1';
        available[value >> 1] = false;
    }
    for (size_t i = 0; i < available.size(); ++i)
    {
        if (available[i])
        {
            std::string result(n, '0');
            std::stack<bool> q;
            for (size_t work = i; work > 0; work /= 2)
                q.push(work & 1);
            for (size_t j = n - q.size(); !q.empty(); ++j)
            {
                result[j] += q.top();
                q.pop();
            }
            return result;
        }
    }
    return "";
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> nums,
          std::set<std::string> solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findDifferentBinaryString(nums);
    showResult(solution.find(result) != solution.end(), solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"01", "10"}, {"11", "00"}, trials);
    test({"00", "01"}, {"11", "10"}, trials);
    test({"00", "10"}, {"11", "01"}, trials);
    test({"111", "011", "001"}, {"101", "000", "010", "100", "110"}, trials);
    return 0;
}


