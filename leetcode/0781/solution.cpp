#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int numRabbits(std::vector<int> answers)
{
    std::unordered_map<int, int> histogram;
    for (int answer : answers)
        ++histogram[answer + 1];
    int result = 0;
    for (auto [group_size, number_of_rabits] : histogram)
        result += group_size * (number_of_rabits / group_size
                             + (number_of_rabits % group_size != 0));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> answer, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numRabbits(answer);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2}, 5, trials);
    test({10, 10, 10}, 11, trials);
    return 0;
}


