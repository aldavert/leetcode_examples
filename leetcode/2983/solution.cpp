#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<bool> canMakePalindromeQueries(std::string s,
                                           std::vector<std::vector<int> > queries)
{
    auto subtract =
        [](const std::vector<int> &l, const std::vector<int> &r) -> std::vector<int>
    {
        std::vector<int> d(26);
        for (int i = 0; i < 26; ++i)
            d[i] = l[i] - r[i];
        return d;
    };
    auto cmp = [](int freq) { return freq >= 0; };
    const int n = static_cast<int>(s.size());
    std::vector<int> mirrored_diffs(1);
    for (int i = 0, j = n - 1; i < j; ++i, --j)
        mirrored_diffs.push_back(mirrored_diffs.back() + (s[i] != s[j]));
    std::vector<std::vector<int> > counts;
    std::vector<int> histogram(26);
    counts.push_back(histogram);
    for (char c : s)
    {
        ++histogram[c - 'a'];
        counts.push_back(histogram);
    }
    std::vector<bool> result;
    
    for (const auto &query : queries)
    {
        const int a = query[0], b = query[1] + 1, c = query[2], d = query[3] + 1;
        const int ra = n - a, rb = n - b, rc = n - c, rd = n - d;
        
        if (((std::min(a, rd) > 0) && (mirrored_diffs[std::min(a, rd)] > 0))
        || ((n / 2 > std::max(b, rc)) && (mirrored_diffs[n / 2]
                                        - mirrored_diffs[std::max(b, rc)] > 0))
        || ((rd > b) && (mirrored_diffs[rd] - mirrored_diffs[b] > 0))
        || ((a > rc) && (mirrored_diffs[a] - mirrored_diffs[rc] > 0)))
        {
            result.push_back(false);
            continue;
        }
        std::vector<int> left_count = subtract(counts[b], counts[a]),
                         right_count = subtract(counts[d], counts[c]);
        if (a > rd)
            right_count = subtract(right_count,
                                   subtract(counts[std::min(a, rc)], counts[rd]));
        if (rc > b)
            right_count = subtract(right_count,
                                   subtract(counts[rc], counts[std::max(b, rd)]));
        if (c > rb)
            left_count = subtract(left_count,
                                  subtract(counts[std::min(c, ra)], counts[rb]));
        if (ra > d)
            left_count = subtract(left_count,
                                  subtract(counts[ra], counts[std::max(d, rb)]));
        result.push_back(std::all_of(left_count.begin(), left_count.end(), cmp)
                      && std::all_of(right_count.begin(), right_count.end(), cmp)
                      && (left_count == right_count));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<std::vector<int> > queries,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = canMakePalindromeQueries(s, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcabc", {{1, 1, 3, 5}, {0, 2, 5, 5}}, {true, true}, trials);
    test("abbcdecbba", {{0, 2, 7, 9}}, {false}, trials);
    test("acbcab", {{1, 2, 4, 5}}, {true}, trials);
    return 0;
}


