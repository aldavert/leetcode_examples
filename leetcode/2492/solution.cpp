#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minScore(int n, std::vector<std::vector<int> > roads)
{
    std::vector<int> id(n + 1);
    for (int i = 0; i <= n; ++i) id[i] = i;
    auto find = [&](auto &&self, int idx) -> int
    {
        return (id[idx] != idx)?id[idx] = self(self, id[idx]):idx;
    };
    for (const auto &road : roads)
    {
        int u = find(find, road[0]);
        int v = find(find, road[1]);
        id[u] = id[v] = std::min(u, v);
    }
    for (int i = 1; i <= n; ++i) find(find, i);
    int result = 100'000;
    for (const auto &road : roads)
        if ((id[road[0]] == 1) || (id[road[1]] == 1))
            result = std::min(result, road[2]);
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > roads,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minScore(n, roads);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{1, 2, 9}, {2, 3, 6}, {2, 4, 5}, {1, 4, 7}}, 5, trials);
    test(4, {{1, 2, 2}, {1, 3, 4}, {3, 4, 7}}, 2, trials);
    test(7, {{1, 3, 1484}, {3, 2, 3876}, {2, 4, 6823},
             {6, 7,  579}, {5, 6, 4436}, {4, 5, 8830}}, 579, trials);
    return 0;
}


