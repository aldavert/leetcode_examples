#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int scoreOfString(std::string s)
{
    int result = 0;
    for (size_t i = 1; i < s.size(); ++i)
        result += std::abs(static_cast<int>(s[i]) - static_cast<int>(s[i - 1]));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = scoreOfString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("hello", 13, trials);
    test("zaz", 50, trials);
    return 0;
}


