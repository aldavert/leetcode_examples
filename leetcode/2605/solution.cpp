#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minNumber(std::vector<int> nums1, std::vector<int> nums2)
{
    bool present1[10] = {}, present2[10] = {};
    int min1 = 9, min2 = 9;
    for (size_t i = 0; i < nums1.size(); ++i)
    {
        present1[nums1[i]] = true;
        min1 = std::min(min1, nums1[i]);
    }
    for (size_t i = 0; i < nums2.size(); ++i)
    {
        present2[nums2[i]] = true;
        min2 = std::min(min2, nums2[i]);
    }
    for (int i = 0; i < 10; ++i)
        if (present1[i] && present2[i]) return i;
    return (min1 < min2)?(min1 * 10 + min2):(min2 * 10 + min1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minNumber(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 1, 3}, {5, 7}, 15, trials);
    test({3, 5, 2, 6}, {3, 1, 7}, 3, trials);
    test({4, 1, 9}, {5, 9}, 9, trials);
    return 0;
}


