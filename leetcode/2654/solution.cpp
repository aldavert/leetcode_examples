#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size()),
              ones = static_cast<int>(std::count(nums.begin(), nums.end(), 1));
    if (ones > 0) return n - ones;
    int min_ops = std::numeric_limits<int>::max();
    for (int i = 0; i < n; ++i)
    {
        int current = nums[i];
        for (int j = i + 1; j < n; ++j)
        {
            current = std::gcd(current, nums[j]);
            if (current == 1)
            {
                min_ops = std::min(min_ops, j - i);
                break;
            }
        }
    }
    return (min_ops == std::numeric_limits<int>::max())?-1:min_ops + n - 1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 6, 3, 4}, 4, trials);
    test({2, 10, 6, 14}, -1, trials);
    return 0;
}


