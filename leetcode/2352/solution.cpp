#include "../common/common.hpp"
#include <unordered_map>
#include <cstring>

// ############################################################################
// ############################################################################

int equalPairs(std::vector<std::vector<int> > grid)
{
    struct KeyHash
    {
        size_t operator()(const std::vector<int> &vec) const
        {
            size_t seed = vec.size();
            for (int xi : vec)
            {
                //size_t x = std::bit_cast<unsigned int>(xi);
                // Leetcode doesn't accept bit_cast!
                unsigned int xui;
                std::memcpy(&xui, &xi, sizeof(unsigned int));
                size_t x = xui;
                
                x = ((x >> 16) ^ x) * 0x45d9f3b;
                x = ((x >> 16) ^ x) * 0x45d9f3b;
                x = (x >> 16) ^ x;
                seed ^= x + 0x9e3779b9 + (seed << 6) + (seed >> 2);
            }
            return seed;
        }
    };
    struct KeyEqual
    {
        bool operator()(const std::vector<int> &l, const std::vector<int> &r) const
        {
            if (l.size() != r.size()) return false;
            for (size_t i = 0; i < l.size(); ++i)
                if (l[i] != r[i]) return false;
            return true;
        }
    };
    std::unordered_map<std::vector<int>, int, KeyHash, KeyEqual> histogram;
    for (const auto &row : grid)
        ++histogram[row];
    std::vector<int> column(grid.size());
    int result = 0;
    for (size_t i = 0; i < grid.size(); ++i)
    {
        for (size_t j = 0; j < grid.size(); ++j)
            column[j] = grid[j][i];
        result += histogram[column];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = equalPairs(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 2, 1}, {1, 7, 6}, {2, 7, 7}}, 1, trials);
    test({{3, 1, 2, 2}, {1, 4, 4, 5}, {2, 4, 2, 2}, {2, 4, 2, 2}}, 3, trials);
    return 0;
}


