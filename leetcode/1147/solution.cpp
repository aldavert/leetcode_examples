#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestDecomposition(std::string text)
{
    int count = 0, l = 0;
    for (int r = 1, n = static_cast<int>(text.size()); 2 * r <= n; ++r)
        if (std::equal(text.begin() + l, text.begin() + r, text.end() - r))
            count += 2,
            l = r;
    return count + (2 * l < static_cast<int>(text.size()));
}

// ############################################################################
// ############################################################################

void test(std::string text, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestDecomposition(text);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ghiabcdefhelloadamhelloabcdefghi", 7, trials);
    test("merchant", 1, trials);
    test("antaprezatepzapreanta", 11, trials);
    return 0;
}


