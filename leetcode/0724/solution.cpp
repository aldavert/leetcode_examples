#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int pivotIndex(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int total = 0;
    for (int num : nums)
        total += num;
    for (int i = 0, left = 0; i < n; left += nums[i++])
        if (total - left - nums[i] == left)
            return i;
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = pivotIndex(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 7, 3, 6, 5, 6}, 3, trials);
    test({1, 2, 3}, -1, trials);
    test({2, 1, -1}, 0, trials);
    return 0;
}


