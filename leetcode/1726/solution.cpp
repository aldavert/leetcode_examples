#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int tupleSameProduct(std::vector<int> nums)
{
    std::unordered_map<int, int> count;
    int result = 0;
    
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        for (int j = 0; j < i; ++j)
            result += count[nums[i] * nums[j]]++ * 8;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = tupleSameProduct(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 4, 6}, 8, trials);
    test({1, 2, 4, 5, 10}, 16, trials);
    return 0;
}


