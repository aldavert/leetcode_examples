#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string subStrHash(std::string s, int power, int modulo, int k, int hashValue)
{
    const int n = static_cast<int>(s.size());
    long max_power = 1, hashed = 0;
    int best_left = -1;
    auto val = [](char c) -> int { return c - 'a' + 1; };
    
    for (int i = n - 1; i >= 0; --i)
    {
        hashed = (hashed * power + val(s[i])) % modulo;
        if (i + k < n)
            hashed = (hashed - val(s[i + k]) * max_power % modulo + modulo) % modulo;
        else
            max_power = max_power * power % modulo;
        if (hashed == hashValue)
            best_left = i;
    }
    return s.substr(best_left, k);
}

// ############################################################################
// ############################################################################

void test(std::string s,
          int power,
          int modulo,
          int k,
          int hashValue,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = subStrHash(s, power, modulo, k, hashValue);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leetcode", 7, 20, 2, 0, "ee", trials);
    test("fbxzaad", 31, 100, 3, 32, "fbx", trials);
    return 0;
}


