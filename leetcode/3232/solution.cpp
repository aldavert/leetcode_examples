#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

bool canAliceWin(std::vector<int> nums)
{
    return std::accumulate(nums.begin(), nums.end(), 0,
            [](int subtotal, int num) { return subtotal + ((num < 10)?num:-num); }) != 0;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canAliceWin(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 10}, false, trials);
    test({1, 2, 3, 4, 5, 14}, true, trials);
    test({5, 5, 5, 25}, true, trials);
    return 0;
}


