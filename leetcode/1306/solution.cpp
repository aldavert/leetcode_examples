#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

bool canReach(std::vector<int> arr, int start)
{
    const int n = static_cast<int>(arr.size());
    std::vector<bool> not_visited(n, true);
    std::queue<int> position;
    
    position.push(start);
    while (!position.empty())
    {
        int current = position.front();
        position.pop();
        if (not_visited[current])
        {
            if (arr[current] == 0) return true;
            not_visited[current] = false;
            if (current - arr[current] >= 0) position.push(current - arr[current]);
            if (current + arr[current] < n) position.push(current + arr[current]);
        }
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int start, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canReach(arr, start);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 3, 0, 3, 1, 2}, 5, true, trials);
    test({4, 2, 3, 0, 3, 1, 2}, 0, true, trials);
    test({3, 0, 2, 1, 2}, 2, false, trials);
    return 0;
}


