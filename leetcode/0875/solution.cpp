#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minEatingSpeed(std::vector<int> piles, int h)
{
    int minimum = 1, maximum = 0;
    for (int p : piles)
        maximum = std::max(maximum, p);
    auto check = [&](int middle) -> bool
    {
        int time = 0;
        for (int b : piles)
            time += (b - 1) / middle + 1;
        return time <= h;
    };
    while (minimum < maximum)
    {
        if (int middle = (maximum + minimum) / 2; check(middle))
            maximum = middle;
        else minimum = middle + 1;
    }
    return maximum;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> piles, int h, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minEatingSpeed(piles, h);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 6, 7, 11}, 8, 4, trials);
    test({30, 11, 23, 4, 20}, 5, 30, trials);
    test({30, 11, 23, 4, 20}, 6, 23, trials);
    return 0;
}


