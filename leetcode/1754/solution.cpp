#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::string largestMerge(std::string word1, std::string word2)
{
    const int n = static_cast<int>(word1.size()),
              m = static_cast<int>(word2.size()),
              nm = n + m;
    std::string result(n + m, ' ');
    auto ge = [&](int i, int j)
    {
        while ((i < n) && (j < m) && (word1[i] == word2[j])) ++i, ++j;
        return word1[i] > word2[j];
    };
    
    for (int i = 0, j = 0, k = 0; k < nm;)
    {
        if (i == n) result[k++] = word2[j++];
        else if (j == m) result[k++] = word1[i++];
        else if (word1[i] > word2[j]) result[k++] = word1[i++];
        else if (word1[i] < word2[j]) result[k++] = word2[j++];
        else if (ge(i, j))
            while ((i < n) && (word1[i] == word2[j])) result[k++] = word1[i++];
        else while ((j < m) && (word1[i] == word2[j])) result[k++] = word2[j++];
    }
    return result;
}
#else
std::string largestMerge(std::string word1, std::string word2)
{
    if (word1.empty()) return word2;
    if (word2.empty()) return word1;
    return (word1 > word2)?word1[0] + largestMerge(word1.substr(1), word2)
                          :word2[0] + largestMerge(word1, word2.substr(1));
}
#endif

// ############################################################################
// ############################################################################

void test(std::string word1,
          std::string word2,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestMerge(word1, word2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("cabaa", "bcaaa", "cbcabaaaaa", trials);
    test("abcabc", "abdcaba", "abdcabcabcaba", trials);
    return 0;
}


