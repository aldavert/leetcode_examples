#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> canSeePersonsCount(std::vector<int> heights)
{
    const int n = static_cast<int>(heights.size());
    std::vector<int> result(n);
    std::stack<int> st;
    st.push(n - 1);
    for (int i = n - 2; i >= 0; --i)
    {
        while (!st.empty() && (heights[st.top()] <= heights[i]))
        {
            ++result[i];
            st.pop();
        }
        if (!st.empty()) ++result[i];
        st.push(i);
    }
    return result;
}
#else
std::vector<int> canSeePersonsCount(std::vector<int> heights)
{
    const int n = static_cast<int>(heights.size());
    std::vector<int> result(n);
    std::stack<int> stack;
    
    for (int i = 0; i < n; ++i)
    {
        while (!stack.empty() && (heights[stack.top()] <= heights[i]))
            ++result[stack.top()], stack.pop();
        if (!stack.empty()) ++result[stack.top()];
        stack.push(i);
    }
    
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> heights, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = canSeePersonsCount(heights);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 6, 8, 5, 11, 9}, {3, 1, 2, 1, 1, 0}, trials);
    test({5, 1, 2, 3, 10}, {4, 1, 1, 1, 0}, trials);
    return 0;
}


