#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<long long> countOfPairs(int n, int x, int y)
{
    --x, --y;
    if (x > y) std::swap(x, y);
    std::vector<long long> diff(n);
    for (int i = 0; i < n; ++i)
    {
        diff[0] += 1 + 1;
        ++diff[std::min(std::abs(i - x), std::abs(i - y) + 1)];
        ++diff[std::min(std::abs(i - y), std::abs(i - x) + 1)];
        --diff[std::min(std::abs(i - 0), std::abs(i - y) + 1 + std::abs(x - 0))];
        --diff[std::min(std::abs(i - (n - 1)),
                        std::abs(i - x) + 1 + std::abs(y - (n - 1)))];
        --diff[std::max(x - i, 0) + std::max(i - y, 0) + ((y - x) + 0) / 2];
        --diff[std::max(x - i, 0) + std::max(i - y, 0) + ((y - x) + 1) / 2];
    }
    for (int i = 0; i + 1 < n; ++i) diff[i + 1] += diff[i];
    return diff;
}

// ############################################################################
// ############################################################################

void test(int n, int x, int y, std::vector<long long> solution, unsigned int trials = 1)
{
    std::vector<long long> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countOfPairs(n, x, y);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 1, 3, {6, 0, 0}, trials);
    test(5, 2, 4, {10, 8, 2, 0, 0}, trials);
    test(4, 1, 1, {6, 4, 2, 0}, trials);
    return 0;
}


