#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int hIndex(std::vector<int> citations)
{
    const int n = static_cast<int>(citations.size());
    std::sort(citations.begin(), citations.end());
    for (int i = n - 1; i >= 0; --i)
        if (citations[i] < n - i) return n - i - 1;
    return n;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> citations, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = hIndex(citations);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 0, 6, 1, 5}, 3, trials);
    test({1, 3, 1}, 1, trials);
    test({100}, 1, trials);
    test({100, 102, 105}, 3, trials);
    test({4, 4, 0, 0}, 2, trials);
    test({10, 8, 5, 4, 3}, 4, trials);
    test({25, 8, 5, 3, 3}, 3, trials);
    return 0;
}


