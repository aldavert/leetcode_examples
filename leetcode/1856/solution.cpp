#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

int maxSumMinProduct(std::vector<int> nums)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(nums.size());
    std::vector<long> prefix(nums.size() + 1);
    std::stack<int> s;
    long result = 0;
    
    for (int i = 0; i < n; ++i)
        prefix[i + 1] = prefix[i] + nums[i];
    for (int i = 0; i <= n; ++i)
    {
        while (!s.empty() && ((i == n) || (nums[s.top()] > nums[i])))
        {
            int min_value = nums[s.top()];
            s.pop();
            long sum = s.empty()?prefix[i]:prefix[i] - prefix[s.top() + 1];
            result = std::max(result, min_value * sum);
        }
        s.push(i);
    }
    return static_cast<int>(result % MOD);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSumMinProduct(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 2}, 14, trials);
    test({2, 3, 3, 1, 2}, 18, trials);
    test({3, 1, 5, 6, 4, 2}, 60, trials);
    return 0;
}


