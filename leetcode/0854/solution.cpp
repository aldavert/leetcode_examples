#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int kSimilarity(std::string s1, std::string s2)
{
    const int n = static_cast<int>(s1.size());
    int result = 0;
    std::queue<std::string> q{{s1}};
    std::unordered_set<std::string> seen{{s1}};
    auto createChildren = [&](std::string current) -> void
    {
        int i = 0;
        while (current[i] == s2[i]) ++i;
        for (int j = i + 1; j < n; ++j)
        {
            if (current[j] == s2[i])
            {
                std::swap(current[i], current[j]);
                if (seen.find(current) == seen.end())
                {
                    q.push(current);
                    seen.insert(current);
                }
                std::swap(current[i], current[j]);
            }
        }
    };
    while (!q.empty())
    {
        for (size_t i = 0, m = q.size(); i < m; ++i)
        {
            std::string current = q.front();
            q.pop();
            if (current == s2) return result;
            createChildren(current);
        }
        ++result;
    }
    return -1;
}
#else
int kSimilarity(std::string s1, std::string s2)
{
    const int n = static_cast<int>(s1.size());
    auto heuristic = [&](void) -> int
    {
        int count = 0;
        for (int i = 0; i < n; ++i)
            count += s1[i] != s2[i];
        return (count + 1) / 2;
    };
    std::function<bool(int)> dfs = [&](int depth) -> bool
    {
        if (!depth) return s1 == s2;
        if (heuristic() > depth) return false;
        for (int i = 0; i < n; ++i)
        {
            if (s1[i] == s2[i]) continue;
            for (int j = i + 1; j < n; ++j)
            {
                if (s2[i] == s1[j])
                {
                    std::swap(s1[i], s1[j]);
                    if (dfs(depth - 1)) return true;
                    std::swap(s1[i], s1[j]);
                }
            }
        }
        return false;
    };
    if (s1 == s2) return 0;
    int depth = 1;
    while (!dfs(depth)) ++depth;
    return depth;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s1, std::string s2, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = kSimilarity(s1, s2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ab", "ba", 1, trials);
    test("abc", "bca", 2, trials);
    return 0;
}


