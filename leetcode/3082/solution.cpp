#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sumOfPower(std::vector<int> nums, int k)
{
    constexpr int MOD = 1'000'000'007;
    auto modPow = [&](auto &&self, long x, long v) -> long
    {
        if (v == 0) return 1;
        if (v % 2 == 1)
            return x * self(self, x % MOD, (v - 1)) % MOD;
        return self(self, x * x % MOD, (v / 2)) % MOD;
    };
    const int n = static_cast<int>(nums.size());
    std::vector<std::vector<long> > mem(n, std::vector<long>(k + 1, -1));
    auto sumOfPower = [&](auto &&self, int i, int j) -> long
    {
        if (j == 0)
            return modPow(modPow, 2, nums.size() - i);
        if ((i == n) || j < 0)
            return 0;
        if (mem[i][j] != -1)
            return mem[i][j];
        return mem[i][j] = (self(self, i + 1, j - nums[i])
                         +  2L * self(self, i + 1, j)) % MOD;
    };
    return static_cast<int>(sumOfPower(sumOfPower, 0, k));
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOfPower(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 3, 6, trials);
    test({2, 3, 3}, 5, 4, trials);
    test({1, 2, 3}, 7, 0, trials);
    return 0;
}


