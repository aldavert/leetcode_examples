#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int digArtifacts([[maybe_unused]] int n,
                 std::vector<std::vector<int> > artifacts,
                 std::vector<std::vector<int> > dig)
{
    std::unordered_set<int> digged;
    auto hash = [](int i, int j) -> int { return i << 16 | j; };
    auto canExtract = [&](const std::vector<int> &a) -> bool
    {
        for (int i = a[0]; i <= a[2]; ++i)
            for (int j = a[1]; j <= a[3]; ++j)
                if (!digged.contains(hash(i, j)))
                    return false;
        return true;
    };
    
    for (const auto &d : dig)
        digged.insert(hash(d[0], d[1]));
    return static_cast<int>(std::count_if(artifacts.begin(), artifacts.end(),
                                          [&](const auto &a) { return canExtract(a); }));
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > artifacts,
          std::vector<std::vector<int> > dig,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = digArtifacts(n, artifacts, dig);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {{0, 0, 0, 0}, {0, 1, 1, 1}}, {{0, 0}, {0, 1}}, 1, trials);
    test(2, {{0, 0, 0, 0}, {0, 1, 1, 1}}, {{0, 0}, {0, 1}, {1, 1}}, 2, trials);
    return 0;
}


