#include "../common/common.hpp"
#include <unordered_map>
#include <queue>

// ############################################################################
// ############################################################################

#if 0
int numOfMinutes(int n,
                 int /*headID*/,
                 std::vector<int> manager,
                 std::vector<int> informTime)
{
    auto dfs = [&](auto &&self, int i) -> int
    {
        if (manager[i] != -1)
        {
            informTime[i] += self(self, manager[i]);
            manager[i] = -1;
        }
        return informTime[i];
    };
    int result = 0;
    for (int i = 0; i < n; ++i)
        result = std::max(result, dfs(dfs, i));
    return result;
}
#else
int numOfMinutes(int n,
                 int headID,
                 std::vector<int> manager,
                 std::vector<int> informTime)
{
    std::unordered_map<int, std::vector<int> > manager_subordinates;
    for (int i = 0; i < n; ++i) manager_subordinates[manager[i]].push_back(i);
    std::queue<std::pair<int, int> > queue;
    int result = 0;
    queue.push({headID, informTime[headID]});
    while (!queue.empty())
    {
        for (int i = static_cast<int>(queue.size()); i > 0; --i)
        {
            auto [id, time] = queue.front();
            queue.pop();
            result = std::max(result, time);
            for (int subordinate : manager_subordinates[id])
                queue.push({subordinate, time + informTime[subordinate]});
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          int headID,
          std::vector<int> manager,
          std::vector<int> informTime,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numOfMinutes(n, headID, manager, informTime);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 0, {-1}, {0}, 0, trials);
    test(6, 2, {2, 2, -1, 2, 2, 2}, {0, 0, 1, 0, 0, 0}, 1, trials);
    return 0;
}


