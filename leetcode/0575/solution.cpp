#include "../common/common.hpp"

// ############################################################################
// ############################################################################
#if 1
int distributeCandies(std::vector<int> candyType)
{
    bool used[300'000] = {};
    size_t different = 0;
    for (int ct : candyType)
    {
        different += !used[ct + 150'000];
        used[ct + 150'000] = true;
    }
    return static_cast<int>(std::min(different, candyType.size() / 2));
}
#else
int distributeCandies(std::vector<int> candyType)
{
    std::unordered_set<int> types;
    for (int ct : candyType)
        types.insert(ct);
    return static_cast<int>(std::min(types.size(), candyType.size() / 2));
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> candyType, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = distributeCandies(candyType);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2, 2, 3, 3}, 3, trials);
    test({1, 1, 2, 3}, 2, trials);
    test({6, 6, 6, 6}, 1, trials);
    return 0;
}


