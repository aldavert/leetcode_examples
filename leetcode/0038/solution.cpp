#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string countAndSay(int n)
{
    if (n <= 1) return "1";
    else
    {
        std::string digits = countAndSay(n - 1);
        std::string result;
        char previous = digits[0];
        int count = 0;
        for (char c : digits)
        {
            if (c == previous) ++count;
            else
            {
                result += std::to_string(count) + previous;
                count = 1;
            }
            previous = c;
        }
        if (count > 0)
            result += std::to_string(count) + previous;
        return result;
    }
}

// ############################################################################
// ############################################################################

void test(int n, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countAndSay(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, "1", trials);
    test(2, "11", trials);
    test(3, "21", trials);
    test(4, "1211", trials);
    return 0;
}


