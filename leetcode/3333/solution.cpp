#include "../common/common.hpp"
#include <utility>
#include <numeric>

// ############################################################################
// ############################################################################

int possibleStringCount(std::string word, int k)
{
    constexpr int MOD = 1'000'000'007;
    auto acc_op1 = [&](long subtotal, int group) { return subtotal * group % MOD; };
    auto acc_op2 = [&](int subtotal, int count) { return (subtotal + count) % MOD; };
    std::vector<int> groups;
    int group = 1;
    for (int i = 1, n = static_cast<int>(word.size()); i < n; ++i)
    {
        if (word[i] == word[i - 1]) ++group;
        else groups.push_back(std::exchange(group, 1));
    }
    groups.push_back(group);
    int total_combinations =
        static_cast<int>(std::accumulate(groups.begin(), groups.end(), 1L, acc_op1));
    if (k <= static_cast<int>(groups.size()))
        return total_combinations;
    std::vector<int> dp(k);
    dp[0] = 1;
    for (int i = 0, n = static_cast<int>(groups.size()); i < n; ++i)
    {
        std::vector<int> new_dp(k);
        int window_sum = 0;
        group = groups[i];
        for (int j = i; j < k; ++j)
        {
            new_dp[j] = (new_dp[j] + window_sum) % MOD;
            window_sum = (window_sum + dp[j]) % MOD;
            if (j >= group)
                window_sum = (window_sum - dp[j - group] + MOD) % MOD;
        }
        dp = std::move(new_dp);
    }
    return (total_combinations
         - std::accumulate(dp.begin(), dp.end(), 0, acc_op2) + MOD) % MOD;
}

// ############################################################################
// ############################################################################

void test(std::string word, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = possibleStringCount(word, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aabbccdd", 7, 5, trials);
    test("aabbccdd", 8, 1, trials);
    test("aaabbb", 3, 8, trials);
    return 0;
}


