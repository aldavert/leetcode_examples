#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool satisfiesConditions(std::vector<std::vector<int> > grid)
{
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    for (int i = 0; i + 1 < m; ++i)
        for (int j = 0; j < n; ++j)
            if (grid[i][j] != grid[i + 1][j])
                return false;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j + 1 < n; ++j)
            if (grid[i][j] == grid[i][j + 1])
                return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = satisfiesConditions(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0, 2}, {1, 0, 2}}, true, trials);
    test({{1, 1, 1}, {0, 0, 0}}, false, trials);
    test({{1}, {2}, {3}}, false, trials);
    return 0;
}


