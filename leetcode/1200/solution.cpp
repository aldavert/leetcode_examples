#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > minimumAbsDifference(std::vector<int> arr)
{
    const int n = static_cast<int>(arr.size());
    std::sort(arr.begin(), arr.end());
    int minimum_difference = arr[1] - arr[0];
    for (int i = 2; i < n; ++i)
        minimum_difference = std::min(minimum_difference, arr[i] - arr[i - 1]);
    std::vector<std::vector<int> > result;
    for (int i = 1; i < n; ++i)
        if (arr[i] - arr[i - 1] == minimum_difference)
            result.push_back({arr[i - 1], arr[i]});
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumAbsDifference(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 1, 3}, {{1, 2}, {2, 3}, {3, 4}}, trials);
    test({1, 3, 6, 10, 15}, {{1, 3}}, trials);
    test({3, 8, -10, 23, 19, -4, -14, 27}, {{-14, -10}, {19, 23}, {23, 27}}, trials);
    return 0;
}


