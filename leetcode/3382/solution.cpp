#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

long long maxRectangleArea(std::vector<int> xCoord, std::vector<int> yCoord)
{
    class SegmentTree
    {
    public:
        explicit SegmentTree(int n) : m_tree(4 * n, -1) {}
        inline void update(int i, int val)
        {
            update(0, 0, static_cast<int>(m_tree.size()) / 4 - 1, i, val);
        }
        inline int query(int i, int j) const
        {
            return query(0, 0, static_cast<int>(m_tree.size()) / 4 - 1, i, j);
        }
    private:
        std::vector<int> m_tree;
        
        void update(int tree_index, int lo, int hi, int i, int val)
        {
            if (lo == hi)
            {
                m_tree[tree_index] = val;
                return;
            }
            const int mid = (lo + hi) / 2;
            if (i <= mid) update(2 * tree_index + 1,      lo, mid, i, val);
            else          update(2 * tree_index + 2, mid + 1,  hi, i, val);
            m_tree[tree_index] = merge(m_tree[2 * tree_index + 1],
                                       m_tree[2 * tree_index + 2]);
        }
        int query(int tree_index, int lo, int hi, int i, int j) const
        {
            if ((i <= lo) && (hi <= j)) return m_tree[tree_index];
            if ((j <  lo) || (hi <  i)) return -1;
            const int mid = (lo + hi) / 2;
            return merge(query(tree_index * 2 + 1,      lo, mid, i, j),
                         query(tree_index * 2 + 2, mid + 1,  hi, i, j));
        }
        int merge(int left, int right) const { return std::max(left, right); }
    };
    std::unordered_map<int, int> y_to_index, y_to_x;
    std::vector<std::pair<int, int> > points;
    std::vector<int> ys = yCoord;
    long result = -1;
    
    for (size_t i = 0; i < xCoord.size(); ++i)
        points.emplace_back(xCoord[i], yCoord[i]);
    std::sort(points.begin(), points.end());
    std::sort(ys.begin(), ys.end());
    ys.erase(std::unique(ys.begin(), ys.end()), ys.end());
    SegmentTree tree(static_cast<int>(ys.size()));
    
    for (int i = 0; i < static_cast<int>(ys.size()); ++i)
        y_to_index[ys[i]] = i;
    auto [prev_x, prev_y] = points[0];
    for (size_t i = 1; i < points.size(); ++i)
    {
        const auto [x, y] = points[i];
        if (y_to_x.contains(prev_y) && y_to_x.contains(y))
        {
            const int x_left = y_to_x[y];
            if ((prev_x == x)
            &&  (y_to_x[prev_y] == x_left)
            &&  (x_left > tree.query(y_to_index[prev_y] + 1, y_to_index[y] - 1)))
                result = std::max(result, static_cast<long>(y - prev_y) * (x - x_left));
        }
        y_to_x[prev_y] = prev_x;
        tree.update(y_to_index[prev_y], prev_x);
        prev_x = x;
        prev_y = y;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> xCoord,
          std::vector<int> yCoord,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxRectangleArea(xCoord, yCoord);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 3, 3}, {1, 3, 1, 3}, 4, trials);
    test({1, 1, 3, 3, 2}, {1, 3, 1, 3, 2}, -1, trials);
    test({1, 1, 3, 3, 1, 3}, {1, 3, 1, 3, 2, 2}, 2, trials);
    return 0;
}


