#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

std::string tree2str(TreeNode * root)
{
    if (root == nullptr) return "";
    return std::to_string(root->val)
         + ((root->left )?('(' + tree2str(root->left ) + ')'):"")
         + ((!root->left && root->right)?"()":"")
         + ((root->right)?('(' + tree2str(root->right) + ')'):"");
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = tree2str(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, "1(2(4))(3)", trials);
    test({1, 2, 3, null, 4}, "1(2()(4))(3)", trials);
    return 0;
}


