# Convert Sorted Array to Binary Search Tree

Given an integer array `nums` where the elements are sorted in **ascending order**, convert *it to a* ***height-balanced*** *binary search tree*.

A **height-balanced** binary tree is a binary tree in which the depth of the two subtrees of every node never differs by more than one.
 
#### Example 1:
> *Input:* `nums = [-10, -3, 0, 5, 9]`  
> *Output:* `[0, -3, 9, -10, null, 5]`  
> ```mermaid 
> graph TD;
> A((0))-->B(( -3))
> A-->D((9))
> B-->C(( -10))
> B---EMPTY1(( ))
> D-->E((5))
> D---EMPTY2(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class EMPTY1,EMPTY2 empty;
> linkStyle 3,5 stroke-width:0px
> ```
> *Explanation:* `[0, -10, 5, null, -3, null, 9]` is also accepted:  
> ```mermaid 
> graph TD;
> A((0))-->B(( -10))
> A-->D((5))
> B---EMPTY1(( ))
> B-->C(( -3))
> D---EMPTY2(( ))
> D-->E((9))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class EMPTY1,EMPTY2 empty;
> linkStyle 2,4 stroke-width:0px
> ```

#### Example 2
> *Input:* `nums = [1, 3]`  
> *Output:* `[3, 1]` or `[1, 3]` are both a height-balanced BSTs.  
> ```mermaid 
> graph TD;
> A((3))-->B((1))
> A---EMPTY1(( ))
> C((1))---EMPTY2(( ))
> C-->D((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class EMPTY1,EMPTY2 empty;
> linkStyle 1,2 stroke-width:0px
> ```

#### Constrains:
- `1 <= nums.length <= 10^4`
- `-10^4 <= nums[i] <= 10^4`
- `nums` is sorted in a **strictly increasing** order.


