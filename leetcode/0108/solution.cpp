#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * sortedArrayToBST(std::vector<int> nums)
{
    auto process = [&](auto &&self, int begin, int end) -> TreeNode*
    {
        if (begin > end) return nullptr;
        int pivot = begin + (end - begin + 1) / 2;
        return new TreeNode(nums[pivot],
                            self(self, begin, pivot - 1),
                            self(self, pivot + 1, end));
    };
    return process(process, 0, static_cast<int>(nums.size()) - 1);
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &solutions, const std::vector<int> &results)
{
    for (const auto &s : solutions)
        if (s == results) return true;
    return false;
}

void test(std::vector<int> nums, std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = sortedArrayToBST(nums);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-10, -3, 0, 5, 9}, {{0, -3, 9, -10, null, 5}, {0, -10, 5, null, -3, null, 9}}, trials);
    test({1, 3}, {{1, 3}, {3, 1}}, trials);
    return 0;
}


