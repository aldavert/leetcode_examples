#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minimumOperations(std::vector<int> nums, int start, int goal)
{
    std::queue<int> q{{start}};
    std::vector<bool> seen(1001);
    int result = 0;
    seen[start] = true;
    
    while (!q.empty())
    {
        ++result;
        for (int sz = static_cast<int>(q.size()); sz > 0; --sz)
        {
            int x = q.front();
            q.pop();
            for (int num : nums)
            {
                for (int res : {x + num, x - num, x ^ num})
                {
                    if (res == goal)
                        return result;
                    if ((res < 0) || (res > 1000) || seen[res])
                        continue;
                    seen[res] = true;
                    q.push(res);
                }
            }
        }
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int start,
          int goal,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumOperations(nums, start, goal);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4, 12}, 2, 12, 2, trials);
    test({3, 5,  7}, 0, -4, 2, trials);
    test({2, 8, 16}, 0, 1, -1, trials);
    return 0;
}


