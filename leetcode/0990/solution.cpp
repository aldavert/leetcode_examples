#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool equationsPossible(std::vector<std::string> equations)
{
    int values[26] = {};
    for (int i = 0; i < 26; ++i) values[i] = i;
    auto search = [&](auto &&self, int idx) -> int
    {
        return (values[idx] == idx)?idx:(values[idx] = self(self, values[idx]));
    };
    for (std::string current : equations)
        if (current[1] == '=')
            values[search(search, current[0] - 'a')] = search(search, current[3] - 'a');
    for (std::string current : equations)
        if (current[1] == '!')
            if (search(search, current[0] - 'a') == search(search, current[3] - 'a'))
                return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> equations, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = equationsPossible(equations);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"a==b", "b!=a"}, false, trials);
    test({"a==b", "a!=b"}, false, trials);
    test({"b==a", "a==b"},  true, trials);
    return 0;
}


