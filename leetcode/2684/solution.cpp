#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxMoves(std::vector<std::vector<int> > grid)
{
    const int n_rows = static_cast<int>(grid.size()),
              n_cols = static_cast<int>(grid[0].size());
    std::vector<std::vector<int> > dp(n_rows, std::vector<int>(n_cols));
    
    for (int j = n_cols - 2; j >= 0; --j)
    {
        for (int i = 0; i < n_rows; ++i)
        {
            if (grid[i][j + 1] > grid[i][j])
                dp[i][j] = 1 + dp[i][j + 1];
            if ((i > 0) && (grid[i - 1][j + 1] > grid[i][j]))
                dp[i][j] = std::max(dp[i][j], 1 + dp[i - 1][j + 1]);
            if ((i + 1 < n_rows) && (grid[i + 1][j + 1] > grid[i][j]))
                dp[i][j] = std::max(dp[i][j], 1 + dp[i + 1][j + 1]);
        }
    }
    int result = 0;
    for (int i = 0; i < n_rows; ++i)
        result = std::max(result, dp[i][0]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxMoves(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 4, 3, 5}, {5, 4, 9, 3}, {3, 4, 2, 11}, {10, 9, 13, 15}}, 3, trials);
    test({{3, 2, 4}, {2, 1, 9}, {1, 1, 7}}, 0, trials);
    return 0;
}


