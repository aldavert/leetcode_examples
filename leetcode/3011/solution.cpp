#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canSortArray(std::vector<int> nums)
{
    int prev_set_bits = 0;
    int prev_max = std::numeric_limits<int>::lowest();
    int curr_max = std::numeric_limits<int>::lowest();
    int curr_min = std::numeric_limits<int>::max();
    for (const int num : nums)
    {
        const int set_bits = std::popcount(static_cast<unsigned int>(num));
        if (set_bits != prev_set_bits)
        {
            if (prev_max > curr_min) return false;
            prev_set_bits = set_bits;
            prev_max = curr_max;
            curr_max = num;
            curr_min = num;
        }
        else
        {
            curr_max = std::max(curr_max, num);
            curr_min = std::min(curr_min, num);
        }
    }
    return prev_max <= curr_min;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canSortArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 4, 2, 30, 15}, true, trials);
    test({1, 2, 3, 4, 5}, true, trials);
    return 0;
}


