#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numSpecialEquivGroups(std::vector<std::string> words)
{
    std::unordered_set<std::string> set;
    for (const auto &word : words)
    {
        std::string even, odd;
        for (size_t i = 0; i < word.size(); ++i)
            if (i % 2 == 0) even += word[i];
            else odd += word[i];
        std::sort(even.begin(), even.end());
        std::sort(odd.begin(), odd.end());
        set.insert(even + odd);
    }
    return static_cast<int>(set.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numSpecialEquivGroups(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"abcd", "cdab", "cbad", "xyzz", "zzxy", "zzyx"}, 3, trials);
    test({"abc", "acb", "bac", "bca", "cab", "cba"}, 3, trials);
    return 0;
}


