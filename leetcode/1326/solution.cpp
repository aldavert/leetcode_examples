#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minTaps(int n, std::vector<int> ranges)
{
    int result = 0, end = 0, farthest = 0;
    std::vector<int> nums(n + 1);
    
    for (int i = 0; i <= n; ++i)
    {
        int l = std::max(0, i - ranges[i]), r = std::min(n, i + ranges[i]);
        nums[l] = std::max(nums[l], r - l);
    }
    for (int i = 0; i < n; ++i)
    {
        farthest = std::max(farthest, i + nums[i]);
        if (i == end)
        {
            ++result;
            end = farthest;
        }
    }
    return (end == n)?result:-1;
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<int> ranges, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minTaps(n, ranges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {3, 4, 1, 1, 0, 0}, 1, trials);
    test(3, {0, 0, 0, 0}, -1, trials);
    return 0;
}


