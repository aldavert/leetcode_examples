#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxCount(int m, int n, std::vector<std::vector<int> > ops)
{
    for (const auto &coor : ops)
    {
        m = std::min(coor[0], m);
        n = std::min(coor[1], n);
    }
    return m * n;
}

// ############################################################################
// ############################################################################

void test(int m,
          int n,
          std::vector<std::vector<int> > ops,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxCount(m, n, ops);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 3, {{2, 2}, {3, 3}}, 4, trials);
    test(3, 3, {{2, 2}, {3, 3}, {3, 3}, {3, 3}, {2, 2}, {3, 3}, {3, 3}, {3, 3},
         {2, 2}, {3, 3}, {3, 3}, {3, 3}}, 4, trials);
    test(3, 3, {}, 9, trials);
    return 0;
}


