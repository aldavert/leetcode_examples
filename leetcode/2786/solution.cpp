#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maxScore(std::vector<int> nums, int x)
{
    long dp[2] = {nums[0] - (nums[0] % 2 == 1) * x,
                  nums[0] - (nums[0] % 2 == 0) * x};
    for (int i = 1, n = static_cast<int>(nums.size()); i < n; ++i)
        dp[nums[i] & 1] = nums[i] + std::max(dp[nums[i] & 1], dp[1 - (nums[i] & 1)] - x);
    return std::max(dp[0], dp[1]);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int x, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxScore(nums, x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 6, 1, 9, 2}, 5, 13, trials);
    test({2, 4, 6, 8}, 3, 20, trials);
    return 0;
}


