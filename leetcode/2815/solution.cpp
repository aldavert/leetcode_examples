#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxSum(std::vector<int> nums)
{
    auto maximumDigit = [](int number) -> int
    {
        int result = 0;
        for (; number; number /= 10) result = std::max(result, number % 10);
        return result;
    };
    int max_digit_number[10] = {};
    int result = -1;
    for (int number : nums)
    {
        int md = maximumDigit(number);
        if (max_digit_number[md])
            result = std::max(result, max_digit_number[md] + number);
        max_digit_number[md] = std::max(max_digit_number[md], number);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({51, 71, 17, 24, 42}, 88, trials);
    test({1, 2, 3, 4}, -1, trials);
    return 0;
}


