#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countRangeSum(std::vector<int> nums, int lower, int upper)
{
    const int n = static_cast<int>(nums.size());
    std::vector<long long> sums(n + 1), aux(n + 1);
    for (int i = 0; i < n; ++i)
        sums[i + 1] = sums[i] + nums[i];
    auto countAndMergeSort = [&](auto &&self, int start, int end) -> int
    {
        if (end - start <= 1) return 0;
        const int mid = start + (end - start) / 2;
        int count = self(self, start, mid)
                  + self(self, mid,  end);
        int idx = 0;
        for (int i = start, j = mid, k = mid, r = mid; i < mid; ++i)
        {
            for (; (k < end) && (sums[k] - sums[i] <  lower); ++k);
            for (; (j < end) && (sums[j] - sums[i] <= upper); ++j);
            count += j - k;
            for (; (r < end) && (sums[r] < sums[i]); ++r)
                aux[idx++] = sums[r];
            aux[idx++] = sums[i];
        }
        std::copy(aux.begin(), aux.begin() + idx, sums.begin() + start);
        return count;
    };
    return countAndMergeSort(countAndMergeSort, 0, n + 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int lower,
          int upper,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countRangeSum(nums, lower, upper);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-2, 5, -1}, -2, 2, 3, trials);
    test({0}, 0, 0, 1, trials);
    return 0;
}


