#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findIntegers(int n)
{
    constexpr int fib[] = {1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610,
                           987, 1597, 2584, 4181, 6765, 10946, 17711, 28657,
                           46368, 75025, 121393, 196418, 317811, 514229, 832040,
                           1346269, 2178309, 3524578, 5702887};
    bool binary[32] = {};
    int length = -1;
    ++n;
    for (int i = 0; i < 32; ++i, n >>= 1)
        if ((binary[i] = (n & 1)))
            length = i;
    ++length;
    
    int result = 0;
    for (int i = length - 1; i >= 0; --i)
    {
        if (binary[i])
        {
            result += fib[i];
            if (binary[i + 1]) break;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findIntegers(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 5, trials);
    test(1, 2, trials);
    test(2, 3, trials);
    test(29374, 1597, trials);
    return 0;
}


