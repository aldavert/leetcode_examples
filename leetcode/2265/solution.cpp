#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int averageOfSubtree(TreeNode * root)
{
    struct Counter
    {
        int sum = 0;
        int frequency = 0;
        Counter& operator+=(const Counter &other)
        {
            sum += other.sum;
            frequency += other.frequency;
            return *this;
        }
    };
    auto process = [](TreeNode * head) -> int
    {
        int result = 0;
        auto recursive = [&](auto &&self, TreeNode * node) -> Counter
        {
            if (node == nullptr) return {0, 0};
            Counter count{node->val, 1};
            count += self(self, node->left);
            count += self(self, node->right);
            result += (count.sum / count.frequency == node->val);
            return count;
        };
        recursive(recursive, head);
        return result;
    };
    return process(root);
}

// ############################################################################
// ############################################################################

void test(const std::vector<int> &tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = averageOfSubtree(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 8, 5, 0, 1, null, 6}, 5, trials);
    test({1}, 1, trials);
    return 0;
}


