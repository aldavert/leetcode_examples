# Count Nodes Equal to Average Subtree

Given the `root` of a binary tree, return *the number of nodes where the value of the node is equal to the* ***average*** *of the values in its* ***subtree.***

**Note:**
- The **average** of `n` elements is the sum of the `n` elements divided by n and **rounded down** to the nearest integer.
- A **subtree** of `root` is a tree consisting of `root` and all of its descendants.

#### Example 1:
> ```mermaid
> graph TD;
> A((4))---B((8))
> B---C((0))
> B---D((1))
> A---E((5))
> E---EMPTY(( ))
> E---F((6))
> linkStyle 4 stroke-width:0px
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> classDef selected fill:#FDA,stroke:#000,stroke-width:2px;
> classDef white fill:#FFF,stroke:#000,stroke-width:2px;
> class EMPTY empty;
> class A,C,D,E,F selected;
> class B white;
> ```
> *Input:* `root = [4, 8, 5, 0, 1, null, 6]`  
> *Output:* `5`  
> *Explanation:*
> - For the node with value `4`: The average of its subtree is `(4 + 8 + 5 + 0 + 1 + 6) / 6 = 24 / 6 = 4`.
> - For the node with value `5`: The average of its subtree is `(5 + 6) / 2 = 11 / 2 = 5`.
> - For the node with value `0`: The average of its subtree is `0 / 1 = 0`.
> - For the node with value `1`: The average of its subtree is `1 / 1 = 1`.
> - For the node with value `6`: The average of its subtree is `6 / 1 = 6`.

#### Example 2:
> *Input:* `root = [1]`  
> *Output:* `1`  
> *Explanation:* For the node with value `1`: The average of its subtree is `1 / 1 = 1`.

#### Constraints:
- The number of nodes in the tree is in the range `[1, 1000]`.
- `0 <= Node.val <= 1000`


