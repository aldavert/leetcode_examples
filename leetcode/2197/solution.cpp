#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

std::vector<int> replaceNonCoprimes(std::vector<int> nums)
{
    std::vector<int> result;
    for (int num : nums)
    {
        while (!result.empty() && (std::gcd(result.back(), num) > 1))
            num = std::lcm(result.back(), num), result.pop_back();
        result.push_back(num);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = replaceNonCoprimes(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({6, 4, 3, 2, 7, 6, 2}, {12, 7, 6}, trials);
    test({2, 2, 1, 1, 3, 3, 3}, {2, 1, 1, 3}, trials);
    return 0;
}


