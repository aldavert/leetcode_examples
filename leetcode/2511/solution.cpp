#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int captureForts(std::vector<int> forts)
{
    const int n = static_cast<int>(forts.size());
    int result = 0;
    for (int previous = 0, position = 0, i = 0; i < n; ++i)
    {
        if (forts[i] != 0)
        {
            if (previous && (previous != forts[i]))
                result = std::max(result, i - position - 1);
            previous = forts[i];
            position = i;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> forts, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = captureForts(forts);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 0, -1, 0, 0, 0, 0, 1}, 4, trials);
    test({0, 0, 1, -1}, 0, trials);
    test({1, 0, 0, 0, 0, -1, 0, 0, 1}, 4, trials);
    test({1, -1, 0, 0}, 0, trials);
    test({0, 1, -1, 0}, 0, trials);
    return 0;
}



