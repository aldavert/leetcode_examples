#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int minGroupsForValidAssignment(std::vector<int> balls)
{
    std::unordered_map<int, int> count;
    int min_freq = static_cast<int>(balls.size());
    auto numGroups = [&](int group_size) -> int
    {
        int num_groups = 0;
        for (const auto& [_, freq] : count)
        {
            const int div = freq / (group_size + 1),
                      mod = freq % (group_size + 1);
            if (mod == 0) num_groups += div;
            else if (group_size - mod <= div) num_groups += div + 1;
            else return 0;
        }
        return num_groups;
    };
    
    for (const int num : balls)
        ++count[num];
    for (const auto& [_, freq] : count)
        min_freq = std::min(min_freq, freq);
    for (int group_size = min_freq; group_size >= 1; --group_size)
    {
        const int num_groups = numGroups(group_size);
        if (num_groups > 0)
            return num_groups;
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> balls, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minGroupsForValidAssignment(balls);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 3, 2, 3}, 2, trials);
    test({10, 10, 10, 3, 1, 1}, 4, trials);
    return 0;
}


