#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string addBinary(std::string a, std::string b)
{
    const std::string &str_s = (a.size() > b.size())?b:a;
    const std::string &str_l = (a.size() > b.size())?a:b;
    int n = static_cast<int>(str_l.size()) - 1,
        m = static_cast<int>(str_s.size()) - 1,
        r = 0, idx = n;
    std::string result(n + 1, '\0');
    for (; (n >= 0) && (m >= 0); --n, --m)
    {
        int c = (str_l[n] == '1') + (str_s[m] == '1') + r;
        r = c > 1;
        result[idx--] = (c & 1)?'1':'0';
    }
    for (; n >= 0; --n)
    {
        int c = (str_l[n] == '1') + r;
        r = c > 1;
        result[idx--] = (c & 1)?'1':'0';
    }
    if (r) result = "1" + result;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string a, std::string b, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = addBinary(a, b);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("11", "1", "100", trials);
    test("1", "11", "100", trials);
    test("1010", "1011", "10101", trials);
    test("1011", "1010", "10101", trials);
    test("0", "0", "0", trials);
    return 0;
}


