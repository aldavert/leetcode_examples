#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> validSequence(std::string word1, std::string word2)
{
    const int n = static_cast<int>(word1.size()),
              m = static_cast<int>(word2.size());
    std::vector<int> result(word2.size()), last(word2.size(), -1);
    int i = n - 1, j = m - 1;
    while ((i >= 0) && (j >= 0))
    {
        if (word1[i] == word2[j])
            last[j--] = i;
        --i;
    }
    bool can_skip = true;
    for (i = 0, j = 0; i < n; ++i)
    {
        if (j == m) break;
        if (word1[i] == word2[j])
            result[j++] = i;
        else if (can_skip && ((j == m - 1) || (i < last[j + 1])))
            can_skip = false,
            result[j++] = i;
    }
    return (j == m)?result:std::vector<int>();
}

// ############################################################################
// ############################################################################

void test(std::string word1,
          std::string word2,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = validSequence(word1, word2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("vbcca", "abc", {0, 1, 2}, trials);
    test("bacdc", "abc", {1, 2, 4}, trials);
    test("aaaaaa", "aaabc", {}, trials);
    test("abc", "ab", {0, 1}, trials);
    return 0;
}


