#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> findDuplicates(std::vector<int> nums)
{
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);
    const size_t n = nums.size();
    std::vector<int> result, histogram(n + 1);
    result.reserve(n);
    for (int v : nums)
    {
        ++histogram[v];
        if (histogram[v] == 2)
            result.push_back(v);
    }
    return result;
}
#else
std::vector<int> findDuplicates(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> result, histogram(n + 1);
    result.reserve(n);
    for (int value : nums)
        ++histogram[value];
    for (int i = 0; i <= n; ++i)
        if (histogram[i] == 2)
            result.push_back(i);
    return result;
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_map<int, int> unique;
    for (int l : left) ++unique[l];
    for (int r : right)
    {
        if (auto it = unique.find(r); it != unique.end())
        {
            --it->second;
            if (it->second == 0)
                unique.erase(it);
        }
        else return false;
    }
    return unique.size() == 0;
}


void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findDuplicates(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 3, 2, 7, 8, 2, 3, 1}, {2, 3}, trials);
    test({1, 1, 2}, {1}, trials);
    test({1}, {}, trials);
    return 0;
}


