#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > floodFill(std::vector<std::vector<int> > image,
                                         int sr,
                                         int sc,
                                         int color)
{
    constexpr int dir[] = {1, 0, -1, 0, 1};
    std::vector<std::vector<int> > result(image);
    const int m = static_cast<int>(image.size());
    const int n = (m > 0)?static_cast<int>(image[0].size()):0;
    std::stack<std::pair<int, int> > active({{sr, sc}});
    const int target = image[sr][sc];
    while (!active.empty())
    {
        auto [x, y] = active.top();
        active.pop();
        if ((result[x][y] == color) || (result[x][y] != target))
            continue;
        result[x][y] = color;
        for (int i = 0; i < 4; ++i)
        {
            int nx = x + dir[i];
            int ny = y + dir[i + 1];
            if ((nx >= 0) && (ny >= 0) && (nx < m) && (ny < n)
            &&  (result[nx][ny] == target))
                active.push({nx, ny});
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > image,
          int sr,
          int sc,
          int color,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = floodFill(image, sr, sc, color);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 1}, {1, 1, 0}, {1, 0, 1}}, 1, 1, 2,
         {{2, 2, 2}, {2, 2, 0}, {2, 0, 1}}, trials);
    test({{0, 0, 0}, {0, 0, 0}}, 0, 0, 0,
         {{0, 0, 0}, {0, 0, 0}}, trials);
    return 0;
}


