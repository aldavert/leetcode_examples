#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int partitionArray(std::vector<int> nums, int k)
{
    std::sort(nums.begin(), nums.end());
    int result = 1;
    for (int i = 1, n = static_cast<int>(nums.size()), mn = nums[0]; i < n; ++i)
    {
        if (mn + k < nums[i])
        {
            ++result;
            mn = nums[i];
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = partitionArray(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 6, 1, 2, 5}, 2, 2, trials);
    test({1, 2, 3}, 1, 2, trials);
    test({2, 2, 4, 5}, 0, 3, trials);
    return 0;
}


