#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int amountOfTime(TreeNode * root, int start)
{
    int result = 0;
    auto distance = [&](auto &&self, TreeNode * node) -> std::tuple<int, bool>
    {
        if (node == nullptr) return {0, false};
        auto [distance_left , found_left ] = self(self, node->left );
        auto [distance_right, found_right] = self(self, node->right);
        if (node->val == start)
        {
            result = std::max(result, std::max(distance_left, distance_right));
            return {0, true};
        }
        else if (found_left)
        {
            ++distance_left;
            result = std::max(result, distance_left + distance_right);
            return {distance_left, true};
        }
        else if (found_right)
        {
            ++distance_right;
            result = std::max(result, distance_left + distance_right);
            return {distance_right, true};
        }
        else return {std::max(distance_left, distance_right) + 1, false};
    };
    distance(distance, root);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int start, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = amountOfTime(root, start);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 5, 3, null, 4, 10, 6, 9, 2}, 3, 4, trials);
    test({1, 5, 3, null, 4, 10, 6, null, null, 9, 2, null, null, null, null, 11, null, null, 12, 17}, 3, 5, trials);
    test({1}, 1, 0, trials);
    return 0;
}


