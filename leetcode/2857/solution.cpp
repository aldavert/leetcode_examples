#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int countPairs(std::vector<std::vector<int> > coordinates, int k)
{
    std::unordered_map<long long, int> freq;
    auto hash = [](int x, int y) -> long long { return ((long long)x << 32) + y; };
    for (const auto &coord : coordinates)
        ++freq[hash(coord[0], coord[1])];
    long long result = 0;
    for (int i = 0; i <= k; i ++)
    {
        int dis_x = i, dis_y = k - i;
        for (const auto &coord : coordinates)
        {
            int match_x = dis_x ^ coord[0], match_y = dis_y ^ coord[1];
            if (auto it = freq.find(hash(match_x, match_y)); it != freq.end())
                result += it->second - ((match_x == coord[0]) && (match_y == coord[1]));
        }
    }
    return static_cast<int>(result / 2);
}
#else
int countPairs(std::vector<std::vector<int> > coordinates, int k)
{
    std::unordered_map<long, int> count;
    auto uniqueKey = [](int x, int y) -> long {
        return static_cast<long>(x) * 1000000L + y; 
    };
    int result = 0;
    for (auto& point : coordinates)
    {
        int x2 = point[0], y2 = point[1];
        for (int a = 0; a <= k; ++a)
            result += count[uniqueKey(a ^ x2, (k - a) ^ y2)];
        ++count[uniqueKey(x2, y2)];
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > coordinates,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPairs(coordinates, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {4, 2}, {1, 3}, {5, 2}}, 5, 2, trials);
    test({{1, 3}, {1, 3}, {1, 3}, {1, 3}, {1, 3}}, 0, 10, trials);
    return 0;
}


