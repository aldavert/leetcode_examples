# Remove Duplicate Letters

Given a string `s`, remove duplicate letters so that every letter appears once and only once. You must make sure your result is *the smallest in lexicographical order* among all possible results.

**Note:** A string `a` is *lexicographically smaller* than a string `b` if in the first position where `a` and `b` differ, string `a` has a letter that appears earlier in the alphabet than the corresponding letter in `b`. If the first `min(a.length, b.length)` characters do not differ, then the shorter string is the lexicographically smaller one.

#### Example 1:
> *Input:* `s = "bcabc"`  
> *Output:* `"abc"`

#### Example 2:
> *Input:* `s = "cbacdcbc"`  
> *Output:* `"acdb"`

#### Constraints:
- `1 <= s.length <= 10^4`
- `s` consists of lowercase English letters.


