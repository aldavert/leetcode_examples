#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string removeDuplicateLetters(std::string s)
{
    std::string result;
    int histogram['z' - 'a' + 1] = {};
    bool used['z' - 'a' + 1] = {};
    for (char c : s) ++histogram[c - 'a'];
    for (char c : s)
    {
        int idx = static_cast<int>(c - 'a');
        --histogram[idx];
        if (used[idx]) continue;
        while (!result.empty()
            && (result.back() > c)
            && (histogram[result.back() - 'a'] > 0))
        {
            used[result.back() - 'a'] = false;
            result.pop_back();
        }
        used[idx] = true;
        result.push_back(c);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result = "";
    for (unsigned int i = 0; i < trials; ++i)
        result = removeDuplicateLetters(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("bcabc", "abc", trials);
    test("cbacdcbc", "acdb", trials);
    return 0;
}


