#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumXORSum(std::vector<int> nums1, std::vector<int> nums2)
{
    const int n1 = static_cast<int>(nums1.size()),
              n2 = static_cast<int>(nums2.size());
    std::vector<int> dp(1 << nums2.size(), std::numeric_limits<int>::max());
    auto dfs = [&](auto &&self, int i, int mask) -> int
    {
        if (i == n1) return 0;
        if (dp[mask] < std::numeric_limits<int>::max()) return dp[mask];
        for (int j = 0; j < n2; ++j)
            if (!(mask >> j & 1))
                dp[mask] = std::min(dp[mask], (nums1[i] ^ nums2[j])
                                  + self(self, i + 1, mask | 1 << j));
        return dp[mask];
    };
    
    return dfs(dfs, 0, 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumXORSum(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2}, {2, 3}, 2, trials);
    test({1, 0, 3}, {5, 3, 4}, 8, trials);
    return 0;
}


