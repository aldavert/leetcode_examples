#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <stack>

// ############################################################################
// ############################################################################

class BSTIterator
{
    std::stack<const TreeNode *> m_backtrace;
public:
    BSTIterator(TreeNode * root)
    {
        for (const TreeNode * ptr = root; ptr; ptr = ptr->left)
            m_backtrace.push(ptr);
    }
    int next(void)
    {
        if (m_backtrace.empty()) [[unlikely]] return -1;
        else [[likely]]
        {
            const TreeNode * current = m_backtrace.top();
            m_backtrace.pop();
            for (const TreeNode * ptr = current->right; ptr; ptr = ptr->left)
                m_backtrace.push(ptr);
            return current->val;
        }
    }
    bool hasNext(void)
    {
        return !m_backtrace.empty();
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          std::vector<bool> op_is_next,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result.clear();
        BSTIterator obj(root);
        for (bool is_next : op_is_next)
        {
            if (is_next)
                result.push_back(obj.next());
            else result.push_back(obj.hasNext());
        }
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({7, 3, 15, null, null, 9, 20},
         {true, true, false, true, false, true, false, true, false},
         {3, 7, true, 9, true, 15, true, 20, false}, trials);
    return 0;
}


