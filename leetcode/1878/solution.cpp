#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> getBiggestThree(std::vector<std::vector<int> > grid)
{
    auto findArea = [&](int i, int j, int d)
    {
        if (d == 0) return grid[i][j];
        int ai = i - d, aj = j, bi = i, bj = j + d;
        int ci = i + d, cj = j, di = i, dj = j - d;
        int sum = grid[ai][aj] + grid[bi][bj] + grid[ci][cj] + grid[di][dj];
        for (int p = 1, m = bi - ai; p < m; ++p)
        {
            sum += grid[ai + p][aj + p];
            sum += grid[bi + p][bj - p];
            sum += grid[ci - p][cj - p];
            sum += grid[di - p][dj + p];
        }
        return sum;
    };
    const int n = static_cast<int>(grid.size()),
              m = static_cast<int>(grid[0].size());
    std::priority_queue<int> q;
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            q.push(grid[i][j]);
            for (int d = 1; (i - d >= 0) && (i + d < n)
                         && (j - d >= 0) && (j + d < m); ++d)
                q.push(findArea(i, j, d));
        }
    }
    std::vector<int> result;
    while (!q.empty() && (result.size() != 3))
    {
        int top = q.top();
        q.pop();
        if (result.empty()
        || (std::find(result.begin(), result.end(), top) == result.end()))
            result.push_back(top);
    }
    return result;
}
#else
std::vector<int> getBiggestThree(std::vector<std::vector<int> > grid)
{
    auto calcSum = [&grid](int x, int y, int sz)
    {
        int sum = 0;
        for (int k = 0; k < sz; ++k) sum += grid[--x][++y];
        for (int k = 0; k < sz; ++k) sum += grid[++x][++y];
        for (int k = 0; k < sz; ++k) sum += grid[++x][--y];
        for (int k = 0; k < sz; ++k) sum += grid[--x][--y];
        return sum;
    };
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    std::set<int> sums;
    
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            for (int sz = 0; i + sz < m && i - sz >= 0 && j + 2 * sz < n; ++sz)
            {
                const int sum = (sz == 0)?grid[i][j]:calcSum(i, j, sz);
                sums.insert(sum);
                if (sums.size() > 3)
                    sums.erase(sums.begin());
            }
        }
    }
    return std::vector<int>(sums.rbegin(), sums.rend());
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getBiggestThree(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 4, 5, 1, 3},
          {3, 3, 4, 2, 3},
          {20, 30, 200, 40, 10},
          {1, 5, 5, 4, 1},
          {4, 3, 2, 2, 5}}, {228, 216, 211}, trials);
    test({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, {20, 9, 8}, trials);
    test({{7, 7, 7}}, {7}, trials);
    return 0;
}


