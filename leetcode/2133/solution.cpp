#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

bool checkValid(std::vector<std::vector<int> > matrix)
{
    const int n = static_cast<int>(matrix.size());
    std::bitset<101> valid_columns[101], valid_row;
    for (int r = 0; r < n; ++r)
    {
        valid_row.reset();
        for (int c = 0; c < n; ++c)
        {
            if (valid_row[matrix[r][c]]) return false;
            valid_row[matrix[r][c]] = true;
            if (valid_columns[c][matrix[r][c]]) return false;
            valid_columns[c][matrix[r][c]] = true;
        }
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkValid(matrix);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3},
          {3, 1, 2},
          {2, 3, 1}}, true, trials);
    test({{1, 1, 1},
          {1, 2, 3},
          {1, 2, 3}}, false, trials);
    return 0;
}


