#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int maxRemoval(std::vector<int> nums, std::vector<std::vector<int> > queries)
{
    const int n = static_cast<int>(nums.size()),
              m = static_cast<int>(queries.size());
    std::priority_queue<int, std::vector<int>, std::greater<>> running;
    std::priority_queue<int> available;
    
    std::sort(queries.begin(), queries.end());
    for (int i = 0, query_index = 0; i < n; ++i)
    {
        while ((query_index < m) && (queries[query_index][0] <= i))
            available.push(queries[query_index++][1]);
        while (!running.empty() && running.top() < i)
            running.pop();
        while (nums[i] > static_cast<int>(running.size()))
        {
            if (available.empty() || (available.top() < i))
                return -1;
            running.push(available.top()), available.pop();
        }
    }
    return static_cast<int>(available.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::vector<int> > queries,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxRemoval(nums, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 0, 2}, {{0, 2}, {0, 2}, {1, 1}}, 1, trials);
    test({1, 1, 1, 1}, {{1, 3}, {0, 2}, {1, 3}, {1, 2}}, 2, trials);
    test({1, 2, 3, 4}, {{0, 3}}, -1, trials);
    return 0;
}


