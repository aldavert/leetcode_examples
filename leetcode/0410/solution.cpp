#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int splitArray(std::vector<int> nums, int m)
{
    auto valid = [&](long target) -> bool
    {
        int count = 1;
        for (long total = 0; int num : nums)
        {
            total += num;
            if (total > target)
            {
                ++count;
                total = num;
                if (count > m) return false;
            }
        }
        return true;
    };
    int max_val = 0; long sum = 0;
    for (int num : nums)
    {
        max_val = std::max(max_val, num);
        sum += num;
    }
    long l = max_val , r = sum;
    while (l <= r)
    {
        long mid = (l + r) / 2;
        if (valid(mid))
            r = mid - 1;
        else l = mid + 1;
    }
    return static_cast<int>(l);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int m, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = splitArray(nums, m);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({7, 2, 5, 10, 8}, 2, 18, trials);
    test({1, 2, 3, 4, 5}, 2, 9, trials);
    test({1, 4, 4}, 3, 4, trials);
    return 0;
}


