#include "../common/common.hpp"

// ############################################################################
// ############################################################################

using namespace std;
std::vector<int> survivedRobotsHealths(std::vector<int> positions,
                                       std::vector<int> healths,
                                       std::string directions)
{
    struct Robot
    {
        int index;
        int position;
        int health;
        char direction;
        bool operator<(const Robot &other) const { return position < other.position; }
    };
    auto cmpIndex = [](const Robot &a, const Robot &b) { return a.index < b.index; };
    std::vector<Robot> robots, stack;
    
    for (int i = 0, n = static_cast<int>(positions.size()); i < n; ++i)
        robots.push_back(Robot{i, positions[i], healths[i], directions[i]});
    std::sort(robots.begin(), robots.end());
    
    for (Robot& robot : robots)
    {
        if (robot.direction == 'R')
        {
            stack.push_back(robot);
            continue;
        }
        while (!stack.empty() && (stack.back().direction == 'R') && (robot.health > 0))
        {
            if (stack.back().health == robot.health)
            {
                stack.pop_back();
                robot.health = 0;
            }
            else if (stack.back().health < robot.health)
            {
                stack.pop_back();
                robot.health -= 1;
            }
            else
            {
                stack.back().health -= 1;
                robot.health = 0;
            }
        }
        if (robot.health > 0) stack.push_back(robot);
    }
    std::sort(stack.begin(), stack.end(), cmpIndex);
    
    std::vector<int> result;
    for (auto &robot : stack)
        result.push_back(robot.health);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> positions,
          std::vector<int> health,
          std::string directions,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = survivedRobotsHealths(positions, health, directions);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 3, 2, 1}, {2, 17, 9, 15, 10}, "RRRRR", {2, 17, 9, 15, 10}, trials);
    test({3, 5, 2, 6}, {10, 10, 15, 12}, "RLRL", {14}, trials);
    test({1, 2, 5, 6}, {10, 10, 11, 11}, "RLRL", {}, trials);
    return 0;
}


