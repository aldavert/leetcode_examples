#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<bool> isArraySpecial(std::vector<int> nums,
                                 std::vector<std::vector<int> > queries)
{
    std::vector<bool> result;
    std::vector<int> parity_ids = { 0 };
    for (int i = 1, id = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        parity_ids.push_back(id += (nums[i] % 2 == nums[i - 1] % 2));
    for (const auto &query : queries)
        result.push_back(parity_ids[query[0]] == parity_ids[query[1]]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::vector<int> > queries,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = isArraySpecial(nums, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 4, 1, 2, 6}, {{0, 4}}, {false}, trials);
    test({4, 3, 1, 6}, {{0, 2}, {2, 3}}, {false, true}, trials);
    return 0;
}


