#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long minimumCost(std::vector<int> nums, int k, int dist)
{
    long window_sum = 0;
    std::multiset<int> selected;
    std::multiset<int> candidates;
    auto balance = [&]() -> void
    {
        while (static_cast<int>(selected.size()) < k - 1)
        {
            const int min_candidate = *candidates.begin();
            window_sum += min_candidate;
            selected.insert(min_candidate);
            candidates.erase(candidates.find(min_candidate));
        }
        while (static_cast<int>(selected.size()) > k - 1)
        {
            const int max_selected = *selected.rbegin();
            window_sum -= max_selected;
            selected.erase(selected.find(max_selected));
            candidates.insert(max_selected);
        }
    };
    
    for (int i = 1; i <= dist + 1; ++i)
    {
        window_sum += nums[i];
        selected.insert(nums[i]);
    }
    balance();
    long min_window_sum = window_sum;
    for (int i = dist + 2, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        const int out_of_scope = nums[i - dist - 1];
        if (selected.find(out_of_scope) != selected.end())
        {
            window_sum -= out_of_scope;
            selected.erase(selected.find(out_of_scope));
        }
        else candidates.erase(candidates.find(out_of_scope));
        if (nums[i] < *selected.rbegin())
        {
            window_sum += nums[i];
            selected.insert(nums[i]);
        }
        else candidates.insert(nums[i]);
        balance();
        min_window_sum = std::min(min_window_sum, window_sum);
    }
    return nums[0] + min_window_sum;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int dist, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumCost(nums, k, dist);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 6, 4, 2}, 3, 3, 5, trials);
    test({10, 1, 2, 2, 2, 1}, 4, 3, 15, trials);
    test({10, 8, 18, 9}, 3, 1, 36, trials);
    return 0;
}


