#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int specialPerm(std::vector<int> nums)
{
    const int MOD = 1'000'000'007;
    int n = static_cast<int>(nums.size()), m = 1 << n;
    std::vector<std::vector<int> > f(m, std::vector<int>(n));
    for (int i = 1; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            if ((i >> j & 1) == 1)
            {
                if (int ii = i ^ (1 << j); ii != 0)
                {
                    for (int k = 0; k < n; ++k)
                        if ((nums[j] % nums[k] == 0) || (nums[k] % nums[j] == 0))
                            f[i][j] = (f[i][j] + f[ii][k]) % MOD;
                }
                else f[i][j] = 1;
            }
        }
    }
    int result = 0;
    for (int x : f[m - 1])
        result = (result + x) % MOD;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = specialPerm(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 6}, 2, trials);
    test({1, 4, 3}, 2, trials);
    return 0;
}


