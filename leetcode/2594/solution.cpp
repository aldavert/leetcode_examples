#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

long long repairCars(std::vector<int> ranks, int cars)
{
    long l = 0, r = static_cast<long>(*std::min_element(ranks.begin(), ranks.end()))
                  * cars * cars;
    auto numCarsFixed = [&](long minutes) -> long
    {
        long cars_fixed = 0;
        for (const int rank : ranks)
            cars_fixed += static_cast<long>(std::sqrt(minutes / rank));
        return cars_fixed;
    };
    while (l < r)
    {
        const long m = (l + r) / 2;
        if (numCarsFixed(m) >= cars) r = m;
        else l = m + 1;
    }
    return l;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> ranks, int cars, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = repairCars(ranks, cars);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 3, 1}, 10, 16, trials);
    test({5, 1, 8}, 6, 16, trials);
    return 0;
}


