#include "../common/common.hpp"
#include <unordered_map>
#include <cmath>

// ############################################################################
// ############################################################################

int mostFrequentPrime(std::vector<std::vector<int> > mat)
{
    constexpr int dirs[8][2] = {{ 1,  0}, { 1, -1}, { 0, -1}, {-1, -1},
                                {-1,  0}, {-1,  1}, { 0,  1}, { 1,  1}};
    const int m = static_cast<int>(mat.size()),
              n = static_cast<int>(mat[0].size());
    int result = -1, max_freq = 0;
    std::unordered_map<int, int> count;
    auto isPrime = [](int num) -> bool
    {
        for (int i = 2, e = static_cast<int>(std::sqrt(num)); i < e + 1; ++i)
            if (num % i == 0)
                return false;
        return true;
    };
    
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            for (const auto& [dx, dy] : dirs)
            {
                int num = 0;
                for (int x = i, y = j;
                     (0 <= x) && (x < m) && (0 <= y) && (y < n); x += dx, y += dy)
                {
                    num = num * 10 + mat[x][y];
                    if (num > 10 && isPrime(num))
                        ++count[num];
                }
            }
        }
    }
    for (const auto& [prime, freq] : count)
    {
        if (freq > max_freq)
        {
            result = prime;
            max_freq = freq;
        }
        else if (freq == max_freq)
            result = std::max(result, prime);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > mat, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = mostFrequentPrime(mat);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1}, {9, 9}, {1, 1}}, 19, trials);
    test({{7}}, -1, trials);
    return 0;
}


