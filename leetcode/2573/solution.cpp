#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string findTheString(std::vector<std::vector<int> > lcp)
{
    const int n = static_cast<int>(lcp.size());
    constexpr char NONLETTER = 'a' - 1;
    std::vector<char> word(n, NONLETTER);
    
    for (int i = 0, c = NONLETTER; i < n; ++i)
    {
        if (word[i] != NONLETTER) continue;
        if (++c > 'z') return "";
        for (int j = i; j < n; ++j)
            if (lcp[i][j] > 0)
                word[j] = static_cast<char>(c);
    }
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            int next_lcp = ((i + 1 < n)&&(j + 1 < n))?lcp[i + 1][j + 1]:0;
            int curr_lcp = (word[i] == word[j])?1 + next_lcp:0;
            if (lcp[i][j] != curr_lcp)
                return "";
        }
    }
    
    std::string result;
    for (char c : word)
        result += c;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > lcp,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findTheString(lcp);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{4, 0, 2, 0}, {0, 3, 0, 1}, {2, 0, 2, 0}, {0, 1, 0, 1}}, "abab", trials);
    test({{4, 3, 2, 1}, {3, 3, 2, 1}, {2, 2, 2, 1}, {1, 1, 1, 1}}, "aaaa", trials);
    test({{4, 3, 2, 1}, {3, 3, 2, 1}, {2, 2, 2, 1}, {1, 1, 1, 3}}, "", trials);
    return 0;
}


