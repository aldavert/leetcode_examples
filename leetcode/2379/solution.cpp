#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumRecolors(std::string blocks, int k)
{
    int count_black = 0, max_count_black = 0;
    for (int i = 0, n = static_cast<int>(blocks.size()); i < n; ++i)
    {
        if (blocks[i] == 'B') ++count_black;
        if ((i >= k) && (blocks[i - k] == 'B')) --count_black;
        max_count_black = std::max(max_count_black, count_black);
    }
    return k - max_count_black;
}

// ############################################################################
// ############################################################################

void test(std::string blocks, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumRecolors(blocks, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("WBBWWBBWBW", 7, 3, trials);
    test("WWWBWWBBWBW", 7, 3, trials);
    test("WBWWBBWBW", 7, 3, trials);
    test("WBWBBBW", 2, 0, trials);
    return 0;
}


