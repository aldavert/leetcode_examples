#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string triangleType(std::vector<int> nums)
{
    int a = nums[0], b = nums[1], c = nums[2];
    if ((a == b) && (a == c)) return "equilateral";
    if (a > b) std::swap(a, b);
    if (b > c) std::swap(b, c);
    if (a > b) std::swap(a, b);
    if (a + b <= c) return "none";
    if ((a == b) || (b == c)) return "isosceles";
    return "scalene";
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = triangleType(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 3, 3}, "equilateral", trials);
    test({3, 4, 5}, "scalene", trials);
    return 0;
}


