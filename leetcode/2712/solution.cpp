#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long minimumCost(std::string s)
{
    long result = 0;
    for (int i = 1, n = static_cast<int>(s.size()); i < n; ++i)
        if (s[i] != s[i - 1])
            result += std::min(i, n - i);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumCost(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("0011", 2, trials);
    test("010101", 9, trials);
    return 0;
}


