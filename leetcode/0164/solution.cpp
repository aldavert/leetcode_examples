#include "../common/common.hpp"
#include <cmath>
#include <limits>

// ############################################################################
// ############################################################################

int maximumGap(std::vector<int> &nums)
{
    struct Bucket
    {
        int mn = -1;
        int mx = -1;
    };
    if (nums.size() < 2) return 0;
    const int mn = *std::min_element(nums.begin(), nums.end());
    const int mx = *std::max_element(nums.begin(), nums.end());
    if (mn == mx) return 0;
    const int gap =
        static_cast<int>(std::ceil((mx - mn) / static_cast<double>(nums.size() - 1)));
    std::vector<Bucket> buckets((mx - mn) / gap + 1,
            {std::numeric_limits<int>::max(), std::numeric_limits<int>::lowest()});
    for (const int num : nums)
    {
        const int i = (num - mn) / gap;
        buckets[i].mn = std::min(buckets[i].mn, num);
        buckets[i].mx = std::max(buckets[i].mx, num);
    }
    int result = 0;
    for (int prev_max = mn; const Bucket& bucket : buckets)
    {
        if (bucket.mn == std::numeric_limits<int>::max()) continue;
        result = std::max(result, bucket.mn - prev_max);
        prev_max = bucket.mx;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumGap(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 6, 9, 1}, 3, trials);
    test({10}, 0, trials);
    return 0;
}


