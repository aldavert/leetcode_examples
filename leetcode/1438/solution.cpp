#include "../common/common.hpp"
#include <deque>

// ############################################################################
// ############################################################################

int longestSubarray(std::vector<int> nums, int limit)
{
    std::deque<int> q_min, q_max;
    int result = 1;
    
    for (int l = 0, r = 0, n = static_cast<int>(nums.size()); r < n; ++r)
    {
        while (!q_min.empty() && (q_min.back() > nums[r]))
            q_min.pop_back();
        q_min.push_back(nums[r]);
        while (!q_max.empty() && (q_max.back() < nums[r]))
            q_max.pop_back();
        q_max.push_back(nums[r]);
        while (q_max.front() - q_min.front() > limit)
        {
            if (q_min.front() == nums[l]) q_min.pop_front();
            if (q_max.front() == nums[l]) q_max.pop_front();
            ++l;
        }
        result = std::max(result, r - l + 1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int limit, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestSubarray(nums, limit);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 2, 4, 7}, 4, 2, trials);
    test({10, 1, 2, 4, 7, 2}, 5, 4, trials);
    test({4, 2, 2, 2, 4, 4, 2, 2}, 0, 3, trials);
    return 0;
}


