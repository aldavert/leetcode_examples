#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int largestMagicSquare(std::vector<std::vector<int> > grid)
{
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    std::vector<std::vector<int> > prefix_row(m, std::vector<int>(n + 1)),
                                   prefix_col(n, std::vector<int>(m + 1));
    auto sum = [&](const std::vector<std::vector<int> > &prefix, int i, int l, int r)
    {
        return prefix[i][r + 1] - prefix[i][l];
    };
    auto isMagicSquare = [&](int i, int j, int k) -> bool
    {
        int diagonal = 0, antidiagonal = 0;
        for (int d = 0; d < k; ++d)
        {
            diagonal += grid[i + d][j + d];
            antidiagonal += grid[i + d][j + k - 1 - d];
        }
        if (diagonal != antidiagonal) return false;
        for (int d = 0; d < k; ++d)
        {
            if (sum(prefix_row, i + d, j, j + k - 1) != diagonal) return false;
            if (sum(prefix_col, j + d, i, i + k - 1) != diagonal) return false;
        }
        return true;
    };
    auto containsMagicSquare = [&](int k) -> bool
    {
        for (int i = 0; i + k - 1 < m; ++i)
            for (int j = 0; j + k - 1 < n; ++j)
                if (isMagicSquare(i, j, k))
                    return true;
        return false;
    };
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            prefix_row[i][j + 1] = prefix_row[i][j] + grid[i][j];
            prefix_col[j][i + 1] = prefix_col[j][i] + grid[i][j];
        }
    }
    for (int k = std::min(m, n); k >= 2; --k)
        if (containsMagicSquare(k))
            return k;
    return 1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestMagicSquare(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{7, 1, 4, 5, 6}, {2, 5, 1, 6, 4}, {1, 5, 4, 3, 2}, {1, 2, 7, 3, 4}},
          3, trials);
    test({{5, 1, 3, 1}, {9, 3, 3, 1}, {1, 3, 3, 8}}, 2, trials);
    return 0;
}


