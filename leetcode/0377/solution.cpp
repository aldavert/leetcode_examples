#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int combinationSum4(std::vector<int> nums, int target)
{
    unsigned long dp[1001] = {};
    dp[0] = 1;
    for (int i = 1; i <= target; ++i)
        for (const int num : nums)
            if (i >= num)
                dp[i] += dp[i - num];
    return static_cast<int>(dp[target]);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = combinationSum4(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 4, 7, trials);
    test({9}, 3, 0, trials);
    return 0;
}

