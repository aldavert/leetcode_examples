#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

int minimumOperations(std::vector<int> nums)
{
    std::bitset<101> present;
    for (size_t i = 0, n = nums.size(); i < n; ++i) present[nums[i]] = true;
    int result = 0, accum = 0;
    for (int i = 1; i <= 100; ++i)
    {
        if (present[i] && (accum < i))
        {
            accum += (i - accum);
            ++result;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 5, 0, 3, 5}, 3, trials);
    test({1, 2, 3, 4, 5}, 5, trials);
    test({0}, 0, trials);
    return 0;
}


