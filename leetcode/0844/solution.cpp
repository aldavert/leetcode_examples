#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool backspaceCompare(std::string s, std::string t)
{
    auto process = [](std::string &str)
    {
        const size_t n = str.size();
        size_t j = 0;
        for (size_t i = 0; i < n; ++i)
        {
            if (str[i] == '#')
            {
                if (j > 0) --j;
            }
            else str[j++] = str[i];
        }
        str = str.substr(0, j);
    };
    process(s);
    process(t);
    return s == t;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string t, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = backspaceCompare(s, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ab#c", "ad#c",  true, trials);
    test("ab##", "c#d#",  true, trials);
    test("a#c" , "b"   , false, trials);
    return 0;
}


