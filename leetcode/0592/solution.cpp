#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

std::string fractionAddition(std::string expression)
{
    bool negative = false, numerator = true;
    int current_numerator = 0, current_denominator = 0,
        result_numerator = 0, result_denominator = 0;
    auto update = [&](void) -> void
    {
        current_numerator *= 1 - 2 * negative;
        if (result_denominator == 0)
        {
            result_numerator = current_numerator;
            result_denominator = current_denominator;
        }
        else
        {
            result_numerator = current_numerator * result_denominator
                             + result_numerator * current_denominator;
            result_denominator *= current_denominator;
            int reduction = std::gcd(result_numerator, result_denominator);
            result_numerator /= reduction;
            result_denominator /= reduction;
        }
        current_numerator = 0;
        current_denominator = 0;
    };
    for (char symbol : expression)
    {
        switch (symbol)
        {
        case '-':
            update();
            negative = true;
            numerator = true;
            break;
        case '+':
            update();
            negative = false;
            numerator = true;
            break;
        case '/':
            numerator = false;
            break;
        default:
            if (numerator)
                current_numerator = current_numerator * 10 + (symbol - '0');
            else current_denominator = current_denominator * 10 + (symbol - '0');
            break;
        }
    }
    update();
    if (result_numerator == 0) result_denominator = 1;
    return std::to_string(result_numerator) + '/' + std::to_string(result_denominator);
}

// ############################################################################
// ############################################################################

void test(std::string expression, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = fractionAddition(expression);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("-1/2+1/2", "0/1", trials);
    test("-1/2+1/2+1/3", "1/3", trials);
    test("1/3-1/2", "-1/6", trials);
    return 0;
}


