#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string breakPalindrome(std::string palindrome)
{
    const int n = static_cast<int>(palindrome.size());
    if (n <= 1) return "";
    for (int i = 0; i < n / 2; ++i)
    {
        if (palindrome[i] != 'a')
        {
            palindrome[i] = 'a';
            return palindrome;
        }
    }
    palindrome[n - 1] = 'b';
    return palindrome;
}

// ############################################################################
// ############################################################################

void test(std::string palindrome, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = breakPalindrome(palindrome);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abccba", "aaccba", trials);
    test("a", "", trials);
    test("aa", "ab", trials);
    test("aba", "abb", trials);
    return 0;
}


