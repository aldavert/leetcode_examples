#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maxProduct(std::string s)
{
    const int n = static_cast<int>(s.size());
    long long result = 1;
    auto manacher = [](const std::string st, int ns)
    {
        std::vector<int> max_extends(ns), l2r(ns, 1);
        int center = 0;
        
        for (int i = 0; i < ns; ++i)
        {
            const int r = center + max_extends[center] - 1;
            int extend = (i > r)?1:std::min(max_extends[center - (i - center)],
                                            r - i + 1);
            while ((i - extend >= 0)
               &&  (i + extend < ns)
               &&  (st[i - extend] == st[i + extend]))
            {
                l2r[i + extend] = 2 * extend + 1;
                ++extend;
            }
            max_extends[i] = extend;
            if (i + max_extends[i] >= r) center = i;
        }
        for (int i = 1; i < ns; ++i)
            l2r[i] = std::max(l2r[i], l2r[i - 1]);
        
        return l2r;
    };
    
    std::vector<int> l = manacher(s, n),
                     r = manacher(std::string(s.rbegin(), s.rend()), n);
    std::reverse(r.begin(), r.end());
    
    for (int i = 0; i + 1 < n; ++i)
        result = std::max(result, static_cast<long long>(l[i])
                                * static_cast<long long>(r[i + 1]));
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxProduct(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ababbb", 9, trials);
    test("zaaaxbbby", 9, trials);
    return 0;
}


