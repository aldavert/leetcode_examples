#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sumOddLengthSubarrays(std::vector<int> arr)
{
    const int n = static_cast<int>(arr.size());
    int result = 0;
    for (int i = 0; i < n; ++i)
        result += arr[i] * (((i + 1) * (n - i) + 1) / 2);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOddLengthSubarrays(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 2, 5, 3}, 58, trials);
    test({1, 2}, 3, trials);
    test({10, 11, 12}, 66, trials);
    return 0;
}


