#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

struct Employee
{
    int id = -1;
    int importance = -1;
    std::vector<int> subordinates;
};

int getImportance(std::vector<Employee *> employees, int id)
{
    Employee lut[2001] = {};
    for (const auto &employee : employees)
        lut[employee->id] = *employee;
    int result = 0;
    std::stack<int> active;
    active.push(id);
    while (!active.empty())
    {
        int current = active.top();
        active.pop();
        if (lut[current].id == -1) continue;
        lut[current].id = -1;
        result += lut[current].importance;
        for (int employee : lut[current].subordinates)
            active.push(employee);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<Employee> employees,
          int id,
          int solution,
          unsigned int trials = 1)
{
    std::vector<Employee *> employees_ptrs;
    for (auto &employee : employees)
        employees_ptrs.push_back(&employee);
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getImportance(employees_ptrs, id);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 5, {2, 3}}, {2, 3, {}}, {3, 3, {}}}, 1, 11, trials);
    test({{1, 2, {5}}, {5, -3, {}}}, 5, -3, trials);
    return 0;
}


