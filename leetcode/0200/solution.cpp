#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numIslands(std::vector<std::vector<char> > grid)
{
    const int nrows = static_cast<int>(grid.size());
    if (nrows == 0) return 0;
    const int ncols = static_cast<int>(grid[0].size());
    if (ncols == 0) return 0;
    short colored_grid[301][301];
    short coloring_lut[301 * 301];
    short number_of_regions = 0;
    
    // Special case: first row.
    if (grid[0][0] == '1')
    {
        coloring_lut[number_of_regions] = number_of_regions;
        colored_grid[0][0] = number_of_regions;
        ++number_of_regions;
    }
    else colored_grid[0][0] = -1;
    for (int x = 1; x < ncols; ++x)
    {
        if (grid[0][x] == '1')
        {
            if (grid[0][x - 1] == '1')
                colored_grid[0][x] = colored_grid[0][x - 1];
            else
            {
                coloring_lut[number_of_regions] = number_of_regions;
                colored_grid[0][x] = number_of_regions;
                ++number_of_regions;
            }
        }
        else colored_grid[0][x] = -1;
    }
    
    for (int y = 1; y < nrows; ++y)
    {
        // Special case: first element of the row.
        if (grid[y][0] == '1')
        {
            if (grid[y - 1][0] == '1')
                colored_grid[y][0] = colored_grid[y - 1][0];
            else
            {
                coloring_lut[number_of_regions] = number_of_regions;
                colored_grid[y][0] = number_of_regions;
                ++number_of_regions;
            }
        }
        else colored_grid[y][0] = -1;
        // General case.
        for (int x = 1; x < ncols; ++x)
        {
            if (grid[y][x] == '1')
            {
                if ((grid[y][x - 1] == '1') || (grid[y - 1][x] == '1'))
                {
                    if ((grid[y][x - 1] == '1') && (grid[y - 1][x] == '1'))
                    {
                        if (colored_grid[y][x - 1] == colored_grid[y - 1][x])
                            colored_grid[y][x] = colored_grid[y][x - 1];
                        else
                        {
                            short left = colored_grid[y][x - 1];
                            while (coloring_lut[left] != left)
                                left = coloring_lut[left];
                            short up = colored_grid[y - 1][x];
                            while (coloring_lut[up] != up)
                                up = coloring_lut[up];
                            if (left < up)
                            {
                                colored_grid[y][x] = left;
                                coloring_lut[up] = left;
                            }
                            else
                            {
                                colored_grid[y][x] = up;
                                coloring_lut[left] = up;
                            }
                        }
                    }
                    else if (grid[y][x - 1] == '1')
                        colored_grid[y][x] = colored_grid[y][x - 1];
                    else colored_grid[y][x] = colored_grid[y - 1][x];
                }
                else
                {
                    coloring_lut[number_of_regions] = number_of_regions;
                    colored_grid[y][x] = number_of_regions;
                    ++number_of_regions;
                }
            }
            else colored_grid[y][x] = -1;
        }
    }
    int result = 0;
    for (int i = 0; i < number_of_regions; ++i)
        if (coloring_lut[i] == i)
            ++result;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numIslands(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'1', '1', '1', '1', '0'},
          {'1', '1', '0', '1', '0'},
          {'1', '1', '0', '0', '0'},
          {'0', '0', '0', '0', '0'}}, 1, trials);
    test({{'1', '1', '0', '0', '0'},
          {'1', '1', '0', '0', '0'},
          {'0', '0', '1', '0', '0'},
          {'0', '0', '0', '1', '1'}}, 3, trials);
    test({{'1', '1', '1'},
          {'0', '1', '0'},
          {'1', '1', '1'}}, 1, trials);
    return 0;
}


