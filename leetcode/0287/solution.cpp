#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

#if 0
int findDuplicate(std::vector<int> nums)
{
    int slow = nums[nums[0]];
    int fast = nums[nums[nums[0]]];
    while (slow != fast)
    {
        slow = nums[slow];
        fast = nums[nums[fast]];
    }
    slow = nums[0];
    while (slow != fast)
    {
        slow = nums[slow];
        fast = nums[fast];
    }
    return slow;
}
#else
int findDuplicate(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    const int nbits = static_cast<int>(std::log2(n)) + 1;
    int result = 0;
    for (int p = 0, bit = 1; p < nbits; ++p, bit <<= 1)
    {
        int expected = 0, obtained = ((nums[0] & bit) > 0);
        for (int i = 1; i < n; ++i)
        {
            expected += ((     i  & bit) > 0);
            obtained += ((nums[i] & bit) > 0);
        }
        result += (obtained > expected) * bit;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findDuplicate(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 4, 2, 2}, 2, trials);
    test({3, 1, 3, 4, 2}, 3, trials);
    test({3, 3, 3, 4, 2}, 3, trials);
    return 0;
}


