#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> minBitwiseArray(std::vector<int> nums)
{
    std::vector<int> result(nums.size());
    for (size_t i = 0; i < nums.size(); ++i)
    {
        if (nums[i] == 2)
        {
            result[i] = -1;
            continue;
        }
        int value = nums[i], index = 0;
        while (value & 1)
            ++index, value >>= 1;
        result[i] = std::max(-1, nums[i]^(1 << (index - 1)));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minBitwiseArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 5, 7}, {-1, 1, 4, 3}, trials);
    test({11, 13, 31}, {9, 12, 15}, trials);
    return 0;
}


