#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestPalindrome(std::vector<std::string> words)
{
    int palindromes[26][26] = {};
    int result = 0;
    for (auto word : words)
    {
        const int i = static_cast<int>(word[0] - 'a');
        const int j = static_cast<int>(word[1] - 'a');
        if (palindromes[j][i])
        {
            result += 4;
            --palindromes[j][i];
        }
        else ++palindromes[i][j];
    }
    for (int i = 0; i < 26; ++i)
        if (palindromes[i][i])
            return result + 2;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestPalindrome(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"lc", "cl", "gg"}, 6, trials);
    test({"ab", "ty", "yt", "lc", "cl", "ab"}, 8, trials);
    test({"cc", "ll", "xx"}, 2, trials);
    return 0;
}


