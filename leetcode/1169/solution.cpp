#include "../common/common.hpp"
#include <sstream>
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<std::string> invalidTransactions(std::vector<std::string> transactions)
{
    struct Transaction
    {
        std::string name;
        int time;
        int amount;
        std::string city;
    };
    auto getTransaction = [&](const std::string &t) -> Transaction
    {
        std::istringstream iss(t);
        std::vector<std::string> s(4, "");
        for (int i = 0; std::getline(iss, s[i++], ','););
        return { s[0], std::stoi(s[1]), std::stoi(s[2]), s[3] };
    };
    std::unordered_map<std::string, std::vector<Transaction> > name_transaction_lut;
    
    for (const std::string &t : transactions)
    {
        Transaction transaction = getTransaction(t);
        name_transaction_lut[transaction.name].push_back(transaction);
    }
    std::vector<std::string> result;
    for (const std::string &t : transactions)
    {
        const Transaction current = getTransaction(t);
        if (current.amount > 1000)
            result.push_back(t);
        else if (const auto it = name_transaction_lut.find(current.name);
                 it != name_transaction_lut.cend())
        {
            for (Transaction transaction : it->second)
            {
                if ((std::abs(transaction.time - current.time) <= 60)
                &&  (transaction.city != current.city))
                {
                    result.push_back(t);
                    break;
                }
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<std::string> left_work(left), right_work(right);
    std::sort(left_work.begin(), left_work.end());
    std::sort(right_work.begin(), right_work.end());
    for (size_t i = 0; i < left_work.size(); ++i)
        if (left_work[i] != right_work[i])
            return false;
    return true;
}

void test(std::vector<std::string> transactions,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = invalidTransactions(transactions);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"alice, 20, 800, mtv", "alice, 50, 100, beijing"},
         {"alice, 20, 800, mtv", "alice, 50, 100, beijing"}, trials);
    test({"alice, 20, 800, mtv", "alice, 50, 1200, mtv"},
         {"alice, 50, 1200, mtv"}, trials);
    test({"alice, 20, 800, mtv", "bob, 50, 1200, mtv"}, {"bob, 50, 1200, mtv"}, trials);
    return 0;
}


