#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > closestNodes(TreeNode * root, std::vector<int> queries)
{
    std::vector<std::vector<int> > result;
    std::vector<int> sorted_values;
    auto inorder = [&](auto &&self, TreeNode * node)
    {
        if (node == nullptr) return;
        self(self, node->left);
        sorted_values.push_back(node->val);
        self(self, node->right);
    };
    inorder(inorder, root);
    for (const int query : queries)
    {
        auto it = std::lower_bound(sorted_values.begin(), sorted_values.end(), query);
        if ((it != sorted_values.end()) && (*it == query))
            result.push_back({query, query});
        else result.push_back({(it == sorted_values.begin())?-1:*std::prev(it),
                               (it == sorted_values.end())?-1:*it});
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          std::vector<int> queries,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = closestNodes(root, queries);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({6, 2, 13, 1, 4, 9, 15, null, null, null, null, null, null, 14},
         {2, 5, 16}, {{2, 2}, {4, 6}, {15, -1}}, trials);
    test({4, null, 9}, {3}, {{-1, 4}}, trials);
    return 0;
}


