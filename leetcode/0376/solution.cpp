#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int wiggleMaxLength(std::vector<int> nums)
{
    if      (nums.size() == 1) return 1;
    else if (nums.size() == 2) return 1 + (nums[0] != nums[1]);
    int result = 0;
    for (int previous = nums[0], sign = 0; int n : nums)
    {
        int diff = n - previous;
        if (((diff < 0) && (sign >= 0))
        ||  ((diff > 0) && (sign <= 0)))
        {
            ++result;
            previous = n;
            sign = diff;
        }
        else
        {
            if (((diff < 0) && (n < previous))
            ||  ((diff > 0) && (n > previous)))
                previous = n;
        }
    }
    return result + 1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = wiggleMaxLength(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 7, 4, 9, 2, 5}, 6, trials);
    test({1, 17, 5, 10, 13, 15, 10, 5, 16, 8}, 7, trials);
    test({1, 2, 3, 4, 5, 6, 7, 8, 9}, 2, trials);
    return 0;
}


