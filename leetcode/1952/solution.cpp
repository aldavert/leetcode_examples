#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isThree(int n)
{
    int divisors = 1;
    for (int i = 2; i <= n; ++i)
        divisors += n % i == 0;
    return divisors == 3;
}

// ############################################################################
// ############################################################################

void test(int n, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isThree(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, false, trials);
    test(4, true, trials);
    return 0;
}


