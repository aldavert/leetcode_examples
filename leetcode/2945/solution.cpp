#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findMaximumLength(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<long> prefix(n + 1);
    std::vector<int> dp(n + 1), best_left(n + 2);
    for (int i = 0; i < n; ++i)
    prefix[i + 1] = prefix[i] + nums[i];
    for (int i = 1; i <= n; ++i)
    {
        best_left[i] = std::max(best_left[i], best_left[i - 1]);
        int l = best_left[i];
        auto search =
            std::lower_bound(prefix.begin(), prefix.end(), 2 * prefix[i] - prefix[l]);
        int r = static_cast<int>(std::distance(prefix.begin(), search));
        dp[i] = dp[l] + 1;
        best_left[r] = i;
    }
    return dp[n];
}

// ############################################################################
// ############################################################################

void test(const std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMaximumLength(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 2, 2}, 1, trials);
    test({1, 2, 3, 4}, 4, trials);
    test({4, 3, 2, 6}, 3, trials);
    return 0;
}


