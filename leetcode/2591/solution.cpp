#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int distMoney(int money, int children)
{
    money -= children;
    if (money < 0) return -1;
    int result = money / 7, remainder = money % 7;
    if ((result == children) && (remainder == 0)) return result;
    if ((result == children - 1) && (remainder == 3)) return result - 1;
    return std::min(children - 1, result);
}

// ############################################################################
// ############################################################################

void test(int money, int children, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = distMoney(money, children);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(20, 3, 1, trials);
    test(16, 2, 2, trials);
    test(2, 2, 0, trials);
    return 0;
}


