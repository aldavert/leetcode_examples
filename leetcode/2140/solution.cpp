#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long mostPoints(std::vector<std::vector<int> > questions)
{
    const int n = static_cast<int>(questions.size());
    std::vector<long long> dp(n + 1);
    for (int i = n - 1; i >= 0; --i)
    {
        int next_index = i + questions[i][1] + 1;
        dp[i] = std::max(questions[i][0] + ((next_index < n)?dp[next_index]:0),
                         dp[i + 1]);
    }
    return dp[0];
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > questions,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = mostPoints(questions);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 2}, {4, 3}, {4, 4}, {2, 5}}, 5, trials);
    test({{1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}}, 7, trials);
    return 0;
}



