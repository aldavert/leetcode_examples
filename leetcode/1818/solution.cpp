#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int minAbsoluteSumDiff(std::vector<int> nums1, std::vector<int> nums2)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(nums1.size());
    int max_difference = 0, result = 0;
    std::vector<int> difference(n);
    
    for (int i = 0; i < n; i++)
        difference[i] = abs(nums1[i] - nums2[i]);
    std::sort(nums1.begin(), nums1.end());
    for (int i = 0; i < n; i++)
    {
        int current_diff = 0;
        int index = static_cast<int>(std::lower_bound(nums1.cbegin(), nums1.cend(),
                                     nums2[i]) - nums1.cbegin());
        if (index == 0) current_diff = nums1[0] - nums2[i];
        else if (index == n) current_diff = nums2[i] - nums1[n - 1];
        else
        {
            current_diff = std::min(nums1[index] - nums2[i],
                                    nums2[i] - nums1[index - 1]);
        }
        max_difference = std::max(max_difference, difference[i] - current_diff);
    }
    for (int i = 0; i < n; i++)
        result = (result + difference[i]) % MOD;
    return (result - max_difference + MOD) % MOD;
}
#else
int minAbsoluteSumDiff(std::vector<int> nums1, std::vector<int> nums2)
{
    constexpr int MOD = 1'000'000'007;
    long sum_difference = 0, max_decrement = 0;
    std::set<int> sorted(nums1.begin(), nums1.end());
    
    for (size_t i = 0; i < nums1.size(); ++i)
    {
        const long current_difference = std::abs(nums1[i] - nums2[i]);
        sum_difference += current_difference;
        auto it = sorted.lower_bound(nums2[i]);
        if (it != sorted.begin())
            max_decrement = std::max(max_decrement, current_difference
                                                  - abs(*prev(it) - nums2[i]));
        if (it != sorted.end())
            max_decrement = std::max(max_decrement, current_difference
                                                  - abs(*it - nums2[i]));
    }
    return static_cast<int>((sum_difference - max_decrement) % MOD);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minAbsoluteSumDiff(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 7, 5}, {2, 3, 5}, 3, trials);
    test({2, 4, 6, 8, 10}, {2, 4, 6, 8, 10}, 0, trials);
    test({1, 10, 4, 4, 2, 7}, {9, 3, 5, 1, 7, 4}, 20, trials);
    return 0;
}


