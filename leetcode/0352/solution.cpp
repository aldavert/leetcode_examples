#include "../common/common.hpp"
#include <set>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class SummaryRanges
{
    std::set<int> m_elements;
public:
    SummaryRanges()
    {
    }
    
    void addNum(int val)
    {
        m_elements.insert(val);
    }
    
    std::vector<std::vector<int> > getIntervals()
    {
        std::vector<std::vector<int> > result;
        if (!m_elements.empty())
        {
            int prev = -1, start = -1;
            for (int v : m_elements)
            {
                if (start == -1) start = prev = v;
                else if (prev + 1 == v) prev = v;
                else
                {
                    result.push_back({start, prev});
                    start = prev = v;
                }
            }
            result.push_back({start, prev});
        }
        return result;
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<bool> add_element,
          std::vector<int> elements,
          std::vector<std::vector<std::vector<int> > > solution,
          unsigned int trials = 1)
{
    const int n = static_cast<int>(add_element.size());
    std::vector<std::vector<std::vector<int> > > result(n);
    for (unsigned int t = 0; t < trials; ++t)
    {
        SummaryRanges sr;
        for (int i = 0; i < n; ++i)
        {
            if (add_element[i])
                sr.addNum(elements[i]);
            else result[i] = sr.getIntervals();
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({true, false, true, false, true, false, true, false, true, false},
         {   1,  null,    3,  null,    7,  null,    2,  null,    6,  null},
         {{}, {{1, 1}},
          {}, {{1, 1}, {3, 3}},
          {}, {{1, 1}, {3, 3}, {7, 7}},
          {}, {{1, 3}, {7, 7}},
          {}, {{1, 3}, {6, 7}}}, trials);
    return 0;
}


