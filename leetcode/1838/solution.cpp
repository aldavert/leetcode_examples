#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxFrequency(std::vector<int> nums, int k)
{
    if (nums.size() <= 1) return static_cast<int>(nums.size());
    std::sort(nums.begin(), nums.end());
    int result = 1;
    long ops = 0;
    for (size_t l = 0, r = 0, n = nums.size() - 1; (l < n) && (r < n); )
    {
        if (ops <= static_cast<long>(k))
        {
            ops += (nums[r + 1] - nums[r]) * static_cast<long>(r - l + 1);
            ++r;
        }
        else
        {
            ops -= (nums[r] - nums[l]);
            ++l;
        }
        if (ops <= static_cast<long>(k))
            result = std::max(result, static_cast<int>(r - l + 1));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxFrequency(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 4}, 5, 3, trials);
    test({1, 4, 8, 13}, 5, 2, trials);
    test({3, 9, 6}, 2, 1, trials);
    test({9930, 9923, 9983, 9997, 9934, 9952, 9945, 9914, 9985, 9982, 9970,
          9932, 9985, 9902, 9975, 9990, 9922, 9990, 9994, 9937, 9996, 9964,
          9943, 9963, 9911, 9925, 9935, 9945, 9933, 9916, 9930, 9938, 10000,
          9916, 9911, 9959, 9957, 9907, 9913, 9916, 9993, 9930, 9975, 9924,
          9988, 9923, 9910, 9925, 9977, 9981, 9927, 9930, 9927, 9925, 9923,
          9904, 9928, 9928, 9986, 9903, 9985, 9954, 9938, 9911, 9952, 9974,
          9926, 9920, 9972, 9983, 9973, 9917, 9995, 9973, 9977, 9947, 9936,
          9975, 9954, 9932, 9964, 9972, 9935, 9946, 9966}, 3056, 73, trials);
    std::vector<int> long_test(99'991, 1);
    long_test.back() = 100'000;
    test(long_test, 100'000, 99'990, trials);
    return 0;
}


