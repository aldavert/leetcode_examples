#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<int> shortestAlternatingPaths(int n,
                                          std::vector<std::vector<int> > redEdges,
                                          std::vector<std::vector<int> > blueEdges)
{
    struct Visitor
    {
        int node = -1;
        bool is_red = false; 
    };
    std::vector<int> distances[2] = {
        std::vector<int>(n, std::numeric_limits<int>::max()),
        std::vector<int>(n, std::numeric_limits<int>::max()) };
    std::vector<int> result(n, -1);
    std::vector<std::vector<int> > edges[2] = {
        std::vector<std::vector<int> >(n),
        std::vector<std::vector<int> >(n) };
    for (auto edge : redEdges ) edges[1][edge[0]].push_back(edge[1]);
    for (auto edge : blueEdges) edges[0][edge[0]].push_back(edge[1]);
    std::queue<Visitor> active;
    active.push({0, false});
    active.push({0, true });
    for (int distance = 0; !active.empty(); ++distance)
    {
        for (size_t i = 0, m = active.size(); i < m; ++i)
        {
            auto [node, is_red] = active.front();
            active.pop();
            if (distance >= distances[is_red][node]) continue;
            distances[is_red][node] = distance;
            if (distance < distances[!is_red][node])
                result[node] = distance;
            for (int next : edges[!is_red][node])
                active.push({next, !is_red});
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > redEdges,
          std::vector<std::vector<int> > blueEdges,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestAlternatingPaths(n, redEdges, blueEdges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{0, 1}, {1, 2}}, {}, {0, 1, -1}, trials);
    test(3, {{0, 1}}, {{2, 1}}, {0, 1, -1}, trials);
    test(3, {{0, 1}}, {{1, 2}}, {0, 1, 2}, trials);
    return 0;
}


