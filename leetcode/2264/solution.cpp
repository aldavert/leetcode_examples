#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string largestGoodInteger(std::string num)
{
    char max_digit = -1;
    for (int i = 2, n = static_cast<int>(num.size()); i < n; ++i)
        if ((num[i] == num[i - 1]) && (num[i] == num[i - 2]))
            max_digit = std::max(max_digit, num[i]);
    return (max_digit >= 0)?std::string({max_digit, max_digit, max_digit}):"";
}

// ############################################################################
// ############################################################################

void test(std::string num, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestGoodInteger(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("6777133339", "777", trials);
    test("2300019", "000", trials);
    test("42352338", "", trials);
    return 0;
}


