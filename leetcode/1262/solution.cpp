#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxSumDivThree(std::vector<int> nums)
{
    int dp[3] = {};
    for (int num : nums)
    {
        int prev[3] = { dp[0], dp[1], dp[2] };
        for (size_t i = 0; i < 3; ++i)
            dp[(prev[i] + num) % 3] = std::max(dp[(prev[i] + num) % 3], prev[i] + num);
    }
    return dp[0];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSumDivThree(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 6, 5, 1, 8}, 18, trials);
    test({4}, 0, trials);
    test({1, 2, 3, 4, 4}, 12, trials);
    return 0;
}


