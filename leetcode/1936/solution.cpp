#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int addRungs(std::vector<int> rungs, int dist)
{
    int result = 0;
    for (int prev = 0; int rung : rungs)
        result += (rung - std::exchange(prev, rung) - 1) / dist;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> rungs, int dist, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = addRungs(rungs, dist);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 5, 10}, 2, 2, trials);
    test({3, 6, 8, 10}, 3, 0, trials);
    test({3, 4, 6, 7}, 2, 1, trials);
    return 0;
}


