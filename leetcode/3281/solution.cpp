#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxPossibleScore(std::vector<int> start, int d)
{
    const int n = static_cast<int>(start.size());
    auto isPossible = [&](long m) -> bool
    {
        long last_pick = start[0];
        for (int i = 1; i < n; ++i)
        {
            if (last_pick + m > start[i] + d) return false;
            last_pick = std::max(last_pick + m, static_cast<long>(start[i]));
        }
        return true;
    };
    std::sort(start.begin(), start.end());
    long l = 0, r = (start.back() + d) - start.front() + 1;
    while (l < r)
    {
        if (long m = (l + r) / 2; isPossible(m)) l = m + 1;
        else r = m;
    }
    return static_cast<int>(l - 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> start, int d, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxPossibleScore(start, d);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({6, 0, 3}, 2, 4, trials);
    test({2, 6, 13, 13}, 5, 5, trials);
    return 0;
}


