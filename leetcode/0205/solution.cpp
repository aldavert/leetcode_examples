#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
bool isIsomorphic(std::string s, std::string t)
{
    char chr_lut[128];
    bool chr_used[128] = {};
    for (int i = 0; i < 128; ++i)
        chr_lut[i] = -1;
    const size_t n = s.size();
    if (n != t.size()) return false; // This is no necessary.
    for (size_t i = 0; i < n; ++i)
    {
        if (chr_lut[static_cast<size_t>(s[i])] == -1) // Not in the LUT.
        {
            // The character in t[i] has already been replaced by a different character.
            if (chr_used[static_cast<size_t>(t[i])]) return false;
            chr_used[static_cast<size_t>(t[i])] = true;
            chr_lut[static_cast<size_t>(s[i])] = t[i];
        }
        // The character has to be replaced by the same character.
        else if (chr_lut[static_cast<size_t>(s[i])] != t[i]) return false;
    }
    return true;
}
#else
bool isIsomorphic(std::string s, std::string t)
{
    std::unordered_map<char, char> chr_lut;
    std::unordered_set<char> chr_used;
    const size_t n = s.size();
    if (n != t.size()) return false; // This is no necessary.
    for (size_t i = 0; i < n; ++i)
    {
        if (auto it = chr_lut.find(s[i]); it == chr_lut.end()) // Not in the LUT.
        {
            // The character in t[i] has already been replaced by a different character.
            if (chr_used.find(t[i]) != chr_used.end()) return false;
            chr_used.insert(t[i]);
            chr_lut[s[i]] = t[i];
        }
        // The character has to be replaced by the same character.
        else if (it->second != t[i]) return false;
    }
    return true;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, std::string t, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isIsomorphic(s, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("egg", "add", true, trials);
    test("foo", "bar", false, trials);
    test("paper", "title", true, trials);
    return 0;
}


