#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

long long kSum(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> abs_nums;
    long long max_sum = 0;
    for (int number : nums)
    {
        if (number > 0) max_sum += number;
        abs_nums.push_back(std::abs(number));
    }
    std::sort(abs_nums.begin(), abs_nums.end());
    
    long long result = max_sum;
    std::priority_queue<std::pair<long long, int> > heap;
    heap.push({max_sum - abs_nums[0], 0});
    for (int j = 0; j < k - 1; ++j)
    {
        auto [next_max_sum, i] = heap.top();
        heap.pop();
        result = next_max_sum;
        if (i + 1 < n)
        {
            heap.push({next_max_sum - abs_nums[i + 1], i + 1});
            heap.push({next_max_sum - abs_nums[i + 1] + abs_nums[i], i + 1});
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = kSum(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4, -2}, 5, 2, trials);
    test({1, -2, 3, 4, -10, 12}, 16, 10, trials);
    return 0;
}


