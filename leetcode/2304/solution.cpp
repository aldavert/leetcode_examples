#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minPathCost(std::vector<std::vector<int> > grid,
                std::vector<std::vector<int> > moveCost)
{
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    std::vector<std::vector<int> > dp(m, std::vector<int>(n,
                                                std::numeric_limits<int>::max()));
    dp[0] = grid[0];
    for (int i = 1; i < m; ++i)
        for (int j = 0; j < n; ++j)
            for (int k = 0; k < n; ++k)
                dp[i][j] = std::min(dp[i][j], dp[i - 1][k]
                         + moveCost[grid[i - 1][k]][j] + grid[i][j]);
    return *std::min_element(dp.back().begin(), dp.back().end());
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<std::vector<int> > moveCost,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minPathCost(grid, moveCost);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{5, 3}, {4, 0}, {2, 1}},
         {{9, 8}, {1, 5}, {10, 12}, {18, 6}, {2, 4}, {14, 3}}, 17, trials);
    test({{5, 1, 2}, {4, 0, 3}},
         {{12, 10, 15}, {20, 23, 8}, {21, 7, 1}, {8, 1, 13}, {9, 10, 25}, {5, 3, 2}},
         6, trials);
    return 0;
}


