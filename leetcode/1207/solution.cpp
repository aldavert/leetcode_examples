#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool uniqueOccurrences(std::vector<int> arr)
{
    int histogram[2001] = {};
    bool frequency[1001] = {};
    for (int v : arr)
        ++histogram[v + 1000];
    for (int i = 0; i < 2001; ++i)
    {
        if (histogram[i])
        {
            if (frequency[histogram[i]])
                return false;
            frequency[histogram[i]] = true;
        }
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, bool solution, unsigned int trials = 1)
{
    int result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = uniqueOccurrences(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 1, 1, 3}, true, trials);
    test({1, 2}, false, trials);
    test({-3, 0, 1, -3, 1, 1, 1, -3, 10, 0}, true, trials);
    return 0;
}


