#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> maxSumOfThreeSubarrays(std::vector<int> nums, int k)
{
    const int m = static_cast<int>(nums.size()),
              n = m - k + 1;
    std::vector<int> sums(n), l(n), r(n), result{-1, -1, -1};
    
    int sum = 0;
    for (int i = 0; i < k - 1; ++i)
        sum += nums[i];
    sums[0] = sum += nums[k - 1];
    for (int i = k; i < m; ++i)
        sums[i - k + 1] = sum += nums[i] - nums[i - k];
    
    for (int i = 0, max_index = 0; i < n; ++i)
    {
        if (sums[i] > sums[max_index]) max_index = i;
        l[i] = max_index;
    }
    for (int i = n - 1, max_index = n - 1; i >= 0; --i)
    {
        if (sums[i] >= sums[max_index]) max_index = i;
        r[i] = max_index;
    }
    
    for (int i = k; i < n - k; ++i)
    {
        if ((result[0] == -1)
        || (sums[result[0]] + sums[result[1]] + sums[result[2]]
            < sums[l[i - k]] + sums[i] + sums[r[i + k]]))
        {
            result[0] = l[i - k];
            result[1] = i;
            result[2] = r[i + k];
        }
    }
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSumOfThreeSubarrays(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1, 2, 6, 7, 5, 1}, 2, {0, 3, 5}, trials);
    test({1, 2, 1, 2, 1, 2, 1, 2, 1}, 2, {0, 2, 4}, trials);
    return 0;
}


