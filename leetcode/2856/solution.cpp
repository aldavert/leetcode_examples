#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int minLengthAfterRemovals(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int freq = 1, cnt = 1;
    for (int i = 1; i < n; ++i)
    {
        if (nums[i] == nums[i - 1]) ++cnt;
        else
        {
            freq = std::max(freq, cnt);
            cnt = 1;
        }
    }
    freq = std::max(cnt, freq);
    if ((n - freq) >= freq)
        return n & 1;
    return 2 * freq - n;
}
#else
int minLengthAfterRemovals(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::unordered_map<int, int> count;
    int max_freq = 0;
    for (const int num : nums)
        ++count[num];
    for (const auto& [_, freq] : count)
        max_freq = std::max(max_freq, freq);
    if (max_freq <= n / 2)
        return n % 2;
    return max_freq - (n - max_freq);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minLengthAfterRemovals(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 0, trials);
    test({1, 1, 2, 2, 3, 3}, 0, trials);
    test({1000000000, 1000000000}, 2, trials);
    test({2, 3, 4, 4, 4}, 1, trials);
    return 0;
}


