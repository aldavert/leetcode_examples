#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int lastStoneWeight(std::vector<int> stones)
{
    std::priority_queue<int> heap(stones.begin(), stones.end());
    while (heap.size() > 1)
    {
        int weight = heap.top();
        heap.pop();
        weight -= heap.top();
        heap.pop();
        if (weight) heap.push(weight);
    }
    return (heap.empty()?0:heap.top());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> stones, int solution, unsigned int trials = 1)
{
    int result = 0;
    for (unsigned int i = 0; i < trials; ++i)
        result = lastStoneWeight(stones);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 7, 4, 1, 8, 1}, 1, trials);
    test({1}, 1, trials);
    return 0;
}


