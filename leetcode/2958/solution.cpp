#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int maxSubarrayLength(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    int left = 0, right = 0, result = 0;
    std::unordered_map<int, int> histogram;
    while (right < n)
    {
        if (auto search = histogram.find(nums[right]); search != histogram.end())
        {
            while ((left < right) && (search->second >= k))
                    --histogram[nums[left++]];
            ++search->second;
            ++right;
            result = std::max(result, right - left);
        }
        else
        {
            histogram[nums[right++]] = 1;
            result = std::max(result, right - left);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSubarrayLength(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 1, 2, 3, 1, 2}, 2, 6, trials);
    test({1, 2, 1, 2, 1, 2, 1, 2}, 1, 2, trials);
    test({5, 5, 5, 5, 5, 5, 5}, 4, 4, trials);
    return 0;
}


