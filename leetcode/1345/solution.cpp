#include "../common/common.hpp"
#include <unordered_map>
#include <queue>

// ############################################################################
// ############################################################################

int minJumps(std::vector<int> arr)
{
    constexpr int LIMIT = std::numeric_limits<int>::max();
    const int n = static_cast<int>(arr.size());
    std::vector<int> cost(n, LIMIT);
    std::unordered_map<int, std::vector<int> > positions;
    std::queue<int> available;
    cost[n - 1] = 0;
    for (int i = 0; i < n; ++i)
        positions[arr[i]].push_back(i);
    available.push(n - 1);
    while (!available.empty())
    {
        int pos = available.front();
        if (pos == 0) break;
        int new_cost = cost[pos] + 1;
        available.pop();
        if ((pos > 0) && (cost[pos - 1] == LIMIT))
        {
            cost[pos - 1] = new_cost;
            available.push(pos - 1);
        }
        if ((pos < n - 1) && (cost[pos + 1] == LIMIT))
        {
            cost[pos + 1] = new_cost;
            available.push(pos + 1);
        }
        for (int jump_pos : positions[arr[pos]])
        {
            if (cost[jump_pos] == LIMIT)
            {
                cost[jump_pos] = new_cost;
                available.push(jump_pos);
            }
        }
        positions[arr[pos]].clear();
    }
    return cost[0];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minJumps(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({100, -23, -23, 404, 100, 23, 23, 23, 3, 404}, 3, trials);
    test({7}, 0, trials);
    test({7, 6, 9, 6, 9, 6, 9, 7}, 1, trials);
    std::vector<int> big_test(50'000, 7);
    big_test[0] = 11;
    test(big_test, 2);
    return 0;
}


