#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int waysToSplitArray(std::vector<int> nums)
{
    long prefix = 0, suffix = std::accumulate(nums.begin(), nums.end(), 0L);
    int result = 0;
    
    for (int i = 0, n = static_cast<int>(nums.size()) - 1; i < n; ++i)
    {
        prefix += nums[i];
        suffix -= nums[i];
        result += (prefix >= suffix);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = waysToSplitArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 4, -8, 7}, 2, trials);
    test({2, 3, 1, 0}, 2, trials);
    return 0;
}


