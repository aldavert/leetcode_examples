#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countCollisions(std::string directions)
{
    const int n = static_cast<int>(directions.size());
    int l = 0, r = n - 1;
    
    while ((l < n) && (directions[l] == 'L'))
        ++l;
    while ((r >= 0) && (directions[r] == 'R'))
        --r;
    return static_cast<int>(std::count_if(directions.begin() + l,
                                          directions.begin() + r + 1,
                                          [](char c) { return c != 'S'; }));
}

// ############################################################################
// ############################################################################

void test(std::string directions, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countCollisions(directions);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("RLRSLL", 5, trials);
    test("LLRR", 0, trials);
    return 0;
}


