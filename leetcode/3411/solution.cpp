#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int maxLength(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = 1;
    for (int i = 0; i < n; ++i)
    {
        int gcd_val = nums[i], lcm_val = nums[i], prod_val = nums[i];
        for (int j = i + 1; j < n; ++j)
        {
            gcd_val = std::gcd(gcd_val, nums[j]);
            lcm_val = std::lcm(lcm_val, nums[j]);
            prod_val *= nums[j];
            if (prod_val == gcd_val * lcm_val)
                result = std::max(result, j - i + 1);
            else break;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxLength(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1, 2, 1, 1, 1}, 5, trials);
    test({2, 3, 4, 5, 6}, 3, trials);
    test({1, 2, 3, 1, 4, 5, 1}, 5, trials);
    test({1, 3, 9, 5, 9, 5, 1, 5, 7, 5, 5, 8, 6, 3, 7}, 3, trials);
    test({1, 2, 8, 3, 5, 5, 9, 3, 5, 6, 7, 5, 5, 6, 6, 7, 7, 3, 5, 1, 2, 2, 1,
          7, 6, 8, 3, 4, 7, 3, 1, 5, 5, 4, 4, 6, 8, 8, 7, 6, 3, 8, 7, 3, 6, 10,
          8, 7, 4, 8}, 5, trials);
    return 0;
}


