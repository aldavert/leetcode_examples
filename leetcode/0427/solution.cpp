#include "../common/common.hpp"
#include <queue>

constexpr int null = -1000;

class Node
{
public:
    bool val = false;
    bool isLeaf = false;
    Node * topLeft = nullptr;
    Node * topRight = nullptr;
    Node * bottomLeft = nullptr;
    Node * bottomRight = nullptr;
    
    Node(void) = default;
    Node(bool _val,
         bool _isLeaf,
         Node * _topLeft = nullptr,
         Node * _topRight = nullptr,
         Node * _bottomLeft = nullptr,
         Node * _bottomRight = nullptr) :
        val(_val),
        isLeaf(_isLeaf),
        topLeft(_topLeft),
        topRight(_topRight),
        bottomLeft(_bottomLeft),
        bottomRight(_bottomRight) {}
    ~Node(void)
    {
        delete topLeft;
        delete topRight;
        delete bottomLeft;
        delete bottomRight;
    }
};

// ############################################################################
// ############################################################################

Node * construct(std::vector<std::vector<int> > grid)
{
    struct Partition
    {
        int x_min = -1;
        int y_min = -1;
        int x_max = -1;
        int y_max = -1;
        inline int area(void) const { return (y_max - y_min) * (x_max - x_min); }
    };
    const int n = static_cast<int>(grid.size());
    std::vector<std::vector<int> > integral(n + 1, std::vector<int>(n + 1, 0));
    for (int y = 1; y <= n; ++y)
        for (int x = 1; x <= n; ++x)
            integral[y][x] = integral[y - 1][x    ] + integral[y][x - 1]
                           - integral[y - 1][x - 1] + grid[y - 1][x - 1];
    auto countOnes = [&](Partition p) -> int
    {
        return integral[p.y_max][p.x_max] - integral[p.y_max][p.x_min]
             - integral[p.y_min][p.x_max] + integral[p.y_min][p.x_min];
    };
    auto build = [&](auto &&self, Partition p) -> Node *
    {
        int area = p.area();
        if (area == 0) return nullptr;
        if (area == 1) return new Node(grid[p.y_min][p.x_min], true);
        int ones = countOnes(p);
        int zeros = p.area() - ones;
        Node * current = new Node();
        current->isLeaf = (ones == 0) || (zeros == 0);
        current->val = ((ones != 0) && current->isLeaf) || (!current->isLeaf);
        if (!current->isLeaf)
        {
            int middle_x = (p.x_max + p.x_min) / 2;
            int middle_y = (p.y_max + p.y_min) / 2;
            current->topLeft     = self(self, { p.x_min,  p.y_min, middle_x, middle_y});
            current->topRight    = self(self, {middle_x,  p.y_min,  p.x_max, middle_y});
            current->bottomLeft  = self(self, { p.x_min, middle_y, middle_x,  p.y_max});
            current->bottomRight = self(self, {middle_x, middle_y,  p.x_max,  p.y_max});
        }
        return current;
    };
    return build(build, {0, 0, n, n});
}

// ############################################################################
// ############################################################################

bool compare(const Node * root, const std::vector<std::vector<int> > &solution)
{
    std::queue<const Node *> q;
    int number_of_nullptr = root == nullptr, index = 0;
    q.push(root);
    while (!q.empty() && (number_of_nullptr < static_cast<int>(q.size())))
    {
        const Node * current = q.front();
        number_of_nullptr -= current == nullptr;
        q.pop();
        if (current)
        {
            if ((solution[index][0] != current->isLeaf)
            ||  (solution[index][1] != current->val)) return false;
            q.push(current->topLeft);
            q.push(current->topRight);
            q.push(current->bottomLeft);
            q.push(current->bottomRight);
            number_of_nullptr += (current->topLeft     == nullptr)
                              +  (current->topRight    == nullptr)
                              +  (current->bottomLeft  == nullptr)
                              +  (current->bottomRight == nullptr);
        }
        else
        {
            if (solution[index][0] != null) return false;
        }
        ++index;
    }
    return true;
}

std::ostream& operator<<(std::ostream &out, const Node * root)
{
    std::queue<const Node *> q;
    int number_of_nullptr = root == nullptr;
    q.push(root);
    out << '{';
    bool next = false;
    while (!q.empty() && (number_of_nullptr < static_cast<int>(q.size())))
    {
        const Node * current = q.front();
        number_of_nullptr -= current == nullptr;
        q.pop();
        if (next) [[likely]] out << ", ";
        if (current)
        {
            out << '{' << current->isLeaf << ", " << current->val << '}';
            q.push(current->topLeft);
            q.push(current->topRight);
            q.push(current->bottomLeft);
            q.push(current->bottomRight);
            number_of_nullptr += (current->topLeft     == nullptr)
                              +  (current->topRight    == nullptr)
                              +  (current->bottomLeft  == nullptr)
                              +  (current->bottomRight == nullptr);
        }
        else out << "null";
        next = true;
    }
    out << '}';
    return out;
}

void test(std::vector<std::vector<int> > grid,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    Node * result = nullptr;
    for (unsigned int i = 0; i < trials; ++i)
    {
        delete result;
        result = construct(grid);
    }
    showResult(compare(result, solution), solution, result);
    delete result;
}

int main(int, char **)
{
    const std::vector<int> NULLPTR = { null, null };
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1},
          {1, 0}},
         {{0, 1}, {1, 0}, {1, 1}, {1, 1}, {1, 0}}, trials);
    test({{1, 1, 1, 1, 0, 0, 0, 0},
          {1, 1, 1, 1, 0, 0, 0, 0},
          {1, 1, 1, 1, 1, 1, 1, 1},
          {1, 1, 1, 1, 1, 1, 1, 1},
          {1, 1, 1, 1, 0, 0, 0, 0},
          {1, 1, 1, 1, 0, 0, 0, 0},
          {1, 1, 1, 1, 0, 0, 0, 0},
          {1, 1, 1, 1, 0, 0, 0, 0}},
         {{0, 1},
          {1, 1}, {0, 1}, {1, 1}, {1, 0},
          NULLPTR, NULLPTR, NULLPTR, NULLPTR,
          {1, 0}, {1, 0}, {1, 1}, {1, 1}}, trials);
    return 0;
}


