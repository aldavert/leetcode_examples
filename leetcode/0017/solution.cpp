#include "../common/common.hpp"
#include <deque>

// ############################################################################
// ############################################################################

std::vector<std::string> letterCombinations(std::string digits)
{
    std::string keyboard[10] =
        { "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };
    std::deque<std::string> result;
    if (digits.size() > 0)
    {
        result.push_back("");
        for (char c : digits)
        {
            int digit = c - '0';
            const size_t n = result.size();
            for (size_t i = 0; i < n; ++i)
            {
                std::string current = result.front();
                result.pop_front();
                for (char l : keyboard[digit])
                    result.push_back(current + l);
            }
        }
    }
    return { result.begin(), result.end() };
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_set<std::string> lut(left.begin(), left.end());
    for (auto r : right)
    {
        if (auto iter = lut.find(r); iter != lut.end())
            lut.erase(iter);
        else return false;
    }
    return lut.empty();
}

void test(std::string digits,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = letterCombinations(digits);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("23", {"ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"}, trials);
    test("", {}, trials);
    test("2", {"a", "b", "c"}, trials);
    return 0;
}


