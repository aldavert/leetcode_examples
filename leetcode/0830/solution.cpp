#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > largeGroupPositions(std::string s)
{
    std::vector<std::vector<int> > result;
    const int n = static_cast<int>(s.size());
    for (int start = 0, end = 0; start < n; start = end)
    {
        while ((end < n) && (s[end] == s[start])) ++end;
        if (end - start >= 3)
            result.push_back({start, end - 1});
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = largeGroupPositions(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abbxxxxzzy", {{3, 6}}, trials);
    test("abc", {}, trials);
    test("abcdddeeeeaabbbcd", {{3, 5}, {6, 9}, {12, 14}}, trials);
    test("abcdddeeeeaabbbcddd", {{3, 5}, {6, 9}, {12, 14}, {16, 18}}, trials);
    return 0;
}


