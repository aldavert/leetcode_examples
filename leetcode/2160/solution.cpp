#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int minimumSum(int num)
{
    int min[2] = {9, 9}, max[2] = {0, 0};
    for (int i = 0, place = 1000; i < 4; ++i, place /= 10)
    {
        int current = num / place;
        num -= current * place;
        if      (current < min[0]) min[1] = std::exchange(min[0], current);
        else if (current < min[1]) min[1] = current;
        if      (current > max[0]) max[1] = std::exchange(max[0], current);
        else if (current > max[1]) max[1] = current;
    }
    return 10 * (min[0] + min[1]) + max[0] + max[1];
}

// ############################################################################
// ############################################################################

void test(int num, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumSum(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2932, 52, trials);
    test(4009, 13, trials);
    return 0;
}


