#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countPairs(std::vector<int> nums, int target)
{
    std::vector<int> sorted_nums(nums);
    std::sort(sorted_nums.begin(), sorted_nums.end());
    int result = 0;
    for (int l = 0, r = static_cast<int>(nums.size()) - 1; l < r;)
    {
        if (sorted_nums[l] + sorted_nums[r] >= target) --r;
        else result += r - l++;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPairs(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, 1, 2, 3, 1}, 2, 3, trials);
    test({-6, 2, 5, -2, -7, -1, 3}, -2, 10, trials);
    return 0;
}


