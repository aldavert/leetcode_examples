#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<std::vector<int> > imageSmoother(std::vector<std::vector<int> > img)
{
    const int n = static_cast<int>(img.size()),
              m = static_cast<int>((n > 0)?img[0].size():0);
    std::vector<std::vector<int> > result(n, std::vector<int>(m));
    auto smooth = [&](int i, int j) -> int
    {
        int counter = 0, sum = 0;
        for (int x = i - 1; x <= i + 1; ++x)
        {
            for (int y = j - 1; y <= j + 1; ++y)
            {
                if ((x >= 0) && (y >= 0) && (x < n) && (y < m))
                {
                    sum += img[x][y];
                    ++counter;
                }
            }
        }
        return sum / counter;
    };
    for (int x = 0; x < n; ++x)
        for (int y = 0; y < m; ++y)
            result[x][y] = smooth(x, y);
    return result;
}
#else
std::vector<std::vector<int> > imageSmoother(std::vector<std::vector<int> > img)
{
    const size_t n = img.size(),
                 m = (n > 0)?img[0].size():0;
    std::vector<std::vector<int> > result(n, std::vector<int>(m));
    if ((n == 1) || (m == 1))
    {
        if (n == m) result[0][0] = img[0][0];
        else if (n == 1)
        {
            result[0][0] = (img[0][0] + img[0][1]) / 2;
            result[0][m - 1] = (img[0][m - 1] + img[0][m - 2]) / 2;
            for (size_t i = 1; i < m - 1; ++i)
                result[0][i] = (img[0][i - 1] + img[0][i] + img[0][i + 1]) / 3;
        }
        else
        {
            result[0][0] = (img[0][0] + img[1][0]) / 2;
            result[n - 1][0] = (img[n - 1][0] + img[n - 2][0]) / 2;
            for (size_t i = 1; i < n - 1; ++i)
                result[i][0] = (img[i - 1][0] + img[i][0] + img[i + 1][0]) / 3;
        }
    }
    else
    {
        result[0    ][0    ] = (img[0    ][0    ] + img[0    ][1    ]
                              + img[1    ][0    ] + img[1    ][1    ]) / 4;
        result[0    ][m - 1] = (img[0    ][m - 1] + img[0    ][m - 2]
                              + img[1    ][m - 1] + img[1    ][m - 2]) / 4;
        result[n - 1][0    ] = (img[n - 1][0    ] + img[n - 1][1    ]
                              + img[n - 2][0    ] + img[n - 2][1    ]) / 4;
        result[n - 1][m - 1] = (img[n - 1][m - 1] + img[n - 1][m - 2]
                              + img[n - 2][m - 1] + img[n - 2][m - 2]) / 4;
        for (size_t i = 1; i < n - 1; ++i)
        {
            result[i][0] = (img[i - 1][0] + img[i][0] + img[i + 1][0]
                         +  img[i - 1][1] + img[i][1] + img[i + 1][1]) / 6;
            result[i][m - 1] = (img[i - 1][m - 1] + img[i][m - 1] + img[i + 1][m - 1]
                           +  img[i - 1][m - 2] + img[i][m - 2] + img[i + 1][m - 2]) / 6;
        }
        for (size_t i = 1; i < m - 1; ++i)
        {
            result[0][i] = (img[0][i - 1] + img[0][i] + img[0][i + 1]
                         +  img[1][i - 1] + img[1][i] + img[1][i + 1]) / 6;
            result[n - 1][i] = (img[n - 1][i - 1] + img[n - 1][i] + img[n - 1][i + 1]
                           +  img[n - 2][i - 1] + img[n - 2][i] + img[n - 2][i + 1]) / 6;
        }
        for (size_t i = 1; i < n - 1; ++i)
            for (size_t j = 1; j < m - 1; ++j)
                result[i][j] = (img[i - 1][j - 1] + img[i - 1][j] + img[i - 1][j + 1]
                             +  img[i + 1][j - 1] + img[i + 1][j] + img[i + 1][j + 1]
                             +  img[i][j - 1] + img[i][j] + img[i][j + 1]) / 9;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > img,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = imageSmoother(img);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 1}, {1, 0, 1}, {1, 1, 1}},
         {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}, trials);
    test({{100, 200, 100}, {200,  50, 200}, {100, 200, 100}},
         {{137, 141, 137}, {141, 138, 141}, {137, 141, 137}}, trials);
    return 0;
}


