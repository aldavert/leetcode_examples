#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

long long minDamage(int power, std::vector<int> damage, std::vector<int> health)
{
    struct Enemy
    {
        Enemy(int d, int t) :
            damage(d), time_taken_down(t), ratio(static_cast<double>(d) / t) {}
        int damage = -1;
        int time_taken_down = -1;
        double ratio = 0.0;
        bool operator<(const Enemy &other) const { return ratio > other.ratio; }
    };
    long sum_damage = std::accumulate(damage.begin(), damage.end(), 0L), result = 0;
    std::vector<Enemy> enemies;
    for (size_t i = 0; i < damage.size(); ++i)
        enemies.emplace_back(damage[i], (health[i] + power - 1) / power);
    std::sort(enemies.begin(), enemies.end());
    for (const auto &enemy : enemies)
    {
        result += sum_damage * enemy.time_taken_down;
        sum_damage -= enemy.damage;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int power,
          std::vector<int> damage,
          std::vector<int> health,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDamage(power, damage, health);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {1, 2, 3, 4}, {4, 5, 6, 8}, 39, trials);
    test(1, {1, 1, 1, 1}, {1, 2, 3, 4}, 20, trials);
    test(8, {40}, {59}, 320, trials);
    return 0;
}


