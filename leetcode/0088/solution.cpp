#include "../common/common.hpp"

// ############################################################################
// ############################################################################

void merge(std::vector<int> &nums1, int m, std::vector<int> nums2, int n)
{
    std::vector<int> result(nums1.size());
    int i, j, k;
    for (i = 0, j = 0, k = 0; (i < m) && (j < n);)
    {
        if (nums1[i] <= nums2[j]) result[k++] = nums1[i++];
        else result[k++] = nums2[j++];
    }
    for (; i < m; ++i, ++k)
        result[k] = nums1[i];
    for (; j < n; ++j, ++k)
        result[k] = nums2[j];
    std::swap(nums1, result);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          int m,
          std::vector<int> nums2,
          int n,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = nums1;
        merge(result, m, nums2, n);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 0, 0, 0}, 3, {2, 5, 6}, 3, {1, 2, 2, 3, 5, 6}, trials);
    test({1}, 1, {}, 0, {1}, trials);
    test({0}, 0, {1}, 1, {1}, trials);
    return 0;
}


