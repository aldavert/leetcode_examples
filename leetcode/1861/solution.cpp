#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<char> > rotateTheBox(std::vector<std::vector<char> > box)
{
    const int m = static_cast<int>(box.size()),
              n = static_cast<int>(box[0].size());
    std::vector<std::vector<char> > result(n, std::vector<char>(m, '.'));
    
    for (int i = 0; i < m; ++i)
    {
        for (int j = n - 1, k = n - 1; j >= 0; --j)
        {
            if (box[i][j] != '.')
            {
                if (box[i][j] == '*') k = j;
                result[k--][m - i - 1] = box[i][j];
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > box,
          std::vector<std::vector<char> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<char> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = rotateTheBox(box);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'#', '.', '#'}}, {{'.'},
                             {'#'},
                             {'#'}}, trials);
    test({{'#', '.', '*', '.'},
          {'#', '#', '*', '.'}},
          {{'#', '.'},
     {'#', '#'},
     {'*', '*'},
     {'.', '.'}}, trials);
    test({{'#', '#', '*', '.', '*', '.'},
          {'#', '#', '#', '*', '.', '.'},
          {'#', '#', '#', '.', '#', '.'}},
          {{'.', '#', '#'},
           {'.', '#', '#'},
           {'#', '#', '*'},
           {'#', '*', '.'},
           {'#', '.', '*'},
           {'#', '.', '.'}}, trials);
    return 0;
}


