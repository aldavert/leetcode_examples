#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

bool checkPerfectNumber(int num)
{
    int result = 0;
    for (int i = 1, e = static_cast<int>(std::sqrt(num)); i <= e; ++i)
    {
        if (num % i == 0)
        {
            int j = num / i;
            result += (i != num) * i + ((i != j) && (j != num)) * j;
        }
    }
    return result == num;
}

// ############################################################################
// ############################################################################

void test(int n, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkPerfectNumber(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(28, true, trials);
    test(7, false, trials);
    test(1, false, trials);
    return 0;
}


