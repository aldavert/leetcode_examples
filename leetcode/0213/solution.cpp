#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int rob(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    if (nums.empty()) return 0;
    if (nums.size() == 1) return nums[0];
    
    auto dp = [&](int l, int r)
    {
        int prev1 = 0, prev2 = 0;
        for (int i = l; i <= r; ++i)
            prev2 = std::exchange(prev1, std::max(prev1, prev2 + nums[i]));
        return prev1;
    };
    return std::max(dp(0, n - 2), dp(1, n - 1));
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = rob(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 2}, 3, trials);
    test({1, 2, 3, 1}, 4, trials);
    test({1, 2, 3}, 3, trials);
    return 0;
}


