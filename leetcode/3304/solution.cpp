#include "../common/common.hpp"

// ############################################################################
// ############################################################################

char kthCharacter(int k)
{
    char buffer[1024] = {};
    buffer[0] = 'a';
    for (int size = 1; size < k;)
        for (int i = 0, n = size; i < n; ++i)
            buffer[size++] = static_cast<char>((buffer[i] - 'a' + 1) % 26 + 'a');
    return buffer[k - 1];
}

// ############################################################################
// ############################################################################

void test(int k, char solution, unsigned int trials = 1)
{
    char result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = kthCharacter(k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 'b', trials);
    test(10, 'c', trials);
    return 0;
}


