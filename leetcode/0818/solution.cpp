#include "../common/common.hpp"
#include <cstring>
#include <queue>
#include <cmath>

// ############################################################################
// ############################################################################

#if 0
int racecar(int target)
{
    int dp[10'001] = {};
    auto process = [&](auto &&self, int t)
    {
        if (dp[t] > 0) return dp[t];
        int n = static_cast<int>(floor(std::log2(t))) + 1;
        if ((1 << n) == t + 1) dp[t] = n; 
        else
        { 
            dp[t] = self(self, (1 << n) - t - 1) + n + 1; 
            for(int m = 0; m < n - 1; ++m)
                dp[t] = std::min(dp[t],
                                 self(self, t - (1 << (n - 1)) + (1 << m))
                               + n + m + 1);
        }
        return dp[t];
    };
    return process(process, target);
}
#else
int racecar(int target)
{
    struct Position
    {
        int position = 0;
        int speed = 1;
        unsigned long id(void) const
        {
            unsigned int p, s;
            std::memcpy(&s, &speed   , sizeof(s));
            std::memcpy(&p, &position, sizeof(p));
            return static_cast<unsigned long>(s) << 32
                 | static_cast<unsigned long>(p);
        }
        Position accelerate(void) const { return { position + speed, speed * 2 }; }
        Position reverse(void) const { return { position, 1 - 2 * (speed > 0) }; }
    };
    std::queue<Position> q;
    Position start;
    int skip = static_cast<int>(log2(target));
    for (int i = 0; i < skip; ++i)
        start.position += start.speed,
        start.speed *= 2;
    q.push(start);
    std::unordered_set<unsigned long> visited;
    for (int moves = skip; !q.empty(); ++moves)
    {
        for (size_t i = 0, n = q.size(); i < n; ++i)
        {
            auto current = q.front();
            q.pop();
            if ((std::abs(current.position) > 50'000)
            ||  (std::abs(current.speed) > 8192))
                continue;
            if (auto id = current.id(); visited.find(id) != visited.end())
                continue;
            else visited.insert(id);
            if (current.position == target) return moves;
            q.push(current.accelerate());
            q.push(current.reverse());
        }
    }
    return -1;
}
#endif

// ############################################################################
// ############################################################################

void test(int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = racecar(target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 4, trials);
    test(3, 2, trials);
    test(6, 5, trials);
    test(5'132, 31, trials);
    test(9'132, 43, trials);
    return 0;
}


