#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool circularArrayLoop(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<bool> available(nums.size(), true);
    auto isCycle = [&](int i) -> bool
    {
        bool positive = nums[i] >= 0;
        std::vector<int> in_cycle(nums.size(), -1);
        for (int count = 0; true; ++count)
        {
            if (in_cycle[i] >= 0) return (count - in_cycle[i]) > 1;
            if (positive != (nums[i] >= 0)) return false;
            available[i] = false;
            in_cycle[i] = count;
            i += nums[i];
            while (i >= n) i -= n;
            while (i <  0) i += n;
        }
        return false;
    };
    for (int i = 0; i < n; ++i)
    {
        if (!available[i]) continue;
        if (isCycle(i)) return true;
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = circularArrayLoop(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, -1, 1, 2, 2}, true, trials);
    test({-1, -2, -3, -4, -5, 6}, false, trials);
    test({1, -1, 5, 1, 4}, true, trials);
    test({-1, -2, -3, -4, -5}, false, trials);
    return 0;
}


