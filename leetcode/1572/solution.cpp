#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int diagonalSum(std::vector<std::vector<int> > mat)
{
    const size_t n = mat.size();
    int result = 0;
    for (size_t i = 0; i < n; ++i)
        result += mat[i][i] + mat[n - i - 1][i];
    if (n & 1)
        result -= mat[n / 2][n / 2];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > mat, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = diagonalSum(mat);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, 25, trials);
    test({{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}}, 8, trials);
    test({{5}}, 5, trials);
    test({{7, 3, 1, 9}, {3, 4, 6, 9}, {6, 9, 6, 6}, {9, 5, 8, 5}}, 55, trials);
    return 0;
}


