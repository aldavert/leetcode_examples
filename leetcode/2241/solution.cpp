#include "../common/common.hpp"

// ############################################################################
// ############################################################################

class ATM
{
    static constexpr int banknotes[5] = {20, 50, 100, 200, 500};
    long bank[5] = {};
public:
    ATM(void) = default;
    void deposit(std::vector<int> banknotesCount)
    {
        for (int i = 0; i < 5; ++i) bank[i] += banknotesCount[i];
    }
    std::vector<int> withdraw(int amount)
    {
        std::vector<int> withdrew(5);
        
        for (int i = 4; i >= 0; --i)
        {
            withdrew[i] = static_cast<int>(std::min(bank[i],
                                    static_cast<long>(amount) / banknotes[i]));
            amount -= withdrew[i] * banknotes[i];
        }
        if (amount > 0) return {-1};
        for (int i = 0; i < 5; ++i)
            bank[i] -= withdrew[i];
        return withdrew;
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > input,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        ATM obj;
        result.clear();
        for (const auto &in : input)
        {
            if (in.size() == 1)
                result.push_back(obj.withdraw(in[0]));
            else
            {
                obj.deposit(in);
                result.push_back({});
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0, 1, 2, 1}, {600}, {0, 1, 0, 1, 1}, {600}, {550}},
         {{}, {0, 0, 1, 0, 1}, {}, {-1}, {0, 1, 0, 0, 1}}, trials);
    return 0;
}


