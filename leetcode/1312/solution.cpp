#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minInsertions(std::string s)
{
    const int n = static_cast<int>(s.size());
    int dp[500][500] = {};
    for (int i = 0; i < n; ++i) dp[i][i] = 1;
    for (int d = 1; d < n; ++d)
        for (int i = 0, j = d; j < n; ++i, ++j)
            dp[i][j] = (s[i] == s[j])?(2 + dp[i + 1][j - 1])
                                     :std::max(dp[i + 1][j], dp[i][j - 1]);
    return n - dp[0][n - 1];
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minInsertions(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("zzazz", 0, trials);
    test("mbadm", 2, trials);
    test("leetcode", 5, trials);
    return 0;
}


