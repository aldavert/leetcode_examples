#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool detectCapitalUse(std::string word)
{
    const int n = static_cast<int>(word.size());
    if (n == 1) return true;
    if ((word[0] < 'a') && (word[1] < 'a'))
    {
        if ((word[0] >= 'a') || (word[1] >= 'a'))
            return false;
        for (int i = 2; i < n; ++i)
            if (word[i] >= 'a')
                return false;
    }
    else
    {
        if (word[1] < 'a') return false;
        for (int i = 2; i < n; ++i)
            if (word[i] < 'a')
                return false;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string word, bool solution, unsigned int trials = 1)
{
    bool result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = detectCapitalUse(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("USA", true, trials);
    test("FlaG", false, trials);
    test("mL", false, trials);
    return 0;
}


