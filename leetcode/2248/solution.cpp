#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> intersection(std::vector<std::vector<int>> nums)
{
    const int n = static_cast<int>(nums.size());
    int histogram[1001] = {};
    for (const auto &vec : nums)
        for (int number : vec)
            ++histogram[number];
    std::vector<int> result;
    for (int i = 0; i <= 1000; ++i)
        if (histogram[i] == n)
            result.push_back(i);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > nums,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = intersection(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 1, 2, 4, 5}, {1, 2, 3, 4}, {3, 4, 5, 6}}, {3, 4}, trials);
    test({{1, 2, 3}, {4, 5, 6}}, {}, trials);
    test({{ 437,  986,  622,  435, 1000},
          {1000,  622,  437,  435,  986},
          { 417,  986,  622,  435, 1000, 437},
          { 986,  437, 1000,  435,  622},
          { 622,  437,  986,  435, 1000}},
          { 435,  437,  622,  986, 1000}, trials);
    return 0;
}


