#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::string shortestPalindrome(std::string s)
{
    const int n = static_cast<int>(s.size());
    std::string work_s = s + "#" + std::string(s.rbegin(), s.rend());
    std::vector<int> offset(work_s.size());
    for (int i = 1, j = 0, nw = static_cast<int>(work_s.size()); i < nw; )
    {
        if (work_s[i] == work_s[j])
        {
            offset[i] = j + 1;
            ++j;
            ++i;
        }
        else
        {
            if (j > 0) j = offset[j - 1];
            else ++i;
        }
    }
    return std::string(s.rbegin(), std::next(s.rbegin(), n - offset.back())) + s;
}
#elif 0
std::string shortestPalindrome(std::string s)
{
    const int n = static_cast<int>(s.size());
    if (n == 0) return "";
    int max_right = 0;
    for (int index = 0, max_left = 0, right_boundry = (n + 1) / 2; index < right_boundry; )
    {
        int left = index;
        int right = index;
        while ((right < n - 1) && (s[left] == s[right + 1]))
            ++right;
        index = right + 1;
        while ((left > 0) && (right < n - 1) && (s[left - 1] == s[right + 1]))
        {
            --left;
            ++right;
        }
        if ((left == 0) && (right - left > max_right - max_left))
        {
            max_left = left;
            max_right = right;
        }
    }
    return std::string(s.rbegin(), std::next(s.rbegin(), n - max_right - 1)) + s;
}
#else
std::string shortestPalindrome(std::string s)
{
    auto isPalindrome = [](std::string word, int begin, int end) -> bool
    {
        for (int i = begin, j = end; i < j; ++i, --j)
            if (word[i] != word[j]) return false;
        return true;
    };
    const int n = static_cast<int>(s.size());
    for (int i = n - 1; i >= 0; --i)
        if (isPalindrome(s, 0, i))
            return std::string(s.rbegin(), std::next(s.rbegin(), n - 1 - i)) + s;
    return std::string(s.rbegin(), s.rend()) + s;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestPalindrome(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aacecaaa", "aaacecaaa", trials);
    test("abcd", "dcbabcd", trials);
    return 0;
}


