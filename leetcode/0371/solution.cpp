#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

int getSum(int a, int b)
{
    std::bitset<8> logic_table[2] = { 0b10010110, 0b11101000 };
    unsigned int bit_a = std::bit_cast<unsigned int>(a),
                 bit_b = std::bit_cast<unsigned int>(b);
    bool carry = false;
    unsigned int result = 0;
    for (int i = 0, mask = 1; i < 32; ++i, mask <<= 1)
    {
        int index = ((bit_a & mask) > 0) << 2
                  | ((bit_b & mask) > 0) << 1
                  | carry;
        result = result | (logic_table[0][index] << i);
        carry = logic_table[1][index];
    }
    return std::bit_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(int a, int b, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getSum(a, b);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test( 1,  2,  3, trials);
    test( 2,  3,  5, trials);
    test(-2,  3,  1, trials);
    test(-2, -3, -5, trials);
    return 0;
}


