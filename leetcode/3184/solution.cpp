#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countCompleteDayPairs(std::vector<int> hours)
{
    int result = 0;
    for (int count[24] = {}; int hour : hours)
    {
        result += count[(24 - hour % 24) % 24];
        ++count[hour % 24];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> hours, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countCompleteDayPairs(hours);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({12, 12, 30, 24, 24}, 2, trials);
    test({72, 48, 24, 3}, 3, trials);
    return 0;
}


