#include "../common/common.hpp"
#include <unordered_map>
#include "test_a.hpp"

// ############################################################################
// ############################################################################

#if 0
std::vector<int>
findNumOfValidWords(std::vector<std::string> words, std::vector<std::string> puzzles)
{
    auto mask = [](const std::string &s) -> int
    {
        int code = 0;
        for (char c : s)
            code |= 1 << static_cast<int>(c - 'a');
        return code;
    };
    const int n = static_cast<int>(puzzles.size());
    const int nw = static_cast<int>(words.size());
    std::vector<int> words_codes(nw), result(n);
    for (int i = 0; i < nw; ++i)
        words_codes[i] = mask(words[i]);
    for (int i = 0; i < n; ++i)
    {
        int first_word = 1 << static_cast<int>(puzzles[i][0] - 'a');
        int puzzle_code = mask(puzzles[i]);
        for (int j = 0; j < nw; ++j)
            result[i] += ((words_codes[j] & first_word)
                      && ((puzzle_code & words_codes[j]) == words_codes[j]));
    }
    return result;
}
#else
std::vector<int>
findNumOfValidWords(std::vector<std::string> words, std::vector<std::string> puzzles)
{
    auto mask = [](const std::string &s) -> int
    {
        int code = 0;
        for (char c : s)
            code |= 1 << static_cast<int>(c - 'a');
        return code;
    };
    const int n = static_cast<int>(puzzles.size());
    std::vector<int> result(n, 0);
    std::unordered_map<int, int> m;
    for (const auto &w : words)
        ++m[mask(w)];
    for (int i = 0; i < n; ++i)
    {
        int base_mask = mask(puzzles[i]);
        int first = 1 << (puzzles[i][0] - 'a');
        int current = base_mask;
        int freq = 0;
        while (current > 0)
        {
            if (current & first)
                freq += m[current];
            current = (current - 1) & base_mask;
        }
        result[i] = freq;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::vector<std::string> puzzles,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findNumOfValidWords(words, puzzles);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
    constexpr unsigned int trials = 100;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"aaaa", "asas", "able", "ability", "actt", "actor", "access"},
         {"aboveyz", "abrodyz", "abslute", "absoryz", "actresz", "gaswxyz"},
         {1, 1, 3, 2, 4, 0}, trials);
    test({"apple", "pleas", "please"},
         {"aelwxyz", "aelpxyz", "aelpsxy", "saelpxy", "xaelpsy"},
         {0, 1, 3, 2, 0}, trials);
    testA test_a;
    test(test_a.words, test_a.puzzles, test_a.solution, trials); 
    return 0;
}


