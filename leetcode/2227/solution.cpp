#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

class Encrypter
{
private:
    std::string m_values[26];
    std::unordered_map<std::string, int> m_counts;
public:
    Encrypter(std::vector<char> &keys,
              std::vector<std::string> &values,
              std::vector<std::string> &dictionary)
    {
        for (int i = 0, n = static_cast<int>(keys.size()); i < n; ++i)
            m_values[keys[i] - 'a'] = values[i];
        for (const auto &d : dictionary)
        {
            std::string enc;
            for (char c : d)
            {
                if (m_values[c - 'a'].size() == 0)
                {
                    enc = "";
                    break;
                }
                enc += m_values[c - 'a'];
            }
            if (enc != "")
                ++m_counts[enc];
        }
    }
    std::string encrypt(std::string word1)
    {
        std::string result;
        for (char c : word1)
        {
            if (m_values[c - 'a'].size() == 0)
                return "";
            result += m_values[c - 'a'];
        }
        return result;
    }
    int decrypt(std::string word2)
    {
        if (m_counts.find(word2) == m_counts.end())
            return 0;
        return m_counts[word2];
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<char> keys,
          std::vector<std::string> values,
          std::vector<std::string> dictionary,
          std::vector<bool> is_encrypt,
          std::vector<std::string> input,
          std::vector<std::variant<std::string, int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::variant<std::string, int> > result;
    const int n = static_cast<int>(is_encrypt.size());
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        Encrypter object(keys, values, dictionary);
        for (int i = 0; i < n; ++i)
        {
            if (is_encrypt[i]) result.push_back(object.encrypt(input[i]));
            else result.push_back(object.decrypt(input[i]));
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({'a', 'b', 'c', 'd'},
         {"ei", "zf", "ei", "am"},
         {"abcd", "acbd", "adbc", "badc", "dacb", "cadb", "cbda", "abad"},
         {true, false}, {"abcd", "eizfeiam"}, {"eizfeiam", 2}, trials);
    test({'b'},
         {"ca"},
         {"aaa", "cacbc", "bbaba", "bb"},
         {true, false}, {"bbb", "cacaca"}, {"cacaca", 0}, trials);
    return 0;
}


