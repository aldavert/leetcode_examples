#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minOperations(std::vector<std::vector<int> > grid, int x)
{
    std::vector<int> values;
    for (const auto &row : grid)
        values.insert(values.end(), row.begin(), row.end());
    if (std::any_of(values.begin(), values.end(),
                    [&](int v) { return (v - values[0]) % x; }))
        return -1;
    int result = 0;
    std::nth_element(values.begin(), values.begin() + values.size() / 2, values.end());
    for (const int v : values)
        result += std::abs(v - values[values.size() / 2]) / x;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          int x,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(grid, x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 4}, {6, 8}}, 2, 4, trials);
    test({{1, 5}, {2, 3}}, 1, 5, trials);
    test({{1, 2}, {3, 4}}, 2, -1, trials);
    return 0;
}


