#include "../common/common.hpp"
#include <unordered_map>
#include <random>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class RandomizedCollection
{
    std::unordered_map<int, std::unordered_set<int> > m_lut;
    std::vector<int> m_data;
    std::random_device rd;
    std::mt19937 gen;
public:
    RandomizedCollection() :
        gen(rd())
    {
    }
    bool insert(int val)
    {
        const int position = static_cast<int>(m_data.size());
        m_data.push_back(val);
        if (auto it = m_lut.find(val); it != m_lut.end())
        {
            it->second.insert(position);
            return false;
        }
        m_lut[val].insert(position);
        return true;
    }
    bool remove(int val)
    {
        if (auto it = m_lut.find(val); it != m_lut.end())
        {
            int position = *it->second.begin();
            m_data[position] = m_data.back();
            it->second.erase(it->second.begin());
            if (it->second.empty())
                m_lut.erase(it);
            if (position != static_cast<int>(m_data.size()) - 1)
            {
                m_lut[m_data.back()].erase(static_cast<int>(m_data.size()) - 1);
                m_lut[m_data.back()].insert(position);
            }
            m_data.pop_back();
            return true;
        }
        return false;
    }
    int getRandom()
    {
        const int n = static_cast<int>(m_data.size());
        std::uniform_int_distribution<> distribution(0, n - 1);
        return m_data[distribution(gen)];
    }
};

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::variant<bool, int> > &left,
                const std::vector<std::variant<bool, int> > &right)
{
    if (left.size() == 0) return right.size() == 0;
    if (std::holds_alternative<bool>(left[0]))
    {
        if ((left.size() != 1) || (right.size() != 1))
            return false;
        return left[0] == right[0];
    }
    else
    {
        if (left.size() == 1)
        {
            bool found = false;
            int element = std::get<int>(left[0]);
            for (const auto &v : right)
            {
                if (element == std::get<int>(v))
                {
                    found = true;
                    break;
                }
            }
            return found;
        }
        else if (right.size() == 1)
        {
            bool found = false;
            int element = std::get<int>(right[0]);
            for (const auto &v : left)
            {
                if (element == std::get<int>(v))
                {
                    found = true;
                    break;
                }
            }
            return found;
        }
        else return false;
    }
}

bool operator==(const std::vector<std::vector<std::variant<bool, int> > > &left,
                const std::vector<std::vector<std::variant<bool, int> > > &right)
{
    if (left.size() != right.size()) return false;
    const int n = static_cast<int>(left.size());
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

enum class OP { RANDOMIZEDCOLLECTION, INSERT, REMOVE, GETRANDOM };

void test(std::vector<OP> operation,
          std::vector<int> values,
          std::vector<std::vector<std::variant<bool, int> > > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<std::variant<bool, int> > > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        RandomizedCollection * rs = nullptr;
        std::vector<std::vector<std::variant<bool, int> > > current_result;
        const int n_op = static_cast<int>(operation.size());
        for (int j = 0; j < n_op; ++j)
        {
            switch (operation[j])
            {
            case OP::RANDOMIZEDCOLLECTION:
                current_result.push_back({});
                delete rs;
                rs = new RandomizedCollection();
                break;
            case OP::INSERT:
                current_result.push_back({rs->insert(values[j])});
                break;
            case OP::REMOVE:
                current_result.push_back({rs->remove(values[j])});
                break;
            case OP::GETRANDOM:
                current_result.push_back({rs->getRandom()});
                break;
            default:
                std::cerr << "[ERROR] Unknown operation.\n";
                return;
            }
        }
        delete rs;
        result = current_result;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::RANDOMIZEDCOLLECTION, OP::INSERT, OP::INSERT, OP::INSERT,
          OP::GETRANDOM, OP::REMOVE, OP::GETRANDOM},
         {null, 1, 1, 2, null, 1, null},
         {{}, {true}, {false}, {true}, {1, 2}, {true}, {1, 2}}, trials);
    test({OP::RANDOMIZEDCOLLECTION, OP::INSERT, OP::REMOVE, OP::INSERT},
         {null, 1, 1, 1}, {{}, {true}, {true}, {true}}, trials);
    return 0;
}


