#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sumCounts(std::vector<int> nums)
{
    int result = 0;
    for (size_t i = 0; i < nums.size(); ++i)
    {
        int histogram[101] = {}, count = 0;
        for (size_t j = i; j < nums.size(); ++j)
        {
            count += (++histogram[nums[j]] == 1);
            result += count * count;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumCounts(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1}, 15, trials);
    test({1, 1}, 3, trials);
    return 0;
}


