#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findPeakGrid(std::vector<std::vector<int> > mat)
{
    int l = 0, r = static_cast<int>(mat.size()) - 1;
    while (l < r)
    {
        int m = (l + r) / 2;
        if (*std::max_element(mat[m].begin(), mat[m].end()) >=
            *std::max_element(mat[m + 1].begin(), mat[m + 1].end())) r = m;
        else l = m + 1;
    }
    int max_index = 0, max_value = mat[l][0];
    for (int i = 1, n = static_cast<int>(mat[l].size()); i < n; ++i)
        if (mat[l][i] > max_value)
            max_value = mat[l][i],
            max_index = i;
    return {l, max_index};
}

// ############################################################################
// ############################################################################

bool operator==(const std::set<std::vector<int> > &left,
                const std::vector<int> &right)
{
    return left.find(right) != left.end();
}

void test(std::vector<std::vector<int> > mat,
          std::set<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findPeakGrid(mat);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 4}, {3, 2}}, {{0, 1}, {1, 0}}, trials);
    test({{10, 20, 15}, {21, 30, 14}, {7, 16, 32}}, {{1, 1}, {2, 2}}, trials);
    return 0;
}


