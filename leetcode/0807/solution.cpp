#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxIncreaseKeepingSkyline(std::vector<std::vector<int> > grid)
{
    int horizontal[51] = {}, vertical[51] = {};
    const int n = static_cast<int>(grid.size());
    int result = 0;
    
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            horizontal[i] = std::max(horizontal[i], grid[i][j]),
            vertical[j] = std::max(vertical[j], grid[i][j]);
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            result += std::min(horizontal[i], vertical[j]) - grid[i][j];
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxIncreaseKeepingSkyline(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 0, 8, 4}, {2, 4, 5, 7}, {9, 2, 6, 3}, {0, 3, 1, 0}}, 35, trials);
    test({{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}, 0, trials);
    return 0;
}


