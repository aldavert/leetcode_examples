#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> diffWaysToCompute(std::string expression)
{
    std::unordered_map<std::string, std::vector<int> > dp_table;
    auto dp = [&](auto &&self, std::string s) -> std::vector<int>
    {
        if (auto search = dp_table.find(s); search != dp_table.end())
            return search->second;
        std::vector<int> result;
        for (size_t i = 0; i < s.size(); ++i)
        {
            if (ispunct(s[i]))
            {
                for (const int a : self(self, s.substr(0, i)))
                {
                    for (const int b : self(self, s.substr(i + 1)))
                    {
                        if      (s[i] == '+') result.push_back(a + b);
                        else if (s[i] == '-') result.push_back(a - b);
                        else                           result.push_back(a * b);
                    }
                }
            }
        }
        return dp_table[s] = (result.empty()?std::vector<int>{std::stoi(s)}:result);
    };
    return dp(dp, expression);
}

// ############################################################################
// ############################################################################

bool operator==(std::vector<int> left, std::vector<int> right)
{
    if (left.size() != right.size()) return false;
    std::sort(left.begin(), left.end());
    std::sort(right.begin(), right.end());
    for (size_t i = 0; i < left.size(); ++i)
        if (left[i] != right[i]) return false;
    return true;
}

void test(std::string expression, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = diffWaysToCompute(expression);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("2-1-1", {0, 2}, trials);
    test("2*3-4*5", {-34, -14, -10, -10, 10}, trials);
    return 0;
}


