#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxOperations(std::vector<int> nums)
{
    if (nums.size() < 2) return 0;
    int result = 1, target = nums[0] + nums[1];
    for (size_t i = 2;
         (i + 2 <= nums.size()) && (nums[i] + nums[i + 1] == target);
         i += 2, ++result);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 1, 4, 5}, 2, trials);
    test({3, 2, 6, 1, 4}, 1, trials);
    test({3, 2, 1, 4, 0, 5}, 3, trials);
    return 0;
}


