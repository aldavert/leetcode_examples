# Binary Tree Tilt

Given the `root` of a binary tree, return *the sum of every tree node's* ***tilt***.

The **tilt** of a tree node is the **absolute difference** between the sum of all left subtree node **values** and all right subtree node **values**. If a node does not have a left child, then the sum of the left subtree node **values** is treated as `0`. The rule is similar if there the node does not have a right child.

#### Example 1:
> ```mermaid
> flowchart LR;
> subgraph SA [ ]
> direction TB;
> A1((1))---B1((2))
> A1---C1((3))
> end
> subgraph SB [ ]
> direction TB;
> A2((1))---B2((0))
> A2---C2((0))
> end
> SA-->SB
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef white fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef selected fill:#FDA,stroke:#000,stroke-width:2px;
> class A2,B2,C2 selected;
> class SA,SB white;
> ```
> *Input:* `root = [1, 2, 3]`  
> *Output:* `1`  
> *Explanation:*
> - Tilt of node 2: `|0 - 0| = 0` (no children)
> - Tilt of node 3: `|0 - 0| = 0` (no children)
> - Tilt of node 1: `|2 - 3| = 1` (left subtree is just left child, so sum is `2`; right subtree is just right child, so sum is `3`)
>
> Sum of every tilt : `0 + 0 + 1 = 1`.

#### Example 2
> ```mermaid
> flowchart LR;
> subgraph SA [ ]
> direction TB;
> A1((4))---B1((2))
> A1---C1((9))
> B1---D1((3))
> B1---E1((5))
> C1---EMPTY1(( ))
> C1---F1((7))
> end
> subgraph SB [ ]
> direction TB;
> A2((6))---B2((2))
> A2---C2((7))
> B2---D2((0))
> B2---E2((0))
> C2---EMPTY2(( ))
> C2---F2((0))
> end
> SA-->SB
> linkStyle 4,10 stroke-width:0px;
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef white fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef selected fill:#FDA,stroke:#000,stroke-width:2px;
> class A2,B2,C2,D2,E2,F2 selected;
> class EMPTY1,EMPTY2,SA,SB white;
> ```
> *Input:* `root = [4, 2, 9, 3, 5, null, 7]`  
> *Output:* `15`  
> *Explanation:*
> - Tilt of node 3: `|0 - 0| = 0` (no children)
> - Tilt of node 5: `|0 - 0| = 0` (no children)
> - Tilt of node 7: `|0 - 0| = 0` (no children)
> - Tilt of node 2: `|3 - 5| = 2` (left subtree is just left child, so sum is `3`; right subtree is just right child, so sum is `5`)
> - Tilt of node 9: `|0 - 7| = 7` (no left child, so sum is `0`; right subtree is just right child, so sum is `7`)
> - Tilt of node 4: `|(3 + 5 + 2) - (9 + 7)| = |10 - 16| = 6` (left subtree values are `3`, `5`, and `2`, which sums to `10`; right subtree values are `9` and `7`, which sums to `16`)
> 
> Sum of every tilt: `0 + 0 + 0 + 2 + 7 + 6 = 15`

#### Example 3
> ```mermaid
> flowchart LR;
> subgraph SA [ ]
> A1((21))---B1((7))
> B1---C1((1))
> B1---D1((1))
> C1---E1((3))
> C1---F1((3))
> A1---G1((14))
> G1---H1((2))
> G1---I1((2))
> H1---EMPTY1(( ))
> end
> subgraph SB [ ]
> A2((3))---B2((6))
> B2---C2((0))
> B2---D2((0))
> C2---E2((0))
> C2---F2((0))
> A2---G2((0))
> G2---H2((0))
> G2---I2((0))
> H2---EMPTY2(( ))
> end
> SA-->SB
> linkStyle 8,17 stroke-width:0px;
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef white fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> classDef selected fill:#FDA,stroke:#000,stroke-width:2px;
> class A2,B2,C2,D2,E2,F2,G2,H2,I2 selected;
> class EMPTY1,EMPTY2,SA,SB white;
> ```
> *Input:* `root = [21, 7, 14, 1, 1, 2, 2, 3, 3]`  
> *Output:* `9`

#### Constraints:
- The number of nodes in the tree is in the range `[0, 10^4]`.
- `-1000 <= Node.val <= 1000`


