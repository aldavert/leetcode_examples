#include "../common/common.hpp"
#include "../common/tree.hpp"


// ############################################################################
// ############################################################################

#if 1
int findTilt(TreeNode * root)
{
    int result = 0;
    auto tilt = [&](auto &&self, TreeNode * node) -> int
    {
        if (node == nullptr) return 0;
        auto sum_left = self(self, node->left);
        auto sum_right = self(self, node->right);
        result += std::abs(sum_left - sum_right);
        return sum_left + sum_right + node->val;
    };
    tilt(tilt, root);
    return result;
}
#else
int findTilt(TreeNode * root)
{
    auto tilt = [&](auto &&self, TreeNode * node) -> std::pair<int, int>
    {
        if (node == nullptr) return {0, 0};
        auto [sum_left, tilt_left] = self(self, node->left);
        auto [sum_right, tilt_right] = self(self, node->right);
        return {sum_left + sum_right + node->val,
                std::abs(sum_left - sum_right) + tilt_left + tilt_right};
    };
    return tilt(tilt, root).second;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = findTilt(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 1, trials);
    test({4, 2, 9, 3, 5, null, 7}, 15, trials);
    test({21, 7, 14, 1, 1, 2, 2, 3, 3}, 9, trials);
    return 0;
}


