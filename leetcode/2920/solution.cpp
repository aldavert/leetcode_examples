#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int maximumPoints(std::vector<std::vector<int> > edges, std::vector<int> coins, int k)
{
    constexpr int MAXHALVED = 13;
    const int n = static_cast<int>(coins.size());
    std::vector<std::vector<int> > graph(n);
    std::vector<std::vector<int> > memory(n, std::vector<int>(MAXHALVED + 1, -1));
    auto dfs = [&](auto &&self, int u, int prev, int halved) -> int
    {
        if (halved > MAXHALVED) return 0;
        if (memory[u][halved] != -1) return memory[u][halved];
        
        int val = coins[u] / (1 << halved), take_all = val - k,
        take_half = static_cast<int>(std::floor(val / 2.0));
        for (const int v : graph[u])
        {
            if (v == prev) continue;
            take_all  += self(self, v, u, halved);
            take_half += self(self, v, u, halved + 1);
        }
        return memory[u][halved] = std::max(take_all, take_half);
    };
    for (const auto &edge : edges)
    {
        graph[edge[0]].push_back(edge[1]);
        graph[edge[1]].push_back(edge[0]);
    }
    return dfs(dfs, 0, -1, 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges,
          std::vector<int> coins,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumPoints(edges, coins, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {1, 2}, {2, 3}}, {10, 10, 3, 3}, 5, 11, trials);
    test({{0, 1}, {0, 2}}, {8, 4, 4}, 0, 16, trials);
    return 0;
}


