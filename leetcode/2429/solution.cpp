#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimizeXor(int num1, int num2)
{
    constexpr int MAXBIT = 30;
    int bits = std::popcount(static_cast<size_t>(num2));
    if (std::popcount(static_cast<size_t>(num1)) == bits)
        return num1;
    int result = 0;
    for (int i = MAXBIT; i >= 0; --i)
    {
        if (num1 >> i & 1)
        {
            result |= 1 << i;
            if (--bits == 0) return result;
        }
    }
    for (int i = 0; i < MAXBIT; ++i)
    {
        if ((num1 >> i & 1) == 0)
        {
            result |= 1 << i;
            if (--bits == 0) return result;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int num1, int num2, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimizeXor(num1, num2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 5, 3, trials);
    test(1, 12, 3, trials);
    return 0;
}


