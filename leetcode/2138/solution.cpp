#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> divideString(std::string s, int k, char fill)
{
    std::vector<std::string> result;
    int i = 0, n = static_cast<int>(s.size());
    for (; i + k < n; i += k)
        result.push_back(s.substr(i, k));
    result.push_back(s.substr(i, n - i));
    while (static_cast<int>(result.back().size()) < k)
        result.back() += fill;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          int k,
          char fill,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = divideString(s, k, fill);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcdefghi" , 3, 'x', {"abc", "def", "ghi"}, trials);
    test("abcdefghij", 3, 'x', {"abc", "def", "ghi", "jxx"}, trials);
    return 0;
}


