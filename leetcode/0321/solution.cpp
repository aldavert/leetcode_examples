#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> maxNumber(std::vector<int> nums1, std::vector<int> nums2, int k)
{
    auto maxArray = [&](const std::vector<int> &nums, int ne) -> std::vector<int>
    {
        const int n = static_cast<int>(nums.size());
        std::vector<int> res(ne, 0);
        for (int i = 0, j = 0; i < n; ++i)
        {
            while ((n - i + j > ne) && (j > 0) && (res[j - 1] < nums[i]))
                --j;
            if (j < ne) res[j++] = nums[i];
        }
        return res;
    };
    auto Greater = [](const std::vector<int> &array1, int i,
                      const std::vector<int> &array2, int j) -> bool
    {
        const int n1 = static_cast<int>(array1.size());
        const int n2 = static_cast<int>(array2.size());
        for (; (i < n1) && (j < n2) && (array1[i] == array2[j]); ++i, ++j);
        return (j == n2) || ((i < n1) && (array1[i] > array2[j]));
    };
    auto merge = [&](const std::vector<int> &array1,
                     const std::vector<int> &array2) -> std::vector<int>
    {
        std::vector<int> res(k, 0);
        for (int i = 0, j = 0, r = 0; r < k; ++r)
            res[r] = Greater(array1, i, array2, j)?array1[i++]:array2[j++];
        return res;
    };
    const int n1 = static_cast<int>(nums1.size());
    const int n2 = static_cast<int>(nums2.size());
    std::vector<int> result;
    for (int i = std::max(0, k - n2); (i <= k) && (i <= n1); ++i)
        if (std::vector<int> data = merge(maxArray(nums1, i), maxArray(nums2, k - i));
            Greater(data, 0, result, 0))
            result = data;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxNumber(nums1, nums2, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 4, 6, 5}, {9, 1, 2, 5, 8, 3}, 5, {9, 8, 6, 5, 3}, trials);
    test({6, 7}, {6, 0, 4}, 5, {6, 7, 6, 0, 4}, trials);
    test({3, 9}, {8, 9}, 3, {9, 8, 9}, trials);
    return 0;
}


