#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool PredictTheWinner(std::vector<int> nums)
{
    int dp[21] = {};
    const int n = static_cast<int>(nums.size());
    for (int i = 0; i < n; ++i) dp[i] = nums[i];
    
    for (int d = 1; d < n; ++d)
        for (int j = n - 1; j - d >= 0; --j)
            dp[j] = std::max(nums[j - d] - dp[j], nums[j] - dp[j - 1]);
    return dp[n - 1] >= 0;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = PredictTheWinner(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 5, 2}, false, trials);
    test({1, 5, 233, 7}, true, trials);
    return 0;
}


