#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int compareVersion(std::string version1, std::string version2)
{
    const int n1 = static_cast<int>(version1.size());
    const int n2 = static_cast<int>(version2.size());
    auto version = [](std::string ver, int &pos, int size) -> int
    {
        int result = 0;
        for (; (pos < size) && (ver[pos] != '.'); ++pos)
            result = result * 10 + static_cast<int>(ver[pos] - '0');
        if (pos < size) ++pos;
        return result;
    };
    int pos1 = 0, pos2 = 0;
    while ((pos1 < n1) || (pos2 < n2))
    {
        int res1 = version(version1, pos1, n1);
        int res2 = version(version2, pos2, n2);
        if (res1 < res2) return -1;
        else if (res1 > res2) return 1;
    }
    return 0;
}

// ############################################################################
// ############################################################################

void test(std::string version1,
          std::string version2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = compareVersion(version1, version2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1.01", "1.001", 0, trials);
    test("1.0" , "1.0.0", 0, trials);
    test("0.1", "1.1", -1, trials);
    test("1.0.1", "1", 1, trials);
    return 0;
}


