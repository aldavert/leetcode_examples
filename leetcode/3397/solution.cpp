#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxDistinctElements(std::vector<int> nums, int k)
{
    std::sort(nums.begin(), nums.end());
    int result = 0;
    for (int prev = -k; int value : nums)
    {
        if (prev < value + k)
        {
            prev = std::max(prev + 1, value - k);
            ++result;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxDistinctElements(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 3, 3, 4}, 2, 6, trials);
    test({4, 4, 4, 4}, 1, 3, trials);
    return 0;
}


