#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

bool validPath(int n,
               std::vector<std::vector<int> > edges,
               int source,
               int destination)
{
    std::vector<std::set<int> > neighbors(n);
    std::vector<bool> visited(n, false);
    for (const auto &edge : edges)
    {
        neighbors[edge[0]].insert(edge[1]);
        neighbors[edge[1]].insert(edge[0]);
    }
    std::queue<int> q;
    q.push(source);
    while (!q.empty())
    {
        int current = q.front();
        if (current == destination) return true;
        q.pop();
        if (visited[current]) continue;
        visited[current] = true;
        for (int neighbor : neighbors[current])
            q.push(neighbor);
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          int source,
          int destination,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = validPath(n, edges, source, destination);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{0, 1}, {1, 2}, {2, 0}}, 0, 2, true, trials);
    test(6, {{0, 1}, {0, 2}, {3, 5}, {5, 4}, {4, 3}}, 0, 5, false, trials);
    return 0;
}


