#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

#if 0
int canCompleteCircuit(std::vector<int> gas, std::vector<int> cost)
{
    const int n = static_cast<int>(gas.size());
    int tank = 0, idx = -1;
    for (int i = 0, min = std::numeric_limits<int>::max(); i < n; ++i)
    {
        tank += gas[i] - cost[i];
        if (tank < min)
        {
            min = tank;
            idx = i;
        }
    }
    return (tank >= 0)?((idx + 1) % n):-1;
}
#else
int canCompleteCircuit(std::vector<int> gas, std::vector<int> cost)
{
    const int n = static_cast<int>(gas.size());
    int tank = 0, start = 0;
    for (int i = 0, curr = 0; i < n; ++i)
    {
        int diff = gas[i] - cost[i];
        tank += diff;
        curr += diff;
        if (curr < 0)
        {
            start = i + 1;
            curr = 0;
        }
    }
    if (tank >= 0) return start;
    return -1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> gas,
          std::vector<int> cost,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = canCompleteCircuit(gas, cost);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, {3, 4, 5, 1, 2}, 3, trials);
    test({2, 3, 4}, {3, 4, 3}, -1, trials);
    test({5, 1, 2, 3, 4}, {4, 4, 1, 5, 1}, 4, trials);
    return 0;
}


