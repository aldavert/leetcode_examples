#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int balancedString(std::string s)
{
    const int n = static_cast<int>(s.size()),
              k = n / 4;
    int result = n;
    int count[128] = {};
    
    for (char c : s) ++count[static_cast<int>(c)];
    for (int i = 0, j = 0; i < n; ++i)
    {
        --count[static_cast<int>(s[i])];
        while ((j < n) && (count['Q'] <= k) && (count['W'] <= k)
                       && (count['E'] <= k) && (count['R'] <= k))
        {
            result = std::min(result, i - j + 1);
            ++count[static_cast<int>(s[j])];
            ++j;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = balancedString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("QWER", 0, trials);
    test("QQWE", 1, trials);
    test("QQQW", 2, trials);
    return 0;
}


