#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minMalwareSpread(std::vector<std::vector<int> > graph, std::vector<int> initial)
{
    const int n = static_cast<int>(graph.size());
    int result = 0;
    int min_count = n;
    auto search = [&](int removed) -> int
    {
        std::queue<int> q;
        std::vector<bool> seen(n);
        seen[removed] = true;
        int count = 0;
        for (const int i : initial)
            if (i != removed)
                seen[i] = true,
                q.push(i);
        while (!q.empty())
        {
            int u = q.front();
            q.pop();
            ++count;
            for (int i = 0; i < n; ++i)
            {
                if (seen[i]) continue;
                if (i != u && graph[i][u])
                    seen[i] = true,
                    q.push(i);
            }
        }
        return count;
    };
    std::sort(initial.begin(), initial.end());
    for (int i : initial)
        if (int count = search(i); count < min_count)
            min_count = count,
            result = i;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > graph,
          std::vector<int> initial,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minMalwareSpread(graph, initial);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 0}, {1, 1, 0}, {0, 0, 1}}, {0, 1}, 0, trials);
    test({{1, 1, 0}, {1, 1, 1}, {0, 1, 1}}, {0, 1}, 1, trials);
    test({{1, 1, 0, 0}, {1, 1, 1, 0}, {0, 1, 1, 1}, {0, 0, 1, 1}}, {0, 1}, 1, trials);
    return 0;
}


