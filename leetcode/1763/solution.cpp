#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

#if 1
std::string longestNiceSubstring(std::string s)
{
    const size_t n = s.size();
    struct Histogram
    {
        std::bitset<26> histogram[2] = {};
        void update(char chr)
        {
            constexpr static char start_chr[] = {'A', 'a'};
            const bool lowercase = (chr >= 'a') && (chr <= 'z');
            histogram[lowercase][chr - start_chr[lowercase]] = true;
        }
        bool isNice(void) const { return (histogram[0] ^ histogram[1]) == 0; }
    };
    std::vector<Histogram> histograms(n);
    std::string result;
    for (size_t length = 0; length < n; ++length)
    {
        bool not_updated = true;
        for (size_t i = 0; i < n - length; ++i)
        {
            histograms[i].update(s[i + length]);
            if (not_updated and histograms[i].isNice())
                result = s.substr(i, length + 1),
                not_updated = false;
        }
    }
    return result;
}
#elif 0
std::string longestNiceSubstring(std::string s)
{
    const size_t n = s.size();
    std::bitset<26> histogram[101][2] = {};
    auto update = [&](char chr, size_t idx) -> void
    {
        constexpr char start_chr[] = {'A', 'a'};
        const bool lowercase = (chr >= 'a') && (chr <= 'z');
        histogram[idx][lowercase][chr - start_chr[lowercase]] = true;
    };
    auto isNice = [&](size_t idx) -> bool
    {
        return (histogram[idx][0] ^ histogram[idx][1]) == 0;
    };
    std::string result;
    for (size_t length = 0; length < n; ++length)
    {
        bool not_updated = true;
        for (size_t i = 0; i < n - length; ++i)
        {
            update(s[i + length], i);
            if (not_updated and isNice(i))
                result = s.substr(i, length + 1),
                not_updated = false;
        }
    }
    return result;
}
#else
std::string longestNiceSubstring(std::string s)
{
    const size_t n = s.size();
    std::vector<std::vector<int> > uppercase(s.size(), std::vector<int>(26, 0));
    std::vector<std::vector<int> > lowercase(s.size(), std::vector<int>(26, 0));
    auto update = [&](char chr, size_t idx) -> void
    {
        if ((chr >= 'a') && (chr <= 'z')) ++lowercase[idx][chr - 'a'];
        else ++uppercase[idx][chr - 'A'];
    };
    auto isNice = [&](size_t idx) -> bool
    {
        for (size_t i = 0; i < 26; ++i)
            if ((uppercase[idx][i] > 0) ^ (lowercase[idx][i] > 0))
                return false;
        return true;
    };
    std::string result;
    for (size_t length = 0; length < n; ++length)
    {
        bool not_updated = true;
        for (size_t i = 0; i < n - length; ++i)
        {
            update(s[i + length], i);
            if (not_updated and isNice(i))
                result = s.substr(i, length + 1),
                not_updated = false;
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestNiceSubstring(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("YazaAay", "aAa", trials);
    test("Bb", "Bb", trials);
    test("c", "", trials);
    return 0;
}


