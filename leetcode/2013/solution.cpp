#include "../common/common.hpp"
#include <unordered_map>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class DetectSquares
{
    std::unordered_map<int, int> m_point_count;
    inline int hash(int i, int j) const { return i << 10 | j; }
public:
    DetectSquares(void) = default;
    void add(std::vector<int> point)
    {
        ++m_point_count[hash(point[0], point[1])];
    }
    int count(std::vector<int> point)
    {
        const int x1 = point[0], y1 = point[1];
        int result = 0;
        
        for (const auto& [h, count] : m_point_count)
        {
            int x3 = h >> 10, y3 = h & 1023;
            if ((x1 != x3) && (std::abs(x1 - x3) == std::abs(y1 - y3)))
            {
                const int p = hash(x1, y3), q = hash(x3, y1);
                auto search_p = m_point_count.find(p),
                     search_q = m_point_count.find(q);
                if ((search_p != m_point_count.end()) && (search_q != m_point_count.end()))
                    result += count * search_p->second * search_q->second;
            }
        }
        return result;
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<bool> add,
          std::vector<std::vector<int> > points,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        DetectSquares obj;
        result.clear();
        for (size_t i = 0; i < add.size(); ++i)
        {
            if (add[i])
            {
                result.push_back(null);
                obj.add(points[i]);
            }
            else result.push_back(obj.count(points[i]));
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({true, true, true, false, false, true, false},
         {{3, 10}, {11, 2}, {3, 2}, {11, 10}, {14, 8}, {11, 2}, {11, 10}},
         {null, null, null, 1, 0, null, 2}, trials);
    return 0;
}


