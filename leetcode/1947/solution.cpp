#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int maxCompatibilitySum(std::vector<std::vector<int> > students,
                        std::vector<std::vector<int> > mentors)
{
    const int m = static_cast<int>(mentors.size());
    std::vector<std::vector<int> > dp(m, std::vector<int>((1 << m) + 1, -1));
    auto solve = [&](auto &&self, size_t i, int mask)
    {
        if (i >= students.size()) return 0;
        if (dp[i][mask] != -1) return dp[i][mask];
        int result = 0;
        for (size_t j = 0; j < mentors.size(); ++j)
        {
            int bit = (mask >> j) & 1;
            if (bit != 0) continue;
            int count = 0;
            for (size_t k = 0; k < students[0].size(); k++)
                count += (students[i][k] == mentors[j][k]);
            result = std::max(result, count + self(self, i + 1, mask | (1 << j)));
        }
        return dp[i][mask] = result;
    };
    return solve(solve, 0, 0);
}
#else
int maxCompatibilitySum(std::vector<std::vector<int> > students,
                        std::vector<std::vector<int> > mentors)
{
    auto calcScore = [&](size_t i, size_t j)
    {
        int score = 0;
        for (size_t k = 0; k < students[i].size(); ++k)
            score += students[i][k] == mentors[j][k];
        return score;
    };
    std::vector<bool> used(students.size());
    int result = 0;
    auto dfs = [&](auto &&self, size_t i, int score_sum)
    {
        if (i == students.size())
        {
            result = std::max(result, score_sum);
            return;
        }
        for (size_t j = 0; j < students.size(); ++j)
        {
            if (used[j]) continue;
            used[j] = true;
            self(self, i + 1, score_sum + calcScore(i, j));
            used[j] = false;
        }
    };
    dfs(dfs, 0, 0);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > students,
          std::vector<std::vector<int> > mentors,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxCompatibilitySum(students, mentors);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 0}, {1, 0, 1}, {0, 0, 1}},
         {{1, 0, 0}, {0, 0, 1}, {1, 1, 0}}, 8, trials);
    test({{0, 0}, {0, 0}, {0, 0}}, {{1, 1}, {1, 1}, {1, 1}}, 0, trials);
    return 0;
}


