#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumNumbers(int num, int k)
{
    if (num == 0) return 0;
    for (int i = 1; (i <= 10) && (i * k <= num); ++i)
        if (i * k % 10 == num % 10)
            return i;
    return -1;
}

// ############################################################################
// ############################################################################

void test(int num, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumNumbers(num, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(58, 9, 2, trials);
    test(37, 2, -1, trials);
    test(0, 7, 0, trials);
    return 0;
}


