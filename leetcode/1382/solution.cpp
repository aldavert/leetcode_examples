#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * balanceBST(TreeNode * root)
{
    auto process = [&](void) -> TreeNode *
    {
        std::vector<int> values;
        auto inorder = [&values](auto &&self, TreeNode * node) -> void
        {
            if (node == nullptr) return;
            self(self, node->left);
            values.push_back(node->val);
            self(self, node->right);
        };
        auto build = [&values](auto &&self, int l, int r) -> TreeNode *
        {
            if (l > r) return nullptr;
            int m = (l + r) / 2;
            return new TreeNode(values[m], self(self, l, m - 1), self(self, m + 1, r));
        };
        inorder(inorder, root);
        return build(build, 0, static_cast<int>(values.size()) - 1);
    };
    return process();
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        TreeNode * head = balanceBST(root);
        result = tree2vec(head);
        delete root;
        delete head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, null, 2, null, 3, null, 4, null, null},
         {2, 1, 3, null, null, null, 4}, trials);
    test({2, 1, 3}, {2, 1, 3}, trials);
    return 0;
}


