#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string validIPAddress(std::string queryIP)
{
    auto isIPv4 = [](std::string query) -> bool
    {
        std::string substring;
        int number_of_dots = 0;
        auto isError = [&]() -> bool
        {
            return (number_of_dots > 3)
                || (substring.size() == 0)
                || ((substring.size() > 1) && (substring[0] == '0'))
                || (std::atoi(substring.c_str()) > 255);
        };
        for (char symbol : query)
        {
            if (symbol == '.')
            {
                ++number_of_dots;
                if (isError()) return false;
                substring = "";
            }
            else if ((symbol < '0') || (symbol > '9')) return false;
            else if (substring.size() == 3) return false;
            else substring += symbol;
        }
        return !(isError() || (number_of_dots != 3));
    };
    auto isIPv6 = [](std::string query) -> bool
    {
        int valid = 0, number_of_colons = 0;
        for (char symbol : query)
        {
            if (symbol == ':')
            {
                ++number_of_colons;
                if ((number_of_colons > 7) || (valid == 0) || (valid > 4))
                    return false;
                valid = 0;
                continue;
            }
            int previous = valid;
            valid += ((symbol >= '0') && (symbol <= '9'))
                  || ((symbol == 'a') || (symbol == 'A'))
                  || ((symbol == 'b') || (symbol == 'B'))
                  || ((symbol == 'c') || (symbol == 'C'))
                  || ((symbol == 'd') || (symbol == 'D'))
                  || ((symbol == 'e') || (symbol == 'E'))
                  || ((symbol == 'f') || (symbol == 'F'));
            if ((valid == previous) || (valid > 4)) return false;
        }
        return (valid > 0) && (number_of_colons == 7);
    };
    if      (isIPv4(queryIP)) return "IPv4";
    else if (isIPv6(queryIP)) return "IPv6";
    else return "Neither";
}

// ############################################################################
// ############################################################################

void test(std::string queryIP, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = validIPAddress(queryIP);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("172.16.254.1", "IPv4", trials);
    test("2001:0db8:85a3:0:0:8A2E:0370:7334", "IPv6", trials);
    test("256.256.256.256", "Neither", trials);
    test("172.16.1", "Neither", trials);
    test("172.16.1.32.21", "Neither", trials);
    test("192.0.0.1", "IPv4", trials);
    test("2001:db8:85a3:0:8a2E:8a2E:7334:", "Neither", trials);
    return 0;
}


