#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int strongPasswordChecker(std::string password)
{
    const int n = static_cast<int>(password.size());
    int present = 0;
    for (char c : password)
    {
        present = present
                | (((c >= 'A') && (c <= 'Z')) << 2)
                | (((c >= 'a') && (c <= 'z')) << 1)
                | (((c >= '0') && (c <= '9')) << 0);
    }
    const int missing = !(present & 1)
                      + !(present & 2)
                      + !(present & 4);
    int replace = 0, burst = 0, burst_res[3] = {0, 0, 0};
    for (char previous = '\0'; char c : password)
    {
        if (previous == c) ++burst;
        else
        {
            if (burst > 2)
            {
                replace += burst / 3;
                ++burst_res[(burst % 3)];
            }
            burst = 1;
        }
        previous = c;
    }
    if (burst > 2)
    {
        replace += burst / 3;
        ++burst_res[(burst % 3)];
    }
    if (n <   6) return std::max(6 - n  , missing);
    if (n <= 20) return std::max(replace, missing);
    const int remove = n - 20;
    replace -= std::min(burst_res[0], remove)
             + std::min(std::max(remove - burst_res[0], 0), burst_res[1] * 2) / 2
             + std::max(remove - burst_res[0] - burst_res[1] * 2, 0) / 3;
    return remove + std::max(replace, missing);
}

// ############################################################################
// ############################################################################

void test(std::string password, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = strongPasswordChecker(password);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("a", 5, trials);
    test("aA1", 3, trials);
    test("1337C0d3", 0, trials);
    test("aaa111", 2, trials);
    return 0;
}


