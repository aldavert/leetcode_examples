#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxSumAfterPartitioning(std::vector<int> arr, int k)
{
    int dp[502] = {};
    const int n = static_cast<int>(arr.size());
    for (int i = 1; i <= n; ++i)
    {
        int max_k = std::numeric_limits<int>::lowest();
        for (int j = 1, m = std::min(i, k); j <= m; ++j)
        {
            max_k = std::max(max_k, arr[i - j]);
            dp[i] = std::max(dp[i], dp[i - j] + max_k * j);
        }
    }
    return dp[n];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSumAfterPartitioning(arr, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 15, 7, 9, 2, 5, 10}, 3, 84, trials);
    test({1, 4, 1, 5, 7, 3, 6, 1, 9, 9, 3}, 4, 83, trials);
    test({1}, 1, 1, trials);
    return 0;
}


