#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canMouseWin(std::vector<std::string> grid, int catJump, int mouseJump)
{
    constexpr int dirs[] = {0, 1, 0, -1, 0};
    int m = static_cast<int>(grid.size());
    int n = static_cast<int>(grid[0].size());
    int cat = 0, mouse = 0, n_floors = 0;
    auto hash = [&](int i, int j) { return i * n + j; };
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            if      (grid[i][j] != '#') ++n_floors;
            if      (grid[i][j] == 'C') cat   = hash(i, j);
            else if (grid[i][j] == 'M') mouse = hash(i, j);
        }
    }
    std::vector<std::vector<std::vector<char> > > dp(m * n,
            std::vector<std::vector<char> >(m * n, std::vector<char>(n_floors * 2, -1)));
    auto solve = [&](auto &&self, int cat_pos, int mouse_pos, int turn) -> bool
    {
        if (turn == n_floors * 2) return false;
        if (dp[cat_pos][mouse_pos][turn] != -1) return dp[cat_pos][mouse_pos][turn];
        if (turn % 2 == 0)
        {
            const int i = mouse_pos / n;
            const int j = mouse_pos % n;
            for (int k = 0; k < 4; ++k)
            {
                for (int jump = 0; jump <= mouseJump; ++jump)
                {
                    const int x = i + dirs[k    ] * jump;
                    const int y = j + dirs[k + 1] * jump;
                    if ((x < 0) || (x == m) || (y < 0) || (y == n)) break;
                    if (grid[x][y] == '#') break;
                    if (grid[x][y] == 'F')
                        return (dp[cat_pos][mouse_pos][turn] = true);
                    if (self(self, cat_pos, hash(x, y), turn + 1))
                        return (dp[cat_pos][mouse_pos][turn] = true);
                }
            }
            return (dp[cat_pos][mouse_pos][turn] = false);
        }
        else
        {
            const int i = cat_pos / n;
            const int j = cat_pos % n;
            for (int k = 0; k < 4; ++k)
            {
                for (int jump = 0; jump <= catJump; ++jump)
                {
                    const int x = i + dirs[k    ] * jump;
                    const int y = j + dirs[k + 1] * jump;
                    if ((x < 0) || (x == m) || (y < 0) || (y == n)) break;
                    if (grid[x][y] == '#') break;
                    if (grid[x][y] == 'F')
                        return (dp[cat_pos][mouse_pos][turn] = false);
                    int next_cat = hash(x, y);
                    if (next_cat == mouse_pos)
                        return (dp[cat_pos][mouse_pos][turn] = false);
                    if (!self(self, next_cat, mouse_pos, turn + 1))
                        return (dp[cat_pos][mouse_pos][turn] = false);
                }
            }
            return (dp[cat_pos][mouse_pos][turn] = true);
        }
    };
    return solve(solve, cat, mouse, 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> grid,
          int catJump,
          int mouseJump,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canMouseWin(grid, catJump, mouseJump);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"####F", "#C...", "M...."}, 1, 2, true, trials);
    test({"M.C...F"}, 1, 4, true, trials);
    test({"M.C...F"}, 1, 3, false, trials);
    return 0;
}


