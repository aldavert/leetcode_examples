# Number of Subarrays with Bounded Maximum

We are given an array `nums` of positive integers, and two positive integers `left` and `right` (`left <= right`).

Return *the number of (contiguous, non-empty)* ***subarrays*** *such that the value of the maximum array element in that subarray is at least* `left` *and at most* `right`.

The test cases are generated so that the answer will fit in a **32-bit** integer.

#### Example 1:
> *Input:* `nums = [2, 1, 4, 3] left = 2 right = 3`  
> *Output:* `3`  
> *Explanation:* There are three subarrays that meet the requirements: `[2], [2, 1], [3]`.

#### Example 2:
> *Input:* `nums = [2, 9, 2, 5, 6], left = 2, right = 8`  
> *Output:* `7`

#### Constrains:
- `1 <= nums.length <= 10^5`
- `0 <= nums[i] <= 10^9`
- `0 <= left <= right <= 10^9`


