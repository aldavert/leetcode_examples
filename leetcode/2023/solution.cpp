#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int numOfPairs(std::vector<std::string> nums, std::string target)
{
    const int n = static_cast<int>(target.size());
    std::unordered_map<std::string, int> count;
    int result = 0;
    
    for (const auto &num : nums)
    {
        const int k = static_cast<int>(num.size());
        if (k >= n)
            continue;
        if (target.substr(0, k) == num)
            result += count[target.substr(k)];
        if (target.substr(n - k) == num)
            result += count[target.substr(0, n - k)];
        ++count[num];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> nums,
          std::string target,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numOfPairs(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"777", "7", "77", "77"}, "7777", 4, trials);
    test({"123", "4", "12", "34"}, "1234", 2, trials);
    test({"1", "1", "1"}, "11", 6, trials);
    return 0;
}


