#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countGoodStrings(int low, int high, int zero, int one)
{
    constexpr int MOD = 1'000'000'007;
    int result = 0;
    std::vector<int> dp(high + 1);
    dp[0] = 1;
    
    for (int i = 1; i <= high; ++i)
    {
        if (i >= zero) dp[i] = (dp[i] + dp[i - zero]) % MOD;
        if (i >= one)  dp[i] = (dp[i] + dp[i  - one]) % MOD;
        if (i >= low)  result = (result + dp[i]) % MOD;
    }
    
    return result;
}

// ############################################################################
// ############################################################################

void test(int low, int high, int zero, int one, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countGoodStrings(low, high, zero, one);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 3, 1, 1, 8, trials);
    test(2, 3, 1, 2, 5, trials);
    return 0;
}


