#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int findClosestNumber(std::vector<int> nums)
{
    int result = std::numeric_limits<int>::max();
    for (int number : nums)
        if (((number == -result) && (number > 0))
        ||  (std::abs(number) < std::abs(result))) result = number;
    return result;
}
#else
int findClosestNumber(std::vector<int> nums)
{
    int result = std::numeric_limits<int>::max();
    for (int number : nums)
        if (std::abs(number) < std::abs(result)) result = number;
        else if ((number == -result) && (number > 0)) result = number;
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findClosestNumber(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-4, -2, 1, 4, 8}, 1, trials);
    test({2, -1, 1}, 1, trials);
    return 0;
}


