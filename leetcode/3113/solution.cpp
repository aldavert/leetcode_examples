#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long numberOfSubarrays(std::vector<int> nums)
{
    long result = 0;
    for (std::vector<std::pair<int, int> > stack; const int num : nums)
    {
        while (!stack.empty() && (stack.back().first < num))
        stack.pop_back();
        if (stack.empty() || (stack.back().first != num))
            stack.emplace_back(num, 0);
        result += ++stack.back().second;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfSubarrays(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 3, 3, 2}, 6, trials);
    test({3, 3, 3}, 6, trials);
    test({1}, 1, trials);
    return 0;
}


