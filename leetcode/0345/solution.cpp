#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string reverseVowels(std::string s)
{
    const int n = static_cast<int>(s.size());
    for (int left = 0, right = n; left < right;)
    {
        while ((left < n) && (s[left] != 'a') && (s[left] != 'e')
            && (s[left] != 'i') && (s[left] != 'o') && (s[left] != 'u')
            && (s[left] != 'A') && (s[left] != 'E') && (s[left] != 'I')
            && (s[left] != 'O') && (s[left] != 'U')) ++left;
        while ((right >= 0) && (s[right] != 'a') && (s[right] != 'e')
            && (s[right] != 'i') && (s[right] != 'o') && (s[right] != 'u')
            && (s[right] != 'A') && (s[right] != 'E') && (s[right] != 'I')
            && (s[right] != 'O') && (s[right] != 'U')) --right;
        if (left < right) std::swap(s[left++], s[right--]);
    }
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = reverseVowels(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("hello", "holle", trials);
    test("leetcode", "leotcede", trials);
    test(" ", " ", trials);
    return 0;
}


