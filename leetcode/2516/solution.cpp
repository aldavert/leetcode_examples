#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int takeCharacters(std::string s, int k)
{
    const int n = static_cast<int>(s.length());
    int result = n, count[3] = {};
    
    for (const char c : s) ++count[c - 'a'];
    if ((count[0] < k) || (count[1] < k) || (count[2] < k))
        return -1;
    for (int l = 0, r = 0; r < n; ++r)
    {
        --count[s[r] - 'a'];
        while (count[s[r] - 'a'] < k)
            ++count[s[l++] - 'a'];
        result = std::min(result, n - (r - l + 1));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = takeCharacters(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aabaaaacaabc", 2, 8, trials);
    test("a", 1, -1, trials);
    return 0;
}


