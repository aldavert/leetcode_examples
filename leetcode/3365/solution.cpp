#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

bool isPossibleToRearrange(std::string s, std::string t, int k)
{
    const int n = static_cast<int>(s.size());
    const int m = n / k;
    std::unordered_map<std::string, int> histogram;
    for (int i = 0; i < n; i += m)
        ++histogram[s.substr(i, m)];
    for (int i = 0; i < n; i += m)
    {
        if (auto search = histogram.find(t.substr(i, m)); search != histogram.end())
        {
            if (--search->second == 0) histogram.erase(search);
        }
        else return false;
    }
    return histogram.empty();
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string t, int k, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPossibleToRearrange(s, t, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcd", "cdab", 2, true, trials);
    test("aabbcc", "bbaacc", 3, true, trials);
    test("aabbcc", "bbaacc", 2, false, trials);
    return 0;
}


