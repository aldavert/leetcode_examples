#include "../common/common.hpp"

// ############################################################################
// ############################################################################

void solveSudoku(std::vector<std::vector<char> > &board)
{
    bool row[9][9] = {}, column[9][9] = {}, used_board[9][9] = {};
    auto solve = [&](auto &&self, int r, int c) -> bool
    {
        for (; r < 9; ++r)
        {
            for (; c < 9; ++c)
            {
                if (board[r][c] == '.')
                {
                    int cell = r / 3 + 3 * (c / 3);
                    for (int n = 0; n < 9; ++n)
                    {
                        if (!(row[r][n] || column[c][n] || used_board[cell][n]))
                        {
                            row[r][n] = column[c][n] = used_board[cell][n] = true;
                            board[r][c] = static_cast<char>('1' + n);
                            if (self(self, r, c + 1))
                                return true;
                            row[r][n] = column[c][n] = used_board[cell][n] = false;
                        }
                    }
                    board[r][c] = '.';
                    return false;
                }
            }
            c = 0;
        }
        return true;
    };

    for (int r = 0; r < 9; ++r)
    {
        for (int c = 0; c < 9; ++c)
        {
            if (board[r][c] != '.')
            {
                int idx = board[r][c] - '1';
                row[r][idx] = true;
                column[c][idx] = true;
                int cell = r / 3 + 3 * (c / 3);
                used_board[cell][idx] = true;
            }
        }
    }
    solve(solve, 0, 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > board,
          std::vector<std::vector<char> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<char> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = board;
        solveSudoku(result);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif

    test({{'5','3','.','.','7','.','.','.','.'}
         ,{'6','.','.','1','9','5','.','.','.'}
         ,{'.','9','8','.','.','.','.','6','.'}
         ,{'8','.','.','.','6','.','.','.','3'}
         ,{'4','.','.','8','.','3','.','.','1'}
         ,{'7','.','.','.','2','.','.','.','6'}
         ,{'.','6','.','.','.','.','2','8','.'}
         ,{'.','.','.','4','1','9','.','.','5'}
         ,{'.','.','.','.','8','.','.','7','9'}},
         {{'5','3','4','6','7','8','9','1','2'}
         ,{'6','7','2','1','9','5','3','4','8'}
         ,{'1','9','8','3','4','2','5','6','7'}
         ,{'8','5','9','7','6','1','4','2','3'}
         ,{'4','2','6','8','5','3','7','9','1'}
         ,{'7','1','3','9','2','4','8','5','6'}
         ,{'9','6','1','5','3','7','2','8','4'}
         ,{'2','8','7','4','1','9','6','3','5'}
         ,{'3','4','5','2','8','6','1','7','9'}}, trials);

    return 0;
}


