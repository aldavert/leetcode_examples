#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxScoreSightseeingPair(std::vector<int> values)
{
    int result = 0, best_previous = 0;
    for (int value : values)
    {
        result = std::max(result, value + best_previous);
        best_previous = std::max(best_previous, value) - 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> values, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxScoreSightseeingPair(values);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 1, 5, 2, 6}, 11, trials);
    test({1, 2}, 2, trials);
    return 0;
}


