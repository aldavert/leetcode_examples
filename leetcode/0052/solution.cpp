#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

int totalNQueens(int n)
{
    int n2 = n * n;
    auto solve = [&](auto &&self, int row, char * grid) -> int
    {
        if (row == n) // Done.
            return 1;
        else
        {
            int result = 0;
            char * grid_row = grid + n * row;
            for (int c = 0; c < n; ++c)
            {
                if (grid_row[c] == 1)
                {
                    char * dst = grid + n2;
                    std::memcpy(dst, grid, n2);
                    for (int i = row + 1, j = 1; i < n; ++i, ++j)
                    {
                        dst[n * i + c] = 0; // Mark the current column as occupied.
                        if (c - j >= 0)
                            dst[n * i + c - j] = 0;
                        if (c + j <  n)
                            dst[n * i + c + j] = 0;
                    }
                    dst[n * row + c] = 2; // Put the queen.
                    result += self(self, row + 1, dst);
                }
            }
            return result;
        }
    };
    char grid[9 * 9 * 10];
    std::memset(grid, 1, sizeof(grid));
    
    return solve(solve, 0, grid);
}

// ############################################################################
// ############################################################################

void test(const int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = totalNQueens(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 2, trials);
    test(1, 1, trials);
    return 0;
}


