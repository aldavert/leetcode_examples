#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isReachableAtTime(int sx, int sy, int fx, int fy, int t)
{
    if ((sx == fx) && (sy == fy) && (t == 1)) return false;
    return std::max(std::abs(sx - fx), std::abs(sy - fy)) <= t;
}

// ############################################################################
// ############################################################################

void test(int sx, int sy, int fx, int fy, int t, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isReachableAtTime(sx, sy, fx, fy, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 4, 7, 7, 6,  true, trials);
    test(3, 1, 7, 3, 3, false, trials);
    test(1, 2, 1, 2, 1, false, trials);
    return 0;
}


