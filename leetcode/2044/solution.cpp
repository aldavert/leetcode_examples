#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int countMaxOrSubsets(std::vector<int> nums)
{
    const int ors = std::accumulate(nums.begin(), nums.end(), 0, std::bit_or<>());
    int result = 0;
    auto dfs = [&](auto &&self, size_t i, int path) -> void
    {
        if (i == nums.size()) result += (path == ors);
        else
        {
            self(self, i + 1, path);
            self(self, i + 1, path | nums[i]);
        }
    };
    dfs(dfs, 0, 0);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countMaxOrSubsets(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1}, 2, trials);
    test({2, 2, 2}, 7, trials);
    test({3, 2, 1, 5}, 6, trials);
    return 0;
}


