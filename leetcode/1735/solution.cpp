#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1 // Runtime: 124 ms
std::vector<int> waysToFillArray(std::vector<std::vector<int> > queries)
{
    constexpr long MOD = 1'000'000'007;
    int comb[10013][14] = { 1 };
    constexpr static int primes[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37,
                                     41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83,
                                     89, 97};
    const int n_queries = static_cast<int>(queries.size());
    for (int n = 1; n < 10013; ++n)
        for (int r = 0; r < 14; ++r)
            comb[n][r] = (r == 0)?1:(comb[n - 1][r - 1] + comb[n - 1][r]) % MOD;
    std::vector<int> result(n_queries, 1);
    for (int i = 0; i < n_queries; ++i)
    {
        int n = queries[i][0], k = queries[i][1];
        for (auto p : primes)
        {
            int r = 0;
            while (k % p == 0) ++r, k /= p;
            result[i] = static_cast<int>(static_cast<long>(result[i])
                                       * static_cast<long>(comb[n - 1 + r][r]) % MOD);
        }
        if (k != 1)
            result[i] = static_cast<int>(static_cast<long>(result[i]) * n % MOD);
    }
    return result;
}
#elif 1 // Runtime: 598 ms
std::vector<int> waysToFillArray(std::vector<std::vector<int> > queries)
{
    constexpr int MOD = 1'000'000'007, N = 10020;
    std::vector<int> result, p[N];
    long f[N], g[N];
    auto comb = [&](int n, int k) -> int
    {
        return static_cast<int>((f[n] * g[k] % MOD) * g[n - k] % MOD);
    };
    auto qmi = [&](long a, long k) -> long
    {
        long r = 1;
        while (k != 0)
        {
            if ((k & 1) == 1) r = r * a % MOD;
            k >>= 1;
            a = a * a % MOD;
        }
        return r;
    };
    f[0] = 1;
    g[0] = 1;
    for (int i = 1; i < N; ++i)
    {
        f[i] = f[i - 1] * i % MOD;
        g[i] = qmi(f[i], MOD - 2);
        int x = i;
        for (int j = 2; j <= x / j; ++j)
        {
            if (x % j == 0)
            {
                int count = 0;
                while (x % j == 0)
                    ++count,
                    x /= j;
                p[i].push_back(count);
            }
        }
        if (x > 1) p[i].push_back(1);
    }
    
    for (auto &q : queries)
    {
        int n = q[0], k = q[1];
        long long t = 1;
        for (int x : p[k])
            t = t * comb(x + n - 1, n - 1) % MOD;
        result.push_back(static_cast<int>(t));
    }
    return result;
}
#else  // Runtime: 983 ms
std::vector<int> waysToFillArray(std::vector<std::vector<int> > queries)
{
    constexpr int MOD = 1'000'000'007;
    int n = 0;
    std::unordered_map<int, int> c, dp;
    std::function<int(int, int)> cnk = [&](int m, int k)
    {
        if (k > m) return 0;
        if ((k == 0) || (k == m)) return 1;      
        int &result = c[(m << 16) | k];
        if (!result) result = (cnk(m - 1, k - 1) + cnk(m - 1, k)) % MOD; 
        return result;      
    };
    std::function<int(int, int)> dfs = [&](int s, int k) -> int
    {
        if (s == 0) return k == 1;
        if (k == 1) return cnk(n, s);
        int &result = dp[(s << 16) | k];
        if (result) return result;      
        for (int i = 1; i * i <= k; ++i)
        {
            if (k % i) continue;
            if (    i != 1) result = (result + dfs(s - 1, k / i)) % MOD;
            if (i * i != k) result = (result + dfs(s - 1, i)) % MOD;
        }
        return result;
    };
    
    std::vector<int> result;
    for (const auto &q : queries)
    {
        dp.clear();
        n = q[0];
        result.push_back(dfs(n, q[1]));
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = waysToFillArray(queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 6}, {5, 1}, {73, 660}}, {4, 1, 50734910}, trials);
    test({{1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}}, {1, 2, 3, 10, 5}, trials);
    return 0;
}


