#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > findPrimePairs(int n)
{
    std::vector<bool> primes(n, true);
    for (int i = 2; i < n; ++i)
        if (primes[i])
            for (int j = i + i; j < n; j += i)
                primes[j] = false;
    std::vector<std::vector<int> > result;
    for (int x = 2; x <= n / 2; ++x)
        if (int y = n - x; primes[x] && primes[y])
            result.push_back({x, y});
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findPrimePairs(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, {{3, 7}, {5, 5}}, trials);
    test(2, {}, trials);
    return 0;
}


