#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int minimumDifference(std::vector<int> nums, int k)
{
    int const n = static_cast<int>(nums.size());
    int result = std::numeric_limits<int>::max();
    for (int i = 0; i < n; ++i)
    {
        result = std::min(result, std::abs(nums[i] - k));
        for (int j = i - 1; (j >= 0) && ((nums[j] | nums[i]) != nums[j]); --j)
        {
            nums[j] = nums[j] | nums[i];
            result = std::min(result, std::abs(nums[j] - k));
            if (result == 0) return 0;
        }
    }
    return result;
}
#else
int minimumDifference(std::vector<int> nums, int k)
{
    int result = std::numeric_limits<int>::max();
    for (std::unordered_set<int> prev; int num : nums)
    {
        std::unordered_set<int> next{num};
        for (int val : prev)
            next.insert(val | num);
        for (int val : next)
            result = std::min(result, std::abs(k - val));
        prev = std::move(next);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumDifference(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 4, 5}, 3, 0, trials);
    test({1, 3, 1, 3}, 2, 1, trials);
    return 0;
}


