#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int wateringPlants(std::vector<int> plants, int capacity)
{
    const int n = static_cast<int>(plants.size());
    int result = 0, current_capacity = 0;
    for (int i = 0; i < n; ++i)
    {
        if (current_capacity + plants[i] <= capacity)
            current_capacity += plants[i];
        else
        {
            current_capacity = plants[i];
            result += i * 2;
        }
    }
    return result + n;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> plants,
          int capacity,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = wateringPlants(plants, capacity);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 2, 3, 3}, 5, 14, trials);
    test({1, 1, 1, 4, 2, 3}, 4, 30, trials);
    test({7, 7, 7, 7, 7, 7, 7}, 8, 49, trials);
    return 0;
}


