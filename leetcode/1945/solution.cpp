#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int getLucky(std::string s, int k)
{
    int result = 0;
    for (char letter : s)
    {
        letter -= 'a' - 1;
        if      (letter >= 20) result += static_cast<int>(letter - 20) + 2;
        else if (letter >= 10) result += static_cast<int>(letter - 10) + 1;
        else                   result += static_cast<int>(letter);
    }
    for (int t = 1; t < k; ++t)
    {
        int work = result;
        result = 0;
        while (work > 0)
        {
            result += work % 10;
            work /= 10;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getLucky(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("iiii", 1, 36, trials);
    test("leetcode", 2, 6, trials);
    test("zbax", 2, 8, trials);
    return 0;
}


