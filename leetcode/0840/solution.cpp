#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numMagicSquaresInside(std::vector<std::vector<int> > grid)
{
    constexpr char numbers[] = "0123456789          ";
    std::string pattern(8, ' ');
    constexpr int row_offset[] = {0, 0, 0, 1, 2, 2, 2, 1},
                  col_offset[] = {0, 1, 2, 2, 2, 1, 0, 0};
    auto isMagic = [&](size_t i, size_t j) -> bool
    {
        for (size_t p = 0; p < 8; ++p)
            pattern[p] = numbers[grid[i + row_offset[p]][j + col_offset[p]]];
        return (std::string("4381672943816729").find(pattern) != std::string::npos)
            || (std::string("9276183492761834").find(pattern) != std::string::npos);
    };
    int result = 0;
    for (size_t i = 0; i + 2 < grid.size(); ++i)
        for (size_t j = 0; j + 2 < grid[0].size(); ++j)
            result += ((grid[i][j] & 1) == 0)
                    * (grid[i + 1][j + 1] == 5)
                    * isMagic(i, j);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numMagicSquaresInside(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{4, 3, 8, 4}, {9, 5, 1, 9}, {2, 7, 6, 2}}, 1, trials);
    test({{8}}, 0, trials);
    test({{10, 3, 5}, {1, 6, 11}, {7, 9, 2}}, 0, trials);
    return 0;
}


