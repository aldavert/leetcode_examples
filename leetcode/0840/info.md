# Magic Squares in Grid

A `3 * 3` **magic square** is a `3 * 3` grid filled with distinct numbers **from** `1` **to** `9` such that each row, column, and both diagonals all have the same sum.

Given a `row * col` grid of integers, how many `3 * 3` "magic square" subgrids are there? (Each subgrid is contiguous).

**Note:** while a magic square can only contain number from `1` to `9`, `grid` may contain numbers up to `15`.

#### Example 1:
> ```
> +-----+-----+-----+-----+
> |     |     |     |     |
> |  4  |  3  |  8  |  4  |
> |     |     |     |     |
> +-----+-----+-----+-----+
> |     |     |     |     |
> |  9  |  5  |  1  |  9  |
> |     |     |     |     |
> +-----+-----+-----+-----+
> |     |     |     |     |
> |  2  |  7  |  6  |  2  |
> |     |     |     |     |
> +-----+-----+-----+-----+
> ```
> *Input:* `grid = [[4, 3, 8, 4], [9, 5, 1, 9], [2, 7, 6, 2]]`  
> *Output:* `1`  
> *Explanation:* The following subgrid is a `3 * 3` magic square:
> ```
>                     +-> 15
>                    /
> +-----+-----+-----+
> |     |     |     |
> |  4  |  3  |  8  | -> 15
> |     |     |     |
> +-----+-----+-----+
> |     |     |     |
> |  9  |  5  |  1  | -> 15
> |     |     |     |
> +-----+-----+-----+
> |     |     |     |
> |  2  |  7  |  6  | -> 15
> |     |     |     |
> +-----+-----+-----+
>    |     |     |   \
>    V     V     V    +-> 15
>   15    15    15
> ```
> while this one is not:
> ```
>                     +-> 12
>                    /
> +-----+-----+-----+
> |     |     |     |
> |  3  |  8  |  4  | -> 15
> |     |     |     |
> +-----+-----+-----+
> |     |     |     |
> |  5  |  1  |  9  | -> 15
> |     |     |     |
> +-----+-----+-----+
> |     |     |     |
> |  7  |  6  |  2  | -> 15
> |     |     |     |
> +-----+-----+-----+
>    |     |     |   \
>    V     V     V    +-> 6
>   15    15    15
> ```
> 
> In total, there is only one magic square inside the given grid.

#### Example 2:
> *Input:* `grid = [[8]]`  
> *Output:* `0`

#### Constraints:
- `row == grid.length`
- `col == grid[i].length`
- `1 <= row, col <= 10`
- `0 <= grid[i][j] <= 15`


