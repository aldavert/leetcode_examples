#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> stringSequence(std::string target)
{
    std::vector<std::string> result;
    for (std::string prefix = ""; char symbol : target)
    {
        for (char next = 'a'; next <= symbol; ++next)
            result.push_back(prefix + next);
        prefix += symbol;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string target, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = stringSequence(target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", {"a", "aa", "ab", "aba", "abb", "abc"}, trials);
    test("he", {"a", "b", "c", "d", "e", "f", "g", "h", "ha", "hb", "hc", "hd",
                "he"}, trials);
    return 0;
}


