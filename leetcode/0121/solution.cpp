#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int maxProfit(std::vector<int> prices)
{
    const int n = static_cast<int>(prices.size());
    if (n == 1) return 0;
    int maximum_profit = 0;
    for (int i = n - 1, max_price = 0; i >= 0; --i)
    {
        maximum_profit = std::max(maximum_profit, max_price - prices[i]);
        max_price = std::max(max_price, prices[i]);
    }
    return maximum_profit;
}
#else // Leetcode doesn't have ranges
int maxProfit(std::vector<int> prices)
{
    int maximum_profit = 0;
    for (int max_price = 0; int p : prices | std::ranges::views::reverse)
    {
        maximum_profit = std::max(maximum_profit, max_price - p);
        max_price = std::max(max_price, p);
    }
    return maximum_profit;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> prices, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxProfit(prices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({7, 1, 5, 3, 6, 4}, 5, trials);
    test({7, 6, 4, 3, 1}, 0, trials);
    return 0;
}


