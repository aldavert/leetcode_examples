#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canReachCorner(int xCorner, int yCorner, std::vector<std::vector<int> > circles)
{
    const int n = static_cast<int>(circles.size());
    std::vector<bool> visited(n);
    auto inCircle = [](long x, long y, long r, long px, long py)
    {
        long dx = x - px, dy = y - py;
        return dx * dx + dy * dy <= r * r;
    };
    auto dfs = [&](auto &&self, int i) -> bool
    {
        long x1 = circles[i][0], y1 = circles[i][1], r1 = circles[i][2];
        if (((x1 <= xCorner) && (y1 - r1 <= 0))
        ||  ((y1 <= yCorner) && (x1 + r1 >= xCorner)))
            return true;
        visited[i] = true;
        for (int j = 0; j < n; ++j)
        {
            if (visited[j]) continue;
            long x2 = circles[j][0], y2 = circles[j][1], r2 = circles[j][2];
            long dx = x1 - x2, dy = y1 - y2;
            
            if ((dx * dx + dy * dy <= (r1 + r2) * (r1 + r2))
            &&  (x1 * r2 + x2 * r1 < (r1 + r2) * xCorner)
            &&  (y1 * r2 + y2 * r1 < (r1 + r2) * yCorner)
            && self(self, j))
                return true;
        }
        return false;
    };
    for (int i = 0; i < n; i++)
    {
        long x = circles[i][0], y = circles[i][1], r = circles[i][2];
        if ((inCircle(x, y, r, 0, 0)
        ||  inCircle(x, y, r, xCorner, yCorner)
        || (((x <= xCorner) && (y + r >= yCorner)) || ((y <= yCorner) && (x - r <= 0))))
        && !visited[i] && dfs(dfs, i))
            return false;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(int xCorner,
          int yCorner,
          std::vector<std::vector<int> > circles,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canReachCorner(xCorner, yCorner, circles);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 4, {{2, 1, 1}}, true, trials);
    test(3, 3, {{1, 1, 2}}, false, trials);
    test(3, 3, {{2, 1, 1}, {1, 2, 1}}, false, trials);
    test(4, 4, {{5, 5, 1}}, true, trials);
    test(3, 3, {{2, 1000, 997}, {1000, 2, 997}}, true, trials);
    test(15, 15, {{1, 99, 85}, {99, 1, 85}}, true, trials);
    return 0;
}


