#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumDeleteSum(std::string s1, std::string s2)
{
    const int m = static_cast<int>(s1.length()), n = static_cast<int>(s2.length());
    std::vector<std::vector<int>> dp(m + 1, std::vector<int>(n + 1));
    
    for (int i = 1; i <= m; ++i) dp[i][0] = dp[i - 1][0] + s1[i - 1];
    for (int j = 1; j <= n; ++j) dp[0][j] = dp[0][j - 1] + s2[j - 1];
    for (int i = 1; i <= m; ++i)
        for (int j = 1; j <= n; ++j)
            dp[i][j] = (s1[i - 1] == s2[j - 1])
                     ?dp[i - 1][j - 1]
                     :std::min(dp[i - 1][j] + s1[i - 1],
                               dp[i][j - 1] + s2[j - 1]);
    return dp[m][n];
}

// ############################################################################
// ############################################################################

void test(std::string s1, std::string s2, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumDeleteSum(s1, s2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("sea", "eat", 231, trials);
    test("delete", "leet", 403, trials);
    return 0;
}


