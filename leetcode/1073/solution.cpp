#include "../common/common.hpp"
#include <deque>

// ############################################################################
// ############################################################################

std::vector<int> addNegabinary(std::vector<int> arr1, std::vector<int> arr2)
{
    std::deque<int> result;
    int i = static_cast<int>(arr1.size()) - 1,
        j = static_cast<int>(arr2.size()) - 1,
        carry = 0;
    while (carry || i >= 0 || j >= 0)
    {
        if (i >= 0) carry += arr1[i--];
        if (j >= 0) carry += arr2[j--];
        result.push_front(carry & 1);
        carry = -(carry >> 1);
    }
    while ((result.size() > 1) && (result.front() == 0))
        result.pop_front();
    return {result.begin(), result.end()};
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr1,
          std::vector<int> arr2,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = addNegabinary(arr1, arr2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 1, 1, 1}, {1, 0, 1}, {1, 0, 0, 0, 0}, trials);
    test({0}, {0}, {0}, trials);
    test({0}, {1}, {1}, trials);
    return 0;
}


