#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string maskPII(std::string s)
{
    if (size_t position = s.find('@'); position != std::string::npos)
    {
        std::string address = std::string(1, s.front())
                            + std::string("*****")
                            + std::string(1, s[position - 1])
                            + s.substr(position);
        for (char &letter : address)
            if (std::isupper(letter))
                letter = static_cast<char>(std::tolower(letter));
        return address;
    }
    else
    {
        const std::string mask[] = {"***-***-",
                                    "+*-***-***-",
                                    "+**-***-***-",
                                    "+***-***-***-"};
        std::string digits;
        for (char symbol : s)
            if (std::isdigit(symbol))
                digits.push_back(symbol);
        return mask[digits.size() - 10] + digits.substr(digits.size() - 4);
    }
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maskPII(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("LeetCode@LeetCode.com", "l*****e@leetcode.com", trials);
    test("AB@qq.com", "a*****b@qq.com", trials);
    test("1(234)567-890", "***-***-7890", trials);
    return 0;
}


