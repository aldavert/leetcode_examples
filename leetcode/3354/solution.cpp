#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int countValidSelections(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = 0, prefix = 0, suffix = std::accumulate(nums.begin(), nums.end(), 0);
    for (int i = 0; i < n; ++i)
    {
        suffix -= nums[i];
        prefix += nums[i];
        if (nums[i] > 0) continue;
        if (prefix == suffix) result += 2;
        if (std::abs(prefix - suffix) == 1) result += 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countValidSelections(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 2, 0, 3}, 2, trials);
    test({2, 3, 4, 0, 4, 1, 0}, 0, trials);
    return 0;
}


