#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isAdditiveNumber(std::string num)
{
    const int n = static_cast<int>(num.size());
    auto recursive =
        [&](auto &&self, long first_num, long second_num, long idx) -> bool
    {
        if (idx == n) return true;

        const long third_num = first_num + second_num;
        const std::string &third_txt = std::to_string(third_num);
        return (num.find(third_txt, idx) == static_cast<size_t>(idx))
            && self(self, second_num, third_num, idx + third_txt.size());

    };
    for (int i = 0; i < n / 2; ++i)
    {
        if ((i > 0) && (num[0] == '0')) return false;
        long first_num = std::stol(num.substr(0, i + 1));
        for (int j = i + 1; std::max(i, j - i) < n - j; ++j)
        {
            if ((j > i + 1) && (num[i + 1] == '0')) break;
            long second_num = std::stol(num.substr(i + 1, j - i));
            if (recursive(recursive, first_num, second_num, j + 1))
                return true;
        }
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::string num, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isAdditiveNumber(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("112358", true, trials);
    test("199100199", true, trials);
    return 0;
}


