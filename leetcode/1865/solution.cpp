#include "../common/common.hpp"
#include <unordered_map>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class FindSumPairs
{
    std::vector<int> m_nums1;
    std::vector<int> m_nums2;
    std::unordered_map<int, int> m_count2;
public:
    FindSumPairs(std::vector<int> &nums1, std::vector<int> &nums2) :
        m_nums1(nums1), m_nums2(nums2)
    {
        for (int number : nums2) ++m_count2[number];
    }
    void add(int index, int val)
    {
        --m_count2[m_nums2[index]];
        m_nums2[index] += val;
        ++m_count2[m_nums2[index]];
    }
    int count(int tot)
    {
        int result = 0;
        for (int number : m_nums1)
            if (auto it = m_count2.find(tot - number); it != m_count2.end())
                result += it->second;
        return result;
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          std::vector<std::vector<int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        FindSumPairs obj(nums1, nums2);
        result.clear();
        for (auto params : input)
        {
            if (params.size() == 1)
                result.push_back(obj.count(params[0]));
            else
            {
                result.push_back(null);
                obj.add(params[0], params[1]);
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2, 2, 2, 3}, {1, 4, 5, 2, 5, 4},
         {{7}, {3, 2}, {8}, {4}, {0, 1}, {1, 1}, {7}},
         {8, null, 2, 1, null, null, 11}, trials);
    return 0;
}


