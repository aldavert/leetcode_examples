#include "../common/common.hpp"

#if 1
int minDistance(std::string word1, std::string word2)
{
    auto LCS = [](auto &&self, const std::string &w1, const std::string &w2) -> int
    {
        const size_t n = w1.size(),
                     m = w2.size();
        if (m > n) return self(self, w2, w1);
        int dp[501] = {};
        for (unsigned int i = 1; i <= n; ++i)
        {
            int previous = 0;
            for (unsigned int j = 1; j <= m; ++j)
            {
                int backup = dp[j];
                if (w1[i - 1] == w2[j - 1])
                     dp[j] = previous + 1;
                else dp[j] = std::max(dp[j], dp[j - 1]);
                previous = backup;
            }
        }
        return dp[m];
    };
    return static_cast<int>(word1.size())
         + static_cast<int>(word2.size())
         - 2 * LCS(LCS, word1, word2);
}
#elif 0
int minDistance(std::string word1, std::string word2)
{
    const int m = static_cast<int>(word1.size()),
              n = static_cast<int>(word2.size());
    char word1l[501] = {}, word2l[501] = {};
    // Check vocabulary size (only English lowercase).
    bool available_characters1[128] = { false };
    bool available_characters2[128] = { false };
    for (int i = 0; i < m; ++i)
        available_characters1[static_cast<int>(word1[i])] = true;
    for (int i = 0; i < n; ++i)
        available_characters2[static_cast<int>(word2[i])] = true;
    for (int i = 'a'; i <= 'z'; ++i)
        if (!(available_characters1[i] && available_characters2[i]))
            available_characters1[i] = false;
    // Reduce the words to the available items only.
    unsigned int rm = 0, rn = 0;
    for (int i = 0; i < m; ++i)
        if (available_characters1[static_cast<int>(word1[i])])
            word1l[rm++] = word1[i];
    for (int i = 0; i < n; ++i)
        if (available_characters1[static_cast<int>(word2[i])])
            word2l[rn++] = word2[i];
    // Compute the longest common sub-sequence.
    int dp[502][502] = {};
    for (unsigned int i = 0; i <= rm; ++i)
    {
        for (unsigned int j = 0; j <= rn; ++j)
        {
            if ((i == 0) || (j == 0))
                dp[i][j] = 0;
            else if (word1l[i - 1] == word2l[j - 1])
                dp[i][j] = 1 + dp[i - 1][j - 1];
            else dp[i][j] = std::max(dp[i - 1][j], dp[i][j - 1]);
        }
    }
    return static_cast<int>(m + n - 2 * dp[rm][rn]);
}
#else
// Hunt-Szymanski algorithm
int minDistance(std::string word1, std::string word2)
{
    const int m = static_cast<int>(word1.size()),
              n = static_cast<int>(word2.size());
    // Make sure that the longest string is the first string.
    if (n > m) return minDistance(word2, word1);
    // Check vocabulary size (only English lowercase).
    bool available_characters1[26] = {}, available_characters2[26] = {};
    int character_index[26] = {};
    int vocabulary_size = 0;
    for (int i = 0; i < m; ++i)
        available_characters1[word1[i] - 'a'] = true;
    for (int i = 0; i < n; ++i)
        available_characters2[word2[i] - 'a'] = true;
    for (int i = 0; i < 26; ++i)
    {
        if (available_characters1[i] && available_characters2[i])
        {
            character_index[i] = vocabulary_size;
            ++vocabulary_size;
        }
        else character_index[i] = -1;
    }
    
    std::vector<std::vector<short> > matchlist(vocabulary_size);
    std::vector<int> l_value(n + 1, 0);
    for (int i = 0; i < vocabulary_size; ++i)
        matchlist[i].resize(n + 2);
    for (int i = 0; i < m; ++i)
    {
        const int idx = character_index[word1[i] - 'a'];
        if ((idx >= 0) && (matchlist[idx][0] == 0))
        {
            matchlist[idx][0] = 1;
            int k = 1;
            for (int j = n; j > 0; --j)
                if (word1[i] == word2[j - 1])
                    matchlist[idx][k++] = static_cast<short>(j);
            matchlist[idx][k] = -1;
        }
    }
    unsigned int lcs = 0;
    for (int i = 0; i < m; ++i)
    {
        const int idx = character_index[word1[i] - 'a'];
        if (idx >= 0)
        {
            for (unsigned int j = 1; matchlist[idx][j] != -1; ++j)
            {
                const int value = matchlist[idx][j];
                if (value > l_value[lcs]) l_value[++lcs] = value;
                else
                {
                    int high = lcs;
                    int low = 0;
                    int k = 0;
                    int mid;
                    while (true)
                    {
                        mid = low + ((high - low) / 2);
                        if (l_value[mid] == value)
                        {
                            k = 1;
                            break;
                        }
                        if (high - low <= 1)
                        {
                            mid = high;
                            break;
                        }
                        if (l_value[mid] > value) high = mid;
                        else if (l_value[mid] < value) low = mid;
                    }
                    if (k == 0) l_value[mid] = value;
                }
            }
        }
    }
    return n + m - 2 * lcs;
}
#endif

void test(const std::string &word1,
          const std::string &word2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDistance(word1, word2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("sea", "eat", 2, trials);
    test("leetcode", "etco", 4, trials);
    test("etco", "leetcode", 4, trials);
    test("banana", "atana", 3, trials);
    test("atana", "banana", 3, trials);
    return 0;
}


