#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int earliestFullBloom(std::vector<int> plantTime, std::vector<int> growTime)
{
    struct Seed
    {
        int plant;
        int grow;
        bool operator<(const Seed &other) const { return grow > other.grow; }
    };
    int result = 0, time = 0;
    std::vector<Seed> seeds;
    
    for (size_t i = 0, n = plantTime.size(); i < n; ++i)
      seeds.push_back({plantTime[i], growTime[i]});
    std::sort(seeds.begin(), seeds.end());
    for (const auto& [plant, grow] : seeds)
        result = std::max(result, (time += plant) + grow);
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> plantTime,
          std::vector<int> growTime,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = earliestFullBloom(plantTime, growTime);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 3}, {2, 3, 1}, 9, trials);
    test({1, 2, 3, 2}, {2, 1, 2, 1}, 9, trials);
    test({1}, {1}, 2, trials);
    return 0;
}



