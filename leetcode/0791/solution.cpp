#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
std::string customSortString(std::string order, std::string s)
{
    std::string result(s.size(), ' ');
    int lut[128] = {};
    short histogram[26] = {};
    for (int i = 0; i < 128; ++i) lut[i] = -1;
    int idx = 0;
    for (size_t i = 0; i < order.size(); ++i)
        lut[static_cast<int>(order[i])] = idx++;
    for (size_t i = 0; i < 128; ++i)
        if (lut[i] == -1) lut[i] = idx;
    for (size_t i = 0; i < s.size(); ++i)
        ++histogram[lut[static_cast<int>(s[i])]];
    for (int i = 1; i <= idx; ++i)
        histogram[i] += histogram[i - 1];
    for (auto begin = s.rbegin(), end = s.rend(); begin != end; ++begin)
        result[--histogram[lut[static_cast<int>(*begin)]]] = *begin;
    return result;
}
#else
std::string customSortString(std::string order, std::string s)
{
    std::string result(s.size(), ' ');
    std::unordered_map<char, int> lut;
    short histogram[26] = {};
    int idx = 0;
    for (char c : order)
        lut[c] = idx++;
    for (char c : s)
    {
        if (auto it = lut.find(c); it != lut.end())
            ++histogram[it->second];
        else ++histogram[idx];
    }
    for (int i = 1; i <= idx; ++i)
        histogram[i] += histogram[i - 1];
    for (auto begin = s.rbegin(), end = s.rend(); begin != end; ++begin)
    {
        char c = *begin;
        if (auto it = lut.find(c); it != lut.end())
            result[--histogram[it->second]] = c;
        else result[--histogram[idx]] = c;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string order,
          std::string s,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = customSortString(order, s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("cba", "abcd", "cbad", trials);
    test("cba", "adbcd", "cbadd", trials);
    test("cba", "axbcd", "cbaxd", trials);
    return 0;
}


