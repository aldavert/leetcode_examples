#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxConsecutive(int bottom, int top, std::vector<int> special)
{
    int result = 0;
    std::sort(special.begin(), special.end());
    for (int i = 1, n = static_cast<int>(special.size()); i < n; ++i)
        result = std::max(result, special[i] - special[i - 1] - 1);
    return std::max({result, special.front() - bottom, top - special.back()});
}

// ############################################################################
// ############################################################################

void test(int bottom,
          int top,
          std::vector<int> special,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxConsecutive(bottom, top, special);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 9, {4, 6}, 3, trials);
    test(6, 8, {7, 6, 8}, 0, trials);
    return 0;
}


