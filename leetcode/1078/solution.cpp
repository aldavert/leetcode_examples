#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

#if 1
std::vector<std::string> findOcurrences(std::string text,
                                        std::string first,
                                        std::string second)
{
    std::vector<std::string> result;
    std::string current, last = "", before_last = "";
    for (char c : text)
    {
        if (c == ' ')
        {
            if ((before_last == first) && (last == second))
                result.push_back(current);
            before_last = last;
            last = std::exchange(current, "");
        }
        else current += c;
    }
    if ((before_last == first) && (last == second))
        result.push_back(current);
    return result;
}
#else
std::vector<std::string> findOcurrences(std::string text,
                                        std::string first,
                                        std::string second)
{
    std::vector<std::string> result, words;
    std::string current;
    for (char c : text)
    {
        if (c == ' ')
            words.push_back(std::exchange(current, ""));
        else current += c;
    }
    words.push_back(current);
    const size_t n = words.size();
    for (size_t i = 2; i < n; ++i)
        if ((words[i - 2] == first) && (words[i - 1] == second))
            result.push_back(words[i]);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string text,
          std::string first,
          std::string second,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findOcurrences(text, first, second);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("alice is a good girl she is a good student",
         "a", "good", {"girl", "student"}, trials);
    test("we will we will rock you",
         "we", "will", {"we", "rock"}, trials);
    test("we we we will we will rock you",
         "we", "will", {"we", "rock"}, trials);
    test("we we will we will rock you",
         "we", "will", {"we", "rock"}, trials);
    test("we we we we will rock you",
         "we", "we", {"we", "we", "will"}, trials);
    return 0;
}


