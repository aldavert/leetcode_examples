#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<int> maximizeXor(std::vector<int> nums,
                             std::vector<std::vector<int> > queries)
{
    const int q = static_cast<int>(queries.size());
    std::vector<int> result(q, -1);
    std::sort(nums.begin(), nums.end());

    for (int i = 0; i < q; ++i)
    {
        int x = queries[i][0], m = queries[i][1];
        if (m < nums[0]) continue;

        int end = static_cast<int>(std::upper_bound(nums.begin(), nums.end(), m)
                                 - nums.begin());
        int start = 0, k = 0, cur = 0;
        for (int bit = 31; bit >= 0; --bit)
        {
            if (x & (1 << bit))
            {
                if (!(nums[start] & (1 << bit)))
                {
                    k |= 1 << bit;
                    end = static_cast<int>(std::lower_bound(nums.begin() + start,
                                                            nums.begin() + end,
                                                            cur | (1 << bit))
                                         - nums.begin());
                }
                else cur |= 1 << bit;
            }
            else
            {
                if (start <= end - 1 && (nums[end - 1] & (1 << bit)))
                {
                    k |= 1 << bit;
                    cur |= 1 << bit;
                    start = static_cast<int>(std::lower_bound(nums.begin() + start,
                                                              nums.begin() + end,
                                                              cur)
                                           - nums.begin());
                }
            }
        }
        result[i] = k;
    }
    return result;
}
#else
std::vector<int> maximizeXor(std::vector<int> nums,
                             std::vector<std::vector<int> > queries)
{
    struct TrieBinary
    {
    public:
        TrieBinary(void) = default;
        ~TrieBinary(void) { delete children[0]; delete children[1]; }
        void insert(int num)
        {
            TrieBinary * node = this;
            for (int i = 31; i >= 0; --i)
            {
                int bit = (num >> i) & 1;
                if (!node->children[bit])
                    node->children[bit] = new TrieBinary();
                node = node->children[bit];
            }
        }
        int query(int num)
        {
            TrieBinary * node = this;
            int sum = 0;
            for (int i = 31; i >= 0; --i)
            {
                if (!node) return -1;
                int bit = (num >> i) & 1;
                if (node->children[1 - bit])
                {
                    sum |= (1 << i);
                    node = node->children[1 - bit];
                }
                else node = node->children[bit];
            }
            return sum;
        }
    protected:
        TrieBinary * children[2] = {nullptr, nullptr};
    };
    const int n_nums = static_cast<int>(nums.size());
    const int n_queries = static_cast<int>(queries.size());
    
    std::sort(nums.begin(), nums.end());    
    for (int i = 0; i < n_queries; ++i) queries[i].push_back(i);
    std::sort(queries.begin(), queries.end(),
              [](const auto& q1, const auto& q2) { return q1[1] < q2[1]; });
    
    TrieBinary root;    
    std::vector<int> result(n_queries);
    for (int i = 0; const auto &q : queries)
    {
      while ((i < n_nums) && (nums[i] <= q[1])) root.insert(nums[i++]);
      result[q[2]] = root.query(q[0]);
    }  
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximizeXor(nums, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 2, 3, 4}, {{3, 1}, {1, 3}, {5, 6}}, {3, 3, 7}, trials);
    test({5, 2, 4, 6, 6, 3}, {{12, 4}, {8, 1}, {6, 3}}, {15, -1, 5}, trials);
    return 0;
}


