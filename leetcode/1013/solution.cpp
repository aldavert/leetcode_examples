#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
bool canThreePartsEqualSum(std::vector<int> arr)
{
    int sum = std::accumulate(arr.begin(), arr.end(), 0);
    if (sum % 3 != 0) return false;
    int parts = 1;
    for (int accum = 0; int value : arr)
        if ((accum += value) == sum * parts / 3)
            ++parts;
    return parts > 3;
}
#else
bool canThreePartsEqualSum(std::vector<int> arr)
{
    const size_t n = arr.size();
    
    int left = 0;
    for (size_t i = 0; i < n - 2; ++i)
        left += arr[i];
    if ((left + arr[n - 2] + arr[n - 1]) % 3 != 0) return false;
    int middle = arr[n - 2];
    if ((left == middle) && (left == arr[n - 1])) return true;
    
    for (size_t i = n - 3; i > 0; --i)
    {
        left -= arr[i];
        middle += arr[i];
        if ((left == middle) && (left == arr[n - 1])) return true;
        int middle_inner = middle, last = arr[n - 1];
        for (size_t j = n - 2; j > i; --j)
        {
            middle_inner -= arr[j];
            last += arr[j];
            if ((left == middle_inner) && (left == last)) return true;
        }
    }
    return false;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canThreePartsEqualSum(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 2, 1, -6, 6, -7, 9, 1, 2, 0, 1}, true, trials);
    test({0, 2, 1, -6, 6, 7, 9, -1, 2, 0, 1}, false, trials);
    test({3, 3, 6, 5, -2, 2, 5, 1, -9, 4}, true, trials);
    test({1, -1, 1, -1}, false, trials);
    return 0;
}


