#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canChoose(std::vector<std::vector<int> > groups, std::vector<int> nums)
{
    const size_t m = groups.size(), n = nums.size();
    auto isMatch = [&](size_t i, size_t j)
    {
        if (j + groups[i].size() > nums.size())
            return false;
        for (size_t k = 0; k < groups[i].size(); ++k)
            if (groups[i][k] != nums[j + k])
                return false;
        return true;
    };
    
    size_t i = 0, j = 0;
    while ((i < m) && (j < n))
    {
        if (isMatch(i, j))
            j += groups[i++].size();
        else ++j;
    }
    return i == groups.size();
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > groups,
          std::vector<int> nums,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canChoose(groups, nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, -1, -1}, {3, -2, 0}}, {1, -1, 0, 1, -1, -1, 3, -2, 0}, true, trials);
    test({{10, -2}, {1, 2, 3, 4}}, {1, 2, 3, 4, 10, -2}, false, trials);
    test({{1, 2, 3}, {3, 4}}, {7, 7, 1, 2, 3, 4, 7, 7}, false, trials);
    return 0;
}


