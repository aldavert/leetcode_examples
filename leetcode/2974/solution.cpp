#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> numberGame(std::vector<int> nums)
{
    std::vector<int> result = nums;
    std::sort(result.begin(), result.end());
    for (size_t i = 0; i < nums.size(); i += 2)
        std::swap(result[i], result[i + 1]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberGame(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 2, 3}, {3, 2, 5, 4}, trials);
    test({2, 5}, {5, 2}, trials);
    return 0;
}


