#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool isIdealPermutation(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int local = 0, global = 0, out_of_order_pool = 0;
    for (int i = 0; i < n - 1; ++i)
    {
        local += nums[i] > nums[i + 1];
        out_of_order_pool = std::max(0, out_of_order_pool
                                      + std::max(0, nums[i] - i)
                                      - (nums[i] <= i));
        global += out_of_order_pool;
    }
    return local == global;
}
#else
bool isIdealPermutation(std::vector<int> nums)
{
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        if (std::abs(nums[i] - i) > 1) return false;
    return true;
}
#endif

// ############################################################################
// ############################################################################

std::tuple<int, int> checkGlobalLocal(std::vector<int> nums)
{
    int local = 0, global = 0;
    for (int i = 0, n = static_cast<int>(nums.size() - 1); i < n; ++i)
    {
        local += nums[i] > nums[i + 1];
        for (int j = i + 1; j <= n; ++j)
            global += nums[i] > nums[j];
    }
    return {local, global};
}

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isIdealPermutation(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
#if 1
    test({1, 0, 2},  true, trials);
    test({1, 2, 0}, false, trials);
    constexpr int test_size = 10;
    std::vector<int> numbers(test_size);
    for (int i = 0; i < test_size; ++i)
        numbers[i] = i;
    int number_of_failures = 0, number_of_permutations = 0, error = 0;
    do
    {
        auto [local, global] = checkGlobalLocal(numbers);
        if (isIdealPermutation(numbers) != (local == global))
        {
            std::cout << "Failed for " << numbers << " expected "
                      << (local == global) << " but obtained "
                      << (local != global) << " because local="
                      << local << " and global=" << global << '\n';
            ++error;
            if (++number_of_failures >= 10) break;
        }
        ++number_of_permutations;
    } while (std::next_permutation(numbers.begin(), numbers.end()));
    if (error == 0)
        std::cout << showResult(true) << " Obtained the expected result for all "
                  << "the '" << number_of_permutations << "' permutations of a " 
                  << test_size << " elements vector.\n";
#else
    test({1, 0, 3, 2}, true, trials);
#endif
    return 0;
}


