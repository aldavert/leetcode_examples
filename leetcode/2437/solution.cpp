#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countTime(std::string time)
{
    int result = 1;
    if      ((time[0] == '?') && (time[1] == '?')) result *= 24;
    else if (time[0] == '?') result *= 2 + (time[1] < '4');
    else if (time[1] == '?') result *= 4 + 6 * (time[0] < '2');
    if (time[3] == '?') result *= 6;
    if (time[4] == '?') result *= 10;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string time, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countTime(time);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("?5:00", 2, trials);
    test("0?:0?", 100, trials);
    test("??:??", 1440, trials);
    return 0;
}


