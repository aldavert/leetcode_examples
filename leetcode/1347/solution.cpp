#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minSteps(std::string s, std::string t)
{
    int histogram[26] = {};
    for (char letter : s) ++histogram[letter - 'a'];
    for (char letter : t) --histogram[letter - 'a'];
    int result = 0;
    for (int i = 0; i < 26; ++i)
        result += std::abs(histogram[i]);
    return result / 2;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string t, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSteps(s, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("bab", "aba", 1, trials);
    test("leetcode", "practice", 5, trials);
    test("anagram", "mangaar", 0, trials);
    return 0;
}


