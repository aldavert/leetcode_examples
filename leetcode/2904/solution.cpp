#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string shortestBeautifulSubstring(std::string s, int k)
{
    int best_left = -1, min_length = static_cast<int>(s.size() + 1);
    
    for (int l = 0, r = 0, ones = 0, n = static_cast<int>(s.size()); r < n; ++r)
    {
        if (s[r] == '1') ++ones;
        while (ones == k)
        {
            if (r - l + 1 < min_length)
            {
                best_left = l;
                min_length = r - l + 1;
            }
            else if ((r - l + 1 == min_length)
            &&       (s.compare(l, min_length, s, best_left, min_length) < 0))
                best_left = l;
            ones -= (s[l++] == '1');
        }
    }
    return (best_left == -1)?"": s.substr(best_left, min_length);
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestBeautifulSubstring(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("100011001", 3, "11001", trials);
    test("1011", 2, "11", trials);
    return 0;
}


