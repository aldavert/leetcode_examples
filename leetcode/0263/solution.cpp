#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isUgly(int n)
{
    if (n <= 0) return false;
    while ((n > 1) && (n % 2 == 0)) n /= 2;
    while ((n > 1) && (n % 3 == 0)) n /= 3;
    while ((n > 1) && (n % 5 == 0)) n /= 5;
    return n == 1;
}

// ############################################################################
// ############################################################################

void test(int n, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isUgly(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, true, trials);
    test(1, true, trials);
    test(14, false, trials);
    test(-2147483648, false, trials);
    return 0;
}


