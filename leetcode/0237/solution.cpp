#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

void deleteNode(ListNode * node)
{
    node->val = node->next->val;
    ListNode * aux = node->next;
    node->next = aux->next;
    aux->next = nullptr;
    delete aux;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> input,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(input);
        ListNode * selected = head;
        while (selected->val != k) selected = selected->next;
        deleteNode(selected);
        result = list2vec(head);
        delete head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 5, 1, 9}, 5, {4, 1, 9}, trials);
    test({4, 5, 1, 9}, 1, {4, 5, 9}, trials);
    return 0;
}

