#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int numberOfArrays(std::vector<int> differences, int lower, int upper)
{
    long min = 0, max = 0, previous = 0;
    for (size_t i = 0; i < differences.size(); ++i)
    {
        previous += differences[i];
        max = std::max(max, previous);
        min = std::min(min, previous);
    }
    return static_cast<int>(std::max(0L, (upper - lower) - (max - min) + 1));
}
#else
int numberOfArrays(std::vector<int> differences, int lower, int upper)
{
    std::vector<long> prefix(differences.size() + 1);
    for (size_t i = 0; i < differences.size(); ++i)
        prefix[i + 1] += prefix[i] + differences[i];
    long max = *std::max_element(prefix.begin(), prefix.end()),
         min = *std::min_element(prefix.begin(), prefix.end());
    return static_cast<int>(std::max(0L, (upper - lower) - (max - min) + 1));
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> differences,
          int lower,
          int upper,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfArrays(differences, lower, upper);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, -3, 4}, 1, 6, 2, trials);
    test({3, -4, 5, 1, -2}, -4, 5, 4, trials);
    test({4, -7, 2}, 3, 6, 0, trials);
    return 0;
}


