#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int findMinimumTime(std::vector<int> strength, int k)
{
    const int n = static_cast<int>(strength.size()),
              mask_stop = (1 << n) - 1;
    auto search = [&](auto &&self, int x, int mask) -> int
    {
        if (mask == mask_stop) return 0;
        int result = std::numeric_limits<int>::max();
        for (int i = 0; i < n; ++i)
            if ((mask >> i & 1) == 0)
                result = std::min(result, (strength[i] - 1) / x + 1
                                        + self(self, x + k, mask | 1 << i));
        return result;
    };
    return search(search, 1, 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> strength, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMinimumTime(strength, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 4, 1}, 1, 4, trials);
    test({2, 5, 4}, 2, 5, trials);
    return 0;
}


