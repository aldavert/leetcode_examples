#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int orangesRotting(std::vector<std::vector<int> > grid)
{
    const int n = static_cast<int>(grid.size());
    const int m = static_cast<int>(grid[0].size());
    unsigned int fresh = 0;
    std::queue<std::pair<char, char> > active;
    for (int x = 0; x < n; ++x)
    {
        for (int y = 0; y < m; ++y)
        {
            if (grid[x][y] == 2)
                active.push({x, y});
            else if (grid[x][y] == 1) ++fresh;
        }
    }
    auto update = [&](int x, int y)
    {
        if ((x >= 0) && (y >= 0) && (x < n) && (y < m) && (grid[x][y] == 1))
        {
            active.push({x, y});
            grid[x][y] = 2;
            --fresh;
        }
    };
    int time = 0;
    while (fresh && active.size())
    {
        const int ne = static_cast<int>(active.size());
        for (int i = 0; i < ne; ++i)
        {
            auto [x, y] = active.front();
            active.pop();
            update(x + 1, y);
            update(x - 1, y);
            update(x, y + 1);
            update(x, y - 1);
        }
        ++time;
    }
    return (fresh > 0)?-1:time;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = orangesRotting(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 1, 1}, {1, 1, 0}, {0, 1, 1}}, 4, trials);
    test({{2, 1, 1}, {0, 1, 1}, {1, 0, 1}}, -1, trials);
    test({{0, 2}}, 0, trials);
    return 0;
}


