#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<bool> getResults(std::vector<std::vector<int> > queries)
{
    class FenwickTree
    {
        std::vector<int> vals;
        inline int lowbit(int i) const { return i & -i; }
    public:
        FenwickTree(int n) : vals(n + 1) {}
        void maximize(int i, int val)
        {
            for (int n = static_cast<int>(vals.size()); i < n; i += lowbit(i))
                vals[i] = std::max(vals[i], val);
        }
        int get(int i) const
        {
            int result = 0;
            for (; i > 0; i -= lowbit(i))
                result = std::max(result, vals[i]);
            return result;
        }
    };
    const int n = std::min(50'000, static_cast<int>(queries.size()) * 3);
    std::vector<bool> result;
    FenwickTree tree(n + 1);
    std::set<int> obstacles{0, n};
    
    for (const auto &query : queries)
        if (query[0] == 1)
            obstacles.insert(query[1]);
    for (auto it = obstacles.begin(); std::next(it) != obstacles.end(); ++it)
        tree.maximize(*std::next(it), *std::next(it) - *it);
    for (int i = static_cast<int>(queries.size()) - 1; i >= 0; --i)
    {
        if (queries[i][0] == 1)
        {
            const auto it = obstacles.find(queries[i][1]);
            if (next(it) != obstacles.end())
                tree.maximize(*next(it), *next(it) - *prev(it));
            obstacles.erase(it);
        }
        else
        {
            const int prev = *std::prev(obstacles.upper_bound(queries[i][1]));
            result.push_back((tree.get(prev) >= queries[i][2])
                          || (queries[i][1] - prev >= queries[i][2]));
        }
    }
    std::reverse(result.begin(), result.end());
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > queries,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getResults(queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 3, 3}, {2, 3, 1}, {2, 2, 2}},
         {false, true, true}, trials);
    test({{1, 7}, {2, 7, 6}, {1, 2}, {2, 7, 5}, {2, 7, 6}},
         {true, true, false}, trials);
    return 0;
}


