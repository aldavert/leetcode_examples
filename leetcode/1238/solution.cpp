#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> circularPermutation(int n, int start)
{
    std::vector<int> result;
    
    for (int i = 0; i < (1 << n); ++i)
        result.push_back(start ^ i ^ i >> 1);
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int start, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = circularPermutation(n, start);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 3, {3, 2, 0, 1}, trials);
    test(3, 2, {2, 3, 1, 0, 4, 5, 7, 6}, trials);
    return 0;
}


