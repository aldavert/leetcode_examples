#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<long long> maximumSegmentSum(std::vector<int> nums,
                                         std::vector<int> removeQueries)
{
    const int n = static_cast<int>(nums.size());
    long long max_sum = 0;
    std::vector<long long> result(n), sum(n);
    std::vector<int> count(n);
    for (int i = n - 1; i >= 0; --i)
    {
        result[i] = max_sum;
        int j = removeQueries[i];
        long long left_sum = j > 0 ? sum[j - 1] : 0,
                  right_sum = j + 1 < n ? sum[j + 1] : 0,
                  segment_sum = nums[j] + left_sum + right_sum;
        int left_count = j > 0 ? count[j - 1] : 0,
            right_count = j + 1 < n ? count[j + 1] : 0,
            segment_count = 1 + left_count + right_count;
        int l = j - left_count,
            r = j + right_count;
        sum[l] = sum[r] = segment_sum;
        count[l] = count[r] = segment_count;
        max_sum = std::max(max_sum, segment_sum);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> removeQueries,
          std::vector<long long> solution,
          unsigned int trials = 1)
{
    std::vector<long long> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumSegmentSum(nums, removeQueries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 5, 6, 1}, {0, 3, 2, 4, 1}, {14, 7, 2, 2, 0}, trials);
    test({3, 2, 11, 1}, {3, 2, 1, 0}, {16, 5, 3, 0}, trials);
    return 0;
}


