#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canPlaceFlowers(std::vector<int> flowerbed, int n)
{
    const int nf = static_cast<int>(flowerbed.size());
    if      (n  == 0) return true;
    else if (nf == 0) return false;
    else if (nf == 1) return (flowerbed[0] == 0) && (n == 1);
    else if (nf == 2) return ((flowerbed[0] + flowerbed[1]) == 0) && (n == 1);
    else if (n - 1 > nf / 2) return false;
    
    if ((flowerbed[0] + flowerbed[1]) == 0)
    {
        flowerbed[0] = 1;
        if (!(--n)) return true;
    }
    for (int i = 1; i < nf - 1; ++i)
    {
        if ((flowerbed[i - 1] + flowerbed[i] + flowerbed[i + 1]) == 0)
        {
            flowerbed[i] = 1;
            if (!(--n)) return true;
        }
    }
    if ((flowerbed[nf - 2] + flowerbed[nf - 1]) == 0)
        --n;
    return n == 0;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> flowerbed, int n, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canPlaceFlowers(flowerbed, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 0, 0, 1}, 1, true , trials);
    test({1, 0, 0, 0, 1}, 2, false, trials);
    test({1, 0, 0, 0, 0, 0, 1}, 2, true, trials);
    test({1, 0, 0, 0, 0, 0, 0, 1}, 2, true, trials);
    return 0;
}


