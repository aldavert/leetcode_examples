#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfPoints(std::vector<std::vector<int> > nums)
{
    std::vector<std::tuple<int, int> > points;
    for (const auto &numbers : nums)
        points.push_back({numbers[0], numbers[1]});
    std::sort(points.begin(), points.end());
    int result = 0;
    for (int previous = 0; auto [min, max] : points)
    {
        result += std::max(0, max - std::max(min, previous) + 1);
        previous = std::max(previous, max + 1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfPoints(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 6}, {1, 5}, {4, 7}}, 7, trials);
    test({{1, 3}, {5, 8}}, 7, trials);
    test({{4, 4}, {9, 10}, {9, 10}, {3, 8}}, 8, trials);
    test({{1, 2}, {3, 4}, {5, 6}, {7, 8}}, 8, trials);
    return 0;
}


