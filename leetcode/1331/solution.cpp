#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> arrayRankTransform(std::vector<int> arr)
{
    std::vector<std::tuple<int, size_t> > rank(arr.size());
    for (size_t i = 0; i < arr.size(); ++i)
        rank[i] = {arr[i], i};
    std::sort(rank.begin(), rank.end());
    for (int previous = -2'000'000'000, r = 0; auto [num, pos] : rank)
    {
        r += (previous != num);
        arr[pos] = r;
        previous = num;
    }
    return arr;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = arrayRankTransform(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({40, 10, 20, 30}, {4, 1, 2, 3}, trials);
    test({100, 100, 100}, {1, 1, 1}, trials);
    test({37, 12, 28, 9, 100, 56, 80, 5, 12}, {5, 3, 4, 2, 8, 6, 7, 1, 3}, trials);
    return 0;
}


