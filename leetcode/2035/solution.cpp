#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int minimumDifference(std::vector<int> nums)
{
    struct Info
    {
        Info(std::vector<int>::iterator begin, std::vector<int>::iterator end, int sz) :
            nums(begin, end),
            sums(sz + 1) {};
        std::vector<int> nums;
        std::vector<std::vector<int> > sums;
    };
    const int n = static_cast<int>(nums.size()) / 2,
              sum = std::accumulate(nums.begin(), nums.end(), 0),
              goal = sum / 2;
    Info left_side(nums.begin(), nums.begin() + n, n),
         right_side(nums.begin() + n, nums.end(), n);
    int result = std::numeric_limits<int>::max();
    auto dfs = [&](auto &&self, Info &vec, int i, int count, int path) -> void
    {
        if (i == static_cast<int>(vec.nums.size()))
            vec.sums[count].push_back(path);
        else
        {
            self(self, vec, i + 1, count + 1, path + vec.nums[i]);
            self(self, vec, i + 1, count, path);
        }
    };
    
    dfs(dfs, left_side, 0, 0, 0);
    dfs(dfs, right_side, 0, 0, 0);
    for (int left_count = 0; left_count <= n; ++left_count)
    {
        auto &left = left_side.sums[left_count];
        auto &right = right_side.sums[n - left_count];
        std::sort(right.begin(), right.end());
        for (int left_sum : left)
        {
            int i = static_cast<int>(std::lower_bound(right.begin(), right.end(),
                                                      goal - left_sum) - right.begin());
            if (i < static_cast<int>(right.size()))
            {
                const int sum_part_one = sum - left_sum - right[i];
                const int sum_part_two = sum - sum_part_one;
                result = std::min(result, std::abs(sum_part_one - sum_part_two));
            }
            if (i > 0)
            {
                const int sum_part_one = sum - left_sum - right[i - 1];
                const int sum_part_two = sum - sum_part_one;
                result = std::min(result, std::abs(sum_part_one - sum_part_two));
            }
        }
    }
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumDifference(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 9, 7, 3}, 2, trials);
    test({-36, 36}, 72, trials);
    test({2, -1, 0, 4, -2, -9}, 0, trials);
    return 0;
}


