#include "../common/common.hpp"
#include <queue>
#include <unordered_map>

// ############################################################################
// ############################################################################

int minOperations(int n, int m)
{
    constexpr int pow[] = { 1, 10, 100, 1'000, 10'000, 100'000, 1'000'000 };
    std::vector<bool> is_prime(10'001, true);
    is_prime[0] = is_prime[1] = false;
    for (int i = 2; i <= 10'000; ++i)
    {
        if (is_prime[i])
            for (int j = i * i; j <= 10'000; j += i)
                is_prime[j] = false;
    }
    struct Information
    {
        int cost;
        int value;
        std::string digit;
        bool operator<(const Information &other) const { return cost > other.cost; }
    };
    if (is_prime[n] || is_prime[m]) return -1;
    const std::string goal = std::to_string(m);
    std::priority_queue<Information> q;
    std::unordered_map<std::string, int> visited;
    q.push({n, n, std::to_string(n)});
    const int n_digits = static_cast<int>(q.top().digit.size());
    while (!q.empty())
    {
        auto [cost, value, digit] = q.top();
        q.pop();
        if (digit == goal)
            return cost;
        if (is_prime[value])
            continue;
        if (auto search = visited.find(digit);
            (search != visited.end()) && (search->second <= cost))
            continue;
        visited[digit] = cost;
        for (int i = 0; i < n_digits; ++i)
        {
            const int idx = n_digits - i - 1;
            if (digit[i] < '9')
            {
                ++digit[i];
                q.push({cost + value + pow[idx], value + pow[idx], digit});
                --digit[i];
            }
            if (((i == 0) && (digit[i] > '1')) || ((i != 0) && (digit[i] > '0')))
            {
                --digit[i];
                q.push({cost + value - pow[idx], value - pow[idx], digit});
                ++digit[i];
            }
        }
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(int n, int m, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(n, m);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, 12, 85, trials);
    test(4, 8, -1, trials);
    test(6, 2, -1, trials);
    test(5637, 2034, 34943, trials);
    return 0;
}


