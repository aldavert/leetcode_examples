#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int largestVariance(std::string s)
{
    auto kadane = [&s](char a, char b) -> int
    {
        int result = 0, count_a = 0, count_b = 0;
        bool can_extend_previous_b = false;
        
        for (char c : s)
        {
            if ((c != a) && (c != b)) continue;
            if (c == a) ++count_a;
            else ++count_b;
            
            if (count_b > 0)
                result = std::max(result, count_a - count_b);
            else if ((count_b == 0) && can_extend_previous_b)
                result = std::max(result, count_a - 1);
            if (count_b > count_a)
            {
                count_a = count_b = 0;
                can_extend_previous_b = true;
            }
        }
        return result;
    };
    int result = 0;
    
    for (char c1 = 'a'; c1 <= 'z'; ++c1)
        for (char c2 = 'a'; c2 <= 'z'; ++c2)
            if (c1 != c2)
                result = std::max(result, kadane(c1, c2));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestVariance(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aababbb", 3, trials);
    test("abcde", 0, trials);
    return 0;
}


