#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool search(std::vector<int> nums, int target)
{
    for (int begin = 0, end = static_cast<int>(nums.size()) - 1; begin <= end;)
    {
        int mid = (begin + end) / 2;
        if (nums[mid] == target) return true;
        if ((nums[begin] == nums[mid]) && (nums[mid] == nums[end]))
        {
            ++begin;
            --end;
        }
        else if (nums[begin] <= nums[mid])
        {
            if ((nums[begin] <= target) && (target <= nums[mid])) end = mid - 1;
            else begin = mid + 1;
        }
        else
        {
            if ((nums[mid] <= target) && (target <= nums[end])) begin = mid + 1;
            else end = mid - 1;
        }
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int target, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = search(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 5, 6, 0, 0, 1, 2}, 0,  true, trials);
    test({2, 5, 6, 0, 0, 1, 2}, 3, false, trials);
    return 0;
}


