# Find Bottom Left Tree Value

Given the `root` of a binary tree, return the leftmost value in the last row of the tree.

#### Example 1:
> ```mermaid 
> graph TD;
> A((2))---B((1))
> A---C((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [2, 1, 3]`  
> *Output:* `1`

#### Example 2:
> ```mermaid 
> graph TD;
> A((1))---B((2))
> A---C((3))
> B---D((4))
> B---EMPTY1(( ))
> C---E((5))
> C---F((6))
> E---G((7))
> E---EMPTY2(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF,stoke:#FFF,stroke-width:0px;
> class EMPTY1,EMPTY2 empty;
> linkStyle 3,7 stroke-width:0px
> ```
> *Input:* `root = [1, 2, 3, 4, null, 5, 6, null, null, 7]`
> *Output:* `7`

#### Constraints:
- The number of nodes in the tree is in the range `[1, 10^4]`.
- `-2^{31} <= Node.val <= 2^{31} - 1`


