#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int findBottomLeftValue(TreeNode * root)
{
    if (!root) return 0;
    std::queue<const TreeNode *> q;
    q.push(root);
    int result = root->val;
    while (!q.empty())
    {
        result = q.front()->val;
        for (size_t i = 0, n = q.size(); i < n; ++i)
        {
            auto node = q.front();
            q.pop();
            if (node->left) q.push(node->left);
            if (node->right) q.push(node->right);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = findBottomLeftValue(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3}, 1, trials);
    test({1, 2, 3, 4, null, 5, 6, null, null, 7}, 7, trials);
    return 0;
}


