#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

bool hasPathSum(TreeNode * root, int targetSum)
{
    if (!root) return false;
    targetSum -= root->val;
    if (!root->left && !root->right)
        return targetSum == 0;
    return hasPathSum(root->left, targetSum) || hasPathSum(root->right, targetSum);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int targetSum, bool solution, unsigned int trials = 1)
{
    bool result = false;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = hasPathSum(root, targetSum);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 8, 11, null, 13, 4, 7, 2, null, null, null, 1}, 22, trials);
    test({1, 2, 3}, 5, false, trials);
    test({}, 0, false, trials);
    return 0;
}


