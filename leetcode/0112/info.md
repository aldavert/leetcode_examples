# Path Sum

Given the `root` of a binary tree and an integer `targetSum`, return `true` if the tree has a **root-to-leaf** path such that adding up all the values along the path equals `targetSum`.

A **leaf** is a node with no children.
 
#### Example 1:
> ```mermaid 
> graph TD;
> A((5))---B((4))
> A---C((8))
> B---D((11))
> B---EMPTY1(( ))
> C---E((13))
> C---F((4))
> D---G((7))
> D---H((2))
> F---EMPTY2(( ))
> F---I((1))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#DDF,stroke:#44A,stroke-width:2px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class A,B,D,H selected;
> class EMPTY1,EMPTY2 empty;
> linkStyle 3,8 stroke-width:0px
> ```
> *Input:* `root = [5, 4, 8, 11, null, 13, 4, 7, 2, null, null, null, 1], targetSum = 22`  
> *Output:* `true`  
> *Explanation:* The root-to-leaf path with the target sum is shown.

#### Example 2:
> ```mermaid 
> graph TD;
> A((1))---B((2))
> A---C((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [1, 2, 3], targetSum = 5`  
> *Output:* `false`  
> *Explanation:* There two root-to-leaf paths in the tree: `(1 --> 2)`: The `sum` is `3`. `(1 --> 3)`: The sum is `4`. There is no root-to-leaf path with `sum = 5`.

#### Example 3:
> *Input:* `root = [], targetSum = 0`  
> *Output:* `false`  
> *Explanation:* Since the tree is empty, there are no root-to-leaf paths.

#### Constraints:
- The number of nodes in the tree is in the range `[0, 5000]`.
- `-1000 <= Node.val <= 1000`
- `-1000 <= targetSum <= 1000`


