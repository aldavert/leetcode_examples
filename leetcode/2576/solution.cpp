#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxNumOfMarkedIndices(std::vector<int> nums)
{
    std::sort(nums.begin(), nums.end());
    int i = 0;
    for (int n = static_cast<int>(nums.size()), j = n / 2; j < n; ++j)
        if ((2 * nums[i] <= nums[j]) && (++i == n / 2))
            break;
    return i * 2;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxNumOfMarkedIndices(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 5, 2, 4}, 2, trials);
    test({9, 2, 5, 4}, 4, trials);
    test({7, 6, 8}, 0, trials);
    return 0;
}


