#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

int arithmeticTriplets(std::vector<int> nums, int diff)
{
    std::bitset<201> present;
    for (size_t i = 0, n = nums.size(); i < n; ++i) present[nums[i]] = true;
    int result = 0;
    for (int i = 0, n = 200 - 2 * diff; i <= n; ++i)
        result += (present[i] && present[i + diff] && present[i + diff * 2]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int diff, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = arithmeticTriplets(nums, diff);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 4, 6, 7, 10}, 3, 2, trials);
    test({4, 5, 6, 7, 8, 9}, 2, 2, trials);
    return 0;
}


