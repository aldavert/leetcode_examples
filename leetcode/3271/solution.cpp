#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string stringHash(std::string s, int k)
{
    std::string result;
    for (int i = 0, n = static_cast<int>(s.size()); i < n; i += k)
    {
        int sum_hash = 0;
        for (int j = i; j < i + k; ++j)
            sum_hash += s[j] - 'a';
        result += static_cast<char>('a' + sum_hash % 26);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = stringHash(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcd", 2, "bf", trials);
    test("mxz", 3, "i", trials);
    return 0;
}


