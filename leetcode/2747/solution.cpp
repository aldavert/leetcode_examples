#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> countServers(int n,
                              std::vector<std::vector<int> > logs,
                              int x,
                              std::vector<int> queries)
{
    const int n_logs = static_cast<int>(logs.size());
    std::sort(logs.begin(), logs.end(),
              [](const auto& a, const auto& b) { return a[1] < b[1]; });
    const int m = static_cast<int>(queries.size());
    std::vector<std::pair<int, int> > query_index(m);
    for (int i = 0; i < m; ++i)
        query_index[i] = {queries[i], i};
    std::sort(query_index.begin(), query_index.end());
    std::unordered_map<int, int> count;
    std::vector<int> result(m);
    for (int j = 0, k = 0; auto& [r, i] : query_index)
    {
        int l = r - x;
        while ((k < n_logs) && (logs[k][1] <= r))
            ++count[logs[k++][0]];
        for (; (j < n_logs) && (logs[j][1] < l); ++j)
            if (--count[logs[j][0]] == 0)
                count.erase(logs[j][0]);
        result[i] = n - static_cast<int>(count.size());
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > logs,
          int x,
          std::vector<int> queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countServers(n, logs, x, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{1, 3}, {2, 6}, {1, 5}}, 5, {10, 11}, {1, 2}, trials);
    test(3, {{2, 4}, {2, 1}, {1, 2}, {3, 1}}, 2, {3, 4}, {0, 1}, trials);
    return 0;
}


