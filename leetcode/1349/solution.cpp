#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxStudents(std::vector<std::vector<char> > seats)
{
    const std::pair<int, int> directions[] = {{-1, -1}, {0, -1}, {1, -1},
                                              {-1,  1}, {0,  1}, {1,  1}};
    const int n_rows = static_cast<int>(seats.size());
    const int n_cols = static_cast<int>(seats[0].size());
    std::vector<std::vector<int> > seen(n_rows, std::vector<int>(n_cols)),
                                   match(n_rows, std::vector<int>(n_cols, -1));
    auto dfs = [&](auto &&self, int i, int j, int id) -> int
    {
        for (int d = 0; d < 6; ++d)
        {
            const int x = i + directions[d].first;
            const int y = j + directions[d].second;
            if ((x < 0) || (x == n_rows) || (y < 0) || (y == n_cols)) continue;
            if ((seats[x][y] != '.') || (seen[x][y] == id)) continue;
            seen[x][y] = id;
            if ((match[x][y] == -1)
            ||  self(self, match[x][y] / n_cols, match[x][y] % n_cols, id))
            {
                match[x][y] = i * n_cols + j;
                match[i][j] = x * n_cols + y;
                return 1;
            }
        }
        return 0;
    };
    int number_of_seats = 0, hungarian = 0;
    for (int i = 0; i < n_rows; ++i)
    {
        for (int j = 0; j < n_cols; ++j)
        {
            if (seats[i][j] == '.')
            {
                ++number_of_seats;
                if (match[i][j] == -1)
                    hungarian += dfs(dfs, i, j, seen[i][j] = i * n_cols + j);
            }
        }
    }
    return number_of_seats - hungarian;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > seats, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxStudents(seats);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'#', '.', '#', '#', '.', '#'},
          {'.', '#', '#', '#', '#', '.'},
          {'#', '.', '#', '#', '.', '#'}}, 4, trials);
    test({{'.', '#'},
          {'#', '#'},
          {'#', '.'},
          {'#', '#'},
          {'.', '#'}}, 3, trials);
    test({{'#', '.', '.', '.', '#'},
          {'.', '#', '.', '#', '.'},
          {'.', '.', '#', '.', '.'},
          {'.', '#', '.', '#', '.'},
          {'#', '.', '.', '.', '#'}}, 10, trials);
    return 0;
}


