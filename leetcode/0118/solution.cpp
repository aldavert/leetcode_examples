#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 0
std::vector<std::vector<int> > generate(int numRows)
{
    std::vector<std::vector<int> > result{{1}};
    for (int i = 1; i < numRows; ++i)
    {
        std::vector<int> current{{1}};
        auto &last = result.back();
        const int n = static_cast<int>(last.size());
        for (int j = 1; j < n; ++j)
            current.push_back(last[j - 1] + last[j]);
        current.push_back(1);
        result.push_back(std::move(current));
    }
    return result;
}    
#elif 1
std::vector<std::vector<int> > generate(int numRows)
{
    std::vector<std::vector<int> > result{{1}};
    for (int i = 1; i < numRows; ++i)
    {
        std::vector<int> current(i + 1);
        current[0] = 1;
        auto &last = result.back();
        const int n = static_cast<int>(last.size());
        for (int j = 1; j < n; ++j)
            current[j] = last[j - 1] + last[j];
        current[n] = 1;
        result.push_back(std::move(current));
    }
    return result;
}
#else
std::vector<std::vector<int> > generate(int numRows)
{
    std::vector<std::vector<int> > result{{1}};
    for (int i = 1; i < numRows; ++i)
    {
        std::vector<int> current;
        current.reserve(i + 1);
        current.push_back(1);
        auto &last = result.back();
        const int n = static_cast<int>(last.size());
        for (int j = 1; j < n; ++j)
            current.push_back(last[j - 1] + last[j]);
        current.push_back(1);
        result.push_back(std::move(current));
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int numRows,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = generate(numRows);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    const std::vector<std::vector<int> > solutions = {
        /* 1*/{1},
        /* 2*/{1, 1},
        /* 3*/{1, 2, 1},
        /* 4*/{1, 3, 3, 1},
        /* 5*/{1, 4, 6, 4, 1},
        /* 6*/{1, 5, 10, 10, 5, 1},
        /* 7*/{1, 6, 15, 20, 15, 6, 1},
        /* 8*/{1, 7, 21, 35, 35, 21, 7, 1},
        /* 9*/{1, 8, 28, 56, 70, 56, 28, 8, 1},
        /*10*/{1, 9, 36, 84, 126, 126, 84, 36, 9, 1},
        /*11*/{1, 10, 45, 120, 210, 252, 210, 120, 45, 10, 1},
        /*12*/{1, 11, 55, 165, 330, 462,462, 330, 165, 55, 11, 1},
        /*13*/{1, 12, 66, 220, 495, 792, 924, 792, 495, 220, 66, 12, 1},
        /*14*/{1, 13, 78, 286, 715, 1287, 1716, 1716, 1287, 715, 286, 78, 13, 1},
        /*15*/{1, 14, 91, 364, 1001, 2002, 3003, 3432, 3003, 2002, 1001, 364, 91,
               14, 1},
        /*16*/{1, 15, 105, 455, 1365, 3003, 5005, 6435, 6435, 5005, 3003, 1365,
               455, 105, 15, 1},
        /*17*/{1, 16, 120, 560, 1820, 4368, 8008, 11440, 12870, 11440, 8008, 4368,
               1820, 560, 120, 16, 1},
        /*18*/{1, 17, 136, 680, 2380, 6188, 12376, 19448, 24310, 24310, 19448,
               12376, 6188, 2380, 680, 136, 17, 1}
    };
    test(1, {{1}}, trials);
    test(5, {{1}, {1, 1}, {1, 2, 1}, {1, 3, 3, 1}, {1, 4, 6, 4, 1}}, trials);
    for (unsigned int i = 0; i < solutions.size(); ++i)
    {
        std::vector<std::vector<int> > current;
        for (unsigned int j = 0; j <= i; ++j)
            current.push_back(solutions[j]);
        test(i + 1, current, trials);
    }
    return 0;
}


