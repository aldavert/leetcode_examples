#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minSwap(std::vector<int> nums1, std::vector<int> nums2)
{
    int keep = 0, swap = 1, prev_keep = 0, prev_swap = 1;
    for (int i = 1, n = static_cast<int>(nums1.size()); i < n; ++i)
    {
        keep = std::numeric_limits<int>::max();
        swap = std::numeric_limits<int>::max();
        if ((nums1[i] > nums1[i - 1]) && (nums2[i] > nums2[i - 1]))
            keep = prev_keep,
            swap = prev_swap + 1;
        if ((nums1[i] > nums2[i - 1]) && (nums2[i] > nums1[i - 1]))
            keep = std::min(keep, prev_swap),
            swap = std::min(swap, prev_keep + 1);
        prev_keep = keep;
        prev_swap = swap;
    }
    return std::min(keep, swap);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSwap(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 5, 4}, {1, 2, 3, 7}, 1, trials);
    test({0, 3, 5, 8, 9}, {2, 1, 4, 6, 9}, 1, trials);
    return 0;
}


