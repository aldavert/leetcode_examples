#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int kthLargestValue(std::vector<std::vector<int> > matrix, int k)
{
    const int m = static_cast<int>(matrix.size()),
              n = static_cast<int>(matrix[0].size());
    std::vector<std::vector<int> > xors(m + 1, std::vector<int>(n + 1));
    std::priority_queue<int, std::vector<int>, std::greater<> > min_heap;
    
    for (int i = 1; i <= m; ++i)
    {
        for (int j = 1; j <= n; ++j)
        {
            xors[i][j] = xors[i - 1][j] ^ xors[i][j - 1] ^ xors[i - 1][j - 1]
                       ^ matrix[i - 1][j - 1];
            min_heap.push(xors[i][j]);
            if (static_cast<int>(min_heap.size()) > k)
                min_heap.pop();
        }
    }
    return min_heap.top();
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = kthLargestValue(matrix, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{5, 2}, {1, 6}}, 1, 7, trials);
    test({{5, 2}, {1, 6}}, 2, 5, trials);
    test({{5, 2}, {1, 6}}, 3, 4, trials);
    return 0;
}


