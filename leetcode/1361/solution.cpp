#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
// In-depth search solution
bool validateBinaryTreeNodes(int n,
                             std::vector<int> leftChild,
                             std::vector<int> rightChild)
{
    std::vector<int> number_of_parents(n);
    for (int i = 0; i < n; ++i)
    {
        if ((leftChild[i] != -1)
        &&  (++number_of_parents[leftChild[i]] > 1)) return false; // Multiple parents.
        if ((rightChild[i] != -1)
        &&  (++number_of_parents[rightChild[i]] > 1)) return false;
    }
    int root_node = -1;
    for (int i = 0; i < n; ++i)
    {
        if (number_of_parents[i] == 0)
        {
            if (root_node != -1) return false; // Multiple root nodes.
            root_node = i;
        }
    }
    if (root_node == -1) return false; // No root node.
    std::vector<int> stack(n + 1);
    std::vector<bool> visited(n, false);
    int size = 0;
    stack[++size] = root_node;
    while (size > 0)
    {
        int current_node = stack[--size];
        if (visited[current_node]) return false;
        visited[current_node] = true;
        if (rightChild[current_node] != -1)
            stack[size++] = rightChild[current_node];
        if (leftChild[current_node] != -1)
            stack[size++] = leftChild[current_node];
    }
    return true;
}
#else
// Union-find solution
bool validateBinaryTreeNodes(int n,
                             std::vector<int> leftChild,
                             std::vector<int> rightChild)
{
    std::vector<int> id(n), ins(n);
    for (int i = 0; i < n; ++i) id[i] = i;
    auto find = [&](auto &&self, int i) -> int
    {
        return id[i] = (id[i] == i)?i:id[i] = self(self, id[i]);
    };
    for (int i = 0; i < n; ++i)
    {
        if ((leftChild[i] != -1) && (rightChild[i] != -1))
        {
            if (++ins[leftChild[i]] > 1) return false;
            if (++ins[rightChild[i]] > 1) return false;
            int u = find(find, leftChild[i]), v = find(find, rightChild[i]),
                w = find(find, i);
            if ((u == v) || (w == u) || (w == v)) return false;
            id[w] = id[u] = id[v] = std::min(id[w], std::min(id[v], id[u]));
        }
        else if (leftChild[i] != -1)
        {
            if (++ins[leftChild[i]] > 1) return false;
            int u = find(find, leftChild[i]), w = find(find, i);
            if (w == u) return false;
            id[w] = id[u] = std::min(id[w], id[u]);
        }
        else if (rightChild[i] != -1)
        {
            if (++ins[rightChild[i]] > 1) return false;
            int v = find(find, rightChild[i]), w = find(find, i);
            if (w == v) return false;
            id[w] = id[v] = std::min(id[w], id[v]);
        }
    }
    find(find, 0);
    for (int i = 1; i < n; ++i)
        if (find(find, i) != id[0]) return false;
    return true;
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<int> leftChild,
          std::vector<int> rightChild,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = validateBinaryTreeNodes(n, leftChild, rightChild);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {1, -1, 3, -1}, {2, -1, -1, -1}, true, trials);
    test(4, {1, -1, 3, -1}, {2, 3, -1, -1}, false, trials);
    test(2, {1, 0}, {-1, -1}, false, trials);
    test(4, {1, -1, 3, -1}, {-1, -1, -1, -1}, false, trials);
    test(3, {1, -1, -1}, {-1, -1, 1}, false, trials);
    test(1, {-1}, {-1}, true, trials);
    test(3, {-1, -1, -1}, {-1, -1, -1}, false, trials);
    test(3, {-1, 2, -1}, {-1, -1, -1}, false, trials);
    test(4, {3, -1, 1, -1}, {-1, -1, 0, -1}, true, trials);
    return 0;
}


