# Validate Binary Tree Nodes

You have `n` binary tree nodes numbered from `0` to `n - 1` where node `i` has two children `leftChild[i]` and `rightChild[i]`, return `true` if and only if **all** the given nodes form **exactly one** valid binary tree.

If node `i` has no left child then `leftChild[i]` will equal `-1`, similarly for the right child.

Note that the nodes have no values and that we only use the node numbers in this problem.

#### Example 1:
> ```mermaid 
> graph TD;
> A((0))-->B((1))
> A-->C((2))
> C-->D((3))
> C---E((  ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> linkStyle 3 stroke-width:0px
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class E empty;
> ```
> *Input:* `n = 4, leftChild = [1, -1, 3, -1], rightChild = [2, -1, -1, -1]`  
> *Output:* `true`

#### Example 2:
> ```mermaid 
> graph TD;
> A((0))-->B((1))
> A-->C((2))
> B---E((  ))
> B-->D((3))
> C-->D
> C---F((  ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> linkStyle 2,5 stroke-width:0px
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class E,F empty;
> ```
> *Input:* `n = 4, leftChild = [1, -1, 3, -1], rightChild = [2, 3, -1, -1]`  
> *Output:* `false`

#### Example 3:
> ```mermaid 
> graph TD;
> A((0))-->B((1))
> A---E((  ))
> B-->A
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> linkStyle 1 stroke-width:0px
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class E empty;
> ```
> *Input:* `n = 2, leftChild = [1, 0], rightChild = [-1, -1]`  
> *Output:* `false`

#### Constraints:
- `n == leftChild.length == rightChild.length`
- `1 <= n <= 10^4`
- `-1 <= leftChild[i], rightChild[i] <= n - 1`


