#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string interpret(std::string command)
{
    std::string result;
    for (int i = 0, n = static_cast<int>(command.size()); i < n; ++i)
    {
        if (command[i] == '(')
        {
            if (command[i + 1] == ')')
                result += "o",
                i += 1;
            else result += "al",
                i += 3;
        }
        else result += command[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string command, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = interpret(command);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("G()(al)", "Goal", trials);
    test("G()()()()(al)", "Gooooal", trials);
    test("(al)G(al)()()G", "alGalooG", trials);
    return 0;
}


