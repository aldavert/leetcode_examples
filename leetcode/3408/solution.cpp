#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

class TaskManager
{
    struct Task
    {
        int user_id = -1;
        int task_id = -1;
        int priority = -1;
        bool operator<(const Task &other) const
        {
            return (priority > other.priority)
                || ((priority == other.priority) && (task_id > other.task_id));
        }
    };
    std::set<Task> m_tasks;
    std::unordered_map<int, int> m_task_priority;
public:
    TaskManager(const std::vector<std::vector<int> > &tasks)
    {
        for (auto &task : tasks)
        {
            m_tasks.insert({task[0], task[1], task[2]});
            m_task_priority[task[1]] = task[2];
        }
    }
    void add(int userId, int taskId, int priority)
    {
        m_tasks.insert({userId, taskId, priority});
        m_task_priority[taskId] = priority;
    }
    void edit(int taskId, int newPriority)
    {
        auto priority_search = m_task_priority.find(taskId);
        if (priority_search == m_task_priority.end()) return;
        auto task_search = m_tasks.find({ -1, taskId, priority_search->second });
        Task new_task = {task_search->user_id, taskId, newPriority};
        m_tasks.erase(task_search);
        m_task_priority[taskId] = newPriority;
        m_tasks.insert(new_task);
    }
    void rmv(int taskId)
    {
        auto priority_search = m_task_priority.find(taskId);
        if (priority_search == m_task_priority.end()) return;
        m_tasks.erase(m_tasks.find({ -1, taskId, priority_search->second }));
        m_task_priority.erase(priority_search);
    }
    int execTop(void)
    {
        if (m_tasks.empty()) return -1;
        int user_id = m_tasks.begin()->user_id;
        m_task_priority.erase(m_task_priority.find(m_tasks.begin()->task_id));
        m_tasks.erase(m_tasks.begin());
        return user_id;
    }
};

// ############################################################################
// ############################################################################

constexpr int null = -1'000;
enum class OP { ADD, EDIT, RMV, TOP };
void test(const std::vector<std::vector<int> > &tasks,
          const std::vector<OP> &operations,
          const std::vector<std::vector<int> > &input,
          const std::vector<int> &solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        TaskManager manager(tasks);
        result.clear();
        for (size_t i = 0; i < operations.size(); ++i)
        {
            switch (operations[i])
            {
            case OP::ADD:
                manager.add(input[i][0], input[i][1], input[i][2]);
                result.push_back(null);
                break;
            case OP::EDIT:
                manager.edit(input[i][0], input[i][1]);
                result.push_back(null);
                break;
            case OP::RMV:
                manager.rmv(input[i][0]);
                result.push_back(null);
                break;
            case OP::TOP:
                result.push_back(manager.execTop());
                break;
            default:
                std::cerr << "[ERROR] Unknown operation!\n";
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 101, 10}, {2, 102, 20}, {3, 103, 15}},
         {OP::ADD, OP::EDIT, OP::TOP, OP::RMV, OP::ADD, OP::TOP},
         {{4, 104, 5}, {102, 8}, {}, {101}, {5, 105, 15}, {}},
         {null, null, 3, null, null, 5}, trials);
    test({{10, 26, 25}},
         {OP::RMV, OP::TOP},
         {{26}, {}},
         {null, -1}, trials);
    test({{8, 3, 24}, {7, 11, 1}},
         {OP::RMV, OP::EDIT, OP::TOP},
         {{3}, {11, 1}, {}},
         {null, null, 7}, trials);
    return 0;
}


