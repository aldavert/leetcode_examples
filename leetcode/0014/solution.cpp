#include "../common/common.hpp"
#include <vector>

// ############################################################################
// ############################################################################

std::string longestCommonPrefix(std::vector<std::string> strs)
{
    const size_t n = strs.size();
    const size_t m = strs[0].size();
    std::string result;
    for (size_t p = 0; p < m; ++p)
    {
        char current = strs[0][p];
        bool equal = true;
        for (size_t i = 1; i < n; ++i)
            equal = equal && strs[i][p] == current;
        if (equal) result += current;
        else break;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> strs, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestCommonPrefix(strs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"flower", "flow", "flight"}, "fl", trials);
    test({"dog", "racecar", "car"}, "", trials);
    return 0;
}


