#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

class SORTracker
{
    struct Location
    {
        std::string name;
        int score;
    };
    template <int is_min>
    struct CompareHeap
    {
        bool operator()(const Location &a, const Location &b)
        {
            bool result = (a.score == b.score)?(a.name < b.name):(a.score > b.score);
            return (is_min)?result:!result;
        }
    };
    std::priority_queue<Location, std::vector<Location>, CompareHeap<true>  > m_left;
    std::priority_queue<Location, std::vector<Location>, CompareHeap<false> > m_right;
    int m_k = 0;
public:
    SORTracker(void) {}
    void add(std::string name, int score)
    {
        m_left.push({name, score});
        if (static_cast<int>(m_left.size()) > m_k + 1)
        {
            m_right.push(m_left.top());
            m_left.pop();
        }
    }
    std::string get()
    {
        std::string name = m_left.top().name;
        if (!m_right.empty())
        {
            m_left.push(m_right.top());
            m_right.pop();
        }
        ++m_k;
        return name;
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<bool> op_add,
          std::vector<std::pair<std::string, int> > info,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        SORTracker obj;
        result.clear();
        for (size_t i = 0; i < op_add.size(); ++i)
        {
            if (op_add[i])
            {
                obj.add(info[i].first, info[i].second);
                result.push_back("");
            }
            else result.push_back(obj.get());
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({true, true, false, true, false, true, false, true, false, true, false, false},
         {{"bradford", 2}, {"branford", 3}, {}, {"alps", 2}, {}, {"orland", 2}, {},
          {"orlando", 3}, {}, {"alpine", 2}, {}, {}},
         {"", "", "branford", "", "alps", "", "bradford", "", "bradford", "",
          "bradford", "orland"}, trials);
    return 0;
}


