#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isToeplitzMatrix(std::vector<std::vector<int> > matrix)
{
    const size_t m = matrix.size(),
                 n = matrix[0].size();
    for (size_t i = 1; i < m; ++i)
        for (size_t j = 1; j < n; ++j)
            if (matrix[i][j] != matrix[i - 1][j - 1])
                return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isToeplitzMatrix(matrix);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3, 4}, {5, 1, 2, 3}, {9, 5, 1, 2}}, true, trials);
    test({{1, 2}, {2, 2}}, false, trials);
    return 0;
}


