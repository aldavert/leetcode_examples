#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> minOperations(std::string boxes)
{
    const int n = static_cast<int>(boxes.size());
    std::vector<int> result(n);
    
    for (int i = 0, count = 0, moves = 0; i < n; ++i)
    {
        result[i] += moves;
        count += boxes[i] - '0';
        moves += count;
    }
    for (int i = n - 1, count = 0, moves = 0; i >= 0; --i)
    {
        result[i] += moves;
        count += boxes[i] - '0';
        moves += count;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string boxes, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(boxes);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("110", {1, 1, 3}, trials);
    test("001011", {11, 8, 5, 4, 3, 4}, trials);
    return 0;
}


