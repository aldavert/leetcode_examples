#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int maxDotProduct(std::vector<int> nums1, std::vector<int> nums2)
{
    const int m = static_cast<int>(nums1.size());
    const int n = static_cast<int>(nums2.size());
    std::vector<std::vector<int> > dp(m + 1, std::vector<int>(n + 1,
                                                std::numeric_limits<int>::lowest()));
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            dp[i + 1][j + 1] = std::max({dp[i][j + 1], dp[i + 1][j],
                                         std::max(0, dp[i][j]) + nums1[i] * nums2[j]});
    return dp[m][n];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxDotProduct(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, -2, 5}, {3, 0, -6}, 18, trials);
    test({3, -2}, {2, -6, 7}, 21, trials);
    test({-1, -1}, {1, 1}, -1, trials);
    return 0;
}


