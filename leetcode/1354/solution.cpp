#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 0
bool isPossible(std::vector<int> &target)
{
    const int size = static_cast<int>(target.size());
    std::vector<int> source(size, 1);
    auto work = [&](auto &&self, int sum) -> bool
    {
        unsigned int correct = 0;
        for (unsigned int i = 0; i < target.size(); ++i)
        {
            if (sum <= target[i])
            {
                int previous = source[i];
                int new_sum = sum + sum - source[i];
                source[i] = sum;
                if (self(self, new_sum)) return true;
                source[i] = previous;
            }
            else if (target[i] == source[i]) ++correct;
        }
        return correct == target.size();
    };
    return work(work, size);
}
#elif 0
bool isPossible(std::vector<int> &target)
{
    if (target.size() == 1) return target[0] == 1;
    long sum = 0;
    for (const auto &v : target)
        sum += v;
    std::make_heap(target.begin(), target.end());
    while (target[0] != 1)
    {
        std::pop_heap(target.begin(), target.end());
        long selected = target.back();
        long step = sum - selected;
        if (selected - step < 1) return false;
        
        long new_value = 1 + ((selected - step - 1) % step);
        sum += new_value - selected;
        target.back() = static_cast<int>(new_value);
        std::push_heap(target.begin(), target.end());
    }
    return target[0] == 1;
}
#else
bool isPossible(std::vector<int> &target)
{
    if (target.size() == 1) return target[0] == 1;
    long sum = 0;
    for (const auto &v : target)
        sum += v;
    std::priority_queue<long> queue(target.begin(), target.end());
    while (queue.top() != 1)
    {
        long selected = queue.top();
        queue.pop();
        long step = sum - selected;
        if (selected - step < 1) return false;
        
        long new_value = 1 + ((selected - step - 1) % step);
        sum += new_value - selected;
        queue.push(new_value);
    }
    return queue.top() == 1;
}
#endif

// ############################################################################
// ############################################################################

//sp + sp - x = 17
//r = s - 9 = 8
//sp = r + x = r + x
//
//(r + x) + (r + x) - x = s
//2r + 2x - x = s
//x = s - 2r = s - 2 * (s - c) = s - 2 * s + 2 * c = 2 * c - s;
//x = 17 - 2 * 8 = 1 ---- 2 * 9 - 17 = 18 - 17 = 1;

void test(std::vector<int> target, bool solution, unsigned int trials)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPossible(target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 100000;
    constexpr unsigned int trials = 10000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({9,3,5}, true, trials);
    test({1,1,1,2}, false, trials);
    test({8,5}, true, trials);
    test({3,5,33}, true, trials);
    test({1,1000000000}, true, trials);
    test({9,9,9}, false, trials);
    test({1,1,2}, false, trials);
    test({2}, false, trials);
    return 0;
}


