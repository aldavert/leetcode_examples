#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string generateTheString(int n)
{
    std::string result(n, 'a');
    result[0] = static_cast<char>('a' + ((n & 1) == 0));
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, unsigned int trials = 1)
{
    auto check = [](std::string str) -> bool
    {
        int histogram[26] = {};
        for (char c : str) ++histogram[static_cast<int>(c)];
        for (int i = 0; i < 26; ++i)
            if (histogram[i] && ((histogram[i] & 1) == 0))
                return false;
        return true;
    };
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = generateTheString(n);
    bool valid = check(result);
    
    std::cout << showResult(valid);
    std::cout << " the string " << result << " is" << ((valid)?(" "):(" not "));
    std::cout << "properly formatted.\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, trials);
    test(2, trials);
    test(7, trials);
    test(50, trials);
    return 0;
}


