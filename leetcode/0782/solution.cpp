#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int movesToChessboard(std::vector<std::vector<int> > board)
{
    const int n = static_cast<int>(board.size());
    int cnt1 = 1, cnt2 = 0;
    for (int i = 1; i < n; ++i)
    {
        if (board[0][0] == board[i][0])
        {
            ++cnt1;
            for (int j = 0; j < n; ++j)
                if (board[0][j] != board[i][j])
                    return -1;
        }
        else
        {
            ++cnt2;
            for (int j = 0; j < n; ++j)
                if (board[0][j] == board[i][j])
                    return -1;
        }
    }
    if (std::abs(cnt1 - cnt2) > 1)
        return -1;
    
    cnt1 = 1, cnt2 = 0;
    for (int j = 1; j < n; ++j)
    {
        if (board[0][0] == board[0][j])
        {
            ++cnt1;
            for (int i = 0; i < n; ++i)
                if (board[i][0] != board[i][j])
                    return -1;
        }
        else
        {
            ++cnt2;
            for (int i = 0; i < n; ++i)
                if (board[i][0] == board[i][j])
                    return -1;
        }
    }
    if (abs(cnt1 - cnt2) > 1)
        return -1;
    
    int swap_row = 0, swap_column = 0;
    bool bit = false;
    for (int i = 0; i < n; ++i, bit = !bit)
        if (board[i][0] != bit)
            ++swap_row;
    bit = false;
    for (int j = 0; j < n; ++j, bit = !bit)
        if (board[0][j] != bit)
            ++swap_column;
    
    if (n & 1)
    {
        if (swap_row & 1) swap_row = n - swap_row;
        if (swap_column & 1) swap_column = n - swap_column;
        return swap_row / 2 + swap_column / 2;
    }
    else
    {
        return (std::min(swap_row, n - swap_row)
              + std::min(swap_column, n - swap_column)) / 2;
    }
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > board, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = movesToChessboard(board);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 1, 0}, {0, 1, 1, 0}, {1, 0, 0, 1}, {1, 0, 0, 1}}, 2, trials);
    test({{0, 1}, {1, 0}}, 0, trials);
    test({{1, 0}, {1, 0}}, -1, trials);
    return 0;
}


