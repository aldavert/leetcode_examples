#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> beautifulIndices(std::string s, std::string a, std::string b, int k)
{
    auto kmp = [&](const std::string &pattern) -> std::vector<int>
    {
        std::vector<int> res, lps(pattern.size());
        for (size_t i = 1, j = 0; i < pattern.size(); ++i)
        {
            while ((j > 0) && (pattern[j] != pattern[i])) j = lps[j - 1];
            if (pattern[i] == pattern[j]) lps[i] = static_cast<int>(++j);
        }
        for (size_t i = 0, j = 0; i < s.size(); )
        {
            if (s[i] == pattern[j])
            {
                ++i;
                ++j;
                if (j == pattern.size())
                {
                    res.push_back(static_cast<int>(i) - static_cast<int>(j));
                    j = lps[j - 1];
                }
            }
            else if (j > 0) j = lps[j - 1];
            else ++i;
        }
        return res;
    };
    std::vector<int> result;
    const std::vector<int> indices_a = kmp(a), indices_b = kmp(b);
    size_t indices_bindex = 0;
    for (int i : indices_a)
    {
        while ((indices_bindex < indices_b.size())
           &&  (indices_b[indices_bindex] - i < -k)) ++indices_bindex;
        if ((indices_bindex < indices_b.size()) && (indices_b[indices_bindex] - i <= k))
            result.push_back(i);
    }
    return result;

}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::string a,
          std::string b,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = beautifulIndices(s, a, b, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("isawsquirrelnearmysquirrelhouseohmy", "my", "squirrel", 15, {16, 33}, trials);
    test("abcd", "a", "a", 4, {0}, trials);
    return 0;
}


