#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> findingUsersActiveMinutes(std::vector<std::vector<int> > logs, int k)
{
    std::vector<int> result(k);
    std::unordered_map<int, std::unordered_set<int> > id_to_times;
    
    for (const auto &log : logs)
        id_to_times[log[0]].insert(log[1]);
    for (const auto &[_, mins] : id_to_times)
        ++result[mins.size() - 1];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > logs,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findingUsersActiveMinutes(logs, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 5}, {1, 2}, {0, 2}, {0, 5}, {1, 3}}, 5, {0, 2, 0, 0, 0}, trials);
    test({{1, 1}, {2, 2}, {2, 3}}, 4, {1, 1, 0, 0}, trials);
    return 0;
}


