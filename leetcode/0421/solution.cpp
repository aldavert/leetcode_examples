#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int findMaximumXOR(std::vector<int> nums)
{
    struct Range
    {
        std::vector<int>::iterator begin;
        std::vector<int>::iterator end;
        Range cropR(std::vector<int>::iterator other) const { return { begin, other }; }
        Range cropL(std::vector<int>::iterator other) const { return { other, end }; }
        bool empty(void) const { return begin == end; }
    };
    auto range = [&](void) -> Range { return { nums.begin(), nums.end() }; };
    auto helper = [&](auto &&self, Range left, Range right, int val, int mask) -> int
    {
        if (left.empty() || right.empty() || (mask == 0)) return val;
        auto cond = [mask](int num) { return num & mask; };
        auto lmid = std::partition(left.begin, left.end, cond);
        auto rmid = std::partition(right.begin, right.end, cond);
        if (((lmid == left.end  ) && (rmid == right.end  ))
         || ((lmid == left.begin) && (rmid == right.begin)))
            return self(self, left, right, val, mask >> 1);
        else 
        {
            val |= mask;
            mask >>= 1;
            if (right.begin < left.end)
                return self(self, left.cropR(lmid), right.cropL(rmid), val, mask);
            return std::max(self(self, left.cropR(lmid), right.cropL(rmid), val, mask),
                            self(self, left.cropL(lmid), right.cropR(rmid), val, mask));
        }
    };
    return helper(helper, range(), range(), 0, 1 << 30);
}
#else
int findMaximumXOR(std::vector<int> nums)
{
    int result = 0;
    
    for (int i = 30, mask = 0; i >= 0; --i)
    {
        std::unordered_set<int> putative;
        
        mask |= (1 << i);
        for (int n : nums)
            putative.insert(n & mask);
        for (int current_max = result | (1 << i); int prefix : putative)
        {
            if (putative.find(current_max ^ prefix) != putative.end())
            {
                result = current_max;
                break;
            }
        }
    }
    
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMaximumXOR(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 10, 5, 25, 2, 8}, 28, trials);
    test({25, 5, 3, 10, 2, 8}, 28, trials);
    test({5, 25, 3, 10, 2, 8}, 28, trials);
    test({3, 10, 2, 8, 25, 5}, 28, trials);
    test({3, 10, 2, 8, 5, 25}, 28, trials);
    test({14, 70, 53, 83, 49, 91, 36, 80, 92, 51, 66, 70}, 127, trials);
    test({36, 80, 92, 51, 66, 14, 70, 53, 83, 49, 91, 70}, 127, trials);
    test({14, 70, 53, 66, 70, 83, 49, 91, 36, 80, 92, 51}, 127, trials);
    return 0;
}


