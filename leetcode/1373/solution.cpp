#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int maxSumBST(TreeNode* root)
{
    struct Status { bool status = false; int min = 0; int max = 0; int sum = 0; };
    int result = 0;
    auto traverse = [&](auto &&self, TreeNode * node) -> Status
    {
        if (!node) return {true, 0, 0, 0};
        Status left  = self(self, node->left),
               right = self(self, node->right);
        if (node->left && (node->val <= left.max)) return {};
        if (node->right && (node->val >= right.min)) return {};
        if (left.status && right.status)
        {
            int current = node->val + left.sum + right.sum;
            result = std::max(result, current);
            return {true,
                    node->left?left.min:node->val,
                    node->right?right.max:node->val,
                    current};
        }
        else return {};
    };
    traverse(traverse, root);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = maxSumBST(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 3, 2, 4, 2, 5, null, null, null, null, null, null, 4, 6}, 20, trials);
    test({4, 3, null, 1, 2}, 2, trials);
    test({-4, -2, -5}, 0, trials);
    test({1, null, 10, -5, 20}, 25, trials);
    return 0;
}


