#include "../common/common.hpp"
#include <unordered_map>
#include <set>
#include <sstream>

#if 1
std::vector<std::vector<int> > threeSum(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<std::vector<int> > result;
    int inc[3001] = {};
    
    if (n < 3) return result;
    std::sort(nums.begin(), nums.end());
    
    inc[0] = 0;
    for (int i = 1; i < n; ++i) inc[i] = nums[i] - nums[i - 1];
    for (int i = 0; i < n - 2;)
    {
        int l = i + 1;
        int r = n - 1;
        int sum = nums[i] + nums[l] + nums[r];
        while (l < r)
        {
            if (sum > 0) sum -= inc[r--];
            else
            {
                if (sum < 0) sum += inc[++l];
                else
                {
                    result.push_back({nums[i], nums[l], nums[r]});
                    while (++l < r)
                    {
                        if (inc[l])
                        {
                            sum += inc[l];
                            break;
                        }
                    }
                    while (l < --r)
                    {
                        if (inc[r + 1])
                        {
                            sum -= inc[r + 1];
                            break;
                        }
                    }
                }
            }
        }
        while ((++i < n - 2) && (!inc[i]));
    }
    return result;
}
#else
std::vector<std::vector<int> > threeSum(std::vector<int> nums)
{
    if (nums.size() < 3) return {};
    std::unordered_map<int, int> negatives, positives;
    int zero = 0;
    for (int v : nums)
    {
        if (v < 0)
            ++negatives[v];
        else if (v > 0)
            ++positives[v];
        else ++zero;
    }
    
    std::unordered_map<int, std::set<std::pair<int, int> > > sum_negatives, sum_positives;
    for (auto begin1 = negatives.begin(), end = negatives.end(); begin1 != end; ++begin1)
    {
        if (begin1->second > 1)
            sum_negatives[2 * begin1->first].insert({begin1->first, begin1->first});
        for (auto begin2 = std::next(begin1); begin2 != end; ++begin2)
            sum_negatives[begin1->first + begin2->first].insert({begin1->first, begin2->first});
    }
    for (auto begin1 = positives.begin(), end = positives.end(); begin1 != end; ++begin1)
    {
        if (begin1->second > 1)
            sum_positives[2 * begin1->first].insert({begin1->first, begin1->first});
        for (auto begin2 = std::next(begin1); begin2 != end; ++begin2)
            sum_positives[begin1->first + begin2->first].insert({begin1->first, begin2->first});
    }
    
#if 0
    for (int v : nums)
        std::cout << v << ' ';
    std::cout << '\n';
    std::cout << "Zero: " << ((zero)?"Yes":"No") << '\n';
    std::cout << "Positive:";
    for (auto v : positives) std::cout << ' ' << v.first << ':' << v.second;
    std::cout << '\n';
    std::cout << "Negative:";
    for (auto v : negatives) std::cout << ' ' << v.first << ':' << v.second;
    std::cout << '\n';
    std::cout << "Sum negative:";
    for (auto v : sum_negatives)
    {
        std::cout << v.first << ":{";
        bool next = false;
        for (auto [f, s] : v.second)
        {
            if (next) [[likely]] std::cout << ", ";
            next = true;
            std::cout << f << ':' << s;
        }
        std::cout << "} ";
    }
    std::cout << '\n';
#endif
    
    std::vector<std::vector<int> > result;
    if (zero)
    {
        for (auto &p : positives)
        {
            if (negatives.find(-(p.first)) != negatives.end())
                result.push_back({p.first, 0, -p.first});
        }
        if (zero >= 3)
            result.push_back({0, 0, 0});
    }
    for (auto &p : positives)
    {
        if (auto it = sum_negatives.find(-(p.first)); it != sum_negatives.end())
        {
            for (const auto &v : it->second)
                result.push_back({p.first, v.first, v.second});
        }
    }
    for (auto &p : negatives)
    {
        if (auto it = sum_positives.find(-(p.first)); it != sum_positives.end())
        {
            for (const auto &v : it->second)
                result.push_back({p.first, v.first, v.second});
        }
    }
    
    return result;
}
#endif

#if 0
bool operator==(const std::vector<std::vector<int> > &left, const std::vector<std::vector<int> > &right)
{
    struct data
    {
        int first = 0;
        int second = 0;
        int third = 0;
    };
    if (left.size() != right.size()) return false;
    const int n = static_cast<int>(left.size());
    typedef std::tuple<int, int, int> KEY_T;

    struct key_hash : public std::unary_function<KEY_T, std::size_t>
    {
        std::size_t operator()(const KEY_T &k) const
        {
            return std::hash<int>{}(std::get<0>(k))
                 ^ std::hash<int>{}(std::get<1>(k))
                 ^ std::hash<int>{}(std::get<2>(k));
        }
    };
    struct key_equal : public std::binary_function<KEY_T, KEY_T, bool>
    {
        constexpr bool operator()(const KEY_T &v0, const KEY_T &v1) const
        {
            return std::get<0>(v0) == std::get<0>(v1)
                && std::get<1>(v0) == std::get<1>(v1)
                && std::get<2>(v0) == std::get<2>(v1);
        }
    };
    typedef std::unordered_set<KEY_T, key_hash, key_equal> lut_t;
    lut_t lut;
    for (const auto &v : left)
    {
        std::vector<int> c(v);
        std::sort(c.begin(), c.end());
        lut.insert({c[0], c[1], c[2]});
    }
    for (const auto &v : right)
    {
        std::vector<int> c(v);
        std::sort(c.begin(), c.end());
        if (auto it = lut.find({c[0], c[1], c[2]}); it != lut.end())
            lut.erase(it);
        else return false;
    }
    return true;
}
#else
bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    std::unordered_set<std::string> elements;
    for (std::vector<int> tuple : left)
    {
        std::ostringstream oss;
        std::sort(tuple.begin(), tuple.end());
        oss << tuple[0] << ',' << tuple[1] << ',' << tuple[2];
        elements.insert(oss.str());
    }
    for (std::vector<int> tuple : right)
    {
        std::ostringstream oss;
        std::sort(tuple.begin(), tuple.end());
        oss << tuple[0] << ',' << tuple[1] << ',' << tuple[2];
        if (auto it = elements.find(oss.str()); it != elements.end())
            elements.erase(it);
        else return false;
    }
    return true;
}
#endif

void test(std::vector<int> nums, std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = threeSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, 0, 0, 0, 0, 1, 2, -1, -4}, {{0, 0, 0}, {-1, -1, 2}, {-1, 0, 1}}, trials);
    test({-1, 0, 1, 2, -1, -4}, {{-1, -1, 2}, {-1, 0, 1}}, trials);
    test({-1, 1, 2, -1, -4}, {{-1, -1, 2}}, trials);
    test({-1, 0, 0, 0, 0, -1, -4}, {{0, 0, 0}}, trials);
    test({0, 0, 0, 0, 1, 2}, {{0, 0, 0}}, trials);
    test({0}, {}, trials);
    test({}, {}, trials);
    test({1, 1, -2}, {{-2, 1, 1}}, trials);
    return 0;
}
