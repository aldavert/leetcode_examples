#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int mostBooked(int n, std::vector<std::vector<int> > meetings)
{
    struct Info
    {
        long end_time;
        int room_id;
        bool operator<(const Info &other) const
        {
            return (end_time == other.end_time)?room_id > other.room_id
                                               :end_time > other.end_time;
        }
    };
    std::vector<int> count(n);
    auto meetings_work = meetings;
    std::sort(meetings_work.begin(), meetings_work.end());
    
    std::priority_queue<Info> occupied;
    std::priority_queue<int, std::vector<int>, std::greater<> > available_room;
    for (int i = 0; i < n; ++i) available_room.push(i);
    for (const auto &meeting : meetings_work)
    {
        int start = meeting[0], end = meeting[1];
        while (!occupied.empty() && (occupied.top().end_time <= start))
            available_room.push(occupied.top().room_id),
            occupied.pop();
        if (available_room.empty())
        {
            auto [new_start, room_id] = occupied.top();
            occupied.pop();
            ++count[room_id];
            occupied.push({new_start + (end - start), room_id});
        }
        else
        {
            int room_id = available_room.top();
            available_room.pop();
            ++count[room_id];
            occupied.push({end, room_id});
        }
    }
    return static_cast<int>(std::max_element(count.begin(), count.end())
                          - count.begin());
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > meetings,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = mostBooked(n, meetings);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, {{0, 10}, {1, 5}, {2, 7}, {3, 4}}, 0, trials);
    test(3, {{1, 20}, {2, 10}, {3, 5}, {4, 9}, {6, 8}}, 1, trials);
    return 0;
}


