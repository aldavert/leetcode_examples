#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minPairSum(std::vector<int> nums)
{
    std::sort(nums.begin(), nums.end());
    int result = 0;
    for (size_t l = 0, r = nums.size() - 1; l < r; ++l, --r)
        result = std::max(result, nums[l] + nums[r]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minPairSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 5, 2, 3}, 7, trials);
    test({3, 5, 4, 2, 4, 6}, 8, trials);
    return 0;
}


