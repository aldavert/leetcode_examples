#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestPalindrome(std::string s)
{
    std::vector<int> histogram(52, 0);
    for (char c : s)
    {
        if ((c >= 'A') && (c <= 'Z'))
            ++histogram[c - 'A'];
        else ++histogram[c - 'a' + 26];
    }
    int result = 0;
    bool extra = false;
    for (size_t i = 0; i < 52; ++i)
    {
        int pair = histogram[i] / 2;
        result += 2 * pair;
        histogram[i] -= 2 * pair;
        extra = extra || histogram[i];
    }
    return result + extra;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestPalindrome(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abccccdd", 7, trials);
    test("a", 1, trials);
    return 0;
}


