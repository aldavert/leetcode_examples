#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findLUSlength(std::string a, std::string b)
{
    return (a == b)?-1:std::max(a.size(), b.size());
}

// ############################################################################
// ############################################################################

void test(std::string a, std::string b, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLUSlength(a, b);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aba", "cdc",  3, trials);
    test("aaa", "bbb",  3, trials);
    test("aaa", "aaa", -1, trials);
    test("aefawfawfawfaw", "aefawfeawfwafwaef", 17, trials);
    return 0;
}


