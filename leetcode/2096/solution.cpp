#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

std::string getDirections(TreeNode * root, int startValue, int destValue)
{
    auto dfs = [&](auto &&self, TreeNode * node, int val, std::string &path) -> bool
    {
        if (node->val == val)
            return true;
        if (node->left && self(self, node->left, val, path))
            path.push_back('L');
        else if (node->right && self(self, node->right, val, path))
            path.push_back('R');
        return !path.empty();
    };
    std::string path_to_start, path_to_destination;
    dfs(dfs, root, startValue, path_to_start);
    dfs(dfs, root, destValue, path_to_destination);
    while (!path_to_start.empty() && !path_to_destination.empty()
       &&  (path_to_start.back() == path_to_destination.back()))
    {
        path_to_start.pop_back();
        path_to_destination.pop_back();
    }
    return std::string(path_to_start.length(), 'U')
         + std::string(path_to_destination.rbegin(), path_to_destination.rend());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          int startValue,
          int destValue,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = getDirections(root, startValue, destValue);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 1, 2, 3, null, 6, 4}, 3, 6, "UURL", trials);
    test({2, 1}, 2, 1, "L", trials);
    return 0;
}


