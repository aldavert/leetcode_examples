#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <stack>

// ############################################################################
// ############################################################################

std::vector<int> getAllElements(TreeNode * root1, TreeNode * root2)
{
    std::stack<TreeNode *> stack1, stack2;
    std::vector<int> result;
    auto descend = [](std::stack<TreeNode *> &s)
    {
        while (s.top()->left)
            s.push(s.top()->left);
    };
    auto update = [&](std::stack<TreeNode *> &s)
    {
        TreeNode * ptr = s.top();
        s.pop();
        if (ptr->right)
        {
            s.push(ptr->right);
            descend(s);
        }
        result.push_back(ptr->val);
    };
    auto init = [&](TreeNode * root, std::stack<TreeNode *> &s)
    {
        if (root)
        {
            s.push(root);
            descend(s);
        }
    };
    
    init(root1, stack1);
    init(root2, stack2);
    while (!(stack1.empty() || stack2.empty()))
    {
        if (stack1.top()->val < stack2.top()->val)
            update(stack1);
        else update(stack2);
    }
    while (!stack1.empty()) update(stack1);
    while (!stack2.empty()) update(stack2);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree1,
          std::vector<int> tree2,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root1 = vec2tree(tree1);
        TreeNode * root2 = vec2tree(tree2);
        result = getAllElements(root1, root2);
        delete root1;
        delete root2;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    /*
     *   2     1
     *  / \   / \
     * 1   4 0   3
     */
    test({2, 1, 4}, {1, 0, 3}, {0, 1, 1, 2, 3, 4}, trials);
    /*
     *   1     8
     *  / \   / \
     * ·   8 1   ·
     */
    test({1, null, 8}, {8, 1}, {1, 1, 8, 8}, trials);
    /*
     *   3     1
     *  / \   / \
     * 1   4 0   3
     *  \
     *   2
     */
    test({3, 1, 4, null, 2}, {1, 0, 3}, {0, 1, 1, 2, 3, 3, 4}, trials);
    test({}, {5, 1, 7, 0, 2}, {0, 1, 2, 5, 7}, trials);
    return 0;
}


