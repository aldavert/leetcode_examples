# All Elements in Two Binary Search Trees

Given two binary search trees `root1` and `root2`, return *a list containing all the integers from both trees sorted in* ***ascending*** *order*.

#### Example 1:
> ```mermaid
> graph TD;
> A((2))---B((1))
> A---C((4))
> D((1))---E((0))
> D---F((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root1 = [2, 1, 4], root2 = [1, 0, 3]`  
> *Output:* `[0, 1, 1, 2, 3, 4]`

#### Example 2:
> ```mermaid
> graph TD;
> A((1))---EMPTY1(( ))
> A---B((8))
> D((8))---E((1))
> D---EMPTY2(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY1,EMPTY2 empty;
> linkStyle 0,3 stroke-width:0px;
> ```
> *Input:* `root1 = [1, null, 8], root2 = [8, 1]`  
> *Output:* `[1, 1, 8, 8]`
 
#### Constraints:
- The number of nodes in each tree is in the range `[0, 5000]`.
- `-10^5 <= Node.val <= 10^5`


