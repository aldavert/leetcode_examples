#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int xorAllNums(std::vector<int> nums1, std::vector<int> nums2)
{
    const int xors1 = std::accumulate(nums1.begin(), nums1.end(), 0, std::bit_xor<>());
    const int xors2 = std::accumulate(nums2.begin(), nums2.end(), 0, std::bit_xor<>());
    return (nums1.size() % 2 * xors2) ^ (nums2.size() % 2 * xors1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = xorAllNums(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3}, {10, 2, 5, 0}, 13, trials);
    test({1, 2}, {3, 4}, 0, trials);
    return 0;
}


