#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 0
std::vector<std::string> findWords(std::vector<std::string> words)
{
    std::vector<std::string> result;
    std::unordered_set<char> rows[3] = {
        {'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p',
         'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'},
        {'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
         'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L'},
        {'z', 'x', 'c', 'v', 'b', 'n', 'm',
         'Z', 'X', 'C', 'V', 'B', 'N', 'M'}};
    for (const std::string &word : words)
    {
        if (word.empty()) continue;
        int row = -1;
        if      (rows[0].find(word[0]) != rows[0].end()) row = 0;
        else if (rows[1].find(word[0]) != rows[1].end()) row = 1;
        else if (rows[2].find(word[0]) != rows[2].end()) row = 2;
        if (row == -1) continue;
        bool valid = true;
        for (char c : word)
        {
            if (rows[row].find(c) == rows[row].end())
            {
                valid = false;
                break;
            }
        }
        if (valid) result.push_back(word);
    }
    return result;
}
#else
std::vector<std::string> findWords(std::vector<std::string> words)
{
    std::vector<std::string> result;
    std::unordered_map<char, char> rows = {
        {'q', 0}, {'w', 0}, {'e', 0}, {'r', 0}, {'t', 0}, {'y', 0}, {'u', 0},
        {'i', 0}, {'o', 0}, {'p', 0}, {'Q', 0}, {'W', 0}, {'E', 0}, {'R', 0},
        {'T', 0}, {'Y', 0}, {'U', 0}, {'I', 0}, {'O', 0}, {'P', 0},
        {'a', 1}, {'s', 1}, {'d', 1}, {'f', 1}, {'g', 1}, {'h', 1}, {'j', 1},
        {'k', 1}, {'l', 1}, {'A', 1}, {'S', 1}, {'D', 1}, {'F', 1}, {'G', 1},
        {'H', 1}, {'J', 1}, {'K', 1}, {'L', 1},
        {'z', 2}, {'x', 2}, {'c', 2}, {'v', 2}, {'b', 2}, {'n', 2}, {'m', 2},
        {'Z', 2}, {'X', 2}, {'C', 2}, {'V', 2}, {'B', 2}, {'N', 2}, {'M', 2}};
    for (const std::string &word : words)
    {
        if (word.empty()) continue;
        int row = rows[word[0]];
        bool valid = true;
        for (char c : word)
        {
            if (rows[c] != row)
            {
                valid = false;
                break;
            }
        }
        if (valid) result.push_back(word);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findWords(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"Hello", "Alaska", "Dad", "Peace"}, {"Alaska", "Dad"}, trials);
    test({"omk"}, {}, trials);
    test({"adsdf", "sfd"}, {"adsdf", "sfd"}, trials);
    return 0;
}


