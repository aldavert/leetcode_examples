#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
std::vector<long long> mostFrequentIDs(std::vector<int> nums, std::vector<int> freq)
{
    int size = *std::max_element(nums.begin(), nums.end());
    std::vector<long long> temp(size + 1), result;
    std::priority_queue<std::pair<long long, long long> > pq;
    for (size_t i = 0; i < nums.size(); ++i)
    {
        temp[nums[i]] += freq[i];
        pq.push({temp[nums[i]],nums[i]});
        while (!pq.empty() && (pq.top().first != temp[pq.top().second]))
            pq.pop();
        result.push_back(pq.top().first);
    }
    return result;
}
#else
std::vector<long long> mostFrequentIDs(std::vector<int> nums, std::vector<int> freq)
{
    std::vector<long long> result;
    std::unordered_map<int, long> num_count;
    std::map<long, int> freq_count;
    for (size_t i = 0; i < nums.size(); ++i)
    {
        const int num = nums[i];
        const int f = freq[i];
        if (const auto it = num_count.find(num); it != num_count.cend())
        {
            const long num_freq = it->second;
            if (--freq_count[num_freq] == 0)
            freq_count.erase(num_freq);
        }
        if (long new_freq = num_count[num] + f; new_freq == 0) num_count.erase(num);
        else
        {
            num_count[num] = new_freq;
            ++freq_count[new_freq];
        }
        result.push_back(freq_count.empty() ? 0 : freq_count.rbegin()->first);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> freq,
          std::vector<long long> solution,
          unsigned int trials = 1)
{
    std::vector<long long> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = mostFrequentIDs(nums, freq);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 2, 1}, {3, 2, -3, 1}, {3, 3, 2, 2}, trials);
    test({5, 5, 3}, {2, -2, 1}, {2, 0, 1}, trials);
    return 0;
}


