#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int maxDistance(std::vector<std::vector<int> > grid)
{
    const int m = static_cast<int>(grid.size());
    const int n = static_cast<int>(grid[0].size());
    const std::vector<int> dirs{0, 1, 0, -1, 0};
    std::queue<std::pair<int, int> > q;
    int water = 0;
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            if (grid[i][j] == 0) ++water;
            else q.emplace(i, j);
        }
    }
    if ((water == 0) || (water == m * n)) return -1;
    int result = 0;
    for (int d = 0; !q.empty(); ++d)
    {
        for (int sz = static_cast<int>(q.size()); sz > 0; --sz)
        {
            const auto [i, j] = q.front();
            q.pop();
            result = d;
            for (int k = 0; k < 4; ++k)
            {
                const int x = i + dirs[k];
                const int y = j + dirs[k + 1];
                if ((x < 0) || (x == m) || (y < 0) || (y == n))
                    continue;
                if (grid[x][y] > 0)
                    continue;
                q.emplace(x, y);
                grid[x][y] = 2;
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxDistance(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0, 1}, {0, 0, 0}, {1, 0, 1}}, 2, trials);
    test({{1, 0, 0}, {0, 0, 0}, {0, 0, 0}}, 4, trials);
    return 0;
}


