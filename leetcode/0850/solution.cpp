#include "../common/common.hpp"
#include <unordered_map>
#include <set>
#include <algorithm>

// ############################################################################
// ############################################################################

int rectangleArea(std::vector<std::vector<int> > rectangles)
{
    class SegmentTree
    {
        std::unordered_map<int, int> m_count;
        std::unordered_map<int, int> m_total;
        std::vector<int> m_ordered_x;
    public:
        SegmentTree(const std::set<int> &ordered_x) :
            m_ordered_x(ordered_x.begin(), ordered_x.end()) {}
        void update(int v, int tree_left, int tree_right, int left, int right, int op_cl)
        {
            if (left > right) return;
            if ((left == tree_left) && (right == tree_right))
                m_count[v] += op_cl;
            else
            {
                int tree_middle = (tree_left + tree_right) / 2;
                update(v * 2,
                       tree_left,
                       tree_middle,
                       left,
                       std::min(right, tree_middle),
                       op_cl);
                update(v * 2 + 1,
                       tree_middle + 1,
                       tree_right,
                       std::max(left, tree_middle + 1),
                       right,
                       op_cl);
            }
            if (m_count[v] > 0)
                m_total[v] = m_ordered_x[tree_right + 1] - m_ordered_x[tree_left];
            else
                m_total[v] = m_total[v * 2] + m_total[v * 2 + 1];
        }
        int total(int value)
        {
            if (auto it = m_total.find(value); it != m_total.end())
                return it->second;
            else return 0;
        }
    };
    
    std::unordered_map<int, int> ordered_x_lut;
    std::set<int> ordered_x;
    for (const auto &rect : rectangles)
    {
        ordered_x.insert(rect[0]);
        ordered_x.insert(rect[2]);
    }
    const int nx = static_cast<int>(ordered_x.size());
    int idx = 0;
    for (int x : ordered_x)
        ordered_x_lut[x] = idx++;
    SegmentTree tree(ordered_x);
    std::vector<std::tuple<int, int, int, int> > ordered_y;
    ordered_y.reserve(rectangles.size() * 2);
    for (const auto &rect : rectangles)
    {
        ordered_y.push_back({rect[1],  1, rect[0], rect[2]});
        ordered_y.push_back({rect[3], -1, rect[0], rect[2]});
    }
    std::sort(ordered_y.begin(), ordered_y.end());
    int current_y = 0;
    long area = 0, current_x_sum = 0;
    for (auto [y, op_cl, x1, x2] : ordered_y)
    {
        area += (y - current_y) * current_x_sum;
        current_y = y;
        tree.update(1, 0, nx - 1, ordered_x_lut[x1], ordered_x_lut[x2] - 1, op_cl);
        current_x_sum = tree.total(1);
    }
    return static_cast<int>(area % 1'000'000'007);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > rectangles,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = rectangleArea(rectangles);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0, 2, 2}, {1, 0, 2, 3}, {1, 0, 3, 1}}, 6, trials);
    test({{0, 0, 1000000000, 1000000000}}, 49, trials);
    return 0;
}


