#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findDelayedArrivalTime(int arrivalTime, int delayedTime)
{
    return (arrivalTime + delayedTime) % 24;
}

// ############################################################################
// ############################################################################

void test(int arrivalTime, int delayedTime, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findDelayedArrivalTime(arrivalTime, delayedTime);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(15, 5, 20, trials);
    test(13, 11, 0, trials);
    return 0;
}


