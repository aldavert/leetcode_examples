#include "../common/common.hpp"
#include <iostream>

// ############################################################################
// ############################################################################

std::string longestPalindrome(std::string s)
{
    const int n = static_cast<int>(s.size());
    int left = 0, size = 1;
    auto process = [&](bool left_offset) -> void
    {
        for (int i = left_offset; i < n - 1; ++i)
        {
            if (s[i - left_offset] == s[i + 1])
            {
                const int m = std::min(i, n - i - !left_offset);
                int k;
                for (k = 1 + left_offset; k <= m; ++k)
                    if (s[i - k] != s[i + k + !left_offset])
                        break;
                if (int l = (k - 1) * 2 + 1 + !left_offset; l > size)
                {
                    size = l;
                    left = i - k + 1;
                }
            }
        }
    };
    process(true);
    process(false);
    return s.substr(left, size);
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::string> &solution, const std::string &query)
{
    const int n = static_cast<int>(solution.size());
    for (int i = 0; i < n; ++i)
        if (solution[i] == query)
            return true;
    return false;
}

void test(std::string s, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestPalindrome(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("babad", {"bab", "aba"}, trials);
    test("cbbd", {"bb"}, trials);
    test("aaaa", {"aaaa"}, trials);
    return 0;
}

