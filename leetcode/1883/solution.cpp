#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int minSkips(std::vector<int> dist, int speed, int hoursBefore)
{
    constexpr double INF = 1e7, EPS = 1e-9;
    const int n = static_cast<int>(dist.size());
    std::vector<std::vector<double> > dp(n + 1, std::vector<double>(n + 1, INF));
    dp[0][0] = 0;
    
    for (int i = 1; i <= n; ++i)
    {
        double d = dist[i - 1];
        dp[i][0] = std::ceil(dp[i - 1][0] + d / speed - EPS);
        for (int j = 1; j <= i; ++j)
            dp[i][j] = std::min(dp[i - 1][j - 1] + d / speed,
        std::ceil(dp[i - 1][j] + d / speed - EPS));
    }
    
    for (int j = 0; j <= n; ++j)
        if (dp[n][j] <= hoursBefore)
            return j;
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> dist,
          int speed,
          int hoursBefore,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSkips(dist, speed, hoursBefore);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2}, 4, 2, 1, trials);
    test({7, 3, 5, 5}, 2, 10, 2, trials);
    test({7, 3, 5, 5}, 1, 10, -1, trials);
    return 0;
}


