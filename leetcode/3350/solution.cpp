#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int maxIncreasingSubarrays(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = 0;
    for (int i = 1, inc = 1, prev = 0; i < n; ++i)
    {
        if (nums[i] > nums[i - 1]) ++inc;
        else prev = std::exchange(inc, 1);
        result = std::max(std::max(result, inc / 2), std::min(prev, inc));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxIncreasingSubarrays(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 5, 7, 8, 9, 2, 3, 4, 3, 1}, 3, trials);
    test({1, 2, 3, 4, 4, 4, 4, 5, 6, 7}, 2, trials);
    return 0;
}


