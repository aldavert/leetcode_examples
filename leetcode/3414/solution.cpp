#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<int> maximumWeight(std::vector<std::vector<int> > intervals)
{
    const int n = static_cast<int>(intervals.size());
    constexpr int MAX = 1'000'000;
    for (int i = 0; i < n; ++i)
        intervals[i].push_back(-i);
    std::sort(intervals.begin(), intervals.end());
    
    int next[50'001] = {};
    auto cmp = [](const auto &tup, int value) -> bool { return tup[0] < value; };
    for (int i = 0; i < n; i++)
    {
        next[i] = -1;
        auto it = std::lower_bound(intervals.begin(), intervals.end(),
                                   intervals[i][1] + 1, cmp);
        if (it != intervals.end())
            next[i] = static_cast<int>(std::distance(intervals.begin(), it));
    }
    std::array<long long, 5> dp[4][50'001];
    for (int i = n - 1; i >= 0; --i)
    {
        dp[0][i] = { intervals[i][2], intervals[i][3], -MAX, -MAX, -MAX };
        if ((i < n - 1) && (dp[0][i + 1] > dp[0][i]))
            dp[0][i] = dp[0][i+1];
    }
    for (int j = 2; j <= 4; ++j)
    {
        for (int i = n - 1; i >= 0; --i)
        {
            if (next[i] != -1)
            {
                dp[j - 1][i] = dp[j - 2][next[i]];
                dp[j - 1][i][j] = intervals[i][3];
                dp[j - 1][i][0] += intervals[i][2];
                std::sort(dp[j-1][i].begin() + 1, dp[j-1][i].end(), std::greater<int>());
            }
            else dp[j - 1][i] = dp[j - 2][i];
            if ((i < n - 1) && (dp[j - 1][i + 1] > dp[j - 1][i]))
                dp[j - 1][i] = dp[j - 1][i + 1];
        }
    }
    std::vector<int> result;
    for (int i = 1; i < 5; i++)
        if (dp[3][0][i] > -MAX) result.push_back(static_cast<int>(-dp[3][0][i]));
    return result;
}
#else
std::vector<int> maximumWeight(std::vector<std::vector<int> > intervals)
{
    struct Result
    {
        long weight;
        std::vector<int> selected;
        bool operator>(const Result &other) const
        {
            return (weight > other.weight)
                || ((weight == other.weight) && (selected < other.selected));
        }
    };
    struct Information
    {
        int left;
        int right;
        int weight;
        int index;
        bool operator<(const Information &other) const { return left < other.left; }
    };
    const int n = static_cast<int>(intervals.size());
    std::vector<Information> info;
    std::vector<std::vector<Result> > memo(n, std::vector<Result>(5));
    
    auto findFirstGreater = [&](int start_from, int right_boundary) -> int
    {
        int l = start_from, r = n;
        while (l < r)
        {
            const int m = (l + r) / 2;
            if (info[m].left > right_boundary) r = m;
            else l = m + 1;
        }
        return l;
    };
    auto dp = [&](auto &&self, int i, int quota) -> Result
    {
        if ((i == n) || (quota == 0)) return Result();
        if (memo[i][quota].weight > 0) return memo[i][quota];
        Result skip = self(self, i + 1, quota),
               next = self(self, findFirstGreater(i + 1, info[i].right), quota - 1);
        
        std::vector<int> new_selected = next.selected;
        new_selected.push_back(info[i].index);
        std::sort(new_selected.begin(), new_selected.end());
        Result pick(info[i].weight + next.weight, new_selected);
        return memo[i][quota] = (pick > skip)?pick:skip;
    };
    
    for (int i = 0; i < n; ++i)
        info.push_back({intervals[i][0], intervals[i][1], intervals[i][2], i});
    std::sort(info.begin(), info.end());
    return dp(dp, 0, 4).selected;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > intervals,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumWeight(intervals);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 3, 2}, {4, 5, 2}, {1, 5, 5}, {6, 9, 3}, {6, 7, 1}, {8, 9, 1}},
         {2, 3}, trials);
    test({{5, 8, 1}, {6, 7, 7}, {4, 7, 3}, {9, 10, 6}, {7, 8, 2}, {11, 14, 3},
          {3, 5, 5}}, {1, 3, 5, 6}, trials);
    return 0;
}


