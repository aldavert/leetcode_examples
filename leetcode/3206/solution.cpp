#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfAlternatingGroups(std::vector<int> colors)
{
    int result = 0;
    for (int i = 0, n = static_cast<int>(colors.size()); i < n; ++i)
        if ((colors[i] != colors[(i - 1 + n) % n]) && (colors[i] != colors[(i + 1) % n]))
            ++result;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> colors, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfAlternatingGroups(colors);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 1}, 0, trials);
    test({0, 1, 0, 0, 1}, 3, trials);
    return 0;
}


