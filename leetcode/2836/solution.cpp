#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

#if 1
long long getMaxFunctionValue(std::vector<int> receiver, long long k)
{
    int n = static_cast<int>(receiver.size());
    std::vector<long long> nxt[34], dis[34];
    for (int i = 0; i < 34; ++i)
    {
        nxt[i].resize(n);
        dis[i].resize(n);
    }
    for (int i = 0; i < n; ++i)
    {
        nxt[0][i] = receiver[i];
        dis[0][i] = i;
    }
    for (int b = 1; b < 34; ++b)
    {
        for (int i = 0; i < n; ++i)
        {
            nxt[b][i] = nxt[b - 1][nxt[b - 1][i]];
            dis[b][i] = dis[b - 1][i] + dis[b - 1][nxt[b - 1][i]];
        }
    }
    long long result = 0;
    ++k;
    for(int node = 0; node < n; node++)
    {
        long long curr = node, temp = 0;
        for (int b = 0; b < 34; ++b)
        {
            if (((1ll) << b) & k)
            {
                temp += dis[b][curr];
                curr = nxt[b][curr];
            }
        }
        result = std::max(temp, result);
    }
    return result;
}
#else
long long getMaxFunctionValue(std::vector<int> receiver, long long k)
{
    const int n = static_cast<int>(receiver.size());
    const int m = static_cast<int>(std::log2(k) + 1);
    long result = 0;
    std::vector<std::vector<int> > jump(n, std::vector<int>(m));
    std::vector<std::vector<long> > sum(n, std::vector<long>(m));
    
    for (int i = 0; i < n; ++i)
    {
        jump[i][0] = receiver[i];
        sum[i][0] = receiver[i];
    }
    for (int j = 1; j < m; ++j)
    {
        for (int i = 0; i < n; ++i)
        {
            jump[i][j] = jump[jump[i][j - 1]][j - 1];
            sum[i][j] = sum[i][j - 1] + sum[jump[i][j - 1]][j - 1];
        }
    }
    for (int i = 0; i < n; ++i)
    {
        long curr_sum = i;
        int curr_pos = i;
        for (int j = 0; j < m; ++j)
        {
            if (k >> j & 1)
            {
                curr_sum += sum[curr_pos][j];
                curr_pos = jump[curr_pos][j];
            }
        }
        result = std::max(result, curr_sum);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> receiver,
          long long k,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getMaxFunctionValue(receiver, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 0, 1}, 4, 6, trials);
    test({1, 1, 1, 2, 3}, 3, 10, trials);
    return 0;
}


