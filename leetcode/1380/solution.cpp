#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> luckyNumbers(std::vector<std::vector<int> > matrix)
{
    const int n = static_cast<int>(matrix.size());
    const int m = static_cast<int>(matrix[0].size());
    int maximum_columns[51] = {};
    bool minimum_rows[100'001] = {};
    for (int i = 0; i < n; ++i)
    {
        int minimum_row = matrix[i][0];
        for (int j = 0; j < m; ++j)
        {
            minimum_row = std::min(minimum_row, matrix[i][j]);
            maximum_columns[j] = std::max(maximum_columns[j], matrix[i][j]);
        }
        minimum_rows[minimum_row] = true;
    }
    for (int j = 0; j < m; ++j)
        if (minimum_rows[maximum_columns[j]])
            return {maximum_columns[j]};
    return {};
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_set<int> present_in_left;
    for (int value : left)
        present_in_left.insert(value);
    for (int value : right)
    {
        if (auto search = present_in_left.find(value);
            search != present_in_left.end())
            present_in_left.erase(search);
        else return false;
    }
    return present_in_left.empty();
}

void test(std::vector<std::vector<int> > matrix,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = luckyNumbers(matrix);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 7, 8}, {9, 11, 13}, {15, 16, 17}}, {15}, trials);
    test({{1, 10, 4, 2}, {9, 3, 8, 7}, {15, 16, 17, 12}}, {12}, trials);
    test({{7, 8}, {1, 2}}, {7}, trials);
    return 0;
}


