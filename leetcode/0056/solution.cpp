#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > merge(std::vector<std::vector<int> > intervals)
{
    std::vector<std::pair<int, int> > interval_pair;
    std::vector<std::vector<int> > result;
    
    for (const auto &current : intervals)
        interval_pair.push_back({ current[0], current[1] });
    std::sort(interval_pair.begin(), interval_pair.end());
    
    auto [current_begin, current_end] = interval_pair[0];
    for (auto [begin, end] : interval_pair)
    {
        if (begin <= current_end)
            current_end = std::max(current_end, end);
        else
        {
            result.push_back({current_begin, current_end});
            current_begin = begin;
            current_end = end;
        }
    }
    result.push_back({current_begin, current_end});
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > intervals,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = merge(intervals);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 3}, {2, 6}, {8, 10}, {15, 18}}, {{1, 6}, {8, 10}, {15, 18}}, trials);
    test({{1, 4}, {4, 5}}, {{1, 5}}, trials);
    test({{1, 4}, {2, 3}}, {{1, 4}}, trials);
    return 0;
}


