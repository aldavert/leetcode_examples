#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minDeletionSize(std::vector<std::string> strs)
{
    const size_t n = strs.size();
    const size_t m = strs[0].size();
    int result = 0;
    for (size_t i = 0; i < m; ++i)
    {
        char last = strs[0][i];
        for (size_t j = 1; j < n; ++j)
        {
            if (last > strs[j][i])
            {
                ++result;
                break;
            }
            last = strs[j][i];
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> strs, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDeletionSize(strs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"cba", "daf", "ghi"}, 1, trials);
    test({"a", "b"}, 0, trials);
    test({"zyx", "wvu", "tsr"}, 3, trials);
    return 0;
}


