#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> searchRange(std::vector<int> nums, int target)
{
    if (nums.empty()) return {-1, -1};
    auto begin = std::distance(nums.begin(),
                               std::lower_bound(nums.begin(), nums.end(), target));
    auto end = std::distance(nums.begin(),
                             std::upper_bound(nums.begin(), nums.end(), target)) - 1;
    if (begin <= end) return {static_cast<int>(begin), static_cast<int>(end)};
    else return {-1, -1};
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int target,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = searchRange(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 7, 7, 8, 8, 10}, 8, {3, 4}, trials);
    test({5, 7, 7, 8, 8, 10}, 6, {-1, -1}, trials);
    test({}, 0, {-1, -1}, trials);
    test({0, 1, 2, 3, 3, 3, 3, 5, 7, 7, 8, 8, 8, 8, 8, 10}, 8, {10, 14}, trials);
    test({0, 1, 2, 3, 3, 3, 3, 5, 7, 7, 8, 10}, 8, {10, 10}, trials);
    return 0;
}


