#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int numTriplets(std::vector<int> nums1, std::vector<int> nums2)
{
    auto countTriplets =
        [](const std::vector<int> &a, const std::vector<int> &b) -> int
    {
        std::unordered_map<int, int> count;
        int result = 0;
        
        for (int value_b : b) ++count[value_b];
        for (int value_a : a)
        {
            long target = static_cast<long>(value_a) * value_a;
            for (auto [value_b, freq] : count)
            {
                if (target % value_b > 0)
                    continue;
                auto search = count.find(static_cast<int>(target / value_b));
                if (search == count.end()) continue;
                if (target / value_b == value_b)
                    result += freq * (freq - 1);
                else
                    result += freq * search->second;
            }
        }
        return result / 2;
    };
    return countTriplets(nums1, nums2) + countTriplets(nums2, nums1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numTriplets(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({7, 4}, {5, 2, 8, 9}, 1, trials);
    test({1, 1}, {1, 1, 1}, 9, trials);
    test({7, 7, 8, 3}, {1, 2, 9, 7}, 2, trials);
    return 0;
}


