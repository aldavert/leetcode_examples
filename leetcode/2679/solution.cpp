#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int matrixSum(std::vector<std::vector<int> > nums)
{
    const int n_rows = static_cast<int>(nums.size()),
              n_cols = static_cast<int>(nums[0].size());
    int result = 0;
    for (auto &row : nums)
        std::sort(row.begin(), row.end());
    for (int j = 0; j < n_cols; ++j)
    {
        int max_value = 0;
        for (int i = 0; i < n_rows; ++i)
            max_value = std::max(max_value, nums[i][j]);
        result += max_value;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = matrixSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{7, 2, 1}, {6, 4, 2}, {6, 5, 3}, {3, 2, 1}}, 15, trials);
    test({{1}}, 1, trials);
    return 0;
}


