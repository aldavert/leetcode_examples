#include "../common/common.hpp"
#include <functional>
#include <semaphore.h>
#include <thread>

// ############################################################################
// ############################################################################

std::string output;
void printFoo(void) { output += "foo"; }
void printBar(void) { output += "bar"; }

class FooBar
{
private:
    int _n;
    sem_t fooSemaphore;
    sem_t barSemaphore;
public:
    FooBar(int n) : _n(n)
    {
        sem_init(&fooSemaphore, /*pshared=*/0, /*value=*/1);
        sem_init(&barSemaphore, /*pshared=*/0, /*value=*/0);
    }
    ~FooBar()
    {
        sem_destroy(&fooSemaphore);
        sem_destroy(&barSemaphore);
    }
    void foo(std::function<void()> printFoo)
    {
        for (int i = 0; i < _n; i++)
        {
            sem_wait(&fooSemaphore);
        	// printFoo() outputs "foo". Do not change or remove this line.
        	printFoo();
            sem_post(&barSemaphore);
        }
    }
    void bar(std::function<void()> printBar)
    {
        for (int i = 0; i < _n; i++)
        {
            sem_wait(&barSemaphore);
        	// printBar() outputs "bar". Do not change or remove this line.
        	printBar();
            sem_post(&fooSemaphore);
        }
    }
};

// ############################################################################
// ############################################################################

void test(int n, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        FooBar obj(n);
        output.clear();
        std::thread t1([&]() { obj.foo(printFoo); }),
                    t2([&]() { obj.bar(printBar); });
        t1.join();
        t2.join();
        result = output;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, "foobar", trials);
    test(2, "foobarfoobar", trials);
    return 0;
}


