#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
std::vector<std::vector<int> > findMatrix(std::vector<int> nums)
{
    std::unordered_map<int, int> histogram;
    int max_value = 0;
    for (size_t i = 0, n = nums.size(); i < n; ++i)
        max_value = std::max(max_value, ++histogram[nums[i]]);
    std::vector<std::vector<int> > result(max_value);
    for (auto begin = histogram.begin(), end = histogram.end(); begin != end; ++begin)
        for (int l = 0; l < begin->second; ++l)
            result[l].push_back(begin->first);
    return result;
}
#else
std::vector<std::vector<int> > findMatrix(std::vector<int> nums)
{
    int histogram[201] = {}, max_value = 0;
    for (int value : nums)
        max_value = std::max(max_value, ++histogram[value]);
    std::vector<std::vector<int> > result(max_value);
    for (int l = 0; l < max_value; ++l)
    {
        for (int i = 1; i <= 200; ++i)
        {
            if (histogram[i])
            {
                result[l].push_back(i);
                --histogram[i];
            }
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    for (size_t i = 0, n = left.size(); i < n; ++i)
    {
        if (left[i].size() != right[i].size())
            return false;
        std::vector<int> copy_left = left[i], copy_right = right[i];
        std::sort(copy_left.begin() , copy_left.end() );
        std::sort(copy_right.begin(), copy_right.end());
        if (copy_left != copy_right) return false;
    }
    return true;
}

void test(std::vector<int> nums,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMatrix(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 4, 1, 2, 3, 1}, {{1, 3, 4, 2}, {1, 3}, {1}}, trials);
    test({1, 2, 3, 4}, {{4, 3, 2, 1}}, trials);
    return 0;
}


