#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool areAlmostEqual(std::string s1, std::string s2)
{
    const size_t n = s1.size();
    size_t first = s1.size(), second = s1.size();
    for (size_t i = 0; i < n; ++i)
    {
        if (s1[i] != s2[i])
        {
            if      (first  == n) first = i;
            else if (second == n) second = i;
            else return false;
        }
    }
    if (first == n) return true;
    if (second == n) return false;
    return (s1[first] == s2[second]) && (s1[second] == s2[first]);
}

// ############################################################################
// ############################################################################

void test(std::string s1, std::string s2, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = areAlmostEqual(s1, s2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("bank", "kanb", true, trials);
    test("attack", "defend", false, trials);
    test("kelb", "kelb", true, trials);
    return 0;
}


