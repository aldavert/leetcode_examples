#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * getTargetCopy(TreeNode * original,
                         TreeNode * cloned,
                         TreeNode * target)
{
    if (original)
    {
        if (original == target) return cloned;
        TreeNode * ptr = getTargetCopy(original->left, cloned->left, target);
        if (ptr) return ptr;
        return getTargetCopy(original->right, cloned->right, target);
    }
    else return nullptr;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int target_value, unsigned int trials = 1)
{
    auto search = [&](auto &&self, TreeNode * ptr, int value) -> TreeNode *
    {
        if (ptr)
        {
            if (ptr->val == value) return ptr;
            TreeNode * to_the_left = self(self, ptr->left, value);
            if (to_the_left) return to_the_left;
            return self(self, ptr->right, value);
        }
        return nullptr;
    };
    TreeNode * original = vec2tree(tree);
    TreeNode * cloned = vec2tree(tree);
    TreeNode * target = search(search, original, target_value);
    TreeNode * solution = search(search, cloned, target_value);
    TreeNode * result = nullptr;
    for (unsigned int i = 0; i < trials; ++i)
        result = getTargetCopy(original, cloned, target);
    delete original;
    delete cloned;
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({7, 4, 3, null, null, 6, 19}, 3, trials);
    test({7}, 7, trials);
    test({8, null, 6, null, 5, null, 4, null, 3, null, 2, null, 1}, 4, trials);
    return 0;
}


