#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int maximumGroups(std::vector<int> grades)
{
    const double n = static_cast<double>(grades.size());
    return static_cast<int>(std::sqrt(n * 2.0 + 0.25) - 0.5);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> grades, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumGroups(grades);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 6, 12, 7, 3, 5}, 3, trials);
    test({8, 8}, 1, trials);
    return 0;
}


