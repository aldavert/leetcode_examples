#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

ListNode * partition(ListNode * head, int x)
{
    ListNode * ptr_small_begin = nullptr, * ptr_small_end = nullptr;
    ListNode * ptr_big_begin = nullptr, * ptr_big_end = nullptr;
    ListNode * ptr = head;
    while (ptr != nullptr)
    {
        if (ptr->val < x)
        {
            if (!ptr_small_begin)
                ptr_small_begin = ptr_small_end = ptr;
            else
            {
                ptr_small_end->next = ptr;
                ptr_small_end = ptr;
            }
        }
        else
        {
            if (!ptr_big_begin)
                ptr_big_begin = ptr_big_end = ptr;
            else
            {
                ptr_big_end->next = ptr;
                ptr_big_end = ptr;
            }
        }
        ptr = ptr->next;
    }
    if (ptr_small_begin) ptr_small_end->next = ptr_big_begin;
    else ptr_small_begin = ptr_big_begin;
    if (ptr_big_end) ptr_big_end->next = nullptr;
    return ptr_small_begin;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> vec,
          int x,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(vec);
        head = partition(head, x);
        result = list2vec(head);
        delete head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 3, 2, 5, 2}, 3, {1, 2, 2, 4, 3, 5}, trials);
    test({2, 1}, 2, {1, 2}, trials);
    return 0;
}


