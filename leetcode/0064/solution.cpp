#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minPathSum(std::vector<std::vector<int> > grid)
{
    if ((grid.size() == 0) || (grid[0].size() == 0)) return 0;
    const int MAX = std::numeric_limits<int>::max();
    const int n = static_cast<int>(grid.size());
    const int m = static_cast<int>(grid[0].size());
    std::vector<std::vector<int> > result(n, std::vector<int>(m, MAX));
    result[0][0] = grid[0][0];
    for (int i = 1; i < m; ++i)
        result[0][i] = result[0][i - 1] + grid[0][i];
    for (int i = 1; i < n; ++i)
        result[i][0] = result[i - 1][0] + grid[i][0];
    for (int i = 1; i < n; ++i)
    {
        for (int j = 1; j < m; ++j)
        {
            result[i][j] = grid[i][j] + std::min(result[i - 1][j], result[i][j - 1]);
        }
    }
    return result[n - 1][m - 1];
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minPathSum(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 3, 1}, {1, 5, 1}, {4, 2, 1}}, 7, trials);
    test({{1, 2, 3}, {4, 5, 6}}, 12, trials);
    return 0;
}


