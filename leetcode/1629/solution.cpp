#include "../common/common.hpp"

// ############################################################################
// ############################################################################

char slowestKey(std::vector<int> releaseTimes, std::string keysPressed)
{
    int previous = 0;
    int maximum_time = 0;
    char result = '\0';
    for (size_t i = 0; i < releaseTimes.size(); ++i)
    {
        int dt = releaseTimes[i] - previous;
        if (dt > maximum_time)
        {
            maximum_time = dt;
            result = keysPressed[i];
        }
        else if ((dt == maximum_time) && (keysPressed[i] > result))
            result = keysPressed[i];
        previous = releaseTimes[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> releaseTimes,
          std::string keysPressed,
          char solution,
          unsigned int trials = 1)
{
    char result = '\0';
    for (unsigned int i = 0; i < trials; ++i)
        result = slowestKey(releaseTimes, keysPressed);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({9, 29, 49, 50}, "cbcd", 'c', trials);
    test({12, 23, 36, 46, 62}, "spuda", 'a', trials);
    return 0;
}


