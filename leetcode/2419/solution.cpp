#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestSubarray(std::vector<int> nums)
{
    int result = 0, max_index = 0, same_num_length = 0;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        if (nums[i] == nums[max_index])
            result = std::max(result, ++same_num_length);
        else if (nums[i] > nums[max_index])
        {
            max_index = i;
            same_num_length = 1;
            result = 1;
        }
        else same_num_length = 0;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestSubarray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 3, 2, 2}, 2, trials);
    test({1, 2, 3, 4}, 1, trials);
    return 0;
}


