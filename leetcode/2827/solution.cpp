#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

int numberOfBeautifulIntegers(int low, int high, int k)
{
    int f[11][21][21];
    std::memset(f, -1, sizeof(f));
    std::string s = std::to_string(high);
    auto dfs = [&](auto &&self, int pos, int mod, int diff, bool lead, bool limit) -> int
    {
        if (pos >= static_cast<int>(s.size()))
            return ((mod == 0) && (diff == 10));
        if (!lead && !limit && (f[pos][mod][diff] != -1))
            return f[pos][mod][diff];
        int result = 0;
        int up = limit ? s[pos] - '0' : 9;
        for (int i = 0; i <= up; ++i)
        {
            if (bool new_limit = limit && (i == up); (i == 0) && lead)
                result += self(self, pos + 1, mod, diff, true, new_limit);
            else
            {
                int nxt = diff + ((i % 2 == 1) ? 1 : -1);
                result += self(self, pos + 1, (mod * 10 + i) % k, nxt, false, new_limit);
            }
        }
        if (!lead && !limit)
            f[pos][mod][diff] = result;
        return result;
    };
    int a = dfs(dfs, 0, 0, 10, true, true);
    std::memset(f, -1, sizeof(f));
    s = std::to_string(low - 1);
    int b = dfs(dfs, 0, 0, 10, true, true);
    return a - b;
}

// ############################################################################
// ############################################################################

void test(int low, int high, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfBeautifulIntegers(low, high, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, 20, 3, 2, trials);
    test(1, 10, 1, 1, trials);
    test(5, 5, 2, 0, trials);
    return 0;
}


