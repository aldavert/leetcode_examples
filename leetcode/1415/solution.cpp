#include "../common/common.hpp"
#include <unordered_map>
#include <queue>

// ############################################################################
// ############################################################################

std::string getHappyString(int n, int k)
{
    const std::unordered_map<char, std::string> nextLetters {
        {'a', "bc"}, {'b', "ac"}, {'c', "ab"}};
    std::queue<std::string> q{{{"a", "b", "c"}}};
    
    while (static_cast<int>(q.front().size()) != n)
    {
        const std::string u = q.front();
        q.pop();
        for (const char next_letter : nextLetters.at(u.back()))
            q.push(u + next_letter);
    }
    
    if (static_cast<int>(q.size()) < k)
        return "";
    for (int i = 0; i < k - 1; ++i)
        q.pop();
    return q.front();
}

// ############################################################################
// ############################################################################

void test(int n, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getHappyString(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 3, "c", trials);
    test(1, 4, "", trials);
    test(3, 9, "cab", trials);
    return 0;
}


