#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

int distinctAverages(std::vector<int> nums)
{
    std::bitset<201> present;
    std::vector<int> work = nums;
    std::sort(work.begin(), work.end());
    int result = 0;
    for (int l = 0, r = static_cast<int>(work.size()) - 1; l < r; ++l, --r)
    {
        int key = work[l] + work[r];
        result += !present[key];
        present[key] = true;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = distinctAverages(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 1, 4, 0, 3, 5}, 2, trials);
    test({1, 100}, 1, trials);
    return 0;
}


