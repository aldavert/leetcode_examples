#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumWhiteTiles(std::vector<std::vector<int> > tiles, int carpetLen)
{
    if (std::any_of(tiles.begin(), tiles.end(),
                [&](const auto &tile) { return tile[1] - tile[0] + 1 >= carpetLen; }))
        return carpetLen;
    const int n = static_cast<int>(tiles.size());
    std::vector<int> starts, prefix(n + 1);
    
    std::sort(tiles.begin(), tiles.end());
    for (const auto &tile : tiles)
        starts.push_back(tile[0]);
    for (int i = 0; i < n; ++i)
        prefix[i + 1] = prefix[i] + tiles[i][1] - tiles[i][0] + 1;
    int result = 0;
    for (int i = 0; i < n; ++i)
    {
        int s = tiles[i][0],
            carpet_end = s + carpetLen - 1,
            end_idx = static_cast<int>(std::upper_bound(starts.begin(), starts.end(), 
                                       carpet_end) - starts.begin() - 1),
            not_cover = std::max(0, tiles[end_idx][1] - carpet_end);
        result = std::max(result, prefix[end_idx + 1] - prefix[i] - not_cover);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > tiles,
          int carpetLen,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumWhiteTiles(tiles, carpetLen);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 5}, {10, 11}, {12, 18}, {20, 25}, {30, 32}}, 10, 9, trials);
    test({{10, 11}, {1, 1}}, 2, 2, trials);
    return 0;
}


