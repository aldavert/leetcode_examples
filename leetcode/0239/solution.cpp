#include "../common/common.hpp"
#include <deque>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> maxSlidingWindow(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> result(n - k + 1);
    struct Info
    {
        int value;
        int index;
    };
    std::deque<Info> buffer;
    
    for (int i = 0; i < k; ++i)
    {
        while (!buffer.empty() && (buffer.back().value < nums[i]))
            buffer.pop_back();
        buffer.push_back({ nums[i], i });
    }
    result[0] = buffer.front().value;
    for (int j = 1, i = k; i < n; ++i, ++j)
    {
        while (!buffer.empty() && (buffer.front().index < j)) buffer.pop_front();
        while (!buffer.empty() && (buffer.back().value < nums[i])) buffer.pop_back();
        buffer.push_back({ nums[i], i });
        result[j] = buffer.front().value;
    }
    return result;
}
#elif 0
std::vector<int> maxSlidingWindow(std::vector<int> nums, int k)
{
    struct Info
    {
        int value;
        int index;
    };
    Info buffer[100'000];
    int n = static_cast<int>(nums.size());
    std::vector<int> result(n - k + 1);
    int i = 0;
    int left = 0, right = 0;
    for (; i < k; ++i)
    {
        while ((right > 0) && (buffer[right - 1].value < nums[i]))
            --right;
        buffer[right++] = { nums[i], i };
    }
    result[0] = buffer[0].value;
    for (int j = 1; i < n; ++i, ++j)
    {
        while ((left < right) && (buffer[left].index < j)) ++left;
        while ((right > left) && (buffer[right - 1].value < nums[i])) --right;
        buffer[right++] = { nums[i], i };
        result[j] = buffer[left].value;
    }
    return result;
}
#else
std::vector<int> maxSlidingWindow(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> result;
    std::priority_queue<int> remove;
    std::priority_queue<int> maximum;
    result.reserve(n - k + 1);
    for (int i = 0; i < k; ++i)
        maximum.push(nums[i]);
    result.push_back(maximum.top());
    for (int i = k, j = 0; i < n; ++i, ++j)
    {
        maximum.push(nums[i]);
        remove.push(nums[j]);
        while (!remove.empty() && (remove.top() == maximum.top()))
        {
            maximum.pop();
            remove.pop();
        }
        result.push_back(maximum.top());
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSlidingWindow(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, -1, -3, 1, 5, 3, 1, 3, 6, 7}, 3, {3, 3, 1, 5, 5, 5, 3,  6, 7}, trials);
    test({1, 3, -1, -3, 5, 3, 6, 7}, 3, {3, 3, 5, 5, 6, 7}, trials);
    test({1}, 1, {1}, trials);
    return 0;
}


