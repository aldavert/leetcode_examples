#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::bitset<51> found;
    int number_found = 0;
    for (int i = n - 1; i >= 0; --i)
    {
        if ((nums[i] <= k) && !found[nums[i]])
        {
            ++number_found;
            found[nums[i]] = true;
            if (number_found == k)
                return n - i;
        }
    }
    return n - 1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1, 5, 4, 2}, 2, 4, trials);
    test({3, 1, 5, 4, 2}, 5, 5, trials);
    test({3, 2, 5, 3, 1}, 3, 4, trials);
    return 0;
}


