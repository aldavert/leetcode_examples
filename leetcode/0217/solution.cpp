#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool containsDuplicate(std::vector<int> nums)
{
    std::unordered_set<int> lut;
    for (int n : nums)
    {
        if (lut.find(n) != lut.end())
            return true;
        lut.insert(n);
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = containsDuplicate(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 1}, true, trials);
    test({1, 2, 3, 4}, false, trials);
    test({1, 1, 1, 3, 3, 4, 3, 2, 4, 2}, true, trials);
    return 0;
}


