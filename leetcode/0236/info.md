#Lowest Common Ancestor of a Binary Tree

Given a binary tree, find the lowest common ancestor (LCA) of two given nodes in the tree.

According to the definition of LCA on Wikipedia: "The lowest common ancestor is defined between two nodes `p` and `q` as the lowest node in `T` that has both `p` and `q` as descendants (where we allow **a node to be a descendant of itself**)."

#### Example 1:
> ```mermaid 
> graph TB
> A((3))-->B((5))
> A-->C((1))
> B-->D((6))
> B-->E((2))
> E-->F((7))
> E-->G((4))
> C-->H((0))
> C-->I((8))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [3, 5, 1, 6, 2, 0, 8, null, null, 7, 4], p = 5, q = 1`  
> *Output:* `3`  
> *Explanation:* The LCA of nodes `5` and `1` is `3`.

#### Example 2:
> ```mermaid 
> graph TB
> A((3))-->B((5))
> A-->C((1))
> B-->D((6))
> B-->E((2))
> E-->F((7))
> E-->G((4))
> C-->H((0))
> C-->I((8))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [3, 5, 1, 6, 2, 0, 8, null, null, 7, 4], p = 5, q = 4`  
> *Output:* `5`  
> *Explanation:* The LCA of nodes `5` and `4` is `5`, since a node can be a descendant of itself according to the LCA definition.

#### Example 3:
> *Input:* `root = [1, 2], p = 1, q = 2`  
> *Output:* `1`
 
#### Constraints:
- The number of nodes in the tree is in the range `[2, 10^5]`.
- `-10^9 <= Node.val <= 10^9`
- All `Node.val` are **unique**.
- `p != q`
- `p` and `q` will exist in the tree.


