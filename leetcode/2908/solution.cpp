#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int minimumSum(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> min_right(nums.size());
    for (int min_v = nums.back(), i = n - 1; i >= 0; --i)
        min_right[i] = std::exchange(min_v, std::min(min_v, nums[i]));
    int result = std::numeric_limits<int>::max();
    for (int i = 1, min_v = nums[0]; i < n - 1; ++i)
    {
        if ((nums[i] > min_v) && (nums[i] > min_right[i]))
            result = std::min(result, nums[i] + min_v + min_right[i]);
        min_v = std::min(min_v, nums[i]);
    }
    return (result < std::numeric_limits<int>::max())?result:-1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 6, 1, 5, 3}, 9, trials);
    test({5, 4, 8, 7, 10, 2}, 13, trials);
    test({6, 5, 4, 3, 4, 5}, -1, trials);
    return 0;
}


