# Maximum Difference between Node and Ancestor

Given the `root` of a binary tree, find the maximum value `v` for which there exist **different** nodes `a` and `b` where `v = |a.val - b.val|` and `a` is an ancestor of `b`.

A node `a` is an ancestor of `b` if either: any child of `a` is equal to `b` or any child of `a` is an ancestor of `b`.

#### Example 1:
> ```mermaid 
> graph TD;
> A((8))---B((3))
> B---C((1))
> B---D((6))
> D---E((4))
> D---F((7))
> A---G((10))
> G---EMPTY1(( ))
> G---H((14))
> H---I((13))
> H---EMPTY2(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef delete fill:#FFF, stroke-width:0px
> class EMPTY1,EMPTY2 delete;
> linkStyle 6,9 stroke-width:0px
> ```
> 
> *Input:* `root = [8, 3, 10, 1, 6, null, 14, null, null, 4, 7, 13]`  
> *Output:* `7`  
> *Explanation:* We have various ancestor-node differences, some of which are given below:
> ```
> |8 - 3| = 5
> |3 - 7| = 4
> |8 - 1| = 7
> |10 - 13| = 3
> ```
> Among all possible differences, the maximum value of `7` is obtained by `|8 - 1| = 7`.

#### Example 2:
> ```mermaid 
> graph TD;
> A((1))---EMPTY1(( ))
> A---B((2))
> B---EMPTY2(( ))
> B---C((0))
> C---D((3))
> C---EMPTY3(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef delete fill:#FFF, stroke-width:0px
> class EMPTY1,EMPTY2,EMPTY3 delete;
> linkStyle 0,2,5 stroke-width:0px
> ```
> *Input:* `root = [1, null, 2, null, 0, 3]`  
> *Output:* `3`
 
#### Constraints:
- The number of nodes in the tree is in the range `[2, 5000]`.
- `0 <= Node.val <= 10^5`


