#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int maxAncestorDiff(TreeNode * root)
{
    int result = 0;
    auto process = [&](auto &&self, TreeNode * node) -> std::pair<int, int>
    {
        if (!node)
            return {std::numeric_limits<int>::max(), std::numeric_limits<int>::lowest()};
        auto [min_left, max_left] = self(self, node->left);
        auto [min_right, max_right] = self(self, node->right);
        int min = std::min({min_left, min_right, node->val});
        int max = std::max({max_left, max_right, node->val});
        result = std::max({result, max - node->val, node->val - min});
        return {min, max};
    };
    process(process, root);
    return result;
}

// ############################################################################
// ############################################################################


void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = maxAncestorDiff(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 3, 10, 1, 6, null, 14, null, null, 4, 7, 13}, 7, trials);
    test({1, null, 2, null, 0, 3}, 3, trials);
    return 0;
}


