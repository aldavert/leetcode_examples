/**
 * @param {Function} fn
 * @return {Array}
 */
Array.prototype.groupBy = function(fn) {
    let l = {};
    for (let i of this)
    {
        l[fn(i)] = [];
    }
    for (let i of this)
    {
        l[fn(i)].push(i);
    }
    return l;
};

/**
 * [1,2,3].groupBy(String) // {"1":[1],"2":[2],"3":[3]}
 */
