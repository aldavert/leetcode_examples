#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int getWinner(std::vector<int> arr, int k)
{
    int maximum_value = arr[0];
    int maximum_frequency = 0;
    for (size_t i = 1, n = arr.size(); i < n; ++i)
    {
        if (arr[i] > maximum_value)
        {
            maximum_value = arr[i];
            maximum_frequency = 1;
        }
        else ++maximum_frequency;
        if (maximum_frequency == k) break;
    }
    return maximum_value;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getWinner(arr, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3, 5, 4, 6, 7}, 2, 5, trials);
    test({3, 2, 1}, 10, 3, trials);
    return 0;
}


