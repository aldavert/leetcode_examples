#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int binaryGap(int n)
{
    int result = 0;
    for (int pos = 0, previous = -1; n; ++pos, n >>= 1)
    {
        if (n & 1)
        {
            result = std::max(result, (previous > -1) * (pos - previous));
            previous = pos;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = binaryGap(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(22, 2, trials);
    test(8, 0, trials);
    test(5, 2, trials);
    return 0;
}

