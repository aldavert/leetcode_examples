#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minMutation(std::string start, std::string end, std::vector<std::string> bank)
{
    const int n = static_cast<int>(bank.size());
    std::vector<bool> available(n, true);
    auto distance = [](std::string geneL, std::string geneR) -> int
    {
        int result = 0;
        for (int i = 0; i < 8; ++i) result += (geneL[i] != geneR[i]);
        return result;
    };
    auto traverse = [&](auto &&self, std::string gene, int navailable) -> int
    {
        if (distance(gene, end) == 0) return 0;
        if (navailable == 0) return n + 1;
        int result = n + 1;
        for (int i = 0; i < n; ++i)
        {
            if (available[i] && (distance(gene, bank[i]) == 1))
            {
                available[i] = false;
                result = std::min(result, 1 + self(self, bank[i], navailable - 1));
                available[i] = true;
            }
        }
        return result;
    };
    int result = traverse(traverse, start, n);
    return (result < n + 1)?result:-1;
}

// ############################################################################
// ############################################################################

void test(std::string start,
          std::string end,
          std::vector<std::string> bank,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minMutation(start, end, bank);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("AACCGGTT", "AACCGGTA", {"AACCGGTA"}, 1, trials);
    test("AACCGGTT", "AAACGGTA", {"AACCGGTA","AACCGCTA","AAACGGTA"}, 2, trials);
    test("AAAAACCC", "AACCCCCC", {"AAAACCCC","AAACCCCC","AACCCCCC"}, 3, trials);
    test("AAAAACCC", "AACCCCCG", {"AAAACCCC","AAACCCCC","AACCCCCC"}, -1, trials);
    return 0;
}


