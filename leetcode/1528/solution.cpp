#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string restoreString(std::string s, std::vector<int> indices)
{
    std::string result(s.size(), '_');
    for (size_t i = 0, n = indices.size(); i < n; ++i)
        result[indices[i]] = s[i];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<int> indices,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = restoreString(s, indices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("codeleet", {4, 5, 6, 7, 0, 2, 1, 3}, "leetcode", trials);
    test("abc", {0, 1, 2}, "abc", trials);
    return 0;
}


