#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int findLength(std::vector<int> nums1, std::vector<int> nums2)
{
    const int n1 = static_cast<int>(nums1.size()) + 1;
    const int n2 = static_cast<int>(nums2.size()) + 1;
    int result = 0;
    for (int i = 1, dp[2][1002] = {}, prev = 0; i < n1; ++i, prev = !prev)
        for (int j = 1; j < n2; ++j)
            dp[prev][j] = (nums1[i - 1] == nums2[j - 1])?(dp[!prev][j - 1] + 1):0,
            result = std::max(result, dp[prev][j]);
    return result;
}
#else
int findLength(std::vector<int> nums1, std::vector<int> nums2)
{
    std::unordered_set<std::string> subarrays;
    const int n1 = static_cast<int>(nums1.size());
    for (int i = 0; i < n1; ++i)
        for (int j = i; j < n1; ++j)
            subarrays.insert(std::string(&nums1[i], &nums1[j + 1]));
    const int n2 = static_cast<int>(nums2.size());
    int result = 0;
    for (int i = 0; i < n2 - result; ++i)
    {
        for (int j = i + result; j < n2; ++j)
        {
            std::string search(std::string(&nums2[i], &nums2[j + 1]));
            if (subarrays.find(search) != subarrays.end())
                result = std::max(result, j - i + 1);
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLength(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 2, 1}, {3, 2, 1, 4, 7}, 3, trials);
    test({0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, 5, trials);
    return 0;
}


