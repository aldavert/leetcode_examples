#include "../common/common.hpp"
#include <bitset>
#include <list>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

#if 1
class MyHashSet
{
    std::bitset<1'000'001> m_hash;
public:
    MyHashSet(void) = default;
    void add(int key)
    {
        m_hash[key] = true;
    }
    void remove(int key)
    {
        m_hash[key] = false;
    }
    bool contains(int key)
    {
        return m_hash[key];
    }
};
#else
class MyHashSet
{
    std::array<std::list<int>, 256> m_buckets;
    std::hash<int> m_hash;
public:
    MyHashSet()
    {
    }
    void add(int key)
    {
        auto &current = m_buckets[m_hash(key) % 256];
        if (std::find(current.begin(), current.end(), key) == current.end())
            current.push_back(key);
    }
    void remove(int key)
    {
        auto &current = m_buckets[m_hash(key) % 256];
        if (auto s = std::find(current.begin(), current.end(), key); s != current.end())
            current.erase(s);
    }
    bool contains(int key)
    {
        auto &current = m_buckets[m_hash(key) % 256];
        return std::find(current.begin(), current.end(), key) != current.end();
    }
};
#endif

// ############################################################################
// ############################################################################

enum class OP { ADD = 1, CONTAINS = 2, REMOVE = 4 };

void test(std::vector<OP> ops,
          std::vector<int> input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    const size_t n = input.size();
    std::vector<int> result(n);
    for (unsigned int t = 0; t < trials; ++t)
    {
        MyHashSet hash;
        
        for (size_t i = 0; i < n; ++i)
        {
            result[i] = null;
            if      (ops[i] == OP::ADD)
                hash.add(input[i]);
            else if (ops[i] == OP::CONTAINS)
                result[i] = hash.contains(input[i]);
            else if (ops[i] == OP::REMOVE)
                hash.remove(input[i]);
            else throw std::invalid_argument("Unknown operation!");
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::ADD, OP::ADD, OP::CONTAINS, OP::CONTAINS, OP::ADD, OP::CONTAINS,
          OP::REMOVE, OP::CONTAINS},
         {1, 2, 1, 3, 2, 2, 2, 2},
         {null, null, true, false, null, true, null, false},
         trials);
    return 0;
}


