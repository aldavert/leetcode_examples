#include "../common/common.hpp"
#include <deque>

// ############################################################################
// ############################################################################

int minimumCoins(std::vector<int> prices)
{
    const int n = static_cast<int>(prices.size());
    int result = std::numeric_limits<int>::max();
    std::deque<std::pair<int, int> > min_q{{0, n}};
    for (int i = n - 1; i >= 0; --i)
    {
        while (!min_q.empty() && (min_q.front().second > (i + 1) * 2))
            min_q.pop_front();
        result = prices[i] + min_q.front().first;
        while (!min_q.empty() && (min_q.back().first >= result))
            min_q.pop_back();
        min_q.emplace_back(result, i);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> prices, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumCoins(prices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1, 2}, 4, trials);
    test({1, 10, 1, 1}, 2, trials);
    test({26, 18, 6, 12, 49, 7, 45, 45}, 39, trials);
    return 0;
}


