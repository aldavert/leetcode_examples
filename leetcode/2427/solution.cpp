#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
int commonFactors(int a, int b)
{
    const int n = std::gcd(a, b);
    int result = 1 + (n != 1);
    for(int i = 2; i < n; ++i)
        result += (a % i == 0) && (b % i == 0);
    return result;
}
#else
int commonFactors(int a, int b)
{
    int result = 0;
    if (a < b) std::swap(a, b);
    for (int i = 1; i <= b; ++i)
        result += (a % i == 0) && (b % i == 0);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int a, int b, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = commonFactors(a, b);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(12, 6, 4, trials);
    test(25, 30, 2, trials);
    test(1000, 434, 2, trials);
    test(1000, 435, 2, trials);
    test(1000, 400, 12, trials);
    test(1000, 750, 8, trials);
    test(1000, 900, 9, trials);
    return 0;
}


