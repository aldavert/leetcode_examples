#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maximumTripletValue(std::vector<int> nums)
{
    long result = 0;
    for (int max_diff = 0, max_num = 0; const int num : nums)
    {
        result = std::max(result, static_cast<long>(max_diff) * num);
        max_diff = std::max(max_diff, max_num - num);
        max_num = std::max(max_num, num);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumTripletValue(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({12, 6, 1, 2, 7}, 77, trials);
    test({1, 10, 3, 4, 19}, 133, trials);
    test({1, 2, 3}, 0, trials);
    return 0;
}


