#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

long long maxPower(std::vector<int> stations, int r, int k)
{
    long long left = *std::min_element(stations.begin(), stations.end());
    long long right = std::accumulate(stations.begin(), stations.end(), 0LL) + k + 1;
    const int n = static_cast<int>(stations.size());
    auto check = [&](std::vector<int> current_stations,
                     long additional_stations,
                     long min_power) -> bool
    {
        long long power = std::accumulate(current_stations.begin(),
                                          current_stations.begin() + r, 0LL);
        for (int i = 0; i < n; ++i)
        {
            if (i + r < n) power += current_stations[i + r];
            if (power < min_power)
            {
                long long required_power = min_power - power;
                if (required_power > additional_stations) return false;
                current_stations[std::min(n - 1, i + r)] +=
                                                static_cast<int>(required_power);
                additional_stations -= required_power;
                power += required_power;
            }
            if (i - r >= 0) power -= current_stations[i - r];
        }
        return true;
    };
    while (left < right)
    {
        long long mid = (left + right) / 2;
        if (check(stations, k, mid)) left = mid + 1;
        else right = mid;
    }
    return left - 1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> stations,
          int r,
          int k,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxPower(stations, r, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 4, 5, 0}, 1, 2, 5, trials);
    test({4, 4, 4, 4}, 0, 3, 4, trials);
    return 0;
}


