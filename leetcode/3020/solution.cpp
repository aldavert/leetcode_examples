#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int maximumLength(std::vector<int> nums)
{
    const int maxnum = *std::max_element(nums.begin(), nums.end());
    std::unordered_map<long, int> count;
    
    for (int num : nums) ++count[num];
    int result = count.contains(1)?count[1] - (count[1] % 2 == 0):1;
    for (const int num : nums)
    {
        if (num == 1) continue;
        int length = 0;
        long x = num;
        while ((x <= maxnum) && count.contains(x) && (count[x] >= 2))
        {
            x *= x;
            length += 2;
        }
        result = std::max(result, length + 2 * count.contains(x) - 1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumLength(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 1, 2, 2}, 3, trials);
    test({1, 3, 2, 4}, 1, trials);
    return 0;
}


