#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

long long countBadPairs(std::vector<int> nums)
{
    std::unordered_map<int, int> count;
    long result = 0;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        result += i - count[nums[i] - i]++;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countBadPairs(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 1, 3, 3}, 5, trials);
    test({1, 2, 3, 4, 5}, 0, trials);
    return 0;
}


