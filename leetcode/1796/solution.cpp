#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int secondHighest(std::string s)
{
    int largest = -1, second_largest = -1;
    for (char c : s)
    {
        if ((c >= '0') && (c <= '9'))
        {
            int value = c - '0';
            if (value == largest) continue;
            if (value > largest)
                second_largest = std::exchange(largest, value);
            else if (value > second_largest)
                second_largest = value;
        }
    }
    return second_largest;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = secondHighest(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("dfa12321afd", 2, trials);
    test("abc1111", -1, trials);
    return 0;
}


