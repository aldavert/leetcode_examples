#include "../common/common.hpp"
#include <unordered_map>
#include <utility>

// ############################################################################
// ############################################################################

int scoreOfStudents(std::string s, std::vector<int> answers)
{
    const int n = static_cast<int>(s.size()),
              m = n / 2 + 1;
    std::vector<std::vector<std::unordered_set<int> > > dp(m,
            std::vector<std::unordered_set<int> >(m));
    
    for (int i = 0; i < m; ++i)
        dp[i][i].insert(s[i * 2] - '0');
    for (int d = 1; d < m; ++d)
        for (int i = 0; i + d < m; ++i)
            for (int k = i, j = i + d; k < j; ++k)
                for (char op = s[k * 2 + 1]; int a : dp[i][k])
                    for (int b : dp[k + 1][j])
                        if (int res = (op == '+')?(a + b):(a * b); res <= 1000)
                            dp[i][j].insert(res);
    
    int correct_answer = 0, prev_num = 0;
    for (int i = 0, curr_num = 0, op = '+'; i < n; ++i)
    {
        const char c = s[i];
        if (std::isdigit(c)) curr_num = curr_num * 10 + (c - '0');
        if (!std::isdigit(c) || (i == n - 1))
        {
            if      (op == '+') correct_answer += std::exchange(prev_num, curr_num);
            else if (op == '*') prev_num = prev_num * curr_num;
            op = c;
            curr_num = 0;
        }
    }
    correct_answer += prev_num;
    
    int result = 0;
    std::unordered_map<int, int> count;
    for (int answer : answers)
        ++count[answer];
    for (const auto& [answer, freq] : count)
    {
        if (answer == correct_answer)
            result += 5 * freq;
        else if (dp[0][m - 1].find(answer) != dp[0][m - 1].end())
            result += 2 * freq;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<int> answers,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = scoreOfStudents(s, answers);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("7+3*1*2", {20, 13, 42}, 7, trials);
    test("3+5*2", {13, 0, 10, 13, 13, 16, 16}, 19, trials);
    test("6+0*1", {12, 9, 6, 4, 8, 6}, 10, trials);
    return 0;
}


