#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<int> goodDaysToRobBank(std::vector<int> security, int time)
{
    const int n = static_cast<int>(security.size());
    if (2 * time + 1 > n) return {};
    std::vector<std::pair<int, int> > dp(n, {0, 0});
    for (int i = 0, p = 0; i < n; )
    {
        for (i = p + 1; i < n && security[i] <= security[i - 1]; ++i)
            dp[i].first = i - p;
        p = i;
    }
    for (int p = n - 1, i = n - 1; i >= 0; )
    {
        for (i = p - 1; i >= 0 && security[i] <= security[i + 1]; --i)
            dp[i].second = p - i;
        p = i;
    }
    std::vector<int> result;
    for (int i = time; i < n - time; ++i)
        if (dp[i].first >= time && dp[i].second >= time) result.push_back(i);
    return result;
}
#else
std::vector<int> goodDaysToRobBank(std::vector<int> security, int time)
{
    const int n = static_cast<int>(security.size());
    std::vector<int> result, dec(n), inc(n);
    
    for (int i = 1; i < n; ++i)
        if (security[i - 1] >= security[i])
            dec[i] = dec[i - 1] + 1;
    for (int i = n - 2; i >= 0; --i)
        if (security[i] <= security[i + 1])
            inc[i] = inc[i + 1] + 1;
    for (int i = 0; i < n; ++i)
        if (dec[i] >= time && inc[i] >= time)
            result.push_back(i);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> security,
          int time,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = goodDaysToRobBank(security, time);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 3, 3, 3, 5, 6, 2}, 2, {2, 3}, trials);
    test({1, 1, 1, 1, 1}, 0, {0, 1, 2, 3, 4}, trials);
    test({1, 2, 3, 4, 5, 6}, 2, {}, trials);
    return 0;
}


