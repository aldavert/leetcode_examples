#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> rearrangeBarcodes(std::vector<int> barcodes)
{
    const int n = static_cast<int>(barcodes.size());
    std::vector<int> result(n);
    int count[10001] = {};
    
    int i = 0, select_max = 0, select_pos = 0;
    for (int b : barcodes)
    {
        if (++count[b] > select_max)
        {
            select_max = count[b];
            select_pos = b;
        }
    }
    auto fillAns = [&](int num)
    {
        while (count[num]-- > 0)
        {
            result[i] = num;
            i = (i + 2 < n)?i + 2:1;
        }
    };
    fillAns(select_pos);
    for (int num = 1; num < 10001; ++num)
        fillAns(num);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> barcodes, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = rearrangeBarcodes(barcodes);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 1, 2, 2, 2}, {1, 2, 1, 2, 1, 2}, trials);
    test({1, 1, 1, 1, 2, 2, 3, 3}, {1, 2, 1, 2, 1, 3, 1, 3}, trials);
    return 0;
}


