#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> shuffle(std::vector<int> nums, int n)
{
    std::vector<int> result;
    result.reserve(n);
    for (int i = 0, j = n; i < n; ++i, ++j)
        result.push_back(nums[i]), result.push_back(nums[j]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int n,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = shuffle(nums, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 5, 1, 3, 4, 7}, 3, {2, 3, 5, 4, 1, 7}, trials);
    test({1, 2, 3, 4, 4, 3, 2, 1}, 4, {1, 4, 2, 3, 3, 2, 4, 1}, trials);
    test({1, 1, 2, 2}, 2, {1, 2, 1, 2}, trials);
    return 0;
}


