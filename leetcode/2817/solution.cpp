#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minAbsoluteDifference(std::vector<int> nums, int x)
{
    int result = std::numeric_limits<int>::max();
    std::set<int> seen;
    for (int i = x, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        seen.insert(nums[i - x]);
        const auto it = seen.lower_bound(nums[i]);
        if (it != seen.cend())
            result = std::min(result, *it - nums[i]);
        if (it != seen.cbegin())
            result = std::min(result, nums[i] - *prev(it));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int x, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minAbsoluteDifference(nums, x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 3, 2, 4}, 2, 0, trials);
    test({5, 3, 2, 10, 15}, 1, 1, trials);
    test({1, 2, 3, 4}, 3, 3, trials);
    return 0;
}


