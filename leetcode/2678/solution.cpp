#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countSeniors(std::vector<std::string> details)
{
    int result = 0;
    for (auto &id : details)
        result += ((static_cast<int>(id[11] - '0') * 10
                  + static_cast<int>(id[12] - '0')) > 60);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> details, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSeniors(details);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"7868190130M7522", "5303914400F9211", "9273338290F4010"}, 2, trials);
    test({"1313579440F2036", "2921522980M5644"}, 0, trials);
    return 0;
}

