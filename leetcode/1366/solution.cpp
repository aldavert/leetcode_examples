#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string rankTeams(std::vector<std::string> votes)
{
    struct Team
    {
        Team(char n, int team_size) : name(n), rank(team_size) {}
        char name = '\0';
        std::vector<int> rank;
        bool operator<(const Team &other) const
        {
            return (rank == other.rank)?name < other.name:rank > other.rank;
        }
    };
    const int team_size = static_cast<int>(votes[0].size());
    std::vector<Team> teams;
    
    for (int i = 0; i < 26; ++i)
        teams.push_back(Team(static_cast<char>('A' + i), team_size));
    for (const auto &vote : votes)
        for (int i = 0; i < team_size; ++i)
            ++teams[static_cast<int>(vote[i] - 'A')].rank[i];
    std::sort(teams.begin(), teams.end());
    
    std::string result;
    for (int i = 0; i < team_size; ++i)
        result += teams[i].name;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> votes, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = rankTeams(votes);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"ABC", "ACB", "ABC", "ACB", "ACB"}, "ACB", trials);
    test({"WXYZ", "XYZW"}, "XWYZ", trials);
    test({"ZMNAGUEDSJYLBOPHRQICWFXTVK"}, "ZMNAGUEDSJYLBOPHRQICWFXTVK", trials);
    return 0;
}


