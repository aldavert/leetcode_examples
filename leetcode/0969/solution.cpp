#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> pancakeSort(std::vector<int> arr)
{
    int n = static_cast<int>(arr.size());
    std::vector<int> result, indices(n);
    for (int i = 0; i < n; ++i) indices[i] = i + 1;
    std::sort(indices.begin(), indices.end(), [&arr](int i, int j)
                                              { return arr[i - 1] > arr[j - 1]; });
    for (int index : indices)
    {
        for (int flip : result)
            if (index <= flip)
                index = flip + 1 - index;
        result.push_back(index);
        result.push_back(n--);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = pancakeSort(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    //test({3, 2, 4, 1}, {4, 2, 4, 3}, trials);
    //test({1, 2, 3}, {}, trials);
    // These results are also valid:
    test({3, 2, 4, 1}, {3, 4, 2, 3, 1, 2, 1, 1}, trials);
    test({1, 2, 3}, {3, 3, 2, 2, 1, 1}, trials);
    return 0;
}


