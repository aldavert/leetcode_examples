#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countArrays(std::vector<int> original, std::vector<std::vector<int> > bounds)
{
    const int n = static_cast<int>(original.size());
    long l = bounds[0][0], r = bounds[0][1];
    for (int i = 1; (i < n) && (l <= r); ++i)
    {
        long d = original[i] - original[i - 1];
        l += d;
        r += d;
        l = std::max<long>(l, bounds[i][0]);
        r = std::min<long>(r, bounds[i][1]);
    }
    return static_cast<int>(std::max(0l, r - l + 1));
}

// ############################################################################
// ############################################################################

void test(std::vector<int> original,
          std::vector<std::vector<int> > bounds,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countArrays(original, bounds);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, {{1, 2}, {2, 3}, {3, 4}, {4, 5}}, 2, trials);
    test({1, 2, 3, 4}, {{1, 10}, {2, 9}, {3, 8}, {4, 7}}, 4, trials);
    test({1, 2, 1, 2}, {{1, 1}, {2, 3}, {3, 3}, {2, 3}}, 0, trials);
    return 0;
}


