#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int minimumHammingDistance(std::vector<int> source,
                           std::vector<int> target,
                           std::vector<std::vector<int> > allowedSwaps)
{
    const int n = static_cast<int>(source.size());
    int result = 0;
    std::vector<int> id(n), rank(n);
    for (int i = 0; i < n; ++i) id[i] = i;
    std::vector<std::unordered_map<int, int> > group_id_to_count(n);
    auto find = [&](auto &&self, int u) -> int
    {
        return (id[u] == u)?u:id[u] = self(self, id[u]);
    };
    auto merge = [&](int u, int v) -> void
    {
        int i = find(find, u), j = find(find, v);
        if (i == j) return;
        if (rank[i] < rank[j]) id[i] = j;
        else if (rank[i] > rank[j]) id[j] = i;
        else
        {
            id[i] = j;
            ++rank[j];
        }
    };
    
    for (const auto &allowed_swap : allowedSwaps)
        merge(allowed_swap[0], allowed_swap[1]);
    for (int i = 0; i < n; ++i)
        ++group_id_to_count[find(find, i)][source[i]];
    for (int i = 0; i < n; ++i)
    {
        std::unordered_map<int, int>& count = group_id_to_count[find(find, i)];
        if (!count.count(target[i]))
            ++result;
        else if (--count[target[i]] == 0)
            count.erase(target[i]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> source,
          std::vector<int> target,
          std::vector<std::vector<int> > allowedSwaps,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumHammingDistance(source, target, allowedSwaps);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, {2, 1, 4, 5}, {{0, 1}, {2, 3}}, 1, trials);
    test({1, 2, 3, 4}, {1, 3, 2, 4}, {}, 2, trials);
    test({5, 1, 2, 4, 3}, {1, 5, 4, 2, 3}, {{0, 4}, {4, 2}, {1, 3}, {1, 4}}, 0, trials);
    return 0;
}


