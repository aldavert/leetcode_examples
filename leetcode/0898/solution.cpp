#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int subarrayBitwiseORs(std::vector<int> arr)
{
    std::vector<int> subarray;
    for (int left = 0; int value : arr)
    {
        int right = static_cast<int>(subarray.size());
        subarray.push_back(value);
        for (int i = left; i < right; ++i)
            if (subarray.back() != (subarray[i] | value))
                subarray.push_back(subarray[i] | value);
        left = right;
    }
    return static_cast<int>(std::unordered_set<int>(subarray.begin(),
                                                    subarray.end()).size());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = subarrayBitwiseORs(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0}, 1, trials);
    test({1, 1, 2}, 3, trials);
    test({1, 2, 4}, 6, trials);
    return 0;
}


