#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

long long countGood(std::vector<int> nums, int k)
{
    std::unordered_map<int, int> count;
    long result = 0;
    for (int l = 0, pairs = 0, r = 0, n = static_cast<int>(nums.size()); r < n; ++r)
    {
        pairs += count[nums[r]]++;
        while (pairs >= k)
            pairs -= --count[nums[l++]];
        result += l;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countGood(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 1, 1, 1}, 10, 1, trials);
    test({3, 1, 4, 3, 2, 2, 4}, 2, 4, trials);
    return 0;
}


