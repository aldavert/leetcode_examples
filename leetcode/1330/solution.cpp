#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int maxValueAfterReverse(std::vector<int> nums)
{
    int total = 0, result = 0, min = std::numeric_limits<int>::max(),
        max = std::numeric_limits<int>::lowest(), n = static_cast<int>(nums.size());
    for (int i = 0; i < n - 1; ++i)
    {
        int a = nums[i], b = nums[i + 1];
        total += abs(a - b);
        result = std::max({result, std::abs(nums[0]     - b) - std::abs(a - b),
                                   std::abs(nums[n - 1] - a) - std::abs(a - b)});
        min = std::min(min, std::max(a, b));
        max = std::max(max, std::min(a, b));
    }
    return total + std::max(result, (max - min) * 2);
}
#else
int maxValueAfterReverse(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size()) - 1;
    int total = 0, min = std::numeric_limits<int>::max(),
        max = std::numeric_limits<int>::lowest();
    for (int i = 0; i < n; ++i)
        total += abs(nums[i] - nums[i + 1]),
        min = std::min(min, std::max(nums[i], nums[i + 1])),
        max = std::max(max, std::min(nums[i], nums[i + 1]));
    int diff = std::max(0, (max - min) * 2);
    for (int i = 0; i < n; ++i)
        diff = std::max({diff,
                         std::abs(nums.front() - nums[i + 1])
                       - std::abs(nums[i] - nums[i + 1]),
                         std::abs(nums.back()  - nums[i]    )
                       - std::abs(nums[i] - nums[i + 1])});
    return total + diff;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxValueAfterReverse(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 1, 5, 4}, 10, trials);
    test({2, 4, 9, 24, 2, 1, 10}, 68, trials);
    return 0;
}


