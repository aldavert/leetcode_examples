#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findPermutation(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<std::vector<int> > mem(n, std::vector<int>(1 << n));
    std::vector<std::vector<int> > best_pick(n, std::vector<int>(1 << n));
    auto calcScore = [&](auto &&self, int last, unsigned int mask) -> int
    {
        if (std::popcount(mask) == n) return std::abs(last - nums[0]);
        if (mem[last][mask] > 0) return mem[last][mask];
        int min_score = std::numeric_limits<int>::max();
        for (int i = 1; i < n; ++i)
        {
            if (mask >> i & 1) continue;
            int next_min_score = std::abs(last - nums[i]) + self(self, i, mask | 1 << i);
            if (next_min_score < min_score)
            {
                min_score = next_min_score;
                best_pick[last][mask] = i;
            }
        }
        return mem[last][mask] = min_score;
    };
    
    calcScore(calcScore, 0, 1);
    std::vector<int> result;
    for (int i = 0, last = 0, mask = 1; i < n; ++i)
    {
        result.push_back(last);
        last = best_pick[last][mask];
        mask |= 1 << last;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findPermutation(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 2}, {0, 1, 2}, trials);
    test({0, 2, 1}, {0, 2, 1}, trials);
    return 0;
}


