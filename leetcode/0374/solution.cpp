#include "../common/common.hpp"

static int& pick(void)
{
    static int number = 0;
    return number;
}
int guess(int num)
{
    return (num != pick()) * (2 * (num < pick()) - 1);
}

// ############################################################################
// ############################################################################

int guessNumber(int n)
{
    if (n == 1) return 1;
    int result = n / 2;
    for (int begin = 1, end = n, g = guess(result); g; g = guess(result))
    {
        if (g < 0) end = result - 1;
        else begin = result + 1;
        result = static_cast<int>((static_cast<long>(begin)
                                 + static_cast<long>(end)) / 2);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    pick() = solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = guessNumber(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, 6, trials);
    test(1, 1, trials);
    test(2, 1, trials);
    test(2, 2, trials);
    test(2126753390, 1702766719, trials);
    return 0;
}


