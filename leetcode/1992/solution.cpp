#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > findFarmland(std::vector<std::vector<int> > land)
{
    int n_rows = static_cast<int>(land.size()),
        n_cols = static_cast<int>(land[0].size());
    std::vector<std::vector<int> > result;
    for (int r = 0; r < n_rows; ++r)
    {
        for (int c = 0; c < n_cols; ++c)
        {
            if (land[r][c])
            {
                std::queue<std::tuple<int, int> > q;
                q.push({r, c});
                int top_x = 1'000, top_y = 1'000, bottom_x = -1, bottom_y = -1;
                while (!q.empty())
                {
                    auto [y, x] = q.front();
                    q.pop();
                    if ((y < 0) || (x < 0) || (x >= n_cols) || (y >= n_rows))
                        continue;
                    if (!land[y][x]) continue;
                    land[y][x] = 0;
                    top_x = std::min(top_x, x);
                    top_y = std::min(top_y, y);
                    bottom_x = std::max(bottom_x, x);
                    bottom_y = std::max(bottom_y, y);
                    q.push({y, x + 1});
                    q.push({y, x - 1});
                    q.push({y + 1, x});
                    q.push({y - 1, x});
                }
                result.push_back({top_y, top_x, bottom_y, bottom_x});
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    std::set<std::vector<int> > left_set(left.begin(), left.end());
    for (const auto &rectangle : right)
    {
        if (auto search = left_set.find(rectangle); search != left_set.end())
            left_set.erase(search);
        else return false;
    }
    return left_set.empty();
}

void test(std::vector<std::vector<int> > land,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findFarmland(land);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0, 0}, {0, 1, 1}, {0, 1, 1}}, {{0, 0, 0, 0}, {1, 1, 2, 2}}, trials);
    test({{1, 1}, {1, 1}}, {{0, 0, 1, 1}}, trials);
    test({{0}}, {}, trials);
    return 0;
}

