#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int minFallingPathSum(std::vector<std::vector<int> > matrix)
{
    const size_t n = matrix.size();
    if (n == 1) return matrix[0][0];
    std::vector<int> ledge(n, 0);
    for (size_t i = 0; i < n; ++i)
    {
        int prev = std::exchange(ledge[0], matrix[i][0]
                                         + std::min(ledge[0], ledge[1]));
        for (size_t j = 1; j < n - 1; ++j)
            prev = std::exchange(ledge[j], matrix[i][j]
                                         + std::min({prev, ledge[j], ledge[j + 1]}));
        ledge[n - 1] = matrix[i][n - 1] + std::min(prev, ledge[n - 1]);
    }
    return *std::min_element(ledge.begin(), ledge.end());
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int > > matrix, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minFallingPathSum(matrix);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 1, 3}, {6, 5, 4}, {7, 8, 9}}, 13, trials);
    test({{-19, 57}, {-40, -5}}, -59, trials);
    return 0;
}


