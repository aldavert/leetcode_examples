#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int minimumMountainRemovals(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    auto reversed = [](const std::vector<int> &vec) -> std::vector<int>
    {
        return {vec.rbegin(), vec.rend()};
    };
    auto lengthOfLIS = [&](const std::vector<int> &vec) -> std::vector<int>
    {
        std::vector<int> tail, dp(n);
        for (int i = 0; i < n; ++i)
        {
            const int num = vec[i];
            if ((tail.empty()) || (num > tail.back()))
                tail.push_back(num);
            else
            {
                int l = 0;
                int r = static_cast<int>(tail.size());
                while (l < r)
                {
                    if (int m = (l + r) / 2; tail[m] >= num) r = m;
                    else l = m + 1;
                }
                tail[l] = num;
            }
            dp[i] = static_cast<int>(tail.size());
        }
        return dp;
    };
    std::vector<int> l = lengthOfLIS(nums);
    std::vector<int> r = reversed(lengthOfLIS(reversed(nums)));
    int max_mountain_seq = 0;
    
    for (int i = 0; i < n; ++i)
        if ((l[i] > 1) && (r[i] > 1))
            max_mountain_seq = std::max(max_mountain_seq, l[i] + r[i] - 1);
    return n - max_mountain_seq;
}
#else
int minimumMountainRemovals(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int left_lis_len[1001] = {};
    std::vector<int> lis;
    for (int i = 0; i + 1 < n; ++i)
    {
        auto it = std::lower_bound(lis.begin(), lis.end(), nums[i]);
        left_lis_len[i] = static_cast<int>(std::distance(lis.begin(), it));
        if (it == lis.end()) lis.push_back(nums[i]);
        else *it = nums[i];
    }
    lis.clear();
    int max_len = 0;
    for (int i = n - 1; i > 0; --i)
    {
        auto it = std::lower_bound(lis.begin(), lis.end(), nums[i]);
        max_len = std::max(max_len, left_lis_len[i]
                                  + static_cast<int>(std::distance(lis.begin(), it)));
        if (it == lis.end()) lis.push_back(nums[i]);
        else *it = nums[i];
    }
    return n - (1 + max_len);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumMountainRemovals(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 1}, 0, trials);
    test({2, 1, 1, 5, 6, 2, 3, 1}, 3, trials);
    test({100, 92, 89, 77, 74, 66, 64, 66, 64}, 6, trials);
    return 0;
}


