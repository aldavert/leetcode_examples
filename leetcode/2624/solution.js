/**
 * @param {number} rowsCount
 * @param {number} colsCount
 * @return {Array<Array<number>>}
 */
Array.prototype.snail = function (rowsCount, colsCount) {
    if (rowsCount * colsCount !== this.length) return [];
    
    let res = [], counter = 0;
    for (let i = 0; i < rowsCount; i++) {
        res.push([]);
    }
    for (let j = 0; j < colsCount; j++) {
        for (let i = 0; i < rowsCount; i++) {
            res[j % 2 === 0 ? i : rowsCount - i - 1][j] = this[counter++];
        }
    }
    return res;
}

/**
 * const arr = [1,2,3,4];
 * arr.snail(1,4); // [[1,2,3,4]]
 */

