#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

#if 1
int firstMissingPositive(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    for (int &v : nums)
        if (v <= 0) v = n + 1;
    for (int v : nums)
    {
        int idx = std::abs(v) - 1;
        if (idx < n)
        {
            if (nums[idx] > 0) nums[idx] *= -1;
            else if (nums[idx] == 0) nums[idx] = -n - 1;
        }
    }
    for (int i = 0; i < n; ++i)
        if (nums[i] >= 0) return i + 1;
    return n + 1;
}
#else
int firstMissingPositive(std::vector<int> nums)
{
    std::unordered_set<int> present;
    int result = std::numeric_limits<int>::max();
    
    for (int n : nums)
        if (n > 0)
            present.insert(n);
    // If 1 is not present, that is the smallest number.
    if (present.find(1) == present.end())
        return 1;
    // Use the hash table to check if the next number is in the input array.
    for (int n : nums)
        if ((n > 0)
         && (n < std::numeric_limits<int>::max())
         && (present.find(n + 1) == present.end()))
            result = std::min(result, n + 1);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = firstMissingPositive(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 0}, 3, trials);
    test({3, 4, -1, 1}, 2, trials);
    test({7, 8, 9, 11, 12}, 1, trials);
    return 0;
}


