#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sumOfGoodNumbers(std::vector<int> nums, int k)
{
    int result = 0;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        bool left = (i - k < 0)
                 || ((i - k >= 0) && (nums[i - k] < nums[i]));
        bool right = (i + k >= n)
                  || ((i + k < n) && (nums[i + k] < nums[i]));
        if (left && right) result += nums[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOfGoodNumbers(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 1, 5, 4}, 2, 12, trials);
    test({2, 1}, 1, 2, trials);
    return 0;
}


