#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumScore(std::vector<int> nums, std::vector<std::vector<int> > edges)
{
    const int n_nums = static_cast<int>(nums.size()),
              n_edges = static_cast<int>(edges.size());
    std::vector<std::vector<int> > graph(n_nums);
    std::vector<int> x(n_nums), child(n_nums);
    int id = 0;
    auto process = [&](auto &&self, int node, int previous) -> int
    {
        int result = 0;
        for (int neighbor : graph[node])
        {
            if (neighbor == previous) continue;
            int current_id = id++;
            x[current_id] = nums[neighbor] ^ self(self, neighbor, node);
            child[current_id] = id - 1;
            result ^= x[current_id];
        }
        return result;
    };
    
    for (const auto &edge: edges)
    {
        graph[edge[0]].push_back(edge[1]);
        graph[edge[1]].push_back(edge[0]);
    }
    int r = nums[0] ^ process(process, 0, -1);
    int result = std::numeric_limits<int>::max();
    for (int i = 0; i < n_edges; ++i)
    {
        for (int j = i + 1; j < n_edges; ++j)
        {
            if (child[i] < child[j])
                result = std::min(result, std::max({r ^ x[i] ^ x[j], x[i], x[j]})
                                        - std::min({r ^ x[i] ^ x[j], x[i], x[j]}));
            else
                result = std::min(result, std::max({r ^ x[i], x[i] ^ x[j], x[j]})
                                        - std::min({r ^ x[i], x[i] ^ x[j], x[j]})); 
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::vector<int> > edges,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumScore(nums, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 5, 5, 4, 11}, {{0, 1}, {1, 2}, {1, 3}, {3, 4}}, 9, trials);
    test({5, 5, 2, 4, 4, 2}, {{0, 1}, {1, 2}, {5, 2}, {4, 3}, {1, 3}}, 0, trials);
    return 0;
}


