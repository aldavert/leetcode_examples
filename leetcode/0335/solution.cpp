#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isSelfCrossing(std::vector<int> distance)
{
    distance.insert(distance.begin(), 4, 0);
    const int n = static_cast<int>(distance.size());
    int i = 4;
    for(; (i < n) && (distance[i] > distance[i - 2]); ++i);
    if (i == n) return false;
    if (distance[i] >= distance[i - 2] - distance[i - 4])
        distance[i - 1] -= distance[i - 3];
    for (++i; (i < n) && (distance[i] < distance[i - 2]); ++i);
    return i != n;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> distance, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isSelfCrossing(distance);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 1, 2},  true, trials);
    test({1, 2, 3, 4}, false, trials);
    test({1, 1, 1, 1},  true, trials);
    return 0;
}


