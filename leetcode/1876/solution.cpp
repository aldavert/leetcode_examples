#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countGoodSubstrings(std::string s)
{
    const size_t n = s.size();
    if (n < 2) return 0;
    struct Histogram
    {
        int histogram[26] = {};
        int number_of_active_bins = 0;
        void add(char value)
        {
            const int index = static_cast<int>(value - 'a');
            number_of_active_bins += (histogram[index] == 0);
            ++histogram[index];
        }
        void remove(char value)
        {
            const int index = static_cast<int>(value - 'a');
            number_of_active_bins -= (histogram[index] == 1);
            --histogram[index];
        }
    };
    Histogram histogram;
    int result = 0;
    
    histogram.add(s[0]);
    histogram.add(s[1]);
    for (size_t i = 2; i < n; ++i)
    {
        histogram.add(s[i]);
        result += (histogram.number_of_active_bins == 3);
        histogram.remove(s[i - 2]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countGoodSubstrings(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("xyzzaz", 1, trials);
    test("aababcabc", 4, trials);
    return 0;
}


