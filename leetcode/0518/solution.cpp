#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int change(int amount, std::vector<int> coins)
{
    int dp[5002] = {};
    dp[0] = 1;
    for (int coin : coins)
        for (int i = coin; i <= amount; ++i)
            dp[i] += dp[i - coin];
    return dp[amount];
}

// ############################################################################
// ############################################################################

void test(int amount, std::vector<int> coins, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = change(amount, coins);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {1, 2, 5}, 4, trials);
    test(3, {2}, 0, trials);
    test(10, {10}, 1, trials);
    return 0;
}


