#include "../common/common.hpp"
#include <limits>

// ######################################################################################
// ######################################################################################

int smallestRangeI(std::vector<int> nums, int k)
{
    int value_min = std::numeric_limits<int>::max();
    int value_max = std::numeric_limits<int>::lowest();
    for (int v : nums)
    {
        if (v > value_max) value_max = v;
        if (v < value_min) value_min = v;
    }
    return std::max(0, value_max - value_min - 2 * k);
}

// ######################################################################################
// ######################################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestRangeI(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1}, 0, 0, trials);
    test({0, 10}, 2, 6, trials);
    test({1, 3, 6}, 3, 0, trials);
    test({3, 1, 10}, 4, 1, trials);
    test({9, 9, 2, 8, 7}, 4, 0, trials);
    return 0;
}


