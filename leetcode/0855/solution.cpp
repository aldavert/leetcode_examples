#include "../common/common.hpp"
#include <unordered_map>
#include <list>

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class ExamRoom
{
    int number_of_seats;
    std::list<int> students;
    std::unordered_map<int, std::list<int>::iterator> lut;
public:
    ExamRoom(int n) : number_of_seats(n)
    {
    }
    int seat(void)
    {
        if (students.empty())
        {
            students.push_back(0);
            lut[0] = students.begin();
            return 0;
        }
        int prev_student = -1, max_distance_to_closest = 0, val = 0;
        std::list<int>::iterator pos;
        for (auto it = students.begin(); it != students.end(); ++it)
        {
            if (prev_student == -1)
            {
                max_distance_to_closest = *it;
                pos = it;
            }
            else if ((*it - prev_student) / 2 > max_distance_to_closest)
            {
                max_distance_to_closest = (*it - prev_student) / 2;
                val = (*it + prev_student) / 2;
                pos = it;
            }
            prev_student = *it;
        }
        if (number_of_seats - 1 - students.back() > max_distance_to_closest)
        {
            pos = students.end();
            val = number_of_seats - 1;
        }
        lut[val] = students.insert(pos, val);
        return val;
    }
    void leave(int p)
    {
        students.erase(lut[p]);
    }
};

// ############################################################################
// ############################################################################

void test(int number_of_seats,
          std::vector<int> seat_id,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        ExamRoom object(number_of_seats);
        result.clear();
        for (auto id : seat_id)
        {
            if (id < 0) result.push_back(object.seat());
            else
            {
                result.push_back(null);
                object.leave(id);
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, {-1, -1, -1, -1, 4, -1}, {0, 9, 4, 2, null, 5}, trials);
    return 0;
}


