#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> sortEvenOdd(std::vector<int> nums)
{
    const size_t n = nums.size();
    std::vector<int> even_odd[2], result(n);
    for (size_t i = 0; i < n; ++i)
        even_odd[i & 1].push_back(nums[i]);
    std::sort(even_odd[0].begin(), even_odd[0].end());
    std::sort(even_odd[1].begin(), even_odd[1].end(), std::greater());
    for (size_t i = 0, m = even_odd[0].size(); i < m; ++i)
        result[2 * i] = even_odd[0][i];
    for (size_t i = 0, m = even_odd[1].size(); i < m; ++i)
        result[2 * i + 1] = even_odd[1][i];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sortEvenOdd(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 1, 2, 3}, {2, 3, 4, 1}, trials);
    test({4, 1, 2, 3, 0}, {0, 3, 2, 1, 4}, trials);
    test({2, 1}, {2, 1}, trials);
    return 0;
}


