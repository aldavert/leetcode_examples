#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<std::vector<int> > colorBorder(std::vector<std::vector<int> > grid,
                                           int row, int col, int color)
{
    auto process = [&]() -> void
    {
        constexpr int directions[] = {-1, 0, 1, 0, -1};
        const int n = static_cast<int>(grid.size()),
                  m = static_cast<int>(grid[0].size());
        bool vis[51][51] = {};
        auto inner = [&](auto &&self, int i, int j, int start_color) -> void
        {
            vis[i][j] = true;
            for (int k = 0; k < 4; ++k)
            {
                int x = i + directions[k], y = j + directions[k + 1];
                if ((x >= 0) && (x < n) && (y >= 0) && (y < m))
                {
                    if (!vis[x][y])
                    {
                        if (grid[x][y] == start_color)
                            self(self, x, y, start_color);
                        else grid[i][j] = color;
                    }
                }
                else grid[i][j] = color;
            }
        };
        inner(inner, row, col, grid[row][col]);
    };
    process();
    return grid;
}
#else
std::vector<std::vector<int> > colorBorder(std::vector<std::vector<int> > grid,
                                           int row, int col, int color)
{
    auto process = [&]() -> void
    {
        const int n = static_cast<int>(grid.size()),
                  m = static_cast<int>(grid[0].size());
        auto inner = [&](auto &&self, int i, int j, int start_color) -> void
        {
            if ((i < 0) || (i == n) || (j < 0) || (j == m)) return;
            if (grid[i][j] != start_color) return;
            grid[i][j] = -start_color;
            self(self, i + 1, j, start_color);
            self(self, i - 1, j, start_color);
            self(self, i, j + 1, start_color);
            self(self, i, j - 1, start_color);
            if ((i == 0) || (i == n - 1) || (j == 0) || (j == m - 1))
                return;
            if ((std::abs(grid[i + 1][j]) == start_color)
            &&  (std::abs(grid[i - 1][j]) == start_color)
            &&  (std::abs(grid[i][j + 1]) == start_color)
            &&  (std::abs(grid[i][j - 1]) == start_color))
                grid[i][j] = start_color;
        };
        inner(inner, row, col, grid[row][col]);
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m; ++j)
                if (grid[i][j] < 0)
                    grid[i][j] = color;
    };
    process();
    return grid;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          int row,
          int col,
          int color,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = colorBorder(grid, row, col, color);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1}, {1, 2}}, 0, 0, 3, {{3, 3}, {3, 2}}, trials);
    test({{1, 2, 2}, {2, 3, 2}}, 0, 1, 3, {{1, 3, 3}, {2, 3, 3}}, trials);
    test({{1, 1, 1}, {1, 1, 1}, {1, 1, 1}}, 1, 1, 2,
         {{2, 2, 2}, {2, 1, 2}, {2, 2, 2}}, trials);
    return 0;
}


