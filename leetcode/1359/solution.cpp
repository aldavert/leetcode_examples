#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countOrders(int n)
{
    constexpr int mod = 1e9 + 7;
    long result = 1;
    for (int i = 1; i <= n; ++i)
        result = result * i * (i * 2 - 1) % mod;
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = 0;
    for (unsigned int i = 0; i < trials; ++i)
        result = countOrders(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 1, trials);
    test(2, 6, trials);
    test(3, 90, trials);
    return 0;
}


