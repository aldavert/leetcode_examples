#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int countPairs(std::vector<int> nums)
{
    std::unordered_map<int, int> count;
    auto calcSwaps = [&](const std::string &digits) -> std::unordered_set<int>
    {
        const int n = static_cast<int>(digits.length());
        std::unordered_set<int> swaps{{std::stoi(digits)}};
        for (int i = 0; i < n; ++i)
        {
            for (int j = i + 1; j < n; ++j)
            {
                std::string new_digits = digits;
                std::swap(new_digits[i], new_digits[j]);
                swaps.insert(std::stoi(new_digits));
            }
        }
        return swaps;
    };
    int max_element = *std::max_element(nums.begin(), nums.end()),
        max_len = static_cast<int>(std::to_string(max_element).size()),
        result = 0;
    for (const int num : nums)
    {
        const int m = static_cast<int>(std::to_string(num).size());
        std::string digits = std::string(max_len - m, '0') + std::to_string(num);
        for (const int swap : calcSwaps(digits))
            result += count[swap];
        ++count[num];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPairs(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 12, 30, 17, 21}, 2, trials);
    test({1, 1, 1, 1, 1}, 10, trials);
    test({123, 231}, 0, trials);
    return 0;
}


