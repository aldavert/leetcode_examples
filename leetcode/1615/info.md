# Maximal Network Rank

There is an infrastructure of `n` cities with some number of `roads` connecting these cities. Each `roads[i] = [a_i, b_i]` indicates that there is a bidirectional road between cities `a_i` and `b_i`.

The **network rank** of **two different cities** is defined as the total number of **directly** connected roads to **either** city. If a road is directly connected to both cities, it is only counted **once**.

The **maximal network rank** of the infrastructure is the **maximum network rank** of all pairs of different cities.

Given the integer `n` and the array `roads`, return *the* ***maximal network rank*** *of the entire infrastructure.*

#### Example 1:
> ```mermaid
> graph LR;
> A((0))---B((1))
> B---C((2))
> A---D((3))
> B---D
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#AFA,stroke:#494,stroke-width:2px;
> class A,B selected;
> linkStyle 0,1,2,3 stroke:#494,stroke-width:2px
> ```
> *Input:* `n = 4, roads = [[0, 1], [0, 3], [1, 2], [1, 3]]`  
> *Output:* `4`  
> *Explanation:* The network rank of cities `0` and `1` is `4` as there are `4` roads that are connected to either `0` or `1`. The road between `0` and `1` is only counted once.

#### Example 2:
> ```mermaid
> graph LR;
> A((0))---B((1))
> B---C((2))
> A---D((3))
> B---D
> C---D
> C---E((4))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#AFA,stroke:#494,stroke-width:2px;
> class B,C selected;
> linkStyle 0,1,3,4,5 stroke:#494,stroke-width:2px
> ```
> *Input:* `n = 5, roads = [[0, 1], [0, 3], [1, 2], [1, 3], [2, 3], [2, 4]]`  
> *Output:* `5`  
> *Explanation:* There are `5` roads that are connected to cities `1` or `2`.

#### Example 3:
> ```mermaid
> graph LR;
> A((0))---B((1))
> B---C((2))
> C---D((3))
> C---E((4))
> F((5))---G((6))
> F---H((7))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#AFA,stroke:#494,stroke-width:2px;
> class C,F selected;
> linkStyle 1,2,3,4,5 stroke:#494,stroke-width:2px
> ```
> *Input:* `n = 8, roads = [[0, 1], [1, 2], [2, 3], [2, 4], [5, 6], [5, 7]]`  
> *Output:* `5`  
> *Explanation:* The network rank of `2` and `5` is `5`. Notice that all the cities do not have to be connected.

#### Constraints:
- `2 <= n <= 100`
- `0 <= roads.length <= n * (n - 1) / 2`
- `roads[i].length == 2`
- `0 <= a_i, b_i <= n-1`
- `a_i != b_i`
- Each pair of cities has **at most one** road connecting them.


