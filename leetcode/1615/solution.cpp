#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int maximalNetworkRank(int n, std::vector<std::vector<int> > roads)
{
    std::vector<std::vector<bool> > adjacency_matrix(n, std::vector<bool>(n));
    std::vector<int> out_degree(n);
    for (const auto &edge : roads)
    {
        ++out_degree[edge[0]];
        ++out_degree[edge[1]];
        adjacency_matrix[edge[0]][edge[1]] = adjacency_matrix[edge[1]][edge[0]] = true;
    }
    int result = 0;
    for (int i = 0; i < n; ++i)
        for (int j = i + 1; j < n; ++j)
            result = std::max(result, out_degree[i] + out_degree[j]
                                    - adjacency_matrix[i][j]);
    return result;
}
#else
int maximalNetworkRank(int n, std::vector<std::vector<int> > roads)
{
    std::vector<std::unordered_set<int> > graph(n);
    for (const auto &edge : roads)
    {
        graph[edge[0]].insert(edge[1]);
        graph[edge[1]].insert(edge[0]);
    }
    size_t result = 0;
    for (int i = 0; i < n; ++i)
        for (int j = i + 1; j < n; ++j)
            result = std::max(result, graph[i].size() + graph[j].size()
                                    - (graph[i].find(j) != graph[i].end()));
    return static_cast<int>(result);
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > roads,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximalNetworkRank(n, roads);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{0, 1}, {0, 3}, {1, 2}, {1, 3}}, 4, trials);
    test(5, {{0, 1}, {0, 3}, {1, 2}, {1, 3}, {2, 3}, {2, 4}}, 5, trials);
    test(8, {{0, 1}, {1, 2}, {2, 3}, {2, 4}, {5, 6}, {5, 7}}, 5, trials);
    return 0;
}


