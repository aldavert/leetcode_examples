#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > rotateGrid(std::vector<std::vector<int> > grid, int k)
{
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    for (int t = 0, l = 0, b = m - 1, r = n - 1; (t < b) && (l < r); ++t, ++l, --b, --r)
    {
        const int n_elements_layer = 2 * (b - t + 1) + 2 * (r - l + 1) - 4;
        for (int rot = 0, stop = k % n_elements_layer; rot < stop; ++rot)
        {
            const int top_left = grid[t][l];
            for (int j = l; j < r; ++j) grid[t][j] = grid[t][j + 1];
            for (int i = t; i < b; ++i) grid[i][r] = grid[i + 1][r];
            for (int j = r; j > l; --j) grid[b][j] = grid[b][j - 1];
            for (int i = b; i > t; --i) grid[i][l] = grid[i - 1][l];
            grid[t + 1][l] = top_left;
        }
    }
    return grid;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          int k,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = rotateGrid(grid, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{40, 10}, {30, 20}}, 1, {{10, 20}, {40, 30}}, trials);
    test({{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}},
         2, {{3, 4, 8, 12}, {2, 11, 10, 16}, {1, 7, 6, 15}, {5, 9, 13, 14}}, trials);
    return 0;
}


