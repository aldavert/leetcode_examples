#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> corpFlightBookings(std::vector<std::vector<int> > bookings, int n)
{
    std::vector<int> result(n);
    
    for (const auto &booking : bookings)
    {
        result[booking[0] - 1] += booking[2];
        if (booking[1] < n)
            result[booking[1]] -= booking[2];
    }
    for (int i = 1; i < n; ++i)
        result[i] += result[i - 1];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > bookings,
          int n,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = corpFlightBookings(bookings, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 10}, {2, 3, 20}, {2, 5, 25}}, 5, {10, 55, 45, 25, 25}, trials);
    test({{1, 2, 10}, {2, 2, 15}}, 2, {10, 25}, trials);
    return 0;
}


