#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int missingNumber(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = n;
    for (int i = 0; i < n; ++i)
        result ^= i ^ nums[i];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = missingNumber(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 0, 1}, 2, trials);
    test({0, 1}, 2, trials);
    test({9, 6, 4, 2, 3, 5, 7, 0, 1}, 8, trials);
    return 0;
}


