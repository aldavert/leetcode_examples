#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

long long minCost(std::vector<int> nums, int x)
{
    const long n = static_cast<long>(nums.size());
    long result = std::numeric_limits<long>::max();
    std::vector<long> min_cost(n, std::numeric_limits<long>::max());
    
    for (long rotate = 0; rotate < n; ++rotate)
    {
        for (int i = 0; i < n; ++i)
            min_cost[i] = std::min(min_cost[i],
                                   static_cast<long>(nums[(i - rotate + n) % n]));
        result = std::min(result,
                          std::accumulate(min_cost.begin(), min_cost.end(), 0L)
                        + rotate * x);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int x, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCost(nums, x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({20, 1, 15}, 5, 13, trials);
    test({1, 2, 3}, 4, 6, trials);
    return 0;
}


