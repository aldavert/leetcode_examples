#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> getWordsInLongestSubsequence(std::vector<std::string> words,
                                                      std::vector<int> groups)
{
    const int n = static_cast<int>(words.size());
    std::vector<std::string> result;
    std::vector<int> dp(n, 1), prev(n, -1);
    auto hammingDist = [](const std::string &s1, const std::string &s2) -> int
    {
        int dist = 0;
        for (size_t i = 0; i < s1.size(); ++i)
            dist += (s1[i] != s2[i]);
        return dist;
    };
    
    for (int i = 1; i < n; ++i)
    {
        for (int j = 0; j < i; ++j)
        {
            if ((groups[i] == groups[j])
            ||  (words[i].size() != words[j].size())
            ||  (hammingDist(words[i], words[j]) != 1))
                continue;
            if (dp[i] < dp[j] + 1)
            {
                dp[i] = dp[j] + 1;
                prev[i] = j;
            }
        }
    }
    int index = static_cast<int>(std::max_element(dp.begin(), dp.end()) - dp.begin());
    while (index != -1)
    {
        result.push_back(words[index]);
        index = prev[index];
    }
    std::reverse(result.begin(), result.end());
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::vector<int> groups,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getWordsInLongestSubsequence(words, groups);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"bab", "dab", "cab"}, {1, 2, 2}, {"bab", "dab"}, trials);
    test({"a", "b", "c", "d"}, {1, 2, 3, 4}, {"a", "b", "c", "d"}, trials);
    return 0;
}


