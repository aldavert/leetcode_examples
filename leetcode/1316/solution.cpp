#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int distinctEchoSubstrings(std::string text)
{
    int n = static_cast<int>(text.size());
    long prime = 31;
    int MOD = 1'000'000'007;
    std::vector<long> pow(n + 1, 0);
    
    pow[0] = 1;
    for (int i = 1; i <= n; ++i)
        pow[i] = (pow[i - 1] * prime) % MOD;
    std::vector<long> hash(n, 0);
    for (int i = 0; i < n; ++i)
        hash[i] = ((i == 0?0:hash[i - 1]) + ((text[i] - 'a' + 1) * pow[i]) % MOD) % MOD;
    
    std::unordered_set<long> seen;
    for (int len = 1; len < n; ++len)
    {
        for (int i = 0; i + len * 2 - 1 < n; ++i)
        {
            long hash1 = hash[i + len - 1];
            if (i > 0) hash1 = (hash1 - hash[i-1] + MOD) % MOD;
            hash1 = (hash1 * pow[n - i]) % MOD;
            long hash2 = hash[i + 2 * len - 1];
            hash2 = (hash2 - hash[i + len - 1] + MOD) % MOD;
            hash2 = (hash2 * pow[n - (i + len)]) % MOD;
            if (hash1 == hash2) seen.insert(hash1);
        }
    }
    return static_cast<int>(seen.size());
}
#else
int distinctEchoSubstrings(std::string text)
{
    const int n = static_cast<int>(text.size());
    std::unordered_set<std::string> seen;
    for (int k = 1; k <= n / 2; ++k)
    {
        int same = 0;
        for (int l = 0, r = k; r < n; ++l, ++r)
        {
            if (text[l] == text[r]) ++same;
            else same = 0;
            if (same == k)
            {
                seen.insert(text.substr(l - k + 1, k));
                --same;
            }
        }
    }
    return static_cast<int>(seen.size());
}
#endif

// ############################################################################
// ############################################################################

void test(std::string text, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = distinctEchoSubstrings(text);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcabcabc", 3, trials);
    test("leetcodeleetcode", 2, trials);
    return 0;
}


