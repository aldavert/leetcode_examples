#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string decodeCiphertext(std::string encodedText, int rows)
{
    const int cols = static_cast<int>(encodedText.size()) / rows;
    std::string result;
    std::vector<std::vector<char> > matrix(rows, std::vector<char>(cols, ' '));
    for (int r = 0; r < rows; ++r)
        for (int c = 0; c < cols; ++c)
            matrix[r][c] = encodedText[r * cols + c];
    for (int c = 0; c < cols; ++c)
        for (int i = 0, j = c; (i < rows) && (j < cols); ++i, ++j)
            result.push_back(matrix[i][j]);
    while (!result.empty() && (result.back() == ' ')) result.pop_back();
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string encodedText,
          int rows,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = decodeCiphertext(encodedText, rows);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ch   ie   pr", 3, "cipher", trials);
    test("iveo    eed   l te   olc", 4, "i love leetcode", trials);
    test("coding", 1, "coding", trials);
    return 0;
}


