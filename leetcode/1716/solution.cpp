#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int totalMoney(int n)
{
    int number_full = n / 7;
    int number_partial = n % 7;
    return number_full * (1 + 2 + 3 + 4 + 5 + 6 + 7)
         + 7 * number_full * (number_full - 1) / 2
         + number_partial * (number_partial + 1) / 2
         + number_full * number_partial;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = totalMoney(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test( 4, 10, trials);
    test(10, 37, trials);
    test(20, 96, trials);
    return 0;
}


