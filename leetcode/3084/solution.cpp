#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long countSubstrings(std::string s, char c)
{
    long amount = std::count(s.begin(), s.end(), c);
    return amount * (amount + 1L) / 2L;
}

// ############################################################################
// ############################################################################

void test(std::string s, char c, long long solution, unsigned int trials = 1)
{
    long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSubstrings(s, c);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abada", 'a', 6, trials);
    test("zzz", 'z', 6, trials);
    return 0;
}


