#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int countPairs(TreeNode * root, int distance)
{
    auto process = [&](void) -> int
    {
        int result = 0;
        auto inner = [&](auto &&self, TreeNode * node) -> std::vector<int>
        {
            std::vector<int> d(distance + 1);
            if (!node)
                return d;
            if (!node->left && !node->right)
            {
                d[0] = 1;
                return d;
            }
            const std::vector<int> dl = self(self, node->left),
                                   dr = self(self, node->right);
            for (int i = 0; i < distance; ++i)
                for (int j = 0; j < distance; ++j)
                    if (i + j + 2 <= distance)
                        result += dl[i] * dr[j];
            for (int i = 0; i < distance; ++i)
                d[i + 1] = dl[i] + dr[i];
            return d;
        };
        inner(inner, root);
        return result;
    };
    return process();
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int distance, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = countPairs(root, distance);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, null, 4}, 3, 1, trials);
    test({1, 2, 3, 4, 5, 6, 7}, 3, 2, trials);
    test({7, 1, 4, 6, null, 5, 3, null, null, null, null, null, 2}, 3, 1, trials);
    return 0;
}


