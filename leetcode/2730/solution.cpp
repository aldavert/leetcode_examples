#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestSemiRepetitiveSubstring(std::string s)
{
    int result = 1;
    for (int i = 1, n = static_cast<int>(s.size()), start[2] = {0, 0}; i < n; ++i)
    {
        if (s[i] == s[i - 1])
        {
            if (start[0] > 0)
                start[1] = start[0];
            start[0] = i;
        }
        result = std::max(result, i - start[1] + 1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestSemiRepetitiveSubstring(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("52233", 4, trials);
    test("5494", 4, trials);
    test("1111111", 2, trials);
    return 0;
}


