#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <iostream>

// ############################################################################
// ############################################################################

int maxPathSum(TreeNode* root)
{
    int result = std::numeric_limits<int>::lowest();
    auto pathSum = [&](auto &&self, TreeNode * node) -> int
    {
        if (node == nullptr) return 0;
        int left  = std::max(0, self(self, node->left));
        int right = std::max(0, self(self, node->right));
        result = std::max(result, left + right + node->val);
        return std::max(left, right) + node->val;
    };
    pathSum(pathSum, root);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = maxPathSum(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 6, trials);
    test({-10, 9, 20, null, null, 15, 7}, 42, trials);
    test({2, -1}, 2, trials);
    test({2, 1, 1, -1, -1}, 4, trials);
    return 0;
}


