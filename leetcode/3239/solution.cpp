#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minFlips(std::vector<std::vector<int> > grid)
{
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    int row_flips = 0, col_flips = 0;
    for (const auto &row : grid)
        for (int i = 0; i < n / 2; ++i)
            row_flips += (row[i] != row[n - 1 - i]);
    for (int j = 0; j < n; ++j)
        for (int i = 0; i < m / 2; ++i)
            col_flips += (grid[i][j] != grid[m - 1 - i][j]);
    return std::min(row_flips, col_flips);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minFlips(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0, 0}, {0, 0, 0}, {0, 0, 1}}, 2, trials);
    test({{0, 1}, {0, 1}, {0, 0}}, 1, trials);
    test({{1}, {0}}, 0, trials);
    return 0;
}


