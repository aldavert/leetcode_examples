#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int eatenApples(std::vector<int> apples, std::vector<int> days)
{
    struct Stock
    {
        int deadline = -1;
        int amount = -1;
        bool operator<(const Stock &other) const { return (deadline > other.deadline); }
    };
    const int n = static_cast<int>(apples.size());
    int result = 0;
    std::priority_queue<Stock> heap;
    
    for (int day = 0; (day < n) || !heap.empty(); ++day)
    {
        while (!heap.empty() && (day >= heap.top().deadline))
            heap.pop();
        if ((day < n) && (apples[day] > 0))
            heap.emplace(day + days[day], apples[day]);
        if (!heap.empty())
        {
            const auto [deadline, num_apples] = heap.top();
            heap.pop();
            if (num_apples > 1)
                heap.emplace(deadline, num_apples - 1);
            ++result;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> apples,
          std::vector<int> days,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = eatenApples(apples, days);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 5, 2}, {3, 2, 1, 4, 2}, 7, trials);
    test({3, 0, 0, 0, 0, 2}, {3, 0, 0, 0, 0, 2}, 5, trials);
    test({2, 1, 10}, {2, 10, 1}, 4, trials);
    return 0;
}


