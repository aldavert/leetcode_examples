#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minMovesToMakePalindrome(std::string s)
{
    std::string work = s;
    int result = 0;
    
    while (work.size() > 1)
    {
        int i = static_cast<int>(work.find(work.back()));
        if (i == static_cast<int>(work.size()) - 1) result += i / 2;
        else
        {
            work.erase(i, 1);
            result += i;
        }
        work.pop_back();
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minMovesToMakePalindrome(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aabb", 2, trials);
    test("letelt", 2, trials);
    return 0;
}


