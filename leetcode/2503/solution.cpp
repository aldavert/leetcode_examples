#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<int> maxPoints(std::vector<std::vector<int> > grid,
                           std::vector<int> queries)
{
    struct Info
    {
        int i;
        int j;
        int val;
        bool operator<(const Info &other) const { return val > other.val; }
    };
    constexpr int directions[] = { 0, 1, 0, -1, 0 };
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size()),
              n_queries = static_cast<int>(queries.size());
    std::vector<int> result(n_queries);
    std::vector<std::pair<int, int> > query_and_indexes;
    std::priority_queue<Info> min_heap;
    std::vector<std::vector<bool> > seen(m, std::vector<bool>(n));
    
    for (int i = 0; i < n_queries; ++i)
        query_and_indexes.push_back({queries[i], i});
    std::sort(query_and_indexes.begin(), query_and_indexes.end());
    
    min_heap.push({0, 0, grid[0][0]});
    seen[0][0] = true;
    for (int accumulate = 0; auto& [query, index] : query_and_indexes)
    {
        while (!min_heap.empty())
        {
            auto [i, j, val] = min_heap.top();
            min_heap.pop();
            if (val >= query)
            {
                min_heap.push({i, j, val});
                break;
            }
            ++accumulate;
            for (int k = 0; k < 4; ++k)
            {
                int x = i + directions[k], y = j + directions[k + 1];
                if ((x < 0) || (x == m) || (y < 0) || (y == n)) continue;
                if (seen[x][y]) continue;
                min_heap.push({x, y, grid[x][y]});
                seen[x][y] = true;
            }
        }
        result[index] = accumulate;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<int> queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxPoints(grid, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {2, 5, 7}, {3, 5, 1}}, {5, 6, 2}, {5, 8, 1}, trials);
    test({{5, 2, 1}, {1, 1, 2}}, {3}, {0}, trials);
    return 0;
}


