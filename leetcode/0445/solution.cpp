#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

#if 1
ListNode * addTwoNumbers(ListNode * l1, ListNode * l2)
{
    char values1[101], values2[101];
    int size1 = 0, size2 = 0;
    for (ListNode * ptr = l1; ptr; ptr = ptr->next, ++size1)
        values1[size1] = static_cast<char>(ptr->val);
    for (ListNode * ptr = l2; ptr; ptr = ptr->next, ++size2)
        values2[size2] = static_cast<char>(ptr->val);
    ListNode * result = nullptr;
    bool remainder = false;
    for (; (size1 > 0) && (size2 > 0); --size1, --size2)
    {
        int value = static_cast<int>(values1[size1 - 1])
                  + static_cast<int>(values2[size2 - 1]) + remainder;
        result = new ListNode(value % 10, result);
        remainder = value > 9;
    }
    for (; size1 > 0; --size1)
    {
        int value = static_cast<int>(values1[size1 - 1]) + remainder;
        result = new ListNode(value % 10, result);
        remainder = value > 9;
    }
    for (; size2 > 0; --size2)
    {
        int value = static_cast<int>(values2[size2 - 1]) + remainder;
        result = new ListNode(value % 10, result);
        remainder = value > 9;
    }
    return (remainder)?new ListNode(1, result):result;
}
#else
ListNode * addTwoNumbers(ListNode * l1, ListNode * l2)
{
    auto reverse = [](ListNode * node) -> ListNode *
    {
        ListNode * previous = nullptr;
        while (node)
        {
            ListNode * next = node->next;
            node->next = previous;
            previous = node;
            node = next;
        }
        return previous;
    };
    l1 = reverse(l1);
    l2 = reverse(l2);
    
    ListNode * ptr1 = l1, * ptr2 = l2, * result = new ListNode();
    ListNode * current = result;
    int remainder = 0;
    for (; ptr1 && ptr2; ptr1 = ptr1->next, ptr2 = ptr2->next, current = current->next)
    {
        int value = ptr1->val + ptr2->val + remainder;
        current->next = new ListNode(value % 10);
        remainder = value > 9;
    }
    for (; ptr1; ptr1 = ptr1->next, current = current->next)
    {
        int value = ptr1->val + remainder;
        current->next = new ListNode(value % 10);
        remainder = value > 9;
    }
    for (; ptr2; ptr2 = ptr2->next, current = current->next)
    {
        int value = ptr2->val + remainder;
        current->next = new ListNode(value % 10);
        remainder = value > 9;
    }
    if (remainder)
        current->next = new ListNode(1);
    current = result;
    result = result->next;
    current->next = nullptr;
    delete current;
    reverse(l1);
    reverse(l2);
    return reverse(result);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> num1,
          std::vector<int> num2,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * l1 = vec2list(num1), * l2 = vec2list(num2);
        ListNode * lr = addTwoNumbers(l1, l2);
        result = list2vec(lr);
        delete l1;
        delete l2;
        delete lr;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({7, 2, 4, 3}, {5, 6, 4}, {7, 8, 0, 7}, trials);
    test({2, 4, 3}, {5, 6, 4}, {8, 0, 7}, trials);
    test({0}, {0}, {0}, trials);
    test({5}, {5}, {1, 0}, trials);
    return 0;
}


