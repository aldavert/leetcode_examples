#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int maxTotalFruits(std::vector<std::vector<int> > fruits, int startPos, int k)
{
    const int max_right = std::max(startPos, fruits.back()[0]);
    int result = 0;
    std::vector<int> amounts(1 + max_right), prefix(2 + max_right);

    for (const auto &fruit : fruits) amounts[fruit[0]] = fruit[1];
    std::partial_sum(amounts.begin(), amounts.end(), prefix.begin() + 1);

    auto getFruits = [&](int left_steps, int right_steps)
    {
        int left = std::max(0, startPos - left_steps);
        int right = std::min(max_right, startPos + right_steps);
        return prefix[right + 1] - prefix[left];
    };

    const int max_right_steps = std::min(max_right - startPos, k);
    for (int right_steps = 0; right_steps <= max_right_steps; ++right_steps)
    {
        int left_steps = std::max(0, k - 2 * right_steps);
        result = std::max(result, getFruits(left_steps, right_steps));
    }
    const int max_left_steps = std::min(startPos, k);
    for (int left_steps = 0; left_steps <= max_left_steps; ++left_steps)
    {
        const int right_steps = std::max(0, k - 2 * left_steps);
        result = std::max(result, getFruits(left_steps, right_steps));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > fruits,
          int startPos,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxTotalFruits(fruits, startPos, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 8}, {6, 3}, {8, 6}}, 5, 4, 9, trials);
    test({{0, 9}, {4, 1}, {5, 7}, {6, 2}, {7, 4}, {10, 9}}, 5, 4, 14, trials);
    test({{0, 3}, {6, 4}, {8, 5}}, 3, 2, 0, trials);
    return 0;
}


