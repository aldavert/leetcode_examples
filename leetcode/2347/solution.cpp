#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string bestHand(std::vector<int> ranks, std::vector<char> suits)
{
    int histogram[14] = {};
    if ((suits[0] == suits[1]) && (suits[0] == suits[2])
    &&  (suits[0] == suits[3]) && (suits[0] == suits[4]))
        return "Flush";
    ++histogram[ranks[0]];
    int max_rank = 1;
    max_rank = std::max(max_rank, ++histogram[ranks[1]]);
    max_rank = std::max(max_rank, ++histogram[ranks[2]]);
    max_rank = std::max(max_rank, ++histogram[ranks[3]]);
    max_rank = std::max(max_rank, ++histogram[ranks[4]]);
    if (max_rank >= 3) return "Three of a Kind";
    else if (max_rank == 2) return "Pair";
    return "High Card";
}

// ############################################################################
// ############################################################################

void test(std::vector<int> ranks,
          std::vector<char> suits,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = bestHand(ranks, suits);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({13, 2, 3, 1, 9}, {'a', 'a', 'a', 'a', 'a'}, "Flush", trials);
    test({4, 4, 2, 4, 4}, {'d', 'a', 'a', 'b', 'c'}, "Three of a Kind", trials);
    test({10, 10, 2, 12, 9}, {'a', 'b', 'c', 'a', 'd'}, "Pair", trials);
    return 0;
}


