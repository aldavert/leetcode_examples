#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> rearrangeArray(std::vector<int> nums)
{
    std::sort(nums.begin(), nums.end());
    for (size_t i = 1; i < nums.size(); i += 2)
        std::swap(nums[i], nums[i - 1]);
    return nums;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = rearrangeArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, {2, 1, 4, 3, 5}, trials);
    test({6, 2, 0, 9, 7}, {2, 0, 7, 6, 9}, trials);
    return 0;
}


