#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumLengthEncoding(std::vector<std::string> &words)
{
    struct TrieNode
    {
        std::vector<TrieNode*> children;
        int depth = 0;
        TrieNode(void) : children(26) {}
        ~TrieNode(void)
        {
            for (TrieNode* child : children)
                delete child;
        }
    };
    int result = 0;
    TrieNode root;
    std::vector<TrieNode *> heads;
    std::unordered_set<std::string> words_set(words.begin(), words.end());
    for (const auto &word : words_set)
    {
        TrieNode* node = &root;
        for (char c : std::string(word.rbegin(), word.rend()))
        {
            if (!node->children[c - 'a'])
                node->children[c - 'a'] = new TrieNode;
            node = node->children[c - 'a'];
        }
        node->depth = static_cast<int>(word.size());
        heads.push_back(node);
    }
    for (TrieNode * head : heads)
        if (all_of(head->children.begin(), head->children.end(),
                [](const auto& child) { return child == nullptr; }))
            result += head->depth + 1;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumLengthEncoding(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"time", "me", "bell"}, 10, trials);
    test({"t"}, 2, trials);
    return 0;
}


