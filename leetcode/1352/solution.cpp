#include "../common/common.hpp"

constexpr int null = -1;

// ############################################################################
// ############################################################################

class ProductOfNumbers
{
    std::vector<int> m_prefix;
public:
    ProductOfNumbers(void) : m_prefix{1} {}
    void add(int num)
    {
        if (num == 0) m_prefix = {1};
        else m_prefix.push_back(m_prefix.back() * num);
    }
    int getProduct(int k)
    {
        return (k >= static_cast<int>(m_prefix.size()))?0
             : m_prefix.back() / m_prefix[m_prefix.size() - k - 1];
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<bool> add,
          std::vector<int> input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        ProductOfNumbers obj;
        result.clear();
        for (size_t i = 0; i < input.size(); ++i)
        {
            if (add[i])
            {
                obj.add(input[i]);
                result.push_back(null);
            }
            else result.push_back(obj.getProduct(input[i]));
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({true, true, true, true, true, false, false, false, true, false},
         {3, 0, 2, 5, 4, 2, 3, 4, 8, 2},
         {null, null, null, null, null, 20, 40, 0, null, 32}, trials);
    return 0;
}


