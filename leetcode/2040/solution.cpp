#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long kthSmallestProduct(std::vector<int> nums1,
                             std::vector<int> nums2,
                             long long k)
{
    auto seperate = [](const std::vector<int> &numbers,
                       std::vector<int> &negative,
                       std::vector<int> &positive) -> void
    {
        for (int value : numbers)
        {
            if (value < 0) negative.push_back(-value);
            else positive.push_back(value);
        }
        std::reverse(negative.begin(), negative.end());
    };
    auto numProductNoGreaterThan = [](const std::vector<int> &numbers1,
                                      const std::vector<int> &numbers2,
                                      long m) -> long
    {
        long count = 0;
        int j = static_cast<int>(numbers2.size()) - 1;
        for (long value : numbers1)
        {
            while ((j >= 0) && (value * numbers2[j] > m)) --j;
            count += j + 1;
        }
        return count;
    };
    std::vector<int> negative1, positive1, negative2, positive2;

    
    seperate(nums1, negative1, positive1);
    seperate(nums2, negative2, positive2);

    long negative_count = negative1.size() * positive2.size()
                        + positive1.size() * negative2.size();
    int sign = 1;
    
    if (k > negative_count) k -= negative_count;
    else
    {
        k = negative_count - k + 1;
        sign = -1;
        std::swap(positive2, negative2);
    }
    
    long l = 0;
    long r = 1e10;
    while (l < r)
    {
        const long m = (l + r) / 2;
        if (numProductNoGreaterThan(negative1, negative2, m)
          + numProductNoGreaterThan(positive1, positive2, m) >= k)
            r = m;
        else l = m + 1;
    }
    
    return sign * l;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          long long k,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = kthSmallestProduct(nums1, nums2, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 5}, {3, 4}, 2, 8, trials);
    test({-4, -2, 0, 3}, {2, 4}, 6, 0, trials);
    test({-2, -1, 0, 1, 2}, {-3, -1, 2, 4, 5}, 3, -6, trials);
    return 0;
}


