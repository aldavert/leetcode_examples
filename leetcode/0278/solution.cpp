#include "../common/common.hpp"

static int& badVersion(void)
{
    static int bad_version = 0;
    return bad_version;
}

bool isBadVersion(int version)
{
    return version >= badVersion();
}

// ############################################################################
// ############################################################################

int firstBadVersion(int n)
{
    int left = 1;
    int right = n;
    while (left < right)
    {
        int mid = left + (right - left) / 2;
        if (isBadVersion(mid)) right = mid;
        else left = mid + 1;
    }
    return left;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    badVersion() = solution;
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = firstBadVersion(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 2, trials);
    test(5, 4, trials);
    test(1, 1, trials);
    return 0;
}


