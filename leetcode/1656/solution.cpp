#include "../common/common.hpp"

// ############################################################################
// ############################################################################

class OrderedStream
{
    std::vector<bool> m_enabled;
    std::vector<std::string> m_values;
    size_t m_position = 0;
public:
    OrderedStream(int n) :
        m_enabled(n, false),
        m_values(n),
        m_position(0)
    {
    }
    std::vector<std::string> insert(int idKey, std::string value)
    {
        std::vector<std::string> result;
        m_enabled[idKey - 1] = true;
        m_values[idKey - 1] = value;
        while ((m_position < m_values.size()) && m_enabled[m_position])
            result.push_back(m_values[m_position++]);
        return result;
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<std::tuple<int, std::string> > input,
          std::vector<std::vector<std::string> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<std::string> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result.clear();
        OrderedStream obj(static_cast<int>(input.size()));
        for (auto [idKey, value] : input)
            result.push_back(obj.insert(idKey, value));
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, "ccccc"}, {1, "aaaaa"}, {2, "bbbbb"}, {5, "eeeee"}, {4, "ddddd"}},
         {{}, {"aaaaa"}, {"bbbbb", "ccccc"}, {}, {"ddddd", "eeeee"}}, trials);
    return 0;
}


