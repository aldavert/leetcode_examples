#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > reconstructMatrix(int upper,
                                                 int lower,
                                                 std::vector<int> colsum)
{
    if (upper + lower != std::accumulate(colsum.begin(), colsum.end(), 0))
        return {};
    if (std::min(upper, lower) < std::count_if(colsum.begin(), colsum.end(),
                                               [](int c) { return c == 2; }))
        return {};
    std::vector<std::vector<int> > result(2, std::vector<int>(colsum.size()));
    for (size_t j = 0; j < colsum.size(); ++j)
    {
        if (colsum[j] == 2)
        {
            result[0][j] = 1;
            result[1][j] = 1;
            --upper;
            --lower;
        }
    }
    for (size_t j = 0; j < colsum.size(); ++j)
    {
        if ((colsum[j] == 1) && (upper > 0))
        {
            result[0][j] = 1;
            --colsum[j];
            --upper;
        }
        if ((colsum[j] == 1) && (lower > 0))
        {
            result[1][j] = 1;
            --lower;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int upper,
          int lower,
          std::vector<int> colsum,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = reconstructMatrix(upper, lower, colsum);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 1, {1, 1, 1}, {{1, 1, 0}, {0, 0, 1}}, trials);
    test(2, 3, {2, 2, 1, 1}, {}, trials);
    test(5, 5, {2, 1, 2, 0, 1, 0, 1, 2, 0, 1},
         {{1, 1, 1, 0, 1, 0, 0, 1, 0, 0}, {1, 0, 1, 0, 0, 0, 1, 1, 0, 1}}, trials);
    return 0;
}


