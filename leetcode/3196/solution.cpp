#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

long long maximumTotalCost(std::vector<int> &nums)
{
    long keep = nums[0], flip = nums[0];
    for (int i = 1, n = static_cast<int>(nums.size()); i < n; ++i)
        flip = std::exchange(keep, std::max(keep, flip) + nums[i]) - nums[i];
    return std::max(keep, flip);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumTotalCost(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, -2, 3, 4}, 10, trials);
    test({1, -1, 1, -1}, 4, trials);
    test({0}, 0, trials);
    test({1, -1}, 2, trials);
    return 0;
}


