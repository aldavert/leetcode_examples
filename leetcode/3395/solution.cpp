#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int subsequencesWithMiddleMode(std::vector<int> nums)
{
    constexpr int MOD = 1'000'000'007;
    auto nC2 = [&](long n) -> long { return n * (n - 1) / 2 % MOD; };
    auto calc = [&](int a,
                    long other1,
                    long other2,
                    const std::unordered_map<int, int> &count1,
                    const std::unordered_map<int, int> &count2) -> long
    {
        long result = other1 * nC2(other2) % MOD;
        for (const auto &[b, b1] : count1)
        {
            if (b == a) continue;
            const long b2 = count2.contains(b)?count2.at(b):0;
            result = (result - b1 * nC2(b2) % MOD + MOD) % MOD;
            result = (result - b1 * b2 % MOD * (other2 - b2) % MOD + MOD) % MOD;
        }
        for (const auto& [b, b2] : count2)
        {
            if (b == a) continue;
            const long b1 = count1.contains(b)?count1.at(b):0;
            result = (result - (other1 - b1) * nC2(b2) % MOD + MOD) % MOD;
        }
        return result;
    };
    const int n = static_cast<int>(nums.size());
    long long result = 0;
    std::unordered_map<int, int> left, right;
    
    for (int i = 0; i < 2; ++i) ++left[nums[i]];
    for (int i = 2; i < n; ++i) ++right[nums[i]];
    for (int i = 2; i < n - 2; ++i)
    {
        const int num = nums[i];
        if (--right[num] == 0) right.erase(num);
        
        const int left_count = left[num];
        const int right_count = right[num];
        const int left_other = i - left_count;
        const int right_other = n - i - 1 - right_count;
        result = result
               + nC2(left_count) * nC2(right_count) % MOD
               + nC2(left_count) * right_count % MOD * right_other % MOD
               + left_count * left_other % MOD * nC2(right_count) % MOD
               + nC2(left_count) * nC2(right_other) % MOD
               + nC2(left_other) * nC2(right_count) % MOD
               + left_count * left_other % MOD * right_count % MOD * right_other % MOD
               + left_count * calc(num, left_other, right_other, left, right) % MOD
               + right_count * calc(num, right_other, left_other, right, left) % MOD;
        result %= MOD;
        ++left[num];
    }
    return static_cast<int>(result % MOD);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = subsequencesWithMiddleMode(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 1, 1, 1, 1}, 6, trials);
    test({1, 2, 2, 3, 3, 4}, 4, trials);
    test({0, 1, 2, 3, 4, 5, 6, 7, 8}, 0, trials);
    return 0;
}


