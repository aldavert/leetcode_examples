#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

double separateSquares(std::vector<std::vector<int> > squares)
{
    struct SegmentTree
    {
        std::vector<int> values;
        std::vector<int> count;
        std::vector<int> total;
        int length = 0;
        SegmentTree(void) = default;
        SegmentTree(const std::vector<std::vector<int> > &info)
        {
            std::set<int> unique;
            for (const auto &square : info)
            {
                unique.insert(square[0]);
                unique.insert(square[0] + square[2]);
            }
            for (int val : unique)
                values.push_back(val);
            count.resize(4 * unique.size() + 1, 0);
            total.resize(4 * unique.size() + 1, 0);
            length = static_cast<int>(unique.size());
        }
        void update(int ql, int qr, int val)
        {
            update(ql, qr, val, 1, 0, length - 1);
        }
        inline int query(void) const { return total[1]; }
    private:
        void update(int ql, int qr, int val, int idx, int l, int r)
        {
            if (ql >= qr) return;
            if ((ql == values[l]) && (qr == values[r]))
                count[idx] += val;
            else
            {
                int mid = (l + r) / 2;
                if (ql < values[mid])
                    update(ql, std::min(qr, values[mid]), val, 2 * idx, l, mid);
                if (qr > values[mid])
                    update(std::max(ql, values[mid]), qr, val, 2 * idx + 1, mid, r);
            }
            if (count[idx] > 0)
                total[idx] = values[r] - values[l];
            else
            {
                if (int next = idx * 2 + 1; next < static_cast<int>(total.size()))
                    total[idx] = total[idx * 2] + total[next];
                else total[idx] = 0;
            }
        }
    };
    SegmentTree st(squares);
    std::vector<std::tuple<int, int, int, int> > events;
    for (const auto &square : squares)
    {
        int x = square[0], y = square[1], l = square[2];
        events.push_back({y, 1, x, x + l});
        events.push_back({y + l, -1, x, x + l});
    }
    std::sort(events.begin(), events.end());
    double total = 0.0;
    for (double py = -1'000'000'000; auto [y, delta, left, right] : events)
    {
        total += st.query() * (y - py);
        st.update(left, right, delta);
        py = y;
    }
    for (double py = -1'000'000'000, sum = 0.0; auto [y, delta, left, right] : events)
    {
        double current_width = st.query();
        double area = current_width * (y - py);
        if ((sum + area) * 2.0 >= total)
            return (total / 2.0 - sum) / current_width + py;
        sum += area;
        st.update(left, right, delta);
        py = y;
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > squares,
          double solution,
          unsigned int trials = 1)
{
    double result = -1.0;
    for (unsigned int i = 0; i < trials; ++i)
        result = separateSquares(squares);
    showResult(std::abs(solution - result) < 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0, 1}, {2, 2, 1}}, 1.00000, trials);
    test({{0, 0, 2}, {1, 1, 1}}, 1.00000, trials);
    test({{17, 27, 1}, {9, 13, 1}, {1, 17, 4}}, 19.0, trials);
    return 0;
}


