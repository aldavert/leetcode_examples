#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

bool isPrintable(std::vector<std::vector<int> > targetGrid)
{
    constexpr int MAX_COLOR = 60;
    const int n_rows = static_cast<int>(targetGrid.size());
    const int n_cols = static_cast<int>(targetGrid[0].size());
    std::bitset<MAX_COLOR + 1> deps[MAX_COLOR + 1];
    int seen[MAX_COLOR + 1] = {};
    auto hasCycle = [&](auto &&self, int color) -> bool
    {
        if (seen[color] == 1) return true;
        if (seen[color] == 2) return false;
        seen[color] = 1;
        for (int t = 0; t <= MAX_COLOR; ++t)
            if (deps[color][t] && self(self, t)) return true;
        seen[color] = 2;
        return false;
    };
    
    for (int color = 1; color <= MAX_COLOR; ++color)
    {
        int left = n_cols, right = -1, top = n_rows, bottom = -1;      
        for (int row = 0; row < n_rows; ++row)
            for (int col = 0; col < n_cols; ++col)
                if (targetGrid[row][col] == color)
                    left   = std::min(left  , col),
                    right  = std::max(right , col),
                    top    = std::min(top   , row),
                    bottom = std::max(bottom, row);
        if (left == -1) continue;
        for (int row = top; row <= bottom; ++row)
            for (int col = left; col <= right; ++col)
                if (targetGrid[row][col] != color) 
                    deps[color][targetGrid[row][col]] = true;
    }
    for (int color = 1; color <= MAX_COLOR; ++color)
        if (hasCycle(hasCycle, color)) return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > targetGrid,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPrintable(targetGrid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 1, 1}, {1, 2, 2, 1}, {1, 2, 2, 1}, {1, 1, 1, 1}}, true, trials);
    test({{1, 1, 1, 1}, {1, 1, 3, 3}, {1, 1, 3, 4}, {5, 5, 1, 4}}, true, trials);
    test({{1, 2, 1}, {2, 1, 2}, {1, 2, 1}}, false, trials);
    return 0;
}



