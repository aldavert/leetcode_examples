#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> >
constructGridLayout(int n, std::vector<std::vector<int> > edges)
{
    std::vector<std::vector<int> > graph(n);
    for (const auto &edge : edges)
    {
        graph[edge[0]].push_back(edge[1]);
        graph[edge[1]].push_back(edge[0]);
    }
    int corner = 0;
    for (int i = 0; i < n; ++i)
        if (graph[i].size() < graph[corner].size())
            corner = i;
    std::vector<bool> seen(n);
    seen[corner] = true;
    std::vector<int> first_row = {corner};
    for (int corner_degree = static_cast<int>(graph[corner].size());
         (first_row.size() == 1)
      || (static_cast<int>(graph[first_row.back()].size()) == corner_degree + 1);)
    {
        std::vector<int> &neighbors = graph[first_row.back()];
        std::sort(neighbors.begin(), neighbors.end(),
                [&](int u, int v) { return graph[u].size() < graph[v].size(); });
        for (const int v : neighbors)
        {
            if (!seen[v]
            && ((static_cast<int>(graph[v].size()) == corner_degree)
            ||  (static_cast<int>(graph[v].size()) == corner_degree + 1)))
            {
                first_row.push_back(v);
                seen[v] = true;
                break;
            }
        }
    }
    const int cols = static_cast<int>(first_row.size());
    const int rows = n / cols;
    std::vector<std::vector<int> > result(rows, std::vector<int>(cols));
    result[0] = first_row;
    for (int i = 1; i < rows; ++i)
    {
        for (int j = 0; j < cols; ++j)
        {
            for (const int v : graph[result[i - 1][j]])
            {
                if (!seen[v])
                {
                    result[i][j] = v;
                    seen[v] = true;
                    break;
                }
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = constructGridLayout(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{0, 1}, {0, 2}, {1, 3}, {2, 3}}, {{0, 1}, {2, 3}}, trials);
    test(5, {{0, 1}, {1, 3}, {2, 3}, {2, 4}}, {{0, 1, 3, 2, 4}}, trials);
    test(9, {{0, 1}, {0, 4}, {0, 5}, {1, 7}, {2, 3}, {2, 4}, {2, 5}, {3, 6},
         {4, 6}, {4, 7}, {6, 8}, {7, 8}}, {{1, 0, 5}, {7, 4, 2}, {8, 6, 3}}, trials);
    return 0;
}


