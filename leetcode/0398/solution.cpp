#include "../common/common.hpp"
#include <unordered_map>
#include <random>

// ############################################################################
// ############################################################################

class Solution
{
    std::unordered_map<int, std::vector<int> > m_information;
public:
    Solution(std::vector<int> nums)
    {
        for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
            m_information[nums[i]].push_back(i);
    }
    int pick(int target)
    {
        if (auto search = m_information.find(target); search != m_information.end())
        {
            if (search->second.size() == 0) return -1;
            if (search->second.size() == 1) return search->second.front();
            std::random_device rd;
            std::mt19937 gen(rd());
            std::uniform_int_distribution<> distrib(0, static_cast<int>(search->second.size() - 1));
            return search->second[distrib(gen)];
        }
        else return -1;
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> pick,
          unsigned int trials = 1)
{
    std::unordered_map<int, std::unordered_set<int> > solution;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        solution[nums[i]].insert(i);
    std::vector<std::vector<int> > solution_display;
    for (int value : pick)
    {
        std::vector<int> current;
        if  (auto search = solution.find(value); search != solution.end())
            for (auto index : search->second)
                current.push_back(index);
        std::sort(current.begin(), current.end());
        solution_display.push_back(current);
    }
    std::vector<int> result;
    auto check = [&](void) -> bool
    {
        for (size_t i = 0; i < pick.size(); ++i)
        {
            if (auto search = solution.find(pick[i]); search != solution.end())
            {
                if (search->second.find(result[i]) == search->second.end())
                    return false;
            }
            else return false;
        }
        return true;
    };
    for (unsigned int i = 0; i < trials; ++i)
    {
        Solution obj(nums);
        result.clear();
        for (int target : pick)
            result.push_back(obj.pick(target));
    }
    showResult(check(), solution_display, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 3, 3}, {3, 1, 3}, trials);
    return 0;
}


