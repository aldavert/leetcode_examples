#include "../common/common.hpp"
#include <set>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > combinationSum3(int k, int n)
{
    std::vector<std::vector<int> > result;
    std::vector<int> current;
    auto process = [&](auto &&self, int index, int operands, int remaining) -> void
    {
        if ((remaining > 0) && (operands > 0))
        {
            for (int next = index + 1; next <= 9; ++next)
            {
                current.push_back(next);
                self(self, next, operands - 1, remaining - next);
                current.pop_back();
            }
        }
        else if ((operands == 0) && (remaining == 0))
            result.push_back(current);
    };
    process(process, 0, k, n);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    std::set<std::vector<int> > lut(left.begin(), left.end());
    for (const auto &r : right)
    {
        if (auto search = lut.find(r); search != lut.end())
            lut.erase(search);
        else return false;
    }
    return lut.empty();
}

void test(int k,
          int n,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = combinationSum3(k, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 7, {{1, 2, 4}}, trials);
    test(3, 9, {{1, 2, 6}, {1, 3, 5}, {2, 3, 4}}, trials);
    test(4, 1, {}, trials);
    return 0;
}


