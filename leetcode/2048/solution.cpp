#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int nextBeautifulNumber(int n)
{
    auto isBalance = [](int num) -> bool
    {
        int count[10] = {};
        while (num)
        {
            if (num % 10 == 0) return false;
            ++count[num % 10];
            num /= 10;
        }
        for (int i = 1; i < 10; ++i)
            if (count[i] && (count[i] != i))
                return false;
        return true;
    };
    while (!isBalance(++n));
    return n;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = nextBeautifulNumber(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 22, trials);
    test(1000, 1333, trials);
    test(3000, 3133, trials);
    return 0;
}


