#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool buddyStrings(std::string s, std::string goal)
{
    if (s.size() != goal.size()) return false;
    if (s == goal)
        return std::unordered_set<char>(s.begin(), s.end()).size() < s.size();
    int indexes[2] = {}, number_of_differences = 0;
    const size_t n = s.size();
    for (size_t i = 0; i < n; ++i)
    {
        if (s[i] != goal[i])
        {
            ++number_of_differences;
            if (number_of_differences > 2) break;
            indexes[number_of_differences - 1] = static_cast<int>(i);
        }
    }
    return (number_of_differences == 2) && (s[indexes[0]] == goal[indexes[1]])
        && (s[indexes[1]] == goal[indexes[0]]);
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::string goal,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = buddyStrings(s, goal);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ab", "ba",  true, trials);
    test("ab", "ab", false, trials);
    test("aa", "aa",  true, trials);
    return 0;
}


