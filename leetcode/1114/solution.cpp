#include "../common/common.hpp"
#include <functional>
#include <thread>
#include <mutex>

// ############################################################################
// ############################################################################

class Foo
{
    std::mutex mutex_second;
    std::mutex mutex_third;
public:
    Foo(void)
    {
        mutex_second.lock();
        mutex_third.lock();
    }
    void first(std::function<void(void)> printFirst)
    {
        printFirst();
        mutex_second.unlock();
    }
    void second(std::function<void(void)> printSecond)
    {
        mutex_second.lock();
        printSecond();
        mutex_third.unlock();
    }
    void third(std::function<void(void)> printThird)
    {
        mutex_third.lock();
        printThird();
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<int> call_order,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        Foo foo;
        auto dummy = [&](void) { return ""; };
        std::vector<std::function<void(std::function<void(void)>)> > functions = {
            std::bind(&Foo::first, &foo, [&](void){ result += "BAD"; }),
            std::bind(&Foo::first, &foo, [&](void){ result += "first"; }),
            std::bind(&Foo::second, &foo, [&](void){ result += "second"; }),
            std::bind(&Foo::third, &foo, [&](void){ result += "third"; })
        };
        std::vector<std::thread> threads;
        result = "";
        for (int idx : call_order)
        {
            threads.push_back(std::thread(functions[idx], dummy));
        }
        for (auto &t : threads)
            t.join();
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, "firstsecondthird", trials);
    test({1, 3, 2}, "firstsecondthird", trials);
    test({3, 2, 1}, "firstsecondthird", trials);
    return 0;
}


