#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string addSpaces(std::string s, std::vector<int> spaces)
{
    const int n = static_cast<int>(s.size()),
              m = static_cast<int>(spaces.size());
    std::string result;
    for (int i = 0, j = 0; i < n; ++i)
    {
        if ((j < m) && (i == spaces[j]))
            result += " ",
            ++j;
        result += s[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<int> spaces,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = addSpaces(s, spaces);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("LeetcodeHelpsMeLearn", {8, 13, 15}, "Leetcode Helps Me Learn", trials);
    test("icodeinpython", {1, 5, 7, 9}, "i code in py thon", trials);
    test("spacing", {0, 1, 2, 3, 4, 5, 6}, " s p a c i n g", trials);
    return 0;
}


