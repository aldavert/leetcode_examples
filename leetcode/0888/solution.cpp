#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<int> fairCandySwap(std::vector<int> aliceSizes,
                               std::vector<int> bobSizes)
{
    std::vector<bool> present(200'010, false);
    std::unordered_set<int> lut;
    int Sa = 0, Sb = 0;
    for (int c : aliceSizes)
    {
        present[2 * c] = true;
        Sa += c;
    }
    for (int c : bobSizes)
        Sb += c;
    int d = Sa - Sb;
    for (int c : bobSizes)
        if (int search = d + 2 * c;
           (search >= 0) && (search < 200'010) && present[search])
            return {search / 2, c};
    return {};
}
#else
std::vector<int> fairCandySwap(std::vector<int> aliceSizes,
                               std::vector<int> bobSizes)
{
    std::unordered_set<int> lut;
    int Sa = 0, Sb = 0;
    for (int c : aliceSizes)
    {
        lut.insert(2 * c);
        Sa += c;
    }
    for (int c : bobSizes)
        Sb += c;
    int d = Sa - Sb;
    for (int c : bobSizes)
        if (auto search = lut.find(d + 2 * c); search != lut.end())
            return {*search / 2, c};
    return {};
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> aliceSizes,
          std::vector<int> bobSizes,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = fairCandySwap(aliceSizes, bobSizes);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1}, {2, 2}, {1, 2}, trials);
    test({1, 2}, {2, 3}, {1, 2}, trials);
    test({2}, {1, 3}, {2, 3}, trials);
    test({8, 73, 2, 86, 32}, {56, 5, 67, 100, 31}, {2, 31}, trials);
    test({2025, 9743, 29182, 36306, 21848, 27201, 12509, 30821, 27045, 17650,
          15945, 5303, 5154, 44123, 32886, 32995, 47502, 15160, 25094, 17320,
          1194, 19154, 6156, 14968, 7075, 3774, 40473, 22441, 25366, 28820,
          48152, 29402, 41285, 3080, 155, 7190, 44466, 36178, 7430, 46916, 47726,
          278, 47368, 20631, 5780, 3948, 14286, 30343, 46722, 28897, 40892, 16150,
          28857, 28547, 27260, 32589, 46340, 10003, 47826, 8694, 31087, 1439,
          29515, 13621, 151, 15590, 45395, 43106, 10692, 31160, 8935, 27513, 10330,
          3351, 27362, 38987, 40862, 4995, 1170, 24970, 48135, 31012, 47818, 35366,
          30553, 4926, 16471, 19422, 22397, 34361, 5104, 46907, 38123, 1393, 5506,
          9694, 30954, 24876, 5317, 20743}, {77285, 23125, 76864, 23296, 72297,
          72430, 99980, 41019, 93155, 28957, 69801, 41384, 58065, 17874, 39033,
          7063, 98358, 85430, 79302, 55864, 27679, 52795, 49712, 22742, 66121,
          22036, 7566, 12093, 38933, 88825, 29279, 16087, 2510, 50776, 72786,
          57609, 30560, 90561, 57601, 62170, 5830, 30285, 67039, 16561, 82103,
          6175, 13971, 89264, 21468, 18046}, {38123, 67039}, trials);
    return 0;
}


