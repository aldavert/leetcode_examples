#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

std::string thousandSeparator(int n)
{
    std::stack<char> digits;
    int counter = 0;
    do
    {
        if (counter == 3)
            digits.push('.'), counter = 0;
        int next = n / 10;
        digits.push(static_cast<char>('0' + (n - next * 10)));
        n = next;
        ++counter;
    }
    while (n);
    std::string result;
    while (!digits.empty())
        result += digits.top(), digits.pop();
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = thousandSeparator(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(987, "987", trials);
    test(1234, "1.234", trials);
    return 0;
}


