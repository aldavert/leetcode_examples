#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countCharacters(std::vector<std::string> words, std::string chars)
{
    int histogram[128] = {};
    int result = 0;
    for (int c : chars)
        ++histogram[c];
    for (const std::string &w : words)
    {
        int current[128] = {};
        for (int c : w)
            ++current[c];
        bool valid = true;
        for (int i = 'a'; valid && (i <= 'z'); ++i)
            valid = current[i] <= histogram[i];
        result += valid * static_cast<int>(w.size());
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::string chars,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countCharacters(words, chars);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"cat", "bt", "hat", "tree"}, "atach", 6, trials);
    test({"hello", "world", "leetcode"}, "welldonehoneyr", 10, trials);
    return 0;
}

