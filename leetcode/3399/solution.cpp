#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minLength(std::string s, int numOps)
{
    const int n = static_cast<int>(s.size());
    auto minOps = [&](int k) -> int
    {
        if (k == 1)
        {
            int result = 0;
            for (int i = 0; i < n; ++i)
                result += (s[i] - '0' == i % 2);
            return std::min(result, n - result);
        }
        int result = 0, running_len = 1;
        for (int i = 1; i < n; ++i)
        {
            if (s[i] == s[i - 1]) ++running_len;
            else
            {
                result += running_len / (k + 1);
                running_len = 1;
            }
        }
        return result + running_len / (k + 1);
    };
    int l = 1, r = n;
    while (l < r)
    {
        const int m = (l + r) / 2;
        if (minOps(m) <= numOps) r = m;
        else l = m + 1;
    }
    return l;
}

// ############################################################################
// ############################################################################

void test(std::string s, int numOps, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minLength(s, numOps);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("000001", 1, 2, trials);
    test("0000", 2, 1, trials);
    test("0101", 0, 1, trials);
    return 0;
}


