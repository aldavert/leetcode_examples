#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int concatenatedBinary(int n)
{
    long result = 0;
    for (int i = 1, len = 1; i <= n; ++i)
        result = (result * (len <<= (i == len)) + i) % 1'000'000'007;
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = concatenatedBinary(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 1, trials);
    test(3, 27, trials);
    test(12, 505379714, trials);
    return 0;
}


