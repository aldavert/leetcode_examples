#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<int> deckRevealedIncreasing(std::vector<int> deck)
{
    if (deck.empty()) return {};
    std::vector<int> sorted_deck(deck), result(deck.size());
    std::sort(sorted_deck.begin(), sorted_deck.end());
    std::queue<int> q;
    q.push(sorted_deck.back());
    for (int i = static_cast<int>(deck.size()) - 2; i >= 0; --i)
    {
        q.push(q.front());
        q.pop();
        q.push(sorted_deck[i]);
    }
    for (int i = static_cast<int>(deck.size()) - 1; i >= 0; --i)
    {
        result[i] = q.front();
        q.pop();
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> deck,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = deckRevealedIncreasing(deck);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({17, 13, 11, 2, 3, 5, 7}, {2, 13, 3, 11, 5, 17, 7}, trials);
    test({1, 1000}, {1, 1000}, trials);
    return 0;
}


