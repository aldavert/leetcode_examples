#include "../common/common.hpp"
#include <map>

// ############################################################################
// ############################################################################

#if 0
bool pyramidTransition(std::string bottom, std::vector<std::string> allowed)
{
    auto canBuild = [&]() -> bool
    {
        std::unordered_set<std::string> invalid;
        std::map<std::string, std::string> next_allowed;
        for(size_t i = 0; i < allowed.size(); ++i)
            next_allowed[allowed[i].substr(0, 2)].push_back(allowed[i][2]);
        auto inner = [&](auto &&self, std::string &state, int i, std::string next_state) -> bool
        {
            size_t n = state.size(), m = next_state.size();
            if (n < 2) return true;
            if (invalid.find(state) != invalid.end()) return false;
            if (m == n - 1) return self(self, next_state, 0, "");
            if ((m > 1)
            &&  (next_allowed.find(next_state.substr(m - 2, 2)) == next_allowed.end()))
                return false;
            for (char chr : next_allowed[state.substr(i, 2)])
            {
                next_state.push_back(chr);
                if (self(self, state, i + 1, next_state))
                    return true;
                next_state.pop_back();
            }
            invalid.insert(state);
            return false;

        };
        return inner(inner, bottom, 0, "");
    };
    return canBuild();
}
#else
bool pyramidTransition(std::string bottom, std::vector<std::string> allowed)
{
    bool dictionary[6][6][6] = {}, is_valid[6][6];
    auto IDX = [](char letter) -> int { return static_cast<int>(letter - 'A'); };
    for (std::string pattern : allowed)
    {
        dictionary[IDX(pattern[0])][IDX(pattern[1])][IDX(pattern[2])] = true;
        is_valid[IDX(pattern[0])][IDX(pattern[1])] = true;
    }
    auto canBuild = [&]() -> bool
    {
        auto inner = [&](auto &&self, std::string current) -> bool
        {
            const size_t n = current.size();
            if (n == 1) return true;
            std::string next(n - 1, '\0');
            std::vector<char> possible[6];
            size_t state[6] = {};
            for (size_t i = 1; i < n; ++i)
            {
                for (size_t k = 0; k < 6; ++k)
                    if (dictionary[IDX(current[i - 1])][IDX(current[i])][k])
                        possible[i].push_back(static_cast<char>('A' + k));
                if (possible[i].empty()) return false;
            }
            for (size_t carry = 0; !carry; )
            {
                bool valid = true;
                next[0] = possible[1][state[1]];
                for (size_t i = 2; valid && (i < n); ++i)
                {
                    next[i - 1] = possible[i][state[i]];
                    valid = is_valid[IDX(next[i - 2])][IDX(next[i - 1])];
                }
                if (valid && self(self, next))
                    return true;
                carry = 1;
                for (size_t i = 1; i < n; ++i)
                {
                    state[i] += carry;
                    if (state[i] == possible[i].size())
                    {
                        state[i] = 0;
                        carry = 1;
                    }
                    else carry = 0;
                }
            }
            return false;
        };
        return inner(inner, bottom);
    };
    return canBuild();
}
#endif

// ############################################################################
// ############################################################################

void test(std::string bottom,
          std::vector<std::string> allowed,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = pyramidTransition(bottom, allowed);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("BCD", {"BCC", "CDE", "CEA", "FFF"}, true, trials);
    test("AAAA", {"AAB", "AAC", "BCD", "BBE", "DEF"}, false, trials);
    return 0;
}


