#include "../common/common.hpp"
#include <unordered_map>
#include <queue>
#include <numeric>

// ############################################################################
// ############################################################################

int earliestSecondToMarkIndices(std::vector<int> nums, std::vector<int> changeIndices)
{
    const long nums_sum = std::accumulate(nums.begin(), nums.end(), 0L);
    const int n = static_cast<int>(changeIndices.size());
    std::unordered_map<int, int> first_to_index, second_to_index;
    auto canMark = [&](int max_second) -> bool
    {
        std::priority_queue<int, std::vector<int>, std::greater<int> > heap;
        int marks = 0;
        
        for (int second = max_second - 1; second >= 0; --second)
        {
            if (const auto it = second_to_index.find(second);
                it != second_to_index.end())
            {
                heap.push(nums[it->second]);
                if (marks == 0)
                {
                    heap.pop();
                    ++marks;
                }
                else --marks;
            }
            else ++marks;
        }
        const int heap_size = static_cast<int>(heap.size());
        long decrement_and_mark_cost = 0;
        while (!heap.empty())
            decrement_and_mark_cost += heap.top(),
            heap.pop();
        return (nums_sum - decrement_and_mark_cost + static_cast<int>(nums.size())
             + heap_size) <= max_second;
    };
    for (int i = 0; i < n; ++i)
    {
        int idx = changeIndices[i] - 1;
        if ((nums[idx] > 0) && !first_to_index.contains(idx))
            first_to_index[idx] = i;
    }
    for (auto [index, second] : first_to_index)
        second_to_index[second] = index;
    
    int l = 0;
    for (int r = n + 1; l < r;)
    {
        const int m = (l + r) / 2;
        if (canMark(m))
            r = m;
        else l = m + 1;
    }
    return (l <= n)?l:-1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> changeIndices,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = earliestSecondToMarkIndices(nums, changeIndices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 3}, {1, 3, 2, 2, 2, 2, 3}, 6, trials);
    test({0, 0, 1, 2}, {1, 2, 1, 2, 1, 2, 1, 2}, 7, trials);
    return 0;
}


