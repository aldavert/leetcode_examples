#include "../common/common.hpp"
#include <deque>

// ############################################################################
// ############################################################################

int constrainedSubsetSum(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> dp(n);
    std::deque<int> q;
    
    for (int i = 0; i < n; ++i)
    {
        if (q.empty()) dp[i] = nums[i];
        else dp[i] = std::max(q.front(), 0) + nums[i];
        while (!q.empty() && q.back() < dp[i])
            q.pop_back();
        q.push_back(dp[i]);
        if (i >= k && dp[i - k] == q.front())
            q.pop_front();
    }
    int result = dp[0];
    for (int i = 1; i < n; ++i)
        result = std::max(result, dp[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = constrainedSubsetSum(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 2, -10, 5, 20}, 2, 37, trials);
    test({-1, -2, -3}, 1, -1, trials);
    test({10, -2, -10, -5, 20}, 2, 23, trials);
    return 0;
}


