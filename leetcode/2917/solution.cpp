#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findKOr(std::vector<int> nums, int k)
{
    int accumulator[34] = {};
    for (size_t i = 0; i < nums.size(); ++i)
        for (int position = 0, number = nums[i]; number != 0; ++position, number /= 2)
            accumulator[position] += number & 1;
    int result = 0;
    for (size_t i = 0; i < 32; ++i)
        result = result | ((accumulator[i] >= k) << i);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findKOr(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({7, 12, 9, 8, 9, 15}, 4, 9, trials);
    test({2, 12, 1, 11, 4, 5}, 6, 0, trials);
    test({10, 8, 5, 9, 11, 6, 8}, 1, 15, trials);
    return 0;
}


