#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> pathsWithMaxScore(std::vector<std::string> board)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(board.size());
    const std::vector<std::pair<int, int> > directions{{0, 1}, {1, 0}, {1, 1}};
    int dp[101][101], count[101][101] = {};
    for (int i = 0; i <= n; ++i)
        for (int j = 0; j <= n; ++j)
            dp[i][j] = -1;
    dp[0][0] = 0;
    dp[n - 1][n - 1] = 0;
    count[n - 1][n - 1] = 1;
    for (int i = n - 1; i >= 0; --i)
    {
        for (int j = n - 1; j >= 0; --j)
        {
            if ((board[i][j] == 'S') || (board[i][j] == 'X'))
                continue;
            for (auto [dx, dy] : directions)
                if (int x = i + dx, y = j + dy; dp[i][j] < dp[x][y])
                    dp[i][j] = dp[x][y],
                    count[i][j] = count[x][y];
                else if (dp[i][j] == dp[x][y])
                    count[i][j] = (count[i][j] + count[x][y]) % MOD;
            if ((dp[i][j] != -1) && (board[i][j] != 'E'))
                dp[i][j] = (dp[i][j] + (board[i][j] - '0')) % MOD;
        }
    }
    return {dp[0][0], count[0][0]};
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> board,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = pathsWithMaxScore(board);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"E23", "2X2", "12S"}, {7, 1}, trials);
    test({"E12", "1X1", "21S"}, {4, 2}, trials);
    test({"E11", "XXX", "11S"}, {0, 0}, trials);
    return 0;
}


