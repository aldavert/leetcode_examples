#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string getSmallestString(int n, int k)
{
    std::string result(n, 'a');
    int sum = n;
    for (int i = n - 1; (i >= 0) && (sum != k); --i)
    {
        int diff = std::min(k - sum, 25);
        result[i] = static_cast<char>('a' + diff);
        sum += diff;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getSmallestString(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 27, "aay", trials);
    test(5, 73, "aaszz", trials);
    return 0;
}


