#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 0
class MedianFinder
{
    std::priority_queue<int, std::vector<int>, std::greater<int> > m_upper_values;
    std::priority_queue<int, std::vector<int>, std::less<int> > m_lower_values;
public:
    MedianFinder(void) = default;
    void addNum(int num)
    {
        if (m_lower_values.empty() || m_lower_values.top() > num)
             m_lower_values.push(num);
        else m_upper_values.push(num);
        
        if (m_lower_values.size() > m_upper_values.size() + 1)
        {
            m_upper_values.push(m_lower_values.top());
            m_lower_values.pop();
        }
        else if (m_upper_values.size() > m_lower_values.size() + 1)
        {
            m_lower_values.push(m_upper_values.top());
            m_upper_values.pop();
        }
    }
    double findMedian(void)
    {
        if (m_upper_values.size() == m_lower_values.size())
            return (m_lower_values.empty())?0:(static_cast<double>(m_upper_values.top() + m_lower_values.top()) / 2.0);
        else return (m_upper_values.size() > m_lower_values.size())?static_cast<double>(m_upper_values.top()):static_cast<double>(m_lower_values.top());
    }
};
#else
class MedianFinder
{
    std::priority_queue<int, std::vector<int>, std::greater<int> > m_upper_values;
    std::priority_queue<int, std::vector<int>, std::less<int> > m_lower_values;
public:
    MedianFinder(void) = default;
    void addNum(int num)
    {
        if (m_upper_values.empty() || (num >= m_upper_values.top()))
             m_upper_values.push(num);
        else m_lower_values.push(num);
        
        if (m_upper_values.size() > m_lower_values.size() + 1)
        {
            m_lower_values.push(m_upper_values.top());
            m_upper_values.pop();
        }
        else if (m_lower_values.size() > m_upper_values.size() + 1)
        {
            m_upper_values.push(m_lower_values.top());
            m_lower_values.pop();
        }
    }
    double findMedian(void)
    {
        if (m_upper_values.size() == m_lower_values.size())
            return static_cast<double>(m_upper_values.top() + m_lower_values.top()) / 2.0;
        else if (m_upper_values.size() > m_lower_values.size())
            return static_cast<double>(m_upper_values.top());
        else return static_cast<double>(m_lower_values.top());
    }
};
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<double> &left, const std::vector<double> &right)
{
    constexpr double eps = 1e-6;
    if (left.size() != right.size()) return false;
    for (size_t i = 0, n = left.size(); i < n; ++i)
        if (std::abs(left[i] - right[i]) > eps)
            return true;
    return true;
}

void test(std::vector<std::vector<int> > numbers,
          std::vector<double> solution,
          unsigned int trials = 1)
{
    std::vector<double> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result.clear();
        MedianFinder finder;
        for (const auto &nums : numbers)
        {
            if (nums.size() == 0)
                result.push_back(finder.findMedian());
            else
            {
                for (int v : nums)
                    finder.addNum(v);
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1}, {2}, {}, {3}, {}}, {1.5, 2.0}, trials);
    test({{10, 3, 1}, {}, {20, 5, 8, -1}, {}}, {3, 5}, trials);
    return 0;
}


