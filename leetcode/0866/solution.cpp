#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int primePalindrome(int n)
{
    auto isPrime = [](int num) -> bool
    {
        for (int i = 2, m = static_cast<int>(std::sqrt(num)) + 1; i < m; ++i)
            if (num % i == 0)
                return false;
        return true;
    };
    auto getPalindromes = [](int length) -> std::vector<int>
    {
        std::vector<int> palindromes;
        length /= 2;
        for (int i = static_cast<int>(std::pow(10, length - 1)), m = i * 10; i < m; ++i)
        {
            const std::string str_value = std::to_string(i);
            std::string str_value_rev = str_value;
            std::reverse(str_value_rev.begin(), str_value_rev.end());
            for (int j = 0; j < 10; ++j)
                palindromes.push_back(stoi(str_value + std::to_string(j)
                                         + str_value_rev));
        }
        return palindromes;
    };
    
    if (n <=  2) return 2;
    if (n ==  3) return 3;
    if (n <=  5) return 5;
    if (n <=  7) return 7;
    if (n <= 11) return 11;
    for (int n_length = static_cast<int>(std::to_string(n).size()); true; ++n_length)
    {
        for (int num : getPalindromes(n_length))
            if ((num >= n) && isPrime(num))
                return num;
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = primePalindrome(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, 7, trials);
    test(8, 11, trials);
    test(13, 101, trials);
    return 0;
}


