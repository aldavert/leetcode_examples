#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int tallestBillboard(std::vector<int> rods)
{
    int sum = 0;
    for (int height : rods) sum += height;
    std::vector<int> dp[2] = { std::vector<int>(sum + 1, -1),
                               std::vector<int>(sum + 1, -1) };
    dp[0][0] = dp[1][0] = 0;
    int active = 1;
    for (const int h : rods)
    {
        active = !active;
        for (int i = 0; i <= sum - h; ++i)
        {
            if (dp[!active][i] < 0) continue;
            dp[active][i] = std::max(dp[active][i], dp[!active][i]);
            dp[active][i + h] = std::max(dp[active][i + h], dp[!active][i]);
            dp[active][abs(i - h)] = std::max(dp[active][abs(i - h)],
                                              dp[!active][i] + std::min(i, h));
        }
    }
    return dp[active][0];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> rods, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = tallestBillboard(rods);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 6}, 6, trials);
    test({1, 2, 3, 4, 5, 6}, 10, trials);
    test({1, 2}, 0, trials);
    return 0;
}


