#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

void solve(std::vector<std::vector<char> > &board)
{
    const unsigned char m = static_cast<unsigned char>(board.size());
    const unsigned char n = static_cast<unsigned char>(board[0].size());
    if ((m == 1) && (n == 1)) return;
    std::queue<std::pair<unsigned char, unsigned char> > fill;
    
    for (unsigned char i = 0; i < m; ++i)
    {
        if (board[i][0] == 'O')
            fill.push({i, 0});
        if (board[i][n - 1] == 'O')
            fill.push({i, n - 1});
    }
    for (unsigned char i = 0; i < n; ++i)
    {
        if (board[0][i] == 'O')
            fill.push({0, i});
        if (board[m - 1][i] == 'O')
            fill.push({m - 1, i});
    }
    
    auto check = [&](unsigned char x, unsigned char y)
    {
        if ((x < m) && (y < n) && (board[x][y] == 'O'))
            fill.push({x, y});
    };
    
    while (!fill.empty())
    {
        auto [x, y] = fill.front();
        fill.pop();
        board[x][y] = '#';
        check(x + 1, y);
        check(x - 1, y);
        check(x, y + 1);
        check(x, y - 1);
    }
    for (unsigned char x = 0; x < m; ++x)
    {
        for (unsigned char y = 0; y < n; ++y)
        {
            if (board[x][y] == 'O')
                board[x][y] = 'X';
            if (board[x][y] == '#')
                board[x][y] = 'O';
        }
    }
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > board,
          std::vector<std::vector<char> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<char> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = board;
        solve(result);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'X', 'X', 'X', 'X'},
          {'X', 'O', 'O', 'X'},
          {'X', 'X', 'O', 'X'},
          {'X', 'O', 'X', 'X'}},
         {{'X', 'X', 'X', 'X'},
          {'X', 'X', 'X', 'X'},
          {'X', 'X', 'X', 'X'},
          {'X', 'O', 'X', 'X'}}, trials);
    test({{'X', 'X', 'X', 'X'},
          {'X', 'O', 'O', 'X'},
          {'X', 'X', 'O', 'X'},
          {'X', 'O', 'X', 'X'},
          {'X', 'O', 'X', 'X'},
          {'X', 'O', 'X', 'X'}},
         {{'X', 'X', 'X', 'X'},
          {'X', 'X', 'X', 'X'},
          {'X', 'X', 'X', 'X'},
          {'X', 'O', 'X', 'X'},
          {'X', 'O', 'X', 'X'},
          {'X', 'O', 'X', 'X'}}, trials);
    test({{'X'}}, {{'X'}}, trials);
    return 0;
}


