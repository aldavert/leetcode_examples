#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sumSubseqWidths(std::vector<int> nums)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(nums.size());
    long result = 0, exp = 1;
    std::sort(nums.begin(), nums.end());
    for (int i = 0; i < n; ++i, exp = exp * 2 % MOD)
      result += (nums[i] - nums[n - i - 1]) * exp,
      result %= MOD;
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumSubseqWidths(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3}, 6, trials);
    test({2}, 0, trials);
    return 0;
}


