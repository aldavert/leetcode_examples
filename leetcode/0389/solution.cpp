#include "../common/common.hpp"

// ############################################################################
// ############################################################################

char findTheDifference(std::string s, std::string t)
{
    char histogram[128] = {};
    for (char c : t)
        ++histogram[static_cast<int>(c)];
    for (char c : s)
        --histogram[static_cast<int>(c)];
    for (int i = 'a'; i <= 'z'; ++i)
        if (histogram[i])
            return static_cast<char>(i);
    return '\0';
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string t, char solution, unsigned int trials = 1)
{
    char result = 0;
    for (unsigned int i = 0; i < trials; ++i)
        result = findTheDifference(s, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcd", "abcde", 'e', trials);
    test("", "y", 'y', trials);
    return 0;
}


