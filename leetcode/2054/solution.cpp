#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxTwoEvents(std::vector<std::vector<int> > events)
{
    struct Event
    {
        int time;
        int value;
        bool is_start;
        Event(int t, int v, bool is) : time(t), value(v), is_start(is) {}
        inline bool operator<(const Event &o) const
        {
            return (time == o.time)?(is_start < o.is_start):(time < o.time);
        }
    };
    int result = 0, max_value = 0;
    std::vector<Event> sorted_events;
    
    for (const auto &event : events)
    {
        sorted_events.emplace_back(event[0]    , event[2], true);
        sorted_events.emplace_back(event[1] + 1, event[2], false);
    }
    std::sort(sorted_events.begin(), sorted_events.end());
    for (auto [_, value, is_start] : sorted_events)
    {
        if (is_start) result = std::max(result, value + max_value);
        else max_value = std::max(max_value, value);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > events, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxTwoEvents(events);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 3, 2}, {4, 5, 2}, {2, 4, 3}}, 4, trials);
    test({{1, 3, 2}, {4, 5, 2}, {1, 5, 5}}, 5, trials);
    test({{1, 5, 3}, {1, 5, 1}, {6, 6, 5}}, 8, trials);
    return 0;
}


