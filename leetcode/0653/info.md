# Two Sum IV - Input is a BST

Given the `root` of a Binary Search Tree and a target number `k`, return `true` *if there exist two elements in the BST such that their sum is equal to the given target*.

#### Example 1:
> ```mermaid
> graph TD;
> A((5))---B((3))
> A---C((6))
> B---D((2))
> B---E((4))
> C---EMPTY(( ))
> C---F((7))
> classDef orange fill:#FDA,stroke:#000,stroke-width:2px;
> classDef green fill:#DFA,stroke:#000,stroke-width:2px;
> classDef blue fill:#ADF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY empty;
> class A,E orange;
> class B,C green;
> class D,F blue;
> linkStyle 4 stroke-width:0px;
> ```
> *Input:* `root = {5, 3, 6, 2, 4, null, 7}, k = 9`  
> *Output:* `true`

#### Example 2
> ```mermaid
> graph TD;
> A((5))---B((3))
> A---C((6))
> B---D((2))
> B---E((4))
> C---EMPTY(( ))
> C---F((7))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY empty;
> linkStyle 4 stroke-width:0px;
> ```
> *Input:* `root = {5, 3, 6, 2, 4, null, 7}, k = 28`  
> *Output:* `false`

#### Example 3:
> *Input:* `root = {2, 1, 3}, k = 4`  
> *Output:* `true`

#### Example 4:
> *Input:* `root = {2, 1, 3}, k = 1`  
> *Output:* `false`

#### Example 5:
> *Input:* `root = {2, 1, 3}, k = 3`  
> *Output:* `true`

#### Constrains:
- The number of nodes in the tree is in the range `[1, 10^4]`.
- `-10^4 <= Node.val <= 10^4`
- `root` is guaranteed to be a valid binary search tree.
- `-10^5 <= k <= 10^5`


