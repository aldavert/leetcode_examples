#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

bool findTarget(TreeNode * root, int k)
{
    std::unordered_set<int> lower_than_half;
    const int threshold = k / 2;
    auto process = [&](auto &&self, TreeNode * node) -> bool
    {
        if (node == nullptr) return false;
        if (node->val < threshold)
        {
            lower_than_half.insert(node->val);
            self(self, node->left);
            return self(self, node->right);
        }
        else
        {
            if (self(self, node->left))
                return true;
            if (auto it = lower_than_half.find(k - node->val);
                it != lower_than_half.end())
                return true;
            if (node->val == threshold)
                lower_than_half.insert(node->val);
            return self(self, node->right);
        }
    };
    return process(process, root);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int k, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = findTarget(root, k);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 3, 6, 2, 4, null, 7}, 9, true, trials);
    test({5, 3, 6, 2, 4, null, 7}, 28, false, trials);
    test({2, 1, 3}, 4, true, trials);
    test({2, 1, 3}, 1, false, trials);
    test({2, 1, 3}, 3, true, trials);
    test({-2, -3, -1}, -4, true, trials);
    test({-2, -3, -1}, -3, true, trials);
    return 0;
}


