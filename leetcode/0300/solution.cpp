#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int lengthOfLIS(std::vector<int> nums)
{
    const size_t n = nums.size();
    std::vector<int> buffer(n);
    const auto first = buffer.begin();
    int size = 1;
    buffer[0] = nums[0];
    for(size_t i = 1; i < n; ++i)
    {
        auto pos = std::distance(first, std::lower_bound(first, first + size, nums[i]));
        if (pos >= size) buffer[size++] = nums[i];
        else buffer[pos] = nums[i];
    }
    return size;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = lengthOfLIS(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 9, 2, 5, 3, 7, 101, 18}, 4, trials);

    //test({10, 9, 2, 5, 3, 7, 101, 18}, 4, trials);
    // 10->101
    //  +->18
    // 9->101
    // +->18
    // 2->3->7->101
    // |     +->18
    // +->5->101
    //    +->18
    // 10
    // 9
    // 2->3
    // 5
    // 3
    test({0, 1, 0, 3, 2, 3}, 4, trials);
    test({7, 7, 7, 7, 7, 7, 7}, 1, trials);
    test({101, 102, 103, 104, 105, 1, 2, 3, 4, 5, 6, 7}, 7, trials);
    return 0;
}


