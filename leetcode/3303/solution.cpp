#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minStartingIndex(std::string s, std::string pattern)
{
    auto reversed = [](const std::string &t) -> std::string
    {
        return {t.rbegin(), t.rend()};
    };
    auto zFunction = [](const std::string &t) -> std::vector<int>
    {
        const int n = static_cast<int>(t.size());
        std::vector<int> z(n);
        for (int i = 1, l = 0, r = 0; i < n; ++i)
        {
            if (i < r)
                z[i] = std::min(r - i, z[i - l]);
            while ((i + z[i] < n) && (t[z[i]] == t[i + z[i]]))
                ++z[i];
            if (i + z[i] > r)
            {
                l = i;
                r = i + z[i];
            }
        }
        return z;
    };
    const int n = static_cast<int>(s.size()),
              m = static_cast<int>(pattern.size());
    const std::vector<int> z1 = zFunction(pattern + s);
    const std::vector<int> z2 = zFunction(reversed(pattern) + reversed(s));
    for (int i = 0; i <= n - m; ++i)
        if (z1[m + i] + z2[n - i] >= m - 1)
            return i;
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string pattern, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minStartingIndex(s, pattern);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcdefg", "bcdffg", 1, trials);
    test("ababbababa", "bacaba", 4, trials);
    test("abcd", "dba", -1, trials);
    test("dde", "d", 0, trials);
    return 0;
}


