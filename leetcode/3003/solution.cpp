#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int maxPartitionsAfterOperations(std::string s, int k)
{
    auto count = [](unsigned int bits) -> int { return std::popcount(bits); };
    if (k == 26) return 1;
    const int n = static_cast<int>(s.size());
    s = '@' + s + '@';
    std::vector<int> pref(n + 2), pval(n + 2);
    for (int i = 1, prefix = 0, pbit = 0; i <= n; ++i)
    {
        int bit = (1 << (s[i] - 'a'));
        pbit |= bit;
        if (count(pbit) > k)
        {
            ++prefix;
            pbit = bit;
        }
        pref[i] = prefix;
        pval[i] = pbit;
    }
    std::vector<int> suff(n + 2), sval(n + 2);
    for (int i = n, suffix = 0, sbit = 0; i >= 1; --i)
    {
        int bit = (1 << (s[i] - 'a'));
        sbit |= bit;
        if (count(sbit) > k)
        {
            ++suffix;
            sbit = bit;
        }
        suff[i] = suffix;
        sval[i] = sbit;
    }
    int result = 0;
    for (int i = 1; i <= n; ++i)
    {
        int val = pref[i - 1] + suff[i + 1], prefix = pval[i - 1], suffix = sval[i + 1],
            x = prefix | suffix;
        if (count(x) + 1 <= k) val += 1;
        else if ((count(prefix) == k) && (count(suffix) == k) && (count(x) < 26)) val += 3;
        else val += 2;
        result = std::max(val, result);
    }
    return result;
}
#else
int maxPartitionsAfterOperations(std::string s, int k)
{
    const int n = static_cast<int>(s.size());
    std::unordered_map<long, int> mem;
    
    auto process = [&](auto &&self, int i, bool can_change, int mask) -> int
    {
        if (i == n) return 0;
        long key = static_cast<long>(i) << 27 | (can_change?1:0) << 26 | mask;
        if (const auto it = mem.find(key); it != mem.end())
            return it->second;
        
        const unsigned int first_bit = 1 << (s[i] - 'a');
        const unsigned int first_mask = mask | first_bit;
        int result = (std::popcount(first_mask) > k)
                   ? 1 + self(self, i + 1, can_change, first_bit)
                   : self(self, i + 1, can_change, first_mask);
        if (can_change)
        {
            for (int j = 0; j < 26; ++j)
            {
                const unsigned int new_bit = 1 << j;
                const unsigned int next_mask = mask | new_bit;
                int current = (std::popcount(next_mask) > k)
                            ? 1 + self(self, i + 1, false, new_bit)
                            : self(self, i + 1, false, next_mask);
                result = std::max(result, current);
            }
        }
        return mem[key] = result;
    };
    return process(process, 0, true, 0) + 1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxPartitionsAfterOperations(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("accca", 2, 3, trials);
    test("aabaab", 3, 1, trials);
    return 0;
}


