#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int lastStoneWeightII(std::vector<int> stones)
{
    const int sum = std::accumulate(stones.begin(), stones.end(), 0);
    std::vector<bool> dp(sum + 1);
    dp[0] = true;
    int s = 0;
    for (int stone : stones)
    {
        for (int w = sum / 2; w > 0; --w)
        {
            if (w >= stone) dp[w] = dp[w] || dp[w - stone];
            if (dp[w]) s = std::max(s, w);
        }
    }
    return sum - 2 * s;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> stones, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = lastStoneWeightII(stones);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 7, 4, 1, 8, 1}, 1, trials);
    test({31, 26, 33, 21, 40}, 5, trials);
    return 0;
}


