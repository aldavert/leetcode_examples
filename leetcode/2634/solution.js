var filter = function(arr, fn) {
    let res = []
    arr.forEach((punch, i) => {
        if (fn.call(this, punch, i))
            res.push(punch)
    });
    return res;
};
