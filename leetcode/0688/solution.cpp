#include "../common/common.hpp"

// ############################################################################
// ############################################################################

double knightProbability(int n, int k, int row, int column)
{
    constexpr double PROB = 0.125;
    const std::vector<std::pair<int, int>> directions = {
        {-2, 1}, {-1, 2}, {1, 2}, {2, 1}, {2, -1}, {1, -2}, {-1, -2}, {-2, -1}};
    std::vector<std::vector<double> > dp(n, std::vector<double>(n));
    dp[row][column] = 1.0;
    
    for (int m = 0; m < k; ++m)
    {
        std::vector<std::vector<double> > new_dp(n, std::vector<double>(n));
        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                if (dp[i][j] > 0.0)
                {
                    for (const auto& [dx, dy] : directions)
                    {
                        int x = i + dx, y = j + dy;
                        if ((x < 0) || (x >= n) || (y < 0) || (y >= n)) continue;
                        new_dp[x][y] += dp[i][j] * PROB;
                    }
                }
            }
        }
        dp = new_dp;
    }
    
    double result = 0.0;
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            result += dp[i][j];
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int k, int row, int column, double solution, unsigned int trials = 1)
{
    double result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = knightProbability(n, k, row, column);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 2, 0, 0, 0.06250, trials);
    test(1, 0, 0, 0, 1.00000, trials);
    return 0;
}


