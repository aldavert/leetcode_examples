#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int kConcatenationMaxSum(std::vector<int> arr, int k)
{
    constexpr int MOD = 1'000'000'007;
    int size = static_cast<int>(arr.size()) * (2 - (k == 1)),
        sum = std::max(0, std::accumulate(arr.begin(), arr.end(), 0)),
        result = 0;
    for (int i = 0, accum = 0; i < size; ++i)
    {
        const int value = arr[i % arr.size()];
        accum = std::max(value, accum + value);
        result = std::max(result, accum);
    }
    return static_cast<int>((result + sum * std::max<long>(0, k - 2)) % MOD);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = kConcatenationMaxSum(arr, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2}, 3, 9, trials);
    test({1, -2, 1}, 5, 2, trials);
    test({-1, -2}, 7, 0, trials);
    return 0;
}


