#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minFlips(std::string s)
{
    const int n = static_cast<int>(s.size());
    int count[2][2] = {};
    for (int i = 0; i < n; ++i)
        ++count[s[i] - '0'][i % 2];
    int result = std::min(count[1][0] + count[0][1], count[0][0] + count[1][1]);
    for (int i = 0; i < n; ++i)
    {
        --count[s[i] - '0'][i % 2];
        ++count[s[i] - '0'][(n + i) % 2];
        result = std::min({result, count[1][0] + count[0][1],
                                   count[0][0] + count[1][1]});
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minFlips(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("111000", 2, trials);
    test("010", 0, trials);
    test("1110", 1, trials);
    return 0;
}


