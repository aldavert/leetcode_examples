#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

#if 0
int countNodes(TreeNode * root)
{
    int maximum = 0, minimum = 0;
    for (TreeNode * ptr = root; ptr != nullptr; ptr = ptr->left) ++maximum;
    for (TreeNode * ptr = root; ptr != nullptr; ptr = ptr->right) ++minimum;
    if (maximum == minimum)
        return (1 << maximum) - 1;
    auto countInnerNodes = [&](auto &&self, TreeNode * curr, int left, int right) -> int
    {
        if (left < 0)
        {
            left = 0;
            for (TreeNode * ptr = curr; ptr != nullptr; ptr = ptr->left) ++left;
        }
        else
        {
            right = 0;
            for (TreeNode * ptr = curr; ptr != nullptr; ptr = ptr->right) ++right;
        }
        if (left == right)
            return (1 << left) - 1;
        return 1
             + self(self, curr->left , left - 1,        -1)
             + self(self, curr->right,       -1, right - 1);
    };
    return 1
         + countInnerNodes(countInnerNodes, root->left , maximum - 1,          -1)
         + countInnerNodes(countInnerNodes, root->right,          -1, minimum - 1);
}
#elif 1
int countNodes(TreeNode * root)
{
    int maximum = 0, minimum = 0;
    for (TreeNode * ptr = root; ptr != nullptr; ptr = ptr->left) ++maximum;
    for (TreeNode * ptr = root; ptr != nullptr; ptr = ptr->right) ++minimum;
    if (maximum == minimum)
        return (1 << maximum) - 1;
    return 1
         + countNodes(root->left )
         + countNodes(root->right);
}
#elif 0
int countNodes(TreeNode * root)
{
    if (root == nullptr) return 0;
    if (root->left == nullptr) return 1;
    int maximum_height = 0;
    for (TreeNode * ptr = root; ptr != nullptr; ptr = ptr->left) ++maximum_height;
    const int number_of_bits = maximum_height - 1;
    const int number_of_leafs = 1 << number_of_bits;
    int minimum_height = 0;
    for (TreeNode * ptr = root; ptr != nullptr; ptr = ptr->right) ++minimum_height;
    if (maximum_height == minimum_height)
        return (number_of_leafs << 1) - 1;
    int right = number_of_leafs - 1;
    for (int left = 0; right - left > 1;)
    {
        int aux = (left + right) / 2;
        TreeNode * current = root;
        for (int displacement = number_of_bits - 1; displacement >= 0; --displacement)
        {
            if ((aux >> displacement) & 1) current = current->right;
            else current = current->left;
        }
        if (current == nullptr) right = aux;
        else left = aux;
    }
    return (number_of_leafs << 1) - 1 - number_of_leafs + right;
}
#else
int countNodes(TreeNode * root)
{
    if (root == nullptr) return 0;
    if (root->left == nullptr) return 1;
    int maximum_height = 0;
    for (TreeNode * ptr = root; ptr != nullptr; ptr = ptr->left) ++maximum_height;
    int number_empty = 0;
    
    auto traverse = [&](auto &&self, TreeNode * current, int height) -> bool
    {
        if (current == nullptr)
        {
            if (height == maximum_height) return true;
            ++number_empty;
            return false;
        }
        if (self(self, current->right, height + 1))
            return true;
        return self(self, current->left, height + 1);
    };
    traverse(traverse, root, 0);
    
    return (1 << maximum_height) - 1 - number_empty;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = countNodes(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
    constexpr int TEST_N [[maybe_unused]] = 256;
    constexpr int BIG_TREE_SIZE [[maybe_unused]] = 5'000'000;
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 4}, 3, trials);
    test({1, 2, 3, 4}, 4, trials);
    test({1, 2, 3, 4, 5}, 5, trials);
    test({1, 2, 3, 4, 5, 6}, 6, trials);
    test({1, 2, 3, 4, 5, 6, 7}, 7, trials);
    test({}, 0, trials);
    test({1}, 1, trials);
#if 0
    std::vector<int> tree;
    tree.reserve(TEST_N);
    for (int i = 0; i < TEST_N; i += 4)
    {
        test(tree, i, trials);
        for (int j = 0; j < 4; ++j)
            tree.push_back(i + 1 + j);
    }
#endif
    std::vector<int> big_tree(BIG_TREE_SIZE);
    for (int i = 0; i < BIG_TREE_SIZE; ++i) big_tree[i] = i + 1;
    test(big_tree, BIG_TREE_SIZE, trials);
    return 0;
}


