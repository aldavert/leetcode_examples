#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

bool isValid(std::string s)
{
    std::stack<char> stack;
    for (char c : s)
    {
        if (c == 'c')
        {
            if (stack.size() < 2) return false;
            if (stack.top() != 'b') return false;
            stack.pop();
            if (stack.top() != 'a') return false;
            stack.pop();
        }
        else stack.push(c);
    }
    return stack.empty();
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isValid(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aabcbc", true, trials);
    test("abcabcababcc", true, trials);
    test("abccba", false, trials);
    return 0;
}


