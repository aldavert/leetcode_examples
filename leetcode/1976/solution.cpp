#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int countPaths(int n, std::vector<std::vector<int> > roads)
{
    std::vector<std::vector<std::pair<int, int> > > graph(n);
    constexpr int MOD = 1'000'000'007;
    std::vector<long> ways(graph.size()),
                      dist(graph.size(), std::numeric_limits<long>::max());
    std::priority_queue<std::pair<long, int>,
                        std::vector<std::pair<long, int> >,
                        std::greater<> > heap;
    for (const auto &road : roads)
    {
        graph[road[0]].emplace_back(road[1], road[2]);
        graph[road[1]].emplace_back(road[0], road[2]);
    }
    ways[0] = 1;
    dist[0] = 0;
    heap.emplace(dist[0], 0);
    while (!heap.empty())
    {
        const auto [d, u] = heap.top();
        heap.pop();
        if (d > dist[u]) continue;
        for (const auto &[v, w] : graph[u])
        {
            if (d + w < dist[v])
            {
                dist[v] = d + w;
                ways[v] = ways[u];
                heap.emplace(dist[v], v);
            }
            else if (d + w == dist[v])
                ways[v] = (ways[v] + ways[u]) % MOD;
        }
    }
    return static_cast<int>(ways[n - 1]);
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > roads,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPaths(n, roads);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, {{0, 6, 7}, {0, 1, 2}, {1, 2, 3}, {1, 3, 3}, {6, 3, 3}, {3, 5, 1},
             {6, 5, 1}, {2, 5, 1}, {0, 4, 5}, {4, 6, 2}}, 4, trials);
    test(2, {{1, 0, 10}}, 1, trials);
    return 0;
}


