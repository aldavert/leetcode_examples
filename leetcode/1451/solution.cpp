#include "../common/common.hpp"
#include <map>

// ############################################################################
// ############################################################################

std::string arrangeWords(std::string text)
{
    std::map<size_t, std::vector<std::string> > count;
    std::string token;
    for (char symbol : text)
    {
        if (symbol == ' ')
        {
            if (token.size() == 0) continue;
            count[token.size()].push_back(token);
            token = "";
        }
        else token += static_cast<char>(std::tolower(symbol));
    }
    if (token.size() != 0)
        count[token.size()].push_back(token);
    std::string result;
    for (bool next = false; auto &[length, list] : count)
    {
        for (auto &word: list)
        {
            if (next) [[likely]] result += ' ';
            next = true;
            result += word;
        }
    }
    result[0] = static_cast<char>(std::toupper(result[0]));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string text, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = arrangeWords(text);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("Leetcode is cool", "Is cool leetcode", trials);
    test("Keep calm and code on", "On and keep calm code", trials);
    test("To be or not to be", "To be or to be not", trials);
    return 0;
}


