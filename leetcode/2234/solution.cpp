#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

long long maximumBeauty(std::vector<int> flowers,
                        long long newFlowers,
                        int target,
                        int full,
                        int partial)
{
    const long n = static_cast<long>(flowers.size());
    
    for (int &flower : flowers)
        flower = std::min(flower, target);
    std::sort(flowers.begin(), flowers.end());
    if (flowers[0] == target)
        return n * full;
    if (newFlowers >= n * target - std::accumulate(flowers.begin(), flowers.end(), 0LL))
        return std::max(n * full,
                        (n - 1) * full + static_cast<long>(target - 1) * partial);
    
    long result = 0, left_flowers = newFlowers;
    std::vector<long> cost(n);
    for (long i = 1; i < n; ++i)
        cost[i] = cost[i - 1] + i * (flowers[i] - flowers[i - 1]);
    long i = n - 1;
    while (flowers[i] == target) --i;
    for (; left_flowers >= 0; --i)
    {
        long j = std::upper_bound(cost.begin(), cost.begin() + i + 1, left_flowers)
               - cost.begin();
        long min_incomplete = flowers[j - 1] + (left_flowers - cost[j - 1]) / j;
        result = std::max(result, (n - 1 - i) * full + min_incomplete * partial);
        left_flowers -= std::max(0, target - flowers[i]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> flowers,
          long long newFlowers,
          int target,
          int full,
          int partial,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumBeauty(flowers, newFlowers, target, full, partial);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 1, 1},  7, 6, 12, 1, 14, trials);
    test({2, 4, 5, 3}, 10, 5,  2, 6, 30, trials);
    test({20, 1, 15, 17, 10, 2, 4, 16, 15, 11}, 2, 20,  10, 2, 14, trials);
    return 0;
}
