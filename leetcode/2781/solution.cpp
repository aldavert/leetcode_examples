#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestValidSubstring(std::string word, std::vector<std::string> forbidden)
{
    const int n = static_cast<int>(word.size());
    std::unordered_set<std::string> forbidden_set{forbidden.begin(), forbidden.end()};
    
    int result = 0;
    for (int l = n - 1, r = n - 1; l >= 0; --l)
    {
        for (int end = l; end < std::min(l + 10, r + 1); ++end)
        {
            if (forbidden_set.find(word.substr(l, end - l + 1)) != forbidden_set.end())
            {
                r = end - 1;
                break;
            }
        }
        result = std::max(result, r - l + 1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word,
          std::vector<std::string> forbidden,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestValidSubstring(word, forbidden);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("cbaaaabc", {"aaa", "cb"}, 4, trials);
    test("leetcode", {"de", "le", "e"}, 4, trials);
    return 0;
}


