#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int minNumberOperations(std::vector<int> target)
{
    int result = target[0];
    for (int previous = target[0]; int current : target)
        result += std::max(0, current - std::exchange(previous, current));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minNumberOperations(target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 2, 1}, 3, trials);
    test({3, 1, 1, 2}, 4, trials);
    test({3, 1, 5, 4, 2}, 7, trials);
    return 0;
}



