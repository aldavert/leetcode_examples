#include "../common/common.hpp"
#include <deque>

// ############################################################################
// ############################################################################

bool checkArray(std::vector<int> nums, int k)
{
    if (k == 1) return true;
    int need_decrease = 0;
    std::deque<int> dq;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        if (i >= k)
        {
            need_decrease -= dq.front();
            dq.pop_front();
        }
        if (nums[i] < need_decrease) return false;
        const int decreased_num = nums[i] - need_decrease;
        dq.push_back(decreased_num);
        need_decrease += decreased_num;
    }
    return dq.back() == 0;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkArray(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 2, 3, 1, 1, 0}, 3, true, trials);
    test({1, 3, 1, 1}, 2, false, trials);
    return 0;
}


