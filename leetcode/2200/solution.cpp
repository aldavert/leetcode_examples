#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findKDistantIndices(std::vector<int> nums, int key, int k)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> result;
    int last_key = n, i = 0;
    for (; i < k; ++i)
        if (nums[i] == key)
            last_key = i;
    for (; i < n; ++i)
    {
        if (nums[i] == key)
            last_key = i;
        if (std::abs(i - k - last_key) <= k)
            result.push_back(i - k);
    }
    for (int j = k; j > 0; --j)
        if (std::abs(n - j - last_key) <= k)
            result.push_back(n - j);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int key,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findKDistantIndices(nums, key, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 4, 9, 1, 3, 9, 5}, 9, 1, {1, 2, 3, 4, 5, 6}, trials);
    test({3, 4, 9, 1, 3, 9, 5}, 9, 2, {0, 1, 2, 3, 4, 5, 6}, trials);
    test({2, 2, 2, 2, 2}, 2, 2, {0, 1, 2, 3, 4}, trials);
    return 0;
}


