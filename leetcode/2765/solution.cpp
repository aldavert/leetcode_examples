#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int alternatingSubarray(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = -1;
    for (int i = 1, previous = 0, sequence = 0; i < n; ++i)
    {
        int diff = nums[i] - nums[i - 1];
        if ((diff == 1) || (diff == -1))
        {
            if ((previous == 0) && (diff == -1)) continue;
            if ((previous == 0) || (previous == diff))
                sequence = 2;
            else ++sequence;
            result = std::max(result, sequence);
            previous = diff;
        }
        else previous = sequence = 0;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = alternatingSubarray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 4, 3, 4}, 4, trials);
    test({4, 5, 6}, 2, trials);
    test({14, 30, 29, 49, 3, 23, 44, 21, 26, 52}, -1, trials);
    return 0;
}


