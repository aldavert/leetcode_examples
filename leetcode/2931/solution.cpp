#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maxSpending(std::vector<std::vector<int> > values)
{
    long result = 0, d = 1;
    std::vector<int> items;
    for (const auto &shop : values)
        for (const int item : shop)
            items.push_back(item);
    std::sort(items.begin(), items.end());
    for (const int item : items)
        result += item * d++;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > values,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSpending(values);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{8, 5, 2}, {6, 4, 1}, {9, 7, 3}}, 285, trials);
    test({{10, 8, 6, 4, 2}, {9, 7, 5, 3, 2}}, 386, trials);
}


