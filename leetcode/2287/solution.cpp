#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int rearrangeCharacters(std::string s, std::string target)
{
    int histogram_s[26] = {}, histogram_t[26] = {};
    for (char letter : s) ++histogram_s[letter - 'a'];
    for (char letter : target) ++histogram_t[letter - 'a'];
    int result = static_cast<int>(s.size());
    for (int i = 0; i < 26; ++i)
        if (histogram_t[i])
            result = std::min(result, histogram_s[i] / histogram_t[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = rearrangeCharacters(s, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ilovecodingonleetcode", "code", 2, trials);
    test("abcba", "abc", 1, trials);
    test("abbaccaddaeea", "aaaaa", 1, trials);
    return 0;
}


