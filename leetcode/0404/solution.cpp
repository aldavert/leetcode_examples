#include "../common/common.hpp"
#include "../common/tree.hpp"

// #############################################################################
// #############################################################################

int sumOfLeftLeaves(TreeNode * root)
{
    int result = 0;
    auto search = [&](auto &&self, TreeNode * node, bool left) -> void
    {
        if (left && !node->left && !node->right)
            result += node->val;
        else
        {
            if (node->left) self(self, node->left, true);
            if (node->right) self(self, node->right, false);
        }
    };
    search(search, root, false);
    return result;
}

// #############################################################################
// #############################################################################

void test(const std::vector<int> &tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = sumOfLeftLeaves(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 9, 20, null, null, 15, 7}, 24, trials);
    test({1}, 0, trials);
    return 0;
}


