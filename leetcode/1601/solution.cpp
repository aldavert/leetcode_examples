#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumRequests(int n, std::vector<std::vector<int> > requests)
{
    const int m = static_cast<int>(requests.size());
    int result = 0;
    std::vector<int> degree(n);
    
    auto dfs = [&](auto &&self, int i, int processed_requests) -> void
    {
        if (i == m)
        {
            if (all_of(degree.begin(), degree.end(), [](int d){ return d == 0; }))
                result = std::max(result, processed_requests);
            return;
        }
        self(self, i + 1, processed_requests);
        --degree[requests[i][0]];
        ++degree[requests[i][1]];
        self(self, i + 1, processed_requests + 1);
        --degree[requests[i][1]];
        ++degree[requests[i][0]];
    };
    dfs(dfs, 0, 0);
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > requests,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumRequests(n, requests);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{0, 1}, {1, 0}, {0, 1}, {1, 2}, {2, 0}, {3, 4}}, 5, trials);
    test(3, {{0, 0}, {1, 2}, {2, 1}}, 3, trials);
    test(4, {{0, 3}, {3, 1}, {1, 2}, {2, 0}}, 4, trials);
    return 0;
}


