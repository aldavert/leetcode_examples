#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

std::vector<int> gardenNoAdj(int n, std::vector<std::vector<int> > paths)
{
    std::vector<int> result(n);
    std::vector<std::vector<int> > graph(n);
    std::bitset<5> used;
    
    for (const auto &p : paths)
    {
        graph[p[0] - 1].push_back(p[1] - 1);
        graph[p[1] - 1].push_back(p[0] - 1);
    }
    for (int i = 0; i < n; ++i)
    {
        used.reset();
        for (int v : graph[i])
            used[result[v]] = true;
        for (int type = 1; type < 5; ++type)
        {
            if (!used[type])
            {
                result[i] = type;
                break;
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > paths,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = gardenNoAdj(n, paths);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{1, 2}, {2, 3}, {3, 1}}, {1, 2, 3}, trials);
    test(4, {{1, 2}, {3, 4}}, {1, 2, 1, 2}, trials);
    test(4, {{1, 2}, {2, 3}, {3, 4}, {4, 1}, {1, 3}, {2, 4}}, {1, 2, 3, 4}, trials);
    return 0;
}


