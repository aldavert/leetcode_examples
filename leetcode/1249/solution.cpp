#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 0
std::string minRemoveToMakeValid(std::string s)
{
    const int n = static_cast<int>(s.size());
    std::stack<int> parenthesis_to_remove;
    for (int i = 0; i < n; ++i)
    {
        if (s[i] == '(')
            parenthesis_to_remove.push(i);
        else if (s[i] == ')')
        {
            if (parenthesis_to_remove.empty())
                s[i] = '#';
            else parenthesis_to_remove.pop();
        }
    }
    for (; !parenthesis_to_remove.empty(); parenthesis_to_remove.pop())
        s[parenthesis_to_remove.top()] = '#';
    int idx = 0;
    for (int j = 0; j < n; ++j)
        if (s[j] != '#')
            s[idx++] = s[j];
    return s.substr(0, idx);
}
#else
std::string minRemoveToMakeValid(std::string s)
{
    const int n = static_cast<int>(s.size());
    std::stack<int> parenthesis_to_remove;
    for (int i = 0; i < n; ++i)
    {
        if (s[i] == '(')
            parenthesis_to_remove.push(i);
        else if (s[i] == ')')
        {
            if (parenthesis_to_remove.empty())
                s[i] = '#';
            else parenthesis_to_remove.pop();
        }
    }
    for (; !parenthesis_to_remove.empty(); parenthesis_to_remove.pop())
        s[parenthesis_to_remove.top()] = '#';
    s.erase(std::remove(s.begin(), s.end(), '#'), s.end());
    return s;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minRemoveToMakeValid(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("lee(t(c)o)de)", "lee(t(c)o)de", trials);
    test("a)b(c)d", "ab(c)d", trials);
    test("))((", "", trials);
    test("(())", "(())", trials);
    test("", "", trials);
    return 0;
}


