#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string reverseOnlyLetters(std::string s)
{
    auto valid = [](char c)
    {
        return ((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z'));
    };
    for (int i = 0, j = static_cast<int>(s.size()) - 1; i < j;)
    {
        bool vi = valid(s[i]);
        bool vj = valid(s[j]);
        if (vi && vj) std::swap(s[i++], s[j--]);
        else if (vi) --j;
        else ++i;
    }
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = reverseOnlyLetters(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ab-cd", "dc-ba", trials);
    test("a-bC-dEf-ghIj", "j-Ih-gfE-dCba", trials);
    test("Test1ng-Leet=code-Q!", "Qedo1ct-eeLg=ntse-T!", trials);
    return 0;
}


