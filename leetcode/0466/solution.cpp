#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int getMaxRepetitions(std::string s1, int n1, std::string s2, int n2)
{
    const int l1 = static_cast<int>(s1.size());
    const int l2 = static_cast<int>(s2.size());
    struct Info 
    {
        int count = 0;
        int next = 0;
    };
    std::vector<Info> records(l2);
    for (int i = 0; i < l2; ++i)
    {
        records[i].next = i;
        for (int j = 0; j < l1; ++j)
        {
            if (s2[records[i].next] == s1[j])
            {
                if (++records[i].next == l2)
                {
                    ++records[i].count;
                    records[i].next = 0;
                }
            }
        }
    }
    int matches = 0, index = 0;
    while (n1--)
    {
        matches += records[index].count;
        index = records[index].next;
    }
    return matches / n2;
}

// ############################################################################
// ############################################################################

void test(std::string s1,
          int n1,
          std::string s2,
          int n2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getMaxRepetitions(s1, n1, s2, n2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("acb", 4, "ab", 2, 2, trials);
    test("acb", 1, "acb", 1, 1, trials);
    return 0;
}


