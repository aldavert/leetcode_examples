#include "../common/common.hpp"
#include <utility>
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int fillCups(std::vector<int> amount)
{
    int a = amount[0], b = amount[1], c = amount[2];
    if (a > b) std::swap(a, b);
    if (a > c) a = std::exchange(c, std::exchange(b, a));
    else if (b > c) std::swap(b, c);
    int result = 0;
    while (c > 0)
    {
        --c;
        --b;
        if (a > b) std::swap(a, b);
        if (a > c) a = std::exchange(c, std::exchange(b, a));
        else if (b > c) std::swap(b, c);
        ++result;
    }
    return result;
}
#elif 0
int fillCups(std::vector<int> amount)
{
    std::sort(amount.begin(), amount.end());
    int result = 0;
    while (amount[2] > 0)
    {
        --amount[2];
        --amount[1];
        std::sort(amount.begin(), amount.end());
        ++result;
    }
    return result;
}
#else
int fillCups(std::vector<int> amount)
{
    std::priority_queue<int> q;
    for (int value : amount) if (value > 0) q.push(value);
    int result = 0;
    for (; q.size() > 1; ++result)
    {
        int a = q.top() - 1;
        q.pop();
        int b = q.top() - 1;
        q.pop();
        if (a > 0) q.push(a);
        if (b > 0) q.push(b);
    }
    return result + (q.empty()?0:q.top());
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> amount, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = fillCups(amount);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 2}, 4, trials);
    test({5, 4, 4}, 7, trials);
    test({5, 0, 0}, 5, trials);
    return 0;
}


