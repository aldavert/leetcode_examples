#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int minimumOperations(TreeNode * root)
{
    std::queue<TreeNode* > q{{root}};
    int result = 0;
    
    while (!q.empty())
    {
        const int n = static_cast<int>(q.size());
        std::vector<int> vals, ids(n);
        for (int sz = n; sz > 0; --sz)
        {
            TreeNode * node = q.front();
            q.pop();
            vals.push_back(node->val);
            if (node->left != nullptr)
                q.push(node->left);
            if (node->right != nullptr)
                q.push(node->right);
        }
        for (int i = 0; i < n; ++i) ids[i] = i;
        std::sort(ids.begin(), ids.end(),
                  [&vals](int i, int j) { return vals[i] < vals[j]; });
        for (int i = 0; i < n; ++i)
            for (; ids[i] != i; ++result)
                std::swap(ids[i], ids[ids[i]]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = minimumOperations(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 3, 7, 6, 8, 5, null, null, null, null, 9, null, 10}, 3, trials);
    test({1, 3, 2, 7, 6, 5, 4}, 3, trials);
    test({1, 2, 3, 4, 5, 6}, 0, trials);
    return 0;
}


