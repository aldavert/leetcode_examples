#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sumImbalanceNumbers(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> left(n), right(n), num_to_index(n + 2, -1);
    for (int i = 0; i < n; ++i)
    {
        left[i] = std::max(num_to_index[nums[i]], num_to_index[nums[i] + 1]);
        num_to_index[nums[i]] = i;
    }
    
    for (int i = 0; i <= n + 1; ++i) num_to_index[i] = n;
    for (int i = n - 1; i >= 0; --i)
    {
        right[i] = num_to_index[nums[i] + 1];
        num_to_index[nums[i]] = i;
    }
    
    int result = 0;
    for (int i = 0; i < n; ++i) result += (i - left[i]) * (right[i] - i);
    return result - n * (n + 1) / 2;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumImbalanceNumbers(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 1, 4}, 3, trials);
    test({1, 3, 3, 3, 5}, 8, trials);
    return 0;
}


