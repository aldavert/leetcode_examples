#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int beautifulPartitions(std::string s, int k, int minLength)
{
    auto isPrime = [](char c) -> bool
    {
        return c == '2' || c == '3' || c == '5' || c == '7';
    };
    constexpr int MOD = 1'000'000'007;
    if (!isPrime(s.front()) || isPrime(s.back())) return 0;
    const int n = static_cast<int>(s.size());
    std::vector<std::vector<int> > dp(n, std::vector<int>(k, -1));
    auto partitions = [&](auto &&self, int i, int q) -> int
    {
        if ((i <= n) && (q == 0)) return 1;
        if (i >= n) return 0;
        if (dp[i][q] != -1) return dp[i][q];
        
        int result = self(self, i + 1, q) % MOD;
        if (isPrime(s[i]) && !isPrime(s[i - 1]))
            result += self(self, i + minLength, q - 1);
        return dp[i][q] = result % MOD;
    };
    return partitions(partitions, minLength, k - 1);
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int minLength, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = beautifulPartitions(s, k, minLength);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("23542185131", 3, 2, 3, trials);
    test("23542185131", 3, 3, 1, trials);
    test("3312958", 3, 1, 1, trials);
    return 0;
}


