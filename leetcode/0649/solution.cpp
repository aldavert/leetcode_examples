#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::string predictPartyVictory(std::string senate)
{
    const int n = static_cast<int>(senate.size());
    std::queue<int> dire, radiant;
    for (int i = 0; i < n; ++i)
        if (senate[i] == 'R') radiant.push(i);
        else dire.push(i);
    while (!radiant.empty() && !dire.empty())
    {
        if (radiant.front() < dire.front())
            radiant.push(radiant.front() + n);
        else dire.push(dire.front() + n);
        radiant.pop();
        dire.pop();
    }
    return (dire.empty())?"Radiant":"Dire";
}

// ############################################################################
// ############################################################################

void test(std::string senate, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = predictPartyVictory(senate);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("RD", "Radiant", trials);
    test("RDD", "Dire", trials);
    return 0;
}


