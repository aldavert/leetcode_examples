#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::string kthLargestNumber(std::vector<std::string> nums, int k)
{
    auto compare = [](const std::string &a, const std::string &b) -> bool
    {
        return (a.size() == b.size())?a > b:a.size() > b.size();
    };
    std::priority_queue<std::string,
                        std::vector<std::string>,
                        decltype(compare)> heap(compare);
    for (const auto &num : nums)
    {
        heap.push(num);
        if (static_cast<int>(heap.size()) > k)
            heap.pop();
    }
    return heap.top();
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> nums,
          int k,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = kthLargestNumber(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"3", "6", "7", "10"}, 4, "3", trials);
    test({"2", "21", "12", "1"}, 3, "2", trials);
    test({"0", "0"}, 2, "0", trials);
    return 0;
}


