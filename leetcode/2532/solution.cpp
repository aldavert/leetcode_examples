#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int findCrossingTime(int n, int k, std::vector<std::vector<int> > time)
{
    int result = 0;
    using P = std::pair<int, int>;
    std::priority_queue<P> left_bridge, right_bridge;
    std::priority_queue<P, std::vector<P>, std::greater<> > left_workers, right_workers;
    
    for (int i = 0; i < k; ++i)
        left_bridge.push({time[i][0] + time[i][2], i});
    while (n > 0 || !right_bridge.empty() || !right_workers.empty())
    {
        while (!left_workers.empty() && left_workers.top().first <= result)
        {
            const int i = left_workers.top().second;
            left_workers.pop();
            left_bridge.push({time[i][0] + time[i][2], i});
        }
        while (!right_workers.empty() && right_workers.top().first <= result)
        {
            const int i = right_workers.top().second;
            right_workers.pop();
            right_bridge.push({time[i][0] + time[i][2], i});
        }
        if (!right_bridge.empty())
        {
            const int i = right_bridge.top().second;
            right_bridge.pop();
            result += time[i][2];
            left_workers.push({result + time[i][3], i});
        }
        else if (!left_bridge.empty() && n > 0)
        {
            const int i = left_bridge.top().second;
            left_bridge.pop();
            result += time[i][0];
            right_workers.push({result + time[i][1], i});
            --n;
        }
        else
        {
            result = std::min(
                (!left_workers.empty() && (n > 0))?
                    left_workers.top().first:
                    std::numeric_limits<int>::max(),
                (!right_workers.empty())?
                    right_workers.top().first:
                    std::numeric_limits<int>::max());
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          int k,
          std::vector<std::vector<int> > time,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findCrossingTime(n, k, time);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 3, {{1, 1, 2, 1}, {1, 1, 3, 1}, {1, 1, 4, 1}}, 6, trials);
    test(3, 2, {{1, 9, 1, 8}, {10, 10, 10, 10}}, 50, trials);
    return 0;
}


