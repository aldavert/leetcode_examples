#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sumOfMultiples(int n)
{
    int result = 0;
    for (int i = 1; i <= n; ++i)
        if ((i % 3 == 0) || (i % 5 == 0) || (i % 7 == 0)) result += i;
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOfMultiples(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, 21, trials);
    test(10, 40, trials);
    test(9, 30, trials);
    return 0;
}


