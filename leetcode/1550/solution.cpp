#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool threeConsecutiveOdds(std::vector<int> arr)
{
    for (int number_of_odds = 0; int value : arr)
    {
        number_of_odds = (value & 1) * (number_of_odds + 1);
        if (number_of_odds == 3) return true;
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = threeConsecutiveOdds(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 6, 4, 1}, false, trials);
    test({1, 2, 34, 3, 4, 5, 7, 23, 12}, true, trials);
    return 0;
}


