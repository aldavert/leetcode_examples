#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumOperationsToWriteY(std::vector<std::vector<int> > grid)
{
    const int n = static_cast<int>(grid.size());
    const int mid = n / 2;
    auto operations = [&](int a, int b) -> int
    {
        int result = 0;
        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                if (((i < mid) && ((i == j) || (i + j == n - 1)))
                ||  ((i >= mid) && (j == mid)))
                    result += (grid[i][j] != a);
                else if (grid[i][j] != b)
                    ++result;
            }
        }
        return result;
    };
    return std::min({operations(0, 1),
                     operations(0, 2),
                     operations(1, 0),
                     operations(1, 2),
                     operations(2, 0),
                     operations(2, 1)});
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumOperationsToWriteY(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 2},
          {1, 1, 0},
          {0, 1, 0}}, 3, trials);
    test({{0, 1, 0, 1, 0},
          {2, 1, 0, 1, 2},
          {2, 2, 2, 0, 1},
          {2, 2, 2, 2, 2},
          {2, 1, 2, 2, 2}}, 12, trials);
    return 0;
}


