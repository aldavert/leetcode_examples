#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findPaths(int m, int n, int maxMove, int startRow, int startColumn)
{
    const int direction[] = {-1, 0, 1, 0, -1};
    constexpr int MOD = 1'000'000'007;
    unsigned int dp[2][51][51] = {};
    bool previous = true;
    for (int move = 1; move <= maxMove; ++move, previous = !previous)
    {
        for (int y = 0; y < m; ++y)
        {
            for (int x = 0; x < n; ++x)
            {
                dp[previous][y][x] = 0;
                for (int k = 0; k < 4; ++k)
                {
                    int dy = y + direction[k];
                    int dx = x + direction[k + 1];
                    if ((dy < 0) || (dy >= m) || (dx < 0) || (dx >= n))
                        ++dp[previous][y][x];
                    else dp[previous][y][x] += dp[!previous][dy][dx] % MOD;
                }
            }
        }
    }
    return dp[!previous][startRow][startColumn] % MOD;
}

// ############################################################################
// ############################################################################

void test(int m,
          int n,
          int maxMove,
          int startRow,
          int startColumn,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findPaths(m, n, maxMove, startRow, startColumn);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 2, 2, 0, 0, 6, trials);
    test(1, 3, 3, 0, 1, 12, trials);
    return 0;
}


