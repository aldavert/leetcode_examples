#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int edgeScore(std::vector<int> edges)
{
    std::vector<long> scores(edges.size());
    for (int i = 0, n = static_cast<int>(edges.size()); i < n; ++i)
        scores[edges[i]] += i;
    const auto max = std::max_element(scores.begin(), scores.end());
    return static_cast<int>(max - scores.begin());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> edges, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = edgeScore(edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 0, 0, 0, 7, 7, 5}, 7, trials);
    test({2, 0, 0, 2}, 0, trials);
    return 0;
}


