#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string destCity(std::vector<std::vector<std::string> > paths)
{
    std::unordered_set<std::string> cities_src, cities_dst;
    for (const std::vector<std::string> &path : paths)
    {
        cities_src.insert(path[0]);
        cities_dst.insert(path[1]);
    }
    for (std::string source : cities_src)
        if (auto search = cities_dst.find(source); search != cities_dst.end())
            cities_dst.erase(search);
    return *cities_dst.begin();
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<std::string> > paths,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = destCity(paths);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{"London", "New York"}, {"New York", "Lima"}, {"Lima", "Sao Paulo"}}, "Sao Paulo", trials);
    test({{"B", "C"}, {"D", "B"}, {"C", "A"}}, "A", trials);
    test({{"A", "Z"}}, "Z", trials);
    return 0;
}


