#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<int> resultsArray(std::vector<std::vector<int> > queries, int k)
{
    std::priority_queue<int> heap;
    std::vector<int> result;
    for (const auto &query : queries)
    {
        int x = query[0], y = query[1];
        heap.push(std::abs(x) + std::abs(y));
        if (static_cast<int>(heap.size()) > k)
            heap.pop();
        result.push_back((static_cast<int>(heap.size()) == k)?heap.top():-1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > queries,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = resultsArray(queries, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {3, 4}, {2, 3}, {-3, 0}}, 2, {-1, 7, 5, 3}, trials);
    test({{5, 5}, {4, 4}, {3, 3}}, 1, {10, 8, 6}, trials);
    return 0;
}


