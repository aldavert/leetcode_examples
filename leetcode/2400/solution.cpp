#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfWays(int startPos, int endPos, int k)
{
    constexpr int MOD = 1'000'000'007;
    std::vector<std::vector<int> > f(k + 1, std::vector<int>(k + 1, -1));
    auto dfs = [&](auto &&self, int i, int j) -> int
    {
        if ((i > j) || (j < 0)) return 0;
        if (j == 0) return (i == 0);
        if (f[i][j] != -1) return f[i][j];
        f[i][j] = (self(self, i + 1, j - 1) + self(self, std::abs(i - 1), j - 1)) % MOD;
        return f[i][j];
    };
    return dfs(dfs, std::abs(startPos - endPos), k);
}

// ############################################################################
// ############################################################################

void test(int startPos, int endPos, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfWays(startPos, endPos, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 2, 3, 3, trials);
    test(2, 5, 10, 0, trials);
    return 0;
}


