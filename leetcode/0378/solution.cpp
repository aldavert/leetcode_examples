#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int kthSmallest(std::vector<std::vector<int> > matrix, int k)
{
    const int n = static_cast<int>(matrix.size());
    int result = std::numeric_limits<int>::lowest();
    auto lessEqual = [&](int x)
    {
        int count = 0, col = n - 1;
        for (int row = 0; row < n; ++row)
        {
            while ((col >= 0) && (matrix[row][col] > x)) --col;
            count += col + 1;
        }
        return count;
    };
    for (int left = matrix[0][0], right = matrix[n - 1][n - 1]; left <= right; )
    {
        int mid = (left + right) / 2;
        if (lessEqual(mid) >= k)
        {
            result = mid;
            right = mid - 1;
        }
        else left = mid + 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = kthSmallest(matrix, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 5, 9}, {10, 11, 13}, {12, 13, 15}}, 8, 13, trials);
    test({{-5}}, 1, -5, trials);
    return 0;
}


