#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int monkeyMove(int n)
{
    static constexpr int MOD = 1'000'000'007;
    auto modPow = [&](auto &&self, long x, long m) -> long
    {
        if (m == 0) return 1;
        if (m % 2 == 1) return x * self(self, x % MOD, (m - 1)) % MOD;
        return self(self, x * x % MOD, (m / 2)) % MOD;
    };
    const int res = static_cast<int>(modPow(modPow, 2, n)) - 2;
    return (res < 0)?res + MOD:res;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = monkeyMove(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 6, trials);
    test(4, 14, trials);
    return 0;
}


