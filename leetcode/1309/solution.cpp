#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string freqAlphabets(std::string s)
{
    char buffer[2] = {};
    int size = 0;
    std::string result;
    for (char c : s)
    {
        if (c == '#')
        {
            result += static_cast<char>('a' + (buffer[0] * 10 + buffer[1] - 1));
            size = 0;
        }
        else
        {
            if (size == 2)
            {
                result += 'a' + buffer[0] - 1;
                buffer[0] = buffer[1];
                buffer[1] = c - '0';
            }
            else buffer[size++] = c - '0';
        }
    }
    for (int i = 0; i < size; ++i)
        result += 'a' + buffer[i] - 1;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = freqAlphabets(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("10#11#12", "jkab", trials);
    test("1326#", "acz", trials);
    return 0;
}


