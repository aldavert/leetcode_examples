# Remove Nodes from Linked List

You are given the `head` of a linked list.

Remove every node which has a node with a greater value anywhere to the right side of it.

Return *the* `head` *of the modified linked list*.

#### Example 1:
> ```mermaid
> flowchart LR;
> subgraph S1 [ ]
> direction LR;
> A((5))-->B((2))-->C((13))-->D((3))-->E((8))
> end
> subgraph S2 [ ]
> direction LR;
> F((13))-->G((8))
> end
> S1-->S2
> classDef remove fill:#FAA,stroke:#000,stroke-width:2px;
> class A,B,D remove;
> classDef keep fill:#FFF,stroke:#000,stroke-width:2px;
> class C,E,F,G keep;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class S1,S2 empty;
> ```
> *Input:* `head = [5, 2, 13, 3, 8]`  
> *Output:* `[13, 8]`  
> *Explanation:* The nodes that should be removed are `5`, `2` and `3`.
> - `Node 13` is to the right of `node 5`.
> - `Node 13` is to the right of `node 2`.
> - `Node 8` is to the right of `node 3`.

#### Example 2:
> *Input:* `head = [1, 1, 1, 1]`  
> *Output:* `[1, 1, 1, 1]`  
> *Explanation:* Every node has value `1`, so no nodes are removed.

#### Constraints:
- The number of the nodes in the given list is in the range `[1, 10^5]`.
- `1 <= Node.val <= 10^5`

