#include "../common/common.hpp"
#include "../common/list.hpp"
#include <stack>

// ############################################################################
// ############################################################################

#if 0
ListNode * removeNodes(ListNode * head)
{
    if (head == nullptr) return nullptr;
    if (head->next == nullptr) return new ListNode(head->val);
    ListNode * result = removeNodes(head->next);
    if (head->val >= result->val)
        result = new ListNode(head->val, result);
    return result;
}
#else
ListNode * removeNodes(ListNode * head)
{
    std::stack<ListNode *> nodes;
    for (ListNode * current = head; current != nullptr; current = current->next)
        nodes.push(current);
    ListNode * result = nullptr;
    for (int max_value = 0; !nodes.empty(); nodes.pop())
    {
        if (nodes.top()->val >= max_value)
        {
            max_value = nodes.top()->val;
            result = new ListNode(nodes.top()->val, result);
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> list, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(list);
        ListNode * nw_head = removeNodes(head);
        result = list2vec(nw_head);
        delete head;
        delete nw_head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 2, 13, 3, 8}, {13, 8}, trials);
    test({1, 1, 1, 1}, {1, 1, 1, 1}, trials);
    return 0;
}


