#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool checkOnesSegment(std::string s)
{
    bool zeros = false;
    for (char c : s)
    {
        if (zeros && (c == '1')) return false;
        zeros = zeros || (c == '0');
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = checkOnesSegment(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1001", false, trials);
    test("110", true, trials);
    return 0;
}


