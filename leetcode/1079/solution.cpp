#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numTilePossibilities(std::string tiles)
{
    auto dfs = [&tiles](void) -> int
    {
        int histogram[26] = {};
        for (size_t i = 0, n = tiles.size(); i < n; ++i)
            ++histogram[tiles[i] - 'A'];
        auto inner = [&](auto &&self) -> int
        {
            int possible_sequences = 0;
            for (size_t i = 0; i < 26; ++i)
            {
                if (!histogram[i]) continue;
                --histogram[i];
                possible_sequences += 1 + self(self);
                ++histogram[i];
            }
            return possible_sequences;
        };
        return inner(inner);
    };
    return dfs();
}

// ############################################################################
// ############################################################################

void test(std::string tiles, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numTilePossibilities(tiles);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("AAB", 8, trials);
    test("AAABBC", 188, trials);
    test("V", 1, trials);
    return 0;
}


