#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countPyramids(std::vector<std::vector<int> > grid)
{
    auto count = [](std::vector<std::vector<int> > dp, bool reverse) -> int
    {
        const int m = static_cast<int>(dp.size()),
                  n = static_cast<int>(dp[0].size());
        int result = 0, begin = (reverse)?(m - 2):1, direction = 1 - 2 * reverse;
        for (int k = 0, i = begin; k < m - 1; ++k, i += direction)
        {
            for (int j = 1; j + 1 < n; ++j)
            {
                if (dp[i][j] == 1)
                {
                    dp[i][j] = std::min({dp[i - direction][j - 1],
                                         dp[i - direction][j],
                                         dp[i - direction][j + 1]}) + 1;
                    result += dp[i][j] - 1;
                }
            }
        }
        return result;
    };
    return count(grid, true) + count(grid, false);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPyramids(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 1, 0},
          {1, 1, 1, 1}}, 2, trials);
    test({{1, 1, 1},
          {1, 1, 1}}, 2, trials);
    test({{1, 1, 1, 1, 0},
          {1, 1, 1, 1, 1},
          {1, 1, 1, 1, 1},
          {0, 1, 0, 0, 1}}, 13, trials);
    return 0;
}


