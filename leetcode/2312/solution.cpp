#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long sellingWood(int m, int n, std::vector<std::vector<int> > prices)
{
    long dp[201][201] = {};
    
    for (const auto &p : prices)
        dp[p[0]][p[1]] = p[2];
    for (int i = 1; i <= m; ++i)
    {
        for (int j = 1; j <= n; ++j)
        {
            for (int h = 1; h <= i / 2; ++h)
                dp[i][j] = std::max(dp[i][j], dp[h][j] + dp[i - h][j]);
            for (int w = 1; w <= j / 2; ++w)
                dp[i][j] = std::max(dp[i][j], dp[i][w] + dp[i][j - w]);
        }
    }
    return dp[m][n];
}

// ############################################################################
// ############################################################################

void test(int m,
          int n,
          std::vector<std::vector<int> > prices,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sellingWood(m, n, prices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 5, {{1, 4, 2}, {2, 2, 7}, {2, 1, 3}}, 19, trials);
    test(4, 6, {{3, 2, 10}, {1, 4, 2}, {4, 1, 3}}, 32, trials);
    return 0;
}


