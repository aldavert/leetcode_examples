#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> sortedSquares(std::vector<int> nums)
{
    struct Position { int index = 0; int value = 0; };
    const int n = static_cast<int>(nums.size());
    std::vector<int> result(n);
    Position left = {0, nums.front() * nums.front()},
             right = {n - 1, nums.back() * nums.back()};
    for (int p = n - 1; p > 0; --p)
    {
        const int sign = 2 * (left.value >= right.value) - 1;
        Position &current = (sign > 0)?left:right;
        current.index += sign;
        result[p] = std::exchange(current.value,
                                  nums[current.index] * nums[current.index]);
    }
    result[0] = left.value;
    return result;
}
#else
std::vector<int> sortedSquares(std::vector<int> nums)
{
    std::vector<int> result(nums.size());
    int left = 0, right = static_cast<int>(nums.size()) - 1;
    int lvalue = nums[0] * nums[0], rvalue = nums[right] * nums[right];
    for (int p = right; left <= right; --p)
    {
        if (lvalue >= rvalue)
        {
            result[p] = lvalue;
            ++left;
            lvalue = nums[left] * nums[left];
        }
        else
        {
            result[p] = rvalue;
            --right;
            rvalue = nums[right] * nums[right];
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sortedSquares(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-4, -1, 0, 3, 10}, {0, 1, 9, 16, 100}, trials);
    test({-7, -3, 2, 3, 11}, {4, 9, 9, 49, 121}, trials);
    test({1}, {1}, trials);
    return 0;
}


