#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool sumOfNumberAndReverse(int num)
{
    for (int i = num / 2; i <= num; ++i)
    {
        int value = 0;
        for (int n = i; n > 0; n /= 10) value = value * 10 + n % 10;
        if (num == i + value)
            return true;
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(int num, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOfNumberAndReverse(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(443, true, trials);
    test(63, false, trials);
    test(181, true, trials);
    return 0;
}


