#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxPointsInsideSquare(std::vector<std::vector<int> > points, std::string s)
{
    int second_min_size = std::numeric_limits<int>::max();
    std::array<int, 26> min_sizes;
    min_sizes.fill(std::numeric_limits<int>::max());
    for (int i = 0, n = static_cast<int>(points.size()); i < n; ++i)
    {
        const int x = points[i][0], y = points[i][1],
                  sz = std::max(abs(x), abs(y)),
                  j = s[i] - 'a';
        if (min_sizes[j] == std::numeric_limits<int>::max())
            min_sizes[j] = sz;
        else if (sz < min_sizes[j])
        {
            second_min_size = std::min(second_min_size, min_sizes[j]);
            min_sizes[j] = sz;
        }
        else second_min_size = std::min(second_min_size, sz);
    }
    return static_cast<int>(std::count_if(min_sizes.begin(), min_sizes.end(),
        [&](int sz) { return sz < second_min_size; }));
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points,
          std::string s,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxPointsInsideSquare(points, s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 2}, {-1, -2}, {-4, 4}, {-3, 1}, {3, -3}}, "abdca", 2, trials);
    test({{1, 1}, {-2, -2}, {-2, 2}}, "abb", 1, trials);
    test({{1, 1}, {-1, -1}, {2, -2}}, "ccd", 0, trials);
    return 0;
}


