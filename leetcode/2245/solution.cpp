#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxTrailingZeros(std::vector<std::vector<int> > grid)
{
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    std::vector<std::vector<int> > left_prefix2(m, std::vector<int>(n)),
        left_prefix5(m, std::vector<int>(n)),
        top_prefix2(m, std::vector<int>(n)),
        top_prefix5(m, std::vector<int>(n));
    auto count = [](int num, int factor) -> int
    {
        int c = 0;
        for (; num % factor == 0; num /= factor, ++c);
        return c;
    };
    
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            left_prefix2[i][j] = count(grid[i][j], 2);
            left_prefix5[i][j] = count(grid[i][j], 5);
            if (j > 0)
            {
                left_prefix2[i][j] += left_prefix2[i][j - 1];
                left_prefix5[i][j] += left_prefix5[i][j - 1];
            }
        }
    }
    for (int j = 0; j < n; ++j)
    {
        for (int i = 0; i < m; ++i)
        {
            top_prefix2[i][j] = count(grid[i][j], 2);
            top_prefix5[i][j] = count(grid[i][j], 5);
            if (i > 0)
            {
                top_prefix2[i][j] += top_prefix2[i - 1][j];
                top_prefix5[i][j] += top_prefix5[i - 1][j];
            }
        }
    }
    
    int result = 0;
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            int curr2 = count(grid[i][j], 2),
                curr5 = count(grid[i][j], 5),
                l2 = left_prefix2[i][j],
                l5 = left_prefix5[i][j],
                r2 = left_prefix2[i][n - 1] - (j ? left_prefix2[i][j - 1] : 0),
                r5 = left_prefix5[i][n - 1] - (j ? left_prefix5[i][j - 1] : 0),
                t2 = top_prefix2[i][j],
                t5 = top_prefix5[i][j],
                d2 = top_prefix2[m - 1][j] - (i ? top_prefix2[i - 1][j] : 0),
                d5 = top_prefix5[m - 1][j] - (i ? top_prefix5[i - 1][j] : 0);
            result = std::max({result, std::min(l2 + t2 - curr2, l5 + t5 - curr5),
                                       std::min(r2 + t2 - curr2, r5 + t5 - curr5),
                                       std::min(l2 + d2 - curr2, l5 + d5 - curr5),
                                       std::min(r2 + d2 - curr2, r5 + d5 - curr5)});
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxTrailingZeros(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{23, 17, 15,  3, 20},
          { 8,  1, 20, 27, 11},
          { 9,  4,  6,  2, 21},
          {40,  9,  1, 10,  6},
          {22,  7,  4,  5,  3}}, 3, trials);
    test({{4, 3, 2}, {7, 6, 1}, {8, 8, 8}}, 0, trials);
    return 0;
}


