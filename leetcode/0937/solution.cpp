#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> reorderLogFiles(std::vector<std::string> logs)
{
    struct LogLetters
    {
        std::string identifier;
        std::string letters;
        inline bool operator<(const LogLetters &other) const
        {
            return (letters == other.letters)?(identifier < other.identifier)
                                             :(letters < other.letters);
        }
    };
    std::vector<std::string> result, logs_digits;
    std::vector<LogLetters> logs_letters;
    
    for (const auto &log : logs)
    {
        size_t i = log.find_first_of(' ');
        if (std::isdigit(log[i + 1])) logs_digits.push_back(log);
        else logs_letters.emplace_back(log.substr(0, i), log.substr(i + 1));
    }
    std::sort(logs_letters.begin(), logs_letters.end());
    for (const auto &[identifier, letters] : logs_letters)
        result.push_back(identifier + ' ' + letters);
    for (const auto &digitLog : logs_digits)
        result.push_back(digitLog);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> logs,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = reorderLogFiles(logs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"dig1 8 1 5 1", "let1 art can", "dig2 3 6", "let2 own kit dig",
          "let3 art zero"}, {"let1 art can", "let3 art zero", "let2 own kit dig",
          "dig1 8 1 5 1", "dig2 3 6"}, trials);
    test({"a1 9 2 3 1", "g1 act car", "zo4 4 7", "ab1 off key dog", "a8 act zoo"},
         {"g1 act car", "a8 act zoo", "ab1 off key dog", "a1 9 2 3 1", "zo4 4 7"},
         trials);
    return 0;
}


