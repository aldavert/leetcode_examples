#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> productQueries(int n, std::vector<std::vector<int> > queries)
{
    constexpr int MOD = 1'000'000'007, MAXBIT = 30;
    std::vector<int> result, powers;
    
    for (int i = 0; i < MAXBIT; ++i)
        if (n >> i & 1)
            powers.push_back(1 << i);
    for (const auto &query : queries)
    {
        long prod = 1;
        for (int i = query[0]; i <= query[1]; ++i)
            prod = (prod * powers[i]) % MOD;
        result.push_back(static_cast<int>(prod));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = productQueries(n, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(15, {{0, 1}, {2, 2}, {0, 3}}, {2, 4, 64}, trials);
    test(2, {{0, 0}}, {2}, trials);
    return 0;
}


