#include "../common/common.hpp"
#include <unordered_map>
#include <bitset>

// ############################################################################
// ############################################################################

#if 1
int countPairs(std::vector<int> nums)
{
    int max_len = 0, result = 0;
    std::vector<std::string> str_nums;
    for (int number : nums)
    {
        str_nums.emplace_back(std::to_string(number));
        max_len = std::max(max_len, static_cast<int>(str_nums.back().size()));
    }
    std::unordered_map<std::string, std::bitset<5000> > table;
    for (int k = 0, n = static_cast<int>(str_nums.size()); k < n; k++)
    {
        int padding_size = max_len - static_cast<int>(str_nums[k].size());
        std::string s = std::string(padding_size, '0') + str_nums[k];
        auto mark = table[s];
        for (int i = 0; i < max_len; i++)
        {
            for (int j = 0; j < i; j++)
            {
                if (s[i] != s[j])
                {
                    std::swap(s[i], s[j]);
                    mark |= table[s];
                    std::swap(s[i], s[j]);
                }
            }
        }
        result += static_cast<int>(mark.count());
        table[s][k] = 1;
        for (int i = 0; i < max_len; i++)
        {
            for (int j = 0; j < i; j++)
            {
                std::swap(s[i], s[j]);
                table[s][k] = 1;
                std::swap(s[i], s[j]);
            }
        }
    }
    return result;
}
#else
int countPairs(std::vector<int> nums)
{
    auto calcSwaps = [](const std::string &digits) -> std::unordered_set<int>
    {
        const int n = static_cast<int>(digits.size());
        std::unordered_set<int> swaps{{std::stoi(digits)}};
        for (int i = 0; i < n; ++i)
        {
            for (int j = i + 1; j < n; ++j)
            {
                std::string new_digits = digits;
                std::swap(new_digits[i], new_digits[j]);
                swaps.insert(std::stoi(new_digits));
            }
        }
        for (int i1 = 0; i1 < n; ++i1)
        {
            for (int j1 = i1 + 1; j1 < n; ++j1)
            {
                for (int i2 = 0; i2 < n; ++i2)
                {
                    for (int j2 = i2 + 1; j2 < n; ++j2)
                    {
                        std::string new_digits = digits;
                        std::swap(new_digits[i1], new_digits[j1]);
                        std::swap(new_digits[i2], new_digits[j2]);
                        swaps.insert(std::stoi(new_digits));
                    }
                }
            }
        }
        return swaps;
    };
    int result = 0;
    std::unordered_map<int, int> count;
    int max_len = *std::max_element(nums.begin(), nums.end());
    max_len = static_cast<int>(std::to_string(max_len).size());
    
    for (const int num : nums)
    {
        int num_len = static_cast<int>(std::to_string(num).size());
        const std::string digits = std::string(max_len - num_len, '0')
                                 + std::to_string(num);
        for (const int swap : calcSwaps(digits))
            result += count[swap];
        ++count[num];
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPairs(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1023, 2310, 2130, 213}, 4, trials);
    test({1, 10, 100}, 3, trials);
    return 0;
}


