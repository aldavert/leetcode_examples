#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

int magicalString(int n)
{
    // "1221121221221121122" -> "2211211221221211221"
    std::bitset<100'010> magic_string{"2211211221221211221", 19, '1', '2'};
    if (n < 20)
    {
        int result = 0;
        for (int i = 0; i < n; ++i)
            result += magic_string[i] == 0;
        return result;
    }
    int result = 9;
    bool current = false;
    for (int start = 12, last = 19; last < n; current = !current, ++start)
    {
        result += (current == false);
        magic_string[last++] = current;
        if (magic_string[start])
        {
            result += (last < n) * (current == false);
            magic_string[last++] = current;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = magicalString(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(     6,      3, trials);
    test(     1,      1, trials);
    test(100000,  49972, trials);
    test( 99999,  49972, trials);
    test( 94828,  47385, trials);
    test(   348,    173, trials);
    test(    84,     41, trials);
    test(  8422,   4209, trials);
    test(    18,      9, trials);
    test(    19,      9, trials);
    test(    20,     10, trials);
    test(    21,     10, trials);
    test(    22,     11, trials);
    test(    23,     12, trials);
    test(    24,     12, trials);
    test(    25,     13, trials);
    test(    26,     13, trials);
    test(    27,     13, trials);
    test(    28,     14, trials);
    test(    29,     15, trials);
    return 0;
}


