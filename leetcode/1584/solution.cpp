#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minCostConnectPoints(std::vector<std::vector<int> > points)
{
    const int MAX = std::numeric_limits<int>::max();
    const int n = static_cast<int>(points.size());
    int result = 0, u = 0;
    std::vector<int> distances(n, MAX);
    std::vector<bool> not_visited(n, true);
    auto manhattan = [&](size_t i, size_t j)
    {
        return std::abs(points[i][0] - points[j][0])
             + std::abs(points[i][1] - points[j][1]);
    };
    for (int i = 1; i < n; ++i)
    {
        not_visited[u] = false;
        for (int v = 0; v < n; ++v)
            if (not_visited[v])
                distances[v] = std::min(distances[v], manhattan(u, v));
        int dist = MAX;
        for (int v = 0; v < n; ++v)
        {
            if (distances[v] < dist)
            {
                dist = distances[v];
                u = v;
            }
        }
        distances[u] = MAX;
        result += dist;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCostConnectPoints(points);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0}, {2, 2}, {3, 10}, {5, 2}, {7, 0}}, 20, trials);
    test({{3, 12}, {-2, 5}, {-4, 1}}, 18, trials);
    return 0;
}


