#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string largestTimeFromDigits(std::vector<int> arr)
{
    std::string result;
    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            for (int k = 0; k < 4; ++k)
            {
                if ((i == j) || (i == k) || (j == k))
                    continue;
                const std::string hours = std::to_string(arr[i])
                                        + std::to_string(arr[j]);
                const std::string minutes = std::to_string(arr[k])
                                          + std::to_string(arr[6 - i - j - k]);
                if ((hours < "24") && (minutes < "60"))
                    result = std::max(result, hours + ':' + minutes);
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestTimeFromDigits(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, "23:41", trials);
    test({5, 5, 5, 5}, "", trials);
    return 0;
}


