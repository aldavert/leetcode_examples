#include "../common/common.hpp"
#include <set>

// ############################################################################
// ############################################################################

class Trie
{
    std::set<std::string> m_data;
public:
    Trie()
    {
    }
    void insert(std::string word)
    {
        m_data.insert(word);
    }
    bool search(std::string word)
    {
        return m_data.find(word) != m_data.end();
    }
    bool startsWith(std::string prefix)
    {
        if (auto it = m_data.lower_bound(prefix); it != m_data.end())
        {
            const size_t n = prefix.size();
            if (it->size() < n) return false;
            if (it->size() == n) return *it == prefix;
            return it->substr(0, n) == prefix;
        }
        return m_data.lower_bound(prefix) != m_data.end();
    }
};

// ############################################################################
// ############################################################################

enum class OP { trie, insert, search, startWith };

void test(std::vector<OP> commands,
          std::vector<std::string> word,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    const int n = static_cast<int>(commands.size());
    std::vector<bool> result(n, false);
    for (unsigned int trial = 0; trial < trials; ++trial)
    {
        Trie * t = nullptr;
        for (int j = 0; j < n; ++j)
        {
            switch (commands[j])
            {
            case OP::trie:
                t = new Trie();
                break;
            case OP::insert:
                t->insert(word[j]);
                break;
            case OP::search:
                result[j] = t->search(word[j]);
                break;
            case OP::startWith:
                result[j] = t->startsWith(word[j]);
                break;
            default:
                std::cerr << "[ERROR] Unknown or unimplemented command.\n";
                return;
            };
        }
        delete t;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::trie, OP::insert, OP::search, OP::search, OP::startWith, OP::insert, OP::search},
         {""      , "apple"   , "apple"   , "app"     , "app"        , "app"     , "app"},
         {false   , false     , true      , false     , true         , false     , true}, trials);
    test({OP::trie, OP::insert, OP::insert, OP::insert, OP::startWith, OP::startWith, OP::startWith},
         {""      , "apple"   , "app"     , "ball"    , "appl"       , "apb"        , "bal"        },
         {false   , false     , false     , false     , true         , false        , true         }, trials);
    return 0;
}


