#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int maximumNumberOfStringPairs(std::vector<std::string> words)
{
    const int n = static_cast<int>(words.size());
    std::unordered_map<std::string, int> position;
    for (int i = 0; i < n; ++i)
        position[words[i]] = i;
    int result = 0;
    for (int i = 0; i < n; ++i)
    {
        std::string matching_pair = words[i];
        std::reverse(matching_pair.begin(), matching_pair.end());
        auto search = position.find(matching_pair);
        result += (search != position.end()) && (search->second < i);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumNumberOfStringPairs(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"cd", "ac", "dc", "ca", "zz"}, 2, trials);
    test({"ab", "ba", "cc"}, 1, trials);
    test({"aa", "ab"}, 0, trials);
    return 0;
}


