#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> processQueries(std::vector<int> queries, int m)
{
    std::vector<int> perm(m), result;
    for (int i = 0; i < m; ++i) perm[i] = i + 1;
    for(int q : queries)
    {
        int ix = static_cast<int>(std::find(perm.begin(), perm.end(), q) - perm.begin());
        int to_move = perm[ix];
        result.push_back(ix);
        perm.erase(perm.begin() + ix);
        perm.insert(perm.begin(), to_move);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> queries,
          int m,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = processQueries(queries, m);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1, 2, 1}, 5, {2, 1, 2, 1}, trials);
    test({4, 1, 2, 2}, 4, {3, 1, 2, 0}, trials);
    test({7, 5, 5, 8, 3}, 8, {6, 5, 0, 7, 5}, trials);
    return 0;
}


