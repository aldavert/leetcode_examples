#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestPalindrome(std::string word1, std::string word2)
{
    std::string s = word1 + word2;
    const int n = static_cast<int>(s.size()),
              nw1 = static_cast<int>(word1.size());
    int result = 0;
    std::vector<std::vector<int> > dp(n, std::vector<int>(n));
    for (int i = 0; i < n; ++i) dp[i][i] = 1;
    
    for (int d = 1; d < n; ++d)
        for (int i = 0; i + d < n; ++i)
            if (const int j = i + d; s[i] == s[j])
            {
                dp[i][j] = 2 + dp[i + 1][j - 1];
                if ((i < nw1) && (j >= nw1))
                    result = std::max(result, dp[i][j]);
            }
            else dp[i][j] = std::max(dp[i + 1][j], dp[i][j - 1]);
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word1, std::string word2, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestPalindrome(word1, word2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("cacb", "cbba", 5, trials);
    test("ab", "ab", 3, trials);
    test("aa", "bb", 0, trials);
    return 0;
}


