#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

int rangeSumBST(TreeNode * root, int low, int high)
{
    int result = 0;
    auto compute = [&](auto &&self, TreeNode * node) -> void
    {
        if (node)
        {
            bool left;
            if ((left = (low <= node->val)))
                self(self, node->left);
            if (node->val <= high)
            {
                self(self, node->right);
                if (left) result += node->val;
            }
        }
    };
    compute(compute, root);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          int low,
          int high,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = rangeSumBST(root, low, high);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 5, 15, 3, 7, null, 18}, 7, 15, 32, trials);
    test({10, 5, 15, 3, 7, 13, 18, 1, null, 6}, 6, 10, 23, trials);
    return 0;
}


