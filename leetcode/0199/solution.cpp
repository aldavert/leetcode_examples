#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

std::vector<int> rightSideView(TreeNode * root)
{
    std::vector<int> result;
    auto traverse = [&](auto &&self, TreeNode * node, size_t level) -> void
    {
        if (!node) return;
        if (level >= result.size())
            result.push_back(node->val);
        self(self, node->right, level + 1);
        self(self, node->left, level + 1);
    };
    traverse(traverse, root, 0);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = rightSideView(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, null, 5, null, 4}, {1, 3, 4}, trials);
    test({1, null, 3}, {1, 3}, trials);
    test({}, {}, trials);
    return 0;
}
