# Binary Tree Right Side View

Given the `root` of a binary tree, imagine yourself standing on the **right side** of it, return *the values of the nodes you can see ordered from top to bottom*.

#### Example 1:
> ```mermaid
> graph TD;
> A((1))---B((2))
> A---C((3))
> B---EMPTY1(( ))
> B---D((5))
> C---EMPTY2(( ))
> C---E((4))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> classDef selected fill:#FDA,stroke:#000,stroke-width:2px;
> class EMPTY1,EMPTY2 empty;
> linkStyle 2,4 stroke-width:0px;
> class A,C,E selected;
> ```
> *Input:* `root = [1, 2, 3, null, 5, null, 4]`  
> *Output:* `[1, 3, 4]`

#### Example 2:
> *Input:* `root = [1, null, 3]`  
> *Output:* `[1, 3]`

#### Example 3:
> *Input:* `root = []`  
> *Output:* `[]`

#### Constraints:
- The number of nodes in the tree is in the range `[0, 100]`.
- `-100 <= Node.val <= 100`



