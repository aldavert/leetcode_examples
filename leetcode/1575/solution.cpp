#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

int countRoutes(std::vector<int>& locations, int start, int finish, int fuel)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(locations.size());
    int memo[101][201];
    std::memset(memo, -1, sizeof(memo));
    auto dfs = [&](auto &&self, int position, int current_fuel) -> int
    {
        if      (current_fuel < 0) return 0;
        else if (memo[position][current_fuel] != -1) return memo[position][current_fuel];
        int ways = (position == finish);
        
        if (current_fuel > 0)
        {
            for (int next = 0; next < n; ++next)
            {
                if (next == position) continue;
                int next_fuel = current_fuel
                              - std::abs(locations[position] - locations[next]);
                ways = (ways + self(self, next, next_fuel)) % MOD;
            }
        }
        return memo[position][current_fuel] = ways;
    };
    return dfs(dfs, start, fuel);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> locations,
          int start,
          int finish,
          int fuel,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countRoutes(locations, start, finish, fuel);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 6, 8, 4}, 1, 3, 5, 4, trials);
    test({4, 3, 1}, 1, 0, 6, 5, trials);
    test({5, 2, 1}, 0, 2, 3, 0, trials);
    return 0;
}


