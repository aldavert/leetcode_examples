#include "../common/common.hpp"
#include <deque>

// ############################################################################
// ############################################################################

int shortestSubarray(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    int result = n + 1;
    std::deque<int> window;
    std::vector<long> prefix(n + 1);
    
    for (int i = 0; i < n; ++i) prefix[i + 1] = prefix[i] + nums[i];
    for (int i = 0; i < n + 1; ++i)
    {
        while (!window.empty() && (prefix[i] - prefix[window.front()] >= k))
            result = std::min(result, i - window.front()),
            window.pop_front();
        while (!window.empty() && (prefix[i] <= prefix[window.back()]))
            window.pop_back();
        window.push_back(i);
    }
    
    return (result <= n)?result:-1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestSubarray(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1}, 1, 1, trials);
    test({1, 2}, 4, -1, trials);
    test({2, -1, 2}, 3, 3, trials);
    std::vector<int> long_sequence = {
        1239, 659, 701, 1629, -1351, -2095, -2168, -465, -812, 398, -2386,
        -830, 1369, -2486, 1056, -556, -1574, -225, -2230, -2158, 563, -782,
        792, 838, 2301, -1137, 959, 2347, -228, 2248, 570, 456, 618, -1417,
        1414, -2343, -1324, -1989, 56, 729, -918, 2244, -1414, 2228, 480,
        -1819, -1725, -713, 2400, 2477, -832, 1496, -1497, 39, 266, 919, 443,
        -1964, 1061, -729, -2341, 1565, 1886, 1549, 2101, 2087, 2380, 902,
        -1756, 2275, -1287, -86, 180, 1623, 995, -532, 1717, 1731, 1803, 588,
        431, -1924, -1623, 2395, -215, 1068, 1631, 1524, 1865, 239, 1573,
        -2171, 961, 1452, -2109, -1799, 1013, 2409, 238, -203, 997, 552, -273,
        -1373, 570, 822, -1653, 288, -310, 850, 856, -1520, -1133, 1367, 484,
        1820, 1214, 640, -134, -1074, -1362, 2223, -2296, -488, -59, 136, 619,
        2200, -584, 74, -1761, 1697, 1264, -1798, 421, 95, 1737, -919, -1203,
        -2349, 2350, -885, 380, 27, -409, -753, 2396, -2236, 1927, -1004,
        -1425, -34, -1016, -159, -2280, -1677, -408, -1456, -653, -2280, -360,
        -1914, 1930, 582, 944, 399, 1763, 2103, 261, -539, 1681, 1421, 264,
        1233, 2033, 2131, 2429, 336, -1721, -1895, -1783, 1737, -1172, 111,
        810, -335, 74, -390, -983, 78, -1091, 1707, -1176, 2303, 49, 570, 6,
        1257, 843, 1567};
    test(long_sequence, 5000, 3, trials);
    test(long_sequence, 15000, 12, trials);
    test(long_sequence, 20000, 20, trials);
    test(long_sequence, 50000, -1, trials);
    return 0;
}


