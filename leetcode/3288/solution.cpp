#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxPathLength(std::vector<std::vector<int> > coordinates, int k)
{
    struct Coordinate
    {
        int x = -1;
        int y = -1;
        bool operator<(const Coordinate &other) const
                    { return (x < other.x) || ((x == other.x) && (y > other.y)); }
    };
    auto lengthOfLIS = [](std::vector<Coordinate> &coords) -> int
    {
        std::sort(coords.begin(), coords.end());
        std::vector<int> tail;
        for (const auto& [_, y] : coords)
        {
            if (tail.empty() || (y > tail.back())) tail.push_back(y);
            else tail[std::lower_bound(tail.begin(), tail.end(), y) - tail.begin()] = y;
        }
        return static_cast<int>(tail.size());
    };
    const int xk = coordinates[k][0], yk = coordinates[k][1];
    std::vector<Coordinate> left_coordinates, right_coordinates;
    for (const auto &coordinate : coordinates)
    {
        const int x = coordinate[0], y = coordinate[1];
        if      ((x < xk) && (y < yk)) left_coordinates.emplace_back(x, y);
        else if ((x > xk) && (y > yk)) right_coordinates.emplace_back(x, y);
    }
    return 1 + lengthOfLIS(left_coordinates) + lengthOfLIS(right_coordinates);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > coordinates,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxPathLength(coordinates, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 1}, {2, 2}, {4, 1}, {0, 0}, {5, 3}}, 1, 3, trials);
    test({{2, 1}, {7, 0}, {5, 6}}, 2, 2, trials);
    return 0;
}


