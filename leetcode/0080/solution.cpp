#include "../common/common.hpp"
#include <sstream>

// ############################################################################
// ############################################################################

int removeDuplicates(std::vector<int> &nums)
{
    const int n = static_cast<int>(nums.size());
    int length = 1;
    for (int i = 1, previous = nums[0], count = 1; i < n; ++i)
    {
        if (nums[i] == previous) ++count;
        else
        {
            count = 1;
            previous = nums[i];
        }
        if (count <= 2)
            nums[length++] = nums[i];
    }
    return length;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    auto check = [&](const std::vector<int> &vec) -> bool
    {
        if ((static_cast<int>(vec.size()) < k)
        ||  (static_cast<int>(solution.size()) < k))
            return false;
        for (int i = 0; i < k; ++i)
            if (vec[i] != solution[i])
                return false;
        return true;
    };
    std::vector<int> result;
    int length = 0;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = nums;
        length = removeDuplicates(result);
    }
    showResult((length == k) && check(result), solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 1, 2, 2, 3}, 5, {1, 1, 2, 2, 3}, trials);
    test({0, 0, 1, 1, 1, 1, 2, 3, 3}, 7, {0, 0, 1, 1, 2, 3, 3}, trials);
    return 0;
}


