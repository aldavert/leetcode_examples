#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool asteroidsDestroyed(int mass, std::vector<int> asteroids)
{
    int frequency[100'001] = {};
    long long x_max = 0, planet = mass;
    for (int x : asteroids)
    {
        ++frequency[x];
        x_max = std::max<long long>(x_max, x);
    }
    for (long long x = 1; x <= x_max; ++x)
    {
        if (!frequency[x]) continue;
        if (x > planet) return false;
        planet += x * frequency[x];
    }
    return true;
}
#else
bool asteroidsDestroyed(int mass, std::vector<int> asteroids)
{
    std::sort(asteroids.begin(), asteroids.end());
    for (long m = mass; int asteroid : asteroids)
    {
        if (m >= asteroid) m += asteroid;
        else return false;
    }
    return true;
}
#endif

// ############################################################################
// ############################################################################

void test(int mass, std::vector<int> asteroids, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = asteroidsDestroyed(mass, asteroids);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, {3, 9, 19, 5, 21}, true, trials);
    test(5, {4, 9, 23, 4}, false, trials);
    return 0;
}


