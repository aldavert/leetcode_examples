#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int poorPigs(int buckets, int minutesToDie, int minutesToTest)
{
    return static_cast<int>(std::ceil(std::log(static_cast<double>(buckets))
         / std::log(static_cast<double>(minutesToTest)
         / static_cast<double>(minutesToDie) + 1) - 1e-7));
}

// ############################################################################
// ############################################################################

void test(int buckets,
          int minutesToDie,
          int minutesToTest,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = poorPigs(buckets, minutesToDie, minutesToTest);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1000, 15, 60, 5, trials);
    test(4, 15, 15, 2, trials);
    test(4, 15, 30, 2, trials);
    test(125, 1, 4, 3, trials);
    return 0;
}


