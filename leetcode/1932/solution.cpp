#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

TreeNode * canMerge(std::vector<TreeNode *> &trees)
{
    std::vector<std::tuple<TreeNode *, TreeNode *, TreeNode *> > restore;
    std::unordered_map<int, TreeNode *> val_to_node;
    std::unordered_map<int, int> count;
    auto isValidBST =
        [&](auto &&self, TreeNode * node, TreeNode * min, TreeNode * max) -> bool
    {
        if (node == nullptr) return true;
        if (min && (node->val <= min->val)) return false;
        if (max && (node->val >= max->val)) return false;
        if (auto search = val_to_node.find(node->val);
            !node->left && !node->right && (search != val_to_node.end()))
        {
            restore.push_back({node, node->left, node->right});
            node->left = search->second->left;
            node->right = search->second->right;
            val_to_node.erase(search);
        }

        return self(self, node->left, min, node)
            && self(self, node->right, node, max);
    };
    
    for (TreeNode * node : trees)
    {
        val_to_node[node->val] = node;
        ++count[node->val];
        if (node->left) ++count[node->left->val];
        if (node->right) ++count[node->right->val];
    }
    
    TreeNode * head = nullptr;
    for (TreeNode * node : trees)
    {
        if (count[node->val] == 1)
        {
            if (isValidBST(isValidBST, node, nullptr, nullptr)
            &&  (val_to_node.size() <= 1))
                head = node;
            break;
        }
    }
    // Avoid memory leaks.
    auto copy = [&](auto &&self, TreeNode * node) -> TreeNode *
    {
        if (!node) return nullptr;
        return new TreeNode(node->val,
                            self(self, node->left),
                            self(self, node->right));
    };
    TreeNode * result = copy(copy, head);
    for (auto [node, left, right] : restore)
    {
        node->left = left;
        node->right = right;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > flat_trees,
          std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        std::vector<TreeNode *> trees;
        for (const auto &ft : flat_trees)
            trees.push_back(vec2tree(ft));
        TreeNode * root = canMerge(trees);
        result = tree2vec(root);
        for (auto &node : trees) delete node;
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 1}, {3, 2, 5}, {5, 4}}, {3, 2, 5, 1, null, 4}, trials);
    test({{5, 3, 8}, {3, 2, 6}}, {}, trials);
    test({{5, 4}, {3}}, {}, trials);
    return 0;
}


