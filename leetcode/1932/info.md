# Merge BSTs to Create Single BST

You are given `n` **BST (binary search tree) root nodes** for `n` separate BSTs stored in an array `trees` **(0-indexed)**. Each BST in `trees` has **at most 3 nodes, ** and no two roots have the same value. In one operation, you can:

- Select two distinct indices `i` and `j` such that the value stored at one of the **leaves** of `trees[i]` is equal to the **root value** of `trees[j]`.
- Replace the leaf node in `trees[i]` with `trees[j]`.
- Remove `trees[j]` from `trees`.

Return *the* ***root*** *of the resulting BST if it is possible to form a valid BST after performing* `n - 1` *operations, or* `null` *if it is impossible to create a valid BST.*

A BST (binary search tree) is a binary tree where each node satisfies the following property:

- Every node in the node's left subtree has a value **strictly less** than the node's value.
- Every node in the node's right subtree has a value **strictly greater** than the node's value.

A leaf is a node that has no children.

#### Example 1:
> ```mermaid
> graph TD;
> subgraph S1 ["trees[2]"]
> F((5))---G((4))
> F---TMP2(( ))
> end
> subgraph S2 ["trees[1]"]
> C((3))---D((2))
> C---E((5))
> end
> subgraph S3 ["trees[0]"]
> A((2))---B((1))
> A---TMP1(( ))
> end
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef delete fill:#FFF,stroke-width:0px
> class TMP1,TMP2,S1,S2,S3 delete;
> linkStyle 1 stroke-width:0px
> linkStyle 5 stroke-width:0px
> ```
> *Input:* `trees = [[2, 1], [3, 2, 5], [5, 4]]`  
> *Output:* `[3, 2, 5, 1, null, 4]`  
> *Explanation:* In the first operation, pick `i = 1` and `j = 0`, and merge `trees[0]` into `trees[1]`. Delete `trees[0]`, so `trees = [[3, 2, 5, 1], [5, 4]]`.
> ```mermaid
> graph TD;
> A((3))---B((2))
> B---C((1))
> B---TMP1(( ))
> A---D((5))
> E((5))---F((4))
> E---TMP2(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef delete fill:#FFF,stroke-width:0px
> class TMP1,TMP2 delete;
> linkStyle 2 stroke-width:0px
> linkStyle 5 stroke-width:0px
> classDef selected fill:#88F,stroke-width:2px
> class B selected;
> ```
> In the second operation, pick `i = 0` and `j = 1`, and merge `trees[1]` into `trees[0]`. Delete `trees[1]`, so `trees = [[3, 2, 5, 1, null, 4]]`.
> ```mermaid
> graph TD;
> A((3))---B((2))
> B---C((1))
> B---TMP1(( ))
> A---D((5))
> D---F((4))
> D---TMP2(( ))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef delete fill:#FFF,stroke-width:0px
> class TMP1,TMP2 delete;
> linkStyle 2 stroke-width:0px
> linkStyle 5 stroke-width:0px
> classDef selected fill:#88F,stroke-width:2px
> class D selected;
> ```
> The resulting tree, shown above, is a valid BST, so return its root.

#### Example 2:
> ```mermaid
> graph TD;
> subgraph S1 ["trees[1]"]
> D((3))---E((2))
> D---F((6))
> end
> subgraph S2 ["trees[0]"]
> A((5))---B((3))
> A---C((8))
> end
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef delete fill:#FFF,stroke-width:0px
> class S1,S2 delete;
> ```
> *Input:* `trees = [[5, 3, 8], [3, 2, 6]]`  
> *Output:* `[]`  
> *Explanation:* Pick `i = 0` and `j = 1` and merge `trees[1]` into `trees[0]`. Delete `trees[1]`, so `trees = [[5, 3, 8, 2, 6]]`.
> ```mermaid
> graph TD;
> A((5))---B((3))
> B---C((2))
> B---E((6))
> A---D((8))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef selected fill:#88F,stroke-width:2px
> class B selected;
> ```
> The resulting tree is shown above. This is the only valid operation that can be performed, but the resulting tree is not a valid BST, so return null.

#### Example 3:
> ```mermaid
> graph TD;
> subgraph S1 ["trees[1]"]
> D((3))
> end
> subgraph S2 ["trees[0]"]
> A((5))---B((4))
> A---TMP1(( ))
> end
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef delete fill:#FFF,stroke-width:0px
> class TMP1,S1,S2 delete;
> linkStyle 1 stroke-width:0px
> ```
> *Input:* `trees = [[5, 4], [3]]`  
> *Output:* `[]`  
> *Explanation:* It is impossible to perform any operations.

#### Constraints:
- `n == trees.length`
- `1 <= n <= 5 * 10^4`
- The number of nodes in each tree is in the range `[1, 3]`.
- Each node in the input may have children but no grandchildren.
- No two roots of `trees` have the same value.
- All the trees in the input are **valid BSTs.**
- `1 <= TreeNode.val <= 5 * 10^4`.


