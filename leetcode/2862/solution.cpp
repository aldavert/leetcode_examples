#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

long long maximumSum(std::vector<int> nums)
{
    auto divideSquares = [](int val) -> int
    {
        for (int num = 2; num * num <= val; ++num)
            while (val % (num * num) == 0)
                val /= num * num;
        return val;
    };
    std::unordered_map<int, long> odd_power_to_sum;
    long result = 0;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        result = std::max(result, odd_power_to_sum[divideSquares(i + 1)] += nums[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 7, 3, 5, 7, 2, 4, 9}, 16, trials);
    test({8, 10, 3, 8, 1, 13, 7, 9, 4}, 20, trials);
    return 0;
}


