#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int ladderLength(std::string beginWord,
                 std::string endWord,
                 std::vector<std::string> wordList)
{
    std::queue<std::vector<std::string> > q;
    std::unordered_set<std::string> dictionary(wordList.begin(), wordList.end());
    if (dictionary.find(endWord) == dictionary.end()) return 0;
    
    q.push({beginWord});
    while (!q.empty())
    {
        std::unordered_set<std::string> visited;
        
        const int current_level_length = static_cast<int>(q.size());
        for (int i = 0; i < current_level_length; ++i)
        {
            std::vector<std::string> curr = std::move(q.front());
            q.pop();
            
            std::vector<std::string> words;
            std::string current = curr.back();
            for (int j = 0, n = static_cast<int>(current.size()); j < n; ++j)
            {
                char previous_character = current[j];
                for (char c = 'a'; c <= 'z'; ++c)
                {
                    current[j] = c;
                    if (dictionary.find(current) != dictionary.end())
                        words.push_back(current);
                }
                current[j] = previous_character;
            }
            
            for (auto s: words)
            {
                if (s == endWord)
                    return static_cast<int>(curr.size() + 1);
                std::vector<std::string> new_path(curr.begin(), curr.end());
                new_path.push_back(s);
                visited.insert(s);
                q.push(new_path);
            }
        }
        for (auto s: visited) dictionary.erase(s);
    }
    return 0;
}

// ############################################################################
// ############################################################################

void test(std::string beginWord,
          std::string endWord,
          std::vector<std::string> wordList,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = ladderLength(beginWord, endWord, wordList);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("hit", "cog", {"hot", "dot", "dog", "lot", "log", "cog"}, 5, trials);
    test("hit", "cog", {"hot", "dot", "dog", "lot", "log"}, 0, trials);
    return 0;
}


