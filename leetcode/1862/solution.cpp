#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sumOfFlooredPairs(std::vector<int> nums)
{
    constexpr int MOD = 1'000'000'007;
    const int k_max = *std::max_element(nums.begin(), nums.end());
    long result = 0;
    std::vector<int> count(k_max + 1);
    
    for (int num : nums) ++count[num];
    for (int i = 1; i <= k_max; ++i) count[i] += count[i - 1];
    
    for (int i = 1; i <= k_max; ++i)
    {
        if (count[i] > count[i - 1])
        {
            long sum = 0;
            for (int j = 1; i * j <= k_max; ++j)
            {
                int lo = i *  j      - 1,
                    hi = i * (j + 1) - 1;
                sum += (count[std::min(hi, k_max)] - count[lo]) * j;
            }
            result += sum * (count[i] - count[i - 1]);
            result %= MOD;
        }
    }
    
    return static_cast<int>(result);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOfFlooredPairs(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 5, 9}, 10, trials);
    test({7, 7, 7, 7, 7, 7, 7}, 49, trials);
    return 0;
}


