#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumRefill(std::vector<int> plants, int capacityA, int capacityB)
{
    int result = 0, i = 0, j = static_cast<int>(plants.size()) - 1,
        can_a = capacityA, can_b = capacityB;
    while (i < j)
    {
        result += (can_a < plants[i]) + (can_b < plants[j]);
        if (can_a < plants[i]) can_a = capacityA;
        if (can_b < plants[j]) can_b = capacityB;
        can_a -= plants[i++];
        can_b -= plants[j--];
    }
    return result + ((i == j) && (std::max(can_a, can_b) < plants[i]));
}

// ############################################################################
// ############################################################################

void test(std::vector<int> plants,
          int capacityA,
          int capacityB,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumRefill(plants, capacityA, capacityB);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 2, 3, 3}, 5, 5, 1, trials);
    test({2, 2, 3, 3}, 3, 4, 2, trials);
    test({5}, 10, 8, 0, trials);
    return 0;
}


