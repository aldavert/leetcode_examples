#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findDiagonalOrder(std::vector<std::vector<int> > mat)
{
    const size_t m = mat.size(),
                 n = mat[0].size(),
                 l = std::min(mat.size(), mat[0].size());
    std::vector<int> result;
    bool upward = true;
    for (size_t i = 0; i < m; ++i, upward = !upward)
    {
        if (upward)
            for (size_t k = 0, length = std::min(l, i + 1); k < length; ++k)
                result.push_back(mat[i - k][k]);
        else
            for (size_t k = 0, length = std::min(l, i + 1); k < length; ++k)
                result.push_back(mat[1 + i - length + k][length - 1 - k]);
    }
    for (size_t i = 1; i < n; ++i, upward = !upward)
    {
        if (upward)
            for (size_t k = 0, length = std::min(n - i, l); k < length; ++k)
                result.push_back(mat[m - 1 - k][i + k]);
        else
            for (size_t k = 0, length = std::min(n - i, l); k < length; ++k)
                result.push_back(mat[m - length + k][i + length - 1 - k]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > mat,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findDiagonalOrder(mat);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, {1, 2, 4, 7, 5, 3, 6, 8, 9}, trials);
    test({{1, 2}, {3, 4}}, {1, 2, 3, 4}, trials);
    test({{ 1,  2,  3,  4},
          { 5,  6,  7,  8},
          { 9, 10, 11, 12}},
          {1, 2, 5, 9, 6, 3, 4, 7, 10, 11, 8, 12}, trials);
    test({{ 1,  2,  3},
          { 4,  5,  6},
          { 7,  8,  9},
          {10, 11, 12}},
          {1, 2, 4, 7, 5, 3, 6, 8, 10, 11, 9, 12}, trials);
    return 0;
}


