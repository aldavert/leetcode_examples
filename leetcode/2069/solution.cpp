#include "../common/common.hpp"

constexpr int null = -1000;

// ############################################################################
// ############################################################################

class Robot
{
    bool m_is_origin = true;
    int m_i = 0;
    std::vector<std::pair<std::vector<int>, std::string> > m_pos;
public:
    Robot(int width, int height)
    {
        m_pos.push_back({{0, 0}, "South"});
        for (int i = 1; i < width ; ++i) m_pos.push_back({{i, 0}, "East"});
        for (int j = 1; j < height; ++j) m_pos.push_back({{width - 1, j}, "North"});
        for (int i = width - 2; i >= 0; --i) m_pos.push_back({{i, height - 1}, "West"});
        for (int j = height - 2; j > 0; --j) m_pos.push_back({{0, j}, "South"});
    }
    void step(int num)
    {
        m_is_origin = false;
        m_i = static_cast<int>((m_i + num) % m_pos.size());
    }
    std::vector<int> getPos(void)
    {
        return m_pos[m_i].first;
    }
    std::string getDir(void)
    {
        return m_is_origin?"East":m_pos[m_i].second;
    }
};

// ############################################################################
// ############################################################################

enum class OP { STEP, GETPOS, GETDIR };

void test(int width, int height,
          std::vector<OP> operations,
          std::vector<int> num,
          std::vector<std::variant<std::vector<int>, std::string> > solution,
          unsigned int trials = 1)
{
    std::vector<std::variant<std::vector<int>, std::string> > result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        Robot obj(width, height);
        result.clear();
        for (size_t i = 0; i < operations.size(); ++i)
        {
            switch (operations[i])
            {
            case OP::STEP:
                obj.step(num[i]);
                result.push_back({});
                break;
            case OP::GETPOS:
                result.push_back(obj.getPos());
                break;
            case OP::GETDIR:
                result.push_back(obj.getDir());
                break;
            default:
                std::cerr << "[ERROR] Unknown operation.\n";
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    auto V = [](auto ...parameters) -> std::vector<int>
    {
        return std::vector<int>{parameters...};
    };
    test(6, 3,
         {OP::STEP, OP::STEP, OP::GETPOS, OP::GETDIR, OP::STEP, OP::STEP,
          OP::STEP, OP::GETPOS, OP::GETDIR},
         {2, 2, null, null, 2, 1, 4, null, null},
         {V(), V(), V(4, 0), "East", V(), V(), V(), V(1, 2), "West"},
         trials);
    return 0;
}


