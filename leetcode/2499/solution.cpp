#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long minimumTotalCost(std::vector<int> nums1, std::vector<int> nums2)
{
    const int n = static_cast<int>(nums1.size());
    long result = 0;
    int max_frequency = 0, max_frequency_num = 0, should_be_swapped = 0;
    std::vector<int> conflicted_number_count(n + 1);
    
    for (int i = 0; i < n; ++i)
    {
        if (nums1[i] == nums2[i])
        {
            if (++conflicted_number_count[nums1[i]] > max_frequency)
            {
                max_frequency = conflicted_number_count[nums1[i]];
                max_frequency_num = nums1[i];
            }
            ++should_be_swapped;
            result += i;
        }
    }
    for (int i = 0; i < n; ++i)
    {
        if (max_frequency * 2 <= should_be_swapped) break;
        if (nums1[i] == nums2[i]) continue;
        if ((nums1[i] == max_frequency_num) || (nums2[i] == max_frequency_num))
            continue;
        ++should_be_swapped;
        result += i;
    }
    return (max_frequency * 2 > should_be_swapped)?-1:result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumTotalCost(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, {1, 2, 3, 4, 5}, 10, trials);
    test({2, 2, 2, 1, 3}, {1, 2, 2, 3, 3}, 10, trials);
    return 0;
}


