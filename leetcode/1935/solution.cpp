#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int canBeTypedWords(std::string text, std::string brokenLetters)
{
    bool invalid[128] = {};
    for (char letter : brokenLetters)
        invalid[static_cast<size_t>(letter)] = true;
    int result = 0;
    bool valid = true;
    for (char letter : text)
    {
        valid = valid && (!invalid[static_cast<size_t>(letter)]);
        if (letter == ' ')
        {
            result += valid;
            valid = true;
        }
    }
    return result + valid;
}

// ############################################################################
// ############################################################################

void test(std::string text,
          std::string brokenLetters,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = canBeTypedWords(text, brokenLetters);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("hello world", "ad", 1, trials);
    test("leet code", "lt", 1, trials);
    test("leet code", "e", 0, trials);
    return 0;
}


