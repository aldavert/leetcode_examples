#include "../common/common.hpp"
#include <bitset>
#include <numeric>

// ############################################################################
// ############################################################################

#if 1
int minimizeTheDifference(std::vector<std::vector<int> > mat, int target)
{
    const int m = static_cast<int>(mat.size());
    std::bitset<900> bs;
    bs[0] = true;
    int mn = 0;
    for (int i = 0; i < m; i++)
    {
        std::bitset<900> tmp;
        int cur_mn = std::numeric_limits<int>::max();
        for (int j : mat[i])
        {
            tmp = (tmp | (bs << j));
            cur_mn = std::min(cur_mn, j);
        }
        bs = tmp;
        mn += cur_mn;
    }
    int sz = static_cast<int>(bs.count());
    if (sz == 0)
        return mn - target;
    int result = std::numeric_limits<int>::max();
    for (int i = 0; i < 900; i++)
    {
        if (!bs[i]) continue;
        if (i >= target)
        {
            result = std::min(result, i - target);
            break;
        }
        result = target - i;
    }
    return result;
}
#else
int minimizeTheDifference(std::vector<std::vector<int> > mat, int target)
{
    const int min_sum = std::accumulate(mat.begin(), mat.end(), 0,
                      [](int subtotal, const std::vector<int> &row) {
                      return subtotal + *std::min_element(row.begin(), row.end()); });
    if (min_sum >= target)
        return min_sum - target;
    const int max_sum = std::accumulate(mat.begin(), mat.end(), 0,
                      [](int subtotal, const std::vector<int> &row) {
                      return subtotal + *std::max_element(row.begin(), row.end()); });
    std::vector<std::vector<int> > mem(mat.size(), std::vector<int>(max_sum + 1, -1));
    auto process = [&](auto &&self, size_t i, int sum)
    {
        if (i == mat.size())
            return std::abs(sum - target);
        if (mem[i][sum] != -1)
            return mem[i][sum];
        int res = std::numeric_limits<int>::max();
        for (int num : mat[i])
            res = std::min(res, self(self, i + 1, sum + num));
        return mem[i][sum] = res;
    };
    return process(process, 0, 0);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > mat,
          int target,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimizeTheDifference(mat, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, 13, 0, trials);
    test({{1}, {2}, {3}}, 100, 94, trials);
    test({{1, 2, 9, 8, 7}}, 6, 1, trials);
    return 0;
}


