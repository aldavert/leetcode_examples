#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumPrimeDifference(std::vector<int> nums)
{
    std::vector<bool> is_prime(101, true);
    is_prime[0] = is_prime[1] = false;
    for (int i = 2; i * i < 101; ++i)
        if (is_prime[i])
            for (int j = i * i; j < 101; j += i)
                is_prime[j] = false;
    int min_prime_index = -1, max_prime_index = -1;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        if (is_prime[nums[i]])
        {
            if (min_prime_index == -1)
                min_prime_index = i;
            max_prime_index = i;
        }
    }
    return max_prime_index - min_prime_index;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumPrimeDifference(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 9, 5, 3}, 3, trials);
    test({4, 8, 2, 8}, 0, trials);
    return 0;
}


