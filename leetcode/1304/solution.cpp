#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

std::vector<int> sumZero(int n)
{
    std::vector<int> result(n);
    for (int l = 0, r = n - 1, v = n / 2; l <= r; ++l, --r, --v)
    {
        result[l] = -v;
        result[r] =  v;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumZero(n);
    int sum = std::accumulate(result.begin(), result.end(), 0);
    if ((static_cast<int>(result.size()) == n) && (sum == 0))
        std::cout << showResult(true) << " sum(" << result << ") = " << sum << ".\n";
    else
    {
        std::cout << showResult(false) << " ";
        if (static_cast<int>(result.size()) != n)
            std::cout << "len(" << result << ") != " << n << ".\n";
        else std::cout << "sum(" << result << ") = " << sum << " != 0.\n";
    }
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, trials);
    test(3, trials);
    test(1, trials);
    test(4, trials);
    return 0;
}


