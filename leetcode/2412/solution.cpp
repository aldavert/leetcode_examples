#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long minimumMoney(std::vector<std::vector<int> > transactions)
{
    long long result = 0, losses = 0;
    for (const auto &transaction : transactions)
    {
        int cost = transaction[0], cashback = transaction[1];
        losses += std::max(0, cost - cashback);
    }
    for (const auto &transaction : transactions)
    {
        int cost = transaction[0], cashback = transaction[1];
        if (cost > cashback)
            result = std::max(result, losses + cashback);
        else result = std::max(result, losses + cost);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > transactions,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumMoney(transactions);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 1}, {5, 0}, {4, 2}}, 10, trials);
    test({{3, 0}, {0, 3}}, 3, trials);
    return 0;
}


