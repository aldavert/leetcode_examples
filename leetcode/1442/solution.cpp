#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int countTriplets(std::vector<int> arr)
{
    std::vector<int> work(arr.size() + 1);
    for (size_t i = 0; i < arr.size(); ++i)
        work[i + 1] = arr[i];
    int n = static_cast<int>(work.size()), result = 0;
    for (int i = 1; i < n; ++i)
        work[i] ^= work[i - 1];
    for (int i = 0; i < n; ++i)
        for (int j = i + 1; j < n; ++j)
            if (work[i] == work[j])
                result += j - i - 1;
    return result;
}
#else
int countTriplets(std::vector<int> arr)
{
    const int n = static_cast<int>(arr.size());
    int result = 0, prefix = 0;
    std::vector<int> xors = {0};
    for (int i = 0; i < n; ++i)
    {
        prefix ^= arr[i];
        xors.push_back(prefix);
    }
    for (int j = 1; j < n; ++j)
    {
        for (int i = 0; i < j; ++i)
        {
            int xors_i = xors[j] ^ xors[i];
            for (int k = j; k < n; ++k)
            {
                int xors_k = xors[k + 1] ^ xors[j];
                result += xors_k == xors_i;
            }
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countTriplets(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 1, 6, 7}, 4, trials);
    test({1, 1, 1, 1, 1}, 10, trials);
    return 0;
}


