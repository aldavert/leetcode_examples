#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool halvesAreAlike(std::string s)
{
    auto islower = [](char c)
    {
        c = static_cast<char>(std::tolower(c));
        return (c == 'a') || (c == 'e') || (c == 'i') || (c == 'o') || (c == 'u');
    };
    const size_t n_half = s.size() / 2, n = s.size();
    int frequency[2] = {};
    for (size_t i = 0; i < n; ++i)
        frequency[i >= n_half] += islower(s[i]);
    return frequency[0] == frequency[1];
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = halvesAreAlike(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("book", true, trials);
    test("textbook", false, trials);
    return 0;
}


