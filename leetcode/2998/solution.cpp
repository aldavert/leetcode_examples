#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minimumOperationsToMakeEqual(int x, int y)
{
    if (x <= y) return y - x;
    std::queue<int> q{{x}};
    std::unordered_set<int> seen;
    for (int result = 0; !q.empty(); ++result)
    {
        for (int sz = static_cast<int>(q.size()); sz > 0; --sz)
        {
            const int num = q.front();
            q.pop();
            if (num == y) return result;
            if (seen.find(num) != seen.end()) continue;
            seen.insert(num);
            if (num % 11 == 0) q.push(num / 11);
            if (num % 5 == 0) q.push(num / 5);
            q.push(num - 1);
            q.push(num + 1);
        }
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(int x, int y, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumOperationsToMakeEqual(x, y);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(26, 1, 3, trials);
    test(54, 2, 4, trials);
    test(25, 30, 5, trials);
    return 0;
}


