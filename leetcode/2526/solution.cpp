#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

class DataStream
{
    int m_value = -1;
    int m_k = -1;
    std::queue<int> m_q;
    int m_count = 0;
public:
    DataStream(int value, int k) : m_value(value), m_k(k), m_count(0) {}
    bool consec(int num)
    {
        if (static_cast<int>(m_q.size()) == m_k)
        {
            if (m_q.front() == m_value)
                --m_count;
            m_q.pop();
        }
        if (num == m_value)
            ++m_count;
        m_q.push(num);
        return m_count == m_k;
    }
};

// ############################################################################
// ############################################################################

void test(int value,
          int k,
          std::vector<int> input,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        DataStream obj(value, k);
        result.clear();
        for (size_t i = 0; i < input.size(); ++i)
            result.push_back(obj.consec(input[i]));
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 3, {4, 4, 4, 3}, {false, false, true, false}, trials);
    return 0;
}


