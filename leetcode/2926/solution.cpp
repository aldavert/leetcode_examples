#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maxBalancedSubsequenceSum(std::vector<int> nums)
{
    class Tree
    {
        std::vector<long> m_values;
        int m_n;
        inline int lowbit(int i) const { return i & -i; }
    public:
        Tree(int n) : m_values(n + 1), m_n(n + 1) {}
        void maximize(int i, long val)
        {
            while (i < m_n)
            {
                m_values[i] = std::max(m_values[i], val);
                i += lowbit(i);
            }
        }
        long get(int i) const
        {
            long r = 0;
            while (i > 0)
            {
                r = std::max(r, m_values[i]);
                i -= lowbit(i);
            }
            return r;
        }
    };
    long result = std::numeric_limits<long>::lowest();
    std::vector<std::pair<int, int> > pairs;
    Tree tree(static_cast<int>(nums.size()));
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        pairs.emplace_back(nums[i] - i, i);
    std::sort(pairs.begin(), pairs.end());
    for (const auto& [_, i] : pairs)
    {
        const long subseq_sum = tree.get(i) + nums[i];
        tree.maximize(i + 1, subseq_sum);
        result = std::max(result, subseq_sum);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxBalancedSubsequenceSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 3, 5, 6}, 14, trials);
    test({5, -1, -3, 8}, 13, trials);
    test({-2, -1}, -1, trials);
    return 0;
}


