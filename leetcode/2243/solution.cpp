#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string digitSum(std::string s, int k)
{
    for (int n = static_cast<int>(s.size()); n > k; n = static_cast<int>(s.size()))
    {
        std::string reduced;
        int sum = 0, j = 0;
        for (int i = 0; i < n; ++i, ++j)
        {
            if (j == k)
                reduced += std::to_string(sum),
                sum = j = 0;
            sum += s[i] - '0';
        }
        if (j) reduced += std::to_string(sum);
        s = reduced;
    }
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = digitSum(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("11111222223", 3, "135", trials);
    test("00000000", 3, "000", trials);
    return 0;
}


