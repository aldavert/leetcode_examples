#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumBeauty(std::vector<int> nums, int k)
{
    int result = 0;
    std::sort(nums.begin(), nums.end());
    for (int l = 0, r = 0, n = static_cast<int>(nums.size()); r < n; ++r)
    {
        while (nums[r] - nums[l] > 2 * k) ++l;
        result = std::max(result, r - l + 1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumBeauty(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 6, 1, 2}, 2, 3, trials);
    test({1, 1, 1, 1}, 10, 4, trials);
    return 0;
}


