#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfPairs(std::vector<std::vector<int> > points)
{
    auto cmp = [](const std::vector<int> &a, const std::vector<int> &b)
    {
        return (a[0] < b[0]) || ((a[0] == b[0]) && (a[1] > b[1]));
    };
    int result = 0;
    std::sort(points.begin(), points.end(), cmp);
    for (int i = 0, n = static_cast<int>(points.size()); i < n; ++i)
    {
        int max_y = std::numeric_limits<int>::lowest();
        for (int j = i + 1; j < n; ++j)
        {
            if ((points[i][1] >= points[j][1]) && (points[j][1] > max_y))
            {
                ++result;
                max_y = points[j][1];
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfPairs(points);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1}, {2, 2}, {3, 3}}, 0, trials);
    test({{6, 2}, {4, 4}, {2, 6}}, 2, trials);
    test({{3, 1}, {1, 3}, {1, 1}}, 2, trials);
    return 0;
}


