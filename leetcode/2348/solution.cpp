#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long zeroFilledSubarray(std::vector<int> nums)
{
    long long result = 0, count = 0;
    for (int number : nums)
    {
        if (number == 0) ++count;
        else result += count * (count + 1) / 2, count = 0;
    }
    return result + count * (count + 1) / 2;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = zeroFilledSubarray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 0, 0, 2, 0, 0, 4}, 6, trials);
    test({0, 0, 0, 2, 0, 0}, 9, trials);
    test({2, 10, 2019}, 0, trials);
    return 0;
}


