#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int getXORSum(std::vector<int> arr1, std::vector<int> arr2)
{
    int r1 = 0, r2 = 0;
    for (int i = 0, n = static_cast<int>(arr1.size()); i < n; ++i) r1 ^= arr1[i];
    for (int i = 0, n = static_cast<int>(arr2.size()); i < n; ++i) r2 ^= arr2[i];
    return r1 & r2;
}
#else
int getXORSum(std::vector<int> arr1, std::vector<int> arr2)
{
    return std::accumulate(arr1.begin(), arr1.end(), 0, std::bit_xor<>())
         & std::accumulate(arr2.begin(), arr2.end(), 0, std::bit_xor<>());
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> arr1,
          std::vector<int> arr2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getXORSum(arr1, arr2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, {6, 5}, 0, trials);
    test({12}, {4}, 4, trials);
    return 0;
}


