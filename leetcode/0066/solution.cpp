#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> plusOne(std::vector<int> digits)
{
    const int n = static_cast<int>(digits.size());
    std::vector<int> result;
    result.reserve(n + 1);
    bool carry = true;
    for (int i = n - 1; i >= 0; --i)
    {
        int r = carry + digits[i];
        carry = (r > 9);
        if (r > 9) r -= 10;
        result.push_back(r);
    }
    if (carry) result.push_back(1);
    std::reverse(result.begin(), result.end());
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> digits, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = plusOne(digits);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, {1, 2, 4}, trials);
    test({4, 3, 2, 1}, {4, 3, 2, 2}, trials);
    test({9}, {1, 0}, trials);
    return 0;
}


