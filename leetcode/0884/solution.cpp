#include "../common/common.hpp"
#include <unordered_map>
#include <sstream>

// ############################################################################
// ############################################################################

std::vector<std::string> uncommonFromSentences(std::string s1, std::string s2)
{
    std::unordered_map<std::string, int> lut1, lut2;
    std::string token;
    for (std::stringstream stream(s1); stream >> token;)
        ++lut1[token];
    for (std::stringstream stream(s2); stream >> token;)
        ++lut2[token];
    std::vector<std::string> result;
    for (auto element : lut1)
    {
        if (auto search = lut2.find(element.first); search != lut2.end())
            lut2.erase(element.first);
        else if (element.second == 1)
            result.push_back(element.first);
    }
    for (auto element : lut2)
        if (element.second == 1)
            result.push_back(element.first);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_set<std::string> lut(left.begin(), left.end());
    for (std::string s : right)
    {
        if (auto search = lut.find(s); search != lut.end())
            lut.erase(search);
        else return false;
    }
    return lut.empty();
}

void test(std::string s1,
          std::string s2,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = uncommonFromSentences(s1, s2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("this apple is sweet", "this apple is sour", {"sweet", "sour"}, trials);
    test("apple apple", "banana", {"banana"}, trials);
    return 0;
}


