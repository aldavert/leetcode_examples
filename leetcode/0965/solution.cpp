#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

bool isUnivalTree(TreeNode* root)
{
    if (!root) return true;
    bool result = true;
    if (root->left) result = result
                          && (root->val == root->left->val)
                          && isUnivalTree(root->left);
    if (root->right) result = result
                           && (root->val == root->right->val)
                           && isUnivalTree(root->right);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = isUnivalTree(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 1, 1, 1, null, 1}, true, trials);
    test({2, 2, 2, 5, 2}, false, trials);
    return 0;
}


