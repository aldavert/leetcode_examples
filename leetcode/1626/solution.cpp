#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int bestTeamScore(std::vector<int> scores, std::vector<int> ages)
{
    struct Player
    {
        int age;
        int score;
        bool operator<(const Player &other) const
        {
            return (age > other.age) || ((age == other.age) && (score > other.score));
        }
    };
    const size_t n = scores.size();
    std::vector<Player> players;
    std::vector<int> dp(n);
    
    for (size_t i = 0; i < n; ++i)
        players.push_back({ages[i], scores[i]});
    std::sort(players.begin(), players.end());
    for (size_t i = 0; i < n; ++i)
    {
        dp[i] = players[i].score;
        for (size_t j = 0; j < i; ++j)
            if (players[j].score >= players[i].score)
                dp[i] = std::max(dp[i], dp[j] + players[i].score);
    }
    
    return *max_element(dp.begin(), dp.end());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> scores,
          std::vector<int> ages,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = bestTeamScore(scores, ages);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 5, 10, 15}, {1, 2, 3, 4, 5}, 34, trials);
    test({4, 5, 6, 5}, {2, 1, 2, 1}, 16, trials);
    test({1, 2, 3, 5}, {8, 9, 10, 1}, 6, trials);
    return 0;
}


