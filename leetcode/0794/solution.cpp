#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool validTicTacToe(std::vector<std::string> board)
{
    auto win = [&](char s) -> bool
    {
        for (int i = 0; i < 3; ++i)
            if (((board[i][0] == s) && (board[i][1] == s) && (board[i][2] == s))
            ||  ((board[0][i] == s) && (board[1][i] == s) && (board[2][i] == s)))
                return true;
        return (board[1][1] == s)
            && (((board[0][0] == s) && (board[2][2] == s))
            ||  ((board[0][2] == s) && (board[2][0] == s)));
    };
    if (board.size() != 3) return false;
    if ((board[0].size() != 3) || (board[1].size() != 3) || (board[2].size() != 3))
        return false;
    int number_os = 0, number_xs = 0, number_spaces = 0;
    for (int i = 0; i < 3; ++i)
        for (int j = 0; j < 3; ++j)
            number_os += board[i][j] == 'O',
            number_xs += board[i][j] == 'X',
            number_spaces += board[i][j] == ' ';
    if (number_os + number_xs + number_spaces != 9) return false;
    if ((number_os != number_xs) && (number_os + 1 != number_xs)) return false;
    if ((win('X') && (number_xs != number_os + 1))
    ||  (win('O') && (number_os != number_xs)))
        return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> board, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = validTicTacToe(board);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"O  ", "   ", "   "}, false, trials);
    test({"XOX", " X ", "   "}, false, trials);
    test({"XOX", "O O", "XOX"}, true, trials);
    test({"XXX", "   ", "OOO"}, false, trials);
    test({"XXX", "OOX", "OOX"}, true, trials);
    return 0;
}


