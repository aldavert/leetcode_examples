#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int largestSumAfterKNegations(std::vector<int> nums, int k)
{
    int result = 0;
    if (nums.size() > 200)
    {
        int histogram[201] = {};
        int max = -101;
        for (int n : nums)
        {
            max = std::max(max, n);
            ++histogram[n + 100];
        }
        max += 100;
        int i = 0;
        int previous = -1;
        for (int limit = std::min(100, max); k && (i < limit); ++i)
        {
            if (histogram[i])
            {
                int diff = std::min(histogram[i], k);
                k -= diff;
                result += (histogram[i] - diff) * (i - 100) - diff * (i - 100);
                previous = i;
            }
        }
        if (k & 1)
        {
            for (; (i < 201) && (histogram[i] == 0); ++i);
            if ((previous > -1) && (-(previous - 100) < (i - 100)))
                result += 2 * (previous - 100);
            else
            {
                result += (histogram[i] - 1) * (i - 100) - 1 * (i - 100);
                ++i;
            }
        }
        for (; i < 201; ++i)
            result += histogram[i] * (i - 100);
    }
    else
    {
        std::vector<int> sorted_nums(nums);
        std::sort(sorted_nums.begin(), sorted_nums.end());
        const size_t n = nums.size();
        size_t i = 0;
        for (size_t limit = std::min<size_t>(k, n - 1);
             (i < limit) && (sorted_nums[i] < 0); ++i)
            result += -sorted_nums[i];
        k = (k - static_cast<int>(i)) & 1;
        if (k && (i < n))
        {
            if ((i > 0)
            && (-sorted_nums[i - 1] < sorted_nums[i]))
                result += 2 * sorted_nums[i - 1];
            else result -= sorted_nums[i++];
        }
        for (; i < n; ++i) result += sorted_nums[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestSumAfterKNegations(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 3}, 1, 5, trials);
    test({3, -1, 0, 2}, 3, 6, trials);
    test({2, -3, -1, 5, -4}, 2, 13, trials);
    test({-2, -3, -1, -5, -4}, 10, 13, trials);
    test({-8, 3, -5, -3, -5, -2}, 6, 22, trials);
    return 0;
}


