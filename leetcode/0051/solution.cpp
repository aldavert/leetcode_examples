#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

std::vector<std::vector<std::string> > solveNQueens(int n)
{
    std::vector<std::vector<std::string> > results;
    int n2 = n * n;
    auto solve = [&](auto &&self, int row, char * grid) -> void
    {
        if (row == n) // Done.
        {
            results.push_back({});
            for (int i = 0; i < n; ++i)
            {
                std::string current_row(n, '.');
                for (int j = 0; j < n; ++j, ++grid)
                    if (*grid == 2)
                        current_row[j] = 'Q';
                results.back().push_back(current_row);
            }
        }
        else
        {
            char * grid_row = grid + n * row;
            for (int c = 0; c < n; ++c)
            {
                if (grid_row[c] == 1)
                {
                    char * dst = grid + n2;
                    std::memcpy(dst, grid, n2);
                    for (int i = row + 1, j = 1; i < n; ++i, ++j)
                    {
                        dst[n * i + c] = 0; // Mark the current column as occupied.
                        if (c - j >= 0)
                            dst[n * i + c - j] = 0;
                        if (c + j <  n)
                            dst[n * i + c + j] = 0;
                    }
                    dst[n * row + c] = 2; // Put the queen.
                    self(self, row + 1, dst);
                }
            }
        }
    };
    char grid[9 * 9 * 10];
    std::memset(grid, 1, sizeof(grid));
    solve(solve, 0, grid);
    return results;
}

// ############################################################################
// ############################################################################

void test(const int n,
          const std::vector<std::vector<std::string> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<std::string> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = solveNQueens(n);
    bool valid = false;
    if (solution.size() == result.size())
    {
        const int m = static_cast<int>(solution.size());
        std::vector<std::string> solutions_strs;
        std::vector<std::string> results_strs;
        
        for (int i = 0; i < m; ++i)
        {
            std::string current;
            for (int j = 0; j < n; ++j)
                current += solution[i][j];
            solutions_strs.push_back(current);
            current.clear();
            for (int j = 0; j < n; ++j)
                current += result[i][j];
            results_strs.push_back(current);
        }
        std::sort(solutions_strs.begin(), solutions_strs.end());
        std::sort(results_strs.begin(), results_strs.end());
        valid = true;
        for (int i = 0; valid && (i < m); ++i)
            valid = solutions_strs[i] == results_strs[i];
    }
    showResult(valid, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{".Q..", "...Q", "Q...", "..Q."},
             {"..Q.", "Q...", "...Q", ".Q.."}}, trials);
    test(1, {{"Q"}}, trials);
    return 0;
}


