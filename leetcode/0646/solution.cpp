#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int findLongestChain(std::vector<std::vector<int> > pairs)
{
    struct Pair
    {
        Pair(const std::vector<int> &pair) : left(pair[0]), right(pair[1]) {}
        int left = -2000;
        int right = -2000;
        bool operator<(const Pair &other) const { return right < other.right; }
    };
    std::vector<Pair> sorted_pairs(pairs.begin(), pairs.end());
    int result = 0, previous_end = std::numeric_limits<int>::lowest();
    
    std::sort(sorted_pairs.begin(), sorted_pairs.end());
    for (auto [left, right] : sorted_pairs)
    {
        if (left > previous_end)
        {
            ++result;
            previous_end = right;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > pairs, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLongestChain(pairs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 3}, {3, 4}}, 2, trials);
    test({{1, 2}, {7, 8}, {4, 5}}, 3, trials);
    return 0;
}


