#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <stack>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > levelOrderBottom(TreeNode * root)
{
    std::stack<std::vector<int> > level_stack;
    std::queue<TreeNode *> active_nodes;
    if (root) active_nodes.push(root);
    while (!active_nodes.empty())
    {
        std::vector<int> current_level;
        for (size_t i = 0, n = active_nodes.size(); i < n; ++i)
        {
            auto node = active_nodes.front();
            active_nodes.pop();
            current_level.push_back(node->val);
            if (node->left)  active_nodes.push(node->left);
            if (node->right) active_nodes.push(node->right);
        }
        level_stack.push(current_level);
    }
    std::vector<std::vector<int> > result;
    while (!level_stack.empty())
    {
        result.emplace_back(std::move(level_stack.top()));
        level_stack.pop();
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(const std::vector<int> &tree,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = levelOrderBottom(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 9, 20, null, null, 15, 7}, {{15, 7}, {9, 20}, {3}}, trials);
    test({1}, {{1}}, trials);
    test({}, {}, trials);
    return 0;
}


