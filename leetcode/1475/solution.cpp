#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

std::vector<int> finalPrices(std::vector<int> prices)
{
    std::vector<int> result(prices.size());
    std::stack<int> s;
    for (int i = static_cast<int>(prices.size()) - 1; i >= 0; --i)
    {
        while (s.size() && (s.top() > prices[i]))
            s.pop();
        result[i] = (s.size())?(prices[i] - s.top()):prices[i];
        s.push(prices[i]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> prices,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = finalPrices(prices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 4, 6, 2, 3}, {4, 2, 4, 2, 3}, trials);
    test({1, 2, 3, 4, 5}, {1, 2, 3, 4, 5}, trials);
    test({10, 1, 1, 6}, {9, 0, 1, 6}, trials);
    return 0;
}


