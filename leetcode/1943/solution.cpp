#include "../common/common.hpp"
#include <map>

// ############################################################################
// ############################################################################

std::vector<std::vector<long long> >
splitPainting(std::vector<std::vector<int> > segments)
{
    std::vector<std::vector<long long> > result;
    std::map<int, long> timeline;
    long running_mix = 0;
    int prev_index = 0;
    
    for (const auto &segment : segments)
        timeline[segment[0]] += segment[2],
        timeline[segment[1]] -= segment[2];
    for (const auto &[i, mix] : timeline)
    {
        if (running_mix > 0)
            result.push_back({prev_index, i, running_mix});
        running_mix += mix;
        prev_index = i;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > segments,
          std::vector<std::vector<long long> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<long long> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = splitPainting(segments);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 4, 5}, {4, 7, 7}, {1, 7, 9}}, {{1, 4, 14}, {4, 7, 16}}, trials);
    test({{1, 7, 9}, {6, 8, 15}, {8, 10, 7}},
         {{1, 6, 9}, {6, 7, 24}, {7, 8, 15}, {8, 10, 7}}, trials);
    test({{1, 4, 5}, {1, 4, 7}, {4, 7, 1}, {4, 7, 11}},
         {{1, 4, 12}, {4, 7, 12}}, trials);
    return 0;
}


