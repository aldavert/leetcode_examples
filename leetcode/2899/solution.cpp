#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> lastVisitedIntegers(std::vector<int> nums)
{
    std::vector<int> result, seen;
    for (size_t k = 0; int number : nums)
    {
        if (number == -1)
        {
            ++k;
            if (k > seen.size()) result.push_back(-1);
            else result.push_back(seen[seen.size() - k]);
        }
        else
        {
            seen.push_back(number);
            k = 0;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = lastVisitedIntegers(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, -1, -1, -1}, {2, 1, -1}, trials);
    test({1, -1, 2, -1, -1}, {1, 2, 1}, trials);
    return 0;
}


