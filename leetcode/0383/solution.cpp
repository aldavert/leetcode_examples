#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canConstruct(std::string ransomNote, std::string magazine)
{
    int histogram[26] = {};
    for (char m : magazine)
        ++histogram[m - 'a'];
    for (char r : ransomNote)
    {
        if (histogram[r - 'a'] > 0)
            --histogram[r - 'a'];
        else return false;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string ransomNote,
          std::string magazine,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canConstruct(ransomNote, magazine);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("a" , "b"  , false, trials);
    test("aa", "ab" , false, trials);
    test("aa", "aab",  true, trials);
    return 0;
}


