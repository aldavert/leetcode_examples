#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int superpalindromesInRange(std::string left, std::string right)
{
    auto reverseNumber = [](long v) -> long
    {
        long accumulate = 0;
        for (long copy = v; copy; )
        {
            long b = copy / 10, r = copy - b * 10;
            accumulate = accumulate * 10 + r;
            copy = b;
        }
        return accumulate;
    };
    auto isPalindrome = [&](long v) -> bool { return v == reverseNumber(v); };
    auto values = [&](unsigned int digits, long vleft, long vright) -> unsigned int
    {
        const bool cut_number = digits % 2 == 1;
        const unsigned int half = digits / 2 + cut_number;
        unsigned int max_digit = 1;
        for (unsigned int i = 0; i < half; ++i)
            max_digit *= 10;
        unsigned int number_of_superpalindromes = 0;
        if (cut_number)
        {
            for (unsigned int d = max_digit / 10; d < max_digit; ++d)
            {
                long v = max_digit * (d / 10) + reverseNumber(d);
                v = v * v;
                if ((v >= vleft) && (v <= vright) && isPalindrome(v))
                    ++number_of_superpalindromes;
            }
        }
        else
        {
            for (unsigned int d = max_digit / 10; d < max_digit; ++d)
            {
                long v = max_digit * d + reverseNumber(d);
                v = v * v;
                if ((v >= vleft) && (v <= vright) && isPalindrome(v))
                    ++number_of_superpalindromes;
            }
        }
        return number_of_superpalindromes;
    };
    const long vleft = std::stol(left);
    const long vright = std::stol(right);
    const unsigned int left_digits =
        static_cast<unsigned int>(std::ceil(static_cast<double>(left.size()) / 2.0));
    const unsigned int right_digits =
        static_cast<unsigned int>(std::ceil(static_cast<double>(right.size()) / 2.0));
    unsigned int number_of_superpalindromes = 0;
    for (unsigned int digits = left_digits; digits <= right_digits; ++digits)
        number_of_superpalindromes += values(digits, vleft, vright);
    return number_of_superpalindromes;
}

// ############################################################################
// ############################################################################

void test(std::string left,
          std::string right,
          int solution,
          unsigned int trials)
{
    int result = -1;
    for (unsigned int t = 0; t < trials; ++t)
        result = superpalindromesInRange(left, right);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("4", "1000", 4, trials);
    test("1", "2", 1, trials);
    test("10", "10000", 2, trials);
    return 0;
}


