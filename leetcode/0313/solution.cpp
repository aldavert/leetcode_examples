#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int nthSuperUglyNumber(int n, std::vector<int>& primes)
{
    const size_t m = primes.size();
    std::vector<long> result = {0, 1}, product(m), index(m, 1);
    for(int i = 2; i <= n; ++i)
    {
        long min_product = std::numeric_limits<long>::max();
        for (size_t j = 0; j < m; ++j)
        {
            product[j] = result[index[j]] * primes[j];
            if (product[j] < min_product)
                min_product = product[j];
        }
        result.push_back(min_product);
        for (size_t j = 0; j < m; ++j)
            if (product[j] == min_product)
                ++index[j];
    }
    return static_cast<int>(result.back());
}

// ############################################################################
// ############################################################################

void test(int n, std::vector<int> primes, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = nthSuperUglyNumber(n, primes);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(12, {2, 7, 13, 19}, 32, trials);
    test(1, {2, 3, 5}, 1, trials);
    return 0;
}


