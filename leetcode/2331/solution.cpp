#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

bool evaluateTree(TreeNode * root)
{
    if      (root->val == 1) return true;
    else if (root->val == 0) return false;
    else if (root->val == 2) return evaluateTree(root->left)
                                 || evaluateTree(root->right);
    else return evaluateTree(root->left)
             && evaluateTree(root->right);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = evaluateTree(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3, null, null, 0, 1}, true, trials);
    test({0}, false, trials);
    return 0;
}


