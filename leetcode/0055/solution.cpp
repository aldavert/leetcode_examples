#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool canJump(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int reach = 0;
    for (int i = 0; i < n; ++i)
    {
        if (reach >= n - 1) return true;
        if ((i == reach) && (nums[i] == 0)) return false;
        reach = std::max(reach, i + nums[i]);
    }
    return false;
}
#elif 0
bool canJump(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int reach = 0;
    for (int i = 0; (i <= reach) && (i < n); ++i)
        reach = std::max(reach, i + nums[i]);
    return reach >= n - 1;
}
#elif 0
bool canJump(std::vector<int> nums)
{
    int n = static_cast<int>(nums.size());
    std::vector<bool> reach_end(n, false);
    reach_end[n - 1] = true;
    
    for (int i = n - 2; i >= 0; --i)
    {
        const int end = std::min(n, i + nums[i] + 1);
        bool reachable = false;
        for (int j = i + 1; !reachable && (j < end); ++j)
            reachable = reach_end[j];
        reach_end[i] = reachable;
        if (reachable) n = i + 1;
    }
    return reach_end[0];
}
#else
bool canJump(std::vector<int> nums)
{
    int n = static_cast<int>(nums.size());
    std::vector<bool> reach_end(n, false);
    reach_end[n - 1] = true;
    
    for (int i = n - 2; i >= 0; --i)
    {
        const int end = std::min(n, i + nums[i] + 1);
        bool reachable = false;
        for (int j = i + 1; !reachable && (j < end); ++j)
            reachable = reach_end[j];
        reach_end[i] = reachable;
        if (reachable) n = i + 1;
    }
    return reach_end[0];
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canJump(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 1, 1, 4}, true , trials);
    test({3, 2, 1, 0, 4}, false, trials);
    return 0;
}


