#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int accountBalanceAfterPurchase(int purchaseAmount)
{
    int remaining = purchaseAmount % 10;
    return ((100 - purchaseAmount) / 10 + ((remaining < 5) && (remaining > 0))) * 10;
}

// ############################################################################
// ############################################################################

void test(int purchaseAmount, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = accountBalanceAfterPurchase(purchaseAmount);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test( 9,  90, trials);
    test(15,  80, trials);
    test(14,  90, trials);
    test( 4, 100, trials);
    test(10,  90, trials);
    return 0;
}


