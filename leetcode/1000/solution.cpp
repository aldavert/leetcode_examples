#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int mergeStones(std::vector<int> stones, int k)
{
    const int n = static_cast<int>(stones.size());
    if ((n - 1) % (k - 1)) return -1;
    int prefix[31] = {}, dp_table[30][30];
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
            dp_table[i][j] = 1'000'000'000;
        dp_table[i][i] = 0;
    }
    
    std::partial_sum(stones.begin(), stones.end(), &prefix[1]);
    for (int d = 1; d < n; ++d)
    {
        for (int i = 0; i + d < n; ++i)
        {
            int j = i + d;
            for (int m = i; m < j; m += k - 1)
                dp_table[i][j] = std::min(dp_table[i][j],
                                          dp_table[i][m] + dp_table[m + 1][j]);
            if (d % (k - 1) == 0)
                dp_table[i][j] += prefix[j + 1] - prefix[i];
        }
    }
    
    return dp_table[0][n - 1];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> stones, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = mergeStones(stones, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 4, 1}, 2, 20, trials);
    test({3, 2, 4, 1}, 3, -1, trials);
    test({3, 5, 1, 2, 6}, 3, 25, trials);
    return 0;
}


