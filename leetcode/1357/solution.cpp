#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

class Cashier
{
    int m_n;
    int m_discount;
    std::unordered_map<int, int> m_product_to_price;
    int m_count = 0;
public:
    Cashier(int n, int discount, std::vector<int> products, std::vector<int> prices) :
      m_n(n), m_discount(discount)
    {
        for (size_t i = 0; i < products.size(); ++i)
            m_product_to_price[products[i]] = prices[i];
    }
    double getBill(std::vector<int> product, std::vector<int> amount)
    {
        ++m_count;
        int total = 0;
        for (size_t i = 0; i < product.size(); ++i)
            total += m_product_to_price[product[i]] * amount[i];
        return (m_count % m_n == 0)?total*(1 - m_discount / 100.0) : total;
    }
};

// ############################################################################
// ############################################################################

void test(int n,
          int discount,
          std::vector<int> products,
          std::vector<int> prices,
          std::vector<std::vector<std::vector<int> > > input,
          std::vector<double> solution,
          unsigned int trials = 1)
{
    std::vector<double> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        Cashier obj(n, discount, products, prices);
        result.clear();
        for (size_t i = 0; i < input.size(); ++i)
            result.push_back(obj.getBill(input[i][0], input[i][1]));
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 50, {1, 2, 3, 4, 5, 6, 7}, {100, 200, 300, 400, 300, 200, 100},
         {{{1, 2}, {1, 2}},
          {{3, 7}, {10, 10}},
          {{1, 2, 3, 4, 5, 6, 7}, {1, 1, 1, 1, 1, 1, 1}},
          {{4}, {10}},
          {{7, 3}, {10, 10}},
          {{7, 5, 3, 1, 6, 4, 2}, {10, 10, 10, 9, 9, 9, 7}},
          {{2, 3, 5}, {5, 3, 2}}},
         {500.0, 4000.0, 800.0, 4000.0, 4000.0, 7350.0, 2500.0}, trials);
    return 0;
}


