#include "../common/common.hpp"
#include <limits>
#include <cstring>

// ############################################################################
// ############################################################################

#if 1
int connectTwoGroups(std::vector<std::vector<int> > cost)
{
    const int m = static_cast<int>(cost.size());
    const int n = static_cast<int>(cost[0].size());
    int dp[12][1 << 12], lowest[12];
    auto dfs = [&](auto &&self, int i, int mask) -> int
    {
        if (i == m)
        {
            int result = 0;
            for (int j = 0; j < n; ++j)
                if ((mask & (1 << j)) == 0)
                    result += lowest[j];
            return result;
        }
        if (dp[i][mask] != -1)
            return dp[i][mask];
        int result = std::numeric_limits<int>::max();
        for (int j = 0; j < n; ++j)
            result = std::min(result, cost[i][j] + self(self, i + 1, mask | (1 << j)));
        return dp[i][mask] = result;
    };
    
    std::memset(dp, -1, sizeof(dp));
    for (int i = 0; i < n; ++i)
    {
        int lowest_value = std::numeric_limits<int>::max();
        for (int j = 0; j < m; ++j)
            lowest_value = std::min(lowest_value, cost[j][i]);
        lowest[i] = lowest_value;
    }
    return dfs(dfs, 0, 0);
}
#else
int connectTwoGroups(std::vector<std::vector<int> > cost)
{
    constexpr int INIT = std::numeric_limits<int>::max();
    const int m = static_cast<int>(cost.size());
    const int n = static_cast<int>(cost[0].size());
    const int o = 1 << n;
    
    std::vector<std::vector<int> > dp(m + 1, std::vector<int>(o, INIT / 2));
    dp[0][0] = 0;                
    for (int i = 1; i <= m; ++i)
    {
        std::vector<int> cost_subset(o);
        for (int state = 0; state < o; ++state)
        {
            int sum = 0;
            for (int j = 0; j < n; ++j)
                if (((state >> j) & 1) == 1)
                    sum += cost[i - 1][j];
            cost_subset[state] = sum;
        }
        dp[i][0] = INIT / 2;
        for (int state = 1; state < o; ++state)
        {
            dp[i][state] = INIT / 2;
            for (int subset = state; subset > 0; subset = (subset - 1) & state)
                dp[i][state] = std::min(dp[i][state],
                                        dp[i - 1][state - subset] + cost_subset[subset]);

            int min_path = INIT;
            for (int j = 0; j < n; ++j)
                min_path = std::min(min_path, cost[i - 1][j]);
            dp[i][state] = std::min(dp[i][state],
                                    dp[i - 1][state] + min_path);                
        }
    }
    return dp[m][o - 1];
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > cost, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = connectTwoGroups(cost);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{15, 96}, {36, 2}}, 17, trials);
    test({{1, 3, 5}, {4, 1, 1}, {1, 5, 3}}, 4, trials);
    test({{2, 5, 1}, {3, 4, 7}, {8, 1, 2}, {6, 2, 4}, {3, 8, 8}}, 10, trials);
    return 0;
}


