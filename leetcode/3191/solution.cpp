#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = 0;
    for (int i = 0; i + 2 < n; ++i)
    {
        if (nums[i] == 0)
        {
            nums[i + 1] ^= 1;
            nums[i + 2] ^= 1;
            ++result;
        }
    }
    return ((nums[n - 1] == 0) || (nums[n - 2] == 0))?-1:result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 1, 1, 0, 0}, 3, trials);
    test({0, 1, 1, 1}, -1, trials);
    return 0;
}


