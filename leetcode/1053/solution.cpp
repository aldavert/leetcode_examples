#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> prevPermOpt1(std::vector<int> arr)
{
    const int n = static_cast<int>(arr.size());
    int left = n - 2, right = n - 1;
    while ((left >= 0) && (arr[left] <= arr[left + 1])) --left;
    if (left < 0) return arr;
    while ((arr[right] >= arr[left]) || (arr[right] == arr[right - 1])) --right;
    std::swap(arr[left], arr[right]);
    return arr;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = prevPermOpt1(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 1}, {3, 1, 2}, trials);
    test({1, 1, 5}, {1, 1, 5}, trials);
    test({1, 9, 4, 6, 7}, {1, 7, 4, 6, 9}, trials);
    return 0;
}


