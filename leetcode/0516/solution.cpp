#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestPalindromeSubseq(std::string s)
{
    const int n = static_cast<int>(s.size());
    std::vector<std::vector<int> > dp_table(n, std::vector<int>(n));
    for (int i = 0; i < n; ++i) dp_table[i][i] = 1;
    
    for (int d = 1; d < n; ++d)
    {
        for (int i = 0; i + d < n; ++i)
        {
            
            if (int j = i + d; s[i] == s[j])
                 dp_table[i][j] = 2 + dp_table[i + 1][j - 1];
            else dp_table[i][j] = std::max(dp_table[i + 1][j], dp_table[i][j - 1]);
        }
    }
    return dp_table[0][n - 1];
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestPalindromeSubseq(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("bbbab", 4, trials);
    test("cbbd", 2, trials);
    return 0;
}


