#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string clearDigits(std::string s)
{
    std::string result;
    for (char letter : s)
    {
        if (std::isdigit(letter))
        {
            if (!result.empty()) result.pop_back();
        }
        else result.push_back(letter);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = clearDigits(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", "abc", trials);
    test("cb34", "", trials);
    return 0;
}


