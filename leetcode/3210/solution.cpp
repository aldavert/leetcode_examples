#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string getEncryptedString(std::string s, int k)
{
    k %= static_cast<int>(s.size());
    return s.substr(k) + s.substr(0, k);
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getEncryptedString(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("dart", 3, "tdar", trials);
    test("aaa", 1, "aaa", trials);
    return 0;
}


