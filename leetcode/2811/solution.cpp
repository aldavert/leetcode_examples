#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canSplitArray(std::vector<int> nums, int m)
{
    if (nums.size() < 3) return true;
    for (size_t i = 1; i < nums.size(); ++i)
        if (nums[i] + nums[i - 1] >= m)
            return true;
    return false;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int m, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canSplitArray(nums, m);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 2, 1}, 4, true, trials);
    test({2, 1, 3}, 5, false, trials);
    test({2, 3, 3, 2, 3}, 6, true, trials);
    return 0;
}


