#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> recoverArray(std::vector<int> nums)
{
    auto sorted_nums(nums);
    std::sort(sorted_nums.begin(), sorted_nums.end());
    
    for(int i = 1, n = static_cast<int>(sorted_nums.size()); i < n; ++i)
    {
        int d = sorted_nums[i] - sorted_nums[0];
        if ((d == 0) || (d % 2 == 1)) continue;
        std::vector<bool> visited(n);
        std::vector<int> result;
        visited[i] = true;
        result.push_back((sorted_nums[0] + sorted_nums[i]) / 2);
        
        for (int l = 1, r = i + 1; r < n; ++l, ++r)
        {
            while ((l < n) && visited[l]) ++l;
            while ((r < n) && (sorted_nums[r] - sorted_nums[l] < d)) ++r;
            if ((r == n) || (sorted_nums[r] - sorted_nums[l] > d)) break;
            visited[r] = true;
            result.push_back((sorted_nums[l] + sorted_nums[r]) / 2);
            if (static_cast<int>(result.size()) > n / 2) return result;
        }
        if (static_cast<int>(result.size()) == n / 2) return result;
    }
    return {};
}
#else
std::vector<int> recoverArray(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> sorted_nums(nums), result;
    std::unordered_map<int, int> count;
    
    std::sort(sorted_nums.begin(), sorted_nums.end());
    for (int num : nums) ++count[num];
    
    for (int i = 1; (i < n) && result.empty(); ++i)
    {
        const int x = sorted_nums[i] - sorted_nums[0];
        if ((x <= 0) || (x & 1)) continue;
        auto copy = count;
        for (int num : sorted_nums)
        {
            if (auto it = copy.find(num)    ; (it == copy.end()) || (it->second == 0))
                continue;
            if (auto it = copy.find(num + x); (it == copy.end()) || (it->second == 0))
            {
                result.clear();
                break;
            }
            --copy[num];
            --copy[num + x];
            result.push_back(num + x / 2);
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = recoverArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 10, 6, 4, 8, 12}, {3, 7, 11}, trials);
    test({1, 1, 3, 3}, {2, 2}, trials);
    test({5, 435}, {220}, trials);
    test({1, 50, 99, 101, 150, 199}, {51, 100, 149}, trials);
    return 0;
}


