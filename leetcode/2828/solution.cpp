#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isAcronym(std::vector<std::string> words, std::string s)
{
    if (words.size() != s.size()) return false;
    for (size_t i = 0; i < s.size(); ++i)
        if (words[i][0] != s[i]) return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::string s,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isAcronym(words, s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"alice", "bob", "charlie"}, "abc", true, trials);
    test({"an", "apple"}, "a", false, trials);
    test({"never", "gonna", "give", "up", "on", "you"}, "ngguoy", true, trials);
    return 0;
}


