#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long findTheArrayConcVal(std::vector<int> nums)
{
    long long result = 0;
    for (int l = 0, r = static_cast<int>(nums.size()) - 1; l <= r; ++l, --r)
    {
        if (l == r) result += nums[l];
        else
        {
            int product = 1;
            for (int copy = nums[r]; copy; copy /= 10, product *= 10);
            result += nums[l] * product + nums[r];
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findTheArrayConcVal(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({7, 52, 2, 4}, 596, trials);
    test({5, 14, 13, 8, 12}, 673, trials);
    return 0;
}


