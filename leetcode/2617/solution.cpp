#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minimumVisitedCells(std::vector<std::vector<int> > grid)
{
    struct Info
    {
        int distance = -1, index = -1;
        bool operator<(const Info &other) const
        {
            return (distance > other.distance)
                || ((distance == other.distance) && (index > other.index));
        }
    };
    const int m = static_cast<int>(grid.size()),
              n = static_cast<int>(grid[0].size());
    std::vector<std::vector<int> > distance(m, std::vector<int>(n, -1));
    std::vector<std::priority_queue<Info> > row(m), col(n);
    distance[0][0] = 1;
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            while (!row[i].empty()
               &&  (grid[i][row[i].top().index] + row[i].top().index < j))
                row[i].pop();
            if (!row[i].empty()
            &&  ((distance[i][j] == -1) || (row[i].top().distance + 1 < distance[i][j])))
                distance[i][j] = row[i].top().distance + 1;
            while (!col[j].empty()
               &&  (grid[col[j].top().index][j] + col[j].top().index < i))
                col[j].pop();
            if (!col[j].empty()
            &&  ((distance[i][j] == -1) || (col[j].top().distance + 1 < distance[i][j])))
                distance[i][j] = col[j].top().distance + 1;
            if (distance[i][j] != -1)
            {
                row[i].push({distance[i][j], j});
                col[j].push({distance[i][j], i});
            }
        }
    }
    return distance[m - 1][n - 1];
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumVisitedCells(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 4, 2, 1}, {4, 2, 3, 1}, {2, 1, 0, 0}, {2, 4, 0, 0}}, 4, trials);
    test({{3, 4, 2, 1}, {4, 2, 1, 1}, {2, 1, 1, 0}, {3, 4, 1, 0}}, 3, trials);
    test({{2, 1, 0}, {1, 0, 0}}, -1, trials);
    return 0;
}


