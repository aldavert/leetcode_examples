#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int minFlips(std::vector<std::vector<int> > mat)
{
    const int directions[] = {0, 1, 0, -1, 0};
    const int n_rows = static_cast<int>(mat.size());
    const int n_cols = static_cast<int>(mat[0].size());
    std::queue<int> q;
    std::unordered_set<int> seen;
    {
        int hashed = 0;
        for (int row = 0, k = 0; row < n_rows; ++row)
            for (int col = 0; col < n_cols; ++col, ++k)
                if (mat[row][col]) hashed |= 1 << k;
        if (hashed == 0) return 0;
        q.push(hashed);
        seen.insert(hashed);
    }
    for (int step = 1; !q.empty(); ++step)
    {
        for (int sz = static_cast<int>(q.size()); sz > 0; --sz)
        {
            const int current = q.front();
            q.pop();
            for (int row = 0; row < n_rows; ++row)
            {
                for (int col = 0; col < n_cols; ++col)
                {
                    int next = current ^ (1 << (row * n_cols + col));
                    for (int k = 0; k < 4; ++k)
                    {
                        const int x = row + directions[k];
                        const int y = col + directions[k + 1];
                        if ((x < 0) || (x == n_rows) || (y < 0) || (y == n_cols))
                            continue;
                        next ^= 1 << (x * n_cols + y);
                    }
                    if (next == 0) return step;
                    if (seen.find(next) != seen.end()) continue;
                    q.push(next);
                    seen.insert(next);
                }
            }
        }
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > mat, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minFlips(mat);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0}, {0, 1}}, 3, trials);
    test({{0}}, 0, trials);
    test({{1, 0, 0}, {1, 0, 0}}, -1, trials);
    return 0;
}


