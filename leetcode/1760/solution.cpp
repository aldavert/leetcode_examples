#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumSize(std::vector<int> nums, int maxOperations)
{
    int left = 1, right = *std::max_element(nums.begin(), nums.end());
    
    while (left < right)
    {
        int middle = (left + right) / 2, operations = 0;
        for (int number : nums)
            operations += (number - 1) / middle;
        if (operations <= maxOperations)
            right = middle;
        else left = middle + 1;
    }
    return left;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int maxOperations,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumSize(nums, maxOperations);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({9}, 2, 3, trials);
    test({2, 4, 8, 2}, 4, 2, trials);
    return 0;
}


