#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

#if 0
void recoverTree(TreeNode* root)
{
    TreeNode * previous = nullptr;
    TreeNode * wrong_1st = nullptr;
    TreeNode * wrong_2on = nullptr;
    auto findPredecessor = [](TreeNode * current) -> TreeNode *
    {
        TreeNode * forward = current->left;
        while (forward->right && (forward->right != current))
            forward = forward->right;
        return forward;
    };
    auto visit = [&](TreeNode* current)
    {
        if (previous && (current->val < previous->val))
        {
            wrong_2on = current;
            if (!wrong_1st) wrong_1st = previous;
        }
        previous = current;
    };
    
    for (TreeNode * current = root; current; )
    {
        if (current->left)
        {
            TreeNode * previous_morris = findPredecessor(current);
            if (previous_morris->right)
            {
                visit(current);
                previous_morris->right = nullptr;
                current = current->right;
            }
            else
            {
                previous_morris->right = current;
                current = current->left;
            }
        }
        else
        {
            visit(current);
            current = current->right;
        }
    }
    std::swap(wrong_1st->val, wrong_2on->val);
}
#else
void recoverTree(TreeNode* root)
{
    TreeNode * previous = nullptr;
    TreeNode * wrong_1st = nullptr;
    TreeNode * wrong_2on = nullptr;
    auto inorder = [&](auto &&self, TreeNode * current) -> void
    {
        if (current)
        {
            self(self, current->left);
            if (previous && (current->val < previous->val))
            {
                wrong_2on = current;
                if (!wrong_1st) wrong_1st = previous;
                else return;
            }
            previous = current;
            self(self, current->right);
        }
    };
    inorder(inorder, root);
    std::swap(wrong_1st->val, wrong_2on->val);
}
#endif


// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        recoverTree(root);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, null, null, 2}, {3, 1, null, null, 2}, trials);
    test({3, 1, 4, null, null, 2}, {2, 1, 4, null, null, 3}, trials);
    return 0;
}


