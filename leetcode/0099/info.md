# Recover Binary Search Tree

You are given the `root` of a binary search tree (BST), where the values of **exactly** two nodes of the tree were swapped by mistake. *Recover the tree without changing its structure.*
 
#### Example 1:
> ```mermaid 
> graph TD;
> A((3))-->B((2))
> A-->C((1))
> D((1))-->E((2))
> D-->F((3))
> classDef wrong fill:#FAA,stroke:#000,stroke-width:5px;
> class A,C wrong;
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `root = [1, 3, null, null, 2]`  
> *Output:* `[3, 1, null, null, 2]`  
> *Explanation:* `3` cannot be a left child of `1` because `3 > 1`. Swapping `1` and `3` makes the BST valid.

#### Example 2:
> ```mermaid 
> graph TD;
> A((3))-->B((1))
> A-->C((4))
> C-->D((2))
> C-->E1(( ))
> F((2))-->G((1))
> F-->H((4))
> H-->I((3))
> H-->E2(( ))
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> class E1,E2 empty;
> classDef wrong fill:#FAA,stroke:#000,stroke-width:5px;
> class A,D wrong;
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> linkStyle 3,7 stroke-width:0px
> ```
> *Input:* `root = [3, 1, 4, null, null, 2]`  
> *Output:* `[2, 1, 4, null, null, 3]`  
> *Explanation:* `2` cannot be in the right subtree of `3` because `2 < 3`. Swapping `2` and `3` makes the BST valid.
 
#### Constraints:
- The number of nodes in the tree is in the range `[2, 1000]`.
- `-2^31 <= Node.val <= 2^31 - 1`
 
**Follow up:** A solution using `O(n)` space is pretty straight-forward. Could you devise a constant `O(1)` space solution?


