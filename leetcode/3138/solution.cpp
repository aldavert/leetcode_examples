#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minAnagramLength(std::string s)
{
    const int n = static_cast<int>(s.size());
    auto canFormAnagram = [&](int k) -> bool
    {
        int anagram_count[26] = {}, running_count[26] = {};
        for (int i = 0; i < k; ++i) ++anagram_count[s[i] - 'a'];
        for (int i = k; i < n; ++i)
        {
            ++running_count[s[i] - 'a'];
            if (i % k == k - 1)
            {
                for (int j = 0; j < 26; ++j)
                {
                    if (running_count[j] != anagram_count[j]) return false;
                    running_count[j] = 0;
                }
            }
        }
        return true;
    };
    for (int k = 1; k <= n; ++k)
        if ((n % k == 0) && canFormAnagram(k))
            return k;
    return n;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minAnagramLength(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abba", 2, trials);
    test("cdef", 4, trials);
    return 0;
}


