#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minAddToMakeValid(std::string s)
{
    int open = 0, close = 0;
    for (char symbol : s)
    {
        if (symbol == '(') ++open;
        else
        {
            if (open == 0) ++close;
            else --open;
        }
    }
    return open + close;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minAddToMakeValid(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("())", 1, trials);
    test("(((", 3, trials);
    return 0;
}


