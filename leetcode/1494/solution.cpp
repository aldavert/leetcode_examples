#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minNumberOfSemesters(int n, std::vector<std::vector<int> > relations, int k)
{
    std::vector<int> dp(1 << n, n), prereq(n);
    for (const auto &r : relations)
        prereq[r[1] - 1] |= 1 << (r[0] - 1);
    dp[0] = 0;
    for (int i = 0, m = static_cast<int>(dp.size()); i < m; ++i)
    {
        int courses_can_be_taken = 0;
        for (int j = 0; j < n; ++j)
            if ((i & prereq[j]) == prereq[j])
                courses_can_be_taken |= 1 << j;
        courses_can_be_taken &= ~i;
        for (int s = courses_can_be_taken; s; s = (s - 1) & courses_can_be_taken)
            if (__builtin_popcount(s) <= k)
                dp[i | s] = std::min(dp[i | s], dp[i] + 1);
    }
    return dp.back();
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > relations,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minNumberOfSemesters(n, relations, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{2, 1}, {3, 1}, {1, 4}}, 2, 3, trials);
    test(5, {{2, 1}, {3, 1}, {4, 1}, {1, 5}}, 2, 4, trials);
    return 0;
}


