#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countMatchingSubarrays(std::vector<int> nums, std::vector<int> pattern)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> nums_pattern;
    for (int i = 1; i < n; ++i)
        nums_pattern.push_back((nums[i - 1] < nums[i]) - (nums[i - 1] > nums[i]));
    std::vector<int> lps(pattern.size());
    for (int i = 1, j = 0, m = static_cast<int>(pattern.size()); i < m; ++i)
    {
        while ((j > 0) && (pattern[j] != pattern[i])) j = lps[j - 1];
        if (pattern[i] == pattern[j]) lps[i] = ++j;
    }
    int result = 0;
    for (int i = 0, j = 0, m = static_cast<int>(pattern.size()); i < n - 1;)
    {
        if (nums_pattern[i] == pattern[j])
        {
            ++i;
            ++j;
            if (j == m)
            {
                ++result;
                j = lps[j - 1];
            }
        }
        else if (j > 0) j = lps[j - 1];
        else ++i;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<int> pattern,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countMatchingSubarrays(nums, pattern);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6}, {1, 1}, 4, trials);
    test({1, 4, 4, 1, 3, 5, 5, 3}, {1, 0, -1}, 2, trials);
    test({872500231, 190002870}, {-1}, 1, trials);
    return 0;
}


