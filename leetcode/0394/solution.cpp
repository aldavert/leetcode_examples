#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string decodeString(std::string s)
{
    const int n = static_cast<int>(s.size());
    int pos = 0;
    auto process = [&](auto &&self)->std::string
    {
        std::string result;
        for (; pos < n; ++pos)
        {
            if ((s[pos] >= '0') && (s[pos] <= '9'))
            {
                int number = 0;
                for (; s[pos] != '['; ++pos)
                    number = number * 10 + static_cast<int>(s[pos] - '0');
                ++pos;
                std::string pattern = self(self);
                for (int k = 0; k < number; ++k)
                    result += pattern;
            }
            else if (s[pos] == ']') return result;
            else result += s[pos];
        }
        return result;
    };
    return process(process);
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = decodeString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("3[a]2[bc]", "aaabcbc", trials);
    test("3[a2[c]]", "accaccacc", trials);
    test("2[abc]3[cd]ef", "abcabccdcdcdef", trials);
    test("abc3[cd]xyz", "abccdcdcdxyz", trials);
    return 0;
}


