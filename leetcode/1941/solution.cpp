#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool areOccurrencesEqual(std::string s)
{
    int histogram[128] = {};
    for (char letter : s)
        ++histogram[static_cast<int>(letter)];
    int i = 0;
    while (!histogram[i] && (i < 128)) ++i;
    for (int j = i + 1; j < 128; ++j)
        if (histogram[j] && (histogram[j] != histogram[i]))
            return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = areOccurrencesEqual(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abacbc", true, trials);
    test("aaabb", false, trials);
    return 0;
}


