#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxScore(std::vector<std::vector<int> > grid)
{
    constexpr int MAX = 200'000;
    int result = -MAX;
    for (int i = 0, n = static_cast<int>(grid.size()); i < n; ++i)
    {
        for (int j = 0, m = static_cast<int>(grid[0].size()); j < m; ++j)
        {
            const int prev_min = std::min((i > 0)?grid[i - 1][j]:MAX,
                                          (j > 0)?grid[i][j - 1]:MAX);
            result = std::max(result, grid[i][j] - prev_min);
            grid[i][j] = std::min(grid[i][j], prev_min);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxScore(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{9, 5, 7, 3}, {8, 9, 6, 1}, {6, 7, 14, 3}, {2, 5, 3, 1}}, 9, trials);
    test({{4, 3, 2}, {3, 2, 1}}, -1, trials);
    return 0;
}


