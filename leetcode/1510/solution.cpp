#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

#if 1
bool winnerSquareGame(int n)
{
    std::bitset<100'002> dp;
    dp[1] = true;
    for (int i = 0; i <=n ; ++i)
    {
        if (dp[i]) continue;
        for (int j = 1, s = i + 1; s <= n; ++j, s = i + j * j)
            dp[s] = true;
    }
    return dp[n];
}
#else
bool winnerSquareGame(int n)
{
    std::bitset<100'002> dp;
    for (int i = 1; i <= n; ++i)
    {
        for (int j = 1, s = 1; s <= i; ++j, s = j * j)
        {
            if (!dp[i - s])
            {
                dp[i] = true;
                break;
            }
        }
    }
    return dp[n];
}
#endif

// ############################################################################
// ############################################################################

void test(int n, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = winnerSquareGame(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, true, trials);
    test(2, false, trials);
    test(4, true, trials);
    test(7, false, trials);
    return 0;
}


