#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int countPartitions(std::vector<int> nums, int k)
{
    constexpr int MOD = 1'000'000'007;
    const long sum = std::accumulate(nums.begin(), nums.end(), 0L);
    std::vector<long> dp(k + 1);
    
    long result = 1;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        result = (result * 2) % MOD;
    
    dp[0] = 1;
    for (int num : nums)
        for (int i = k; i >= num; --i)
            dp[i] = (dp[i] + dp[i - num]) % MOD;
    
    for (int i = 0; i < k; ++i)
    {
        if (sum - i < k) result -= dp[i];
        else result -= dp[i] * 2;
    }
    return static_cast<int>((result % MOD + MOD) % MOD);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPartitions(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 4, 6, trials);
    test({3, 3, 3}, 4, 0, trials);
    test({6, 6}, 2, 2, trials);
    return 0;
}

