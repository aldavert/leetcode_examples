#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int getMaximumConsecutive(std::vector<int> coins)
{
    int result = 1;
    
    std::sort(coins.begin(), coins.end());
    for (int coin : coins)
    {
        if (coin > result)
            return result;
        result += coin;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> coins, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = getMaximumConsecutive(coins);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3}, 2, trials);
    test({1, 1, 1, 4}, 8, trials);
    test({1, 4, 10, 3, 1}, 20, trials);
    return 0;
}


