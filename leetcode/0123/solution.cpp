#include "../common/common.hpp"
#include <unordered_map>
#include <limits>
#include "testA.hpp"

// ############################################################################
// ############################################################################

#if 1
int maxProfit(std::vector<int> prices)
{
    int buy_1st = std::numeric_limits<int>::min(),
        buy_2on = std::numeric_limits<int>::min();
    int sell_1st = 0, sell_2on = 0;
    for (int p : prices)
    {
        buy_1st  = std::max(buy_1st ,           -p);
        sell_1st = std::max(sell_1st, buy_1st  + p);
        buy_2on  = std::max(buy_2on,  sell_1st - p);
        sell_2on = std::max(sell_2on, buy_2on  + p);
    }
    return sell_2on;
}
#else
int maxProfit(std::vector<int> prices)
{
    const int n = static_cast<int>(prices.size());
    if (n > 1000) return 0; // IT DOESN'T WORK FOR LARGE NUMBERS
    std::unordered_map<unsigned int, int> memo;
    auto recursive = [&](auto &&self, short day, short price, char ops) -> int
    {
        if (ops == 0) return 0;
        if (day >= n) return 0;
        const unsigned int key = static_cast<unsigned int>((static_cast<long>(ops) << 30)
                               | (static_cast<int>(day) << 16
                               | (0x0000FFFF & static_cast<int>(price))));
        if (auto it = memo.find(key); it != memo.end()) return it->second;
        if (price < 0) // BUYING PHASE ...
            return memo[key] = std::max(self(self, day + 1, (short)prices[day], ops), // BUY
                                        self(self, day + 1, -1, ops));         // PASS
        else // SELLING PHASE ...
            return memo[key] = std::max(self(self, day + 1, -1, ops - 1)
                                                       + prices[day] - price, // SELL
                                        self(self, day + 1, price, ops));      // PASS
    };
    return recursive(recursive, 0, -1, 2);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> prices, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxProfit(prices);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 0, 2, 9, 3}, 11, trials);
    test({3, 3, 5, 0, 0, 3, 1, 4}, 6, trials);
    test({1, 2, 3, 4, 5}, 4, trials);
    test({2, 1, 2, 0, 1}, 2, trials);
    test({7, 6, 4, 3, 1}, 0, trials);
    test({1}, 0, trials);
    test(testA::prices, testA::solution, trials);
    return 0;
}


