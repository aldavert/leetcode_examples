#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int beautifulSplits(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    std::vector<std::vector<int> > all_z;
    int result = 0;
    auto zFunction = [&](int start) -> std::vector<int>
    {
        std::vector<int> z(n);
        int l = 0, r = 0;
        for (int i = 1 + start; i < n; ++i)
        {
            if (i < r) z[i] = std::min(r - i, z[i - l + start]);
            while ((i + z[i] < n) && (nums[z[i] + start] == nums[i + z[i]]))
                ++z[i];
            if (i + z[i] > r)
            {
                l = i;
                r = i + z[i];
            }
        }
        return z;
    };
    
    for (int start = 0; start < n; ++start)
        all_z.push_back(zFunction(start));
    for (int i = 0; i < n - 2; ++i)
        for (int j = i + 1; j < n - 1; ++j)
            result += (((j - i >= i + 1) && (all_z[0][i + 1] >= i + 1))
                   ||  (all_z[i + 1][j + 1] >= j - i));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = beautifulSplits(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2, 1}, 2, trials);
    test({1, 2, 3, 4}, 0, trials);
    return 0;
}


