#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int numWays(std::vector<std::string> words, std::string target)
{
    const int MOD = 1'000'000'007;
    int n = static_cast<int>(words[0].size()),
        k = static_cast<int>(target.size());
    int dp_table[1001][2] = {}, histogram[1001][26] = {};
    dp_table[0][0] = 1;
    for (auto &s : words)
        for (int i = 0; i < n; ++i)
            ++histogram[i][s[i] - 'a'];
    for (int j = 1; j <= k; ++j)
    {
        long prefsum = 0;
        for (int i = j; i <= n; ++i)
        {
            prefsum = (prefsum + static_cast<long>(dp_table[i - 1][(j - 1) % 2])) % MOD;
            if (histogram[i - 1][target[j - 1] - 'a'] == 0)
            {
                dp_table[i][j % 2] = 0;
                continue;
            }
            dp_table[i][j % 2] = static_cast<int>((prefsum
                                        * histogram[i - 1][target[j - 1] - 'a']) % MOD);
        }
    }
    int result = 0;
    for (int i = k; i <= n; ++i)
        result = (result + dp_table[i][k % 2]) % MOD;
    return result;
}
#else
int numWays(std::vector<std::string> words, std::string target)
{
    const int MOD = 1'000'000'007;
    int m = static_cast<int>(target.size()), n = static_cast<int>(words[0].size());
    std::vector<std::vector<int> > count(n, std::vector<int>(26));
    for (auto &w : words)
        for (int j = 0; j < n; ++j)
            ++count[j][w[j] - 'a'];
    std::vector<std::vector<int> > dp_table(m, std::vector<int>(n, -1));
    auto dp_process = [&](auto &&self, int i, int j) -> int
    {
        if (i >= m) return 1;
        if (j >= n) return 0;
        if (dp_table[i][j] != -1) return dp_table[i][j];
        long result = self(self, i, j + 1);
        result = result
               + static_cast<long>(self(self, i + 1, j + 1))
               * static_cast<long>(count[j][target[i] - 'a']);
        return dp_table[i][j] = static_cast<int>(result % MOD);
    };
    return dp_process(dp_process, 0, 0);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words,
          std::string target,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numWays(words, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"acca", "bbbb", "caca"}, "aba", 6, trials);
    test({"abba", "baab"}, "bab", 4, trials);
    return 0;
}


