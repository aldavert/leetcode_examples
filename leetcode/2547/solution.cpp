#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minCost(std::vector<int> nums, int k)
{
    constexpr int MAX = 1001;
    const int n = static_cast<int>(nums.size());
    std::vector<std::vector<int> > trimmed_length(n, std::vector<int>(n));
    std::vector<int> dp(n + 1, std::numeric_limits<int>::max() / 2);
    
    for (int i = 0; i < n; ++i)
    {
        int length = 0, count[MAX] = {};
        for (int j = i; j < n; ++j)
        {
            if (++count[nums[j]] == 2)
                length += 2;
            else if (count[nums[j]] > 2)
                ++length;
            trimmed_length[i][j] = length;
        }
    }
    
    dp[n] = 0;
    for (int i = n - 1; i >= 0; --i)
        for (int j = i; j < n; ++j)
            dp[i] = std::min(dp[i], k + trimmed_length[i][j] + dp[j + 1]);
    return dp[0];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCost(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1, 2, 1, 3, 3}, 2, 8, trials);
    test({1, 2, 1, 2, 1}, 2, 6, trials);
    test({1, 2, 1, 2, 1}, 5, 10, trials);
    return 0;
}


