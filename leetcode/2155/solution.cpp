#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> maxScoreIndices(std::vector<int> nums)
{
    const size_t zeros = std::count(nums.begin(), nums.end(), 0);
    const int ones = static_cast<int>(nums.size() - zeros);
    std::vector<int> result{0};
    int left_zeros = 0, left_ones = 0, max_score = ones;
    
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        left_zeros += (nums[i] == 0);
        left_ones  += (nums[i] == 1);
        int score = left_zeros + ones - left_ones;
        if (max_score == score)
            result.push_back(i + 1);
        else if (max_score < score)
        {
            max_score = score;
            result = {i + 1};
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxScoreIndices(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 0, 1, 0}, {2, 4}, trials);
    test({0, 0, 0}, {3}, trials);
    test({1, 1}, {0}, trials);
    return 0;
}


