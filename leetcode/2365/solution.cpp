#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

long long taskSchedulerII(std::vector<int> tasks, int space)
{
    std::unordered_map<int, long> next_available;
    long result = 0;
    for (int task : tasks)
    {
        result = std::max(result + 1, next_available[task]);
        next_available[task] = result + space + 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tasks,
          int space,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = taskSchedulerII(tasks, space);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1, 2, 3, 1}, 3, 9, trials);
    test({5, 8, 8, 5}, 2, 6, trials);
    return 0;
}


