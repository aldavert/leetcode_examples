#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isFascinating(int n)
{
    int numbers[10] = {};
    auto extractDigits = [&](int value) -> void
    {
        for (; value; value /= 10)
            ++numbers[value % 10];
    };
    extractDigits(n);
    extractDigits(2 * n);
    extractDigits(3 * n);
    if (numbers[0] != 0) return false;
    for (int i = 1; i < 10; ++i) if (numbers[i] != 1) return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(int n, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isFascinating(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(192, true, trials);
    test(100, false, trials);
    return 0;
}


