#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int largestSubmatrix(std::vector<std::vector<int> > matrix)
{
    const int ncols = static_cast<int>(matrix[0].size());
    std::vector<int> histogram(ncols), sorted_histogram(ncols);
    int result = 0;
    for (const auto &row : matrix)
    {
        for (int col = 0; col < ncols; ++col)
            histogram[col] = (row[col] != 0) * (histogram[col] + 1);
        std::copy(histogram.begin(), histogram.end(), sorted_histogram.begin());
        std::sort(sorted_histogram.begin(), sorted_histogram.end());
        for (int col = 0; col < ncols; ++col)
            result = std::max(result, sorted_histogram[col] * (ncols - col));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > matrix, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestSubmatrix(matrix);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0, 1}, {1, 1, 1}, {1, 0, 1}}, 4, trials);
    test({{1, 0, 1, 0, 1}}, 3, trials);
    test({{1, 1, 0}, {1, 0, 1}}, 2, trials);
    test({{0, 1, 1, 0, 0},
          {1, 1, 1, 0, 0},
          {0, 1, 1, 1, 0},
          {1, 0, 1, 1, 1},
          {1, 1, 1, 1, 1}}, 8, trials);
    return 0;
}


