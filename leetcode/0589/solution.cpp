#include "../common/common.hpp"
#include <stack>
#include <queue>

constexpr int null = -1000;

class Node
{
public:
    int val = 0;
    std::vector<Node *> children;

    Node() = default;
    Node(int _val) : val(_val) {}
    Node(int _val, std::vector<Node *> _children) : val(_val), children(_children) {}
    ~Node(void)
    {
        for (auto &n : children) delete n;
    }
};

Node * vec2tree(const std::vector<int> &tree)
{
    const int n = static_cast<int>(tree.size());
    if (n == 0) return nullptr;
    Node * root = new Node(tree[0]);
    std::queue<Node *> queue;
    queue.push(root);
    for (int i = 2; i < n; ++i)
    {
        Node * current = queue.front();
        queue.pop();
        int j;
        for (j = i; (j < n) && (tree[j] != null); ++j);
        current->children.resize(j - i, nullptr);
        for (int k = i; k < j; ++k)
            queue.push(current->children[k - i] = new Node(tree[k]));
        i = j;
    }
    return root;
}

// ############################################################################
// ############################################################################

#if 1
std::vector<int> preorder(Node * root)
{
    std::vector<int> result;
    std::stack<Node *> s;
    s.push(root);
    while (!s.empty())
    {
        Node * ptr = s.top();
        s.pop();
        if (!ptr) continue;
        result.push_back(ptr->val);
        for (int i = static_cast<int>(ptr->children.size()) - 1; i >= 0; --i)
            s.push(ptr->children[i]);
    }
    return result;
}
#else
std::vector<int> preorder(Node * root)
{
    std::vector<int> result;
    auto traverse = [&](auto &&self, Node * ptr) -> void
    {
        if (!ptr) return;
        result.push_back(ptr->val);
        for (Node * p : ptr->children)
            self(self, p);
    };
    traverse(traverse, root);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        Node * root = vec2tree(tree);
        result = preorder(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, null, 3, 2, 4, null, 5, 6}, {1, 3, 5, 6, 2, 4}, trials);
    test({1, null, 2, 3, 4, 5, null, null, 6, 7, null, 8, null, 9, 10, null,
          null, 11, null, 12, null, 13, null, null, 14},
         {1, 2, 3, 6, 7, 11, 14, 4, 8, 12, 5, 9, 13, 10}, trials);
    return 0;
}


