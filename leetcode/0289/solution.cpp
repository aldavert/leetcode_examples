#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
void gameOfLife(std::vector<std::vector<int> > &board)
{
    const int m = static_cast<int>(board.size()),
              n = static_cast<int>(board[0].size());
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            int ones = ((j - 1 >= 0)?(board[i][j - 1] & 1):0)
                     + ((j + 1 <  n)?(board[i][j + 1] & 1):0);
            if (i - 1 >= 0)
                ones += ((j - 1 >= 0)?(board[i - 1][j - 1] & 1):0)
                     +                (board[i - 1][j    ] & 1)
                     +  ((j + 1 <  n)?(board[i - 1][j + 1] & 1):0);
            if (i + 1 <  m)
                ones += ((j - 1 >= 0)?(board[i + 1][j - 1] & 1):0)
                     +                (board[i + 1][j    ] & 1)
                     +  ((j + 1 <  n)?(board[i + 1][j + 1] & 1):0);
            board[i][j] |= 2 * ((ones == 3) || ((board[i][j] & 1) && (ones == 2)));
        }
    }
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            board[i][j] >>= 1;
}
#else
void gameOfLife(std::vector<std::vector<int> > &board)
{
    const int m = static_cast<int>(board.size() - 1),
              n = static_cast<int>(board[0].size() - 1);
    auto update = [&](int x, int y, int ones)
    {
        board[x][y] |= 2 * ((ones == 3) || ((board[x][y] & 1) && (ones == 2)));
    };
    if ((m == 0) && (n == 0))
    {
        board[0][0] = 0;
        return;
    }
    else if (m == 0)
    {
        update(0, 0, board[    0][    1] & 1);
        update(m, 0, board[    0][n - 1] & 1);
        for (int j = 1; j < n; ++j)
            update(0, j, (board[0][j - 1] & 1) + (board[0][j + 1] & 1));
    }
    else if (n == 0)
    {
        update(0, 0, board[    1][    0] & 1);
        update(m, 0, board[m - 1][    0] & 1);
        for (int i = 1; i < m; ++i)
            update(i, 0, (board[i - 1][    0] & 1) + (board[i + 1][    0] & 1));
    }
    else
    {
        // Top-left corner.
        update(0, 0, (board[    0][    1] & 1)
                   + (board[    1][    0] & 1)
                   + (board[    1][    1] & 1));
        // Bottom-left corner.
        update(m, 0, (board[m    ][    1] & 1)
                   + (board[m - 1][    0] & 1)
                   + (board[m - 1][    1] & 1));
        // Top-right corner.
        update(0, n, (board[    0][n - 1] & 1)
                   + (board[    1][n    ] & 1)
                   + (board[    1][n - 1] & 1));
        // Bottom-right corner.
        update(m, n, (board[m    ][n - 1] & 1)
                   + (board[m - 1][n    ] & 1)
                   + (board[m - 1][n - 1] & 1));
        // Top and bottom rows.
        for (int j = 1; j < n; ++j)
        {
            update(0, j, (board[    0][j - 1] & 1)
                       + (board[    0][j + 1] & 1)
                       + (board[    1][j - 1] & 1)
                       + (board[    1][j    ] & 1)
                       + (board[    1][j + 1] & 1));
            update(m, j, (board[m    ][j - 1] & 1)
                       + (board[m    ][j + 1] & 1)
                       + (board[m - 1][j - 1] & 1)
                       + (board[m - 1][j    ] & 1)
                       + (board[m - 1][j + 1] & 1));
        }
        // Left and right columns.
        for (int i = 1; i < m; ++i)
        {
            update(i, 0, (board[i - 1][    0] & 1)
                       + (board[i + 1][    0] & 1)
                       + (board[i - 1][    1] & 1)
                       + (board[i    ][    1] & 1)
                       + (board[i + 1][    1] & 1));
            update(i, n, (board[i - 1][n    ] & 1)
                       + (board[i + 1][n    ] & 1)
                       + (board[i - 1][n - 1] & 1)
                       + (board[i    ][n - 1] & 1)
                       + (board[i + 1][n - 1] & 1));
        }
        // Center of the grid.
        for (int i = 1; i < m; ++i)
            for (int j = 1; j < n; ++j)
                update(i, j, (board[i    ][j - 1] & 1)
                           + (board[i    ][j + 1] & 1)
                           + (board[i - 1][j - 1] & 1)
                           + (board[i - 1][j    ] & 1)
                           + (board[i - 1][j + 1] & 1)
                           + (board[i + 1][j - 1] & 1)
                           + (board[i + 1][j    ] & 1)
                           + (board[i + 1][j + 1] & 1));
    }
    for (int i = 0; i <= m; ++i)
        for (int j = 0; j <= n; ++j)
            board[i][j] >>= 1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > board,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = board;
        gameOfLife(result);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 0},
          {0, 0, 1},
          {1, 1, 1},
          {0, 0, 0}},
         {{0, 0, 0},
          {1, 0, 1},
          {0, 1, 1},
          {0, 1, 0}}, trials);
    test({{1, 1},
          {1, 0}},
         {{1, 1},
          {1, 1}}, trials);
    test({{0, 0}}, {{0, 0}}, trials);
    test({{1, 0}}, {{0, 0}}, trials);
    test({{1, 0, 1, 0, 1}}, {{0, 0, 0, 0, 0}}, trials);
    test({{1, 0, 1, 1, 1, 0, 1}}, {{0, 0, 0, 1, 0, 0, 0}}, trials);
    test({{0}, {0}}, {{0}, {0}}, trials);
    test({{1}, {0}}, {{0}, {0}}, trials);
    test({{1}, {0}, {1}, {0}, {1}}, {{0}, {0}, {0}, {0}, {0}}, trials);
    test({{1}, {0}, {1}, {1}, {1}, {0}, {1}}, {{0}, {0}, {0}, {1}, {0}, {0}, {0}}, trials);
    return 0;
}


