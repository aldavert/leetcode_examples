#include "../common/common.hpp"
#include <cmath>

#if 1
int countPrimes(int n)
{
    if (n < 3) return 0;
    std::vector<bool> primes(n / 2, true);
    const int max_n = static_cast<int>(std::ceil(std::sqrt(n)));
    int number_of_primes = n / 2;
    for (int i = 3; i <= max_n; i += 2)
    {
        const long li = static_cast<long>(i);
        if (primes[i / 2])
            for (long j = li * li; j < n; j += li * 2)
                if (primes[j / 2])
                {
                    primes[j / 2] = false;
                    --number_of_primes;
                }
    }
    return number_of_primes;
}
#elif 0
int countPrimes(int n)
{
    std::vector<bool> primes(n + 1, true);
    const int max_n = static_cast<int>(ceil(std::sqrt(n)));
    for (int i = 2; i <= max_n; ++i)
    {
        const long li = static_cast<long>(i);
        if (primes[i] && (li * li <= n))
        {
            for (long j = li * li; j <= n; j += li)
                primes[j] = false;
        }
    }
    int number_of_primes = 0;
    for (int i = 2; i < n; ++i)
        number_of_primes += primes[i];
    return number_of_primes;
}
#else
int countPrimes(int n)
{
    const int maximum_primes_stored = 500;
    int primes[maximum_primes_stored] = {2, 3, 5, 7};
    long squared_primes[maximum_primes_stored]  = {4, 9, 25, 49};
    if      (n <= 2) return 0;
    else if (n <= 3) return 1;
    else if (n <= 5) return 2;
    else if (n <= 7) return 3;
    int number_of_primes = 4;
    for (int i = 11, m = 3; i < n; i += 2, ++m)
    {
        if (m % 5 == 0) continue;
        long li = static_cast<long>(i);
        bool prime = true;
        for (int j = 0; j < std::min(maximum_primes_stored, number_of_primes); ++j)
        {
            if (squared_primes[j] > li)
                break;
            if (i % primes[j] == 0)
            {
                prime = false;
                break;
            }
        }
        if (prime)
        {
            if (number_of_primes < maximum_primes_stored)
            {
                primes[number_of_primes] = i;
                squared_primes[number_of_primes] = li * li;
            }
            ++number_of_primes;
        }
    }
    
    return number_of_primes;
}
#endif

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPrimes(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 100;
#else
    constexpr unsigned int trials = 1;
#endif
    test(0, 0, trials);
    test(1, 0, trials);
    test(        10,       4, trials);
    test(       100,      25, trials);
    test(     1'000,     168, trials);
    test(    10'000,   1'229, trials);
    test(   100'000,   9'592, trials);
    test( 1'000'000,  78'498, trials);
    test(10'000'000, 664'579, trials);
    return 0;
}


