#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string decodeAtIndex(std::string s, int k)
{
    long size = 0, kl = k;
    for (char c : s)
        size = (std::isdigit(c))?(size * (c - '0')):size + 1;
    for (int i = static_cast<int>(s.size()) - 1; i >= 0; --i)
    {
        kl %= size;
        if ((kl == 0) && std::isalpha(s[i])) return std::string(1, s[i]);
        size = (std::isdigit(s[i]))?(size / (s[i] - '0')):size - 1;
    }
    return "";
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, std::string solution, unsigned int trials = 1)
{
    std::string result = "";
    for (unsigned int i = 0; i < trials; ++i)
        result = decodeAtIndex(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("leet2code3", 10, "o", trials);
    test("ha22", 5, "h", trials);
    test("a2345678999999999999999", 10, "a", trials);
    return 0;
}


