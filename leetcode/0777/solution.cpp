#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canTransform(std::string start, std::string end)
{
    if (start.size() != end.size()) return false;
    const size_t n = start.size();
    size_t s = 0, e = 0;
    while ((s < n) && (start[s] == 'X')) ++s;
    while ((e < n) && (end[e] == 'X')) ++e;
    while ((s < n) && (e < n))
    {
        if ( (start[s] != end[e])
        ||  ((start[s] == 'L') && (s < e))
        ||  ((start[s] == 'R') && (s > e)))
            return false;
        ++s;
        ++e;
        while ((s < n) && (start[s] == 'X')) ++s;
        while ((e < n) && (end[e] == 'X')) ++e;
    }
    return s == e;
}

// ############################################################################
// ############################################################################

void test(std::string start,
          std::string end,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canTransform(start, end);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("RXXLRXRXL", "XRLXXRRLX", true, trials);
    test("X", "L", false, trials);
    test("LXXLXRLXXL", "XLLXRXLXLX", false, trials);
    return 0;
}


