#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximizeSquareHoleArea([[maybe_unused]] int n,
                           [[maybe_unused]] int m,
                           std::vector<int> hBars,
                           std::vector<int> vBars)
{
    auto maxGap = [](std::vector<int> &bars) -> int
    {
        int result = 2, running_gap = 2;
        std::sort(bars.begin(), bars.end());
        for (size_t i = 1; i < bars.size(); ++i)
        {
            running_gap = (bars[i] == bars[i - 1] + 1)?running_gap + 1:2;
            result = std::max(result, running_gap);
        }
        return result;
    };
    const int gap = std::min(maxGap(hBars), maxGap(vBars));
    return gap * gap;
}

// ############################################################################
// ############################################################################

void test(int n,
          int m,
          std::vector<int> hBars,
          std::vector<int> vBars,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximizeSquareHoleArea(n, m, hBars, vBars);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 1, {2, 3}, {2}, 4, trials);
    test(1, 1, {2}, {2}, 4, trials);
    test(2, 3, {2, 3}, {2, 4}, 4, trials);
    return 0;
}


