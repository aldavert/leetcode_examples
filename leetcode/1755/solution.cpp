#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int minAbsDifference(std::vector<int> nums, int goal)
{
    auto getSums = [](std::vector<int> a)
    {
        const int m = 1 << a.size();
        std::vector<int> res(m, 0);
        for(int mask = 1; mask < m; ++mask)
            res[mask] = res[mask & (mask - 1)] + a[__builtin_ctz(mask)];
        std::sort(res.begin(), res.end());
        return res;
    };
    const int n = static_cast<int>(nums.size());
    if (n == 1) return std::min(std::abs(goal), std::abs(goal - nums[0]));
    std::vector<int> l = getSums(std::vector<int>(nums.begin(), nums.begin() + n / 2)),
                     r = getSums(std::vector<int>(nums.begin() + n / 2, nums.end()));
    int at = static_cast<int>(r.size()) - 1;
    int res = std::abs(goal);
    for (auto x : l)
    {
        while ((at >= 0) && (x + r[at] > goal)) --at;
        if (at >= 0)
            res = std::min(res, std::abs(goal - x - r[at]));
        if (at + 1 < static_cast<int>(r.size()))
            res = std::min(res, std::abs(goal - x - r[at + 1]));
    }

    return res;
}
#elif 0
int minAbsDifference(std::vector<int> nums, int goal)
{
    const int n = static_cast<int>(nums.size());
    std::vector<int> t1{0}, t2{0};
    t1.reserve(1 << (n / 2 + 1));
    t2.reserve(1 << (n / 2 + 1));
    
    for (int i = 0; i < n / 2; ++i)
        for (int j = static_cast<int>(t1.size()) - 1; j >= 0; --j)
            t1.push_back(t1[j] + nums[i]);
    for (int i = n / 2; i < n; ++i)
        for (int j = static_cast<int>(t2.size()) - 1; j >= 0; --j)
            t2.push_back(t2[j] + nums[i]);
    
    std::set<int> s2(t2.begin(), t2.end());
    int result = std::abs(goal);
    for (int x : std::unordered_set<int>(t1.begin(), t1.end()))
    {
        auto it = s2.lower_bound(goal - x);
        if (it != s2.end())
            result = std::min(result, abs(goal - x - *it));
        if (it != s2.begin())
            result = std::min(result, abs(goal - x - *prev(it)));
    }
    return result;
}
#else
int minAbsDifference(std::vector<int> nums, int goal)
{
    const int n = static_cast<int>(nums.size() / 2);
    const std::vector<int> l_nums(nums.begin(), nums.begin() + n);
    const std::vector<int> r_nums(nums.begin() + n, nums.end());
    std::vector<int> l_sums, r_sums;
    auto dfs = [&](auto &&self,
                   const std::vector<int> &in,
                   int i,
                   int path,
                   std::vector<int> &sums) -> void
    {
        if (i == static_cast<int>(in.size()))
        {
            sums.push_back(path);
            return;
        }
        self(self, in, i + 1, path + in[i], sums);
        self(self, in, i + 1, path, sums);
    };
    
    dfs(dfs, l_nums, 0, 0, l_sums);
    dfs(dfs, r_nums, 0, 0, r_sums);
    std::sort(r_sums.begin(), r_sums.end());
    
    int result = std::numeric_limits<int>::max();
    for (const int l_sum : l_sums)
    {
        int i = static_cast<int>(std::lower_bound(r_sums.begin(), r_sums.end(),
                                                  goal - l_sum) - begin(r_sums));
        if (i < static_cast<int>(r_sums.size()))
            result = std::min(result, std::abs(goal - l_sum - r_sums[i]));
        if (i > 0)
            result = std::min(result, std::abs(goal - l_sum - r_sums[i - 1]));
    }
    
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int goal, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minAbsDifference(nums, goal);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, -7, 3, 5}, 6, 0, trials);
    test({7, -9, 15, -2}, -5, 1, trials);
    test({1, 2, 3}, -7, 7, trials);
    test({0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 1000000000, 1000000000, trials);
    return 0;
}


