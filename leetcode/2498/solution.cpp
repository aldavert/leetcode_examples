#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxJump(std::vector<int> stones)
{
    int result = stones[1] - stones[0];
    for (size_t i = 2; i < stones.size(); ++i)
        result = std::max(result, stones[i] - stones[i - 2]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> stones, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxJump(stones);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 2, 5, 6, 7}, 5, trials);
    test({0, 3, 9}, 9, trials);
    return 0;
}


