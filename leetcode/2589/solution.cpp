#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

int findMinimumTime(std::vector<std::vector<int> > tasks)
{
    struct Info
    {
        int start = -1, end = -1, duration = -1;
        bool operator<(const Info &other) const { return end < other.end; };
    };
    std::bitset<2'001> running;
    std::vector<Info> information(tasks.size());
    for (size_t i = 0; i < tasks.size(); ++i)
        information[i] = {tasks[i][0], tasks[i][1], tasks[i][2]};
    std::sort(information.begin(), information.end());
    
    for (auto &info : information)
    {
        auto [start, end, duration] = info;
        int needed_duration = 0;
        for (int i = start; i <= end; ++i)
            needed_duration += running[i];
        needed_duration = duration - needed_duration;
        for (int i = end; needed_duration > 0; --i)
        {
            if (!running[i])
            {
                running[i] = true;
                --needed_duration;
            }
        }
    }
    return static_cast<int>(running.count());
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > tasks, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMinimumTime(tasks);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 3, 1}, {4, 5, 1}, {1, 5, 2}}, 2, trials);
    test({{1, 3, 2}, {2, 5, 3}, {5, 6, 2}}, 4, trials);
    return 0;
}


