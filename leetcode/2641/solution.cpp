#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * replaceValueInTree(TreeNode * root)
{
    int level_sums[100'001] = {}, max_level = 0;
    auto dfs = [&](auto &&self, TreeNode * node, int level) -> void
    {
        if (node == nullptr) return;
        max_level = std::max(max_level, level + 1);
        level_sums[level] += node->val;
        self(self, node->left , level + 1);
        self(self, node->right, level + 1);
    };
    auto replace =
        [&](auto &&self, TreeNode * node, int level, TreeNode * curr) -> TreeNode *
    {
        int next_level_cousins_sum = (level + 1 >= max_level)?0:
            level_sums[level + 1] - ((!node->left)?0:node->left->val)
            - ((!node->right)?0:node->right->val);
        if (node->left)
        {
            curr->left = new TreeNode(next_level_cousins_sum);
            self(self, node->left, level + 1, curr->left);
        }
        if (node->right)
        {
            curr->right = new TreeNode(next_level_cousins_sum);
            self(self, node->right, level + 1, curr->right);
        }
        return curr;
    };
    dfs(dfs, root, 0);
    return replace(replace, root, 0, new TreeNode(0));
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        TreeNode * node = replaceValueInTree(root);
        result = tree2vec(node);
        delete node;
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 9, 1, 10, null, 7}, {0, 0, 0, 7, 7, null, 11}, trials);
    test({3, 1, 2}, {0, 0, 0}, trials);
    return 0;
}


