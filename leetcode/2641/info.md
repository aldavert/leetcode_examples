# Cousins in Binary Tree II

Given the `root` of a binary tree, replace the value of each node in the tree with the **sum of all its cousins' values.**

Two nodes of a binary tree are **cousins** if they have the same depth with different parents.

Return *the* `root` *of the modified tree.*

**Note** that the depth of a node is the number of edges in the path from the root node to it.

#### Example 1:
> ```mermaid 
> flowchart LR;
> subgraph GA [ ]
> A1((5))---B1((4))
> A1---C1((9))
> B1---D1((1))
> B1---E1((10))
> C1---EMPTY1(( ))
> C1---F1((7))
> end
> subgraph GB [ ]
> A2((0))---B2((0))
> A2---C2((0))
> B2---D2((7))
> B2---E2((7))
> C2---EMPTY2(( ))
> C2---F2((11))
> end
> GA-->GB
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class EMPTY1,EMPTY2,GA,GB empty;
> linkStyle 4,10 stroke-width:0px;
> ```
> *Input:* `root = [5, 4, 9, 1, 10, null, 7]`  
> *Output:* `[0, 0, 0, 7, 7, null, 11]`  
> *Explanation:* The diagram above shows the initial binary tree and the binary tree after changing the value of each node.
> - Node with value `5` does not have any cousins so its sum is `0`.
> - Node with value `4` does not have any cousins so its sum is `0`.
> - Node with value `9` does not have any cousins so its sum is `0`.
> - Node with value `1` has a cousin with value `7` so its sum is `7`.
> - Node with value `10` has a cousin with value `7` so its sum is `7`.
> - Node with value `7` has cousins with values `1` and 10 so its sum is `11`.

#### Example 2:
> ```mermaid 
> flowchart LR;
> subgraph G1 [ ]
> A1((3))---B1((1))
> A1---C1((2))
> end
> subgraph G2 [ ]
> A2((0))---B2((0))
> A2---C2((0))
> end
> G1-->G2
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class G1,G2 empty;
> ```
> *Input:* `root = [3, 1, 2]`  
> *Output:* `[0, 0, 0]`  
> *Explanation:* The diagram above shows the initial binary tree and the binary tree after changing the value of each node.
> - Node with value `3` does not have any cousins so its sum is `0`.
> - Node with value `1` does not have any cousins so its sum is `0`.
> - Node with value `2` does not have any cousins so its sum is `0`.

#### Constraints:
- The number of nodes in the tree is in the range `[1, 10^5]`.
- `1 <= Node.val <= 10^4`


