#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int removeDuplicates(std::vector<int> &nums)
{
    const int n = static_cast<int>(nums.size());
    int k = 1;
    for (int i = 1; i < n; ++i)
        if (nums[i] != nums[i - 1])
            nums[k++] = nums[i];
    for (int i = n - k; i > 0; --i)
        nums.pop_back();
    return k;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    int ndifferent = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = nums;
        ndifferent = removeDuplicates(result);
    }
    showResult((k == ndifferent) && (solution == result), solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2}, 2, {1, 2}, trials);
    test({0, 0, 1, 1, 1, 2, 2, 3, 3, 4}, 5, {0, 1, 2, 3, 4}, trials);
    return 0;
}


