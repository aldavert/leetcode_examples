#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string lastNonEmptyString(std::string s)
{
    std::string result;
    int count[26] = {};
    
    for (char c : s) ++count[c - 'a'];
    int max_frequency = count[0];
    for (int i = 1; i < 26; ++i)
        max_frequency = std::max(max_frequency, count[i]);
    for (int i = static_cast<int>(s.size()) - 1; i >= 0; --i)
        if (count[s[i] - 'a']-- == max_frequency)
            result += s[i];
    std::reverse(result.begin(), result.end());
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = lastNonEmptyString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aabcbbca", "ba", trials);
    test("abcd", "abcd", trials);
    return 0;
}


