#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<long long> findXSum(std::vector<int> nums, int k, int x)
{
    std::multiset<std::pair<int, int> > high, low;
    std::unordered_map<int, int> histogram;
    long window_sum = 0;
    
    auto update = [&](int num, int freq) -> void
    {
        if (histogram[num] > 0)
        {
            if (auto it = low.find({histogram[num], num}); it != low.end())
                low.erase(it);
            else
            {
                it = high.find({histogram[num], num});
                high.erase(it);
                window_sum -= static_cast<long>(num) * histogram[num];
            }
        }
        histogram[num] += freq;
        if (histogram[num] > 0)
            low.insert({histogram[num], num});
    };
    auto compute = [&](void) -> void
    {
        while (!low.empty() && (static_cast<int>(high.size()) < x))
        {
            const auto [countB, b] = *low.rbegin();
            low.erase(--low.end());
            high.insert({countB, b});
            window_sum += static_cast<long>(b) * countB;
        }
        while (!low.empty() && *low.rbegin() > *high.begin())
        {
            const auto [countB, b] = *low.rbegin();
            const auto [countT, t] = *high.begin();
            low.erase(--low.end());
            high.erase(high.begin());
            low.insert({countT, t});
            high.insert({countB, b});
            window_sum += static_cast<long>(b) * countB;
            window_sum -= static_cast<long>(t) * countT;
        }
    };
    const int n = static_cast<int>(nums.size());
    std::vector<long long> result(n - k + 1);
    for (int i = 0; i < k; ++i)
        update(nums[i], 1);
    compute();
    result[0] = window_sum;
    for (int i = k; i < n; ++i)
    {
        update(nums[i], 1);
        update(nums[i - k], -1);
        compute();
        result[i - k + 1] = window_sum;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int k,
          int x,
          std::vector<long long> solution,
          unsigned int trials = 1)
{
    std::vector<long long> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findXSum(nums, k, x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2, 2, 3, 4, 2, 3}, 6, 2, {6, 10, 12}, trials);
    test({3, 8, 7, 8, 7, 5}, 2, 2, {11, 15, 15, 15, 12}, trials);
    return 0;
}


