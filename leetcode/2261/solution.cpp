#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countDistinct(std::vector<int> nums, int k, int p)
{
    const int n = static_cast<int>(nums.size());
    std::unordered_set<std::string> s;
    for (int i = 0; i < n; ++i)
    {
        std::string t;
        for (int j = i, count = 0; j < n; ++j)
        {
            if ((nums[j] % p == 0) && (++count > k))
                break;
            t += std::to_string(nums[j]) + ",";
            s.insert(t);
        }
    }
    return static_cast<int>(s.size());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int p, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countDistinct(nums, k, p);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 3, 2, 2}, 2, 2, 11, trials);
    test({1, 2, 3, 4}, 4, 1, 10, trials);
    return 0;
}


