#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximizeGreatness(std::vector<int> nums)
{
    int result = 0;
    std::sort(nums.begin(), nums.end());
    for (int num : nums)
        result += (num > nums[result]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximizeGreatness(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 5, 2, 1, 3, 1}, 4, trials);
    test({1, 2, 3, 4}, 3, trials);
    return 0;
}


