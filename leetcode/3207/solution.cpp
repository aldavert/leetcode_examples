#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

long long maximumPoints(std::vector<int> enemyEnergies, int currentEnergy)
{
    int min_energy = *std::min_element(enemyEnergies.begin(), enemyEnergies.end());
    return (currentEnergy < min_energy)
         ? 0
         : (currentEnergy - min_energy
         + std::accumulate(enemyEnergies.begin(), enemyEnergies.end(), 0LL))
         / min_energy;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> enemyEnergies,
          int currentEnergy,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumPoints(enemyEnergies, currentEnergy);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 2}, 2, 3, trials);
    test({2}, 10, 5, trials);
    return 0;
}


