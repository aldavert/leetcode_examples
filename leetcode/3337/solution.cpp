#include "../common/common.hpp"
#include <cstring>
#include <numeric>

// ############################################################################
// ############################################################################

int lengthAfterTransformations(std::string s, int t, std::vector<int> nums)
{
    constexpr int MOD = 1'000'000'007;
    int transform[26][26] = {}, powered_transform[26][26] = {};
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        for (int step = 1; step <= nums[i]; ++step)
            ++transform[i][(i + step) % 26];
    auto matrixPow = [&](auto &&self, int n) -> void
    {
        if (n == 0)
        {
            std::memset(powered_transform, 0, sizeof(powered_transform));
            for (int i = 0; i < 26; ++i)
                powered_transform[i][i] = 1;
        }
        else if (n % 2 == 1)
        {
            int a[26][26], b[26][26];
            std::memcpy(a, transform, sizeof(transform));
            self(self, n - 1);
            std::memcpy(b, powered_transform, sizeof(b));
            std::memset(powered_transform, 0, sizeof(powered_transform));
            for (int i = 0; i < 26; ++i)
                for (int j = 0; j < 26; ++j)
                    for (int k = 0; k < 26; ++k)
                        powered_transform[i][j] = (powered_transform[i][j]
                                                + a[i][k] * b[k][j]) % MOD;
        }
        else
        {
            int copy[26][26];
            std::memcpy(copy, transform, sizeof(transform));
            std::memset(transform, 0, sizeof(transform));
            for (int i = 0; i < 26; ++i)
                for (int j = 0; j < 26; ++j)
                    for (int k = 0; k < 26; ++k)
                        transform[i][j] = (transform[i][j]
                                        + copy[i][k] * copy[k][j]) % MOD;
            self(self, n / 2);
        }
    };
    matrixPow(matrixPow, t);
    
    long count[26] = {}, lengths[26] = {};
    for (char c : s) ++count[c - 'a'];
    for (int i = 0; i < 26; ++i)
        for (int j = 0; j < 26; ++j)
            lengths[j] = (lengths[j] + count[i] * powered_transform[i][j]) % MOD;
    return static_cast<int>(std::accumulate(&lengths[0], &lengths[26], 0L) % MOD);
}

// ############################################################################
// ############################################################################

void test(std::string s,
          int t,
          std::vector<int> nums,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = lengthAfterTransformations(s, t, nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcyy", 2, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 2}, 7, trials);
    test("azbk", 1, {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
         2, 2, 2, 2, 2, 2}, 8, trials);
    return 0;
}


