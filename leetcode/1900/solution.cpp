#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> earliestAndLatest(int n, int firstPlayer, int secondPlayer)
{
    std::vector<std::vector<std::vector<std::pair<int, int> > > > dp(n + 1,
            std::vector<std::vector<std::pair<int, int> > >(n + 1,
            std::vector<std::pair<int, int> >(n + 1)));
    auto solve = [&](auto &&self, int l, int r, int k) -> std::pair<int, int>
    {
        if (l == r) return {1, 1};
        if (l > r) std::swap(l, r);
        if (dp[l][r][k] != std::pair<int, int>(0, 0)) return dp[l][r][k];
        
        int a = std::numeric_limits<int>::max();
        int b = std::numeric_limits<int>::lowest();
        for (int i = 1; i <= l; ++i)
        {
            for (int j = l - i + 1; j <= r - i; ++j)
            {
                if ((i + j > (k + 1) / 2) || (i + j < l + r - k / 2)) continue;
                auto [x, y] = self(self, i, j, (k + 1) / 2);
                a = std::min(a, x + 1);
                b = std::max(b, y + 1);
            }
        }
        
        return dp[l][r][k] = {a, b};
    };
    auto [a, b] = solve(solve, firstPlayer, n - secondPlayer + 1, n);
    return {a, b};
}

// ############################################################################
// ############################################################################

void test(int n,
          int firstPlayer,
          int secondPlayer,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = earliestAndLatest(n, firstPlayer, secondPlayer);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(11, 2, 4, {3, 4}, trials);
    test(5, 1, 5, {1, 1}, trials);
    return 0;
}


