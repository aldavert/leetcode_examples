#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<int> minOperationsQueries(int n,
                                      std::vector<std::vector<int> > edges,
                                      std::vector<std::vector<int> > queries)
{
    int m = 32 - std::countl_zero(static_cast<unsigned int>(n));
    std::vector<std::vector<std::pair<int, int> > > g(n);
    std::vector<std::vector<int> > f(n, std::vector<int>(m)),
                                   cnt(n, std::vector<int>(26));
    std::vector<int> p(n), depth(n);
    for (const auto &e : edges)
    {
        g[e[0]].emplace_back(e[1], e[2] - 1);
        g[e[1]].emplace_back(e[0], e[2] - 1);
    }
    std::queue<int> q;
    q.push(0);
    while (!q.empty())
    {
        int i = q.front();
        q.pop();
        f[i][0] = p[i];
        for (int j = 1; j < m; ++j)
            f[i][j] = f[f[i][j - 1]][j - 1];
        for (auto& [j, w] : g[i])
        {
            if (j != p[i])
            {
                p[j] = i;
                cnt[j] = cnt[i];
                ++cnt[j][w];
                depth[j] = depth[i] + 1;
                q.push(j);
            }
        }
    }
    std::vector<int> result;
    for (auto& qq : queries)
    {
        int u = qq[0], v = qq[1], x = u, y = v;
        if (depth[x] < depth[y]) std::swap(x, y);
        for (int j = m - 1; ~j; --j)
            if (depth[x] - depth[y] >= (1 << j))
                x = f[x][j];
        for (int j = m - 1; ~j; --j)
        {
            if (f[x][j] != f[y][j])
            {
                x = f[x][j];
                y = f[y][j];
            }
        }
        if (x != y) x = p[x];
        int mx = 0;
        for (int j = 0; j < 26; ++j)
            mx = std::max(mx, cnt[u][j] + cnt[v][j] - 2 * cnt[x][j]);
        result.push_back(depth[u] + depth[v] - 2 * depth[x] - mx);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperationsQueries(n, edges, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(7, {{0, 1, 1}, {1, 2, 1}, {2, 3, 1}, {3, 4, 2}, {4, 5, 2}, {5, 6, 2}},
         {{0, 3}, {3, 6}, {2, 6}, {0, 6}}, {0, 0, 1, 3}, trials);
    test(8, {{1, 2, 6}, {1, 3, 4}, {2, 4, 6}, {2, 5, 3}, {3, 6, 6}, {3, 0, 8},
         {7, 0, 2}}, {{4, 6}, {0, 4}, {6, 5}, {7, 4}}, {1, 2, 2, 3}, trials);
    return 0;
}


