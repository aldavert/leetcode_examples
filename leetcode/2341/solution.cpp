#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> numberOfPairs(std::vector<int> nums)
{
    int lut[101] = {}, pair = 0, residual = 0;
    for (size_t i = 0, n = nums.size(); i < n; ++i)
    {
        const int value = nums[i];
        residual += (lut[value] == 0) - (lut[value] == 1);
        pair += lut[value] == 1;
        lut[value] += (lut[value] == 0) - (lut[value] == 1); 
    }
    return {pair, residual};
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfPairs(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 1, 3, 2, 2}, {3, 1}, trials);
    test({1, 1}, {1, 0}, trials);
    test({0}, {0, 1}, trials);
    return 0;
}


