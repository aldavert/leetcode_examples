#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string shiftingLetters(std::string s, std::vector<std::vector<int> > shifts)
{
    std::vector<int> timeline(s.length() + 1);
    for (const auto &shift : shifts)
    {
        const int diff = 2 * shift[2] - 1;
        timeline[shift[0]] += diff;
        timeline[shift[1] + 1] -= diff;
    }
    for (int i = 0, curr_shift = 0, n = static_cast<int>(s.size()); i < n; ++i)
    {
        curr_shift = (curr_shift + timeline[i]) % 26;
        s[i] = static_cast<char>('a' + (s[i] - 'a' + curr_shift + 26) % 26);
    }
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<std::vector<int> > shifts,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = shiftingLetters(s, shifts);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", {{0, 1, 0}, {1, 2, 1}, {0, 2, 1}}, "ace", trials);
    test("dztz", {{0, 0, 0}, {1, 1, 1}}, "catz", trials);
    return 0;
}


