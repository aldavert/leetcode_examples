#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfChild(int n, int k)
{
    int round_time = 2 * (n - 1), pos = k % round_time;
    return (pos < n)?pos:round_time - pos;
}

// ############################################################################
// ############################################################################

void test(int n, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfChild(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 5, 1, trials);
    test(5, 6, 2, trials);
    test(4, 2, 2, trials);
    return 0;
}


