#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > sortMatrix(std::vector<std::vector<int> > grid)
{
    const int n = static_cast<int>(grid.size());
    std::vector<std::vector<int> > result(n, std::vector<int>(n));
    std::vector<int> row;
    row.reserve(n);
    for (int i = 0; i < n; ++i)
    {
        row.clear();
        for (int j = 0; j < n - i; ++j)
            row.push_back(grid[j + i][j]);
        std::sort(row.rbegin(), row.rend());
        for (int j = 0; j < n - i; ++j)
            result[j + i][j] = row[j];
    }
    for (int i = 1; i < n; ++i)
    {
        row.clear();
        for (int j = 0; j < n - i; ++j)
            row.push_back(grid[j][j + i]);
        std::sort(row.begin(), row.end());
        for (int j = 0; j < n - i; ++j)
            result[j][j + i] = row[j];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sortMatrix(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 7, 3}, {9, 8, 2}, {4, 5, 6}},
         {{8, 2, 3}, {9, 6, 7}, {4, 5, 1}}, trials);
    test({{0, 1}, {1, 2}}, {{2, 1}, {1, 0}}, trials);
    test({{1}}, {{1}}, trials);
    return 0;
}


