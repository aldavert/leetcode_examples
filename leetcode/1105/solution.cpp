#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minHeightShelves(std::vector<std::vector<int> > books, int shelfWidth)
{
    const int n = static_cast<int>(books.size());
    std::vector<int> dp(n + 1, std::numeric_limits<int>::max());
    dp[0] = 0;
    for (int i = 0; i < n; ++i)
    {
        int sum_thickness = 0, max_height = 0;
        for (int j = i; j >= 0; --j)
        {
            int thickness = books[j][0], height = books[j][1];
            sum_thickness += thickness;
            if (sum_thickness > shelfWidth) break;
            max_height = std::max(max_height, height);
            dp[i + 1] = std::min(dp[i + 1], dp[j] + max_height);
        }
    }
    return dp.back();
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > books,
          int shelfWidth,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minHeightShelves(books, shelfWidth);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1}, {2, 3}, {2, 3}, {1, 1}, {1, 1}, {1, 1}, {1, 2}}, 4, 6, trials);
    test({{1, 3}, {2, 4}, {3, 2}}, 6, 4, trials);
    return 0;
}


