#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

std::string robotWithString(std::string s)
{
    std::string result;
    int count[26] = {};
    std::stack<char> stk;
    auto minChar = [&](void) -> char
    {
        for (int i = 0; i < 26; ++i)
            if (count[i])
                return static_cast<char>('a' + i);
        return 'a';
    };
    
    for (char c : s) ++count[c - 'a'];
    for (char c : s)
    {
        stk.push(c);
        --count[c - 'a'];
        for (char min_char = minChar(); !stk.empty() && (stk.top() <= min_char); )
            result += stk.top(),
            stk.pop();
    }
    while (!stk.empty())
        result += stk.top(),
        stk.pop();
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = robotWithString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("zza", "azz", trials);
    test("bac", "abc", trials);
    test("bdda", "addb", trials);
    return 0;
}


