#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<long long> placedCoins(std::vector<std::vector<int> > edges,
                                   std::vector<int> cost)
{
    class ChildCost
    {
        int m_num_nodes;
        std::vector<long> m_positive;
        std::vector<long> m_negative;
    public:
        ChildCost(int cost) : m_num_nodes(1)
        {
            if (cost > 0) m_positive.push_back(cost);
            else m_negative.push_back(cost);
        }
        void update(const ChildCost &other)
        {
            m_num_nodes += other.m_num_nodes;
            std::copy(other.m_positive.begin(), other.m_positive.end(),
                      std::back_inserter(m_positive));
            std::copy(other.m_negative.begin(), other.m_negative.end(),
                      std::back_inserter(m_negative));
            std::sort(m_positive.begin(), m_positive.end(), std::greater<>());
            std::sort(m_negative.begin(), m_negative.end());
            m_positive.resize(std::min(static_cast<int>(m_positive.size()), 3));
            m_negative.resize(std::min(static_cast<int>(m_negative.size()), 2));
        }
        long maxProduct(void)
        {
            if (m_num_nodes < 3) return 1;
            if (m_positive.empty()) return 0;
            long product = 0;
            if (m_positive.size() == 3)
                product = m_positive[0] * m_positive[1] * m_positive[2];
            if (m_negative.size() == 2)
                product = std::max(product, m_negative[0] * m_negative[1]
                                          * m_positive[0]);
            return product;
        }
    };
    
    const int n = static_cast<int>(cost.size());
    std::vector<long long> result(n);
    std::vector<std::vector<int> > tree(n);
    auto dfs = [&](auto &&self, int u, int prev) -> ChildCost
    {
        ChildCost current(cost[u]);
        for (const int v : tree[u])
            if (v != prev)
                current.update(self(self, v, u));
        result[u] = current.maxProduct();
        return current;
    };
    
    for (const auto &edge : edges)
    {
        tree[edge[0]].push_back(edge[1]);
        tree[edge[1]].push_back(edge[0]);
    }
    dfs(dfs, 0, -1);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges,
          std::vector<int> cost,
          std::vector<long long> solution, unsigned int trials = 1)
{
    std::vector<long long> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = placedCoins(edges, cost);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}},
         {1, 2, 3, 4, 5, 6}, {120, 1, 1, 1, 1, 1}, trials);
    test({{0, 1}, {0, 2}, {1, 3}, {1, 4}, {1, 5}, {2, 6}, {2, 7}, {2, 8}},
         {1, 4, 2, 3, 5, 7, 8, -4, 2}, {280, 140, 32, 1, 1, 1, 1, 1, 1}, trials);
    test({{0, 1}, {0, 2}}, {1, 2, -2}, {0, 1, 1}, trials);
    return 0;
}


