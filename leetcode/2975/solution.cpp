#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
int maximizeSquareArea(int m, int n, std::vector<int> hFences, std::vector<int> vFences)
{
    constexpr long MOD = 1'000'000'007;
    auto calcSides =
        [](const std::vector<int> &fences, int max_side) -> std::priority_queue<int>
    {
        const int fs = static_cast<int>(fences.size());
        std::priority_queue<int> sides;
        for (int i = 0; i < fs - 1; ++i)
            for (int j = i + 1; j < fs; ++j)
                if (int side = std::abs(fences[j] - fences[i]); side <= max_side)
                    sides.emplace(side);
        return sides;
    };
    if (m == n)
        return static_cast<int>(static_cast<long>(m - 1) * (n - 1) % MOD);
    hFences.push_back(1);
    hFences.push_back(m);
    vFences.push_back(1);
    vFences.push_back(n);
    std::priority_queue<int> h_sides = calcSides(hFences, std::min(m, n) - 1),
                             v_sides = calcSides(vFences, std::min(m, n) - 1);
    while (!h_sides.empty() && !v_sides.empty())
    {
        long h_top = h_sides.top(), v_top = v_sides.top();
        if (h_top == v_top)
            return static_cast<int>(h_top * v_top % MOD);
        if (h_top > v_top) h_sides.pop();
        else v_sides.pop();
    }
    return -1;
}
#else
int maximizeSquareArea(int m, int n, std::vector<int> hFences, std::vector<int> vFences)
{
    constexpr long MOD = 1'000'000'007;
    hFences.push_back(1);
    hFences.push_back(m);
    vFences.push_back(1);
    vFences.push_back(n);
    std::sort(hFences.begin(), hFences.end());
    std::sort(vFences.begin(), vFences.end());
    auto calcGaps =[](const std::vector<int> &fences) -> std::unordered_set<long>
    {
        std::unordered_set<long> gaps;
        for (int i = 0, end = static_cast<int>(fences.size()); i < end; ++i)
            for (int j = 0; j < i; ++j)
                gaps.insert(fences[i] - fences[j]);
        return gaps;
    };
    
    const std::unordered_set<long> h_gaps = calcGaps(hFences);
    const std::unordered_set<long> v_gaps = calcGaps(vFences);
    long max_gap = -1;
    for (long h_gap : h_gaps)
        if (v_gaps.contains(h_gap))
            max_gap = std::max(max_gap, h_gap);
    return (max_gap == -1)?-1:static_cast<int>(max_gap * max_gap % MOD);
}
#endif

// ############################################################################
// ############################################################################

void test(int m,
          int n,
          std::vector<int> hFences,
          std::vector<int> vFences,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximizeSquareArea(m, n, hFences, vFences);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 3, {2, 3}, {2}, 4, trials);
    test(6, 7, {2}, {4}, -1, trials);
    return 0;
}


