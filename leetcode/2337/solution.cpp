#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool canChange(std::string start, std::string target)
{
    for (int i = 0, j = 0, n = static_cast<int>(start.size());
         (i <= n) && (j <= n); ++i, ++j)
    {
        while ((i < n) && (start[i]  == '_')) ++i;
        while ((j < n) && (target[j] == '_')) ++j;
        if ((i == n) || (j == n)) return (i == n) && (j == n);
        if (start[i] != target[j]) return false;
        if ((start[i] == 'R') && (i > j)) return false;
        if ((start[i] == 'L') && (i < j)) return false;
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string start, std::string target, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = canChange(start, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("_L__R__R_", "L______RR", true, trials);
    test("R_L_", "__LR", false, trials);
    test("_R", "R_", false, trials);
    return 0;
}


