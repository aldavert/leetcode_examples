#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumPossibleSum(int n, int target)
{
    constexpr int MOD = 1'000'000'007;
    const long mid = target / 2, k = target, m = n;
    if (m <= mid) return static_cast<int>(((m + 1) * m / 2) % MOD);
    return static_cast<int>(((mid + 1) * mid / 2
            + (2 * k + m - mid - 1) * (m - mid) / 2) % MOD);
}

// ############################################################################
// ############################################################################

void test(int n, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumPossibleSum(n, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 3, 4, trials);
    test(3, 3, 8, trials);
    test(1, 1, 1, trials);
    return 0;
}


