#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

std::string minWindow(std::string s, std::string t)
{
    int length = std::numeric_limits<int>::max(), begin = 0, previous = 0,
        count = static_cast<int>(t.size());
    const int n = static_cast<int>(s.size());
    int histogram[128] = {};
    for (char c: t) ++histogram[static_cast<int>(c)];
    for (int i = 0; i < n; ++i)
    {
        if (histogram[static_cast<int>(s[i])] > 0)
            --count;
        --histogram[static_cast<int>(s[i])];
        while (!count && (i >= previous))
        {
            if (i - previous + 1 < length)
            {
                length = i - previous + 1;
                begin = previous;
            }
            ++histogram[static_cast<int>(s[previous])];
            if (histogram[static_cast<int>(s[previous++])] > 0)
                count++;
        }
    }
    return (length == std::numeric_limits<int>::max())?"":s.substr(begin, length);
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string t, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minWindow(s, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ADOBECODEBANC", "ABC", "BANC", trials);
    test("a", "a", "a", trials);
    test("a", "aa", "", trials);
    return 0;
}


