#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumOperations(std::vector<int> nums)
{
    int dp[4] = {};
    for (int num : nums)
    {
        ++dp[num];
        dp[2] = std::max(dp[2], dp[1]);
        dp[3] = std::max(dp[3], dp[2]);
    }
    return static_cast<int>(nums.size()) - dp[3];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 1, 3, 2, 1}, 3, trials);
    test({1, 3, 2, 1, 3, 3}, 2, trials);
    test({2, 2, 2, 2, 3, 3}, 0, trials);
    return 0;
}


