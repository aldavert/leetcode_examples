# Merge in Between Linked Lists

You are given two linked lists: `list1` and `list2` of sizes `n` and `m` respectively.

Remove `list1`'s nodes from the `a^{th}` node to the `b^{th}` node, and put `list2` in their place.

The blue edges and nodes in the following figure indicate the result:
```mermaid 
flowchart LR;
subgraph list1
A[list1]-->B((0))
B-- ... -->D((a - 1))
D-->I((a))
subgraph remove
I-- ... -->J((b))
end
J-->K((b + 1))
K-- ... -->M((n - 1))
end
subgraph list2
E[list2]-->F((0))
F-- ... -->H((m - 1))
H-->K
D-->F
end
classDef default fill:#CCF,stroke:#000,stroke-width:2px;
classDef red fill:#FAA,stroke:#A77,stroke-width:2px;
class I,J,remove red;
classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px,color:#FFF;
class list1,list2 empty;
classDef white fill:#FFF,stroke:#FFF,stroke-width:0px;
class A,E white;
```
*Build the result list and return its head.*

#### Example 1:
> ```mermaid 
> flowchart LR;
> subgraph list1
> A[list1]-->B((10))
> B-->B1((1))
> B1-->D((13))
> D-->I((6))
> subgraph remove
> I-->J((9))
> end
> J-->K((5))
> end
> subgraph list2
> E[list2]-->F((1000000))
> F-->F2((1000001))
> F2-->H((1000002))
> H-->K
> D-->F
> end
> classDef default fill:#CCF,stroke:#000,stroke-width:2px;
> classDef red fill:#FAA,stroke:#A77,stroke-width:2px;
> class I,J,remove red;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px,color:#FFF;
> class list1,list2 empty;
> classDef white fill:#FFF,stroke:#FFF,stroke-width:0px;
> class A,E white;
> ```
> *Input:* `list1 = [10, 1, 13, 6, 9, 5], a = 3, b = 4, list2 = [1000000, 1000001, 1000002]`  
> *Output:* `[10, 1, 13, 1000000, 1000001, 1000002, 5]`  
> *Explanation:* We remove the nodes `3` and `4` and put the entire `list2` in their place. The blue edges and nodes in the above figure indicate the result.

#### Example 2:
> ```mermaid 
> flowchart LR;
> subgraph list1
> A[list1]-->B((0))
> B-->D((1))
> D-->I((2))
> subgraph remove
> I-->J1((3))
> J1-->J2((4))
> J2-->J((5))
> end
> J-->K((6))
> end
> subgraph list2
> E[list2]-->F((1000000))
> F-->F2((1000001))
> F2-->F3((1000002))
> F3-->F4((1000003))
> F4-->H((1000004))
> H-->K
> D-->F
> end
> classDef default fill:#CCF, stroke:#000, stroke-width:2px;
> classDef red fill:#FAA,stroke:#A77,stroke-width:2px;
> class I,J,J1,J2,remove red;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px,color:#FFF;
> class list1,list2 empty;
> classDef white fill:#FFF,stroke:#FFF,stroke-width:0px;
> class A,E white;
> ```
> *Input:* `list1 = [0, 1, 2, 3, 4, 5, 6], a = 2, b = 5, list2 = [1000000, 1000001, 1000002, 1000003, 1000004]`  
> *Output:* `[0, 1, 1000000, 1000001, 1000002, 1000003, 1000004, 6]`  
> *Explanation:* The blue edges and nodes in the above figure indicate the result.

#### Constraints:
- `3 <= list1.length <= 10^4`
- `1 <= a <= b < list1.length - 1`
- `1 <= list2.length <= 10^4`


