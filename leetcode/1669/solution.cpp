#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

ListNode * mergeInBetween(ListNode * list1, int a, int b, ListNode * list2)
{
    ListNode * last2 = list2;
    while (last2->next) last2 = last2->next;
    ListNode * ptr = list1;
    for (int i = 0; i < a - 1; ++i) ptr = ptr->next;
    ListNode * remove = ptr->next;
    ptr->next = list2;
    ptr = remove;
    for (int i = a; i < b; ++i) ptr = ptr->next;
    last2->next = ptr->next;
    ptr->next = nullptr;
    while (remove)
    {
        ptr = remove->next;
        remove->next = nullptr;
        delete remove;
        remove = ptr;
    }
    return list1;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> vec1,
          int a,
          int b,
          std::vector<int> vec2,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        ListNode * list1 = vec2list(vec1);
        ListNode * list2 = vec2list(vec2);
        list1 = mergeInBetween(list1, a, b, list2);
        result = list2vec(list1);
        delete list1;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 1, 13, 6, 9, 5}, 3, 4, {1000000, 1000001, 1000002},
         {10, 1, 13, 1000000, 1000001, 1000002, 5}, trials);
    test({0, 1, 2, 3, 4, 5, 6}, 2, 5, {1000000, 1000001, 1000002, 1000003, 1000004},
         {0, 1, 1000000, 1000001, 1000002, 1000003, 1000004, 6}, trials);
    return 0;
}


