#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<bool>
areConnected(int n, int threshold, std::vector<std::vector<int> > queries)
{
    std::vector<int> index(n + 1);
    for (int i = 0; i < n; ++i) index[i] = i;
    auto find = [&](auto &&self, int u) -> int
    {
        return (index[u] == u)?u:index[u] = self(self, index[u]);
    };
    auto connect = [&](int u, int v) -> void
    {
        index[find(find, u)] = find(find, v);
    };
    for (int d = threshold + 1; d <= n / 2; ++d)
        for (int i = 2 * d; i <= n; i += d)
            connect(d, i);
    std::vector<bool> result;
    for (const auto &q : queries)
        result.push_back(find(find, q[0]) == find(find, q[1]));
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          int threshold,
          std::vector<std::vector<int> > queries,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = areConnected(n, threshold, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, 2, {{1, 4}, {2, 5}, {3, 6}}, {false, false, true}, trials);
    test(6, 0, {{4, 5}, {3, 4}, {3, 2}, {2, 6}, {1, 3}},
               {true, true, true, true, true}, trials);
    test(5, 1, {{4, 5}, {4, 5}, {3, 2}, {2, 3}, {3, 4}},
               {false, false, false, false, false}, trials);
    return 0;
}


