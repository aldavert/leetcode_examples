#include "../common/common.hpp"
#include <bitset>

// ############################################################################
// ############################################################################

std::string smallestSubsequence(std::string s)
{
    int histogram[128] = {};
    std::bitset<128> used;
    std::string result;
    
    for (char c : s) ++histogram[static_cast<int>(c)];
    for (char c : s)
    {
        int i = static_cast<int>(c);
        --histogram[i];
        if (used[i]) continue;
        while (!result.empty()
           && (result.back() > c)
           && (histogram[static_cast<int>(result.back())] > 0))
        {
            used[result.back()] = false;
            result.pop_back();
        }
        used[i] = true;
        result.push_back(c);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestSubsequence(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("bcabc", "abc", trials);
    test("cbacdcbc", "acdb", trials);
    return 0;
}


