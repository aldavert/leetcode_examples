#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> leftmostBuildingQueries(std::vector<int> heights,
                                         std::vector<std::vector<int> > queries)
{
    struct Info
    {
        int idx_query;
        int idx_alice;
        int idx_bob;
        bool operator<(const Info &other) const { return idx_bob > other.idx_bob; }
    };
    const int n = static_cast<int>(queries.size());
    auto indexedQueries = [&](void) -> std::vector<Info>
    {
        std::vector<Info> result;
        for (int i = 0; i < n; ++i)
            result.push_back({i,
                              std::min(queries[i][0], queries[i][1]),
                              std::max(queries[i][0], queries[i][1])});
        std::sort(result.begin(), result.end());
        return result;
    };
    std::vector<int> result(queries.size(), -1), s;
    auto cmp = [&](int a, int b) -> bool { return heights[a] < heights[b]; };
    
    int heights_index = static_cast<int>(heights.size()) - 1;
    for (const auto& [idx_query, idx_alice, idx_bob] : indexedQueries())
    {
        if ((idx_alice == idx_bob) || (heights[idx_alice] < heights[idx_bob]))
        {
            result[idx_query] = idx_bob;
            continue;
        }
        while (heights_index > idx_bob)
        {
            while (!s.empty() && (heights[s.back()] <= heights[heights_index]))
                s.pop_back();
            s.push_back(heights_index--);
        }
        if (const auto it = std::upper_bound(s.rbegin(), s.rend(), idx_alice, cmp);
            it != s.rend())
                result[idx_query] = *it;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> heights,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = leftmostBuildingQueries(heights, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({6, 4, 8, 5, 2, 7}, {{0, 1}, {0, 3}, {2, 4}, {3, 4}, {2, 2}},
         {2, 5, -1, 5, 2}, trials);
    test({5, 3, 8, 2, 6, 1, 4, 6}, {{0, 7}, {3, 5}, {5, 2}, {3, 0}, {1, 6}},
         {7, 6, -1, 4, 6}, trials);
    return 0;
}


