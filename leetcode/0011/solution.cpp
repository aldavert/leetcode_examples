#include "../common/common.hpp"
#include <vector>

// ############################################################################
// ############################################################################

int maxArea(std::vector<int> height)
{
    int max_height = 0;
    for (int left = 0, right = static_cast<int>(height.size() - 1); left < right;)
    {
        max_height = std::max(max_height,
                              std::min(height[left], height[right]) * (right - left));
        if (height[left] < height[right])
            ++left;
        else --right;
    }
    return max_height;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> height, int solution, unsigned int trials = 1)
{
    int result = 0;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxArea(height);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 8, 6, 2, 5, 4, 8, 3, 7}, 49, trials);
    test({1, 1}, 1, trials);
    return 0;
}


