#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> goodSubsetofBinaryMatrix(std::vector<std::vector<int> > grid)
{
    constexpr int MAXBIT = 32;
    const int n = static_cast<int>(grid.size()),
              m = static_cast<int>(grid[0].size());
    std::unordered_map<int, int> mask_to_index;
    auto createMask = [&](int row) -> int
    {
        int mask = 0;
        for (int i = 0; i < m; ++i)
            if (grid[row][i])
                mask |= 1 << i;
        return mask;
    };
    for (int i = 0; i < n; ++i)
    {
        int mask = createMask(i);
        if (mask == 0) return {i};
        for (int previous_mask = 1; previous_mask < MAXBIT; ++previous_mask)
            if (((mask & previous_mask) == 0)
            &&  (mask_to_index.find(previous_mask) != mask_to_index.end()))
                return {mask_to_index[previous_mask], i};
        mask_to_index[mask] = i;
    }
    return {};
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = goodSubsetofBinaryMatrix(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 1, 0}, {0, 0, 0, 1}, {1, 1, 1, 1}}, {0, 1}, trials);
    test({{0}}, {0}, trials);
    test({{1, 1, 1}, {1, 1, 1}}, {}, trials);
    return 0;
}


