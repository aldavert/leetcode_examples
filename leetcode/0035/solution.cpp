#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int searchInsert(std::vector<int> nums, int target)
{
    return static_cast<int>(std::lower_bound(nums.begin(), nums.end(), target)
                          - nums.begin());
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int target, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = searchInsert(nums, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 5, 6}, 5, 2, trials);
    test({1, 3, 5, 6}, 2, 1, trials);
    test({1, 3, 5, 6}, 7, 4, trials);
    test({1, 3, 5, 6}, 0, 0, trials);
    test({1}, 0, 0, trials);
    return 0;
}


