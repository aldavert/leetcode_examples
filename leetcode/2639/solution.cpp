#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findColumnWidth(std::vector<std::vector<int> > grid)
{
    auto length = [](int value) -> int
    {
        int result = value <= 0;
        for (value *= 2 * (value >= 0) - 1; value; value /= 10, ++result);
        return result;
    };
    const size_t n = grid.size();
    const size_t m = grid[0].size();
    std::vector<int> result(m);
    for (size_t i = 0; i < m; ++i)
        for (size_t j = 0; j < n; ++j)
            result[i] = std::max(result[i], length(grid[j][i]));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findColumnWidth(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1}, {22}, {333}}, {3}, trials);
    test({{-15, 1, 3}, {15, 7, 12}, {5, 6, -2}}, {3, 1, 2}, trials);
    return 0;
}


