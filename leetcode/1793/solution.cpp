#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumScore(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    int result = nums[k];
    for (int i = k, j = k, m = nums[k];
         j - i + 1 != n;
         result = std::max(result, m * (j - i + 1)))
    {
        int l =           i?nums[i - 1]:-1,
            r = (j + 1 < n)?nums[j + 1]:-1;
        m = std::min(m, nums[(l >= r)?--i:++j]);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumScore(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 3, 7, 4, 5}, 3, 15, trials);
    test({5, 5, 4, 5, 4, 1, 1, 1}, 0, 20, trials);
    return 0;
}


