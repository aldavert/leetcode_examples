#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> kthSmallestPrimeFraction(std::vector<int> arr, int k)
{
    int n = static_cast<int>(arr.size());
    for (double l = 0, r = 1; l < r; )
    {
        const double m = (l + r) / 2.0;
        int p = 0, q = 1, fractions_no_greater_than_m = 0;
        for (int i = 0, j = 1; i < n; ++i)
        {
            while ((j < n) && (arr[i] > m * arr[j]))
                ++j;
            if (j == n) break;
            fractions_no_greater_than_m += n - j;
            if (p * arr[j] < q * arr[i])
            {
                p = arr[i];
                q = arr[j];
            }
        }
        if (fractions_no_greater_than_m == k) return {p, q};
        if (fractions_no_greater_than_m > k) r = m;
        else l = m;
    }
    return {};
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = kthSmallestPrimeFraction(arr, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 5}, 3, {2, 5}, trials);
    test({1, 7}, 1, {1, 7}, trials);
    return 0;
}


