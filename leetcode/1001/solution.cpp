#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> gridIllumination(int n,
                                  std::vector<std::vector<int> > lamps,
                                  std::vector<std::vector<int> > queries)
{
    struct PairHash
    {
        size_t operator()(std::pair<int, int> p) const { return p.first ^ p.second; }
    };
    std::vector<int> result;
    enum illuminated { ROW = 0, COL = 1, DIAG = 2, ANTIDIAG = 3 };
    std::unordered_map<int, int> illuminated[4];
    std::unordered_set<std::pair<int, int>, PairHash> lamps_set;
    
    for (const auto &lamp : lamps)
    {
        if (lamps_set.insert({lamp[0], lamp[1]}).second)
        {
            ++illuminated[ROW][lamp[0]];
            ++illuminated[COL][lamp[1]];
            ++illuminated[DIAG][lamp[0] + lamp[1]];
            ++illuminated[ANTIDIAG][lamp[0] - lamp[1]];
        }
    }
    
    for (const auto &query : queries)
    {
        int i = query[0], j = query[1];
        if (illuminated[ROW][i] || illuminated[COL][j]
        ||  illuminated[DIAG][i + j] || illuminated[ANTIDIAG][i - j])
        {
            result.push_back(1);
            for (int y = std::max(0, i - 1); y < std::min(n, i + 2); ++y)
                for (int x = std::max(0, j - 1); x < std::min(n, j + 2); ++x)
                    if (lamps_set.erase({y, x}))
                        --illuminated[ROW][y],
                        --illuminated[COL][x],
                        --illuminated[DIAG][y + x],
                        --illuminated[ANTIDIAG][y - x];
        }
        else result.push_back(0);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > lamps,
          std::vector<std::vector<int> > queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = gridIllumination(n, lamps, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{0, 0}, {4, 4}}, {{1, 1}, {1, 0}}, {1, 0}, trials);
    test(5, {{0, 0}, {4, 4}}, {{1, 1}, {1, 1}}, {1, 1}, trials);
    test(5, {{0, 0}, {0, 4}}, {{0, 4}, {0, 1}, {1, 4}}, {1, 1, 0}, trials);
    return 0;
}


