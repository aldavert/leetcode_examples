#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minOperations(std::string s1, std::string s2, int x)
{
    std::vector<int> diff_indices;
    for (int i = 0, n = static_cast<int>(s1.size()); i < n; ++i)
        if (s1[i] != s2[i])
            diff_indices.push_back(i);
    if (diff_indices.empty()) return 0;
    if (diff_indices.size() % 2 == 1) return -1;
    
    double dp = 0.0, dp_next = x / 2.0, dp_next_next = 0.0;
    for (int i = static_cast<int>(diff_indices.size()) - 2; i >= 0; --i)
    {
        dp = std::min(dp_next + x / 2.0,
                      dp_next_next + diff_indices[i + 1] - diff_indices[i]);
        dp_next_next = dp_next;
        dp_next = dp;
    }
    return static_cast<int>(dp);
}

// ############################################################################
// ############################################################################

void test(std::string s1, std::string s2, int x, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(s1, s2, x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1100011000", "0101001010", 2, 4, trials);
    test("10110", "00011", 4, -1, trials);
    test("1011100100111000", "1001010001011100", 19, -1, trials);
    return 0;
}


