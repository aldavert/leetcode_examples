#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

long long countInterestingSubarrays(std::vector<int> nums, int modulo, int k)
{
    std::unordered_map<int, int> prefix_count{{0, 1}};
    long result = 0;
    for (int prefix = 0; int num : nums)
    {
        if (num % modulo == k)
            prefix = (prefix + 1) % modulo;
        result += prefix_count[(prefix - k + modulo) % modulo];
        ++prefix_count[prefix];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int modulo,
          int k,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countInterestingSubarrays(nums, modulo, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 4}, 2, 1, 3, trials);
    test({3, 1, 9, 6}, 3, 0, 2, trials);
    return 0;
}


