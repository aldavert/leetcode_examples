#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

int ways(std::vector<std::string> pizza, int k)
{
    const int m_total = static_cast<int>(pizza.size());
    const int n_total = static_cast<int>(pizza[0].size());
    constexpr int MOD = 1'000'000'007;
    int dp_table[51][51][11], prefix[52][52] = {};
    std::memset(dp_table, -1, sizeof(dp_table));
    auto hasApple = [&](int row1, int row2, int col1, int col2) -> bool
    {
        return (prefix[row2][col2] - prefix[row1][col2]
              - prefix[row2][col1] + prefix[row1][col1]) > 0;
    };
    auto dpProcess = [&](auto &&self, int m, int n, int p) -> int
    {
        if (p == 0) return 1;
        if (dp_table[m][n][p] >= 0) return dp_table[m][n][p];
        dp_table[m][n][p] = 0;
        for (int i = m + 1; i < m_total; ++i)
            if (hasApple(m, i, n, n_total) && hasApple(i, m_total, n, n_total))
                dp_table[m][n][p] = (dp_table[m][n][p]
                                  +  self(self, i, n, p - 1)) % MOD;
        for (int j = n + 1; j < n_total; ++j)
            if (hasApple(m, m_total, n, j) && hasApple(m, m_total, j, n_total))
                dp_table[m][n][p] = (dp_table[m][n][p]
                                  +  self(self, m, j, p - 1)) % MOD;
        return dp_table[m][n][p];
    };
    for (int i = 0; i < m_total; ++i)
        for (int j = 0; j < n_total; ++j)
            prefix[i + 1][j + 1] = (pizza[i][j] == 'A') + prefix[i][j + 1]
                                 + prefix[i + 1][j] - prefix[i][j];
    return dpProcess(dpProcess, 0, 0, k - 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> pizza, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = ways(pizza, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"A..", "AAA", "..."}, 3, 3, trials);
    test({"A..", "AA.", "..."}, 3, 1, trials);
    test({"A..", "A..", "..."}, 1, 1, trials);
    return 0;
}


