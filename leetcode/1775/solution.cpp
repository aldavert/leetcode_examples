#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int minOperations(std::vector<int> nums1, std::vector<int> nums2)
{
    if ((nums1.size() * 6 < nums2.size())
    ||  (nums2.size() * 6 < nums1.size()))
        return -1;
    int sum1 = std::accumulate(nums1.begin(), nums1.end(), 0),
        sum2 = std::accumulate(nums2.begin(), nums2.end(), 0);
    if (sum1 > sum2)
        return minOperations(nums2, nums1);
    
    int result = 0, count[6] = {};
    for (int num : nums1) ++count[6 - num];
    for (int num : nums2) ++count[num - 1];
    for (int i = 5; sum2 > sum1;)
    {
        while (count[i] == 0) --i;
        sum1 += i;
        --count[i];
        ++result;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums1,
          std::vector<int> nums2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minOperations(nums1, nums2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6}, {1, 1, 2, 2, 2, 2}, 3, trials);
    test({1, 1, 1, 1, 1, 1, 1}, {6}, -1, trials);
    test({6, 6}, {1}, 3, trials);
    return 0;
}


