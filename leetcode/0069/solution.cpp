#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 0
//(i * i) <= x < (i + 1) * (i + 1)
//i^2 <= x < i^2 + 2*i + 1
//x - i^2 < 2 * i + 1
int mySqrt(int x)
{
    int result = 0;
    for (; x - result * result >= 2 * result + 1; ++result);
    return result;
}
#else
int mySqrt(int x)
{
    if (x <= 1) return x;
    int result = 0;
    for (int low = 0, high = x; low <= high;)
    {
        int mid = (low + high) / 2;
        if (mid <= x / mid)
        {
            result = mid;
            low = mid + 1;
        }
        else high = mid - 1;
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int x, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = mySqrt(x);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(0, 0, trials);
    test(4, 2, trials);
    test(8, 2, trials);
    return 0;
}

