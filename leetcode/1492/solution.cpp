#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int kthFactor(int n, int k)
{
    int factor = 1, i = 0;
    
    for (; factor * factor < n; ++factor)
        if ((n % factor == 0) && (++i == k))
            return factor;
    for (factor = n / factor; factor >= 1; --factor)
        if ((n % factor == 0) && (++i == k))
            return n / factor;
    return -1;
}

// ############################################################################
// ############################################################################

void test(int n, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = kthFactor(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(12, 3,  3, trials);
    test( 7, 2,  7, trials);
    test( 4, 4, -1, trials);
    return 0;
}


