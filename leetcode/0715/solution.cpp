#include "../common/common.hpp"
#include <map>
#include <memory>

// ############################################################################
// ############################################################################

#if 1
class RangeModule
{
    std::map<int, int> m;
    
    int removeIntervals(int l, int r)
    {
        for (auto it = m.lower_bound(l);
             (it != m.end()) && (it->first <= r); it = m.erase(it))
            r = std::max(r, it->second);
        return r;
    }
public:
    void addRange(int left, int right)
    {
        if (auto it = m.upper_bound(left); it != m.begin())
            if (auto prev = std::prev(it); prev->second >= left)
                left = prev->first;
        m.insert({left, removeIntervals(left, right)});
    }
    bool queryRange(int left, int right)
    {
        auto it = m.upper_bound(left);
        if (it != m.begin()) it = std::prev(it);
        return (left >= it->first) && (right <= it->second);
    }
    void removeRange(int left, int right)
    {
        int new_right = removeIntervals(left, right);
        if (auto it = m.lower_bound(left); it != m.begin())
            it = std::prev(it),
            new_right = std::max(new_right, it->second),
            it->second = std::min(it->second, left);
        if (new_right > right) m.insert({right, new_right});
    }
};
#else
class RangeModule
{
    struct Node
    {
        Node(int lo,
             int hi,
             bool tracked,
             Node * left = nullptr,
             Node * right = nullptr) :
            m_lo(lo), m_hi(hi), m_tracked(tracked), m_left(left), m_right(right) {}
        ~Node()
        {
            delete m_left;
            delete m_right;
            m_left  = nullptr;
            m_right = nullptr;
        }
        inline bool exact(int i, int j) const { return (m_lo == i) && (m_hi == j); }
        inline int mid(void) const { return (m_hi + m_lo) / 2; }
        void split(int mi)
        {
            m_left =  new Node(m_lo  ,   mi, m_tracked);
            m_right = new Node(mi + 1, m_hi, m_tracked);
        }
        inline void updateTracked(void)
        {
            m_tracked = m_left->m_tracked && m_right->m_tracked;
        }
        inline Node * left(void) { return m_left; }
        inline Node * right(void) { return m_right; }
        int m_lo       = -1;
        int m_hi       = -1;
        bool m_tracked = false;
        Node * m_left  = nullptr;
        Node * m_right = nullptr;
    };
    std::unique_ptr<Node> root;
    void update(Node * node, int i, int j, bool tracked)
    {
        if (node->exact(i, j))
        {
            node->m_tracked = tracked;
            node->m_left    = nullptr;
            node->m_right   = nullptr;
            return;
        }
        const int mid = node->mid();
        if (!node->left()) node->split(mid);
        if      (j <= mid) update(node->left(),  i, j, tracked);
        else if (i >  mid) update(node->right(), i, j, tracked);
        else
        {
            update(node->left(),       i, mid, tracked);
            update(node->right(), mid + 1,   j, tracked);
        }
        node->updateTracked();
    }
    bool query(Node * node, int i, int j)
    {
        if (!node->left()) return node->m_tracked;
        if (node->exact(i, j)) return node->m_tracked;
        const int mid = node->mid();
        if (j <= mid) return query(node->left() , i, j);
        if (i >  mid) return query(node->right(), i, j);
        return query(node->left(), i, mid) && query(node->right(), mid + 1, j);
    }
public:
    RangeModule(void) : root(std::make_unique<Node>(0, 1e9, false)) {}
    void addRange(int left, int right) { update(root.get(), left, right - 1, true); }
    bool queryRange(int left, int right) { return query(root.get(), left, right - 1); }
    void removeRange(int left, int right) { update(root.get(), left, right - 1, false); }
};
#endif

// ############################################################################
// ############################################################################

enum class OP { ADD, REMOVE, QUERY };

void test(std::vector<OP> operations,
          std::vector<std::tuple<int, int> > range,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        result.clear();
        RangeModule object;
        for (size_t i = 0, n = operations.size(); i < n; ++i)
        {
            auto [left, right] = range[i];
            switch (operations[i])
            {
            case OP::ADD:
                object.addRange(left, right);
                break;
            case OP::REMOVE:
                object.removeRange(left, right);
                break;
            case OP::QUERY:
                result.push_back(object.queryRange(left, right));
                break;
            default:
                std::cout << "[FAILURE] UNKNOWN/NOT IMPLEMENTED OPERATION: STOPPING..\n";
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::ADD, OP::REMOVE, OP::QUERY, OP::QUERY, OP::QUERY},
         {{10, 20}, {14, 16}, {10, 14}, {13, 15}, {16, 17}},
         {true, false, true}, trials);
    return 0;
}


