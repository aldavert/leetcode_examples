#include "../common/common.hpp"
#include <limits>

// ############################################################################
// ############################################################################

int minDifference(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    if (n < 5) return 0;
    
    int result = std::numeric_limits<int>::max();
    std::sort(nums.begin(), nums.end());
    for (int i = 0; i <= 3; ++i)
        result = std::min(result, nums[n - 4 + i] - nums[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDifference(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 3, 2, 4}, 0, trials);
    test({1, 5, 0, 10, 14}, 1, trials);
    test({3, 100, 20}, 0, trials);
    return 0;
}


