#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minElements(std::vector<int> nums, int limit, int goal)
{
    long diff = std::abs(std::accumulate(nums.begin(), nums.end(), 0L) - goal);
    return static_cast<int>(diff / limit + (diff % limit > 0));
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int limit,
          int goal,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minElements(nums, limit, goal);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, -1, 1}, 3, -4, 2, trials);
    test({1, -10, 9, 1}, 100, 0, 1, trials);
    return 0;
}


