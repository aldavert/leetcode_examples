#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> findIndices(std::vector<int> nums,
                             int indexDifference,
                             int valueDifference)
{
    const int n = static_cast<int>(nums.size());
    for (int i = 0; i < n; ++i)
        for (int j = i + indexDifference; j < n; ++j)
            if (std::abs(nums[i] - nums[j]) >= valueDifference)
                return { i, j };
    return { -1, -1 };
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int indexDifference,
          int valueDifference,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findIndices(nums, indexDifference, valueDifference);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 1, 4, 1}, 2, 4, {0, 3}, trials);
    test({2, 1}, 0, 0, {0, 0}, trials);
    test({1, 2, 3}, 2, 4, {-1, -1}, trials);
    return 0;
}


