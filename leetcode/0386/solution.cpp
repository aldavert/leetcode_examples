#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<int> lexicalOrder(int n)
{
    std::vector<int> result;
    for (int i = 1; static_cast<int>(result.size()) < n;)
    {
        result.push_back(i);
        if (i * 10 <= n) i *= 10;
        else
        {
            while ((i % 10 == 9) || (i == n)) i /= 10;
            ++i;
        }
    }
    return result;
}
#else
std::vector<int> lexicalOrder(int n)
{
    std::vector<int> result;
    auto fill = [&n,&result]() -> void
    {
        auto inner = [&n,&result](auto &&self, int i) -> void
        {
            if (i > n) return;
            result.push_back(i);
            for (int k = 0; k <= 9; ++k)
                self(self, i * 10 + k);
        };
        for (int k = 1; k <= 9; ++k)
            inner(inner, k);
    };
    fill();
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int n, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = lexicalOrder(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(13, {1, 10, 11, 12, 13, 2, 3, 4, 5, 6, 7, 8, 9}, trials);
    test(2, {1, 2}, trials);
    return 0;
}


