#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int twoEggDrop(int n)
{
    std::vector<std::vector<int> > dp(3, std::vector<int>(n + 1, -1));
    auto drop = [&](auto &&self, int lk, int ln) -> int
    {
        if (ln <= 1) return ln;
        if (lk == 1) return ln;
        if (dp[lk][ln] != -1) return dp[lk][ln];
        int result = ln;
        for (int low = 0, high = ln; low <= high; )
        {
            int mid = low + (high - low) / 2;
            int left = 1 + self(self, lk - 1, mid - 1);
            int right = 1 + self(self, lk, ln - mid);
            result = std::min(result, std::max(left, right));
            if (left == right) break;
            if (left < right) low = mid + 1;
            else high = mid - 1;
        }
        return dp[lk][ln] = result;
    };
    return drop(drop, 2, n);
}

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = twoEggDrop(n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 2, trials);
    test(100, 14, trials);
    return 0;
}


