#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minCost(std::string colors, std::vector<int> neededTime)
{
    const int n = static_cast<int>(colors.size());
    int result = 0;
    for(int i = 1; i < n; ++i)
    {
        if (colors[i] == colors[i - 1])
        {
            result += std::min(neededTime[i], neededTime[i - 1]);
            neededTime[i] = std::max(neededTime[i], neededTime[i - 1]);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string colors,
          std::vector<int> neededTime,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCost(colors, neededTime);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abaac", {1, 2, 3, 4, 5}, 3, trials);
    test("abc", {1, 2, 3}, 0, trials);
    test("aabaa", {1, 2, 3, 4, 1}, 2, trials);
    return 0;
}


