#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countBalancedPermutations(std::string num)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(num.size()),
              m = n / 2 + 1;
    long count[80][80] = {};
    for (int i = 0; i < 80; ++i) count[i][0] = 1;
    for (int i = 1; i < 80; ++i)
        for (int j = 1; j <= i; ++j)
            count[i][j] = (count[i - 1][j] + count[i - 1][j - 1]) % MOD;
    
    int histogram[10] = {}, sum = 0;
    for (char c : num)
    {
        ++histogram[c - '0'];
        sum += c - '0';
    }
    if (sum & 1) return 0;
    const int offset3 = (m + 1),
              offset2 = m * offset3,
              offset1 = (sum / 2 + 1) * offset2;
    std::vector<int> cost(offset1 * 10, -1);
    auto at = [&](int i, int j, int a, int b) -> int&
    {
        return cost[i * offset1 + j * offset2 + a * offset3 + b];
    };
    auto dfs = [&](auto &&self, int i, int j, int a, int b) -> int
    {
        if (i > 9) return (j | a | b) == 0;
        if ((a == 0) && (j > 0)) return 0;
        if (at(i, j, a, b) != -1) return at(i, j, a, b);
        long result = 0;
        for (int l = 0; l <= std::min(histogram[i], a); ++l)
        {
            int r = histogram[i] - l;
            if ((r >= 0) && (r <= b) && (l * i <= j))
            {
                long t = count[a][l] * count[b][r] % MOD
                       * self(self, i + 1, j - l * i, a - l, b - r) % MOD;
                result = (result + t) % MOD;
            }
        }
        return at(i, j, a, b) = static_cast<int>(result);
    };
    return dfs(dfs, 0, sum / 2, n / 2, (n + 1) / 2);
}

// ############################################################################
// ############################################################################

void test(std::string num,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int t = 0; t < trials; ++t)
        result = countBalancedPermutations(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("123", 2, trials);
    test("112", 1, trials);
    test("12345", 0, trials);
    test("19", 0, trials);
    return 0;
}


