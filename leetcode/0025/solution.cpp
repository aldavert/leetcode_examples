#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

ListNode * reverseKGroup(ListNode * head, int k)
{
    if (k < 2) return head; // Nothing to do here.
    
    ListNode * stack[5001];
    int count = 0;
    ListNode * result = nullptr, * last = nullptr;
    while (head != nullptr)
    {
        stack[count++] = head;
        head = head->next;
        if (count == k)
        {
            --count;
            if (last != nullptr) last->next = stack[count];
            if (result == nullptr) result = stack[count];
            for (; count > 0; --count)
                stack[count]->next = stack[count - 1];
            stack[0]->next = nullptr;
            last = stack[0];
        }
    }
    if (count > 0)
        last->next = stack[0];
    
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> input,
          int k,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(input);
        head = reverseKGroup(head, k);
        result = list2vec(head);
        delete head;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 2, {2, 1, 4, 3, 5}, trials);
    test({1, 2, 3, 4, 5}, 3, {3, 2, 1, 4, 5}, trials);
    test({1, 2, 3, 4, 5}, 1, {1, 2, 3, 4, 5}, trials);
    test({1}, 1, {1}, trials);
    return 0;
}


