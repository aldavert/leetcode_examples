#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int countCombinations(std::vector<std::string> pieces,
                      std::vector<std::vector<int> > positions)
{
    static const std::unordered_map<std::string,
                                    std::vector<std::pair<int, int> > > directions = {
        {"rook", {{0, 1}, {1, 0}, {0, -1}, {-1, 0}}},
        {"bishop", {{1, 1}, {1, -1}, {-1, 1}, {-1, -1}}},
        {"queen", {{0, 1}, {1, 0}, {0, -1}, {-1, 0}, {1, 1}, {1, -1}, {-1, 1}, {-1, -1}}}
    };
    constexpr int all_mask = (1 << 7) - 1;
    const int n = static_cast<int>(pieces.size());
    std::vector<std::vector<int> > lookup(8, std::vector<int>(8));
    auto backtracking = [&](auto &&self, int i) -> int
    {
        if (i == n) return 1;
        int result = 0, r = positions[i][0] - 1, c = positions[i][1] - 1;
        if (int mask = all_mask; !(lookup[r][c] & mask))
        {
            lookup[r][c] += mask;
            result += self(self, i + 1);
            lookup[r][c] -= mask;
        }
        for (const auto& [dr, dc] : directions.at(pieces[i]))
        {
            int bit = 1, nr = r + dr, nc = c + dc;
            int mask = all_mask;
            for (; (0 <= nr) && (nr < 8) && (0 <= nc) && (nc < 8)
                   && !(lookup[nr][nc] & bit); bit <<= 1, nr += dr, nc += dc)
            {
                lookup[nr][nc] += bit;
                mask -= bit;
                if (!(lookup[nr][nc] & mask))
                {
                    lookup[nr][nc] += mask;
                    result += self(self, i + 1);
                    lookup[nr][nc] -= mask;
                }
            }
            while (bit >> 1) lookup[nr -= dr][nc -= dc] -= bit >>= 1;
        }
        return result;

    };
    return backtracking(backtracking, 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> pieces,
          std::vector<std::vector<int> > positions,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countCombinations(pieces, positions);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"rook"}, {{1, 1}}, 15, trials);
    test({"queen"}, {{1, 1}}, 22, trials);
    test({"bishop"}, {{4, 3}}, 12, trials);
    return 0;
}


