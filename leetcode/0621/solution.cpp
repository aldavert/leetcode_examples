#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int leastInterval(std::vector<char> tasks, int n)
{
    const int m = static_cast<int>(tasks.size());
    if (n == 0) return m;
    int histogram[26] = {};
    for (char task : tasks)
        ++histogram[task - 'A'];
    int max_frequency = 0, max_frequency_count = 0;
    for (int i = 0; i < 26; ++i)
    {
        if (histogram[i] > max_frequency)
        {
            max_frequency = histogram[i];
            max_frequency_count = 1;
        }
        else max_frequency_count += (histogram[i] == max_frequency);
    }
    return std::max((max_frequency - 1) * (n + 1) + max_frequency_count, m);
}

// ############################################################################
// ############################################################################

void test(std::vector<char> tasks, int n, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = leastInterval(tasks, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({'A', 'A', 'A', 'B', 'B', 'B'}, 2, 8, trials);
    test({'A', 'C', 'A', 'B', 'D', 'B'}, 1, 6, trials);
    test({'A', 'A', 'A', 'B', 'B', 'B'}, 3, 10, trials);
    test({'A', 'A'}, 2, 4, trials);
    test({'A', 'A', 'A', 'A', 'A', 'A', 'B', 'C', 'D', 'E', 'F', 'G'}, 1, 12, trials);
    return 0;
}


