#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countTexts(std::string pressedKeys)
{
    const int n = static_cast<int>(pressedKeys.size());
    auto isSame = [&](int i, int k) -> bool
    {
        if (i + k > n) return false;
        for (int j = i + 1; j < i + k; ++j)
            if (pressedKeys[j] != pressedKeys[i])
                return false;
        return true;
    };
    constexpr int MOD = 1'000'000'007;
    std::vector<long> dp(n + 1);
    dp[n] = 1;
    
    for (int i = n - 1; i >= 0; --i)
    {
        dp[i] = dp[i + 1];
        if (isSame(i, 2)) dp[i] += dp[i + 2];
        if (isSame(i, 3)) dp[i] += dp[i + 3];
        if (((pressedKeys[i] == '7') || (pressedKeys[i] == '9')) && isSame(i, 4))
            dp[i] += dp[i + 4];
        dp[i] %= MOD;
    }
    return static_cast<int>(dp[0]);
}

// ############################################################################
// ############################################################################

void test(std::string pressedKeys, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countTexts(pressedKeys);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("22233", 8, trials);
    test("222222222222222222222222222222222222", 82876089, trials);
    return 0;
}


