#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

class MovieRentingSystem
{
public:
    MovieRentingSystem(int /*n*/, std::vector<std::vector<int> > &entries)
    {
        for (const auto &e : entries)
        {
            int shop  = e[0], movie = e[1], price = e[2];
            m_unrented[movie].insert({price, shop});
            m_shop_movie_to_price[{shop, movie}] = price;
        }
    }
    
    std::vector<int> search(int movie)
    {
        std::vector<int> result;
        
        for (int i = 0; const auto& [price, shop] : m_unrented[movie])
        {
            result.push_back(shop);
            if (++i >= 5) break;
        }
        
        return result;
    }
    
    void rent(int shop, int movie)
    {
        int price = m_shop_movie_to_price[{shop, movie}];
        m_unrented[movie].erase({price, shop});
        m_rented.insert({price, {shop, movie}});
    }
    
    void drop(int shop, int movie)
    {
        int price = m_shop_movie_to_price[{shop, movie}];
        m_unrented[movie].insert({price, shop});
        m_rented.erase({price, {shop, movie}});
    }
    
    std::vector<std::vector<int> > report(void)
    {
        std::vector<std::vector<int> > result;
        
        for (int i = 0; const auto& [_, shop_and_movie] : m_rented)
        {
            result.push_back({shop_and_movie.first, shop_and_movie.second});
            if (++i >= 5) break;
        }
        
        return result;
    }
private:
    struct pairHash
    {
        inline size_t operator()(const std::pair<int, int>& p) const
        {
            return p.first ^ p.second;
        }
    };
    
    std::unordered_map<int, std::set<std::pair<int, int> > > m_unrented;
    std::unordered_map<std::pair<int, int>, int, pairHash> m_shop_movie_to_price;
    std::set<std::pair<int, std::pair<int, int> > > m_rented;
};

// ############################################################################
// ############################################################################

enum class OP { SEARCH, RENT, DROP, REPORT };
typedef std::variant<std::vector<int>, std::vector<std::vector<int> > > TRESULT;

void test(int n,
          std::vector<std::vector<int> > init,
          std::vector<OP> options,
          std::vector<std::vector<int> > input,
          std::vector<TRESULT> solution,
          unsigned int trials = 1)
{
    std::vector<TRESULT> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        MovieRentingSystem obj(n, init);
        result.clear();
        for (size_t i = 0; i < options.size(); ++i)
        {
            switch (options[i])
            {
            case OP::SEARCH:
                result.push_back(obj.search(input[i][0]));
                break;
            case OP::RENT:
                obj.rent(input[i][0], input[i][1]);
                result.push_back({});
                break;
            case OP::DROP:
                obj.drop(input[i][0], input[i][1]);
                result.push_back({});
                break;
            case OP::REPORT:
                result.push_back(obj.report());
                break;
            default:
                std::cerr << "[ERROR] The selected option has not been "
                             "implemented, yet.\n";
                return;
            }
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    using T1 = std::vector<int>;
    using T2 = std::vector<std::vector<int> >;
    test(3, {{0, 1, 5}, {0, 2, 6}, {0, 3, 7}, {1, 1, 4}, {1, 2, 7}, {2, 1, 5}},
         {OP::SEARCH, OP::RENT, OP::RENT, OP::REPORT, OP::DROP, OP::SEARCH},
         {{1}, {0, 1}, {1, 2}, {}, {1, 2}, {2}},
         {T1{1, 0, 2}, T1{}, T1{}, T2{{0, 1}, {1, 2}}, T1{}, T1{0, 1}}, trials);
    return 0;
}


