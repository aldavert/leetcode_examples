#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int countPartitions(std::vector<int> nums)
{
    int result = 0, right = std::accumulate(nums.begin(), nums.end(), 0), left = 0;
    for (size_t i = 1; i < nums.size(); ++i)
    {
        left += nums[i - 1];
        right -= nums[i - 1];
        result += (left - right) % 2 == 0;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPartitions(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 10, 3, 7, 6}, 4, trials);
    test({1, 2, 2}, 0, trials);
    test({2, 4, 6, 8}, 3, trials);
    return 0;
}


