#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxScore(std::string s)
{
    int result = 0, ones = 0, zeros = 0;
    for (char c : s)
        ones += (c == '1');
    for (size_t i = 0, n = s.size() - 1; i < n; ++i)
    {
        ones -= (s[i] == '1');
        zeros += (s[i] == '0');
        result = std::max(result, ones + zeros);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxScore(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("011101", 5, trials);
    test("00111", 5, trials);
    test("1111", 3, trials);
    test("00", 1, trials);
    return 0;
}


