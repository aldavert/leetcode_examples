#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

#if 1
double maxProbability(int n,
                      std::vector<std::vector<int> > edges,
                      std::vector<double> succProb,
                      int start,
                      int end)
{
    std::vector<double> prob(n);
    prob[start] = 1.0;
    for (int i = 0; i < n; ++i)
    {
        bool connected = false;
        for (size_t j = 0; j < edges.size(); ++j)
        {
            int a = edges[j][0], b = edges[j][1];
            double p = succProb[j];
            if (prob[a] * p > prob[b])
            {
                prob[b] = prob[a]*p;
                connected = true;
            }
            if (prob[b] * p > prob[a])
            {
                prob[a] = prob[b]*p;
                connected = true;
            }
        }
        if (!connected) break;
    }
    return prob[end];
}
#else
double maxProbability(int n,
                      std::vector<std::vector<int> > edges,
                      std::vector<double> succProb,
                      int start,
                      int end)
{
    struct Info
    {
        double probability;
        int node;
        bool operator<(const Info &other) const
        {
            return probability < other.probability;
        }
    };
    std::vector<std::vector<Info> > graph(n);
    for (size_t i = 0; i < edges.size(); ++i)
    {
        graph[edges[i][0]].push_back({succProb[i], edges[i][1]});
        graph[edges[i][1]].push_back({succProb[i], edges[i][0]});
    }
    std::vector<bool> seen(n);
    std::priority_queue<Info> heap;
    heap.push({1.0, start});
    while (!heap.empty())
    {
        auto [p, node] = heap.top();
        heap.pop();
        if (node == end) return p;
        if (seen[node]) continue;
        seen[node] = true;
        for (auto [edge_p, neighbor] : graph[node])
            if (!seen[neighbor])
                heap.push({p * edge_p, neighbor});
    }
    return 0;
}
#endif

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<double> succProb,
          int start,
          int end,
          double solution,
          unsigned int trials = 1)
{
    double result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxProbability(n, edges, succProb, start, end);
    showResult(std::abs(solution - result) < 1e-5, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{0, 1}, {1, 2}, {0, 2}}, {0.5, 0.5, 0.2}, 0, 2, 0.25, trials);
    test(3, {{0, 1}, {1, 2}, {0, 2}}, {0.5, 0.5, 0.3}, 0, 2, 0.30, trials);
    test(3, {{0, 1}},                 {0.5}          , 0, 2, 0.00, trials);
    return 0;
}


