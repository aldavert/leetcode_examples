#include "../common/common.hpp"
#include <random>
#include <fmt/core.h>

// ############################################################################
// ############################################################################

class Solution
{
    std::vector<long> areas;
    long area;
    std::random_device rd;
    std::mt19937 generator;
    std::discrete_distribution<> dist_rectangle;
    std::vector<std::tuple<std::uniform_int_distribution<>,
                           std::uniform_int_distribution<> > > dist_points;
    std::vector<std::tuple<int, int> > origin;
public:
    Solution(std::vector<std::vector<int> > rects) :
        generator(rd())
    {
        area = 0;
        for (const auto &rectangle : rects)
        {
            int dx = rectangle[2] - rectangle[0],
                dy = rectangle[3] - rectangle[1];
            areas.push_back((dx + 1) * (dy + 1));
            area += areas.back();
            dist_rectangle = std::discrete_distribution<>(areas.begin(), areas.end());
            std::uniform_int_distribution<> dist_x(0, dx), dist_y(0, dy);
            dist_points.push_back({dist_x, dist_y});
            origin.push_back({rectangle[0], rectangle[1]});
        }
    }
    std::vector<int> pick()
    {
        size_t selected = dist_rectangle(generator);
        auto [x, y] = origin[selected];
        auto &[dist_x, dist_y] = dist_points[selected];
        return { x + dist_x(generator), y + dist_y(generator) };
    }
};

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > rects,
          size_t number_of_points,
          unsigned int trials = 1)
{
    std::vector<long> rectangle_offset;
    long area = 0;
    for (const auto &rectangle : rects)
    {
        rectangle_offset.push_back((rectangle[2] - rectangle[0] + 1)
                                 * (rectangle[3] - rectangle[1] + 1));
        area += rectangle_offset.back();
    }
    for (size_t i = 1; i < rects.size(); ++i)
        rectangle_offset[i] += rectangle_offset[i - 1];
    for (size_t i = rects.size() - 1; i > 0; --i)
        rectangle_offset[i] = rectangle_offset[i - 1];
    rectangle_offset[0] = 0;
    std::vector<int> histogram((area <= 10'000'000)?area:0);
    for (unsigned int t = 0; t < trials; ++t)
    {
        Solution object(rects);
        histogram.assign(histogram.size(), 0);
        for (size_t i = 0; i < number_of_points; ++i)
        {
            auto point = object.pick();
            if (point.size() != 2)
            {
                std::cout << showResult(false) << " Point with an incorrect "
                          << "number of coordinates.\n";
                return;
            }
            int x = point[0], y = point[1];
            size_t selected = rects.size();
            for (size_t r = 0; r < rects.size(); ++r)
            {
                if ((x >= rects[r][0]) && (x <= rects[r][2])
                &&  (y >= rects[r][1]) && (y <= rects[r][3]))
                {
                    selected = r;
                    break;
                }
            }
            if (selected == rects.size())
            {
                std::cout << showResult(false) << " Point outside the rectangles.\n";
                return;
            }
            if (area < 10'000'000)
                ++histogram[rectangle_offset[selected]
                          + (x - rects[selected][0])
                          + (y - rects[selected][1])
                          * (rects[selected][2] - rects[selected][0] + 1)];
        }
    }
    if (area >= 10'000'000)
        std::cout << showResult(true) << " All points are inside the rectangles.\n";
    else
    {
        double expected = static_cast<double>(number_of_points)
                        / static_cast<double>(area);
        double deviation_avg = 0, deviation_max = 0;
        for (int value : histogram)
        {
            double difference = static_cast<double>(value) - expected;
            deviation_avg += difference * difference;
            deviation_max = std::max(deviation_max, difference);
        }
        deviation_avg = std::sqrt(deviation_avg / static_cast<double>(area));
        // Standard deviation is smaller than a 5% of the expected value.
        // Maximum error is smaller than a 10% of the expected value.
        std::cout << showResult((100.0 * deviation_avg / expected < 5) 
                            &&  (100.0 * deviation_max / expected < 10));
        std::cout << " points show an average standard deviation of '"
                  << deviation_avg << "' ("
                  << fmt::format("{:.2f}", 100 * deviation_avg / expected)
                  << "% of the expected value) and a max error of '"
                  << deviation_max << "' ("
                  << fmt::format("{:.2f}", 100 * deviation_max / expected)
                  << "% of the expected value) while sampling '"
                  << number_of_points << "' on '" << rects.size()
                  << "' rectangles covering an area of '" << area << "' units^2.\n";
    }
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{-2, -2, 1, 1}, {2, 2, 4, 6}}, 25'000, trials);
    test({{-2, -2, 1, 1}, {2, 2, 4, 6}}, 250'000, trials);
    test({{-2, -2, 1, 1}, {2, 2, 4, 6}}, 1'000'000, trials);
    return 0;
}


