#include "../common/common.hpp"
#include "testA.hpp"

// ############################################################################
// ############################################################################

std::string frequencySort(std::string s)
{
    int histogram[256] = {};
    for (char c : s)
        ++histogram[static_cast<int>(c)];
    unsigned int non_zero = 0;
    for (int i = 0; i < 256; ++i)
        if (histogram[i])
            ++non_zero;
    std::pair<int, char> histogram_pairs[256];
    non_zero = 0;
    for (int i = 0; i < 256; ++i)
        if (histogram[i])
            histogram_pairs[non_zero++] = {histogram[i], static_cast<char>(i)};
    std::sort(&histogram_pairs[0], &histogram_pairs[non_zero]);
    for (int i = non_zero - 1, j = 0; i >= 0; --i)
        for (int k = 0; k < histogram_pairs[i].first; ++k, ++j)
            s[j] = histogram_pairs[i].second;
    return s;
}

// ############################################################################
// ############################################################################

std::ostream& operator<<(std::ostream &out, const std::string &s)
{
    out << '\'';
    if (s.size() < 15)
        std::operator<<(out, s);
    else
    {
        for (int i = 0; i < 5; ++i) out << s[i];
        out << "...";
        for (int i = static_cast<int>(s.size()) - 1; i > static_cast<int>(s.size()) - 6; --i)
            out << s[i];
    }
    out << '\'';
    return out;
}

bool operator==(const std::vector<std::string> &left, const std::string &right)
{
    std::unordered_set<std::string> lut(left.begin(), left.end());
    return lut.find(right) != lut.end();
}

void test(std::string s, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = frequencySort(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("tree", {"eert", "eetr"}, trials);
    test("cccaaa", {"aaaccc", "cccaaa"}, trials);
    test("Aabb", {"bbAa", "bbaA"}, trials);
    test(testA::s, testA::solution, trials);
    return 0;
}


