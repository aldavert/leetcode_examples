#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isItPossible(std::string word1, std::string word2)
{
    int count1[26] = {}, count2[26] = {}, distinct1 = 0, distinct2 = 0;
    for (char c : word1) ++count1[c - 'a'];
    for (char c : word2) ++count2[c - 'a'];
    for (size_t i = 0; i < 26; ++i)
        distinct1 += count1[i] > 0,
        distinct2 += count2[i] > 0;
    for (int i = 0; i < 26; ++i)
    {
        for (int j = 0; j < 26; ++j)
        {
            if ((count1[i] == 0) || (count2[j] == 0))
                continue;
            if (i == j)
            {
                if (distinct1 == distinct2)
                    return true;
                continue;
            }
            if (distinct1 - (count1[i] == 1) + (count1[j] == 0)
            ==  distinct2 - (count2[j] == 1) + (count2[i] == 0))
                return true;
        }
    }
    return false;
}

// ############################################################################
// ############################################################################

void test(std::string word1, std::string word2, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isItPossible(word1, word2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ac", "b", false, trials);
    test("abcc", "aab", true, trials);
    test("abcde", "fghij", true, trials);
    return 0;
}


