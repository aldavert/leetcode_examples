#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> fullBloomFlowers(std::vector<std::vector<int> > flowers,
                                  std::vector<int> people)
{
    std::vector<int> result, starts, ends;
    
    for (const auto &flower : flowers)
    {
        starts.push_back(flower[0]);
        ends.push_back(flower[1]);
    }
    std::sort(starts.begin(), starts.end());
    std::sort(ends.begin(), ends.end());
    for (int p : people)
    {
        auto begin = std::upper_bound(starts.begin(), starts.end(), p),
             end   = std::lower_bound(ends.begin(), ends.end(), p);
        result.push_back(static_cast<int>(begin - starts.begin())
                       - static_cast<int>(end - ends.begin()));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > flowers,
          std::vector<int> people,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = fullBloomFlowers(flowers, people);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 6}, {3, 7}, {9, 12}, {4, 13}},
         {2, 3, 7, 11}, {1, 2, 2, 2}, trials);
    test({{1, 10}, {3, 3}}, {3, 3, 2}, {2, 2, 1}, trials);
    return 0;
}


