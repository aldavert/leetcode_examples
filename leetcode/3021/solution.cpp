#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long flowerGame(int n, int m)
{
    const int x_even = n / 2;
    const int y_even = m / 2;
    const int x_odd = (n + 1) / 2;
    const int y_odd = (m + 1) / 2;
    return static_cast<long>(x_even) * y_odd + static_cast<long>(y_even) * x_odd;
}

// ############################################################################
// ############################################################################

void test(int n, int m, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = flowerGame(n, m);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 2, 3, trials);
    test(1, 1, 0, trials);
    return 0;
}


