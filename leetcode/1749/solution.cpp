#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxAbsoluteSum(std::vector<int> nums)
{
    int max_prefix = 0, min_prefix = 0;
    
    for (int sum = 0; int num : nums)
    {
        sum += num;
        max_prefix = std::max(max_prefix, sum);
        min_prefix = std::min(min_prefix, sum);
    }
    return max_prefix - min_prefix;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxAbsoluteSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, -3, 2, 3, -4}, 5, trials);
    test({2, -5, 1, -4, 3, -2}, 8, trials);
    return 0;
}


