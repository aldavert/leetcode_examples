#include "../common/common.hpp"
#include <unordered_map>
#include "testA.hpp"

#define DEBUG 0

// ############################################################################
// ############################################################################

std::vector<int> findSubstring(std::string s, std::vector<std::string> words)
{
    const int n = static_cast<int>(s.size());
    const int m = static_cast<int>(words[0].size());
    std::vector<int> result;
    std::unordered_map<std::string, int> word2id;
    std::vector<int> histogram;
    for (const auto &w : words)
    {
        if (auto search = word2id.find(w); search == word2id.end())
        {
            word2id[w] = static_cast<int>(histogram.size());
            histogram.push_back(1);
        }
        else ++histogram[search->second];
    }
    const int q = static_cast<int>(words.size());
    const int p = q * m - 1;
    
    std::vector<int> s_id(n - m + 1);
    for (int i = 0; i < n - m + 1; ++i)
    {
        if (auto search = word2id.find(s.substr(i, m)); search != word2id.end())
            s_id[i] = search->second;
        else s_id[i] = -1;
    }
    for (int i = 0; i < n - p; ++i)
    {
        std::vector<int> frequency(histogram);
        for (int j = 0; j < q; ++j)
            if (int val = s_id[i + j * m]; val != -1)
                --frequency[val];
        bool correct = true;
        for (int f : frequency)
            correct = correct && f == 0;
        if (correct)
            result.push_back(i);
    }
    
#if DEBUG
    // ########################################################################
    std::cout << "n: " << n << " (string length).\n";
    std::cout << "m: " << m << " (word length)\n";
    std::cout << "q: " << q << " (number of words)\n";
    std::cout << "p: " << p << " (search limit)\n";
    for (char c : s)
        std::cout << c << ' ';
    std::cout << '\n';
    for (int i : s_id)
        std::cout << i << ' ';
    std::cout << '\n';
    for (const auto &l : word2id)
    {
        std::cout << l.first << ' ';
        std::cout << l.second << ' ';
        std::cout << histogram[l.second] << '\n';
    }
    // ########################################################################
#endif
    
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    std::unordered_map<int, int> lut;
    for (int v : left)
        ++lut[v];
    for (int v : right)
    {
        if (auto search = lut.find(v); search != lut.end())
        {
            search->second--;
            if (search->second == 0)
                lut.erase(search);
        }
        else return false;
    }
    return lut.empty();
}


void test(std::string s,
          std::vector<std::string> words,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findSubstring(s, words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("barfoothefoobarman", {"foo", "bar"}, {0, 9}, trials);
    test("wordgoodgoodgoodbestword", {"word", "good", "best", "word"}, {}, trials);
    test("barfoofoobarthefoobarman", {"bar", "foo", "the"}, {6, 9, 12}, trials);
    test("acaccacbb", {"ac","ca","ac"}, {0}, trials);
    test(testA::s, testA::words, testA::solution, trials);
    return 0;
}


