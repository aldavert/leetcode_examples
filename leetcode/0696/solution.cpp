#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int countBinarySubstrings(std::string s)
{
    int result = 0, histogram[2] = {0, 0};
    char previous = '0';
    for (char c : s)
    {
        int idx = c - '0';
        if (c != previous)
        {
            result += std::min(histogram[0], histogram[1]);
            histogram[idx] = 0;
        }
        ++histogram[idx];
        previous = c;
    }
    return result + std::min(histogram[0], histogram[1]);
}
#else
int countBinarySubstrings(std::string s)
{
    int result = 0, zeros = 0, ones = 0;
    char previous = '0';
    for (char c : s)
    {
        if (c == '0')
        {
            if (previous == '1')
            {
                result += std::min(zeros, ones);
                zeros = 0;
            }
            ++zeros;
        }
        else
        {
            if (previous == '0')
            {
                result += std::min(zeros, ones);
                ones = 0;
            }
            ++ones;
        }
        previous = c;
    }
    return result + std::min(zeros, ones);
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countBinarySubstrings(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("00110011", 6, trials);
    test("10101", 4, trials);
    return 0;
}


