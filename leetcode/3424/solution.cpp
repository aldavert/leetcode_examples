#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long minCost(std::vector<int> arr, std::vector<int> brr, long long k)
{
    long long result_a = 0, result_b = k;
    for (size_t i = 0; i < arr.size(); ++i)
        result_a += std::abs(arr[i] - brr[i]);
    std::sort(arr.begin(), arr.end());
    std::sort(brr.begin(), brr.end());
    for (size_t i = 0; i < arr.size(); ++i)
        result_b += std::abs(arr[i] - brr[i]);
    return std::min(result_a, result_b);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr,
          std::vector<int> brr,
          long long k,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCost(arr, brr, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-7, 9, 5}, {7, -2, -5}, 2, 13, trials);
    test({2, 1}, {2, 1}, 0, 0, trials);
    return 0;
}


