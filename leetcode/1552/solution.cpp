#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxDistance(std::vector<int> position, int m)
{
    auto numBalls = [&](int force) -> int
    {
        int balls = 0, prev_position = -force;
        for (int pos : position)
        {
            if (pos - prev_position >= force)
            {
                ++balls;
                prev_position = pos;
            }
        }
        return balls;
    };
    std::sort(position.begin(), position.end());
    int l = 1, r = position.back() - position.front();
    while (l < r)
    {
        const int mid = r - (r - l) / 2;
        if (numBalls(mid) >= m)
            l = mid;
        else r = mid - 1;
    }
    return l;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> position, int m, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxDistance(position, m);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 7}, 3, 3, trials);
    test({5, 4, 3, 2, 1, 1000000000}, 2, 999999999, trials);
    return 0;
}


