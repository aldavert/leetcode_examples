#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::string largestWordCount(std::vector<std::string> messages,
                             std::vector<std::string> senders)
{
    const int n = static_cast<int>(messages.size());
    std::unordered_map<std::string, int> count;
    std::string result;
    
    for (int i = 0, max_words_sent = 0; i < n; ++i)
    {
        const auto &message = messages[i], &sender = senders[i];
        int num_sent = count[sender] += static_cast<int>(std::count(message.begin(),
                                        message.end(), ' ')) + 1;
        if (num_sent > max_words_sent)
        {
            result = sender;
            max_words_sent = num_sent;
        }
        else if ((num_sent == max_words_sent) && (sender > result))
            result = sender;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> messages,
          std::vector<std::string> senders,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestWordCount(messages, senders);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"Hello userTwooo", "Hi userThree", "Wonderful day Alice",
          "Nice day userThree"},
          {"Alice", "userTwo", "userThree", "Alice"}, "Alice", trials);
    test({"How is leetcode for everyone", "Leetcode is useful for practice"},
         {"Bob", "Charlie"}, "Charlie", trials);
    return 0;
}


