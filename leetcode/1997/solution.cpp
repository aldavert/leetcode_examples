#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int firstDayBeenInAllRooms(std::vector<int> nextVisit)
{
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(nextVisit.size());
    std::vector<int> dp(n);
    
    for (int i = 1; i < n; ++i)
        dp[i] = static_cast<int>((2L * dp[i - 1] - dp[nextVisit[i - 1]] + 2 + MOD)
              % MOD);
    return dp.back();
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nextVisit, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = firstDayBeenInAllRooms(nextVisit);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 0}, 2, trials);
    test({0, 0, 2}, 6, trials);
    test({0, 1, 2, 0}, 6, trials);
    return 0;
}


