#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int numDecodings(std::string s)
{
    int dp[101] = {};
    dp[0] = 1;
    dp[1] = s[0] != '0';
    for (size_t i = 2; i <= s.size(); ++i)
        dp[i] += (s[i - 1] != '0') * dp[i - 1]
              +  ((s[i - 2] == '1') || ((s[i - 2] == '2') && (s[i - 1] < '7')))
              *  dp[i - 2];
    return dp[s.size()];
}
#else
int numDecodings(std::string s)
{
    std::unordered_map<int, int> memory;
    auto process = [&](auto &&self, int idx, int n) -> int
    {
        if (auto it = memory.find(idx); it != memory.end()) return it->second;
        if (idx == n)
            return 1;
        else if (idx > n)
            return 0;
        // 10 to 19
        if (s[idx] == '1')
            return (memory[idx + 1] = self(self, idx + 1, n))
                 + (memory[idx + 2] = self(self, idx + 2, n));
        // 20 to 26
        else if (s[idx] == '2')
            return (((idx + 1 < n) && (s[idx + 1] <= '6'))?
                   (memory[idx + 2] = self(self, idx + 2, n)):0)
                 + (memory[idx + 1] = self(self, idx + 1, n));
        // 3 to 9
        else if ((s[idx] >= '3') && (s[idx] <= '9'))
            return (memory[idx + 1] = self(self, idx + 1, n));
        // 0 is error.
        else return 0;
    };
    return process(process, 0, static_cast<int>(s.size()));
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numDecodings(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("12", 2, trials);
    test("226", 3, trials);
    test("0", 0, trials);
    test("06", 0, trials);
    test("29273192739102981428719231329273192739129814287192313", 2048, trials);
    return 0;
}


