#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minDeletion(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int result = 0;
    for (int i = 0; i + 1 < n; ++i)
        result += ((nums[i] == nums[i + 1]) && ((i - result) % 2 == 0));
    return result + ((n - result) & 1);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDeletion(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2, 3, 5}, 1, trials);
    test({1, 1, 2, 2, 3, 3}, 2, trials);
    return 0;
}


