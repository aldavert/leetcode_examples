#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> rearrangeArray(std::vector<int> nums)
{
    std::vector<int> result(nums.size());
    int positive = 0, negative = 1;
    for (int number : nums)
    {
        if (number >= 0)
        {
            result[positive] = number;
            positive += 2;
        }
        else
        {
            result[negative] = number;
            negative += 2;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = rearrangeArray(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 1, -2, -5, 2, -4}, {3, -2, 1, -5, 2, -4}, trials);
    test({-1, 1}, {1, -1}, trials);
    return 0;
}

