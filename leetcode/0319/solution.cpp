#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

#if 1
int bulbSwitch(int n)
{
    return static_cast<int>(std::ceil(std::sqrt(n + 1)) - 1);
}
#else
int bulbSwitch(int n)
{
    std::vector<bool> flags(n + 1);
    flags.flip();
    int result = (n > 0);
    for (int i = 2; i <= n; ++i)
    {
        for (int j = i; j <= n; j += i)
            flags[j] = !flags[j];
        result += flags[i];
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(int n, int solution, unsigned int trials = 1, bool verbose = true)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = bulbSwitch(n);
    if (verbose)
        showResult(solution == result, solution, result);
    else if (solution != result)
        showResult(false, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 1, trials);
    test(0, 0, trials);
    test(1, 1, trials);
    test(4, 2, trials);
    test(8, 2, trials);
    test(10'000'000, 3162, trials);
    test(100'000'000, 10'000, trials);
    std::cout << '\n';
    for (int i = 0; i < 10'000; ++i)
    {
        test(i, static_cast<int>(std::ceil(std::sqrt(i + 1)) - 1), trials, false);
    }
    return 0;
}


