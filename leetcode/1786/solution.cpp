#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int countRestrictedPaths(int n, std::vector<std::vector<int> > edges)
{
    constexpr int MOD = 1'000'000'007;
    std::vector<std::vector<std::pair<int, int> > > graph(n);
    
    for (const auto &edge : edges)
    {
        graph[edge[0] - 1].emplace_back(edge[1] - 1, edge[2]);
        graph[edge[1] - 1].emplace_back(edge[0] - 1, edge[2]);
    }
    int src = 0, dst = n - 1;
    std::vector<long> ways(graph.size()),
                      distance(graph.size(), std::numeric_limits<long>::max());
    struct Info
    {
        long distance = -1;
        int node = -1;
        bool operator<(const Info &other) const
        {
            return  (distance > other.distance)
                || ((distance == other.distance) && (node > other.node));
        }
    };
    std::priority_queue<Info> heap;
    
    distance[dst] = 0;
    ways[dst] = 1;
    heap.emplace(distance[dst], dst);
    while (!heap.empty())
    {
        const auto [d, u] = heap.top();
        heap.pop();
        if (d > distance[u]) continue;
        for (const auto& [v, w] : graph[u])
        {
            if (d + w < distance[v])
            {
                distance[v] = d + w;
                heap.emplace(distance[v], v);
            }
            if (distance[v] < distance[u])
                ways[u] = (ways[u] + ways[v]) % MOD;
        }
    }
    return static_cast<int>(ways[src]);
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countRestrictedPaths(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{1, 2, 3}, {1, 3, 3}, {2, 3, 1}, {1, 4, 2}, {5, 2, 2},
             {3, 5, 1}, {5, 4, 10}}, 3, trials);
    test(7, {{1, 3, 1}, {4, 1, 2}, {7, 3, 4}, {2, 5, 3}, {5, 6, 1},
             {6, 7, 2}, {7, 5, 3}, {2, 6, 4}}, 1, trials);
    return 0;
}


