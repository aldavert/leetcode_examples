#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numberOfCombinations(std::string num)
{
    if (num[0] == '0') return 0;
    constexpr int MOD = 1'000'000'007;
    const int n = static_cast<int>(num.size());
    std::vector<std::vector<long> > dp(n, std::vector<long>(n + 1));
    std::vector<std::vector<int> > lcs(n + 1, std::vector<int>(n + 1));
    
    for (int i = n - 1; i >= 0; --i)
        for (int j = i + 1; j < n; ++j)
            if (num[i] == num[j])
                lcs[i][j] = lcs[i + 1][j + 1] + 1;
    for (int i = 0; i < n; ++i)
    {
        for (int k = 1; k <= i + 1; ++k)
        {
            dp[i][k] = (dp[i][k] + dp[i][k - 1]) % MOD;
            const int s = i - k + 1;
            if (num[s] == '0') continue;
            else if (s == 0) dp[i][k] += 1;
            else if (s  < k) dp[i][k] += dp[s - 1][s];
            else
            {
                if (int l = lcs[s - k][s]; (l >= k) || (num[s - k + l] <= num[s + l]))
                    dp[i][k] += dp[s - 1][k];
                else
                    dp[i][k] += dp[s - 1][k - 1];
            }
        }
    }
    
    return static_cast<int>(dp[n - 1][n] % MOD);
}

// ############################################################################
// ############################################################################

void test(std::string num, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfCombinations(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("327", 2, trials);
    test("094", 0, trials);
    test("0", 0, trials);
    return 0;
}


