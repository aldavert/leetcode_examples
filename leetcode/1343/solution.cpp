#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numOfSubarrays(std::vector<int> arr, int k, int threshold)
{
    int result = 0;
    for (int i = 0, window_sum = 0, n = static_cast<int>(arr.size()); i < n; ++i)
    {
        window_sum += arr[i];
        if (i >= k)
            window_sum -= arr[i - k];
        result += ((i >= k - 1) && (window_sum / k >= threshold));
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr,
          int k,
          int threshold,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numOfSubarrays(arr, k, threshold);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 2, 2, 2, 5, 5, 5, 8}, 3, 4, 3, trials);
    test({11, 13, 17, 23, 29, 31, 7, 5, 2, 3}, 3, 5, 6, trials);
    return 0;
}


