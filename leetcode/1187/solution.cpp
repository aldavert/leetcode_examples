#include "../common/common.hpp"
#include <limits>
#include <unordered_map>

// ############################################################################
// ############################################################################

int makeArrayIncreasing(std::vector<int> arr1, std::vector<int> arr2)
{
    std::unordered_map<int, int> dp{{-1, 0}};
    std::sort(arr2.begin(), arr2.end());
    for (int value : arr1)
    {
        std::unordered_map<int, int> next_dp;
        auto update = [&](int key, int new_value) -> void
        {
            auto search = next_dp.find(key);
            if (search != next_dp.end())
                search->second = std::min(search->second, new_value);
            else next_dp[key] = new_value;
        };
        for (const auto& [val, steps] : dp)
        {
            if (value > val) update(value, steps);
            if (const auto it = upper_bound(arr2.begin(), arr2.end(), val);
                it != arr2.cend()) update(*it, steps + 1);
        }
        if (next_dp.empty()) return -1;
        dp = std::move(next_dp);
    }
    int result = std::numeric_limits<int>::max();
    for (const auto& [_, steps] : dp)
        result = std::min(result, steps);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr1,
          std::vector<int> arr2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = makeArrayIncreasing(arr1, arr2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 5, 3, 6, 7}, {1, 3, 2, 4}, 1, trials);
    test({1, 5, 3, 6, 7}, {4, 3, 1}, 2, trials);
    test({1, 5, 3, 6, 7}, {1, 6, 3, 3}, -1, trials);
    return 0;
}


