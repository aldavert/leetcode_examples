#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int subarrayLCM(std::vector<int> nums, int k)
{
    int result = 0;
    for (size_t i = 0; i < nums.size(); ++i)
    {
        int running_lcm = nums[i];
        for (size_t j = i; j < nums.size(); ++j)
        {
            running_lcm = std::lcm(running_lcm, nums[j]);
            if (running_lcm > k)
                break;
            if (running_lcm == k)
                ++result;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = subarrayLCM(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 6, 2, 7, 1}, 6, 4, trials);
    test({3}, 2, 0, trials);
    return 0;
}


