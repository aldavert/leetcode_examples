#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countOperations(int num1, int num2)
{
    int count = 0;
    while (num1 && num2)
    {
        if (num1 > num2) num1 -= num2;
        else num2 -= num1;
        ++count;
    }
    return count;
}

// ############################################################################
// ############################################################################

void test(int num1, int num2, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countOperations(num1, num2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, 3, 3, trials);
    test(10, 10, 1, trials);
    return 0;
}


