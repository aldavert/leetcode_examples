#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int closedIsland(std::vector<std::vector<int> > grid)
{
    const int n_rows = static_cast<int>(grid.size());
    const int n_cols = static_cast<int>(grid[0].size());
    std::vector<std::vector<bool> > visited(n_rows, std::vector<bool>(n_cols, false));
    auto fill = [&](int row, int col) -> bool
    {
        std::queue<std::tuple<int, int> > q;
        q.push({row, col});
        bool margin = false;
        while (!q.empty())
        {
            auto [r, c] = q.front();
            q.pop();
            if (visited[r][c]) continue;
            margin = margin || (r == 0) || (c == 0)
                  || (r + 1 == n_rows) || (c + 1 == n_cols);
            visited[r][c] = true;
            if ((r > 0) && (!visited[r - 1][c]) && (!grid[r - 1][c]))
                q.push({r - 1, c});
            if ((c > 0) && (!visited[r][c - 1]) && (!grid[r][c - 1]))
                q.push({r, c - 1});
            if ((r < n_rows - 1) && (!visited[r + 1][c]) && (!grid[r + 1][c]))
                q.push({r + 1, c});
            if ((c < n_cols - 1) && (!visited[r][c + 1]) && (!grid[r][c + 1]))
                q.push({r, c + 1});
        }
        return !margin;
    };
    int result = 0;
    for (int r = 0; r < n_rows; ++r)
        for (int c = 0; c < n_cols; ++c)
            if (!grid[r][c] && !visited[r][c])
                result += fill(r, c);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = closedIsland(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 1, 1, 1, 1, 1, 0},
          {1, 0, 0, 0, 0, 1, 1, 0},
          {1, 0, 1, 0, 1, 1, 1, 0},
          {1, 0, 0, 0, 0, 1, 0, 1},
          {1, 1, 1, 1, 1, 1, 1, 0}}, 2, trials);
    test({{0, 0, 1, 0, 0},
          {0, 1, 0, 1, 0},
          {0, 1, 1, 1, 0}}, 1, trials);
    test({{1, 1, 1, 1, 1, 1, 1},
          {1, 0, 0, 0, 0, 0, 1},
          {1, 0, 1, 1, 1, 0, 1},
          {1, 0, 1, 0, 1, 0, 1},
          {1, 0, 1, 1, 1, 0, 1},
          {1, 0, 0, 0, 0, 0, 1},
          {1, 1, 1, 1, 1, 1, 1}}, 2, trials);
    return 0;
}


