#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int heightChecker(std::vector<int> heights)
{
    std::vector<int> expected(heights);
    std::sort(expected.begin(), expected.end());
    const size_t n = heights.size();
    int result = 0;
    for (size_t i = 0; i < n; ++i)
        result += (heights[i] != expected[i]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> heights, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = heightChecker(heights);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 4, 2, 1, 3}, 3, trials);
    test({5, 1, 2, 3, 4}, 5, trials);
    test({1, 2, 3, 4, 5}, 0, trials);
    return 0;
}


