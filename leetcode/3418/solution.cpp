#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumAmount(std::vector<std::vector<int> > coins)
{
    const int init = -(std::numeric_limits<int>::max() / 2);
    const int m = static_cast<int>(coins.size()),
              n = static_cast<int>(coins[0].size());
    std::vector<std::vector<std::array<int, 4> > > dp(m,
            std::vector<std::array<int, 4> >(n, { init, init, init, init }));
    
    dp[0][0][2] = coins[0][0];
    if (coins[0][0] < 0) dp[0][0][1] = 0;
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            for (int k = 0; k < 3; ++k)
            {
                if (i > 0)
                    dp[i][j][k] = std::max({dp[i][j][k],
                                            dp[i - 1][j][k] + coins[i][j],
                                            dp[i - 1][j][k + 1]});
                if (j > 0)
                    dp[i][j][k] = std::max({dp[i][j][k],
                                            dp[i][j - 1][k] + coins[i][j],
                                            dp[i][j - 1][k + 1]});
            }
        }
    }
    return *std::max_element(dp[m - 1][n - 1].begin(), dp[m - 1][n - 1].end());
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > coins, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumAmount(coins);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, -1}, {1, -2, 3}, {2, -3, 4}}, 8, trials);
    test({{10, 10, 10}, {10, 10, 10}}, 40, trials);
    return 0;
}


