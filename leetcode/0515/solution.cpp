#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <limits>

// ############################################################################
// ############################################################################

std::vector<int> largestValues(TreeNode * root)
{
    std::queue<TreeNode *> queue;
    std::vector<int> result;
    
    if (root) queue.push(root);
    while (!queue.empty())
    {
        int max_value = std::numeric_limits<int>::lowest();
        for (size_t i = 0, n = queue.size(); i < n; ++i)
        {
            auto node = queue.front();
            queue.pop();
            if (node->val > max_value)
                max_value = node->val;
            if (node->left) queue.push(node->left);
            if (node->right) queue.push(node->right);
        }
        result.push_back(max_value);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        TreeNode * root = vec2tree(tree);
        result = largestValues(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 5, 3, null, 9}, {1, 3, 9}, trials);
    test({1, 2, 3}, {1, 3}, trials);
    return 0;
}


