# Find Largest Value in Each Tree Row

Given the `root` of a binary tree, return *an array of the largest value in each row* of the tree **(0-indexed)**.

#### Example 1:
> ```mermaid 
> graph TD;
> A((1))---B((3))
> A---C((2))
> B---D((5))
> B---E((3))
> C---EMPTY(( ))
> C---F((9))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef empty fill:#FFF,stroke:#FFF,stroke-width:0px;
> classDef max fill:#FDA,stroke:#000,stroke-width:2px;
> class EMPTY empty;
> class A,B,F max;
> linkStyle 4 stroke-width:0px
> ```
> *Input:* `root = [1, 3, 2, 5, 3, null, 9]`  
> *Output:* `[1, 3, 9]`

#### Example 2:
> *Input:* `root = [1, 2, 3]`  
> *Output:* `[1, 3]`

#### Constraints:
- The number of nodes in the tree will be in the range `[0, 10^4]`.
- `-2^{31} <= Node.val <= 2^{31} - 1`


