#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> countPairs(int n,
                            std::vector<std::vector<int> > edges,
                            std::vector<int> queries)
{
    std::vector<int> result, count(n + 1);
    std::vector<std::unordered_map<int, int> > shared(n + 1);
    
    for (const auto &edge : edges)
    {
        const int u = edge[0];
        const int v = edge[1];
        ++count[u];
        ++count[v];
        ++shared[std::min(u, v)][std::max(u, v)];
    }
    std::vector<int> sorted_count(count);
    std::sort(sorted_count.begin(), sorted_count.end());
    
    result.reserve(queries.size());
    for (int query : queries)
    {
        int current_result = 0;
        for (int i = 1, j = n; i < j;)
            if (sorted_count[i] + sorted_count[j] > query)
                current_result += (j--) - i;
            else
                ++i;
        for (int i = 1; i <= n; ++i)
            for (auto [j, sh] : shared[i])
                if ((count[i] + count[j] > query)
                &&  (count[i] + count[j] - sh <= query))
                    --current_result;
        result.push_back(current_result);
    }
    
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<int> queries,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPairs(n, edges, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{1, 2}, {2, 4}, {1, 3}, {2, 3}, {2, 1}}, {2, 3}, {6, 5}, trials);
    test(5, {{1, 5}, {1, 5}, {3, 4}, {2, 5}, {1, 3}, {5, 1}, {2, 3}, {2, 5}},
         {1, 2, 3, 4, 5}, {10, 10, 9, 8, 6}, trials);
    return 0;
}


