#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string gcdOfStrings(std::string str1, std::string str2)
{
    if (str1.size() < str2.size()) return gcdOfStrings(str2, str1);
    if (str1.find(str2) != 0) return "";
    if (str2.empty()) return str1;
    return gcdOfStrings(str1.substr(str2.size()), str2);
}

// ############################################################################
// ############################################################################

void test(std::string str1,
          std::string str2,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = gcdOfStrings(str1, str2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ABCABC", "ABC", "ABC", trials);
    test("ABABAB", "ABAB", "AB", trials);
    test("LEET", "CODE", "", trials);
    test("EFGABC", "ABC", "", trials);
    return 0;
}


