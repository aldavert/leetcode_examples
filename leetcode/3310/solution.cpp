#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<int> remainingMethods(int n,
                                  int k,
                                  std::vector<std::vector<int> > invocations)
{
    std::vector<std::vector<int> > graph(n);
    std::vector<int> result;
    
    for (const auto &invocation : invocations)
        graph[invocation[0]].push_back(invocation[1]);
    std::queue<int> q{{k}};
    std::vector<bool> seen(n);
    seen[k] = true;
    while (!q.empty())
    {
        for (int sz = static_cast<int>(q.size()); sz > 0; --sz)
        {
            int u = q.front();
            q.pop();
            for (int v : graph[u])
            {
                if (!seen[v])
                {
                    q.push(v);
                    seen[v] = true;
                }
            }
        }
    }
    for (int u = 0; u < n; ++u)
    {
        if (seen[u]) continue;
        for (const int v : graph[u])
        {
            if (seen[v])
            {
                result.resize(n);
                for (int i = 0; i < n; ++i) result[i] = i;
                return result;
            }
        }
        result.push_back(u);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          int k,
          std::vector<std::vector<int> > invocations,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = remainingMethods(n, k, invocations);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 1, {{1, 2}, {0, 1}, {3, 2}}, {0, 1, 2, 3}, trials);
    test(5, 0, {{1, 2}, {0, 2}, {0, 1}, {3, 4}}, {3, 4}, trials);
    test(3, 2, {{1, 2}, {0, 1}, {2, 0}}, {}, trials);
    return 0;
}


