#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int minCostToMoveChips(std::vector<int> position)
{
    int histogram[2] = {0, 0};
    const int n = static_cast<int>(position.size());
    for (int i = 0; i < n; ++i)
        ++histogram[position[i] & 1];
    return std::min(histogram[0], histogram[1]);
}
#else
int minCostToMoveChips(std::vector<int> position)
{
    int histogram[2] = {0, 0};
    for (int p : position)
        ++histogram[p & 1];
    return std::min(histogram[0], histogram[1]);
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> position, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCostToMoveChips(position);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 1, trials);
    test({2, 2, 2, 3, 3}, 2, trials);
    test({1, 1000000000}, 1, trials);
    return 0;
}


