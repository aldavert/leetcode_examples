#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countElements(std::vector<int> nums)
{
    int min_value = nums[0], min_frequency = 1,
        max_value = nums[0], max_frequency = 1;
    for (int i = 1, n = static_cast<int>(nums.size()); i < n; ++i)
    {
        if (nums[i] < min_value)
            min_value = nums[i],
            min_frequency = 0;
        if (nums[i] > max_value)
            max_value = nums[i],
            max_frequency = 0;
        min_frequency += (nums[i] == min_value);
        max_frequency += (nums[i] == max_value);
    }
    return static_cast<int>(nums.size()) - min_frequency - max_frequency
         + (min_value == max_value) * min_frequency;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countElements(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({11, 7, 2, 15}, 2, trials);
    test({-3, 3, 3, 90}, 2, trials);
    test({1, 1, 1, 1, 1}, 0, trials);
    test({1, 2, 2, 2, 1}, 0, trials);
    test({2, 1, 3, 1, 3}, 1, trials);
    return 0;
}


