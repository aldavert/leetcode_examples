#include "../common/common.hpp"
#include <queue>
#include <set>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > kClosest(std::vector<std::vector<int> > points, int k)
{
    const int n = static_cast<int>(points.size());
    std::vector<std::vector<int> > result;
    std::priority_queue<std::pair<int, int> > selected;
    
    for (int i = 0; i < n; ++i)
    {
        auto &coor = points[i];
        int distance = coor[0] * coor[0] + coor[1] * coor[1];
        if (static_cast<int>(selected.size()) < k)
            selected.push({distance, i});
        else if (distance < selected.top().first)
        {
            selected.pop();
            selected.push({distance, i});
        }
    }
    for (; !selected.empty(); selected.pop())
    {
        auto [distance, idx] = selected.top();
        result.push_back(points[idx]);
    }
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    std::set<std::pair<int, int> > lut;
    for (const auto &l : left)
        lut.insert({l[0], l[1]});
    for (const auto &r : right)
    {
        if (auto search = lut.find({r[0], r[1]}); search != lut.end())
            lut.erase(search);
        else return false;
    }
    return lut.empty();
}

void test(std::vector<std::vector<int> > points,
          int k,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = kClosest(points, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 3}, {-2, 2}}, 1, {{-2, 2}}, trials);
    test({{3, 3}, {5, -1}, {-2, 4}}, 2, {{3, 3}, {-2, 4}}, trials);
    return 0;
}


