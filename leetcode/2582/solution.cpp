#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int passThePillow(int n, int time)
{
    time = time % ((n - 1) * 2);
    return (time < n)?(time + 1):(n - (time - (n - 1)));
}

// ############################################################################
// ############################################################################

void test(int n, int time, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = passThePillow(n, time);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, 5, 2, trials);
    test(3, 2, 3, trials);
    test(18, 38, 5, trials);
    return 0;
}


