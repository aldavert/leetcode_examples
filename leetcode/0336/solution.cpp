#include "../common/common.hpp"
#include <set>
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
std::vector<std::vector<int> > palindromePairs(std::vector<std::string> words)
{
    auto isPalindrome = [](std::string word, int begin, int end) -> bool
    {
        for (int i = begin, j = end; i < j; ++i, --j)
            if (word[i] != word[j]) return false;
        return true;
    };
    const int n = static_cast<int>(words.size());
    std::vector<std::vector<int> > result;
    for (int i = 0; i < n; ++i)
    {
        const int ni = static_cast<int>(words[i].size());
        for (int j = 0; j < n; ++j)
        {
            if (int nj = static_cast<int>(words[j].size()); (i != j)
            &&  isPalindrome(words[i] + words[j], 0, ni + nj - 1))
                result.push_back({i, j});
        }
    }
    return result;
}
#elif 0
std::vector<std::vector<int> > palindromePairs(std::vector<std::string> words)
{
    auto isPalindrome = [](std::string word, int begin, int end) -> bool
    {
        for (int i = begin, j = end; i < j; ++i, --j)
            if (word[i] != word[j]) return false;
        return true;
    };
    const int n = static_cast<int>(words.size());
    std::vector<std::vector<int> > result;
    std::unordered_map<std::string, int> lut;
    for (int i = 0; i < n; ++i) lut[words[i]] = i;
    for (int i = 0; i < n; ++i)
    {
        if (words[i].size() == 0)
        {
            for (int j = 0; j < n; ++j)
            {
                if ((i != j)
                &&  isPalindrome(words[j], 0, static_cast<int>(words[j].size()) - 1))
                {
                    result.push_back({i, j});
                    result.push_back({j, i});
                }
            }
        }
        else
        {
            std::string w(words[i].rbegin(), words[i].rend());
            const int ns = static_cast<int>(w.size());
            if (auto search = lut.find(w); search != lut.end())
                if (search->second != i)
                    result.push_back({i, search->second});
            for (int j = 1; j < ns; ++j)
            {
                if (isPalindrome(w, 0, j - 1))
                {
                    if (auto search = lut.find(w.substr(j, ns - j)); search != lut.end())
                        result.push_back({i, search->second});
                }
                if (isPalindrome(w, j, ns - 1))
                {
                    if (auto search = lut.find(w.substr(0, j)); search != lut.end())
                        result.push_back({search->second, i});
                }
            }
        }
    }
    return result;
}
#else
std::vector<std::vector<int> > palindromePairs(std::vector<std::string> words)
{
    auto isPalindrome = [](const std::string &s, int l, int r) -> bool
    {
        for (; l <= r; ++l, --r)
            if (s[l] != s[r])
                return false;
        return true;
    };
    const int n = static_cast<int>(words.size());
    std::unordered_map<std::string, int> straight;
    std::unordered_map<std::string, int> rev;
    for (int i = 0; i < n; ++i)
    {
        straight[words[i]] = i;
        std::string u = words[i];
        std::reverse(u.begin(), u.end());
        rev[u] = i;
    }
    std::set<std::vector<int> > result;
    for (int i = 0; i < n; ++i)
    {
        const int m = static_cast<int>(words[i].size()) - 1;
        std::string t = "";
        auto update = [&](std::unordered_map<std::string, int> &lut,
                          int begin, int end, bool reverse) -> void
        {
            if (auto search = lut.find(t); (search != lut.end())
                                        && (search->second != i)
                                        &&  isPalindrome(words[i], begin, end))
            {
                if (reverse) result.insert({i, search->second});
                else         result.insert({search->second, i});
            }
        };
        update(rev, 0, m, true);
        for (int j = 0; j <= m; ++j)
        {
            t += words[i][j];
            update(rev, j + 1, m, true);
        }
        t = "";
        update(straight, 0, m, false);
        for (int j = m; j >= 0; --j)
        {
            t += words[i][j];
            update(straight, 0, j - 1, false);
        }
    }
    return std::vector<std::vector<int> >(result.begin(), result.end());
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    std::set<std::pair<int, int> > left_unique, right_unique;
    for (const auto &v : left)
        left_unique.insert({v[0], v[1]});
    for (const auto &v : right)
        right_unique.insert({v[0], v[1]});
    if (left_unique.size() != left.size()) return false;
    if (right_unique.size() != right.size()) return false;
    for (auto left_begin = left_unique.begin(), right_begin = right_unique.begin(),
              end = left_unique.end(); left_begin != end; ++left_begin, ++right_begin)
        if ((left_begin->first != right_begin->first)
        ||  (left_begin->second != right_begin->second))
            return false;
    return true;
}

void test(std::vector<std::string> words,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = palindromePairs(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10000000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"abcd", "dcba", "lls", "s", "sssll"},
         {{0, 1}, {1, 0}, {3, 2}, {2, 4}}, trials);
    test({"bat", "tab", "cat"}, {{0, 1}, {1, 0}}, trials);
    test({"a", ""}, {{0, 1}, {1, 0}}, trials);
    
    return 0;
}


