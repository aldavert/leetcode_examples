#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool exist(std::vector<std::vector<char> > board, std::string word)
{
    const int m = static_cast<int>(board.size()),
              n = static_cast<int>(board[0].size()),
              ws = static_cast<int>(word.size());
    auto search = [&](auto &&self, int x, int y, int p) -> bool
    {
        if (p == ws) return true;
        if ((x < 0) || (x >= m) || (y < 0) || (y >= n)) return false;
        
        bool result = false;
        if (word[p] == board[x][y])
        {
            board[x][y] = '%';
            result = self(self, x - 1, y    , p + 1)
                  || self(self, x    , y - 1, p + 1)
                  || self(self, x + 1, y    , p + 1)
                  || self(self, x    , y + 1, p + 1);
            board[x][y] = word[p];
        }
        return result;
    };
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (search(search, i, j, 0))
                return true;
    return false;
}
#else
bool exist(std::vector<std::vector<char> > board, std::string word)
{
    const int m = static_cast<int>(board.size()),
              n = static_cast<int>(board[0].size()),
              ws = static_cast<int>(word.size());
    std::vector<std::vector<bool> > visited(m);
    for (int i = 0; i < m; ++i)
        visited[i].resize(n, false);
    auto search = [&](auto &&self, int x, int y, int p) -> bool
    {
        if (p == ws) return true;
        if ((x < 0) || (x >= m) || (y < 0) || (y >= n)) return false;
        if (visited[x][y]) return false;
        
        if (word[p] == board[x][y])
        {
            visited[x][y] = true;
            ++p;
            if (self(self, x - 1, y    , p)) return true;
            if (self(self, x    , y - 1, p)) return true;
            if (self(self, x + 1, y    , p)) return true;
            if (self(self, x    , y + 1, p)) return true;
            visited[x][y] = false;
        }
        return false;
    };
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (search(search, i, j, 0))
                return true;
    return false;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<char> > board,
          std::string word,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = exist(board, word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'A', 'B', 'C', 'E'},
          {'S', 'F', 'C', 'S'},
          {'A', 'D', 'E', 'E'}}, "ABCCED", true, trials);
    test({{'A', 'B', 'C', 'E'},
          {'S', 'F', 'C', 'S'},
          {'A', 'D', 'E', 'E'}}, "SEE", true, trials);
    test({{'A', 'B', 'C', 'E'},
          {'S', 'F', 'C', 'S'},
          {'A', 'D', 'E', 'E'}}, "ABCB", false, trials);
    return 0;
}


