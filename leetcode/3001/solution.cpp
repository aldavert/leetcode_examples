#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minMovesToCaptureTheQueen(int a, int b, int c, int d, int e, int f)
{
    if (a == e)
        return 1 + ((c == a) && (((b < d) && (d < f)) || ((b > d) && (d > f))));
    if (b == f)
        return 1 + ((d == f) && (((a < c) && (c < e)) || ((a > c) && (c > e))));
    if (c + d == e + f)
        return 1 + ((a + b == c + d) && (((c < a) && (a < e)) || ((c > a) && (a > e))));
    if (c - d == e - f)
        return 1 + ((a - b == c - d) && (((c < a) && (a < e)) || ((c > a) && (a > e))));
    return 2;
}

// ############################################################################
// ############################################################################

void test(int a, int b, int c, int d, int e, int f, int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minMovesToCaptureTheQueen(a, b, c, d, e, f);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 1, 8, 8, 2, 3, 2, trials);
    test(5, 3, 3, 4, 5, 2, 1, trials);
    return 0;
}


