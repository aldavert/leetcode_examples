#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countHomogenous(std::string s)
{
    constexpr long MOD = 1'000'000'007;
    long result = 0, counter = 0;
    for (char previous = '\0'; char current : s)
    {
        if (previous != current)
        {
            result = (result + counter * (counter + 1) / 2) % MOD;
            counter = 0;
        }
        previous = current;
        ++counter;
    }
    return static_cast<int>((result + counter * (counter + 1) / 2) % MOD);
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countHomogenous(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abbcccaa", 13, trials);
    test("xy", 2, trials);
    test("zzzzz", 15, trials);
    test(std::string(100'000, 'w'), 49965, trials);
    return 0;
}


