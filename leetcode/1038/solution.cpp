#include "../common/common.hpp"
#include "../common/tree.hpp"

// ############################################################################
// ############################################################################

TreeNode * bstToGst(TreeNode * root)
{
    auto process = [&](void) -> void
    {
        int sum = 0;
        auto inner = [&](auto &&self, TreeNode * current) -> void
        {
            if (current)
            {
                self(self, current->right);
                sum += current->val;
                current->val = sum;
                self(self, current->left);
            }
        };
        inner(inner, root);
    };
    process();
    return root;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        root = bstToGst(root);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 1, 6, 0, 2, 5, 7, null, null, null, 3, null, null, null, 8},
         {30, 36, 21, 36, 35, 26, 15, null, null, null, 33, null, null, null, 8},
         trials);
    test({0, null, 1}, {1, null, 1}, trials);
    return 0;
}

