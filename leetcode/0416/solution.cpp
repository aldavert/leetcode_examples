#include "../common/common.hpp"
#include <bitset>
#include <numeric>

#if 1
bool canPartition(std::vector<int> nums)
{
    int sum = std::accumulate(nums.begin(), nums.end(), 0);
    if (sum & 1) return false;
    std::bitset<10001> dp(1); 
    int half = sum / 2;
    for (int i : nums) 
    {
        dp |= dp << i;
        if (dp[half]) return true;
    }
    return dp[half];
}
#else
bool canPartition(std::vector<int> nums)
{
    // You only need to find a group of numbers that add up to half of the vector
    // values sum (target). The remaining numbers will be the other subset.
    int t = 0;
    for (int v : nums)
        t += v;
    if (t % 2 != 0) return false;
    t /= 2;
    std::unordered_set<int> failed_states;
    std::sort(nums.begin(), nums.end());
    const int n = static_cast<int>(nums.size());
    auto solve = [&](auto &&self, int position, int target) -> bool
    {
        if (target == 0) return true;
        if ((target < 0) || (position >= n)) return false;
        const int call_id = target << 8 | position;
        if (failed_states.find(call_id) != failed_states.end())
            return false;
        failed_states.insert(call_id);
        
        // When found, skip the next level of the recursion.
        if (self(self, position + 1, target))
            return true;
        return self(self, position + 1, target - nums[position]);
    };
    return solve(solve, 0, t);
}
#endif

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = canPartition(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 5, 11, 5}, true, trials);
    test({2, 3, 5, 100, 100}, true, trials);
    test({2, 3, 5, 25, 25, 100, 100}, true, trials);
    test({1, 2, 3, 5}, false, trials);
    test({23, 13, 11, 7, 6, 5, 5}, true, trials);
    test({51,54,10,76,88,53,32,37,61,43,10,26,52,26,21,65,70,94,87,14,48,
          10,18, 6,63,57,69,98,45,36,58,39,90,80,58,16,54, 5,88,54,19,38,
          68,33,38,42,83,28, 2,97, 7,49,11,67,93,20,90,10,60,19,99,22,61,
          98,78,21,46,73,17,77,65,81,46,16,12,70,13,19, 2,88,83,42,70,73,
          95,49,61,31,66,50,14,66,73,69, 7,18,69,57,12,83}, true, trials);
    test({1, 2, 3, 5, 17, 6, 14, 12, 6}, true, trials);
    return 0;
}


