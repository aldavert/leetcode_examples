#include "../common/common.hpp"
#include <map>

// ############################################################################
// ############################################################################

int minCost(std::vector<int> nums)
{
    auto order = [](int a, int b, int c) -> std::tuple<int, int, int>
    {
        if (a > b) std::swap(a, b);
        if (b > c) std::swap(b, c);
        if (a > b) std::swap(a, b);
        return {a, b, c};
    };
    std::map<int, int> dp = {{nums[0], 0}};
    int base = 0;
    if (nums.size() % 2 == 0)
        base = nums.back(),
        nums.pop_back();
    for (int i = 1, n = static_cast<int>(nums.size()); i < n; i += 2)
    {
        std::map<int, int> dp_next;
        auto push = [&](int carry, int value) -> void
        {
            if (auto search = dp_next.find(carry); search != dp_next.end())
                search->second = std::min(search->second, value);
            else dp_next[carry] = value;
        };
        int x = nums[i], y = nums[i + 1];
        for (auto [carry, value] : dp)
        {
            auto [a, b, c] = order(x, y, carry);
            push(a, value + c);
            push(c, value + b);
        }
        swap(dp, dp_next);
    }
    int result = std::numeric_limits<int>::max();
    for (auto [carry, value] : dp)
        result = std::min(result, std::max(carry, base) + value);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCost(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({6, 2, 8, 4}, 12, trials);
    test({2, 1, 3, 3}, 5, trials);
    return 0;
}


