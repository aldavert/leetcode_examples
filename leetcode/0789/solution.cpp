#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool escapeGhosts(std::vector<std::vector<int> > ghosts, std::vector<int> target)
{
    int d_target = std::abs(target[0]) + std::abs(target[1]);
    for (const auto &ghost : ghosts)
        if (d_target >= std::abs(target[0] - ghost[0]) + std::abs(target[1] - ghost[1]))
            return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > ghosts,
          std::vector<int> target,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = escapeGhosts(ghosts, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0}, {0, 3}}, {0, 1}, true, trials);
    test({{1, 0}}, {2, 0}, false, trials);
    test({{2, 0}}, {1, 0}, false, trials);
    return 0;
}


