#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countPoints(std::string rings)
{
    char rods[10] = {};
    for (size_t i = 0, n = rings.size(); i < n; i += 2)
        rods[rings[i + 1] - '0'] |= static_cast<char>(((rings[i] == 'R') << 0)
                                 |                    ((rings[i] == 'G') << 1)
                                 |                    ((rings[i] == 'B') << 2));
    int result = 0;
    for (int i = 0; i < 10; ++i) result += rods[i] == 7;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string rings, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPoints(rings);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("B0B6G0R6R0R6G9", 1, trials);
    test("B0R0G0R9R0B0G0", 1, trials);
    test("G4", 0, trials);
    return 0;
}


