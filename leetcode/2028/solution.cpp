#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

std::vector<int> missingRolls(std::vector<int> rolls, int mean, int n)
{
    int missing_sum = static_cast<int>(rolls.size() + n) * mean
                    - std::accumulate(rolls.begin(), rolls.end(), 0);
    if ((missing_sum > n * 6) || (missing_sum < n))
        return {};
    std::vector<int> result(n, missing_sum / n);
    missing_sum %= n;
    for (int i = 0; i < missing_sum; ++i)
        ++result[i];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> rolls,
          int mean,
          int n,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = missingRolls(rolls, mean, n);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 4, 3}, 4, 2, {6, 6}, trials);
    test({1, 5, 6}, 3, 4, {3, 2, 2, 2}, trials);
    test({1, 2, 3, 4}, 6, 4, {}, trials);
    return 0;
}


