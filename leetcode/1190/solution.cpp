#include "../common/common.hpp"
#include <unordered_map>
#include <stack>

// ############################################################################
// ############################################################################

std::string reverseParentheses(std::string s)
{
    const int n = static_cast<int>(s.size());
    std::unordered_map<int, int> pair;
    std::stack<int> stack;
    
    for (int i = 0; i < n; ++i)
    {
        if (s[i] == '(') stack.push(i);
        else if (s[i] == ')')
        {
            pair[i] = stack.top();
            pair[stack.top()] = i;
            stack.pop();
        }
    }
    std::string result;
    for (int i = 0, d = 1; i < n; i += d)
    {
        if (s[i] == '(' || s[i] == ')')
        {
            i = pair[i];
            d = -d;
        }
        else result += s[i];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = reverseParentheses(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("(abcd)", "dcba", trials);
    test("(u(love)i)", "iloveu", trials);
    test("(ed(et(oc))el)", "leetcode", trials);
    return 0;
}


