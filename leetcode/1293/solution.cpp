#include "../common/common.hpp"
#include <cstring>
#include <queue>
#include <limits>

// ############################################################################
// ############################################################################

#if 1
int shortestPath(std::vector<std::vector<int> > grid, int k)
{
    const int m = static_cast<int>(grid.size());
    const int n = static_cast<int>(grid[0].size());
    short visited[41][41];
    std::memset(visited, -1, sizeof(visited));
    struct Info
    {
        short x;
        short y;
        short distance;
        short jumps;
    };
    std::queue<Info> q;
    q.push({0, 0, 0, static_cast<short>(k)});
    while (!q.empty())
    {
        auto [x, y, distance, jumps] = q.front();
        q.pop();
        
        if ((x < 0) || (x >= m) || (y < 0) || (y >= n))
            continue;
        if ((x == m - 1) && (y == n - 1))
            return distance;
        if (grid[x][y] == 1)
        {
            if (jumps <= 0) continue;
            --jumps;
        }
        
        if ((visited[x][y] != -1) && (visited[x][y] >= jumps))
            continue;
        visited[x][y] = jumps;
        
        ++distance;
        q.push({static_cast<short>(x + 1), static_cast<short>(y    ), distance, jumps});
        q.push({static_cast<short>(x - 1), static_cast<short>(y    ), distance, jumps});
        q.push({static_cast<short>(x    ), static_cast<short>(y + 1), distance, jumps});
        q.push({static_cast<short>(x    ), static_cast<short>(y - 1), distance, jumps});
    }
    return -1;
}
#elif 0
int shortestPath(std::vector<std::vector<int> > grid, int k)
{
    const int m = static_cast<int>(grid.size());
    const int n = static_cast<int>(grid[0].size());
    short distance[41][41][(41 * 41) + 1];
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            for (int o = 0; o <= k; ++o)
                distance[i][j][o] = std::numeric_limits<short>::max();
    struct Info
    {
        short x;
        short y;
        short distance;
        short jumps;
    };
    std::queue<Info> q;
    q.push({0, 0, static_cast<short>(k), 0});
    while (!q.empty())
    {
        auto [x, y, r, step] = q.front();
        q.pop();
        if ((x < 0) || (y < 0) || (x >= m) || (y >= n)) continue;
        if (grid[x][y])
        {
            if (r == 0) continue;
            --r;
        }
        short min_distance = distance[x][y][r];
        for (int i = r; i <= k; ++i)
            min_distance = std::min(min_distance, distance[x][y][i]);
        if (step >= min_distance) continue;
        distance[x][y][r] = std::min(distance[x][y][r], step);
        if ((x == m - 1) && (y == n - 1)) continue;
        ++step;
        q.push({static_cast<short>(x - 1), static_cast<short>(y    ), r, step});
        q.push({static_cast<short>(x + 1), static_cast<short>(y    ), r, step});
        q.push({static_cast<short>(x    ), static_cast<short>(y - 1), r, step});
        q.push({static_cast<short>(x    ), static_cast<short>(y + 1), r, step});
    }
    short result = distance[m - 1][n - 1][k];
    for (int i = 0; i < k; ++i)
        result = std::min(result, distance[m - 1][n - 1][i]);
    return (result < m * n)?result:-1;
}
#elif 0
int shortestPath(std::vector<std::vector<int> > grid, int k)
{
    const int m = static_cast<int>(grid.size());
    const int n = static_cast<int>(grid[0].size());
    std::vector<std::vector<std::vector<short> > > distance(m);
    
    auto traverse = [&](auto &&self, int x, int y, int r, short step) -> void
    {
        if ((x < 0) || (y < 0) || (x >= m) || (y >= n)) return;
        if (grid[x][y])
        {
            if (r == 0)
                return;
            --r;
        }
        short min_distance = distance[x][y][r];
        for (int i = r; i <= k; ++i)
            min_distance = std::min(min_distance, distance[x][y][i]);
        if (step >= min_distance) return;
        distance[x][y][r] = std::min(distance[x][y][r], step);
        if ((x == m - 1) && (y == n - 1)) return;
        self(self, x - 1, y    , r, step + 1);
        self(self, x + 1, y    , r, step + 1);
        self(self, x    , y - 1, r, step + 1);
        self(self, x    , y + 1, r, step + 1);
    };
    for (int i = 0; i < m; ++i)
    {
        distance[i].resize(n);
        for (int j = 0; j < n; ++j)
            distance[i][j].resize(k + 1, std::numeric_limits<short>::max());
    }
    traverse(traverse, 0, 0, k, 0);
    short result = distance[m - 1][n - 1][k];
    for (int i = 0; i < k; ++i)
        result = std::min(result, distance[m - 1][n - 1][i]);
    return (result < m * n)?result:-1;
}
#else
int shortestPath(std::vector<std::vector<int> > grid, int k)
{
    const int m = static_cast<int>(grid.size());
    const int n = static_cast<int>(grid[0].size());
    std::vector<std::vector<std::vector<short> > > distance(m);
    for (int i = 0; i < m; ++i)
    {
        distance[i].resize(n);
        for (int j = 0; j < n; ++j)
            distance[i][j].resize(k + 1, std::numeric_limits<short>::max());
    }
    auto lambda_traverse = [&](auto &&self, int x, int y, int r, short step) -> void
    {
        if ((x < 0) || (y < 0) || (x >= m) || (y >= n)) return;
        if (grid[x][y])
        {
            if (r == 0)
                return;
            --r;
        }
        short min_distance = distance[x][y][r];
        for (int i = r; i <= k; ++i)
            min_distance = std::min(min_distance, distance[x][y][i]);
        if (step >= min_distance) return;
        distance[x][y][r] = std::min(distance[x][y][r], step);
        if ((x == m - 1) && (y == n - 1)) return;
        self(self, x - 1, y    , r, step + 1);
        self(self, x + 1, y    , r, step + 1);
        self(self, x    , y - 1, r, step + 1);
        self(self, x    , y + 1, r, step + 1);
    };
    lambda_traverse(lambda_traverse, 0, 0, k, 0);
    short result = distance[m - 1][n - 1][k];
    for (int i = 0; i < k; ++i)
        result = std::min(result, distance[m - 1][n - 1][i]);
    return (result < m * n)?result:-1;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestPath(grid, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 100'000;
    constexpr unsigned int trials = 100;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0, 0},
          {1, 1, 0},
          {0, 0, 0},
          {0, 1, 1},
          {0, 0, 0}}, 1, 6, trials);
    test({{0, 1, 1},
          {1, 1, 1},
          {1, 0, 0}}, 1, -1, trials);
    test({{0, 0, 0},
          {1, 1, 0},
          {0, 0, 0},
          {0, 1, 1},
          {1, 1, 1},
          {0, 0, 0}}, 1, 11, trials);
    test({{0, 0, 0, 0, 0, 0, 0, 0, 0},
          {1, 1, 1, 1, 1, 1, 1, 1, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 1, 1, 1, 1, 1, 1, 1, 1},
          {0, 0, 0, 0, 0, 0, 0, 0, 0},
          {1, 1, 1, 1, 1, 1, 1, 1, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 1, 1, 1, 1, 1, 1, 1, 1},
          {0, 0, 0, 0, 0, 0, 0, 0, 0},
          {1, 1, 1, 1, 1, 1, 1, 1, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 1, 1, 1, 1, 1, 1, 1, 1},
          {1, 1, 1, 1, 1, 1, 1, 1, 1},
          {1, 1, 1, 1, 1, 1, 1, 1, 1},
          {0, 0, 0, 0, 0, 0, 0, 0, 0}}, 2, 70, trials);
    test({{0, 0, 0, 0, 0, 0, 0, 0, 0},
          {1, 1, 1, 1, 1, 1, 1, 1, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 1, 1, 1, 1, 1, 1, 1, 1},
          {0, 0, 0, 0, 0, 0, 0, 0, 0},
          {1, 1, 1, 1, 1, 1, 1, 1, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 1, 1, 1, 1, 1, 1, 1, 1},
          {0, 0, 0, 0, 0, 0, 0, 0, 0},
          {1, 1, 1, 1, 1, 1, 1, 1, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0},
          {0, 1, 1, 1, 1, 1, 1, 1, 1},
          {1, 1, 1, 1, 1, 1, 1, 1, 1},
          {1, 1, 1, 1, 1, 1, 1, 1, 1},
          {0, 0, 0, 0, 0, 0, 0, 0, 0}}, 3, 54, trials);
    test({{0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1,
           1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1},
          {0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1,
           1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1},
          {0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1,
           1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1},
          {0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1,
           1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1},
          {1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1,
           0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 1},
          {1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1,
           1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1},
          {0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1,
           1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1},
          {0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1,
           1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0},
          {0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1,
           0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0},
          {0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0,
           1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0},
          {1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0,
           1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1},
          {0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0,
           1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0},
          {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1,
           0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1},
          {0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1,
           1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1},
          {1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1,
           1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1},
          {1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0,
           1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1},
          {1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0,
           0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1},
          {0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0,
           1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1},
          {1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0,
           1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1},
          {0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1,
           1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0},
          {0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0,
           0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1},
          {0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1,
           0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0},
          {1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1,
           1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1},
          {0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0,
           1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0}}, 617, 62, trials);
    return 0;
}


