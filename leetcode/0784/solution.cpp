#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::string> letterCasePermutation(std::string s)
{
    std::transform(s.begin(), s.end(), s.begin(),
                   [](char c) { return static_cast<char>(std::tolower(c)); });
    std::vector<std::string> result;
    do
    {
        result.push_back(s);
        for (char &symbol : s)
        {
            if (std::isalpha(symbol))
            {
                if (std::islower(symbol))
                {
                    symbol = static_cast<char>(std::toupper(symbol));
                    break;
                }
                symbol = static_cast<char>(std::tolower(symbol));
            }
        }
    } while (result.front() != s);
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    std::vector<std::string> left_work(left), right_work(right);
    std::sort(left_work.begin(), left_work.end());
    std::sort(right_work.begin(), right_work.end());
    const size_t n = left_work.size();
    for (size_t i = 0; i < n; ++i)
        if (left_work[i] != right_work[i])
            return false;
    return true;
}

void test(std::string s,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = letterCasePermutation(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("a1b2", {"a1b2", "a1B2", "A1b2", "A1B2"}, trials);
    test("3z4", {"3z4", "3Z4"}, trials);
    return 0;
}


