#include "../common/common.hpp"
#include "../common/list.hpp"

// ############################################################################
// ############################################################################

ListNode * getIntersectionNode(ListNode * headA, ListNode * headB)
{
    ListNode * a = headA;
    ListNode * b = headB;
    while (a != b)
    {
        a = a?a->next:headB;
        b = b?b->next:headA;
    }
    return a;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> listA,
          std::vector<int> listB,
          int solution,
          unsigned int trials = 1)
{
    int skipA = static_cast<int>(listA.size());
    int skipB = static_cast<int>(listB.size());
    if (solution != 0)
    {
        for (size_t i = 0; i < listA.size(); ++i)
        {
            if (listA[i] == solution)
            {
                skipA = static_cast<int>(i);
                break;
            }
        }
        for (size_t i = 0; i < listB.size(); ++i)
        {
            if (listB[i] == solution)
            {
                skipB = static_cast<int>(i);
                break;
            }
        }
    }
    int result = 0;
    for (unsigned int t = 0; t < trials; ++t)
    {
        ListNode * headA = vec2list(listA);
        if (skipB == 0)
        {
            ListNode * headB = headA;
            for (int i = 0; i < skipA; ++i)
                headB = headB->next;
            ListNode * intersec_node = getIntersectionNode(headA, headB);
            result = (intersec_node)?intersec_node->val:0;
        }
        else
        {
            ListNode * headB = vec2list(listB);
            ListNode * redirect = headB, * dest = headA;
            for (int i = 0; i < skipA; ++i) dest = dest->next;
            for (int i = 1; i < skipB; ++i) redirect = redirect->next;
            ListNode * backup = redirect->next;
            redirect->next = dest;
            ListNode * intersec_node = getIntersectionNode(headA, headB);
            result = (intersec_node)?intersec_node->val:0;
            redirect->next = backup;
            delete headB;
        }
        delete headA;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 1, 8, 4, 5}, {5, 6, 1, 8, 4, 5}, 8, trials);
    test({1, 9, 1, 2, 4}, {3, 2, 4}, 2, trials);
    test({2, 6, 4}, {1, 5}, 0, trials);
    return 0;
}


