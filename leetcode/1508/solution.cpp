#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int rangeSum(std::vector<int> nums, int n, int left, int right)
{
    constexpr int MOD = 1'000'000'007;
    
    auto sumNoGreaterThan = [&](int m) -> std::pair<int, long>
    {
        int count = 0, sum = 0, window = 0;
        long total = 0;
        
        for (int i = 0, j = 0; j < n; ++j)
        {
            sum += nums[j] * (j - i + 1);
            window += nums[j];
            while (window > m)
            {
                sum -= window;
                window -= nums[i++];
            }
            count += j - i + 1;
            total += sum;
        }
        return {count, total};
    };
    const int L = *std::min_element(nums.begin(), nums.end()),
              R = std::accumulate(nums.begin(), nums.end(), 0);
    auto firstKSum = [&](int k) -> long
    {
        int l = L, r = R;
        while (l < r)
        {
            if (int m = l + (r - l) / 2; sumNoGreaterThan(m).first < k)
                l = m + 1;
            else r = m;
        }
        const auto& [count, total] = sumNoGreaterThan(l);
        return total - l * (count - k);
    };
    return static_cast<int>((firstKSum(right) - firstKSum(left - 1)) % MOD);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          int n,
          int left,
          int right,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = rangeSum(nums, n, left, right);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 4, 1,  5, 13, trials);
    test({1, 2, 3, 4}, 4, 3,  4,  6, trials);
    test({1, 2, 3, 4}, 4, 1, 10, 50, trials);
    test({7, 5, 8, 5, 6, 4, 3, 3}, 8, 2, 6, 23, trials);
    return 0;
}


