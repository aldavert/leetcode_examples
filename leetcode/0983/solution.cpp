#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int mincostTickets(std::vector<int> days, std::vector<int> costs)
{
    const int n = static_cast<int>(days.size());
    std::vector<int> minimum_cost(365 + 31, n * costs[0]);
    int result = 0, day = 0;
    for (int k = 0; k < n; ++k)
    {
        for (; day < days[k]; ++day)
            result = std::min(result, minimum_cost[day]);
        minimum_cost[day     ] = std::min(minimum_cost[day     ], result + costs[0]);
        minimum_cost[day +  6] = std::min(minimum_cost[day +  6], result + costs[1]);
        minimum_cost[day + 29] = std::min(minimum_cost[day + 29], result + costs[2]);
        result = minimum_cost[day];
    }
    for (; day < 365 + 31; ++day)
        result = std::min(result, minimum_cost[day]);
    return result;
}
#else
int mincostTickets(std::vector<int> days, std::vector<int> costs)
{
    const int n = static_cast<int>(days.size());
    int result = static_cast<int>(days.size())
               * std::max({costs[0], costs[1], costs[2]});
    std::vector<int> minimum_cost(days.size(), result);
    auto process = [&](auto &&self, int index, int cost) -> void
    {
        if (index >= n)
        {
            result = std::min(result, cost);
            return;
        }
        if ((cost >= result) || (cost >= minimum_cost[index])) return;
        minimum_cost[index] = cost;
        self(self, index + 1, cost + costs[0]);
        int next = index + 1;
        while ((next < n) && (days[next] - days[index] <  7)) ++next;
        self(self, next, cost + costs[1]);
        while ((next < n) && (days[next] - days[index] < 30)) ++next;
        self(self, next, cost + costs[2]);
    };
    process(process, 0, 0);
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> days,
          std::vector<int> costs,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = mincostTickets(days, costs);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 4, 6, 7, 8, 20}, {2, 7, 15}, 11, trials);
    test({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 30, 31}, {2, 7, 15}, 17, trials);
    test({ 2,  3,  5,  6,  7,  8,  9, 10, 11, 17, 18, 19, 23, 26, 27, 29, 31,
          32, 33, 34, 35, 36, 38, 39, 40, 41, 42, 43, 44, 45, 47, 51, 54, 55,
          57, 58, 64, 65, 67, 68, 72, 73, 74, 75, 77, 78, 81, 86, 87, 88, 89,
          91, 93, 94, 95, 96, 98, 99}, {5, 24, 85}, 246, trials);
    return 0;
}


