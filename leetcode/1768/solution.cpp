#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string mergeAlternately(std::string word1, std::string word2)
{
    std::string result;
    size_t m = std::min(word1.size(), word2.size());
    for (size_t i = 0; i < m; ++i)
        result += word1[i],
        result += word2[i];
    return result + word1.substr(m) + word2.substr(m);
}

// ############################################################################
// ############################################################################

void test(std::string word1,
          std::string word2,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = mergeAlternately(word1, word2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", "pqr", "apbqcr", trials);
    test("ab", "pqrs", "apbqrs", trials);
    test("abcd", "pq", "apbqcd", trials);
    return 0;
}


