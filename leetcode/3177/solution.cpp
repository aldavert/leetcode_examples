#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
int maximumLength(std::vector<int> nums, int k)
{
    int result = 0, max_dp[51] = {}, dp[5000][51] = {};
    std::unordered_map<int, int> prev;
    for (int i = static_cast<int>(nums.size()) - 1; i >= 0; --i)
    {
        auto it = prev.find(nums[i]);
        int p = (it != prev.end())?it->second:i;
        for (int j = k; j >= 0; --j)
        {
            dp[i][j] = std::max(1 + ((i != p)?dp[p][j]:0),
                                1 + ((j  > 0)?max_dp[j - 1]:0));
            max_dp[j] = std::max(max_dp[j], dp[i][j]);  
        }     
        prev[nums[i]] = i;
        result = std::max(result, dp[i][k]);
    } 
    return result;
}
#else
int maximumLength(std::vector<int> nums, int k)
{
    std::vector<std::unordered_map<int, int> > dp(k + 1);
    std::vector<int> max_len(k + 1);
    
    for (int num : nums)
    {
        for (int count = k; count >= 0; --count)
        {
            ++dp[count][num];
            if (count > 0)
                dp[count][num] = std::max(dp[count][num], max_len[count - 1] + 1);
            max_len[count] = std::max(max_len[count], dp[count][num]);
        }
    }
    return max_len[k];
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumLength(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 1, 1, 3}, 2, 4, trials);
    test({1, 2, 3, 4, 5, 1}, 0, 2, trials);
    return 0;
}


