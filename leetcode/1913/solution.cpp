#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int maxProductDifference(std::vector<int> nums)
{
    int maximum[2] = { std::numeric_limits<int>::lowest(),
                       std::numeric_limits<int>::lowest() };
    int minimum[2] = { std::numeric_limits<int>::max(),
                       std::numeric_limits<int>::max() };
    for (int value : nums)
    {
        if (value > maximum[0])
            maximum[1] = std::exchange(maximum[0], value);
        else if (value > maximum[1])
            maximum[1] = value;
        if (value < minimum[0])
            minimum[1] = std::exchange(minimum[0], value);
        else if (value < minimum[1])
            minimum[1] = value;
    }
    return maximum[0] * maximum[1] - minimum[0] * minimum[1];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxProductDifference(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 6, 2, 7, 4}, 34, trials);
    test({4, 2, 5, 9, 7, 4, 8}, 64, trials);
    return 0;
}


