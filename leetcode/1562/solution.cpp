#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findLatestStep(std::vector<int> arr, int m)
{
    const int n = static_cast<int>(arr.size());
    if (n == m) return n; 
    int result = -1, step = 0;
    std::vector<int> sizes(n + 2);
    
    for (int i : arr)
    {
        ++step;
        if ((sizes[i - 1] == m) || (sizes[i + 1] == m))
            result = step - 1;
        int head = i - sizes[i - 1], tail = i + sizes[i + 1];
        sizes[head] = tail - head + 1;
        sizes[tail] = tail - head + 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int m, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLatestStep(arr, m);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 5, 1, 2, 4}, 1, 4, trials);
    test({3, 1, 5, 4, 2}, 2, -1, trials);
    return 0;
}


