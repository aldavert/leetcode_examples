#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
std::vector<int> majorityElement(std::vector<int> nums)
{
    const int threshold = static_cast<int>(nums.size()) / 3;
    int largest[2] = {2'000'000'000, 2'000'000'000}, count[2] = {};
    for (int number : nums)
    {
        if (!count[0] && (number != largest[1]))
            largest[0] = number, count[0] = 1;
        else if (!count[1] && (number != largest[0]))
            largest[1] = number, count[1] = 1;
        else if (number == largest[0]) ++count[0];
        else if (number == largest[1]) ++count[1];
        else
        {
            --count[0];
            --count[1];
        }
    }
    count[0] = count[1] = 0;
    for (int number : nums)
    {
        count[0] += number == largest[0];
        count[1] += number == largest[1];
    }
    std::vector<int> result;
    if (count[0] > threshold) result.push_back(largest[0]);
    if (count[1] > threshold) result.push_back(largest[1]);
    return result;
}
#else
std::vector<int> majorityElement(std::vector<int> nums)
{
    const int thr = static_cast<int>(nums.size()) / 3;
    std::unordered_map<int, int> histogram;
    for (int number : nums)
        ++histogram[number];
    std::vector<int> result;
    for (auto [number, frequency] : histogram)
        if (frequency > thr)
            result.push_back(number);
    std::sort(result.begin(), result.end());
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = majorityElement(nums);
    std::sort(solution.begin(), solution.end());
    std::sort(result.begin(), result.end());
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 3}, {3}, trials);
    test({1}, {1}, trials);
    test({1, 2}, {1, 2}, trials);
    test({1, 2, 3}, {}, trials);
    test({1, 1, 1, 1, 1}, {1}, trials);
    test({1, 1, 1, 1, 1, 2, 2, 2, 2, 2}, {1, 2}, trials);
    test({1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3}, {}, trials);
    test({1, 1, 1, 1, 1, 2, 2, 2, 2}, {1, 2}, trials);
    test({1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3}, {1, 3}, trials);
    test({1, 1, 1, 1, 2, 2, 2, 3, 3, 3}, {1}, trials);
    test({1, 1, 1, 1, 2, 3, 4, 5, 6, 7, 8}, {1}, trials);
    test({2, 3, 4, 5, 6, 7, 1, 1, 1, 1}, {1}, trials);
    test({0, 0, 0, 0, 0}, {0}, trials);
    test({1, 1, 1, 1, 1}, {1}, trials);
    return 0;
}


