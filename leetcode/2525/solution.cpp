#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string categorizeBox(int length, int width, int height, int mass)
{
    const static std::string descriptions[] = {"Neither", "Heavy", "Bulky", "Both"};
    bool bulky = (length >= 10'000) || (width >= 10'000) || (height >= 10'000)
              || (static_cast<long>(length) * width * height >= 1'000'000'000);
    bool heavy = mass >= 100;
    return descriptions[bulky * 2 + heavy];
}

// ############################################################################
// ############################################################################

void test(int length,
          int width,
          int height,
          int mass,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = categorizeBox(length, width, height, mass);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1000, 35, 700, 300, "Heavy", trials);
    test(200, 50, 800, 50, "Neither", trials);
    return 0;
}



