#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> avoidFlood(std::vector<int> rains)
{
    std::vector<int> result(rains.size(), -1);
    std::unordered_map<int, int> lake_id_to_full_day;
    std::set<int> empty_days;
    
    for (int i = 0, n = static_cast<int>(rains.size()); i < n; ++i)
    {
        const int lake_id = rains[i];
        if (lake_id == 0)
        {
            empty_days.insert(i);
            continue;
        }
        if (auto full_day = lake_id_to_full_day.find(lake_id);
            full_day != lake_id_to_full_day.end())
        {
            auto empty_day = empty_days.upper_bound(full_day->second);
            if (empty_day == empty_days.end())
                return {};
            result[*empty_day] = lake_id;
            empty_days.erase(empty_day);
        }
        lake_id_to_full_day[lake_id] = i;
    }
    for (int empty_day : empty_days)
        result[empty_day] = 1;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> rains, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = avoidFlood(rains);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, {-1, -1, -1, -1}, trials);
    test({1, 2, 0, 0, 2, 1}, {-1, -1, 2, 1, -1, -1}, trials);
    test({1, 2, 0, 1, 2}, {}, trials);
    return 0;
}


