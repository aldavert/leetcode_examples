#include "../common/common.hpp"

// #############################################################################
// #############################################################################

std::vector<std::vector<int> > updateMatrix(std::vector<std::vector<int>> mat)
{
    const int m = static_cast<int>(mat.size()),
              n = static_cast<int>(mat[0].size());
    std::vector<std::vector<int> > result(m);
    // Initialization.
    for (int i = 0; i < m; ++i)
    {
        result[i].resize(n);
        for (int j = 0; j < n; ++j)
            result[i][j] = (mat[i][j] == 0)?0:100'000;
    }
    // Forward.
    for (int j = 1; j < n; ++j)
        result[0][j] = std::min(result[0][j], result[0][j - 1] + 1);
    for (int i = 1; i < m; ++i)
    {
        result[i][0] = std::min(result[i][0], result[i - 1][0] + 1);
        for (int j = 1; j < n; ++j)
            result[i][j] = std::min(result[i][j],
                                    std::min(result[i - 1][j], result[i][j - 1]) + 1);
    }
    // Backward.
    for (int j = n - 2; j >= 0; --j)
        result[m - 1][j] = std::min(result[m - 1][j], result[m - 1][j + 1] + 1);
    for (int i = m - 2; i >= 0; --i)
    {
        result[i][n - 1] = std::min(result[i][n - 1], result[i + 1][n - 1] + 1);
        for (int j = n - 2; j >= 0; --j)
            result[i][j] = std::min(result[i][j],
                                    std::min(result[i + 1][j], result[i][j + 1]) + 1);
    }
    return result;
}

// #############################################################################
// #############################################################################

void test(std::vector<std::vector<int> > mat,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = updateMatrix(mat);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0, 0}, {0, 1, 0}, {0, 0, 0}}, {{0, 0, 0}, {0, 1, 0}, {0, 0, 0}}, trials);
    test({{0, 0, 0}, {0, 1, 0}, {1, 1, 1}}, {{0, 0, 0}, {0, 1, 0}, {1, 2, 1}}, trials);
    
    test({{0, 1, 1}, {1, 1, 1}, {1, 1, 1}}, {{0, 1, 2}, {1, 2, 3}, {2, 3, 4}}, trials);
    test({{1, 0, 1}, {1, 1, 1}, {1, 1, 1}}, {{1, 0, 1}, {2, 1, 2}, {3, 2, 3}}, trials);
    test({{1, 1, 0}, {1, 1, 1}, {1, 1, 1}}, {{2, 1, 0}, {3, 2, 1}, {4, 3, 2}}, trials);
    test({{1, 1, 1}, {0, 1, 1}, {1, 1, 1}}, {{1, 2, 3}, {0, 1, 2}, {1, 2, 3}}, trials);
    test({{1, 1, 1}, {1, 0, 1}, {1, 1, 1}}, {{2, 1, 2}, {1, 0, 1}, {2, 1, 2}}, trials);
    test({{1, 1, 1}, {1, 1, 0}, {1, 1, 1}}, {{3, 2, 1}, {2, 1, 0}, {3, 2, 1}}, trials);
    test({{1, 1, 1}, {1, 1, 1}, {0, 1, 1}}, {{2, 3, 4}, {1, 2, 3}, {0, 1, 2}}, trials);
    test({{1, 1, 1}, {1, 1, 1}, {1, 0, 1}}, {{3, 2, 3}, {2, 1, 2}, {1, 0, 1}}, trials);
    test({{1, 1, 1}, {1, 1, 1}, {1, 1, 0}}, {{4, 3, 2}, {3, 2, 1}, {2, 1, 0}}, trials);
    
    return 0;
}


