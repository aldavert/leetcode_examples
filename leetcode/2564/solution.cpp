#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > substringXorQueries(std::string s,
        std::vector<std::vector<int> > queries)
{
    constexpr int MAXBIT = 30;
    const int n = static_cast<int>(s.size());
    std::vector<std::vector<int> > result;
    std::unordered_map<int, std::pair<int, int> > val_to_left_and_right;
    for (int left = 0; left < n; ++left)
    {
        int val = 0;
        if (s[left] == '0')
        {
            if (!val_to_left_and_right.contains(0))
                val_to_left_and_right[0] = {left, left};
            continue;
        }
        const int max_right = std::min(n, left + MAXBIT);
        for (int right = left; right < max_right; ++right)
        {
            val = val * 2 + s[right] - '0';
            if (!val_to_left_and_right.contains(val))
                val_to_left_and_right[val] = {left, right};
        }
    }
    for (const auto &query : queries)
    {
        auto it = val_to_left_and_right.find(query[0] ^ query[1]);
        if (it == val_to_left_and_right.end())
            result.push_back({-1, -1});
        else
        {
            const auto [left, right] = it->second;
            result.push_back({left, right});
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          std::vector<std::vector<int> > queries,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = substringXorQueries(s, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("101101", {{0, 5}, {1, 2}}, {{0, 2}, {2, 3}}, trials);
    test("0101", {{12, 8}}, {{-1, -1}}, trials);
    test("1", {{4, 5}}, {{0, 0}}, trials);
    return 0;
}


