#include "../common/common.hpp"
#include <numeric>

// ############################################################################
// ############################################################################

int peopleAwareOfSecret(int n, int delay, int forget)
{
    constexpr int MOD = 1'000'000'007;
    long share = 0;
    std::vector<int> dp(n);
    dp[0] = 1;
    for (int i = 1; i < n; ++i)
    {
        if (i - delay  >= 0) share += dp[i - delay];
        if (i - forget >= 0) share -= dp[i - forget];
        share = (share + MOD) % MOD;
        dp[i] = static_cast<int>(share);
    }
    return std::accumulate(dp.end() - forget, dp.end(), 0,
                           [&](int subtotal, int d) { return (subtotal + d) % MOD; });
}

// ############################################################################
// ############################################################################

void test(int n, int delay, int forget, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = peopleAwareOfSecret(n, delay, forget);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, 2, 4, 5, trials);
    test(4, 1, 3, 6, trials);
    return 0;
}


