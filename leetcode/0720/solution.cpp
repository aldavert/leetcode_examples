#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

std::string longestWord(std::vector<std::string> words)
{
    std::vector<std::string> sorted_words = words;
    std::sort(sorted_words.begin(), sorted_words.end());
    std::string result;
    std::stack<std::string> previous;
    previous.push("");
    for (std::string word : sorted_words)
    {
        while (previous.top().size() + 1 > word.size()) previous.pop();
        if ((previous.top().size() + 1 == word.size())
        &&  (word.substr(0, previous.top().size()) == previous.top()))
        {
            previous.push(word);
            if (word.size() > result.size()) result = word;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> words, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestWord(words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"w", "wo", "wor", "worl", "world"}, "world", trials);
    test({"a", "banana", "app", "appl", "ap", "apply", "apple"}, "apple", trials);
    test({"w", "wo", "worl", "world"}, "wo", trials);
    test({"m", "mo", "moc", "moch", "mocha", "l", "la", "lat", "latt",
          "latte", "c", "ca", "cat"}, "latte", trials);
    test({"rac", "rs", "ra", "on", "r", "otif", "o", "onpdu", "rsf",
          "rs", "ot", "oti", "racy", "onpd"}, "otif", trials);
    return 0;
}


