#include "../common/common.hpp"

// ############################################################################
// ############################################################################

char findKthBit(int n, int k)
{
    if (n == 1) return '0';
    const int m = 1 << (n - 1);
    if (k == m) return '1';
    if (k < m) return findKthBit(n - 1, k);
    return findKthBit(n - 1, m * 2 - k) == '0' ? '1' : '0';
}

// ############################################################################
// ############################################################################

void test(int n, int k, char solution, unsigned int trials = 1)
{
    char result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findKthBit(n, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, 1, '0', trials);
    test(4, 11, '1', trials);
    return 0;
}


