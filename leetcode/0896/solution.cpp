#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool isMonotonic(std::vector<int> nums)
{
    for (int previous = nums[0], sign = 0; int n : nums)
    {
        int value = 1 - 2 * (previous > n) - (previous == n);
        sign = (sign == 0) * value + sign;
        if ((sign - value == 2) || (value - sign == 2)) return false;
        previous = n;
    }
    return true;
}
#else
bool isMonotonic(std::vector<int> nums)
{
    int previous = nums[0];
    int sign = 0;
    for (int n : nums)
    {
        if (previous < n)
        {
            if (sign == -1) return false;
            sign = 1;
        }
        else if (previous > n)
        {
            if (sign == 1) return false;
            sign = -1;
        }
        previous = n;
    }
    return true;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isMonotonic(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 3}   ,  true, trials);
    test({6, 5, 4, 4}   ,  true, trials);
    test({1, 3, 2}      , false, trials);
    test({6, 5, 4, 4, 5}, false, trials);
    return 0;
}


