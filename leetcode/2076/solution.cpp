#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<bool> friendRequests(int n,
                                 std::vector<std::vector<int> > restrictions,
                                 std::vector<std::vector<int> > requests)
{
    std::vector<bool> result;
    std::vector<int> id(n), rank(n);
    for (int i = 0; i < n; ++i) id[i] = i;
    auto find = [&](auto &&self, int i) -> int
    {
        return (i == id[i])?i:id[i] = self(self, id[i]);
    };
    auto merge = [&](int u, int v) -> void
    {
        int i = find(find, u), j = find(find, v);
        if (i == j) return;
        else if (rank[i] < rank[j]) id[i] = id[j];
        else if (rank[i] > rank[j]) id[j] = id[i];
        else id[i] = id[j], ++rank[j];
    };
    
    for (const auto &request : requests)
    {
        int i = find(find, request[0]), j = find(find, request[1]);
        bool is_valid = true;
        if (i != j)
        {
            for (const auto &restriction : restrictions)
            {
                int x = find(find, restriction[0]), y = find(find, restriction[1]);
                if (((i == x) && (j == y)) || ((i == y) && (j == x)))
                {
                    is_valid = false;
                    break;
                }
            }
        }
        result.push_back(is_valid);
        if (is_valid) merge(i, j);
    }
    
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > restrictions,
          std::vector<std::vector<int> > requests,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    std::vector<bool> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = friendRequests(n, restrictions, requests);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{0, 1}}, {{0, 2}, {2, 1}}, {true, false}, trials);
    test(3, {{0, 1}}, {{1, 2}, {0, 2}}, {true, false}, trials);
    test(5, {{0, 1}, {1, 2}, {2, 3}}, {{0, 4}, {1, 2}, {3, 1}, {3, 4}},
         {true, false, true, false}, trials);
    return 0;
}


