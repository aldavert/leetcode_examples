#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int numJewelsInStones(std::string jewels, std::string stones)
{
    std::vector<bool> active(256, false);
    for (char j : jewels) active[j] = true;
    int result = 0;
    for (char s : stones)
        if (active[s])
            ++result;
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string jewels, std::string stones, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numJewelsInStones(jewels, stones);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aA", "aAAbbbb", 3, trials);
    test("z", "ZZ", 0, trials);
    return 0;
}

