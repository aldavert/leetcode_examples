#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumOperations(std::vector<int> nums)
{
    return static_cast<int>(std::count_if(nums.begin(), nums.end(), [](int num) { return num % 3 != 0; }));
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumOperations(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4}, 3, trials);
    test({3, 6, 9}, 0, trials);
    return 0;
}


