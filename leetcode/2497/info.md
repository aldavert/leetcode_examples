# Maximum Start Sum of a Graph

There is an undirected graph consisting of `n` nodes numbered from `0` to `n - 1`. You are given a **0-indexed** integer array `vals` of length `n` where `vals[i]` denotes the value of the `i^{th}` node.

You are also given a 2D integer array `edges` where `edges[i] = [a_i, b_i]` denotes that there exists an **undirected** edge connecting nodes `a_i` and `b_i`.

A **star graph** is a subgraph of the given graph having a center node containing `0` or more neighbors. In other words, it is a subset of edges of the given graph such that there exists a common node for all edges.

The image below shows star graphs with `3` and `4` neighbors respectively, centered at the blue node.

```mermaid
graph TD;
A(( ))---B(( ))
A---C(( ))
A---D(( ))
E(( ))---F(( ))
E---G(( ))
E---H(( ))
E---I(( ))
classDef default fill:#FFF,stroke:#000,stroke-width:2px;
classDef BLUE fill:#AAF,stroke:#004,stroke-width:2px;
class A,E BLUE;
```

The **star sum** is the sum of the values of all the nodes present in the star graph.

Given an integer `k`, return *the* ***maximum star sum*** *of a star graph containing* ***at most*** `k` *edges.*

#### Example 1:
> ```mermaid
> graph TD;
> subgraph SA [2]
> A((1))
> end
> subgraph SB [&nbsp&nbsp&nbsp&nbsp1]
> B((0))
> end
> subgraph SC [4]
> C((3))
> end
> subgraph SD [3&nbsp&nbsp&nbsp&nbsp]
> D((2))
> end
> subgraph SE [&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp-20]
> E((6))
> end
> subgraph SF [&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp-10]
> F((5))
> end
> subgraph SG [10&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp]
> G((4))
> end
> A---B
> A---C
> A---D
> C---E
> C---F
> C---G
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef EMPTY fill:#FFF0,stroke:#FFF0;
> class SA,SB,SC,SD,SE,SF,SG EMPTY;
> linkStyle 1,5 stroke:#00F,stroke-width:4px;
> ```
> *Input:* `vals = [1, 2, 3, 4, 10, -10, -20], edges = [[0, 1], [1, 2], [1, 3], [3, 4], [3, 5], [3, 6]], k = 2`
> *Output:* `16`  
> *Explanation:* The above diagram represents the input graph. The star graph with the maximum star sum is denoted by blue. It is centered at `3` and includes its neighbors `1` and `4`. It can be shown it is not possible to get a star graph with a sum greater than `16`.

#### Example 2:
> *Input:* `vals = [-5], edges = [], k = 0`  
> *Output:* `-5`  
> *Explanation:* There is only one possible star graph, which is node `0` itself. Hence, we return `-5`.

#### Constraints:
- `n == vals.length`
- `1 <= n <= 10^5`
- `-10^4 <= vals[i] <= 10^4`
- `0 <= edges.length <= min(n * (n - 1) / 2, 10^5)`
- `edges[i].length == 2`
- `0 <= a_i, b_i <= n - 1`
- `a_i != b_i`
- `0 <= k <= n - 1`


