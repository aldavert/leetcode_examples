#include "../common/common.hpp"
#include <queue>
#include <limits>

// ############################################################################
// ############################################################################

int maxStarSum(std::vector<int> vals, std::vector<std::vector<int> > edges, int k)
{
    const int n = static_cast<int>(vals.size());
    int result = std::numeric_limits<int>::lowest();
    std::vector<std::vector<std::pair<int, int> > > graph(n);
    
    for (const auto &edge : edges)
    {
        graph[edge[0]].emplace_back(edge[1], vals[edge[1]]);
        graph[edge[1]].emplace_back(edge[0], vals[edge[0]]);
    }
    for (int i = 0; i < n; ++i)
    {
        std::priority_queue<int> heap;
        for (const auto& [_, val] : graph[i])
            if (val > 0)
                heap.push(val);
        int star_sum = vals[i];
        for (int j = 0; (j < k) && !heap.empty(); ++j)
            star_sum += heap.top(),
            heap.pop();
        result = std::max(result, star_sum);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> vals,
          std::vector<std::vector<int> > edges,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxStarSum(vals, edges, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 10, -10, -20},
         {{0, 1}, {1, 2}, {1, 3}, {3, 4}, {3, 5}, {3, 6}}, 2, 16, trials);
    test({-5}, {}, 0, -5, trials);
    return 0;
}


