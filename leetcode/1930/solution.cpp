#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countPalindromicSubsequence(std::string s)
{
    const size_t n = s.size();
    size_t range[26][2] = {};
    size_t chars_present[26][3] = {};
    for (size_t i = 0; i < n; ++i)
    {
        size_t idx = static_cast<size_t>(s[i] - 'a');
        range[idx][range[idx][0] != 0] = i + 1;
    }
    for (size_t i = 0; i < n; ++i)
    {
        size_t idx = static_cast<size_t>(s[i] - 'a');
        for (size_t j = 0; j < 26; ++j)
            chars_present[j][(i >  range[j][0] - 1)
                           + (i >= range[j][1] - 1)] |= (1 << idx);
    }
    int result = 0;
    for (size_t i = 0; i < 26; ++i)
        result += (range[i][1] != 0) * std::popcount(chars_present[i][1]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countPalindromicSubsequence(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aabca", 3, trials);
    test("adc", 0, trials);
    test("bbcbaba", 4, trials);
    return 0;
}


