#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool mergeTriplets(std::vector<std::vector<int> > triplets, std::vector<int> target)
{
    std::vector<int> merged(target.size());
    
    for (const auto &triplet : triplets)
        if (std::equal(triplet.begin(), triplet.end(), target.begin(),
                       [](int a, int b) { return a <= b; }))
    std::transform(triplet.begin(), triplet.end(),
                   merged.begin(), merged.begin(),
                   [](int a, int b) { return std::max(a, b); });
    return merged == target;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > triplets,
          std::vector<int> target,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = mergeTriplets(triplets, target);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{2, 5, 3}, {1, 8, 4}, {1, 7, 5}}, {2, 7, 5}, true, trials);
    test({{3, 4, 5}, {4, 5, 6}}, {3, 2, 5}, false, trials);
    test({{2, 5, 3}, {2, 3, 4}, {1, 2, 5}, {5, 2, 3}}, {5, 5, 5}, true, trials);
    return 0;
}


