#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int minimumWhiteTiles(std::string floor, int numCarpets, int carpetLen)
{
    const int n = static_cast<int>(floor.size());
    int dp[1'001][1'001] = {};
    for (int k = 0; k <= numCarpets; k++)
    {
        for (int i = n - 1; i >= 0; i--)
        {
            dp[k][i] = (floor[i] == '1') + dp[k][i + 1];
            int j = std::min(n, i + carpetLen);
            if (k > 0) dp[k][i] = std::min(dp[k][i], dp[k - 1][j]);
        }
    }
    return dp[numCarpets][0];
}
#else
int minimumWhiteTiles(std::string floor, int numCarpets, int carpetLen)
{
    const int n = static_cast<int>(floor.size());
    std::vector<std::vector<int> > dp(n + 1, std::vector<int>(numCarpets + 1));
    for (int i = n - 1; i >= 0; --i)
        dp[i][0] = floor[i] - '0' + dp[i + 1][0];
    for (int i = n - 1; i >= 0; --i)
    {
        for (int j = 1; j <= numCarpets; ++j)
        {
            int cover = (i + carpetLen < n)?dp[i + carpetLen][j - 1]:0,
                skip = floor[i] - '0' + dp[i + 1][j];
            dp[i][j] = std::min(cover, skip);
        }
    }
    return dp[0][numCarpets];
}
#endif

// ############################################################################
// ############################################################################

void test(std::string floor,
          int numCarpets,
          int carpetLen,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumWhiteTiles(floor, numCarpets, carpetLen);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("10110101", 2, 2, 2, trials);
    test("00000", 2, 3, 0, trials);
    return 0;
}


