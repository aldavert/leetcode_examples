#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isLongPressedName(std::string name, std::string typed)
{
    const size_t n = name.size();
    const size_t m = typed.size();
    size_t i = 0, j = 0;
    while ((i < n) && (j < m))
    {
        size_t p_i = i, p_j = j;
        if (name[p_i] != typed[p_j]) return false;
        while ((i < n) && (name[p_i] == name[i])) ++i;
        while ((j < m) && (typed[p_j] == typed[j])) ++j;
        if (j - p_j < i - p_i) return false;
    }
    return ((i == n) && (j == m));
}

// ############################################################################
// ############################################################################

void test(std::string name, std::string typed, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isLongPressedName(name, typed);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("alex", "aaleex", true, trials);
    test("saeed", "ssaaedd", false, trials);
    test("a", "b", false, trials);
    return 0;
}


