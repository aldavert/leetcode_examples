#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int findMaxValueOfEquation(std::vector<std::vector<int> > points, int k)
{
    int result = std::numeric_limits<int>::lowest();
    std::priority_queue<std::pair<int, int> > heap;
    for (const auto &p : points)
    {
        const int x = p[0];
        const int y = p[1];
        while (!heap.empty() && (x - heap.top().second > k))
            heap.pop();
        if (!heap.empty())
            result = std::max(result, x + y + heap.top().first);
        heap.emplace(y - x, x);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > points,
          int k,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMaxValueOfEquation(points, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 3}, {2, 0}, {5, 10}, {6, -10}}, 1, 4, trials);
    test({{0, 0}, {3, 0}, {9, 2}}, 3, 3, trials);
    return 0;
}


