#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int minSpeedOnTime(std::vector<int> dist, double hour)
{
    if (static_cast<double>(dist.size()) - 1 >= hour) return -1;
    const int n = static_cast<int>(dist.size());
    auto time = [&](int speed) -> double
    {
        double d_speed = static_cast<double>(speed), result = 0;
        for (int i = 0; i < n - 1; ++i)
            result += std::ceil(dist[i] / d_speed);
        return result + dist[n - 1] / d_speed;
    };
    
    int left = 1,
        right = std::max(static_cast<int>(ceil(dist[n - 1] / (hour - n + 1))),
                         *std::max_element(dist.begin(), dist.end()));
    // Right is supposed to be 10'000'000, but its value can be tightened to the
    // maximum distance of 'dist' (since at each stop you have to wait at least
    // an hour, you don't need to go faster). However, you have to take into
    // account that at the last station you don't need to wait for an entire hour,
    // so you have to also consider the maximum speed needed to reach this
    // station in time if this was the furthest away station.
    while (left < right)
    {
        int mid = (left + right) / 2;
        if (time(mid) <= hour) right = mid;
        else left = mid + 1;
    }
    return left;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> dist, double hour, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minSpeedOnTime(dist, hour);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2},   6,  1, trials);
    test({1, 3, 2}, 2.7,  3, trials);
    test({1, 3, 2}, 1.9, -1, trials);
    return 0;
}


