#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::string smallestBeautifulString(std::string s, int k)
{
    const int n = static_cast<int>(s.size());
    for (int i = n - 1; i >= 0; --i)
    {
        for (int m = s[i] - 'a' + 1; m < k; ++m)
        {
            if (((i - 1 >= 0) && (m == s[i - 1] - 'a'))
            || ((i - 2 >= 0) && (m == s[i - 2] - 'a'))) continue;
            s[i] = static_cast<char>(m + 'a');
            for (int j = i + 1; j < n; ++j)
            {
                for (int c = 0; c < k; ++c)
                {
                    if (((j - 1 >= 0) && (c == s[j - 1] - 'a'))
                    ||  ((j - 2 >= 0) && (c == s[j - 2] - 'a'))) continue;
                    s[j] = static_cast<char>(c + 'a');
                    break;
                }
            }
            return s;
        }
    }
    return "";
}
#else
std::string smallestBeautifulString(std::string s, int k)
{
    const int n = static_cast<int>(s.size());
    auto containsPalindrome = [&](int i) -> bool
    {
        return ((i > 0) && (s[i] == s[i - 1])) || ((i > 1) && (s[i] == s[i - 2]));
    };
    for (int i = n - 1; i >= 0; --i)
    {
        do
        {
            ++s[i];
        }
        while (containsPalindrome(i));
        if (s[i] < 'a' + k)
        {
            for (int j = i + 1; j < n; ++j)
                for (s[j] = 'a'; containsPalindrome(j); ++s[j]);
            return s;
        }
    }
    return "";
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestBeautifulString(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcz", 26, "abda", trials);
    test("dc", 4, "", trials);
    return 0;
}


