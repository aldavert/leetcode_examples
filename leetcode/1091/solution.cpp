#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int shortestPathBinaryMatrix(std::vector<std::vector<int> > grid)
{
    const int drow[] = { -1, -1, -1,  0,  1,  1,  1,  0};
    const int dcol[] = { -1,  0,  1,  1,  1,  0, -1, -1};
    if (grid[0][0]) return -1;
    struct position
    {
        int cost;
        int row;
        int col;
    };
    const int n = static_cast<int>(grid.size());
    std::queue<position> queue;
    queue.push({1, 0, 0});
    while (!queue.empty())
    {
        auto [cost, row, col] = queue.front();
        if ((row == n - 1) && (col == n - 1)) return cost;
        queue.pop();
        for (size_t i = 0; i < 8; ++i)
        {
            int new_row = row + drow[i];
            int new_col = col + dcol[i];
            if ((new_row >= 0) && (new_row < n)
             && (new_col >= 0) && (new_col < n)
             && (!grid[new_row][new_col]))
            {
                grid[new_row][new_col] = 1;
                queue.push({cost + 1, new_row, new_col});
            }
        }
    }
    return -1;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestPathBinaryMatrix(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {1, 0}}, 2, trials);
    test({{0, 0, 0}, {1, 1, 0}, {1, 1, 0}}, 4, trials);
    test({{1, 0, 0}, {1, 1, 0}, {1, 1, 0}}, -1, trials);
    test({{0, 0, 0}, {1, 1, 0}, {1, 1, 1}}, -1, trials);
    return 0;
}


