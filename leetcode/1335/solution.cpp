#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minDifficulty(std::vector<int> jobDifficulty, int d)
{
    const int n = static_cast<int>(jobDifficulty.size());
    if (n < d) return -1;
    std::vector<std::vector<int> > dp(n + 1, std::vector<int>(d + 1, d * 2'500));
    dp[0][0] = 0;
    for (int i = 1; i <= n; ++i)
        for (int k = 1; k <= d; ++k)
            for (int j = i - 1, md = 0; j >= k - 1; --j)
                dp[i][k] = std::min(dp[i][k],
                                dp[j][k - 1] + (md = std::max(md, jobDifficulty[j])));
    return dp[n][d];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> jobDifficulty,
          int d,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDifficulty(jobDifficulty, d);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({6, 5, 4, 3, 2, 1}, 2, 7, trials);
    test({9, 9, 9}, 4, -1, trials);
    test({1, 1, 1}, 3, 3, trials);
    return 0;
}


