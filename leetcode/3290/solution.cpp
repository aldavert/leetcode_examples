#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maxScore(std::vector<int> a, std::vector<int> b)
{
    long dp[4] = { std::numeric_limits<long>::lowest() / 2,
                   std::numeric_limits<long>::lowest() / 2,
                   std::numeric_limits<long>::lowest() / 2,
                   std::numeric_limits<long>::lowest() / 2 };
    for (long num : b)
        for (int i = 3; i >= 0; --i)
            dp[i] = std::max(dp[i], (i > 0 ? dp[i - 1] : 0) + a[i] * num);
    return dp[3];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> a,
          std::vector<int> b,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxScore(a, b);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 2, 5, 6}, {2, -6, 4, -5, -3, 2, -7}, 26, trials);
    test({-1, 4, 5, -2}, {-5, -1, -3, -2, -4}, -1, trials);
    return 0;
}


