#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

std::vector<int> getOrder(std::vector<std::vector<int> > tasks)
{
    typedef std::tuple<int, int> INFO;
    std::vector<std::tuple<int, int, int> > sorted_tasks;
    for (int i = 0, n = static_cast<int>(tasks.size()); i < n; ++i)
        sorted_tasks.push_back({tasks[i][0], tasks[i][1], i});
    std::sort(sorted_tasks.begin(), sorted_tasks.end());
    std::priority_queue<INFO, std::vector<INFO>, std::greater<INFO> > q;
    std::vector<int> result;
    int cpu_time = 0;
    for (auto [time, cost, i] : sorted_tasks)
    {
        if (q.empty())
        {
            q.push({cost, i});
            cpu_time = time;
            continue;
        }
        while ((!q.empty()) && (cpu_time < time))
        {
            auto [delta_cost, index] = q.top();
            q.pop();
            result.push_back(index);
            cpu_time += delta_cost;
        }
        cpu_time = std::max(cpu_time, time);
        q.push({cost, i});
    }
    while (!q.empty())
    {
        auto [delta_cost, index] = q.top();
        q.pop();
        result.push_back(index);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > tasks,
          std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = getOrder(tasks);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {2, 4}, {3, 2}, {4, 1}}, {0, 2, 3, 1}, trials);
    test({{7, 10}, {7, 12}, {7, 5}, {7, 4}, {7, 2}}, {4, 3, 2, 0, 1}, trials);
    test({{19, 13}, {16,  9}, {21, 10}, {32, 25}, {37,  4}, {49, 24},
          { 2, 15}, {38, 41}, {37, 34}, {33,  6}, {45,  4}, {18, 18},
          {46, 39}, {12, 24}},
          {6, 1, 2, 9, 4, 10, 0, 11, 5, 13, 3, 8, 12, 7}, trials);
    test({{5, 2}, {7, 2}, {9, 4}, {6, 3}, {5, 10}, {1, 1}}, {5, 0, 1, 3, 2, 4}, trials);
    return 0;
}


