#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int createSortedArray(std::vector<int> instructions)
{
    struct FenwickTree
    {
    public:
        FenwickTree(int n) : m_sums(n + 1), m_n(n + 1) {}
        void update(int i, int delta)
        {
            while (i < m_n) m_sums[i] += delta, i += lowbit(i);
        }
        int query(int i) const
        {
            int sum = 0;
            while (i > 0) sum += m_sums[i], i -= lowbit(i);
            return sum;
        }
    protected:
        inline int lowbit(int x) const {  return x & (-x); }
        std::vector<int> m_sums;
        int m_n;
    };
    constexpr int MOD = 1'000'000'007;
    FenwickTree tree(1e5);
    long result = 0;
    for (int i = 0, n = static_cast<int>(instructions.size()); i < n; ++i)
    {
        int less_than = tree.query(instructions[i] - 1),
            greater_than = i - tree.query(instructions[i]);      
        result += std::min(less_than, greater_than);
        tree.update(instructions[i], 1);
    }
    return static_cast<int>(result % MOD);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> instructions, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = createSortedArray(instructions);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 5, 6, 2}, 1, trials);
    test({1, 2, 3, 6, 5, 4}, 3, trials);
    test({1, 3, 3, 3, 2, 4, 2, 1, 2}, 4, trials);
    return 0;
}


