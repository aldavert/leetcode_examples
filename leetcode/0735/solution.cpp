#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> asteroidCollision(std::vector<int> asteroids)
{
    std::vector<int> result, going_right;
    for (int asteroid : asteroids)
    {
        if (asteroid > 0) going_right.push_back(asteroid);
        else
        {
            while (!going_right.empty() && (-asteroid > going_right.back()))
                going_right.pop_back();
            if (going_right.empty()) result.push_back(asteroid);
            else if (going_right.back() == -asteroid) going_right.pop_back();
        }
    }
    for (int asteroid : going_right)
        result.push_back(asteroid);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> asteroids, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = asteroidCollision(asteroids);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 10, -5}, {5, 10}, trials);
    test({8, -8}, {}, trials);
    test({10, 2, -5}, {10}, trials);
    test({5, 10, -5, 2}, {5, 10, 2}, trials);
    return 0;
}


