#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maxAlternatingSum(std::vector<int> nums)
{
    long even = 0, odd = 0;
    for (int number : nums)
        even = std::max(even, odd + number),
        odd = even - number;
    return even;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxAlternatingSum(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 5, 3}, 7, trials);
    test({5, 6, 7, 8}, 8, trials);
    test({6, 2, 1, 2, 4, 5}, 10, trials);
    return 0;
}


