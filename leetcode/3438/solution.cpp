#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string findValidPair(std::string s)
{
    const size_t n = s.size();
    int histogram[10] = {};
    for (size_t i = 0; i < n; ++i) ++histogram[s[i] - '0'];
    for (size_t i = 1; i < n; ++i)
    {
        if (s[i - 1] == s[i]) continue;
        int l = s[i - 1] - '0', r = s[i] - '0';
        if ((histogram[l] == l) && (histogram[r] == r))
            return s.substr(i - 1, 2);
    }
    return "";
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findValidPair(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("2523533", "23", trials);
    test("221", "21", trials);
    test("22", "", trials);
    return 0;
}


