#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int largestAltitude(std::vector<int> gain)
{
    int result = 0;
    for (int height = 0; int g : gain)
        result = std::max(result, height += g);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> gain, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestAltitude(gain);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-5, 1, 5, 0, -7}, 1, trials);
    test({-4, -3, -2, -1, 4, 3, 2}, 0, trials);
    return 0;
}


