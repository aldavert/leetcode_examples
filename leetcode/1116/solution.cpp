#include "../common/common.hpp"
#include <functional>
#include <semaphore.h>
#include <thread>

// ############################################################################
// ############################################################################

std::string output;
void printNumber(int value) { output += std::to_string(value); }

class ZeroEvenOdd
{
public:
    ZeroEvenOdd(int _n) : n(_n)
    {
        sem_init(&zeroSemaphore, /*pshared=*/0, /*value=*/1);
        sem_init(&evenSemaphore, /*pshared=*/0, /*value=*/0);
        sem_init(&oddSemaphore , /*pshared=*/0, /*value=*/0);
    }
    ~ZeroEvenOdd(void)
    {
        sem_destroy(&zeroSemaphore);
        sem_destroy(&evenSemaphore);
        sem_destroy(&oddSemaphore);
    }
    // printNumber(x) outputs "x", where x is an integer.
    void zero(std::function<void(int)> printNumber)
    {
        for (int i = 0; i < n; ++i)
        {
            sem_wait(&zeroSemaphore);
            printNumber(0);
            sem_post(&((i % 2 == 0)?oddSemaphore:evenSemaphore));
        }
    }
    void even(std::function<void(int)> printNumber)
    {
        for (int i = 2; i <= n; i += 2)
        {
            sem_wait(&evenSemaphore);
            printNumber(i);
            sem_post(&zeroSemaphore);
        }
    }
    void odd(std::function<void(int)> printNumber)
    {
        for (int i = 1; i <= n; i += 2)
        {
            sem_wait(&oddSemaphore);
            printNumber(i);
            sem_post(&zeroSemaphore);
        }
    }
private:
    int n;
    sem_t zeroSemaphore;
    sem_t evenSemaphore;
    sem_t oddSemaphore;
};

// ############################################################################
// ############################################################################

void test(int n, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ZeroEvenOdd obj(n);
        output.clear();
        std::thread t1([&]() { obj.zero(printNumber); }),
                    t2([&]() { obj.even(printNumber); }),
                    t3([&]() { obj.odd(printNumber); });
        t1.join();
        t2.join();
        t3.join();
        result = output;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2, "0102", trials);
    test(5, "0102030405", trials);
    return 0;
}


