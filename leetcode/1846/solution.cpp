#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumElementAfterDecrementingAndRearranging(std::vector<int> arr)
{
    std::sort(arr.begin(), arr.end());
    int result = 0;
    for (int prev = 0; int value : arr)
        result += (value - prev >= 1),
        prev = std::min(value, prev + 1);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> arr, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumElementAfterDecrementingAndRearranging(arr);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 2, 1, 2, 1}, 2, trials);
    test({100, 1, 1000}, 3, trials);
    test({1, 2, 3, 4, 5}, 5, trials);
    std::vector<int> long_test(10'000, 209);
    long_test.push_back(1);
    long_test.push_back(10000);
    test(long_test, 210, trials);
    return 0;
}


