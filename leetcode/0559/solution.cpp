#include "../common/common.hpp"
#include <queue>

constexpr int null = -1000;

class Node
{
public:
    int val = 0;
    std::vector<Node *> children;

    Node() = default;
    Node(int _val) : val(_val) {}
    Node(int _val, std::vector<Node *> _children) : val(_val), children(_children) {}
    ~Node(void)
    {
        for (auto &n : children) delete n;
    }
};

Node * vec2tree(const std::vector<int> &tree)
{
    const int n = static_cast<int>(tree.size());
    if (n == 0) return nullptr;
    Node * root = new Node(tree[0]);
    std::queue<Node *> queue;
    queue.push(root);
    for (int i = 2; i < n; ++i)
    {
        Node * current = queue.front();
        queue.pop();
        int j;
        for (j = i; (j < n) && (tree[j] != null); ++j);
        current->children.resize(j - i, nullptr);
        for (int k = i; k < j; ++k)
            queue.push(current->children[k - i] = new Node(tree[k]));
        i = j;
    }
    return root;
}

// ############################################################################
// ############################################################################

int maxDepth(Node * root)
{
    if (!root) return 0;
    int result = 1;
    for (Node * next : root->children)
        result = std::max(result, maxDepth(next) + 1);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
    {
        Node * root = vec2tree(tree);
        result = maxDepth(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, null, 3, 2, 4, null, 5, 6}, 3, trials);
    test({1, null, 2, 3, 4, 5, null, null, 6, 7, null, 8, null, 9, 10, null, null, 11, null, 12, null, 13, null, null, 14}, 5, trials);
    return 0;
}


