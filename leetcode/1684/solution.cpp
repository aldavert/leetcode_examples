#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int countConsistentStrings(std::string allowed, std::vector<std::string> words)
{
    bool valid[26] = {};
    for (char letter : allowed) valid[letter - 'a'] = true;
    int result = 0;
    for (const auto &word : words)
    {
        bool consistent = true;
        for (char letter : word)
        {
            if (!valid[letter - 'a'])
            {
                consistent = false;
                break;
            }
        }
        result += consistent;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string allowed,
          std::vector<std::string> words,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = countConsistentStrings(allowed, words);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ab", {"ad", "bd", "aaab", "baa", "badab"}, 2, trials);
    test("abc", {"a", "b", "c", "ab", "ac", "bc", "abc"}, 7, trials);
    test("cad", {"cc", "acd", "b", "ba", "bac", "bad", "ac", "d"}, 4, trials);
    return 0;
}


