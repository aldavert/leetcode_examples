#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
bool splitArraySameAverage(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int sum = 0;
    for (int number: nums) sum += number;
    std::vector<std::vector<std::vector<bool> > > dp(n + 1,
            std::vector<std::vector<bool> >(n + 1, std::vector<bool>(sum + 1, true)));
    auto helper = [&](auto &&self, int idx, int l, int acc) -> bool
    {
        if (l == 0) return acc == 0;
        if (idx >= static_cast<int>(nums.size())) return false;
        if (!dp[idx][l][acc]) return false;
        if ((nums[idx] <= acc) && self(self, idx + 1, l - 1, acc - nums[idx]))
            return true;
        if (self(self, idx + 1, l, acc)) return true;
        return dp[idx][l][acc] = false;
    };
    for (int l = 1; l < n; ++l)
        if (((sum * l) % n == 0)
        &&  (helper(helper, 0, l, (sum * l) / n))) return true;
    return false;
}
#else
bool splitArraySameAverage(std::vector<int> nums)
{
    auto isPossible = [](int sum, int n) -> bool
    {
        for (int i = 1; i < n / 2 + 1; ++i)
            if ((i * sum) % n == 0)
                return true;
        return false;
    };
    const int n = static_cast<int>(nums.size());
    int sum = 0;
    for (int number : nums) sum += number;
    if (!isPossible(sum, n)) return false;
    std::vector<std::unordered_set<int> > sums(n / 2 + 1);
    sums[0].insert(0);
    for (int number : nums)
        for (int i = n / 2; i > 0; --i)
            for (int value : sums[i - 1])
                sums[i].insert(number + value);
    for (int i = 1; i < n / 2 + 1; ++i)
        if (((i * sum) % n == 0) && (sums[i].find(i * sum / n) != sums[i].end()))
            return true;
    return false;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = splitArraySameAverage(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6, 7, 8}, true, trials);
    test({3, 1}, false, trials);
    return 0;
}


