#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<int> memLeak(int memory1, int memory2)
{
    int i = 1;
    while ((memory1 >= i) || (memory2 >= i))
    {
        if (memory1 >= memory2)
            memory1 -= i;
        else
            memory2 -= i;
        ++i;
    }
    return {i, memory1, memory2};
}

// ############################################################################
// ############################################################################

void test(int memory1,
          int memory2,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = memLeak(memory1, memory2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2,  2, {3, 1, 0}, trials);
    test(8, 11, {6, 0, 4}, trials);
    return 0;
}


