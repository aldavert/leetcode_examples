#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

std::vector<int> bestCoordinate(std::vector<std::vector<int> > towers, int radius)
{
    auto dist = [](const std::vector<int> &tower, int i, int j) -> double
    {
        int dx = tower[0] - i, dy = tower[1] - j;
        return std::sqrt(dx * dx + dy * dy);
    };
    constexpr int MAX = 50;
    std::vector<int> result(2);
    int max_quality = 0;
    
    for (int i = 0; i <= MAX; ++i)
    {
        for (int j = 0; j <= MAX; ++j)
        {
            int quality_sum = 0;
            for (const auto &tower : towers)
                if (const double d = dist(tower, i, j); d <= radius)
                    quality_sum += static_cast<int>(tower[2] / (1 + d));
            if (quality_sum > max_quality)
            {
                max_quality = quality_sum;
                result = {i, j};
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > towers,
          int radius,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = bestCoordinate(towers, radius);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 5}, {2, 1, 7}, {3, 1, 9}}, 2, {2, 1}, trials);
    test({{23, 11, 21}}, 9, {23, 11}, trials);
    test({{1, 2, 13}, {2, 1, 7}, {0, 1, 9}}, 2, {1, 2}, trials);
    return 0;
}


