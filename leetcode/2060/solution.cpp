#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

bool possiblyEquals(std::string s1, std::string s2)
{
    const int n1 = static_cast<int>(s1.size()),
              n2 = static_cast<int>(s2.size());
    std::vector<std::vector<std::unordered_map<int, bool> > > dp(s1.size() + 1,
            std::vector<std::unordered_map<int, bool> >(s2.size() + 1));
    auto getNums = [](const std::string &s) -> std::vector<int>
    {
        std::vector<int> nums{std::stoi(s)};
        if (s.size() == 2)
            nums.push_back(std::stoi(s.substr(0, 1)) + std::stoi(s.substr(1, 1)));
        else if (s.size() == 3)
        {
            nums.push_back(stoi(s.substr(0, 1)) + stoi(s.substr(1, 2)));
            nums.push_back(stoi(s.substr(0, 2)) + stoi(s.substr(2, 1)));
            nums.push_back(stoi(s.substr(0, 1)) + stoi(s.substr(1, 1))
                                                + stoi(s.substr(2, 1)));
        }
        return nums;
    };
    auto getNextLetterIndex = [](const std::string &s, int i) -> int
    {
        while ((i < static_cast<int>(s.size())) && std::isdigit(s[i])) ++i;
        return i;
    };
    auto dpFunc = [&](auto &&self, int i, int j, int padding_diff) -> bool
    {
        if (auto it = dp[i][j].find(padding_diff); it != dp[i][j].end())
            return it->second;
        if ((i == n1) && (j == n2)) return padding_diff == 0;
        if ((i < n1) && std::isdigit(s1[i]))
        {
            const int next_letter_index = getNextLetterIndex(s1, i);
            for (int num : getNums(s1.substr(i, next_letter_index - i)))
                if (self(self, next_letter_index, j, padding_diff + num))
                    return true;
        }
        else if ((j < n2) && std::isdigit(s2[j]))
        {
            const int next_letter_index = getNextLetterIndex(s2, j);
            for (int num : getNums(s2.substr(j, next_letter_index - j)))
                if (self(self, i, next_letter_index, padding_diff - num))
                    return true;
        }
        else if (padding_diff > 0)
        {
            if (j < n2) return self(self, i, j + 1, padding_diff - 1);
        }
        else if (padding_diff < 0)
        {
            if (i < n1) return self(self, i + 1, j, padding_diff + 1);
        }
        else
        {
            if ((i < n1) && (j < n2) && (s1[i] == s2[j]))
                return self(self, i + 1, j + 1, 0);
        }
        return dp[i][j][padding_diff] = false;
    };
    return dpFunc(dpFunc, 0, 0, 0);
}

// ############################################################################
// ############################################################################

void test(std::string s1, std::string s2, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = possiblyEquals(s1, s2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("internationalization", "i18n", true, trials);
    test("l123e", "44", true, trials);
    test("a5b", "c5b", false, trials);
    return 0;
}


