#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long maximumTripletValue(std::vector<int> nums)
{
    long long result = 0;
    for (size_t i = 0, n = nums.size(); i < n; ++i)
        for (size_t j = i + 1; j < n; ++j)
            for (size_t k = j + 1; k < n; ++k)
                result = std::max(result,
                                  static_cast<long long>(nums[i] - nums[j]) * nums[k]);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumTripletValue(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({12, 6, 1, 2, 7}, 77, trials);
    test({1, 10, 3, 4, 19}, 133, trials);
    test({1, 2, 3}, 0, trials);
    return 0;
}


