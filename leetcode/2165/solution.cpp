#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long smallestNumber(long long num)
{
    std::string s = std::to_string(std::abs(num));
    std::sort(s.begin(), s.end(), [&](int a, int b) { return (num < 0)?a > b:a < b; });
    if (num > 0)
        std::swap(s[0], s[s.find_first_not_of('0')]);
    return (2 * (num >= 0) - 1) * std::stoll(s);
}

// ############################################################################
// ############################################################################

void test(long long num, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = smallestNumber(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(310, 103, trials);
    test(-7605, -7650, trials);
    return 0;
}


