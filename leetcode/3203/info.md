# Find Minimum Diameter After Merging Two Trees

There exist two **undirected** trees with `n` and `m` nodes, numbered from `0` to `n - 1` and from `0` to `m - 1`, respectively. You are given two 2D integer arrays `edges1` and `edges2` of lengths `n - 1` and `m - 1`, respectively, where `edges1[i] = [a_i, b_i]` indicates that there is an edge between nodes `a_i` and `b_i` in the first tree and `edges2[i] = [u_i, v_i]` indicates that there is an edge between nodes `u_i` and `v_i` in the second tree.

You must connect one node from the first tree with another node from the second tree with an edge.

Return the **minimum** possible **diameter** of the resulting tree.

The **diameter** of a tree is the length of the *longest* path between any two nodes in the tree.

#### Example 1:
> ```mermaid 
> graph TD;
> subgraph SA [Tree 1]
> A((0))---B((3))
> A---C((2))
> A---D((1))
> end
> subgraph SB [Tree 2]
> E((0))---F((1))
> end
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef white fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class SA,SB white;
> ```
> *Input:* `edges1 = [[0, 1], [0, 2], [0, 3]], edges2 = [[0, 1]]`  
> *Output:* `3`  
> *Explanation:* We can obtain a tree of diameter `3` by connecting node `0` from the first tree with any node from the second tree.

#### Example 2:
> ```mermaid 
> graph TD;
> subgraph SA [Tree 1]
> A((2))---B((7))
> A---C((4))
> A---D((5))
> A---E((0))
> E---F((1))
> E---G((3))
> G---H((6))
> end
> subgraph SB [Tree 2]
> M((2))---N((4))
> M---O((7))
> M---P((5))
> M---Q((0))
> Q---R((1))
> Q---S((3))
> S---T((6))
> end
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> classDef white fill:#FFF0,stroke:#FFF0,stroke-width:0px;
> class SA,SB white;
> ```
> *Input:* `edges1 = [[0, 1], [0, 2], [0, 3], [2, 4], [2, 5], [3, 6], [2, 7]], edges2 = [[0, 1], [0, 2], [0, 3], [2, 4], [2, 5], [3, 6], [2, 7]]`
> *Output:* `5`  
> *Explanation:* We can obtain a tree of diameter 5 by connecting node 0 from the first tree with node 0 from the second tree.

#### Constraints:
- `1 <= n, m <= 10^5`
- `edges1.length == n - 1`
- `edges2.length == m - 1`
- `edges1[i].length == edges2[i].length == 2`
- `edges1[i] = [a_i, b_i]`
- `0 <= a_i, b_i < n`
- `edges2[i] = [u_i, v_i]`
- `0 <= u_i, v_i < m`
- The input is generated such that `edges1` and `edges2` represent valid trees.


