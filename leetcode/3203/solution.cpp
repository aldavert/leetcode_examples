#include "../common/common.hpp"
#include <utility>

// ############################################################################
// ############################################################################

int minimumDiameterAfterMerge(std::vector<std::vector<int> > edges1,
                              std::vector<std::vector<int> > edges2)
{
    auto calcDiameter = [](const std::vector<std::vector<int> > &edges)
    {
        const int n = static_cast<int>(edges.size()) + 1;
        std::vector<std::vector<int> > graph(n);
        int max_diameter = 0;
        auto maxDepth = [&](auto &&self, int u, int prev) -> int
        {
            int max_sub_depth[2] = {};
            for (const int v : graph[u])
            {
                if (v == prev) continue;
                int current_depth = self(self, v, u);
                if (current_depth > max_sub_depth[0])
                    max_sub_depth[1] = std::exchange(max_sub_depth[0], current_depth);
                else if (current_depth > max_sub_depth[1])
                    max_sub_depth[1] = current_depth;
            }
            max_diameter = std::max(max_diameter, max_sub_depth[0] + max_sub_depth[1]);
            return 1 + max_sub_depth[0];
        };
        for (const auto &edge : edges)
        {
            graph[edge[0]].push_back(edge[1]);
            graph[edge[1]].push_back(edge[0]);
        }
        maxDepth(maxDepth, 0, /*prev=*/-1);
        return max_diameter;
    };
    const int diameter1 = calcDiameter(edges1),
              diameter2 = calcDiameter(edges2),
              combined_diameter = (diameter1 + 1) / 2 + (diameter2 + 1) / 2 + 1;
    return std::max({diameter1, diameter2, combined_diameter});
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > edges1,
          std::vector<std::vector<int> > edges2,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumDiameterAfterMerge(edges1, edges2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1}, {0, 2}, {0, 3}}, {{0, 1}}, 3, trials);
    test({{0, 1}, {0, 2}, {0, 3}, {2, 4}, {2, 5}, {3, 6}, {2, 7}},
         {{0, 1}, {0, 2}, {0, 3}, {2, 4}, {2, 5}, {3, 6}, {2, 7}}, 5, trials);
    return 0;
}


