#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string finalString(std::string s)
{
    std::string result;
    auto reverse = [&](void) -> void
    {
        for (int l = 0, r = static_cast<int>(result.size()) - 1; l < r; ++l, --r)
            std::swap(result[l], result[r]);
    };
    for (char letter : s)
    {
        if (letter == 'i') reverse();
        else result.push_back(letter);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = finalString(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("string", "rtsng", trials);
    test("poiinter", "ponter", trials);
    return 0;
}


