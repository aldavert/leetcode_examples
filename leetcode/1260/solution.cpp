#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > shiftGrid(std::vector<std::vector<int> > grid, int k)
{
    const int m = static_cast<int>(grid.size());
    const int n = static_cast<int>(grid[0].size());
    if ((m == 1) && (n == 1)) return grid;
    std::vector<std::vector<int> > result(m);
    const int shift_m = (k / n) % m;
    const int shift_n = k % n;
    if (!(shift_m || shift_n)) return grid;
    for (int i = 0; i < m; ++i)
        result[i].resize(n, 0);
    for (int i = 0, dst_i = shift_m, dst_j = shift_n; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            result[dst_i][dst_j] = grid[i][j];
            ++dst_j;
            if (dst_j == n)
            {
                dst_j = 0;
                ++dst_i;
                if (dst_i == m)
                    dst_i = 0;
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid,
          int k,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = shiftGrid(grid, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3},
          {4, 5, 6},
          {7, 8, 9}}, 1,
         {{9, 1, 2},
          {3, 4, 5},
          {6, 7, 8}}, trials);
    test({{ 3,  8,  1,  9},
          {19,  7,  2,  5},
          { 4,  6, 11, 10},
          {12,  0, 21, 13}}, 4,
         {{12,  0, 21, 13},
          { 3,  8,  1,  9},
          {19,  7,  2,  5},
          { 4,  6, 11, 10}}, trials);
    test({{1, 2, 3},
          {4, 5, 6},
          {7, 8, 9}}, 9,
         {{1, 2, 3},
          {4, 5, 6},
          {7, 8, 9}}, trials);
    return 0;
}


