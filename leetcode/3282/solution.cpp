#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long findMaximumScore(std::vector<int> nums)
{
    long result = 0;
    for (int mx = 0; const int num : nums)
    {
        result += mx;
        mx = std::max(mx, num);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, long long solution, unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMaximumScore(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 1, 5}, 7, trials);
    test({4, 3, 1, 3, 2}, 16, trials);
    return 0;
}


