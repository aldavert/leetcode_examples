#include "../common/common.hpp"
#include <cstring>

// ############################################################################
// ############################################################################

#if 0
std::vector<int> findAnagrams(std::string s, std::string p)
{
    const int ns = static_cast<int>(s.size()),
              np = static_cast<int>(p.size());
    std::vector<int> result;
    if (np > ns)  return result;
    if (p.empty() || s.empty())   return result;
    
    int freq[26] = {};
    for (char ch : p) ++freq[ch - 'a'];
    for (int l = 0, r = -1, count = 0; true;)
    {
        if (r - l + 1 < np)
        {
            if (r == ns - 1) return result;
            ++r;
            if (freq[s[r] - 'a'] > 0) ++count;
            --freq[s[r] - 'a'];
        }
        else
        {
            if (count == np) result.push_back(l);
            if (freq[s[l] - 'a'] >= 0) --count;
            ++freq[s[l] - 'a'];
            ++l;
        }
    }
    return result;
}
#else
std::vector<int> findAnagrams(std::string s, std::string p)
{
    const int ns = static_cast<int>(s.size()),
              np = static_cast<int>(p.size());
    if (s.empty()) return {};
    std::vector<int> result;
    int histogram[32] = {}, current[32];
    for (char c : p) ++histogram[c - 'a'];
    for (int i = 0; i < ns; ++i)
    {
        std::memcpy(current, histogram, sizeof(histogram));
        bool success = true;
        for (int j = i; j < i + np; ++j)
        {
            if (current[s[j] - 'a']) --current[s[j] - 'a'];
            else
            {
                success = false;
                break;
            }
        }
        if (success) result.push_back(i);
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_set<int> lut(left.begin(), left.end());
    for (const auto &r : right)
    {
        if (auto it = lut.find(r); it != lut.end())
            lut.erase(it);
        else return false;
    }
    return lut.empty();
}

void test(std::string s,
          std::string p,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findAnagrams(s, p);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("cbaebabacd", "abc", {0, 6}, trials);
    test("abab", "ab", {0, 1, 2}, trials);
    return 0;
}


