#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int makeConnected(int n, std::vector<std::vector<int> > connections)
{
    std::vector<int> id(n);
    for (int i = 0; i < n; ++i) id[i] = i;
    auto find = [&](auto &&self, int idx) -> int
    {
        return (id[idx] != idx)?id[idx] = self(self, id[idx]):idx;
    };
    int number_of_removed_cables = 0;
    for (const auto &connection : connections)
    {
        int u = find(find, connection[0]), v = find(find, connection[1]);
        if (u == v) ++number_of_removed_cables;
        else id[u] = id[v] = std::min(u, v);
    }
    std::vector<bool> repeated_nodes(n);
    int number_of_groups = 0;
    for (int i = 0; i < n; ++i)
    {
        id[i] = find(find, i);
        number_of_groups += !repeated_nodes[id[i]];
        repeated_nodes[id[i]] = true;
    }
    return (number_of_groups - 1 <= number_of_removed_cables)?(number_of_groups - 1):-1;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > connections,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = makeConnected(n, connections);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{0, 1}, {0, 2}, {1, 2}}, 1, trials);
    test(6, {{0, 1}, {0, 2}, {0, 3}, {1, 2}, {1, 3}}, 2, trials);
    test(6, {{0, 1}, {0, 2}, {0, 3}, {1, 2}}, -1, trials);
    return 0;
}


