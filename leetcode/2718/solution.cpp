#include "../common/common.hpp"

// ############################################################################
// ############################################################################

long long matrixSumQueries(int n, std::vector<std::vector<int> > queries)
{
    std::vector<bool> seen[2] = { std::vector<bool>(n), std::vector<bool>(n) };
    int not_set[2] = {n, n};
    long result = 0;
    for (int i = static_cast<int>(queries.size()) - 1; i >= 0; --i)
    {
        int type = queries[i][0], index = queries[i][1], val = queries[i][2];
        if (!seen[type][index])
        {
            result += val * not_set[type ^ 1];
            seen[type][index] = true;
            --not_set[type];
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > queries,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = matrixSumQueries(n, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{0, 0, 1}, {1, 2, 2}, {0, 2, 3}, {1, 0, 4}}, 23, trials);
    test(3, {{0, 0, 4}, {0, 1, 2}, {1, 0, 1}, {0, 2, 3}, {1, 2, 1}}, 17, trials);
    return 0;
}


