#include "../common/common.hpp"
#include <sstream>

// ############################################################################
// ############################################################################

std::vector<std::string> printVertically(std::string s)
{
    std::vector<std::string> result, words;
    size_t max_length = 0;
    
    std::istringstream iss(s);
    for (std::string token; iss >> token;)
        words.push_back(token);
    for (const auto &word : words)
        max_length = std::max(max_length, word.size());
    for (size_t i = 0; i < max_length; ++i)
    {
        std::string row;
        for (const auto & word : words)
            row += (i < word.size())?word[i]:' ';
        while (row.back() == ' ')
            row.pop_back();
        result.push_back(row);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = printVertically(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("HOW ARE YOU", {"HAY", "ORO", "WEU"}, trials);
    test("TO BE OR NOT TO BE", {"TBONTB", "OEROOE", "   T"}, trials);
    test("CONTEST IS COMING",
         {"CIC", "OSO", "N M", "T I", "E N", "S G", "T"}, trials);
    return 0;
}


