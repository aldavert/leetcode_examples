#include "../common/common.hpp"
#include "test_a.hpp"

// ############################################################################
// ############################################################################

#if 1
std::string shiftingLetters(std::string s, std::vector<int> shifts)
{
    const long n = s.size();
    const long thr = static_cast<long>('z' - 'a' + 1);
    for (long i = n - 1, acum = 0; i >= 0; --i)
        s[i] = static_cast<char>((static_cast<long>(s[i] - 'a')
                               + (acum += shifts[i])) % thr) + 'a';
    return s;
}
#elif 0
std::string shiftingLetters(std::string s, std::vector<int> shifts)
{
    const long n = s.size();
    const long thr = static_cast<long>('z' - 'a' + 1);
    std::vector<long> values(n);
    for (long i = n - 1, acum = 0; i >= 0; --i)
        values[i] = s[i] - 'a' + (acum += shifts[i]);
    for (long i = 0; i < n; ++i)
        s[i] = static_cast<char>(values[i] % thr) + 'a';
    return s;
}
#else
std::string shiftingLetters(std::string s, std::vector<int> shifts)
{
    const long n = s.size();
    const long thr = static_cast<long>('z' - 'a' + 1);
    std::vector<long> values(n);
    for (long i = 0; i < n; ++i)
        values[i] = s[i] - 'a';
    for (long i = 0; i < n; ++i)
        for (long j = 0; j <= i; ++j)
            values[j] += shifts[i];
    for (long i = 0; i < n; ++i)
        s[i] = static_cast<char>(values[i] % thr) + 'a';
    return s;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, std::vector<int> shifts, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = shiftingLetters(s, shifts);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", {3, 5, 9}, "rpl", trials);
    test("aaa", {1, 2, 3}, "gfd", trials);
    test(testA::s, testA::shifts, testA::solution, trials);
    return 0;
}


