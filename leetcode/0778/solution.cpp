#include "../common/common.hpp"
#include <cstring>
#include <set>

// ############################################################################
// ############################################################################

#if 1
int swimInWater(std::vector<std::vector<int> > grid)
{
    const int n = static_cast<int>(grid.size());
    const int minimum_elevation = std::max(grid[0][0], grid[n - 1][n - 1]);
    std::pair<int, int> stack[5 * 50] = {};
    std::set<int> unique_heights;
    for (const auto &row : grid)
        for (const auto &e : row)
            if (e >= minimum_elevation)
                unique_heights.insert(e);
    if (unique_heights.size() == 1) return *unique_heights.begin();
    std::vector<int> heights(unique_heights.begin(), unique_heights.end());
    int begin = 0;
    bool visited[50][50];
    unsigned int number_of_points = 0;
    auto add = [&](int x, int y)
    {
        if ((x >= 0) && (y >= 0) && (x < n) && (y < n) && !visited[y][x])
            stack[number_of_points++] = {x, y};
    };
    for (int end = static_cast<int>(heights.size()) - 1; begin < end;)
    {
        int midpoint = (begin + end) / 2;
        const int current_level = heights[midpoint];
        std::memset(visited, false, sizeof(visited));
        stack[0] = {0, 0};
        number_of_points = 1;
        while (number_of_points > 0)
        {
            auto [x, y] = stack[--number_of_points];
            if (grid[y][x] > current_level) continue;
            visited[y][x] = true;
            if ((x == n - 1) && (y == n - 1)) break;
            add(x + 1, y    );
            add(x - 1, y    );
            add(x    , y + 1);
            add(x    , y - 1);
        }
        if (visited[n - 1][n - 1]) end = midpoint - 1;
        else begin = midpoint + 1;
    }
    return heights[begin];
}
#else
int swimInWater(std::vector<std::vector<int> > grid)
{
	bool visited[50][50] = {};
	int n = static_cast<int>(grid.size());
	int l = std::max(grid[n - 1][n - 1], 2 * (n - 1)), r = n * n - 1;
    auto solve = [&](auto &&self, int y, int x, int current_level) -> bool
    {
        constexpr int moves[4][2] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        if ((y < 0) || (x < 0) || (y >= n) || (x >= n))
            return false;
        if (visited[y][x])
            return false;
        if (grid[y][x] > current_level)
            return false;
        if ((y == n - 1) && (x == n - 1))
            return true;
        visited[y][x] = true;
        for (int k = 0; k < 4; ++k) 
            if (self(self, y + moves[k][0], x + moves[k][1], current_level))
                return true;
        return false;
    };
	while (l <= r)
    {
		int mid = (l + r) / 2;
		memset(visited, false, sizeof visited);
		if (solve(solve, 0, 0, mid)) r = mid - 1;
		else l = mid + 1;
	}
	return l;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = swimInWater(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 2},
          {1, 3}}, 3, trials);
    test({{ 0,  1,  2,  3,  4},
          {24, 23, 22, 21,  5},
          {12, 13, 14, 15, 16},
          {11, 17, 18, 19, 20},
          {10,  9,  8,  7,  6}}, 16, trials);
    test({{   0,    1,    2,    3,    4},
          {1024, 1023, 1022, 1021,    5},
          {  12,   13,   14,   15,  516},
          {  11, 1017, 1018, 1019, 1020},
          {  10,    9,    8,    7,    6}}, 516, trials);
    test({{10, 12,  4,  6},
          { 9, 11,  3,  5},
          { 1,  7, 13,  8},
          { 2,  0, 15, 14}}, 14, trials);
    test({{ 7, 23, 21,  9,  5},
          { 3, 20,  8, 18, 15},
          {14, 13,  1,  0, 22},
          { 2, 10, 24, 17, 12},
          { 6, 16, 19,  4,  1}}, 17, trials);
    return 0;
}


