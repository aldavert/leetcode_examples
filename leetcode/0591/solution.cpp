#include "../common/common.hpp"
#include <stack>

// ############################################################################
// ############################################################################

bool isValid(std::string code)
{
    auto skipIdentifier = [](const std::string &c, size_t position) -> size_t
    {
        const size_t start = position;
        for (; (position < c.size()) && (c[position] != '>'); ++position)
            if ((c[position] < 'A') || (c[position] > 'Z'))
                return std::string::npos;
        if ((position - start == 0) || (position - start > 9))
            return std::string::npos;
        return position + 1;
    };
    auto removeCData = [](std::string c) -> std::string
    {
        std::string result;
        for (size_t pos = 0; pos != std::string::npos;)
        {
            size_t begin = c.find("<![CDATA[", pos);
            if (begin == std::string::npos)
            {
                result += c.substr(pos, begin - pos);
                pos = begin;
            }
            else
            {
                size_t end = c.find("]]>", begin);
                if (end == std::string::npos) return "";
                result += c.substr(pos, begin -  pos) + "comment";
                pos = end + 3;
            }
        }
        return result;
    };
    code = removeCData(code);
    std::stack<std::string> labels;
    if ((code.size() < 7) || (code[0] != '<') || (code[1] == '/'))
        return false;
    size_t position = skipIdentifier(code, 1);
    if (position == std::string::npos) return false;
    labels.push(code.substr(1, position - 2));
    while (position < code.size())
    {
        size_t start = code.find('<', position);
        if (start + 1 == code.size()) return false;
        if (labels.empty()) return false;
        bool close_tag = code[start + 1] == '/';
        size_t end = skipIdentifier(code, start + close_tag + 1);
        if (end == std::string::npos) return false;
        std::string id = code.substr(start + 1 + close_tag, end - start - 2 - close_tag);
        if (close_tag)
        {
            if (labels.top() != id) return false;
            labels.pop();
        }
        else labels.push(id);
        position = end;
    }
    return labels.empty();
}

// ############################################################################
// ############################################################################

void test(std::string code, bool solution, unsigned int trials = 1)
{
    int result = false;
    for (unsigned int i = 0; i < trials; ++i)
        result = isValid(code);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("<DIV>This is the first line <![CDATA[<div>]]></DIV>", true, trials);
    test("<DIV>>>  ![cdata[]] <![CDATA[<div>]>]]>]]>>]</DIV>", true, trials);
    test("<A>  <B> </A>   </B>", false, trials);
    test("<ABCDEFGHI></ABCDEFGHI>", true, trials);
    test("<ABCDEFGHIJ></ABCDEFGHIJ>", false, trials);
    test("<A> <C> <B></C>   </B> </A>", false, trials);
    test("<A> <C></C> <B>   </B> <B>   </B> </A>", true, trials);
    test("<HI></HI><HI></HI>", false, trials);
    test("<![CDATA[ABC]]><TAG>sometext</TAG>", false, trials);
    test("<TAG>sometext</TAG><![CDATA[ABC]]>", false, trials);
    test("<TAG>sometext</TA<![CDATA[ABC]]>G>", false, trials);
    test("<HI></HI>HI", false, trials);
    test("hi<HI></HI>", false, trials);
    test("AABC>AAA</ABC>", false, trials);
    return 0;
}


