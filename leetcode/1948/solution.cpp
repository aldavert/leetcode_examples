#include "../common/common.hpp"
#include <map>
#include <unordered_map>

// ############################################################################
// ############################################################################

#if 1
std::vector<std::vector<std::string> >
deleteDuplicateFolder(std::vector<std::vector<std::string> > paths)
{
    struct Trie
    {
        void insert(std::vector<std::string>::iterator current,
                    std::vector<std::string>::iterator end)
        {
            if (current != end)
                m_children[*current].insert(++current, end);
        }
        const std::string& partialID(void)
        {
            if (!m_serial_id.empty())
                return m_serial_id;
            for (auto& [child_name, child] : m_children)
                m_serial_id += child_name + "(" + child.partialID() + "),";
            return m_serial_id;
        }
        std::map<std::string, Trie> m_children;
        std::string m_serial_id;
    };
    std::unordered_map<std::string, int> count;
    std::vector<std::string> current_path;
    std::vector<std::vector<std::string> > nonduplicates;
    auto countIDS = [&](auto &&self, Trie &current) -> void
    {
        ++count[current.partialID()];
        for (auto& [child_name, child] : current.m_children)
            self(self, child);
    };
    auto gatherNonduplicates = [&](auto &&self, Trie &current) -> void
    {
        if (current.partialID().empty())
        {
            nonduplicates.push_back(current_path);
            return;
        }
        if (count.at(current.partialID()) != 1) return;
        if (!current_path.empty()) nonduplicates.push_back(current_path);
        
        for (auto& [child_name, child] : current.m_children)
        {
            current_path.push_back(child_name);
            self(self, child);
            current_path.pop_back();
        }
    };
    
    Trie root;
    for (auto &path : paths)
        root.insert(path.begin(), path.end());
    countIDS(countIDS, root);
    gatherNonduplicates(gatherNonduplicates, root);
    return nonduplicates;
}
#else
std::vector<std::vector<std::string> >
deleteDuplicateFolder(std::vector<std::vector<std::string> > paths)
{
    struct TrieNode
    {
        std::unordered_map<std::string, std::shared_ptr<TrieNode> > children;
        bool deleted = false;
    };
    using TriePtr = std::shared_ptr<TrieNode>;
    std::vector<std::vector<std::string> > result;
    std::vector<std::string> path;
    std::unordered_map<std::string, std::vector<TriePtr> > subtree_to_nodes;
    TriePtr root = std::make_shared<TrieNode>();
    auto buildSubtreeToRoots =
        [&](auto &&self, TriePtr node) -> std::string
    {
        std::string subtree = "(";
        for (const auto& [s, child] : node->children)
            subtree += s + self(self, child);
        subtree += ")";
        if (subtree != "()")
            subtree_to_nodes[subtree].push_back(node);
        return subtree;
    };
    auto constructPath = [&](auto &&self, TriePtr node) -> void
    {
        for (const auto& [s, child] : node->children)
        {
            if (!child->deleted)
            {
                path.push_back(s);
                self(self, child);
                path.pop_back();
            }
        }
        if (!path.empty()) result.push_back(path);
    };
    
    std::sort(paths.begin(), paths.end());
    for (auto &p : paths)
    {
        TriePtr node = root;
        for (const auto &s : p)
        {
            if (!node->children.count(s))
                node->children[s] = std::make_shared<TrieNode>();
            node = node->children[s];
        }
    }
    
    buildSubtreeToRoots(buildSubtreeToRoots, root);
    for (const auto& [_, nodes] : subtree_to_nodes)
        if (nodes.size() > 1)
            for (auto node : nodes)
                node->deleted = true;
    
    constructPath(constructPath, root);
    return result;
}
#endif

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::vector<std::string> > &left,
                const std::vector<std::vector<std::string> > &right)
{
    if (left.size() != right.size()) return false;
    std::map<std::vector<std::string>, int> histogram;
    for (const auto &path : left)
        ++histogram[path];
    for (const auto &path : right)
    {
        auto search = histogram.find(path);
        if (search == histogram.end()) return false;
        --search->second;
        if (search->second == 0) histogram.erase(search);
    }
    return histogram.empty();
}

void test(std::vector<std::vector<std::string> > paths,
          std::vector<std::vector<std::string> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<std::string> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = deleteDuplicateFolder(paths);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{"a"}, {"c"}, {"d"}, {"a", "b"}, {"c", "b"}, {"d", "a"}},
         {{"d"}, {"d", "a"}}, trials);
    test({{"a"}, {"c"}, {"a", "b"}, {"c", "b"}, {"a", "b", "x"},
          {"a", "b", "x", "y"}, {"w"}, {"w", "y"}},
         {{"c"}, {"c", "b"}, {"a"}, {"a", "b"}}, trials);
    test({{"a", "b"}, {"c", "d"}, {"c"}, {"a"}},
         {{"c"}, {"c", "d"}, {"a"}, {"a", "b"}}, trials);
    return 0;
}


