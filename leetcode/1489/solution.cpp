#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> >
findCriticalAndPseudoCriticalEdges(int n, std::vector<std::vector<int> > edges)
{
    std::vector<int> critical_edges, pseudo_critical_edges, id(n), rank(n);
    for (int i = 0, m = static_cast<int>(edges.size()); i < m; ++i)
        edges[i].push_back(i);
    std::sort(edges.begin(), edges.end(),
              [](const auto &a, const auto &b) { return a[2] < b[2]; });
    auto find = [&](auto &&self, int u) -> int
    {
        return (id[u] == u)?u:id[u] = self(self, id[u]);
    };
    auto merge = [&](int u, int v) -> void
    {
        const int i = find(find, u);
        const int j = find(find, v);
        if (i == j) return;
        if      (rank[i] < rank[j]) id[i] = id[j];
        else if (rank[i] > rank[j]) id[j] = id[i];
        else                        id[i] = id[j], ++rank[j];
    };
    auto getMSTWeight = [&](const std::vector<int> &first_edge,
                            int deleted_edge_index) -> int
    {
        int mst_weight = 0;
        for (int i = 0; i < n; ++i) id[i] = i;
        
        if (!first_edge.empty())
        {
            merge(first_edge[0], first_edge[1]);
            mst_weight += first_edge[2];
        }
        for (const auto &edge : edges)
        {
            const int u = edge[0];
            const int v = edge[1];
            const int weight = edge[2];
            const int index = edge[3];
            if (index == deleted_edge_index) continue;
            if (find(find, u) == find(find, v)) continue;
            merge(u, v);
            mst_weight += weight;
        }
        const int root = find(find, 0);
        for (int i = 0; i < n; ++i)
            if (find(find, i) != root)
                return std::numeric_limits<int>::max();
        return mst_weight;
    };
    const int mst_weight = getMSTWeight({}, -1);
    for (const auto &edge : edges)
    {
        const int index = edge[3];
        if      (getMSTWeight({}, index) > mst_weight)
            critical_edges.push_back(index);
        else if (getMSTWeight(edge, -1) == mst_weight)
            pseudo_critical_edges.push_back(index);
    }
    return {critical_edges, pseudo_critical_edges};
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<std::vector<int> > edges,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findCriticalAndPseudoCriticalEdges(n, edges);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, {{0, 1, 1}, {1, 2, 1}, {2, 3, 2}, {0, 3, 2}, {0, 4, 3}, {3, 4, 3},
             {1, 4, 6}}, {{0, 1}, {2, 3, 4, 5}}, trials);
    test(4, {{0, 1, 1}, {1, 2, 1}, {2, 3, 1}, {0, 3, 1}}, {{}, {0, 1, 2, 3}}, trials);
    return 0;
}


