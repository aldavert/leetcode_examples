# Find Critical and Pseudo-Critical Edges in Minimum Spanning Trees

Given a weighted undirected connected graph with `n` vertices numbered from `0` to `n - 1`, and an array `edges` where `edges[i] = [a_i, b_i, weight_i]` represents a bidirectional and weighted edge between nodes `a_i` and `b_i`. A minimum spanning tree (MST) is a subset of the graph's edges that connects all vertices without cycles and with the minimum possible total edge weight.

Find *all the critical and pseudo-critical edges in the given graph's minimum spanning tree (MST)*. An MST edge whose deletion from the graph would cause the MST weight to increase is called a *critical edge*. On the other hand, a pseudo-critical edge is that which can appear in some MSTs but not all.

Note that you can return the indices of the edges in any order.

#### Example 1:
> ```mermaid
> graph TD;
> A((2))-- <1, 1> ---B((1))
> A-- <2, 2> ---C((3))
> B-- <6, 6> ---E((4))
> C-- <5, 3> ---E
> D-- <4, 3> ---E
> B-- <0, 1> ---D((0))
> C-- <3, 2> ---D
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `n = 5, edges = [[0, 1, 1], [1, 2, 1], [2, 3, 2], [0, 3, 2], [0, 4, 3], [3, 4, 3], [1, 4, 6]]`  
> *Output:* `[[0, 1], [2, 3, 4, 5]]`  
> *Explanation:* The figure above describes the graph. The following figure shows all the possible MSTs:
> ```mermaid
> graph TD;
> A1((2))-- <1, 1> ---B1((1))
> A1-- <2, 2> ---C1((3))
> C1-- <5, 3> ---E1((4))
> B1-- <0, 1> ---D1((0))
> 
> A2((2))-- <1, 1> ---B2((1))
> B2-- <0, 1> ---D2((0))
> D2-- <4, 3> ---E2((4))
> A2-- <2, 2> ---C2((3))
> 
> A3((2))-- <1, 1> ---B3((1))
> B3-- <0, 1> ---D3((0))
> D3-- <3, 2> ---C3((3))
> C3-- <5, 3> ---E3((4))
> 
> A4((2))-- <1, 1> ---B4((1))
> B4-- <0, 1> ---D4((0))
> D4-- <4, 3> ---E4((4))
> D4-- <3, 2> ---C4((3))
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> Notice that the two edges `0` and `1` appear in all MSTs, therefore they are critical edges, so we return them in the first list of the output. The edges `2`, `3`, `4`, and `5` are only part of some MSTs, therefore they are considered pseudo-critical edges. We add them to the second list of the output.

#### Example 2:
> ```mermaid
> graph TD;
> A((1))-- <0, 1> ---B((0))
> A-- <1, 1> ---C((2))
> B-- <3, 1> ---D((3))
> C-- <2, 1> ---D
> classDef default fill:#FFF,stroke:#000,stroke-width:2px;
> ```
> *Input:* `n = 4, edges = [[0, 1, 1], [1, 2, 1], [2, 3, 1], [0, 3, 1]]`  
> *Output:* `[[], [0, 1, 2, 3]]`  
> *Explanation:* We can observe that since all `4` edges have equal weight, choosing any `3` edges from the given `4` will yield an MST. Therefore all `4` edges are pseudo-critical.

##### Constraints:
- `2 <= n <= 100`
- `1 <= edges.length <= min(200, n * (n - 1) / 2)`
- `edges[i].length == 3`
- `0 <= a_i < b_i < n`
- `1 <= weight_i <= 1000`
- All pairs `(a_i, b_i)` are distinct.


