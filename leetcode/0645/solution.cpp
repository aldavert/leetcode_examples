#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
std::vector<int> findErrorNums(std::vector<int> nums)
{
    const size_t n = nums.size();
    std::vector<char> lut(n + 1, 0);
    size_t repeated = 0, skipped = 0;
    for (int value : nums)
        ++lut[value];
    for (size_t i = 1; i <= n; ++i)
    {
        if      (lut[i] == 0) skipped  = i;
        else if (lut[i] == 2) repeated = i;
    }
    return {static_cast<int>(repeated), static_cast<int>(skipped)};
}
#else
std::vector<int> findErrorNums(std::vector<int> nums)
{
    std::sort(nums.begin(), nums.end());
    int previous = 0, repeated = 0, skipped = 0;
    for (int n : nums)
    {
        if (n - previous == 0)
            repeated = n;
        else if (n - previous > 1)
            skipped = n;
        previous = n;
    }
    return {repeated, (skipped != 0)?(skipped - 1):static_cast<int>(nums.size())};
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findErrorNums(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 4}, {2, 3}, trials);
    test({1, 1}, {1, 2}, trials);
    return 0;
}


