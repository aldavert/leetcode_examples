#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <set>
#include <unordered_map>

bool operator==(const std::vector<std::vector<int> > &left,
                const std::vector<std::vector<int> > &right)
{
    std::set<std::vector<int> > unique(left.begin(), left.end());
    for (const auto &vec : right)
    {
        if (auto search = unique.find(vec); search != unique.end())
            unique.erase(search);
        else return false;
    }
    return unique.empty();
}

// ############################################################################
// ############################################################################

std::vector<TreeNode *> findDuplicateSubtrees(TreeNode * root)
{
    std::unordered_map<std::string, size_t> histogram;
    std::vector<TreeNode *> result;
    auto traverse = [&](auto &&self, TreeNode * ptr) -> std::string
    {
        if (ptr == nullptr) return "";
        std::string code = std::to_string(ptr->val) + '#'
                         + self(self, ptr->left) + '#'
                         + self(self, ptr->right);
        if (++histogram[code] == 2) result.push_back(ptr);
        return code;
    };
    traverse(traverse, root);
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> tree,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        auto output = findDuplicateSubtrees(root);
        result.clear();
        for (auto &ptr : output)
            result.push_back(tree2vec(ptr));
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, null, 2, 4, null, null, 4}, {{2, 4}, {4}}, trials);
    test({2, 1, 1}, {{1}}, trials);
    test({2, 2, 2, 3, null, 3, null}, {{2, 3}, {3}}, trials);
    return 0;
}


