#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int minCost(std::vector<int> startPos,
            std::vector<int> homePos,
            std::vector<int> rowCosts,
            std::vector<int> colCosts)
{
    int result = 0;
    for (int i = startPos[0], d = 2 * (i < homePos[0]) - 1; i != homePos[0];)
        result += rowCosts[i += d];
    for (int i = startPos[1], d = 2 * (i < homePos[1]) - 1; i != homePos[1];)
        result += colCosts[i += d];
    return result;
}
#else
int minCost(std::vector<int> startPos,
            std::vector<int> homePos,
            std::vector<int> rowCosts,
            std::vector<int> colCosts)
{
    int result = 0;
    for (int i = startPos[0]; i != homePos[0]; )
        result += (i < homePos[0])?rowCosts[++i]:rowCosts[--i];
    for (int i = startPos[1]; i != homePos[1]; )
        result += (i < homePos[1])?colCosts[++i]:colCosts[--i];
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> startPos,
          std::vector<int> homePos,
          std::vector<int> rowCosts,
          std::vector<int> colCosts,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCost(startPos, homePos, rowCosts, colCosts);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0}, {2, 3}, {5, 4, 3}, {8, 2, 6, 7}, 18, trials);
    test({0, 0}, {0, 0}, {5}, {26}, 0, trials);
    return 0;
}


