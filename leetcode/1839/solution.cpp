#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestBeautifulSubstring(std::string word)
{
    int result = 0;
    
    for (int l = 0, r = 1, count = 1, n = static_cast<int>(word.size()); r < n; ++r)
    {
        const char curr = word[r];
        const char prev = word[r - 1];
        if (curr >= prev)
        {
            count += (curr > prev);
            if (count == 5)
                result = std::max(result, r - l + 1);
        }
        else
        {
            count = 1;
            l = r;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string word, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestBeautifulSubstring(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aeiaaioaaaaeiiiiouuuooaauuaeiu", 13, trials);
    test("aeeeiiiioooauuuaeiou", 5, trials);
    test("a", 0, trials);
    return 0;
}


