#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int possibleStringCount(std::string word)
{
    int result = 1, count = 0;
    for (int i = 1, n = static_cast<int>(word.size()); i < n; ++i)
    {
        if (word[i] != word[i - 1])
        {
            result += count;
            count = 0;
        }
        else ++count;
    }
    return result + count;
}

// ############################################################################
// ############################################################################

void test(std::string word, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = possibleStringCount(word);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abbcccc", 5, trials);
    test("abcd", 1, trials);
    test("aaaa", 4, trials);
    return 0;
}


