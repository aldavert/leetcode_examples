#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

int maxScore(std::vector<std::vector<int> > grid)
{
    std::unordered_map<int, std::unordered_set<int> > num_to_indices_lut;
    for (int index = 0, n = static_cast<int>(grid.size()); index < n; ++index)
        for (const int num : grid[index])
            num_to_indices_lut[num].insert(index);
    std::vector<std::vector<int> > mem(num_to_indices_lut.size(),
            std::vector<int>(1 << grid.size()));
    std::vector<std::pair<int, std::unordered_set<int> > > num_to_indices(
            num_to_indices_lut.begin(), num_to_indices_lut.end());
    const int n = static_cast<int>(num_to_indices.size());
    auto maxScore = [&](auto &&self, int i, int mask) -> int
    {
        if (i == n) return 0;
        if (mem[i][mask] != 0) return mem[i][mask];
        int result = self(self, i + 1, mask);
        for (const int index : num_to_indices[i].second)
            if ((mask >> index & 1) == 0)
                result = std::max(result, num_to_indices[i].first
                                        + self(self, i + 1, mask | 1 << index));
        return mem[i][mask] = result;
    };
    return maxScore(maxScore, 0, 0);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxScore(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {4, 3, 2}, {1, 1, 1}}, 8, trials);
    test({{8, 7, 6}, {8, 3, 2}}, 15, trials);
    return 0;
}


