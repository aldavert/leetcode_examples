#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minScoreTriangulation(std::vector<int> values)
{
    const int n = static_cast<int>(values.size());
    int dp[51][51] = {};
    for (int j = 2; j < n; ++j)
        for (int i = j - 2, k = 0; i >= 0; --i)
            for (k = i + 1, dp[i][j] = std::numeric_limits<int>::max(); k < j; ++k)
                dp[i][j] = std::min(dp[i][j], dp[i][k] + dp[k][j]
                                            + values[i] * values[k] * values[j]);
    return dp[0][n - 1];
}

// ############################################################################
// ############################################################################

void test(std::vector<int> values, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minScoreTriangulation(values);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 6, trials);
    test({3, 7, 4, 5}, 144, trials);
    test({1, 3, 1, 4, 1, 5}, 13, trials);
    return 0;
}


