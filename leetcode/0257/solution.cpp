#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<std::string> binaryTreePaths(TreeNode* root)
{
    std::vector<std::string> result;
    auto traverse = [&](auto &&self, TreeNode * current, std::string path) -> void
    {
        if (current)
        {
            if (!path.empty()) path += "->";
            path += std::to_string(current->val);
            if (!current->left && !current->right)
                result.push_back(path);
            self(self, current->left, path);
            self(self, current->right, path);
        }
    };
    traverse(traverse, root, "");
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    std::unordered_map<std::string, int> histogram;
    for (std::string s : left)
        ++histogram[s];
    for (std::string s : right)
    {
        if (auto search = histogram.find(s); search != histogram.end())
        {
            --search->second;
            if (!search->second)
                histogram.erase(search);
        }
        else return false;
    }
    return histogram.empty();
}

void test(std::vector<int> tree,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = binaryTreePaths(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, null, 5}, {"1->2->5", "1->3"}, trials);
    test({1}, {"1"}, trials);
    return 0;
}


