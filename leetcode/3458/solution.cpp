#include "../common/common.hpp"
#include <deque>
#include <queue>

// ############################################################################
// ############################################################################

#if 1
bool maxSubstringLength(std::string s, int k)
{
    const int n = static_cast<int>(s.size());
    int first[26], end[26], last[26];
    
    for (int i = 0; i < 26; ++i) first[i] = end[i] = last[i] = -1;
    for (int i = 0; i < n; ++i)
    {
        if (first[s[i] - 'a'] == -1)
            first[s[i] - 'a'] = i;
        end[s[i] - 'a'] = i;
    }
    for (int i = 0; i < 26; ++i)
    {
        if (first[i] == -1) continue;
        int max_i = end[i];
        for (int j = first[i]; j < n; ++j)
        {
            max_i = std::max(max_i, end[s[j] - 'a']);
            if (first[s[j] - 'a'] < first[i]) break;
            if (j == max_i)
            {
                last[i] = j + 1;
                break;
            }
        }
        if ((first[i] == 0) && (last[i] == n))
            last[i] = -1;
    }
    std::vector<int> dp(n, -1);
    auto compute = [&](auto &&self, int i) -> int
    {
        if (i == n) return 0;
        if (dp[i] != -1) return dp[i];
        int result = self(self, i + 1);
        if ((last[s[i] - 'a'] != -1) && (i == first[s[i] - 'a']))
            result = std::max(result, 1 + self(self, last[s[i] - 'a']));
        return dp[i] = result;
    };
    return compute(compute, 0) >= k;
}
#else
bool maxSubstringLength(std::string s, int k)
{
    const int n = static_cast<int>(s.size());
    int first[26], last[26];
    for (int i = 0; i < 26; ++i) first[i] = last[i] = -1;
    for (int i = 0; i < n; ++i)
    {
        if (first[s[i] - 'a'] == -1) first[s[i] - 'a'] = i;
        last[s[i] - 'a'] = i;
    }
    for (int i = 0; i < 26; ++i)
    {
        if (first[i] == -1) continue;
        int left = first[i], right = last[i];
        std::deque<int> calc;
        for (int j = left + 1; j < right; ++j)
            calc.push_back(j);
        
        while (!calc.empty())
        {
            int x = calc.front();
            calc.pop_front();
            int leftx = first[s[x] - 'a'], rightx = last[s[x] - 'a'];
            for (int j = leftx + 1; j < left; ++j)
                calc.push_back(j);
            for (int j = right + 1; j < rightx; ++j)
                calc.push_back(j);
            left = std::min(left, leftx);
            right = std::max(right, rightx);
        }
        first[i] = left;
        last[i] = right;
    }
    std::priority_queue<std::tuple<int, int, int, int>,
                        std::vector<std::tuple<int, int, int, int> >,
                        std::greater<> > events;
    for (int i = 0; i < 26; ++i)
    {
        if (first[i] == -1) continue;
        if ((first[i] != 0) || (last[i] != n - 1))
            events.push({first[i], i + 'a', 1, -1});
    }
    int most_so_far = 0;
    while (!events.empty())
    {
        auto [_, c, t, m] = events.top();
        events.pop();
        if (t > 0)
            events.push({last[c - 'a'], c, -1, most_so_far + 1});
        else most_so_far = std::max(m, most_so_far);
        if (most_so_far >= k) return true;
    }
    return most_so_far >= k;
}
#endif

// ############################################################################
// ############################################################################

void test(std::string s, int k, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSubstringLength(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcdbaefab", 2, true, trials);
    test("cdefdc", 3, false, trials);
    test("abeabe", 0, true, trials);
    test("nbuirvanjiccnsyyyoirleqsrwrvxepaglcidqplyryujytzqoncxjgwdmatytgwhzyhls"
         "odrbzrpbbitovtdasazjtoyyfhowqqrzuvjveydceouscrfazzoblqhalhfybwheybkpcr"
         "oijxvarrtqrqnmwslkpdducfeblvfecyjyulxgahxlzlyztssfzwvfujrriryslkvdwhmk"
         "cyebfhkadrahunvxivkwitilyzknwyujtylahgmlddymlbrbrniomepbmdieasuvdcqnzf"
         "wspxewbbpruxrznjxwnjjxvblxyrgv", 1, false, trials);
    return 0;
}


