#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int sumIndicesWithKSetBits(std::vector<int> nums, int k)
{
    auto countBits = [](size_t number) -> int
    {
        int nbits = 0;
        for (; number; number /= 2) nbits += number & 1;
        return nbits;
    };
    int result = 0;
    for (size_t i = 0; i < nums.size(); ++i)
        if (countBits(i) == k)
            result += nums[i];
    return result;
}
#else
int sumIndicesWithKSetBits(std::vector<int> nums, int k)
{
    int result = 0;
    for (int i = 0, n = static_cast<int>(nums.size()); i < n; ++i)
        if (__builtin_popcount(i) == k)
            result += nums[i];
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumIndicesWithKSetBits(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 10, 1, 5, 2}, 1, 13, trials);
    test({4, 3, 2, 1}, 2, 1, trials);
    return 0;
}


