#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::vector<std::vector<int> > flipAndInvertImage(std::vector<std::vector<int> > image)
{
    const int n = static_cast<int>(image.size());
    std::vector<std::vector<int> > result = image;
    for (int i = 0; i < n; ++i)
    {
        int l, r;
        for (l = 0, r = n - 1; l < r; ++l, --r)
        {
            std::swap(result[i][l], result[i][r]);
            result[i][l] = !result[i][l];
            result[i][r] = !result[i][r];
        }
        if (l == r) result[i][l] = !result[i][l];
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > image,
          std::vector<std::vector<int> > solution,
          unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = flipAndInvertImage(image);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 0}, {1, 0, 1}, {0, 0, 0}}, {{1, 0, 0}, {0, 1, 0}, {1, 1, 1}}, trials);
    test({{1, 1, 0, 0}, {1, 0, 0, 1}, {0, 1, 1, 1}, {1, 0, 1, 0}}, {{1, 1, 0, 0}, {0, 1, 1, 0}, {0, 0, 0, 1}, {1, 0, 1, 0}}, trials);
    return 0;
}


