#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maximumSwap(int num)
{
    if (num < 10) return num;
    std::vector<int> digits;
    for (int position = 0; num > 0; num /= 10, ++position)
        digits.push_back(num % 10);
    for (int i = static_cast<int>(digits.size() - 1); i > 0; --i)
    {
        int selected = -1, value = digits[i];
        for (int j = 0; j < i; ++j)
        {
            if (value < digits[j])
            {
                value = digits[j];
                selected = j;
            }
        }
        if (selected != -1)
        {
            std::swap(digits[i], digits[selected]);
            break;
        }
    }
    for (int i = static_cast<int>(digits.size() - 1); i >= 0; --i)
        num = num * 10 + digits[i];
    return num;
}

// ############################################################################
// ############################################################################

void test(int num, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumSwap(num);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(2736, 7236, trials);
    test(9973, 9973, trials);
    test(98368, 98863, trials);
    test(1993, 9913, trials);
    return 0;
}


