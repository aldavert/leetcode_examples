#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxEnvelopes(std::vector<std::vector<int> > envelopes)
{
    const int n = static_cast<int>(envelopes.size());
    auto cmp = [](const std::vector<int> &a, const std::vector<int> &b)
    {
        return ((a[0] == b[0])?(b[1] - a[1]):a[0] - b[0]) < 0;
    };
    std::sort(envelopes.begin(), envelopes.end(), cmp);
    std::vector<int> buffer(n);
    int size = 1;
    buffer[0] = envelopes[0][1];
    for(int i = 1; i < n; ++i)
    {
        auto pos = std::lower_bound(buffer.begin(),
                                    buffer.begin() + size,
                                    envelopes[i][1]) - buffer.begin();
        if (pos >= size) buffer[size++] = envelopes[i][1];
        else buffer[pos] = envelopes[i][1];
        
    }
    return size;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > envelopes,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxEnvelopes(envelopes);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{5, 4}, {6, 4}, {6, 7}, {2, 3}}, 3, trials);
    test({{1, 1}, {1, 1}, {1, 1}}, 1, trials);
    return 0;
}


