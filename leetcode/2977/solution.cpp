#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

long long minimumCost(std::string source,
                      std::string target,
                      std::vector<std::string> original,
                      std::vector<std::string> changed,
                      std::vector<int> cost)
{
    constexpr long MAX = std::numeric_limits<long>::max();
    std::unordered_set<int> sub_lengths;
    for (const std::string &s : original)
        sub_lengths.insert(static_cast<int>(s.size()));
    
    std::unordered_map<std::string, int> sub_to_id;
    for (const std::string &s : original)
        if (!sub_to_id.contains(s))
            sub_to_id[s] = static_cast<int>(sub_to_id.size());
    for (const std::string &s : changed)
        if (!sub_to_id.contains(s))
            sub_to_id[s] = static_cast<int>(sub_to_id.size());
    
    const int sub_count = static_cast<int>(sub_to_id.size());
    std::vector<std::vector<long> > dist(sub_count,
            std::vector<long>(sub_count, MAX));
    std::vector<long> dp(source.size() + 1, MAX);
    
    for (size_t i = 0; i < cost.size(); ++i)
    {
        const int u = sub_to_id.at(original[i]),
                  v = sub_to_id.at(changed[i]);
        dist[u][v] = std::min(dist[u][v], static_cast<long>(cost[i]));
    }
    for (int k = 0; k < sub_count; ++k)
        for (int i = 0; i < sub_count; ++i)
            if (dist[i][k] < MAX)
                for (int j = 0; j < sub_count; ++j)
                    if (dist[k][j] < MAX)
                        dist[i][j] = std::min(dist[i][j], dist[i][k] + dist[k][j]);
    dp[0] = 0;
    for (size_t i = 0; i < source.size(); ++i)
    {
        if (dp[i] == MAX) continue;
        if (target[i] == source[i])
            dp[i + 1] = std::min(dp[i + 1], dp[i]);
        for (const int sub_length : sub_lengths)
        {
            if (i + sub_length > source.size()) continue;
            const std::string sub_source = source.substr(i, sub_length);
            const std::string sub_target = target.substr(i, sub_length);
            if (!sub_to_id.contains(sub_source) || !sub_to_id.contains(sub_target))
                continue;
            const int u = sub_to_id.at(sub_source);
            const int v = sub_to_id.at(sub_target);
            if (dist[u][v] < MAX)
                dp[i + sub_length] = std::min(dp[i + sub_length], dp[i] + dist[u][v]);
        }
    }
    return (dp[source.length()] == MAX)?-1:dp[source.size()];
}

// ############################################################################
// ############################################################################

void test(std::string source,
          std::string target,
          std::vector<std::string> original,
          std::vector<std::string> changed,
          std::vector<int> cost,
          long long solution,
          unsigned int trials = 1)
{
    long long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumCost(source, target, original, changed, cost);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcd", "acbe", {"a", "b", "c", "c", "e", "d"},
         {"b", "c", "b", "e", "b", "e"}, {2, 5, 5, 1, 2, 20}, 28, trials);
    test("abcdefgh", "acdeeghh", {"bcd", "fgh", "thh"},
         {"cde", "thh", "ghh"}, {1, 3, 5}, 9, trials);
    test("abcdefgh", "addddddd", {"bcd", "defgh"},
         {"ddd", "ddddd"}, {100, 1578}, -1, trials);
    return 0;
}


