#include "../common/common.hpp"
#include <cmath>

// ############################################################################
// ############################################################################

int maximumStrongPairXor(std::vector<int> nums)
{
    class BitTrie
    {
        struct TrieNode
        {
            TrieNode(void) = default;
            ~TrieNode(void)
            {
                delete children[0];
                delete children[1];
            }
            TrieNode * children[2] = { nullptr, nullptr };
            int min_value = std::numeric_limits<int>::max();
            int max_value = std::numeric_limits<int>::lowest();
        };
        const int m_max_bit;
        TrieNode m_root;
    public:
        BitTrie(int max_bit) : m_max_bit(max_bit) {}
        void insert(int num)
        {
            TrieNode * node = &m_root;
            for (int i = m_max_bit; i >= 0; --i)
            {
                const int bit = (num >> i) & 1;
                if (node->children[bit] == nullptr)
                    node->children[bit] = new TrieNode();
                node = node->children[bit];
                node->min_value = std::min(node->min_value, num);
                node->max_value = std::max(node->max_value, num);
            }
        }
        int maxXOR(int x) const
        {
            int result = 0;
            const TrieNode * node = &m_root;
            for (int i = m_max_bit; i >= 0; --i)
            {
                const int bit = (x >> i) & 1;
                const int toggle_bit = bit ^ 1;
                if ((node->children[toggle_bit] != nullptr)
                &&  (node->children[toggle_bit]->max_value > x)
                &&  (node->children[toggle_bit]->min_value <= 2 * x))
                {
                    result = result | (1 << i);
                    node = node->children[toggle_bit];
                }
                else if (node->children[bit] != nullptr)
                    node = node->children[bit];
                else return 0;
            }
            return result;
        }
    };
    const int max_num = *std::max_element(nums.begin(), nums.end()),
              max_bit = static_cast<int>(std::log2(max_num));
    int result = 0;
    BitTrie trie(max_bit);
    
    for (const int num : nums) trie.insert(num);
    for (const int num : nums) result = std::max(result, trie.maxXOR(num));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumStrongPairXor(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 7, trials);
    test({10, 100}, 0, trials);
    test({500, 520, 2500, 3000}, 1020, trials);
    return 0;
}


