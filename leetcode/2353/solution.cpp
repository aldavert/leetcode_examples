#include "../common/common.hpp"
#include <unordered_map>
#include <set>

// ############################################################################
// ############################################################################

#if 1
class FoodRatings
{
    struct Food
    {
        std::string name;
        int rating = 0;
        bool operator<(const Food &other) const
        {
            return (rating == other.rating)
                ?(name < other.name)
                :(rating > other.rating);
        }
    };
    std::unordered_map<std::string, Food> food_to_cuisine;
    std::unordered_map<std::string, std::set<Food> > rated_cuisine;
public:
    FoodRatings(const std::vector<std::string> &foods,
                const std::vector<std::string> &cuisines,
                const std::vector<int> &ratings)
    {
        for (size_t i = 0; i < foods.size(); ++i)
        {
            food_to_cuisine[foods[i]] = {cuisines[i], ratings[i]};
            rated_cuisine[cuisines[i]].insert({foods[i], ratings[i]});
        }
    }
    void changeRating(std::string food, int newRating)
    {
        auto &[name, old_rating] = food_to_cuisine[food];
        auto &s = rated_cuisine[name];
        s.erase({food, old_rating});
        old_rating = newRating;
        s.insert({food, newRating});
    }
    std::string highestRated(std::string cuisine)
    {
        return rated_cuisine[cuisine].begin()->name;
    }
};
#else
class FoodRatings
{
    struct Dishes
    {
        std::unordered_map<std::string, int> ratings;
        int highest_rate = 0;
        std::string highest_dish = "";
    };
    std::unordered_map<std::string, Dishes> information;
    std::unordered_map<std::string, std::string> dish_to_cuisine;
public:
    FoodRatings(const std::vector<std::string> &foods,
                const std::vector<std::string> &cuisines,
                const std::vector<int> &ratings)
    {
        for (size_t i = 0; i < foods.size(); ++i)
        {
            dish_to_cuisine[foods[i]] = cuisines[i];
            information[cuisines[i]].ratings[foods[i]] = ratings[i];
            auto &record = information.find(cuisines[i])->second;
            if ((ratings[i] >  record.highest_rate)
            || ((ratings[i] == record.highest_rate)
            &&  (foods[i] < record.highest_dish)))
            {
                record.highest_rate = ratings[i];
                record.highest_dish = foods[i];
            }
        }
    }
    void changeRating(std::string food, int newRating)
    {
        if (auto search = dish_to_cuisine.find(food);
            search != dish_to_cuisine.end())
        {
            auto &record = information.find(search->second)->second;
            bool research = ((record.highest_dish == food)
                         && (newRating < record.ratings[food]))
                         || ((record.highest_dish != food)
                         && (newRating == record.highest_rate));
            if (newRating > record.highest_rate)
            {
                record.highest_rate = newRating;
                record.highest_dish = food;
            }
            record.ratings[food] = newRating;
            if (research)
            {
                record.highest_rate = 0;
                record.highest_dish = "";
                for (const auto &[name, value] : record.ratings)
                {
                    if ((value >  record.highest_rate)
                    || ((value == record.highest_rate)
                    &&  (name < record.highest_dish)))
                    {
                        record.highest_rate = value;
                        record.highest_dish = name;
                    }
                }
            }
        }
    }
    std::string highestRated(std::string cuisine)
    {
        if (auto search = information.find(cuisine);
            search != information.end())
            return search->second.highest_dish;
        else return "";
    }
};
#endif

// ############################################################################
// ############################################################################

void test(std::vector<std::string> foods,
          std::vector<std::string> cuisines,
          std::vector<int> ratings,
          std::vector<bool> change_ratings,
          std::vector<std::string> name,
          std::vector<int> rating,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int t = 0; t < trials; ++t)
    {
        FoodRatings object(foods, cuisines, ratings);
        result.clear();
        for (size_t i = 0; i < change_ratings.size(); ++i)
        {
            if (change_ratings[i])
            {
                object.changeRating(name[i], rating[i]);
                result.push_back("");
            }
            else result.push_back(object.highestRated(name[i]));
        }
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"kimchi", "miso", "sushi", "moussaka", "ramen", "bulgogi"},
         {"korean", "japanese", "japanese", "greek", "japanese", "korean"},
         {9, 12, 8, 15, 14, 7},
         {   false,      false,    true,      false,    true,      false},
         {"korean", "japanese", "sushi", "japanese", "ramen", "japanese"},
         {       0,          0,      16,          0,      16,          0},
         {"kimchi", "ramen", "", "sushi", "", "ramen"}, trials);
    return 0;
}


