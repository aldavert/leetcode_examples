#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool hasSameDigits(std::string s)
{
    std::vector<int> digits(s.size());
    for (size_t i = 0; i < s.size(); ++i)
        digits[i] = s[i] - '0';
    while (digits.size() != 2)
    {
        for (size_t i = 1; i < digits.size(); ++i)
            digits[i - 1] = (digits[i - 1] + digits[i]) % 10;
        digits.pop_back();
    }
    return digits[0] == digits[1];
}

// ############################################################################
// ############################################################################

void test(std::string s, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = hasSameDigits(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("3902", true, trials);
    test("34789", false, trials);
    return 0;
}


