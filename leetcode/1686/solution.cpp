#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int stoneGameVI(std::vector<int> aliceValues, std::vector<int> bobValues)
{
    const int n = static_cast<int>(aliceValues.size());
    struct Value
    {
        int alice = -1;
        int bob = -1;
        bool operator<(const Value &other) const
        {
            return alice + bob > other.alice + other.bob;
        }
    };
    std::vector<Value> values;
    int a = 0, b = 0;

    for (int i = 0; i < n; ++i)
        values.push_back({aliceValues[i], bobValues[i]});
    std::sort(values.begin(), values.end());
    for (int i = 0; i < n; ++i)
    {
        if (i % 2 == 0) a += values[i].alice;
        else b += values[i].bob;
    }
    return (a > b)?1:((a < b)?-1:0);
}

// ############################################################################
// ############################################################################

void test(std::vector<int> aliceValues,
          std::vector<int> bobValues,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = stoneGameVI(aliceValues, bobValues);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3}, {2, 1}, 1, trials);
    test({1, 2}, {3, 1}, 0, trials);
    test({2, 4, 3}, {1, 6, 7}, -1, trials);
    return 0;
}


