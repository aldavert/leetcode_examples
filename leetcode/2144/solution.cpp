#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumCost(std::vector<int> cost)
{
    const int n = static_cast<int>(cost.size());
    std::sort(cost.begin(), cost.end());
    int i = n - 1, result = 0;
    for (; i >= 2; i -= 3) result += cost[i] + cost[i - 1];
    for (; i >= 0; --i) result += cost[i];
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> cost, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumCost(cost);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 5, trials);
    test({6, 5, 7, 9, 2, 2}, 23, trials);
    test({5, 5}, 10, trials);
    return 0;
}


