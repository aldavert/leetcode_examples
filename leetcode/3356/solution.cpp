#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minZeroArray(std::vector<int> nums, std::vector<std::vector<int> > queries)
{
    const int nn = static_cast<int>(nums.size()),
              nq = static_cast<int>(queries.size());
    std::vector<int> line(nums.size() + 1);
    int k = 0;
    for (int i = 0, decrement = 0; i < nn; ++i)
    {
        while (decrement + line[i] < nums[i])
        {
            if (k == nq) return -1;
            const int l = queries[k][0],
                      r = queries[k][1],
                      val = queries[k][2];
            ++k;
            if (r < i) continue;
            line[std::max(l, i)] += val;
            line[r + 1] -= val;
        }
        decrement += line[i];
    }
    return k;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums,
          std::vector<std::vector<int> > queries,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minZeroArray(nums, queries);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 0, 2}, {{0, 2, 1}, {0, 2, 1}, {1, 1, 3}}, 2, trials);
    test({4, 3, 2, 1}, {{1, 3, 2}, {0, 2, 1}}, -1, trials);
    return 0;
}


