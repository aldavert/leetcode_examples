#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int removeStones(std::vector<std::vector<int> > stones)
{
    unsigned short number_of_blobs = 0;
    const unsigned short n = static_cast<unsigned short>(stones.size());
    std::vector<unsigned short> graph[1001];
    bool seen[1001] = {};
    auto search = [&](auto &&self, unsigned short u) -> void
    {
        for (unsigned short v : graph[u])
            if (!seen[v])
                seen[v] = true, self(self, v);
    };
    for (unsigned short i = 0; i < n; ++i)
        for (unsigned short j = i + 1; j < n; ++j)
            if ((stones[i][0] == stones[j][0])
            ||  (stones[i][1] == stones[j][1]))
                graph[i].push_back(j), graph[j].push_back(i);
    for (unsigned short i = 0; i < stones.size(); ++i)
        if (!seen[i])
            seen[i] = true, search(search, i), ++number_of_blobs;
    return static_cast<int>(n - number_of_blobs);
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > stones,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = removeStones(stones);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0}, {0, 1}, {1, 0}, {1, 2}, {2, 1}, {2, 2}}, 5, trials);
    test({{0, 0}, {0, 2}, {1, 1}, {2, 0}, {2, 2}}, 3, trials);
    test({{0, 0}}, 0, trials);
    return 0;
}


