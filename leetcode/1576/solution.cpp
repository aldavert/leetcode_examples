#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string modifyString(std::string s)
{
    const size_t n = s.size();
    for (size_t i = 0; i < n; ++i)
    {
        if (s[i] == '?')
        {
            size_t j = i + 1;
            while ((j < n) && (s[j] == '?')) ++j;
            char prev = (i > 0)?s[i - 1]:'\0';
            char next = (j < n)?s[j]:'\0';
            char curr = 'z';
            for (size_t k = i; k < j; ++k)
            {
                do curr = (curr < 'z')?curr + 1:'a';
                while ((curr == prev) || (curr == next));
                s[k] = curr;
            }
        }
    }
    return s;
}

// ############################################################################
// ############################################################################

void test(std::string s,
          [[maybe_unused]] std::string solution,
          unsigned int trials = 1)
{
    auto valid = [](std::string test) -> bool
    {
        if (test.find('?') != std::string::npos) return false;
        const size_t n = test.size() - 1;
        if      (n == 0) return true;
        else if (n == 1) return test[0] != test[1];
        for (size_t i = 1; i < n; ++i)
            if ((test[i - 1] == test[i])
            ||  (test[i + 1] == test[i]))
                return false;
        return true;
    };
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = modifyString(s);
    showResult(valid(result), solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("?zs", "azs", trials);
    test("ubv?w", "ubvaw", trials);
    test("ba?", "bab", trials);
    test("???", "abc", trials);
    return 0;
}


