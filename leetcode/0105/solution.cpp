#include "../common/common.hpp"
#include "../common/tree.hpp"
#include <unordered_map>

// ################################################################################################
// ################################################################################################

#if 1
TreeNode * buildTree(std::vector<int> preorder, std::vector<int> inorder)
{
    const int n = static_cast<int>(preorder.size());
    std::unordered_map<int, int> position;
    for (int i = 0; i < n; ++i)
        position[inorder[i]] = i;
    auto build = [&](auto &&self, int begin_pre, int begin_in, int length) -> TreeNode *
    {
        if (length > 0)
        {
            int idx_in = position.at(preorder[begin_pre]);
            TreeNode * current = new TreeNode(preorder[begin_pre]);
            const int left_length = idx_in - begin_in;
            current->left  = self(self, begin_pre + 1,
                                  begin_in, left_length);
            current->right = self(self, begin_pre + 1 + left_length,
                                  idx_in + 1, length - 1 - left_length);
            return current;
        }
        else return nullptr;
    };
    return build(build, 0, 0, n);
}
#else
TreeNode * buildTree(const std::vector<int> &preorder,
                     const std::vector<int> &inorder,
                     const std::unordered_map<int, int> &position,
                     int begin_pre,
                     int begin_in,
                     int length)
{
    if (length > 0)
    {
        int idx_in = position.at(preorder[begin_pre]);
        TreeNode * current = new TreeNode(preorder[begin_pre]);
        const int left_length = idx_in - begin_in;
        current->left = buildTree(preorder, inorder, position,
                                  begin_pre + 1, begin_in, left_length);
        current->right = buildTree(preorder, inorder, position,
                                   begin_pre + 1 + left_length, idx_in + 1, length - 1 - left_length);
        return current;
    }
    else return nullptr;
}

TreeNode * buildTree(std::vector<int> preorder, std::vector<int> inorder)
{
    const int n = static_cast<int>(preorder.size());
    std::unordered_map<int, int> position;
    for (int i = 0; i < n; ++i)
        position[inorder[i]] = i;
    return buildTree(preorder, inorder, position, 0, 0, n);
}
#endif

// ################################################################################################
// ################################################################################################

void test(std::vector<int> preorder,
          std::vector<int> inorder,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = buildTree(preorder, inorder);
        result = tree2vec(root);
        delete root;
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 9, 20, 15, 7}, {9, 3, 15, 20, 7}, {3, 9, 20, null, null, 15, 7}, trials);
    test({-1}, {-1}, {-1}, trials);
    return 0;
}


