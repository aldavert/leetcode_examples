#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumSubarrayLength(std::vector<int> nums, int k)
{
    const int n = static_cast<int>(nums.size());
    int count[32] = {}, result = n + 1;
    for (int i = 0, j = 0, s = 0; j < n; ++j)
    {
        s |= nums[j];
        for (int h = 0; h < 32; ++h)
            count[h] += (nums[j] >> h) & 1;
        for (; (s >= k) && (i <= j); ++i)
        {
            result = std::min(result, j - i + 1);
            for (int h = 0; h < 32; ++h)
                if (((nums[i] >> h) & 1) && (--count[h] == 0))
                    s ^= 1 << h;
        }
    }
    return (result > n)?-1:result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumSubarrayLength(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 2, 1, trials);
    test({2, 1, 8}, 10, 3, trials);
    test({1, 2}, 0, 1, trials);
    return 0;
}


