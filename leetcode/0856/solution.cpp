#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int scoreOfParentheses(std::string s)
{
    const int n = static_cast<int>(s.size());
    int score = 0;
    for (int i = 1, layer = 0; i < n; ++i)
    {
        score += ((s[i - 1] == '(') && (s[i] == ')')) * (1 << layer);
        layer += 1 - static_cast<int>(s[i - 1] - '(') * 2;
    }
    return score;
}

// ############################################################################
// ############################################################################

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = scoreOfParentheses(s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("()", 1, trials);
    test("(())", 2, trials);
    test("()()", 2, trials);
    return 0;
}


