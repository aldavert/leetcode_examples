#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int minDays(std::vector<int> bloomDay, int m, int k)
{
    long long r = *std::max_element(bloomDay.begin(), bloomDay.end()), l = 0;
    int result = -1;
    while (l <= r)
    {
        long long day = l + (r - l) / 2;
        int number_of_bouquets = 0, count = 0;
        for (size_t i = 0; i < bloomDay.size(); ++i)
        {
            count = (bloomDay[i] <= day) * (count + 1);
            if (count >= k)
            {
                ++number_of_bouquets;
                count = 0;
            }
        }
        if (number_of_bouquets >= m)
        {
            result = static_cast<int>(day);
            r = day - 1;
        }
        else if (number_of_bouquets < m)
            l = day + 1;
    }
    return result;
}
#else
int minDays(std::vector<int> bloomDay, int m, int k)
{
    auto getBouquetCount = [&](int waiting_days) -> int
    {
        int bouquet_count = 0;
        for (int required_flowers = k; int day : bloomDay)
        {
            if (day > waiting_days)
                required_flowers = k;
            else if (--required_flowers == 0)
            {
                ++bouquet_count;
                required_flowers = k;
            }
        }
        return bouquet_count;
    };
    if (bloomDay.size() < static_cast<size_t>(m) * k)
        return -1;
    int l = bloomDay[0], r = bloomDay[0];
    for (size_t i = 1; i < bloomDay.size(); ++i)
    {
        l = std::min(l, bloomDay[i]);
        r = std::max(r, bloomDay[i]);
    }
    while (l < r)
    {
        if (int mid = (l + r) / 2; getBouquetCount(mid) >= m)
            r = mid;
        else l = mid + 1;
    }
    return l;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> bloomDay, int m, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDays(bloomDay, m, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 10, 3, 10, 2}, 3, 1,  3, trials);
    test({1, 10, 3, 10, 2}, 3, 2, -1, trials);
    test({7, 7, 7, 7, 12, 7, 7}, 2, 3, 12, trials);
    return 0;
}


