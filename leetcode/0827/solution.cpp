#include "../common/common.hpp"
#include <fmt/core.h>
#include <fmt/color.h>
#include <random>
#include <list>

// ############################################################################
// ############################################################################

void connectedComponentsB(std::vector<std::vector<int> > grid,
                          std::vector<std::vector<int> > &colored_grid,
                          std::vector<int> &histogram)
{
    const int n = static_cast<int>(grid.size());
    std::list<int> region_size;
    
    for (int y = 0; y < n; ++y)
    {
        for (int x = 0; x < n; ++x)
        {
            if (grid[y][x])
            {
                unsigned int id = static_cast<unsigned int>(region_size.size());
                std::list<std::pair<int, int> > queue;
                queue.push_back({x, y});
                int rs = 0;
                while (!queue.empty())
                {
                    auto [cx, cy] = queue.front();
                    queue.pop_front();
                    if (grid[cy][cx])
                    {
                        grid[cy][cx] = 0;
                        colored_grid[cy + 1][cx + 1] = id;
                        ++rs;
                        if ((cx > 0    ) && grid[cy][cx - 1])
                            queue.push_back({cx - 1, cy});
                        if ((cy > 0    ) && grid[cy - 1][cx])
                            queue.push_back({cx, cy - 1});
                        if ((cx < n - 1) && grid[cy][cx + 1])
                            queue.push_back({cx + 1, cy});
                        if ((cy < n - 1) && grid[cy + 1][cx])
                            queue.push_back({cx, cy + 1});
                    }
                }
                region_size.push_back(rs);
            }
        }
    }
    int number_of_regions = static_cast<int>(region_size.size());
    histogram.resize(number_of_regions + 1, 0);
    if (number_of_regions == 0) return;
    for (int i = 0; i < number_of_regions; ++i)
    {
        histogram[i] = region_size.front();
        region_size.pop_front();
    }
    for (int y = 0; y <= n + 1; ++y)
        for (int x = 0; x <= n + 1; ++x)
            if (colored_grid[y][x] == -1)
                colored_grid[y][x] = number_of_regions;

#if 1
    // ADD -lfmt TO THE BUILD COMMAND TO AVOID LINKER ERROR.
    int nchrs = static_cast<int>(std::ceil(std::log10(number_of_regions)));
    std::vector<fmt::color> color_show(number_of_regions);
    fmt::color color_available[] = { fmt::color::light_blue, fmt::color::light_coral,
                                     fmt::color::light_cyan, fmt::color::light_green,
                                     fmt::color::light_salmon, fmt::color::light_yellow,
                                     fmt::color::light_sky_blue, fmt::color::orange,
                                     fmt::color::light_sea_green, fmt::color::red,
                                     fmt::color::indigo, fmt::color::golden_rod,
                                     fmt::color::gold, fmt::color::medium_blue,
                                     fmt::color::navy, fmt::color::powder_blue,
                                     fmt::color::plum, fmt::color::spring_green,
                                     fmt::color::yellow_green, fmt::color::sky_blue };
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distrib(0, 19);
    for (int i = 0; i < number_of_regions; ++i)
        color_show[i] = color_available[distrib(gen)];
    for (int y = 0; y < n + 2; ++y)
    {
        for (int x = 0; x < n + 2; ++x)
        {
            if (colored_grid[y][x] < number_of_regions)
                fmt::print(bg(color_show[colored_grid[y][x]])
                         | fg(fmt::color::black), "{:{}}", colored_grid[y][x], nchrs);
            else fmt::print(bg(fmt::color::dark_slate_gray), "{:{}}", ' ', nchrs);
        }
        fmt::print("\n");
    }
    for (size_t i = 0; i < histogram.size(); ++i)
        std::cout << histogram[i] << ' ';
    std::cout << '\n';
#endif
}

void connectedComponentsA(const std::vector<std::vector<int> > &grid,
                          std::vector<std::vector<int> > &colored_grid,
                          std::vector<int> &histogram)
{
    const int n = static_cast<int>(grid.size());
    std::vector<int> coloring_lut(grid.size() * grid.size());
    int number_of_regions = 0;
    
    // Special case: first row.
    if (grid[0][0])
    {
        coloring_lut[number_of_regions] = number_of_regions;
        colored_grid[1][1] = number_of_regions;
        ++number_of_regions;
    }
    for (int x = 1; x < n; ++x)
    {
        if (grid[0][x])
        {
            if (grid[0][x - 1])
                colored_grid[1][x + 1] = colored_grid[1][x];
            else
            {
                coloring_lut[number_of_regions] = number_of_regions;
                colored_grid[1][x + 1] = number_of_regions;
                ++number_of_regions;
            }
        }
    }
    
    for (int y = 1; y < n; ++y)
    {
        // Special case: first element of the row.
        if (grid[y][0])
        {
            if (grid[y - 1][0])
                colored_grid[y + 1][1] = colored_grid[y][1];
            else
            {
                coloring_lut[number_of_regions] = number_of_regions;
                colored_grid[y + 1][1] = number_of_regions;
                ++number_of_regions;
            }
        }
        // General case.
        for (int x = 1; x < n; ++x)
        {
            if (grid[y][x])
            {
                if ((grid[y][x - 1]) || (grid[y - 1][x]))
                {
                    if ((grid[y][x - 1]) && (grid[y - 1][x]))
                    {
                        if (colored_grid[y + 1][x] == colored_grid[y][x + 1])
                            colored_grid[y + 1][x + 1] = colored_grid[y + 1][x];
                        else
                        {
                            int left = colored_grid[y + 1][x];
                            while (coloring_lut[left] != left)
                                left = coloring_lut[left];
                            int up = colored_grid[y][x + 1];
                            while (coloring_lut[up] != up)
                                up = coloring_lut[up];
                            if (left < up)
                            {
                                colored_grid[y + 1][x + 1] = left;
                                coloring_lut[up] = left;
                            }
                            else
                            {
                                colored_grid[y + 1][x + 1] = up;
                                coloring_lut[left] = up;
                            }
                        }
                    }
                    else if (grid[y][x - 1])
                        colored_grid[y + 1][x + 1] = colored_grid[y + 1][x];
                    else colored_grid[y + 1][x + 1] = colored_grid[y][x + 1];
                }
                else
                {
                    coloring_lut[number_of_regions] = number_of_regions;
                    colored_grid[y + 1][x + 1] = number_of_regions;
                    ++number_of_regions;
                }
            }
        }
    }
    if (number_of_regions == 0)
    {
        histogram.resize(1, 0);
        return;
    }
    std::vector<int> compact_lut(number_of_regions);
    int actual_number_of_regions = 0;
    for (int i = 0; i < number_of_regions; ++i)
    {
        if (coloring_lut[i] != i)
        {
            int aux = i;
            while (coloring_lut[aux] != aux) aux = coloring_lut[aux];
            coloring_lut[i] = aux;
        }
        else compact_lut[i] = actual_number_of_regions++;
    }
    for (int i = 0; i < number_of_regions; ++i)
        if (coloring_lut[i] != i)
            compact_lut[i] = compact_lut[coloring_lut[i]];
    histogram.resize(actual_number_of_regions + 1, 0);
    if (actual_number_of_regions == 0) return;
    for (int y = 0; y <= n + 1; ++y)
    {
        for (int x = 0; x <= n + 1; ++x)
        {
            if (colored_grid[y][x] >= 0)
            {
                int id = compact_lut[colored_grid[y][x]];
                colored_grid[y][x] = id;
                ++histogram[id];
            }
            else colored_grid[y][x] = actual_number_of_regions;
        }
    }
#if 1
    // ADD -lfmt TO THE BUILD COMMAND TO AVOID LINKER ERROR.
    int nchrs = static_cast<int>(std::ceil(std::log10(number_of_regions)));
    std::vector<fmt::color> color_show(number_of_regions);
    fmt::color color_available[] = { fmt::color::light_blue, fmt::color::light_coral,
                                     fmt::color::light_cyan, fmt::color::light_green,
                                     fmt::color::light_salmon, fmt::color::light_yellow,
                                     fmt::color::light_sky_blue, fmt::color::orange,
                                     fmt::color::light_sea_green, fmt::color::red,
                                     fmt::color::indigo, fmt::color::golden_rod,
                                     fmt::color::gold, fmt::color::medium_blue,
                                     fmt::color::navy, fmt::color::powder_blue,
                                     fmt::color::plum, fmt::color::spring_green,
                                     fmt::color::yellow_green, fmt::color::sky_blue };
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distrib(0, 19);
    for (int i = 0; i < number_of_regions; ++i)
        color_show[i] = color_available[distrib(gen)];
    for (int y = 0; y < n + 2; ++y)
    {
        for (int x = 0; x < n + 2; ++x)
        {
            if (colored_grid[y][x] < actual_number_of_regions)
                fmt::print(bg(color_show[colored_grid[y][x]])
                         | fg(fmt::color::black), "{:{}}", colored_grid[y][x], nchrs);
            else fmt::print(bg(fmt::color::dark_slate_gray), "{:{}}", ' ', nchrs);
        }
        fmt::print("\n");
    }
    for (size_t i = 0; i < histogram.size(); ++i)
        std::cout << histogram[i] << ' ';
    std::cout << '\n';
#endif
}

int largestIsland(std::vector<std::vector<int> > grid)
{
    const int n = static_cast<int>(grid.size());
    if      (n == 0) return 0;
    else if (n == 1) return 1;
    std::vector<int> histogram;
    std::vector<std::vector<int> > colored_grid(n + 2);
    for (int i = 0; i < n + 2; ++i) colored_grid[i].resize(n + 2, -1);
    //connectedComponentsA(grid, colored_grid, histogram);
    connectedComponentsB(grid, colored_grid, histogram);
    if      (histogram.size() == 1) return 1;
    else if (histogram.size() == 2) return std::min(n * n, histogram[0] + 1);
    int number_of_regions = static_cast<int>(histogram.size()) - 1;
    std::vector<bool> selected(number_of_regions + 1);
    for (int i = 0; i <= number_of_regions; ++i) selected[i] = false;
    
    int largest_area = 1;
    for (int y = 1; y <= n; ++y)
    {
        for (int x = 1; x <= n; ++x)
        {
            if (colored_grid[y][x] == number_of_regions)
            {
                int area = 1;
                int positionA = colored_grid[y - 1][x];
                int positionB = colored_grid[y][x - 1];
                int positionC = colored_grid[y + 1][x];
                int positionD = colored_grid[y][x + 1];
                if (!selected[positionA])
                    area += histogram[positionA];
                selected[positionA] = true;
                if (!selected[positionB])
                    area += histogram[positionB];
                selected[positionB] = true;
                if (!selected[positionC])
                    area += histogram[positionC];
                selected[positionC] = true;
                if (!selected[positionD])
                    area += histogram[positionD];
                if (area > largest_area) largest_area = area;
                selected[positionA] = false;
                selected[positionB] = false;
                selected[positionC] = false;
            }
        }
    }
    return largest_area;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int>> grid, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = largestIsland(grid);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0},
          {0, 0}}, 1, trials);
    test({{1, 0},
          {0, 0}}, 2, trials);
    test({{1, 0},
          {0, 1}}, 3, trials);
    test({{1, 1},
          {0, 1}}, 4, trials);
    test({{1, 1},
          {1, 1}}, 4, trials);
    test({{1, 1, 1, 0, 1, 1, 1},
          {1, 1, 1, 0, 1, 1, 1},
          {1, 1, 1, 0, 1, 1, 1},
          {0, 0, 0, 1, 0, 0, 0},
          {1, 1, 1, 0, 1, 1, 1},
          {1, 1, 1, 0, 1, 1, 1},
          {1, 1, 1, 0, 1, 1, 1}}, 20, trials);
    test({{1, 1, 1, 0, 1, 1, 1},
          {1, 0, 1, 0, 0, 0, 1},
          {1, 0, 1, 0, 1, 1, 1},
          {0, 0, 1, 1, 0, 0, 0},
          {1, 1, 0, 0, 1, 1, 1},
          {1, 0, 1, 1, 1, 0, 1},
          {1, 1, 1, 0, 1, 0, 1}}, 32, trials);
    return 0;
}


