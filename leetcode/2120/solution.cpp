#include "../common/common.hpp"
#include <unordered_map>

// ############################################################################
// ############################################################################

std::vector<int> executeInstructions(int n, std::vector<int> startPos, std::string s)
{
    const int m = static_cast<int>(s.size());
    const int u_most = startPos[0] + 1, d_most = n - startPos[0],
              l_most = startPos[1] + 1, r_most = n - startPos[1];
    const std::unordered_map<char, std::pair<int, int> > moves{
            {'L', { 0, -1}},
            {'R', { 0,  1}},
            {'U', {-1,  0}},
            {'D', { 1,  0}}};
    std::unordered_map<int, int> reach_x{{0, m}}, reach_y{{0, m}};
    std::vector<int> result(m);
    
    for (int i = m - 1, x = 0, y = 0; i >= 0; --i)
    {
        auto [dx, dy] = moves.at(s[i]);
        x -= dx;
        y -= dy;
        reach_x[x] = i;
        reach_y[y] = i;
        int out = std::numeric_limits<int>::max();
        if (const auto it = reach_x.find(x - u_most); it != reach_x.cend())
            out = std::min(out, it->second);
        if (const auto it = reach_x.find(x + d_most); it != reach_x.cend())
            out = std::min(out, it->second);
        if (const auto it = reach_y.find(y - l_most); it != reach_y.cend())
            out = std::min(out, it->second);
        if (const auto it = reach_y.find(y + r_most); it != reach_y.cend())
            out = std::min(out, it->second);
        result[i] = (out == std::numeric_limits<int>::max())?m - i:out - i - 1;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(int n,
          std::vector<int> startPos,
          std::string s,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = executeInstructions(n, startPos, s);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {0, 1}, "RRDDLU", {1, 5, 4, 3, 1, 0}, trials);
    test(2, {1, 1}, "LURD", {4, 1, 0, 0}, trials);
    test(1, {0, 0}, "LRUD", {0, 0, 0, 0}, trials);
    return 0;
}


