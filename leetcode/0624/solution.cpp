#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxDistance(std::vector<std::vector<int> > arrays)
{
    int result = 0;
    for (int min = 10000, max = -10000; const auto &array : arrays)
    {
        result = std::max({result, array.back() - min, max - array.front()});
        min = std::min(min, array.front());
        max = std::max(max, array.back());
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > arrays, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxDistance(arrays);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3}, {4, 5}, {1, 2, 3}}, 4, trials);
    test({{1}, {1}}, 0, trials);
    return 0;
}


