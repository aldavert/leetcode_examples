#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool isAnagram(std::string s, std::string t)
{
    int histogram[26] = {};
    for (char c : s)
        ++histogram[c - 'a'];
    for (char c : t)
        --histogram[c - 'a'];
    for (int i = 0; i < 26; ++i)
        if (histogram[i] != 0) return false;
    return true;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string t, bool solution, unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = isAnagram(s, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("anagram", "nagaram", true, trials);
    test("rat", "car", false, trials);
    return 0;
}


