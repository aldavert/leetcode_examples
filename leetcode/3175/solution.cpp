#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int findWinningPlayer(std::vector<int> skills, int k)
{
    int result = 0;
    for (int i = 1, w = 0, n = static_cast<int>(skills.size()); (i < n) && (w < k); ++i)
    {
        if (skills[i] > skills[result])
        {
            result = i;
            w = 1;
        }
        else ++w;
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> skills, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = findWinningPlayer(skills, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 6, 3, 9}, 2, 2, trials);
    test({2, 5, 4}, 3, 1, trials);
    return 0;
}


