#include "../common/common.hpp"

// ############################################################################
// ############################################################################

std::string replaceWords(std::vector<std::string> dictionary, std::string sentence)
{
    struct Trie
    {
        Trie(void) = default;
        ~Trie(void) { for (size_t i = 0; i < 26; ++i) delete next[i]; }
        Trie * next[26] = {};
        bool word = false;
    };
    Trie root;
    auto insert = [&root](const std::string &word) -> void
    {
        Trie * node = &root;
        for (char letter : word)
        {
            int i = static_cast<int>(letter - 'a');
            if (!node->next[i]) node->next[i] = new Trie();
            node = node->next[i];
        }
        node->word = true;
    };
    auto search = [&root](const std::string &word) -> size_t
    {
        Trie * node = &root;
        for (size_t counter = 0; char letter : word)
        {
            int i = static_cast<int>(letter - 'a');
            if (node->word) return counter;
            if (!node->next[i]) return word.size();
            node = node->next[i];
            ++counter;
        }
        return word.size();
    };
    for (const std::string &codeword : dictionary)
        insert(codeword);
    
    std::string word, result = "";
    for (char symbol : sentence)
    {
        if (symbol == ' ')
        {
            if (!result.empty()) result += " ";
            result += word.substr(0, search(word));
            word = "";
        }
        else word += symbol;
    }
    if (!result.empty()) result += " ";
    result += word.substr(0, search(word));
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> dictionary,
          std::string sentence,
          std::string solution,
          unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = replaceWords(dictionary, sentence);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"cat", "bat", "rat"}, "the cattle was rattled by the battery",
         "the cat was rat by the bat", trials);
    test({"a", "b", "c"}, "aadsfasf absbs bbab cadsfafs", "a a b c", trials);
    return 0;
}


