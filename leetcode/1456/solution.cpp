#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int maxVowels(std::string s, int k)
{
    const int n = static_cast<int>(s.size());
    int number_of_vowels = 0;
    for (int i = 0; i < k; ++i)
        number_of_vowels += (s[i] == 'a') || (s[i] == 'e') || (s[i] == 'i')
                         || (s[i] == 'o') || (s[i] == 'u');
    int result = number_of_vowels;
    for (int i = k, j = 0; i < n; ++i, ++j)
    {
        number_of_vowels -= (s[j] == 'a') || (s[j] == 'e') || (s[j] == 'i')
                         || (s[j] == 'o') || (s[j] == 'u');
        number_of_vowels += (s[i] == 'a') || (s[i] == 'e') || (s[i] == 'i')
                         || (s[i] == 'o') || (s[i] == 'u');
        result = std::max(result, number_of_vowels);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, int k, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxVowels(s, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abciiidef", 3, 3, trials);
    test("aeiou", 2, 2, trials);
    test("leetcode", 3, 2, trials);
    return 0;
}


