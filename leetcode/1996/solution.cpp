#include "../common/common.hpp"
#include <queue>

// ############################################################################
// ############################################################################

int numberOfWeakCharacters(std::vector<std::vector<int> > properties)
{
    struct Info
    {
        Info(void) = default;
        Info(const std::vector<int> &prop) : attack(prop[0]), defense(prop[1]) {}
        Info(int a, int d) : attack(a), defense(d) {}
        int attack = 0;
        int defense = 0;
        bool operator<(const Info &other) const
        {
            return (attack < other.attack)
                || ((attack == other.attack) && (defense < other.defense));
        }
        bool operator>(const Info &other) const
        {
            return (defense < other.defense)
                || ((defense == other.defense) && (attack < other.attack));
        }
    };
    std::vector<Info> sorted_properties(properties.begin(), properties.end());
    std::sort(sorted_properties.begin(), sorted_properties.end());
    const int n = static_cast<int>(properties.size());
    std::priority_queue<Info, std::vector<Info>, std::greater<Info> > queue;
    for (int i = n - 1; i > 0; --i)
        queue.push(sorted_properties[i]);
    int weak = 0;
    for (int i = 0; i < n - 1; ++i)
    {
        while (!queue.empty() && (queue.top().attack <= sorted_properties[i].attack))
            queue.pop();
        if (queue.empty()) break;
        if (sorted_properties[i].defense < queue.top().defense)
            ++weak;
    }
    return weak;
}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > properties,
          int solution,
          unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfWeakCharacters(properties);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{5, 5}, {6, 3}, {3, 6}}, 0, trials);
    test({{2, 2}, {3, 3}}, 1, trials);
    test({{1, 5}, {10, 4}, {4, 3}}, 1, trials);
    test({{1, 1}, {2, 1}, {2, 2}, {1, 2}}, 1, trials);
    return 0;
}


