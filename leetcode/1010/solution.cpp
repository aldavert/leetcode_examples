#include "../common/common.hpp"

// ############################################################################
// ############################################################################

#if 1
int numPairsDivisibleBy60(std::vector<int> time)
{
    int lut[60] = {};
    for (int t : time) ++lut[t % 60];
    int result = lut[ 0] * (lut[ 0] - 1) / 2
               + lut[30] * (lut[30] - 1) / 2;
    for (int t = 1; t < 30; ++t)
        result += lut[60 - t] * lut[t];
    return result;
}
#elif 0
int numPairsDivisibleBy60(std::vector<int> time)
{
    int lut[501] = {};
    for (int t : time) ++lut[t];
    int result = 0;
    for (int t = 1; t <= 500; ++t)
    {
        if (lut[t] > 0)
        {
            int begin = 60 * (t / 60) - t % 60;
            while (begin < t) begin += 60;
            if (begin == t)
            {
                result += lut[begin] * (lut[begin] - 1) / 2;
                begin += 60;
            }
            for (int other = begin; other <= 500; other += 60)
                result += lut[other] * lut[t];
        }
    }
    return result;
}
#elif 0
int numPairsDivisibleBy60(std::vector<int> time)
{
    int lut[501] = {};
    for (int t : time) ++lut[t];
    int result = 0;
    for (int i = 0; i < 501; ++i)
    {
        if (lut[i] > 0)
        {
            if ((i + i) % 60 == 0)
                result += lut[i] * (lut[i] - 1) / 2;
            for (int j = i + 1; j < 501; ++j)
            {
                if ((lut[j] > 0) && ((i + j) % 60 == 0))
                    result += lut[i] * lut[j];
            }
        }
    }
    return result;
}
#else
int numPairsDivisibleBy60(std::vector<int> time)
{
    const int n = static_cast<int>(time.size());
    int result = 0;
    for (int i = 0; i < n; ++i)
        for (int j = i + 1; j < n; ++j)
            if ((time[i] + time[j]) % 60 == 0)
                ++result;
    return result;
}
#endif

// ############################################################################
// ############################################################################

void test(std::vector<int> time, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = numPairsDivisibleBy60(time);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({30, 20, 150, 100, 40}, 3, trials);
    test({60, 60, 60}, 3, trials);
    test({269, 230, 318, 468, 171, 158, 350,  60, 287,  27,  11, 384, 332, 267,
          412, 478, 280, 303, 242, 378, 129, 131, 164, 467, 345, 146, 264, 332,
          276, 479, 284, 433, 117, 197, 430, 203, 100, 280, 145, 287,  91, 157,
            5, 475, 288, 146, 370, 199,  81, 428, 278,   2, 400,  23, 470, 242,
          411, 470, 330, 144, 189, 204,  62, 318, 475,  24, 457,  83, 204, 322,
          250, 478, 186, 467, 350, 171, 119, 245, 399, 112, 252, 201, 324, 317,
          293,  44, 295,  14, 379, 382, 137, 280, 265,  78,  38, 323, 347, 499,
          238, 110,  18, 224, 473, 289, 198, 106, 256, 279, 275, 349, 210, 498,
          201, 175, 472, 461, 116, 144,   9, 221, 473}, 105, trials);
    return 0;
}


