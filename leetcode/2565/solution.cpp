#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int minimumScore(std::string s, std::string t)
{
    const int n_s = static_cast<int>(s.size()),
              n_t = static_cast<int>(t.size());
    std::vector<int> left_most(n_t, -1), right_most(n_t, - 1);
    int j = 0;
    for (int i = 0; (i < n_s) && (j < n_t); ++i)
        if (s[i] == t[j])
            left_most[j++] = i;
    j = n_t - 1;
    for (int i = n_s - 1; (i >= 0) && (j >= 0); --i)
        if (s[i] == t[j]) right_most[j--] = i;
    int result = j + 1;
    j = 0;
    for (int i = 0; i < n_t; ++i)
    {
        if (left_most[i] == -1) break;
        while ((j < n_t) && (left_most[i] >= right_most[j]))
            ++j;
        if (i == j) return 0;
        result = std::min(result, j - i - 1);
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::string s, std::string t, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = minimumScore(s, t);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abacaba", "bzaa", 1, trials);
    test("cde", "xyz", 3, trials);
    return 0;
}


