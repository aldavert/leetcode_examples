#include "../common/common.hpp"

// ############################################################################
// ############################################################################

void moveZeroes(std::vector<int> &nums)
{
    size_t i = 0;
    for (int number : nums)
        if (number != 0)
            nums[i++] = number;
    for (; i < nums.size(); ++i)
        nums[i] = 0;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = nums;
        moveZeroes(result);
    }
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 0, 3, 12}, {1, 3, 12, 0, 0}, trials);
    test({0}, {0}, trials);
    return 0;
}


