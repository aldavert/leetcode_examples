#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int longestArithSeqLength(std::vector<int> nums)
{
    const int n = static_cast<int>(nums.size());
    int dp[1000][1001] = {};
    int result = 0;
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < i; ++j)
        {
            int difference = nums[i] - nums[j] + 500;
            dp[i][difference] = std::max(2, dp[j][difference] + 1);
            result = std::max(result, dp[i][difference]);
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestArithSeqLength(nums);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 6, 9, 12}, 4, trials);
    test({9, 4, 7, 2, 10}, 3, trials);
    test({20, 1, 15, 3, 10, 5, 8}, 4, trials);
    return 0;
}


