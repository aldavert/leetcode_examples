#include "../common/common.hpp"
#include <deque>

// ############################################################################
// ############################################################################

std::vector<int> pathInZigZagTree(int label)
{
    auto boundarySum = [](int level) -> int
    {
        return (1 << level) + (1 << (level + 1)) - 1;
    };
    std::deque<int> result;
    int level = 8 * sizeof(unsigned int)
              - std::countl_zero(std::bit_cast<unsigned int>(label)) - 1;
    if (level & 1)
        label = boundarySum(level) - label;
    for (int l = level; l >= 0; --l, label /= 2)
        result.push_front((l & 1)?boundarySum(l) - label:label);
    return {result.begin(), result.end()};
}

// ############################################################################
// ############################################################################

void test(int label, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = pathInZigZagTree(label);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(14, {1, 3, 4, 14}, trials);
    test(26, {1, 2, 6, 10, 26}, trials);
    return 0;
}


