#include "../common/common.hpp"

// ############################################################################
// ############################################################################

bool kLengthApart(std::vector<int> nums, int k)
{
    for (int separation = k; int n : nums)
    {
        if ((n == 1) && (separation < k))
            return false;
        separation = (n != 1) * (separation + 1);
    }
    return true;
}

// ############################################################################
// ############################################################################

void test(const std::vector<int> nums,
          int k,
          bool solution,
          unsigned int trials = 1)
{
    bool result = !solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = kLengthApart(nums, k);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 0, 0, 1, 0, 0, 1}, 2, true, trials);
    test({1, 0, 0, 1, 0, 1}, 2, false, trials);
    test({1, 1, 1, 0}, 3, false, trials);
    test({0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1}, 5, false, trials);
    return 0;
}


