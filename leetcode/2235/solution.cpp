#include "../common/common.hpp"

// ############################################################################
// ############################################################################

int sum(int num1, int num2)
{
    return num1 + num2;
}

// ############################################################################
// ############################################################################

void test(int num1, int num2, int solution, unsigned int trials = 1)
{
    int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = sum(num1, num2);
    showResult(solution == result, solution, result);
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(12, 5, 17, trials);
    test(-10, 4, -6, trials);
    return 0;
}


