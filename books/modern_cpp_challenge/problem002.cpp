#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <numeric>

/*****************************************************************************/
/* PROBLEM: Greatest common divisor                                          */
/* ------------------------------------------------------------------------- */
/* Write a program, that given two positive integers, will calculate and     */
/* print the greatest common divisor of the two.                             */
/*****************************************************************************/

// ############################################################################
// ############################################################################

unsigned int gcd_euclid(unsigned int a, unsigned int b)
{
    while (a != b)
    {
        if (a > b) a -= b;
        else b -= a;
    }
    return a;
}

unsigned int gcd_euclidean(unsigned int a, unsigned int b)
{
    while (b != 0)
        a = std::exchange(b, a % b);
    return a;
}

unsigned int gcd_binary(unsigned int a, unsigned int b)
{
    if (a == 0) return b;
    if (b == 0) return a;
    int i = std::countr_zero(a);
    int j = std::countr_zero(b);
    a >>= i;
    b >>= j;
    int k = std::min(i, j);
    do
    {
        b >>= std::countr_zero(b);
        if (a > b) std::swap(a, b);
        b -= a;
    } while (b != 0);
    return a << k;
}

unsigned int problem(unsigned int a, unsigned int b)
{
    //return gcd_euclid(a, b);            // 3034964 ms
    //return gcd_euclidean(a, b);         //    3047 ms
    return gcd_binary(a, b);            //    6899 ms
    //return std::gcd(a, b);              //    4331 ms
}

// ############################################################################
// ############################################################################

long solution(unsigned int a, unsigned int b)
{
    while (b != 0)
    {
        unsigned int r = a % b;
        a = b;
        b = r;
    }
    return a;
}

// ############################################################################
// ############################################################################

bool test(unsigned int a,
          unsigned int b,
          unsigned int solution,
          unsigned int trials = 1)
{
    bool correct = true;
    long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = problem(a, b);
    if (solution != result)
    {
        correct = false;
        std::cout << "[FAILURE] GCD(" << a << ", " << b << ") expected ";
        std::cout << solution << " and obtained " << result << ".\n";
    }
    return correct;
}

int main(int argc, char * * argv)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    bool correct = true;
    for (unsigned int a = 1; a < 1'000'000'000; a += 57493)
        for (unsigned int b = 1; b < 1'000'000'000; b += 571019)
            correct = test(a, b, solution(a, b), trials) && correct;
    if (correct) std::cout << "All tests been passed correctly\n";
    return 0;
}
