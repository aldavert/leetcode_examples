#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>

/*****************************************************************************/
/* PROBLEM: IPv4 data type                                                   */
/* ------------------------------------------------------------------------- */
/* Write a class that represents an IPv4 address. Implement the functions    */
/* required to be able to read and write such addresses from or to the       */
/* console. The user should be able to input values in dotted form, such as  */
/* 127.0.0.1 or 168.192.0.100. This is also the form in which IPv4 addresses */
/* should be formatted to an output stream.                                  */
/*****************************************************************************/

// ############################################################################
// ############################################################################

class IPv4Information
{
public:
    template <std::integral T>
    IPv4Information(T a, T b, T c, T d)
    {
        m_address[0] = static_cast<unsigned char>(a);
        m_address[1] = static_cast<unsigned char>(b);
        m_address[2] = static_cast<unsigned char>(c);
        m_address[3] = static_cast<unsigned char>(d);
    }
    template <std::integral T>
    IPv4Information(std::array<T, 4> address) :
        m_address(address) {}
    template <std::integral T>
    IPv4Information& operator=(std::array<T, 4> address)
    {
        m_address = address;
        return *this;
    }
    template <typename T = unsigned int>
    T A(void) const { return static_cast<T>(m_address[0]); }
    template <typename T = unsigned int>
    T B(void) const { return static_cast<T>(m_address[1]); }
    template <typename T = unsigned int>
    T C(void) const { return static_cast<T>(m_address[2]); }
    template <typename T = unsigned int>
    T D(void) const { return static_cast<T>(m_address[3]); }
protected:
    std::array<unsigned char, 4> m_address = {0, 0, 0, 0};
};

std::ostream& operator<<(std::ostream &out, const IPv4Information &ipv4)
{
    out << ipv4.A() << '.' << ipv4.B() << '.' << ipv4.C() << '.' << ipv4.D();
    return out;
}

std::istream& operator>>(std::istream &in, IPv4Information &ipv4)
{
    int a, b, c, d;
    char dot_ab, dot_bc, dot_cd;
    in >> a >> dot_ab >> b >> dot_bc >> c >> dot_cd >> d;
    if ((dot_ab == '.') && (dot_bc == '.') && (dot_cd == '.'))
        ipv4 = {a, b, c, d};
    else in.setstate(std::ios_base::failbit);
    return in;
}

std::string problem(const std::vector<std::vector<int> > &input)
{
    IPv4Information address(127, 0, 0, 1);
    std::string user_input_data;
    for (const auto &ip : input)
    {
        user_input_data += std::to_string(ip[0]);
        for (size_t i = 1; i < ip.size(); ++i)
            user_input_data += "." + std::to_string(ip[i]);
        user_input_data += '\n';
    }
    std::istringstream input_stream(user_input_data);
    std::ostringstream output;
    for (size_t i = 0; i < input.size(); ++i)
    {
        input_stream >> address;
        output << "Address: " << address << '\n';
    }
    return output.str();
}

// ############################################################################
// ############################################################################

class ipv4
{
    std::array<unsigned char, 4> data;
public:
    constexpr ipv4() : data {{0}} {}
    constexpr ipv4(unsigned char a, unsigned char b, unsigned char c, unsigned char d):
        data({a, b, c, d}) {}
    explicit constexpr ipv4(unsigned long a) :
        data{{static_cast<unsigned char>((a >> 24) & 0xFF),
              static_cast<unsigned char>((a >> 16) & 0xFF),
              static_cast<unsigned char>((a >>  8) & 0xFF),
              static_cast<unsigned char>( a        & 0xFF)}} {}
    ipv4(const ipv4 &other) noexcept : data(other.data) {}
    ipv4& operator=(const ipv4 &other) noexcept
    {
        data = other.data;
        return *this;
    }
    std::string to_string(void) const
    {
        std::stringstream sstr;
        sstr << *this;
        return sstr.str();
    }
    constexpr unsigned long to_ulong() const noexcept
    {
        return (static_cast<unsigned long>(data[0]) << 24)
             | (static_cast<unsigned long>(data[1]) << 16)
             | (static_cast<unsigned long>(data[2]) <<  8)
             | (static_cast<unsigned long>(data[3])      );
    }
    friend std::ostream& operator<<(std::ostream &os, const ipv4 &a)
    {
        os << static_cast<int>(a.data[0]) << '.'
           << static_cast<int>(a.data[1]) << '.'
           << static_cast<int>(a.data[2]) << '.'
           << static_cast<int>(a.data[3]);
        return os;
    }
    friend std::istream& operator>>(std::istream &is, ipv4 &a)
    {
        char d1, d2, d3;
        int b1, b2, b3, b4;
        is >> b1 >> d1 >> b2 >> d2 >> b3 >> d3 >> b4;
        if (d1 == '.' && d2 == '.' && d3 == '.')
            a = ipv4(b1, b2, b3, b4);
        else is.setstate(std::ios_base::failbit);
        return is;
    }
};

std::string solution(const std::vector<std::vector<int> > &input)
{
    ipv4 address(127, 0, 0, 1);
    std::string user_input_data;
    for (const auto &ip : input)
    {
        user_input_data += std::to_string(ip[0]);
        for (size_t i = 1; i < ip.size(); ++i)
            user_input_data += "." + std::to_string(ip[i]);
        user_input_data += '\n';
    }
    std::istringstream input_stream(user_input_data);
    std::ostringstream output;
    for (size_t i = 0; i < input.size(); ++i)
    {
        input_stream >> address;
        output << "Address: " << address << '\n';
    }
    return output.str();

}

// ############################################################################
// ############################################################################

void test(std::vector<std::vector<int> > input,
          std::string expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::string result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution(input);
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem(input);
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] The IP operations generate a different output:";
        std::cout << '\n' << result << "\nis different than the Book's expected";
        std::cout << " output:\n" << expected << '\n';
    }
}

int main(int argc, char * * argv)
{
    if ((argc > 1) && (std::strcmp(argv[1], "time") == 0))
    {
#if 1
        //constexpr unsigned int trials = 10'000'000;
        constexpr unsigned int trials = 10'000;
#else
        constexpr unsigned int trials = 1;
#endif
        std::locale::global(std::locale("de_CH.utf8"));
        fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
        std::vector<std::vector<int> > addresses;
        addresses.push_back({127, 0, 0, 5});
        addresses.push_back({10, 1, 1, 5});
        addresses.push_back({158, 109, 4, 108});
        addresses.push_back({192, 168, 1, 1});
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<> dist(0, 254);
        for (unsigned int i = 0; i < 200; ++i)
            addresses.push_back({dist(gen), dist(gen), dist(gen), dist(gen)});
        std::string expected = solution(addresses);
        
        fmt::print("[TEST] User's solution runtime...\n");
        auto time_problem_begin = std::chrono::high_resolution_clock::now();
        test(addresses, expected, false, trials);
        auto time_problem_end = std::chrono::high_resolution_clock::now();
        auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
        fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
        
        fmt::print("[TEST] Book's solution runtime...\n");
        auto time_solution_begin = std::chrono::high_resolution_clock::now();
        test(addresses, expected, true, trials);
        auto time_solution_end = std::chrono::high_resolution_clock::now();
        auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
        fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    }
    else
    {
        IPv4Information ipv4 = {127, 0, 1, 1};
        std::cout << "Constructor address: " << ipv4 << '\n';
        std::cout << "Input IPv4 address: ";
        std::cin >> ipv4;
        if (!std::cin.fail())
            std::cout << "User input address: " << ipv4 << '\n';
        else std::cout << "IP address wrongly formatted.\n";
    }
    return 0;
}
