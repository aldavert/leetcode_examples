#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>

/*****************************************************************************/
/* PROBLEM: Prime factors of a number                                        */
/* ------------------------------------------------------------------------- */
/* Write a program that prints the prime factors of a number entered by the  */
/* user.                                                                     */
/*****************************************************************************/

// ############################################################################
// ############################################################################

std::vector<unsigned long> problem(unsigned long number)
{
    std::vector<unsigned long> result;
    if (number > 1)
    {
        // Check number divisible by 2.
        while ((number & 1) == 0)
        {
            result.push_back(2);
            number >>= 1;
        }
        // Check number divisible by 3.
        while (number % 3 == 0)
        {
            result.push_back(3);
            number /= 3;
        }
        // Check the remaining possible prime numbers.
        for (unsigned long factor = 5, jump = 0;
             factor * factor <= number;
             factor += 2 * (jump + 1), jump = !jump)
        {
            while (number % factor == 0)
            {
                result.push_back(factor);
                number /= factor;
            }
        }
        if (number > 1)
            result.push_back(number);
    }
    else result.push_back(number == 1);
    return result;
}

// ############################################################################
// ############################################################################

std::vector<unsigned long> solution(unsigned long number)
{
    std::vector<unsigned long> result;
    while (number % 2 == 0)
    {
        result.push_back(2);
        number = number / 2;
    }
    for (unsigned long i = 3; i <= std::sqrt(number); i += 2)
    {
        while (number % i == 0)
        {
            result.push_back(i);
            number = number / i;
        }
    }
    if (number > 2)
        result.push_back(number);
    return result;
}

// ############################################################################
// ############################################################################

std::ostream& operator<<(std::ostream &out,
                         const std::vector<unsigned long> &vec)
{
    out << '{';
    for (bool test = false; const auto &v : vec)
    {
        if (test) [[likely]] out << ", ";
        test = true;
        out << v;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<unsigned long> &left,
                const std::vector<unsigned long> &right)
{
    if (left.size() != right.size()) return false;
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

void test(unsigned long number,
          const std::vector<unsigned long> &expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::vector<unsigned long> result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution(number);
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem(number);
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] The prime factors of '" << number << "' are expected";
        std::cout << " to be:\n" << expected << "\n However, the results obtained are:\n";
        std::cout << result << '\n';
    }
}

int main(int argc, char * * argv)
{
    if ((argc > 1) && (std::strcmp(argv[1], "time") == 0))
    {
#if 1
        //constexpr unsigned int trials = 10'000'000;
        constexpr unsigned int trials = 100;
#else
        constexpr unsigned int trials = 1;
#endif
        std::locale::global(std::locale("de_CH.utf8"));
        auto time_problem_begin = std::chrono::high_resolution_clock::now();
        fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
        fmt::print("[TEST] Generating 1'000 random test numbers ranging between 1'000 and 1'000'000'000'000.\n");
                
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<unsigned long> dist_numbers(1'000, 1'000'000'000'000);
        std::vector<unsigned long> test_numbers(1000);
        for (size_t i = 0; i < 1000; ++i)
            test_numbers[i] = dist_numbers(gen);
        
        fmt::print("[TEST] User's solution runtime...\n");
        for (unsigned long number : test_numbers)
            test(number, solution(number), false, trials);
        auto time_problem_end = std::chrono::high_resolution_clock::now();
        auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
        fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
        
        auto time_solution_begin = std::chrono::high_resolution_clock::now();
        fmt::print("[TEST] Book's solution runtime...\n");
        for (unsigned long number : test_numbers)
            test(number, solution(number), true, trials);
        auto time_solution_end = std::chrono::high_resolution_clock::now();
        auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
        fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    }
    else
    {
        unsigned long number = 0;
        std::cout << "Input number: ";
        std::cin >> number;
        auto result = problem(number);
        std::cout << "The prime factors of '" << number << "' are:\n" << result;
        if (auto sol = solution(number); sol == result)
            std::cout << "\n[CORRECT]\n";
        else std::cout << "\n[FAILURE] The expected solution is:\n" << sol << '\n';
    }
    return 0;
}
