#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>

/*****************************************************************************/
/* PROBLEM: Average rating of movies                                         */
/* ------------------------------------------------------------------------- */
/* Write a program that calculates and prints the average rating of a list   */
/* of movies. Each movie has a list of ratings from 1 to 10 (where 1 is the  */
/* the lowest and 10 is the highest rating). In order to compute the rating, */
/* you must remove 5% of the highest and lowest ratings before computing     */
/* their average. The result must be displayed with a single decimal point.  */
/*****************************************************************************/

// ############################################################################
// ############################################################################

struct movie
{
    int id;
    std::string title;
    std::vector<int> ratings;
};

template <typename RANGE>
requires std::ranges::range<RANGE>
std::vector<float> solution(const RANGE &movies)
{
    std::vector<float> result;
    for (const auto &m : movies)
    {
        auto sorted_ratings = m.ratings;
        std::sort(sorted_ratings.begin(), sorted_ratings.end());
        const size_t n = m.ratings.size();
        const size_t b = static_cast<size_t>(n * 0.05 + 0.5);
        int average = 0;
        for (size_t i = b; i < n - b; ++i)
            average += sorted_ratings[i];
        result.push_back(static_cast<float>(average) / static_cast<float>(n - 2 * b));
    }
    return result;
}

// ############################################################################
// ############################################################################

int main(int /***argc*/, char * * /***argv*/)
{
    std::vector<movie> movies_v = {
        { 101, "The Matrix", {10, 9, 10, 9, 9, 8, 7, 10, 5, 9, 9, 8}},
        { 102, "Gladiator", {10, 5, 7, 8, 9, 8, 9, 10, 10, 5, 9, 8, 10}},
        { 103, "Interstellar", {10, 10, 10, 9, 3, 8, 8, 9, 6, 4, 7, 10}}};
    std::list<movie> movies_l(movies_v.begin(), movies_v.end());
    const size_t n = movies_v.size();
    
    auto average_ratings = solution(movies_v);
    for (size_t i = 0; i < n; ++i)
        fmt::print("{:>5} {:<15} {:4.1f}\n",
                movies_v[i].id,
                movies_v[i].title,
                average_ratings[i]);
    
    average_ratings = solution(movies_l);
    for (size_t i = 0; i < n; ++i)
        fmt::print("{:>5} {:<15} {:4.1f}\n",
                movies_v[i].id,
                movies_v[i].title,
                average_ratings[i]);
    return 0;
}

