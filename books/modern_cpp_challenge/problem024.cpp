#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>
#include <iomanip>

/*****************************************************************************/
/* PROBLEM: String to binary conversion                                      */
/* ------------------------------------------------------------------------- */
/* Write a function that, given a string containing hexadecimal digits as    */
/* the input argument, returns a vector of 8-bit integers that represent the */
/* numerical deserialization of the string content. The following are        */
/* examples:                                                                 */
/*                                                                           */
/* Input: "BAADF00D" or "baadf00d", Output: { 0xBA, 0xAD, 0xF0, 0x0D }       */
/* Input: "010203040506", Output: { 1, 2, 3, 4, 5, 6 }                       */
/*****************************************************************************/

// ############################################################################
// ############################################################################

std::vector<unsigned char> problem(std::string input)
{
    std::vector<unsigned char> result;
    const size_t n = input.size();
    auto toDigit = [](char chr) -> unsigned char
    {
        if      (chr >= 'a') return (chr - 'a') + 10;
        else if (chr >= 'A') return (chr - 'A') + 10;
        else                 return (chr - '0');
    };
    for (size_t i = 0; i < n; i += 2)
        result.push_back((toDigit(input[i    ]) << 4) | toDigit(input[i + 1]));
    return result;
}

// ############################################################################
// ############################################################################

std::vector<unsigned char> solution(std::string str)
{
    auto hexchar_to_int = [](char ch) -> unsigned char
    {
        if (ch >= '0' && ch <= '9') return ch - '0';
        if (ch >= 'A' && ch <= 'Z') return ch - 'A' + 10;
        if (ch >= 'a' && ch <= 'z') return ch - 'a' + 10;
        throw std::invalid_argument("Invalid hexadecimal character.");
    };
    std::vector<unsigned char> result;
    for (size_t i = 0; i < str.size(); i += 2)
        result.push_back((hexchar_to_int(str[i]) << 4) | hexchar_to_int(str[i + 1]));
    return result;
}

// ############################################################################
// ############################################################################

template <template <typename> typename CONTAINER>
std::string toString(const CONTAINER<unsigned char> &range)
{
    const char values[] = "0123456789ABCDEF";
    std::string result;
    for (unsigned char n : range)
    {
        result += values[(n >> 4) & 0x0F];
        result += values[ n       & 0x0F];
    }
    return result;
}

std::ostream& operator<<(std::ostream &out, const std::vector<unsigned char> &vec)
{
    out << '{';
    for (bool next = false; unsigned char v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<unsigned char> &left,
                const std::vector<unsigned char> &right)
{
    if (left.size() != right.size()) return false;
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

void test(std::string hexadecimal,
          const std::vector<unsigned char> &expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::vector<unsigned char> result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution(hexadecimal);
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem(hexadecimal);
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] The vector representation of the hexadecimal string:";
        std::cout << '\n' << result << "\nis different than the Book's expected";
        std::cout << " output:\n" << expected << '\n';
    }
}

int main(int argc, char * * argv)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100'000;
#else
    constexpr unsigned int trials = 1;
#endif
    std::locale::global(std::locale("de_CH.utf8"));
    fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
    std::vector<std::pair<std::string, std::vector<unsigned char>> > ranges;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist_values(0, 255);
    std::uniform_int_distribution<> dist_length(4, 15);
    for (unsigned int i = 0; i < 200; ++i)
    {
        std::vector<unsigned char> current(dist_length(gen));
        for (size_t j = 0; j < current.size(); ++j)
            current[j] = static_cast<unsigned char>(dist_values(gen));
        std::string hex = toString(current);
        ranges.push_back({hex, solution(hex)});
    }
    
    fmt::print("[TEST] User's solution runtime...\n");
    auto time_problem_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[hex, expected] : ranges)
        test(hex, expected, false, trials);
    auto time_problem_end = std::chrono::high_resolution_clock::now();
    auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
    
    fmt::print("[TEST] Book's solution runtime...\n");
    auto time_solution_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[hex, expected] : ranges)
        test(hex, expected, true, trials);
    auto time_solution_end = std::chrono::high_resolution_clock::now();
    auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    return 0;
}
