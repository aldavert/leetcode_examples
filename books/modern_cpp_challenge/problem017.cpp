#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>
#include <array>

/*****************************************************************************/
/* PROBLEM: Creating a 2D array with basic operations.                       */
/* ------------------------------------------------------------------------- */
/* Write a class template that represents a two-dimensional array container  */
/* with methods for access (at() and data()), capacity querying, iterators,  */
/* filling, and swapping. It should be possible to move objects of this      */
/* type.                                                                     */
/*****************************************************************************/

// ############################################################################
// ############################################################################

template <typename T, size_t rows, size_t columns>
class Array2D
{
    std::array<std::array<T, rows>, columns> m_data;
public:
    Array2D(void) {}
    Array2D(std::initializer_list<std::initializer_list<T> > l)
    {
        for (size_t i = 0; const auto &c : l)
        {
            for (size_t j = 0; auto v : c)
                m_data[j++][i] = v;
            ++i;
        }
    }
    Array2D(const T &value)
    {
        for (auto &row : m_data)
            for (auto &v : row)
                v = value;
    }
    constexpr std::array<T, rows> * data(void) noexcept { return m_data.data(); }
    constexpr const std::array<T, rows> * data(void) const noexcept { return m_data.data(); }
    constexpr T& at(size_t r, size_t c) { return m_data.at(c).at(r); }
    constexpr const T& at(size_t r, size_t c) const { return m_data.at(c).at(r); }
    constexpr T& operator()(size_t r, size_t c) { return m_data.at(c).at(r); }
    constexpr const T& operator()(size_t r, size_t c) const { return m_data.at(c).at(r); }
    constexpr bool empty() const noexcept { return rows == 0 || columns == 0; }
    constexpr std::pair<size_t, size_t> size(void) const { return { rows, columns }; }
    constexpr size_t size(const int dimension) const
    {
        if      (dimension == 0) return rows;
        else if (dimension == 1) return columns;
        throw std::out_of_range("Dimension out of range");
    }
    void fill(const T &value)
    {
        for (auto &row : m_data)
            for (auto &v : row)
                v = value;
    }
    void swap(Array2D &other) noexcept { m_data.swap(other.m_data); }
    auto begin(void) { return m_data.begin(); }
    auto end(void) { return m_data.end(); }
    friend std::ostream& operator<<(std::ostream &out, const Array2D<T, rows, columns> &arr)
    {
        for (size_t j = 0; j < rows; ++j)
        {
            out << "[ ";
            for (size_t i = 0; i < columns; ++i)
                out << '\t' << arr.m_data[i][j];
            out << " ]\n";
        }
        return out;
    }
};

int main(int argc, char * * argv)
{
    Array2D<int, 3, 4> arr1 = {{0, 1, 2, 3},
                               {0, 1, 2, 3},
                               {0, 1, 2, 3}};
    Array2D<int, 3, 4> arr2(1);
    std::cout << "Matrix size: " << arr1.size(0) << 'x' << arr1.size(1) << '\n';
    std::cout << arr1 << '\n';
    arr1.swap(arr2);
    auto [row, col] = arr1.size();
    std::cout << "Matrix size: " << row << 'x' << col << '\n';
    std::cout << arr1 << '\n';
    arr1.fill(7);
    std::cout << arr1 << '\n';
    arr2.swap(arr1);
    std::cout << arr1 << '\n';
    arr1(1, 2) = 5;
    arr1.at(2, 1) = 7;
    std::cout << arr1 << '\n';
    return 0;
}
