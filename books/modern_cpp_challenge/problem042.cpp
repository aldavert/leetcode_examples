#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>

/*****************************************************************************/
/* PROBLEM: Day and week of the year                                         */
/* ------------------------------------------------------------------------- */
/* Write a function that, given a date, returns the day of the year (from 1  */
/* to 365 or 366 for leap years) and another function that, for the same     */
/* input, returns the calendar week of the year.                             */
/*****************************************************************************/

using namespace std::literals::chrono_literals;
using std::chrono::January;
using std::chrono::February;
using std::chrono::March;
using std::chrono::April;
using std::chrono::May;
using std::chrono::June;
using std::chrono::July;
using std::chrono::August;
using std::chrono::September;
using std::chrono::October;
using std::chrono::November;
using std::chrono::December;
std::ostream& operator<<(std::ostream &out, const std::chrono::year_month_day &ymd)
{
    out << static_cast<int>(ymd.year()) << '/'
        << std::setw(2) << std::setfill('0')
        << static_cast<unsigned>(ymd.month()) << '/'
        << std::setw(2) << static_cast<unsigned>(ymd.day());
    return out;
}

// ############################################################################
// ############################################################################

unsigned int dayYear(std::chrono::year_month_day dayA)
{
    std::chrono::year_month_day first{dayA.year(), std::chrono::January, 1d};
    return (std::chrono::sys_days{dayA} - std::chrono::sys_days{first}).count() + 1;
}
unsigned int dayYear(int yearA, unsigned int monthA, unsigned int dayA)
{
    return dayYear(std::chrono::year_month_day{std::chrono::year{yearA}, std::chrono::month{monthA}, std::chrono::day{dayA}});
}
unsigned int dayYear(std::chrono::year yearA, std::chrono::month monthA, std::chrono::day dayA)
{
    return dayYear(std::chrono::year_month_day{yearA, monthA, dayA});
}

// ############################################################################
// ############################################################################

unsigned int weekYear(std::chrono::year_month_day dayA)
{
    std::chrono::year_month_day first{dayA.year(), std::chrono::January, 1d};
    std::chrono::weekday weekday{std::chrono::sys_days{first}};
    int day_week = weekday.iso_encoding() - 1;
    int day_year = (std::chrono::sys_days{dayA} - std::chrono::sys_days{first}).count();
    int result = (day_year - (7 - day_week));
    if (result < 0)
        return (day_week < 4);
    result = result / 7
           + 1               // Add 1 so the week the first week is week number 1.
           + (day_week < 4); // Add an extra week for the first week of the year?
    return result;
}
unsigned int weekYear(int yearA, unsigned int monthA, unsigned int dayA)
{
    return weekYear(std::chrono::year_month_day{std::chrono::year{yearA}, std::chrono::month{monthA}, std::chrono::day{dayA}});
}
unsigned int weekYear(std::chrono::year yearA, std::chrono::month monthA, std::chrono::day dayA)
{
    return weekYear(std::chrono::year_month_day{yearA, monthA, dayA});
}

// ############################################################################
// ############################################################################

int main(int argc, char * * argv)
{
    std::string day_name[8] = { "", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
    std::chrono::year_month_day dayA{1981y/April/30d}, dayB{2022y/May/12d};
    std::chrono::time_point<std::chrono::system_clock> now{std::chrono::system_clock::now()};
    std::chrono::year_month_day today{std::chrono::floor<std::chrono::days>(now)};
    
    std::cout << "Today " << today << " is day " << dayYear(today) << " and week "
              << weekYear(today) << " of " << static_cast<int>(today.year()) << ".\n";
    std::cout << dayB << " was day " << dayYear(dayB) << " and week "
              << weekYear(dayB) << " of " << static_cast<int>(dayB.year()) << ".\n";
    std::cout << dayA << " was day " << dayYear(dayA) << " and week "
              << weekYear(dayA) << " of " << static_cast<int>(dayA.year()) << ".\n";
    std::cout << '\n';
    std::cout << "2008/01/01 was day " << dayYear(2008, 1, 01) << " and week "
              << weekYear(2008, 1, 01) << " of 2008.\n";
    std::cout << "2010/01/01 was day " << dayYear(2010, 1, 01) << " and week "
              << weekYear(2010, 1, 01) << " of 2010.\n";
    std::cout << "2008/01/10 was day " << dayYear(2008, 1, 10) << " and week "
              << weekYear(2008, 1, 10) << " of 2008.\n";
    std::cout << "2010/01/10 was day " << dayYear(2010, 1, 10) << " and week "
              << weekYear(2010, 1, 10) << " of 2010.\n";
    std::cout << "2008/04/30 was day " << dayYear(2008, 4, 30) << " and week "
              << weekYear(2008, 4, 30) << " of 2008.\n";
    std::cout << "2008/12/29 was day " << dayYear(2008, 12, 29) << " and week "
              << weekYear(2008, 12, 29) << " of 2008.\n";
    std::cout << "2010/04/30 was day " << dayYear(2010, 4, 30) << " and week "
              << weekYear(2010, 4, 30) << " of 2010.\n";
    std::cout << "2010/12/29 was day " << dayYear(2010, 12, 29) << " and week "
              << weekYear(2010, 12, 29) << " of 2010.\n";
    std::cout << "1981/04/30 was day " << dayYear(1981, 4, 30) << " and week "
              << weekYear(1981, 4, 30) << " of 1981.\n";
    std::cout << "1981/12/29 was day " << dayYear(1981, 12, 29) << " and week "
              << weekYear(1981, 12, 29) << " of 1981.\n";
    std::cout << "1975/09/11 was day " << dayYear(1975y, September, 11d) << " and week "
              << weekYear(1975y, September, 11d) << " of 1975.\n";
    std::cout << "1948/03/11 was day " << dayYear(1948y, March, 11d) << " and week "
              << weekYear(1948y, March, 11d) << " of 1948.\n";
    std::cout << "1948/12/09 was day " << dayYear(1948y, December, 9d) << " and week "
              << weekYear(1948y, December, 9d) << " of 1948.\n";
    
    // ...........  Weeks   Days
    // 2008-04-30 -    18 -  121
    // 2008-12-29 -    53 -  364
    // 2010-04-30 -    17 -  121
    // 2010-12-29 -    52 -  364
    // 1981-04-30 -    18 -  120
    // 1981-12-29 -    53 -  363
    // 1975-09-11 -    37 -  254
    // 1948-03-11 -    11 -   71
    // 1948-12-09 -    50 -  344
}

