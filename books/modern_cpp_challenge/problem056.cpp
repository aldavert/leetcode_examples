#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>

/*****************************************************************************/
/* PROBLEM: Select algorithm                                                 */
/* ------------------------------------------------------------------------- */
/* Write a function that, given a range of values and a projection function  */
/* transforms each value into a new one and returns a new range with the     */
/* selected values. For instance, if you have a type book  that has an id,   */
/* title, and author, and have a range of such book values, it should be     */
/* possible for the function to select only the title of the books. Here is  */
/* an example of how the function should be used:                            */
/* struct book                                                               */
/* {                                                                         */
/*     int         id;                                                       */
/*     std::string title;                                                    */
/*     std::string author;                                                   */
/* };                                                                        */
/* std::vector<book> books {                                                 */
/*     {101, "The C++ Programming Language", "Bjarne Stroustrup"},           */
/*     {203, "Effective Modern C++", "Scott Meyers"},                        */
/*     {404, "The Modern C++ Programming Codebook", "Marius Bancilla"}};     */
/* auto titles = select(books, [](book const &b) { return b.title; });       */
/*****************************************************************************/

// ############################################################################
// ############################################################################

template <typename T,
          template <typename> typename RANGE,
          typename SELECT>
requires std::ranges::range<RANGE<T> >
         &&  std::invocable<SELECT&, T>
auto problem(const RANGE<T> &range, SELECT select)
{
    RANGE<decltype(select(T{}))> result;
    for (const auto &val : range)
        result.push_back(select(val));
    return result;
}

// ############################################################################
// ############################################################################

struct book
{
    int         id;
    std::string title;
    std::string author;
};

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::pair<T, T> &tup)
{
    out << '{' << tup.first << ", " << tup.second << '}';
    return out;
}

template <typename T, template <typename> typename RANGE>
requires std::ranges::range<RANGE<T> >
         && (!std::same_as<RANGE<void>, std::basic_string<void> >)
std::ostream& operator<<(std::ostream &out, const RANGE<T> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

int main(int /***argc*/, char * * /***argv*/)
{
    std::vector<book> books{
        {101, "The C++ Programming Language", "Bjarne Stroustrup"},
        {203, "Effective Modern C++", "Scott Meyers"},
        {404, "The Modern C++ Programming Codebook", "Marius Bancilla"}};
    std::cout << "Titles: "
              << problem(books, [](const book &b){ return b.title; })
              << '\n';
    return 0;
}

