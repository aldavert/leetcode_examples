#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>

/*****************************************************************************/
/* PROBLEM: Double buffer                                                    */
/* ------------------------------------------------------------------------- */
/* Write a class that represents a buffer that could be written and read at  */
/* the same time without the two operations colliding. A read operation must */
/* provide access to the old data while a write operation is in progress.    */
/* Newly written data must be available for reading upon completion of the   */
/* write operation.                                                          */
/*****************************************************************************/

// ############################################################################
// ############################################################################

template <typename T>
class DoubleBuffer
{
    std::vector<T> m_input_buffer;
    std::vector<T> m_output_buffer;
    std::mutex m_swap_mutex;
public:
    DoubleBuffer(void) = default;
    DoubleBuffer(const DoubleBuffer &other) = delete;
    DoubleBuffer& operator=(const DoubleBuffer &other) = delete;
    DoubleBuffer(DoubleBuffer &&other) = default;
    DoubleBuffer& operator=(DoubleBuffer &&other) = default;
    void write(const std::vector<T> &input)
    {
        m_input_buffer = input;
        std::unique_lock<std::mutex> m_swap_mutex;
        m_output_buffer.swap(m_input_buffer);
    }
    template <std::input_or_output_iterator ITER>
    void read(ITER it)
    {
        std::unique_lock<std::mutex> m_swap_mutex;
        std::copy(m_output_buffer.begin(), m_output_buffer.end(), it);
    }
    void read(std::vector<T> &read)
    {
        std::unique_lock<std::mutex> m_swap_mutex;
        read = m_output_buffer;
    }
};

// ############################################################################
// ############################################################################

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::vector<T> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

template <typename T>
bool operator==(const std::vector<T> &left, const std::vector<T> &right)
{
    if (left.size() != right.size())
        return false;
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

int main(int /***argc*/, char * * /***argv*/)
{
    DoubleBuffer<int> buffer;
    std::thread t([&buffer]()
    {
        std::vector<int> elements(10);
        for (int i = 0, p0 = 1, p1 = 1; i < 100; ++i)
        {
            using namespace std::chrono_literals;
            if (p1 > 1'000'000) p0 = p1 = 1;
            for (int j = 0; j < 10; ++j)
            {
                elements[j] = p0 = std::exchange(p1, p1 + p0);
                std::this_thread::sleep_for(11ms);
            }
            buffer.write(elements);
            std::this_thread::sleep_for(50ms);
        }
    });
    auto start = std::chrono::system_clock::now();
    do
    {
        buffer.read(std::ostream_iterator<int>(std::cout, ", "));
        std::cout << '\n';
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(250ms);
    } while (std::chrono::duration_cast<std::chrono::seconds>(
             std::chrono::system_clock::now() - start).count() < 10);
    std::cout << "out of main loop\n";
    t.join();
    return 0;
}

