#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>
#include <iomanip>

/*****************************************************************************/
/* PROBLEM: Binary to string conversion                                      */
/* ------------------------------------------------------------------------- */
/* Write a function that, given a range of 8-bit integers (such as an array  */
/* or vector), returns a string that contains a hexadecimal representation   */
/* of the input data. The function should be able to produce both uppercase  */
/* and lowercase content. Here are some input and output examples:           */
/*                                                                           */
/* Input: { 0xBA, 0xAD, 0xF0, 0x0D }, Output: "BAADF00D" or "baadf00d"       */
/* Input: { 1, 2, 3, 4, 5, 6 }, Output: "010203040506"                       */
/*****************************************************************************/

// ############################################################################
// ############################################################################

template <template <typename> typename CONTAINER>
std::string problem(const CONTAINER<unsigned char> &range)
{
    const char values[] = "0123456789ABCDEF";
    std::string result;
    for (unsigned char n : range)
    {
        result += values[(n >> 4) & 0x0F];
        result += values[ n       & 0x0F];
    }
    return result;
}

// ############################################################################
// ############################################################################

template <template <typename> typename CONTAINER>
std::string solution(const CONTAINER<unsigned char> &range)
{
    std::ostringstream oss;
    oss.setf(std::ios_base::uppercase);
    for (unsigned char n : range)
        oss << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(n);
    return oss.str();
}

// ############################################################################
// ############################################################################

template <template <typename> typename CONTAINER>
void test(const CONTAINER<unsigned char> &range,
          std::string expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::string result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution(range);
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem(range);
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] The hexadecimal representation of the range:";
        std::cout << '\n' << result << "\nis different than the Book's expected";
        std::cout << " output:\n" << expected << '\n';
    }
}

int main(int argc, char * * argv)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 10'000;
#else
    constexpr unsigned int trials = 1;
#endif
    std::locale::global(std::locale("de_CH.utf8"));
    fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
    std::vector<std::pair<std::vector<unsigned char>, std::string> > ranges;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist_values(0, 255);
    std::uniform_int_distribution<> dist_length(4, 15);
    for (unsigned int i = 0; i < 200; ++i)
    {
        std::vector<unsigned char> current(dist_length(gen));
        for (size_t j = 0; j < current.size(); ++j)
            current[j] = static_cast<unsigned char>(dist_values(gen));
        ranges.push_back({current, solution(current)});
    }
    
    fmt::print("[TEST] User's solution runtime...\n");
    auto time_problem_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[range, expected] : ranges)
        test(range, expected, false, trials);
    auto time_problem_end = std::chrono::high_resolution_clock::now();
    auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
    
    fmt::print("[TEST] Book's solution runtime...\n");
    auto time_solution_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[range, expected] : ranges)
        test(range, expected, true, trials);
    auto time_solution_end = std::chrono::high_resolution_clock::now();
    auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    return 0;
}
