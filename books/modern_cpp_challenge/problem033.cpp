#include <vector>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <concepts>

/*****************************************************************************/
/* PROBLEM: Tabular printing of a list of processes                          */
/* ------------------------------------------------------------------------- */
/* Suppose you have a snapshot of the list of all processes in a system. The */
/* information for each process includes name, identifier, status (which can */
/* be either running or suspended), account name (under which the process    */
/* runs), memory size in bytes, and platform (which can be either 32-bit or  */
/* 64-bit). Your task is to write a function that takes such a list of       */
/* processes and prints them to the console alphabetically, in tabular       */
/* format. All columns must be left-aligned, except for memory column which  */
/* must be right-aligned. The value of the memory size must be displayed     */
/* in KB. The following is an example of the output of this function:        */
/*                                                                           */
/* chrome.exe     1044    Running   marius.bancilla    25180  32-bit         */
/* chrome.exe     10100   Running   marius.bancilla   227756  32-bit         */
/* cmd.exe        512     Running   SYSTEM                48  64-bit         */
/* explorer.exe   7108    Running   marius.bancilla    29529  64-bit         */
/* skype.exe      22456   Suspended marius.bancilla      656  64-bit         */
/*****************************************************************************/

enum class procstatus { suspended, running };
enum class platforms { p32bit, p64bit };
struct procinfo
{
    int id;
    std::string name;
    procstatus status;
    std::string account;
    size_t memory;
    platforms platform;
};

// ############################################################################
// ############################################################################

void problem(const std::vector<procinfo> &processes)
{
    const size_t n = processes.size();
    std::vector<size_t> order(n);
    for (size_t i = 0; i < n; ++i)
        order[i] = i;
    std::sort(order.begin(), order.end(), [&](int i1, int i2){return processes[i1].name < processes[i2].name;});
    for (size_t i = 0; i < n; ++i)
    {
        const auto &p = processes[i];
        fmt::print("{:<24} {:<7} {:<11} {:<15} {:>9} {}\n",
                p.name,
                p.id,
                (p.status == procstatus::running)?"running":"suspended",
                p.account,
                p.memory / 1024,
                (p.platform == platforms::p32bit)?"32-bit":"64-bit");
    }
}

// ############################################################################
// ############################################################################

std::string status_to_string(procstatus const status)
{
    if (status == procstatus::suspended) return "suspended";
    else return "running";
}

std::string platform_to_string(platforms const platform)
{
    if (platform == platforms::p32bit) return "32-bit";
    else return "64-bit";
}

void solution(std::vector<procinfo> processes)
{
    std::sort(std::begin(processes), std::end(processes),
            [](procinfo const &p1, procinfo const &p2)
            { return p1.name < p2.name; });
    for (auto const &pi: processes)
    {
        std::cout << std::left << std::setw(25) << std::setfill(' ') << pi.name;
        std::cout << std::left << std::setw(8) << std::setfill(' ') << pi.id;
        std::cout << std::left << std::setw(12) << std::setfill(' ') << status_to_string(pi.status);
        std::cout << std::left << std::setw(15) << std::setfill(' ') << pi.account;
        std::cout << std::left << std::right << std::setw(10) << std::setfill(' ') << (int)(pi.memory / 1024);
        std::cout << std::left << ' ' << platform_to_string(pi.platform);
        std::cout << std::endl;
    }
}

// ############################################################################
// ############################################################################

int main(int argc, char * * argv)
{
    std::locale::global(std::locale("de_CH.utf8"));
    std::vector<procinfo> processes;
    processes.push_back({ 1044, "chrome.exe"  , procstatus::running  , "marius.bancilla",  25180 * 1024, platforms::p32bit});
    processes.push_back({10100, "chrome.exe"  , procstatus::running  , "marius.bancilla", 227756 * 1024, platforms::p32bit});
    processes.push_back({  512, "cmd.exe"     , procstatus::running  , "SYSTEM"         ,     48 * 1024, platforms::p64bit});
    processes.push_back({ 7108, "explorer.exe", procstatus::running  , "marius.bancilla",  29529 * 1024, platforms::p64bit});
    processes.push_back({22456, "skype.exe"   , procstatus::suspended, "marius.bancilla",    656 * 1024, platforms::p64bit});
    

    fmt::print("[OWN] Process information:\n");
    fmt::print("---------------------------------------------------------------\n");
    problem(processes);
    fmt::print("\n");
    
    fmt::print("[BOOK] Process information:\n");
    fmt::print("---------------------------------------------------------------\n");
    solution(processes);
    fmt::print("\n");
    
    return 0;
}
