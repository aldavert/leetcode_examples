#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>

/*****************************************************************************/
/* PROBLEM: Sexy prime pairs.                                                */
/* ------------------------------------------------------------------------- */
/* Write a program that prints all the sexy prime pairs up to a limit        */
/* entered by the user.                                                      */
/*****************************************************************************/

// ############################################################################
// ############################################################################

std::vector<std::pair<unsigned int, unsigned int> > problem(unsigned int limit)
{
    std::vector<std::pair<unsigned int, unsigned int> > result;
    // +1 to include the number in the sieve and +6 extra to take into account
    // the pairs of the numbers close to the limit that are outside the limit
    // (e.g. the pair {97, 103} when limit = 100).
    limit += 7;
    std::vector<bool> sieve(limit, true);
    sieve[0] = sieve[1] = false;
    for (unsigned int i = 2; i * i < limit; ++i)
        if (sieve[i])
            for (unsigned int j = i * i; j < limit; j += i)
                sieve[j] = false;
    for (unsigned int i = 0; i < limit - 6; ++i)
        if (sieve[i] && sieve[i + 6])
            result.push_back({i, i + 6});
    return result;
}

// ############################################################################
// ############################################################################

std::vector<std::pair<unsigned int, unsigned int> > solution(unsigned int limit)
{
    auto is_prime = [](int value) -> bool
    {
        if (value <= 3) return value > 1;
        else if ((value & 1) == 0) return false;
        else if (value % 3 == 0) return false;
        for (int i = 5; i * i <= value; i += 6)
            if ((value % i == 0) || (value % (i + 2) == 0))
                return false;
        return true;
    };
    std::vector<std::pair<unsigned int, unsigned int> > result;
    for (unsigned int i = 2; i <= limit; ++i)
        if (is_prime(i) && is_prime(i + 6))
            result.push_back({i, i + 6});
    return result;
}

// ############################################################################
// ############################################################################

std::ostream& operator<<(std::ostream &out,
                         const std::vector<std::pair<unsigned int, unsigned int> > &vec)
{
    out << '{';
    for (bool test = false; const auto &v : vec)
    {
        if (test) [[likely]] out << ", ";
        test = true;
        out << '{' << v.first << ", " << v.second << '}';
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<std::pair<unsigned int, unsigned int> > &left,
                const std::vector<std::pair<unsigned int, unsigned int> > &right)
{
    if (left.size() != right.size()) return false;
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

void test(unsigned int limit,
          const std::vector<std::pair<unsigned int, unsigned int> > &expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::vector<std::pair<unsigned int, unsigned int> > result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution(limit);
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem(limit);
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] sexy prime pairs under '" << limit;
        std::cout << "' is expected to be:\n" << expected << "\n However, the results obtained are:\n";
        std::cout << result << '\n';
    }
}

int main(int argc, char * * argv)
{
    if ((argc > 1) && (std::strcmp(argv[1], "time") == 0))
    {
#if 1
        //constexpr unsigned int trials = 10'000'000;
        constexpr unsigned int trials = 1'000;
#else
        constexpr unsigned int trials = 1;
#endif
        std::locale::global(std::locale("de_CH.utf8"));
        auto time_problem_begin = std::chrono::high_resolution_clock::now();
        std::cout << "Testing the runtime of the user's solution...\n";
        for (unsigned int l : std::vector<unsigned int>({1, 1'000, 10'000, 100'000, 1'000'000}))
            test(l, solution(l), false, trials);
        auto time_problem_end = std::chrono::high_resolution_clock::now();
        auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
        fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
        
        auto time_solution_begin = std::chrono::high_resolution_clock::now();
        std::cout << "Testing the runtime of the book's solution...\n";
        for (unsigned int l : std::vector<unsigned int>({1, 1'000, 10'000, 100'000, 1'000'000}))
            test(l, solution(l), true, trials);
        auto time_solution_end = std::chrono::high_resolution_clock::now();
        auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
        fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    }
    else
    {
        unsigned int limit = 0;
        std::cout << "Input number: ";
        std::cin >> limit;
        auto result = problem(limit);
        std::cout << "The sexy prime pairs under " << limit << " are:\n" << result;
        if (auto sol = solution(limit); sol == result)
            std::cout << "\n[CORRECT]\n";
        else std::cout << "\n[FAILURE] The expected solution is:\n" << sol << '\n';
    }
    return 0;
}
