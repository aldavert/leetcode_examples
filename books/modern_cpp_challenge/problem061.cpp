#include <vector>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>
#include <execution>

/*****************************************************************************/
/* PROBLEM: Parallel transform algorithm                                     */
/* ------------------------------------------------------------------------- */
/* Write a general-purpose algorithm that applies a given unary function to  */
/* transform the elements of a range in parallel. The unary operation used   */
/* to transform the range must not invalidate range iterators or modify the  */
/* elements of the range. The level of parallelism, that is, the number of   */
/* execution threads and the way it is achieved, is an implementation        */
/* detail.                                                                   */
/*****************************************************************************/

// ############################################################################
// ############################################################################

int main(int /***argc*/, char * * /***argv*/)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dist(0, 1);
    std::vector<double> value(200'000'000);
    for (size_t i = 0; i < value.size(); ++i)
        value[i] = dist(gen);
    
    {
        auto begin = std::chrono::high_resolution_clock::now();
        std::transform(std::execution::par,
                       value.begin(),
                       value.end(),
                       value.begin(),
                       [](double v)
                       { return std::sqrt(std::exp(-v)); });
        auto time = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::high_resolution_clock::now() - begin).count();
        fmt::print("Some values:");
        for (size_t i = 0; i < 20; ++i) fmt::print(" {:.4f}", value[i]);
        fmt::print("\n");
        fmt::print("Parallel elapsed time {} ms.\n", time);
        fmt::print("\n");
    }
    {
        auto begin = std::chrono::high_resolution_clock::now();
        std::transform(std::execution::par_unseq,
                       value.begin(),
                       value.end(),
                       value.begin(),
                       [](double v)
                       { return std::sqrt(std::exp(-v)); });
        auto time = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::high_resolution_clock::now() - begin).count();
        fmt::print("Some values:");
        for (size_t i = 0; i < 20; ++i) fmt::print(" {:.4f}", value[i]);
        fmt::print("\n");
        fmt::print("Parallel unsequenced elapsed time {} ms.\n", time);
        fmt::print("\n");
    }
    {
        auto begin = std::chrono::high_resolution_clock::now();
        std::transform(std::execution::seq,
                       value.begin(),
                       value.end(),
                       value.begin(),
                       [](double v)
                       { return std::sqrt(std::exp(-v)); });
        auto time = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::high_resolution_clock::now() - begin).count();
        fmt::print("Some values:");
        for (size_t i = 0; i < 20; ++i) fmt::print(" {:.4f}", value[i]);
        fmt::print("\n");
        fmt::print("Sequential elapsed time {} ms.\n", time);
        fmt::print("\n");
    }
    return 0;
}

