#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <concepts>

/*****************************************************************************/
/* PROBLEM: Pascal's triangle                                                */
/* ------------------------------------------------------------------------- */
/* Write a function that prints up to 10 rows of Pascal's triangle to the    */
/* console.                                                                  */
/*****************************************************************************/

// ############################################################################
// ############################################################################
//    x
//   x x
//  x x x
// x x x x
//    xx
//   xx xx
//  xx xx xx
// xx xx xx xx
//      xxx 
//    xxx xxx
//  xxx xxx xxx
//xxx xxx xxx xxx

void problem(int n)
{
    int largest = 1;
    for (int c = 0; c < n / 2; ++c)
        largest = largest * (n - 1 - c) / (c + 1);
    int spaces = ((largest > 0)?(static_cast<int>(std::log10(largest)) + 1):1) + 2;
    std::cout << "Largest: " << largest << ' ' << spaces << '\n';
    for (int row = 0; row < n; ++row)
    {
        fmt::print("{:<{}}", ' ',  (n - row) * (spaces / 2 + 1));
        for (int value = 1, column = 0; column <= row; ++column)
        {
            fmt::print("{:^{}d}{:<{}}", value, spaces, ' ', 1 + ((spaces & 1) == 0));
            value = value * (row - column) / (column + 1);
        }
        std::cout << '\n';
    }
}

// ############################################################################
// ############################################################################

unsigned int number_of_digits(unsigned int i)
{
    return (i > 0)?(int)std::log10((double)i) + 1:1;
}

void solution(int n)
{
    for (int i = 0; i < n; ++i)
    {
        auto x = 1;
        std::cout << std::string((n - i - 1) * (n / 2), ' ');
        for (int j = 0; j <= i; ++j)
        {
            auto y = x;
            x = x * (i - j) / (j + 1);
            auto maxlen = number_of_digits(x) - 1;
            std::cout << y << std::string(n - 1 - maxlen - n % 2, ' ');
        }
        std::cout << '\n';
    }
}

// ############################################################################
// ############################################################################

int main(int argc, char * * argv)
{
    std::locale::global(std::locale("de_CH.utf8"));
    fmt::print("[OWN] 4 rows of the Pascal triangle\n");
    fmt::print("---------------------------------------------------------------\n");
    problem(4);
    fmt::print("\n");
    fmt::print("[BOOK] 4 rows of the Pascal triangle\n");
    fmt::print("---------------------------------------------------------------\n");
    solution(4);
    fmt::print("\n");
    
    fmt::print("[OWN] 7 rows of the Pascal triangle\n");
    fmt::print("---------------------------------------------------------------\n");
    problem(7);
    fmt::print("\n");
    fmt::print("[BOOK] 7 rows of the Pascal triangle\n");
    fmt::print("---------------------------------------------------------------\n");
    solution(7);
    fmt::print("\n");
    
    fmt::print("[OWN] 10 rows of the Pascal triangle\n");
    fmt::print("---------------------------------------------------------------\n");
    problem(10);
    fmt::print("\n");
    fmt::print("[BOOK] 10 rows of the Pascal triangle\n");
    fmt::print("---------------------------------------------------------------\n");
    solution(10);
    fmt::print("\n");
    return 0;
}
