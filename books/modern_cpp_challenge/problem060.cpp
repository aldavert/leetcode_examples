#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>

/*****************************************************************************/
/* PROBLEM: The Game of Life                                                 */
/* ------------------------------------------------------------------------- */
/* Write a program that implements the Game of Life cellular automaton       */
/* proposed by John Horton Conway. The universe of this game is a grid of    */
/* square cells that could have one of two states: dead or alive. Every cell */
/* interacts with its adjacent neighbors, with the following transactions    */
/* occurring on every step:                                                  */
/*     - Any live cell with fewer than two live neighbors dies, as if caused */
/*       by underpopulation.                                                 */
/*     - Any live cell with two or three live neighbors lives on the next    */
/*       generation.                                                         */
/*     - Any live cell with more than three live neighbors dies, as if by    */
/*       overpopulation.                                                     */
/*     - Any dead cell with exactly three live neighbors become a live cell, */
/*       as if by reproduction.                                              */
/*  The status of the game of each iteration should be displayed on the      */
/*  console, and for convenience, you should pick a reasonable size such as  */
/*  20 rows * 50 columns.                                                    */
/*****************************************************************************/

template <typename T, typename U>
std::ostream& operator<<(std::ostream &out, const std::pair<T, U> &tup)
{
    out << '{' << tup.first << ", " << tup.second << '}';
    return out;
}

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::vector<T> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

// ############################################################################
// ############################################################################

void display(const std::vector<std::vector<bool> > &world)
{
    const unsigned int nrows = static_cast<unsigned int>(world.size());
    const unsigned int ncols = static_cast<unsigned int>(world[0].size());
    const std::string symbols[] = {" ", "▀", "▄", "█"};
    std::cout << "\033[1;1H";//"\033[2J\033[1;1H";
    unsigned int r;
    for (r = 0; r + 1 < nrows; r += 2)
    {
        for (unsigned int c = 0; c < ncols; ++c)
            std::cout << symbols[(static_cast<int>(world[r][c]) | (static_cast<int>(world[r + 1][c]) << 1))];
        std::cout << '\n';
    }
    if (r < nrows)
        for (unsigned int c = 0; c < ncols; ++c)
            std::cout << symbols[world[r][c]];
    std::cout << '\n';
}

// ############################################################################
// ############################################################################

std::vector<std::vector<bool> > update(const std::vector<std::vector<bool> > &world)
{
    const int dx[] = { -1,  0,  1,  1,  1,  0, -1, -1 };
    const int dy[] = { -1, -1, -1,  0,  1,  1,  1,  0 };
    const int nrows = static_cast<int>(world.size()) - 1;
    const int ncols = static_cast<int>(world[0].size()) - 1;
    std::vector<std::vector<bool> > result(nrows + 1, std::vector<bool>(ncols + 1));
    
    auto countFrontier = [&](int row, int col) -> int
    {
        int counter = 0;
        for (unsigned int i = 0; i < 8; ++i)
        {
            int new_col = col + dx[i];
            int new_row = row + dy[i];
            if ((new_col >= 0) && (new_col <= ncols) && (new_row >= 0) && (new_row <= nrows))
                counter += world[new_row][new_col];
        }
        return counter;
    };
    auto count = [&](int row, int col) -> int
    {
        int counter = 0;
        for (unsigned int i = 0; i < 8; ++i)
            counter += world[row + dy[i]][col + dx[i]];
        return counter;
    };
    
    for (int r = 0; r <= nrows; ++r)
    {
        int neighbors = countFrontier(r, 0);
        if (world[r][0]) result[r][0] = (neighbors >= 2) && (neighbors <= 3);
        else result[r][0] = neighbors == 3;
        neighbors = countFrontier(r, ncols);
        if (world[r][ncols]) result[r][ncols] = (neighbors >= 2) && (neighbors <= 3);
        else result[r][ncols] = neighbors == 3;
    }
    for (int c = 0; c <= ncols; ++c)
    {
        int neighbors = countFrontier(0, c);
        if (world[0][c]) result[0][c] = (neighbors >= 2) && (neighbors <= 3);
        else result[0][c] = neighbors == 3;
        neighbors = countFrontier(nrows, c);
        if (world[nrows][c]) result[nrows][c] = (neighbors >= 2) && (neighbors <= 3);
        else result[nrows][c] = neighbors == 3;
    }
    for (int r = 1; r < nrows; ++r)
    {
        for (int c = 1; c < ncols; ++c)
        {
            int neighbors = count(r, c);
            if (world[r][c]) result[r][c] = (neighbors >= 2) && (neighbors <= 3);
            else result[r][c] = neighbors == 3;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

int main(int argc, char * * argv)
{
    using namespace std::chrono_literals;
    unsigned int nrows = 75;
    unsigned int ncols = 75;
    double start_probability = 0.15;
    unsigned int number_of_iterations = 100;
    unsigned int wait = 100;
    
    auto message = [](void) -> void
    {
        std::cout << "\nThe Weasel program\n";
        std::cout << "$ ./problem059 (-h|-p <value>|-r <value>|-c <value>|-t <value>|-w <value>)*\n";
        std::cout << "\n";
        std::cout << "        -h Displays this help message.\n";
        std::cout << "-p <value> Probability that a cell will be set alive. Value must\n";
        std::cout << "           be set between 0 and 1.\n";
        std::cout << "-r <value> Number of rows of the world. Value must be set between\n";
        std::cout << "           10 and 250.\n";
        std::cout << "-c <value> Number of columns of the world. Value must be set\n";
        std::cout << "           between 10 and 250.\n";
        std::cout << "-t <value> Number of iterations of the simulation. Value must be\n";
        std::cout << "           set between 1 and 10'000.\n";
        std::cout << "-w <value> Milliseconds sleep/wait between iterations of the\n";
        std::cout << "           simulation. Value must be between 10 and 1'000.\n";
    };
    for (int idx = 1; idx < argc; ++idx)
    {
        if (strcmp(argv[idx], "-h") == 0)
        {
            message();
            return 0;
        }
        else if (strcmp(argv[idx], "-p") == 0)
        {
            if (idx + 1 == argc)
            {
                std::cerr << "[ERROR] A value setting the probability parameter was expected.\n";
                message();
                return -1;
            }
            start_probability = std::atof(argv[++idx]);
            if ((start_probability <= 0) || (start_probability > 1))
            {
                std::cerr << "[ERROR] Probability value '" << start_probability << "'out of range.\n";
                message();
                return -1;
            }
        }
        else if (strcmp(argv[idx], "-r") == 0)
        {
            if (idx + 1 == argc)
            {
                std::cerr << "[ERROR] A value setting the number of world's rows was expected.\n";
                message();
                return -1;
            }
            nrows = std::atoi(argv[++idx]);
            if ((nrows < 10) || (nrows > 250))
            {
                std::cerr << "[ERROR] Number of rows value '" << nrows << "' out of range.\n";
                message();
                return -1;
            }
        }
        else if (strcmp(argv[idx], "-c") == 0)
        {
            if (idx + 1 == argc)
            {
                std::cerr << "[ERROR] A value setting the number of world's columns was expected.\n";
                message();
                return -1;
            }
            ncols = std::atoi(argv[++idx]);
            if ((ncols < 10) || (ncols > 250))
            {
                std::cerr << "[ERROR] Number of columns value '" << ncols << "' out of range.\n";
                message();
                return -1;
            }
        }
        else if (strcmp(argv[idx], "-t") == 0)
        {
            if (idx + 1 == argc)
            {
                std::cerr << "[ERROR] A value setting the number of simulation's iterations was expected.\n";
                message();
                return -1;
            }
            number_of_iterations = std::atoi(argv[++idx]);
            if ((number_of_iterations < 1) || (number_of_iterations > 10'000))
            {
                std::cerr << "[ERROR] Number of iterations value '" << number_of_iterations << "' out of range.\n";
                message();
                return -1;
            }
        }
        else if (strcmp(argv[idx], "-w") == 0)
        {
            if (idx + 1 == argc)
            {
                std::cerr << "[ERROR] A value setting the wait time was expected.\n";
                message();
                return -1;
            }
            wait = std::atoi(argv[++idx]);
            if ((wait < 10) || (wait > 1'000))
            {
                std::cerr << "[ERROR] Wait time '" << wait << "' out of range.\n";
                message();
                return -1;
            }
        }
        else
        {
            std::cerr << "[ERROR] Unexpected parameter '" << argv[idx] << "'.\n";
            message();
            return -1;
        }
    }
    
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dist(0, 1);
    std::vector<std::vector<bool> > world(nrows, std::vector<bool>(ncols));
    for (unsigned int r = 0; r < nrows; ++r)
        for (unsigned int c = 0; c < ncols; ++c)
            world[r][c] = dist(gen) < start_probability;
    std::cout << "\033[2J";
    for (unsigned int iter = 0; iter < number_of_iterations; ++iter)
    {
        display(world);
        world = update(world);
        fmt::print("{}/{} Iteration...       \n", iter + 1, number_of_iterations);
        std::this_thread::sleep_for(std::chrono::milliseconds(wait));
    }
    display(world);
    return 0;
}

