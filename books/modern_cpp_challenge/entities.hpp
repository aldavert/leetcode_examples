#include <array>
#include <unordered_map>
#include <string>

#ifndef __ENTITIES_HEADER_FILE__
#define __ENTITIES_HEADER_FILE__

class HTMLEntities
{
public:
    struct Entry
    {
        wchar_t symbol_utf32 = 0;
        std::string symbol_utf8 = "";
        std::string entity = "";
        std::string name = "";
    };
    /// Default constructor.
    HTMLEntities(void);
    /// Access to the index-th HTML entity.
    const Entry& operator[](const unsigned int idx) const { return m_entries[idx]; }
    /// Converts the UTF-8 characters into HTML entities.
    std::string encode(const std::string &input) const;
    /// Converts the HTML entities into UTF-8 characters.
    std::string decode(const std::string &input) const;
protected:
    std::array<Entry, 799> m_entries;
    std::unordered_map<std::string, size_t> m_entity2entry;
    std::unordered_map<wchar_t, size_t> m_symbol2entry;
};

#endif

