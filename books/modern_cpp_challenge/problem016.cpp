#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>

/*****************************************************************************/
/* PROBLEM: Enumerating IPv4 addresses in a range.                           */
/* ------------------------------------------------------------------------- */
/* Write a program that allows the user to input two IPv4 addresses          */
/* representing a range and list all the addresses in that range. Extend the */
/* structure defined for the previous problem to implement the requested     */
/* functionality.                                                            */
/*****************************************************************************/

// ############################################################################
// ############################################################################

class IPv4Information
{
public:
    IPv4Information(void) :
        m_address({0}) {}
    template <std::integral T>
    IPv4Information(T a, T b, T c, T d)
    {
        m_address[0] = static_cast<unsigned char>(a);
        m_address[1] = static_cast<unsigned char>(b);
        m_address[2] = static_cast<unsigned char>(c);
        m_address[3] = static_cast<unsigned char>(d);
    }
    template <std::integral T>
    IPv4Information(std::array<T, 4> address) :
        m_address(address) {}
    IPv4Information(unsigned int address)
    {
        m_address[0] = static_cast<unsigned char>((address >> 24) & 0xFF);
        m_address[1] = static_cast<unsigned char>((address >> 16) & 0xFF);
        m_address[2] = static_cast<unsigned char>((address >>  8) & 0xFF);
        m_address[3] = static_cast<unsigned char>((address      ) & 0xFF);
    }
    template <std::integral T>
    IPv4Information& operator=(std::array<T, 4> address)
    {
        m_address = address;
        return *this;
    }
    IPv4Information& operator=(unsigned int address)
    {
        m_address[0] = static_cast<unsigned char>((address >> 24) & 0xFF);
        m_address[1] = static_cast<unsigned char>((address >> 16) & 0xFF);
        m_address[2] = static_cast<unsigned char>((address >>  8) & 0xFF);
        m_address[3] = static_cast<unsigned char>((address      ) & 0xFF);
        return *this;
    }
    template <typename T = unsigned int>
    T A(void) const { return static_cast<T>(m_address[0]); }
    template <typename T = unsigned int>
    T B(void) const { return static_cast<T>(m_address[1]); }
    template <typename T = unsigned int>
    T C(void) const { return static_cast<T>(m_address[2]); }
    template <typename T = unsigned int>
    T D(void) const { return static_cast<T>(m_address[3]); }
    unsigned int toInteger(void) const
    {
        return (static_cast<unsigned int>(m_address[0]) << 24)
             | (static_cast<unsigned int>(m_address[1]) << 16)
             | (static_cast<unsigned int>(m_address[2]) <<  8)
             | (static_cast<unsigned int>(m_address[3])      );
    }
    friend std::ostream& operator<<(std::ostream &out, const IPv4Information &ipv4)
    {
        out << ipv4.A() << '.' << ipv4.B() << '.' << ipv4.C() << '.' << ipv4.D();
        return out;
    }
    friend std::istream& operator>>(std::istream &in, IPv4Information &ipv4)
    {
        int a, b, c, d;
        char dot_ab, dot_bc, dot_cd;
        in >> a >> dot_ab >> b >> dot_bc >> c >> dot_cd >> d;
        if ((dot_ab == '.') && (dot_bc == '.') && (dot_cd == '.'))
            ipv4 = {a, b, c, d};
        else in.setstate(std::ios_base::failbit);
        return in;
    }
    IPv4Information& operator++(void)
    {
        return *this = (toInteger() + 1);
    }
    IPv4Information& operator++(int)
    {
        return *this = (toInteger() + 1);
    }
    auto operator<=>(const IPv4Information &other) const
    {
        return toInteger() <=> other.toInteger();
    }
    std::tuple<int, int, int, int> tuple(void) const
    {
        return std::make_tuple(static_cast<int>(m_address[0]),
                               static_cast<int>(m_address[1]),
                               static_cast<int>(m_address[2]),
                               static_cast<int>(m_address[3]));
    }
protected:
    std::array<unsigned char, 4> m_address = {0, 0, 0, 0};
};

std::string problem(std::tuple<int, int, int, int> ab,
                    std::tuple<int, int, int, int> ae)
{
    IPv4Information range_start(std::get<0>(ab), std::get<1>(ab),
                                std::get<2>(ab), std::get<3>(ab));
    IPv4Information range_end(std::get<0>(ae), std::get<1>(ae),
                              std::get<2>(ae), std::get<3>(ae));
    std::ostringstream output;
    for (auto it = range_start; it <= range_end; ++it)
        output << "Address: " << it << '\n';
    return output.str();
}

// ############################################################################
// ############################################################################

class ipv4
{
    std::array<unsigned char, 4> data;
public:
    constexpr ipv4() : data {{0}} {}
    constexpr ipv4(unsigned char a, unsigned char b, unsigned char c, unsigned char d):
        data({a, b, c, d}) {}
    explicit constexpr ipv4(unsigned long a) :
        data{{static_cast<unsigned char>((a >> 24) & 0xFF),
              static_cast<unsigned char>((a >> 16) & 0xFF),
              static_cast<unsigned char>((a >>  8) & 0xFF),
              static_cast<unsigned char>( a        & 0xFF)}} {}
    ipv4(const ipv4 &other) noexcept : data(other.data) {}
    ipv4& operator=(const ipv4 &other) noexcept
    {
        data = other.data;
        return *this;
    }
    std::string to_string(void) const
    {
        std::stringstream sstr;
        sstr << *this;
        return sstr.str();
    }
    constexpr unsigned long to_ulong() const noexcept
    {
        return (static_cast<unsigned long>(data[0]) << 24)
             | (static_cast<unsigned long>(data[1]) << 16)
             | (static_cast<unsigned long>(data[2]) <<  8)
             | (static_cast<unsigned long>(data[3])      );
    }
    friend std::ostream& operator<<(std::ostream &os, const ipv4 &a)
    {
        os << static_cast<int>(a.data[0]) << '.'
           << static_cast<int>(a.data[1]) << '.'
           << static_cast<int>(a.data[2]) << '.'
           << static_cast<int>(a.data[3]);
        return os;
    }
    friend std::istream& operator>>(std::istream &is, ipv4 &a)
    {
        char d1, d2, d3;
        int b1, b2, b3, b4;
        is >> b1 >> d1 >> b2 >> d2 >> b3 >> d3 >> b4;
        if (d1 == '.' && d2 == '.' && d3 == '.')
            a = ipv4(b1, b2, b3, b4);
        else is.setstate(std::ios_base::failbit);
        return is;
    }
    ipv4& operator++(void)
    {
        *this = ipv4(1 + to_ulong());
        return *this;
    }
    ipv4& operator++(int)
    {
        ipv4 result(*this);
        ++(*this);
        return *this;
    }
    friend bool operator==(const ipv4 &a1, const ipv4 &a2) noexcept
    {
        return a1.data == a2.data;
    }
    friend bool operator!=(const ipv4 &a1, const ipv4 &a2) noexcept
    {
        return !(a1.data == a2.data);
    }
    friend bool operator<(const ipv4 &a1, const ipv4 &a2) noexcept
    {
        return a1.to_ulong() < a2.to_ulong();
    }
    friend bool operator>(const ipv4 &a1, const ipv4 &a2) noexcept
    {
        return a2 < a1;
    }
    friend bool operator<=(const ipv4 &a1, const ipv4 &a2) noexcept
    {
        return !(a1 > a2);
    }
    friend bool operator>=(const ipv4 &a1, const ipv4 &a2) noexcept
    {
        return !(a1 < a2);
    }
};

std::string solution(std::tuple<int, int, int, int> ab,
                     std::tuple<int, int, int, int> ae)
{
    ipv4 range_start(std::get<0>(ab), std::get<1>(ab),
                     std::get<2>(ab), std::get<3>(ab));
    ipv4 range_end(std::get<0>(ae), std::get<1>(ae),
                   std::get<2>(ae), std::get<3>(ae));
    std::ostringstream output;
    for (auto it = range_start; it <= range_end; ++it)
        output << "Address: " << it << '\n';
    return output.str();
}

// ############################################################################
// ############################################################################

void test(std::tuple<int, int, int, int> address_first,
          std::tuple<int, int, int, int> address_last,
          std::string expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::string result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution(address_first, address_last);
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem(address_first, address_last);
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] The range of IP addresses from '";
        std::cout << std::get<0>(address_first) << '.' << std::get<1>(address_first) << '.';
        std::cout << std::get<2>(address_first) << '.' << std::get<3>(address_first);
        std::cout << "' to address '";
        std::cout << std::get<0>(address_last) << '.' << std::get<1>(address_last) << '.';
        std::cout << std::get<2>(address_last) << '.' << std::get<3>(address_last);
        std::cout << "':\n" << result << "\nis different than the Book's expected";
        std::cout << " output:\n" << expected << '\n';
    }
}

int main(int argc, char * * argv)
{
    if ((argc > 1) && (std::strcmp(argv[1], "time") == 0))
    {
#if 1
        //constexpr unsigned int trials = 10'000'000;
        constexpr unsigned int trials = 10'000;
#else
        constexpr unsigned int trials = 1;
#endif
        std::locale::global(std::locale("de_CH.utf8"));
        fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
        std::vector<std::pair<std::tuple<int, int, int, int>,
                              std::tuple<int, int, int, int> > > addresses;
        addresses.push_back({{127, 0, 0, 5}, {127, 0, 0, 4}});
        addresses.push_back({{127, 0, 0, 5}, {127, 0, 0, 5}});
        addresses.push_back({{127, 0, 0, 5}, {127, 0, 0, 6}});
        addresses.push_back({{127, 0, 0, 5}, {127, 0, 1, 6}});
        addresses.push_back({{192, 168, 1, 1}, {192, 168, 1, 32}});
        const size_t n = addresses.size();
        std::vector<std::string> expected;
        for (auto [begin, end] : addresses)
            expected.push_back(solution(begin, end));
        
        fmt::print("[TEST] User's solution runtime...\n");
        auto time_problem_begin = std::chrono::high_resolution_clock::now();
        for (size_t i = 0; i < n; ++i)
        {
            auto [begin, end] = addresses[i];
            test(begin, end, expected[i], false, trials);
        }
        auto time_problem_end = std::chrono::high_resolution_clock::now();
        auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
        fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
        
        fmt::print("[TEST] Book's solution runtime...\n");
        auto time_solution_begin = std::chrono::high_resolution_clock::now();
        for (size_t i = 0; i < n; ++i)
        {
            auto [begin, end] = addresses[i];
            test(begin, end, expected[i], true, trials);
        }
        auto time_solution_end = std::chrono::high_resolution_clock::now();
        auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
        fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    }
    else
    {
        IPv4Information address_first, address_last;
        while (true)
        {
            std::cout << "Start IPv4 address: ";
            std::cin >> address_first;
            if (!std::cin.fail())
                break;
            std::cin.clear();
            std::cout << "IP address wrongly formatted.\n";
        }
        while (true)
        {
            std::cout << "End IPv4 address: ";
            std::cin >> address_last;
            if (!std::cin.fail())
                break;
            std::cin.clear();
            std::cout << "IP address wrongly formatted.\n";
        }
        std::cout << "Range of addresses in range " << address_first << '-';
        std::cout << address_last << ":\n";
        std::cout << problem(address_first.tuple(), address_last.tuple()) << '\n';
    }
    return 0;
}
