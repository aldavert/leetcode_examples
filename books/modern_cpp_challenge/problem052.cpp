#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>

/*****************************************************************************/
/* PROBLEM: Generating all the permutations of a string                      */
/* ------------------------------------------------------------------------- */
/* Write a function that, prints on the console all the possible             */
/* permutations of a given string. You should provide two versions of this   */
/* function: one that uses recursion, and one that does not.                 */
/*****************************************************************************/

std::vector<std::string> solution(std::string str)
{
    std::vector<std::string> result;
    std::sort(str.begin(), str.end());
    do
    {
        result.push_back(str);
    } while (std::next_permutation(str.begin(), str.end()));
    return result;
}

// ############################################################################
// ############################################################################

bool operator<(const std::vector<char> &left,
               const std::vector<char> &right)
{
    if (left.size() != right.size()) return false;
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (left[i] < right[i])
            return true;
    return false;
}

std::vector<std::string> iterativePermutation(std::string str)
{
    if (str.size() == 0) return {};
    const size_t n = str.size();
    std::vector<std::string> result = {std::string(1, str[0])};
    for (size_t i = 1; i < n; ++i)
    {
        std::unordered_set<std::string> unique;
        for (auto &current : result)
        {
            std::string next(i + 1, '\0');
            for (size_t p = 0; p <= i; ++p)
            {
                for (size_t j = 0, k = 0; j <= i; ++j)
                    next[j] = (p == j)?str[i]:current[k++];
                unique.insert(next);
            }
        }
        result.clear();
        result.reserve(unique.size());
        for (auto it = unique.begin(); it != unique.end();)
            result.push_back(std::move(unique.extract(it++).value()));
    }
    return result;
}

// ============================================================================

std::vector<std::string> recursivePermutation(std::string str)
{
    std::vector<std::string> result;
    std::unordered_set<std::string> unique;
    const size_t n = str.size();
    std::function<void(size_t, const std::string)> generate =
        [&](size_t position, const std::string &current) -> void
    {
        if (unique.find(current) != unique.end())
            return;
        else unique.insert(current);
        if (position == n)
            result.push_back(current);
        else
        {
            std::string next(position + 1, '\0');
            for (size_t p = 0; p <= position; ++p)
            {
                for (size_t j = 0, k = 0; j <= position; ++j)
                    next[j] = (p == j)?str[position]:current[k++];
                generate(position + 1, next);
            }
        }
    };
    generate(0, "");
    return result;
}

// ############################################################################
// ############################################################################

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::vector<T> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

template <typename T>
bool operator==(const std::vector<T> &left, const std::vector<T> &right)
{
    if (left.size() != right.size())
        return false;
    std::unordered_map<T, size_t> lut;
    for (const auto &l : left)
        ++lut[l];
    for (const auto &r : right)
    {
        if (auto search = lut.find(r); search != lut.end())
        {
            --(search->second);
            if (!search->second)
                lut.erase(search);
        }
        else return false;
    }
    return lut.empty();
}

int main(int /***argc*/, char * * /***argv*/)
{
    constexpr size_t trials = 10'000 - 1;
    std::locale::global(std::locale("de_CH.utf8"));
    std::vector<std::pair<std::string, std::vector<std::string> > > examples =
        { {"abc"  , solution("abc")  }, {"aaa", solution("aaa")        },
          {"abcd" , solution("abcd") }, {"abcde", solution("abcde")    },
          {"edcba", solution("edcba")}, {"cba", solution("cba")        },
          {"eecab", solution("eecab")}, {"abbddce", solution("abbddce")} };
    fmt::print("Each computation is repeated {} times.", trials + 1);
    for (const auto &[query, s] : examples)
    {
        fmt::print("Query: '{}'; Expected number of permutations: {}\n", query, s.size());
#if 1
        auto begin_sol = std::chrono::high_resolution_clock::now();
        auto s2 = s;
        for (size_t t = 0; t <= trials; ++t)
            s2 = iterativePermutation(query);
        auto time_sol = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::high_resolution_clock::now() - begin_sol).count();
        fmt::print("Solution (STD) computed in {:L} milliseconds ({:.3f} milliseconds per iteration).\n",
                time_sol,
                static_cast<double>(time_sol) / (1.0 + trials));
#endif
        fmt::print("Checking the iterative algorithm...\n");
        auto begin_iter = std::chrono::high_resolution_clock::now();
        auto result_iterative = iterativePermutation(query);
        for (size_t t = 0; t < trials; ++t)
            result_iterative = iterativePermutation(query);
        auto time_iter = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::high_resolution_clock::now() - begin_iter).count();
        fmt::print("[{}] Computed in {:L} milliseconds ({:.3f} milliseconds per iteration).\n",
                (result_iterative == s)?"CORRECT":"WRONG", time_iter,
                static_cast<double>(time_iter) / (1.0 + trials));
        
        fmt::print("Checking the recursive algorithm...\n");
        auto begin_recur = std::chrono::high_resolution_clock::now();
        auto result_recursive = recursivePermutation(query);
        for (size_t t = 0; t < trials; ++t)
            result_recursive = recursivePermutation(query);
        auto time_recur = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::high_resolution_clock::now() - begin_recur).count();
        fmt::print("[{}] Computed in {:L} milliseconds ({:.3f} milliseconds per iteration).\n",
                (result_recursive == s)?"CORRECT":"WRONG", time_iter,
                static_cast<double>(time_recur) / (1.0 + trials));
        fmt::print("\n");
    }
}

