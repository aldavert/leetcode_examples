#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>
#include <iomanip>
#include <unordered_set>
#include <algorithm>
#include <string_view>
#include <regex>
#include <optional>

/*****************************************************************************/
/* PROBLEM: Extracting URL parts                                             */
/* ------------------------------------------------------------------------- */
/* Write a function that, given a string that represents a URL, parses and   */
/* extracts the parts of the URL (protocol, domain, port, path, query, and   */
/* fragment).                                                                */
/*****************************************************************************/

// ############################################################################
// ############################################################################

// URI = scheme ":" ["//" authority] path ["?" query] ["#" fragment]
// authority = [userinfo "@"] host [":" port]
// C++ Regex expressions are very slow. Hana Dusikova's version will be much better (https://github.com/hanickadot/compile-time-regular-expressions)
struct URI
{
    std::string scheme;
    std::optional<std::string> userinfo;
    std::string host;
    std::optional<int> port;
    std::optional<std::string> path;
    std::optional<std::string> query;
    std::optional<std::string> fragment;
    bool operator==(const URI &other)
    {
        return (scheme == other.scheme)
            && (userinfo == other.userinfo)
            && (host == other.host)
            && (path == other.path)
            && (query == other.query)
            && (fragment == other.fragment);
    }
};

URI problem(std::string input)
{
    URI result;
    const size_t n = input.size();
    size_t pos = 0;
    
    auto retrieveAuthority = [&](void) -> void
    {
        ++pos;
        size_t start = ++pos;
        bool port = false;
        for (; pos < n; ++pos)
        {
            if (input[pos] == '@')
            {
                result.userinfo = input.substr(start, pos - start);
                start = ++pos;
            }
            else if (input[pos] == ':')
            {
                result.host = input.substr(start, pos - start);
                start = ++pos;
                port = true;
            }
            else if (input[pos] == '/')
                break;
        }
        if (port) result.port = std::stoi(input.substr(start, pos - start));
        else result.host = input.substr(start, pos - start);
    };
    
    // Retrieve scheme ........................................................
    while ((pos < n) && (input[pos] != ':')) ++pos;
    result.scheme = input.substr(0, pos);
    ++pos; // Has to be a /
    if (pos >= n) return {};
    
    // It's authority or path .................................................
    if (input[pos] == '/')
        retrieveAuthority();
    size_t start = pos;
    while ((pos < n) && (input[pos] != '?') && (input[pos] != '#')) ++pos;
    if (pos > start)
        result.path = input.substr(start, pos - start);
    // Query ..................................................................
    if (input[pos] == '?')
    {
        start = ++pos;
        while ((pos < n) && (input[pos] != '#')) ++pos;
        result.query = input.substr(start, pos - start);
    }
    // Fragment ...............................................................
    if (input[pos] == '#')
    {
        start = ++pos;
        while (pos < n) ++pos;
        result.fragment = input.substr(start, pos - start);
    }
    
    return result;
}

// ############################################################################
// ############################################################################

URI solution(std::string uri)
{
    std::regex rx(R"(^(\w+):\/\/([\w.-]+)(:(\d+))?([\w\/\.]+)?(\?([\w=&]*)(#?(\w+))?)?$)");
    auto matches = std::smatch{};
    if (std::regex_match(uri, matches, rx))
    {
        if (matches[1].matched && matches[2].matched)
        {
            URI parts;
            parts.scheme = matches[1].str();
            parts.host = matches[2].str();
            if (matches[4].matched)
                parts.port = std::stoi(matches[4]);
            if (matches[5].matched)
                parts.path = matches[5];
            if (matches[7].matched)
                parts.query = matches[7];
            if (matches[9].matched)
                parts.fragment = matches[9];
            return parts;
        }
    }
    return {};
}

// ############################################################################
// ############################################################################

std::ostream& operator<<(std::ostream &out, const URI &uri)
{
    out << '{';
    out << "Scheme: '" << uri.scheme;
    if (uri.userinfo.has_value()) out << "', Userinfo: '" << uri.userinfo.value();
    out << "', Host: '" << uri.host;
    if (uri.port.has_value()) out << "', Port: '" << uri.port.value();
    if (uri.path.has_value()) out << "', Path: '" << uri.path.value();
    if (uri.query.has_value()) out << "', Query: '" << uri.query.value();
    if (uri.fragment.has_value()) out << "', Fragment: '" << uri.fragment.value();
    out << "'}";
    return out;
}

void test(std::string input,
          URI expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    URI result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution(input);
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem(input);
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] The URI retrieved:";
        std::cout << '\n' << result << "\nis different than the Book's expected";
        std::cout << " output:\n" << expected << '\n';
    }
}

int main(int argc, char * * argv)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000;
#else
    constexpr unsigned int trials = 1;
#endif
    std::locale::global(std::locale("de_CH.utf8"));
    fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
    std::vector<std::tuple<std::string, URI> > examples;
    {
        URI expected;
        expected.scheme = "https";
        expected.host = "packt.com";
        examples.push_back({"https://packt.com", expected});
    }
    {
        URI expected;
        expected.scheme = "https";
        expected.host = "bbc.com";
        expected.port = 80;
        expected.path = "/en/index.html";
        expected.query = "lite=true";
        expected.fragment = "ui";
        examples.push_back({"https://bbc.com:80/en/index.html?lite=true#ui", expected});
    }
    
    fmt::print("[TEST] User's solution runtime...\n");
    auto time_problem_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[input, expected] : examples)
        test(input, expected, false, trials);
    auto time_problem_end = std::chrono::high_resolution_clock::now();
    auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
    
    fmt::print("[TEST] Book's solution runtime...\n");
    auto time_solution_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[input, expected] : examples)
        test(input, expected, true, trials);
    auto time_solution_end = std::chrono::high_resolution_clock::now();
    auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    return 0;
}
