#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>

/*****************************************************************************/
/* PROBLEM: Converting numerical values to Roman                             */
/* ------------------------------------------------------------------------- */
/* Write a program that, given a number entered by the user, prints its      */
/* Roman numeral equivalent.                                                 */
/*****************************************************************************/

// ############################################################################
// ############################################################################

std::string problem(unsigned int number)
{
    std::unordered_map<unsigned int, std::string> roman = {
        {1, "I"}, {5, "V"}, {10, "X"}, {50, "L"}, {100, "C"}, {500, "D"}, {1000, "M"}
    };
    std::string result;
    if ((number >= 1) && (number < 3000))
    {
        for (int divisor = 1'000; divisor >= 1; divisor /= 10)
        {
            unsigned int value = number / divisor;
            number -= value * divisor;
            if ((value >= 1) && (value <= 9))
            {
                if (value == 9) result += roman[divisor] + roman[divisor * 10];
                else
                {
                    if (value == 4)
                        result += roman[divisor] + roman[divisor * 5];
                    else
                    {
                        if (value >= 5)
                        {
                            result += roman[divisor * 5];
                            value -= 5;
                        }
                        for (unsigned int r = 0; r < value; ++r)
                            result += roman[divisor];
                    }
                }
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

std::string solution(unsigned int number)
{
    std::vector<std::pair<unsigned int, const char *> > roman {
        {1000, "M"}, {900, "CM"}, {500, "D"}, {400, "CD"}, {100, "C"}, {90, "XC"},
        {50, "L"}, {40, "XL"}, {10, "X"}, {9, "IX"}, {5, "V"}, {4, "IV"}, {1, "I"}};
    std::string result;
    for (const auto &kpv : roman)
    {
        while (number >= kpv.first)
        {
            result += kpv.second;
            number  -= kpv.first;
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

void test(unsigned int number,
          std::string expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::string result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution(number);
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem(number);
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] The Roman value conversion of '" << number;
        std::cout << "' is expected to be:\n" << expected;
        std::cout << "\n However, the results obtained are:\n" << result << '\n';
    }
}

int main(int argc, char * * argv)
{
    if ((argc > 1) && (std::strcmp(argv[1], "time") == 0))
    {
#if 1
        //constexpr unsigned int trials = 10'000'000;
        constexpr unsigned int trials = 10'000;
#else
        constexpr unsigned int trials = 1;
#endif
        std::locale::global(std::locale("de_CH.utf8"));
        auto time_problem_begin = std::chrono::high_resolution_clock::now();
        fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
        fmt::print("[TEST] Generating all Roman numerals from 1 to 3'000.\n");
                
        std::vector<unsigned long> test_numbers(3'000);
        for (size_t i = 1; i <= 3'000; ++i)
            test_numbers[i] = i;
        
        fmt::print("[TEST] User's solution runtime...\n");
        for (unsigned int number : test_numbers)
            test(number, solution(number), false, trials);
        auto time_problem_end = std::chrono::high_resolution_clock::now();
        auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
        fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
        
        auto time_solution_begin = std::chrono::high_resolution_clock::now();
        fmt::print("[TEST] Book's solution runtime...\n");
        for (unsigned int number : test_numbers)
            test(number, solution(number), true, trials);
        auto time_solution_end = std::chrono::high_resolution_clock::now();
        auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
        fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    }
    else
    {
        unsigned int number = 0;
        do
        {
            std::cout << "Input number (1-3000): ";
            std::cin >> number;
        } while ((number < 1) || (number > 3'000));
        auto result = problem(number);
        std::cout << "The Roman representation of '" << number << "' is: " << result;
        if (auto sol = solution(number); sol == result)
            std::cout << "\n[CORRECT]\n";
        else std::cout << "\n[FAILURE] The expected solution is: " << sol << '\n';
    }
    return 0;
}
