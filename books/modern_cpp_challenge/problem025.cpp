#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>
#include <iomanip>
#include <unordered_set>

/*****************************************************************************/
/* PROBLEM: Capitalizing an article title                                    */
/* ------------------------------------------------------------------------- */
/* Write a function that transforms an input text into a capitalized         */
/* version, where every word starts with an uppercase letter and has all     */
/* other letters in lowercase. For instance, the text "the c++ challenger"   */
/* should be transformed to "The C++ Challenger".                            */
/*****************************************************************************/

// ############################################################################
// ############################################################################

template <typename T>
auto problem(std::basic_string<T> input)
{
    for (bool word_begin = true; auto &c : input)
    {
        word_begin = word_begin || std::ispunct(c) || std::isspace(c);
        if (std::isalpha(c))
        {
            if (word_begin) [[unlikely]]
            {
                c = std::toupper(c);
                word_begin = false;
            }
            else c = std::tolower(c);
        }
        //else word_begin = true;
    }
    return input;
}

// ############################################################################
// ############################################################################

template <class Elem>
using tstring = std::basic_string<Elem, std::char_traits<Elem>, std::allocator<Elem> >;
template <class Elem>
using tstringstream = std::basic_stringstream<Elem, std::char_traits<Elem>, std::allocator<Elem> >;

template <class Elem>
tstring<Elem> solution(const tstring<Elem> &text)
{
    tstringstream<Elem> result;
    bool newWord = true;
    for (const auto ch : text)
    {
        newWord = newWord || std::ispunct(ch) || std::isspace(ch);
        if (std::isalpha(ch))
        {
            if (newWord)
            {
                result << static_cast<Elem>(std::toupper(ch));
                newWord = false;
            }
            else result << static_cast<Elem>(std::tolower(ch));
        }
        else result << ch;
    }
    return result.str();
}

// ############################################################################
// ############################################################################

void test(std::string input,
          std::string expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::string result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution(input);
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem(input);
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] The capitalized string:";
        std::cout << '\n' << result << "\nis different than the Book's expected";
        std::cout << " output:\n" << expected << '\n';
    }
}

int main(int argc, char * * argv)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 10'000;
#else
    constexpr unsigned int trials = 1;
#endif
    std::locale::global(std::locale("de_CH.utf8"));
    fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
    std::vector<std::pair<std::string, std::string> > examples;
    std::vector<std::string> separators = {" ", " ", " ", " ", " ", " ", " ", " ", " ",
                                           ", ", ". ", ",", ".", "?", "!", " ", " "};
    std::string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist_values(0, characters.size() - 1);
    std::uniform_int_distribution<> dist_length(1, 8);
    std::uniform_int_distribution<> dist_word(4, 15);
    std::uniform_int_distribution<> dist_sep(0, separators.size() - 1);
    for (unsigned int i = 0; i < 200; ++i)
    {
        std::string current;
        for (int i = 0, nw = dist_word(gen); i < nw; ++i)
        {
            if (i > 0) current += separators[dist_sep(gen)];
            for (int j = 0, l = dist_length(gen); j < l; ++j)
                current += static_cast<char>(characters[dist_values(gen)]);
        }
        examples.push_back({current, solution(current)});
    }
    
    fmt::print("[TEST] User's solution runtime...\n");
    auto time_problem_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[input, expected] : examples)
        test(input, expected, false, trials);
    auto time_problem_end = std::chrono::high_resolution_clock::now();
    auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
    
    fmt::print("[TEST] Book's solution runtime...\n");
    auto time_solution_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[input, expected] : examples)
        test(input, expected, true, trials);
    auto time_solution_end = std::chrono::high_resolution_clock::now();
    auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    return 0;
}
