#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>
#include <iomanip>
#include <unordered_set>
#include <algorithm>
#include <string_view>
#include <regex>
#include <optional>

/*****************************************************************************/
/* PROBLEM: Transforming dates in string                                     */
/* ------------------------------------------------------------------------- */
/* Write a function that, given a text containing dates in the format        */
/* dd.mm.yyyy or dd-mm-yyyy, transforms to text so that it contains dates in */
/* format yyyy-mm-dd.                                                        */
/*****************************************************************************/

// ############################################################################
// ############################################################################

// C++ Regex expressions are very slow. Hana Dusikova's version will be much better (https://github.com/hanickadot/compile-time-regular-expressions)
std::string problem(std::string input)
{
    const size_t n = input.size();
    for (size_t i = 0; i < n - 10; ++i)
    {
        if (std::isdigit(input[i + 0])
        &&  std::isdigit(input[i + 1])
        &&  ((input[i + 2] == '.') || (input[i + 2] == '-'))
        &&  std::isdigit(input[i + 3])
        &&  std::isdigit(input[i + 4])
        &&  ((input[i + 5] == '.') || (input[i + 5] == '-'))
        &&  std::isdigit(input[i + 6])
        &&  std::isdigit(input[i + 7])
        &&  std::isdigit(input[i + 8])
        &&  std::isdigit(input[i + 9]))
        {
            std::string date = input.substr(i + 6, 4)
                             + "-" + input.substr(i + 3, 2)
                             + "-" + input.substr(i, 2);
            input.replace(i, 10, date);
            i += 9;
        }
    }
    return input;
}

// ############################################################################
// ############################################################################

std::string solution(std::string text)
{
    auto rx = std::regex{ R"((\d{1,2})(\.|-|/)(\d{1,2})(\.|-|/)(\d{4}))" };
    return std::regex_replace(text.data(), rx, R"($5-$3-$1)");
}

// ############################################################################
// ############################################################################

void test(std::string input,
          std::string expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::string result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution(input);
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem(input);
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] The text retrieved:";
        std::cout << '\n' << result << "\nis different than the Book's expected";
        std::cout << " output:\n" << expected << '\n';
    }
}

int main(int argc, char * * argv)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000;
#else
    constexpr unsigned int trials = 1;
#endif
    std::locale::global(std::locale("de_CH.utf8"));
    fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
    std::vector<std::tuple<std::string, std::string> > examples;
    examples.push_back({"today is 01.12.2017!", "today is 2017-12-01!"});
    examples.push_back({"today is 01-12-2017!", "today is 2017-12-01!"});
    examples.push_back({"today is between 01.04.2022-03-04-2022!",
                        "today is between 2022-04-01-2022-04-03!"});
    examples.push_back({"today is between 01.04.202203-04-2022!",
                        "today is between 2022-04-012022-04-03!"});
    
    fmt::print("[TEST] User's solution runtime...\n");
    auto time_problem_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[input, expected] : examples)
        test(input, expected, false, trials);
    auto time_problem_end = std::chrono::high_resolution_clock::now();
    auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
    
    fmt::print("[TEST] Book's solution runtime...\n");
    auto time_solution_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[input, expected] : examples)
        test(input, expected, true, trials);
    auto time_solution_end = std::chrono::high_resolution_clock::now();
    auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    return 0;
}
