#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

/*****************************************************************************/
/* PROBLEM: Large prime smaller than given number.                           */
/* ------------------------------------------------------------------------- */
/* Write a program that computes and prints the largest prime number that is */
/* smaller than a number provided by the user, which must be a positive      */
/* integer.                                                                  */
/*****************************************************************************/

// ############################################################################
// ############################################################################

unsigned int problem(unsigned int limit)
{
    if (limit <= 1) return 0;
    if (limit <= 3) return limit;
    for (int i = limit - ((limit & 1) == 0); i > 1; i -= 2)
    {
        bool is_prime = true;
        for (int j = 3; j * j <= i; j += 2)
            if (!(is_prime = (i % j != 0)))
                break;
        if (is_prime) return i;
    }
    return 0;
}

// ############################################################################
// ############################################################################

unsigned int solution(unsigned int limit)
{
    auto is_prime = [](int value) -> bool
    {
        if (value <= 3) return value > 1;
        else if ((value & 1) == 0) return false;
        else if (value % 3 == 0) return false;
        for (int i = 5; i * i <= value; i += 6)
            if ((value % i == 0) || (value % (i + 2) == 0))
                return false;
        return true;
    };
    for (int i = limit; i > 1; --i)
        if (is_prime(i))
            return i;
    return 0;
}

// ############################################################################
// ############################################################################

void test(unsigned int limit, unsigned int solution, unsigned int trials = 1)
{
    unsigned int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = problem(limit);
    if (solution != result)
    {
        std::cout << "[FAILURE] largest prime under '" << limit;
        std::cout << "' is expected to be " << solution << ", but ";
        std::cout << result << " is obtained instead.\n";
    }
}

int main(int argc, char * * argv)
{
    if ((argc > 1) && (std::strcmp(argv[1], "time") == 0))
    {
#if 1
        constexpr unsigned int trials = 10'000'000;
        //constexpr unsigned int trials = 1'000'000;
#else
        constexpr unsigned int trials = 1;
#endif
        for (unsigned int l = 1; l < 1'000'000; l += 1'000)
            test(l, solution(l), trials);
    }
    else
    {
        unsigned int limit = 0;
        std::cout << "Input number: ";
        std::cin >> limit;
        auto result = problem(limit);
        std::cout << "Largest prime under " << limit << " is = " << result;
        if (auto sol = solution(limit); sol == result)
            std::cout << " (correct)\n";
        else std::cout << " (" << sol << " was expected)\n";
    }
    return 0;
}
