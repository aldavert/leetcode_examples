#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>

/*****************************************************************************/
/* PROBLEM: Amicable numbers.                                                */
/* ------------------------------------------------------------------------- */
/* Write a program that prints the list of all pairs of amicable numbers     */
/* smaller than 1'000'000.                                                   */
/*****************************************************************************/

// ############################################################################
// ############################################################################

std::vector<std::pair<unsigned int, unsigned int> > problem(void)
{
    const unsigned int limit = 1'000'000;
    std::vector<std::pair<unsigned int, unsigned int> > result;
    std::vector<unsigned int> proper_sum(limit + 1);
    proper_sum[0] = 0;
    for (unsigned int i = 1; i < limit; ++i)
    {
        unsigned int sum = 1
                         + ((i & 1) == 0) * (2 + (2 != i / 2) * (i / 2))
                         + ((i % 3) == 0) * (3 + (3 != i / 3) * (i / 3));
        for (unsigned int j = 4; j * j <= i; ++j)
            if (i % j == 0)
                sum += j + (j != i / j) * (i / j);
        proper_sum[i] = sum;
    }
    for (unsigned int i = 1; i <= limit; ++i)
        if ((proper_sum[i] <= limit)
         && (proper_sum[i] != i)
         && (proper_sum[proper_sum[i]] == i)
         && (i < proper_sum[i]))
            result.push_back({i, proper_sum[i]});
    return result;
}

// ############################################################################
// ############################################################################

std::vector<std::pair<unsigned int, unsigned int> > solution(void)
{
    const unsigned int limit = 1'000'000;
    std::vector<std::pair<unsigned int, unsigned int> > result;
    auto sum_proper_divisors = [](unsigned int number) -> unsigned int
    {
        unsigned int result = 1;
        for (unsigned int i = 2; i <= std::sqrt(number); ++i)
            if (number % i == 0)
                result += (i == (number / i))?i:(i + number / i);
        return result;
    };
    for (unsigned int number = 4; number < limit; ++number)
    {
        auto sum1 = sum_proper_divisors(number);
        if (sum1 < limit)
        {
            auto sum2 = sum_proper_divisors(sum1);
            if ((sum2 == number) && (number != sum1) && (number < sum1))
                result.push_back({number, sum1});
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

std::ostream& operator<<(std::ostream &out,
                         const std::vector<std::pair<unsigned int, unsigned int> > &vec)
{
    out << '{';
    for (bool test = false; const auto &v : vec)
    {
        if (test) [[likely]] out << ", ";
        test = true;
        out << '{' << v.first << ", " << v.second << '}';
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<std::pair<unsigned int, unsigned int> > &left,
                const std::vector<std::pair<unsigned int, unsigned int> > &right)
{
    if (left.size() != right.size()) return false;
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

void test(const std::vector<std::pair<unsigned int, unsigned int> > &expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::vector<std::pair<unsigned int, unsigned int> > result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution();
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem();
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] Amicable numbers under 1'000'000 are expected to be:\n";
        std::cout << expected << "\n However, the results obtained are:\n";
        std::cout << result << '\n';
    }
}

int main(int argc, char * * argv)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100;
#else
    constexpr unsigned int trials = 1;
#endif
    std::locale::global(std::locale("de_CH.utf8"));
    auto time_problem_begin = std::chrono::high_resolution_clock::now();
    std::cout << "Testing the runtime of the user's solution...\n";
    test(solution(), false, trials);
    auto time_problem_end = std::chrono::high_resolution_clock::now();
    auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
    
    auto time_solution_begin = std::chrono::high_resolution_clock::now();
    std::cout << "Testing the runtime of the book's solution...\n";
    test(solution(), true, trials);
    auto time_solution_end = std::chrono::high_resolution_clock::now();
    auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    return 0;
}
