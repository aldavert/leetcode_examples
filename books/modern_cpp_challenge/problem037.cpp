#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <regex>

/*****************************************************************************/
/* PROBLEM: Finding files in a directory that match a regular expression     */
/* ------------------------------------------------------------------------- */
/* Write a function that, given the path to a directory and a regular        */
/* expression, returns a list of all the directory entries whose names match */
/* the regular expression.                                                   */
/*****************************************************************************/

// ############################################################################
// ############################################################################

std::vector<std::filesystem::directory_entry> problem(
        std::filesystem::path path, std::string regex)
{
    std::vector<std::filesystem::directory_entry> result;
    std::regex rx(regex.data());
    for (const auto &entry : std::filesystem::recursive_directory_iterator(path))
        if ((entry.is_regular_file())
        && (std::regex_match(entry.path().filename().string(), rx)))
            result.push_back(entry);
    return result;
}

// ############################################################################
// ############################################################################

std::vector<std::filesystem::directory_entry> solution(
        std::filesystem::path path, std::string regex)
{
    std::vector<std::filesystem::directory_entry> result;
    std::regex rx(regex.data());
    std::copy_if(
            std::filesystem::recursive_directory_iterator(path),
            std::filesystem::recursive_directory_iterator(),
            std::back_inserter(result),
            [&rx](std::filesystem::directory_entry const &entry)
            {
                return std::filesystem::is_regular_file(entry.path()) &&
                       std::regex_match(entry.path().filename().string(), rx);
            });
    return result;
}

// ############################################################################
// ############################################################################

int main(int argc, char * * argv)
{
    if (argc < 3)
    {
        fmt::print("[ERROR] No path to folder passed as parameter.\n");
        fmt::print("[ERROR] Format: ./problem037 (<path> <regex>)+\n");
        return 1;
    }
    auto to_string = [](std::filesystem::file_time_type const& ftime) -> std::string
    {
        std::time_t cftime = std::chrono::system_clock::to_time_t(
            std::chrono::file_clock::to_sys(ftime));
        std::string str = std::asctime(std::localtime(&cftime));
        str.pop_back();
        return str;
    };
    std::locale::global(std::locale("de_CH.utf8"));
    for (int i = 1; i + 1 < argc; i += 2)
    {
        std::filesystem::path p = argv[i];
        fmt::print("Processing path='{}' with REGEX='{}'.\n", p.string(), argv[i + 1]);
        try
        {
            fmt::print("{:<20} {:>10} {:>27}\n", "Filename", "Size", "Last Write");
            fmt::print("------------------------------------------------------------\n");
            for (const auto &entry : problem(p, argv[i + 1]))
                fmt::print("{:<20} {:>8}Kb {:>27}\n",
                        entry.path().string(),
                        entry.file_size() / 1024,
                        to_string(entry.last_write_time()));
            fmt::print("\n");
        }
        catch (...)
        {
            fmt::print("Could not process '{}' for [OWN].\n", argv[i]);
        }
        try
        {
            fmt::print("{:<20} {:>10} {:>27}\n", "Filename", "Size", "Last Write");
            fmt::print("------------------------------------------------------------\n");
            for (const auto &entry : solution(p, argv[i + 1]))
                fmt::print("{:<20} {:>8}Kb {:>27}\n",
                        entry.path().string(),
                        entry.file_size() / 1024,
                        to_string(entry.last_write_time()));
            fmt::print("\n");
        }
        catch (...)
        {
            fmt::print("Could not process '{}' for [BOOK].\n", argv[i]);
        }
    }
    
    return 0;
}
