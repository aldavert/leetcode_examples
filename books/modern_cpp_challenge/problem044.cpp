#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>

/*****************************************************************************/
/* PROBLEM: Monthly calendar                                                 */
/* ------------------------------------------------------------------------- */
/* Write a function that, given a year and a month, prints to the console    */
/* the month calendar.                                                       */
/*****************************************************************************/

using namespace std::literals::chrono_literals;
using std::chrono::January;
using std::chrono::February;
using std::chrono::March;
using std::chrono::April;
using std::chrono::May;
using std::chrono::June;
using std::chrono::July;
using std::chrono::August;
using std::chrono::September;
using std::chrono::October;
using std::chrono::November;
using std::chrono::December;

// ############################################################################
// ############################################################################

void drawCalendar(std::chrono::year y, std::chrono::month m)
{
    auto first = std::chrono::sys_days{std::chrono::year_month_day(y, m, 1d)};
    auto last = first + std::chrono::months(1);
    int day_week = std::chrono::weekday{first}.iso_encoding() - 1;
    static constexpr char months[][4] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    
    fmt::print("Year: {:4d}       Month: {}\n", static_cast<int>(y), months[static_cast<unsigned int>(m) - 1]);
    fmt::print("Mon Tue Wed Thu Fri Sat Sun\n");
    fmt::print("{:<{}}", ' ', 4 * day_week);
    for (unsigned int counter = 1; first <= last; first += std::chrono::days(1), ++day_week, ++counter)
    {
        if (day_week == 7)
        {
            day_week = 0;
            fmt::print("\n");
        }
        fmt::print(" {:2d} ", counter);
    }
    fmt::print("\n");
}

// ############################################################################
// ############################################################################

int main(int argc, char * * argv)
{
    int month = 5, year = 2022;
    std::cout << "Year: ";
    std::cin >> year;
    std::cout << "Month (1-12): ";
    std::cin >> month;
    drawCalendar(std::chrono::year(year), std::chrono::month(month));
}

