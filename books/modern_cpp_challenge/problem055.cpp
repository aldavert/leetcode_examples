#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>

/*****************************************************************************/
/* PROBLEM: Zip algorithm                                                    */
/* ------------------------------------------------------------------------- */
/* Write a function that, given two ranges, returns a new range with pairs   */
/* of elements from two ranges. Should the two ranges have different sizes,  */
/* the result must contain as many elements as the smallest of the input     */
/* ranges. For example, if the ranges were {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}   */
/* and {1, 1, 3, 5, 8, 13, 21}, the result should be {{1, 1}, {2, 1},        */
/* {3, 3}, {4, 5}, {5, 8}, {6, 13}, {7, 21}}.                                */
/*****************************************************************************/

// ############################################################################
// ############################################################################

template <typename TA, template <typename> typename RANGEA,
          typename TB, template <typename> typename RANGEB>
requires std::ranges::range<RANGEA<TA> > && std::ranges::range<RANGEB<TB> >
RANGEA<std::pair<TA, TB> > solution(const RANGEA<TA> &r_a, const RANGEB<TB> &r_b)
{
    RANGEA<std::pair<TA, TB> > result;
    size_t n = std::min(r_a.size(), r_b.size());
    for (auto b_a = r_a.begin(), b_b = r_b.begin(); n > 0; --n, ++b_a, ++b_b)
        result.push_back({*b_a, *b_b});
    return result;
}

// ############################################################################
// ############################################################################

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::pair<T, T> &tup)
{
    out << '{' << tup.first << ", " << tup.second << '}';
    return out;
}

template <typename T, template <typename> typename RANGE>
requires std::ranges::range<RANGE<T> >
         && (!std::same_as<RANGE<void>, std::basic_string<void> >)
std::ostream& operator<<(std::ostream &out, const RANGE<T> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

int main(int /***argc*/, char * * /***argv*/)
{
    std::vector<int> v{1, 1, 3, 5, 8, 13, 21};
    std::list<int> l{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::cout << "Vector: " << v << '\n';
    std::cout << "List: " << l << '\n';
    std::cout << "Zip vector-list: " << solution(v, l) << '\n';
    std::cout << "Zip list-vector: " << solution(l, v) << '\n';
    return 0;
}

