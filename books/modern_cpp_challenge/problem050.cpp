#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>

/*****************************************************************************/
/* PROBLEM: Filtering a list of phone numbers                                */
/* ------------------------------------------------------------------------- */
/* Write a program that, given a list of phone numbers, returns only the     */
/* numbers that are from a specified country. The country is indicated by    */
/* its phone country code, such as 44 for Great Britain. Phone numbers may   */
/* start with the country code, a + followed by the country code, or have no */
/* country code. The ones from this last category must be ignored.           */
/*****************************************************************************/

// ############################################################################
// ############################################################################

std::vector<std::string> filterPhonesNumbers(
        const std::vector<std::string> &numbers,
        std::string code)
{
    std::string to_remove = " +";
    std::vector<std::string> result;
    code.erase(0, code.find_first_not_of(to_remove));
    for (const std::string &number : numbers)
    {
        std::string trim = number;
        trim.erase(0, trim.find_first_not_of(to_remove));
        if (trim.size() <= 2) continue;
        if (trim.substr(0, code.size()) == code)
            result.push_back(number);
    }
    return result;
}

// ############################################################################
// ############################################################################


int main(int /***argc*/, char * * /***argv*/)
{
    std::vector<std::string> numbers = {"+40744909080", "44 7520 112233",
        "+44 7555 123456", "40 7200 123456", "7555 123456" };
    auto result = filterPhonesNumbers(numbers, "44");
    fmt::print("List of filtered numbers:\n");
    fmt::print("-------------------------------------\n");
    for (const auto &r : result)
        fmt::print("{}\n", r);
    fmt::print("-------------------------------------\n\n");
    return 0;
}

