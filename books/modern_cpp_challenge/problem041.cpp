#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>

/*****************************************************************************/
/* PROBLEM: Day of the week                                                  */
/* ------------------------------------------------------------------------- */
/* Write a function that, given a date, determines the day of the week. This */
/* function should return a value between 1 (for Monday) and 7 (for Sunday). */
/*****************************************************************************/

using namespace std::literals::chrono_literals;
using std::chrono::January;
using std::chrono::February;
using std::chrono::March;
using std::chrono::April;
using std::chrono::May;
using std::chrono::June;
using std::chrono::July;
using std::chrono::August;
using std::chrono::September;
using std::chrono::October;
using std::chrono::November;
using std::chrono::December;
std::ostream& operator<<(std::ostream &out, const std::chrono::year_month_day &ymd)
{
    out << static_cast<int>(ymd.year()) << '/'
        << std::setw(2) << std::setfill('0')
        << static_cast<unsigned>(ymd.month()) << '/'
        << std::setw(2) << static_cast<unsigned>(ymd.day());
    return out;
}

// ############################################################################
// ############################################################################

unsigned int problem(std::chrono::year_month_day dayA)
{
    //if (dayA > dayB)
    //    return (std::chrono::sys_days{dayA} - std::chrono::sys_days{dayB}).count();
    //else
    //    return (std::chrono::sys_days{dayB} - std::chrono::sys_days{dayA}).count();

    std::chrono::weekday weekday{std::chrono::sys_days{dayA}};
    return weekday.iso_encoding();
}

unsigned int problem(int yearA, unsigned int monthA, unsigned int dayA)
{
    return problem(std::chrono::year_month_day{std::chrono::year{yearA}, std::chrono::month{monthA}, std::chrono::day{dayA}});
}

unsigned int problem(std::chrono::year yearA, std::chrono::month monthA, std::chrono::day dayA)
{
    return problem(std::chrono::year_month_day{yearA, monthA, dayA});
}

// ############################################################################
// ############################################################################

int main(int argc, char * * argv)
{
    std::string day_name[8] = { "", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
    std::chrono::year_month_day dayA{1981y/April/30d}, dayB{2022y/May/12d};
    std::chrono::time_point<std::chrono::system_clock> now{std::chrono::system_clock::now()};
    std::chrono::year_month_day today{std::chrono::floor<std::chrono::days>(now)};
    std::cout << "Today " << today << " is " << day_name[problem(today)] << ".\n";
    std::cout << dayA << " was " << day_name[problem(today)] << ".\n";
    std::cout << dayB << " was " << day_name[problem(today)] << ".\n";
    std::cout << "1981/04/30 was " << day_name[problem(1981, 4, 30)] << ".\n";
    std::cout << "1981/12/29 was " << day_name[problem(1981, 12, 29)] << ".\n";
    std::cout << "1975/09/11 was " << day_name[problem(1975y, September, 11d)] << ".\n";
    std::cout << "1948/03/11 was " << day_name[problem(1948y, March, 11d)] << ".\n";
    std::cout << "1948/12/09 was " << day_name[problem(1948y, December, 9d)] << ".\n";
}

