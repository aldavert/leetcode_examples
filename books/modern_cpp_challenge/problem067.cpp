#include <vector>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>
#include <bitset>

/*****************************************************************************/
/* PROBLEM: Validating passwords                                             */
/* ------------------------------------------------------------------------- */
/* Write a program that validates password strength based on predefined      */
/* rules, which may then be selected in various combinations. At a minimum,  */
/* every password must meet a minimum length requirement. In addition, other */
/* rules could be enforced, such as the presence of at least one symbol,     */
/* digit, uppercase and lowercase letter, and so on.                         */
/*****************************************************************************/

// ############################################################################
// ############################################################################

class IValidatePWD
{
public:
    virtual ~IValidatePWD(void) = default;
    virtual bool valid(std::string password) const noexcept = 0;
};

class ValidatePWD : public IValidatePWD
{
public:
    ValidatePWD(size_t minimum_length) :
        m_minimum_length(minimum_length) {}
    bool valid(std::string password) const noexcept override
    {
        return password.size() >= m_minimum_length;
    }
private:
    size_t m_minimum_length = 0;
};

class UppercasePWD : public IValidatePWD
{
public:
    UppercasePWD(const IValidatePWD &validate) :
        m_wrapper(validate) {}
    bool valid(std::string password) const noexcept override
    {
        return m_wrapper.valid(password)
            && std::any_of(password.begin(), password.end(), ::isupper);
    }
private:
    const IValidatePWD &m_wrapper;
};

class LowercasePWD : public IValidatePWD
{
public:
    LowercasePWD(const IValidatePWD &validate) :
        m_wrapper(validate) {}
    bool valid(std::string password) const noexcept override
    {
        return m_wrapper.valid(password)
            && std::any_of(password.begin(), password.end(), ::islower);
    }
private:
    const IValidatePWD &m_wrapper;
};

class DigitPWD : public IValidatePWD
{
public:
    DigitPWD(const IValidatePWD &validate) :
        m_wrapper(validate) {}
    bool valid(std::string password) const noexcept override
    {
        return m_wrapper.valid(password)
            && std::any_of(password.begin(), password.end(), ::isdigit);
    }
private:
    const IValidatePWD &m_wrapper;
};

class SymbolPWD : public IValidatePWD
{
public:
    SymbolPWD(const IValidatePWD &validate) :
        m_wrapper(validate) {}
    bool valid(std::string password) const noexcept override
    {
        return m_wrapper.valid(password)
            && std::any_of(password.begin(), password.end(), ::ispunct);
    }
private:
    const IValidatePWD &m_wrapper;
};

// ############################################################################
// ############################################################################

int main(int /***argc*/, char * * /***argv*/)
{
    std::vector<std::string> passwords = { "jfljkenv", "avke", "af3jfl3g", "jfSKNvdk",
                                           "df.3GB!!", "123456", "ABDKF", "ADBK44GBB",
                                           "341958393" };
    fmt::print("Is password X valid?\n");
    fmt::print("   Password | LEN | UPP | LOW | DIG | SYM | U&L | ULD | ALL \n");
    fmt::print("------------+-----+-----+-----+-----+-----+-----+-----+-----\n");
    for (std::string password : passwords)
    {
        fmt::print("{:>11} | {:^3} | {:^3} | {:^3} | {:^3} | {:^3} | {:^3} | {:^3} | {:^3}\n",
                password,
                ValidatePWD(8).valid(password)?"YES":"NO",
                UppercasePWD(ValidatePWD(8)).valid(password)?"YES":"NO",
                LowercasePWD(ValidatePWD(8)).valid(password)?"YES":"NO",
                DigitPWD(ValidatePWD(8)).valid(password)?"YES":"NO",
                SymbolPWD(ValidatePWD(8)).valid(password)?"YES":"NO",
                LowercasePWD(UppercasePWD(ValidatePWD(8))).valid(password)?"YES":"NO",
                DigitPWD(LowercasePWD(UppercasePWD(ValidatePWD(8)))).valid(password)?"YES":"NO",
                SymbolPWD(DigitPWD(LowercasePWD(UppercasePWD(ValidatePWD(8))))).valid(password)?"YES":"NO"
                );
    }
    return 0;
}

