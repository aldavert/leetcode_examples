#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>

/*****************************************************************************/
/* PROBLEM: Pairwise algorithm                                               */
/* ------------------------------------------------------------------------- */
/* Write a general-purpose function that, given a range, returns a new range */
/* with pairs of consecutive elements from the input range. Should the input */
/* range have an odd number of elements, the last one must be ignored. For   */
/* example, if the input range was {1, 1, 3, 5, 8, 13, 21], the result must  */
/* be {{1, 1}, {3, 5}, {8, 13}}.                                             */
/*****************************************************************************/

// ############################################################################
// ############################################################################

template <typename T, template <typename> typename RANGE>
requires std::ranges::range<RANGE<T> >
RANGE<std::pair<T, T> > solution(const RANGE<T> &range)
{
    RANGE<std::pair<T, T> > result;
    T previous;
    for (bool second = false; const T &element : range)
    {
        if (!second) previous = element;
        else result.push_back({std::move(previous), element});
        second = !second;
    }
    return result;
}

// ############################################################################
// ############################################################################

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::pair<T, T> &tup)
{
    out << '{' << tup.first << ", " << tup.second << '}';
    return out;
}

template <typename T, template <typename> typename RANGE>
requires std::ranges::range<RANGE<T> >
         && (!std::same_as<RANGE<void>, std::basic_string<void> >)
std::ostream& operator<<(std::ostream &out, const RANGE<T> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

int main(int /***argc*/, char * * /***argv*/)
{
    std::vector<int> v{1, 1, 3, 5, 8, 13, 21};
    std::cout << "Vector " << v << " becomes " << solution(v) << '\n';
    std::list<int> l(v.begin(), v.end());
    std::cout << "List " << l << " becomes " << solution(v) << '\n';
    return 0;
}

