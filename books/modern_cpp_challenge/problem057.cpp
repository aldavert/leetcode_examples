#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>

/*****************************************************************************/
/* PROBLEM: Sort algorithm                                                   */
/* ------------------------------------------------------------------------- */
/* Write a function that, given a pair of random-access iterators to define  */
/* its lower and upper bounds, sorts the elements of the range using the     */
/* quicksort algorithm. There should be two overloads of the sort function:  */
/* one that uses operator< to compare the elements of the range and put them */
/* in ascending order, and one that uses a user-defined binary comparison    */
/* function for comparing the elements.                                      */
/*****************************************************************************/

// ############################################################################
// ############################################################################

template <std::forward_iterator ITER,
          typename T = ITER::value_type,
          typename COMPARE = std::less<T>>
requires std::invocable<COMPARE&, T, T>
void quickSort(ITER begin,
               ITER end,
               COMPARE cmp = COMPARE{})
{
    auto middle = [](ITER lo, ITER hi)
    {
        auto slow = lo, fast = lo;
        while (fast != hi)
        {
            slow = std::next(slow);
            fast = std::next(fast);
            if (fast == hi) break;
            fast = std::next(fast);
        }
        return slow;
    };
    auto partition = [&cmp,&middle](ITER lo, ITER hi)
    {
        if ((lo == hi) || (std::next(lo) == hi)) return lo; // There is nothing or a single element.
        auto mid = middle(lo, hi);
        hi = std::prev(hi);
        std::iter_swap(mid, hi);
        auto &pivot = *hi;
        auto i = std::prev(lo);
        for (auto j = lo, end = hi; j != end; j = std::next(j))
        {
            if (cmp(*j, pivot))
            {
                i = std::next(i);
                std::swap(*i, *j);
            }
        }
        i = std::next(i);
        std::swap(*i, pivot);
        return i;
    };
    if (begin == end) return;
    auto p = partition(begin, end);
    quickSort(begin, p, cmp);
    quickSort(std::next(p), end, cmp);
}

template <typename T,
          template <typename> typename RANGE,
          typename COMPARE = std::less<T> >
requires std::ranges::range<RANGE<T> > && std::invocable<COMPARE&, T, T>
void quickSort(RANGE<T> &range, COMPARE cmp = COMPARE{})
{
    quickSort(range.begin(), range.end(), cmp);
}

// ############################################################################
// ############################################################################

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::pair<T, T> &tup)
{
    out << '{' << tup.first << ", " << tup.second << '}';
    return out;
}

template <typename T, template <typename> typename RANGE>
requires std::ranges::range<RANGE<T> >
         && (!std::same_as<RANGE<void>, std::basic_string<void> >)
std::ostream& operator<<(std::ostream &out, const RANGE<T> &vec)
{
    out << '{';
    if (vec.size() > 200)
    {
        const size_t n = vec.size();
        out << vec[0];
        for (size_t i = 1; i < 10; ++i)
            out << ", " << vec[i];
        out << "...";
        for (size_t i = n - 10; i < n; ++i)
            out << ", " << vec[i];
    }
    else
    {
        for (bool next = false; const auto &v : vec)
        {
            if (next) [[likely]] out << ", ";
            next = true;
            out << v;
        }
    }
    out << '}';
    return out;
}

template <typename T, template <typename> typename RANGE_A, template <typename> typename RANGE_B>
requires std::ranges::range<RANGE_A<T> > && std::ranges::range<RANGE_B<T> >
bool equal(const RANGE_A<T> &left, const RANGE_B<T> &right)
{
    if (left.size() != right.size())
        return false;
    for (auto iter_l = left.begin(), iter_r = right.begin(), end = left.end(); iter_l != end; ++iter_l, ++iter_r)
        if (*iter_l != *iter_r)
            return false;
    return true;
}

int main(int /***argc*/, char * * /***argv*/)
{
    const size_t number_of_elements = 10'000'000;
    const size_t number_of_trials = 10;
    std::vector<int> numbers;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist(-5'000, 5'000);
    
    // Baseline ...............................................................
    fmt::print("Generating the test samples.\n");
    for (size_t i = 0; i < number_of_elements; ++i)
        numbers.push_back(dist(gen));
    fmt::print("Testing the time needed by the standard algorithm to sort a vector.\n");
    std::vector<int> expected = numbers;
    auto begin_std = std::chrono::high_resolution_clock::now();
    for (size_t t = 0; t < number_of_trials; ++t)
        std::sort(expected.begin(), expected.end());
    auto time_std = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::high_resolution_clock::now() - begin_std).count();
    fmt::print("[STD::SORT] Computed in {:L} milliseconds ({:.3f} milliseconds per iteration).\n",
            time_std,
            static_cast<double>(time_std) / number_of_trials);
    
    // Quicksort ..............................................................
    fmt::print("Testing QUICKSORT on std::vector.\n");
    std::vector<int> test = numbers;
    auto begin_quick01 = std::chrono::high_resolution_clock::now();
    for (size_t t = 0; t < number_of_trials; ++t)
        quickSort(test);
    auto time_quick01 = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::high_resolution_clock::now() - begin_quick01).count();
    fmt::print("[QUICKSORT] Computed in {:L} milliseconds ({:.3f} milliseconds per iteration).\n",
            time_quick01,
            static_cast<double>(time_quick01) / number_of_trials);
    fmt::print("[QUICKSORT] The result is [{}].\n", equal(expected, test)?"CORRECT":"WRONG");
    
    fmt::print("Testing QUICKSORT on std::list.\n");
    std::list<int> test_list(numbers.begin(), numbers.end());
    auto begin_quick02 = std::chrono::high_resolution_clock::now();
    for (size_t t = 0; t < number_of_trials; ++t)
        quickSort(test_list);
    auto time_quick02 = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::high_resolution_clock::now() - begin_quick02).count();
    fmt::print("[QUICKSORT] Computed in {:L} milliseconds ({:.3f} milliseconds per iteration).\n",
            time_quick02,
            static_cast<double>(time_quick02) / number_of_trials);
    fmt::print("[QUICKSORT] The result is [{}].\n", equal(expected, test_list)?"CORRECT":"WRONG");
    
    fmt::print("Testing QUICKSORT on std::vector.\n");
    test = numbers;
    expected = numbers;
    std::sort(expected.begin(), expected.end(), std::greater<int>());
    auto begin_quick03 = std::chrono::high_resolution_clock::now();
    for (size_t t = 0; t < number_of_trials; ++t)
        quickSort(test, std::greater<int>());
    auto time_quick03 = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::high_resolution_clock::now() - begin_quick03).count();
    fmt::print("[QUICKSORT] Computed in {:L} milliseconds ({:.3f} milliseconds per iteration).\n",
            time_quick03,
            static_cast<double>(time_quick03) / number_of_trials);
    fmt::print("[QUICKSORT] The result is [{}].\n", equal(expected, test)?"CORRECT":"WRONG");
    
    fmt::print("Testing QUICKSORT on std::list.\n");
    test_list = std::list<int>(numbers.begin(), numbers.end());
    auto begin_quick04 = std::chrono::high_resolution_clock::now();
    for (size_t t = 0; t < number_of_trials; ++t)
        quickSort(test_list, std::greater<int>());
    auto time_quick04 = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::high_resolution_clock::now() - begin_quick04).count();
    fmt::print("[QUICKSORT] Computed in {:L} milliseconds ({:.3f} milliseconds per iteration).\n",
            time_quick04,
            static_cast<double>(time_quick04) / number_of_trials);
    fmt::print("[QUICKSORT] The result is [{}].\n", equal(expected, test_list)?"CORRECT":"WRONG");
    
    return 0;
}

