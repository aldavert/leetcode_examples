#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>
#include <iomanip>
#include <unordered_set>
#include <algorithm>
#include <string_view>
#include <regex>

/*****************************************************************************/
/* PROBLEM: License plate validation                                         */
/* ------------------------------------------------------------------------- */
/* Considering license plates with the format LLL-LL DDD or LL-LL DDDD       */
/* (where L is an uppercase letter from A to Z and D is a digit), write:     */
/* - One function that validates that a license plate number is of the       */
/*   format.                                                                 */
/* - One function that, given an input text, extracts and returns all the    */
/*   license plate numbers found in the text.                                */
/*****************************************************************************/

// ############################################################################
// ############################################################################

// C++ Regex expressions are very slow. Hana Dusikova's version will be much better (https://github.com/hanickadot/compile-time-regular-expressions)
unsigned char problem_validate(std::string_view input)
{
    if (input.length() >= 10)
    {
        if ((input[0] < 'A') || (input[0] > 'Z')) return 0;
        if ((input[1] < 'A') || (input[1] > 'Z')) return 0;
        if ((input[2] < 'A') || (input[2] > 'Z')) return 0;
        if (input[3] != '-') return 0;
        if ((input[4] < 'A') || (input[4] > 'Z')) return 0;
        if ((input[5] < 'A') || (input[5] > 'Z')) return 0;
        if (input[6] != ' ') return 0;
        if ((input[7] < '0') || (input[7] > '9')) return 0;
        if ((input[8] < '0') || (input[8] > '9')) return 0;
        if ((input[9] < '0') || (input[9] > '9')) return 0;
        if ((input.length() >= 11) && (input[10] >= '0') && (input[10] <= '9'))
            return 10;
        else return 9;
    }
    return 0;
}

std::vector<std::string> problem(std::string input)
{
    std::vector<std::string> result;
    if (input.size() < 10) return result;
    std::string_view sv(input);
    const size_t n = input.size();
    size_t i = 0;
    while (i < n - 10)
    {
        unsigned char step = problem_validate(sv.substr(i, 11));
        if (step == 10)
            result.push_back(input.substr(i, 11));
        else if (step == 9)
            result.push_back(input.substr(i, 10));
        i += step + 1;
    }
    if ((n - i == 10) && (problem_validate(sv.substr(i, 10)) == 9))
        result.push_back(input.substr(i, 10));
    return result;
}

// ############################################################################
// ############################################################################

bool solution_validate(std::string_view str)
{
    std::regex rx(R"([A-Z]{3}-[A-Z]{2} \d{3,4})");
    return std::regex_match(str.data(), rx);
}

std::vector<std::string> solution(std::string str)
{
    std::regex rx(R"(([A-Z]{3}-[A-Z]{2} \d{3,4})*)");
    std::smatch match;
    std::vector<std::string> results;
    for (auto i = std::sregex_iterator(std::cbegin(str), std::cend(str), rx);
            i != std::sregex_iterator(); ++i)
    {
        if ((*i)[1].matched)
            results.push_back(i->str());
    }
    return results;
}

// ############################################################################
// ############################################################################

std::ostream& operator<<(std::ostream &out, const std::vector<std::string> &vec)
{
    out << '{';
    for (bool next = false; const auto &s : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << s;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

void test(std::string input,
          std::vector<std::string> expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution(input);
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem(input);
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] The set of license plates retrieved:";
        std::cout << '\n' << result << "\nis different than the Book's expected";
        std::cout << " output:\n" << expected << '\n';
    }
}

int main(int argc, char * * argv)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000;
#else
    constexpr unsigned int trials = 1;
#endif
    std::locale::global(std::locale("de_CH.utf8"));
    fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
    std::vector<std::tuple<std::string, std::vector<std::string> > > examples;
    examples.push_back({"AAA-AA 123qwe-ty 1234 ABC-DE 123456..XYZ-WW 0001",
                       {"AAA-AA 123", "ABC-DE 1234", "XYZ-WW 0001"}});
    const size_t buffer_size = 4096;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::string characters = "abcdefghijklmnopqrstuvwxyz! .;:?         ";
    std::string plate_letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    std::string plate_digits = "0123456789";
    std::uniform_int_distribution<> dist_values(0, characters.size() - 1);
    std::uniform_int_distribution<> dist_plate_length(3, 4);
    std::uniform_int_distribution<> dist_plate_amount(5, buffer_size / 128);
    std::uniform_int_distribution<> dist_plate_alpha(0, plate_letters.size() - 1);
    std::uniform_int_distribution<> dist_plate_digit(0, plate_digits.size() - 1);
    std::vector<size_t> random_slots;
    for (size_t i = 0; i + 16 <= buffer_size; i += 16)
        random_slots.push_back(i);
    for (unsigned int i = 0; i < 10; ++i)
    {
        std::string text = "";
        for (size_t i = 0; i < buffer_size; ++i)
            text += characters[dist_values(gen)];
        std::vector<std::string> expected;
        const size_t n_plates = dist_plate_amount(gen);
        std::shuffle(random_slots.begin(), random_slots.end(), gen);
        std::sort(random_slots.begin(), random_slots.begin() + n_plates);
        for (size_t i = 0; i < n_plates; ++i)
        {
            std::string plate;
            plate += plate_letters[dist_plate_alpha(gen)];
            plate += plate_letters[dist_plate_alpha(gen)];
            plate += plate_letters[dist_plate_alpha(gen)];
            plate += '-';
            plate += plate_letters[dist_plate_alpha(gen)];
            plate += plate_letters[dist_plate_alpha(gen)];
            plate += ' ';
            for (size_t j = 0, m = dist_plate_length(gen); j < m; ++j)
                plate += plate_digits[dist_plate_digit(gen)];
            expected.push_back(plate);
            text.replace(random_slots[i], plate.size(), plate);
        }
        examples.push_back({text, expected});
    }
    
    fmt::print("[TEST] User's solution runtime...\n");
    auto time_problem_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[input, expected] : examples)
        test(input, expected, false, trials);
    auto time_problem_end = std::chrono::high_resolution_clock::now();
    auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
    
    fmt::print("[TEST] Book's solution runtime...\n");
    auto time_solution_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[input, expected] : examples)
        test(input, expected, true, trials);
    auto time_solution_end = std::chrono::high_resolution_clock::now();
    auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    return 0;
}
