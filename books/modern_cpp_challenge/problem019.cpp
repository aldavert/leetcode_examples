#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>
#include <array>
#include <functional>
#include <list>

/*****************************************************************************/
/* PROBLEM: Adding a range of values to a container.                         */
/* ------------------------------------------------------------------------- */
/* Write a general-purpose function that can add any number of elements to   */
/* the end of a container that has a method push_back(T&& value).            */
/*****************************************************************************/

// ############################################################################
// ############################################################################

#if 0
template <typename Container, typename T>
void addValues(Container &c, const T &val)
{
    c.push_back(val);
}
template <typename Container, typename T, typename... Ts>
void addValues(Container &c, const T &val, Ts... other)
{
    c.push_back(val);
    addValues(c, other...);
}
#else
template <typename Container, typename... Ts>
void addValues(Container &c, Ts... other)
{
    (c.push_back(other), ...);
}
#endif

template <template <typename> typename CONTAINER, typename T>
std::ostream& operator<<(std::ostream &out, const CONTAINER<T> &container)
{
    out << '{';
    for (bool next = false; const auto &v : container)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

int main(int argc, char * * argv)
{
    std::vector<int> vec;
    std::list<int> lst;
    addValues(vec, 1, 2, 3, 4, 5, 6, 7, 8, 9);
    addValues(lst, 1, 2, 3, 4, 5, 6, 7, 8, 9);
    std::cout << "Vector: " << vec << '\n';
    std::cout << "List: " << lst << '\n';
    return 0;
}
