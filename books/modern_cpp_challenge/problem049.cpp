#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>

/*****************************************************************************/
/* PROBLEM: Text histogram                                                   */
/* ------------------------------------------------------------------------- */
/* Write a program that, given a text, determines and prints a histogram     */
/* with the frequency of each letter of the alphabet. The frequency is the   */
/* percentage of the number of appearance of each letter from the total      */
/* count of letters. The program should count only the appearances of        */
/* letters and ignore digits, signs, and other possible characters. The      */
/* frequency must be determined based on the count of letters and not the    */
/* text size.                                                                */
/*****************************************************************************/

// ############################################################################
// ############################################################################

std::map<char, double> frequency(const std::string &text)
{
    std::unordered_map<char, unsigned long> histogram;
    unsigned long total = 0;
    for (char c : text)
    {
        if (std::isalpha(c))
        {
            ++histogram[c];
            ++total;
        }
    }
    std::map<char, double> result;
    for (auto [chr, hist] : histogram)
        result[chr] = static_cast<double>(hist) / static_cast<double>(total);
    return result;
}

// ############################################################################
// ############################################################################


int main(int /***argc*/, char * * /***argv*/)
{
    std::string text =
        "Write a program that, given a text, determines and prints a histogram "
        "with the frequency of each letter of the alphabet. The frequency is the "
        "percentage of the number of appearance of each letter from the total "
        "count of letters. The program should count only the appearances of "
        "letters and ignore digits, signs, and other possible characters. The "
        "frequency must be determined based on the count of letters and not the "
        "text size.";
    auto freq = frequency(text);
    for (unsigned int col = 0; auto [f, r] : freq)
    {
        fmt::print("{}: {:5.1f}", f, r * 100.0);
        if (col == 6) { col = 0; fmt::print("\n"); }
        else { ++col; fmt::print(" | "); }
    }
    fmt::print("\n\n");
}

