#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>
#include <iomanip>
#include <unordered_set>
#include <algorithm>

/*****************************************************************************/
/* PROBLEM: Longest palindromic substring                                    */
/* ------------------------------------------------------------------------- */
/* Write a function that, given an input string, locates and returns the     */
/* longest sequence in the string that is a palindrome. If multiple          */
/* palindromes exist, the first one should be returned.                      */
/*****************************************************************************/

// ############################################################################
// ############################################################################

std::string problem(std::string input)
{
    const int n = input.size();
    int left = 0, size = 1;
    for (int i = 1; i < n - 1; ++i)
    {
        if (input[i - 1] == input[i + 1])
        {
            const int m = std::min(i, n - i);
            int k;
            for (k = 2; k <= m; ++k)
                if (input[i - k] != input[i + k])
                    break;
            if (int input = (k - 1) * 2 + 1; input > size)
            {
                size = input;
                left = i - k + 1;
            }
        }
    }
    for (int i = 0; i < n - 1; ++i)
    {
        if (input[i] == input[i + 1])
        {
            const int m = std::min(i, n - i - 1);
            int k;
            for (k = 1; k <= m; ++k)
                if (input[i - k] != input[i + k + 1])
                    break;
            if (int input = (k - 1) * 2 + 2; input > size)
            {
                size = input;
                left = i - k + 1;
            }
        }
    }
    return input.substr(left, size);
}

// ############################################################################
// ############################################################################

std::string solution(std::string str)
{
    size_t const len = str.size();
    if (len == 0) return ""; // <---
    size_t longestBegin = 0;
    size_t maxLen = 1;
    std::vector<bool> table(len * len, false);
    for (size_t i = 0; i < len; ++i)
        table[i * len + i] = true;
    for (size_t i = 0; i < len - 1; ++i)
    {
        if (str[i] == str[i + 1])
        {
            table[i * len + i + 1] = true;
            if (maxLen < 2)
            {
                longestBegin = i;
                maxLen = 2;
            }
        }
    }
    for (size_t k = 3; k <= len; ++k)
    {
        for (size_t i = 0; i < len - k + 1; ++i)
        {
            size_t j = i + k - 1;
            if (str[i] == str[j] && table[(i + 1) * len + j - 1])
            {
                table[i * len + j] = true;
                if (maxLen < k)
                {
                    longestBegin = i;
                    maxLen = k;
                }
            }
        }
    }
    return std::string(str.substr(longestBegin, maxLen));
}

// ############################################################################
// ############################################################################

void test(std::string input,
          std::string expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::string result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution(input);
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem(input);
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] The palindrome string:";
        std::cout << '\n' << result << "\nis different than the Book's expected";
        std::cout << " output:\n" << expected << '\n';
    }
}

int main(int argc, char * * argv)
{
#if 1
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    std::locale::global(std::locale("de_CH.utf8"));
    fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
    std::vector<std::tuple<std::string, std::string> > examples;
    examples.push_back({"babad", "bab"});
    examples.push_back({"cbbd", "bb"});
    examples.push_back({"aaaa", "aaaa"});
    examples.push_back({"level", "level"});
    examples.push_back({"sahararahnide", "hararah"});
    examples.push_back({"m", "m"});
    examples.push_back({"", ""});

    
    fmt::print("[TEST] User's solution runtime...\n");
    auto time_problem_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[input, expected] : examples)
        test(input, expected, false, trials);
    auto time_problem_end = std::chrono::high_resolution_clock::now();
    auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
    
    fmt::print("[TEST] Book's solution runtime...\n");
    auto time_solution_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[input, expected] : examples)
        test(input, expected, true, trials);
    auto time_solution_end = std::chrono::high_resolution_clock::now();
    auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    return 0;
}
