#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>

/*****************************************************************************/
/* PROBLEM: Number of days between two dates                                 */
/* ------------------------------------------------------------------------- */
/* Write a function that, given two dates, returns the number of days        */
/* between the two dates. The function should work regardless of the order   */
/* of the input dates.                                                       */
/*****************************************************************************/

using namespace std::literals::chrono_literals;
using std::chrono::January;
using std::chrono::February;
using std::chrono::March;
using std::chrono::April;
using std::chrono::May;
using std::chrono::June;
using std::chrono::July;
using std::chrono::August;
using std::chrono::September;
using std::chrono::October;
using std::chrono::November;
using std::chrono::December;
std::ostream& operator<<(std::ostream &out, const std::chrono::year_month_day &ymd)
{
    out << static_cast<int>(ymd.year()) << '/'
        << std::setw(2) << std::setfill('0')
        << static_cast<unsigned>(ymd.month()) << '/'
        << std::setw(2) << static_cast<unsigned>(ymd.day());
    return out;
}

// ############################################################################
// ############################################################################

unsigned int problem(std::chrono::year_month_day dayA, std::chrono::year_month_day dayB)
{
    if (dayA > dayB)
        return (std::chrono::sys_days{dayA} - std::chrono::sys_days{dayB}).count();
    else
        return (std::chrono::sys_days{dayB} - std::chrono::sys_days{dayA}).count();
}

unsigned int problem(int yearA, unsigned int monthA, unsigned int dayA,
                     int yearB, unsigned int monthB, unsigned int dayB)
{
    return problem(std::chrono::year_month_day{std::chrono::year{yearA}, std::chrono::month{monthA}, std::chrono::day{dayA}},
                   std::chrono::year_month_day{std::chrono::year{yearB}, std::chrono::month{monthB}, std::chrono::day{dayB}});
}

unsigned int problem(std::chrono::year yearA, std::chrono::month monthA, std::chrono::day dayA,
                     std::chrono::year yearB, std::chrono::month monthB, std::chrono::day dayB)
{
    return problem(std::chrono::year_month_day{yearA, monthA, dayA},
                   std::chrono::year_month_day{yearB, monthB, dayB});
}

// ############################################################################
// ############################################################################

int main(int argc, char * * argv)
{
    std::chrono::year_month_day dayA{1981y/April/30d}, dayB{2022y/May/12d};
    std::cout << "Days between " << dayA << " and " << dayB << ": " << problem(dayA, dayB) << " days.\n";
    std::cout << "Days between " << dayB << " and " << dayA << ": " << problem(dayB, dayA) << " days.\n";
    std::cout << "Days between 1981/April/30 and 1981/December/29: " << problem(1981, 4, 30, 1981, 12, 29) << '\n';
    std::cout << "Days between 1981/April/30 and 1975/September/11: " << problem(1981y, April, 30d, 1975y, September, 11d) << '\n';
}

