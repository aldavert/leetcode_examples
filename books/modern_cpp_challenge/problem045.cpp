#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <list>

/*****************************************************************************/
/* PROBLEM: Priority Queue                                                   */
/* ------------------------------------------------------------------------- */
/* Write a data structure that represents a priority queue that provides     */
/* constant time lookup for the largest element, but has logarithmic time    */
/* complexity for adding and removing elements. A queue inserts new elements */
/* at the end and removes elements from the top. By default, the queue       */
/* should use operator< to compare elements, but it should be possible for   */
/* the user to provide a comparison function object that returns true if the */
/* first argument is less than the second. The implementation must provide   */
/* at least the following operations:                                        */
/*     - push() to add a new element                                         */
/*     - pop() to remove the top element                                     */
/*     - top() to provide access to the top element                          */
/*     - size() to indicate the number of elements in the queue              */
/*     - empty() to indicate whether the queue is empty                      */
/*****************************************************************************/

// ############################################################################
// ############################################################################

template <typename T, typename COMPARE = std::less<T> >
requires std::invocable<COMPARE&, T, T>
class PriorityQueue
{
protected:
    std::vector<T> m_data = {0};
    COMPARE m_cmp = COMPARE{};
public:
    PriorityQueue(void) = default;
    template <std::forward_iterator ITER>
    PriorityQueue(ITER begin, ITER end)
    {
        for (; begin != end; ++begin)
            push(*begin);
    }
    PriorityQueue(std::initializer_list<T> init)
    {
        for (const auto &e : init)
            push(e);
    }
    void push(const T &element)
    {
        m_data.push_back(element);
        push2Top();
    }
    void push(T &&element)
    {
        m_data.emplace_back(element);
        push2Top();
    }
    void pop(void)
    {
        std::swap(m_data[1], m_data.back());
        m_data.pop_back();
        if (empty()) return;
        size_t pos = 1;
        while (true)
        {
            const size_t left = 2 * pos;
            const size_t right = 2 * pos + 1;
            size_t selected = pos;
            if ((left < m_data.size()) && m_cmp(m_data[selected], m_data[left]))
                selected = left;
            if ((right < m_data.size()) && m_cmp(m_data[selected], m_data[right]))
                selected = right;
            if (pos != selected)
            {
                std::swap(m_data[pos], m_data[selected]);
                pos = selected;
            }
            else break;
        }
    }
    const T& top(void) const { return m_data.at(1); }
    constexpr size_t size(void) const noexcept { return m_data.size() - 1; }
    constexpr bool empty(void) const noexcept { return m_data.size() == 1; }
private:
    void push2Top(void)
    {
        size_t pos = m_data.size() - 1;
        while ((pos > 1) && m_cmp(m_data[pos / 2], m_data[pos]))
        {
            std::swap(m_data[pos], m_data[pos / 2]);
            pos /= 2;
        }
    }
};

// ############################################################################
// ############################################################################

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::vector<T> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

template <typename T>
bool operator==(const std::vector<T> &left, const std::vector<T> &right)
{
    if (left.size() != right.size())
        return false;
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

int main(int /***argc*/, char * * /***argv*/)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist(-50, 50);
    std::vector<int> elements(100);
    for (size_t i = 0; i < 100; ++i) elements[i] = dist(gen);
    
    PriorityQueue<int> queue_max;
    PriorityQueue<int, std::greater<int> > queue_min;
    for (int e : elements)
    {
        queue_max.push(e);
        queue_min.push(e);
    }
    std::vector<int> vector_min, vector_max;
    for (; !queue_max.empty(); queue_max.pop())
        vector_max.push_back(queue_max.top());
    std::cout << "Max heap order: " << vector_max << '\n';
    std::sort(elements.begin(), elements.end(), std::greater<int>());
    std::cout << "Compared to sort: " << ((elements == vector_max)?"[CORRECT]":"[WRONG]") << '\n';
    
    for (; !queue_min.empty(); queue_min.pop())
        vector_min.push_back(queue_min.top());
    std::cout << "Min heap order: " << vector_min << '\n';
    std::sort(elements.begin(), elements.end(), std::less<int>());
    std::cout << "Compared to sort: " << ((elements == vector_min)?"[CORRECT]":"[WRONG]") << '\n';
    
    std::cout << "Initialized through begin/end iterators of VECTOR\n";
    PriorityQueue<int> queue_max2(elements.begin(), elements.end());
    vector_max.clear();
    for (; !queue_max2.empty(); queue_max2.pop())
        vector_max.push_back(queue_max2.top());
    std::cout << "Max heap order: " << vector_max << '\n';
    std::sort(elements.begin(), elements.end(), std::greater<int>());
    std::cout << "Compared to sort: " << ((elements == vector_max)?"[CORRECT]":"[WRONG]") << '\n';
    
    std::cout << "Initialized through begin/end iterators of LIST\n";
    std::list<int> elementsl(elements.begin(), elements.end());
    PriorityQueue<int> queue_max4(elementsl.begin(), elementsl.end());
    vector_max.clear();
    for (; !queue_max4.empty(); queue_max4.pop())
        vector_max.push_back(queue_max4.top());
    std::cout << "Max heap order: " << vector_max << '\n';
    std::sort(elements.begin(), elements.end(), std::greater<int>());
    std::cout << "Compared to sort: " << ((elements == vector_max)?"[CORRECT]":"[WRONG]") << '\n';
    
    std::cout << "Initialized through initialization list\n";
    std::vector<int> elements2 = { 3, 4, 7, 1, 4, 2, 8, 3, 9, 4, 3, 1, 9, 4, 5 };
    PriorityQueue<int> queue_max3 = { 3, 4, 7, 1, 4, 2, 8, 3, 9, 4, 3, 1, 9, 4, 5 };
    vector_max.clear();
    for (; !queue_max3.empty(); queue_max3.pop())
        vector_max.push_back(queue_max3.top());
    std::cout << "Max heap order: " << vector_max << '\n';
    std::sort(elements2.begin(), elements2.end(), std::greater<int>());
    std::cout << "Compared to sort: " << ((elements2 == vector_max)?"[CORRECT]":"[WRONG]") << '\n';
    return 0;
}

