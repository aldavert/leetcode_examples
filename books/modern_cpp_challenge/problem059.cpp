#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>

/*****************************************************************************/
/* PROBLEM: The Weasel program                                               */
/* ------------------------------------------------------------------------- */
/* Write a program that implements Richard Dawkins' weasel computer          */
/* simulation, described in Dawkins' words as follows (The Blind Watchmaker, */
/* chapter 3):                                                               */
/* We again use our computer monkey, but with a crucial difference in its    */
/* program. It again begins by choosing a random sequence of 28 letters,     */
/* just as before ... it duplicates it repeatedly, but with a certain chance */
/* of random - 'mutation' - in the copying. The computer examines the mutant */
/* nonsense phrases, the 'progeny' of the original phrase, and chooses the   */
/* one which, however slightly, most resembles the target phrase, METHINKS   */
/* IT IS LIKE A WEASEL.                                                      */
/*****************************************************************************/

template <typename T, typename U>
std::ostream& operator<<(std::ostream &out, const std::pair<T, U> &tup)
{
    out << '{' << tup.first << ", " << tup.second << '}';
    return out;
}

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::vector<T> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

// ############################################################################
// ############################################################################

std::vector<std::pair<std::string, size_t> > shortestPath(
        std::string start,
        std::string target,
        double probability = 0.1,
        unsigned int pool_size = 100)
{
    if (start.size() != target.size())
        return {};
    const size_t n = start.size();
    std::string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist_letter(0, characters.size() - 1);
    std::uniform_real_distribution<> dist_change(0.0, 1.0);
    std::vector<std::pair<std::string, size_t> > result;
    auto evaluate = [&n](std::string sequence, std::string goal) -> size_t
    {
        size_t result = 0;
        for (size_t i = 0; i < n; ++i)
            result += sequence[i] == goal[i];
        return result;
    };
    
    result.push_back({start, evaluate(start, target)});
    while (start != target)
    {
        size_t max_score = 0;
        std::string max_string = start;
        for (unsigned int ps = 0; ps < pool_size; ++ps)
        {
            std::string current = start;
            for (size_t i = 0; i < n; ++i)
                if (dist_change(gen) < probability)
                    current[i] = characters[dist_letter(gen)];
            auto score = evaluate(current, target);
            if (score > max_score)
            {
                max_score = score;
                max_string = current;
            }
        }
        result.push_back({max_string, max_score});
        start = max_string;
    }
    return result;
}

// ############################################################################
// ############################################################################

int main(int argc, char * * argv)
{
    double change_probability = 0.05;
    unsigned int pool_size = 100;
    auto message = [](void) -> void
    {
        std::cout << "\nThe Weasel program\n";
        std::cout << "$ ./problem059 (-h | -p <value> | -s <value>)*\n";
        std::cout << "\n";
        std::cout << "        -h Displays this help message.\n";
        std::cout << "-p <value> Sets the probability that a character is randomly\n";
        std::cout << "           replaced (value between 0 and 1).\n";
        std::cout << "-s <value> Sets the size of the pool of randomly generated strings\n";
        std::cout << "           at each generation (value between 1 and 1'000'000).\n";
    };
    
    for (int idx = 1; idx < argc; ++idx)
    {
        if (strcmp(argv[idx], "-h") == 0)
        {
            message();
            return 0;
        }
        else if (strcmp(argv[idx], "-p") == 0)
        {
            if (idx + 1 == argc)
            {
                std::cerr << "[ERROR] A value setting the probability parameter was expected.\n";
                message();
                return -1;
            }
            change_probability = std::atof(argv[++idx]);
            if ((change_probability <= 0) || (change_probability > 1))
            {
                std::cerr << "[ERROR] Probability value '" << change_probability << "'out of range.\n";
                message();
                return -1;
            }
        }
        else if (strcmp(argv[idx], "-s") == 0)
        {
            if (idx + 1 == argc)
            {
                std::cerr << "[ERROR] A value setting the pool size was expected.\n";
                message();
                return -1;
            }
            pool_size = std::atoi(argv[++idx]);
            if ((pool_size < 1) || (pool_size > 1'000'000))
            {
                std::cerr << "[ERROR] Pool size value '" << pool_size << "' out of range.\n";
                message();
                return -1;
            }
        }
        else
        {
            std::cerr << "[ERROR] Unexpected parameter '" << argv[idx] << "'.\n";
            message();
            return -1;
        }
    }
    std::string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist_letter(0, characters.size() - 1);
    std::string goal = "METHINKS IT IS LIKE A WEASEL";
    std::string start = goal;
    for (size_t i = 0; i < goal.size(); ++i)
        start[i] = characters[dist_letter(gen)];
    auto result = shortestPath(start, goal, change_probability, pool_size);
    size_t iteration = 0;
    for (const auto &[str, score] : result)
        fmt::print("{:03d}: {} -- score: {}\n", iteration++, str, score);
}

