#include <vector>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>
#include <bitset>

/*****************************************************************************/
/* PROBLEM: Computing order price with discounts                             */
/* ------------------------------------------------------------------------- */
/* A retail store sells a variety of goods and can offer various types of    */
/* discount, for selected customers, articles, or per order. The following   */
/* types of discount could be available:                                     */
/*     - A fixed discount, such as 5%, regardless of the article or the      */
/*       quantity that is purchased.                                         */
/*     - A volume discount, such as 10%, for each article when buying more   */
/*       than a particular quality of that article.                          */
/*     - A price discount per total order of an article, that is, a discount */
/*       for an article when a customer buys a quantity of that article so   */
/*       that the total cost exceeds a particular amount. For instance, a    */
/*       15% discount for an article when the total cost of that article     */
/*       exceeds $100. If the article costs $5, and the customer buys 30     */
/*       units, the total cost is $150; therefore, a 15% discount applies to */
/*       the order of that article.                                          */
/*     - A price discount per entire order (regardless what articles and in  */
/*       which quantity they were ordered).                                  */
/* Write a program that can calculate the final price of a particular order. */
/* It is possible to compute the final price in different ways; for          */
/* instance, all discounts could be cumulative, or on the other hand, if an  */
/* article has a discount, a customer or  total order discount might not be  */
/* considered.                                                               */
/*****************************************************************************/

// ############################################################################
// ############################################################################

enum class StrategyID { NONE, FIXED, VOLUME, THRESHOLD };

class BillingStrategy
{
public:
    virtual ~BillingStrategy(void) = default;
    virtual std::pair<double, bool> pricing(double price, unsigned int amount) const = 0;
    StrategyID id(void) const { return m_id; }
    virtual std::unique_ptr<BillingStrategy> clone(void) const = 0;
    virtual std::string description(void) const = 0;
protected:
    BillingStrategy(StrategyID id) :
        m_id(id) {}
    StrategyID m_id = StrategyID::NONE;
};

class NoDiscount : public BillingStrategy
{
public:
    NoDiscount(void) :
        BillingStrategy(StrategyID::NONE) {}
    std::pair<double, bool> pricing(double price, unsigned int amount) const
    {
        return {price * amount, false};
    }
    std::unique_ptr<BillingStrategy> clone(void) const
    {
        return std::make_unique<NoDiscount>(*this);
    }
    std::string description(void) const
    {
        return "";
    }
};

class FixedDiscount : public BillingStrategy
{
public:
    FixedDiscount(double percent) :
        BillingStrategy(StrategyID::FIXED),
        m_discount(percent / 100.0) {}
    std::pair<double, bool> pricing(double price, unsigned int amount) const
    {
        double value = price * amount;
        return {value - value * m_discount, true};
    }
    std::unique_ptr<BillingStrategy> clone(void) const
    {
        return std::make_unique<FixedDiscount>(*this);
    }
    std::string description(void) const
    {
        return fmt::format("fixed {:.2f}% discount",
                m_discount * 100.0);
    }
private:
    double m_discount;
};

class VolumeDiscount : public BillingStrategy
{
public:
    VolumeDiscount(double percent, unsigned int amount) :
        BillingStrategy(StrategyID::VOLUME),
        m_discount(percent / 100.0),
        m_amount(amount) {}
    std::pair<double, bool> pricing(double price, unsigned int amount) const
    {
        double value = price * amount;
        if (amount >= m_amount)
            return {value - value * m_discount, true};
        else return {value, false};
    }
    std::unique_ptr<BillingStrategy> clone(void) const
    {
        return std::make_unique<VolumeDiscount>(*this);
    }
    std::string description(void) const
    {
        return fmt::format("{:.2f}% discount for {} units or more",
                m_discount * 100.0,
                m_amount);
    }
private:
    double m_discount;
    unsigned int m_amount;
};

class ThresholdDiscount : public BillingStrategy
{
public:
    ThresholdDiscount(double percent, double threshold) :
        BillingStrategy(StrategyID::THRESHOLD),
        m_discount(percent / 100.0),
        m_threshold(threshold) {}
    std::pair<double, bool> pricing(double price, unsigned int amount) const
    {
        double value = price * amount;
        if (value >= m_threshold)
            return {value - value * m_discount, true};
        else return {value, false};
    }
    std::unique_ptr<BillingStrategy> clone(void) const
    {
        return std::make_unique<ThresholdDiscount>(*this);
    }
    std::string description(void) const
    {
        return fmt::format("{:.2f}% discount for purchases more expensive than {}",
                m_discount * 100.0,
                m_threshold);
    }
private:
    double m_discount;
    double m_threshold;
};

class Bill
{
public:
    struct Entry
    {
        std::string item = "";
        unsigned int amount = 0;
        double price = 0.0;
        double billing = 0.0;
        bool active_discount = false;
        std::unique_ptr<BillingStrategy> discount_strategy =
            std::make_unique<NoDiscount>();
    };
    Bill(void) = default;
    void add(std::string item,
             unsigned int amount,
             double price)
    {
        auto [billing, discounted] = m_default_strategy->pricing(price, amount);
        m_elements.push_back({item,
                              amount,
                              price,
                              billing,
                              discounted,
                              m_default_strategy->clone()});
    }
    void add(std::string item,
             unsigned int amount,
             double price,
             const BillingStrategy &strategy)
    {
        auto [billing, discounted] = strategy.pricing(price, amount);
        m_elements.push_back({item,
                              amount,
                              price,
                              billing,
                              discounted,
                              strategy.clone()});
    }
    void defaultStrategy(const BillingStrategy &strategy)
    {
        m_default_strategy = strategy.clone();
    }
    const BillingStrategy& defaultStrategy(void)
    {
        return *m_default_strategy;
    }
    void clear(void)
    {
        m_elements.clear();
    }
    double pricing(bool global_if_no_local = false,
                   const BillingStrategy &strategy = NoDiscount()) const
    {
        double price = 0.0;
        bool discount = false;
        for (const auto &e : m_elements)
        {
            discount = discount || e.active_discount;
            price += e.billing;
        }
        if (!global_if_no_local || (global_if_no_local && !discount))
            price = strategy.pricing(price, 1).first;
        return price;
    }
    void display(bool global_if_no_local = false,
                 const BillingStrategy &strategy = NoDiscount()) const
    {
        size_t width_desc = 0;
        size_t width_amount = 0;
        size_t width_price = 0;
        size_t width_billing = 0;
        for (const auto &e : m_elements)
        {
            width_desc = std::max(width_desc, e.item.size());
            width_amount = std::max(width_amount, fmt::format("{}", e.amount).size());
            width_price = std::max(width_price, fmt::format("{}", static_cast<int>(e.price)).size());
            width_billing = std::max(width_billing, fmt::format("{}", static_cast<int>(e.billing)).size());
        }
        size_t width_line = width_desc + width_amount + width_price
                          + width_billing + 9;
        for (size_t i = 0; i < width_line; ++i)
            fmt::print("-");
        fmt::print("\n");
        for (const auto &e : m_elements)
        {
            fmt::print("{:>{}} {:>{}} {:{}.2f} {:{}.2f} {}\n",
                    e.item, width_desc,
                    e.amount, width_amount,
                    e.price, width_price + 3,
                    e.billing, width_billing + 3,
                    e.active_discount?e.discount_strategy->description():"");
        }
        for (size_t i = 0; i < width_line; ++i)
            fmt::print("-");
        fmt::print("\n");
        
        double price = 0.0;
        bool discount = false;
        for (const auto &e : m_elements)
        {
            discount = discount || e.active_discount;
            price += e.billing;
        }
        bool global_discount = false;
        if (!global_if_no_local || (global_if_no_local && !discount))
        {
            auto [p, g] = strategy.pricing(price, 1);
            price = p;
            global_discount = g;
        }
        fmt::print("Total: {:{}.2f} {}\n",
                price,
                width_line - 7,
                global_discount?strategy.description():"");
    }
protected:
    std::vector<Entry> m_elements;
    std::unique_ptr<BillingStrategy> m_default_strategy =
        std::make_unique<NoDiscount>();
};

// ############################################################################
// ############################################################################

int main(int /***argc*/, char * * /***argv*/)
{
    Bill bill;
    bill.add("eggs", 24, 0.2, NoDiscount());
    bill.add("1.5L water bottle", 12, 0.38, NoDiscount());
    bill.add("1L milk bottle", 6, 0.38, FixedDiscount(4));
    bill.add("Cocoa 1Kg", 1, 5.45, FixedDiscount(4));
    bill.add("Potatoes 5Kg", 1, 3.5, VolumeDiscount(20, 2));
    bill.add("Small tomatoes", 12, 0.1, VolumeDiscount(7, 6));
    bill.add("Pork meat", 10, 6.7, ThresholdDiscount(10, 50));
    bill.add("Chicken meat", 10, 2.4, ThresholdDiscount(10, 50));
    fmt::print("No global discount.\n");
    bill.display();
    fmt::print("\n");
    fmt::print("Global 5% discount.\n");
    bill.display(false, FixedDiscount(5));
    fmt::print("\n");
    fmt::print("Global 5% discount applied only when no product has been discounted.\n");
    bill.display( true, FixedDiscount(5));
    fmt::print("\n");
    return 0;
}

