#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <bitset>
#include <limits>
#include <functional>

/*****************************************************************************/
/* PROBLEM: Largest Collatz sequence                                         */
/* ------------------------------------------------------------------------- */
/* Write a program that determines and prints which number up to 1 million   */
/* produces the longest Collatz sequence and what length is.                 */
/*****************************************************************************/

// ############################################################################
// ############################################################################

std::pair<unsigned int, unsigned int> problem(void)
{
    const unsigned int MAX = std::numeric_limits<unsigned int>::max();
    std::vector<unsigned int> length(1'000'001, MAX);
    std::function<unsigned int(unsigned int)> collatz =
        [&](unsigned int number) -> unsigned int
    {
        if (number <= 1) return 0;
        if (number > 1'000'000)
        {
            if ((number & 1) == 0)
                return 1 + collatz(number / 2);
            else return 1 + collatz(3 * number + 1);
        }
        else
        {
            if (length[number] == MAX)
            {
                if ((number & 1) == 0)
                    length[number] = 1 + collatz(number / 2);
                else length[number] = 1 + collatz(3 * number + 1);
            }
            return length[number];
        }
    };
    unsigned int maximum_length = 0, maximum_value = 0;
    for (unsigned int i = 0; i <= 1'000'000; ++i)
    {
        length[i] = collatz(i);
        if (length[i] > maximum_length)
        {
            maximum_length = length[i];
            maximum_value = i;
        }
    }
    return {maximum_value, maximum_length};
}

// ############################################################################
// ############################################################################

std::pair<unsigned int, unsigned int> solution(void)
{
    long length = 0;
    unsigned long long number = 0;
    std::vector<int> cache(1'000'001, 0);
    for (unsigned long long i = 2; i <= 1'000'000; ++i)
    {
        auto n = i;
        long step = 0;
        while (n != 1 && n >= i)
        {
            if ((n % 2) == 0) n = n / 2;
            else n = n * 3 + 1;
            ++step;
        }
        cache[i] = step + cache[n];
        if (cache[i] > length)
        {
            length = cache[i];
            number = i;
        }
    }
    return {number, length};
}

// ############################################################################
// ############################################################################

void test(const std::pair<unsigned int, unsigned int> &expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::pair<unsigned int, unsigned int> result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution();
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem();
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] Largest Collatz sequence produced by '";
        std::cout << result.first << "' with length '" << result.second << "'\n";
        std::cout << "          However, number '" << expected.first;
        std::cout << "' with length '" << expected.second << "' is expected.\n";
    }
}

int main(int argc, char * * argv)
{
#if 1
    //constexpr unsigned int trials = 1'000'000;
    constexpr unsigned int trials = 1'000;
#else
    constexpr unsigned int trials = 1;
#endif
    std::locale::global(std::locale("de_CH.utf8"));
    auto time_problem_begin = std::chrono::high_resolution_clock::now();
    fmt::print("[TEST] Computing solution.\n");
    auto expected = solution();
    fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
    fmt::print("[TEST] User's solution runtime...\n");
    test(expected, false, trials);
    auto time_problem_end = std::chrono::high_resolution_clock::now();
    auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
    
    auto time_solution_begin = std::chrono::high_resolution_clock::now();
    fmt::print("[TEST] Book's solution runtime...\n");
    test(expected, true, trials);
    auto time_solution_end = std::chrono::high_resolution_clock::now();
    auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    
    auto result = problem();
    fmt::print("Largest Collatz sequence is generated by {:L} with a length of {:L}.\n", result.first, result.second);
    return 0;
}
