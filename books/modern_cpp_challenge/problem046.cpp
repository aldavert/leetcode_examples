#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>

/*****************************************************************************/
/* PROBLEM: Circular buffer                                                  */
/* ------------------------------------------------------------------------- */
/* Create a data structure that represents a circular buffer of a fixed      */
/* size. A circular buffer overwrites existing elements when the buffer is   */
/* being filled beyond its fixed size. The class you must write should:      */
/*     - Prohibit default construction                                       */
/*     - Support the creation of objects with a specified size.              */
/*     - Allow checking of the buffer capacity and status                    */
/*       (empty(), full(), size(), capacity())                               */
/*     - Add a new element, an operation that would potentially overwrite    */
/*       the oldest element in the buffer.                                   */
/*     - Remove the oldest element from the buffer.                          */
/*     - Support iteration through its elements.                             */
/*****************************************************************************/

// ############################################################################
// ############################################################################

// 0 1 2 3 4 5 6 7 8 9 A B C D E F
// ^               ^
// F               L
// 0 1 2 3 4 5 6 7 8 9 A B C D E F
//         ^           ^
//         L           F

template <typename T>
class CircularQueue
{
protected:
    std::vector<T> m_data;
    size_t m_first = 0;
    size_t m_last = 0;
    size_t m_size = 0;
public:
    struct iterator
    {
        size_t m_position = 0;
        size_t m_size;
        CircularQueue * m_info = 0;
        using iterator_category = std::forward_iterator_tag;
        using difference_type   = std::ptrdiff_t;
        using value_type        = T;
        using pointer           = T*;
        using reference         = T&;
        T& operator*(void) { return m_info->m_data[m_position]; }
        const T& operator*(void) const { return m_info->m_data[m_position]; }
        iterator& operator++(void)
        {
            ++m_position;
            --m_size;
            if (m_position == m_info->m_data.size()) m_position = 0;
            return *this;
        }
        iterator operator++(int)
        {
            iterator tmp = *this;
            ++(*this);
            return tmp;
        }
        bool operator==(const iterator &other)
        {
            return m_size == other.m_size && m_info == other.m_info;
        }
    };
    CircularQueue(void) = delete;
    CircularQueue(size_t capacity) :
        m_data(capacity),
        m_first(0),
        m_last(0),
        m_size(0) {}
    constexpr bool empty(void) const noexcept { return m_size == 0; }
    constexpr bool full(void) const noexcept { return m_size == m_data.size(); }
    constexpr size_t capacity(void) const noexcept { return m_data.size(); }
    constexpr size_t size(void) const noexcept { return m_size; }
    const T& operator[](size_t pos) const { return m_data[(m_first + pos) % m_data.size()]; }
    T& operator[](size_t pos) { return m_data[(m_first + pos) % m_data.size()]; }
    void push(const T &element)
    {
        m_data[m_last] = element;
        ++m_last;
        if (m_last == m_data.size()) m_last = 0;
        if (m_size == m_data.size())
            m_first = m_last;
        else ++m_size;
    }
    T pop(void)
    {
        if (m_size > 0)
        {
            size_t pos = m_first;
            ++m_first;
            if (m_first == m_data.size()) m_first = 0;
            --m_size;
            return std::move(m_data[pos]);
        }
        else return {};
    }
    iterator begin(void) { return { m_first, m_size, this }; }
    iterator end(void)  { return { m_last, 0, this }; }
private:
};

// ############################################################################
// ############################################################################

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::vector<T> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

template <typename T>
bool operator==(const std::vector<T> &left, const std::vector<T> &right)
{
    if (left.size() != right.size())
        return false;
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

int main(int /***argc*/, char * * /***argv*/)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist(-50, 50);
    std::vector<int> elements(100);
    for (size_t i = 0; i < 100; ++i) elements[i] = dist(gen);
    
    std::cout << "CREATE CIRCULAR QUEUE WITH 10 ELEMENTS\n";
    CircularQueue<int> q(10);
    auto display = [&](void) -> void
    {
        std::cout << "Status:\n";
        std::cout << "     · empty: " << q.empty() << '\n';
        std::cout << "      · full: " << q.full() << '\n';
        std::cout << "      · size: " << q.size() << '\n';
        std::cout << "  · capacity: " << q.capacity() << '\n';
        std::cout << "Second element: " << q[1] << '\n';
        std::cout << "Loop: {";
        for (bool next = false; int e : q)
        {
            if (next) [[likely]] std::cout << ", ";
            next = true;
            std::cout << e;
        }
        std::cout << "}\n";
    };
    
    std::cout << "PUSH 4 ELEMENTS\n";
    q.push(10);
    q.push(9);
    q.push(8);
    q.push(7);
    display();
    std::cout << "POP\n";
    std::cout << "Output: " << q.pop() << '\n';
    display();
    std::cout << "PUSH 10 ELEMENTS\n";
    for (int i = 0; i < 10; ++i)
        q.push(i + 20);
    display();
    std::cout << "PUSH +15 ELEMENTS\n";
    for (int i = 0; i < 15; ++i)
        q.push(i + 70);
    display();
    std::cout << "POP 20 ELEMENTS\n";
    std::cout << "Pop output:";
    for (int i = 0; i < 15; ++i)
        std::cout << " " << q.pop();
    std::cout << '\n';
    display();
    std::cout << "PUSH +7 ELEMENTS\n";
    for (int i = 0; i < 7; ++i)
        q.push(-i);
    display();
    std::cout << "PUSH/POP +17 ELEMENTS\n";
    for (int i = 0; i < 17; ++i)
    {
        q.push(-i - 20);
        q.pop();
    }
    display();
    std::cout << "POP/PUSH +17 ELEMENTS\n";
    for (int i = 0; i < 17; ++i)
    {
        q.push(-i - 20);
        q.pop();
    }
    display();
    
    return 0;
}

