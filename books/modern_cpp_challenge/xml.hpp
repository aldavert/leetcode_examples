#include <cstdlib>
#include <ios>
#include <iostream>
#include <sstream>
#include <complex>
#include <fmt/core.h>
#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <variant>
#include <type_traits>

#ifndef __XML_HEADER_FILE__
#define __XML_HEADER_FILE__

constexpr size_t XML_STRING_SIZE = 4096;
enum class XmlTag { Open, Close, String, Assign, Identfier, Slash };

class Proxy
{
public:
    Proxy(const std::string &data) { m_data = data.c_str(); }
    Proxy(const char * data) { m_data = data; }
    ~Proxy(void) = default;
    inline operator const char * (void) { return m_data; }
    inline operator bool(void) { return std::atoi(m_data) > 0; }
    template <typename T>
    requires std::integral<T>
    inline operator T(void)
    {
        if constexpr (sizeof(T) <= 4)
            return static_cast<T>(std::atoi(m_data));
        else return static_cast<T>(std::atol(m_data));
    }
    template <typename T>
    inline operator T(void)
    {
        T value;
        std::istringstream istr(m_data);
        istr.imbue(std::locale("C"));
        istr >> value;
        return value;
    }
private:
    const char * m_data = nullptr;
};


template <typename CharT = char>
class XmlParser
{
public:
    XmlParser(std::basic_istream<CharT>  &input_stream, bool attributes_case_sensitive = false) :
        m_stream(&input_stream),
        m_line(0) {}
    XmlParser(std::basic_ostream<CharT> &output_stream, bool attributes_case_sensitive = false) :
        m_stream(&output_stream),
        m_line(0)
    {
        *std::get<std::basic_ostream<CharT> *>(m_stream) << "<?xml version=\"1.0\" encoding='UTF-8'?>\n";
    }
    ~XmlParser(void) = default;
    ////void open(const char * name);
    ////void children(void);
    ////void close(void);
    ////inline void write(const auto &value) { *m_output_stream << value; }
    ////template <typename T>
    ////inline void succeeding(const char * name, const T &value)
    ////{
    ////    m_succeeding_attributes[name] = fmt::format("{}", value);
    ////}
    ////template <typename T>
    ////inline void attribute(const char * name, const T &value)
    ////{
    ////    m_attributes[name] = fmt::format("{}", value);
    ////}
protected:
    inline CharT read(void)
    {
        auto &stream = *std::get<std::basic_istream<CharT> *>(m_stream);
        CharT c = stream.get();
        if (stream.eof()) throw std::ios_base::failure("Reached the end of the stream.");
        if (c == '\n') ++m_line;
        return c;
    }
    inline void putback(CharT value)
    {
        if (value == '\n') --m_line;
        std::get<std::basic_istream<CharT> *>(m_stream)->putback(value);
    }
    static bool ignore(CharT value)
    {
        if constexpr (std::is_same_v<CharT, char>)
            return value <= 32;
        else
        {
            static const std::unordered_set<CharT> ignored_symbols = {' ', '\n', '\t', '\r' };
            return ignored_symbols.find(value) != ignored_symbols.end();
        }
    }
    static CharT entity2symbol(const std::string &input)
    {
        static const std::unordered_map<std::basic_string<CharT>, CharT> lut =
            { {"lt", '<'}, {"amp", '&'}, {"gt", '>'}, {"quot", '"'}, {"apos", '\''} };
        if (auto search = lut.find(input); search != lut.end())
            return search->second;
        return {};
    }
    static std::string symbol2entity(CharT symbol)
    {
        static const std::unordered_map<CharT, std::basic_string<CharT> > lut =
            { {'<', "lt"}, {'&', "amp"}, {'>', "gt"}, {'"', "quot"}, {'\'', "apos"} };
        if (auto search = lut.find(symbol); search != lut.end())
            return search->second;
        return {};
    }
    void scanner(bool data = false);
    //// // Parser information.
    //// XmlTag m_category;
    //// char m_string_value[XML_STRING_SIZE];
    //// char m_value[XML_STRING_SIZE];
    //// size_t m_line;
    //// size_t m_token;
    //// // XML information.
    //// bool m_is_close_tag;
    //// bool m_is_value;
    //// bool m_is_eof;
    //// bool m_attributes_case_sensitive;
    //// 
    //// std::string m_node_data;
    //// std::unordered_map<std::string, std::string> m_attributes;
    //// std::unordered_map<std::string, std::string> m_succeeding_attributes;
    //// std::unordered_map<std::string, std::string>::const_iterator m_attributes_iterator;
    //// std::vector<std::string> m_tag_identifier;
    //// std::string m_current_tag_identifier;
    
    std::variant<std::basic_istream<CharT> *, std::basic_ostream<CharT> *> m_stream;
    XmlTag m_category;
    size_t m_line = 0;
    std::basic_string<CharT> m_value;
};

template <typename CharT>
void XmlParser<CharT>::scanner(bool data)
{
    CharT c;
    auto parseString = [&](CharT limiter) -> void
    {
        m_value.clear();
        c = read();
        for (; c != limiter; c = read())
        {
            if (c == '\\')
            {
                c = read();
                if      (c == '\'') m_value += '\'';
                else if (c == '"') m_value += '"';
                else
                {
                    m_value += '\\';
                    m_value += c;
                }
            }
            else if (c == '&')
            {
                std::basic_string<CharT> buffer;
                for (c = read(); c != ';'; c = read())
                    buffer += c;
                if (c = entity2symbol(buffer); c != 0)
                    m_value += c;
            }
            else m_value += c;
        }
    };
    for (;;)
    {
        c = read();
        if (ignore(c)) continue;
        switch (c)
        {
        case '<':                                   // XML Comments <!-- COMENT -->
            c = read();
            if (c == '!')
            {
                c = read();
                if (c == '-')
                {
                    c = read();
                    if (c == '-')
                    {
                        while (true)
                        {
                            c = read();
                            if (c == '-')
                            {
                                c = read();
                                if (c == '-')
                                {
                                    c = read();
                                    if (c == '>') break;
                                }
                            }
                        }
                    }
                    else
                    {
                        putback(c);                 // Open tag symbol.
                        putback('-');
                        putback('!');
                        m_category = XmlTag::Open;
                        return;
                    }
                }
                else                                // Open tag symbol.
                {
                    putback(c);
                    putback('!');
                    m_category = XmlTag::Open;
                    return;
                }
            }
            else if (c == '?')                      // Version initial tag.
            {
                while (true)
                {
                    c = read();
                    if (c == '?')
                    {
                        c = read();
                        if (c == '>') break;
                    }
                }
                break;
            }
            else                                    // Open tag symbol.
            {
                putback((char)c);
                m_category = XmlTag::Open;
                return;
            }
            break;
        case '>':                                   // Close tag symbol.
            m_category = XmlTag::Close;
            return;
        case '/':                                   // Slash symbol.
            m_category = XmlTag::Slash;
            return;
        case '=':                                   // Assignation symbol.
            m_category = XmlTag::Assign;
            return;
        case '"':                                   // Strings.
            parseString('"');
            m_category = XmlTag::String;
            return;
        case '\'': // Strings.
            parseString('\'');
            m_category = XmlTag::String;
            return;
        default:
            if (data) // This is a fast patch... Spaghetti coding.
            {
                m_value.clear();
                for (; (c != '<') && (c != '\n'); c = read())
                    m_value += c;
                putback(c);
                m_category = XmlTag::Identfier;
                return;
            }
            else
            {
                // Identifiers.
                if (((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || (c == '_') || ((c >= '0') && (c <= '9')) || (c == '.'))
                {
                    m_value.clear();
                    for (; ((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || (c == '_') || ((c >= '0') && (c <= '9')) || (c == '.'); c = read())
                        m_value += c;
                    putback(c);
                    m_category = XmlTag::Identfier;
                    return;
                }
                else
                {
                    if constexpr (std::is_same_v<CharT, char>)
                        throw std::runtime_error(fmt::format("Unexpected symbol '{}' ({:d})", c, c));
                    else throw std::runtime_error(fmt::format("Unexpected symbol with hex code: {:x}", c));
                }
            }
        }
    }
}

#endif
