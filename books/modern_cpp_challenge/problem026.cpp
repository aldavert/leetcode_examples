#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>
#include <iomanip>
#include <unordered_set>

/*****************************************************************************/
/* PROBLEM: Join strings together separated by a delimiter                   */
/* ------------------------------------------------------------------------- */
/* Write a function that, given a list of strings and a delimiter, creates   */
/* a new string by concatenating all the input strings separated with the    */
/* specified delimiter. The delimiter must not appear after the last string, */
/* and when no input string is provided, the function must return an empty   */
/* string.                                                                   */
/*                                                                           */
/* Example: input {"this", "is", "an", "example"} and delimiter ' ',         */
/* output: "this is an example".                                             */
/*****************************************************************************/

// ############################################################################
// ############################################################################

std::string problem(std::vector<std::string> input, char delimiter)
{
    std::string result;
    for (bool next = false; const auto &s : input)
    {
        if (next) [[likely]] result += delimiter;
        next = true;
        result += s;
    }
    return result;
}

// ############################################################################
// ############################################################################

std::string solution(std::vector<std::string> input, char delimiter)
{
    std::string separator{delimiter};
    if (input.size() == 0) return std::string{};
    std::ostringstream os;
    std::copy(std::begin(input), std::end(input) - 1, std::ostream_iterator<std::string>(os, separator.c_str()));
    os << *(std::end(input) - 1);
    return os.str();
}

// ############################################################################
// ############################################################################

void test(std::vector<std::string> input,
          char delimiter,
          std::string expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::string result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution(input, delimiter);
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem(input, delimiter);
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] The joined string:";
        std::cout << '\n' << result << "\nis different than the Book's expected";
        std::cout << " output:\n" << expected << '\n';
    }
}

int main(int argc, char * * argv)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 10'000;
#else
    constexpr unsigned int trials = 1;
#endif
    std::locale::global(std::locale("de_CH.utf8"));
    fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
    std::vector<std::tuple<std::vector<std::string>, char, std::string> > examples;
    std::vector<char> separators = {' ', '|', '#', '.', ':', ';'};
    std::string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist_values(0, characters.size() - 1);
    std::uniform_int_distribution<> dist_length(1, 8);
    std::uniform_int_distribution<> dist_word(4, 15);
    std::uniform_int_distribution<> dist_sep(0, separators.size() - 1);
    for (unsigned int i = 0; i < 200; ++i)
    {
        std::vector<std::string> input;
        char separator = separators[dist_sep(gen)];
        for (int i = 0, nw = dist_word(gen); i < nw; ++i)
        {
            std::string current;
            for (int j = 0, l = dist_length(gen); j < l; ++j)
                current += static_cast<char>(characters[dist_values(gen)]);
            input.push_back(current);
        }
        examples.push_back({input, separator, solution(input, separator)});
    }
    
    fmt::print("[TEST] User's solution runtime...\n");
    auto time_problem_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[input, separator, expected] : examples)
        test(input, separator, expected, false, trials);
    auto time_problem_end = std::chrono::high_resolution_clock::now();
    auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
    
    fmt::print("[TEST] Book's solution runtime...\n");
    auto time_solution_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[input, separator, expected] : examples)
        test(input, separator, expected, true, trials);
    auto time_solution_end = std::chrono::high_resolution_clock::now();
    auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    return 0;
}
