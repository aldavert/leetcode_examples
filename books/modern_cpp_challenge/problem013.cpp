#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <bitset>
#include <limits>
#include <functional>

/*****************************************************************************/
/* PROBLEM: Computing the value of Pi                                        */
/* ------------------------------------------------------------------------- */
/* Write a program that computes the value of Pi with a precision of two     */
/* decimal digits.                                                           */
/*****************************************************************************/

// ############################################################################
// ############################################################################

double problem(void)
{
#if 1
    const unsigned int partitions = 1'000'000;
    double previous_y = 0.0, previous_x = 1.0;
    double distance = 0.0;
    for (unsigned int y = 1; y <= partitions; ++y)
    {
        const double ry = static_cast<double>(y) / static_cast<double>(partitions);
        double rx = std::sqrt(1.0 - ry * ry);
        double dx = rx - previous_x;
        double dy = ry - previous_y;
        previous_x = rx;
        previous_y = ry;
        distance += std::sqrt(dx * dx + dy * dy);
    }
    return 2.0 * distance;
#else
    const long radius = 100;
    const long radius2 = radius * radius;
    int area = 0;
    for (long y = -radius; y <= radius; ++y)
    {
        for (long x = -radius; x <= radius; ++x)
        {
            if (x * x + y * y <= radius2)
                ++area;
        }
    }
    return static_cast<double>(area) / static_cast<double>(radius2);
#endif
}

// ############################################################################
// ############################################################################

double solution(void)
{
    const int samples = 1'000'000;
    std::random_device rd;
    auto seed_data = std::array<int, std::mt19937::state_size>{};
    std::generate(std::begin(seed_data), std::end(seed_data), std::ref(rd));
    std::seed_seq seq(std::begin(seed_data), std::end(seed_data));
    auto eng = std::mt19937{seq};
    auto dist = std::uniform_real_distribution<>{0, 1};
    double result = 0.0;
    for (unsigned int trial = 0; trial < 10; ++trial)
    {
        auto hit = 0;
        for (auto i = 0; i < samples; ++i)
        {
            auto x = dist(eng);
            auto y = dist(eng);
            if (y <= std::sqrt(1 - std::pow(x, 2))) hit += 1;
        }
        result += 4.0 * hit / samples;
    }
    return result / 10.0;
}

// ############################################################################
// ############################################################################

void test(double expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    double result = -1;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution();
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem();
    }
    if (static_cast<int>(100 * expected) != static_cast<int>(100 * result))
    {
        std::cout << "[FAILURE] The obtained approximation of pi is '";
        std::cout << result << "' while the expected value is ''" << expected;
        std::cout << "          However, number '" << expected;
        std::cout << "' (the value has to be 3.14XXXX where the X doesn't matter).\n";
    }
}

int main(int argc, char * * argv)
{
#if 0
    //constexpr unsigned int trials = 1'000'000;
    constexpr unsigned int trials = 1'000;
#else
    constexpr unsigned int trials = 1;
#endif
    std::locale::global(std::locale("de_CH.utf8"));
    fmt::print("[TEST] Computing solution.\n");
    auto expected = solution();
    auto time_problem_begin = std::chrono::high_resolution_clock::now();
    fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
    fmt::print("[TEST] User's solution runtime...\n");
    test(expected, false, trials);
    auto time_problem_end = std::chrono::high_resolution_clock::now();
    auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
    
    auto time_solution_begin = std::chrono::high_resolution_clock::now();
    fmt::print("[TEST] Book's solution runtime...\n");
    test(expected, true, trials);
    auto time_solution_end = std::chrono::high_resolution_clock::now();
    auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    
    fmt::print("Approximation of PI:\n");
    fmt::print("    Real=3.141592653589793238462643383279502884197....\n");
    fmt::print("Solution={:.20f}\n", problem());
    fmt::print("  Book's={:.20f}\n", solution());
    return 0;
}
