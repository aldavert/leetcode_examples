#include <vector>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>
#include <execution>

/*****************************************************************************/
/* PROBLEM: Parallel min and max element algorithms using threads            */
/* ------------------------------------------------------------------------- */
/* Implement general-purpose parallel algorithms that find the minimum value */
/* and, respectively, the maximum value in a given range. The parallelism    */
/* should be implemented using threads, although the number of concurrent    */
/* threads is an implementation detail.                                      */
/*****************************************************************************/

// ############################################################################
// ############################################################################

template <typename T, template <typename> typename RANGE>
requires std::ranges::range<RANGE<T> >
std::pair<T, T> manual_minmax(const RANGE<T> &range)
{
    const size_t nthreads = std::thread::hardware_concurrency();
    std::vector<std::pair<T, T> > results(nthreads);
    {
        auto compute = [&](size_t id)
        {
            auto begin = std::next(range.begin(), id * range.size() / nthreads);
            auto end = (id == nthreads - 1)?
                range.end():
                std::next(range.begin(), (id + 1) * range.size() / nthreads);
            auto [min, max] = std::minmax_element(begin, end);
            results[id] = { *min, *max };
        };
        std::vector<std::jthread> thread(nthreads);
        for (size_t t = 0; t < nthreads; ++t)
            thread[t] = std::jthread(compute, t);
    }
    std::pair<T, T> result = results[0];
    for (size_t t = 1; t < nthreads; ++t)
        result = {std::min(result.first, results[t].first),
                  std::max(result.second, results[t].second)};
    return result;
}

// ############################################################################
// ############################################################################

int main(int /***argc*/, char * * /***argv*/)
{
#if 0
    std::vector<double> value(250'000'000);
    for (size_t i = 0; i < value.size(); ++i)
        value[i] = static_cast<double>(i) / static_cast<double>(value.size());
#else
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dist(0, 1);
    std::vector<double> value(250'000'000);
    for (size_t i = 0; i < value.size(); ++i)
        value[i] = dist(gen);
#endif
    
    if constexpr (true)
    {
        auto begin = std::chrono::high_resolution_clock::now();
        auto [min, max] = manual_minmax(value);
        auto time = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::high_resolution_clock::now() - begin).count();
        fmt::print("Values min={:.4f} and max={:.4f} in the range.\n", min, max);
        fmt::print("Manual implementation elapsed time {} ms.\n", time);
        fmt::print("\n");
    }
    if constexpr (true)
    {
        auto begin = std::chrono::high_resolution_clock::now();
        auto [min, max] = std::minmax_element(
                std::execution::par,
                value.begin(),
                value.end());
        auto time = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::high_resolution_clock::now() - begin).count();
        fmt::print("Values min={:.4f} and max={:.4f} in the range.\n", *min, *max);
        fmt::print("Parallel elapsed time {} ms.\n", time);
        fmt::print("\n");
    }
    if constexpr (true)
    {
        auto begin = std::chrono::high_resolution_clock::now();
        auto [min, max] = std::minmax_element(
                std::execution::par_unseq,
                value.begin(),
                value.end());
        auto time = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::high_resolution_clock::now() - begin).count();
        fmt::print("Values min={:.4f} and max={:.4f} in the range.\n", *min, *max);
        fmt::print("Parallel unsequenced elapsed time {} ms.\n", time);
        fmt::print("\n");
    }
    if constexpr (true)
    {
        auto begin = std::chrono::high_resolution_clock::now();
        auto [min, max] = std::minmax_element(
                std::execution::seq,
                value.begin(),
                value.end());
        auto time = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::high_resolution_clock::now() - begin).count();
        fmt::print("Values min={:.4f} and max={:.4f} in the range.\n", *min, *max);
        fmt::print("Sequential elapsed time {} ms.\n", time);
        fmt::print("\n");
    }
    return 0;
}

