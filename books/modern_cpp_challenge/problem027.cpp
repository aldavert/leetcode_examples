#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>
#include <iomanip>
#include <unordered_set>
#include <algorithm>

/*****************************************************************************/
/* PROBLEM: Splitting a string into tokens with a list of possible           */
/*          delimiters.                                                      */
/* ------------------------------------------------------------------------- */
/* Write a function that, given a string and a list of possible delimiters   */
/* characters, splits the string into tokens separated by any of the         */
/* delimiters and returns them in an std::vector.                            */
/*                                                                           */
/* Example:                                                                  */
/* input: {"this,is.a sample!!"} and delimiters ",.! ",                      */
/* output: {"this", "is", "a", "sample"}                                     */
/*****************************************************************************/

// ############################################################################
// ############################################################################

#if 1
template <typename T, template <typename> typename CONTAINER>
std::vector<std::string> problem(std::basic_string<T> input,
                                 const CONTAINER<T> &delimiters)
{
    const size_t nd = static_cast<size_t>(*std::max_element(delimiters.begin(), delimiters.end())) + 1;
    std::vector<bool> lut(nd, true);
    for (T d : delimiters)
        lut[static_cast<size_t>(d)] = false;
    std::vector<std::string> result;
    const size_t n = input.size();
    auto next = [&](size_t i) -> size_t
    {
        for (; (i < n) && ((static_cast<size_t>(input[i]) > nd) || lut[input[i]]); ++i);
        return i;
    };
    size_t start = 0;
    for (size_t pos; (pos = next(start)) < n; start = pos + 1)
        if (pos > start)
            result.push_back(input.substr(start, pos - start));
    if (start < n)
        result.push_back(input.substr(start, n));
    return result;
}
#elif 0
template <typename T, template <typename> typename CONTAINER>
std::vector<std::string> problem(std::basic_string<T> input,
                                 const CONTAINER<T> &delimiters)
{
    std::basic_string<T> lut(delimiters.begin(), delimiters.end());
    std::vector<std::string> result;
    const size_t n = input.size();
    size_t start = 0, pos;
    while ((pos = input.find_first_of(lut, start)) < n)
    {
        if (pos > start)
            result.push_back(input.substr(start, pos - start));
        start = pos + 1;
    }
    if (start < n)
        result.push_back(input.substr(start, n));
    return result;
}
#else
template <typename T, template <typename> typename CONTAINER>
std::vector<std::string> problem(std::basic_string<T> input,
                                 const CONTAINER<T> &delimiters)
{
    std::unordered_set<T> lut(delimiters.begin(), delimiters.end());
    std::vector<std::string> result;
    std::string current;
    for (const auto &c : input)
    {
        if (lut.find(c) == lut.end())
            current += c;
        else
        {
            if (current.size() > 0)
            {
                result.push_back(current);
                current = "";
            }
        }
    }
    return result;
}
#endif

// ############################################################################
// ############################################################################

template <class Elem>
using tstring = std::basic_string<Elem, std::char_traits<Elem>, std::allocator<Elem> >;

template <class Elem>
using tstringstream = std::basic_stringstream<Elem, std::char_traits<Elem>, std::allocator<Elem> >;

template <typename Elem>
inline std::vector<tstring<Elem> > solution(tstring<Elem> text, Elem const delimiter)
{
    auto sstr = tstringstream<Elem>{text};
    auto tokens = std::vector<tstring<Elem>>{};
    auto token = tstring<Elem>{};
    while (std::getline(sstr, token, delimiter))
    {
        if (!token.empty()) tokens.push_back(token);
    }
    return tokens;
}

template <typename Elem>
inline std::vector<tstring<Elem> > solution(tstring<Elem> text, tstring<Elem> const &delimiters)
{
    auto tokens = std::vector<tstring<Elem> >{};
    size_t pos, prev_pos = 0;
    while ((pos = text.find_first_of(delimiters, prev_pos)) != std::string::npos)
    {
        if (pos > prev_pos)
            tokens.push_back(text.substr(prev_pos, pos - prev_pos));
        prev_pos = pos + 1;
    }
    if (prev_pos < text.length())
        tokens.push_back(text.substr(prev_pos, std::string::npos));
    return tokens;
}

// ############################################################################
// ############################################################################

std::ostream& operator<<(std::ostream &out, const std::vector<std::string> &vec)
{
    out << '{';
    for (bool next = false; const auto &s : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << s;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<std::string> &left,
                const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

void test(std::string input,
          std::string delimiters,
          std::vector<std::string>  expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution(input, delimiters);
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem(input, delimiters);
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] The split string:";
        std::cout << '\n' << result << "\nis different than the Book's expected";
        std::cout << " output:\n" << expected << '\n';
    }
}

int main(int argc, char * * argv)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100'000;
#else
    constexpr unsigned int trials = 1;
#endif
    std::locale::global(std::locale("de_CH.utf8"));
    fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
    std::vector<std::tuple<std::string, std::string, std::vector<std::string>> > examples;
    std::string separators = " |#.:;";
    std::vector<int> separators_rep = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3};
    std::string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist_values(0, characters.size() - 1);
    std::uniform_int_distribution<> dist_length(1, 8);
    std::uniform_int_distribution<> dist_word(4, 15);
    std::uniform_int_distribution<> dist_sep(0, separators.size() - 1);
    std::uniform_int_distribution<> dist_sep_rep(0, separators_rep.size() - 1);
    for (unsigned int i = 0; i < 200; ++i)
    {
        std::vector<std::string> expected;
        std::string input;
        for (int i = 0, nw = dist_word(gen); i < nw; ++i)
        {
            std::string current;
            for (int j = 0, l = dist_length(gen); j < l; ++j)
            {
                char c = static_cast<char>(characters[dist_values(gen)]);
                current += c;
                input += c;
            }
            for (int j = 0, l = separators_rep[dist_sep_rep(gen)]; j < l; ++j)
                input += separators[dist_sep(gen)];
            expected.push_back(current);
        }
        examples.push_back({input, separators, expected});
    }
    
    fmt::print("[TEST] User's solution runtime...\n");
    auto time_problem_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[input, separator, expected] : examples)
        test(input, separator, expected, false, trials);
    auto time_problem_end = std::chrono::high_resolution_clock::now();
    auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
    
    fmt::print("[TEST] Book's solution runtime...\n");
    auto time_solution_begin = std::chrono::high_resolution_clock::now();
    for (const auto &[input, separator, expected] : examples)
        test(input, separator, expected, true, trials);
    auto time_solution_end = std::chrono::high_resolution_clock::now();
    auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    return 0;
}
