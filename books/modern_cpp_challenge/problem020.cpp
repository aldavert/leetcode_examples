#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>
#include <array>
#include <functional>
#include <list>

/*****************************************************************************/
/* PROBLEM: Container any, all, none.                                        */
/* ------------------------------------------------------------------------- */
/* Write a set of general-purpose functions that enable checking whether any */
/* all, or non of the specified arguments are present in a given container.  */
/* These functions should make it possible to write code as follows:         */
/*                                                                           */
/* std::vector<int> v {1, 2, 3, 4, 5};                                       */
/* assert(contains_any(v, 0, 3, 30));                                        */
/*                                                                           */
/* std::array<int, 5> a{{1, 2, 3, 4, 5}};                                    */
/* assert(contains_all(a, 1, 3, 5));                                         */
/*                                                                           */
/* std::list<int> l {1, 2, 3, 4, 5};                                         */
/* assert(contains_none(l, 0, 7));                                           */
/*****************************************************************************/

// ############################################################################
// ############################################################################

template <template <typename> typename CONTAINER, typename T>
std::ostream& operator<<(std::ostream &out, const CONTAINER<T> &container)
{
    out << '{';
    for (bool next = false; const auto &v : container)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

template <typename Container, typename... Ts>
bool contains_any(const Container &c, Ts &&... arguments)
{
    auto search = [&](const auto &value) -> bool
    {
        return std::find(std::begin(c), std::end(c), value) != std::end(c);
    };
    return (... || search(arguments));
}
template <typename Container, typename... Ts>
bool contains_all(const Container &c, Ts &&... arguments)
{
    auto search = [&](const auto &value) -> bool
    {
        return std::find(std::begin(c), std::end(c), value) != std::end(c);
    };
    return (... && search(arguments));
}
template <typename Container, typename... Ts>
bool contains_none(const Container &c, Ts &&... arguments)
{
    return !contains_any(c, std::forward<Ts>(arguments)...);
}

int main(int argc, char * * argv)
{
    auto dsp = [](bool value) -> std::string
    {
        return (value)?std::string("True"):std::string("False");
    };
    std::vector<int> vec{1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::array<int, 9> arr{{1, 2, 3, 4, 5, 6, 7, 8, 9}};
    std::list<int> lst{1, 2, 3, 4, 5, 6, 7, 8, 9};
    // --------------------------------------------------------------
    std::cout << "Range 1-9 contains any of {0, 5, 10}: "
              << dsp(contains_any(vec, 0, 5, 10)).c_str() << '\n';
    std::cout << "Range 1-9 contains any of {0, 10}: "
              << dsp(contains_any(arr, 0, 10)).c_str() << '\n';
    std::cout << "Range 1-9 contains any of {3}: "
              << dsp(contains_any(arr, 3)).c_str() << '\n';
    // --------------------------------------------------------------
    std::cout << "Range 1-9 contains all of {1, 3, 5, 7}: "
              << dsp(contains_all(vec, 1, 3, 5, 7)).c_str() << '\n';
    std::cout << "Range 1-9 contains all of {0, 10}: "
              << dsp(contains_all(arr, 0, 10)).c_str() << '\n';
    std::cout << "Range 1-9 contains all of {3, 10}: "
              << dsp(contains_all(arr, 3, 10)).c_str() << '\n';
    // --------------------------------------------------------------
    std::cout << "Range 1-9 contains none of {0, 5, 10}: "
              << dsp(contains_none(vec, 0, 5, 10)).c_str() << '\n';
    std::cout << "Range 1-9 contains none of {0, 10}: "
              << dsp(contains_none(arr, 0, 10)).c_str() << '\n';
    std::cout << "Range 1-9 contains none of {3}: "
              << dsp(contains_none(arr, 3)).c_str() << '\n';
    // --------------------------------------------------------------
    return 0;
}
