#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>

/*****************************************************************************/
/* PROBLEM: The shortest path between nodes                                  */
/* ------------------------------------------------------------------------- */
/* Write a program that, given a network of nodes and the distance between   */
/* them, computes and displays the shortest distance from a specified node   */
/* to all the others, as well as the path between the start and end node.    */
/* As input, consider the following graph:                                   */
/* { A : [{B, 7}, {C, 9}, {F, 14}], B : [{A, 7}, {C, 10}, {D, 15}],          */
/*   C : [{A, 9}, {B, 10}, {D, 11}, {F, 2}], D : [{B, 15}, {C, 11}, {E, 6}], */
/*   E : [{D, 6}, {F, 9}], F : [{A, 14}, {E, 9}] }                           */
/* The program output for this graph should be the following:                */
/* A -> A: 0    A                                                            */
/* A -> B: 7    A -> B                                                       */
/* A -> C: 9    A -> C                                                       */
/* A -> D: 20   A -> C -> D                                                  */
/* A -> E: 20   A -> C -> F -> E                                             */
/* A -> F: 11   A -> C -> F                                                  */
/*****************************************************************************/

template <typename T, typename U>
std::ostream& operator<<(std::ostream &out, const std::pair<T, U> &tup)
{
    out << '{' << tup.first << ", " << tup.second << '}';
    return out;
}

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::vector<T> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

// ############################################################################
// ############################################################################

template <typename T>
struct edge
{
    std::string start;
    std::string end;
    T cost;
};

template <typename T>
struct path
{
    std::string id;
    T cost;
    std::vector<std::string> nodes;
    bool operator<(const path<T> &other) const { return id < other.id; }
};

template <typename T>
std::vector<path<T> > shortestPath(const std::vector<edge<T> > &edges,
                               std::string start)
{
    struct node
    {
        T distance = std::numeric_limits<T>::max();
        size_t previous = std::numeric_limits<size_t>::max();
        std::vector<std::pair<size_t, T> > neighbors;
        bool available = true;
    };
    std::unordered_map<std::string, size_t> node2id;
    std::vector<std::string> id2node;
    for (const auto &e : edges)
    {
        if (auto search = node2id.find(e.start); search == node2id.end())
        {
            node2id[e.start] = id2node.size();
            id2node.push_back(e.start);
        }
        if (auto search = node2id.find(e.end); search == node2id.end())
        {
            node2id[e.end] = id2node.size();
            id2node.push_back(e.end);
        }
    }
    if (node2id.find(start) == node2id.end()) return {};
    const size_t n = id2node.size();
    const size_t source = node2id[start];
    std::vector<node> nodes(n);
    for (const auto &e : edges)
    {
        size_t begin = node2id[e.start];
        size_t end = node2id[e.end];
        if (begin != end)
            nodes[begin].neighbors.push_back({end, e.cost});
    }
    nodes[source].distance = 0;
    
    while (true)
    {
        T min_distance = std::numeric_limits<T>::max();
        size_t min_selected = 0;
        for (size_t i = 0; i < n; ++i)
        {
            if (nodes[i].available && (nodes[i].distance < min_distance))
            {
                min_distance = nodes[i].distance;
                min_selected = i;
            }
        }
        if (min_distance == std::numeric_limits<T>::max())
            break;
        nodes[min_selected].available = false;
        for (auto [v, c] : nodes[min_selected].neighbors)
        {
            T alt = nodes[min_selected].distance + c;
            if (alt < nodes[v].distance)
            {
                nodes[v].distance = alt;
                nodes[v].previous = min_selected;
            }
        }
    }
    std::vector<path<T> > paths(n);
    for (size_t i = 0; i < n; ++i)
    {
        paths[i].id = id2node[i];
        paths[i].cost = nodes[i].distance;
        for (size_t u = i; u != source; u = nodes[u].previous)
            paths[i].nodes.push_back(id2node[u]);
        paths[i].nodes.push_back(id2node[source]);
        const size_t m = paths[i].nodes.size();
        for (size_t l = 0, r = m - 1; l < r; ++l, --r)
            std::swap(paths[i].nodes[l], paths[i].nodes[r]);
    }
    return paths;
}

// ############################################################################
// ############################################################################

int main(int /***argc*/, char * * /***argv*/)
{
    std::string start = "A";
    std::vector<edge<int> > graph;
    graph.push_back({"A", "B",  7});
    graph.push_back({"A", "C",  9});
    graph.push_back({"A", "F", 14});
    graph.push_back({"B", "A",  7});
    graph.push_back({"B", "C", 10});
    graph.push_back({"B", "D", 15});
    graph.push_back({"C", "A",  9});
    graph.push_back({"C", "B", 10});
    graph.push_back({"C", "D", 11});
    graph.push_back({"C", "F",  2});
    graph.push_back({"D", "B", 15});
    graph.push_back({"D", "C", 11});
    graph.push_back({"D", "E",  6});
    graph.push_back({"E", "D",  6});
    graph.push_back({"E", "F",  9});
    graph.push_back({"F", "A", 14});
    graph.push_back({"F", "E",  9});
    auto result = shortestPath(graph, start);
    std::sort(result.begin(), result.end());
    for (const auto &p : result)
    {
        std::cout << start << " -> " << p.id << " : " << p.cost << "  \t ";
        for (bool next = false; std::string s : p.nodes)
        {
            if (next) [[likely]] std::cout << " -> ";
            next = true;
            std::cout << s;
        }
        std::cout << '\n';
    }
}

