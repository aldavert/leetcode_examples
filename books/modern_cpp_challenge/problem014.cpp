#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>

/*****************************************************************************/
/* PROBLEM: Validating ISBNs                                                 */
/* ------------------------------------------------------------------------- */
/* Write a program that validates that 10-digit values entered by the user,  */
/* as a string, represent valid ISBN-10 numbers.                             */
/*****************************************************************************/

// ############################################################################
// ############################################################################

bool problem(std::string code)
{
    const size_t n = code.size();
    int value = 0, i = 10;
    for (size_t j = 0; j < n; ++j)
    {
        if ((code[j] >= '0') && (code[j] <= '9'))
        {
            value += (code[j] - '0') * i;
            --i;
        }
        else if (code[j] == 'X')
        {
            value += 10 * i;
            --i;
        }
    }
    return (i == 0) && (value % 11 == 0);
}

// ############################################################################
// ############################################################################

bool solution(std::string isbn)
{
    auto valid = false;
    if (isbn.size() == 10 && std::count_if(std::begin(isbn), std::end(isbn), isdigit) == 10)
    {
        auto w = 10;
        auto sum = std::accumulate(
                std::begin(isbn), std::end(isbn), 0, [&w](const int total, const char c)
                { return total + w-- * (c - '0'); });
        valid = !(sum % 11);
    }
    return valid;
}

// ############################################################################
// ############################################################################

void test(std::string isbn,
          bool expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    bool result = false;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution(isbn);
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem(isbn);
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] ISBN code '" << isbn << "' is evaluated as '";
        std::cout << ((!result)?("IN"):("")) << "VALID' but it should have been ";
        std::cout << "evaluated as '" << ((!expected)?("IN"):("")) << "VALID'.\n";
    }
}

int main(int argc, char * * argv)
{
    if ((argc > 1) && (std::strcmp(argv[1], "time") == 0))
    {
#if 1
        constexpr unsigned int trials = 10'000'000;
        //constexpr unsigned int trials = 10'000;
#else
        constexpr unsigned int trials = 1;
#endif
        std::locale::global(std::locale("de_CH.utf8"));
        fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
        std::vector<std::pair<std::string, bool> > test_codes;
        test_codes.push_back({"3161484100", false});
        test_codes.push_back({"2123456802", true});
        test_codes.push_back({"2123456804", false});
        test_codes.push_back({"0198526636", true});
        test_codes.push_back({"0198526638", false});
        test_codes.push_back({"8175257660", true});
        test_codes.push_back({"8175257661", false});
        test_codes.push_back({"0716703440", true});
        test_codes.push_back({"0716703447", false});
        test_codes.push_back({"1861972717", true});
        test_codes.push_back({"1861972713", false});
        
        
        fmt::print("[TEST] User's solution runtime...\n");
        auto time_problem_begin = std::chrono::high_resolution_clock::now();
        for (auto [isbn, expected] : test_codes)
            test(isbn, expected, false, trials);
        auto time_problem_end = std::chrono::high_resolution_clock::now();
        auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
        fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
        
        fmt::print("[TEST] Book's solution runtime...\n");
        auto time_solution_begin = std::chrono::high_resolution_clock::now();
        for (auto [isbn, expected] : test_codes)
            test(isbn, expected, true, trials);
        auto time_solution_end = std::chrono::high_resolution_clock::now();
        auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
        fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    }
    else
    {
        std::string isbn;
        std::cout << "Input ISBN code: ";
        std::cin >> isbn;
        auto result = problem(isbn);
        std::cout << "The ISBN code '" << isbn << "' is evaluated as: ";
        std::cout << ((!result)?("IN"):("")) << "VALID.\n";
        std::cout << "This response is ";
        if (auto sol = solution(isbn); sol == result)
            std::cout << "[CORRECT]\n";
        else
        {
            std::cout << "[INCORRECT] The expected evaluation is: ";
            std::cout << ((!sol)?("IN"):("")) << "VALID.\n";
        }
    }
    return 0;
}
