#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <numeric>
#include <random>

/*****************************************************************************/
/* PROBLEM: Least common multiple.                                           */
/* ------------------------------------------------------------------------- */
/* Write a program that will, given two or more positive integers, calculate */
/* and print the least common multiple of them all.                          */
/*****************************************************************************/

// ############################################################################
// ############################################################################

unsigned int problem(std::vector<unsigned int> numbers)
{
    // The GCD of the STD library is faster.
    auto gcd = [](unsigned int a, unsigned int b) -> unsigned int
    {
#if 0
        while (b != 0)
            a = std::exchange(b, a % b);
        return a;
#elif 1
        if (a == 0) return b;
        if (b == 0) return a;
        int i = std::countr_zero(a);
        int j = std::countr_zero(b);
        a >>= i;
        b >>= j;
        int k = std::min(i, j);
        do
        {
            b >>= std::countr_zero(b);
            if (a > b) std::swap(a, b);
            b -= a;
        } while (b != 0);
        return a << k;
#else
        return std::gcd(a, b);
#endif
    };
    if (numbers.size() == 0) return 0;
    if (numbers.size() == 1) return numbers[0];
    const size_t n = numbers.size();
    unsigned int lcm = numbers[0];
    for (size_t i = 1; i < n; ++i)
    {
        auto dn = gcd(lcm, numbers[i]);
        if (!dn) return 0;
        lcm *= numbers[i] / dn;
    }
    return lcm;
}

// ############################################################################
// ############################################################################

unsigned int solution(std::vector<unsigned int> numbers)
{
    auto lcm = [](unsigned int a, unsigned int b)
    {
        unsigned int h = std::gcd(a, b);
        return h?(a * (b / h)):0;
    };
    return std::accumulate(numbers.begin(), numbers.end(), 1, lcm);
}

// ############################################################################
// ############################################################################

std::ostream& operator<<(std::ostream &out, const std::vector<unsigned int> &vec)
{
    out << '{';
    for (bool next = false; unsigned int v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

void test(std::vector<unsigned int> numbers,
          unsigned int solution,
          unsigned int trials = 1)
{
    unsigned int result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = problem(numbers);
    if (solution != result)
    {
        std::cout << "[FAILURE] The LCM of " << numbers;
        std::cout << " is expected to be " << solution << ", but ";
        std::cout << result << " is obtained instead.\n";
    }
}

int main(int argc, char * * argv)
{
    if ((argc > 1) && (std::strcmp(argv[1], "time") == 0))
    {
#if 1
        //constexpr unsigned int trials = 10'000'000;
        constexpr unsigned int trials = 1'000;
#else
        constexpr unsigned int trials = 1;
#endif
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<> dist_numbers(1'000, 10'000'000);
        std::uniform_int_distribution<> dist_size(2, 100);
        for (unsigned int t = 1; t < 1'000; ++t)
        {
            std::vector<unsigned int> numbers(dist_size(gen));
            for (size_t i = 0; i < numbers.size(); ++i)
                numbers[i] = dist_numbers(gen);
            test(numbers, solution(numbers), trials);
        }
    }
    else
    {
        size_t amount_of_digits = 0;
        do
        {
            std::cout << "Number of integers (2-20): ";
            std::cin >> amount_of_digits;
        } while ((amount_of_digits < 2) || (amount_of_digits > 20));
        std::vector<unsigned int> numbers(amount_of_digits);
        for (size_t i = 0; i < amount_of_digits; ++i)
        {
            std::cout << "Number nº " << i + 1 << ": ";
            std::cin >> numbers[i];
        }
        auto result = problem(numbers);
        std::cout << "The LCM of " << numbers << " is = " << result;
        if (auto sol = solution(numbers); sol == result)
            std::cout << " (correct)\n";
        else std::cout << " (" << sol << " was expected)\n";
    }
    return 0;
}
