#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <bitset>

/*****************************************************************************/
/* PROBLEM: Gray code                                                        */
/* ------------------------------------------------------------------------- */
/* Write a program that displays the normal binary representation, Gray code */
/* representation, and decoded Gray code values for all 5-bit numbers.       */
/*****************************************************************************/

// ############################################################################
// ############################################################################

struct GrayCodeInformation
{
    unsigned int number = 0;
    std::string binary = "";
    std::string gray_code = "";
    unsigned int decoded = 0;
    bool operator==(const GrayCodeInformation &other) const
    {
        return number == other.number
            && binary == other.binary
            && gray_code == other.gray_code
            && decoded == other.decoded;
    }
};

std::ostream& operator<<(std::ostream &out,
                         const std::vector<GrayCodeInformation> &info)
{
    out << '\n' << "Number\tBinary\tGray \tDecoded\n";
    out <<         "------\t------\t-----\t-------\n";
    for (const auto &i : info)
    {
        out << i.number    << '\t' << i.binary << '\t';
        out << i.gray_code << '\t' << i.decoded << '\n';
    }
    return out;
}

std::vector<GrayCodeInformation> problem(void)
{
    std::vector<GrayCodeInformation> result(32);
    for (unsigned int i = 0; i < 32; ++i)
    {
        unsigned int encoded = i ^ (i >> 1);
        unsigned int decoded = encoded;
        for (unsigned int mask = decoded; mask; decoded ^= mask)
            mask >>= 1;
        result[i].number = i;
        result[i].binary = std::bitset<5>(i).to_string();
        result[i].gray_code = std::bitset<5>(encoded).to_string();
        result[i].decoded = decoded;
    }
    return result;
}

// ############################################################################
// ############################################################################

std::vector<GrayCodeInformation> solution(void)
{
    std::vector<GrayCodeInformation> result(32);
    auto gray_encode = [](unsigned int num) -> unsigned int
    {
        return num ^ (num >> 1);
    };
    auto gray_decode = [](unsigned int gray) -> unsigned int
    {
        for (unsigned int bit = 1U << 31; bit > 1; bit >>= 1)
            if (gray & bit) gray ^= bit >> 1;
        return gray;
    };
    auto to_binary = [](unsigned int value, unsigned int digits) -> std::string
    {
        return std::bitset<32>(value).to_string().substr(32 - digits, digits);
    };
    for (unsigned int i = 0; i < 32; ++i)
    {
        auto encg = gray_encode(i);
        auto decg = gray_decode(encg);
        result[i].number = i;
        result[i].binary = to_binary(i, 5);
        result[i].gray_code = to_binary(encg, 5);
        result[i].decoded = decg;
    }
    return result;
}

// ############################################################################
// ############################################################################

bool operator==(const std::vector<GrayCodeInformation> &left,
                const std::vector<GrayCodeInformation> &right)
{
    if (left.size() != right.size()) return false;
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

void test(const std::vector<GrayCodeInformation> &expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::vector<GrayCodeInformation> result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution();
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem();
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] The 5-bit gray codes are expected to be:\n";
        std::cout << expected << "\n However, the results obtained are:\n";
        std::cout << result << '\n';
    }
}

int main(int argc, char * * argv)
{
#if 1
    constexpr unsigned int trials = 1'000'000;
    //constexpr unsigned int trials = 100;
#else
    constexpr unsigned int trials = 1;
#endif
    std::locale::global(std::locale("de_CH.utf8"));
    auto time_problem_begin = std::chrono::high_resolution_clock::now();
    fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
    fmt::print("[TEST] User's solution runtime...\n");
    test(solution(), false, trials);
    auto time_problem_end = std::chrono::high_resolution_clock::now();
    auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
    
    auto time_solution_begin = std::chrono::high_resolution_clock::now();
    fmt::print("[TEST] Book's solution runtime...\n");
    test(solution(), true, trials);
    auto time_solution_end = std::chrono::high_resolution_clock::now();
    auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    std::cout << "5-bit Gray codes:\n";
    std::cout << problem() << '\n';
    return 0;
}
