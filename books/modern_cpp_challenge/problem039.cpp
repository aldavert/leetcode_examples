#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>

/*****************************************************************************/
/* PROBLEM: Measuring function execution time                                */
/* ------------------------------------------------------------------------- */
/* Write a function that can measure the execution time of a function        */
/* (with any number of arguments) in any required duration (such as seconds, */
/* milliseconds, microseconds, and so on).                                   */
/*****************************************************************************/

// ############################################################################
// ############################################################################

template <typename Time = std::chrono::milliseconds, typename F, typename... Args>
auto problem(F &&func, Args... args)
{
    auto time_begin = std::chrono::high_resolution_clock::now();
    std::invoke(std::forward<F>(func), std::forward<Args>(args)...);
    auto time_end = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<Time>(time_end - time_begin).count();
}

// ############################################################################
// ############################################################################

using namespace std::literals::chrono_literals;

int main(int argc, char * * argv)
{
    auto test = [](auto time)
    {
        std::this_thread::sleep_for(time);
    };
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist(1000, 3000);
    auto duration = std::chrono::milliseconds(dist(gen));
    fmt::print("Executing wait function for {} ms...\n", duration.count());
    fmt::print("Elapsed time {} ms.\n", problem(test, duration));
}

