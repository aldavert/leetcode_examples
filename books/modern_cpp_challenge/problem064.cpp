#include <vector>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>
#include <execution>
#include <future>
#include <bitset>

/*****************************************************************************/
/* PROBLEM: Parallel sort algorithm                                          */
/* ------------------------------------------------------------------------- */
/* Write a parallel version of the sort algorithm as defined for problem 53. */
/* Sort Algorithm, in Chapter 6, Algorithms and Data Structures, which,      */
/* given a pair of random access iterators to define its own lower and upper */
/* bounds, sorts the elements of the range using the quicksort algorithm.    */
/* The function should use the comparison operators for comparing the        */
/* elements of the range. The level of parallelism and the way to achieve it */
/* is an implementation detail.                                              */
/*****************************************************************************/

// ############################################################################
// ############################################################################

template <std::random_access_iterator ITER,
          typename T = ITER::value_type,
          typename COMPARE = std::less<T>>
requires std::invocable<COMPARE&, T, T>
void quickSort(ITER begin,
               ITER end,
               COMPARE cmp = COMPARE{})
{
    auto partition = [&cmp](ITER lo, ITER hi)
    {
        if ((lo == hi) || (lo + 1 == hi)) return lo; // There is nothing or a single element.
        auto mid = (hi - lo) / 2 + lo;
        --hi;
        std::iter_swap(mid, hi);
        auto &pivot = *hi;
        auto i = lo - 1;
        for (auto j = lo, end = hi; j != end; ++j)
        {
            if (cmp(*j, pivot))
            {
                ++i;
                std::swap(*i, *j);
            }
        }
        ++i;
        std::swap(*i, pivot);
        return i;
    };
    
    const size_t nthreads = std::min(32U, std::thread::hardware_concurrency());
    std::queue<std::pair<ITER, ITER> > partitions;
    std::bitset<32> active;
    std::mutex m;
    partitions.push({begin, end});
    auto worker = [&partitions, &partition, &m, &active](size_t id)
    {
        while (active.any())
        {
            ITER lo, hi;
            bool waiting = true;
            {
                auto lock = std::scoped_lock{m};
                if (!partitions.empty())
                {
                    lo = partitions.front().first;
                    hi = partitions.front().second;
                    partitions.pop();
                    waiting = false;
                }
            }
            if (waiting)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds{1});
                auto lock = std::scoped_lock{m};
                active[id] = false;
            }
            else
            {
                {
                    auto lock = std::scoped_lock{m};
                    active[id] = true;
                }
                if (hi - lo > 10'000)
                {
                    auto p = partition(lo, hi);
                    {
                        auto lock = std::scoped_lock{m};
                        if (p - lo > 1)
                            partitions.push({lo, p});
                        if (hi - (p + 1) > 1)
                            partitions.push({p + 1, hi});
                    }
                }
                else
                {
                    std::queue<std::pair<ITER, ITER> > local;
                    local.push({lo, hi});
                    while (!local.empty())
                    {
                        auto [llo, lhi] = local.front();
                        local.pop();
                        auto p = partition(llo, lhi);
                        if (p - llo > 1)
                            local.push({llo, p});
                        if (lhi - (p + 1) > 1)
                            local.push({p + 1, lhi});
                    }
                }
            }
        }
    };
    std::vector<std::jthread> thread(nthreads);
    for (size_t t = 0; t < nthreads; ++t)
    {
        active[t] = true;
        thread[t] = std::jthread(worker, t);
    }
}

template <typename T,
          template <typename> typename RANGE,
          typename COMPARE = std::less<T> >
requires std::ranges::range<RANGE<T> > && std::invocable<COMPARE&, T, T>
void quickSort(RANGE<T> &range, COMPARE cmp = COMPARE{})
{
    quickSort(range.begin(), range.end(), cmp);
}

// ############################################################################
// ############################################################################

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::pair<T, T> &tup)
{
    out << '{' << tup.first << ", " << tup.second << '}';
    return out;
}

template <typename T, template <typename> typename RANGE>
requires std::ranges::range<RANGE<T> >
         && (!std::same_as<RANGE<void>, std::basic_string<void> >)
std::ostream& operator<<(std::ostream &out, const RANGE<T> &vec)
{
    out << '{';
    if (vec.size() > 200)
    {
        const size_t n = vec.size();
        out << vec[0];
        for (size_t i = 1; i < 10; ++i)
            out << ", " << vec[i];
        out << "...";
        for (size_t i = n - 10; i < n; ++i)
            out << ", " << vec[i];
    }
    else
    {
        for (bool next = false; const auto &v : vec)
        {
            if (next) [[likely]] out << ", ";
            next = true;
            out << v;
        }
    }
    out << '}';
    return out;
}

template <typename T, template <typename> typename RANGE_A, template <typename> typename RANGE_B>
requires std::ranges::range<RANGE_A<T> > && std::ranges::range<RANGE_B<T> >
bool equal(const RANGE_A<T> &left, const RANGE_B<T> &right)
{
    if (left.size() != right.size())
        return false;
    for (auto iter_l = left.begin(), iter_r = right.begin(), end = left.end(); iter_l != end; ++iter_l, ++iter_r)
        if (*iter_l != *iter_r)
            return false;
    return true;
}

int main(int /***argc*/, char * * /***argv*/)
{
    const size_t number_of_elements = 10'000'000;
    const size_t number_of_trials = 10;
    std::vector<int> numbers;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist(-5'000, 5'000);
    
    fmt::print("THIS IS THE CONCURRENT VERSION OF THE QUICKSORT ALGORITHM\n");
    // Baseline ...............................................................
    fmt::print("Generating the test samples.\n");
    for (size_t i = 0; i < number_of_elements; ++i)
        numbers.push_back(dist(gen));
    fmt::print("Testing the time needed by the standard algorithm to sort a vector.\n");
    std::vector<int> expected = numbers;
    auto begin_std = std::chrono::high_resolution_clock::now();
    for (size_t t = 0; t < number_of_trials; ++t)
        std::sort(expected.begin(), expected.end());
    auto time_std = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::high_resolution_clock::now() - begin_std).count();
    fmt::print("[STD::SORT] Computed in {:L} milliseconds ({:.3f} milliseconds per iteration).\n",
            time_std,
            static_cast<double>(time_std) / number_of_trials);
    
    // Quicksort ..............................................................
    fmt::print("Testing QUICKSORT on std::vector.\n");
    std::vector<int> test = numbers;
    auto begin_quick01 = std::chrono::high_resolution_clock::now();
    for (size_t t = 0; t < number_of_trials; ++t)
        quickSort(test);
    auto time_quick01 = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::high_resolution_clock::now() - begin_quick01).count();
    fmt::print("[QUICKSORT] Computed in {:L} milliseconds ({:.3f} milliseconds per iteration).\n",
            time_quick01,
            static_cast<double>(time_quick01) / number_of_trials);
    fmt::print("[QUICKSORT] The result is [{}].\n", equal(expected, test)?"CORRECT":"WRONG");
    
    fmt::print("Testing QUICKSORT on std::vector.\n");
    test = numbers;
    expected = numbers;
    std::sort(expected.begin(), expected.end(), std::greater<int>());
    auto begin_quick03 = std::chrono::high_resolution_clock::now();
    for (size_t t = 0; t < number_of_trials; ++t)
        quickSort(test, std::greater<int>());
    auto time_quick03 = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::high_resolution_clock::now() - begin_quick03).count();
    fmt::print("[QUICKSORT] Computed in {:L} milliseconds ({:.3f} milliseconds per iteration).\n",
            time_quick03,
            static_cast<double>(time_quick03) / number_of_trials);
    fmt::print("[QUICKSORT] The result is [{}].\n", equal(expected, test)?"CORRECT":"WRONG");
    
    fmt::print("The method proposed in the book with std::async doesn't show any improvement.\n"
               "The recursive method used here with a queue of tasks, std::jthread and std::mutex,\n"
               "is very cumbersome but at least improves the runtime of the algorithm.\n\n");
    return 0;

}

