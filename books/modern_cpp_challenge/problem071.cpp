#include <vector>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>
#include <bitset>

/*****************************************************************************/
/* PROBLEM: Observable vector container                                      */
/* ------------------------------------------------------------------------- */
/* Write a class template that behaves like a vector but can notify          */
/* registered parties of internal state changes. The class must provide at   */
/* least the following operations:                                           */
/*     - Various constructors fro creating new instances of the class.       */
/*     - operator= to assign values to the container.                        */
/*     - push_back() to add a new element at the end of the container.       */
/*     - pop_back() to remove the last element from the container.           */
/*     - clear() to remove all the elements from the container.              */
/*     - size() to return the number of elements from the container.         */
/*     - empty() to indicate whether the container is empty or has elements. */
/* operator=, push_back(), pop_back(), and clear() must notify others of the */
/* state changes. The notification should include the type of the change,    */
/* and, when the case, the index of the element that was changed (such as    */
/* added or removed).                                                        */
/*****************************************************************************/

// ############################################################################
// ############################################################################

template <typename... Args>
class Event
{
public:
    virtual ~Event(void) = default;
    [[nodiscard]] size_t operator+=(std::function<void(Args...)> observer)
    {
        m_observers[++m_counter] = observer;
        return m_counter;
    }
    Event& operator-=(size_t handle)
    {
        m_observers.erase(handle);
        return *this;
    }
    void raise(Args... args)
    {
        for (auto &observer : m_observers)
            (observer.second)(args...);
    }
private:
    size_t m_counter = 0;
    std::unordered_map<size_t, std::function<void(Args...)>> m_observers;
};

template <typename T>
class VectorContainer
{
public:
    VectorContainer(void)
    {
    }
    VectorContainer(size_t n, T data = 0) :
        m_vector(n, 0)
    {
    }
    VectorContainer(const VectorContainer &other) :
        m_vector(other.m_vector)
    {
    }
    VectorContainer(VectorContainer &&other)
    {
        std::swap(m_vector, other.m_vector);
    }
    VectorContainer& operator=(const VectorContainer &other)
    {
        if (this != &other)
        {
            m_vector = other.m_vector;
            eventAssignation().raise();
        }
        return *this;
    }
    VectorContainer& operator=(VectorContainer &&other)
    {
        if (this != &other)
            std::swap(m_vector, other.m_vector);
        return *this;
    }
    void push_back(const T &value)
    {
        eventPush().raise(m_vector.size());
        m_vector.push_back(value);
    }
    void pop_back(void)
    {
        m_vector.pop_back();
        eventPop().raise(m_vector.size());
    }
    void clear(void)
    {
        m_vector.clear();
        eventClear().raise();
    }
    size_t size(void) const
    {
        return m_vector.size();
    }
    bool empty(void) const
    {
        return m_vector.empty();
    }
    auto& eventAssignation(void) { return m_event_assignation; }
    auto& eventPush(void) { return m_event_push; }
    auto& eventPop(void) { return m_event_pop; }
    auto& eventClear(void) { return m_event_clear; }
protected:
    std::vector<T> m_vector;
    Event<> m_event_assignation;
    Event<size_t> m_event_push;
    Event<size_t> m_event_pop;
    Event<> m_event_clear;
};

template <typename T>
class Observer
{
public:
    Observer(VectorContainer<T> &vector) :
        m_vector(vector)
    {
        m_handle[0] = m_vector.eventAssignation() +=
            [this](void) { onAssignation(); };
        m_handle[1] = m_vector.eventPush() +=
            [this](size_t pos) { onPush(pos); };
        m_handle[2] = m_vector.eventPop() +=
            [this](size_t pos) { onPop(pos); };
        m_handle[3] = m_vector.eventClear() +=
            [this](void) { onClear(); };
    }
    virtual ~Observer(void)
    {
        m_vector.eventAssignation() -= m_handle[0];
        m_vector.eventPush() -= m_handle[1];
        m_vector.eventPop() -= m_handle[2];
        m_vector.eventClear() -= m_handle[3];
    }
protected:
    void onAssignation(void)
    {
        std::cout << "[VECTOR] Assignation.\n";
    }
    void onPush(size_t pos)
    {
        std::cout << "[VECTOR] New element pushed at position '" << pos << "'.\n";
    }
    void onPop(size_t pos)
    {
        std::cout << "[VECTOR] Element pop from position '" << pos << "'.\n";
    }
    void onClear(void)
    {
        std::cout << "[VECTOR] Clear.\n";
    }
    VectorContainer<T> &m_vector;
    size_t m_handle[4];
};

// ############################################################################
// ############################################################################

int main(int /***argc*/, char * * /***argv*/)
{
    VectorContainer<int> vecA(10, 7), vecB(5, 3);
    Observer<int> observerA(vecA);
    vecA = vecB;
    vecB.clear();
    vecB.push_back(1);
    vecB.pop_back();
    vecB.clear();
    vecB.empty();
    vecA.push_back(10);
    vecA.push_back(11);
    vecA.push_back(12);
    vecA.pop_back();
    vecA.pop_back();
    vecA.clear();
    return 0;
}

