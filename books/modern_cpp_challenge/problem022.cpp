#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>
#include <array>
#include <functional>
#include <list>

/*****************************************************************************/
/* PROBLEM: Literals of various temperature scales                           */
/* ------------------------------------------------------------------------- */
/* Write a small library that enables expressing temperatures in the three   */
/* most used scales, Celsius, Fahrenheit, and Kelvin, and converting between */
/* them. The library must enable you to write temperature literals in all    */
/* these scales, such as 36.5_deg for Celsius, 97.7_f for Fahrenheit, and    */
/* 309.65_K for Kelvin; perform operations with these values; and convert    */
/* between them.                                                             */
/*****************************************************************************/

// ############################################################################
// ############################################################################

class Celsius {};
class Fahrenheit {};
class Kelvin {};

template <typename T = Celsius>
class Temperature
{
    double m_value;
public:
    Temperature(double value) : m_value(value) {}
    Temperature(const Temperature<T> &other) :
        m_value(other.m_value) {}
    // -[ Celsius <-> Fahrenheit ]----------------------------------
    template <typename U>
    requires std::same_as<T, Celsius> && std::same_as<U, Fahrenheit>
    Temperature(const Temperature<U> &other) :
        m_value((other() - 32) * 5.0 / 9.0) {}
    template <typename U>
    requires std::same_as<T, Fahrenheit> && std::same_as<U, Celsius>
    Temperature(const Temperature<U> &other) :
        m_value(other() * 9.0 / 5.0 + 32) {}
    // -[ Celsius <-> Kelvin ]--------------------------------------
    template <typename U>
    requires std::same_as<T, Celsius> && std::same_as<U, Kelvin>
    Temperature(const Temperature<U> &other) :
        m_value(other() - 273.15) {}
    template <typename U>
    requires std::same_as<T, Kelvin> && std::same_as<U, Celsius>
    Temperature(const Temperature<U> &other) :
        m_value(other() + 273.15) {}
    // -[ Fahrenheit <-> Kelvin ]-----------------------------------
    template <typename U>
    requires std::same_as<T, Kelvin> && std::same_as<U, Fahrenheit>
    Temperature(const Temperature<U> &other) :
        m_value((other() - 32) * 5.0 / 9.0 + 273.15) {}
    template <typename U>
    requires std::same_as<T, Fahrenheit> && std::same_as<U, Kelvin>
    Temperature(const Temperature<U> &other) :
        m_value((other() - 273.15) * 9.0 / 5.0 + 32) {}
    
    // -[ Comparing operators ]-------------------------------------
    template <typename U>
    auto operator<=>(const Temperature<U> &other) const
    {
        return m_value <=> Temperature<T>(other).m_value;
    }
    template <typename U>
    auto operator==(const Temperature<U> &other) const
    {
        return m_value == Temperature<T>(other).m_value;
    }
    
    // -[ Arithmetic operations ]-----------------------------------
    friend Temperature<T> operator*(const Temperature<T> &t, double factor)
    {
        return Temperature<T>(t() * factor);
    }
    friend Temperature<T> operator*(double factor, const Temperature<T> &t)
    {
        return Temperature<T>(t() * factor);
    }
    template <typename U>
    Temperature<T> operator+(const Temperature<U> &t)
    {
        return Temperature<T>(Temperature<U>(Temperature<U>(*this)() + t()));
    }
    template <typename U>
    Temperature<T> operator-(const Temperature<U> &t)
    {
        return Temperature<T>(Temperature<U>(Temperature<U>(*this)() - t()));
    }
    template <typename U>
    Temperature<T>& operator+=(const Temperature<U> &t)
    {
        m_value = Temperature<T>(Temperature<U>(Temperature<U>(*this)() + t())).m_value;
        return *this,
    }
    template <typename U>
    Temperature<T>& operator-=(const Temperature<U> &t)
    {
        m_value = Temperature<T>(Temperature<U>(Temperature<U>(*this)() - t())).m_value;
        return *this,
    }
    Temperature<T> operator/(double factor) const
    {
        return Temperature<T>(m_value / factor);
    }
    
    // -[ Access functions ]----------------------------------------
    double operator()(void) const { return m_value; }
    friend std::ostream& operator<<(std::ostream &out, const Temperature<T> &t)
    {
        if constexpr (std::is_same<T, Celsius>::value)
            out << t.m_value << 'C';
        else if constexpr (std::is_same<T, Fahrenheit>::value)
            out << t.m_value << 'F';
        else if constexpr (std::is_same<T, Kelvin>::value)
            out << t.m_value << 'K';
        else out << t.m_value << "UNKNOWN";
        return out;
    }
};

Temperature<Celsius> operator"" _C(long double value) { return Temperature<Celsius>(value); }
Temperature<Fahrenheit> operator"" _F(long double value) { return Temperature<Fahrenheit>(value); }
Temperature<Kelvin> operator"" _K(long double value) { return Temperature<Kelvin>(value); }


int main(int, char * *)
{
    Temperature<Celsius> temp_c = 25.4;
    Temperature<Fahrenheit> temp_f(temp_c);
    Temperature<Kelvin> temp_k = temp_c;
    std::cout << "   Celsius: " << temp_c << '\n';
    std::cout << "Fahrenheit: " << temp_f << '\n';
    std::cout << "    Kelvin: " << temp_k << '\n';
    std::cout << "-----------------------------------\n";
    std::cout << "Celsius ----> Fahrenheit: " << static_cast<Temperature<Fahrenheit> >(temp_c) << '\n';
    std::cout << "Celsius ----> Kelvin....: " << static_cast<Temperature<Kelvin>     >(temp_c) << '\n';
    std::cout << "Fahrenheit -> Kelvin....: " << static_cast<Temperature<Kelvin>     >(temp_f) << '\n';
    std::cout << "Fahrenheit -> Celsius...: " << static_cast<Temperature<Celsius>    >(temp_f) << '\n';
    std::cout << "Kelvin -----> Celsius...: " << static_cast<Temperature<Celsius>    >(temp_k) << '\n';
    std::cout << "Kelvin -----> Fahrenheit: " << static_cast<Temperature<Fahrenheit> >(temp_k) << '\n';
    std::cout << "-----------------------------------\n";
    std::cout << "User Defined Literals:\n";
    std::cout << "   Celsius: " << 25.4_C;
    std::cout << " -> " << static_cast<Temperature<Fahrenheit> >(25.4_C);
    std::cout << " -> " << static_cast<Temperature<Kelvin> >(25.4_C) << '\n';
    std::cout << "Fahrenheit: " << 77.3_F;
    std::cout << " -> " << static_cast<Temperature<Celsius> >(77.3_F);
    std::cout << " -> " << static_cast<Temperature<Kelvin> >(77.3_F) << '\n';
    std::cout << "    Kelvin: " << 200.1_K;
    std::cout << " -> " << static_cast<Temperature<Celsius> >(200.1_K);
    std::cout << " -> " << static_cast<Temperature<Fahrenheit> >(200.1_K) << '\n';
    std::cout << "-----------------------------------\n";
    std::cout << "Comparing temperatures:\n";
    std::cout << "Is " << 25.4_C << " == " << static_cast<Temperature<Fahrenheit> >(25.4_C) << ": ";
    std::cout << (25.4_C == static_cast<Temperature<Fahrenheit> >(25.4_C)) << '\n';
    std::cout << "Is " << 25.3_C << " < " << static_cast<Temperature<Fahrenheit> >(25.4_C) << ": ";
    std::cout << (25.3_C < static_cast<Temperature<Fahrenheit> >(25.4_C)) << '\n';
    std::cout << "Is " << 25.3_C << " > " << static_cast<Temperature<Fahrenheit> >(25.4_C) << ": ";
    std::cout << (25.3_C > static_cast<Temperature<Fahrenheit> >(25.4_C)) << '\n';
    std::cout << "-----------------------------------\n";
    std::cout << "Factors:\n";
    std::cout << 25.4_C << " * 1.1 = " << 25.4_C * 1.1 << '\n';
    std::cout << "1.1 * " << 25.4_C << " = " << 1.1 * 25.4_C << '\n';
    std::cout << 25.4_C << " / 1.1 = " << 25.4_C / 1.1 << '\n';
    std::cout << 25.4_C << " + " << 0.5_C << " = " << 25.4_C + 0.5_C << '\n';
    std::cout << 25.4_C << " + " << 0.5_F << " = " << 25.4_C + 0.5_F << '\n';
    std::cout << 25.4_C << " + " << 0.5_K << " = " << 25.4_C + 0.5_K << '\n';
    std::cout << 25.4_C << " - " << 0.5_C << " = " << 25.4_C - 0.5_C << '\n';
    std::cout << 25.4_C << " - " << 0.5_F << " = " << 25.4_C - 0.5_F << '\n';
    std::cout << 25.4_C << " - " << 0.5_K << " = " << 25.4_C - 0.5_K << '\n';
    return 0;
}
