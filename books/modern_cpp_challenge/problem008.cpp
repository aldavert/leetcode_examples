#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>

/*****************************************************************************/
/* PROBLEM: Armstrong numbers.                                               */
/* ------------------------------------------------------------------------- */
/* Write a program that prints all Armstrong numbers with three digits.      */
/*****************************************************************************/

// ############################################################################
// ############################################################################

std::vector<unsigned int> problem(void)
{
    std::vector<unsigned int> result;
    unsigned int digit_power[10];
    digit_power[0] = 0;
    digit_power[1] = 1;
    for (unsigned int i = 2; i < 10; ++i)
        digit_power[i] = i * i * i;
    for (unsigned int i = 1; i < 10; ++i)
        for (unsigned int j = 0; j < 10; ++j)
            for (unsigned int k = 0; k < 10; ++k)
                if (unsigned int sum = digit_power[i] + digit_power[j] + digit_power[k],
                                 value = i * 100 + j * 10 + k;
                    sum == value)
                    result.push_back(value);
    return result;
}

// ############################################################################
// ############################################################################

std::vector<unsigned int> solution(void)
{
    std::vector<unsigned int> result;
    for (unsigned int a = 1; a <= 9; ++a)
    {
        for (unsigned int b = 0; b <= 9; ++b)
        {
            for (unsigned int c = 0; c <= 9; ++c)
            {
                unsigned int abc = a * 100 + b * 10 + c;
                unsigned int arm = a * a * a + b * b * b + c * c * c;
                if (abc == arm)
                    result.push_back(arm);
            }
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

std::ostream& operator<<(std::ostream &out,
                         const std::vector<unsigned int> &vec)
{
    out << '{';
    for (bool test = false; const auto &v : vec)
    {
        if (test) [[likely]] out << ", ";
        test = true;
        out << v;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<unsigned int> &left,
                const std::vector<unsigned int> &right)
{
    if (left.size() != right.size()) return false;
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

void test(const std::vector<unsigned int> &expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::vector<unsigned int> result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution();
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem();
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] The Armstrong numbers of three digits are expected to be:\n";
        std::cout << expected << "\n However, the results obtained are:\n";
        std::cout << result << '\n';
    }
}

int main(int argc, char * * argv)
{
#if 1
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 100;
#else
    constexpr unsigned int trials = 1;
#endif
    std::locale::global(std::locale("de_CH.utf8"));
    auto time_problem_begin = std::chrono::high_resolution_clock::now();
    fmt::print("[TEST] Tests are repeated {:L} times.\n", trials);
    fmt::print("[TEST] User's solution runtime...\n");
    test(solution(), false, trials);
    auto time_problem_end = std::chrono::high_resolution_clock::now();
    auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
    
    auto time_solution_begin = std::chrono::high_resolution_clock::now();
    fmt::print("[TEST] Book's solution runtime...\n");
    test(solution(), true, trials);
    auto time_solution_end = std::chrono::high_resolution_clock::now();
    auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
    fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    
    std::cout << "Armstrong numbers of three digits are: " << problem() << '\n';
    return 0;
}
