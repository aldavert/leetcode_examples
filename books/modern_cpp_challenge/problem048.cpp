#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>

/*****************************************************************************/
/* PROBLEM: The most frequent element in a range                             */
/* ------------------------------------------------------------------------- */
/* Write a function that, given a range, returns the most frequent element   */
/* and the number of times it appears in a range. If more than one element   */
/* appears the same maximum number of times then the function should return  */
/* all the elements. For instance, for the range {1, 1, 3, 5, 8, 13, 3, 5,   */
/* 8, 8, 5}, it should return {5, 3} and {8, 3}                              */
/*****************************************************************************/

// ############################################################################
// ############################################################################

template <typename T, template <typename> typename Range>
requires std::ranges::range<Range<T> >
std::vector<std::pair<T, size_t> > mostFrequent(Range<T> range)
{
    std::vector<std::pair<T, size_t> > result;
    typename std::conditional<std::is_invocable_v<std::hash<T>, T>,
                              std::unordered_map<T, size_t>,
                              std::map<T, size_t> >::type histogram;
    for (const auto &el : range)
        histogram[el] += 1;
    size_t max_frequency = 0;
    for (const auto &[key, frequency] : histogram)
        max_frequency = std::max(max_frequency, frequency);
    for (const auto &[key, frequency] : histogram)
        if (frequency == max_frequency)
            result.push_back({key, frequency});
    return result;
}

// ############################################################################
// ############################################################################

template <typename A, typename B>
std::ostream& operator<<(std::ostream &out, const std::pair<A, B> &p)
{
    out << '{' << p.first << ", " << p.second << '}';
    return out;
}

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::vector<T> &vec)
{
    out << '{';
    for (bool next = false; const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

template <typename T>
bool operator==(const std::vector<T> &left, const std::vector<T> &right)
{
    if (left.size() != right.size())
        return false;
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

int main(int /***argc*/, char * * /***argv*/)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist(-50, 50);
    std::vector<int> elements(100);
    for (size_t i = 0; i < 100; ++i) elements[i] = dist(gen);
    auto result = mostFrequent(elements);
    
    std::sort(elements.begin(), elements.end());
    std::cout << "Elements:\n" << elements << "\n\n";
    std::cout << "Most frequent: " << result << '\n';
    
    std::cout << "\n\nNON-TRIVIAL RANGE:\n";
    std::vector<std::vector<int> > elements_complex =
        {{1, 2, 3}, {1, 4, 2}, {6, 4, 1, 5}, {1, 2, 3}};
    std::cout << "Elements:\n" << elements_complex << "\n\n";
    std::cout << "Most frequent: " << mostFrequent(std::list<std::vector<int> >(elements_complex.begin(), elements_complex.end())) << '\n';
}

