#include <vector>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>
#include <execution>
#include <future>
#include <bitset>

/*****************************************************************************/
/* PROBLEM: Customer service system                                          */
/* ------------------------------------------------------------------------- */
/* Write a program that simulates the way customers are served in an office. */
/* The office has three desks where customers can be served at the same      */
/* time. Customers can enter the office at any time. They take a ticket with */
/* a service number from a ticketing machine and wait until their number is  */
/* next for service at one of the desks. Customers are served in the order   */
/* they entered the office, or more precisely, in the order given by their   */
/* ticket. Every time a service desk finishes serving a customer, the next   */
/* customer in order is served. The simulation should stop after a           */
/* particular number of customers have been issued tickets and served.       */
/*****************************************************************************/

// ############################################################################
// ############################################################################

class logger
{
    int m_x = 0;
    int m_y = 0;
    int m_current_line = 0;
    int m_number_of_lines = -1;
    std::deque<std::string> m_buffer;
    std::mutex m_mutex;
public:
    logger(void) = default;
    logger(int x, int y, int number_of_lines = -1) :
        m_x(x),
        m_y(y),
        m_current_line(0),
        m_number_of_lines(number_of_lines) {}
    void log(std::string msg)
    {
        auto lock = std::scoped_lock{m_mutex};
        if (m_number_of_lines < 0)
        {
            fmt::print("{}", msg);
        }
        else
        {
            m_buffer.push_back(msg);
            if (m_current_line < m_number_of_lines)
            {
                fmt::print("\033[{};{}H{}", m_y + m_current_line, m_x, msg);
                ++m_current_line;
            }
            else
            {
                m_buffer.pop_front();
                for (int i = 0; const auto &s : m_buffer)
                {
                    fmt::print("\033[{};{}H\033[2K\r", m_y + i, m_x);
                    fmt::print("\033[{};{}H{}", m_y + i, m_x, s);
                    ++i;
                }
            }
        }
    }
};

// ############################################################################
// ############################################################################

struct customer
{
    std::string id = "";
    int ticket = -1;
    int time = -1;
};

// ############################################################################
// ############################################################################

// GENERATE --> TICKET MACHINE (Waiting)
//                      |
//                      |
//              +-------+-------+
//              |       |       |
//              V       V       V
//           DESK_1  DESK_2  DESK_3

int main(int /***argc*/, char * * /***argv*/)
{
    auto cv_ticket = std::condition_variable{};
    auto cv_desk = std::condition_variable{};
    logger log(5, 7, 40);
    std::queue<customer> qentering, qwaiting;
    std::mutex mgen, mdesk;
    auto generate = [&](std::stop_token stoken)
    {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<> dist_enter(100, 450);
        std::uniform_int_distribution<> dist_usr(0, 1000);
        std::uniform_int_distribution<> dist_work(500, 1500);
        size_t counter = 0;
        while (!stoken.stop_requested())
        {
            std::this_thread::sleep_for(std::chrono::milliseconds{dist_enter(gen)});
            customer c = { fmt::format("{:04d}{:04d}", dist_usr(gen), ++counter),
                           -1,
                           dist_work(gen) };
            log.log(fmt::format("New user '{}' enters with {}ms of work.\n", c.id, c.time));
            {
                auto lock = std::scoped_lock{mgen};
                qentering.push(c);
            }
            cv_ticket.notify_one();
        }
        log.log(fmt::format("Generator thread stopped.\n"));
    };
    auto ticket = [&](std::stop_token stoken)
    {
        fmt::print("\033[2;1HThere are {} users waiting...", qwaiting.size());
        size_t counter = 100;
        while (!stoken.stop_requested())
        {
            customer c;
            {
                auto lock = std::unique_lock<std::mutex>{mgen};
                while (qentering.empty() && !stoken.stop_requested())
                    cv_ticket.wait(lock);
                if (stoken.stop_requested())
                    break;
                c = qentering.front();
                qentering.pop();
            }
            c.ticket = ++counter;
            log.log(fmt::format("Ticket '{}' assigned to user '{}'.\n", c.ticket, c.id));
            {
                auto lock = std::scoped_lock{mdesk};
                qwaiting.push(c);
                fmt::print("\033[2;1HThere are {} users waiting...", qwaiting.size());
            }
            cv_desk.notify_one();
        }
        log.log(fmt::format("Ticket machine thread stopped.\n"));
    };
    auto desk = [&](std::stop_token stoken, size_t id)
    {
        fmt::print("\033[{};1H[DESK {}]: EMPTY                ", 3 + id, id);
        while (!stoken.stop_requested())
        {
            customer c;
            {
                auto lock = std::unique_lock<std::mutex>{mgen};
                while (qwaiting.empty() && !stoken.stop_requested())
                    cv_desk.wait(lock);
                if (stoken.stop_requested())
                    break;
                c = qwaiting.front();
                qwaiting.pop();
            }
            log.log(fmt::format("Desk {} is processing '{}' user's ticket nº {}.\n", id, c.id, c.ticket));
            fmt::print("\033[{};1H[DESK {}]: User '{}'.", 3 + id, id, c.id);
            std::this_thread::sleep_for(std::chrono::milliseconds{c.time});
            log.log(fmt::format("Desk {} finished '{}' user's request in {}ms.\n", id, c.id, c.time));
        fmt::print("\033[{};1H[DESK {}]: EMPTY                ", 3 + id, id);
        }
        log.log(fmt::format("Desk '{}' thread stopped.\n", id));
    };
    
    fmt::print("\033[2J\033[1;1H");
    fmt::print("\033[1;38;2;0;0;0;48;2;250;170;0mCUSTOMER SERVICE SIMULATION RUNNING FOR 10 SECONDS...\n");
    fmt::print("\033[0m\n");
    // Waiting: # of customers.
    // Desk 0: % completion
    // Desk 1: % completion
    // Desk 2: % completion
    auto t0 = std::jthread(generate);
    auto t1 = std::jthread(ticket);
    auto t2 = std::jthread(desk, 0);
    auto t3 = std::jthread(desk, 1);
    auto t4 = std::jthread(desk, 2);
    std::this_thread::sleep_for(std::chrono::seconds{10});
    t0.request_stop();
    std::this_thread::sleep_for(std::chrono::milliseconds{50});
    t1.request_stop();
    while (!qwaiting.empty())
        std::this_thread::sleep_for(std::chrono::milliseconds{50});
    
    return 0;

}

