#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>

/*****************************************************************************/
/* PROBLEM: Abundant numbers.                                                */
/* ------------------------------------------------------------------------- */
/* Write a program that prints all abundant numbers and their abundance,     */
/* up to a number entered by the user.                                       */
/*****************************************************************************/

// ############################################################################
// ############################################################################

std::vector<std::pair<unsigned int, unsigned int> > problem(unsigned int limit)
{
    std::vector<std::pair<unsigned int, unsigned int> > result;
    for (unsigned int i = 12; i <= limit; ++i)
    {
        if (int mod2 = (i & 1) == 0, mod3 = (i % 3) == 0; mod2 || mod3)
        {
            unsigned int abundance = 1
                                   + mod2 * (2 + (2 != i / 2) * (i / 2))
                                   + mod3 * (3 + (3 != i / 3) * (i / 3));
            for (unsigned int j = 4; j * j <= i; ++j)
                if (i % j == 0)
                    abundance += j + (j != i / j) * (i / j);
            if (abundance > i)
                result.push_back({i, abundance - i});
        }
    }
    return result;
}

// ############################################################################
// ############################################################################

std::vector<std::pair<unsigned int, unsigned int> > solution(unsigned int limit)
{
    std::vector<std::pair<unsigned int, unsigned int> > result;
    auto sum_proper_divisors = [](unsigned int number) -> unsigned int
    {
        unsigned int result = 1;
        for (unsigned int i = 2; i <= std::sqrt(number); ++i)
            if (number % i == 0)
                result += (i == (number / i))?i:(i + number / i);
        return result;
    };
    for (unsigned int number = 10; number <= limit; ++number)
    {
        auto sum = sum_proper_divisors(number);
        if (sum > number)
            result.push_back({number, sum - number});
    }
    return result;
}

// ############################################################################
// ############################################################################

std::ostream& operator<<(std::ostream &out,
                         const std::vector<std::pair<unsigned int, unsigned int> > &vec)
{
    out << '{';
    for (bool test = false; const auto &v : vec)
    {
        if (test) [[likely]] out << ", ";
        test = true;
        out << '{' << v.first << ", " << v.second << '}';
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<std::pair<unsigned int, unsigned int> > &left,
                const std::vector<std::pair<unsigned int, unsigned int> > &right)
{
    if (left.size() != right.size()) return false;
    const size_t n = left.size();
    for (size_t i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

void test(unsigned int limit,
          const std::vector<std::pair<unsigned int, unsigned int> > &expected,
          bool run_solution = false,
          unsigned int trials = 1)
{
    std::vector<std::pair<unsigned int, unsigned int> > result;
    if (run_solution)
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = solution(limit);
    }
    else
    {
        for (unsigned int i = 0; i < trials; ++i)
            result = problem(limit);
    }
    if (expected != result)
    {
        std::cout << "[FAILURE] sexy prime pairs under '" << limit;
        std::cout << "' is expected to be:\n" << expected << "\n However, the results obtained are:\n";
        std::cout << result << '\n';
    }
}

int main(int argc, char * * argv)
{
    if ((argc > 1) && (std::strcmp(argv[1], "time") == 0))
    {
#if 0
        //constexpr unsigned int trials = 10'000'000;
        constexpr unsigned int trials = 100;
#else
        constexpr unsigned int trials = 1;
#endif
        std::locale::global(std::locale("de_CH.utf8"));
        auto time_problem_begin = std::chrono::high_resolution_clock::now();
        std::cout << "Testing the runtime of the user's solution...\n";
        for (unsigned int l : std::vector<unsigned int>({1, 1'000, 10'000, 100'000, 1'000'000}))
            test(l, solution(l), false, trials);
        auto time_problem_end = std::chrono::high_resolution_clock::now();
        auto time_problem_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_problem_end - time_problem_begin);
        fmt::print("Elapsed time: {:L} milliseconds.\n", time_problem_duration.count());
        
        auto time_solution_begin = std::chrono::high_resolution_clock::now();
        std::cout << "Testing the runtime of the book's solution...\n";
        for (unsigned int l : std::vector<unsigned int>({1, 1'000, 10'000, 100'000, 1'000'000}))
            test(l, solution(l), true, trials);
        auto time_solution_end = std::chrono::high_resolution_clock::now();
        auto time_solution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_solution_end - time_solution_begin);
        fmt::print("Elapsed time: {:L} milliseconds.\n", time_solution_duration.count());
    }
    else
    {
        unsigned int limit = 0;
        std::cout << "Input number: ";
        std::cin >> limit;
        auto result = problem(limit);
        std::cout << "The sexy prime pairs under " << limit << " are:\n" << result;
        if (auto sol = solution(limit); sol == result)
            std::cout << "\n[CORRECT]\n";
        else std::cout << "\n[FAILURE] The expected solution is:\n" << sol << '\n';
    }
    return 0;
}
