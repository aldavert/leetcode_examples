#include <vector>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>
#include <bitset>

/*****************************************************************************/
/* PROBLEM: Generating random passwords                                      */
/* ------------------------------------------------------------------------- */
/* Write a program that can random passwords according to some predefined    */
/* rules. Every password must have a configurable minimum length. In         */
/* addition, it should be possible to include in the generation rules such   */
/* as the presence of at least one digit, symbol, lower or upper character,  */
/* and so on. These additional rules must be configurable and composable.    */
/*****************************************************************************/

// ############################################################################
// ############################################################################

class IGeneratePWD
{
public:
    virtual ~IGeneratePWD(void) = default;
    virtual std::string create(void) const noexcept = 0;
    static std::mt19937& gen(void)
    {
        static std::random_device rd;
        static std::mt19937 gen(rd());
        return gen;
    }
protected:
    template <typename CHECK, typename FUNC>
    void modify(std::string &str, size_t amount, CHECK check, FUNC func) const
    {
        if (amount >= str.size())
            std::transform(str.begin(), str.end(), str.begin(), func);
        else
        {
            const size_t n = str.size();
            std::vector<size_t> pos(n);
            size_t valid = 0;
            for (size_t i = 0; i < n; ++i)
                if (check(str[i]))
                    pos[valid++] = i;
            std::shuffle(pos.begin(), pos.begin() + valid, gen());
            valid = std::min(valid, amount);
            for (size_t i = 0; i < valid; ++i)
                str[pos[i]] = func(str[pos[i]]);
        }
    }
};

class GeneratePWD : public IGeneratePWD
{
public:
    GeneratePWD(size_t length) :
        m_length(length) {}
    std::string create(void) const noexcept override
    {
        const char possible[] = "abcdefghijklmnopqrstuvwxyz";
        std::uniform_int_distribution<> dist(0, 25);
        std::string result(m_length, ' ');
        for (size_t i = 0; i < m_length; ++i)
            result[i] = possible[dist(gen())];
        return result;
    }
private:
    size_t m_length = 0;
};

class UppercasePWD : public IGeneratePWD
{
public:
    UppercasePWD(const IGeneratePWD &validate, size_t number_of_uppercase = 1) :
        m_wrapper(validate),
        m_number_of_uppercase(number_of_uppercase) {}
    std::string create(void) const noexcept override
    {
        std::string result = m_wrapper.create();
        modify(result, m_number_of_uppercase, ::isalpha, ::toupper);
        return result;
    }
private:
    const IGeneratePWD &m_wrapper;
    size_t m_number_of_uppercase = 1;
};

class LowercasePWD : public IGeneratePWD
{
public:
    LowercasePWD(const IGeneratePWD &validate, size_t number_of_lowercase = 1) :
        m_wrapper(validate),
        m_number_of_lowercase(number_of_lowercase) {}
    std::string create(void) const noexcept override
    {
        std::string result = m_wrapper.create();
        modify(result, m_number_of_lowercase, ::isalpha, ::tolower);
        return result;
    }
private:
    const IGeneratePWD &m_wrapper;
    size_t m_number_of_lowercase = 1;
};

class DigitPWD : public IGeneratePWD
{
public:
    DigitPWD(const IGeneratePWD &validate, size_t number_of_digits = 1) :
        m_wrapper(validate),
        m_number_of_digits(number_of_digits) {}
    std::string create(void) const noexcept override
    {
        std::string result = m_wrapper.create();
        char digits[] = "0123456789";
        std::uniform_int_distribution<> dist(0, 9);
        modify(result,
               m_number_of_digits,
               [](char c) -> bool{ return !std::isdigit(c); },
               [&](char) -> char { return digits[dist(gen())]; });
        return result;
    }
private:
    const IGeneratePWD &m_wrapper;
    size_t m_number_of_digits = 1;
};

class SymbolPWD : public IGeneratePWD
{
public:
    SymbolPWD(const IGeneratePWD &validate, size_t number_of_symbols = 1) :
        m_wrapper(validate),
        m_number_of_symbols(number_of_symbols) {}
    std::string create(void) const noexcept override
    {
        std::string result = m_wrapper.create();
        char symbols[] = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
        std::uniform_int_distribution<> dist(0, 31);
        modify(result,
               m_number_of_symbols,
               [](char c) -> bool{ return !std::ispunct(c); },
               [&](char) -> char { return symbols[dist(gen())]; });
        return result;
    }
private:
    const IGeneratePWD &m_wrapper;
    size_t m_number_of_symbols = 1;
};

// ############################################################################
// ############################################################################

int main(int /***argc*/, char * * /***argv*/)
{
    fmt::print("{:>40}: {}\n", "Base password length 10", GeneratePWD(10).create());
    fmt::print("{:>40}: {}\n", "adding 3 uppercase characters", UppercasePWD(GeneratePWD(10), 3).create());
    fmt::print("{:>40}: {}\n", "adding 3 digits", DigitPWD(GeneratePWD(10), 3).create());
    fmt::print("{:>40}: {}\n", "adding 1 symbol", SymbolPWD(GeneratePWD(10)).create());
    fmt::print("{:>40}: {}\n", "adding 1 symbol and 2 digits", DigitPWD(SymbolPWD(GeneratePWD(10)), 2).create());
    fmt::print("{:>40}: {}\n", "adding symbol, digits and uppercase", UppercasePWD(DigitPWD(SymbolPWD(GeneratePWD(10)), 2), 4).create());
    fmt::print("{:>40}: {}\n", "all decorators", LowercasePWD(UppercasePWD(DigitPWD(SymbolPWD(GeneratePWD(10)), 2), 10), 3).create());
    return 0;
}

