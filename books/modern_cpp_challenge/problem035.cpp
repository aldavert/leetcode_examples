#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <concepts>
#include <numeric>

/*****************************************************************************/
/* PROBLEM: Computing the size of a directory                                */
/* ------------------------------------------------------------------------- */
/* Write a function that computes the size of a directory, in bytes,         */
/* recursively, It should be possible to indicate whether symbolic links     */
/* should be followed or not.                                                */
/*****************************************************************************/

// ############################################################################
// ############################################################################

size_t problem(std::filesystem::path path, bool follow_symlinks = false)
{
    auto iterator = std::filesystem::recursive_directory_iterator(path,
            follow_symlinks?std::filesystem::directory_options::follow_directory_symlink:
                            std::filesystem::directory_options::none);
    size_t result = 0;
    for (const auto &entry : iterator)
        if (entry.is_regular_file())
            result += entry.file_size();
    return result;
}

// ############################################################################
// ############################################################################

size_t solution(std::filesystem::path path, bool follow_symlinks = false)
{
    auto iterator = std::filesystem::recursive_directory_iterator(path,
            follow_symlinks?std::filesystem::directory_options::follow_directory_symlink:
                            std::filesystem::directory_options::none);
    return std::accumulate(
            std::filesystem::begin(iterator),
            std::filesystem::end(iterator),
            0ull,
            [](std::uintmax_t const total, const std::filesystem::directory_entry &entry)
            {
                return total + (std::filesystem::is_regular_file(entry)?
                        std::filesystem::file_size(entry.path()):0);
            });
}

// ############################################################################
// ############################################################################

int main(int argc, char * * argv)
{
    if (argc < 2)
    {
        fmt::print("[ERROR] No path to folder passed as parameter.");
        return -1;
    }
    std::locale::global(std::locale("de_CH.utf8"));
    bool symbolic = false;
    for (int i = 1; i < argc; ++i)
    {
        if (std::strcmp(argv[i], "--symbolic") == 0)
            symbolic = true;
        else
        {
            std::filesystem::path p = argv[i];
            try
            {
                fmt::print("Size to '{}' with [OWN]: {:L}\n", argv[i], problem(p, symbolic));
            }
            catch (...)
            {
                fmt::print("Could not process '{}' for [OWN].\n", argv[i]);
            }
            try
            {
                fmt::print("Size to '{}' with [BOOK]: {:L}\n", argv[i], solution(p, symbolic));
            }
            catch (...)
            {
                fmt::print("Could not process '{}' for [BOOK].\n", argv[i]);
            }
        }
    }
    
    return 0;
}
