#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

/*****************************************************************************/
/* PROBLEM: Sum of naturals divisible by 3 and 5.                            */
/* ------------------------------------------------------------------------- */
/* Write a program that calculates and prints the sum of all the natural     */
/* numbers divisible by either 3 or 5, up to a given limit entered by the    */
/* user.                                                                     */
/*****************************************************************************/

// ############################################################################
// ############################################################################

long problem(unsigned int limit)
{
    const int range_size = 15;
    const int range_valid = 7;
    const int range_accum[] =
                        {0, 0, 0, 3, 3, 8, 14, 14, 14, 23, 33, 33, 45, 45, 45, 60};
    const int range_count[] =
                        {0, 0, 0, 1, 1, 2,  3,  3,  3,  4,  5,  5,  6,  6,  6,  7};
    --limit;
    long d = limit / range_size;
    long r = limit - d * range_size;
    return d * range_accum[range_size]
         + range_size * range_valid * d * (d - 1) / 2
         + range_size * d * range_count[r] + range_accum[r];
}

// ############################################################################
// ############################################################################

long solution(unsigned int limit)
{
    long sum = 0;
    for (unsigned int i = 3; i < limit; ++i)
    {
        if (i % 3 == 0 || i % 5 == 0)
            sum += i;
    }
    return sum;
}

// ############################################################################
// ############################################################################

void test(unsigned int limit, long solution, unsigned int trials = 1)
{
    long result = -1;
    for (unsigned int i = 0; i < trials; ++i)
        result = problem(limit);
    if (solution != result)
        std::cout << "[FAILURE] expected " << solution << " and obtained " << result << ".\n";
}

int main(int argc, char * * argv)
{
    if ((argc > 1) && (std::strcmp(argv[1], "time") == 0))
    {
#if 1
        constexpr unsigned int trials = 10'000'000;
        //constexpr unsigned int trials = 1'000'000;
#else
        constexpr unsigned int trials = 1;
#endif
        for (unsigned int l = 1; l < 1'000'000; l += 1'000)
            test(l, solution(l), trials);
    }
    else
    {
        unsigned int limit = 0;
        std::cout << "Upper limit: ";
        std::cin >> limit;
        auto result = problem(limit);
        std::cout << "Obtained result = " << result;
        if (auto sol = solution(limit); sol == result)
            std::cout << " (correct)\n";
        else std::cout << " (" << sol << " was expected)\n";
    }
    return 0;
}
