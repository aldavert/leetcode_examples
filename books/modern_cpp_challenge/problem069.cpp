#include <vector>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>
#include <bitset>

/*****************************************************************************/
/* PROBLEM: Generating social security numbers                               */
/* ------------------------------------------------------------------------- */
/* Write a program that can generate social security numbers for two         */
/* countries, Northeria and Southeria, that have different but similar       */
/* formats for the numbers:                                                  */
/*     - In Notheria, the number have the format SYYYYMMDDNNNNNC, where S is */
/*       a digit representing the sex, 9 for females and 7 for males,        */
/*       YYYYMMDD is the birth date, NNNNN is a five-digit random number,    */
/*       unique for a day (meaning, that the same number can appear twice    */
/*       for different dates, but not the same date), and C is a digit       */
/*       picked so that the checksum computed as described later is a        */
/*       multiple of 11.                                                     */
/*     - In Southeria, the numbers have the format SYYYYMMDDNNNNC, where S   */
/*       is a digit representing the sex, 1 for females and 2 for males,     */
/*       YYYYMMDD is the birth date, NNNN is a four-digit random number,     */
/*       unique for a day, and C is a digit picket so that the checksum      */
/*       computed as described below is a multiple of 10.                    */
/* The checksum in both cases is a sum of all the digits, each multiplied by */
/* its weight (the position from the most significant digit to the least).   */
/* For example, the checksum for the Southerian number 12017120134895 is     */
/* computed as follows:                                                      */
/*      crc = 14 * 1 + 13 * 2 + 12 * 0 + 11 * 1 + 10 * 7 + 9 * 1 + 8 * 2     */
/*          + 7 * 0 + 6 * 1 + 5 * 3 + 4 * 4 + 3 * 8 + 2 * 9 + 1 * 5          */
/*          = 230 = 23 * 10                                                  */
/*****************************************************************************/

// ############################################################################
// ############################################################################

using namespace std::literals::chrono_literals;
using std::chrono::January;
using std::chrono::February;
using std::chrono::March;
using std::chrono::April;
using std::chrono::May;
using std::chrono::June;
using std::chrono::July;
using std::chrono::August;
using std::chrono::September;
using std::chrono::October;
using std::chrono::November;
using std::chrono::December;
enum class Country { Northeria, Southeria };
enum class Sex { Male, Female };

class SocialSecurity
{
public:
    // This makes the function pure virtual.
    virtual ~SocialSecurity(void) = 0;
    std::string generate(Sex sex, std::chrono::year year, std::chrono::month month, std::chrono::day day)
    {
        std::ostringstream oss;
        oss << ((sex == Sex::Female)?m_female:m_male);
        oss << fmt::format("{}{:02d}{:02d}",
                static_cast<int>(year),
                static_cast<unsigned int>(month),
                static_cast<unsigned int>(day));
        unsigned int key = static_cast<unsigned int>(day)
                         + static_cast<unsigned int>(month) * 100
                         + static_cast<int>(year) * 10'000;
        int random_number = -1;
        if (auto current = m_random.find(key); current != m_random.end())
        {
            while (current->second.find(random_number = m_dist(m_gen)) != current->second.end());
            current->second.insert(random_number);
        }
        else m_random[key].insert(random_number = m_dist(m_gen));
        oss << fmt::format("{:0{}d}", random_number, m_random_length);
        std::string code = oss.str();
        size_t crc = 0;
        for (size_t i = code.size(); i > 0; --i)
            crc += i * (code[i - 1] - '0');
        if (crc % m_modulo == 0)
            code += "0";
        else code += std::to_string(m_modulo - crc % m_modulo);
        
        return code;
    }
protected:
    SocialSecurity(char female, char male, size_t random_length, size_t modulo) :
        m_female(female),
        m_male(male),
        m_random_length(random_length),
        m_modulo(modulo),
        m_gen(m_rd()),
        m_dist(0, std::pow(10, random_length) - 1) {}
    std::unordered_map<unsigned int, std::unordered_set<unsigned int> > m_random;
    char m_female;
    char m_male;
    size_t m_random_length;
    size_t m_modulo;
    std::random_device m_rd;
    std::mt19937 m_gen;
    std::uniform_int_distribution<> m_dist;
};

SocialSecurity::~SocialSecurity(void) {}

class NortheriaSocialSecurity : public SocialSecurity
{
public:
    NortheriaSocialSecurity(void) :
        SocialSecurity('9', '7', 5, 11) {}
};

class SoutheriaSocialSecurity : public SocialSecurity
{
public:
    SoutheriaSocialSecurity(void) :
        SocialSecurity('1', '2', 4, 10) {}
};

// ############################################################################
// ############################################################################

int main(int /***argc*/, char * * /***argv*/)
{
    NortheriaSocialSecurity north_social;
    SoutheriaSocialSecurity south_social;
    
    std::cout << north_social.generate(Sex::Female, 1990y,    May, 11d) << '\n';
    std::cout << north_social.generate(Sex::Male  , 1990y,    May, 11d) << '\n';
    std::cout << north_social.generate(Sex::Female, 2001y,   June,  1d) << '\n';
    std::cout << north_social.generate(Sex::Male  , 2001y,   June,  1d) << '\n';
    std::cout << north_social.generate(Sex::Female, 1989y,   July, 14d) << '\n';
    std::cout << north_social.generate(Sex::Male  , 1989y,   July, 14d) << '\n';
    std::cout << north_social.generate(Sex::Female, 1985y, August,  3d) << '\n';
    std::cout << north_social.generate(Sex::Male  , 1985y, August,  3d) << '\n';
    std::cout << south_social.generate(Sex::Female, 1990y,    May, 11d) << '\n';
    std::cout << south_social.generate(Sex::Male  , 1990y,    May, 11d) << '\n';
    std::cout << south_social.generate(Sex::Female, 2001y,   June,  1d) << '\n';
    std::cout << south_social.generate(Sex::Male  , 2001y,   June,  1d) << '\n';
    std::cout << south_social.generate(Sex::Female, 1989y,   July, 14d) << '\n';
    std::cout << south_social.generate(Sex::Male  , 1989y,   July, 14d) << '\n';
    std::cout << south_social.generate(Sex::Female, 1985y, August,  3d) << '\n';
    std::cout << south_social.generate(Sex::Male  , 1985y, August,  3d) << '\n';
    return 0;
}

