#include <vector>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>

/*****************************************************************************/
/* PROBLEM: Transforming a list of phone numbers                             */
/* ------------------------------------------------------------------------- */
/* Write a function that, given a list of phone numbers, transforms them so  */
/* they all start with a specified phone country code, preceded by the +     */
/* sign. Any whitespaces from a phone number should also be removed. The     */
/* following is a list of input and output examples:                         */
/*    07555 123456 -->  +447555123456                                        */
/*     07555123456 -->  +447555123456                                        */
/* +44 7555 123456 -->  +447555123456                                        */
/*  44 7555 123456 -->  +447555123456                                        */
/*     7555 123456 -->  +447555123456                                        */
/*****************************************************************************/

// ############################################################################
// ############################################################################

std::vector<std::string> transformPhoneNumbers(
        const std::vector<std::string> &numbers,
        std::string code)
{
    std::string to_remove = " +0";
    std::vector<std::string> result;
    code.erase(0, code.find_first_not_of(to_remove));
    for (std::string number : numbers)
    {
        number.erase(0, number.find_first_not_of(to_remove));
        if (number.substr(0, code.size()) == code)
            number = number.substr(2);
        std::string transform;
        for (char c : number)
            if (std::isdigit(c))
                transform += c;
        result.push_back("+" + code + transform);
    }
    return result;
}

// ############################################################################
// ############################################################################

int main(int /***argc*/, char * * /***argv*/)
{
    std::vector<std::string> numbers = {"07555 123456", "07555123456",
        "+44 7555 123456", " 44 7555 123456", "7555 123456" };
    auto result = transformPhoneNumbers(numbers, "44");
    fmt::print("List of filtered numbers:\n");
    fmt::print("-------------------------------------\n");
    for (const auto &r : result)
        fmt::print("{}\n", r);
    fmt::print("-------------------------------------\n\n");
    return 0;
}

