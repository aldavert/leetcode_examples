#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>
#include <array>
#include <functional>

/*****************************************************************************/
/* PROBLEM: Minimum function with any number of arguments.                   */
/* ------------------------------------------------------------------------- */
/* Write a function template that can take any number of arguments and       */
/* returns the minimum value of them all, using operator< for comparison.    */
/* Write a variant of this function template that can be parameterized by    */
/* a binary comparison function to use instead of operator<.                 */
/*****************************************************************************/

// ############################################################################
// ############################################################################


template <typename T>
T min(T a, T b)
{
    if (a < b) return a;
    else return b;
}
template <typename T, typename... Ts>
T min(T a, Ts... other)
{
    return min(a, min(other...));
}
template <typename T, typename Compare>
requires std::invocable<Compare&, T, T>
T min(Compare cmp, T a, T b)
{
    if (cmp(a, b)) return a;
    else return b;
}
template <typename T, typename... Ts, typename Compare>
requires std::invocable<Compare&, T, T>
T min(Compare cmp, T a, Ts... other)
{
    return min(cmp, a, min(cmp, other...));
}

int main(int argc, char * * argv)
{
    std::cout << "Minimum: " << min(1, 2) << '\n';
    std::cout << "Minimum: " << min(1, 2, 3, 4) << '\n';
    std::cout << "Minimum: " << min(2, 1) << '\n';
    std::cout << "Minimum: " << min(4, 3, 2, 1) << '\n';
    
    std::cout << "Maximum: " << min([](int a, int b) { return a > b; }, 1, 2) << '\n';
    std::cout << "Maximum: " << min([](int a, int b) { return a > b; }, 2, 1) << '\n';
    std::cout << "Maximum: " << min([](int a, int b) { return a > b; }, 1, 2, 3, 4) << '\n';
    std::cout << "Maximum: " << min([](int a, int b) { return a > b; }, 4, 3, 2, 1) << '\n';
    return 0;
}
