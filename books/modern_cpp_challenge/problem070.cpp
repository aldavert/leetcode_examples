#include <vector>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <thread>
#include <fmt/core.h>
#include <concepts>
#include <numeric>
#include <functional>
#include <random>
#include <mutex>
#include <ranges>
#include <list>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>
#include <bitset>

/*****************************************************************************/
/* PROBLEM: Approval system                                                  */
/* ------------------------------------------------------------------------- */
/* Write a program for a purchasing department of a company that allows      */
/* employees to approve new purchases (or expenses). However, based on their */
/* position, each employee may only approve expenses up to a predefined      */
/* limit. For instance, regular employees can approve expenses up to 1'000   */
/* currency units, team managers up to 10'000, and the department manager    */
/* up to 100'000. Any expense greater than that must be explicitly approved  */
/* by the company president.                                                 */
/*****************************************************************************/

// ############################################################################
// ############################################################################

struct Expense
{
    std::string description;
    unsigned int price;
};

std::ostream& operator<<(std::ostream &out, const Expense &ex)
{
    std::locale::global(std::locale("de_CH.utf8"));
    out << fmt::format("{{{}: {:9L}}}", ex.description, ex.price);
    return out;
}

class Employee
{
public:
    virtual ~Employee(void) = 0;
    inline const std::string& name(void) const noexcept { return m_name; }
    void purchase(Expense e) const
    {
        if (e.price <= m_limit)
        {
            std::cout << "Purchase of " << e << " has been approved by";
            if (m_position_name.size() > 0)
                std::cout << ' ' << m_position_name;
            std::cout << ' ' << m_name << '\n';
        }
        else if (m_supervisor != nullptr)
            m_supervisor->purchase(e);
        else std::cout << "Expense " << e << " cannot be approved.\n";
    }
protected:
    Employee(std::string name, std::string position, unsigned int limit, const Employee * supervisor = nullptr) :
        m_name(name),
        m_position_name(position),
        m_limit(limit),
        m_supervisor(supervisor) {}
    std::string m_name;
    std::string m_position_name;
    unsigned int m_limit = 0;
    const Employee * m_supervisor = nullptr;
};

Employee::~Employee(void) {}

class Regular : public Employee
{
public:
    Regular(std::string name) :
        Employee(name, "", 1'000) {}
    Regular(std::string name, const Employee &supervisor) :
        Employee(name, "", 1'000, &supervisor) {}
};

class TeamManager : public Employee
{
public:
    TeamManager(std::string name) :
        Employee(name, "team manager", 10'000) {}
    TeamManager(std::string name, const Employee &supervisor) :
        Employee(name, "team manager", 10'000, &supervisor) {}
};

class DepartmentManager : public Employee
{
public:
    DepartmentManager(std::string name) :
        Employee(name, "department manager", 100'000) {}
    DepartmentManager(std::string name, const Employee &supervisor) :
        Employee(name, "department manager", 100'000, &supervisor) {}
};

class President : public Employee
{
public:
    President(std::string name) :
        Employee(name, "president", std::numeric_limits<unsigned int>::max()) {}
};

// ############################################################################
// ############################################################################

int main(int /***argc*/, char * * /***argv*/)
{
    President president("John Wick");
    DepartmentManager managers[] = { {"Manager Primer", president},
                                     {"Manager Segon" , president} };
    TeamManager teams[] = { {"Team Premier"  , managers[0]},
                            {"Team Deuxième" , managers[0]},
                            {"Team Troisième", managers[1]},
                            {"Team Quatirème", managers[1]} };
    Regular workers[] = { {"Worker First"  , teams[0]},
                          {"Worker Second" , teams[0]},
                          {"Worker Third"  , teams[0]},
                          {"Worker Fourth" , teams[1]},
                          {"Worker Fifth"  , teams[1]},
                          {"Worker Sixth"  , teams[2]},
                          {"Worker Seventh", teams[2]},
                          {"Worker Eighth" , teams[3]},
                          {"Worker Ninth"  , teams[3]},
                          {"Worker Tenth"  , teams[3]} };
    for (unsigned int i = 0; i < 20; ++i)
    {
        Expense e{fmt::format("Item {}", static_cast<char>('A' + i)),
                  static_cast<unsigned int>(std::pow(10, i % 7))};
        workers[i % 10].purchase(e);
    }
    return 0;
}

