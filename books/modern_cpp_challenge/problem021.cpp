#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>
#include <chrono>
#include <fmt/core.h>
#include <locale>
#include <unordered_map>
#include <random>
#include <concepts>
#include <sstream>
#include <array>
#include <functional>
#include <list>
#include <unistd.h>
#include <fcntl.h>

/*****************************************************************************/
/* PROBLEM: System handle wrapper.                                           */
/* ------------------------------------------------------------------------- */
/* Write an operating system handle, such as a file handle. Write a wrapper  */
/* that handles the acquisition and release of the handle, as well as other  */
/* operations such as verifying the validity of the handle and moving handle */
/* ownership one object to another.                                          */
/*****************************************************************************/

// ############################################################################
// ############################################################################

class UniqueHandle
{
    int m_id;
public:
    // Disable copy constructor and assignation operator.
    UniqueHandle(const UniqueHandle &) = delete;
    UniqueHandle& operator=(const UniqueHandle &) = delete;
    // Initialization constructor.
    explicit UniqueHandle(int id = -1) noexcept : m_id(id) {}
    // Move constructor and operator.
    UniqueHandle(UniqueHandle &&other) noexcept : m_id(other.release()) {}
    UniqueHandle& operator=(UniqueHandle &&other) noexcept
    {
        if (this != &other) reset(other.release());
        return *this;
    }
    // Destructor.
    ~UniqueHandle(void) noexcept
    {
        if (m_id != -1) close(m_id);
    }
    // Access functions
    explicit operator bool(void) const noexcept
    {
        return m_id != -1;
    }
    int get(void) const noexcept { return m_id; }
    int release(void) noexcept { return std::exchange(m_id, -1); }
    bool reset(int id = -1) noexcept
    {
        if (m_id != id)
        {
            if (m_id != -1) close(m_id);
            m_id = id;
        }
        return static_cast<bool>(*this);
    }
    void swap(UniqueHandle &other) noexcept
    {
        std::swap(m_id, other.m_id);
    }
};

void call(const char * filename)
{
    UniqueHandle handle(open(filename, O_RDONLY));
    std::vector<char> buffer(4096);
    unsigned long bytesRead = read(handle.get(), buffer.data(), buffer.size());
    if (static_cast<bool>(handle))
    {
        std::cout << "Bytes read: " << bytesRead << '\n';
        std::cout << "===[ BUFFER ]==============================================\n";
        std::cout << buffer.data() << '\n';
        std::cout << "===========================================================\n";
    }
    throw std::invalid_argument("This is an exception call.");
}

int main(int argc, char * * argv)
{
    try
    {
        call("problem021.cpp");
    }
    catch (std::invalid_argument &)
    {
    }
    try
    {
        call("problem021");
    }
    catch (std::invalid_argument &)
    {
    }
    return 0;
}
