#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <fmt/core.h>
#include <fmt/color.h>
#include <bitset>
#include <cctype>
#include <algorithm>
#include <unordered_map>
#include <utility>

inline bool isSkipSymbol(char symbol)
{
    return (symbol == ' ')
       ||  (symbol == '\t')
       ||  (symbol == '\b')
       ||  (symbol == '\r')
       ||  (symbol == '\a');
}

inline std::string toUppercase(std::string text)
{
    std::transform(text.begin(), text.end(), text.begin(),
                   [](char symbol) { return static_cast<char>(std::toupper(symbol)); });
    return text;
}

bool cmp(const std::string &left, const std::string &right)
{
    return std::equal(left.begin(), left.end(),
                      right.begin(), right.end(),
                      [](char l, char r) -> bool
                      { return std::tolower(l) == std::tolower(r); });
}

std::tuple<int, int> parenthesisValue(const std::string &command, int start)
{
    const int n = static_cast<int>(command.size());
    if (command[start] != '(')
        return {-1, -1};
    int end = start + 1;
    while ((end < n) && (command[end] != ')')) ++end;
    int value = std::atoi(command.substr(start + 1, end - start - 1).c_str());
    ++end;
    while ((end < n) && isSkipSymbol(command[end])) ++end;
    return {value, end};
}

int isValid(const std::string &command,
            std::string expected_begin,
            std::string expected_end)
{
    const size_t minimum_length = expected_begin.size() + expected_end.size();
    if (command.size() <= minimum_length)
        return -1;
    if (command.substr(0, expected_begin.size()) != expected_begin)
        return -1;
    if (command.substr(command.size() - expected_end.size()) != expected_end)
        return -1;
    return std::atoi(command.substr(expected_begin.size(), command.size() - minimum_length).c_str());
}

void addInstruction(std::bitset<40> * system_memory,
                    int program_counter,
                    std::bitset<20> instruction)
{
    bool left = (program_counter & 1) == 0;
    program_counter /= 2;
    for (size_t i = 0, j = 20 * left; i < 20; ++i, ++j)
        system_memory[program_counter][j] = instruction[i];
}

std::string memoryInstruction(std::bitset<40> * memory, size_t position, bool left)
{
    std::unordered_map<unsigned char, std::tuple<std::string, std::string> > instruction_set =  {
        {0x0A, {"LOAD MQ", ""}},
        {0x14, {"LSH", ""}},
        {0x15, {"RSH", ""}},
        {0xFF, {"STOP", ""}},
        {0xF0, {"DISP AC", ""}},
        {0xF2, {"DISP MQ", ""}},
        {0xF1, {"DHEX AC", ""}},
        {0xF3, {"DHEX MQ", ""}},
        {0x09, {"LOAD MQ,M(", ")"}},
        {0x21, {"STOR M(", ")"}},
        {0x01, {"LOAD M(", ")"}},
        {0x02, {"LOAD -M(", ")"}},
        {0x03, {"LOAD |M(", ")|"}},
        {0x04, {"LOAD -|M(", ")|"}},
        {0x0D, {"JUMP M(", ",0:19)"}},
        {0x0E, {"JUMP M(", ",20:39)"}},
        {0x0F, {"JUMP +M(", ",0:19)"}},
        {0x10, {"JUMP +M(", ",20:39)"}},
        {0x05, {"ADD M(", ")"}},
        {0x07, {"ADD |M(", ")|"}},
        {0x06, {"SUB M(", ")"}},
        {0x08, {"SUB |M(", ")|"}},
        {0x0B, {"MUL M(", ")"}},
        {0x0C, {"DIV M(", ")"}},
        {0x12, {"STOR M(", ",8:19)"}},
        {0x13, {"STOR M(", ",28:39)"}},
        {0xE0, {"INPUT M(", ")"}},
        {0xE1, {"INHEX M(", ")"}},
        {0xF4, {"DISP M(", ")"}},
        {0xF5, {"DHEX M(", ")"}}
    };
    unsigned char instruction_code = 0;
    int address = 0, idx = 39 - 20 * !left;
    for (int i = 0; i < 8; ++i, --idx)
        instruction_code = (instruction_code << 1) | memory[position][idx];
    for (int i = 0; i < 12; ++i, --idx)
        address = (address << 1) | memory[position][idx];
    if (auto search = instruction_set.find(instruction_code);
        search != instruction_set.end())
    {
        auto [text_left, text_right] = search->second;
        if (text_right.empty())
            return text_left;
        return text_left + std::to_string(address) + text_right;
    }
    else return "<UNKNOWN>";
}

void run(std::bitset<40> * memory)
{
    auto toValue = [](std::bitset<40> bits) -> long
    {
        bool negative = bits[39];
        bits[39] = false;
        return (1 - 2 * negative) * static_cast<long>(bits.to_ulong());
    };
    auto toBits = [](long value) -> std::bitset<40>
    {
        bool negative = value < 0;
        if (value < 0) value = -value;
        std::bitset<40> result = value;
        result[39] = negative;
        return result;
    };
    std::bitset<40> ac, mq;
    int program_counter = 0;
    std::cout << "\n\n";
    while (true)
    {
        unsigned char code = 0;
        int address = 0, idx = 39 - 20 * ((program_counter & 1) == 1);
        for (int i = 0; i < 8; ++i, --idx)
            code = (code << 1) | memory[program_counter / 2][idx];
        for (int i = 0; i < 12; ++i, --idx)
            address = (address << 1) | memory[program_counter / 2][idx];
        
        //std::cout << program_counter << ' ';
        //std::cout << std::hex << (int)code << std::dec << ' ';
        //std::cout << memoryInstruction(memory, program_counter / 2, !(program_counter & 1)) << '\n';
        switch (code)
        {
        case 0x0A: // LOAD MQ: Transfer MQ to AC
            ac = mq;
            ++program_counter;
            break;
        case 0x14: // LSH: Shift AC one bit to the left.
        {
            bool sign = ac[39];
            ac <<= 1;
            ac[39] = sign;
            ++program_counter;
            break;
        }
        case 0x15: // RSH: Shift AC one bit to the right.
        {
            bool sign = ac[39];
            ac[39] = false;
            ac >>= 1;
            ac[39] = sign;
            ++program_counter;
            break;
        }
        case 0xFF: // STOP: Stops the program execution.
            return;
        case 0xF0: // DISP AC: Displays the AC.
            fmt::print("AC(decimal)={}\n", toValue(ac));
            ++program_counter;
            break;
        case 0xF2: // DISP MQ: Displays the MQ.
            fmt::print("MQ(decimal)={}\n", toValue(mq));
            ++program_counter;
            break;
        case 0xF1: // DHEX AC: Displays the AC in hexadecimal.
            fmt::print("AC(hexadecimal)={:x}\n", ac.to_ulong());
            ++program_counter;
            break;
        case 0xF3: // DHEX MQ: Displays the MQ in hexadecimal.
            fmt::print("MQ(hexadecimal)={:x}\n", mq.to_ulong());
            ++program_counter;
            break;
        case 0x09: // LOAD MQ,M(x): Transfer M(X) to MQ.
            mq = memory[address];
            ++program_counter;
            break;
        case 0x21: // STOR M(x): Transfers AC to M(X).
            memory[address] = ac;
            ++program_counter;
            break;
        case 0x01: // LOAD M(x): Transfers M(X) to AC.
            ac = memory[address];
            ++program_counter;
            break;
        case 0x02: // LOAD -M(x): Transfers -M(X) to AC.
            ac = memory[address];
            ac[39] = !ac[39];
            ++program_counter;
            break;
        case 0x03: // LOAD |M(x)|: Transfers |M(X)| to AC.
            ac = memory[address];
            ac[39] = false;
            ++program_counter;
            break;
        case 0x04: // LOAD -|M(x)|: Transfers -|M(X)| to AC.
            ac = memory[address];
            ac[39] = true;
            ++program_counter;
            break;
        case 0x0D: // JUMP M(x,0:19): Jump to the left instruction of M(X).
            program_counter = address * 2;
            break;
        case 0x0E: // JUMP M(x,20:39): Jump to the right instruction to M(X).
            program_counter = address * 2 + 1;
            break;
        case 0x0F: // JUMP +M(x,0:19): If AC >= 0, jump to the left instruction of M(X)
            if (toValue(ac) >= 0)
                program_counter = address * 2;
            else ++program_counter;
            break;
        case 0x10: // JUMP +M(x,20:39): If AC >= 0, jump to the right instruction of M(X)
            if (toValue(ac) >= 0)
                program_counter = address * 2 + 1;
            else ++program_counter;
            break;
        case 0x05: // ADD M(x): Add M(X) to AC and put the result in AC.
            ac = toBits(toValue(memory[address]) + toValue(ac));
            ++program_counter;
            break;
        case 0x07: // ADD |M(x)|: Add |M(X)| to AC and put the result in AC.
        {
            long value = toValue(memory[address]);
            if (value < 0) value = -value;
            ac = toBits(toValue(ac) + value);
            ++program_counter;
            break;
        }
        case 0x06: // SUB M(x): Subtract M(X) from AC and put the result in AC.
            ac = toBits(toValue(ac) - toValue(memory[address]));
            ++program_counter;
            break;
        case 0x08: // SUB |M(x)|: Subtract |M(X)| from AC and put the result in AC.
        {
            long value = toValue(memory[address]);
            if (value < 0) value = -value;
            ac = toBits(toValue(ac) - value);
            ++program_counter;
            break;
        }
        case 0x0B: // MUL M(x): Multiply M(X) by MQ and puts the resulting most
                   // significant bits in AC and least significant bits in MQ.
        {
            bool sign = memory[address][39] ^ mq[39];
            std::bitset<80> result, number = std::abs(toValue(memory[address]));
            number[39] = false;
            for (size_t i = 0; i < 39; ++i, number <<= 1)
            {
                if (mq[i])
                {
                    bool carry = false;
                    for (size_t j = 0; j < 80; ++j)
                    {
                        bool current = result[j] ^ number[j] ^ carry;
                        carry = (result[j] && number[j])
                             || (result[j] && carry)
                             || (number[j] && carry);
                        result[j] = current;
                    }
                }
            }
            size_t i = 0;
            for (; i < 39; ++i)
                mq[i] = result[i];
            for (; i < 39 + 39; ++i)
                ac[i - 39] = result[i];
            ac[39] = mq[39] = sign;
            ++program_counter;
            break;
        }
        case 0x0C: // DIV M(x): Divide AC by M(X); put the quotient in MQ and the
                   // remainder in AC.
        {
            long divident = toValue(ac), divisor = toValue(memory[address]);
            mq = toBits(divident / divisor);
            ac = toBits(divident % divisor);
            ++program_counter;
            break;
        }
        case 0x12: // STOR M(x,8:19): Store the 12 right most bits of AC into the
                   // left address field at M(X).
            for (int i = 40 - 9, k = 11; i >= 40 - 20; --i, --k)
                memory[address][i] = ac[k];
            ++program_counter;
            break;
        case 0x13: // STOR M(x,28:39): Store the 12 right most bits of AC into the
                   // right address field at M(X).
            for (int i = 20 - 9, k = 11; i >= 0; --i, --k)
                memory[address][i] = ac[k];
            ++program_counter;
            break;
        case 0xE0: // INPUT M(x): Stores the user's input decimal value into M(X).
        {
            long value = 0;
            fmt::print("Insert ");
            fmt::print(fmt::emphasis::bold, "DECIMAL");
            fmt::print(" value: ");
            std::cin >> value;
            memory[address] = toBits(value);
            ++program_counter;
            break;
        }
        case 0xE1: // INHEX M(x): Stores the user's input hexadecimal value into M(X).
        {
            long value = 0;
            fmt::print("Insert ");
            fmt::print(fmt::emphasis::bold, "HEXADECIMAL");
            fmt::print(" value: ");
            std::cin >> std::hex >> value >> std::dec;
            memory[address] = toBits(value);
            ++program_counter;
            break;
        }
        case 0xF4: // DISP M(x): Display content of M(X).
            fmt::print("M[{}](decimal)={}\n", address, toValue(memory[address]));
            ++program_counter;
            break;
        case 0xF5: // DHEX M(x): Display content of M(X) in hexadecimal.
            fmt::print("M[{}](hexadecimal)={:x}\n", address, memory[address].to_ulong());
            ++program_counter;
            break;
        default:
            fmt::print(fg(fmt::color::red) | fmt::emphasis::bold, "[ERROR] Unknown instruction at PC={} (memory: {}-{}).\n", program_counter, program_counter / 2, ((program_counter & 1)?"RIGHT":"LEFT"));
            return;
        };
    }
}

int main(int argc, char * * argv)
{
    std::bitset<40> system_memory[4096];
    fmt::print(fmt::emphasis::bold | bg(fmt::color::dark_orange) | fg(fmt::color::black), "Institute of Advanced Studies (IAS) computer emulator.");
    fmt::print("\n");
    if (argc == 1)
    {
        fmt::print("\n");
        fmt::print("$ ./ias (-inset)* <file>\n");
        fmt::print("\n");
        fmt::print("Where:\n");
        fmt::print("-inset Displays the IAS instruction set and its definition.\n");
        fmt::print("<file> is the route to the file containing the IAS assembly code program.\n");
        return EXIT_SUCCESS;
    }
    bool display_instructions = false;
    std::string filename = "";
    for (int i = 1; i < argc; ++i)
    {
        if (std::strcmp("-inset", argv[i]) == 0)
            display_instructions = true;
        else if (filename.empty())
            filename = argv[i];
        else
        {
            fmt::print("Multiple files cannot be loaded at the same time.\n");
            return EXIT_FAILURE;
        }
    }
    if (display_instructions)
    {
        auto addLine = []([[maybe_unused]] unsigned char code, std::string instruction, std::string description) -> void
        {
            if (!instruction.empty())
            {
                if (code != 0)
                {
                    unsigned char left = code >> 4, right = code & 0x0F;
                    fmt::print(fg(fmt::color::fire_brick), " {:04b} {:04b} ", left, right);
                    fmt::print(bg(fmt::color::maroon), "[0x{:02X}]", code);
                }
                else fmt::print("                 ");
                fmt::print(fg(fmt::color::cadet_blue), " {:<17}", instruction);
            }
            else fmt::print("                                   ");
            fmt::print(fg(fmt::color::yellow_green) | fmt::emphasis::italic, "; {}\n", description);
        };
        fmt::text_style color_line = fg(fmt::color::cornflower_blue);
        fmt::print(color_line | fmt::emphasis::bold, "==[ INFORMATION ]====================================================================\n");
        fmt::print(fmt::emphasis::bold, "  - Registers:");
        fmt::print(fmt::emphasis::italic, " AC=Accumulator, MQ=Multiplier Quotient\n");
        fmt::print(fmt::emphasis::bold, "       - M(X):");
        fmt::print(fmt::emphasis::italic, " Memory at position X\n");
        fmt::print(color_line | fmt::emphasis::bold, "==[ INSTRUCTIONS ]===================================================================\n");
        fmt::print(fmt::emphasis::bold, " Opcode           ASM Instruction  Definition\n");
        fmt::print(color_line, " -[ Data Transfer ]------------------------------------------------------------------\n");
        addLine(0x0A, "LOAD MQ"         , "Transfer MQ to AC");
        addLine(0x09, "LOAD MQ,M(X)"    , "Transfer M(X) to MQ");
        addLine(0x21, "STOR M(X)"       , "Transfer AC to M(X)");
        addLine(0x01, "LOAD M(X)"       , "Transfer M(X) to AC (Value of M(X) as is)");
        addLine(0x02, "LOAD -M(X)"      , "Transfer -M(X) to AC (Value of M(X) negated)");
        addLine(0x03, "LOAD |M(X)|"     , "Transfer |M(X)| to AC (Absolute value of M(X))");
        addLine(0x04, "LOAD -|M(X)|"    , "Transfer -|M(X)| to AC (Negative value of M(X))");
        fmt::print(color_line, " -[ Unconditional branch ]-----------------------------------------------------------\n");
        addLine(0x0D, "JUMP M(X,0:19)"  , "Jump to the left instruction of M(X).");
        addLine(0x0E, "JUMP M(X,20:39)" , "Jump to the right instruction of M(X).");
        fmt::print(color_line, " -[ Conditional branch ]-------------------------------------------------------------\n");
        addLine(0x0F, "JUMP +M(X,0:19)" , "If AC >= 0, jump to left instruction of M(X).");
        addLine(0x10, "JUMP +M(X,20:39)", "If AC >= 0, jump to right instruction of M(X).");
        fmt::print(color_line, " -[ Arithmetic ]---------------------------------------------------------------------\n");
        addLine(0x05, "ADD M(X)"        , "Add M(X) to AC and put the result in AC.");
        addLine(0x07, "ADD |M(X)|"      , "Add the absolute value of M(X) to AC and put the");
        addLine(   0, ""                , "result in AC.");
        addLine(0x06, "SUB M(X)"        , "Subtract M(X) from AC and put the result in AC.");
        addLine(0x08, "SUB |M(X)|"      , "Subtract the absolute value of M(X) from AC and");
        addLine(   0, ""                , "put the result in AC.");
        addLine(0x0B, "MUL M(X)"        , "Multiply M(X) by MQ; put most significant bits");
        addLine(   0, ""                , "of the result in AC and least significant bits");
        addLine(   0, ""                , "in MQ.");
        addLine(0x0C, "DIV M(X)"        , "Divide AC by M(X); put the quotient in MQ and");
        addLine(   0, ""                , "the remainder in AC.");
        addLine(0x14, "LSH"             , "Shift AC one bit to the left.");
        addLine(0x15, "RSH"             , "Shift AC one bit to the right.");
        fmt::print(color_line, " -[ Address modification ]-----------------------------------------------------------\n");
        addLine(0x12, "STOR M(X,8:19)"  , "Store the 12 right most bits of AC into the left");
        addLine(   0, ""                , "address field at M(X).");
        addLine(0x13, "STOR M(X,28:39)" , "Store the 12 right most bits of AC into the");
        addLine(   0, ""                , "right address field at M(X).");
        fmt::print(color_line, " -[ Custom instructions ]------------------------------------------------------------\n");
        addLine(0xE0, "INPUT M(X)"      , "Stores the user's input decimal value into M(X).");
        addLine(0xE1, "INHEX M(X)"      , "Stores the user's input hexadecimal value into");
        addLine(   0, ""                , "M(X).");
        addLine(0xF0, "DISP AC"         , "Displays the AC.");
        addLine(0xF2, "DISP MQ"         , "Displays the MQ.");
        addLine(0xF4, "DISP M(X)"       , "Displays content of M(X).");
        addLine(0xF1, "DHEX AC"         , "Displays the AC in hexadecimal.");
        addLine(0xF3, "DHEX MQ"         , "Displays the MQ in hexadecimal.");
        addLine(0xF5, "DHEX M(X)"       , "Displays content of M(X) in hexadecimal.");
        addLine(0xFF, "STOP"            , "Stops the program execution.");
        fmt::print(color_line, " -[ Memory initialization definitions ]----------------------------------------------\n");
        addLine(0   , ".NUM(X) VALUE"   , "Puts decimal VALUE at the X memory location.");
        addLine(0   , ".HEX(X) VALUE"   , "Puts hexadecimal VALUE at the X memory location.");
        addLine(0   , "; <commentary>"  , "Any text after ';' will be ignored.");
        fmt::print("\n");
        fmt::print("\n");
    }
    if (filename.empty())
        return EXIT_SUCCESS;
    
    fmt::print("Running file: '{}'\n", filename);
    std::ifstream file(filename);
    if (!file.is_open())
    {
        fmt::print("[ERROR] File '{}' cannot be opened.\n", filename);
        return EXIT_FAILURE;
    }
    int program_counter = 0;
    for (std::string line = ""; std::getline(file, line); )
    {
        size_t start = 0, end;
        for (; start < line.size(); ++start)
            if (!isSkipSymbol(line[start]))
                break;
        for (end = start; end < line.size(); ++end)
            if (line[end] == ';')
                break;
        if (end == 0) continue;
        while ((end > 0) && isSkipSymbol(line[end - 1])) --end;
        if (start >= end) continue;
        std::string command = toUppercase(line.substr(start, end - start));
        if (command[0] == '.')
        {
            if ((command.size() < 8)
            ||  (!cmp(command.substr(0, 5).c_str(), ".NUM(")
            &&   !cmp(command.substr(0, 5).c_str(), ".HEX(")))
            {
                fmt::print("Unknown memory initialization instruction: '{}'.\n", command);
                return EXIT_FAILURE;
            }
            auto [memory, next] = parenthesisValue(command, 4);
            if ((memory < 0) || (memory > 4095))
            {
                fmt::print("Memory position in '{}' out of bounds.\n", command);
                return EXIT_FAILURE;
            }
            if (cmp(command.substr(0, 5).c_str(), ".NUM("))
            {
                bool negative = command[next] == '-';
                next += negative;
                system_memory[memory] = std::atoi(command.substr(next).c_str());
                system_memory[memory][39] = negative;
            }
            else
            {
                std::string str_hex = command.substr(next);
                unsigned long value = 0;
                for (char h : str_hex)
                {
                    char hex_chr = static_cast<char>(std::tolower(h));
                    value <<= 4;
                    if ((hex_chr >= '0') && (hex_chr <= '9'))
                        value |= hex_chr - '0';
                    else value |= 10 + (hex_chr - 'a');
                }
                system_memory[memory] = value;
            }
        }
        else if (command.find('(') == std::string::npos)
        {
            if (command == "LOAD MQ")
                addInstruction(system_memory, program_counter++, 0x0A000);
            else if (command == "LSH")
                addInstruction(system_memory, program_counter++, 0x14000);
            else if (command == "RSH")
                addInstruction(system_memory, program_counter++, 0x15000);
            else if (command == "STOP")
                addInstruction(system_memory, program_counter++, 0xFF000);
            else if (command == "DISP AC")
                addInstruction(system_memory, program_counter++, 0xF0000);
            else if (command == "DISP MQ")
                addInstruction(system_memory, program_counter++, 0xF2000);
            else if (command == "DHEX AC")
                addInstruction(system_memory, program_counter++, 0xF1000);
            else if (command == "DHEX MQ")
                addInstruction(system_memory, program_counter++, 0xF3000);
            else
            {
                fmt::print("Unknown command '{}'.\n", command);
                return EXIT_FAILURE;
            }
        }
        else
        {
            int memory = -1;
            if ((memory = isValid(command, "LOAD MQ,M(", ")")) != -1)
                addInstruction(system_memory, program_counter++, 0x09000 | memory);
            else if ((memory = isValid(command, "STOR M(", ",8:19)")) != -1)
                addInstruction(system_memory, program_counter++, 0x12000 | memory);
            else if ((memory = isValid(command, "STOR M(", ",28:39)")) != -1)
                addInstruction(system_memory, program_counter++, 0x13000 | memory);
            else if ((memory = isValid(command, "STOR M(", ")")) != -1)
                addInstruction(system_memory, program_counter++, 0x21000 | memory);
            else if ((memory = isValid(command, "LOAD M(", ")")) != -1)
                addInstruction(system_memory, program_counter++, 0x01000 | memory);
            else if ((memory = isValid(command, "LOAD -M(", ")")) != -1)
                addInstruction(system_memory, program_counter++, 0x02000 | memory);
            else if ((memory = isValid(command, "LOAD |M(", ")|")) != -1)
                addInstruction(system_memory, program_counter++, 0x03000 | memory);
            else if ((memory = isValid(command, "LOAD -|M(", ")|")) != -1)
                addInstruction(system_memory, program_counter++, 0x04000 | memory);
            else if ((memory = isValid(command, "JUMP M(", ",0:19)")) != -1)
                addInstruction(system_memory, program_counter++, 0x0D000 | memory);
            else if ((memory = isValid(command, "JUMP M(", ",20:39)")) != -1)
                addInstruction(system_memory, program_counter++, 0x0E000 | memory);
            else if ((memory = isValid(command, "JUMP +M(", ",0:19)")) != -1)
                addInstruction(system_memory, program_counter++, 0x0F000 | memory);
            else if ((memory = isValid(command, "JUMP +M(", ",20:39)")) != -1)
                addInstruction(system_memory, program_counter++, 0x10000 | memory);
            else if ((memory = isValid(command, "ADD M(", ")")) != -1)
                addInstruction(system_memory, program_counter++, 0x05000 | memory);
            else if ((memory = isValid(command, "ADD |M(", ")|")) != -1)
                addInstruction(system_memory, program_counter++, 0x07000 | memory);
            else if ((memory = isValid(command, "SUB M(", ")")) != -1)
                addInstruction(system_memory, program_counter++, 0x06000 | memory);
            else if ((memory = isValid(command, "SUB |M(", ")|")) != -1)
                addInstruction(system_memory, program_counter++, 0x08000 | memory);
            else if ((memory = isValid(command, "MUL M(", ")")) != -1)
                addInstruction(system_memory, program_counter++, 0x0B000 | memory);
            else if ((memory = isValid(command, "DIV M(", ")")) != -1)
                addInstruction(system_memory, program_counter++, 0x0C000 | memory);
            else if ((memory = isValid(command, "INPUT M(", ")")) != -1)
                addInstruction(system_memory, program_counter++, 0xE0000 | memory);
            else if ((memory = isValid(command, "INHEX M(", ")")) != -1)
                addInstruction(system_memory, program_counter++, 0xE1000 | memory);
            else if ((memory = isValid(command, "DISP M(", ")")) != -1)
                addInstruction(system_memory, program_counter++, 0xF4000 | memory);
            else if ((memory = isValid(command, "DHEX M(", ")")) != -1)
                addInstruction(system_memory, program_counter++, 0xF5000 | memory);
            else if (memory == -1)
            {
                fmt::print("Command '{}' is wrongly formatted.\n", command);
                return EXIT_FAILURE;
            }
        }
    }
    file.close();
    fmt::print(fmt::emphasis::bold | fmt::emphasis::underline, "System Memory:\n");
    fmt::print(" Pos: LEFT RIGHT   Numeric value <Left Inst> <Right Inst>\n");
    fmt::text_style color_left[] = {
        bg(fmt::color::steel_blue) | fg(fmt::color::black) | fmt::emphasis::italic,
        bg(fmt::color::cornflower_blue) | fg(fmt::color::black) | fmt::emphasis::italic
    };
    fmt::text_style color_right[] = {
        bg(fmt::color::cadet_blue) | fg(fmt::color::black) | fmt::emphasis::italic,
        bg(fmt::color::dark_cyan) | fg(fmt::color::black) | fmt::emphasis::italic
    };
    for (size_t i = 0, j = 0; i < 4096; ++i)
    {
        if (system_memory[i].any())
        {
            fmt::print(fmt::emphasis::bold, "{:4d}: ", i);
            fmt::print(fg(fmt::color::steel_blue), "{:05x}",
                    system_memory[i].to_ulong() >> 20);
            fmt::print(fg(fmt::color::cadet_blue), "{:05x}",
                    system_memory[i].to_ulong() & 0x00000000FFFFF);
            fmt::print(fg(fmt::color::gray), " ({:14d}) ",
                    system_memory[i].to_ulong());
            fmt::print(color_left[(j & 1)], "{:>20} ", memoryInstruction(system_memory, i, true));
            fmt::print(color_right[(j & 1)], "{:>20}", memoryInstruction(system_memory, i, false));
            fmt::print("\n");
            ++j;
        }
    }
    
    fmt::print(fmt::emphasis::bold, "\n\nExecuting the program...\n\n");
    run(system_memory);
    return EXIT_SUCCESS;
}


