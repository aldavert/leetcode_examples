# The Institute for Advanced Study (IAS) Machine Simulator

This is a simple emulator for the IAS machine. The emulator is given a text file containing any of the original assembler code instruction (symbolic representation):

#### Data Transfer
| Instruction      | Definition                                                |
|:-----------------|:----------------------------------------------------------|
|`LOAD MQ`| Transfer `MQ` to `AC` |
|`LOAD MQ,M(X)`| Transfer `M(X)` to `MQ` |
|`STOR M(X)`| Transfer `AC` to `M(X)` |
|`LOAD M(X)`| Transfer `M(X)` to `AC` |
|`LOAD -M(X)`| Transfer the negatived value of `M(X)` to `AC`  |
|`LOAD \|M(X)\|`| Transfer the absolute value of `M(X)` to `AC` |
|`LOAD -\|M(X)\|`| Transfer the negative value of `M(X)` to `AC` |

#### Unconditional Branch
| Instruction      | Definition                                                |
|:-----------------|:----------------------------------------------------------|
|`JUMP M(X,0:19)`| Jump to the left instruction of `M(X)`.  |
|`JUMP M(X,20:39)`| Jump to the right instruction of `M(X)`. |

#### Conditional Branch
| Instruction      | Definition                                                |
|:-----------------|:----------------------------------------------------------|
|`JUMP +M(X,0:19) `| If `AC >= 0`, jump to left instruction of `M(X)`. |
|`JUMP +M(X,20:39)`| If `AC >= 0`, jump to right instruction of `M(X)`. |

#### Arithmetic
| Instruction      | Definition                                                |
|:-----------------|:----------------------------------------------------------|
|`ADD M(X)`| Add `M(X)` to `AC` and put the result in `AC`. |
|`ADD \|M(X)\|`| Add the absolute value of `M(X)` to `AC` and put the result in `AC`. |
|`SUB M(X)`| Subtract `M(X)` from `AC` and put the result in `AC`. |
|`SUB \|M(X)\|`| Subtract the absolute value of `M(X)` from `AC` and put the result in `AC`. |
|`MUL M(X)`| Multiply `M(X)` by `MQ`. It puts the most significant bits of the result in `AC` and the least significant bits in `MQ`. |
|`DIV M(X)`| Divide `AC` by `M(X)`; put the quotient in `MQ` and the remainder in `AC`. |
|`LSH`| Shift `AC` one bit to the left. |
|`RSH`| Shift `AC` one bit to the right. |

#### Address Modification
| Instruction      | Definition                                                |
|:-----------------|:----------------------------------------------------------|
|`STOR M(X,8:19)`| Store the 12 right most bits of `AC` into the left address field at `M(X)`. |
|`STOR M(X,28:39)`| Store the 12 right most bits of `AC` into the right address field at `M(X)`. |

#### Custom Instructions
Additionally, the emulator adds several instructions to input and output information out of the simulated machine:

| Instruction      | Definition                                                |
|:-----------------|:----------------------------------------------------------|
|`INPUT M(X)`| Stores the user's input decimal value into `M(X)`. |
|`INHEX M(X)`| Stores the user's input hexadecimal value into `M(X)`. |
|`DISP AC`| Displays the `AC`. |
|`DISP MQ`| Displays the `MQ`. |
|`DISP M(X)`| Displays content of `M(X)`. |
|`DHEX AC`| Displays the `AC` in hexadecimal. |
|`DHEX MQ`| Displays the `MQ` in hexadecimal. |
|`DHEX M(X)`| Displays content of `M(X)` in hexadecimal. |
|`STOP`| Stops the program execution. Without this instruction the program can crash when it reaches a memory position without an instruction. |

#### Memory Initialization Definitions

| Instruction      | Definition                                                |
|:-----------------|:----------------------------------------------------------|
|`.NUM(X) VALUE`| Puts decimal VALUE at the `M(X)` memory location. |
|`.HEX(X) VALUE`| Puts hexadecimal VALUE at the `M(X)` memory location. |

## Creating programs

The emulator translates the assembler instruction into its binary representation, stores them beginning at memory position 0 and starts the program execution at this program. Memory positions can be modified live by the program in order to generate new instructions. The program only stops when the instruction `STOP` is executed. When an unknown instruction is found, the program crashes.

The emulator uses the symbol `;` as commentary start and anything after this symbol is going to be ignored.

## Code example: Bubble Sort

The following [code example](problems/bubble_sort.ias) corresponds to an application for the IAS computer that prompts the user to enter a set of numbers, sorts them using the [bubble sort](https://en.wikipedia.org/wiki/Bubble_sort) algorithm and prints the obtained result into screen.

To gather and show information, the code uses custom instruction of the emulator. The program also expects certain memory positions to be previously initialized to the following values:

| Position | Value | Definition |
|:--------:|:-----:|:-----------|
|1000|100|Maximum number of points in the user's inputted values array.|
|1001|1|Value 1.|
|1002|0|Value 0.|
|1004|1025|Base position of the array containing the user's inputted values.|

#### Loading information
First, the program asks the user how many points it wants to sort and checks if the given value is smaller than 100.
```
INPUT M(1024)
LOAD M(1000)
SUB M(1024)
JUMP +M(2,20:39)
STOP
```

The value is stored in memory position 1024. Then a copy stored in position 1023 is used as a index variable to access the different elements user inputted values.
```
LOAD M(1024)
STOR M(1023)
```
The loop starts by loading the index variable and subtracting 1 and storing the index back.
```
LOAD M(1023)    ; Loop starts
SUB M(1001)
STOR M(1023)
```
If the index, which is still in the `AC`, is positive, skip an instruction to keep gathering values. Otherwise, do nothing and execute the unconditional `JUMP` instruction that exits the loop.
```
JUMP +M(6,0:19) ; Skips the next instruction.
JUMP M(8,0:19)  ; Jumps out of the loop.
```
Finally, add the base address to the array in memory so the `AC` has the memory position of the current value. Use a `STOR` instruction to get this memory address from the `AC` and modify the next `INPUT` instruction. Gather the decimal value from the user and store in the memory address modified by the `STOR` and jump at the beginning of the loop.
```
ADD M(1004)
STOR M(7,8:19)  ; Modify the address of the next instruction.
INPUT M(0)
JUMP M(3,20:39) ; Loop ends
```

#### Bubbling Sort
The Bubble sort algorithm requires a flag that keeps track if any value has been swapped and (memory position `1022`), an index that keeps the current position in the array (memory position `1023`).

First, initialize the number of swap counter to zero:
```
LOAD M(1002)            ; BEGINNING OF THE OUTER LOOP: Set the SWAPPED flag to 0.
STOR M(1022)
```
Then, a loop similar to the one used to gather the user values is used to iterate the elements of the array but, starting at the penultimate element of the array instead of the last.
```
LOAD M(1024)
SUB M(1001)
STOR M(1023)
LOAD M(1023)
SUB M(1001)
STOR M(1023)
JUMP +M(13,0:19)
JUMP M(26,20:39)
```
In the body of the loop, get the current value and store it in memory position `1021`:
```
ADD M(1004)
STOR M(14,8:19)
LOAD M(0)
STOR M(1021)
```
and then, get the next value and store it in memory position `1020`:
```
LOAD M(1023)
ADD M(1001)   
ADD M(1004)
STOR M(17,8:19)
LOAD M(0)
STOR M(1020)
```
Check if the two values have to be swapped by subtracting the two gathered values. The result is greater than zero if `A[i] >= A[i + 1]`. In this case, do nothing and jump at the beginning of the inner loop.
```
SUB M(1021)
JUMP +M(10,20:39)
```
Otherwise, the two values need to be swapped. First, increase the swap counter:
```
LOAD M(1022)
ADD M(1001)
STOR M(1022)            ; Increase SWAPPED counter.
```
And then, store the values stored at `1020` and `1021` in the swapped positions by computing the destination addresses and storing the previously saved values.
```
LOAD M(1023)            ; Save A[i] copy (saved@1021) into A[i + 1]
ADD M(1001)
ADD M(1004)
STOR M(23,8:19)
LOAD M(1021)
STOR M(0)
LOAD M(1023)            ; Save A[i + 1] copy (saved#1020) into A[i]
ADD M(1004)
STOR M(25,28:39)
LOAD M(1020)
STOR M(0)
JUMP M(10,20:39)        ; GO TO THE BEGINNING OF THE INNER LOOP.
```
Once all elements has been processed, check how many elements have been swapped. If the value is greater than one, go to the beginning of the loop to start again. Otherwise, keep going and display the obtained result.
```
LOAD M(1022)            ; Check if the values have been swapped.
SUB M(1001)             ; Sub 1 to jump only when SWAPPED > 0.
JUMP +M(8,0:19)         ; GO TO THE BEGINNING OF THE OUTER LOOP.
```

#### Displaying the information

Finally, the program displays the sorted elements. This code is exactly the same code as the program that gathers the user values but replacing the `INPUT M(X)` instruction by a `DISP M(X)` instruction to show the memory values:

```
LOAD M(1024)            ; Set the beginning of the index to N.
STOR M(1023)            ;
LOAD M(1023)            ; BEGINNING OF THE LOOP: index := index - 1
SUB M(1001)             ;  
STOR M(1023)            ; 
JUMP +M(31,20:39)       ; If index >= 0, skip the next instruction
JUMP M(33,20:39)        ; Otherwise, EXIT LOOP.
ADD M(1004)             ; Add the base address of the array to the index.
STOR M(32,28:39)        ; Modify the index of the DISP destination.
DISP M(0)               ; 
JUMP M(29,0:19)         ; Go back to the start of the loop.
STOP
```

#### The program
The resulting program is stored in the first 34 memory positions of the IAS computer emulator and is as follows:

|Pos| Hex | Left | Right |
|---:|:------------:|:------------------|:------------------|
|  0 | `E0400013E8` | `INPUT M(1024)`|`LOAD M(1000)`|
|  1 | `0640010002` | `SUB M(1024)`|` JUMP +M(2,20:39)`|
|  2 | `FF00001400` | `STOP`|`LOAD M(1024)`|
|  3 | `213FF013FF` | `STOR M(1023)`|`LOAD M(1023)`|
|  4 | `063E9213FF` | `SUB M(1001)`|`STOR M(1023)`|
|  5 | `0F0060D008` | `JUMP +M(6,0:19)`|`JUMP M(8,0:19)`|
|  6 | `053EC12007` | `ADD M(1004)`|`STOR M(7,8:19)`|
|  7 | `E00000E003` | `INPUT M(0)`|`JUMP M(3,20:39)`|
|  8 | `013EA213FE` | `LOAD M(1002)`|`STOR M(1022)`|
|  9 | `01400063E9` | `LOAD M(1024)`|`SUB M(1001)`|
| 10 | `213FF013FF` | `STOR M(1023)`|`LOAD M(1023)`|
| 11 | `063E9213FF` | `SUB M(1001)`|`STOR M(1023)`|
| 12 | `0F00D0E01A` | `JUMP +M(13,0:19)`|`JUMP M(26,20:39)`|
| 13 | `053EC1200E` | `ADD M(1004)`|`STOR M(14,8:19)`|
| 14 | `01000213FD` | `LOAD M(0)`|`STOR M(1021)`|
| 15 | `013FF053E9` | `LOAD M(1023)`|`ADD M(1001)`|
| 16 | `053EC12011` | `ADD M(1004)`|`STOR M(17,8:19)`|
| 17 | `01000213FC` | `LOAD M(0)`|`STOR M(1020)`|
| 18 | `063FD1000A` | `SUB M(1021)`|`JUMP +M(10,20:39)`|
| 19 | `013FE053E9` | `LOAD M(1022)`|`ADD M(1001)`|
| 20 | `213FE013FF` | `STOR M(1022)`|`LOAD M(1023)`|
| 21 | `053E9053EC` | `ADD M(1001)`|`ADD M(1004)`|
| 22 | `12017013FD` | `STOR M(23,8:19)`|`LOAD M(1021)`|
| 23 | `21000013FF` | `STOR M(0)`|`LOAD M(1023)`|
| 24 | `053EC13019` | `ADD M(1004)`|`STOR M(25,28:39)`|
| 25 | `013FC21000` | `LOAD M(1020)`|`STOR M(0)`|
| 26 | `0E00A013FE` | `JUMP M(10,20:39)`|`LOAD M(1022)`|
| 27 | `063E90F008` | `SUB M(1001)`|`JUMP +M(8,0:19)`|
| 28 | `01400213FF` | `LOAD M(1024)`|`STOR M(1023)`|
| 29 | `013FF063E9` | `LOAD M(1023)`|`SUB M(1001)`|
| 30 | `213FF1001F` | `STOR M(1023)`|`JUMP +M(31,20:39)`|
| 31 | `0E021053EC` | `JUMP M(33,20:39)`|`ADD M(1004)`|
| 32 | `13020F4000` | `STOR M(32,28:39)`|`DISP M(0)`|
| 33 | `0D01DFF000` | `JUMP M(29,0:19)`|`STOP`|


