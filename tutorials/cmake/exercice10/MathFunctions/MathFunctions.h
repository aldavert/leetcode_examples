// Using the module 'GenerateExportHeader' these preprocessor commands can be
// generated automatically:
#if 0
#if defined(_WIN32)
#if defined(EXPORTING_MYMATH)
#define DECLSPEC __declspec(dllexport)
#else
#define DECLSPEC __declspec(dllimport)
#endif
#else
#define DECLSPEC
#endif
#else
#include "mathfunctions_export.h"
#endif

namespace mathfunctions {
//double sqrt(double x);
//double DECLSPEC sqrt(double x);
double MATHFUNCTIONS_EXPORT sqrt(double x);
}


