#include "mysqrt.h"

// THIS IS NOT NEEDED // #include <cmath>
#include <iostream>
#include "Table.h"

namespace mathfunctions {
namespace detail {
// a hack square root calculation using simple operations
double mysqrt(double x)
{
  if (x <= 0) {
    return 0;
  }

  // if we have both log and exp then use them
// THIS IS NOT NEEDED // #if defined(HAVE_LOG) && defined(HAVE_EXP)
// THIS IS NOT NEEDED //   double result = std::exp(std::log(x) * 0.5);
// THIS IS NOT NEEDED //   std::cout << "Computing sqrt of " << x << " to be " << result
// THIS IS NOT NEEDED //             << " using log and exp" << std::endl;
// THIS IS NOT NEEDED // #else
  double result = x;
  // NEW CODE THAT USES THE TABLE ----------------------------------------------
  if ((x >= 1) && (x < 10))
  {
      std::cout << "Use the table to help find an initial value.\n";
      result = sqrtTable[static_cast<int>(x)];
  }
  // NEW CODE THAT USES THE TABLE ----------------------------------------------

  // do ten iterations
  for (int i = 0; i < 10; ++i) {
    if (result <= 0) {
      result = 0.1;
    }
    double delta = x - (result * result);
    result = result + 0.5 * delta / result;
    std::cout << "Computing sqrt of " << x << " to be " << result << std::endl;
  }
// THIS IS NOT NEEDED // #endif
  return result;
}
}
}
