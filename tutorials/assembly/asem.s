# syscall table: blog.rchapman.org/posts/Linux_System_Call_Table_for_x86_64/

.global _start
.intel_syntax noprefix

_start:
    mov rdi, 8
    mov rsi, rdi
    mov rax, 1          # Call sys_write (rax=1, rdi=fd, rsi=<char *>, rdx=<count>)
    mov rdi, 1          # rdi=1 -> STDOUT
    lea rsi, [hello_world]
    mov rdx, 14
    syscall
    
    mov rax, 60         # Call sys_exit (rax=60, rdi=<error code>)
    mov rdi, 0
    syscall 



hello_world:
    .asciz "Hello, World\n"
