#include <vector>
{
#if 0
    double x[5] = {1, 2, 3, 4, 5};
    double y[5] = {1, 4, 9, 16, 5};
    TGraph gr(5, x, y);
    TCanvas c1;
    gr.Draw("AC*"); // <- Options to draw the Graph:
                    //    A: Axis are drawn around the graph.
                    //    C: The line is draw as a smooth curve. Use 'L' to draw
                    //       a simple polyline.
                    //    *: Marker used to show the location of the original points.
#elif 1
    std::vector<double> xo = {1, 2, 3, 4, 5}, yo = {1, 4, 9, 16, 5};
    TGraph gr(xo.size(), xo.data(), yo.data());
    TCanvas c1;
    gr.Draw("AC*");
#else
    std::vector<double> xo = {1, 2, 3, 4, 5}, yo = {1, 4, 9, 16, 5};
    TVectorD x(xo.size(), xo.data()), y(yo.size(), yo.data());
    TGraph gr(x, y);
    TCanvas c1;
    gr.Draw("AL*");
#endif
}
