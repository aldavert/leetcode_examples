void session06_01(void)
{
    std::ifstream file("data06.txt");
    if (file.is_open())
    {
        TGraph * gr = new TGraph();
        gr->SetMarkerStyle(kFullCircle);
        gr->SetMarkerSize(1.5);
        gr->SetLineWidth(2);
        gr->SetLineColor(kRed);
        gr->SetTitle("Graph");
        gr->GetXaxis()->SetTitle("X Values");
        gr->GetYaxis()->SetTitle("Y Values");
        
        while (!file.eof())
        {
            double x, y;
            file >> x >> y;
            gr->SetPoint(gr->GetN(), x, y);
        }
        file.close();
        
        TCanvas * c1 = new TCanvas();
        c1->SetGrid();
        gr->Draw("ALP");
    }
    else std::cerr << "Cannot load file 'data06.txt' from disk.\n";
}
