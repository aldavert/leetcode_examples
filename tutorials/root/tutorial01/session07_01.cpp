void session07_01(void)
{
    TRandom2 * rand = new TRandom2(1);
    TH1F * hist = new TH1F("hist", "Histogram", 100, 0, 100);
    TH1F * hist2 = new TH1F("hist2", "Histogram", 25, 0, 100);
    for (int i = 0; i < 10'000; ++i)
    {
        double r = rand->Rndm() * 100;
        //std::cout << r << '\n';
        hist->Fill(r);
        hist2->Fill(r);
    }
    TCanvas * c1 = new TCanvas();
    c1->SetGrid();
    hist->GetYaxis()->SetRangeUser(0, 200);
    hist->Draw();
    TCanvas * c2 = new TCanvas();
    c2->SetGrid();
    hist2->GetYaxis()->SetRangeUser(0, 500);
    hist2->Draw();
}
