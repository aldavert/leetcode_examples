void session08_01(void)
{
    const double mean = 5.0;
    const double std_dev = 1.0;
    TH1F * hist = new TH1F("hist", "Histogram", 100, 0, 10);
    TRandom2 * rand = new TRandom2(3);
    std::ofstream file_out("data08.txt");
    for (int i = 0; i < 1'000; ++i)
        file_out << rand->Gaus(mean, std_dev) << '\n';
    file_out.close();
    
    std::ifstream file_in("data08.txt");
    while (true)
    {
        double value;
        file_in >> value;
        hist->Fill(value);
        if (file_in.eof()) break;
    }
    file_in.close();
    hist->GetXaxis()->SetTitle("Distribution");
    hist->GetYaxis()->SetTitle("Entries");
    TCanvas * c1 = new TCanvas();
    c1->SetGrid();
    hist->Draw();
}
