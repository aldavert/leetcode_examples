# ROOT tutorial by '*Physics Matters*':

The tutorial can be found in this [YouTube playlist](https://www.youtube.com/watch?v=KPz-dNjdx40&list=PLLybgCU6QCGWLdDO4ZDaB0kLrO3maeYAe&index=1).

## Session 01: Introduction
- Basic code to create a histogram: [session01_01.cpp](session01_01.cpp).
    - The code can be executed by running: `root session01_01.cpp` or by using the command `.L <filename>` or `.x <filename>` in the ROOT interpreter.
    - **Exit the ROOT interpreter** by typing the `.q` or `.quit` commands.
    - When creating scripts (global code outside of any function), the code must go within brackets or ROOT will complain.
- Basic code to create a graph: [session01_02.cpp](session01_02.cpp).
    - In scripts, C preprocessor directives like `#if`, `#elif` or `#endif` also need to be inside the brackets.

## Session 02: Installation
- Show how to install the ROOT manually from sources. In Fedora 36 ROOT is available in the repos.

## Session 03: Histograms
- How to create histograms in more detail: [session03_01.cpp](session_03_01.cpp).
    - When calling ROOT with a source file as a parameter, ROOT will only execute the function that has the same name as the file. In this case, the function has to be defined as `void session03_01(void)` to be loaded directly by root. Other functions can be called afterwards as function calls from the interpreter.
    - The function `Fill` adds values into the histogram. So, in order to create a histogram of a vector `data` with `data.size()` values we can do: `for (auto value : data) hist->Fill(value);`. The resulting histogram will calculate the mean and standard deviation of the histogram.
    - The histogram object has member functions that allow us to access to all the elements of the graph and modify them.
        - `GetXaxis()` and `GetYaxis()` give axis respectively to the `X` and `Y` axis of the graph, with the function `SetTitle` then we can change the title of the axes (e.g. `hist->GetXaxis()->SetTitle("X Axis");`).
        - `SetXTitle()` and `SetYTitle()` allow to the same directly (e.g. `hist->SetXTitle("X Axis");`).
        - `SetTitle()` allows modify the title that was set using the histogram constructor.
        - ...
    - He is creating all objects in the HEAP instead of using the STACK. This can create memory leaks but he says that he prefers this way (??).

## Session 04: Graphs
- How to create simple graphs to plot functions (looks simpler than matplotlib): [session_04_01.cpp](session_04_01).
- The file contains some of the basic functions to modify the graph.
- The functions of root does not accept STD library containers as parameters, they have to be passed as pointers or points must be introduced in one of the ROOT containers.

## Session 05: Histogram read from file

## Session 06: Graph read from file
- Member function `GetN()` of the `TGraph` object returns the number of points added into the graph.

## Session 07: Random number generators
- ROOT recommends the usage of `TRandom2` over `TRandom` or `TRandom3`.
- `TRandom2` initializes the random seed using a default value. This value can be changed in the constructor or we can set the value to `0` to initialize the seed randomly.

## Session 08: Generating and Fitting a Gaussian Distribution
- In the histogram window, we can select `Tools->Fit Panel` to open the parameters to fit a curve on the histogram. The fitting panel contains all the parameters that can be tuned to select and fit the curve on the data. Using the default parameters usually works well though.

## Session 09: Fit function and parameters.
- Fit the function using code instead the GUI.
- `TF1` defines a function object that can be used to fit data
- You can pass an initial estimation of the parameters of the function object `TF1` with the member function `SetParameter`, where you introduce the parameter number/name and its associated value for each parameter independently. In the case of the Gaussian function you need a *constant*, *mean* and *standard deviation*. The number/name can be found in the help or they are printed by the function `Fit` in the standard output when showing the estimated values on screen.
- The function `Fit("<function>", "<params>")` of `TH1F` fits the data to the given function. The parameters can set to fit the function only on the range of data (`R`), to not display the fitted curve parameter on the screen as text (`Q`) or to not display the fitted function on the current graph (`0`).
