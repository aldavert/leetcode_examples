void session05_01(void)
{
    std::ifstream file("data05.txt");
    if (file.is_open())
    {
        TH1F * hist = new TH1F("hist", "Histogram", 6, 1, 7);
        while (!file.eof())
        {
            double value;
            file >> value;
            hist->Fill(value);
        }
        file.close();
        hist->GetXaxis()->SetTitle("Grade");
        hist->GetYaxis()->SetTitle("Entries");
        TCanvas * c1 = new TCanvas();
        hist->Draw();
    }
    else std::cerr << "Cannot load file 'data05.txt' from disk.\n";
}
