{   // <-- Adding the code between brackets it's important or ROOT won't load the
    //     code in script-like files (functions doesn't need to be defined within
    //     a local scope '{...}').
    // TH1F: Histogram 1D of Floats
    //         +- Name of the histogram
    //         |       +- Histogram title
    //         |       |           +- Number of bins
    //         |       |           |     +- Low edge of the first bin
    //         |       |           |     |  +- Upper edge of the last bin.
    //         |       |           |     |  |
    //         v       v           v     v  v
    TH1F hist("hist", "Histogram", 100, -5, 5);
    TRandom2 r;

    for (int i = 0; i < 1000; ++i)
    {
        hist.Fill(r.Gaus());
    }

    TCanvas * c1 = new TCanvas();
    hist.Draw();
    hist.Fit("gaus");
}

