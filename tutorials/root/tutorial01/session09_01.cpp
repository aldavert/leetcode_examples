void session09_01(void)
{
    const double mean = 5.0;
    const double std_dev = 1.0;
    TH1F * hist = new TH1F("hist", "Histogram", 100, 0, 10);
    TRandom2 * rand = new TRandom2(3);
    std::ofstream file_out("data08.txt");
    for (int i = 0; i < 1'000; ++i)
        file_out << rand->Gaus(mean, std_dev) << '\n';
    file_out.close();
    
    std::ifstream file_in("data08.txt");
    while (true)
    {
        double value;
        file_in >> value;
        hist->Fill(value);
        if (file_in.eof()) break;
    }
    file_in.close();
    hist->GetXaxis()->SetTitle("Distribution");
    hist->GetYaxis()->SetTitle("Entries");
    TCanvas * c1 = new TCanvas();
    c1->SetGrid();
    hist->Draw();
    // Add code to session08_01.cpp here to fit the Gaussian over the data.
#if 0 // Direct method
    hist->Fit("gaus");
#else // Defining a function object.
    //                   +- Name of the function
    //                   |      +- Function used to fit the data. gaus is defined by ROOT
    //                   |      |      +- Minimum X value (interval).
    //                   |      |      |   +- Maximum X value (interval).
    //                   v      v      v   v
    TF1 * fit = new TF1("fit", "gaus", 0, 10);
    fit->SetParameter("Constant", 3.89815e+01); // Constant
    fit->SetParameter("Mean", 5.07077e+00);  // Mean
    fit->SetParameter("Sigma", 9.69988e-01);  // Standard deviation
    hist->Fit("fit", "R"); // Put the label of the user defined function.
#endif
}
