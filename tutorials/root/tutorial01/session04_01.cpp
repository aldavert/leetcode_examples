void session04_01(void)
{
    double x[5] = {1,  2,  3,  4,  5};
    double y[5] = {1,  4,  9, 16, 25};
    TGraph * gr = new TGraph(5, x, y);
    gr->SetMarkerStyle(kFullCircle);
    gr->SetMarkerSize(1.5);
    gr->SetLineWidth(2);
    TCanvas * c1 = new TCanvas();
    c1->SetGrid();
    gr->Draw("ACP");
}
