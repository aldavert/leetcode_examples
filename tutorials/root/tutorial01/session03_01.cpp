void session03_01(void)
{
    TH1F * hist = new TH1F("hist", "Histogram", 10, 0, 100);
    hist->Fill(10);
    hist->Fill(90);
    std::vector<int> values = {2, 4, 1, 4, 7, 6, 0, 1, 3, 9, 4, 2, 3, 5, 9};
    hist->GetXaxis()->SetTitle("X Axis");
    hist->GetYaxis()->SetTitle("Y Axis");
    for (int value : values) hist->Fill(value * 10);
    TCanvas * c1 = new TCanvas();
    hist->Draw();
}
