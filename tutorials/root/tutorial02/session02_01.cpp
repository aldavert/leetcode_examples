{
    TFile * f = new TFile("data/test.root", "recreate");
    f->ls();
    TH1F h0("h0", "histro0", 100, -10, 10); // The "h0" and "h1" are the keys corresponding to
    TH1F h1("h1", "histro1", 100, -10, 10); // each object so we can know which object is which
    h0.FillRandom("gaus", 100);             // when we load them from the file (similar to the
    h1.FillRandom("gaus", 100);             // key of a python's hash table).
    h0.Write();
    h1.Write();
    f->ls();
    
    h0.FillRandom("gaus", 200);
    h0.Draw();
    h0.Write();
    f->ls();
    f->Close();
}
