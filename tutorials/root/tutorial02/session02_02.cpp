{
    TFile f1("data/test1.root", "recreate");
    TFile f2("data/test2.root", "recreate");
    f1.cd(); // Sets f1 as the active file.
    TH1F h0("h0", "histro0", 100, -10, 10);
    h0.Write();
    std::cout << "[COMMAND] f1.ls():\n";
    std::cout << "-------------------------------------\n";
    f1.ls();
    std::cout << '\n';
    std::cout << "[COMMAND] f2.ls():\n";
    std::cout << "-------------------------------------\n";
    f2.ls();
    std::cout << '\n';
    f2.cd();
    h0.Write();
    std::cout << "[COMMAND] f2.ls():\n";
    std::cout << "-------------------------------------\n";
    f2.ls();
    std::cout << '\n';
    f1.cd();
    f1.mkdir("folder");
    f1.cd("folder");
    h0.Write();
    std::cout << "[COMMAND] f1.ls():\n";
    std::cout << "-------------------------------------\n";
    f1.ls();
    std::cout << '\n';
}
