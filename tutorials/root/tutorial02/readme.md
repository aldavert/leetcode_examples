# ROOT tutorial by '*Dai Xination*':

The tutorial can be found in this [YouTube playlist](https://www.youtube.com/watch?v=LfSFGkkBT-k&list=PLJZI0Nq8pgrScd_mR_ruxXD7N8dxFZtXv&index=1).

## Session 01: Installation

## Session 02: I/O
- The object used to read/write to disk in ROOT is TFile (similar to python's pickle?): [session02_01.cpp](session02_01.cpp)
    - You pass to the object the filename and which mode is used on the file ("recreate" to overwrite, "update" to append and *nothing* to read:
        - TFile f("demo.root", "recreate");
        - TFile f("demo.root");
        - TFile f("demo.root", "update");
    - f.Close() to close the file (C is capital!).
    - f.ls() to list what it is in the ROOT file.
    - Any (ROOT) object which uses the command .Write() will save its content to the text file.
    - Use [TAB] in the interactive editor to display all the object member functions.
    - The command `new TBrowser` will open a *incognito mode tab* with the Browser app which allows us to check the elements stored in a root file.
    - To load the content stored in a file (in a new session), we only have to do:
```
root [0] TFile f("data/test.root");
root [1] f.ls();
TFile**         data/test.root
 TFile*         data/test.root
  KEY: TH1F     h0;2    histro0 [current cycle]
  KEY: TH1F     h0;1    histro0 [backup cycle]
  KEY: TH1F     h1;1    histro1
root [2] h0->Draw(); // Displays the last version of the object h0
```
- Other commands: [session02_02.cpp](session02_02.cpp)
    - `<TFile object>.cd()`: Changes the TFile object that is currently active. By default the one that's active is the last that has been created.
    - `gDirectory->pwd()`: Displays the file that is active.
    - `<TFile object>.mkdir(name, title)`: Create a directory/partition inside the file.
    - `<TFile object>.Delete(label)`: Deletes an object or the content of an entire directory from a file.
    - `<TFile object>.rmdir(lable)`: Deletes the folder.
