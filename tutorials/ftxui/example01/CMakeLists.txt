cmake_minimum_required (VERSION 3.11)
project(example01 LANGUAGES CXX VERSION 1.0.0)

add_executable(example01 main.cpp)
# Check of FTXUI is on the system.
find_library(FTXUI_DOM ftxui-dom)
find_library(FTXUI_SCREEN ftxui-screen)
find_library(FTXUI_COMPONENT ftxui-component)
if (NOT (FTXUI_DOM OR FTXUI_SCREEN OR FTXUI_COMPONENT))
    # If it's not on the system download it...
    include(FetchContent)
    set(FETCHCONTENT_UPDATES_DISCONNECTED TRUE)
    FetchContent_Declare(ftxui
      GIT_REPOSITORY https://github.com/ArthurSonzogni/ftxui
      GIT_TAG main # Important: Specify a version or a commit hash here.
    )
    FetchContent_MakeAvailable(ftxui)
    target_link_libraries(example01
      PRIVATE ftxui::screen
      PRIVATE ftxui::dom
      PRIVATE ftxui::component # Not needed for this example.
    )
else()
    # ... otherwise use the system's library.
    find_library(ftxui-dom NAMES ftxui-dom)
    find_library(ftxui-screen NAMES ftxui-screen)
    find_library(ftxui-component ftxui-component NAMES ftxui-component)
    target_link_libraries(example01 ${ftxui-dom} ${ftxui-screen} ${ftxui-component})
endif()

