#include <ftxui/dom/elements.hpp>
#include <ftxui/screen/screen.hpp>
#include <ftxui/component/screen_interactive.hpp>
#include <ftxui/component/component.hpp>
#include <ftxui/component/captured_mouse.hpp>
#include <iostream>

void put(ftxui::Screen &screen, int x, int y, ftxui::Pixel color, const char * text)
{
    for (int i = 0; text[i] != '\0'; ++i)
    {
        auto &pixel = screen.PixelAt(x + i, y);
        pixel = color;
        pixel.character = text[i];
    }
}

/***
    // Default color: ftxui::Color::Default
    ftxui::Pixel color_put;
    color_put.bold = true;
    color_put.underlined = true;
    color_put.background_color = ftxui::Color::Blue;
    color_put.foreground_color = ftxui::Color::Yellow;
    put(screen, screen.dimx() / 2 - 1, screen.dimy() / 2 - 2, color_put, "Mig");
*/
 
int main(void)
{
    constexpr char long_text[] = "A very long long text:"
        "The quick brown fox jumps over the lazy dog and feels as if he were in "
        "the seventh heaven of typography together with Hermann Zapf, the most "
        "famous artist of the typography."
        "The quick brown fox jumps over the lazy dog and feels as if he were in "
        "the seventh heaven of typography together with Hermann Zapf, the most "
        "famous artist of the typography."
        "The quick brown fox jumps over the lazy dog and feels as if he were in "
        "the seventh heaven of typography together with Hermann Zapf, the most "
        "famous artist of the typography."
        "The quick brown fox jumps over the lazy dog and feels as if he were in "
        "the seventh heaven of typography together with Hermann Zapf, the most "
        "famous artist of the typography.";
    ftxui::Element bar =
    ftxui::hbox({ftxui::text("left")   | ftxui::border | ftxui::color(ftxui::Color::Red),
                 ftxui::text("middle") | ftxui::border | ftxui::flex,
                 ftxui::text("right")  | ftxui::border});
    int value = 50;
    auto slider = ftxui::Slider("Value", &value, 0, 100, 1);
#if 0
    auto screen = ftxui::Screen::Create(
            ftxui::Dimension::Full(),       // Width
            ftxui::Dimension::Fit(document) // Height
            //ftxui::Dimension::Full() // Height
            //ftxui::Dimension::Fixed(14) // Height
        );
    ftxui::Render(screen, document);
    screen.Print();
#else
    auto container = ftxui::Container::Vertical({slider});
    auto render = [&]()
    {
        ftxui::Element vbar = ftxui::vbox(bar,
                ftxui::text("Simple text"),
                ftxui::vtext("Hello"),
                ftxui::separator(),
                ftxui::gauge(0.75) | ftxui::color(ftxui::Color::RGB(255, 180, 0)) | ftxui::bgcolor(ftxui::Color::GrayDark) | ftxui::border,
                ftxui::paragraphAlignJustify(long_text),
                ftxui::separator(),
                slider->Render());
        auto document = ftxui::border(vbar);
        document |= ftxui::border;
        document = ftxui::window(ftxui::text("[ The Window ]"), document);
        return document;
    };
    auto renderer = ftxui::Renderer(container, render);
    auto screen = ftxui::ScreenInteractive::Fullscreen();
    screen.Loop(renderer);
#endif
    return EXIT_SUCCESS;
}


